﻿Imports RenaissanceGlobals
Imports C1.Win.C1Command
Imports Microsoft.Win32

Partial Class VeniceMain

	Private C1ToolbarList As New Dictionary(Of String, C1.Win.C1Command.C1ToolBar)
	Private C1CommandList As New Dictionary(Of String, C1.Win.C1Command.C1Command)
	Private Const ToolbarRegistryKey As String = REGISTRY_BASE & "Toolbars"
	Private Const CurrentMinButtonSize As Integer = 32


	Private Sub SetMenuCommand(ByVal ParentMenuName As String, ByRef ItemArray As ToolStripItemCollection)
		' **********************************************************************
		' Iterate through Venice Menu structure building Command objects and toolbars
		' to match.
		' **********************************************************************

		Dim thisItem As ToolStripItem
		Dim thisMenuItem As ToolStripMenuItem
		Dim ThisFeatureName As String
		Dim NewCommand As C1Command

		' Custom ToolBar

		Try

			If (Not C1ToolbarList.ContainsKey("Custom")) Then
				Dim NewToolBar As New C1ToolBar

				NewToolBar.Name = "C1Toolbar_Custom"
				NewToolBar.Text = "Custom"
				NewToolBar.AutoSize = True
				NewToolBar.CustomizeButton = True
				NewToolBar.MinButtonSize = CurrentMinButtonSize
				NewToolBar.Movable = True
				NewToolBar.ShowToolTips = True
				NewToolBar.ToolBarStyle = ToolBarStyleEnum.Default
				NewToolBar.Wrap = False
				NewToolBar.VisualStyle = VisualStyle.Office2003Blue
				NewToolBar.CommandHolder = Me.VeniceCommandHolder

				C1ToolbarList.Add("Custom", NewToolBar)
				NewToolBar = Nothing
			End If

		Catch ex As Exception
		End Try

		' Menu Based Toolbars

		For Each thisItem In ItemArray
			Try
				If (thisItem IsNot Nothing) Then

					If (TypeOf thisItem Is ToolStripMenuItem) Then
						thisMenuItem = CType(thisItem, ToolStripMenuItem)

						If (thisMenuItem.DropDownItems.Count > 0) Then
							If (ParentMenuName.Length <= 0) Then
								Dim MenuItemText As String = thisMenuItem.Text.Replace("&", "")

								If (Not C1ToolbarList.ContainsKey(MenuItemText)) Then
									Dim NewToolBar As New C1ToolBar

									NewToolBar.Name = "C1Toolbar_" & MenuItemText
									NewToolBar.Text = MenuItemText
									NewToolBar.AutoSize = True
									NewToolBar.CustomizeButton = True
									NewToolBar.MinButtonSize = CurrentMinButtonSize
									NewToolBar.Movable = True
									NewToolBar.ShowToolTips = True
									NewToolBar.ToolBarStyle = ToolBarStyleEnum.Default
									NewToolBar.Wrap = False
									NewToolBar.VisualStyle = VisualStyle.Office2003Blue
									NewToolBar.CommandHolder = Me.VeniceCommandHolder

									C1ToolbarList.Add(MenuItemText, NewToolBar)
									NewToolBar = Nothing
								End If

								' Iterate down the Menu Item.

								SetMenuCommand(MenuItemText, thisMenuItem.DropDownItems)

							Else

								' Iterate down the Menu Item.

								SetMenuCommand(ParentMenuName, thisMenuItem.DropDownItems)

							End If

						ElseIf (thisItem.Tag Is Nothing) Then

							'More_Code_Here("Special Cases")

						Else ' thisMenuItem has no children & has a Tag set.

							ThisFeatureName = thisItem.Tag.ToString
							NewCommand = Nothing

							NewCommand = New C1Command
							NewCommand.Name = "C1Command_" & ThisFeatureName
							NewCommand.Category = ParentMenuName
							NewCommand.Text = thisItem.Text.Replace("&", "")
							NewCommand.ToolTipText = ParentMenuName & ", " & NewCommand.Text
							NewCommand.UserData = ThisFeatureName
							NewCommand.Visible = True
							NewCommand.Icon = Nothing
							NewCommand.Image = Nothing

							If System.Enum.IsDefined(GetType(VeniceFormID), ThisFeatureName) Then
								' Tag refers to an existing Form. (VeniceFormID used here as an index of existing forms).

								If (Not C1CommandList.ContainsKey(ThisFeatureName)) Then

									AddHandler NewCommand.Click, AddressOf Generic_FormMenu_Click
									C1CommandList.Add(ThisFeatureName, NewCommand)
									VeniceCommandHolder.Commands.Add(NewCommand)

								Else

									' New Command not needed (Already exists?).

									NewCommand = Nothing

								End If

							Else ' ThisFeatureName is not a recognised form ID.

								If (Not C1CommandList.ContainsKey(ThisFeatureName)) Then

									Select Case ThisFeatureName

										Case "rptRiskPolicyMasterBalanced"

											AddHandler NewCommand.Click, AddressOf Menu_Risk_PolicyReport_MasterBalanced_Click
											C1CommandList.Add(ThisFeatureName, NewCommand)
											VeniceCommandHolder.Commands.Add(NewCommand)

										Case "rptRiskPolicyMasterEvent"

											AddHandler NewCommand.Click, AddressOf Menu_Risk_PolicyReport_MasterEvent_Click
											C1CommandList.Add(ThisFeatureName, NewCommand)
											VeniceCommandHolder.Commands.Add(NewCommand)

										Case "rptRiskPolicyAllFunds"

											AddHandler NewCommand.Click, AddressOf Menu_Risk_PolicyReport_All_Click
											C1CommandList.Add(ThisFeatureName, NewCommand)
											VeniceCommandHolder.Commands.Add(NewCommand)

										Case "ToggleMDI"

											AddHandler NewCommand.Click, AddressOf Menu_File_MDI_Click
											C1CommandList.Add(ThisFeatureName, NewCommand)
											VeniceCommandHolder.Commands.Add(NewCommand)

										Case "ToggleMultithreadedReports"

											AddHandler NewCommand.Click, AddressOf Menu_UseMultithreadedReporting_Click
											C1CommandList.Add(ThisFeatureName, NewCommand)
											VeniceCommandHolder.Commands.Add(NewCommand)

										Case "ToggleReportFadeIn"

											AddHandler NewCommand.Click, AddressOf Menu_File_EnableReportFadeIn_Click
											C1CommandList.Add(ThisFeatureName, NewCommand)
											VeniceCommandHolder.Commands.Add(NewCommand)

										Case Else
											' New Command not needed for unhandled Case.

											NewCommand = Nothing

									End Select

								Else

									' Key already exists

									NewCommand = Nothing

								End If


							End If

							' Set Command Icon & Add to Toolbar.

							If (NewCommand IsNot Nothing) Then

								' Set the Icon / Image for the new Command object

								Dim IconObject As Object = My.Resources.RenaissanceIcons.ResourceManager.GetObject(ThisFeatureName)

								If (IconObject Is Nothing) Then
									IconObject = My.Resources.RenaissanceIcons.ResourceManager.GetObject("DefaultIcon")
								End If

								If (IconObject IsNot Nothing) Then
									If (TypeOf IconObject Is Icon) Then
										NewCommand.Icon = CType(IconObject, Icon)
									ElseIf (TypeOf IconObject Is Bitmap) Then
										NewCommand.Image = CType(IconObject, Image)
									End If
								End If

								' Add Command to the appropriate Tool bar, If Command Exists & Toolbar exists.

								If (C1ToolbarList.ContainsKey(ParentMenuName)) AndAlso (C1CommandList.ContainsKey(ThisFeatureName)) Then

									Dim NewToolBar As C1ToolBar = C1ToolbarList(ParentMenuName)
									Dim LinkExists As Boolean = False
									Dim NewLink As C1CommandLink

									' Look For existing Link to this command.

									For Each ThisLink As C1CommandLink In NewToolBar.CommandLinks
										If ThisLink.Command Is C1CommandList(ThisFeatureName) Then
											LinkExists = True
											Exit For
										End If
									Next

									' Edd Link to Command if necessary.

									If (Not LinkExists) Then
										NewLink = New C1CommandLink(C1CommandList(ThisFeatureName))

										If (C1CommandList(ThisFeatureName).Icon Is Nothing) AndAlso (C1CommandList(ThisFeatureName).Image Is Nothing) Then
											NewLink.ButtonLook = ButtonLookFlags.Text
										Else
											NewLink.ButtonLook = ButtonLookFlags.Image
										End If

										NewToolBar.CommandLinks.Add(NewLink)

									End If

								End If ' Command Exists & Toolbar exists.

							End If ' If (NewCommand IsNot Nothing) Then

						End If

					End If ' If (TypeOf thisItem Is ToolStripMenuItem) Then

				End If ' If (thisItem IsNot Nothing) Then

			Catch ex As Exception
			End Try

		Next



	End Sub

	Private Sub SetCustomToolbar()
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		' Get Cutom Toolbar Items.

		If (C1ToolbarList.ContainsKey("Custom")) Then
			Dim CustomToolBar As C1ToolBar
			Dim RegitryItem As String
			Dim CustomCommandCount As Integer
			Dim NewLink As C1CommandLink

			CustomToolBar = C1ToolbarList("Custom")

			RegitryItem = Get_RegistryItem(Registry.CurrentUser, ToolbarRegistryKey, "Custom_ItemCount", "0")

			If (IsNumeric(RegitryItem)) AndAlso (CInt(RegitryItem) > 0) Then
				CustomCommandCount = CInt(RegitryItem)

				For ItemCounter As Integer = 1 To CustomCommandCount
					RegitryItem = Get_RegistryItem(Registry.CurrentUser, ToolbarRegistryKey, "Custom_Command" & ItemCounter.ToString, "")

					If (C1CommandList.ContainsKey(RegitryItem)) Then

						NewLink = New C1CommandLink(C1CommandList(RegitryItem))

						If (C1CommandList(RegitryItem).Icon Is Nothing) AndAlso (C1CommandList(RegitryItem).Image Is Nothing) Then
							NewLink.ButtonLook = ButtonLookFlags.Text
						Else
							NewLink.ButtonLook = ButtonLookFlags.Image
						End If

						CustomToolBar.CommandLinks.Add(NewLink)

					End If
				Next

			End If

		End If

	End Sub

	Private Sub AddToolbarsToDock()
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Try
			Dim RegitryItem As String

			Me.VeniceCommandDock.Controls.Clear()

			VeniceCommandDock.SuspendLayout()

			For Each ThisToolBar As C1ToolBar In C1ToolbarList.Values
				'  Get Toolbar Defaults

				RegitryItem = CType(Get_RegistryItem(Registry.CurrentUser, ToolbarRegistryKey, ThisToolBar.Text & "_Visible", "False"), String)
				ThisToolBar.Visible = CBool(RegitryItem)

				If (ThisToolBar.Text = "Custom") OrElse (ThisToolBar.CommandLinks.Count > 0) Then
					VeniceCommandDock.Controls.Add(ThisToolBar)
				End If

				RegitryItem = CType(Get_RegistryItem(Registry.CurrentUser, ToolbarRegistryKey, ThisToolBar.Text & "_LocationLeft", "0"), String)
				If (RegitryItem <> "0") Then
					ThisToolBar.Left = CInt(RegitryItem)
				End If
				RegitryItem = CType(Get_RegistryItem(Registry.CurrentUser, ToolbarRegistryKey, ThisToolBar.Text & "_LocationTop", "0"), String)
				If (RegitryItem <> "0") Then
					ThisToolBar.Top = CInt(RegitryItem)
				End If

				RegitryItem = CType(Get_RegistryItem(Registry.CurrentUser, ToolbarRegistryKey, ThisToolBar.Text & "_Floating", "False"), String)
				If (CBool(RegitryItem)) And (Not ThisToolBar.Floating) Then

					Dim FloatLeft As Integer = Me.Left
					Dim FloatTop As Integer = Me.Top

					RegitryItem = CType(Get_RegistryItem(Registry.CurrentUser, ToolbarRegistryKey, ThisToolBar.Text & "_FloatingTop", ""), String)
					If IsNumeric(RegitryItem) Then
						FloatTop = CInt(RegitryItem)
					End If

					RegitryItem = CType(Get_RegistryItem(Registry.CurrentUser, ToolbarRegistryKey, ThisToolBar.Text & "_FloatingLeft", ""), String)
					If IsNumeric(RegitryItem) Then
						FloatLeft = CInt(RegitryItem)
					End If

					VeniceCommandDock.DockOrFloatChild(ThisToolBar, New Point(FloatLeft, FloatTop))
				End If

			Next

			' Set  Context Menu

			VeniceCommandDock.ContextMenuStrip = New ContextMenuStrip
			AddHandler VeniceCommandDock.ContextMenuStrip.Opening, AddressOf cms_Opening_VeniceCommandDock
			VeniceCommandDock.ContextMenuStrip.Items.Add(New ToolStripMenuItem(" ")) ' necessary for the menu to show the first time it is clicked.

		Catch ex As Exception
		Finally

			VeniceCommandDock.ResumeLayout()

		End Try


	End Sub

	Sub cms_Opening_VeniceCommandDock(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
		' *****************************************************************************************
		' Populate Drop-Down menu for the Command Dock control.
		'
		' *****************************************************************************************

		Dim ThisMenuStrip As ContextMenuStrip = Nothing

		' Validate 'Sender'

		If (TypeOf (sender) Is ContextMenuStrip) Then

			ThisMenuStrip = CType(sender, ContextMenuStrip)

		Else

			Exit Sub

		End If

		If (ThisMenuStrip Is Nothing) Then
			Exit Sub
		End If

		' Build Menu :

		Try

			' Clear the ContextMenuStrip control's 
			' Items collection.

			ThisMenuStrip.Items.Clear()

			' Populate CMS with Toolstrip names.

			Dim ThisMenuItem As ToolStripMenuItem

			For Each ThisToolBar As C1ToolBar In C1ToolbarList.Values
				If (ThisToolBar.Text = "Custom") OrElse (ThisToolBar.CommandLinks.Count > 0) Then
					ThisMenuItem = New ToolStripMenuItem(ThisToolBar.Text, Nothing, AddressOf ToolbarCmsMenuClick)
					ThisMenuItem.Checked = ThisToolBar.Visible
					ThisMenuItem.Tag = ThisToolBar

					ThisMenuStrip.Items.Add(ThisMenuItem)
				End If

			Next

			ThisMenuStrip.Visible = True

		Catch ex As Exception
		End Try

	End Sub

	Private Sub ToolbarCmsMenuClick(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		' Toggle Toolstrip visibility on CMS Menu selection.
		'
		' *****************************************************************************************

		Dim ThisToolBar As C1ToolBar
		Dim ThisMenuItem As ToolStripMenuItem

		Try

			ThisMenuItem = CType(sender, ToolStripMenuItem)
			ThisToolBar = CType(ThisMenuItem.Tag, C1ToolBar)
			ThisToolBar.Visible = Not ThisToolBar.Visible
			ThisMenuItem.Checked = ThisToolBar.Visible

		Catch ex As Exception
		End Try

	End Sub

	Private Sub SaveToolbarStatus()
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Try

			For Each ThisToolBar As C1ToolBar In C1ToolbarList.Values
				'  Get Toolbar Defaults

				Set_RegistryItem(Registry.CurrentUser, ToolbarRegistryKey, ThisToolBar.Text & "_Visible", ThisToolBar.Visible.ToString)

				Set_RegistryItem(Registry.CurrentUser, ToolbarRegistryKey, ThisToolBar.Text & "_LocationLeft", ThisToolBar.Left.ToString)
				Set_RegistryItem(Registry.CurrentUser, ToolbarRegistryKey, ThisToolBar.Text & "_LocationTop", ThisToolBar.Top.ToString)

				Set_RegistryItem(Registry.CurrentUser, ToolbarRegistryKey, ThisToolBar.Text & "_Floating", ThisToolBar.Floating.ToString)
				If (ThisToolBar.Floating) Then
					Set_RegistryItem(Registry.CurrentUser, ToolbarRegistryKey, ThisToolBar.Text & "_FloatingTop", ThisToolBar.Parent.Top.ToString)
					Set_RegistryItem(Registry.CurrentUser, ToolbarRegistryKey, ThisToolBar.Text & "_FloatingLeft", ThisToolBar.Parent.Left.ToString)

				End If
			Next

			' Save Custom Toolbar items
			Dim CustomToolBar As C1ToolBar

			If (C1ToolbarList.ContainsKey("Custom")) Then
				CustomToolBar = C1ToolbarList("Custom")

				Set_RegistryItem(Registry.CurrentUser, ToolbarRegistryKey, "Custom_ItemCount", CustomToolBar.CommandLinks.Count.ToString)

				For ItemCounter As Integer = 0 To (CustomToolBar.CommandLinks.Count - 1)
					Set_RegistryItem(Registry.CurrentUser, ToolbarRegistryKey, "Custom_Command" & (ItemCounter + 1).ToString, CustomToolBar.CommandLinks(ItemCounter).Command.UserData.ToString)
				Next

			End If


		Catch ex As Exception
		End Try

	End Sub


End Class
