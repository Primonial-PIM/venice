' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="UnitHolderFeeCalcs.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Option Explicit On
'Option Strict On

Imports System.Data.SqlClient
Imports System.Math

Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals

Imports RenaissanceDataClass
Imports RenaissanceUtilities.DatePeriodFunctions

Imports RenaissancePertracDataClass.PertracDataClass
Imports System.Threading

''' <summary>
''' Class TransactionPoint
''' </summary>
Public Class TransactionPoint

  ''' <summary>
  ''' The point date
  ''' </summary>
  Public PointDate As Date
  ''' <summary>
  ''' The parent ID
  ''' </summary>
  Public ParentID As Integer
  ''' <summary>
  ''' The instrument ID
  ''' </summary>
  Public InstrumentID As Integer
  ''' <summary>
  ''' The fund ID
  ''' </summary>
  Public FundID As Integer
  ''' <summary>
  ''' The counterparty ID
  ''' </summary>
  Public CounterpartyID As Integer
  ''' <summary>
  ''' The holding
  ''' </summary>
  Public Holding As Double
  ''' <summary>
  ''' The value date
  ''' </summary>
  Public ValueDate As Date
  ''' <summary>
  ''' The price
  ''' </summary>
  Public Price As Double
  ''' <summary>
  ''' The initial equalisation
  ''' </summary>
  Public InitialEqualisation As Double
  ''' <summary>
  ''' The current equalisation
  ''' </summary>
  Public CurrentEqualisation As Double
  ''' <summary>
  ''' The contingent redemption
  ''' </summary>
  Public ContingentRedemption As Double
  ''' <summary>
  ''' The management fees_ gross
  ''' </summary>
  Public ManagementFees_Gross As Double ' IS scaled for thisRedemptionRatio
  ''' <summary>
  ''' The management fees_ net
  ''' </summary>
  Public ManagementFees_Net As Double
  ''' <summary>
  ''' The management fees_ debt rebate
  ''' </summary>
  Public ManagementFees_DebtRebate As Double
  ''' <summary>
  ''' The management fees_ equity rebate
  ''' </summary>
  Public ManagementFees_EquityRebate As Double
  ''' <summary>
  ''' The management fees_ marketing FCAM
  ''' </summary>
  Public ManagementFees_MarketingFCAM As Double
  ''' <summary>
  ''' The management fees_ marketing others
  ''' </summary>
  Public ManagementFees_MarketingOthers As Double

  ''' <summary>
  ''' The gross management fee_ percent
  ''' </summary>
  Public GrossManagementFee_Percent As Double
  ''' <summary>
  ''' The gross performance fee_ percent
  ''' </summary>
  Public GrossPerformanceFee_Percent As Double
  ''' <summary>
  ''' The performance hurdle
  ''' </summary>
  Public PerformanceHurdle As Double
  ''' <summary>
  ''' The performance fees_ accrued gross
  ''' </summary>
  Public PerformanceFees_AccruedGross As Double ' - Is NOT scaled for thisRedemptionRatio
  ''' <summary>
  ''' The performance fees_ gross
  ''' </summary>
  Public PerformanceFees_Gross As Double ' IS scaled for thisRedemptionRatio
  ''' <summary>
  ''' The performance fees_ net
  ''' </summary>
  Public PerformanceFees_Net As Double
  ''' <summary>
  ''' The performance fees_ debt rebate
  ''' </summary>
  Public PerformanceFees_DebtRebate As Double
  ''' <summary>
  ''' The performance fees_ equity rebate
  ''' </summary>
  Public PerformanceFees_EquityRebate As Double
  ''' <summary>
  ''' The performance fees_ marketing FCAM
  ''' </summary>
  Public PerformanceFees_MarketingFCAM As Double
  ''' <summary>
  ''' The performance fees_ marketing others
  ''' </summary>
  Public PerformanceFees_MarketingOthers As Double
  ''' <summary>
  ''' The fxto USD
  ''' </summary>
  Public FxtoUSD As Double
  ''' <summary>
  ''' The fund base currency
  ''' </summary>
  Public FundBaseCurrency As Integer
  ''' <summary>
  ''' The redemption ratio
  ''' </summary>
  Public RedemptionRatio As Double ' -- Percent of position remaining after Redemptions.
  ''' <summary>
  ''' The fee transaction watermark date
  ''' </summary>
  Public FeeTransactionWatermarkDate As Date
  ''' <summary>
  ''' The fee knowledgedate used
  ''' </summary>
  Public FeeKnowledgedateUsed As Date

  ''' <summary>
  ''' Initializes a new instance of the <see cref="TransactionPoint"/> class.
  ''' </summary>
  Public Sub New()
    PointDate = #1/1/1900#
    ParentID = 0
    InstrumentID = 0
    FundID = 0
    CounterpartyID = 0
    Holding = 0
    ValueDate = #1/1/1900#
    Price = 0
    InitialEqualisation = 0
    CurrentEqualisation = 0
    ContingentRedemption = 0
    ManagementFees_Gross = 0
    ManagementFees_Net = 0
    ManagementFees_DebtRebate = 0
    ManagementFees_EquityRebate = 0
    ManagementFees_MarketingFCAM = 0
    ManagementFees_MarketingOthers = 0
    GrossManagementFee_Percent = 0
    GrossPerformanceFee_Percent = 0
    PerformanceHurdle = 0
    PerformanceFees_AccruedGross = 0
    PerformanceFees_Gross = 0
    PerformanceFees_Net = 0
    PerformanceFees_DebtRebate = 0
    PerformanceFees_EquityRebate = 0
    PerformanceFees_MarketingFCAM = 0
    PerformanceFees_MarketingOthers = 0
    FxtoUSD = 1

    FundBaseCurrency = 0
    RedemptionRatio = 1.0 ' -- Percent of position remaining after Redemptions.
    FeeTransactionWatermarkDate = Renaissance_BaseDate
    FeeKnowledgedateUsed = Renaissance_BaseDate

  End Sub
End Class

''' <summary>
''' Class UnitHolderFeeClass
''' </summary>
Public Class UnitHolderFeeClass
  'Implements IDisposable

  ''' <summary>
  ''' The mainform
  ''' </summary>
  Private Mainform As VeniceMain
  ''' <summary>
  ''' The _ status group filter
  ''' </summary>
  Private _StatusGroupFilter As String = DEFAULT_STATUSGROUPFILTER
  ''' <summary>
  ''' The _ administrator dates filter
  ''' </summary>
  Private _AdministratorDatesFilter As Integer = DEFAULT_ADMINISTRATORDATESFILTER

  ''' <summary>
  ''' Prevents a default instance of the <see cref="UnitHolderFeeClass"/> class from being created.
  ''' </summary>
  Private Sub New()
    ' Prevent Instantiation without Mainform parameter.

  End Sub

  ''' <summary>
  ''' Initializes a new instance of the <see cref="UnitHolderFeeClass"/> class.
  ''' </summary>
  ''' <param name="pMainform">The p mainform.</param>
  ''' <param name="pStatusGroupFilter">The p status group filter.</param>
  ''' <param name="pAdministratorDatesFilter">The p administrator dates filter.</param>
  Public Sub New(ByRef pMainform As VeniceMain, ByVal pStatusGroupFilter As String, ByVal pAdministratorDatesFilter As Integer)
    Mainform = pMainform

    ' Used to calculate Unit price information when necessary :

    _StatusGroupFilter = pStatusGroupFilter
    _AdministratorDatesFilter = pAdministratorDatesFilter
  End Sub

  ''' <summary>
  ''' Gets the single period complete fund calculations.
  ''' </summary>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pValueStartDate">The p value start date.</param>
  ''' <param name="pValueEndDate">The p value end date.</param>
  ''' <returns>TransactionPoint[][].</returns>
  Public Function GetSinglePeriodCompleteFundCalculations(ByVal pFundID As Integer, ByVal pValueStartDate As Date, ByVal pValueEndDate As Date) As TransactionPoint()
    ' **************************************************************************************
    ' FundID : Must be supplied, Value of Zero will return indeterminate results.
    '
    '
    ' Returns array of TransactionPoint
    ' Note :
    '		InitialEqualisation, scaled for thisRedemptionRatio
    '		LostEqualisation, scaled for thisRedemptionRatio
    '		ContingentRedemptionFactor, scaled for thisRedemptionRatio
    '		
    '		AccruedPerformanceFee - Gross figure, NOT scaled for thisRedemptionRatio
    '
    '		RealAccruedPerformanceFee - IS scaled for thisRedemptionRatio
    '		ThisPeroidGrossPerformanceFee IS scaled for thisRedemptionRatio
    '		ThisPeriodGrossManagementFee IS scaled for thisRedemptionRatio
    '
    ' **************************************************************************************
    '
    ' Please note that the Equalisation & Contingent redemption calculations in this Function
    ' should mirror the calculations in the GetEqualisationCalculations() Function.
    ' Changes to one should be mirrored in the other.
    '
    ' **************************************************************************************

    If (Mainform Is Nothing) Then
      Return Nothing
    End If

    Dim RVal_ArrayList As New ArrayList
    Dim RVal(-1) As TransactionPoint

    Dim FeesPeriodType As RenaissanceGlobals.DealingPeriod = RenaissanceGlobals.DealingPeriod.Daily
    Dim FundUsesEqualisation As Integer = 0
    Dim FundRow As DSFund.tblFundRow
    Dim InstrumentRow As DSInstrument.tblInstrumentRow

    ' Get Pricing period for this fund

    FundRow = CType(LookupTableRow(Mainform, RenaissanceStandardDatasets.tblFund, pFundID), DSFund.tblFundRow)

    If (FundRow IsNot Nothing) Then
      FeesPeriodType = CType(FundRow.FundPricingPeriod, RenaissanceGlobals.DealingPeriod)
      FundUsesEqualisation = CInt(FundRow.FundUsesEqualisation)

      If (FundRow.FundUnit <> 0) Then
        InstrumentRow = CType(LookupTableRow(Mainform, RenaissanceStandardDatasets.tblInstrument, FundRow.FundUnit), DSInstrument.tblInstrumentRow)
      Else
        InstrumentRow = CType(LookupTableRow(Mainform, RenaissanceStandardDatasets.tblInstrument, 0, "", "InstrumentUnitManagementFees DESC", "InstrumentFundID=" & FundRow.FundID.ToString), DSInstrument.tblInstrumentRow)
      End If
    Else
      Mainform.LogError("UnitHolderFeeClass, GetSinglePeriodCompleteFundCalculations()", LOG_LEVELS.Error, "", "Error getting Fund Details.", "")

      Return RVal
      Exit Function
    End If

    Dim thisDate As Date
    Dim LastPeriod_ThisDate As Date

    thisDate = FitDateToPeriod(FeesPeriodType, pValueEndDate, True)
    LastPeriod_ThisDate = pValueStartDate

    ' SQL Locals : Connection & Command

    Dim thisConnection As SqlConnection = Nothing
    Dim ThisCommand As SqlCommand = Nothing

    ' DataSet Locals :

    Dim Milestones As New DSFundMilestones.tblFundMilestonesDataTable
    Dim Watermarks As New DSFundPerformanceFeePoints.tblFundPerformanceFeePointsDataTable
    Dim FundPrices As New DSPrice.tblPriceDataTable
    Dim LastMonthFundFees As New DSUnitHolderFees.tblUnitHolderFeesDataTable
    Dim Counterparties As DSCounterparty.tblCounterpartyDataTable = Nothing
    Dim LeverageDetails As DataTable = Nothing

    Dim DaysThisYear As Double = (FitDateToPeriod(DealingPeriod.Annually, AddPeriodToDate(DealingPeriod.Annually, pValueEndDate, -1), True) - FitDateToPeriod(DealingPeriod.Annually, pValueEndDate, True)).TotalDays

    ' Start Calcs

    Try
      Dim TempAdaptor As SqlDataAdapter = Nothing

      Dim ThisFundID As Integer
      Dim ThisCounterpartyID As Integer
      Dim ThisInstrumentID As Integer
      Dim thisParentID As Integer
      Dim thisValueDate As Date
      Dim thisSignedUnits As Double
      Dim thisSubscriptionGNAV As Double
      Dim thisRedemptionRatio As Double
      Dim thisFundBasicPerformanceFeePercent As Double
      Dim ThisPendingTransaction As Integer
      Dim ThisTransactionType As Integer
      Dim thisFundBaseCurrency As Integer
      Dim thisFxToUSD As Double
      Dim ThisIsTransfer As Boolean
      Dim ThisEffectivePrice As Double
      Dim ThisEffectiveWatermark As Double
      Dim ThisEffectiveValueDate As Date
      Dim ThisSpecificEqualisationFlag As Boolean
      Dim ThisSpecificEqualisationValue As Double
      Dim ThisTransferValueDate As Date

      ' **************************************************************************************
      ' Function Initialisation
      ' **************************************************************************************

      ' Get Connection

      thisConnection = Mainform.GetVeniceConnection
      If (thisConnection Is Nothing) Then
        ' Exit gracefully.

        Return RVal
      End If


      ' **************************************************************************************
      ' Get Constant data : e.g. Watermarks, Milestones,  Last periods Data.
      ' **************************************************************************************

      ' Get Watermarks

      Try
        TempAdaptor = New SqlDataAdapter

        Mainform.MainAdaptorHandler.Set_AdaptorCommands(thisConnection, TempAdaptor, "tblFundPerformanceFeePoints")
        If (TempAdaptor.SelectCommand.Parameters.Contains("@KnowledgeDate")) Then
          TempAdaptor.SelectCommand.Parameters("@KnowledgeDate").Value = Mainform.Main_Knowledgedate
        End If

        SyncLock thisConnection
          Mainform.LoadTable_Custom(Watermarks, TempAdaptor.SelectCommand)
          'Watermarks.Load(TempAdaptor.SelectCommand.ExecuteReader)
        End SyncLock

      Catch ex As Exception
        Mainform.LogError("UnitHolderFeeClass, GetSinglePeriodCompleteFundCalculations()", LOG_LEVELS.Error, ex.Message, "Error getting Fund Watermarks.", ex.StackTrace)
        Return RVal
      Finally
        If (TempAdaptor IsNot Nothing) Then
          TempAdaptor.SelectCommand.Connection = Nothing
          TempAdaptor.UpdateCommand.Connection = Nothing
          TempAdaptor.InsertCommand.Connection = Nothing
          TempAdaptor.DeleteCommand.Connection = Nothing
          TempAdaptor.SelectCommand = Nothing
          TempAdaptor.UpdateCommand = Nothing
          TempAdaptor.InsertCommand = Nothing
          TempAdaptor.DeleteCommand = Nothing
          TempAdaptor = Nothing
        End If
      End Try

      ' Get Milestones 

      Try
        TempAdaptor = New SqlDataAdapter

        Mainform.MainAdaptorHandler.Set_AdaptorCommands(thisConnection, TempAdaptor, "tblFundMilestones")
        If (TempAdaptor.SelectCommand.Parameters.Contains("@KnowledgeDate")) Then
          TempAdaptor.SelectCommand.Parameters("@KnowledgeDate").Value = Mainform.Main_Knowledgedate
        End If

        SyncLock thisConnection
          Mainform.LoadTable_Custom(Milestones, TempAdaptor.SelectCommand)
          'Milestones.Load(TempAdaptor.SelectCommand.ExecuteReader)
        End SyncLock

      Catch ex As Exception
        Mainform.LogError("UnitHolderFeeClass, GetSinglePeriodCompleteFundCalculations()", LOG_LEVELS.Error, ex.Message, "Error getting Fund Milestones.", ex.StackTrace)
        Return RVal
      Finally
        TempAdaptor.SelectCommand.Connection = Nothing
        TempAdaptor.UpdateCommand.Connection = Nothing
        TempAdaptor.InsertCommand.Connection = Nothing
        TempAdaptor.DeleteCommand.Connection = Nothing
        TempAdaptor.SelectCommand = Nothing
        TempAdaptor.UpdateCommand = Nothing
        TempAdaptor.InsertCommand = Nothing
        TempAdaptor.DeleteCommand = Nothing
        TempAdaptor = Nothing
      End Try

      ' Get Milestoned Prices

      Try
        ThisCommand = New SqlCommand()
        ThisCommand.CommandType = CommandType.StoredProcedure
        ThisCommand.CommandText = "adp_tblPrice_SelectMilestonePrices"
        ThisCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime).Value = Mainform.Main_Knowledgedate

        ThisCommand.Connection = thisConnection

        SyncLock thisConnection
          Mainform.LoadTable_Custom(FundPrices, ThisCommand)
          'FundPrices.Load(ThisCommand.ExecuteReader)
        End SyncLock

      Catch ex As Exception
        Mainform.LogError("UnitHolderFeeClass, GetSinglePeriodCompleteFundCalculations()", LOG_LEVELS.Error, ex.Message, "Error getting Milestone Prices.", ex.StackTrace)
        Return RVal
      Finally
        If (ThisCommand IsNot Nothing) Then
          ThisCommand.Connection = Nothing
          ThisCommand.Parameters.Clear()
          ThisCommand = Nothing
        End If
      End Try

      ' Get Last period Fees (For calculation of Period-on-period figures.

      Try
        ThisCommand = New SqlCommand()
        ThisCommand.CommandType = CommandType.StoredProcedure
        ThisCommand.CommandText = "adp_tblUnitHolderFees_SelectByFund"
        ThisCommand.Parameters.Add("@FundID", SqlDbType.Int).Value = pFundID
        ThisCommand.Parameters.Add("@FeeDate", SqlDbType.DateTime).Value = LastPeriod_ThisDate
        ThisCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime).Value = Mainform.Main_Knowledgedate

        ThisCommand.Connection = thisConnection

        SyncLock thisConnection
          Mainform.LoadTable_Custom(LastMonthFundFees, ThisCommand)
          'LastMonthFundFees.Load(ThisCommand.ExecuteReader)
        End SyncLock

      Catch ex As Exception
        Mainform.LogError("UnitHolderFeeClass, GetSinglePeriodCompleteFundCalculations()", LOG_LEVELS.Error, ex.Message, "Error getting last months UnitHolderFees.", ex.StackTrace)
        Return RVal
      Finally
        If (ThisCommand IsNot Nothing) Then
          ThisCommand.Connection = Nothing
          ThisCommand.Parameters.Clear()
          ThisCommand = Nothing
        End If
      End Try

      ' **************************************************************************************
      ' Prepare for Fee Calcs.
      '
      ' As of Feb 2013 :
      '
      ' Lots to do.
      ' Funds with Equalistion : Performance Fees do not calculate vs a benchmark
      ' Period duration to be checked.
      ' Non-Equalisation Fund calc missing.
      ' etc, etc, etc.....
      ' **************************************************************************************

      Dim Transactions As DataTable
      Dim ThisTransaction As DataRow
      Dim TransactionCounter As Integer

      Dim PendingTransaction_Ordinal As Integer
      Dim Counterparty_Ordinal As Integer
      Dim Fund_Ordinal As Integer
      Dim Instrument_Ordinal As Integer
      Dim TransactionType_Ordinal As Integer
      Dim ValueDate_Ordinal As Integer
      Dim ParentID_Ordinal As Integer
      Dim Price_Ordinal As Integer
      Dim SignedUnits_Ordinal As Integer
      Dim RedemptionRatio_Ordinal As Integer
      Dim IsTransfer_Ordinal As Integer
      Dim EffectivePrice_Ordinal As Integer
      Dim EffectiveWatermark_Ordinal As Integer
      Dim EffectiveValueDate_Ordinal As Integer
      Dim SpecificEqualisationFlag_Ordinal As Integer
      Dim SpecificEqualisationValue_Ordinal As Integer

      Dim InitialWatermarkLevel As Double
      Dim InitialWatermarkDate As Date
      Dim LatestWatermarkLevel As Double
      Dim LatestWatermarkDate As Date
      Dim HighestWatermarkLevel As Double
      Dim HighestWatermarkDate As Date
      Dim GrossedUpLatestWatermark As Double
      Dim GrossedUpHighestWatermarkAfterSubscription As Double
      Dim GrossedUpFundGNAV As Double

      Dim InitialEqualisation As Double
      Dim CurrentEqualisation As Double
      Dim LostEqualisation As Double
      Dim ContingentRedemptionFactor As Double
      Dim AccruedPerformanceFee As Double
      Dim ThisMonthsGrossManagementFee As Double
      Dim ThisMonthsGrossPerformanceFee As Double

      Dim GrossManagementFee_Percent As Double = 0.0#
      Dim GrossPerformanceFee_Percent As Double = 0.0#
      Dim PerformanceFeeHurdle_Percent As Double = 0.0#
      Dim thisTransactionPoint As TransactionPoint = Nothing

      Dim FundNAV As Double
      Dim FundBNAV As Double
      Dim FundGNAV As Double
      Dim FundWatermarkRows As DSFundPerformanceFeePoints.tblFundPerformanceFeePointsRow() = Nothing
      Dim thisCounterpartyRow As DSCounterparty.tblCounterpartyRow = Nothing
      Dim SelectedCounterpartyRows() As DSCounterparty.tblCounterpartyRow
      Dim SelectedFundFees() As DSUnitHolderFees.tblUnitHolderFeesRow

      Dim ThisPeriodStartDate As Date
      Dim ThisFundKnowledgeDate As Date

      ' Get Period Dates.

      ThisPeriodStartDate = FitDateToPeriod(FeesPeriodType, thisDate, False)

      ThisFundID = pFundID

      ' Get Applicable KnowledgeDate
      ThisFundKnowledgeDate = GetFundKnowledgeDate(Milestones, ThisFundID, thisDate, FeesPeriodType)

      If (CBool(FundUsesEqualisation)) Then

        ' **************************************************************************************
        ' Calculate Fees for Each Transaction for given Month.
        ' **************************************************************************************

        ' Get Counterparty Details

        Try
          If (Counterparties IsNot Nothing) Then
            Counterparties.Rows.Clear()
            Counterparties = Nothing
          End If
        Catch ex As Exception
        End Try
        Counterparties = New DSCounterparty.tblCounterpartyDataTable

        Try
          ThisCommand = New SqlCommand()
          ThisCommand.CommandType = CommandType.StoredProcedure
          ThisCommand.CommandText = "adp_tblCounterparty_SelectCommand"
          ThisCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime).Value = ThisFundKnowledgeDate

          ThisCommand.Connection = thisConnection

          SyncLock thisConnection
            Mainform.LoadTable_Custom(Counterparties, ThisCommand)
            'Counterparties.Load(ThisCommand.ExecuteReader)
          End SyncLock

        Catch ex As Exception
          Mainform.LogError("UnitHolderFeeClass, GetSingleMonthCompleteFundCalculations()", LOG_LEVELS.Error, ex.Message, "Error getting Counterparty details.", ex.StackTrace)
          Return RVal
        Finally
          If (ThisCommand IsNot Nothing) Then
            ThisCommand.Connection = Nothing
            ThisCommand.Parameters.Clear()
            ThisCommand = Nothing
          End If
        End Try

        ' Get Leverage Details

        Try
          If (LeverageDetails Is Nothing) Then
            LeverageDetails = New DataTable
          Else
            LeverageDetails.Clear()
          End If

          ThisCommand = New SqlCommand()
          ThisCommand.CommandType = CommandType.StoredProcedure
          ThisCommand.CommandText = "spu_LeverageDetails"
          ThisCommand.Parameters.Add("@CounterpartyID", SqlDbType.Int).Value = 0
          ThisCommand.Parameters.Add("@ValueDate", SqlDbType.DateTime).Value = thisDate
          ThisCommand.Parameters.Add("@HedgePriceVariant", SqlDbType.Int).Value = 0
          ThisCommand.Parameters.Add("@UseMilestoneFundValues", SqlDbType.Int).Value = 1
          ThisCommand.Parameters.Add("@OptionPriceKnowledgeDate", SqlDbType.DateTime).Value = KNOWLEDGEDATE_NOW ' ThisFundKnowledgeDate
          ThisCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime).Value = ThisFundKnowledgeDate

          ThisCommand.Connection = thisConnection

          SyncLock thisConnection
            Mainform.LoadTable_Custom(LeverageDetails, ThisCommand)
            ' LeverageDetails.Load(ThisCommand.ExecuteReader)
          End SyncLock

          ' Adjust HedgeValue and GearingRatio for Equalisation holdings.
          ' This must be done on a Counterparty by Counterparty basis and can not be done by just omiting
          ' Equalisation instruments in the SQL as crystalised Equalisation must not be omitted.

          If LeverageDetails.Rows.Count > 0 Then
            Dim LeverageCounter As Integer
            Dim ReportData As DataTable

            For LeverageCounter = 0 To (LeverageDetails.Rows.Count - 1)
              ReportData = Nothing
              ReportData = Mainform.MainReportHandler.GetEqualisationCalculations(ThisFundID, CInt(LeverageDetails.Rows(LeverageCounter)("LeverageCounterparty")), 0, thisDate, ThisFundKnowledgeDate)

              Dim ThisRow As DataRow
              Dim RowCounter As Integer
              Dim SumCurrentEqualisation As Double = 0

              For RowCounter = 0 To (ReportData.Rows.Count - 1)
                ThisRow = ReportData.Rows(RowCounter)

                If (CInt(ThisRow("PendingTransaction")) = 0) Then
                  thisRedemptionRatio = CDbl(ThisRow("RedemptionRatio"))
                  SumCurrentEqualisation += CDbl(ThisRow("CurrentEqualisationFactor")) * thisRedemptionRatio * CDbl(ThisRow("Instrument_FXtoUSD"))
                End If
              Next

              If SumCurrentEqualisation <> 0 Then
                ' I know it is inconsistent, but in order to scale the HedgeValue later, I need the NAV value, thus I am not adjusting the 
                ' HedgeValue here. The GearingRatio will also be re-calculated later.

                'LeverageDetails.Rows(LeverageCounter)("HedgeValueUSD") = CDbl(LeverageDetails.Rows(LeverageCounter)("HedgeValueUSD")) + SumCurrentEqualisation
                LeverageDetails.Rows(LeverageCounter)("DebtValueUSD") = CDbl(LeverageDetails.Rows(LeverageCounter)("DebtValueUSD")) + SumCurrentEqualisation
                'LeverageDetails.Rows(LeverageCounter)("GearingRatio") = CDbl(LeverageDetails.Rows(LeverageCounter)("HedgeValueUSD")) / CDbl(LeverageDetails.Rows(LeverageCounter)("LeverageValueUSD"))
                LeverageDetails.Rows(LeverageCounter).AcceptChanges()
              End If

            Next

          End If


        Catch ex As Exception
          Mainform.LogError("UnitHolderFeeClass, GetSingleMonthCompleteFundCalculations()", LOG_LEVELS.Error, ex.Message, "Error getting Leverage Details.", ex.StackTrace)
          Return RVal
        Finally
          If (ThisCommand IsNot Nothing) Then
            ThisCommand.Connection = Nothing
            ThisCommand.Parameters.Clear()
            ThisCommand = Nothing
          End If
        End Try

        ' Get Watermark Details
        FundWatermarkRows = CType(Watermarks.Select("FeeFund=" & ThisFundID.ToString, "FeeDate"), DSFundPerformanceFeePoints.tblFundPerformanceFeePointsRow())

        ' Get Transactions
        Transactions = New DataTable

        Try
          ThisCommand = New SqlCommand()
          ThisCommand.CommandType = CommandType.StoredProcedure
          ThisCommand.CommandText = "spu_BasicEqualisationCalcs"
          ThisCommand.Parameters.Add("@FundID", SqlDbType.Int).Value = ThisFundID
          ThisCommand.Parameters.Add("@CounterpartyID", SqlDbType.Int).Value = 0
          ThisCommand.Parameters.Add("@InvestorGroup", SqlDbType.Int).Value = 0
          ThisCommand.Parameters.Add("@ValueDate", SqlDbType.DateTime).Value = thisDate
          ThisCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime).Value = ThisFundKnowledgeDate
          ThisCommand.Connection = thisConnection

          SyncLock thisConnection
            Mainform.LoadTable_Custom(Transactions, ThisCommand)
            ' Transactions.Load(ThisCommand.ExecuteReader)
          End SyncLock

        Catch ex As Exception
          Mainform.LogError("UnitHolderFeeClass, GetSingleMonthCompleteFundCalculations()", LOG_LEVELS.Error, ex.Message, "Error getting Transactions. FundID=" & ThisFundID.ToString, ex.StackTrace)
          Return RVal
        Finally
          If (ThisCommand IsNot Nothing) Then
            ThisCommand.Connection = Nothing
            ThisCommand.Parameters.Clear()
            ThisCommand = Nothing
          End If
        End Try

        ' Transaction Details should Include Subscription and Redemption details and Redemption ratios.

        ' Process Active Transaction lines.

        If (Transactions IsNot Nothing) AndAlso (Transactions.Rows.Count > 0) Then

          PendingTransaction_Ordinal = Transactions.Columns.IndexOf("PendingTransaction")
          Counterparty_Ordinal = Transactions.Columns.IndexOf("Counterparty")
          Fund_Ordinal = Transactions.Columns.IndexOf("Fund")
          Instrument_Ordinal = Transactions.Columns.IndexOf("Instrument")
          TransactionType_Ordinal = Transactions.Columns.IndexOf("TransactionType")
          ValueDate_Ordinal = Transactions.Columns.IndexOf("ValueDate")
          ParentID_Ordinal = Transactions.Columns.IndexOf("ParentID")
          Price_Ordinal = Transactions.Columns.IndexOf("Price")
          SignedUnits_Ordinal = Transactions.Columns.IndexOf("SignedUnits")
          RedemptionRatio_Ordinal = Transactions.Columns.IndexOf("RedemptionRatio")

          IsTransfer_Ordinal = Transactions.Columns.IndexOf("TransactionIsTransfer")
          EffectivePrice_Ordinal = Transactions.Columns.IndexOf("TransactionEffectivePrice")
          EffectiveWatermark_Ordinal = Transactions.Columns.IndexOf("TransactionEffectiveWatermark")
          EffectiveValueDate_Ordinal = Transactions.Columns.IndexOf("TransactionEffectiveValueDate")
          SpecificEqualisationFlag_Ordinal = Transactions.Columns.IndexOf("TransactionSpecificInitialEqualisationFlag")
          SpecificEqualisationValue_Ordinal = Transactions.Columns.IndexOf("TransactionSpecificInitialEqualisationValue")

          For TransactionCounter = 0 To (Transactions.Rows.Count - 1)

            ThisTransaction = Transactions.Rows(TransactionCounter)

            ThisPendingTransaction = CInt(ThisTransaction(PendingTransaction_Ordinal))
            ThisTransactionType = CInt(ThisTransaction(TransactionType_Ordinal))
            ThisCounterpartyID = CInt(ThisTransaction(Counterparty_Ordinal))
            ThisInstrumentID = CInt(ThisTransaction(Instrument_Ordinal))
            thisParentID = CInt(ThisTransaction(ParentID_Ordinal))
            thisValueDate = CDate(ThisTransaction(ValueDate_Ordinal))
            thisSignedUnits = CDbl(ThisTransaction(SignedUnits_Ordinal))
            thisSubscriptionGNAV = CDbl(ThisTransaction(Price_Ordinal))
            thisRedemptionRatio = CDbl(ThisTransaction(RedemptionRatio_Ordinal))
            thisFundBasicPerformanceFeePercent = CDbl(ThisTransaction("PerformanceFeesPercent"))
            thisFundBaseCurrency = CInt(ThisTransaction("FundBaseCurrency"))
            thisFxToUSD = CDbl(ThisTransaction("Instrument_FXtoUSD"))

            ThisIsTransfer = CBool(ThisTransaction(IsTransfer_Ordinal))
            ThisEffectivePrice = CDbl(ThisTransaction(EffectivePrice_Ordinal))
            ThisEffectiveWatermark = CDbl(ThisTransaction(EffectiveWatermark_Ordinal))
            ThisEffectiveValueDate = CDate(ThisTransaction(EffectiveValueDate_Ordinal))
            ThisSpecificEqualisationFlag = CBool(ThisTransaction(SpecificEqualisationFlag_Ordinal))
            ThisSpecificEqualisationValue = CDbl(ThisTransaction(SpecificEqualisationValue_Ordinal))

            If (ThisIsTransfer) Then
              thisValueDate = CDate(ThisTransaction(EffectiveValueDate_Ordinal))
              thisSubscriptionGNAV = ThisEffectivePrice
              ThisTransferValueDate = CDate(ThisTransaction(ValueDate_Ordinal))
            Else
              ThisTransferValueDate = thisValueDate
            End If

            SelectedCounterpartyRows = Counterparties.Select("CounterpartyID=" & ThisCounterpartyID.ToString)
            If (SelectedCounterpartyRows IsNot Nothing) AndAlso (SelectedCounterpartyRows.Length > 0) Then
              thisCounterpartyRow = SelectedCounterpartyRows(0)
            Else
              thisCounterpartyRow = Nothing
            End If

            ' Is this a valid transaction ?

            If (thisCounterpartyRow IsNot Nothing) AndAlso (ThisPendingTransaction = 0) AndAlso (ThisTransactionType = TransactionTypes.Subscribe) AndAlso (thisValueDate.Date <= ThisPeriodStartDate.Date) Then

              thisTransactionPoint = New TransactionPoint

              GrossManagementFee_Percent = thisCounterpartyRow.CounterpartyManagementFee
              GrossPerformanceFee_Percent = thisCounterpartyRow.CounterpartyPerformanceFee
              PerformanceFeeHurdle_Percent = thisCounterpartyRow.CounterpartyPerformanceFeeThreshold

              Dim ManagementFee_DebtRebate_Percent As Double = thisCounterpartyRow.Fee_ManagementDebtRebate
              Dim ManagementFee_EquityRebate_Percent As Double = thisCounterpartyRow.Fee_ManagementEquityRebate
              Dim PerformanceFee_DebtRebate_Percent As Double = thisCounterpartyRow.Fee_PerformanceDebtRebate
              Dim PerformanceFee_EquityRebate_Percent As Double = thisCounterpartyRow.Fee_PerformanceEquityRebate

              ' Get Watermark Details

              Call Mainform.MainReportHandler.WatermarkAtTime(thisValueDate, FundWatermarkRows, InitialWatermarkLevel, InitialWatermarkDate)
              Call Mainform.MainReportHandler.WatermarkAtTime(thisDate, FundWatermarkRows, LatestWatermarkLevel, LatestWatermarkDate)
              Call Mainform.MainReportHandler.HighestWatermarkAfterSubscriptionDate(thisValueDate, thisDate, FundWatermarkRows, HighestWatermarkLevel, HighestWatermarkDate)

              If (ThisIsTransfer) AndAlso (ThisEffectiveWatermark > 0) Then
                ' For Transfer Transactions, Allow an explicit Watermark to be specified

                If (InitialWatermarkLevel > ThisEffectiveWatermark) Then
                  InitialWatermarkLevel = ThisEffectiveWatermark
                End If

                If (LatestWatermarkDate < ThisTransferValueDate) OrElse (LatestWatermarkLevel < ThisEffectiveWatermark) Then
                  LatestWatermarkLevel = ThisEffectiveWatermark
                End If

                If (HighestWatermarkDate < ThisTransferValueDate) OrElse (HighestWatermarkLevel < ThisEffectiveWatermark) Then
                  HighestWatermarkLevel = ThisEffectiveWatermark
                End If

              End If

              If LatestWatermarkLevel > InitialWatermarkLevel Then
                GrossedUpLatestWatermark = ((LatestWatermarkLevel - InitialWatermarkLevel) / (1.0 - thisFundBasicPerformanceFeePercent)) + InitialWatermarkLevel
              Else
                GrossedUpLatestWatermark = InitialWatermarkLevel
              End If

              If HighestWatermarkLevel >= 0 Then
                GrossedUpHighestWatermarkAfterSubscription = ((HighestWatermarkLevel - InitialWatermarkLevel) / (1.0 - thisFundBasicPerformanceFeePercent)) + InitialWatermarkLevel
              Else
                GrossedUpHighestWatermarkAfterSubscription = (-1)    ' No Subsequent Watermark.
              End If

              ' Get InstrumentPricing 
              GetInstrumentPrices(thisConnection, FeesPeriodType, ThisFundID, ThisInstrumentID, FundPrices, thisDate, ThisFundKnowledgeDate, FundNAV, FundBNAV, FundGNAV)

              If (InitialWatermarkDate = LatestWatermarkDate) Then
                ' No Watermark since subscription
                GrossedUpFundGNAV = FundGNAV
              Else
                ' GrossedUpFundGNAV
                GrossedUpFundGNAV = ((FundNAV - InitialWatermarkLevel) / (1.0 - thisFundBasicPerformanceFeePercent)) + InitialWatermarkLevel
              End If

              If (thisSubscriptionGNAV <= InitialWatermarkLevel) Or (thisSubscriptionGNAV <= GrossedUpHighestWatermarkAfterSubscription) Then
                ' No Equalisation is Due
                ' Either Subscription was made at-or-below the Watermark point, or Subsequent watermarks have been
                ' recorded above the Subscription point.
                InitialEqualisation = 0
                CurrentEqualisation = 0
                LostEqualisation = 0
              Else
                ' InitialEqualisation
                ' CurrentEqualisation

                If (ThisIsTransfer) AndAlso (ThisSpecificEqualisationFlag) Then
                  InitialEqualisation = ThisSpecificEqualisationValue
                Else
                  InitialEqualisation = (thisSubscriptionGNAV - Max(InitialWatermarkLevel, GrossedUpHighestWatermarkAfterSubscription)) * GrossPerformanceFee_Percent * thisSignedUnits * thisRedemptionRatio
                End If
                LostEqualisation = (thisSubscriptionGNAV - Max(GrossedUpLatestWatermark, GrossedUpFundGNAV)) * GrossPerformanceFee_Percent * thisSignedUnits * thisRedemptionRatio

                If (LostEqualisation < 0) Then
                  LostEqualisation = 0
                  CurrentEqualisation = InitialEqualisation
                ElseIf (LostEqualisation > InitialEqualisation) Then
                  LostEqualisation = InitialEqualisation
                  CurrentEqualisation = 0
                Else
                  CurrentEqualisation = InitialEqualisation - LostEqualisation
                End If
              End If

              ' Contingent Redemption ?
              ContingentRedemptionFactor = 0

              If (GrossedUpHighestWatermarkAfterSubscription >= InitialWatermarkLevel) Then
                ' If there has been a Watermark at-or-above the 
                ' Initial Watermark level, then Contingent redemption is Zero.

                ContingentRedemptionFactor = 0
              Else
                ' Contingent redemption is the performance fee due on the GNAV differential between the
                ' Watermark applicable at time of subscription and the Max of 'Current GNAV' OR 'Highest Watermark since Subscription',
                ' Given that the Current CR can not exceed the Initial CR and can not be negative.

                ContingentRedemptionFactor = (Min(GrossedUpFundGNAV, InitialWatermarkLevel) - Max(thisSubscriptionGNAV, GrossedUpHighestWatermarkAfterSubscription)) * GrossPerformanceFee_Percent * thisSignedUnits * thisRedemptionRatio
                If (ContingentRedemptionFactor < 0) Then
                  ContingentRedemptionFactor = 0
                End If
              End If

              ' Accrued Incentive Fee, Accounting for PerformanceFeeHurdle_Percent

              AccruedPerformanceFee = (GrossedUpFundGNAV - (Max(GrossedUpHighestWatermarkAfterSubscription, thisSubscriptionGNAV) * (1.0 + PerformanceFeeHurdle_Percent))) * GrossPerformanceFee_Percent * thisSignedUnits
              If (AccruedPerformanceFee < 0) Then
                AccruedPerformanceFee = 0
              End If

              ' If the Last Period contains a watermark, this period Perf Fees = Accrued Perf Fees

              If (GrossPerformanceFee_Percent = 0) Then

                ThisMonthsGrossPerformanceFee = 0

              ElseIf (AreDatesEquivalent(FeesPeriodType, FitDateToPeriod(FeesPeriodType, LatestWatermarkDate.AddDays(1), True), thisDate)) Then
                ' Also gets around the issue of "(lastTransactionPoint Is Nothing)"

                ThisMonthsGrossPerformanceFee = AccruedPerformanceFee * thisRedemptionRatio

              Else ' Last period not a watermark.....

                ' Thismonths Incentive Fees
                Dim LastMonth_PerformanceFees_AccruedGross As Double
                LastMonth_PerformanceFees_AccruedGross = Double.NaN

                ' Get Last Months Fee for this Fund/Instrument/Counterparty
                If (LastMonthFundFees IsNot Nothing) Then
                  SelectedFundFees = LastMonthFundFees.Select("(FeeTransactionParentID=" & thisParentID.ToString & ")")

                  If (SelectedFundFees IsNot Nothing) AndAlso (SelectedFundFees.Length > 0) Then

                    ' If PerformanceFees and Holding have not changed use the previous month's value for comparison, 
                    ' else cause the previous month to be re-calculated using the new rate.

                    If (SelectedFundFees(0).GrossPerformanceFee_Percent = GrossPerformanceFee_Percent) AndAlso (Math.Abs(SelectedFundFees(0).TransactionHolding - thisSignedUnits) < 0.000001) Then ' Holding comparison to account for double pricision inaccuracy.
                      LastMonth_PerformanceFees_AccruedGross = SelectedFundFees(0).PerformanceFees_AccruedGross
                    End If

                  End If
                End If

                If (Not Double.NaN.Equals(LastMonth_PerformanceFees_AccruedGross)) Then
                  ' This Months Perf Fees = (ThisMonthAccrued - LastMonthAccrued)

                  ThisMonthsGrossPerformanceFee = (AccruedPerformanceFee - LastMonth_PerformanceFees_AccruedGross) * thisRedemptionRatio

                Else
                  ' lastTransactionPoint IS Nothing

                  ' This could be a transfer....

                  ' Note AddDays() fudge - The First Subscriptions to Balanced USD were entered at the End of Dec '04, rather than
                  ' As of ' Jan '05
                  '

                  If (ThisIsTransfer) OrElse (FitDateToPeriod(FeesPeriodType, thisValueDate.AddDays(15), True) < FitDateToPeriod(FeesPeriodType, thisDate, True)) Then
                    ' Valuedate preceeds this period, but there is no preceeding TransactionPoint
                    ' So, it appears to be a transfer.

                    ' In an ideal World, we would be able to identify the transaction from which this transfer occurred, however
                    ' at this time Venice is not build to accomodate Transfers and operationally they are usually achieved
                    ' by re-booking the original subscription. If the whole position is transferred, then the ParentID
                    ' would probably remain constant, however if this is a partial transfer, as HAS happened, then the 
                    ' ParentID for this transaction will be new and thus can not be matched to an Accrued Perf Fee for
                    ' last month.
                    ' The only sensible approach appears to be to calculate last months Accrued PerfFee again for this 
                    ' Transaction.


                    ' OK, First Calculate Accrued Performance Fee for last period, than you will have the necessary data to calculate 
                    ' This Months Performance fee.

                    ' Regarding Watermark Levels...
                    ' Since we have already established that Last month was not a Watermark point, it should hold that
                    ' The 'HighestWatermarkLevel' and 'GrossedUpHighestWatermarkAfterSubscription' values were the same
                    ' last month as they are this month. Thus I can use the already calculated values...

                    Dim LastMonth_AccruedPerformanceFee As Double
                    Dim LastMonth_GrossedUpFundGNAV As Double
                    Dim LastMonth_ThisFundKnowledgeDate As Date
                    Dim LastMonth_FundNAV As Double
                    Dim LastMonth_FundBNAV As Double
                    Dim LastMonth_FundGNAV As Double

                    ' LastMonth_GrossedUpFundGNAV

                    LastMonth_ThisFundKnowledgeDate = GetFundKnowledgeDate(Milestones, ThisFundID, LastPeriod_ThisDate, FeesPeriodType)

                    ' Get InstrumentPricing ?
                    GetInstrumentPrices(thisConnection, FeesPeriodType, ThisFundID, ThisInstrumentID, FundPrices, LastPeriod_ThisDate, LastMonth_ThisFundKnowledgeDate, LastMonth_FundNAV, LastMonth_FundBNAV, LastMonth_FundGNAV)

                    If (InitialWatermarkDate = LatestWatermarkDate) Then
                      ' No Watermark since subscription
                      LastMonth_GrossedUpFundGNAV = LastMonth_FundGNAV
                    Else
                      ' GrossedUpFundGNAV
                      LastMonth_GrossedUpFundGNAV = ((LastMonth_FundNAV - InitialWatermarkLevel) / (1.0 - thisFundBasicPerformanceFeePercent)) + InitialWatermarkLevel
                    End If

                    ' Calculate 'LastMonth_AccruedPerformanceFee'

                    LastMonth_AccruedPerformanceFee = (LastMonth_GrossedUpFundGNAV - (Max(GrossedUpHighestWatermarkAfterSubscription, thisSubscriptionGNAV) * (1.0 + PerformanceFeeHurdle_Percent))) * GrossPerformanceFee_Percent * thisSignedUnits
                    If (LastMonth_AccruedPerformanceFee < 0) Then
                      LastMonth_AccruedPerformanceFee = 0
                    End If

                    ThisMonthsGrossPerformanceFee = (AccruedPerformanceFee - LastMonth_AccruedPerformanceFee) * thisRedemptionRatio

                  Else
                    ' Appears not to be a transfer, just a new subscription,  thus This months Perf Fee is the Whole Perf Fee.

                    ThisMonthsGrossPerformanceFee = AccruedPerformanceFee * thisRedemptionRatio
                  End If
                End If

              End If


              ' Management Fees
              'thisDate = FitDateToPeriod(FeesPeriodType, pValueDate, True)
              'LastMonth_ThisDate = AddPeriodToDate(FeesPeriodType, thisDate, -1)


              ThisMonthsGrossManagementFee = FundBNAV * GrossManagementFee_Percent * thisSignedUnits * thisRedemptionRatio * ((thisDate - LastPeriod_ThisDate).TotalDays / DaysThisYear)

              ' Set Basic TransactionPoint  Values

              thisTransactionPoint.PointDate = thisDate
              thisTransactionPoint.ParentID = thisParentID
              thisTransactionPoint.InstrumentID = ThisInstrumentID
              thisTransactionPoint.FundID = ThisFundID
              thisTransactionPoint.CounterpartyID = ThisCounterpartyID
              thisTransactionPoint.Holding = thisSignedUnits
              thisTransactionPoint.ValueDate = thisValueDate
              thisTransactionPoint.Price = thisSubscriptionGNAV
              thisTransactionPoint.InitialEqualisation = InitialEqualisation
              thisTransactionPoint.CurrentEqualisation = CurrentEqualisation
              'thisTransactionPoint.Delta_Equalisation = XX
              thisTransactionPoint.ContingentRedemption = ContingentRedemptionFactor
              'thisTransactionPoint.Delta_ContingentRedemption = XX
              thisTransactionPoint.ManagementFees_Gross = ThisMonthsGrossManagementFee
              thisTransactionPoint.ManagementFees_Net = ThisMonthsGrossManagementFee
              thisTransactionPoint.ManagementFees_DebtRebate = 0
              thisTransactionPoint.ManagementFees_EquityRebate = 0
              thisTransactionPoint.ManagementFees_MarketingFCAM = 0
              thisTransactionPoint.ManagementFees_MarketingOthers = 0
              thisTransactionPoint.PerformanceHurdle = PerformanceFeeHurdle_Percent
              thisTransactionPoint.PerformanceFees_AccruedGross = AccruedPerformanceFee
              thisTransactionPoint.PerformanceFees_Gross = ThisMonthsGrossPerformanceFee
              thisTransactionPoint.PerformanceFees_Net = ThisMonthsGrossPerformanceFee
              thisTransactionPoint.PerformanceFees_DebtRebate = 0
              thisTransactionPoint.PerformanceFees_EquityRebate = 0
              thisTransactionPoint.PerformanceFees_MarketingFCAM = 0
              thisTransactionPoint.PerformanceFees_MarketingOthers = 0
              thisTransactionPoint.FundBaseCurrency = thisFundBaseCurrency
              thisTransactionPoint.RedemptionRatio = thisRedemptionRatio
              thisTransactionPoint.FxtoUSD = thisFxToUSD

              thisTransactionPoint.GrossManagementFee_Percent = GrossManagementFee_Percent
              thisTransactionPoint.GrossPerformanceFee_Percent = GrossPerformanceFee_Percent
              thisTransactionPoint.FeeTransactionWatermarkDate = LatestWatermarkDate
              thisTransactionPoint.FeeKnowledgedateUsed = ThisFundKnowledgeDate

              ' Calculate Rebates etc...

              Dim ManagementFeesAfterRebates As Double
              Dim PerformanceFeesAfterRebates As Double

              ' Counterparty Is LeverageProvider ?
              ' If so, get the Gearing ratio and calculate Debt rebates also.

              If (thisCounterpartyRow.CounterpartyLeverageProvider <> 0) Then
                Dim GearingRatio As Double = 1
                Dim AdjustedHedgeValue As Double = 0
                ' Dim DebugRow As DataRow

                Try
                  Dim SelectedLeverageRows() As DataRow

                  ' DebugRow = Nothing
                  SelectedLeverageRows = LeverageDetails.Select("LeverageCounterparty=" & thisCounterpartyRow.CounterpartyID.ToString)

                  If (SelectedLeverageRows IsNot Nothing) AndAlso (SelectedLeverageRows.Length > 0) Then
                    ' Note An Adjustment to the Given Gearing Ratio :
                    ' NPP 15 Jun 2007
                    '
                    ' The Options are valued basis the NAV thus the Normal gearing ratio
                    ' is calculated using the NAV. The Gearing Ratio required for the calculation 
                    ' of Fees requires a valuation of the Fund units at BNAV. Given the debt remains constant,
                    ' the Gearing ratio needs to be adjusted for the increase in Equity value.
                    ' 
                    ' R2 = 1 / (1 - (((1 - (1/R1)) * NAV) / BNAV))

                    'DebugRow = SelectedLeverageRows(0)

                    'R1 = CDbl(SelectedLeverageRows(0)("GearingRatio"))

                    'GearingRatio = 1.0 / (1.0 - (((1.0 - (1.0 / R1)) * FundNAV) / FundBNAV))

                    AdjustedHedgeValue = ((FundBNAV / FundNAV) * CDbl(SelectedLeverageRows(0)("HedgeValueUSD")))
                    GearingRatio = AdjustedHedgeValue / (AdjustedHedgeValue - CDbl(SelectedLeverageRows(0)("DebtValueUSD")))

                  End If

                Catch ex As Exception
                  GearingRatio = 1
                End Try

                If (GearingRatio > 1.0) Then

                  thisTransactionPoint.ManagementFees_EquityRebate = (ThisMonthsGrossManagementFee * ManagementFee_EquityRebate_Percent) / GearingRatio
                  thisTransactionPoint.ManagementFees_DebtRebate = (ThisMonthsGrossManagementFee * ManagementFee_DebtRebate_Percent) * ((GearingRatio - 1.0) / GearingRatio)

                  ManagementFeesAfterRebates = (ThisMonthsGrossManagementFee - (thisTransactionPoint.ManagementFees_EquityRebate + thisTransactionPoint.ManagementFees_DebtRebate))

                  thisTransactionPoint.ManagementFees_MarketingFCAM = ManagementFeesAfterRebates * thisCounterpartyRow.Fee_ManagementMarketingToFCAM
                  thisTransactionPoint.ManagementFees_MarketingOthers = ManagementFeesAfterRebates * thisCounterpartyRow.Fee_ManagementMarketingToOthers
                  thisTransactionPoint.ManagementFees_Net = thisTransactionPoint.ManagementFees_Gross - (thisTransactionPoint.ManagementFees_DebtRebate + thisTransactionPoint.ManagementFees_EquityRebate + thisTransactionPoint.ManagementFees_MarketingFCAM + thisTransactionPoint.ManagementFees_MarketingOthers)

                  thisTransactionPoint.PerformanceFees_EquityRebate = (ThisMonthsGrossPerformanceFee * PerformanceFee_EquityRebate_Percent) / GearingRatio
                  thisTransactionPoint.PerformanceFees_DebtRebate = (ThisMonthsGrossPerformanceFee * PerformanceFee_DebtRebate_Percent) * ((GearingRatio - 1.0) / GearingRatio)

                  PerformanceFeesAfterRebates = (ThisMonthsGrossPerformanceFee - thisTransactionPoint.PerformanceFees_EquityRebate)

                  thisTransactionPoint.PerformanceFees_MarketingFCAM = PerformanceFeesAfterRebates * thisCounterpartyRow.Fee_PerformanceMarketingToFCAM
                  thisTransactionPoint.PerformanceFees_MarketingOthers = PerformanceFeesAfterRebates * thisCounterpartyRow.Fee_PerformanceMarketingToOthers
                  thisTransactionPoint.PerformanceFees_Net = ThisMonthsGrossPerformanceFee - (thisTransactionPoint.PerformanceFees_DebtRebate + thisTransactionPoint.PerformanceFees_EquityRebate + thisTransactionPoint.PerformanceFees_MarketingFCAM + thisTransactionPoint.PerformanceFees_MarketingOthers)

                Else

                  thisTransactionPoint.ManagementFees_DebtRebate = 0
                  thisTransactionPoint.ManagementFees_EquityRebate = (ThisMonthsGrossManagementFee * ManagementFee_EquityRebate_Percent)

                  ManagementFeesAfterRebates = (ThisMonthsGrossManagementFee - thisTransactionPoint.ManagementFees_EquityRebate)

                  thisTransactionPoint.ManagementFees_MarketingFCAM = ManagementFeesAfterRebates * thisCounterpartyRow.Fee_ManagementMarketingToFCAM
                  thisTransactionPoint.ManagementFees_MarketingOthers = ManagementFeesAfterRebates * thisCounterpartyRow.Fee_ManagementMarketingToOthers
                  thisTransactionPoint.ManagementFees_Net = thisTransactionPoint.ManagementFees_Gross - (thisTransactionPoint.ManagementFees_DebtRebate + thisTransactionPoint.ManagementFees_EquityRebate + thisTransactionPoint.ManagementFees_MarketingFCAM + thisTransactionPoint.ManagementFees_MarketingOthers)

                  thisTransactionPoint.PerformanceFees_DebtRebate = 0
                  thisTransactionPoint.PerformanceFees_EquityRebate = (ThisMonthsGrossPerformanceFee * PerformanceFee_EquityRebate_Percent)

                  PerformanceFeesAfterRebates = (ThisMonthsGrossPerformanceFee - thisTransactionPoint.PerformanceFees_EquityRebate)

                  thisTransactionPoint.PerformanceFees_MarketingFCAM = PerformanceFeesAfterRebates * thisCounterpartyRow.Fee_PerformanceMarketingToFCAM
                  thisTransactionPoint.PerformanceFees_MarketingOthers = PerformanceFeesAfterRebates * thisCounterpartyRow.Fee_PerformanceMarketingToOthers
                  thisTransactionPoint.PerformanceFees_Net = ThisMonthsGrossPerformanceFee - (thisTransactionPoint.PerformanceFees_DebtRebate + thisTransactionPoint.PerformanceFees_EquityRebate + thisTransactionPoint.PerformanceFees_MarketingFCAM + thisTransactionPoint.PerformanceFees_MarketingOthers)

                End If

              Else ' Not Leverage Provider

                thisTransactionPoint.ManagementFees_DebtRebate = 0
                thisTransactionPoint.ManagementFees_EquityRebate = (ThisMonthsGrossManagementFee * ManagementFee_EquityRebate_Percent)

                ManagementFeesAfterRebates = (ThisMonthsGrossManagementFee - thisTransactionPoint.ManagementFees_EquityRebate)

                thisTransactionPoint.ManagementFees_MarketingFCAM = ManagementFeesAfterRebates * thisCounterpartyRow.Fee_ManagementMarketingToFCAM
                thisTransactionPoint.ManagementFees_MarketingOthers = ManagementFeesAfterRebates * thisCounterpartyRow.Fee_ManagementMarketingToOthers
                thisTransactionPoint.ManagementFees_Net = thisTransactionPoint.ManagementFees_Gross - (thisTransactionPoint.ManagementFees_DebtRebate + thisTransactionPoint.ManagementFees_EquityRebate + thisTransactionPoint.ManagementFees_MarketingFCAM + thisTransactionPoint.ManagementFees_MarketingOthers)

                thisTransactionPoint.PerformanceFees_DebtRebate = 0
                thisTransactionPoint.PerformanceFees_EquityRebate = (ThisMonthsGrossPerformanceFee * PerformanceFee_EquityRebate_Percent)

                PerformanceFeesAfterRebates = (ThisMonthsGrossPerformanceFee - thisTransactionPoint.PerformanceFees_EquityRebate)

                thisTransactionPoint.PerformanceFees_MarketingFCAM = PerformanceFeesAfterRebates * thisCounterpartyRow.Fee_PerformanceMarketingToFCAM
                thisTransactionPoint.PerformanceFees_MarketingOthers = PerformanceFeesAfterRebates * thisCounterpartyRow.Fee_PerformanceMarketingToOthers
                thisTransactionPoint.PerformanceFees_Net = ThisMonthsGrossPerformanceFee - (thisTransactionPoint.PerformanceFees_DebtRebate + thisTransactionPoint.PerformanceFees_EquityRebate + thisTransactionPoint.PerformanceFees_MarketingFCAM + thisTransactionPoint.PerformanceFees_MarketingOthers)

              End If

              ' Store Return Value.

              RVal_ArrayList.Add(thisTransactionPoint)

            End If ' Is this a valid transaction ?

          Next ' TransactionCounter

        End If ' (Transactions IsNot Nothing)

      Else ' NOT - 'If (FundUsesEqualisation)'
        thisTransactionPoint = New TransactionPoint

        If (InstrumentRow IsNot Nothing) Then
          GrossManagementFee_Percent = InstrumentRow.InstrumentUnitManagementFees
          GrossPerformanceFee_Percent = InstrumentRow.InstrumentUnitPerformanceFees
        End If

        ' Get Watermark Details
        FundWatermarkRows = CType(Watermarks.Select("FeeFund=" & ThisFundID.ToString, "FeeDate"), DSFundPerformanceFeePoints.tblFundPerformanceFeePointsRow())

        ' Get this periods Instrument Pricing 
        GetInstrumentPrices(thisConnection, FeesPeriodType, ThisFundID, FundRow.FundUnit, FundPrices, thisDate, ThisFundKnowledgeDate, FundNAV, FundBNAV, FundGNAV)

        GrossedUpFundGNAV = FundGNAV

        ThisMonthsGrossManagementFee = FundBNAV * GrossManagementFee_Percent * thisSignedUnits * thisRedemptionRatio * ((thisDate - LastPeriod_ThisDate).TotalDays / DaysThisYear)

        thisTransactionPoint.PointDate = thisDate
        thisTransactionPoint.ParentID = 0
        thisTransactionPoint.InstrumentID = 0
        thisTransactionPoint.FundID = ThisFundID
        thisTransactionPoint.CounterpartyID = 0
        thisTransactionPoint.Holding = 0
        thisTransactionPoint.ValueDate = thisValueDate
        thisTransactionPoint.Price = thisSubscriptionGNAV
        thisTransactionPoint.InitialEqualisation = 0
        thisTransactionPoint.CurrentEqualisation = 0
        'thisTransactionPoint.Delta_Equalisation = XX
        thisTransactionPoint.ContingentRedemption = 0
        'thisTransactionPoint.Delta_ContingentRedemption = XX
        thisTransactionPoint.ManagementFees_Gross = ThisMonthsGrossManagementFee
        thisTransactionPoint.ManagementFees_Net = ThisMonthsGrossManagementFee
        thisTransactionPoint.ManagementFees_DebtRebate = 0
        thisTransactionPoint.ManagementFees_EquityRebate = 0
        thisTransactionPoint.ManagementFees_MarketingFCAM = 0
        thisTransactionPoint.ManagementFees_MarketingOthers = 0
        thisTransactionPoint.PerformanceHurdle = PerformanceFeeHurdle_Percent
        thisTransactionPoint.PerformanceFees_AccruedGross = AccruedPerformanceFee
        thisTransactionPoint.PerformanceFees_Gross = ThisMonthsGrossPerformanceFee
        thisTransactionPoint.PerformanceFees_Net = ThisMonthsGrossPerformanceFee
        thisTransactionPoint.PerformanceFees_DebtRebate = 0
        thisTransactionPoint.PerformanceFees_EquityRebate = 0
        thisTransactionPoint.PerformanceFees_MarketingFCAM = 0
        thisTransactionPoint.PerformanceFees_MarketingOthers = 0
        thisTransactionPoint.FundBaseCurrency = thisFundBaseCurrency
        thisTransactionPoint.RedemptionRatio = 1
        thisTransactionPoint.FxtoUSD = thisFxToUSD

        thisTransactionPoint.GrossManagementFee_Percent = GrossManagementFee_Percent
        thisTransactionPoint.GrossPerformanceFee_Percent = GrossPerformanceFee_Percent
        thisTransactionPoint.FeeTransactionWatermarkDate = LatestWatermarkDate
        thisTransactionPoint.FeeKnowledgedateUsed = ThisFundKnowledgeDate

        RVal_ArrayList.Add(thisTransactionPoint)

      End If ' If (FundUsesEqualisation) Then



    Catch ex As Exception
      Mainform.LogError("UnitHolderFeeClass", LOG_LEVELS.Error, ex.Message, "Error in GetSinglePeriodCompleteFundCalculations()", ex.StackTrace)
    Finally
      Try
        If (thisConnection IsNot Nothing) Then
          thisConnection.Close()
          thisConnection = Nothing
        End If
      Catch ex As Exception
        thisConnection = Nothing
      End Try

    End Try

    ' Return Value 

    Try
      If (RVal_ArrayList.Count > 0) Then
        ReDim RVal(RVal_ArrayList.Count - 1)
        Dim Rval_Counter As Integer

        For Rval_Counter = 0 To (RVal_ArrayList.Count - 1)
          RVal(Rval_Counter) = RVal_ArrayList(Rval_Counter)
        Next
        RVal_ArrayList.Clear()
      End If
    Catch ex As Exception
    End Try

    Return RVal

  End Function

  ''' <summary>
  ''' Gets the fund knowledge date.
  ''' </summary>
  ''' <param name="Milestones">The milestones.</param>
  ''' <param name="ThisFundID">The this fund ID.</param>
  ''' <param name="ThisDate">The this date.</param>
  ''' <param name="FeesPeriodType">Type of the fees period.</param>
  ''' <returns>System.DateTime.</returns>
  Private Function GetFundKnowledgeDate(ByRef Milestones As DSFundMilestones.tblFundMilestonesDataTable, ByVal ThisFundID As Integer, ByVal ThisDate As Date, ByVal FeesPeriodType As RenaissanceGlobals.DealingPeriod) As Date
    ' *********************************************************************************
    ' Return the Fund Knowledgedate from the supplied table consistent with the given parameters.
    ' If a Milestone does not exist for the given date, return the previous Milestone KnowledgeDate.
    ' If there is no previous MS then Return now().
    ' *********************************************************************************

    Dim SelectedRows() As DSFundMilestones.tblFundMilestonesRow
    Dim RVal As Date = KNOWLEDGEDATE_NOW
    Dim TargetDate As Date
    Dim RowCount As Integer

    TargetDate = FitDateToPeriod(FeesPeriodType, ThisDate, True)
    SelectedRows = Milestones.Select("(MilestoneFundID=" & ThisFundID.ToString & ")", "MilestoneDate Desc")

    If (SelectedRows IsNot Nothing) AndAlso (SelectedRows.Length > 0) Then
      For RowCount = 0 To (SelectedRows.Length - 1)
        RVal = SelectedRows(RowCount).MilestoneKnowledgeDate

        If FitDateToPeriod(FeesPeriodType, SelectedRows(RowCount).MilestoneDate, True) = TargetDate Then
          Return SelectedRows(RowCount).MilestoneKnowledgeDate
        End If

        If (SelectedRows(RowCount).MilestoneDate < TargetDate) Then
          Exit For
        End If
      Next
    End If

    Return RVal

  End Function

  ''' <summary>
  ''' Gets the instrument prices.
  ''' </summary>
  ''' <param name="thisConnection">The this connection.</param>
  ''' <param name="FeesPeriodType">Type of the fees period.</param>
  ''' <param name="ThisFundID">The this fund ID.</param>
  ''' <param name="ThisInstrumentID">The this instrument ID.</param>
  ''' <param name="tblFundPrices">The TBL fund prices.</param>
  ''' <param name="ThisDate">The this date.</param>
  ''' <param name="ThisFundKnowledgeDate">The this fund knowledge date.</param>
  ''' <param name="FundNAV">The fund NAV.</param>
  ''' <param name="FundBNAV">The fund BNAV.</param>
  ''' <param name="FundGNAV">The fund GNAV.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function GetInstrumentPrices(ByVal thisConnection As SqlConnection, ByVal FeesPeriodType As RenaissanceGlobals.DealingPeriod, ByVal ThisFundID As Integer, ByVal ThisInstrumentID As Integer, ByRef tblFundPrices As DSPrice.tblPriceDataTable, ByVal ThisDate As Date, ByVal ThisFundKnowledgeDate As Date, ByRef FundNAV As Double, ByRef FundBNAV As Double, ByRef FundGNAV As Double) As Boolean
    ' *************************************************************************************
    ' Return NAV, GNAV & BNAV prices for the Given Instrument or Fund.
    '
    ' *************************************************************************************

    Dim SelectedRows() As DSPrice.tblPriceRow

    FundNAV = 100
    FundBNAV = 100
    FundGNAV = 100

    ' OK, first, try to retrieve the appropriate prices from the Milestone Prices table.

    Try
      SelectedRows = tblFundPrices.Select("(PriceInstrument=" & ThisInstrumentID.ToString & ") AND (PriceDate<='" & ThisDate.ToString(QUERY_SHORTDATEFORMAT) & "')", "PriceDate Desc")
    Catch ex As Exception
      SelectedRows = Nothing
    End Try

    If (SelectedRows IsNot Nothing) AndAlso (SelectedRows.Length > 0) Then
      If (FitDateToPeriod(FeesPeriodType, ThisDate, True) = FitDateToPeriod(FeesPeriodType, SelectedRows(0).PriceDate, True)) Then
        FundNAV = SelectedRows(0).PriceNAV
        FundBNAV = SelectedRows(0).PriceBasicNAV
        FundGNAV = SelectedRows(0).PriceGNAV

        Return True
      End If
    End If

    ' If there is no Milestone price, Try to get a price from Venice.

    Dim ThisCommand As SqlCommand = Nothing
    Dim ReturnTable As DataTable

    Try
      ReturnTable = New DataTable

      ThisCommand = New SqlCommand()
      ThisCommand.CommandType = CommandType.Text
      ThisCommand.CommandText = "SELECT UnitPrice FROM fn_UnitPrice(@FundID, @ValueDate, @UnitPriceVariant, @StatusGroupFilter, @AdministratorDatesFilter, 0, @KnowledgeDate)"
      ThisCommand.Parameters.Add("@FundID", SqlDbType.Int).Value = ThisFundID
      ThisCommand.Parameters.Add("@ValueDate", SqlDbType.DateTime).Value = ThisDate
      ThisCommand.Parameters.Add("@UnitPriceVariant", SqlDbType.Int).Value = 4 ' NAV
      ThisCommand.Parameters.Add("@StatusGroupFilter", SqlDbType.NVarChar).Value = _StatusGroupFilter
      ThisCommand.Parameters.Add("@AdministratorDatesFilter", SqlDbType.Int).Value = _AdministratorDatesFilter
      ThisCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime).Value = ThisFundKnowledgeDate

      ThisCommand.Connection = thisConnection

      SyncLock thisConnection
        Mainform.LoadTable_Custom(ReturnTable, ThisCommand)
        'ReturnTable.Load(ThisCommand.ExecuteReader)
      End SyncLock

      If (ReturnTable.Rows.Count <= 0) Then
        FundNAV = 100
        FundBNAV = 100
        FundGNAV = 100

        Return True
      End If

      FundNAV = CDbl(ReturnTable.Rows(0)(0))
      FundBNAV = FundNAV
      FundGNAV = FundNAV

      ThisCommand.Parameters("@UnitPriceVariant").Value = 2 ' BNAV
      ReturnTable.Clear()
      SyncLock thisConnection
        Mainform.LoadTable_Custom(ReturnTable, ThisCommand)
        ' ReturnTable.Load(ThisCommand.ExecuteReader)
      End SyncLock

      If (ReturnTable.Rows.Count > 0) Then
        FundBNAV = CDbl(ReturnTable.Rows(0)(0))
      End If

      ThisCommand.Parameters("@UnitPriceVariant").Value = 3 ' GNAV
      ReturnTable.Clear()
      SyncLock thisConnection
        Mainform.LoadTable_Custom(ReturnTable, ThisCommand)
        'ReturnTable.Load(ThisCommand.ExecuteReader)
      End SyncLock

      If (ReturnTable.Rows.Count > 0) Then
        FundGNAV = CDbl(ReturnTable.Rows(0)(0))
      End If

      Return True

    Catch ex As Exception
      Mainform.LogError("UnitHolderFeeClass, GetInstrumentPrices()", LOG_LEVELS.Error, ex.Message, "Error getting Fund Prices.", ex.StackTrace)
      Return False
    Finally
      If (ThisCommand IsNot Nothing) Then
        ThisCommand.Connection = Nothing
        ThisCommand.Parameters.Clear()
        ThisCommand = Nothing
      End If


      Try
        ' Save this price to the Milestone table, in order to save time if this Fund / Instrument is queried again.

        Dim PriceRow As DSPrice.tblPriceRow

        PriceRow = tblFundPrices.NewtblPriceRow

        PriceRow.RN = 0
        PriceRow.PriceID = 0
        PriceRow.PriceInstrument = ThisInstrumentID
        PriceRow.PriceDate = ThisDate
        PriceRow.PricePercent = 0
        PriceRow.PriceMultiplier = 1.0#
        PriceRow.PriceLevel = FundNAV
        PriceRow.PriceNAV = FundNAV
        PriceRow.PriceBasicNAV = FundBNAV
        PriceRow.PriceGNAV = FundGNAV
        PriceRow.PriceGAV = 0
        PriceRow.PriceBaseDate = #1/1/1900#
        PriceRow.PriceFinal = 0
        PriceRow.PriceIsPercent = False
        PriceRow.PriceIsAdministrator = False
        PriceRow.PriceIsMilestone = True
        PriceRow.PriceComment = ""

        tblFundPrices.Rows.Add(PriceRow)
      Catch ex As Exception
      End Try
    End Try

    Return False
  End Function

  ''' <summary>
  ''' Saves the single period fund fees.
  ''' </summary>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pValueStartDate">The p value start date.</param>
  ''' <param name="pValueEndDate">The p value end date.</param>
  ''' <param name="IsMilestone">if set to <c>true</c> [is milestone].</param>
  Public Sub SaveSinglePeriodFundFees(ByVal pFundID As Integer, ByVal pValueStartDate As Date, ByVal pValueEndDate As Date, ByVal IsMilestone As Boolean)
    ' **************************************************************************************
    '
    '
    ' **************************************************************************************

    If (Mainform Is Nothing) Then
      Exit Sub
    End If

    Dim TableName As String
    If (IsMilestone) Then
      TableName = "tblUnitHolderFees"
    Else
      TableName = "tblUnitHolderFees_NonMilestone"
    End If

    Dim FundFees() As TransactionPoint
    Dim thisFee As TransactionPoint
    Dim FeeAdaptor As New SqlDataAdapter
    Dim FeeDS As New DSUnitHolderFees
    Dim FeeTable As DSUnitHolderFees.tblUnitHolderFeesDataTable = FeeDS.tblUnitHolderFees
    Dim FeeCounter As Integer
    Dim FeeRow As DSUnitHolderFees.tblUnitHolderFeesRow

    ' First Clear existing Fees for this Fund on this period.

    Dim thisCommand As SqlCommand = Nothing
    Try
      thisCommand = New SqlCommand()
      thisCommand.CommandType = CommandType.StoredProcedure
      thisCommand.CommandText = "adp_" & TableName & "_DeleteFundFees"
      thisCommand.Parameters.Add("@FeeDate", SqlDbType.DateTime).Value = pValueEndDate
      thisCommand.Parameters.Add("@FeeFund", SqlDbType.Int).Value = pFundID

      thisCommand.Connection = Mainform.MainDataHandler.Get_Connection(VENICE_CONNECTION)

      SyncLock thisCommand.Connection
        thisCommand.ExecuteNonQuery()
      End SyncLock

    Catch ex As Exception
      Mainform.LogError("UnitHolderFeeClass.SaveSinglePeriodFundFees", LOG_LEVELS.Error, ex.Message, "Error Deleting existing Fee records.", ex.StackTrace, True)
    Finally
      If (thisCommand IsNot Nothing) Then
        thisCommand.Connection = Nothing
        thisCommand.Parameters.Clear()
        thisCommand = Nothing
      End If
    End Try

    ' Now Save Fund Fees

    Try
      FundFees = GetSinglePeriodCompleteFundCalculations(pFundID, pValueStartDate, pValueEndDate)

      If (FundFees IsNot Nothing) AndAlso (FundFees.Length > 0) Then
        Mainform.MainAdaptorHandler.Set_AdaptorCommands(Mainform.MainDataHandler.Get_Connection(VENICE_CONNECTION), FeeAdaptor, TableName) ' "tblUnitHolderFees")

        For FeeCounter = 0 To (FundFees.Length - 1)
          thisFee = FundFees(FeeCounter)
          FeeRow = FeeTable.NewtblUnitHolderFeesRow

          FeeRow.FeeTransactionID = thisFee.ParentID
          FeeRow.FeeTransactionParentID = thisFee.ParentID
          FeeRow.FeeFund = thisFee.FundID
          FeeRow.FundBaseCurrency = thisFee.FundBaseCurrency
          FeeRow.FeeCounterparty = thisFee.CounterpartyID
          FeeRow.FeeInstrumentID = thisFee.InstrumentID
          FeeRow.FeeDate = thisFee.PointDate
          FeeRow.TransactionType = RenaissanceGlobals.TransactionTypes.Subscribe
          FeeRow.TransactionHolding = thisFee.Holding
          FeeRow.TransactionPrice = thisFee.Price
          FeeRow.TransactionValueDate = thisFee.ValueDate
          FeeRow.InitialEqualisation = thisFee.InitialEqualisation
          FeeRow.CurrentEqualisation = thisFee.CurrentEqualisation
          FeeRow.ContingentRedemption = thisFee.ContingentRedemption
          FeeRow.RedemptionRatio = thisFee.RedemptionRatio
          FeeRow.ManagementFees_Gross = thisFee.ManagementFees_Gross
          FeeRow.ManagementFees_Net = thisFee.ManagementFees_Net
          FeeRow.ManagementFees_DebtRebate = thisFee.ManagementFees_DebtRebate
          FeeRow.ManagementFees_EquityRebate = thisFee.ManagementFees_EquityRebate
          FeeRow.ManagementFees_MarketingFCAM = thisFee.ManagementFees_MarketingFCAM
          FeeRow.ManagementFees_MarketingOthers = thisFee.ManagementFees_MarketingOthers
          FeeRow.PerformanceFees_AccruedGross = thisFee.PerformanceFees_AccruedGross
          FeeRow.PerformanceFees_Gross = thisFee.PerformanceFees_Gross
          FeeRow.PerformanceFees_Net = thisFee.PerformanceFees_Net
          FeeRow.PerformanceFees_DebtRebate = thisFee.PerformanceFees_DebtRebate
          FeeRow.PerformanceFees_EquityRebate = thisFee.PerformanceFees_EquityRebate
          FeeRow.PerformanceFees_MarketingFCAM = thisFee.PerformanceFees_MarketingFCAM
          FeeRow.PerformanceFees_MarketingOthers = thisFee.PerformanceFees_MarketingOthers
          FeeRow.FXtoUSD = thisFee.FxtoUSD
          FeeRow.GrossManagementFee_Percent = thisFee.GrossManagementFee_Percent
          FeeRow.GrossPerformanceFee_Percent = thisFee.GrossPerformanceFee_Percent
          FeeRow.PerformanceHurdle = thisFee.PerformanceHurdle
          FeeRow.FeeTransactionWatermarkDate = thisFee.FeeTransactionWatermarkDate
          FeeRow.FeeKnowledgedateUsed = thisFee.FeeKnowledgedateUsed

          FeeTable.Rows.Add(FeeRow)
        Next

        If (FeeTable.Rows.Count > 0) Then
          If (FeeAdaptor.InsertCommand.Parameters.Contains("@KnowledgeDate")) Then
            FeeAdaptor.InsertCommand.Parameters("@KnowledgeDate").Value = Mainform.Main_Knowledgedate
          End If
          If (FeeAdaptor.UpdateCommand.Parameters.Contains("@KnowledgeDate")) Then
            FeeAdaptor.UpdateCommand.Parameters("@KnowledgeDate").Value = Mainform.Main_Knowledgedate
          End If

          Mainform.AdaptorUpdate("UnitHolderFeeClass.SaveSingleMonthFundFees", FeeAdaptor, FeeTable)

          'Me.Mainform.ReloadTable(RenaissanceStandardDatasets.tblUnitHolderFees.ChangeID, False)
          'Call Mainform.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblUnitHolderFees))
          Me.Mainform.ReloadTable_Background(RenaissanceStandardDatasets.tblUnitHolderFees.ChangeID, "", True)

        End If
      End If

    Catch ex As Exception
      Mainform.LogError("UnitHolderFeeClass.SaveSingleMonthFundFees", LOG_LEVELS.Error, ex.Message, "Error saving Fee records.", ex.StackTrace, True)
    End Try

  End Sub


#Region " IDisposable Support "

  ''' <summary>
  ''' The disposed value
  ''' </summary>
  Private disposedValue As Boolean = False    ' To detect redundant calls

  ' IDisposable
  ''' <summary>
  ''' Releases unmanaged and - optionally - managed resources.
  ''' </summary>
  ''' <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
  Protected Overridable Sub Dispose(ByVal disposing As Boolean)
    If Not Me.disposedValue Then
      If disposing Then
        ' TODO: free unmanaged resources when explicitly called
      End If

      ' TODO: free shared unmanaged resources
      Mainform = Nothing

    End If
    Me.disposedValue = True
  End Sub

  '' This code added by Visual Basic to correctly implement the disposable pattern.

  ''' <summary>
  ''' Disposes this instance.
  ''' </summary>
  Public Sub Dispose() ' Implements IDisposable.Dispose
    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
    Dispose(True)
    GC.SuppressFinalize(Me)
  End Sub

  ''' <summary>
  ''' Allows an object to try to free resources and perform other cleanup operations before it is reclaimed by garbage collection.
  ''' </summary>
  Protected Overrides Sub Finalize()
    Dispose(False)
  End Sub

#End Region

End Class

''' <summary>
''' Class SR_InformationClass
''' </summary>
Public Class SR_InformationClass

  ''' <summary>
  ''' The Instrument ID
  ''' </summary>
  Public InstrumentID As Integer
  ''' <summary>
  ''' The trade date
  ''' </summary>
  Public TradeDate As Date
  ''' <summary>
  ''' The units subscribed
  ''' </summary>
  Public UnitsSubscribed As Double
  ''' <summary>
  ''' The units redeemed
  ''' </summary>
  Public UnitsRedeemed As Double
  ''' <summary>
  ''' The value subscribed
  ''' </summary>
  Public ValueSubscribed As Double
  ''' <summary>
  ''' The value redeemed
  ''' </summary>
  Public ValueRedeemed As Double
  ''' <summary>
  ''' The trade count
  ''' </summary>
  Public TradeCount As Integer

  ''' <summary>
  ''' Initializes a new instance of the <see cref="SR_InformationClass"/> class.
  ''' </summary>
  Public Sub New()
    InstrumentID = 0
    TradeDate = Renaissance_BaseDate
    UnitsSubscribed = 0.0#
    UnitsRedeemed = 0.0#
    ValueSubscribed = 0.0#
    ValueRedeemed = 0.0#
    TradeCount = 0
  End Sub

  ''' <summary>
  ''' Initializes a new instance of the <see cref="SR_InformationClass"/> class.
  ''' </summary>
  ''' <param name="ThisDate">The this date.</param>
  ''' <param name="SubscribeUnits">The subscribe units.</param>
  ''' <param name="RedeemUnits">The redeem units.</param>
  ''' <param name="SubscribeValue">The subscribe value.</param>
  ''' <param name="RedeemValue">The redeem value.</param>
  ''' <param name="Count">The count.</param>
  Public Sub New(ByVal ThisInstrumentID As Integer, ByVal ThisDate As Date, ByVal SubscribeUnits As Double, ByVal RedeemUnits As Double, ByVal SubscribeValue As Double, ByVal RedeemValue As Double, ByVal Count As Integer)
    InstrumentID = ThisInstrumentID
    TradeDate = ThisDate
    UnitsSubscribed = SubscribeUnits
    UnitsRedeemed = RedeemUnits
    ValueSubscribed = SubscribeValue
    ValueRedeemed = RedeemValue
    TradeCount = Count
  End Sub

End Class

''' <summary>
''' Class FundFeeCalculator
''' </summary>
Public Class FundFeeCalculator
  ' Implements IDisposable

#Region "Class Variables"

  ''' <summary>
  ''' The mainform
  ''' </summary>
  Private Mainform As VeniceMain

  ''' <summary>
  ''' The _ fund ID
  ''' </summary>
  Private _FundID As Integer
  ''' <summary>
  ''' The _ fee date
  ''' </summary>
  Private _FeeDate As Date
  ''' <summary>
  ''' The _ status group filter
  ''' </summary>
  Private _StatusGroupFilter As String = DEFAULT_STATUSGROUPFILTER
  ''' <summary>
  ''' The _ administrator dates filter
  ''' </summary>
  Private _AdministratorDatesFilter As Integer = DEFAULT_ADMINISTRATORDATESFILTER
  ''' <summary>
  ''' The _ fees need update
  ''' </summary>
  Private _FeesNeedUpdate As Boolean
  ''' <summary>
  ''' The _ valuation needs update
  ''' </summary>
  Private _ValuationNeedsUpdate As Boolean
  ''' <summary>
  ''' The _ valuation object
  ''' </summary>
  Private _ValuationObject As FundValuationDetails
  '''' <summary>
  '''' The _ subscriptions object
  '''' </summary>
  'Private _SubscriptionsObject() As SR_InformationClass
  ''' <summary>
  ''' The _SubscriptionsArrays object.
  ''' Holds a collection of arrays, each of which relates to the Subscriptions and Redemptions for a single Unit Instrument.
  ''' </summary>
  Private _SubscriptionsArrays As Dictionary(Of Integer, SR_InformationClass())
  ''' <summary>
  ''' The _ fees object
  ''' </summary>
  Private _FeesObject As FundFeeDetails
  ''' <summary>
  ''' The _ use administrator price dates
  ''' </summary>
  Private _UseAdministratorPriceDates As Boolean

  ''' <summary>
  ''' The _ override fee start date
  ''' </summary>
  Private _OverrideFeeStartDate As Boolean
  ''' <summary>
  ''' The _ fee start date
  ''' </summary>
  Private _FeeStartDate As Date
  ''' <summary>
  ''' The _ override exclude subscriptions since date
  ''' </summary>
  Private _OverrideExcludeSubscriptionsSinceDate As Boolean
  ''' <summary>
  ''' The _ exclude subscriptions since date
  ''' </summary>
  Private _ExcludeSubscriptionsSinceDate As Date
  ''' <summary>
  ''' The _ fund year start date
  ''' </summary>
  Private _FundYearStartDate As Date

  ''' <summary>
  ''' The _ override valuation expenses start date
  ''' </summary>
  Private _OverrideValuationExpensesStartDate As Boolean
  ''' <summary>
  ''' The _ valuation expenses start date
  ''' </summary>
  Private _ValuationExpensesStartDate As Date
  ''' <summary>
  ''' The _ override valuation performance fee start date
  ''' </summary>
  Private _OverrideValuationPerformanceFeeStartDate As Boolean
  ''' <summary>
  ''' The _ valuation performance fee start date
  ''' </summary>
  Private _ValuationPerformanceFeeStartDate As Date
  ''' <summary>
  ''' The _ override valuation management fee start date
  ''' </summary>
  Private _OverrideValuationManagementFeeStartDate As Boolean
  ''' <summary>
  ''' The _ valuation management fee start date
  ''' </summary>
  Private _ValuationManagementFeeStartDate As Date
  ''' <summary>
  ''' The _ override valuation crystalised performance fee start date
  ''' </summary>
  Private _OverrideValuationCrystalisedPerformanceFeeStartDate As Boolean
  ''' <summary>
  ''' The _ valuation crystalised performance fee start date
  ''' </summary>
  Private _ValuationCrystalisedPerformanceFeeStartDate As Date

  ''' <summary>
  ''' The ASSUM e_ LON g_ FIRS t_ YEA r_ I f_ APPROPRIATE
  ''' </summary>
  Private Const ASSUME_LONG_FIRST_YEAR_IF_APPROPRIATE As Boolean = True

  ''' <summary>
  ''' The lock_tbl fund valuation
  ''' </summary>
  Private lock_tblFundValuation As New ReaderWriterLockSlim
  ''' <summary>
  ''' The lock_tbl fund valuation
  ''' </summary>
  Private lock_tblFundValuationAll As New ReaderWriterLockSlim
  ''' <summary>
  ''' The lock_tbl futures notionals
  ''' </summary>
  Private lock_tblFuturesNotionals As New ReaderWriterLockSlim
  ''' <summary>
  ''' The lock_tbl availabilities
  ''' </summary>
  Private lock_tblAvailabilities As New ReaderWriterLockSlim

  Private _linkedReconciliationform As frmReconciliationProcess

  ''' <summary>
  ''' The administrator inventory cache
  ''' </summary>
  Friend AdministratorInventoryCache As Dictionary(Of Integer, DataTable)
  ''' <summary>
  ''' The administrator holdings cache
  ''' </summary>
  Friend AdministratorHoldingsCache As Dictionary(Of Integer, DataTable)


#End Region

#Region "Public property Get / Set"

  ''' <summary>
  ''' Gets a value indicating whether [fees need update].
  ''' </summary>
  ''' <value><c>true</c> if [fees need update]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property FeesNeedUpdate() As Boolean
    Get
      Return (_FeesNeedUpdate Or _ValuationNeedsUpdate)
    End Get

  End Property

  ''' <summary>
  ''' Gets or sets a value indicating whether [valuation needs update].
  ''' </summary>
  ''' <value><c>true</c> if [valuation needs update]; otherwise, <c>false</c>.</value>
  Public Property ValuationNeedsUpdate() As Boolean
    Get
      Return _ValuationNeedsUpdate
    End Get
    Set(ByVal value As Boolean)
      If (value) Then
        _ValuationNeedsUpdate = True
        _FeesNeedUpdate = True
        _ValuationObject = Nothing
        '_SubscriptionsObject = Array.CreateInstance(GetType(SR_InformationClass), 0)
        _SubscriptionsArrays.Clear()
        _FeesObject = Nothing
      End If
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the fund ID.
  ''' </summary>
  ''' <value>The fund ID.</value>
  Public Property FundID() As Integer
    Get
      Return _FundID
    End Get
    Set(ByVal value As Integer)
      If (_FundID <> value) Then
        ValuationNeedsUpdate = True

        _FundID = value
      End If
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets a value indicating whether [use administrator price dates].
  ''' </summary>
  ''' <value><c>true</c> if [use administrator price dates]; otherwise, <c>false</c>.</value>
  Public Property UseAdministratorPriceDates() As Boolean
    Get
      Return _UseAdministratorPriceDates
    End Get
    Set(ByVal value As Boolean)
      If (_UseAdministratorPriceDates <> value) Then
        ValuationNeedsUpdate = True
        _UseAdministratorPriceDates = value
      End If
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the fee start date.
  ''' </summary>
  ''' <value>The fee start date.</value>
  Public Property FeeStartDate() As Date
    Get
      Return _FeeStartDate
    End Get
    Set(ByVal value As Date)
      If (_FeeStartDate <> value) Then
        ValuationNeedsUpdate = True
        _FeeStartDate = value
        _OverrideFeeStartDate = True
      End If
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the fee date.
  ''' </summary>
  ''' <value>The fee date.</value>
  Public Property FeeDate() As Date
    Get
      Return _FeeDate
    End Get
    Set(ByVal value As Date)
      If (_FeeDate <> value) Then
        ValuationNeedsUpdate = True
        _FeeDate = value

      End If
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the status group filter.
  ''' </summary>
  ''' <value>The status group filter.</value>
  Public Property StatusGroupFilter() As String
    Get
      Return _StatusGroupFilter
    End Get
    Set(ByVal value As String)
      If (_StatusGroupFilter <> value) Then
        ValuationNeedsUpdate = True
        _StatusGroupFilter = value
      End If
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the administrator dates filter.
  ''' </summary>
  ''' <value>The administrator dates filter.</value>
  Public Property AdministratorDatesFilter() As Integer
    Get
      Return _AdministratorDatesFilter
    End Get
    Set(ByVal value As Integer)
      If (_AdministratorDatesFilter <> value) Then
        ValuationNeedsUpdate = True
        _AdministratorDatesFilter = value
      End If
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the exclude subscriptions since date.
  ''' </summary>
  ''' <value>The exclude subscriptions since date.</value>
  Public Property ExcludeSubscriptionsSinceDate() As Date
    Get
      Return _ExcludeSubscriptionsSinceDate
    End Get
    Set(ByVal value As Date)
      If (_ExcludeSubscriptionsSinceDate <> value) Then
        ValuationNeedsUpdate = True

        If (value <= Renaissance_BaseDate) Then
          _ExcludeSubscriptionsSinceDate = Renaissance_BaseDate
          _OverrideExcludeSubscriptionsSinceDate = False
        Else
          _ExcludeSubscriptionsSinceDate = value
          _OverrideExcludeSubscriptionsSinceDate = True
        End If
      End If
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the valuation expenses start date.
  ''' </summary>
  ''' <value>The valuation expenses start date.</value>
  Public Property ValuationExpensesStartDate() As Date
    Get
      Return _ValuationExpensesStartDate
    End Get
    Set(ByVal value As Date)
      If (_ValuationExpensesStartDate <> value) Then
        ValuationNeedsUpdate = True

        If (value <= Renaissance_BaseDate) Then
          _ValuationExpensesStartDate = Renaissance_BaseDate
          _OverrideValuationExpensesStartDate = False
        Else
          _ValuationExpensesStartDate = value
          _OverrideValuationExpensesStartDate = True
        End If
      End If
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the valuation performance fee start date.
  ''' </summary>
  ''' <value>The valuation performance fee start date.</value>
  Public Property ValuationPerformanceFeeStartDate() As Date
    Get
      Return _ValuationPerformanceFeeStartDate
    End Get
    Set(ByVal value As Date)
      If (_ValuationPerformanceFeeStartDate <> value) Then
        ValuationNeedsUpdate = True

        If (value <= Renaissance_BaseDate) Then
          _ValuationPerformanceFeeStartDate = Renaissance_BaseDate
          _OverrideValuationPerformanceFeeStartDate = False
        Else
          _ValuationPerformanceFeeStartDate = value
          _OverrideValuationPerformanceFeeStartDate = True
        End If
      End If
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the valuation management fee start date.
  ''' </summary>
  ''' <value>The valuation management fee start date.</value>
  Public Property ValuationManagementFeeStartDate() As Date
    Get
      Return _ValuationManagementFeeStartDate
    End Get
    Set(ByVal value As Date)
      If (_ValuationManagementFeeStartDate <> value) Then
        ValuationNeedsUpdate = True

        If (value <= Renaissance_BaseDate) Then
          _ValuationManagementFeeStartDate = Renaissance_BaseDate
          _OverrideValuationManagementFeeStartDate = False
        Else
          _ValuationManagementFeeStartDate = value
          _OverrideValuationManagementFeeStartDate = True
        End If
      End If
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the valuation crystalised performance fee start date.
  ''' </summary>
  ''' <value>The valuation crystalised performance fee start date.</value>
  Public Property ValuationCrystalisedPerformanceFeeStartDate() As Date
    Get
      Return _ValuationCrystalisedPerformanceFeeStartDate
    End Get
    Set(ByVal value As Date)
      If (_ValuationCrystalisedPerformanceFeeStartDate <> value) Then
        ValuationNeedsUpdate = True

        If (value <= Renaissance_BaseDate) Then
          _ValuationCrystalisedPerformanceFeeStartDate = Renaissance_BaseDate
          _OverrideValuationCrystalisedPerformanceFeeStartDate = False
        Else
          _ValuationCrystalisedPerformanceFeeStartDate = value
          _OverrideValuationCrystalisedPerformanceFeeStartDate = True
        End If
      End If
    End Set
  End Property

  Public ReadOnly Property SubscriptionsToDate(ByVal ValueDate As Date, ByVal IncludingThisDate As Boolean) As Dictionary(Of Integer, SR_InformationClass)
    Get
      Dim UnitID As Integer
      Dim SRTotals As SR_InformationClass
      Dim SingleSrClass As SR_InformationClass
      Dim RVal As New Dictionary(Of Integer, SR_InformationClass)

      If (_SubscriptionsArrays IsNot Nothing) Then

        For Each UnitID In _SubscriptionsArrays.Keys
          SRTotals = New SR_InformationClass

          SRTotals.InstrumentID = UnitID
          SRTotals.TradeDate = Renaissance_BaseDate

          RVal.Add(UnitID, SRTotals)

          For Each SingleSrClass In _SubscriptionsArrays(UnitID)

            If (SingleSrClass.TradeDate < ValueDate) OrElse ((IncludingThisDate) AndAlso (SingleSrClass.TradeDate <= ValueDate)) Then

              SRTotals.TradeCount += SingleSrClass.TradeCount
              SRTotals.UnitsRedeemed += SingleSrClass.UnitsRedeemed
              SRTotals.UnitsSubscribed += SingleSrClass.UnitsSubscribed
              SRTotals.ValueRedeemed += SingleSrClass.ValueRedeemed
              SRTotals.ValueSubscribed += SingleSrClass.ValueSubscribed

              If (SRTotals.TradeDate < SingleSrClass.TradeDate) Then SRTotals.TradeDate = SingleSrClass.TradeDate

            End If

          Next

        Next

      End If

      Return RVal
    End Get
  End Property

#End Region

  ''' <summary>
  ''' Class FundValuationDetails
  ''' </summary>
  Public Class FundValuationDetails

    ''' <summary>
    ''' The status string
    ''' </summary>
    Public StatusString As String

    ''' <summary>
    ''' The fund ID
    ''' </summary>
    Private _FundID As Integer
    ''' <summary>
    ''' The _ value date
    ''' </summary>
    Private _ValueDate As Date
    ''' <summary>
    ''' The _ status group filter
    ''' </summary>
    Private _StatusGroupFilter As String
    ''' <summary>
    ''' The _ administrator dates filter
    ''' </summary>
    Private _AdministratorDatesFilter As Integer
    ''' <summary>
    ''' The _ ex subscriptions after date
    ''' </summary>
    Private _ExSubscriptionsAfterDate As Date
    ''' <summary>
    ''' The _ ex fund expenses after date
    ''' </summary>
    Private _ExFundExpensesAfterDate As Date
    ''' <summary>
    ''' The _ ex management fees after date
    ''' </summary>
    Private _ExManagementFeesAfterDate As Date
    ''' <summary>
    ''' The _ ex performance fees after date
    ''' </summary>
    Private _ExPerformanceFeesAfterDate As Date
    ''' <summary>
    ''' The _ use administrator price dates
    ''' </summary>
    Private _UseAdministratorPriceDates As Integer
    ''' <summary>
    ''' The _ knowledge date
    ''' </summary>
    Private _KnowledgeDate As Date
    ''' <summary>
    ''' The _ fund NAV
    ''' </summary>
    Private _FundNAV As Double
    ''' <summary>
    ''' The _ fund GAV
    ''' </summary>
    Private _FundGAV As Double
    ''' <summary>
    ''' The _ fund BNAV
    ''' </summary>
    Private _FundBNAV As Double
    ''' <summary>
    ''' The _ fund GNAV
    ''' </summary>
    Private _FundGNAV As Double
    ''' <summary>
    ''' The _ fund NAV Reference
    ''' </summary>
    Private _FundNAV_Reference As Double
    ''' <summary>
    ''' The _ fund GAV Reference
    ''' </summary>
    Private _FundGAV_Reference As Double
    ''' <summary>
    ''' The _ fund BNAV Reference
    ''' </summary>
    Private _FundBNAV_Reference As Double
    ''' <summary>
    ''' The _ fund GNAV Reference Currency
    ''' </summary>
    Private _FundGNAV_Reference As Double
    ''' <summary>
    ''' The _ all expenses
    ''' </summary>
    Private _AllExpenses As Double
    ''' <summary>
    ''' The _ previous expenses
    ''' </summary>
    Private _PreviousExpenses As Double
    ''' <summary>
    ''' The _ pending expenses
    ''' </summary>
    Private _PendingExpenses As Double
    ''' <summary>
    ''' The _ all management fees
    ''' </summary>
    Private _AllManagementFees As Double
    ''' <summary>
    ''' The _ previous management fees
    ''' </summary>
    Private _PreviousManagementFees As Double
    ''' <summary>
    ''' The _ pending management fees
    ''' </summary>
    Private _PendingManagementFees As Double
    ''' <summary>
    ''' The _ all performance fees
    ''' </summary>
    Private _AllPerformanceFees As Double
    ''' <summary>
    ''' The _ previous performance fees
    ''' </summary>
    Private _PreviousPerformanceFees As Double
    ''' <summary>
    ''' The _ pending performance fees
    ''' </summary>
    Private _PendingPerformanceFees As Double
    ''' <summary>
    ''' The _ all performance fees crystalised
    ''' </summary>
    Private _AllPerformanceFeesCrystalised As Double
    ''' <summary>
    ''' The _ previous performance fees crystalised
    ''' </summary>
    Private _PreviousPerformanceFeesCrystalised As Double
    ''' <summary>
    ''' The _ pending performance fees crystalised
    ''' </summary>
    Private _PendingPerformanceFeesCrystalised As Double
    ''' <summary>
    ''' The _ all expenses_ USD
    ''' </summary>
    Private _AllExpenses_Reference As Double
    ''' <summary>
    ''' The _ previous expenses_ USD
    ''' </summary>
    Private _PreviousExpenses_Reference As Double
    ''' <summary>
    ''' The _ pending expenses_ USD
    ''' </summary>
    Private _PendingExpenses_Reference As Double
    ''' <summary>
    ''' The _ all management fees_ USD
    ''' </summary>
    Private _AllManagementFees_Reference As Double
    ''' <summary>
    ''' The _ previous management fees_ USD
    ''' </summary>
    Private _PreviousManagementFees_Reference As Double
    ''' <summary>
    ''' The _ pending management fees_ USD
    ''' </summary>
    Private _PendingManagementFees_Reference As Double
    ''' <summary>
    ''' The _ all performance fees_ USD
    ''' </summary>
    Private _AllPerformanceFees_Reference As Double
    ''' <summary>
    ''' The _ previous performance fees_ USD
    ''' </summary>
    Private _PreviousPerformanceFees_Reference As Double
    ''' <summary>
    ''' The _ pending performance fees_ USD
    ''' </summary>
    Private _PendingPerformanceFees_Reference As Double
    ''' <summary>
    ''' The _ all performance fees crystalised_ USD
    ''' </summary>
    Private _AllPerformanceFeesCrystalised_Reference As Double
    ''' <summary>
    ''' The _ previous performance fees crystalised_ USD
    ''' </summary>
    Private _PreviousPerformanceFeesCrystalised_Reference As Double
    ''' <summary>
    ''' The _ pending performance fees crystalised_ USD
    ''' </summary>
    Private _PendingPerformanceFeesCrystalised_Reference As Double
    ''' <summary>
    ''' The _ fund units
    ''' </summary>
    Private _FundUnits As Double
    ''' <summary>
    ''' The _ fund units subscribed
    ''' </summary>
    Private _FundUnitsSubscribed As Double
    ''' <summary>
    ''' The _ fund units redeemed
    ''' </summary>
    Private _FundUnitsRedeemed As Double
    ''' <summary>
    ''' The _ fund value subscribed
    ''' </summary>
    Private _FundValueSubscribed As Double
    ''' <summary>
    ''' The _ fund value redeemed
    ''' </summary>
    Private _FundValueRedeemed As Double
    ''' <summary>
    ''' The _ fund value subscriptions this year
    ''' </summary>
    Private _FundValueSubscriptionsThisYear As Double
    ''' <summary>
    ''' The _ fund value redemptions this year
    ''' </summary>
    Private _FundValueRedemptionsThisYear As Double

    ''' <summary>
    ''' The _ fund used administrator dates
    ''' </summary>
    Private _FundUsedAdministratorDates As Integer

    ''' <summary>
    ''' The params_ fund last year end
    ''' </summary>
    Public params_FundLastYearEnd As Date
    ''' <summary>
    ''' The params_ ex subscriptions after date
    ''' </summary>
    Public params_ExSubscriptionsAfterDate As Date
    ''' <summary>
    ''' The params_ ex fund expenses after date
    ''' </summary>
    Public params_ExFundExpensesAfterDate As Date
    ''' <summary>
    ''' The params_ ex management fees after date
    ''' </summary>
    Public params_ExManagementFeesAfterDate As Date
    ''' <summary>
    ''' The params_ ex performance fees after date
    ''' </summary>
    Public params_ExPerformanceFeesAfterDate As Date
    ''' <summary>
    ''' The params_ ex crystalised performance fees after date
    ''' </summary>
    Public params_ExCrystalisedPerformanceFeesAfterDate As Date
    ''' <summary>
    ''' The params_ use administrator price dates
    ''' </summary>
    Public params_UseAdministratorPriceDates As Integer

    ''' <summary>
    ''' Prevents a default instance of the <see cref="FundValuationDetails"/> class from being created.
    ''' </summary>
    Private Sub New()
      ' Prevent default insnantiation
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="FundValuationDetails"/> class.
    ''' </summary>
    ''' <param name="Status">The status.</param>
    Sub New(ByVal Status As String)
      Me.StatusString = Status
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="FundValuationDetails"/> class.
    ''' </summary>
    ''' <param name="FundID">The fund ID.</param>
    ''' <param name="ValueDate">The value date.</param>
    ''' <param name="PreviousYearEndDate">The previous year end date.</param>
    ''' <param name="StatusGroupFilter">The status group filter.</param>
    ''' <param name="AdministratorDatesFilter">The administrator dates filter.</param>
    ''' <param name="ExSubscriptionsAfterDate">The ex subscriptions after date.</param>
    ''' <param name="ExFundExpensesAfterDate">The ex fund expenses after date.</param>
    ''' <param name="ExManagementFeesAfterDate">The ex management fees after date.</param>
    ''' <param name="ExPerformanceFeesAfterDate">The ex performance fees after date.</param>
    ''' <param name="UseAdministratorPriceDates">The use administrator price dates.</param>
    ''' <param name="KnowledgeDate">The knowledge date.</param>
    ''' <param name="FundNAV">The fund NAV.</param>
    ''' <param name="FundGAV">The fund GAV.</param>
    ''' <param name="FundBNAV">The fund BNAV.</param>
    ''' <param name="FundGNAV">The fund GNAV.</param>
    ''' <param name="FundNAV_Reference">The fund NAV Reference Currency.</param>
    ''' <param name="FundGAV_Reference">The fund GAV Reference Currency.</param>
    ''' <param name="FundBNAV_Reference">The fund BNAV Reference Currency.</param>
    ''' <param name="FundGNAV_Reference">The fund GNAV Reference Currency.</param>
    ''' <param name="AllExpenses">All expenses.</param>
    ''' <param name="PreviousExpenses">The previous expenses.</param>
    ''' <param name="PendingExpenses">The pending expenses.</param>
    ''' <param name="AllManagementFees">All management fees.</param>
    ''' <param name="PreviousManagementFees">The previous management fees.</param>
    ''' <param name="PendingManagementFees">The pending management fees.</param>
    ''' <param name="AllPerformanceFees">All performance fees.</param>
    ''' <param name="PreviousPerformanceFees">The previous performance fees.</param>
    ''' <param name="PendingPerformanceFees">The pending performance fees.</param>
    ''' <param name="AllPerformanceFeesCrystalised">All performance fees crystalised.</param>
    ''' <param name="PreviousPerformanceFeesCrystalised">The previous performance fees crystalised.</param>
    ''' <param name="PendingPerformanceFeesCrystalised">The pending performance fees crystalised.</param>
    ''' <param name="AllExpenses_Reference">All expenses_ USD.</param>
    ''' <param name="PreviousExpenses_Reference">The previous expenses_ USD.</param>
    ''' <param name="PendingExpenses_Reference">The pending expenses_ USD.</param>
    ''' <param name="AllManagementFees_Reference">All management fees_ USD.</param>
    ''' <param name="PreviousManagementFees_Reference">The previous management fees_ USD.</param>
    ''' <param name="PendingManagementFees_Reference">The pending management fees_ USD.</param>
    ''' <param name="AllPerformanceFees_Reference">All performance fees_ USD.</param>
    ''' <param name="PreviousPerformanceFees_Reference">The previous performance fees_ USD.</param>
    ''' <param name="PendingPerformanceFees_Reference">The pending performance fees_ USD.</param>
    ''' <param name="AllPerformanceFeesCrystalised_Reference">All performance fees crystalised_ USD.</param>
    ''' <param name="PreviousPerformanceFeesCrystalised_Reference">The previous performance fees crystalised_ USD.</param>
    ''' <param name="PendingPerformanceFeesCrystalised_Reference">The pending performance fees crystalised_ USD.</param>
    ''' <param name="FundUnits">The fund units.</param>
    ''' <param name="FundUnitsSubscribed">The fund units subscribed.</param>
    ''' <param name="FundUnitsRedeemed">The fund units redeemed.</param>
    ''' <param name="FundValueSubscribed">The fund value subscribed.</param>
    ''' <param name="FundValueRedeemed">The fund value redeemed.</param>
    ''' <param name="FundValueSubscriptionsThisYear">The fund value subscriptions this year.</param>
    ''' <param name="FundValueRedemptionsThisYear">The fund value redemptions this year.</param>
    ''' <param name="FundUsedAdministratorDates">The fund used administrator dates.</param>
    Public Sub New(ByVal FundID As Integer, ByVal ValueDate As Date, ByVal PreviousYearEndDate As Date, ByVal StatusGroupFilter As String, ByVal AdministratorDatesFilter As Integer, _
    ByVal ExSubscriptionsAfterDate As Date, ByVal ExFundExpensesAfterDate As Date, ByVal ExManagementFeesAfterDate As Date, ByVal ExPerformanceFeesAfterDate As Date, _
    ByVal UseAdministratorPriceDates As Integer, ByVal KnowledgeDate As Date, ByVal FundNAV As Double, ByVal FundGAV As Double, ByVal FundBNAV As Double, ByVal FundGNAV As Double, ByVal FundNAV_Reference As Double, ByVal FundGAV_Reference As Double, ByVal FundBNAV_Reference As Double, ByVal FundGNAV_Reference As Double, _
    ByVal AllExpenses As Double, ByVal PreviousExpenses As Double, ByVal PendingExpenses As Double, _
    ByVal AllManagementFees As Double, ByVal PreviousManagementFees As Double, ByVal PendingManagementFees As Double, _
    ByVal AllPerformanceFees As Double, ByVal PreviousPerformanceFees As Double, ByVal PendingPerformanceFees As Double, _
    ByVal AllPerformanceFeesCrystalised As Double, ByVal PreviousPerformanceFeesCrystalised As Double, ByVal PendingPerformanceFeesCrystalised As Double, _
    ByVal AllExpenses_Reference As Double, ByVal PreviousExpenses_Reference As Double, ByVal PendingExpenses_Reference As Double, _
    ByVal AllManagementFees_Reference As Double, ByVal PreviousManagementFees_Reference As Double, ByVal PendingManagementFees_Reference As Double, _
    ByVal AllPerformanceFees_Reference As Double, ByVal PreviousPerformanceFees_Reference As Double, ByVal PendingPerformanceFees_Reference As Double, _
    ByVal AllPerformanceFeesCrystalised_Reference As Double, ByVal PreviousPerformanceFeesCrystalised_Reference As Double, ByVal PendingPerformanceFeesCrystalised_Reference As Double, _
    ByVal FundUnits As Double, ByVal FundUnitsSubscribed As Double, ByVal FundUnitsRedeemed As Double, ByVal FundValueSubscribed As Double, ByVal FundValueRedeemed As Double, ByVal FundValueSubscriptionsThisYear As Double, ByVal FundValueRedemptionsThisYear As Double, ByVal FundUsedAdministratorDates As Integer)

      _FundID = FundID
      _ValueDate = ValueDate
      _StatusGroupFilter = StatusGroupFilter
      _AdministratorDatesFilter = AdministratorDatesFilter
      _ExSubscriptionsAfterDate = ExSubscriptionsAfterDate
      _ExFundExpensesAfterDate = ExFundExpensesAfterDate
      _ExManagementFeesAfterDate = ExManagementFeesAfterDate
      _ExPerformanceFeesAfterDate = ExPerformanceFeesAfterDate
      _UseAdministratorPriceDates = UseAdministratorPriceDates
      _KnowledgeDate = KnowledgeDate
      _FundNAV = FundNAV
      _FundGAV = FundGAV
      _FundBNAV = FundBNAV
      _FundGNAV = FundGNAV
      _FundNAV_Reference = FundNAV_Reference
      _FundGAV_Reference = FundGAV_Reference
      _FundBNAV_Reference = FundBNAV_Reference
      _FundGNAV_Reference = FundGNAV_Reference
      _AllExpenses = AllExpenses
      _PreviousExpenses = PreviousExpenses
      _PendingExpenses = PendingExpenses
      _AllManagementFees = AllManagementFees
      _PreviousManagementFees = PreviousManagementFees
      _PendingManagementFees = PendingManagementFees
      _AllPerformanceFees = AllPerformanceFees
      _PreviousPerformanceFees = PreviousPerformanceFees
      _PendingPerformanceFees = PendingPerformanceFees
      _AllPerformanceFeesCrystalised = AllPerformanceFeesCrystalised
      _PreviousPerformanceFeesCrystalised = PreviousPerformanceFeesCrystalised
      _PendingPerformanceFeesCrystalised = PendingPerformanceFeesCrystalised
      _AllExpenses_Reference = AllExpenses_Reference
      _PreviousExpenses_Reference = PreviousExpenses_Reference
      _PendingExpenses_Reference = PendingExpenses_Reference
      _AllManagementFees_Reference = AllManagementFees_Reference
      _PreviousManagementFees_Reference = PreviousManagementFees_Reference
      _PendingManagementFees_Reference = PendingManagementFees_Reference
      _AllPerformanceFees_Reference = AllPerformanceFees_Reference
      _PreviousPerformanceFees_Reference = PreviousPerformanceFees_Reference
      _PendingPerformanceFees_Reference = PendingPerformanceFees_Reference
      _AllPerformanceFeesCrystalised_Reference = AllPerformanceFeesCrystalised_Reference
      _PreviousPerformanceFeesCrystalised_Reference = PreviousPerformanceFeesCrystalised_Reference
      _PendingPerformanceFeesCrystalised_Reference = PendingPerformanceFeesCrystalised_Reference
      _FundUnits = FundUnits
      _FundUnitsSubscribed = FundUnitsSubscribed
      _FundUnitsRedeemed = FundUnitsRedeemed
      _FundValueSubscribed = FundValueSubscribed
      _FundValueRedeemed = FundValueRedeemed
      _FundValueSubscriptionsThisYear = FundValueSubscriptionsThisYear
      _FundValueRedemptionsThisYear = FundValueRedemptionsThisYear
      _FundUsedAdministratorDates = FundUsedAdministratorDates

    End Sub

    ''' <summary>
    ''' Gets the fund ID.
    ''' </summary>
    ''' <value>The fund ID.</value>
    Public ReadOnly Property FundID() As Integer
      Get
        Return _FundID
      End Get
    End Property

    ''' <summary>
    ''' Gets the value date.
    ''' </summary>
    ''' <value>The value date.</value>
    Public ReadOnly Property ValueDate() As Date
      Get
        Return _ValueDate
      End Get
    End Property

    ''' <summary>
    ''' Gets the status group filter.
    ''' </summary>
    ''' <value>The status group filter.</value>
    Public ReadOnly Property StatusGroupFilter() As String
      Get
        Return _StatusGroupFilter
      End Get
    End Property

    ''' <summary>
    ''' Gets the administrator dates filter.
    ''' </summary>
    ''' <value>The administrator dates filter.</value>
    Public ReadOnly Property AdministratorDatesFilter() As Integer
      Get
        Return _AdministratorDatesFilter
      End Get
    End Property

    ''' <summary>
    ''' Gets the ex subscriptions after date.
    ''' </summary>
    ''' <value>The ex subscriptions after date.</value>
    Public ReadOnly Property ExSubscriptionsAfterDate() As Date
      Get
        Return _ExSubscriptionsAfterDate
      End Get
    End Property

    ''' <summary>
    ''' Gets the ex fund expenses after date.
    ''' </summary>
    ''' <value>The ex fund expenses after date.</value>
    Public ReadOnly Property ExFundExpensesAfterDate() As Date
      Get
        Return _ExFundExpensesAfterDate
      End Get
    End Property

    ''' <summary>
    ''' Gets the ex management fees after date.
    ''' </summary>
    ''' <value>The ex management fees after date.</value>
    Public ReadOnly Property ExManagementFeesAfterDate() As Date
      Get
        Return _ExManagementFeesAfterDate
      End Get
    End Property

    ''' <summary>
    ''' Gets the ex performance fees after date.
    ''' </summary>
    ''' <value>The ex performance fees after date.</value>
    Public ReadOnly Property ExPerformanceFeesAfterDate() As Date
      Get
        Return _ExPerformanceFeesAfterDate
      End Get
    End Property

    ''' <summary>
    ''' Gets the use administrator price dates.
    ''' </summary>
    ''' <value>The use administrator price dates.</value>
    Public ReadOnly Property UseAdministratorPriceDates() As Integer
      Get
        Return _UseAdministratorPriceDates
      End Get
    End Property

    ''' <summary>
    ''' Gets the knowledge date.
    ''' </summary>
    ''' <value>The knowledge date.</value>
    Public ReadOnly Property KnowledgeDate() As Date
      Get
        Return _KnowledgeDate
      End Get
    End Property

    ''' <summary>
    ''' Gets the fund NAV.
    ''' </summary>
    ''' <value>The fund NAV.</value>
    Public ReadOnly Property FundNAV() As Double
      Get
        Return _FundNAV
      End Get
    End Property

    ''' <summary>
    ''' Gets the fund GAV.
    ''' </summary>
    ''' <value>The fund GAV.</value>
    Public ReadOnly Property FundGAV() As Double
      Get
        Return _FundGAV
      End Get
    End Property

    ''' <summary>
    ''' Gets the fund BNAV.
    ''' </summary>
    ''' <value>The fund BNAV.</value>
    Public ReadOnly Property FundBNAV() As Double
      Get
        Return _FundBNAV
      End Get
    End Property

    ''' <summary>
    ''' Gets the fund GNAV.
    ''' </summary>
    ''' <value>The fund GNAV.</value>
    Public ReadOnly Property FundGNAV() As Double
      Get
        Return _FundGNAV
      End Get
    End Property

    ''' <summary>
    ''' Gets the fund NAV Reference Currency.
    ''' </summary>
    ''' <value>The fund NAV Reference Currency.</value>
    Public ReadOnly Property FundNAV_Reference() As Double
      Get
        Return _FundNAV_Reference
      End Get
    End Property

    ''' <summary>
    ''' Gets the fund GAV Reference Currency.
    ''' </summary>
    ''' <value>The fund GAV Reference Currency.</value>
    Public ReadOnly Property FundGAV_Reference() As Double
      Get
        Return _FundGAV_Reference
      End Get
    End Property

    ''' <summary>
    ''' Gets the fund BNAV Reference Currency.
    ''' </summary>
    ''' <value>The fund BNAV Reference Currency.</value>
    Public ReadOnly Property FundBNAV_Reference() As Double
      Get
        Return _FundBNAV_Reference
      End Get
    End Property

    ''' <summary>
    ''' Gets the fund GNAV Reference Currency.
    ''' </summary>
    ''' <value>The fund GNAV Reference Currency.</value>
    Public ReadOnly Property FundGNAV_Reference() As Double
      Get
        Return _FundGNAV_Reference
      End Get
    End Property

    ''' <summary>
    ''' Gets all expenses.
    ''' </summary>
    ''' <value>All expenses.</value>
    Public ReadOnly Property AllExpenses() As Double
      Get
        Return _AllExpenses
      End Get
    End Property

    ''' <summary>
    ''' Gets the previous expenses.
    ''' </summary>
    ''' <value>The previous expenses.</value>
    Public ReadOnly Property PreviousExpenses() As Double
      Get
        Return _PreviousExpenses
      End Get
    End Property

    ''' <summary>
    ''' Gets the pending expenses.
    ''' </summary>
    ''' <value>The pending expenses.</value>
    Public ReadOnly Property PendingExpenses() As Double
      Get
        Return _PendingExpenses
      End Get
    End Property

    ''' <summary>
    ''' Gets all management fees.
    ''' </summary>
    ''' <value>All management fees.</value>
    Public ReadOnly Property AllManagementFees() As Double
      Get
        Return _AllManagementFees
      End Get
    End Property

    ''' <summary>
    ''' Gets the previous management fees.
    ''' </summary>
    ''' <value>The previous management fees.</value>
    Public ReadOnly Property PreviousManagementFees() As Double
      Get
        Return _PreviousManagementFees
      End Get
    End Property

    ''' <summary>
    ''' Gets the pending management fees.
    ''' </summary>
    ''' <value>The pending management fees.</value>
    Public ReadOnly Property PendingManagementFees() As Double
      Get
        Return _PendingManagementFees
      End Get
    End Property

    ''' <summary>
    ''' Gets all performance fees.
    ''' </summary>
    ''' <value>All performance fees.</value>
    Public ReadOnly Property AllPerformanceFees() As Double
      Get
        Return _AllPerformanceFees
      End Get
    End Property

    ''' <summary>
    ''' Gets the previous performance fees.
    ''' </summary>
    ''' <value>The previous performance fees.</value>
    Public ReadOnly Property PreviousPerformanceFees() As Double
      Get
        Return _PreviousPerformanceFees
      End Get
    End Property

    ''' <summary>
    ''' Gets the pending performance fees.
    ''' </summary>
    ''' <value>The pending performance fees.</value>
    Public ReadOnly Property PendingPerformanceFees() As Double
      Get
        Return _PendingPerformanceFees
      End Get
    End Property

    ''' <summary>
    ''' Gets all performance fees crystalised.
    ''' </summary>
    ''' <value>All performance fees crystalised.</value>
    Public ReadOnly Property AllPerformanceFeesCrystalised() As Double
      Get
        Return _AllPerformanceFeesCrystalised
      End Get
    End Property

    ''' <summary>
    ''' Gets the previous performance fees crystalised.
    ''' </summary>
    ''' <value>The previous performance fees crystalised.</value>
    Public ReadOnly Property PreviousPerformanceFeesCrystalised() As Double
      Get
        Return _PreviousPerformanceFeesCrystalised
      End Get
    End Property

    ''' <summary>
    ''' Gets the pending performance fees crystalised.
    ''' </summary>
    ''' <value>The pending performance fees crystalised.</value>
    Public ReadOnly Property PendingPerformanceFeesCrystalised() As Double
      Get
        Return _PendingPerformanceFeesCrystalised
      End Get
    End Property

    ''' <summary>
    ''' Gets all expenses_ USD.
    ''' </summary>
    ''' <value>All expenses_ USD.</value>
    Public ReadOnly Property AllExpenses_Reference() As Double
      Get
        Return _AllExpenses_Reference
      End Get
    End Property

    ''' <summary>
    ''' Gets the previous expenses_ USD.
    ''' </summary>
    ''' <value>The previous expenses_ USD.</value>
    Public ReadOnly Property PreviousExpenses_Reference() As Double
      Get
        Return _PreviousExpenses_Reference
      End Get
    End Property

    ''' <summary>
    ''' Gets the pending expenses_ USD.
    ''' </summary>
    ''' <value>The pending expenses_ USD.</value>
    Public ReadOnly Property PendingExpenses_Reference() As Double
      Get
        Return _PendingExpenses_Reference
      End Get
    End Property

    ''' <summary>
    ''' Gets all management fees_ USD.
    ''' </summary>
    ''' <value>All management fees_ USD.</value>
    Public ReadOnly Property AllManagementFees_Reference() As Double
      Get
        Return _AllManagementFees_Reference
      End Get
    End Property

    ''' <summary>
    ''' Gets the previous management fees_ USD.
    ''' </summary>
    ''' <value>The previous management fees_ USD.</value>
    Public ReadOnly Property PreviousManagementFees_Reference() As Double
      Get
        Return _PreviousManagementFees_Reference
      End Get
    End Property

    ''' <summary>
    ''' Gets the pending management fees_ USD.
    ''' </summary>
    ''' <value>The pending management fees_ USD.</value>
    Public ReadOnly Property PendingManagementFees_Reference() As Double
      Get
        Return _PendingManagementFees_Reference
      End Get
    End Property

    ''' <summary>
    ''' Gets all performance fees_ USD.
    ''' </summary>
    ''' <value>All performance fees_ USD.</value>
    Public ReadOnly Property AllPerformanceFees_Reference() As Double
      Get
        Return _AllPerformanceFees_Reference
      End Get
    End Property

    ''' <summary>
    ''' Gets the previous performance fees_ USD.
    ''' </summary>
    ''' <value>The previous performance fees_ USD.</value>
    Public ReadOnly Property PreviousPerformanceFees_Reference() As Double
      Get
        Return _PreviousPerformanceFees_Reference
      End Get
    End Property

    ''' <summary>
    ''' Gets the pending performance fees_ USD.
    ''' </summary>
    ''' <value>The pending performance fees_ USD.</value>
    Public ReadOnly Property PendingPerformanceFees_Reference() As Double
      Get
        Return _PendingPerformanceFees_Reference
      End Get
    End Property

    ''' <summary>
    ''' Gets all performance fees crystalised_ USD.
    ''' </summary>
    ''' <value>All performance fees crystalised_ USD.</value>
    Public ReadOnly Property AllPerformanceFeesCrystalised_Reference() As Double
      Get
        Return _AllPerformanceFeesCrystalised_Reference
      End Get
    End Property

    ''' <summary>
    ''' Gets the previous performance fees crystalised_ USD.
    ''' </summary>
    ''' <value>The previous performance fees crystalised_ USD.</value>
    Public ReadOnly Property PreviousPerformanceFeesCrystalised_Reference() As Double
      Get
        Return _PreviousPerformanceFeesCrystalised_Reference
      End Get
    End Property

    ''' <summary>
    ''' Gets the pending performance fees crystalised_ USD.
    ''' </summary>
    ''' <value>The pending performance fees crystalised_ USD.</value>
    Public ReadOnly Property PendingPerformanceFeesCrystalised_Reference() As Double
      Get
        Return _PendingPerformanceFeesCrystalised_Reference
      End Get
    End Property

    ''' <summary>
    ''' Gets the fund units.
    ''' </summary>
    ''' <value>The fund units.</value>
    Public ReadOnly Property FundUnits() As Double
      Get
        Return _FundUnits
      End Get
    End Property

    ''' <summary>
    ''' Gets the fund units subscribed.
    ''' </summary>
    ''' <value>The fund units subscribed.</value>
    Public ReadOnly Property FundUnitsSubscribed() As Double
      Get
        Return _FundUnitsSubscribed
      End Get
    End Property

    ''' <summary>
    ''' Gets the fund units redeemed.
    ''' </summary>
    ''' <value>The fund units redeemed.</value>
    Public ReadOnly Property FundUnitsRedeemed() As Double
      Get
        Return _FundUnitsRedeemed
      End Get
    End Property

    ''' <summary>
    ''' Gets the fund value subscribed.
    ''' </summary>
    ''' <value>The fund value subscribed.</value>
    Public ReadOnly Property FundValueSubscribed() As Double
      Get
        Return _FundValueSubscribed
      End Get
    End Property

    ''' <summary>
    ''' Gets the fund value redeemed.
    ''' </summary>
    ''' <value>The fund value redeemed.</value>
    Public ReadOnly Property FundValueRedeemed() As Double
      Get
        Return _FundValueRedeemed
      End Get
    End Property

    ''' <summary>
    ''' Gets the fund value subscriptions this year.
    ''' </summary>
    ''' <value>The fund value subscriptions this year.</value>
    Public ReadOnly Property FundValueSubscriptionsThisYear() As Double
      Get
        Return _FundValueSubscriptionsThisYear
      End Get
    End Property

    ''' <summary>
    ''' Gets the fund value redemptions this year.
    ''' </summary>
    ''' <value>The fund value redemptions this year.</value>
    Public ReadOnly Property FundValueRedemptionsThisYear() As Double
      Get
        Return _FundValueRedemptionsThisYear
      End Get
    End Property

    ''' <summary>
    ''' Gets the fund used administrator dates.
    ''' </summary>
    ''' <value>The fund used administrator dates.</value>
    Public ReadOnly Property FundUsedAdministratorDates() As Integer
      Get
        Return _FundUsedAdministratorDates
      End Get
    End Property

  End Class

  ''' <summary>
  ''' Class FundFeeDetails
  ''' </summary>
  Public Class FundFeeDetails

    ''' <summary>
    ''' The fund ID
    ''' </summary>
    Public FundID As Integer
    ''' <summary>
    ''' The start date
    ''' </summary>
    Public StartDate As Date
    ''' <summary>
    ''' The value date
    ''' </summary>
    Public ValueDate As Date
    ''' <summary>
    ''' The fund start value
    ''' </summary>
    Public FundStartValue As Double
    ''' <summary>
    ''' The fund year start
    ''' </summary>
    Public FundYearStart As Date
    ''' <summary>
    ''' The fund year end
    ''' </summary>
    Public FundYearEnd As Date
    ''' <summary>
    ''' The days in financial year
    ''' </summary>
    Public DaysInFinancialYear As Integer
    ''' <summary>
    ''' The days so far
    ''' </summary>
    Public DaysSoFar As Integer

    ''' <summary>
    ''' The fund uses equalisation
    ''' </summary>
    Public FundUsesEqualisation As Integer
    ''' <summary>
    ''' The management fees percent
    ''' </summary>
    Public ManagementFeesPercent As Double
    ''' <summary>
    ''' The performance fees percent
    ''' </summary>
    Public PerformanceFeesPercent As Double

    ''' <summary>
    ''' The management fees
    ''' </summary>
    Public ManagementFees As Double
    ''' <summary>
    ''' The performance fees
    ''' </summary>
    Public PerformanceFees As Double
    ''' <summary>
    ''' The performance fees crystalised
    ''' </summary>
    Public PerformanceFeesCrystalised As Double

    ''' <summary>
    ''' The valuation used
    ''' </summary>
    Public ValuationUsed As FundValuationDetails
    ''' <summary>
    ''' The fund performance gnav used
    ''' </summary>
    Public FundPerformanceGnavUsed As Double
    ''' <summary>
    ''' The benchmark NAV
    ''' </summary>
    Public BenchmarkNAV As Double

    ''' <summary>
    ''' The best benchmark
    ''' </summary>
    Public BestBenchmarkItemID As Integer
    ''' <summary>
    ''' The best benchmark return
    ''' </summary>
    Public BestBenchmarkReturn As Double
    ''' <summary>
    ''' The best benchmark description
    ''' </summary>
    Public BestBenchmarkDescription As String
    ''' <summary>
    ''' The todays redemptions
    ''' </summary>
    Public TodaysRedemptions As Double

    ''' <summary>
    ''' The dates array
    ''' </summary>
    Public DatesArray() As Date
    ''' <summary>
    ''' The benchmark NAV array
    ''' </summary>
    Public BenchmarkNAVArray() As Double
    ''' <summary>
    ''' The units subscribed
    ''' </summary>
    Public UnitsSubscribed() As Double
    ''' <summary>
    ''' The units redeemed
    ''' </summary>
    Public UnitsRedeemed() As Double
    ''' <summary>
    ''' The units running total
    ''' </summary>
    Public UnitsRunningTotal() As Double
    ''' <summary>
    ''' The benchmark value array
    ''' </summary>
    Public BenchmarkValueArray() As Double

    ''' <summary>
    ''' The status string
    ''' </summary>
    Public StatusString As String

    ''' <summary>
    ''' Initializes a new instance of the <see cref="FundFeeDetails"/> class.
    ''' </summary>
    Public Sub New()

      Try

        FundID = 0
        StartDate = #1/1/1900#
        ValueDate = #1/1/1900#
        FundUsesEqualisation = 0
        ManagementFees = 0.0#
        PerformanceFees = 0.0#
        PerformanceFeesCrystalised = 0.0#
        DaysInFinancialYear = 0
        DaysSoFar = 0
        BenchmarkNAV = 0.0#
        FundPerformanceGnavUsed = 0.0#
        BestBenchmarkItemID = 0

        StatusString = ""

      Catch ex As Exception
      End Try

    End Sub

  End Class

  ''' <summary>
  ''' Prevents a default instance of the <see cref="FundFeeCalculator"/> class from being created.
  ''' </summary>
  Private Sub New()
    ' Prevent Instantiation without Mainform parameter.

  End Sub

  ''' <summary>
  ''' Initializes a new instance of the <see cref="FundFeeCalculator"/> class.
  ''' </summary>
  ''' <param name="pMainform">The p mainform.</param>
  ''' <param name="FundID">The fund ID.</param>
  ''' <param name="FeeDate">The fee date.</param>
  ''' <param name="StatusGroupFilter">The status group filter.</param>
  ''' <param name="AdministratorDatesFilter">The administrator dates filter.</param>
  Public Sub New(ByRef pMainform As VeniceMain, ByRef linkedReconciliationform As frmReconciliationProcess, ByVal FundID As Integer, ByVal FeeDate As Date, ByVal StatusGroupFilter As String, ByVal AdministratorDatesFilter As Integer)

    Try

      Mainform = pMainform

      ' Used to calculate Unit price information when necessary :

      Me.Reset()

      _FundID = FundID
      _FeeDate = FeeDate
      _StatusGroupFilter = StatusGroupFilter
      _AdministratorDatesFilter = AdministratorDatesFilter
      _linkedReconciliationform = linkedReconciliationform

      If (_linkedReconciliationform IsNot Nothing) Then
        AdministratorInventoryCache = _linkedReconciliationform.AdministratorInventoryCache
        AdministratorHoldingsCache = _linkedReconciliationform.AdministratorHoldingsCache
      Else
        AdministratorInventoryCache = Nothing
        AdministratorHoldingsCache = Nothing
      End If
    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Resets this instance.
  ''' </summary>
  Public Sub Reset()

    Try

      _ValuationObject = Nothing
      _FeesObject = Nothing

      If (_SubscriptionsArrays Is Nothing) Then
        _SubscriptionsArrays = New Dictionary(Of Integer, SR_InformationClass())
      Else
        _SubscriptionsArrays.Clear()
      End If

      _FundID = 0
      _FeeDate = RenaissanceGlobals.Globals.Renaissance_BaseDate
      _FeeStartDate = RenaissanceGlobals.Globals.Renaissance_BaseDate
      _StatusGroupFilter = ""
      _AdministratorDatesFilter = 0
      _UseAdministratorPriceDates = False
      ValuationNeedsUpdate = True
      _OverrideFeeStartDate = False
      _OverrideExcludeSubscriptionsSinceDate = False
      _ExcludeSubscriptionsSinceDate = RenaissanceGlobals.Globals.Renaissance_BaseDate
      _OverrideValuationExpensesStartDate = False
      _ValuationExpensesStartDate = RenaissanceGlobals.Globals.Renaissance_BaseDate
      _OverrideValuationPerformanceFeeStartDate = False
      _ValuationPerformanceFeeStartDate = RenaissanceGlobals.Globals.Renaissance_BaseDate
      _OverrideValuationCrystalisedPerformanceFeeStartDate = False
      _ValuationCrystalisedPerformanceFeeStartDate = RenaissanceGlobals.Globals.Renaissance_BaseDate
      _OverrideValuationManagementFeeStartDate = False
      _ValuationManagementFeeStartDate = RenaissanceGlobals.Globals.Renaissance_BaseDate



    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Gets the fund valuation.
  ''' </summary>
  ''' <returns>FundValuationDetails.</returns>
  Public Function GetFundValuation() As FundValuationDetails
    ' ********************************************************************************
    '
    '
    '
    ' ********************************************************************************

    Try

      If _ValuationNeedsUpdate Then
        Call CalculateFundValuation()
      End If

    Catch ex As Exception
    End Try

    Return _ValuationObject

  End Function

  ''' <summary>
  ''' Calculates the fund valuation.
  ''' </summary>
  ''' <returns><c>true</c> on success, <c>false</c> otherwise</returns>
  Public Function CalculateFundValuation() As Boolean
    ' ********************************************************************************
    '
    '
    '
    ' ********************************************************************************

    Dim RVal As FundValuationDetails = Nothing
    Dim thisConnection As SqlConnection = Nothing

    Try

      _ValuationObject = Nothing

      Dim ThisCommand As SqlCommand = Nothing
      Dim TempAdaptor As New SqlDataAdapter
      Dim FundValuations As DataTable
      Dim SubscriptionsTable As DataTable

      Dim FundRow As DSFund.tblFundRow
      Dim FundPricingPeriod As DealingPeriod = RenaissanceGlobals.DealingPeriod.Daily
      Dim DSMilestones As DSFundMilestones
      Dim tblMilestones As DSFundMilestones.tblFundMilestonesDataTable
      Dim SelectedMilestones() As DSFundMilestones.tblFundMilestonesRow
      Dim DSWatermarks As DSFundPerformanceFeePoints
      Dim tblWatermarks As DSFundPerformanceFeePoints.tblFundPerformanceFeePointsDataTable
      Dim SelectedWatermarks() As DSFundPerformanceFeePoints.tblFundPerformanceFeePointsRow

      Dim FundID As Integer
      Dim FeeDate As Date
      Dim StartDate As Date
      Dim FundLastYearEnd As Date
      Dim Counter As Integer

      Dim ExFundExpensesAfterDate As Date = Renaissance_BaseDate
      Dim ExManagementFeesAfterDate As Date = Renaissance_BaseDate
      Dim ExPerformanceFeesAfterDate As Date = Renaissance_BaseDate
      Dim ExCrystalisedPerformanceFeesAfterDate As Date = Renaissance_BaseDate

      Try
        ' OK, First get Fund Details

        FundRow = CType(LookupTableRow(Mainform, RenaissanceStandardDatasets.tblFund, _FundID), DSFund.tblFundRow)

        If (FundRow Is Nothing) Then

          RVal = New FundValuationDetails("No Fund record for FundID = " & _FundID.ToString)
          Return False

        End If

        FundID = _FundID
        FundPricingPeriod = CType(FundRow.FundPricingPeriod, RenaissanceGlobals.DealingPeriod)

        ' 

        ' Fitting the date to 'FundPricingPeriod' just causes problems, so don't do it...
        ' FeeDate = FitDateToPeriod(FundPricingPeriod, _FeeDate, True)
        FeeDate = _FeeDate

        ' Get StartDate

        StartDate = Renaissance_BaseDate

        If (_OverrideFeeStartDate = False) Then

          ' Calculate since last milestone, or 1 period if no milestone.

          DSMilestones = Mainform.Load_Table(RenaissanceStandardDatasets.tblFundMilestones, False)

          If (DSMilestones IsNot Nothing) Then
            tblMilestones = DSMilestones.tblFundMilestones

            SelectedMilestones = tblMilestones.Select("(MilestoneFundID=" & FundID.ToString & ") AND (MilestoneDate < '" & _FeeDate.ToString(QUERY_SHORTDATEFORMAT) & "')", "MilestoneDate Desc")

            If SelectedMilestones.Length > 0 Then
              StartDate = SelectedMilestones(0).MilestoneDate
            End If

          End If

          ' Milestone not exist, apparently

          If (StartDate = Renaissance_BaseDate) Then

            StartDate = FitDateToPeriod(FundPricingPeriod, AddPeriodToDate(FundPricingPeriod, FeeDate, -1), True)

          End If

        Else
          StartDate = _FeeStartDate
        End If

        ' Fund Year End
        ' Consider long first year, using `FundInvestmentStartDate` to guage.
        ' ASSUME_LONG_FIRST_YEAR_IF_APPROPRIATE

        Try
          Dim FundYearEnd As Date
          Dim FundYearStart As Date

          If FundRow.IsFundYearEndNull Then
            'FundLastYearEnd = New Date(Now.Year - 1, 12, 31) ' Default to calendar year
            FundYearEnd = New Date(Now.Year, 12, 31) ' Default to calendar year
          Else
            'FundLastYearEnd = FitDateToPeriod(DealingPeriod.Monthly, New Date(FeeDate.Year, FundRow.FundYearEnd.Month, 28), True)

            FundYearEnd = FitDateToPeriod(DealingPeriod.Monthly, New Date(FeeDate.Year, FundRow.FundYearEnd.Month, 28), True)

            If (FundYearEnd < FeeDate) Then
              FundYearEnd = FundYearEnd.AddYears(1)
            End If
          End If

          'If (FundLastYearEnd > FeeDate) Then
          '  FundLastYearEnd = FundLastYearEnd.AddYears(-1)
          'End If
          FundYearStart = FundYearEnd.AddYears(-1)

          If (ASSUME_LONG_FIRST_YEAR_IF_APPROPRIATE) Then
            ' Check to see that the fund has been running for at least a year before this year end.
            If FitDateToPeriod(DealingPeriod.Monthly, FundRow.FundInvestmentStartDate, False) > FitDateToPeriod(DealingPeriod.Monthly, AddPeriodToDate(DealingPeriod.Monthly, FundYearEnd, -23), False) Then
              FundYearStart = MaxDate(FundYearStart.AddYears(-1), FitDateToPeriod(DealingPeriod.Monthly, AddPeriodToDate(DealingPeriod.Monthly, FundRow.FundInvestmentStartDate, -1)))
            End If

            'If FitDateToPeriod(DealingPeriod.Monthly, FundRow.FundInvestmentStartDate, False) >= FitDateToPeriod(DealingPeriod.Monthly, AddPeriodToDate(DealingPeriod.Monthly, AddPeriodToDate(DealingPeriod.Annually, FundLastYearEnd, -1), 1), True) Then
            '  FundLastYearEnd = FundLastYearEnd.AddYears(-1)
            'End If
          End If

          FundLastYearEnd = FundYearStart

        Catch ex As Exception
        End Try

        ' Get FundID Valuation

        ' Get Subscriptions And Redemption Information
        Dim SubscriptionStartDate As Date = AddPeriodToDate(DealingPeriod.Monthly, FundLastYearEnd, -2)

        SubscriptionsTable = New DataTable

        Try

          FundValuations = New DataTable

          ' Best guess for Ex Dates for various valuation variants

          If (FundRow.FundUsesEqualisation = 0) Then
            ' Current Primonial Approach.

            ExFundExpensesAfterDate = StartDate
            ExManagementFeesAfterDate = StartDate
            ExPerformanceFeesAfterDate = StartDate
            ExCrystalisedPerformanceFeesAfterDate = StartDate
          Else
            ' Fund Uses Equalisation, Better guess....
            ' Exclude since last watermark
            ' Default will remain 'Renaissance_BaseDate' if not updated here

            DSWatermarks = Mainform.Load_Table(RenaissanceStandardDatasets.tblFundPerformanceFeePoints, False)

            If (DSWatermarks IsNot Nothing) Then
              tblWatermarks = DSWatermarks.tblFundPerformanceFeePoints

              SelectedWatermarks = tblWatermarks.Select("(FeeFund=" & FundID.ToString & ") AND (FeeDate < '" & _FeeDate.ToString(QUERY_SHORTDATEFORMAT) & "')", "FeeDate Desc")

              If SelectedWatermarks.Length > 0 Then
                ExFundExpensesAfterDate = SelectedWatermarks(0).FeeDate
                ExManagementFeesAfterDate = SelectedWatermarks(0).FeeDate
                ExPerformanceFeesAfterDate = SelectedWatermarks(0).FeeDate
                ExCrystalisedPerformanceFeesAfterDate = SelectedWatermarks(0).FeeDate
              End If

            End If

          End If

          If (_OverrideValuationExpensesStartDate) Then ExFundExpensesAfterDate = _ValuationExpensesStartDate
          If (_OverrideValuationManagementFeeStartDate) Then ExManagementFeesAfterDate = _ValuationManagementFeeStartDate
          If (_OverrideValuationPerformanceFeeStartDate) Then ExPerformanceFeesAfterDate = _ValuationPerformanceFeeStartDate
          If (_OverrideValuationCrystalisedPerformanceFeeStartDate) Then ExCrystalisedPerformanceFeesAfterDate = _ValuationCrystalisedPerformanceFeeStartDate


          If (False) OrElse (_linkedReconciliationform Is Nothing) Then

            thisConnection = Mainform.GetVeniceConnection()

            If (thisConnection Is Nothing) Then
              RVal = New FundValuationDetails("Failed to get Venice Database connection.")
              Return False  ' Exit
            End If

            Mainform.MainAdaptorHandler.Set_AdaptorCommands(thisConnection, TempAdaptor, "spu_Valuation_All")

            Dim s_FundID As String = FundID.ToString
            Dim s_FeeDate As String = FeeDate.ToString(QUERY_SHORTDATEFORMAT)
            Dim s_FundLastYearEnd As String = FundLastYearEnd.ToString(QUERY_SHORTDATEFORMAT)
            Dim s_StatusGroupFilter As String = _StatusGroupFilter.ToString
            Dim s_AdministratorDatesFilter As String = _AdministratorDatesFilter.ToString
            Dim s_StartDate As String = StartDate.ToString(QUERY_SHORTDATEFORMAT)
            Dim s_ExFundExpensesAfterDate As String = ExFundExpensesAfterDate.ToString(QUERY_SHORTDATEFORMAT)
            Dim s_ExManagementFeesAfterDate As String = ExManagementFeesAfterDate.ToString(QUERY_SHORTDATEFORMAT)
            Dim s_ExPerformanceFeesAfterDate As String = ExPerformanceFeesAfterDate.ToString(QUERY_SHORTDATEFORMAT)
            Dim s_ExCrystalisedPerformanceFeesAfterDate As String = ExCrystalisedPerformanceFeesAfterDate.ToString(QUERY_SHORTDATEFORMAT)
            Dim s_UseAdministratorPriceDates As String = _UseAdministratorPriceDates.ToString

            If (TempAdaptor.SelectCommand.Parameters.Contains("@FundID")) Then TempAdaptor.SelectCommand.Parameters("@FundID").Value = FundID
            If (TempAdaptor.SelectCommand.Parameters.Contains("@ValueDate")) Then TempAdaptor.SelectCommand.Parameters("@ValueDate").Value = FeeDate
            If (TempAdaptor.SelectCommand.Parameters.Contains("@PreviousYearEndDate")) Then TempAdaptor.SelectCommand.Parameters("@PreviousYearEndDate").Value = FundLastYearEnd
            If (TempAdaptor.SelectCommand.Parameters.Contains("@StatusGroupFilter")) Then TempAdaptor.SelectCommand.Parameters("@StatusGroupFilter").Value = _StatusGroupFilter
            If (TempAdaptor.SelectCommand.Parameters.Contains("@AdministratorDatesFilter")) Then TempAdaptor.SelectCommand.Parameters("@AdministratorDatesFilter").Value = _AdministratorDatesFilter
            If (TempAdaptor.SelectCommand.Parameters.Contains("@ExSubscriptionsAfterDate")) Then TempAdaptor.SelectCommand.Parameters("@ExSubscriptionsAfterDate").Value = StartDate
            If (TempAdaptor.SelectCommand.Parameters.Contains("@ExFundExpensesAfterDate")) Then TempAdaptor.SelectCommand.Parameters("@ExFundExpensesAfterDate").Value = ExFundExpensesAfterDate
            If (TempAdaptor.SelectCommand.Parameters.Contains("@ExManagementFeesAfterDate")) Then TempAdaptor.SelectCommand.Parameters("@ExManagementFeesAfterDate").Value = ExManagementFeesAfterDate
            If (TempAdaptor.SelectCommand.Parameters.Contains("@ExPerformanceFeesAfterDate")) Then TempAdaptor.SelectCommand.Parameters("@ExPerformanceFeesAfterDate").Value = ExPerformanceFeesAfterDate
            If (TempAdaptor.SelectCommand.Parameters.Contains("@ExCrystalisedPerformanceFeesAfterDate")) Then TempAdaptor.SelectCommand.Parameters("@ExCrystalisedPerformanceFeesAfterDate").Value = ExCrystalisedPerformanceFeesAfterDate
            If (TempAdaptor.SelectCommand.Parameters.Contains("@UseAdministratorPriceDates")) Then TempAdaptor.SelectCommand.Parameters("@UseAdministratorPriceDates").Value = _UseAdministratorPriceDates


            If (TempAdaptor.SelectCommand.Parameters.Contains("@KnowledgeDate")) Then
              TempAdaptor.SelectCommand.Parameters("@KnowledgeDate").Value = Mainform.Main_Knowledgedate
            End If

            ' Use Non-UI Blocking 'GetReader' function.
            Dim thisGetReader As New VeniceMain.GetDataReaderClass
            thisGetReader.MyCommand = TempAdaptor.SelectCommand

            SyncLock thisConnection
              Mainform.RunDataReader(thisGetReader)

              'FundValuations.Load(TempAdaptor.SelectCommand.ExecuteReader)
              FundValuations.Load(thisGetReader.myReader)
            End SyncLock

            '@FundID int,
            '@ValueDate datetime,
            '@PreviousYearEndDate datetime,
            '@StatusGroupFilter nvarchar(50),
            '@AdministratorDatesFilter int, 
            '@KnowledgeDate datetime

            ThisCommand = New SqlCommand("spu_GetSubscriptions", thisConnection)
            ThisCommand.Parameters.Add("@FundID", SqlDbType.Int).Value = FundID
            ThisCommand.Parameters.Add("@PreviousYearEndDate", SqlDbType.Date).Value = SubscriptionStartDate ' Hard code 2 Months, Possible source of problems ? Need to capture S/R before year end to get S/R applicable to first period of the new year.
            ThisCommand.Parameters.Add("@ValueDate", SqlDbType.Date).Value = FeeDate
            ThisCommand.Parameters.Add("@StatusGroupFilter", SqlDbType.NVarChar, 50).Value = _StatusGroupFilter
            ThisCommand.Parameters.Add("@AdministratorDatesFilter", SqlDbType.Int).Value = _AdministratorDatesFilter
            ThisCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime).Value = Mainform.Main_Knowledgedate
            ThisCommand.CommandType = CommandType.StoredProcedure

            thisGetReader.MyCommand = ThisCommand
            thisGetReader.myReader = Nothing

            SyncLock thisConnection
              Mainform.RunDataReader(thisGetReader)

              SubscriptionsTable.Load(thisGetReader.myReader)
            End SyncLock
            thisGetReader.MyCommand = Nothing
            thisGetReader.myReader = Nothing

          Else
            ' Non DB Version.

            Dim s_FundID As String = FundID.ToString
            Dim s_FeeDate As String = FeeDate.ToString(QUERY_SHORTDATEFORMAT)
            Dim s_FundLastYearEnd As String = FundLastYearEnd.ToString(QUERY_SHORTDATEFORMAT)
            Dim s_StatusGroupFilter As String = _StatusGroupFilter.ToString
            Dim s_AdministratorDatesFilter As String = _AdministratorDatesFilter.ToString
            Dim s_StartDate As String = StartDate.ToString(QUERY_SHORTDATEFORMAT)
            Dim s_ExFundExpensesAfterDate As String = ExFundExpensesAfterDate.ToString(QUERY_SHORTDATEFORMAT)
            Dim s_ExManagementFeesAfterDate As String = ExManagementFeesAfterDate.ToString(QUERY_SHORTDATEFORMAT)
            Dim s_ExPerformanceFeesAfterDate As String = ExPerformanceFeesAfterDate.ToString(QUERY_SHORTDATEFORMAT)
            Dim s_ExCrystalisedPerformanceFeesAfterDate As String = ExCrystalisedPerformanceFeesAfterDate.ToString(QUERY_SHORTDATEFORMAT)
            Dim s_UseAdministratorPriceDates As String = _UseAdministratorPriceDates.ToString

            Call CalculateFundValuationAll(FundValuations, SubscriptionsTable, FundID, FeeDate, FundLastYearEnd, _StatusGroupFilter, _AdministratorDatesFilter, StartDate, ExFundExpensesAfterDate, ExManagementFeesAfterDate, ExPerformanceFeesAfterDate, ExCrystalisedPerformanceFeesAfterDate, _UseAdministratorPriceDates, REFERENCE_REPORT_CURRENCY)

          End If

          If (SubscriptionsTable.Rows.Count > 0) Then
            Dim DateIndex As Integer = SubscriptionsTable.Columns.IndexOf("TransactionValueDate")
            Dim UnitsSubscribedIndex As Integer = SubscriptionsTable.Columns.IndexOf("UnitsSubscribed")
            Dim TransactionInstrumentIndex As Integer = SubscriptionsTable.Columns.IndexOf("TransactionInstrument")
            Dim UnitsRedeemedIndex As Integer = SubscriptionsTable.Columns.IndexOf("UnitsRedeemed")
            Dim ValueSubscribedIndex As Integer = SubscriptionsTable.Columns.IndexOf("ValueSubscribed")
            Dim ValueRedeemedIndex As Integer = SubscriptionsTable.Columns.IndexOf("ValueRedeemed")
            Dim TradeCountIndex As Integer = SubscriptionsTable.Columns.IndexOf("TradeCount")
            Dim ThisDateIndex As Integer
            Dim ThisInstrument As Integer
            Dim ThisDate As Date
            Dim ThisSRRow As DataRow
            Dim ExistingInformationClass As SR_InformationClass
            Dim InitCounter As Integer

            Dim SubscriptionsArray() As SR_InformationClass = Nothing

            ' Code modified to allow multiple entries per day, for example if the source table is not grouped and ordered by ValueDate.


            For Counter = 0 To (SubscriptionsTable.Rows.Count - 1)
              ThisSRRow = SubscriptionsTable.Rows(Counter)
              ThisDate = CDate(ThisSRRow(DateIndex))
              ThisDateIndex = GetPriceIndex(FundPricingPeriod, SubscriptionStartDate, ThisDate)
              ThisInstrument = CInt(ThisSRRow(TransactionInstrumentIndex))

              If Not _SubscriptionsArrays.TryGetValue(ThisInstrument, SubscriptionsArray) Then

                ' Initialise _SubscriptionsObject array with zero values
                SubscriptionsArray = Array.CreateInstance(GetType(SR_InformationClass), GetPeriodCount(FundPricingPeriod, SubscriptionStartDate, FeeDate) + 5)
                For InitCounter = 0 To (SubscriptionsArray.Length - 1)
                  SubscriptionsArray(InitCounter) = New SR_InformationClass(ThisInstrument, AddPeriodToDate(FundPricingPeriod, SubscriptionStartDate, InitCounter), 0.0#, 0.0#, 0.0#, 0.0#, 0)
                Next

                Try
                  _SubscriptionsArrays.Add(ThisInstrument, SubscriptionsArray)
                Catch ex As Exception
                End Try
              End If

              If (ThisDateIndex < SubscriptionsArray.Length) AndAlso (ThisDate < FeeDate) Then
                ExistingInformationClass = SubscriptionsArray(ThisDateIndex)

                If (ExistingInformationClass.TradeCount = 0) Then
                  ExistingInformationClass.InstrumentID = CInt(ThisSRRow(TransactionInstrumentIndex))
                  ExistingInformationClass.TradeDate = ThisDate
                End If

                ' Should be OK to ignore Subscriptions after the Fee Date
                '_SubscriptionsObject(ThisDateIndex) = New SR_InformationClass(CInt(ThisSRRow(TransactionInstrumentIndex)), ThisDate, CDbl(ThisSRRow(UnitsSubscribedIndex)) + ExistingInformationClass.UnitsSubscribed, CDbl(ThisSRRow(UnitsRedeemedIndex)) + ExistingInformationClass.UnitsRedeemed, CDbl(ThisSRRow(ValueSubscribedIndex)) + ExistingInformationClass.ValueSubscribed, CDbl(ThisSRRow(ValueRedeemedIndex)) + ExistingInformationClass.ValueRedeemed, CInt(ThisSRRow(TradeCountIndex)) + ExistingInformationClass.TradeCount)

                ExistingInformationClass.UnitsSubscribed += CDbl(ThisSRRow(UnitsSubscribedIndex))
                ExistingInformationClass.UnitsRedeemed += CDbl(ThisSRRow(UnitsRedeemedIndex))
                ExistingInformationClass.ValueSubscribed += CDbl(ThisSRRow(ValueSubscribedIndex))
                ExistingInformationClass.ValueRedeemed += CDbl(ThisSRRow(ValueRedeemedIndex))
                ExistingInformationClass.TradeCount += CInt(ThisSRRow(TradeCountIndex))

              End If
            Next
          End If

        Catch ex As Exception
          Mainform.LogError("FundFeeCalculator, GetFundValuation()", LOG_LEVELS.Error, ex.Message, "Error getting Fund Valuations.", ex.StackTrace)
          RVal = New FundValuationDetails("Error getting Fund Valuations.")
          Return False
        Finally
          If (TempAdaptor IsNot Nothing) Then
            Try
              If (TempAdaptor.SelectCommand IsNot Nothing) Then TempAdaptor.SelectCommand.Connection = Nothing
              If (TempAdaptor.UpdateCommand IsNot Nothing) Then TempAdaptor.UpdateCommand.Connection = Nothing
              If (TempAdaptor.InsertCommand IsNot Nothing) Then TempAdaptor.InsertCommand.Connection = Nothing
              If (TempAdaptor.DeleteCommand IsNot Nothing) Then TempAdaptor.DeleteCommand.Connection = Nothing
              TempAdaptor.SelectCommand = Nothing
              TempAdaptor.UpdateCommand = Nothing
              TempAdaptor.InsertCommand = Nothing
              TempAdaptor.DeleteCommand = Nothing
            Catch ex As Exception
            End Try

            TempAdaptor = Nothing
          End If

          If (ThisCommand IsNot Nothing) Then
            ThisCommand.Connection = Nothing
            ThisCommand.Parameters.Clear()
            ThisCommand = Nothing
          End If

        End Try

        ' OK Get Valuation Details

        If (FundValuations.Rows.Count = 0) Then
          RVal = New FundValuationDetails("No Valuations were returned from fn_Valuation_All for FundID = " & FundID.ToString)
          Return False
        End If

        Try
          Dim ValuationRow As DataRow = FundValuations.Rows(0)

          Dim RVal_FundID As Integer = 0
          Dim RVal_ValueDate As Date = Renaissance_BaseDate
          Dim RVal_StatusGroupFilter As String = ""
          Dim RVal_AdministratorDatesFilter As Integer = 0
          Dim RVal_ExSubscriptionsAfterDate As Date = Renaissance_BaseDate
          Dim RVal_ExFundExpensesAfterDate As Date = Renaissance_BaseDate
          Dim RVal_ExManagementFeesAfterDate As Date = Renaissance_BaseDate
          Dim RVal_ExPerformanceFeesAfterDate As Date = Renaissance_BaseDate
          Dim RVal_UseAdministratorPriceDates As Integer = 0
          Dim RVal_KnowledgeDate As Date = Renaissance_BaseDate
          Dim RVal_FundNAV As Double = 0.0#
          Dim RVal_FundGAV As Double = 0.0#
          Dim RVal_FundBNAV As Double = 0.0#
          Dim RVal_FundGNAV As Double = 0.0#
          Dim RVal_FundNAV_Reference As Double = 0.0#
          Dim RVal_FundGAV_Reference As Double = 0.0#
          Dim RVal_FundBNAV_Reference As Double = 0.0#
          Dim RVal_FundGNAV_Reference As Double = 0.0#
          Dim RVal_AllExpenses As Double = 0.0#
          Dim RVal_PreviousExpenses As Double = 0.0#
          Dim RVal_PendingExpenses As Double = 0.0#
          Dim RVal_AllManagementFees As Double = 0.0#
          Dim RVal_PreviousManagementFees As Double = 0.0#
          Dim RVal_PendingManagementFees As Double = 0.0#
          Dim RVal_AllPerformanceFees As Double = 0.0#
          Dim RVal_PreviousPerformanceFees As Double = 0.0#
          Dim RVal_PendingPerformanceFees As Double = 0.0#
          Dim RVal_AllPerformanceFeesCrystalised As Double = 0.0#
          Dim RVal_PreviousPerformanceFeesCrystalised As Double = 0.0#
          Dim RVal_PendingPerformanceFeesCrystalised As Double = 0.0#
          Dim RVal_AllExpenses_Reference As Double = 0.0#
          Dim RVal_PreviousExpenses_Reference As Double = 0.0#
          Dim RVal_PendingExpenses_Reference As Double = 0.0#
          Dim RVal_AllManagementFees_Reference As Double = 0.0#
          Dim RVal_PreviousManagementFees_Reference As Double = 0.0#
          Dim RVal_PendingManagementFees_Reference As Double = 0.0#
          Dim RVal_AllPerformanceFees_Reference As Double = 0.0#
          Dim RVal_PreviousPerformanceFees_Reference As Double = 0.0#
          Dim RVal_PendingPerformanceFees_Reference As Double = 0.0#
          Dim RVal_AllPerformanceFeesCrystalised_Reference As Double = 0.0#
          Dim RVal_PreviousPerformanceFeesCrystalised_Reference As Double = 0.0#
          Dim RVal_PendingPerformanceFeesCrystalised_Reference As Double = 0.0#
          Dim RVal_FundUnits As Double = 0.0#
          Dim RVal_FundUnitsSubscribed As Double = 0.0#
          Dim RVal_FundUnitsRedeemed As Double = 0.0#
          Dim RVal_FundValueSubscribed As Double = 0.0#
          Dim RVal_FundValueRedeemed As Double = 0.0#
          Dim RVal_FundUsedAdministratorDates As Integer = 0
          Dim RVal_FundValueSubscribedThisYear As Double = 0.0#
          Dim RVal_FundValueRedeemedThisYear As Double = 0.0#

          If FundValuations.Columns.Contains("FundID") Then RVal_FundID = CInt(ValuationRow("FundID"))
          If FundValuations.Columns.Contains("ValueDate") Then RVal_ValueDate = CDate(ValuationRow("ValueDate"))
          If FundValuations.Columns.Contains("StatusGroupFilter") Then RVal_StatusGroupFilter = CStr(ValuationRow("StatusGroupFilter"))
          If FundValuations.Columns.Contains("AdministratorDatesFilter") Then RVal_AdministratorDatesFilter = CInt(ValuationRow("AdministratorDatesFilter"))
          If FundValuations.Columns.Contains("ExSubscriptionsAfterDate") Then RVal_ExSubscriptionsAfterDate = CDate(ValuationRow("ExSubscriptionsAfterDate"))
          If FundValuations.Columns.Contains("ExFundExpensesAfterDate") Then RVal_ExFundExpensesAfterDate = CDate(ValuationRow("ExFundExpensesAfterDate"))
          If FundValuations.Columns.Contains("ExManagementFeesAfterDate") Then RVal_ExManagementFeesAfterDate = CDate(ValuationRow("ExManagementFeesAfterDate"))
          If FundValuations.Columns.Contains("ExPerformanceFeesAfterDate") Then RVal_ExPerformanceFeesAfterDate = CDate(ValuationRow("ExPerformanceFeesAfterDate"))
          If FundValuations.Columns.Contains("UseAdministratorPriceDates") Then RVal_UseAdministratorPriceDates = CInt(ValuationRow("UseAdministratorPriceDates"))
          If FundValuations.Columns.Contains("KnowledgeDate") Then RVal_KnowledgeDate = CDate(ValuationRow("KnowledgeDate"))
          If FundValuations.Columns.Contains("FundNAV") Then RVal_FundNAV = CDbl(Nz(ValuationRow("FundNAV"), 0))
          If FundValuations.Columns.Contains("FundGAV") Then RVal_FundGAV = CDbl(Nz(ValuationRow("FundGAV"), 0))
          If FundValuations.Columns.Contains("FundBNAV") Then RVal_FundBNAV = CDbl(Nz(ValuationRow("FundBNAV"), 0))
          If FundValuations.Columns.Contains("FundGNAV") Then RVal_FundGNAV = CDbl(Nz(ValuationRow("FundGNAV"), 0))
          If FundValuations.Columns.Contains("FundNAV_Reference") Then RVal_FundNAV_Reference = CDbl(Nz(ValuationRow("FundNAV_Reference"), 0))
          If FundValuations.Columns.Contains("FundGAV_Reference") Then RVal_FundGAV_Reference = CDbl(Nz(ValuationRow("FundGAV_Reference"), 0))
          If FundValuations.Columns.Contains("FundBNAV_Reference") Then RVal_FundBNAV_Reference = CDbl(Nz(ValuationRow("FundBNAV_Reference"), 0))
          If FundValuations.Columns.Contains("FundGNAV_Reference") Then RVal_FundGNAV_Reference = CDbl(Nz(ValuationRow("FundGNAV_Reference"), 0))
          If FundValuations.Columns.Contains("AllExpenses") Then RVal_AllExpenses = CDbl(Nz(ValuationRow("AllExpenses"), 0))
          If FundValuations.Columns.Contains("PreviousExpenses") Then RVal_PreviousExpenses = CDbl(Nz(ValuationRow("PreviousExpenses"), 0))
          If FundValuations.Columns.Contains("PendingExpenses") Then RVal_PendingExpenses = CDbl(Nz(ValuationRow("PendingExpenses"), 0))
          If FundValuations.Columns.Contains("AllManagementFees") Then RVal_AllManagementFees = CDbl(Nz(ValuationRow("AllManagementFees"), 0))
          If FundValuations.Columns.Contains("PreviousManagementFees") Then RVal_PreviousManagementFees = CDbl(Nz(ValuationRow("PreviousManagementFees"), 0))
          If FundValuations.Columns.Contains("PendingManagementFees") Then RVal_PendingManagementFees = CDbl(Nz(ValuationRow("PendingManagementFees"), 0))
          If FundValuations.Columns.Contains("AllPerformanceFees") Then RVal_AllPerformanceFees = CDbl(Nz(ValuationRow("AllPerformanceFees"), 0))
          If FundValuations.Columns.Contains("PreviousPerformanceFees") Then RVal_PreviousPerformanceFees = CDbl(Nz(ValuationRow("PreviousPerformanceFees"), 0))
          If FundValuations.Columns.Contains("PendingPerformanceFees") Then RVal_PendingPerformanceFees = CDbl(Nz(ValuationRow("PendingPerformanceFees"), 0))
          If FundValuations.Columns.Contains("AllPerformanceFeesCrystalised") Then RVal_AllPerformanceFeesCrystalised = CDbl(Nz(ValuationRow("AllPerformanceFeesCrystalised"), 0))
          If FundValuations.Columns.Contains("PreviousPerformanceFeesCrystalised") Then RVal_PreviousPerformanceFeesCrystalised = CDbl(Nz(ValuationRow("PreviousPerformanceFeesCrystalised"), 0))
          If FundValuations.Columns.Contains("PendingPerformanceFeesCrystalised") Then RVal_PendingPerformanceFeesCrystalised = CDbl(Nz(ValuationRow("PendingPerformanceFeesCrystalised"), 0))
          If FundValuations.Columns.Contains("AllExpenses_Reference") Then RVal_AllExpenses_Reference = CDbl(Nz(ValuationRow("AllExpenses_Reference"), 0))
          If FundValuations.Columns.Contains("PreviousExpenses_Reference") Then RVal_PreviousExpenses_Reference = CDbl(Nz(ValuationRow("PreviousExpenses_Reference"), 0))
          If FundValuations.Columns.Contains("PendingExpenses_Reference") Then RVal_PendingExpenses_Reference = CDbl(Nz(ValuationRow("PendingExpenses_Reference"), 0))
          If FundValuations.Columns.Contains("AllManagementFees_Reference") Then RVal_AllManagementFees_Reference = CDbl(Nz(ValuationRow("AllManagementFees_Reference"), 0))
          If FundValuations.Columns.Contains("PreviousManagementFees_Reference") Then RVal_PreviousManagementFees_Reference = CDbl(Nz(ValuationRow("PreviousManagementFees_Reference"), 0))
          If FundValuations.Columns.Contains("PendingManagementFees_Reference") Then RVal_PendingManagementFees_Reference = CDbl(Nz(ValuationRow("PendingManagementFees_Reference"), 0))
          If FundValuations.Columns.Contains("AllPerformanceFees_Reference") Then RVal_AllPerformanceFees_Reference = CDbl(Nz(ValuationRow("AllPerformanceFees_Reference"), 0))
          If FundValuations.Columns.Contains("PreviousPerformanceFees_Reference") Then RVal_PreviousPerformanceFees_Reference = CDbl(Nz(ValuationRow("PreviousPerformanceFees_Reference"), 0))
          If FundValuations.Columns.Contains("PendingPerformanceFees_Reference") Then RVal_PendingPerformanceFees_Reference = CDbl(Nz(ValuationRow("PendingPerformanceFees_Reference"), 0))
          If FundValuations.Columns.Contains("AllPerformanceFeesCrystalised_Reference") Then RVal_AllPerformanceFeesCrystalised_Reference = CDbl(Nz(ValuationRow("AllPerformanceFeesCrystalised_Reference"), 0))
          If FundValuations.Columns.Contains("PreviousPerformanceFeesCrystalised_Reference") Then RVal_PreviousPerformanceFeesCrystalised_Reference = CDbl(Nz(ValuationRow("PreviousPerformanceFeesCrystalised_Reference"), 0))
          If FundValuations.Columns.Contains("PendingPerformanceFeesCrystalised_Reference") Then RVal_PendingPerformanceFeesCrystalised_Reference = CDbl(Nz(ValuationRow("PendingPerformanceFeesCrystalised_Reference"), 0))
          If FundValuations.Columns.Contains("FundUnits") Then RVal_FundUnits = CDbl(Nz(ValuationRow("FundUnits"), 0))
          If FundValuations.Columns.Contains("FundUnitsSubscribed") Then RVal_FundUnitsSubscribed = CDbl(Nz(ValuationRow("FundUnitsSubscribed"), 0))
          If FundValuations.Columns.Contains("FundUnitsRedeemed") Then RVal_FundUnitsRedeemed = CDbl(Nz(ValuationRow("FundUnitsRedeemed"), 0))
          If FundValuations.Columns.Contains("FundValueSubscribed") Then RVal_FundValueSubscribed = CDbl(Nz(ValuationRow("FundValueSubscribed"), 0))
          If FundValuations.Columns.Contains("FundValueRedeemed") Then RVal_FundValueRedeemed = CDbl(Nz(ValuationRow("FundValueRedeemed"), 0))
          If FundValuations.Columns.Contains("FundValueSubscribedThisYear") Then RVal_FundValueSubscribedThisYear = CDbl(Nz(ValuationRow("FundValueSubscribedThisYear"), 0))
          If FundValuations.Columns.Contains("FundValueRedeemedThisYear") Then RVal_FundValueRedeemedThisYear = CInt(Nz(ValuationRow("FundValueRedeemedThisYear"), 0))
          If FundValuations.Columns.Contains("FundUsedAdministratorDates") Then RVal_FundUsedAdministratorDates = CInt(Nz(ValuationRow("FundUsedAdministratorDates"), 0))

          RVal = New FundValuationDetails(RVal_FundID, RVal_ValueDate, FundLastYearEnd, RVal_StatusGroupFilter, RVal_AdministratorDatesFilter, _
           RVal_ExSubscriptionsAfterDate, RVal_ExFundExpensesAfterDate, RVal_ExManagementFeesAfterDate, RVal_ExPerformanceFeesAfterDate, _
           RVal_UseAdministratorPriceDates, RVal_KnowledgeDate, RVal_FundNAV, RVal_FundGAV, RVal_FundBNAV, RVal_FundGNAV, RVal_FundNAV_Reference, RVal_FundGAV_Reference, RVal_FundBNAV_Reference, RVal_FundGNAV_Reference, _
           RVal_AllExpenses, RVal_PreviousExpenses, RVal_PendingExpenses, _
           RVal_AllManagementFees, RVal_PreviousManagementFees, RVal_PendingManagementFees, _
           RVal_AllPerformanceFees, RVal_PreviousPerformanceFees, RVal_PendingPerformanceFees, _
           RVal_AllPerformanceFeesCrystalised, RVal_PreviousPerformanceFeesCrystalised, RVal_PendingPerformanceFeesCrystalised, _
           RVal_AllExpenses_Reference, RVal_PreviousExpenses_Reference, RVal_PendingExpenses_Reference, _
           RVal_AllManagementFees_Reference, RVal_PreviousManagementFees_Reference, RVal_PendingManagementFees_Reference, _
           RVal_AllPerformanceFees_Reference, RVal_PreviousPerformanceFees_Reference, RVal_PendingPerformanceFees_Reference, _
           RVal_AllPerformanceFeesCrystalised_Reference, RVal_PreviousPerformanceFeesCrystalised_Reference, RVal_PendingPerformanceFeesCrystalised_Reference, _
           RVal_FundUnits, RVal_FundUnitsSubscribed, RVal_FundUnitsRedeemed, RVal_FundValueSubscribed, RVal_FundValueRedeemed, RVal_FundValueSubscribedThisYear, RVal_FundValueRedeemedThisYear, RVal_FundUsedAdministratorDates)

          RVal.StatusString = ""

          RVal.params_ExSubscriptionsAfterDate = StartDate
          RVal.params_FundLastYearEnd = FundLastYearEnd
          RVal.params_ExFundExpensesAfterDate = ExFundExpensesAfterDate
          RVal.params_ExManagementFeesAfterDate = ExManagementFeesAfterDate
          RVal.params_ExPerformanceFeesAfterDate = ExPerformanceFeesAfterDate
          RVal.params_ExCrystalisedPerformanceFeesAfterDate = ExCrystalisedPerformanceFeesAfterDate
          RVal.params_UseAdministratorPriceDates = CInt(_UseAdministratorPriceDates)

        Catch ex As Exception
          Mainform.LogError("FundFeeCalculator, GetFundValuation()", LOG_LEVELS.Error, ex.Message, "Error processing Fund Valuations.", ex.StackTrace)
          RVal = New FundValuationDetails("Error processing Fund Valuations.")
          Return False
        End Try

      Catch ex As Exception
        Mainform.LogError("FundFeeCalculator, GetFundValuation()", LOG_LEVELS.Error, ex.Message, "Error getting Fund Valuations.", ex.StackTrace)
        RVal = New FundValuationDetails("Error getting Fund Valuations.")
        Return False
      End Try

      _ValuationNeedsUpdate = False
      Return True

    Catch ex As Exception
      Mainform.LogError("FundFeeCalculator, GetFundValuation()", LOG_LEVELS.Error, ex.Message, "Error.", ex.StackTrace)
      Return False
    Finally
      _ValuationObject = RVal

      Try
        If (thisConnection IsNot Nothing) AndAlso (thisConnection.State And ConnectionState.Open) Then
          thisConnection.Close()
        End If
      Catch ex As Exception
      End Try
    End Try


  End Function

  ''' <summary>
  ''' Calculates the fund valuation.
  ''' </summary>
  ''' <param name="ReturnTable">The DataTable to return the Fund Valuation details in, may be given as Null, in which case a new DataTable is created. Also the return value of this function.</param>
  ''' <param name="FundID">The Venice Fund ID.</param>
  ''' <param name="ValueDate">The Value date.</param>
  ''' <param name="StatusGroupFilter">The Statusgroup filter.</param>
  ''' <param name="AdministratorDatesFilter">The administrator dates filter.</param>
  ''' <param name="UseAdministratorPriceDates">if set to <c>true</c> then use administrator price dates.</param>
  ''' <returns>DataTable.</returns>
  ''' 
  Private Function CalculateFundValuationAll(ByVal ReturnTable As DataTable, ByVal SubscriptionsTable As DataTable, ByVal FundID As Integer, ByVal ValueDate As Date, ByVal PreviousYearEndDate As Date, ByVal StatusGroupFilter As String, ByVal AdministratorDatesFilter As Integer, ByVal ExSubscriptionsAfterDate As Date, ByVal ExFundExpensesAfterDate As Date, ByVal ExManagementFeesAfterDate As Date, ByVal ExPerformanceFeesAfterDate As Date, ByVal ExCrystalisedPerformanceFeesAfterDate As Date, ByVal UseAdministratorPriceDates As Boolean, ByVal ReferenceCurrency As Integer) As DataTable
    ' *******************************************************************************
    '
    '
    ' @AdministratorDatesFilter : Refine transaction selection according to the given value.
    '--										This Field is a bitmap, the following bit values modify behaviour to match 
    '--										various selection criteria required for administration and reconcilliation.
    '--										1		- Ignore the transaction status for Subscriptions and Redemptions (Should a
    '--										2		- Ignore the Subscriptions and Redemptions on (or after of course) the given Value Date
    '
    ' The locking in this function is largely un-necessary at present, but I was thinking of executing this on a worker thread which would require the locking that is in place.
    '
    ' *******************************************************************************

    Try
      If ReturnTable Is Nothing Then
        ReturnTable = New DataTable
      End If

      ReturnTable.Clear()

      If SubscriptionsTable Is Nothing Then
        SubscriptionsTable = New DataTable
      End If

      SubscriptionsTable.Clear()

      If FundID = 0 Then
        Return ReturnTable
      End If

      ' Lock 

      If (Not lock_tblFundValuationAll.IsWriteLockHeld) Then
        lock_tblFundValuationAll.EnterWriteLock()
      End If

      '
      If (CDate(Nz(ExFundExpensesAfterDate, Renaissance_BaseDate)) <= Renaissance_BaseDate) Then
        ExFundExpensesAfterDate = ValueDate
      End If
      If (CDate(Nz(ExManagementFeesAfterDate, Renaissance_BaseDate)) <= Renaissance_BaseDate) Then
        ExManagementFeesAfterDate = ValueDate
      End If
      If (CDate(Nz(ExPerformanceFeesAfterDate, Renaissance_BaseDate)) <= Renaissance_BaseDate) Then
        ExPerformanceFeesAfterDate = ValueDate
      End If
      If (CDate(Nz(ExCrystalisedPerformanceFeesAfterDate, Renaissance_BaseDate)) <= Renaissance_BaseDate) Then
        ExCrystalisedPerformanceFeesAfterDate = ValueDate
      End If

      ' Variables.
      Dim TransactionDataset As RenaissanceDataClass.DSTransaction = CType(Mainform.Load_Table(RenaissanceStandardDatasets.tblTransaction), RenaissanceDataClass.DSTransaction)
      Dim TransactionDataView As DataView
      Dim ContraTransactionDataset As RenaissanceDataClass.DSTransactionContra = CType(Mainform.Load_Table(RenaissanceStandardDatasets.tblTransactionContra), RenaissanceDataClass.DSTransactionContra)
      Dim ContraTransactionDataView As DataView
      Dim SelectString As String
      Dim OrderString As String

      Dim thisFundRow As RenaissanceDataClass.DSFund.tblFundRow = Nothing
      Dim thisInstrumentRow As RenaissanceDataClass.DSInstrument.tblInstrumentRow = Nothing

      Dim HasStatusGroupFilter As Boolean = False
      Dim StatusGroupID As Integer
      Dim StatusGroupHash As New HashSet(Of Integer)
      Dim ExcludeTodaysSubscriptions As Boolean = False
      Dim YesterdaysValueDate As Date = ValueDate

      Dim VeniceProvisionsTotals As New Dictionary(Of String, Double)

      Dim NewReturnRow As DataRow = Nothing
      Dim NewSubscriptionRow As DataRow = Nothing

      Dim ReturnTable_FundID As Integer
      Dim ReturnTable_ValueDate As Integer
      Dim ReturnTable_StatusGroupFilter As Integer
      Dim ReturnTable_AdministratorDatesFilter As Integer
      Dim ReturnTable_ExSubscriptionsAfterDate As Integer
      Dim ReturnTable_ExFundExpensesAfterDate As Integer
      Dim ReturnTable_ExManagementFeesAfterDate As Integer
      Dim ReturnTable_ExPerformanceFeesAfterDate As Integer
      Dim ReturnTable_ExCrystalisedPerformanceFeesAfterDate As Integer
      Dim ReturnTable_UseAdministratorPriceDates As Integer
      Dim ReturnTable_KnowledgeDate As Integer
      Dim ReturnTable_FundNAV As Integer
      Dim ReturnTable_FundGAV As Integer
      Dim ReturnTable_FundBNAV As Integer
      Dim ReturnTable_FundGNAV As Integer
      Dim ReturnTable_FundNAV_Reference As Integer
      Dim ReturnTable_FundGAV_Reference As Integer
      Dim ReturnTable_FundBNAV_Reference As Integer
      Dim ReturnTable_FundGNAV_Reference As Integer
      Dim ReturnTable_AllExpenses As Integer
      Dim ReturnTable_PreviousExpenses As Integer
      Dim ReturnTable_PendingExpenses As Integer
      Dim ReturnTable_AllManagementFees As Integer
      Dim ReturnTable_PreviousManagementFees As Integer
      Dim ReturnTable_PendingManagementFees As Integer
      Dim ReturnTable_AllPerformanceFees As Integer
      Dim ReturnTable_PreviousPerformanceFees As Integer
      Dim ReturnTable_PendingPerformanceFees As Integer
      Dim ReturnTable_AllPerformanceFeesCrystalised As Integer
      Dim ReturnTable_PreviousPerformanceFeesCrystalised As Integer
      Dim ReturnTable_PendingPerformanceFeesCrystalised As Integer
      Dim ReturnTable_AllExpenses_Reference As Integer
      Dim ReturnTable_PreviousExpenses_Reference As Integer
      Dim ReturnTable_PendingExpenses_Reference As Integer
      Dim ReturnTable_AllManagementFees_Reference As Integer
      Dim ReturnTable_PreviousManagementFees_Reference As Integer
      Dim ReturnTable_PendingManagementFees_Reference As Integer
      Dim ReturnTable_AllPerformanceFees_Reference As Integer
      Dim ReturnTable_PreviousPerformanceFees_Reference As Integer
      Dim ReturnTable_PendingPerformanceFees_Reference As Integer
      Dim ReturnTable_AllPerformanceFeesCrystalised_Reference As Integer
      Dim ReturnTable_PreviousPerformanceFeesCrystalised_Reference As Integer
      Dim ReturnTable_PendingPerformanceFeesCrystalised_Reference As Integer
      Dim ReturnTable_FundUnits As Integer
      Dim ReturnTable_FundUnitsSubscribed As Integer
      Dim ReturnTable_FundUnitsRedeemed As Integer
      Dim ReturnTable_FundValueSubscribed As Integer
      Dim ReturnTable_FundValueRedeemed As Integer
      Dim ReturnTable_FundValueSubscribedThisYear As Integer
      Dim ReturnTable_FundValueRedeemedThisYear As Integer
      Dim ReturnTable_FundUsedAdministratorDates As Integer

      Dim SubscriptionsTable_TransactionFund As Integer
      Dim SubscriptionsTable_TransactionInstrument As Integer
      Dim SubscriptionsTable_TransactionValueDate As Integer
      Dim SubscriptionsTable_UnitsSubscribed As Integer
      Dim SubscriptionsTable_UnitsRedeemed As Integer
      Dim SubscriptionsTable_Units As Integer
      Dim SubscriptionsTable_ValueSubscribed As Integer
      Dim SubscriptionsTable_ValueRedeemed As Integer
      Dim SubscriptionsTable_TradeCount As Integer

      ' ************************************************************************
      ' Initialise table if necessary.
      ' ************************************************************************

      Try

        If (Not ReturnTable.Columns.Contains("FundID")) Then ReturnTable.Columns.Add("FundID", GetType(Integer))
        If (Not ReturnTable.Columns.Contains("ValueDate")) Then ReturnTable.Columns.Add("ValueDate", GetType(Date))
        If (Not ReturnTable.Columns.Contains("StatusGroupFilter")) Then ReturnTable.Columns.Add("StatusGroupFilter", GetType(String))
        If (Not ReturnTable.Columns.Contains("AdministratorDatesFilter")) Then ReturnTable.Columns.Add("AdministratorDatesFilter", GetType(Integer))
        If (Not ReturnTable.Columns.Contains("ExSubscriptionsAfterDate")) Then ReturnTable.Columns.Add("ExSubscriptionsAfterDate", GetType(Date))
        If (Not ReturnTable.Columns.Contains("ExFundExpensesAfterDate")) Then ReturnTable.Columns.Add("ExFundExpensesAfterDate", GetType(Date))
        If (Not ReturnTable.Columns.Contains("ExManagementFeesAfterDate")) Then ReturnTable.Columns.Add("ExManagementFeesAfterDate", GetType(Date))
        If (Not ReturnTable.Columns.Contains("ExPerformanceFeesAfterDate")) Then ReturnTable.Columns.Add("ExPerformanceFeesAfterDate", GetType(Date))
        If (Not ReturnTable.Columns.Contains("ExCrystalisedPerformanceFeesAfterDate")) Then ReturnTable.Columns.Add("ExCrystalisedPerformanceFeesAfterDate", GetType(Date))
        If (Not ReturnTable.Columns.Contains("UseAdministratorPriceDates")) Then ReturnTable.Columns.Add("UseAdministratorPriceDates", GetType(Integer))
        If (Not ReturnTable.Columns.Contains("KnowledgeDate")) Then ReturnTable.Columns.Add("KnowledgeDate", GetType(Date))
        If (Not ReturnTable.Columns.Contains("FundNAV")) Then ReturnTable.Columns.Add("FundNAV", GetType(Double))
        If (Not ReturnTable.Columns.Contains("FundGAV")) Then ReturnTable.Columns.Add("FundGAV", GetType(Double))
        If (Not ReturnTable.Columns.Contains("FundBNAV")) Then ReturnTable.Columns.Add("FundBNAV", GetType(Double))
        If (Not ReturnTable.Columns.Contains("FundGNAV")) Then ReturnTable.Columns.Add("FundGNAV", GetType(Double))
        If (Not ReturnTable.Columns.Contains("FundNAV_Reference")) Then ReturnTable.Columns.Add("FundNAV_Reference", GetType(Double))
        If (Not ReturnTable.Columns.Contains("FundGAV_Reference")) Then ReturnTable.Columns.Add("FundGAV_Reference", GetType(Double))
        If (Not ReturnTable.Columns.Contains("FundBNAV_Reference")) Then ReturnTable.Columns.Add("FundBNAV_Reference", GetType(Double))
        If (Not ReturnTable.Columns.Contains("FundGNAV_Reference")) Then ReturnTable.Columns.Add("FundGNAV_Reference", GetType(Double))
        If (Not ReturnTable.Columns.Contains("AllExpenses")) Then ReturnTable.Columns.Add("AllExpenses", GetType(Double))
        If (Not ReturnTable.Columns.Contains("PreviousExpenses")) Then ReturnTable.Columns.Add("PreviousExpenses", GetType(Double))
        If (Not ReturnTable.Columns.Contains("PendingExpenses")) Then ReturnTable.Columns.Add("PendingExpenses", GetType(Double))
        If (Not ReturnTable.Columns.Contains("AllManagementFees")) Then ReturnTable.Columns.Add("AllManagementFees", GetType(Double))
        If (Not ReturnTable.Columns.Contains("PreviousManagementFees")) Then ReturnTable.Columns.Add("PreviousManagementFees", GetType(Double))
        If (Not ReturnTable.Columns.Contains("PendingManagementFees")) Then ReturnTable.Columns.Add("PendingManagementFees", GetType(Double))
        If (Not ReturnTable.Columns.Contains("AllPerformanceFees")) Then ReturnTable.Columns.Add("AllPerformanceFees", GetType(Double))
        If (Not ReturnTable.Columns.Contains("PreviousPerformanceFees")) Then ReturnTable.Columns.Add("PreviousPerformanceFees", GetType(Double))
        If (Not ReturnTable.Columns.Contains("PendingPerformanceFees")) Then ReturnTable.Columns.Add("PendingPerformanceFees", GetType(Double))
        If (Not ReturnTable.Columns.Contains("AllPerformanceFeesCrystalised")) Then ReturnTable.Columns.Add("AllPerformanceFeesCrystalised", GetType(Double))
        If (Not ReturnTable.Columns.Contains("PreviousPerformanceFeesCrystalised")) Then ReturnTable.Columns.Add("PreviousPerformanceFeesCrystalised", GetType(Double))
        If (Not ReturnTable.Columns.Contains("PendingPerformanceFeesCrystalised")) Then ReturnTable.Columns.Add("PendingPerformanceFeesCrystalised", GetType(Double))
        If (Not ReturnTable.Columns.Contains("AllExpenses_Reference")) Then ReturnTable.Columns.Add("AllExpenses_Reference", GetType(Double))
        If (Not ReturnTable.Columns.Contains("PreviousExpenses_Reference")) Then ReturnTable.Columns.Add("PreviousExpenses_Reference", GetType(Double))
        If (Not ReturnTable.Columns.Contains("PendingExpenses_Reference")) Then ReturnTable.Columns.Add("PendingExpenses_Reference", GetType(Double))
        If (Not ReturnTable.Columns.Contains("AllManagementFees_Reference")) Then ReturnTable.Columns.Add("AllManagementFees_Reference", GetType(Double))
        If (Not ReturnTable.Columns.Contains("PreviousManagementFees_Reference")) Then ReturnTable.Columns.Add("PreviousManagementFees_Reference", GetType(Double))
        If (Not ReturnTable.Columns.Contains("PendingManagementFees_Reference")) Then ReturnTable.Columns.Add("PendingManagementFees_Reference", GetType(Double))
        If (Not ReturnTable.Columns.Contains("AllPerformanceFees_Reference")) Then ReturnTable.Columns.Add("AllPerformanceFees_Reference", GetType(Double))
        If (Not ReturnTable.Columns.Contains("PreviousPerformanceFees_Reference")) Then ReturnTable.Columns.Add("PreviousPerformanceFees_Reference", GetType(Double))
        If (Not ReturnTable.Columns.Contains("PendingPerformanceFees_Reference")) Then ReturnTable.Columns.Add("PendingPerformanceFees_Reference", GetType(Double))
        If (Not ReturnTable.Columns.Contains("AllPerformanceFeesCrystalised_Reference")) Then ReturnTable.Columns.Add("AllPerformanceFeesCrystalised_Reference", GetType(Double))
        If (Not ReturnTable.Columns.Contains("PreviousPerformanceFeesCrystalised_Reference")) Then ReturnTable.Columns.Add("PreviousPerformanceFeesCrystalised_Reference", GetType(Double))
        If (Not ReturnTable.Columns.Contains("PendingPerformanceFeesCrystalised_Reference")) Then ReturnTable.Columns.Add("PendingPerformanceFeesCrystalised_Reference", GetType(Double))
        If (Not ReturnTable.Columns.Contains("FundUnits")) Then ReturnTable.Columns.Add("FundUnits", GetType(Double))
        If (Not ReturnTable.Columns.Contains("FundUnitsSubscribed")) Then ReturnTable.Columns.Add("FundUnitsSubscribed", GetType(Double))
        If (Not ReturnTable.Columns.Contains("FundUnitsRedeemed")) Then ReturnTable.Columns.Add("FundUnitsRedeemed", GetType(Double))
        If (Not ReturnTable.Columns.Contains("FundValueSubscribed")) Then ReturnTable.Columns.Add("FundValueSubscribed", GetType(Double))
        If (Not ReturnTable.Columns.Contains("FundValueRedeemed")) Then ReturnTable.Columns.Add("FundValueRedeemed", GetType(Double))
        If (Not ReturnTable.Columns.Contains("FundValueSubscribedThisYear")) Then ReturnTable.Columns.Add("FundValueSubscribedThisYear", GetType(Double))
        If (Not ReturnTable.Columns.Contains("FundValueRedeemedThisYear")) Then ReturnTable.Columns.Add("FundValueRedeemedThisYear", GetType(Double))
        If (Not ReturnTable.Columns.Contains("FundUsedAdministratorDates")) Then ReturnTable.Columns.Add("FundUsedAdministratorDates", GetType(Integer))

        ReturnTable_FundID = ReturnTable.Columns("FundID").Ordinal
        ReturnTable_ValueDate = ReturnTable.Columns("ValueDate").Ordinal
        ReturnTable_StatusGroupFilter = ReturnTable.Columns("StatusGroupFilter").Ordinal
        ReturnTable_AdministratorDatesFilter = ReturnTable.Columns("AdministratorDatesFilter").Ordinal
        ReturnTable_ExSubscriptionsAfterDate = ReturnTable.Columns("ExSubscriptionsAfterDate").Ordinal
        ReturnTable_ExFundExpensesAfterDate = ReturnTable.Columns("ExFundExpensesAfterDate").Ordinal
        ReturnTable_ExManagementFeesAfterDate = ReturnTable.Columns("ExManagementFeesAfterDate").Ordinal
        ReturnTable_ExPerformanceFeesAfterDate = ReturnTable.Columns("ExPerformanceFeesAfterDate").Ordinal
        ReturnTable_ExCrystalisedPerformanceFeesAfterDate = ReturnTable.Columns("ExCrystalisedPerformanceFeesAfterDate").Ordinal
        ReturnTable_UseAdministratorPriceDates = ReturnTable.Columns("UseAdministratorPriceDates").Ordinal
        ReturnTable_KnowledgeDate = ReturnTable.Columns("KnowledgeDate").Ordinal
        ReturnTable_FundNAV = ReturnTable.Columns("FundNAV").Ordinal
        ReturnTable_FundGAV = ReturnTable.Columns("FundGAV").Ordinal
        ReturnTable_FundBNAV = ReturnTable.Columns("FundBNAV").Ordinal
        ReturnTable_FundGNAV = ReturnTable.Columns("FundGNAV").Ordinal
        ReturnTable_FundNAV_Reference = ReturnTable.Columns("FundNAV_Reference").Ordinal
        ReturnTable_FundGAV_Reference = ReturnTable.Columns("FundGAV_Reference").Ordinal
        ReturnTable_FundBNAV_Reference = ReturnTable.Columns("FundBNAV_Reference").Ordinal
        ReturnTable_FundGNAV_Reference = ReturnTable.Columns("FundGNAV_Reference").Ordinal
        ReturnTable_AllExpenses = ReturnTable.Columns("AllExpenses").Ordinal
        ReturnTable_PreviousExpenses = ReturnTable.Columns("PreviousExpenses").Ordinal
        ReturnTable_PendingExpenses = ReturnTable.Columns("PendingExpenses").Ordinal
        ReturnTable_AllManagementFees = ReturnTable.Columns("AllManagementFees").Ordinal
        ReturnTable_PreviousManagementFees = ReturnTable.Columns("PreviousManagementFees").Ordinal
        ReturnTable_PendingManagementFees = ReturnTable.Columns("PendingManagementFees").Ordinal
        ReturnTable_AllPerformanceFees = ReturnTable.Columns("AllPerformanceFees").Ordinal
        ReturnTable_PreviousPerformanceFees = ReturnTable.Columns("PreviousPerformanceFees").Ordinal
        ReturnTable_PendingPerformanceFees = ReturnTable.Columns("PendingPerformanceFees").Ordinal
        ReturnTable_AllPerformanceFeesCrystalised = ReturnTable.Columns("AllPerformanceFeesCrystalised").Ordinal
        ReturnTable_PreviousPerformanceFeesCrystalised = ReturnTable.Columns("PreviousPerformanceFeesCrystalised").Ordinal
        ReturnTable_PendingPerformanceFeesCrystalised = ReturnTable.Columns("PendingPerformanceFeesCrystalised").Ordinal
        ReturnTable_AllExpenses_Reference = ReturnTable.Columns("AllExpenses_Reference").Ordinal
        ReturnTable_PreviousExpenses_Reference = ReturnTable.Columns("PreviousExpenses_Reference").Ordinal
        ReturnTable_PendingExpenses_Reference = ReturnTable.Columns("PendingExpenses_Reference").Ordinal
        ReturnTable_AllManagementFees_Reference = ReturnTable.Columns("AllManagementFees_Reference").Ordinal
        ReturnTable_PreviousManagementFees_Reference = ReturnTable.Columns("PreviousManagementFees_Reference").Ordinal
        ReturnTable_PendingManagementFees_Reference = ReturnTable.Columns("PendingManagementFees_Reference").Ordinal
        ReturnTable_AllPerformanceFees_Reference = ReturnTable.Columns("AllPerformanceFees_Reference").Ordinal
        ReturnTable_PreviousPerformanceFees_Reference = ReturnTable.Columns("PreviousPerformanceFees_Reference").Ordinal
        ReturnTable_PendingPerformanceFees_Reference = ReturnTable.Columns("PendingPerformanceFees_Reference").Ordinal
        ReturnTable_AllPerformanceFeesCrystalised_Reference = ReturnTable.Columns("AllPerformanceFeesCrystalised_Reference").Ordinal
        ReturnTable_PreviousPerformanceFeesCrystalised_Reference = ReturnTable.Columns("PreviousPerformanceFeesCrystalised_Reference").Ordinal
        ReturnTable_PendingPerformanceFeesCrystalised_Reference = ReturnTable.Columns("PendingPerformanceFeesCrystalised_Reference").Ordinal
        ReturnTable_FundUnits = ReturnTable.Columns("FundUnits").Ordinal
        ReturnTable_FundUnitsSubscribed = ReturnTable.Columns("FundUnitsSubscribed").Ordinal
        ReturnTable_FundUnitsRedeemed = ReturnTable.Columns("FundUnitsRedeemed").Ordinal
        ReturnTable_FundValueSubscribed = ReturnTable.Columns("FundValueSubscribed").Ordinal
        ReturnTable_FundValueRedeemed = ReturnTable.Columns("FundValueRedeemed").Ordinal
        ReturnTable_FundValueSubscribedThisYear = ReturnTable.Columns("FundValueSubscribedThisYear").Ordinal
        ReturnTable_FundValueRedeemedThisYear = ReturnTable.Columns("FundValueRedeemedThisYear").Ordinal
        ReturnTable_FundUsedAdministratorDates = ReturnTable.Columns("FundUsedAdministratorDates").Ordinal

      Catch ex As Exception
      End Try

      ' ************************************************************************
      ' SubscriptionsTable
      ' ************************************************************************
      Try
        If (Not SubscriptionsTable.Columns.Contains("TransactionFund")) Then SubscriptionsTable.Columns.Add("TransactionFund", GetType(Integer))
        If (Not SubscriptionsTable.Columns.Contains("TransactionInstrument")) Then SubscriptionsTable.Columns.Add("TransactionInstrument", GetType(Integer))
        If (Not SubscriptionsTable.Columns.Contains("TransactionValueDate")) Then SubscriptionsTable.Columns.Add("TransactionValueDate", GetType(Date))
        If (Not SubscriptionsTable.Columns.Contains("UnitsSubscribed")) Then SubscriptionsTable.Columns.Add("UnitsSubscribed", GetType(Double))
        If (Not SubscriptionsTable.Columns.Contains("UnitsRedeemed")) Then SubscriptionsTable.Columns.Add("UnitsRedeemed", GetType(Double))
        If (Not SubscriptionsTable.Columns.Contains("Units")) Then SubscriptionsTable.Columns.Add("Units", GetType(Double))
        If (Not SubscriptionsTable.Columns.Contains("ValueSubscribed")) Then SubscriptionsTable.Columns.Add("ValueSubscribed", GetType(Double))
        If (Not SubscriptionsTable.Columns.Contains("ValueRedeemed")) Then SubscriptionsTable.Columns.Add("ValueRedeemed", GetType(Double))
        If (Not SubscriptionsTable.Columns.Contains("TradeCount")) Then SubscriptionsTable.Columns.Add("TradeCount", GetType(Integer))

        SubscriptionsTable_TransactionFund = SubscriptionsTable.Columns("TransactionFund").Ordinal
        SubscriptionsTable_TransactionInstrument = SubscriptionsTable.Columns("TransactionInstrument").Ordinal
        SubscriptionsTable_TransactionValueDate = SubscriptionsTable.Columns("TransactionValueDate").Ordinal
        SubscriptionsTable_UnitsSubscribed = SubscriptionsTable.Columns("UnitsSubscribed").Ordinal
        SubscriptionsTable_UnitsRedeemed = SubscriptionsTable.Columns("UnitsRedeemed").Ordinal
        SubscriptionsTable_Units = SubscriptionsTable.Columns("Units").Ordinal
        SubscriptionsTable_ValueSubscribed = SubscriptionsTable.Columns("ValueSubscribed").Ordinal
        SubscriptionsTable_ValueRedeemed = SubscriptionsTable.Columns("ValueRedeemed").Ordinal
        SubscriptionsTable_TradeCount = SubscriptionsTable.Columns("TradeCount").Ordinal

      Catch ex As Exception

      End Try

      ' ************************************************************************
      ' 'Parameter' values
      ' ************************************************************************

      thisFundRow = LookupTableRow(Mainform, RenaissanceStandardDatasets.tblFund, FundID)

      ' Select this Fund excluding Subscriptions and redemptions (but includes the cash leg)
      SelectString = "(TransactionFund=" & FundID.ToString("###0") & ")"
      OrderString = "TransactionFund, TransactionInstrument, TransactionValueDate"

      TransactionDataView = New DataView(TransactionDataset.tblTransaction, SelectString, OrderString, DataViewRowState.CurrentRows)
      ContraTransactionDataView = New DataView(ContraTransactionDataset.tblTransactionContra, SelectString, OrderString, DataViewRowState.CurrentRows)

      ' OK, Some Prep work...

      If (AdministratorDatesFilter And RenaissanceGlobals.AdministratorDatesFilter.SubscriptionRedemption_IgnoreTodaysTrades) Then
        ExcludeTodaysSubscriptions = True

        ' YesterdaysValueDate, override or calculate

        If (CDate(Nz(ExSubscriptionsAfterDate, Renaissance_BaseDate)) > Renaissance_BaseDate) Then
          YesterdaysValueDate = ExSubscriptionsAfterDate
        Else
          YesterdaysValueDate = AddPeriodToDate(DealingPeriod.Daily, FitDateToPeriod(CType(thisFundRow.FundPricingPeriod, DealingPeriod), ValueDate, False), -1)
        End If

      End If

      If (StatusGroupFilter.Length > 0) Then
        StatusGroupID = CInt(Nz(LookupTableValue(Mainform, RenaissanceStandardDatasets.tblTradeStatusGroup, 0, "TradeStatusGroupID", "RN", "TradeStatusGroupName='" & StatusGroupFilter & "'"), 0))

        If (StatusGroupID) > 0 Then

          Dim StatusItemsDS As DSTradeStatusGroupItems = CType(Mainform.Load_Table(RenaissanceStandardDatasets.tblTradeStatusGroupItems), DSTradeStatusGroupItems)
          Dim StatusItemsRow As DSTradeStatusGroupItems.tblTradeStatusGroupItemsRow

          HasStatusGroupFilter = True

          If (StatusItemsDS IsNot Nothing) Then

            For Each StatusItemsRow In StatusItemsDS.tblTradeStatusGroupItems.Rows
              If (CInt(StatusItemsRow("GroupID")) = StatusGroupID) Then
                StatusGroupHash.Add(StatusItemsRow.StatusID)
              End If
            Next

          End If

        End If ' (StatusGroupID) > 0

      End If ' StatusGroupFilter.Length > 0

      ' Get Administrator Data

      Dim ThisAdminFundCode As String = CStr(Nz(thisFundRow.FundAdministratorCode, ""))
      Dim AdministratorInventoryTable As DataTable = Nothing
      Dim AdministratorInventoryTableIsHoldings As Boolean = False
      Dim Ordinal_Admin_captureID As Integer
      Dim Ordinal_Admin_ISIN As Integer
      Dim Ordinal_Admin_Currency As Integer
      Dim Ordinal_Admin_Description As Integer
      Dim Ordinal_Admin_InstrumentType As Integer
      Dim Ordinal_Admin_Value As Integer
      Dim Ordinal_Admin_Quantity As Integer
      Dim Ordinal_Admin_FXRate As Integer
      Dim Ordinal_Admin_Price As Integer
      Dim Ordinal_Admin_PriceDate As Integer
      Dim Ordinal_Admin_SubAssetType As Integer
      Dim Ordinal_Admin_MaturityDate As Integer
      Dim Ordinal_Admin_QuoteCurrency As Integer

      Dim Hash_AllExpenses As New HashSet(Of Integer)
      Dim Hash_ManagementFees As New HashSet(Of Integer)
      Dim Hash_PerformanceFees As New HashSet(Of Integer)
      Dim Hash_CrystalisedFees As New HashSet(Of Integer)

      If (ThisAdminFundCode.Length > 0) AndAlso (UseAdministratorPriceDates) Then

        If (Not AdministratorInventoryCache.TryGetValue(FundID, AdministratorInventoryTable)) Then
          Dim SelectCommand As SqlCommand = Nothing

          AdministratorInventoryTable = New DataTable

          Try

            'CREATE PROCEDURE [dbo].[adp_tblRecInventory_SelectKD]
            '   @NeoFundcode nvarchar(50) = '',
            '   @ValueDate date = Null,
            '   @MaxCaptureID int = 0,
            '   @KnowledgeDate datetime = Null

            SelectCommand = New SqlCommand
            SelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT
            SelectCommand.Connection = Mainform.GetVeniceConnection()
            SelectCommand.CommandType = CommandType.StoredProcedure
            SelectCommand.CommandText = "adp_tblRecInventory_SelectKD"

            SelectCommand.Parameters.Add(New SqlParameter("@NeoFundcode", SqlDbType.NVarChar, 50)).Value = ThisAdminFundCode
            SelectCommand.Parameters.Add(New SqlParameter("@ValueDate", SqlDbType.DateTime)).Value = ValueDate
            SelectCommand.Parameters.Add(New SqlParameter("@MaxCaptureID", SqlDbType.Int)).Value = 0
            SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", SqlDbType.DateTime)).Value = Mainform.Main_Knowledgedate

            Mainform.LoadTable_Custom(AdministratorInventoryTable, SelectCommand)

            If (AdministratorInventoryTable.Rows.Count > 0) Then
              If (AdministratorInventoryCache.ContainsKey(FundID)) Then
                AdministratorInventoryCache(FundID) = AdministratorInventoryTable
              Else
                AdministratorInventoryCache.Add(FundID, AdministratorInventoryTable)
              End If
            Else

              If (Not AdministratorHoldingsCache.TryGetValue(FundID, AdministratorInventoryTable)) Then
                AdministratorInventoryTable = New DataTable

                SelectCommand.CommandText = "adp_tblRecHoldingsAsInventory"

                AdministratorInventoryTableIsHoldings = True

                Mainform.LoadTable_Custom(AdministratorInventoryTable, SelectCommand)
                'AdministratorInventoryTable.Load(SelectCommand.ExecuteReader)

                If (AdministratorInventoryTable.Rows.Count > 0) Then
                  If (AdministratorHoldingsCache.ContainsKey(FundID)) Then
                    AdministratorHoldingsCache(FundID) = AdministratorInventoryTable
                  Else
                    AdministratorHoldingsCache.Add(FundID, AdministratorInventoryTable)
                  End If
                End If

              End If

            End If

          Catch ex As Exception
            Mainform.LogError("Fund Fee Calculator", LOG_LEVELS.Error, ex.Message, "Error in CalculateFundValuationAll().", ex.StackTrace, False)
          Finally
            If (SelectCommand IsNot Nothing) Then
              If (SelectCommand.Connection IsNot Nothing) AndAlso (SelectCommand.Connection.State And ConnectionState.Open) Then
                SelectCommand.Connection.Close()
              End If
              SelectCommand.Connection = Nothing
            End If
            SelectCommand = Nothing
          End Try

        End If

        If (AdministratorInventoryTable IsNot Nothing) Then
          Ordinal_Admin_captureID = AdministratorInventoryTable.Columns("captureID").Ordinal
          Ordinal_Admin_ISIN = AdministratorInventoryTable.Columns("neo_isin").Ordinal
          Ordinal_Admin_Currency = AdministratorInventoryTable.Columns("neo_quotecurrency").Ordinal
          Ordinal_Admin_Description = AdministratorInventoryTable.Columns("neo_description").Ordinal
          Ordinal_Admin_InstrumentType = AdministratorInventoryTable.Columns("neo_instrumenttype").Ordinal
          Ordinal_Admin_Value = AdministratorInventoryTable.Columns("neo_marketvalue_rc_value").Ordinal
          Ordinal_Admin_Quantity = AdministratorInventoryTable.Columns("neo_quantity_value").Ordinal
          Ordinal_Admin_FXRate = AdministratorInventoryTable.Columns("neo_exchangerate_value").Ordinal
          Ordinal_Admin_Price = AdministratorInventoryTable.Columns("neo_price_value").Ordinal
          Ordinal_Admin_PriceDate = AdministratorInventoryTable.Columns("neo_pricedate_value").Ordinal
          Ordinal_Admin_SubAssetType = AdministratorInventoryTable.Columns("neo_subassettype").Ordinal
          Ordinal_Admin_MaturityDate = AdministratorInventoryTable.Columns("neo_maturitydate").Ordinal
          Ordinal_Admin_QuoteCurrency = AdministratorInventoryTable.Columns("neo_quotecurrency").Ordinal
        End If

      End If

      ' ************************************************************************
      ' Get Relevant transaction sets.
      ' ************************************************************************

      Dim DataViewCount As Integer
      Dim thisDataView As DataView
      Dim thisRowView As DataRowView
      Dim thisTransactionRow As DataRow = Nothing
      Dim Counter As Integer

      Dim Ordinal_Fund As Integer
      Dim Ordinal_Instrument As Integer
      Dim Ordinal_TransactionType As Integer
      Dim Ordinal_TransactionType_Contra As Integer
      Dim Ordinal_SignedUnits As Integer
      Dim Ordinal_SignedSettlement As Integer
      Dim Ordinal_TradeStatusID As Integer
      Dim Ordinal_ValueDate As Integer
      Dim Ordinal_SettlementDate As Integer
      Dim Ordinal_TransactionParentID As Integer

      Dim thisValueDate As Date
      Dim thisSettlementDate As Date
      Dim thisTransactionParentID As Integer
      Dim thisFundID As Integer
      Dim thisTransactionType As Integer
      Dim thisTransactionType_Contra As Integer
      Dim thisTradeStatusID As Integer
      Dim thisInstrumentID As Integer
      Dim thisSignedUnits As Double
      Dim thisSignedSettlement As Double

      Dim PricesRow As DataRow
      Dim AdministratorRow As DataRow
      Dim thisFXRate As Double
      Dim ReferenceCurrencyFXRate As Double
      Dim thisPrice As Double
      Dim thisPriceDate As Date
      Dim thisISIN As String
      Dim thisCurrency As String
      Dim PositionValue As Double

      Dim FundUnits As Double = 0.0#
      Dim FundUnitsSubscribed As Double = 0.0#
      Dim FundValueSubscribed As Double = 0.0#
      Dim FundValueSubscribedThisYear As Double = 0.0#
      Dim FundUnitsRedeemed As Double = 0.0#
      Dim FundValueRedeemed As Double = 0.0#
      Dim FundValueRedeemedThisYear As Double = 0.0#
      Dim FundNAV As Double = 0.0#
      Dim AllExpenses As Double = 0.0#
      Dim AllManagementFees As Double = 0.0#
      Dim AllPerformanceFees As Double = 0.0#
      Dim AllPerformanceFeesCrystalised As Double = 0.0#
      Dim PreviousExpenses As Double = 0.0#
      Dim PreviousManagementFees As Double = 0.0#
      Dim PreviousPerformanceFees As Double = 0.0#
      Dim PreviousPerformanceFeesCrystalised As Double = 0.0#
      Dim FundNAV_Reference As Double = 0.0#
      Dim AllExpenses_Reference As Double = 0.0#
      Dim AllManagementFees_Reference As Double = 0.0#
      Dim AllPerformanceFees_Reference As Double = 0.0#
      Dim AllPerformanceFeesCrystalised_Reference As Double = 0.0#
      Dim PreviousExpenses_Reference As Double = 0.0#
      Dim PreviousManagementFees_Reference As Double = 0.0#
      Dim PreviousPerformanceFees_Reference As Double = 0.0#
      Dim PreviousPerformanceFeesCrystalised_Reference As Double = 0.0#
      Dim PendingExpenses As Double = 0.0#
      Dim PendingManagementFees As Double = 0.0#
      Dim PendingPerformanceFees As Double = 0.0#
      Dim PendingPerformanceFeesCrystalised As Double = 0.0#
      Dim PendingExpenses_Reference As Double = 0.0#
      Dim PendingManagementFees_Reference As Double = 0.0#
      Dim PendingPerformanceFees_Reference As Double = 0.0#
      Dim PendingPerformanceFeesCrystalised_Reference As Double = 0.0#
      Dim FundGAV As Double = 0.0#
      Dim FundGAV_Reference As Double = 0.0#
      Dim FundBNAV As Double = 0.0#
      Dim FundBNAV_Reference As Double = 0.0#
      Dim FundGNAV As Double = 0.0#
      Dim FundGNAV_Reference As Double = 0.0#
      Dim UsedAdministratorPriceDates As Integer = 0
      Dim SubscriptionValueDate As Date
      Dim SubscriptionStartDate As Date = AddPeriodToDate(DealingPeriod.Monthly, PreviousYearEndDate, -2)

      SyncLock TransactionDataset
        SyncLock ContraTransactionDataset

          For DataViewCount = 0 To 1

            If (DataViewCount = 0) Then
              thisDataView = TransactionDataView ' Important that this comes first.
            Else
              thisDataView = ContraTransactionDataView
            End If

            Ordinal_Fund = thisDataView.Table.Columns("TransactionFund").Ordinal
            Ordinal_Instrument = thisDataView.Table.Columns("TransactionInstrument").Ordinal
            Ordinal_TransactionType = thisDataView.Table.Columns("TransactionType").Ordinal
            Ordinal_TransactionType_Contra = thisDataView.Table.Columns("TransactionType_Contra").Ordinal
            Ordinal_SignedUnits = thisDataView.Table.Columns("TransactionSignedUnits").Ordinal
            Ordinal_SignedSettlement = thisDataView.Table.Columns("TransactionSignedSettlement").Ordinal
            Ordinal_TradeStatusID = thisDataView.Table.Columns("TransactionTradeStatusID").Ordinal
            Ordinal_ValueDate = thisDataView.Table.Columns("TransactionValueDate").Ordinal
            Ordinal_SettlementDate = thisDataView.Table.Columns("TransactionSettlementDate").Ordinal
            Ordinal_TransactionParentID = thisDataView.Table.Columns("TransactionParentID").Ordinal

            ' Loop through the selected data (DataView) building an ArrayList of aggregated Instrument positions.

            For Counter = 0 To (thisDataView.Count - 1) ' 

              thisRowView = thisDataView.Item(Counter)
              thisTransactionRow = thisRowView.Row

              thisTransactionParentID = CInt(thisTransactionRow(Ordinal_TransactionParentID))
              thisFundID = CInt(thisTransactionRow(Ordinal_Fund))
              thisTransactionType = CInt(thisTransactionRow(Ordinal_TransactionType))
              thisTransactionType_Contra = CInt(thisTransactionRow(Ordinal_TransactionType_Contra))
              thisValueDate = CDate(thisTransactionRow(Ordinal_ValueDate)).Date
              thisSettlementDate = CDate(thisTransactionRow(Ordinal_SettlementDate)).Date

              ' Check Fund
              If (thisFundID <> FundID) Then
                Continue For
              End If

              If (thisValueDate > ValueDate) Then
                Continue For
              End If

              ' Exclude todays S / R
              If (ExcludeTodaysSubscriptions) AndAlso ((thisTransactionType_Contra = TransactionTypes.Subscribe) OrElse (thisTransactionType_Contra = TransactionTypes.Redeem)) AndAlso (thisValueDate > YesterdaysValueDate) Then
                Continue For
              End If

              ' Check Status Group (if not a subscription / redemption)
              If (HasStatusGroupFilter) AndAlso (thisTransactionType_Contra <> TransactionTypes.Subscribe) AndAlso (thisTransactionType_Contra <> TransactionTypes.Redeem) Then
                thisTradeStatusID = CInt(thisTransactionRow(Ordinal_TradeStatusID))

                If (thisTradeStatusID > 0) AndAlso (Not StatusGroupHash.Contains(thisTradeStatusID)) Then
                  Continue For
                End If
              End If

              ' ************************************************************************
              ' Get Transaction & Instrument details 
              ' ************************************************************************

              thisInstrumentID = CInt(thisTransactionRow(Ordinal_Instrument))
              thisSignedUnits = CDbl(thisTransactionRow(Ordinal_SignedUnits))
              thisSignedSettlement = CDbl(thisTransactionRow(Ordinal_SignedSettlement))

              If (thisInstrumentRow Is Nothing) OrElse (thisInstrumentRow.InstrumentID <> thisInstrumentID) Then
                thisInstrumentRow = LookupTableRow(Mainform, RenaissanceStandardDatasets.tblInstrument, thisInstrumentID)

                PricesRow = _linkedReconciliationform.GetBestPriceRow(thisInstrumentID)
                If (PricesRow Is Nothing) Then
                  thisPrice = 0.0#
                  thisPriceDate = Renaissance_BaseDate
                Else
                  thisPrice = CDbl(Nz(PricesRow("PriceLevel"), 0.0#))
                  thisPriceDate = CDate(Nz(PricesRow("PriceDate"), Renaissance_BaseDate))
                End If
                thisFXRate = _linkedReconciliationform.GetFXToFund(thisInstrumentRow.InstrumentCurrencyID, thisFundRow.FundBaseCurrency)

                If (UseAdministratorPriceDates) AndAlso (AdministratorInventoryTable IsNot Nothing) AndAlso (thisInstrumentRow.InstrumentType <> InstrumentTypes.Cash) Then
                  For Each AdministratorRow In AdministratorInventoryTable.Rows
                    thisISIN = AdministratorRow(Ordinal_Admin_ISIN).ToString.ToUpper
                    thisCurrency = AdministratorRow(Ordinal_Admin_Currency).ToString.ToUpper

                    If (thisInstrumentRow.InstrumentISIN = thisISIN) AndAlso (thisInstrumentRow.InstrumentCurrency = thisCurrency) Then
                      If (thisPriceDate <> CDate(Nz(AdministratorRow(Ordinal_Admin_PriceDate), Renaissance_BaseDate))) Then
                        thisPrice = CDbl(Nz(AdministratorRow(Ordinal_Admin_Price), thisPrice))
                        thisPriceDate = CDate(Nz(AdministratorRow(Ordinal_Admin_PriceDate), Renaissance_BaseDate))

                        UsedAdministratorPriceDates += 1
                      End If

                      Exit For
                    End If
                  Next

                End If
              End If

              PositionValue = (thisSignedUnits * thisPrice * thisInstrumentRow.InstrumentContractSize * thisInstrumentRow.InstrumentMultiplier) * thisFXRate

              If (thisTransactionType = TransactionTypes.Subscribe) OrElse (thisTransactionType = TransactionTypes.Redeem) Then

                FundUnits += thisSignedUnits

                If (thisTransactionType = TransactionTypes.Subscribe) Then
                  FundUnitsSubscribed += thisSignedUnits
                  FundValueSubscribed += thisSignedSettlement * thisFXRate

                  If (thisValueDate > PreviousYearEndDate) Then
                    FundValueSubscribedThisYear += thisSignedSettlement * thisFXRate
                  End If
                Else
                  FundUnitsRedeemed += thisSignedUnits
                  FundValueRedeemed += thisSignedSettlement * thisFXRate

                  If (thisValueDate > PreviousYearEndDate) Then
                    FundValueRedeemedThisYear += thisSignedSettlement * thisFXRate
                  End If
                End If

                ' Subscriptions Table.

                SubscriptionValueDate = IIf(thisValueDate <= SubscriptionStartDate, SubscriptionStartDate, thisValueDate)

                If (NewSubscriptionRow Is Nothing) OrElse (CDate(NewSubscriptionRow(SubscriptionsTable_TransactionValueDate)) <> SubscriptionValueDate) OrElse (CInt(NewSubscriptionRow(SubscriptionsTable_TransactionInstrument)) <> thisInstrumentID) Then
                  NewSubscriptionRow = SubscriptionsTable.NewRow

                  NewSubscriptionRow(SubscriptionsTable_TransactionFund) = FundID
                  NewSubscriptionRow(SubscriptionsTable_TransactionInstrument) = thisInstrumentID
                  NewSubscriptionRow(SubscriptionsTable_TransactionValueDate) = SubscriptionValueDate
                  NewSubscriptionRow(SubscriptionsTable_Units) = 0.0#
                  NewSubscriptionRow(SubscriptionsTable_UnitsRedeemed) = 0.0#
                  NewSubscriptionRow(SubscriptionsTable_UnitsSubscribed) = 0.0#
                  NewSubscriptionRow(SubscriptionsTable_ValueRedeemed) = 0.0#
                  NewSubscriptionRow(SubscriptionsTable_ValueSubscribed) = 0.0#
                  NewSubscriptionRow(SubscriptionsTable_TradeCount) = 0

                  SubscriptionsTable.Rows.Add(NewSubscriptionRow)
                End If

                If (thisTransactionType = TransactionTypes.Subscribe) Then
                  NewSubscriptionRow(SubscriptionsTable_Units) = CDbl(NewSubscriptionRow(SubscriptionsTable_Units)) + Math.Abs(thisSignedUnits)
                  NewSubscriptionRow(SubscriptionsTable_UnitsSubscribed) = CDbl(NewSubscriptionRow(SubscriptionsTable_UnitsSubscribed)) + Math.Abs(thisSignedUnits)
                  NewSubscriptionRow(SubscriptionsTable_ValueSubscribed) = CDbl(NewSubscriptionRow(SubscriptionsTable_ValueSubscribed)) + Math.Abs(thisSignedSettlement)
                Else
                  NewSubscriptionRow(SubscriptionsTable_Units) = CDbl(NewSubscriptionRow(SubscriptionsTable_Units)) - Math.Abs(thisSignedUnits)
                  NewSubscriptionRow(SubscriptionsTable_UnitsRedeemed) = CDbl(NewSubscriptionRow(SubscriptionsTable_UnitsRedeemed)) + Math.Abs(thisSignedUnits)
                  NewSubscriptionRow(SubscriptionsTable_ValueRedeemed) = CDbl(NewSubscriptionRow(SubscriptionsTable_ValueRedeemed)) + Math.Abs(thisSignedSettlement)
                End If

                NewSubscriptionRow(SubscriptionsTable_TradeCount) = CInt(NewSubscriptionRow(SubscriptionsTable_TradeCount)) + 1

              Else
                ' Not Sub or redeem

                FundNAV += PositionValue

                If (thisValueDate > PreviousYearEndDate) Then
                  If (Hash_AllExpenses.Contains(thisTransactionParentID)) OrElse (thisInstrumentRow.InstrumentExcludeGAV <> 0) OrElse (thisInstrumentRow.InstrumentIsEqualisation <> 0) OrElse (thisInstrumentRow.InstrumentIsManagementFee <> 0) OrElse (thisInstrumentRow.InstrumentIsIncentiveFee <> 0) OrElse (thisInstrumentRow.InstrumentIsCrystalisedIncentive <> 0) Then

                    AllExpenses += PositionValue
                    If (thisValueDate <= ExFundExpensesAfterDate) Then PreviousExpenses += PositionValue

                    If (DataViewCount = 0) Then
                      Hash_AllExpenses.Add(thisTransactionParentID)
                    End If

                    If (Hash_ManagementFees.Contains(thisTransactionParentID)) OrElse (thisInstrumentRow.InstrumentIsManagementFee <> 0) Then
                      AllManagementFees += PositionValue
                      If (thisValueDate <= ExManagementFeesAfterDate) Then PreviousManagementFees += PositionValue

                      If (DataViewCount = 0) Then
                        Hash_ManagementFees.Add(thisTransactionParentID)
                      End If

                    ElseIf (Hash_PerformanceFees.Contains(thisTransactionParentID)) OrElse ((thisInstrumentRow.InstrumentIsIncentiveFee <> 0) AndAlso (thisInstrumentRow.InstrumentIsCrystalisedIncentive = 0)) Then
                      AllPerformanceFees += PositionValue
                      If (thisValueDate <= ExPerformanceFeesAfterDate) Then PreviousPerformanceFees += PositionValue

                      If (DataViewCount = 0) Then
                        Hash_PerformanceFees.Add(thisTransactionParentID)
                      End If

                    ElseIf (Hash_CrystalisedFees.Contains(thisTransactionParentID)) OrElse ((thisInstrumentRow.InstrumentIsIncentiveFee <> 0) AndAlso (thisInstrumentRow.InstrumentIsCrystalisedIncentive <> 0)) Then
                      AllPerformanceFeesCrystalised += PositionValue
                      If (thisValueDate <= ExCrystalisedPerformanceFeesAfterDate) Then PreviousPerformanceFeesCrystalised += PositionValue

                      If (DataViewCount = 0) Then
                        Hash_CrystalisedFees.Add(thisTransactionParentID)
                      End If
                    End If

                  End If

                End If ' (thisValueDate > PreviousYearEndDate)

              End If

            Next ' Counter = 0 To (thisDataView.Count - 1) ' 

          Next ' DataViewCount

        End SyncLock
      End SyncLock

      ' ************

      ReferenceCurrencyFXRate = _linkedReconciliationform.GetFXToFund(thisFundRow.FundBaseCurrency, ReferenceCurrency)

      FundNAV_Reference = FundNAV * ReferenceCurrencyFXRate
      AllExpenses_Reference = AllExpenses * ReferenceCurrencyFXRate
      AllManagementFees_Reference = AllManagementFees * ReferenceCurrencyFXRate
      AllPerformanceFees_Reference = AllPerformanceFees * ReferenceCurrencyFXRate
      AllPerformanceFeesCrystalised_Reference = AllPerformanceFeesCrystalised * ReferenceCurrencyFXRate
      PreviousExpenses_Reference = PreviousExpenses * ReferenceCurrencyFXRate
      PreviousManagementFees_Reference = PreviousManagementFees * ReferenceCurrencyFXRate
      PreviousPerformanceFees_Reference = PreviousPerformanceFees * ReferenceCurrencyFXRate
      PreviousPerformanceFeesCrystalised_Reference = PreviousPerformanceFeesCrystalised * ReferenceCurrencyFXRate

      PendingExpenses = (AllExpenses - PreviousExpenses)
      PendingManagementFees = (AllManagementFees - PreviousManagementFees)
      PendingPerformanceFees = (AllPerformanceFees - PreviousPerformanceFees)
      PendingPerformanceFeesCrystalised = (AllPerformanceFeesCrystalised - PreviousPerformanceFeesCrystalised)
      PendingExpenses_Reference = (AllExpenses_Reference - PreviousExpenses_Reference)
      PendingManagementFees_Reference = (AllManagementFees_Reference - PreviousManagementFees_Reference)
      PendingPerformanceFees_Reference = (AllPerformanceFees_Reference - PreviousPerformanceFees_Reference)
      PendingPerformanceFeesCrystalised_Reference = (AllPerformanceFeesCrystalised_Reference - PreviousPerformanceFeesCrystalised_Reference)

      FundGAV = FundNAV - PendingExpenses ' Mgmt Fees and Perf Fees have been included in 'All Expenses'
      FundGAV_Reference = FundNAV_Reference - PendingExpenses_Reference
      FundBNAV = FundNAV - (PendingManagementFees + PendingPerformanceFees + PendingPerformanceFeesCrystalised)
      FundBNAV_Reference = FundNAV_Reference - (PendingManagementFees_Reference + PendingPerformanceFees_Reference + PendingPerformanceFeesCrystalised_Reference)
      FundGNAV = FundNAV - (PendingPerformanceFees + PendingPerformanceFeesCrystalised)
      FundGNAV_Reference = FundNAV_Reference - (PendingPerformanceFees_Reference + PendingPerformanceFeesCrystalised_Reference)

      ' Set Return Values

      NewReturnRow = ReturnTable.NewRow

      NewReturnRow(ReturnTable_FundID) = FundID
      NewReturnRow(ReturnTable_ValueDate) = ValueDate
      NewReturnRow(ReturnTable_StatusGroupFilter) = StatusGroupFilter
      NewReturnRow(ReturnTable_AdministratorDatesFilter) = AdministratorDatesFilter
      NewReturnRow(ReturnTable_ExSubscriptionsAfterDate) = ExSubscriptionsAfterDate
      NewReturnRow(ReturnTable_ExFundExpensesAfterDate) = ExFundExpensesAfterDate
      NewReturnRow(ReturnTable_ExManagementFeesAfterDate) = ExManagementFeesAfterDate
      NewReturnRow(ReturnTable_ExPerformanceFeesAfterDate) = ExPerformanceFeesAfterDate
      NewReturnRow(ReturnTable_ExCrystalisedPerformanceFeesAfterDate) = ExCrystalisedPerformanceFeesAfterDate
      NewReturnRow(ReturnTable_UseAdministratorPriceDates) = UseAdministratorPriceDates
      NewReturnRow(ReturnTable_KnowledgeDate) = Mainform.Main_Knowledgedate
      NewReturnRow(ReturnTable_FundNAV) = FundNAV
      NewReturnRow(ReturnTable_FundGAV) = FundGAV
      NewReturnRow(ReturnTable_FundBNAV) = FundBNAV
      NewReturnRow(ReturnTable_FundGNAV) = FundGNAV
      NewReturnRow(ReturnTable_FundNAV_Reference) = FundBNAV_Reference
      NewReturnRow(ReturnTable_FundGAV_Reference) = FundGAV_Reference
      NewReturnRow(ReturnTable_FundBNAV_Reference) = FundBNAV_Reference
      NewReturnRow(ReturnTable_FundGNAV_Reference) = FundGNAV_Reference
      NewReturnRow(ReturnTable_AllExpenses) = AllExpenses
      NewReturnRow(ReturnTable_PreviousExpenses) = PreviousExpenses
      NewReturnRow(ReturnTable_PendingExpenses) = PendingExpenses
      NewReturnRow(ReturnTable_AllManagementFees) = AllManagementFees
      NewReturnRow(ReturnTable_PreviousManagementFees) = PreviousManagementFees
      NewReturnRow(ReturnTable_PendingManagementFees) = PendingManagementFees
      NewReturnRow(ReturnTable_AllPerformanceFees) = AllPerformanceFees
      NewReturnRow(ReturnTable_PreviousPerformanceFees) = PreviousPerformanceFees
      NewReturnRow(ReturnTable_PendingPerformanceFees) = PendingPerformanceFees
      NewReturnRow(ReturnTable_AllPerformanceFeesCrystalised) = AllPerformanceFeesCrystalised
      NewReturnRow(ReturnTable_PreviousPerformanceFeesCrystalised) = PreviousPerformanceFeesCrystalised
      NewReturnRow(ReturnTable_PendingPerformanceFeesCrystalised) = PendingPerformanceFeesCrystalised
      NewReturnRow(ReturnTable_AllExpenses_Reference) = AllExpenses_Reference
      NewReturnRow(ReturnTable_PreviousExpenses_Reference) = PreviousExpenses_Reference
      NewReturnRow(ReturnTable_PendingExpenses_Reference) = PendingExpenses_Reference
      NewReturnRow(ReturnTable_AllManagementFees_Reference) = AllManagementFees_Reference
      NewReturnRow(ReturnTable_PreviousManagementFees_Reference) = PreviousManagementFees_Reference
      NewReturnRow(ReturnTable_PendingManagementFees_Reference) = PendingManagementFees_Reference
      NewReturnRow(ReturnTable_AllPerformanceFees_Reference) = AllPerformanceFees_Reference
      NewReturnRow(ReturnTable_PreviousPerformanceFees_Reference) = PreviousPerformanceFees_Reference
      NewReturnRow(ReturnTable_PendingPerformanceFees_Reference) = PendingPerformanceFees_Reference
      NewReturnRow(ReturnTable_AllPerformanceFeesCrystalised_Reference) = AllPerformanceFeesCrystalised_Reference
      NewReturnRow(ReturnTable_PreviousPerformanceFeesCrystalised_Reference) = PreviousPerformanceFeesCrystalised_Reference
      NewReturnRow(ReturnTable_PendingPerformanceFeesCrystalised_Reference) = PendingPerformanceFeesCrystalised_Reference
      NewReturnRow(ReturnTable_FundUnits) = FundUnits
      NewReturnRow(ReturnTable_FundUnitsSubscribed) = FundUnitsSubscribed
      NewReturnRow(ReturnTable_FundUnitsRedeemed) = FundUnitsRedeemed
      NewReturnRow(ReturnTable_FundValueSubscribed) = FundValueSubscribed
      NewReturnRow(ReturnTable_FundValueRedeemed) = FundValueRedeemed
      NewReturnRow(ReturnTable_FundValueSubscribedThisYear) = FundValueSubscribedThisYear
      NewReturnRow(ReturnTable_FundValueRedeemedThisYear) = FundValueRedeemedThisYear
      NewReturnRow(ReturnTable_FundUsedAdministratorDates) = UsedAdministratorPriceDates

      ReturnTable.Rows.Add(NewReturnRow)


    Catch ex As Exception
      If (lock_tblFundValuationAll.IsWriteLockHeld) Then
        lock_tblFundValuationAll.ExitWriteLock()
      End If

      Mainform.LogError("Fund Fee Calculator", LOG_LEVELS.Error, ex.Message, "Error in CalculateFundValuationAll().", ex.StackTrace, True)
    Finally

      If (lock_tblFundValuationAll.IsWriteLockHeld) Then
        lock_tblFundValuationAll.ExitWriteLock()
      End If
    End Try

    Return ReturnTable

  End Function



  ''' <summary>
  ''' Calculates the fees.
  ''' </summary>
  ''' <returns><c>true</c> on success, <c>false</c> otherwise</returns>
  Public Function CalculateFees() As Boolean
    ' ********************************************************************************
    '
    '
    '
    ' Note : 
    '--			(1) GAV		Gross Asset Value Excl all expenses and fees, After (@ExFundExpensesAfterDate, ExManagementFeesAfterDate and @ExPerformanceFeesAfterDate)
    '--			(2) Basic NAV 	Excluding Mgmt & Incentive fees (After @ExManagementFeesAfterDate and @ExPerformanceFeesAfterDate), but including all other expenses
    '--			(3) GNAV	NAV, excluding only Performance (Incentive) fees after ExPerformanceFeesAfterDate
    '--			(4) NAV		NAV, including all expenses etc.
    '
    ' ********************************************************************************

    Dim FundRow As DSFund.tblFundRow

    If (_FeesNeedUpdate = False) Then
      Return False
    End If

    Try

      If (_ValuationNeedsUpdate) Then
        Call GetFundValuation()
      End If

      If (_ValuationNeedsUpdate) Then
        ' Clearly a problem
        Return False
      End If

      ' OK Get Fund Details

      FundRow = CType(LookupTableRow(Mainform, RenaissanceStandardDatasets.tblFund, _FundID), DSFund.tblFundRow)

      If (FundRow Is Nothing) Then

        Return False

      End If

      If (FundRow.FundUsesEqualisation <> 0) Then

        ' Handle Equalisation

        ' More_Code_Here()

      Else

        Return CalculateFees_NoEqualisation(FundRow)

      End If

    Catch ex As Exception

    End Try


  End Function

  Private Sub More_MultiShareClass_Code_Here()

  End Sub

  ''' <summary>
  ''' Calculates the fees_ no equalisation.
  ''' </summary>
  ''' <param name="FundRow">The fund row.</param>
  ''' <returns><c>true</c> on success, <c>false</c> otherwise</returns>
  Public Function CalculateFees_NoEqualisation(ByVal FundRow As DSFund.tblFundRow) As Boolean

    ' ********************************************************************************
    '
    '
    '
    ' Note : 
    '--			(1) GAV		Gross Asset Value Excl all expenses and fees, After (@ExFundExpensesAfterDate, ExManagementFeesAfterDate and @ExPerformanceFeesAfterDate)
    '--			(2) Basic NAV 	Excluding Mgmt & Incentive fees (After @ExManagementFeesAfterDate and @ExPerformanceFeesAfterDate), but including all other expenses
    '--			(3) GNAV	NAV, excluding only Performance (Incentive) fees after ExPerformanceFeesAfterDate
    '--			(4) NAV		NAV, including all expenses etc.
    '
    ' At present, Milestone Records are used to establish the last Year-End, i.e. this Year Start, NAV (Milestone field 'Administrator_NAV')
    ' This NAV is used as the starting value for the Benchmark calculations.
    ' Milestones were also used to calculate the day-before-year-end, so that subscriptions / Redemptions during that period, which only take effect after year end,
    ' could be used in the performance fee calculations. This is currently commented out as it is not reliable for the historic funds.
    ' 
    ' ********************************************************************************

    Dim RVal As FundFeeDetails = Nothing
    Dim FundYearEnd As Date
    Dim FundYearStart As Date
    Dim DaysInFinancialYear As Integer
    Dim DaysInFeeDateYear As Integer = 365
    Dim DaysSoFar As Integer
    Dim PerformanceFeeCalculationperiod As DealingPeriod
    Dim Counter As Integer

    ' ********************************************************************************
    ' Needs to change for Multi Share Classes ?
    More_MultiShareClass_Code_Here()
    Dim InstrumentRow As DSInstrument.tblInstrumentRow = Nothing
    'Dim InstrumentRows() As DSInstrument.tblInstrumentRow

    Try
      If (FundRow.FundUnit <> 0) Then
        InstrumentRow = CType(LookupTableRow(Mainform, RenaissanceStandardDatasets.tblInstrument, FundRow.FundUnit), DSInstrument.tblInstrumentRow)
      Else
        InstrumentRow = CType(LookupTableRow(Mainform, RenaissanceStandardDatasets.tblInstrument, 0, "", "InstrumentUnitManagementFees DESC", "InstrumentFundID=" & FundRow.FundID.ToString), DSInstrument.tblInstrumentRow)
      End If
    Catch ex As Exception
    End Try

    ' ********************************************************************************

    Dim MilestonesDS As DSFundMilestones
    Dim MilestonesTable As DSFundMilestones.tblFundMilestonesDataTable
    Dim MilestonesSelectedRows() As DSFundMilestones.tblFundMilestonesRow
    Dim thisMilestonesRow As DSFundMilestones.tblFundMilestonesRow
    'Dim T1_MilestonesRow As DSFundMilestones.tblFundMilestonesRow
    Dim YearStart_MilestonesRow As DSFundMilestones.tblFundMilestonesRow
    'Dim YearStart_MilestonesRow_LessOne As DSFundMilestones.tblFundMilestonesRow
    Dim MilestoneYearStartDate As Date
    Dim MilestoneYearStartButOneDate As Date

    Dim BenchmarkIndexRow As DSBenchmarkIndex.tblBenchmarkIndexRow = Nothing
    Dim BenchmarkItemsDS As DSBenchmarkItems = Nothing
    Dim BenchmarkItemsTable As DSBenchmarkItems.tblBenchmarkItemsDataTable = Nothing
    Dim BenchmarkItemRows() As DSBenchmarkItems.tblBenchmarkItemsRow = Nothing
    Dim thisBenchmarkItemRow As DSBenchmarkItems.tblBenchmarkItemsRow
    Dim bestBenchmarkItem As DSBenchmarkItems.tblBenchmarkItemsRow = Nothing
    Dim thisBenchmarkDescription As String = ""
    Dim BestBenchmarkDescription As String = ""
    Dim BestBenchmarkItemID As Integer = 0
    Dim BestReturnToDate As Double = 0.0#
    Dim ThisReturnToDate As Double
    Dim BestBenchmarkDates() As Date = Nothing
    Dim BestBenchmarkNAVs() As Double = Nothing
    Dim Date_Series() As Date
    Dim NAV_Series() As Double
    Dim DateCount As Integer
    Dim FixedReturn As Double = 0.0#

    Dim SubscriptionsValueThisYear As Double
    Dim RedemptionValueThisYear As Double
    Dim CrystalisedRedemptions As Double = 0.0#
    Dim CrystalisedFees As Double = 0.0#

    Dim FundStartValue As Double
    ' Dim FundValueAdded As Double
    Dim BenchmarkReturn As Double
    ' Dim BenchmarkValueAdded As Double

    If (_FeesNeedUpdate = False) Then
      Return False
    End If

    Try

      If (_ValuationNeedsUpdate) Then
        Call GetFundValuation()
      End If

      If (_ValuationNeedsUpdate) Then
        ' Clearly a problem
        Return False
      End If

      ' OK Get Fund Details

      RVal = New FundFeeDetails

      RVal.FundID = FundID
      RVal.ValuationUsed = _ValuationObject

      RVal.StartDate = FeeStartDate
      RVal.ValueDate = FeeDate
      RVal.FundUsesEqualisation = FundRow.FundUsesEqualisation

      ' ********************************************************************************
      ' Needs to change for Multi Share Classes ?
      More_MultiShareClass_Code_Here()
      If (InstrumentRow IsNot Nothing) Then
        'RVal.ManagementFeesPercent = FundRow.FundManagementFees
        RVal.ManagementFeesPercent = InstrumentRow.InstrumentUnitManagementFees
        RVal.PerformanceFeesPercent = InstrumentRow.InstrumentUnitPerformanceFees
      Else
        RVal.ManagementFeesPercent = 0.0#
        RVal.PerformanceFeesPercent = 0.0#
      End If

      PerformanceFeeCalculationperiod = FundRow.FundPerformanceFeesAccruePeriod

      '_FundYearStartDate = New Date(_FeeDate.Year - 1, 12, 31)

      If FundRow.IsFundYearEndNull Then
        FundYearEnd = New Date(Now.Year, 12, 31) ' Default to calendar year
      Else
        FundYearEnd = FitDateToPeriod(DealingPeriod.Monthly, New Date(FeeDate.Year, FundRow.FundYearEnd.Month, 28), True)

        If (FundYearEnd < FeeDate) Then
          FundYearEnd = FundYearEnd.AddYears(1)
        End If
      End If

      FundYearStart = FundYearEnd.AddYears(-1)

      If (ASSUME_LONG_FIRST_YEAR_IF_APPROPRIATE) Then
        ' Check to see that the fund has been running for at least a year before this year start.

        ' If (FundInvestmentStartDate > (FundYearEnd - 23 Months)) then ....
        If FitDateToPeriod(DealingPeriod.Monthly, FundRow.FundInvestmentStartDate, False) > FitDateToPeriod(DealingPeriod.Monthly, AddPeriodToDate(DealingPeriod.Monthly, FundYearEnd, -23), False) Then
          FundYearStart = MaxDate(FundYearStart.AddYears(-1), FitDateToPeriod(DealingPeriod.Monthly, AddPeriodToDate(DealingPeriod.Monthly, FundRow.FundInvestmentStartDate, -1)))

          If (FundYearStart > FundYearEnd.AddYears(-1)) Then ' 
            FundYearEnd = FundYearEnd.AddYears(1)
          End If
        End If
      End If


      DaysInFinancialYear = (FundYearEnd - FundYearStart).Days
      DaysInFeeDateYear = (FitDateToPeriod(DealingPeriod.Annually, FeeDate, True) - FitDateToPeriod(DealingPeriod.Annually, FeeDate, False)).TotalDays + 1
      DaysSoFar = (FeeDate - FundYearStart).Days

      RVal.FundYearStart = FundYearStart
      RVal.FundYearEnd = FundYearEnd
      RVal.DaysInFinancialYear = DaysInFinancialYear
      RVal.DaysSoFar = DaysSoFar

      ' Management Fees

      RVal.ManagementFees = _ValuationObject.FundBNAV * RVal.ManagementFeesPercent * CDbl((FeeDate - FeeStartDate).Days) / CDbl(DaysInFeeDateYear) ' CDbl(RVal.DaysInYear)

      ' Performance Fees


      ' ************************************************************************
      ' Get Milestone values (For Year Start Fund NAV)
      '
      ' ************************************************************************

      'T1_MilestonesRow = Nothing
      YearStart_MilestonesRow = Nothing
      'YearStart_MilestonesRow_LessOne = Nothing

      MilestonesDS = Mainform.Load_Table(RenaissanceStandardDatasets.tblFundMilestones, False)
      If (MilestonesDS IsNot Nothing) Then
        MilestonesTable = MilestonesDS.tblFundMilestones
        MilestonesSelectedRows = MilestonesTable.Select("MilestoneFundID=" & FundID.ToString, "MilestoneDate Desc")

        Dim MSCounter As Integer

        ' Milestones are ordered Date Descending.

        For MSCounter = 0 To (MilestonesSelectedRows.Length - 1)
          thisMilestonesRow = MilestonesSelectedRows(MSCounter)

          'If (thisMilestonesRow.MilestoneDate.Date < FeeDate.Date) AndAlso (T1_MilestonesRow Is Nothing) Then
          '	T1_MilestonesRow = thisMilestonesRow
          'End If

          If (thisMilestonesRow.MilestoneDate.Date <= FundYearStart.Date) AndAlso (YearStart_MilestonesRow Is Nothing) Then
            YearStart_MilestonesRow = thisMilestonesRow

            'If (MSCounter < (MilestonesSelectedRows.Length - 1)) Then
            '	YearStart_MilestonesRow_LessOne = MilestonesSelectedRows(MSCounter + 1)
            'Else
            '	YearStart_MilestonesRow_LessOne = Nothing
            'End If
          End If

        Next
      End If

      If (YearStart_MilestonesRow IsNot Nothing) AndAlso (YearStart_MilestonesRow.IsAdministrator_NAVNull = False) Then
        FundStartValue = YearStart_MilestonesRow.Administrator_NAV
        MilestoneYearStartDate = YearStart_MilestonesRow.MilestoneDate
      Else
        FundStartValue = 0.0#
        MilestoneYearStartDate = FundYearStart
      End If

      'If (YearStart_MilestonesRow_LessOne IsNot Nothing) AndAlso (YearStart_MilestonesRow_LessOne.IsMilestoneDateNull = False) Then
      '	MilestoneYearStartButOneDate = YearStart_MilestonesRow_LessOne.MilestoneDate
      'Else
      MilestoneYearStartButOneDate = AddPeriodToDate(PerformanceFeeCalculationperiod, FundYearStart, -1)
      'End If

      SubscriptionsValueThisYear = _ValuationObject.FundValueSubscriptionsThisYear
      RedemptionValueThisYear = _ValuationObject.FundValueRedemptionsThisYear

      ' ************************************************************************
      '
      ' ************************************************************************

      If (RVal.PerformanceFeesPercent <= 0.0#) Then
        RVal.PerformanceFees = 0.0#

        RVal.BestBenchmarkItemID = 0
        RVal.BestBenchmarkReturn = 0.0#
        BestBenchmarkDescription = "Performance fee rate is 0%."

      ElseIf (InstrumentRow IsNot Nothing) AndAlso (InstrumentRow.InstrumentUnitPerformanceBenchmark > 0) Then
        ' ********************************************************************************
        ' Needs to change (above) for Multi Share Classes ?
        More_MultiShareClass_Code_Here()

        ' Fees vs Performance of Benchmark(s)

        ' Get Benchmark Info :-

        ' Benchmark index for value set on Fund Details

        BenchmarkIndexRow = CType(LookupTableRow(Mainform, RenaissanceStandardDatasets.tblBenchmarkIndex, InstrumentRow.InstrumentUnitPerformanceBenchmark), DSBenchmarkIndex.tblBenchmarkIndexRow)

        BenchmarkItemsDS = Mainform.Load_Table(RenaissanceStandardDatasets.tblBenchmarkItems, False)

        ' Benchmark Items for this Benchmark Index

        If (BenchmarkIndexRow IsNot Nothing) AndAlso (BenchmarkItemsDS IsNot Nothing) Then
          BenchmarkItemsTable = BenchmarkItemsDS.tblBenchmarkItems

          BenchmarkItemRows = BenchmarkItemsTable.Select("BenchmarkID = " & BenchmarkIndexRow.BenchmarkID.ToString)
        End If

        ' 

        If (BenchmarkIndexRow IsNot Nothing) AndAlso (BenchmarkItemRows IsNot Nothing) AndAlso (BenchmarkItemRows.Length > 0) Then

          For Each thisBenchmarkItemRow In BenchmarkItemRows
            Date_Series = Nothing
            NAV_Series = Nothing

            Select Case CType(thisBenchmarkItemRow.BenchmarkItemType, BenchmarkItemType)

              Case BenchmarkItemType.FixedReturn
                ' Calculate Return to date and NAV Series (For Performance Calculations)

                thisBenchmarkDescription = "Annual Fixed Return : " & thisBenchmarkItemRow.BenchmarkFixedRate.ToString(DISPLAYMEMBER_PERCENTAGEFORMAT)

                DateCount = RenaissanceUtilities.DatePeriodFunctions.GetPeriodCount(PerformanceFeeCalculationperiod, FundYearStart, FeeDate)
                Date_Series = Array.CreateInstance(GetType(Date), DateCount)
                NAV_Series = Array.CreateInstance(GetType(Double), DateCount)

                If (Date_Series.Length > 0) Then
                  Dim DaysThisYear As Double = 365.0#

                  NAV_Series(0) = 100.0#
                  Date_Series(0) = FundYearStart

                  Select Case CType(thisBenchmarkItemRow.BenchmarkItemCalculationMethod, BenchmarkItemCalculationMethod)

                    Case BenchmarkItemCalculationMethod.DailyCompounding

                      FixedReturn = Math.Log(1.0# + (thisBenchmarkItemRow.BenchmarkFixedRate / CDbl(DaysThisYear)))

                    Case Else ' BenchmarkItemCalculationMethod.CompoundToAnnualisedFixedReturn

                      FixedReturn = Math.Log(1.0# + thisBenchmarkItemRow.BenchmarkFixedRate) / CDbl(DaysThisYear)

                  End Select

                  For Counter = 1 To (Date_Series.Length - 1)

                    Date_Series(Counter) = FitDateToPeriod(PerformanceFeeCalculationperiod, AddPeriodToDate(PerformanceFeeCalculationperiod, FundYearStart, Counter), True)

                    'DaysThisYear = (FitDateToPeriod(DealingPeriod.Annually, Date_Series(Counter), True) - FitDateToPeriod(DealingPeriod.Annually, Date_Series(Counter), False)).TotalDays + 1

                    'Select Case CType(thisBenchmarkItemRow.BenchmarkItemCalculationMethod, BenchmarkItemCalculationMethod)

                    '  Case BenchmarkItemCalculationMethod.DailyCompounding

                    '    FixedReturn = Math.Log(1.0# + (thisBenchmarkItemRow.BenchmarkFixedRate / CDbl(DaysThisYear)))

                    '  Case Else ' BenchmarkItemCalculationMethod.CompoundToAnnualisedFixedReturn

                    '    FixedReturn = Math.Log(1.0# + thisBenchmarkItemRow.BenchmarkFixedRate) / CDbl(DaysThisYear)

                    'End Select

                    NAV_Series(Counter) = Math.Exp(CDbl((Date_Series(Counter) - Date_Series(0)).TotalDays) * FixedReturn) * NAV_Series(0)

                  Next

                  'ThisReturnToDate = XX
                  ThisReturnToDate = Math.Exp(CDbl(DaysSoFar) * FixedReturn) - 1.0#

                End If

                'Select Case CType(thisBenchmarkItemRow.BenchmarkItemCalculationMethod, BenchmarkItemCalculationMethod)

                '	Case BenchmarkItemCalculationMethod.DailyCompounding

                '		FixedReturn = Math.Log(1.0# + (thisBenchmarkItemRow.BenchmarkFixedRate / DaysInYear))

                '		ThisReturnToDate = Math.Exp(CDbl(DaysSoFar) * FixedReturn) - 1.0#


                '	Case Else	' BenchmarkItemCalculationMethod.CompoundToAnnualisedFixedReturn

                '		FixedReturn = Math.Log(1.0# + thisBenchmarkItemRow.BenchmarkFixedRate) / DaysInYear

                '		ThisReturnToDate = Math.Exp(CDbl(DaysSoFar) * FixedReturn) - 1.0#

                'End Select

                'For Counter = 1 To (Date_Series.Length - 1)

                '	NAV_Series(Counter) = Math.Exp(CDbl((AddPeriodToDate(PerformanceFeeCalculationperiod, FundYearStart, Counter) - FundYearStart).Days) * FixedReturn) * NAV_Series(0)

                'Next

              Case Else

                ' Get NAV Series, 

                Dim ThisPertracDataType As PertracInstrumentFlags = PertracInstrumentFlags.PertracInstrument

                If CType(thisBenchmarkItemRow.BenchmarkItemType, BenchmarkItemType) = BenchmarkItemType.Group_Mean Then ThisPertracDataType = PertracInstrumentFlags.Group_Mean
                If CType(thisBenchmarkItemRow.BenchmarkItemType, BenchmarkItemType) = BenchmarkItemType.Group_Median Then ThisPertracDataType = PertracInstrumentFlags.Group_Median
                If CType(thisBenchmarkItemRow.BenchmarkItemType, BenchmarkItemType) = BenchmarkItemType.Group_Weighted Then ThisPertracDataType = PertracInstrumentFlags.Group_Weighted
                If CType(thisBenchmarkItemRow.BenchmarkItemType, BenchmarkItemType) = BenchmarkItemType.VeniceInstrument Then ThisPertracDataType = PertracInstrumentFlags.VeniceInstrument
                If CType(thisBenchmarkItemRow.BenchmarkItemType, BenchmarkItemType) = BenchmarkItemType.CTA_Simulation Then ThisPertracDataType = PertracInstrumentFlags.CTA_Simulation

                Dim CombinedID As UInteger

                ' If the given BenchmarkItemType is zero then just use the BenchmarkInstrumentID. This enables us to set a combined ID in the BenchmarkInstrumentID field if we want to.
                ' (The SetFlagsToID() function would otherwise clear existing flags to zero).
                If (thisBenchmarkItemRow.BenchmarkItemType = 0) Then
                  CombinedID = CUInt(thisBenchmarkItemRow.BenchmarkInstrumentID)
                Else
                  CombinedID = SetFlagsToID(CUInt(thisBenchmarkItemRow.BenchmarkInstrumentID), ThisPertracDataType)
                End If

                thisBenchmarkDescription = Mainform.PertracData.GetInformationValue(CombinedID, PertracInformationFields.Mastername)

                Date_Series = Mainform.StatFunctions.DateSeries(PerformanceFeeCalculationperiod, CombinedID, False, 0, 1, False, FundYearStart, FeeDate)
                NAV_Series = Mainform.StatFunctions.NAVSeries(PerformanceFeeCalculationperiod, CombinedID, False, 0, 1, False, FundYearStart, FeeDate, 1.0#, True, 100.0#)

                If (Date_Series IsNot Nothing) AndAlso (Date_Series.Length > 0) Then
                  Dim StartIndex As Integer = RenaissanceUtilities.DatePeriodFunctions.GetPriceIndex(PerformanceFeeCalculationperiod, Date_Series(0), FundYearStart)
                  Dim EndIndex As Integer = RenaissanceUtilities.DatePeriodFunctions.GetPriceIndex(PerformanceFeeCalculationperiod, Date_Series(0), FeeDate)

                  ThisReturnToDate = (NAV_Series(Math.Min(EndIndex, NAV_Series.Length - 1)) / NAV_Series(Math.Max(StartIndex, 1))) - 1.0#
                Else
                  ThisReturnToDate = 0
                End If

            End Select

            Select Case CType(BenchmarkIndexRow.BenchmarkCalculationMethod, BenchmarkCalculationMethod)

              Case BenchmarkCalculationMethod.BestIndex

                If (bestBenchmarkItem Is Nothing) OrElse (ThisReturnToDate > BestReturnToDate) Then
                  BestReturnToDate = ThisReturnToDate
                  bestBenchmarkItem = thisBenchmarkItemRow

                  BestBenchmarkDescription = thisBenchmarkDescription
                  BestBenchmarkItemID = thisBenchmarkItemRow.BenchmarkItemID
                  BestBenchmarkDates = Date_Series
                  BestBenchmarkNAVs = NAV_Series
                End If

              Case BenchmarkCalculationMethod.WorstIndex

                If (bestBenchmarkItem Is Nothing) OrElse (ThisReturnToDate < BestReturnToDate) Then
                  BestReturnToDate = ThisReturnToDate
                  bestBenchmarkItem = thisBenchmarkItemRow
                  BestBenchmarkDescription = thisBenchmarkDescription
                  BestBenchmarkItemID = thisBenchmarkItemRow.BenchmarkItemID

                  BestBenchmarkDates = Date_Series
                  BestBenchmarkNAVs = NAV_Series
                End If

              Case BenchmarkCalculationMethod.AverageIndex

                ' TODO !!!

            End Select

          Next

        End If

        If (bestBenchmarkItem IsNot Nothing) Then
          RVal.BestBenchmarkItemID = bestBenchmarkItem.BenchmarkItemID
          RVal.BestBenchmarkReturn = BestReturnToDate
          RVal.BestBenchmarkDescription = BestBenchmarkDescription
        Else
          RVal.BestBenchmarkItemID = 0
          RVal.BestBenchmarkReturn = 0.0#
          RVal.BestBenchmarkDescription = "None."
        End If

      Else

        ' Fees Vs Absolute Performance - Hurdle, which can be zero
        ' ********************************************************************************
        ' Needs to change for Multi Share Classes ?
        More_MultiShareClass_Code_Here()

        If (InstrumentRow IsNot Nothing) Then
          BestBenchmarkDescription = "No Benchmark, Hurdle rate of " & InstrumentRow.InstrumentUnitPerformanceHurdle.ToString(DISPLAYMEMBER_PERCENTAGEFORMAT)

          FixedReturn = Math.Log(1.0# + InstrumentRow.InstrumentUnitPerformanceHurdle) / 365.0# 'DaysInYear
        Else
          BestBenchmarkDescription = "No Benchmark, Hurdle rate of 0%"

          FixedReturn = Math.Log(1.0#) / 365.0# 'DaysInYear
        End If

        BenchmarkReturn = Math.Exp(CDbl(DaysSoFar) * FixedReturn) - 1.0#

        RVal.BestBenchmarkItemID = 0
        RVal.BestBenchmarkReturn = BenchmarkReturn
        RVal.BestBenchmarkDescription = BestBenchmarkDescription

        ' Calculate Fake Dates and Nav series

        DateCount = RenaissanceUtilities.DatePeriodFunctions.GetPeriodCount(PerformanceFeeCalculationperiod, FundYearStart, FeeDate)
        Date_Series = Array.CreateInstance(GetType(Date), DateCount)
        NAV_Series = Array.CreateInstance(GetType(Double), DateCount)
        ThisReturnToDate = Math.Exp(CDbl(DaysSoFar) * FixedReturn) - 1.0#

        If (Date_Series.Length > 0) Then
          NAV_Series(0) = 100.0#
          Date_Series(0) = FundYearStart

          For Counter = 1 To (Date_Series.Length - 1)

            Date_Series(Counter) = FitDateToPeriod(PerformanceFeeCalculationperiod, AddPeriodToDate(PerformanceFeeCalculationperiod, FundYearStart, Counter), True)
            NAV_Series(Counter) = Math.Exp(CDbl((Date_Series(Counter) - FundYearStart).Days) * FixedReturn) * NAV_Series(0)

          Next

        End If

      End If


      ' ************************************************************************
      ' OK, Got the 'best' index to use. Work out Perf Fees.
      ' ************************************************************************

      ' Performance Fees = ((Total Value Added) - (Value Added - Best Benchmark)) * Fee Rate

      ' OK, Fund Total value added : 

      ' (3) GNAV :- NAV, excluding only Performance (Incentive) fees after ExPerformanceFeesAfterDate and @ExCrystalisedPerformanceFeesAfterDate

      ' Benchmark 'Value Added'
      ' Don't forget to compound the value of subscriptions and redemptions
      ' _SubscriptionsObject

      Dim BenchmarkValue(-1) As Double  '  Array range 0 to (BestBenchmarkDates.Length - 1)
      Dim FinalDateIndex As Integer
      Dim RedemtionUnitsOnly(-1) As Double
      Dim IssuedUnits(-1) As Double
      Dim UnitsRunningTotal(-1) As Double
      Dim RedemptionRunningTotal(-1) As Double
      Dim DateIndex As Integer

      If (BestBenchmarkDates Is Nothing) OrElse (_SubscriptionsArrays Is Nothing) OrElse (_SubscriptionsArrays.Count <= 0) Then ' OrElse (_SubscriptionsObject Is Nothing) OrElse (_SubscriptionsObject.Length <= 0) Then

        'BenchmarkValueAdded = (FundStartValue * BenchmarkReturn)

      Else
        'BestBenchmarkDates = Date_Series
        'BestBenchmarkNAVs = NAV_Series

        FinalDateIndex = RenaissanceUtilities.DatePeriodFunctions.GetPriceIndex(PerformanceFeeCalculationperiod, FundYearStart, FeeDate)

        ReDim BenchmarkValue(Math.Max(BestBenchmarkDates.Length - 1, FinalDateIndex))
        ReDim RedemtionUnitsOnly(Math.Max(BestBenchmarkDates.Length, FinalDateIndex))
        ReDim IssuedUnits(Math.Max(BestBenchmarkDates.Length, FinalDateIndex))
        ReDim UnitsRunningTotal(Math.Max(BestBenchmarkDates.Length, FinalDateIndex))
        ReDim RedemptionRunningTotal(Math.Max(BestBenchmarkDates.Length, FinalDateIndex))
        ' Dim ValueOfNetSubscriptions As Double = 0.0#

        RVal.DatesArray = BestBenchmarkDates
        RVal.BenchmarkNAVArray = BestBenchmarkNAVs
        RVal.UnitsSubscribed = Array.CreateInstance(GetType(Double), BestBenchmarkDates.Length)
        RVal.UnitsRedeemed = RedemtionUnitsOnly
        RVal.UnitsRunningTotal = UnitsRunningTotal
        RVal.BenchmarkValueArray = BenchmarkValue

        BenchmarkValue(0) = FundStartValue


        ' Allocate S / R value to Benchmark NAV series

        ' Temporary Fudge.
        ' This code does not yet handle multiple share classes, so just take the first one.

        If (_SubscriptionsArrays IsNot Nothing) AndAlso (_SubscriptionsArrays.Count > 0) Then
          Dim _SubscriptionsObject() As SR_InformationClass

          _SubscriptionsObject = _SubscriptionsArrays.Values.ToArray(0)

          For Counter = 0 To (_SubscriptionsObject.Length - 1)

            ' Subscriptions before 'MilestoneYearStartButOneDate' should be rolled into the start valuation.
            ' Offset Date index by one.
            ' This reflects the fact that subscriptions on a day are not reflected in that days NAV
            ' Note this approach alters slightly the Benchmark Value calculations.

            DateIndex = 1 + RenaissanceUtilities.DatePeriodFunctions.GetPriceIndex(PerformanceFeeCalculationperiod, FundYearStart, _SubscriptionsObject(Counter).TradeDate)

            IssuedUnits(Min(Max(0, DateIndex), BestBenchmarkDates.Length - 1)) += (_SubscriptionsObject(Counter).UnitsSubscribed - _SubscriptionsObject(Counter).UnitsRedeemed)
            RVal.UnitsSubscribed(Min(Max(0, DateIndex), BestBenchmarkDates.Length - 1)) += (_SubscriptionsObject(Counter).UnitsSubscribed)
            RedemtionUnitsOnly(Min(Max(0, DateIndex), BestBenchmarkDates.Length - 1)) += (_SubscriptionsObject(Counter).UnitsRedeemed)

            ' Apply Subscription value to Benchmark value

            If (DateIndex > 0) AndAlso (_SubscriptionsObject(Counter).TradeDate > MilestoneYearStartButOneDate) Then

              ' Check Index is in range and is not the final (FeeDate) period (for which we will ignore Subs and Reds).

              If (DateIndex < BenchmarkValue.Length) AndAlso (DateIndex <= FinalDateIndex) Then
                BenchmarkValue(DateIndex) += _SubscriptionsObject(Counter).ValueSubscribed ' - _SubscriptionsObject(Counter).ValueRedeemed)
                ' ValueOfNetSubscriptions += (_SubscriptionsObject(Counter).ValueSubscribed - _SubscriptionsObject(Counter).ValueRedeemed)
                ' RedemtionUnitsOnly(DateIndex) += _SubscriptionsObject(Counter).UnitsRedeemed
              End If

            End If

          Next

        End If


        ' Running Total

        UnitsRunningTotal(0) = IssuedUnits(0)
        RedemptionRunningTotal(0) = RedemtionUnitsOnly(0)

        For Counter = 1 To (IssuedUnits.Length - 1)
          UnitsRunningTotal(Counter) = IssuedUnits(Counter) + UnitsRunningTotal(Counter - 1)
          RedemptionRunningTotal(Counter) = RedemtionUnitsOnly(Counter) + RedemptionRunningTotal(Counter - 1)
        Next

        ' Calculate NAVs

        For Counter = 1 To (BenchmarkValue.Length - 1)
          ' Note : formula includes todays (yesterdays really) Subscriptions in the value to be compounded.
          ' BenchmarkValue(Counter) = ((BenchmarkValue(Counter - 1) + BenchmarkValue(Counter)) * (BestBenchmarkNAVs(Counter) / BestBenchmarkNAVs(Counter - 1)))

          ' Value of Subscriptions are already in the BenchmarkValue() array, Roll up the value, adjusting for redemptions
          BenchmarkValue(Counter) = (BenchmarkValue(Counter - 1) + BenchmarkValue(Counter) - (BenchmarkValue(Counter - 1) * IIf(UnitsRunningTotal(Counter - 1) > 0.0#, (RedemtionUnitsOnly(Counter) / UnitsRunningTotal(Counter - 1)), 1.0#))) * (BestBenchmarkNAVs(Math.Min(Counter, BestBenchmarkNAVs.Length - 1)) / BestBenchmarkNAVs(Math.Min(Counter - 1, BestBenchmarkNAVs.Length - 1)))
        Next

        'BenchmarkValueAdded = (BenchmarkValue(FinalDateIndex) - FundStartValue)	' - ValueOfNetSubscriptions
      End If

      ' Calculate Crystalised fees. 
      ' Done here because it uses the Redemption values calculated above.
      ' Perf Fee calc below, uses the crystalised fees value calculated here.

      If (RedemptionRunningTotal.Length <= 0) Then
        CrystalisedRedemptions = 0.0#
      Else
        ' Try to use the Redemptions running total
        DateIndex = RenaissanceUtilities.DatePeriodFunctions.GetPriceIndex(PerformanceFeeCalculationperiod, FundYearStart, FeeStartDate)

        If (DateIndex >= 0) AndAlso (DateIndex < RedemptionRunningTotal.Length) Then
          CrystalisedRedemptions = Math.Abs(_ValuationObject.FundUnitsRedeemed) - Math.Abs(RedemptionRunningTotal(DateIndex))
        ElseIf (DateIndex >= RedemptionRunningTotal.Length) Then
          CrystalisedRedemptions = Math.Abs(_ValuationObject.FundUnitsRedeemed) - Math.Abs(RedemptionRunningTotal(RedemptionRunningTotal.Length - 1))
        Else
          CrystalisedRedemptions = 0.0#
        End If
      End If

      ' Crystalised Fees :

      RVal.TodaysRedemptions = CrystalisedRedemptions

      If (Math.Abs(_ValuationObject.PreviousPerformanceFees) > 0.0#) Then
        If (Math.Abs(_ValuationObject.FundUnits) > 0) Then
          CrystalisedFees = Math.Abs(_ValuationObject.PreviousPerformanceFees) * (CrystalisedRedemptions / Math.Abs(_ValuationObject.FundUnits))
        Else
          CrystalisedFees = Math.Abs(_ValuationObject.PreviousPerformanceFees)
        End If
      End If

      ' Note : Adjust FundPerformanceGnavUsed for Mgmt fees not yet taken.

      RVal.PerformanceFeesCrystalised = CrystalisedFees
      RVal.FundPerformanceGnavUsed = _ValuationObject.FundGNAV - (RVal.ManagementFees + _ValuationObject.PendingManagementFees) + Math.Abs(_ValuationObject.PreviousPerformanceFees) - CrystalisedFees

      ' Outperformance :
      'RVal.BenchmarkValueAdded = BenchmarkValueAdded
      'RVal.FundValueAdded = FundValueAdded
      RVal.FundStartValue = FundStartValue

      ' ********************************************************************************
      ' Needs to change for Multi Share Classes ?
      More_MultiShareClass_Code_Here()

      If (RVal.PerformanceFeesPercent > 0.0#) Then

        FinalDateIndex = RenaissanceUtilities.DatePeriodFunctions.GetPriceIndex(PerformanceFeeCalculationperiod, FundYearStart, FeeDate)

        If (BenchmarkValue.Length > FinalDateIndex) Then

          RVal.BenchmarkNAV = BenchmarkValue(FinalDateIndex)

          'RVal.PerformanceFees = (FundValueAdded - BenchmarkValueAdded) * Math.Max(FundRow.FundPerformanceFees, 0)
          RVal.PerformanceFees = Math.Max((RVal.FundPerformanceGnavUsed - BenchmarkValue(FinalDateIndex)) * Math.Max(RVal.PerformanceFeesPercent, 0), 0)

        Else
          RVal.BenchmarkNAV = 0.0#
          RVal.PerformanceFees = 0.0#
        End If


      End If

      _FeesObject = RVal
      _FeesNeedUpdate = False

      Return True

    Catch ex As Exception
    Finally
    End Try

    _FeesObject = Nothing
    Return False

  End Function

  ''' <summary>
  ''' Gets the fund fees.
  ''' </summary>
  ''' <returns>FundFeeDetails.</returns>
  Public Function GetFundFees() As FundFeeDetails
    ' ********************************************************************************
    '
    '
    ' ********************************************************************************

    Try

      If ValuationNeedsUpdate Then
        Call CalculateFundValuation()
      End If

      If FeesNeedUpdate Then
        Call CalculateFees()
      End If

      If (_FeesNeedUpdate) Then
        Return Nothing
      End If

      Return _FeesObject

    Catch ex As Exception

    End Try

    Return Nothing

  End Function


#Region " IDisposable Support "

  ''' <summary>
  ''' The disposed value
  ''' </summary>
  Private disposedValue As Boolean = False    ' To detect redundant calls

  ' IDisposable
  ''' <summary>
  ''' Releases unmanaged and - optionally - managed resources.
  ''' </summary>
  ''' <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
  Protected Overridable Sub Dispose(ByVal disposing As Boolean)
    If Not Me.disposedValue Then
      If disposing Then
        ' TODO: free unmanaged resources when explicitly called
      End If

      ' TODO: free shared unmanaged resources
      Mainform = Nothing

    End If
    Me.disposedValue = True
  End Sub

  ' This code added by Visual Basic to correctly implement the disposable pattern.

  ''' <summary>
  ''' Disposes this instance.
  ''' </summary>
  Public Sub Dispose() ' Implements IDisposable.Dispose
    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
    Dispose(True)
    GC.SuppressFinalize(Me)
  End Sub

  ''' <summary>
  ''' Allows an object to try to free resources and perform other cleanup operations before it is reclaimed by garbage collection.
  ''' </summary>
  Protected Overrides Sub Finalize()
    Dispose(False)
  End Sub

#End Region

End Class