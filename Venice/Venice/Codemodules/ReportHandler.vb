' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="ReportHandler.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Option Explicit On
'Option Strict On

Imports System.IO
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.Math
Imports System.Threading
Imports System.Reflection
Imports C1.C1Report

Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals
Imports RenaissanceStatFunctions
Imports RenaissanceDataClass



''' <summary>
''' Class classReportFieldValue
''' </summary>
Public Class classReportFieldValue
    ''' <summary>
    ''' The _ system string
    ''' </summary>
	Private _SystemString As String
    ''' <summary>
    ''' The _ report field name
    ''' </summary>
	Private _ReportFieldName As String
    ''' <summary>
    ''' The _ report property name
    ''' </summary>
	Private _ReportPropertyName As String
    ''' <summary>
    ''' The _ report field value
    ''' </summary>
	Private _ReportFieldValue As String

    ''' <summary>
    ''' Prevents a default instance of the <see cref="classReportFieldValue"/> class from being created.
    ''' </summary>
	Private Sub New()
	End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="classReportFieldValue"/> class.
    ''' </summary>
    ''' <param name="pFieldName">Name of the p field.</param>
    ''' <param name="PFieldValue">The P field value.</param>
	Public Sub New(ByVal pFieldName As String, ByVal PFieldValue As String)
		Me._SystemString = ""
		Me._ReportFieldName = pFieldName
		Me._ReportFieldValue = PFieldValue
		Me._ReportPropertyName = "Text"
	End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="classReportFieldValue"/> class.
    ''' </summary>
    ''' <param name="pFieldName">Name of the p field.</param>
    ''' <param name="PFieldValue">The P field value.</param>
    ''' <param name="pPropertyValue">The p property value.</param>
	Public Sub New(ByVal pFieldName As String, ByVal PFieldValue As String, ByVal pPropertyValue As String)
		Me._SystemString = ""
		Me._ReportFieldName = pFieldName
		Me._ReportFieldValue = PFieldValue
		Me._ReportPropertyName = pPropertyValue
	End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="classReportFieldValue"/> class.
    ''' </summary>
    ''' <param name="pSystemString">The p system string.</param>
    ''' <param name="pFieldName">Name of the p field.</param>
    ''' <param name="PFieldValue">The P field value.</param>
    ''' <param name="pPropertyValue">The p property value.</param>
	Public Sub New(ByVal pSystemString As String, ByVal pFieldName As String, ByVal PFieldValue As String, ByVal pPropertyValue As String)
		Me._SystemString = pSystemString
		Me._ReportFieldName = pFieldName
		Me._ReportFieldValue = PFieldValue
		Me._ReportPropertyName = pPropertyValue
	End Sub

    ''' <summary>
    ''' Gets the system string.
    ''' </summary>
    ''' <value>The system string.</value>
	Public ReadOnly Property SystemString() As String
		Get
			Return _SystemString
		End Get
	End Property

    ''' <summary>
    ''' Gets the name of the report field.
    ''' </summary>
    ''' <value>The name of the report field.</value>
	Public ReadOnly Property ReportFieldName() As String
		Get
			Return _ReportFieldName
		End Get
	End Property

    ''' <summary>
    ''' Gets the report field value.
    ''' </summary>
    ''' <value>The report field value.</value>
	Public ReadOnly Property ReportFieldValue() As String
		Get
			Return _ReportFieldValue
		End Get
	End Property

    ''' <summary>
    ''' Gets the name of the report property.
    ''' </summary>
    ''' <value>The name of the report property.</value>
	Public ReadOnly Property ReportPropertyName() As String
		Get
			Return _ReportPropertyName
		End Get
	End Property
End Class

''' <summary>
''' Class ReportHandler
''' </summary>
Public Class ReportHandler
    ''' <summary>
    ''' The REPOR t_ knowledge date_ field name
    ''' </summary>
	Public Const REPORT_KnowledgeDate_FieldName As String = "Text_KnowledgeDate"
    ''' <summary>
    ''' The REPOR t_ value date_ field name
    ''' </summary>
	Public Const REPORT_ValueDate_FieldName As String = "Text_ValueDate"

#Region " Local Variable Declaration"

    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
	Private WithEvents _MainForm As VeniceMain
    ''' <summary>
    ''' The _ reload performance tables
    ''' </summary>
	Private _ReloadPerformanceTables As Boolean

    ''' <summary>
    ''' The pertrac return selected rows
    ''' </summary>
	Private PertracReturnSelectedRows() As RenaissanceDataClass.DSInstrumentPerformance.tblInstrumentPerformanceRow

    ''' <summary>
    ''' The set custom report details lock
    ''' </summary>
	Private Shared SetCustomReportDetailsLock As New Object
    ''' <summary>
    ''' The display report lock
    ''' </summary>
	Private Shared DisplayReportLock As New Object

    ''' <summary>
    ''' The key_ fund ID
    ''' </summary>
	Private Const Key_FundID As Integer = 0
    ''' <summary>
    ''' The key_ counterparty ID
    ''' </summary>
	Private Const Key_CounterpartyID As Integer = 1
    ''' <summary>
    ''' The key_ instrument ID
    ''' </summary>
	Private Const Key_InstrumentID As Integer = 2
    ''' <summary>
    ''' The key_ parent ID
    ''' </summary>
	Private Const Key_ParentID As Integer = 2

#End Region

#Region " Properties"

    ''' <summary>
    ''' The main form lock
    ''' </summary>
	Private MainFormLock As New Object
    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
	Public ReadOnly Property MainForm() As VeniceMain
		Get
			SyncLock MainFormLock
				Return _MainForm
			End SyncLock
		End Get
	End Property

    ''' <summary>
    ''' The reload performance tables lock
    ''' </summary>
	Private ReloadPerformanceTablesLock As New Object
    ''' <summary>
    ''' Gets or sets a value indicating whether [reload performance tables].
    ''' </summary>
    ''' <value><c>true</c> if [reload performance tables]; otherwise, <c>false</c>.</value>
	Private Property ReloadPerformanceTables() As Boolean
		Get
			SyncLock ReloadPerformanceTablesLock
				Return _ReloadPerformanceTables
			End SyncLock
		End Get
		Set(ByVal Value As Boolean)
			SyncLock ReloadPerformanceTablesLock
				_ReloadPerformanceTables = Value
			End SyncLock
		End Set
	End Property

#End Region

#Region " Constructors"

    ''' <summary>
    ''' Prevents a default instance of the <see cref="ReportHandler"/> class from being created.
    ''' </summary>
	Private Sub New()

	End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="ReportHandler"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
	Public Sub New(ByRef pMainForm As VeniceMain)

		_MainForm = pMainForm
		ReloadPerformanceTables = True

	End Sub

#End Region

#Region " AutoUpdate"

    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs) Handles _MainForm.VeniceAutoUpdate
		Dim KnowledgeDateChanged As Boolean

		KnowledgeDateChanged = False

		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
			KnowledgeDateChanged = True
		End If

		' ****************************************************************
		' Check for changes relevant to this form
		' ****************************************************************

		' Changes to the Performance table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Performance) = True) Then
			ReloadPerformanceTables = True
			PertracReturnSelectedRows = Nothing
		End If

		' Changes to the tblInstrument table :-
		' Instrument Pertrac Indices may have changed.
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrument) = True) Then
			ReloadPerformanceTables = True
			PertracReturnSelectedRows = Nothing
		End If


	End Sub


#End Region

#Region " Display Report / Get Report Definition functions"

    ''' <summary>
    ''' Displays the report.
    ''' </summary>
    ''' <param name="pFundID">The p fund ID.</param>
    ''' <param name="pReportName">Name of the p report.</param>
    ''' <param name="pRecordset">The p recordset.</param>
    ''' <param name="pStatusBar">The p status bar.</param>
	Sub DisplayReport(ByVal pFundID As Integer, ByVal pReportName As String, ByVal pRecordset As Object, ByRef pStatusBar As ToolStripProgressBar)
		' ************************************************************************
		' Display the given C1Report in using the given data source.
		'
		' ************************************************************************

		Try
			DisplayReport(pReportName, pReportName, pFundID, pRecordset, pStatusBar)
		Catch ex As Exception
		End Try

	End Sub

    ''' <summary>
    ''' Displays the report.
    ''' </summary>
    ''' <param name="pReportName">Name of the p report.</param>
    ''' <param name="pRecordset">The p recordset.</param>
    ''' <param name="pFundID">The p fund ID.</param>
    ''' <param name="pValueDate">The p value date.</param>
    ''' <param name="pKnowledgeDate">The p knowledge date.</param>
    ''' <param name="pStatusBar">The p status bar.</param>
	Sub DisplayReport(ByVal pReportName As String, ByVal pRecordset As Object, ByVal pFundID As Integer, ByVal pValueDate As Date, ByVal pKnowledgeDate As Date, ByRef pStatusBar As ToolStripProgressBar)
		' ************************************************************************
		' Display the given C1Report in using the given data source.
		' Use the supplied Value Date and knowledgeDate
		' ************************************************************************

		Try
			DisplayReport(pReportName, pReportName, pRecordset, pFundID, pValueDate, pKnowledgeDate, pStatusBar)
		Catch ex As Exception
		End Try

	End Sub

    ''' <summary>
    ''' Displays the report.
    ''' </summary>
    ''' <param name="pReportFile">The p report file.</param>
    ''' <param name="pReportName">Name of the p report.</param>
    ''' <param name="pFundID">The p fund ID.</param>
    ''' <param name="pRecordset">The p recordset.</param>
    ''' <param name="pStatusBar">The p status bar.</param>
	Sub DisplayReport(ByVal pReportFile As String, ByVal pReportName As String, ByVal pFundID As Integer, ByRef pRecordset As Object, ByRef pStatusBar As ToolStripProgressBar)
		' ************************************************************************
		' Display the given C1Report in using the given data source.
		'
		' ************************************************************************

		DisplayReport(pReportName, pReportName, pRecordset, pFundID, Now(), MainForm.Main_Knowledgedate, pStatusBar)
	End Sub

    ''' <summary>
    ''' Displays the report.
    ''' </summary>
    ''' <param name="pReportFile">The p report file.</param>
    ''' <param name="pReportName">Name of the p report.</param>
    ''' <param name="pRecordset">The p recordset.</param>
    ''' <param name="pFundID">The p fund ID.</param>
    ''' <param name="pValueDate">The p value date.</param>
    ''' <param name="pKnowledgeDate">The p knowledge date.</param>
    ''' <param name="pStatusBar">The p status bar.</param>
	Sub DisplayReport(ByVal pReportFile As String, ByVal pReportName As String, ByRef pRecordset As Object, ByVal pFundID As Integer, ByVal pValueDate As Date, ByVal pKnowledgeDate As Date, ByRef pStatusBar As ToolStripProgressBar)
		' ************************************************************************
		' Display the given C1Report in a frmViewReport form.
		'
		' First, Get the report definition,
		' Second, Apply the given data source to it.
		' Third, Display in a preview window.
		'
		' ************************************************************************

		Dim thisReport As C1Report

		' Get report definition.

		thisReport = GetReportDefinition(pFundID, pReportFile, pReportName)

		If (thisReport Is Nothing) Then
			Exit Sub
		End If

		Call DisplayReport(thisReport, pRecordset, pValueDate, pKnowledgeDate, pStatusBar)

	End Sub

    ''' <summary>
    ''' Displays the report.
    ''' </summary>
    ''' <param name="pReportName">Name of the p report.</param>
    ''' <param name="pRecordset">The p recordset.</param>
    ''' <param name="pFundID">The p fund ID.</param>
    ''' <param name="pFieldValues">The p field values.</param>
    ''' <param name="pStatusBar">The p status bar.</param>
	Sub DisplayReport(ByVal pReportName As String, ByVal pRecordset As Object, ByVal pFundID As Integer, ByRef pFieldValues() As classReportFieldValue, ByRef pStatusBar As ToolStripProgressBar)
		' ************************************************************************
		' Display the given C1Report in a frmViewReport form.
		'
		' First, Get the report definition,
		' Second, Apply the given data source to it.
		' Third, Display in a preview window.
		'
		' ************************************************************************

		Dim thisReport As C1Report

		' Get report definition.

		thisReport = GetReportDefinition(pFundID, pReportName, pReportName)

		If (thisReport Is Nothing) Then
			Exit Sub
		End If

		Call DisplayReport(thisReport, pRecordset, pFieldValues, pStatusBar)

	End Sub

    ''' <summary>
    ''' Displays the report.
    ''' </summary>
    ''' <param name="thisReport">The this report.</param>
    ''' <param name="pRecordset">The p recordset.</param>
    ''' <param name="pValueDate">The p value date.</param>
    ''' <param name="pKnowledgeDate">The p knowledge date.</param>
    ''' <param name="pStatusBar">The p status bar.</param>
	Public Sub DisplayReport(ByRef thisReport As C1Report, ByVal pRecordset As Object, ByVal pValueDate As Date, ByVal pKnowledgeDate As Date, ByRef pStatusBar As ToolStripProgressBar)
		' ************************************************************************
		' Display the given C1Report in a frmViewReport form.
		'
		' First, Set ValueDate and KnowledgeDate fields.
		' Second, Display in a preview window.
		'
		' ************************************************************************


		If (thisReport Is Nothing) Then Exit Sub
		If (pRecordset Is Nothing) Then Exit Sub

		Dim KDString As String = ""
		Try
			If pKnowledgeDate.CompareTo(KNOWLEDGEDATE_NOW) <= 0 Then
				KDString = "Live"
			ElseIf pKnowledgeDate.TimeOfDay.TotalSeconds >= LAST_SECOND Then
				KDString = pKnowledgeDate.ToString(REPORT_DATEFORMAT)
			Else
				KDString = pKnowledgeDate.ToString(REPORT_LONGDATEFORMAT)
			End If
		Catch ex As Exception
		End Try

		Call DisplayReport(thisReport, pRecordset, New classReportFieldValue() {New classReportFieldValue(REPORT_ValueDate_FieldName, pValueDate.ToString(REPORT_DATEFORMAT)), New classReportFieldValue(REPORT_KnowledgeDate_FieldName, KDString)}, pStatusBar)

	End Sub

    ''' <summary>
    ''' Displays the report.
    ''' </summary>
    ''' <param name="thisReport">The this report.</param>
    ''' <param name="pRecordset">The p recordset.</param>
    ''' <param name="pFieldValues">The p field values.</param>
    ''' <param name="pStatusBar">The p status bar.</param>
	Public Sub DisplayReport(ByRef thisReport As C1Report, ByRef pRecordset As Object, ByRef pFieldValues() As classReportFieldValue, ByRef pStatusBar As ToolStripProgressBar)
		' ************************************************************************
		'
		'
		' ************************************************************************

		Dim thisFieldValue As classReportFieldValue

		If (Not (pFieldValues Is Nothing)) AndAlso (pFieldValues.Length > 0) Then
			For Each thisFieldValue In pFieldValues
				Call SetFieldValue(thisReport, thisFieldValue)
			Next
		End If

		Try
			thisReport.DataSource.Recordset = pRecordset
		Catch ex As Exception
			Exit Sub
		End Try

		DisplayReport(thisReport, pStatusBar)

		SaveReportData(pRecordset, thisReport.ReportName)

	End Sub

    ''' <summary>
    ''' Displays the report.
    ''' </summary>
    ''' <param name="pC1Report">The p c1 report.</param>
    ''' <param name="pStatusBar">The p status bar.</param>
	Public Sub DisplayReport(ByRef pC1Report As C1Report, ByRef pStatusBar As ToolStripProgressBar)
		' ************************************************************************
		' Display the given C1Report in a frmViewReport form.
		'
		' ************************************************************************

		Dim thisViewReportForm As frmViewReport

		' Open new ViewForm object

		Try
			thisViewReportForm = CType(MainForm.New_VeniceForm(VeniceFormID.frmViewReport).Form, frmViewReport)
			thisViewReportForm.SetOpacity(0)
			thisViewReportForm.SetText(pC1Report.ReportName)
			MainForm.BuildWindowsMenu()
		Catch ex As Exception
			MainForm.LogError("DisplayReport()", LOG_LEVELS.Error, ex.Message, "Error loading Report form", ex.StackTrace, True)
			Exit Sub
		End Try

		If (thisViewReportForm Is Nothing) Then
			Exit Sub
		End If

		' Render Report to Preview object

		Try
			Dim thisCursor As Cursor
			Dim OrgTitle As String

			OrgTitle = thisViewReportForm.Text
			thisViewReportForm.SetText("Rendering " & OrgTitle)
			thisCursor = thisViewReportForm.Cursor
			thisViewReportForm.SetCursor(Cursors.WaitCursor)

			If (Not (pStatusBar Is Nothing)) Then
				AddHandler pC1Report.StartSection, AddressOf IncrementStatusBar
				pC1Report.Tag = pStatusBar
			End If

			pC1Report.Render()

			If thisViewReportForm.InvokeRequired Then
				'If (MainForm.IsMdiContainer = False) Then
				'  ' Bug Avoidance.
				'  pC1Report.Render()
				'End If

				'MainForm.SetDocument(thisViewReportForm.ReportPreview, pC1Report.Document)
				thisViewReportForm.SetDocument(pC1Report.Document)
			Else
				thisViewReportForm.ReportPreview.Document = pC1Report.Document
			End If

			If (pStatusBar IsNot Nothing) Then
				pStatusBar.Visible = False
				pC1Report.Tag = Nothing
				RemoveHandler pC1Report.StartSection, AddressOf IncrementStatusBar
			End If

			thisViewReportForm.SetCursor(thisCursor)
			thisViewReportForm.SetText(OrgTitle)

		Catch ex As Exception
		End Try

		' Show form (probably unnecessary as it is shown by default by the New_VeniceForm() routine)

		DisplayReportForm(thisViewReportForm)

	End Sub

    ''' <summary>
    ''' Displays the report form.
    ''' </summary>
    ''' <param name="thisViewReportForm">The this view report form.</param>
	Public Sub DisplayReportForm(ByVal thisViewReportForm As frmViewReport)
		' *******************************************************************************
		'
		'
		' *******************************************************************************
		Try
			If Not (thisViewReportForm Is Nothing) Then

				If (MainForm.EnableReportFadeIn) Then

					Dim TStart As Date = Now()
					Dim TEnd As Date
					Dim OCount As Integer

					For OCount = 0 To 100 Step 5
						thisViewReportForm.SetOpacity(OCount / 100.0#)
						Threading.Thread.Sleep(10)
					Next

					TEnd = Now()
					If (TEnd - TStart).TotalMilliseconds >= 1000 Then
						MainForm.DisableReportFadeIn()
					End If

				Else

					thisViewReportForm.SetOpacity(1.0#)

				End If

				MainForm.ActivateForm(thisViewReportForm)

			End If
		Catch ex As Exception
		End Try

	End Sub

    ''' <summary>
    ''' Saves the report data.
    ''' </summary>
    ''' <param name="pDataSource">The p data source.</param>
    ''' <param name="pReportName">Name of the p report.</param>
	Public Sub SaveReportData(ByRef pDataSource As Object, ByVal pReportName As String)

		Dim FP As Integer = FreeFile()
		Dim ColCount As Integer
		Dim RowCount As Integer
		Dim ThisDataRow As DataRowView
		Dim ThisRowString As String
		Dim pFileName As String

		Try

			If MainForm.GetMenuChecked(MainForm.Menu_Rpt_ReportData_SaveData) Then
				Dim thisFilePath As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
				Dim thisFileName As String = pReportName & "_" & Now.ToString(FILENAME_DATEFORMAT) & ".txt"

        'If MainForm.GetMenuChecked(MainForm.Menu_Rpt_ReportData_PromptFileName) Then
        'End If

				pFileName = Path.Combine(thisFilePath, thisFileName)
			Else
				Exit Sub
			End If

		Catch ex As Exception
			Exit Sub
		End Try

		Try
			Dim ThisView As DataView

			If (TypeOf pDataSource Is DataTable) Then
				ThisView = CType(pDataSource, DataTable).DefaultView
			ElseIf (TypeOf pDataSource Is DataView) Then
				ThisView = CType(pDataSource, DataView)
			Else
				Exit Sub
			End If

			FileOpen(FP, pFileName, OpenMode.Output)

			For RowCount = (-1) To (ThisView.Count - 1)
				ThisRowString = ""

				For ColCount = 0 To (ThisView.Table.Columns.Count - 1)
					If (RowCount < 0) Then
						If (ColCount > 0) Then
							ThisRowString &= Chr(9)
						End If
						ThisRowString &= ThisView.Table.Columns(ColCount).ColumnName
					Else
						ThisDataRow = ThisView.Item(RowCount)
						If (ColCount > 0) Then
							ThisRowString &= Chr(9)
						End If
						If (TypeOf ThisDataRow.Item(ColCount) Is Date) Then
							ThisRowString &= CType(ThisDataRow.Item(ColCount), Date).ToString(QUERY_LONGDATEFORMAT)
						Else
							ThisRowString &= ThisDataRow.Item(ColCount).ToString
						End If
					End If
				Next

				PrintLine(FP, New Object() {ThisRowString})
			Next

		Catch ex As Exception
		Finally
			Try
				FileClose(FP)
			Catch ex As Exception
			End Try
		End Try
	End Sub

    ''' <summary>
    ''' Increments the status bar.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="ReportEventArgs"/> instance containing the event data.</param>
	Private Sub IncrementStatusBar(ByVal sender As Object, ByVal e As ReportEventArgs)
		' ****************************************************************************************
		'
		'
		'
		' ****************************************************************************************

		Static LastUpdateTime As Date = #1/1/1900#

		' Validate Sender.
		If (Not (TypeOf sender Is C1Report)) Then
			Exit Sub
		End If

		Try
			' Cast Sender to C1Report in order to get at the 'Tag' property
			Dim thisReport As C1Report = CType(sender, C1Report)

			' If the 'Tag' is a Progress Bar.
			If (Not (thisReport.Tag Is Nothing)) AndAlso (TypeOf thisReport.Tag Is ToolStripProgressBar) Then
				MainForm.IncrementStatusBar(CType(thisReport.Tag, ToolStripProgressBar))
			End If

		Catch ex As Exception
		End Try

	End Sub


    ''' <summary>
    ''' Gets the report definition.
    ''' </summary>
    ''' <param name="pFundID">The p fund ID.</param>
    ''' <param name="pReportName">Name of the p report.</param>
    ''' <returns>C1Report.</returns>
	Private Function GetReportDefinition(ByVal pFundID As Integer, ByVal pReportName As String) As C1Report
		' ***********************************************************************************
		'
		' ***********************************************************************************

		Return GetReportDefinition(pFundID, pReportName, pReportName)
	End Function


    ''' <summary>
    ''' Gets the report definition.
    ''' </summary>
    ''' <param name="pFundID">The p fund ID.</param>
    ''' <param name="pReportFile">The p report file.</param>
    ''' <param name="pReportName">Name of the p report.</param>
    ''' <returns>C1Report.</returns>
	Private Function GetReportDefinition(ByVal pFundID As Integer, ByVal pReportFile As String, ByVal pReportName As String) As C1Report
		' ******************************************************************************
		' Return an instance of the given report name from the given report file.
		'
		' This routine is designed to search upwards from the current executable directory
		' for the given report file and the given report within it.
		' In addition to the executable path, it will check for the 'ReportDefinitions'
		' subdirectory on it's way up the directory tree.
		' 
		' ******************************************************************************

		SyncLock SetCustomReportDetailsLock
			Return SetCustomReportDetails(MainForm, pFundID, pReportFile, pReportName)
		End SyncLock


	End Function

    ''' <summary>
    ''' Sets the field value.
    ''' </summary>
    ''' <param name="pReport">The p report.</param>
    ''' <param name="pFieldValue">The p field value.</param>
	Private Sub SetFieldValue(ByRef pReport As C1Report, ByRef pFieldValue As classReportFieldValue)
		' ******************************************************************************
		'
		'
		' ******************************************************************************
		Try
			If (pFieldValue Is Nothing) OrElse (pReport Is Nothing) Then
				Exit Sub
			End If

			If Not pReport.Fields.Contains(pFieldValue.ReportFieldName) Then
				Exit Sub
			End If
		Catch ex As Exception
			Exit Sub
		End Try

		Try
			Dim FieldValue As String
			Dim SystemStringValue As String

			' Establish  Field Value.
			' By Default this is the class 'FieldValue' unless a System String name is given.

			FieldValue = pFieldValue.ReportFieldValue

			If pFieldValue.SystemString.Length > 0 Then
				SystemStringValue = MainForm.Get_SystemString(pFieldValue.SystemString)
				If SystemStringValue.Length > 0 Then
					FieldValue = SystemStringValue
				End If
			End If

			' Set Report Field

			Dim TypeInfo As Type
			Dim thisProperty As PropertyInfo

			TypeInfo = pReport.Fields(pFieldValue.ReportFieldName).GetType
			thisProperty = TypeInfo.GetProperty(pFieldValue.ReportPropertyName)

			If thisProperty.PropertyType.UnderlyingSystemType Is GetType(System.String) Then
				thisProperty.SetValue(pReport.Fields(pFieldValue.ReportFieldName), FieldValue, Nothing)
			ElseIf thisProperty.PropertyType.UnderlyingSystemType Is GetType(System.DateTime) Then
				thisProperty.SetValue(pReport.Fields(pFieldValue.ReportFieldName), CDate(FieldValue), Nothing)
			ElseIf thisProperty.PropertyType.UnderlyingSystemType Is GetType(System.Boolean) Then
				thisProperty.SetValue(pReport.Fields(pFieldValue.ReportFieldName), CBool(FieldValue), Nothing)
			ElseIf thisProperty.PropertyType.UnderlyingSystemType Is GetType(System.Single) Then
				thisProperty.SetValue(pReport.Fields(pFieldValue.ReportFieldName), CSng(FieldValue), Nothing)
			ElseIf thisProperty.PropertyType.UnderlyingSystemType Is GetType(System.Double) Then
				thisProperty.SetValue(pReport.Fields(pFieldValue.ReportFieldName), CDbl(FieldValue), Nothing)
			ElseIf thisProperty.PropertyType.UnderlyingSystemType Is GetType(System.Int16) Then
				thisProperty.SetValue(pReport.Fields(pFieldValue.ReportFieldName), CShort(FieldValue), Nothing)
			ElseIf thisProperty.PropertyType.UnderlyingSystemType Is GetType(System.Int32) Then
				thisProperty.SetValue(pReport.Fields(pFieldValue.ReportFieldName), CInt(FieldValue), Nothing)
			ElseIf thisProperty.PropertyType.UnderlyingSystemType Is GetType(System.Int64) Then
				thisProperty.SetValue(pReport.Fields(pFieldValue.ReportFieldName), CLng(FieldValue), Nothing)
			ElseIf thisProperty.PropertyType.UnderlyingSystemType Is GetType(System.Drawing.Color) Then
				If IsNumeric(FieldValue) Then
					Dim IntValue As Integer
					IntValue = CInt(FieldValue)
					thisProperty.SetValue(pReport.Fields(pFieldValue.ReportFieldName), System.Drawing.Color.FromArgb(CInt((IntValue And 16777215) / 65536), CInt((IntValue And 65535) / 256), CInt(IntValue And 255)), Nothing)
				Else
					thisProperty.SetValue(pReport.Fields(pFieldValue.ReportFieldName), System.Drawing.Color.FromName(CStr(FieldValue)), Nothing)
				End If
			ElseIf thisProperty.PropertyType.UnderlyingSystemType.IsEnum Then
				Dim EnumObject As Object

				If IsNumeric(FieldValue) Then
					EnumObject = System.Enum.ToObject(thisProperty.PropertyType.UnderlyingSystemType, FieldValue)
				Else
					EnumObject = System.Enum.Parse(thisProperty.PropertyType.UnderlyingSystemType, FieldValue, True)
				End If

				thisProperty.SetValue(pReport.Fields(pFieldValue.ReportFieldName), EnumObject, Nothing)
			Else
				thisProperty.SetValue(pReport.Fields(pFieldValue.ReportFieldName), CDbl(FieldValue), Nothing)
			End If

		Catch ex As Exception
		End Try

	End Sub

#End Region

#Region " Generate Report(s) Code"

    ''' <summary>
    ''' RPTs the valuation.
    ''' </summary>
    ''' <param name="pFundID">The p fund ID.</param>
    ''' <param name="pLegalEntity">The p legal entity.</param>
    ''' <param name="pAggregateToParent">if set to <c>true</c> [p aggregate to parent].</param>
    ''' <param name="pValueDate">The p value date.</param>
    ''' <param name="pStatusGroupFilter">The p status group filter.</param>
    ''' <param name="pAdministratorDatesFilter">The p administrator dates filter.</param>
    ''' <param name="pStatusBar">The p status bar.</param>
	Public Sub rptValuation(ByVal pFundID As Integer, ByVal pLegalEntity As String, ByVal pAggregateToParent As Boolean, ByVal pValueDate As Date, ByVal pStatusGroupFilter As String, ByVal pAdministratorDatesFilter As Integer, ByRef pStatusBar As ToolStripProgressBar)

		Dim ReportData As DataTable
		Dim ValuationReport As C1Report

		Try

			ReportData = MainForm.MainReportHandler.GetData_FundValuation(pFundID, pLegalEntity, pAggregateToParent, pValueDate, Trim(pStatusGroupFilter), pAdministratorDatesFilter)

			ValuationReport = GetReportDefinition(pFundID, "rptFundValuation")

			If (Trim(pStatusGroupFilter).Length > 0) Then
				ValuationReport.Fields("Text_ReportTitle").Text = "Fund Valuation Report ('" & Trim(pStatusGroupFilter) & "' Transactions Group)."
			End If

			If (Not (ReportData Is Nothing)) Then

				Call MainForm.MainReportHandler.DisplayReport(ValuationReport, ReportData, pValueDate, MainForm.Main_Knowledgedate, pStatusBar)	'Form_ProgressBar)

			End If


		Catch ex As Exception

		End Try

	End Sub

    ''' <summary>
    ''' RPTs the profit and loss.
    ''' </summary>
    ''' <param name="pFundID">The p fund ID.</param>
    ''' <param name="pDateFrom">The p date from.</param>
    ''' <param name="pDateTo">The p date to.</param>
    ''' <param name="pStatusGroupFilter">The p status group filter.</param>
    ''' <param name="pAdministratorDatesFilter">The p administrator dates filter.</param>
    ''' <param name="pStartKnowledgeDate">The p start knowledge date.</param>
    ''' <param name="pEndKnowledgeDate">The p end knowledge date.</param>
    ''' <param name="pAggregateToParentID">if set to <c>true</c> [p aggregate to parent ID].</param>
    ''' <param name="pLookthroughPandL">if set to <c>true</c> [p lookthrough pand L].</param>
    ''' <param name="pSectorSummary">if set to <c>true</c> [p sector summary].</param>
    ''' <param name="pStatusBar">The p status bar.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	Public Function rptProfitAndLoss( _
	 ByVal pFundID As Integer, ByVal pDateFrom As Date, ByVal pDateTo As Date, _
	 ByVal pStatusGroupFilter As String, ByVal pAdministratorDatesFilter As Integer, _
	 ByVal pStartKnowledgeDate As Date, ByVal pEndKnowledgeDate As Date, _
	 ByVal pAggregateToParentID As Boolean, ByVal pLookthroughPandL As Boolean, _
	 ByVal pSectorSummary As Boolean, ByRef pStatusBar As ToolStripProgressBar) As Boolean
		' **************************************************************************************
		'
		'
		' **************************************************************************************

		Dim PnLReport As C1Report

		' Adjust Dates as necessary
		Try

			' From Date
			pDateFrom = pDateFrom.AddHours(-pDateFrom.Hour)
			pDateFrom = pDateFrom.AddMinutes(-pDateFrom.Minute)
			pDateFrom = pDateFrom.AddSeconds(-pDateFrom.Second)

			' To Date
			pDateTo = pDateTo.AddHours(-pDateTo.Hour)
			pDateTo = pDateTo.AddMinutes(-pDateTo.Minute)
			pDateTo = pDateTo.AddSeconds(-pDateTo.Second)

			' Knowledgedates

			' If the KnowledgeDate is Midnight (0 Seconds in the time) then assume that it means the 'Whole Day'
			' and thus add on a full day's worth of seconds.

			If (pStartKnowledgeDate > KNOWLEDGEDATE_NOW) AndAlso (pStartKnowledgeDate.TimeOfDay.TotalSeconds < 1) Then
				pStartKnowledgeDate = pStartKnowledgeDate.AddSeconds(LAST_SECOND - pStartKnowledgeDate.TimeOfDay.TotalSeconds)
			End If

			If (pEndKnowledgeDate > KNOWLEDGEDATE_NOW) AndAlso (pEndKnowledgeDate.TimeOfDay.TotalSeconds < 1) Then
				pEndKnowledgeDate = pEndKnowledgeDate.AddSeconds(LAST_SECOND - pEndKnowledgeDate.TimeOfDay.TotalSeconds)
			End If

		Catch ex As Exception

			Return False

		End Try

		' Configure Adaptor

		Dim rptAdaptor As New SqlDataAdapter
		Dim rptTable As New DataTable

		Try

			MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.GetVeniceConnection, rptAdaptor, "rptProfitAndLoss")
			If (rptAdaptor.SelectCommand.Connection Is Nothing) Then
				Exit Function
			End If

			rptAdaptor.SelectCommand.Parameters("@FundID").Value = pFundID
			rptAdaptor.SelectCommand.Parameters("@ValueDateFrom").Value = pDateFrom
			rptAdaptor.SelectCommand.Parameters("@ValueDateTo").Value = pDateTo
			rptAdaptor.SelectCommand.Parameters("@UnitPriceVariant").Value = DBNull.Value
			rptAdaptor.SelectCommand.Parameters("@StatusGroupFilter").Value = pStatusGroupFilter
			rptAdaptor.SelectCommand.Parameters("@AdministratorDatesFilter").Value = pAdministratorDatesFilter
			rptAdaptor.SelectCommand.Parameters("@DisregardWatermarkPoints").Value = DBNull.Value

			If (pLookthroughPandL) Then
				rptAdaptor.SelectCommand.Parameters("@IsLookthrough").Value = 1
			Else
				rptAdaptor.SelectCommand.Parameters("@IsLookthrough").Value = 0
			End If

			rptAdaptor.SelectCommand.Parameters("@MaintainConsistentFundPnLs").Value = DBNull.Value
			rptAdaptor.SelectCommand.Parameters("@KnowledgeDateFrom").Value = pStartKnowledgeDate
			rptAdaptor.SelectCommand.Parameters("@KnowledgeDateTo").Value = pEndKnowledgeDate
			rptAdaptor.SelectCommand.Parameters("@KnowledgeDateLinkedTables").Value = MainForm.Main_Knowledgedate

			If (pAggregateToParentID) Then
				rptAdaptor.SelectCommand.Parameters("@AggregateToInstrumentParent").Value = 2
			Else
				rptAdaptor.SelectCommand.Parameters("@AggregateToInstrumentParent").Value = 0
			End If

			rptAdaptor.SelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

			SyncLock rptAdaptor.SelectCommand.Connection
				rptAdaptor.Fill(rptTable)
			End SyncLock

		Catch ex As Exception

			Return False

		Finally
			Try
				If (Not (rptAdaptor.SelectCommand Is Nothing)) AndAlso (Not (rptAdaptor.SelectCommand.Connection Is Nothing)) Then
					rptAdaptor.SelectCommand.Connection.Close()
				End If
			Catch ex As Exception
			End Try

		End Try


		'   Add on Calculated Fields.

		rptTable.Columns.Add("PercentReturn", GetType(Double))
		rptTable.Columns.Add("Month1Return", GetType(Double))
		rptTable.Columns.Add("Month2Return", GetType(Double))
		rptTable.Columns.Add("Month3Return", GetType(Double))
		rptTable.Columns.Add("ThisYearReturn", GetType(Double))
		rptTable.Columns.Add("LastYearReturn", GetType(Double))
		rptTable.Columns.Add("PercentComplete", GetType(Double))
		rptTable.Columns.Add("FundValue", GetType(Double))
		rptTable.Columns.Add("NotActiveFlag", GetType(Integer))

		' Calculate fields
		Dim thisRow As DataRow
		Dim Month1Date As Date
		Dim Month2Date As Date
		Dim Month3Date As Date

		Month1Date = pDateTo.Date
		Month1Date = Month1Date.AddDays(0 - Month1Date.Day)
		Month2Date = Month1Date.AddDays(0 - Month1Date.Day)
		Month3Date = Month2Date.AddDays(0 - Month2Date.Day)

		For Each thisRow In rptTable.Rows

			' PercentReturn
			thisRow("PercentReturn") = 0
			Try
				If IsNumeric(thisRow("StartPrice")) AndAlso IsNumeric(thisRow("EndPrice")) Then
					If CDbl(thisRow("StartPrice")) <> 0 Then
						thisRow("PercentReturn") = (CDbl(thisRow("EndPrice")) / CDbl(thisRow("StartPrice"))) - 1
					End If
				End If
			Catch ex As Exception
			End Try

			' NotActiveFlag: (Abs([value1])<1) And (Abs([value2])<1) And (Abs([Profit])<1) And (Abs([FinalUnits])<1)
			thisRow("NotActiveFlag") = 0
			If (IsNumeric(thisRow("value1")) And IsNumeric(thisRow("value2")) And IsNumeric(thisRow("Profit")) And IsNumeric(thisRow("FinalUnits"))) Then
				If ((Abs(CDbl(thisRow("value1"))) < 1.0) And (Abs(CDbl(thisRow("value2"))) < 1.0) And (Abs(CDbl(thisRow("Profit"))) < 1.0) And (Abs(CDbl(thisRow("FinalUnits"))) < 1.0)) Then
					thisRow("NotActiveFlag") = (-1)
				End If
			End If

			' Monthly Return(s)
			' GetPertracReturn()
			Try

				' FundValue: IIf(([InstrumentTypeDescription]="Fund") Or ([InstrumentTypeDescription]="Option"),1,0)*[Value2]
				thisRow("FundValue") = 0
				If (CInt(thisRow("InstrumentType")) = InstrumentTypes.Fund) Or (CInt(thisRow("InstrumentType")) = InstrumentTypes.Option) Then
					thisRow("FundValue") = thisRow("Value2")
				End If


				' Percent Complete
				' ([EndPriceDate]-Get_LocalDate("rptPandL_FromDate"))/
				' (Get_LocalDate("rptPandL_ToDate") - 
				' Get_LocalDate("rptPandL_FromDate"))*[FundValue])
				Try
					If IsDate(thisRow("Endpricedate")) Then
						If (CDate(thisRow("Endpricedate")).CompareTo(pDateFrom) < 0) Then
							thisRow("PercentComplete") = 0
						ElseIf (CDate(thisRow("Endpricedate")).CompareTo(pDateFrom) >= 0) AndAlso (CDate(thisRow("Endpricedate")).CompareTo(pDateTo) < 0) Then
							thisRow("PercentComplete") = ((CDate(thisRow("Endpricedate")).ToOADate - pDateFrom.ToOADate) / (pDateTo.ToOADate - pDateFrom.ToOADate)) * CDbl(thisRow("FundValue"))
						Else
							' 100% complete.
							thisRow("PercentComplete") = thisRow("FundValue")
						End If
					End If
				Catch ex As Exception
					thisRow("PercentComplete") = 0
				End Try



				If (IsNumeric(thisRow("PertracCode"))) AndAlso (CInt(thisRow("PertracCode")) > 0) Then
					Try
						Dim ThisStat As StatFunctions.SeriesStatsClass

						' Use Stats End date of (Report Date - 1 Month) to avoid double counting the Report month if it is present in Pertrac.

						ThisStat = MainForm.StatFunctions.GetSimpleStats(MainForm.PertracData.GetPertracDataPeriod(CULng(thisRow("PertracCode"))), CULng(thisRow("PertracCode")), False, pDateTo.AddMonths(-1), 0, 1.0#)

						thisRow("Month1Return") = ThisStat.MonthReturns.P1
						thisRow("Month2Return") = ThisStat.MonthReturns.P2
						thisRow("Month3Return") = ThisStat.MonthReturns.P3
						If pDateTo.Month = 1 Then
							thisRow("ThisYearReturn") = CDbl(thisRow("PercentReturn"))
							thisRow("LastYearReturn") = ThisStat.YearReturns.P1
						Else
							thisRow("ThisYearReturn") = ThisStat.YearReturns.P1
							thisRow("ThisYearReturn") = ((1.0 + CDbl(thisRow("ThisYearReturn"))) * (1.0 + CDbl(thisRow("PercentReturn")))) - 1.0
							thisRow("LastYearReturn") = ThisStat.YearReturns.P2
						End If

					Catch ex As Exception
					End Try
				End If


			Catch ex As Exception
			End Try

		Next


		' Show report

		' Set Specific Report fields

		If (pSectorSummary) Then
			PnLReport = GetReportDefinition(pFundID, "rptProfitandLossSectorSummary")
		Else
			PnLReport = GetReportDefinition(pFundID, "rptProfitAndLoss")
		End If

		Try
			PnLReport.Fields("RptFromDate").Text = pDateFrom.ToString(DISPLAYMEMBER_DATEFORMAT)
			PnLReport.Fields("RptToDate").Text = pDateTo.ToString(DISPLAYMEMBER_DATEFORMAT)

			PnLReport.Fields("Text_Month1Name").Text = Month1Date.ToString("MMM")
			PnLReport.Fields("Text_Month2Name").Text = Month2Date.ToString("MMM")
			PnLReport.Fields("Text_Month3Name").Text = Month3Date.ToString("MMM")
			PnLReport.Fields("Text_ThisYearName").Text = pDateTo.Year.ToString
			PnLReport.Fields("Text_LastYearName").Text = pDateTo.AddYears(-1).Year.ToString
		Catch ex As Exception
		End Try

		' KD Details
		If (pStartKnowledgeDate <= KNOWLEDGEDATE_NOW) AndAlso (pEndKnowledgeDate <= KNOWLEDGEDATE_NOW) Then
			Try
				PnLReport.Fields("Text_KDDetails").Text = "Live"
			Catch ex As Exception
			End Try
		Else
			Dim TextFrom As String
			Dim TextTo As String

			Try
				If (pStartKnowledgeDate <= KNOWLEDGEDATE_NOW) Then
					TextFrom = "Live"
				Else
					TextFrom = pStartKnowledgeDate.ToString(DISPLAYMEMBER_LONGDATEFORMAT)
				End If

				If (pEndKnowledgeDate <= KNOWLEDGEDATE_NOW) Then
					TextTo = "Live"
				Else
					TextTo = pEndKnowledgeDate.ToString(DISPLAYMEMBER_LONGDATEFORMAT)
				End If

				PnLReport.Fields("Text_KDDetails").Text = TextFrom & " to " & TextTo
			Catch ex As Exception
			End Try
		End If

		' Display report

		Try
			Dim RptDataView As DataView

			RptDataView = New DataView(rptTable, "NotActiveFlag=0", "", DataViewRowState.CurrentRows)

			PnLReport.DataSource.Recordset = RptDataView

			Me.DisplayReport(PnLReport, pStatusBar)

			SaveReportData(RptDataView, PnLReport.ReportName)

		Catch ex As Exception
		End Try

		Return True

	End Function


    ''' <summary>
    ''' RPTs the asset allocation report.
    ''' </summary>
    ''' <param name="pFundID">The p fund ID.</param>
    ''' <param name="pDateFrom">The p date from.</param>
    ''' <param name="pDateTo">The p date to.</param>
    ''' <param name="pStatusGroupFilter">The p status group filter.</param>
    ''' <param name="pAdministratorDatesFilter">The p administrator dates filter.</param>
    ''' <param name="pStartKnowledgeDate">The p start knowledge date.</param>
    ''' <param name="pEndKnowledgeDate">The p end knowledge date.</param>
    ''' <param name="pAggregateToParentID">if set to <c>true</c> [p aggregate to parent ID].</param>
    ''' <param name="pLookthroughPandL">if set to <c>true</c> [p lookthrough pand L].</param>
    ''' <param name="pSectorSummary">if set to <c>true</c> [p sector summary].</param>
    ''' <param name="pOuterGroupingType">Type of the p outer grouping.</param>
    ''' <param name="pOuterGroupingSelected">The p outer grouping selected.</param>
    ''' <param name="DontShowOthers">if set to <c>true</c> [dont show others].</param>
    ''' <param name="pStatusBar">The p status bar.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	Public Function rptAssetAllocationReport( _
	 ByVal pFundID As Integer, ByVal pDateFrom As Date, ByVal pDateTo As Date, _
	 ByVal pStatusGroupFilter As String, ByVal pAdministratorDatesFilter As Integer, _
	 ByVal pStartKnowledgeDate As Date, ByVal pEndKnowledgeDate As Date, _
	 ByVal pAggregateToParentID As Boolean, ByVal pLookthroughPandL As Boolean, _
	 ByVal pSectorSummary As Boolean, ByVal pOuterGroupingType As Integer, ByVal pOuterGroupingSelected As String, ByRef DontShowOthers As Boolean, ByRef pStatusBar As ToolStripProgressBar) As Boolean

		' **************************************************************************************
		'
		' Initially designed for the FCEV Board. NPP Feb 2008.
		'
		' **************************************************************************************

		Dim PnLReport As C1Report

		' Adjust Dates as necessary
		Try

			' From Date
			pDateFrom = pDateFrom.AddHours(-pDateFrom.Hour)
			pDateFrom = pDateFrom.AddMinutes(-pDateFrom.Minute)
			pDateFrom = pDateFrom.AddSeconds(-pDateFrom.Second)

			' To Date
			pDateTo = pDateTo.AddHours(-pDateTo.Hour)
			pDateTo = pDateTo.AddMinutes(-pDateTo.Minute)
			pDateTo = pDateTo.AddSeconds(-pDateTo.Second)

			' Knowledgedates

			' If the KnowledgeDate is Midnight (0 Seconds in the time) then assume that it means the 'Whole Day'
			' and thus add on a full day's worth of seconds.

			If (pStartKnowledgeDate > KNOWLEDGEDATE_NOW) AndAlso (pStartKnowledgeDate.TimeOfDay.TotalSeconds < 1) Then
				pStartKnowledgeDate = pStartKnowledgeDate.AddSeconds(LAST_SECOND - pStartKnowledgeDate.TimeOfDay.TotalSeconds)
			End If

			If (pEndKnowledgeDate > KNOWLEDGEDATE_NOW) AndAlso (pEndKnowledgeDate.TimeOfDay.TotalSeconds < 1) Then
				pEndKnowledgeDate = pEndKnowledgeDate.AddSeconds(LAST_SECOND - pEndKnowledgeDate.TimeOfDay.TotalSeconds)
			End If

		Catch ex As Exception

			Return False

		End Try

		' Configure Adaptor

		Dim rptAdaptor As New SqlDataAdapter
		Dim rptTable As New DataTable

		Try

			MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.GetVeniceConnection, rptAdaptor, "rptProfitAndLoss")
			If (rptAdaptor.SelectCommand.Connection Is Nothing) Then
				Exit Function
			End If

			rptAdaptor.SelectCommand.Parameters("@FundID").Value = pFundID
			rptAdaptor.SelectCommand.Parameters("@ValueDateFrom").Value = pDateFrom
			rptAdaptor.SelectCommand.Parameters("@ValueDateTo").Value = pDateTo
			rptAdaptor.SelectCommand.Parameters("@UnitPriceVariant").Value = DBNull.Value

			rptAdaptor.SelectCommand.Parameters("@StatusGroupFilter").Value = pStatusGroupFilter
			rptAdaptor.SelectCommand.Parameters("@AdministratorDatesFilter").Value = pAdministratorDatesFilter

			rptAdaptor.SelectCommand.Parameters("@DisregardWatermarkPoints").Value = DBNull.Value

			If (pLookthroughPandL) Then
				rptAdaptor.SelectCommand.Parameters("@IsLookthrough").Value = 1
			Else
				rptAdaptor.SelectCommand.Parameters("@IsLookthrough").Value = 0
			End If

			rptAdaptor.SelectCommand.Parameters("@MaintainConsistentFundPnLs").Value = DBNull.Value
			rptAdaptor.SelectCommand.Parameters("@KnowledgeDateFrom").Value = pStartKnowledgeDate
			rptAdaptor.SelectCommand.Parameters("@KnowledgeDateTo").Value = pEndKnowledgeDate
			rptAdaptor.SelectCommand.Parameters("@KnowledgeDateLinkedTables").Value = MainForm.Main_Knowledgedate

			If (pAggregateToParentID) Then
				rptAdaptor.SelectCommand.Parameters("@AggregateToInstrumentParent").Value = 2
			Else
				rptAdaptor.SelectCommand.Parameters("@AggregateToInstrumentParent").Value = 0
			End If

			rptAdaptor.SelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

			SyncLock rptAdaptor.SelectCommand.Connection
				rptAdaptor.Fill(rptTable)
			End SyncLock

		Catch ex As Exception

			Return False

		Finally
			Try
				If (Not (rptAdaptor.SelectCommand Is Nothing)) AndAlso (Not (rptAdaptor.SelectCommand.Connection Is Nothing)) Then
					rptAdaptor.SelectCommand.Connection.Close()
				End If
			Catch ex As Exception
			End Try

		End Try


		'   Add on Calculated Fields.

		rptTable.Columns.Add("PercentReturn", GetType(Double))
		'rptTable.Columns.Add("ThisYearReturn", GetType(Double))
		'rptTable.Columns.Add("LastYearReturn", GetType(Double))
		rptTable.Columns.Add("PercentComplete", GetType(Double))
		rptTable.Columns.Add("FundValueTotalStart", GetType(Double))
		rptTable.Columns.Add("FundValueTotalEnd", GetType(Double))
		rptTable.Columns.Add("NotActiveFlag", GetType(Integer))
		rptTable.Columns.Add("FundTypeAttributionGrouping", GetType(String))

		' Calculate fields
		Dim thisRow As DataRow
		Dim GroupingDictionary As New Dictionary(Of Integer, String)
		Dim FundStartValuation As Double = 0
		Dim FundEndValuation As Double = 0

		Dim Value1Ordinal As Integer = rptTable.Columns.IndexOf("Value1")
		Dim Value2Ordinal As Integer = rptTable.Columns.IndexOf("Value2")
		Dim FundValueTotalStartOrdinal As Integer = rptTable.Columns.IndexOf("FundValueTotalStart")
		Dim FundValueTotalEndOrdinal As Integer = rptTable.Columns.IndexOf("FundValueTotalEnd")
		Dim FundTypeAttributionGroupingOrdinal As Integer = rptTable.Columns.IndexOf("FundTypeAttributionGrouping")
		Dim InstrumentFundTypeOrdinal As Integer = rptTable.Columns.IndexOf("InstrumentFundType")		' InstrumentFundType

		' Build FundTypeAttributionGrouping Dictionary

		Try
			Dim ThisFundTypeDS As RenaissanceDataClass.DSFundType = Nothing
			Dim ThisFundTypeTbl As RenaissanceDataClass.DSFundType.tblFundTypeDataTable = Nothing
			Dim ThisFundType As RenaissanceDataClass.DSFundType.tblFundTypeRow

			ThisFundTypeDS = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblFundType, False), RenaissanceDataClass.DSFundType)
			If (ThisFundTypeDS IsNot Nothing) Then
				ThisFundTypeTbl = ThisFundTypeDS.tblFundType
			End If
			If (ThisFundTypeTbl IsNot Nothing) AndAlso (ThisFundTypeTbl.Rows.Count > 0) Then
				For Each ThisFundType In ThisFundTypeTbl.Rows
					GroupingDictionary.Add(ThisFundType.FundTypeID, ThisFundType.FundTypeAttributionGrouping)
				Next
			End If
		Catch ex As Exception
		End Try

		For Each thisRow In rptTable.Rows

			' PercentReturn
			thisRow("PercentReturn") = 0
			Try
				If IsNumeric(thisRow("StartPrice")) AndAlso IsNumeric(thisRow("EndPrice")) Then
					If CDbl(thisRow("StartPrice")) <> 0 Then
						thisRow("PercentReturn") = (CDbl(thisRow("EndPrice")) / CDbl(thisRow("StartPrice"))) - 1
					End If
				End If
			Catch ex As Exception
			End Try

			' NotActiveFlag: (Abs([value1])<1) And (Abs([value2])<1) And (Abs([Profit])<1) And (Abs([FinalUnits])<1)
			thisRow("NotActiveFlag") = 0
			If (IsNumeric(thisRow("value1")) And IsNumeric(thisRow("value2")) And IsNumeric(thisRow("Profit")) And IsNumeric(thisRow("FinalUnits"))) Then
				If ((Abs(CDbl(thisRow("value1"))) < 1.0) And (Abs(CDbl(thisRow("value2"))) < 1.0) And (Abs(CDbl(thisRow("Profit"))) < 1.0) And (Abs(CDbl(thisRow("FinalUnits"))) < 1.0)) Then
					thisRow("NotActiveFlag") = (-1)
				End If
			End If

			' Monthly Return(s)
			' GetPertracReturn()
			Try

				' FundTypeAttributionGrouping
				' GroupingDictionary

				thisRow(FundTypeAttributionGroupingOrdinal) = GroupingDictionary(CInt(thisRow(InstrumentFundTypeOrdinal)))

				' Total Fund Values

				FundStartValuation += CDbl(thisRow(Value1Ordinal))
				FundEndValuation += CDbl(thisRow(Value2Ordinal))

			Catch ex As Exception
			End Try

		Next

		' Update Fund Start / End Valuations

		For Each thisRow In rptTable.Rows
			Try

				thisRow(FundValueTotalStartOrdinal) = FundStartValuation
				thisRow(FundValueTotalEndOrdinal) = FundEndValuation

			Catch ex As Exception
			End Try
		Next

		' Show report

		' Set Specific Report fields

		If (pSectorSummary) Then
			PnLReport = GetReportDefinition(pFundID, "rptAssetAllocationSummary")
		Else
			PnLReport = GetReportDefinition(pFundID, "rptAssetAllocation")
		End If

		Try
			PnLReport.Fields("RptFromDate").Text = pDateFrom.ToString(DISPLAYMEMBER_DATEFORMAT)
			PnLReport.Fields("RptToDate").Text = pDateTo.ToString(DISPLAYMEMBER_DATEFORMAT)
		Catch ex As Exception
		End Try

		Try
			Select Case pOuterGroupingType ' Me.Combo_SelectedReportGroup.SelectedIndex

				Case 0
					'No additional Grouping

				Case 1, 2
					'Group by Fund Type Attribution Grouping

					' Update report group to reflect Attribution Grouping

					Try
						PnLReport.Groups("OuterGroup").GroupBy = "FundTypeAttributionGrouping"
						PnLReport.Groups("OuterGroup").SectionHeader.Visible = True
						PnLReport.Groups("OuterGroup").SectionFooter.Visible = True
						PnLReport.Fields("Text_OuterGroupField").Text = "FundTypeAttributionGrouping"
						PnLReport.Fields("Text_OuterGroupName").Text = "Attribution Group : "
					Catch ex As Exception
					End Try

					If (pOuterGroupingType = 2) Then 'Group by Selected fund Type Group
						Dim theseRows() As DataRow

						Try
							theseRows = rptTable.Select("FundTypeAttributionGrouping<>'" & pOuterGroupingSelected & "'")

							For Each thisRow In theseRows
								thisRow("FundTypeAttributionGrouping") = "Other Groups"

								If DontShowOthers Then
									rptTable.Rows.Remove(thisRow)
								End If
							Next
						Catch ex As Exception
						End Try
					End If

				Case 3, 4
					'Group by Selected Instrument Type

					' Update report group to reflect Attribution Grouping

					Try
						PnLReport.Groups("OuterGroup").GroupBy = "InstrumentTypeDescription"
						PnLReport.Groups("OuterGroup").SectionHeader.Visible = True
						PnLReport.Groups("OuterGroup").SectionFooter.Visible = True
						PnLReport.Fields("Text_OuterGroupField").Text = "InstrumentTypeDescription"
						PnLReport.Fields("Text_OuterGroupName").Text = "Instrument Type : "
					Catch ex As Exception
					End Try

					If (pOuterGroupingType = 4) Then 'Group by Selected fund Type Group
						Dim theseRows() As DataRow

						Try
							theseRows = rptTable.Select("InstrumentType<>" & pOuterGroupingSelected & "")

							If DontShowOthers Then
								For Each thisRow In theseRows
									thisRow("InstrumentTypeDescription") = "Other Types"
									rptTable.Rows.Remove(thisRow)
								Next
							Else
								For Each thisRow In theseRows
									thisRow("InstrumentTypeDescription") = "Other Types"
								Next
							End If

						Catch ex As Exception
						End Try
					End If

				Case Else

			End Select



		Catch ex As Exception

		End Try


		' KD Details
		If (pStartKnowledgeDate <= KNOWLEDGEDATE_NOW) AndAlso (pEndKnowledgeDate <= KNOWLEDGEDATE_NOW) Then
			Try
				PnLReport.Fields("Text_KDDetails").Text = "Live"
			Catch ex As Exception
			End Try
		Else
			Dim TextFrom As String
			Dim TextTo As String

			Try
				If (pStartKnowledgeDate <= KNOWLEDGEDATE_NOW) Then
					TextFrom = "Live"
				Else
					TextFrom = pStartKnowledgeDate.ToString(DISPLAYMEMBER_LONGDATEFORMAT)
				End If

				If (pEndKnowledgeDate <= KNOWLEDGEDATE_NOW) Then
					TextTo = "Live"
				Else
					TextTo = pEndKnowledgeDate.ToString(DISPLAYMEMBER_LONGDATEFORMAT)
				End If

				PnLReport.Fields("Text_KDDetails").Text = TextFrom & " to " & TextTo
			Catch ex As Exception
			End Try
		End If

		' Display report

		Try
			Dim RptDataView As DataView

			RptDataView = New DataView(rptTable, "NotActiveFlag=0", "", DataViewRowState.CurrentRows)

			PnLReport.DataSource.Recordset = RptDataView

			Me.DisplayReport(PnLReport, pStatusBar)

			SaveReportData(RptDataView, PnLReport.ReportName)

		Catch ex As Exception
		End Try

		Return True

	End Function


    ''' <summary>
    ''' RPTs the attribution.
    ''' </summary>
    ''' <param name="pReportName">Name of the p report.</param>
    ''' <param name="pFundID">The p fund ID.</param>
    ''' <param name="AttributionPeriod">The attribution period.</param>
    ''' <param name="pLegalEntity">The p legal entity.</param>
    ''' <param name="pAttributionYear">The p attribution year.</param>
    ''' <param name="pAttributionMonth">The p attribution month.</param>
    ''' <param name="pMaxAttributionDate">The p max attribution date.</param>
    ''' <param name="pRecordPreference">The p record preference.</param>
    ''' <param name="pLookthroughAttributions">if set to <c>true</c> [p lookthrough attributions].</param>
    ''' <param name="pIncludeAttributionTotals">if set to <c>true</c> [p include attribution totals].</param>
    ''' <param name="pAggregateToInstrumentParentID">if set to <c>true</c> [p aggregate to instrument parent ID].</param>
    ''' <param name="pOuterGroupingType">Type of the p outer grouping.</param>
    ''' <param name="pOuterGroupingSelected">The p outer grouping selected.</param>
    ''' <param name="DontShowOthers">if set to <c>true</c> [dont show others].</param>
    ''' <param name="pShowBudget">if set to <c>true</c> [p show budget].</param>
    ''' <param name="pStatusBar">The p status bar.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	Public Function rptAttribution( _
	ByVal pReportName As String, ByVal pFundID As Integer, ByVal AttributionPeriod As RenaissanceGlobals.DealingPeriod, ByVal pLegalEntity As String, ByVal pAttributionYear As Integer, ByVal pAttributionMonth As Date, ByVal pMaxAttributionDate As Date, ByVal pRecordPreference As Attribution_RecordPreference, ByVal pLookthroughAttributions As Boolean, ByVal pIncludeAttributionTotals As Boolean, ByVal pAggregateToInstrumentParentID As Boolean, ByVal pOuterGroupingType As Integer, ByVal pOuterGroupingSelected As String, ByRef DontShowOthers As Boolean, ByVal pShowBudget As Boolean, ByRef pStatusBar As ToolStripProgressBar) As Boolean
		' **************************************************************************************
		'
		'
		' **************************************************************************************

		' Configure Adaptor

		Dim rptAdaptor As New SqlDataAdapter
		Dim rptTable As New DataTable
		Dim thisRow As DataRow

		Try

			MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.GetVeniceConnection, rptAdaptor, pReportName)
			If (rptAdaptor.SelectCommand.Connection Is Nothing) Then
				MainForm.LogError("ReportHandler, rptAttribution()", LOG_LEVELS.Warning, "", "Error getting DB Connection while running report : " & pReportName & " data.", "", True)
				Exit Function
			End If

			rptAdaptor.SelectCommand.Parameters("@FundID").Value = pFundID
			rptAdaptor.SelectCommand.Parameters("@AttribPeriod").Value = CInt(AttributionPeriod)
			rptAdaptor.SelectCommand.Parameters("@LegalEntity").Value = pLegalEntity
			rptAdaptor.SelectCommand.Parameters("@AttributionYear").Value = pAttributionYear
			rptAdaptor.SelectCommand.Parameters("@MaxAttributionDate").Value = pMaxAttributionDate
			rptAdaptor.SelectCommand.Parameters("@RecordPreference").Value = pRecordPreference
			If (pLookthroughAttributions) Then
				rptAdaptor.SelectCommand.Parameters("@LookthroughAttributions").Value = 1
			Else
				rptAdaptor.SelectCommand.Parameters("@LookthroughAttributions").Value = 0
			End If
			If (pIncludeAttributionTotals) Then
				rptAdaptor.SelectCommand.Parameters("@IncludeAttributionTotals").Value = 1
			Else
				rptAdaptor.SelectCommand.Parameters("@IncludeAttributionTotals").Value = 0
			End If
			If (pAggregateToInstrumentParentID) Then
				rptAdaptor.SelectCommand.Parameters("@AggregateToInstrumentParentID").Value = 1
			Else
				rptAdaptor.SelectCommand.Parameters("@AggregateToInstrumentParentID").Value = 0
			End If
			rptAdaptor.SelectCommand.Parameters("@KnowledgeDateLinkedTables").Value = MainForm.Main_Knowledgedate

			rptAdaptor.SelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

			SyncLock rptAdaptor.SelectCommand.Connection
        MainForm.LoadTable_Custom(rptTable, rptAdaptor.SelectCommand)
        'rptTable.Load(rptAdaptor.SelectCommand.ExecuteReader)
        ' rptAdaptor.Fill(rptTable)
      End SyncLock

      If rptTable.Columns.Contains("AttribBips") Then
        For Each thisRow In rptTable.Rows
          If CDbl(thisRow("AttribBips")) = 0 Then
            thisRow("AttribBips") = 0.000001
          End If
        Next
      End If

      If rptTable.Columns.Contains("AttribProfit") Then
        For Each thisRow In rptTable.Rows
          If CDbl(thisRow("AttribProfit")) = 0 Then
            thisRow("AttribProfit") = 0.0001
          End If
        Next
      End If

    Catch ex As Exception
      MainForm.LogError("ReportHandler, rptAttribution()", LOG_LEVELS.Error, ex.Message, "Error getting " & pReportName & " data.", ex.StackTrace, True)
      Return False

    Finally
      ' Close Connection
      Try
        If (Not (rptAdaptor.SelectCommand Is Nothing)) AndAlso (Not (rptAdaptor.SelectCommand.Connection Is Nothing)) Then
          rptAdaptor.SelectCommand.Connection.Close()
        End If
      Catch ex As Exception
      End Try
    End Try

    ' Show report

    Dim thisReport As C1Report

    ' Get report definition.

    thisReport = GetReportDefinition(0, pReportName, pReportName)

    If (thisReport Is Nothing) Then
      MainForm.LogError("ReportHandler, rptAttribution()", LOG_LEVELS.Warning, "", "Failed to get report definition for '" & pReportName & "'", "", True)
      Return False
      Exit Function
    End If

    ' Adjust report definition for outer grouping

    ' pOuterGroupingType As Integer
    ' pOuterGroupingSelected as string 

    Select Case pOuterGroupingType ' Me.Combo_SelectedReportGroup.SelectedIndex

      Case 0
        'No additional Grouping

      Case 1, 2
        'Group by Fund Type Attribution Grouping

        ' Update report group to reflect Attribution Grouping

        Try
          thisReport.Groups("OuterGroup").GroupBy = "FundTypeAttributionGrouping"
          thisReport.Groups("OuterGroup").SectionHeader.Visible = True
          thisReport.Groups("OuterGroup").SectionFooter.Visible = True
          thisReport.Fields("Text_OuterGroupField").Text = "FundTypeAttributionGrouping"
          thisReport.Fields("Text_OuterGroupName").Text = "Attribution Group : "
        Catch ex As Exception
        End Try

        If (pOuterGroupingType = 2) Then 'Group by Selected fund Type Group
          Dim theseRows() As DataRow
          Try
            theseRows = rptTable.Select("FundTypeAttributionGrouping<>'" & pOuterGroupingSelected & "'")

            If DontShowOthers Then
              For Each thisRow In theseRows
                thisRow("FundTypeAttributionGrouping") = "Other Groups"
                rptTable.Rows.Remove(thisRow)
              Next
            Else
              For Each thisRow In theseRows
                thisRow("FundTypeAttributionGrouping") = "Other Groups"
              Next
            End If
          Catch ex As Exception
          End Try
        End If

      Case 3, 4
        'Group by Selected Instrument Type

        ' Update report group to reflect Attribution Grouping

        Try
          thisReport.Groups("OuterGroup").GroupBy = "InstrumentTypeDescription"
          thisReport.Groups("OuterGroup").SectionHeader.Visible = True
          thisReport.Groups("OuterGroup").SectionFooter.Visible = True
          thisReport.Fields("Text_OuterGroupField").Text = "InstrumentTypeDescription"
          thisReport.Fields("Text_OuterGroupName").Text = "Instrument Type : "
        Catch ex As Exception
        End Try

        If (pOuterGroupingType = 4) Then 'Group by Selected fund Type Group
          Dim theseRows() As DataRow

          Try
            theseRows = rptTable.Select("InstrumentType<>" & pOuterGroupingSelected & "")

            If DontShowOthers Then
              For Each thisRow In theseRows
                thisRow("InstrumentTypeDescription") = "Other Types"
                rptTable.Rows.Remove(thisRow)
              Next
            Else
              For Each thisRow In theseRows
                thisRow("InstrumentTypeDescription") = "Other Types"
              Next
            End If

          Catch ex As Exception
          End Try
        End If

      Case Else

    End Select

    ' Budget Column ??

    SetFieldValue(thisReport, New classReportFieldValue("Label_Budget", pShowBudget.ToString, "Visible"))
    SetFieldValue(thisReport, New classReportFieldValue("Text_Budget", pShowBudget.ToString, "Visible"))
    SetFieldValue(thisReport, New classReportFieldValue("Text_SectorBudget", pShowBudget.ToString, "Visible"))
    SetFieldValue(thisReport, New classReportFieldValue("Text_GroupBudget", pShowBudget.ToString, "Visible"))
    SetFieldValue(thisReport, New classReportFieldValue("Text_FundBudget", pShowBudget.ToString, "Visible"))

    ' Set InstrumentsDescription appropriately
    ' In order to get the Attribution items sorted Alphabetically the Report now sorts by Instrument Desctiption
    ' When Aggregating to parent Instrument, the original instrument descriptions are still passed through.


    ' Display report

    If (pReportName = "rptPnLDetailReport") Then
      Dim ReportData As New DataView(rptTable)
      ReportData.RowFilter = ("AttribDate='" & pAttributionMonth.ToString(QUERY_SHORTDATEFORMAT) & "'")

      Try
        thisReport.Fields("Text_AttributionDate").Text = pAttributionMonth.ToString(REPORT_DATEFORMAT)
      Catch ex As Exception
      End Try

      Call DisplayReport(thisReport, ReportData, Now(), MainForm.Main_Knowledgedate, pStatusBar)
    Else
      Call DisplayReport(thisReport, rptTable, Now(), MainForm.Main_Knowledgedate, pStatusBar)
    End If

    Return True

  End Function

  ''' <summary>
  ''' RPTs the attribution fund performance.
  ''' </summary>
  ''' <param name="pReportName">Name of the p report.</param>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="AttributionPeriod">The attribution period.</param>
  ''' <param name="pLegalEntity">The p legal entity.</param>
  ''' <param name="pAttributionYear">The p attribution year.</param>
  ''' <param name="pAttributionMonth">The p attribution month.</param>
  ''' <param name="pMaxAttributionDate">The p max attribution date.</param>
  ''' <param name="pRecordPreference">The p record preference.</param>
  ''' <param name="pLookthroughAttributions">if set to <c>true</c> [p lookthrough attributions].</param>
  ''' <param name="pIncludeAttributionTotals">if set to <c>true</c> [p include attribution totals].</param>
  ''' <param name="pAggregateToInstrumentParentID">if set to <c>true</c> [p aggregate to instrument parent ID].</param>
  ''' <param name="pOuterGroupingType">Type of the p outer grouping.</param>
  ''' <param name="pOuterGroupingSelected">The p outer grouping selected.</param>
  ''' <param name="DontShowOthers">if set to <c>true</c> [dont show others].</param>
  ''' <param name="pStatusBar">The p status bar.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function rptAttributionFundPerformance(ByVal pReportName As String, ByVal pFundID As Integer, ByVal AttributionPeriod As RenaissanceGlobals.DealingPeriod, ByVal pLegalEntity As String, ByVal pAttributionYear As Integer, ByVal pAttributionMonth As Date, ByVal pMaxAttributionDate As Date, ByVal pRecordPreference As Attribution_RecordPreference, ByVal pLookthroughAttributions As Boolean, ByVal pIncludeAttributionTotals As Boolean, ByVal pAggregateToInstrumentParentID As Boolean, ByVal pOuterGroupingType As Integer, ByVal pOuterGroupingSelected As String, ByRef DontShowOthers As Boolean, ByRef pStatusBar As ToolStripProgressBar) As Boolean
    ' **************************************************************************************
    '
    '
    ' **************************************************************************************

    ' Configure Adaptor

    Dim rptAdaptor As New SqlDataAdapter
    Dim InitialDataTable As New DataTable
    Dim thisDataRow As DataRow

    Try
      ' rptFundPerformanceReport
      ' spu_ViewAttributions_AggregatedInfo

      MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.GetVeniceConnection, rptAdaptor, pReportName)
      If (rptAdaptor.SelectCommand.Connection Is Nothing) Then
        MainForm.LogError("ReportHandler, rptAttribution()", LOG_LEVELS.Warning, "", "Error getting DB Connection while running report : " & pReportName & " data.", "", True)
        Exit Function
      End If

      rptAdaptor.SelectCommand.Parameters("@FundID").Value = pFundID
      rptAdaptor.SelectCommand.Parameters("@AttribPeriod").Value = CInt(AttributionPeriod)
      rptAdaptor.SelectCommand.Parameters("@LegalEntity").Value = pLegalEntity
      rptAdaptor.SelectCommand.Parameters("@AttributionYear").Value = pAttributionYear
      rptAdaptor.SelectCommand.Parameters("@MaxAttributionDate").Value = pMaxAttributionDate
      rptAdaptor.SelectCommand.Parameters("@RecordPreference").Value = pRecordPreference
      If (pLookthroughAttributions) Then
        rptAdaptor.SelectCommand.Parameters("@LookthroughAttributions").Value = 1
      Else
        rptAdaptor.SelectCommand.Parameters("@LookthroughAttributions").Value = 0
      End If
      If (pIncludeAttributionTotals) Then
        rptAdaptor.SelectCommand.Parameters("@IncludeAttributionTotals").Value = 1
      Else
        rptAdaptor.SelectCommand.Parameters("@IncludeAttributionTotals").Value = 0
      End If
      If (pAggregateToInstrumentParentID) Then
        rptAdaptor.SelectCommand.Parameters("@AggregateToInstrumentParentID").Value = 1
      Else
        rptAdaptor.SelectCommand.Parameters("@AggregateToInstrumentParentID").Value = 0
      End If
      rptAdaptor.SelectCommand.Parameters("@KnowledgeDateLinkedTables").Value = MainForm.Main_Knowledgedate

      rptAdaptor.SelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      SyncLock rptAdaptor.SelectCommand.Connection
        MainForm.LoadTable_Custom(InitialDataTable, rptAdaptor.SelectCommand)
        ' InitialDataTable.Load(rptAdaptor.SelectCommand.ExecuteReader)
        ' rptAdaptor.Fill(rptTable)
      End SyncLock

    Catch ex As Exception
      MainForm.LogError("ReportHandler, rptAttribution()", LOG_LEVELS.Error, ex.Message, "Error getting " & pReportName & " data.", ex.StackTrace, True)
      Return False

    Finally
      ' Close Connection
      Try
        If (Not (rptAdaptor.SelectCommand Is Nothing)) AndAlso (Not (rptAdaptor.SelectCommand.Connection Is Nothing)) Then
          rptAdaptor.SelectCommand.Connection.Close()
        End If
      Catch ex As Exception
      End Try
    End Try


    ' Show report

    Dim thisReport As C1Report

    ' Get report definition.

    thisReport = GetReportDefinition(0, pReportName, pReportName)

    If (thisReport Is Nothing) Then
      MainForm.LogError("ReportHandler, rptAttribution()", LOG_LEVELS.Warning, "", "Failed to get report definition for '" & pReportName & "'", "", True)
      Return False
      Exit Function
    End If

    ' Adjust report definition for outer grouping

    ' pOuterGroupingType As Integer
    ' pOuterGroupingSelected as string 

    Select Case pOuterGroupingType ' Me.Combo_SelectedReportGroup.SelectedIndex

      Case 0
        'No additional Grouping

      Case 1, 2
        'Group by Fund Type Attribution Grouping

        ' Update report group to reflect Attribution Grouping

        Try
          thisReport.Groups("OuterGroup").GroupBy = "FundTypeAttributionGrouping"
          thisReport.Groups("OuterGroup").SectionHeader.Visible = True
          thisReport.Groups("OuterGroup").SectionFooter.Visible = True
          thisReport.Fields("Text_OuterGroupField").Text = "FundTypeAttributionGrouping"
          thisReport.Fields("Text_OuterGroupName").Text = "Attribution Group : "
        Catch ex As Exception
        End Try

        If (pOuterGroupingType = 2) Then 'Group by Selected fund Type Group
          Dim theseRows() As DataRow
          Try
            theseRows = InitialDataTable.Select("FundTypeAttributionGrouping<>'" & pOuterGroupingSelected & "'")

            For Each thisDataRow In theseRows
              If DontShowOthers Then
                InitialDataTable.Rows.Remove(thisDataRow)
              Else
                thisDataRow("FundTypeAttributionGrouping") = "Other Groups"
              End If
            Next

          Catch ex As Exception
          End Try
        End If

      Case 3, 4
        'Group by Selected Instrument Type

        ' Update report group to reflect Attribution Grouping

        Try
          thisReport.Groups("OuterGroup").GroupBy = "InstrumentTypeDescription"
          thisReport.Groups("OuterGroup").SectionHeader.Visible = True
          thisReport.Groups("OuterGroup").SectionFooter.Visible = True
          thisReport.Fields("Text_OuterGroupField").Text = "InstrumentTypeDescription"
          thisReport.Fields("Text_OuterGroupName").Text = "Instrument Type : "
        Catch ex As Exception
        End Try

        If (pOuterGroupingType = 4) Then 'Group by Selected fund Type Group
          Dim theseRows() As DataRow

          Try
            If (InitialDataTable.Columns.Contains("InstrumentType") AndAlso InitialDataTable.Columns.Contains("InstrumentTypeDescription")) Then
              theseRows = InitialDataTable.Select("InstrumentType<>" & pOuterGroupingSelected)

              For Each thisDataRow In theseRows
                If DontShowOthers Then
                  InitialDataTable.Rows.Remove(thisDataRow)
                Else
                  thisDataRow("InstrumentTypeDescription") = "Other Types"
                End If
              Next
            End If

          Catch ex As Exception
          End Try
        End If

      Case Else

    End Select

    ' Add In Supplimental Fields...

    Dim ReportTable As New dsFundPerformanceReport.tblFundPerformanceReportDataTable
    Dim ReportRow As dsFundPerformanceReport.tblFundPerformanceReportRow
    Dim ThisInstrumentID As Integer
    Dim ThisPertracID As Integer
    Dim StatsID As ULong
    Dim FundStats As StatFunctions.SeriesStatsClass = Nothing
    Dim ThisDrawdowns() As StatFunctions.DrawDownInstanceClass
    Dim InformationArray() As Object

    For Each thisDataRow In InitialDataTable.Rows
      If (thisDataRow.RowState And DataRowState.Deleted) <> DataRowState.Deleted Then

        ThisInstrumentID = CInt(thisDataRow("AttribInstrument"))

        ReportRow = ReportTable.NewtblFundPerformanceReportRow

        ReportRow.AttribFund = CInt(thisDataRow("AttribFund")).ToString
        ReportRow.FundTypeID = CInt(thisDataRow("FundTypeID"))
        ReportRow.AttribInstrument = ThisInstrumentID
        ReportRow.FundName = CStr(thisDataRow("FundName"))
        ReportRow.FundTypeDescription = CStr(thisDataRow("FundTypeDescription"))
        ReportRow.FundTypeAttributionGrouping = CStr(thisDataRow("FundTypeAttributionGrouping"))
        ReportRow.InstrumentDescription = CStr(thisDataRow("InstrumentDescription"))

        Try
          ThisPertracID = CInt(Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, ThisInstrumentID, "InstrumentPertracCode"), 0))
        Catch ex As Exception
          ThisPertracID = 0
        End Try
        If (ThisPertracID = 0) Then
          StatsID = MainForm.StatFunctions.CombinedStatsID(ThisInstrumentID, True) ' Use Venice Data if there is no pertrac ID.
        Else
          StatsID = MainForm.StatFunctions.CombinedStatsID(ThisPertracID, False) '  Use Pertrac ID if it is present.
        End If
        InformationArray = MainForm.PertracData.GetInformationValues(ThisPertracID)

        FundStats = MainForm.StatFunctions.GetSimpleStats(MainForm.PertracData.GetPertracDataPeriod(StatsID), StatsID, False, pMaxAttributionDate, 0.0, 1.0#)    ' Zero Risk free rate - GROSS Sharpe.

        If (InformationArray IsNot Nothing) Then
          ReportRow.grManager = Nz(InformationArray(PertracInformationFields.CompanyName), "").ToString
        Else
          ReportRow.grManager = ""
        End If

        ReportRow.grCurrency = CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, ThisInstrumentID, "InstrumentCurrency")) ' From Venice

        ReportRow.M1_Return = FundStats.MonthReturns.P1
        ReportRow.M2_Return = FundStats.MonthReturns.P2
        ReportRow.M3_Return = FundStats.MonthReturns.P3
        ReportRow.YTD_Return = FundStats.SimpleReturn.YTD
        ReportRow.M24_AnnualisedReturn = FundStats.AnnualisedReturn.M24
        ReportRow.M24_Volatility = FundStats.Volatility.M24
        ReportRow.M24_Sharpe = FundStats.SharpeRatio.M24
        ReportRow.ITD_StartDate = FundStats.FirstReturnDate
        ReportRow.ITD_AnnualisedReturn = FundStats.AnnualisedReturn.ITD
        ReportRow.ITD_Volatility = FundStats.Volatility.ITD
        ReportRow.ITD_Sharpe = FundStats.SharpeRatio.ITD

        ReportRow.M24_WorstDrawDown = 0
        ReportRow.ITD_WorstDrawDown = 0

        ' ITD DrawDowns :-

        ThisDrawdowns = MainForm.StatFunctions.GetDrawDownDetails(MainForm.PertracData.GetPertracDataPeriod(StatsID), StatsID, False)
        If (ThisDrawdowns.Length > 0) Then
          ReportRow.ITD_WorstDrawDown = ThisDrawdowns(0).DrawDown
        End If

        ' 24 Month DrawDown.

        ThisDrawdowns = MainForm.StatFunctions.GetDrawDownDetails(MainForm.PertracData.GetPertracDataPeriod(StatsID), StatsID, False, FundStats.MonthReturns.ReferenceDate.AddMonths(-23), Renaissance_EndDate_Data, 1.0#)
        If (ThisDrawdowns.Length > 0) Then
          ReportRow.M24_WorstDrawDown = ThisDrawdowns(0).DrawDown
        End If

        ' AUM - not as easy as it looks !

        If (InformationArray IsNot Nothing) Then

          ' 1st get Pertrac Information Row
          ' 2nd get AUM figure there in.
          If (ConvertIsNumeric(InformationArray(PertracInformationFields.FundAssets))) Then
            ReportRow.grAUM = CDbl(ConvertValue(InformationArray(PertracInformationFields.FundAssets), GetType(Double)))
          Else
            ReportRow.grAUM = 0
          End If

          ' 3rd get Numeric multiplier relating to that Field for that Data Provider

          Dim Multiplier As Double = 1.0

          Try
            Multiplier = CDbl(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblPertracFieldMapping, 0, "Multiplier", "RN", "(DataProvider='" & Nz(InformationArray(PertracInformationFields.DataVendorName), "").ToString & "') AND (FCP_FieldName='" & PertracInformationFields.FundAssets.ToString & "')"))
          Catch ex As Exception
          End Try

          ' 4th Ta Da!

          ReportRow.grAUM = (ReportRow.grAUM * Multiplier / 1000000.0)

        Else
          ReportRow.grAUM = 0

        End If

        ' Add to final table 

        ReportTable.Rows.Add(ReportRow)

      End If
    Next

    ' Set Report Fields - Month and Year labels

    Try
      If (FundStats IsNot Nothing) Then
        Call SetFieldValue(thisReport, New classReportFieldValue("Label_LatestMonth", FundStats.MonthReturns.ReferenceDate.ToString("MMM")))
        Call SetFieldValue(thisReport, New classReportFieldValue("Label_Month2", (FundStats.MonthReturns.ReferenceDate.AddMonths(-1)).ToString("MMM")))
        Call SetFieldValue(thisReport, New classReportFieldValue("Label_Month3", (FundStats.MonthReturns.ReferenceDate.AddMonths(-2)).ToString("MMM")))
        Call SetFieldValue(thisReport, New classReportFieldValue("Label_YTD", "YTD" & vbCrLf & (FundStats.MonthReturns.ReferenceDate).ToString("yyyy")))

        Call SetFieldValue(thisReport, New classReportFieldValue("RptToDate", (pMaxAttributionDate).ToString(RenaissanceGlobals.Globals.REPORT_DATEFORMAT)))
      End If

    Catch ex As Exception
    End Try

    ' Display report

    Call DisplayReport(thisReport, ReportTable, Now(), MainForm.Main_Knowledgedate, pStatusBar)

    Return True

  End Function


  ''' <summary>
  ''' RPTs the assets under management.
  ''' </summary>
  ''' <param name="pValueDate">The p value date.</param>
  ''' <param name="pStatusGroupFilter">The p status group filter.</param>
  ''' <param name="pAdministratorDatesFilter">The p administrator dates filter.</param>
  ''' <param name="pIncludeBorrowing">The p include borrowing.</param>
  ''' <param name="pPriceVariant">The p price variant.</param>
  ''' <param name="pShowNetFlowsReport">if set to <c>true</c> [p show net flows report].</param>
  ''' <param name="pShowNetFlowsBreakdown">if set to <c>true</c> [p show net flows breakdown].</param>
  ''' <param name="pShowAccountingReport">if set to <c>true</c> [p show accounting report].</param>
  ''' <param name="pUseMSFundValues">if set to <c>true</c> [p use MS fund values].</param>
  ''' <param name="ShowDetail">if set to <c>true</c> [show detail].</param>
  ''' <param name="IncludeInternalCounterparties">if set to <c>true</c> [include internal counterparties].</param>
  ''' <param name="ShowDisclaimer">if set to <c>true</c> [show disclaimer].</param>
  ''' <param name="Label_Status">The label_ status.</param>
  ''' <param name="pStatusBar">The p status bar.</param>
  Public Sub rptAssetsUnderManagement(ByVal pValueDate As Date, ByVal pStatusGroupFilter As String, ByVal pAdministratorDatesFilter As Integer, ByVal pIncludeBorrowing As Integer, ByVal pPriceVariant As Integer, ByVal pShowNetFlowsReport As Boolean, ByVal pShowNetFlowsBreakdown As Boolean, ByVal pShowAccountingReport As Boolean, ByVal pUseMSFundValues As Boolean, ByVal ShowDetail As Boolean, ByVal IncludeInternalCounterparties As Boolean, ByVal ShowDisclaimer As Boolean, ByRef Label_Status As ToolStripStatusLabel, ByRef pStatusBar As ToolStripProgressBar)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Dim ReportName As String
    Dim ReportData As DataTable
    Dim AUMReport As C1Report
    Dim AUMToExclude As Double = 0

    Dim ReportString As String

    Try

      If (pShowNetFlowsReport) Then
        If (pShowNetFlowsBreakdown) Then
          ReportName = "rptAssetsUnderManagement_NetFlows_Breakdown"
        Else
          ReportName = "rptAssetsUnderManagement_NetFlows"
        End If
      ElseIf (pShowAccountingReport) Then
        ReportName = "rptAssetsUnderManagementAccountingDetail"
      ElseIf (ShowDetail) Then ' Counterparty Detail
        ReportName = "rptAssetsUnderManagementDetail"
      Else
        ReportName = "rptAssetsUnderManagement"
      End If

      MainForm.SetToolStripText(Label_Status, "Loading AUM Data.")
      ReportData = GetData_AUM(0, pValueDate, pIncludeBorrowing, pPriceVariant, pShowNetFlowsReport, pShowAccountingReport, pUseMSFundValues, ShowDetail, IncludeInternalCounterparties, pStatusBar)
      AUMToExclude = MainForm.MainReportHandler.GetData_AUM_Excluded(0, pValueDate, pStatusGroupFilter, pAdministratorDatesFilter, pStatusBar)

      AUMReport = Me.GetReportDefinition(0, ReportName)

      If (AUMReport.Fields.Contains("Text_AUMToExclude")) Then
        Try
          AUMReport.Fields("Text_AUMToExclude").Value = AUMToExclude
        Catch ex As Exception
        End Try
      End If

      If (ReportData Is Nothing) Then
        MainForm.LogError("ReportHandler, rptAssetsUnderManagement()", LOG_LEVELS.Warning, "", "Failed to get report data.", "", True)
      ElseIf (AUMReport Is Nothing) Then
        MainForm.LogError("ReportHandler, rptAssetsUnderManagement()", LOG_LEVELS.Warning, "", "Failed to get report definition.", "", True)
      Else

        Try
          Select Case pIncludeBorrowing
            Case 1
              ReportString = "Including only the Debt value from leverage providers."

              ' Show Gearing Disclaimer for this Report Variant.

              If (ShowDisclaimer) AndAlso (AUMReport.Fields.Contains("Text_GearingDisclaimer")) Then
                AUMReport.Fields("Text_GearingDisclaimer").Visible = True
              End If

            Case 2
              ReportString = "Including all investments from leverage providers."

            Case Else
              ReportString = "Including no investments from leverage providers."

          End Select

          If pUseMSFundValues = False Then
            ReportString = ReportString & " Using calculated Unit Prices."
          Else
            ReportString = ReportString & " Using posted Unit Prices."
          End If

          If (AUMReport.Fields.Contains("Text_SelectMessage")) Then
            AUMReport.Fields("Text_SelectMessage").Text = ReportString
          End If

        Catch ex As Exception
        End Try

        MainForm.SetToolStripText(Label_Status, "Rendering Report.")
        Call DisplayReport(AUMReport, ReportData, pValueDate, MainForm.Main_Knowledgedate, pStatusBar)

      End If

    Catch ex As Exception
      MainForm.LogError("ReportHandler, rptAssetsUnderManagement()", LOG_LEVELS.Error, ex.Message, "Error generating AUM Report.", ex.StackTrace, True)
    End Try


  End Sub


  ''' <summary>
  ''' RPTs the cash reports.
  ''' </summary>
  ''' <param name="ReportIndex">Index of the report.</param>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pFundName">Name of the p fund.</param>
  ''' <param name="pLegalEntity">The p legal entity.</param>
  ''' <param name="pDetailStartDate">The p detail start date.</param>
  ''' <param name="pDetailEndDate">The p detail end date.</param>
  ''' <param name="pPriceAndFXDate">The p price and FX date.</param>
  ''' <param name="pStatusGroupFilter">The p status group filter.</param>
  ''' <param name="pAdministratorDatesFilter">The p administrator dates filter.</param>
  ''' <param name="pStatusBar">The p status bar.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function rptCashReports(ByVal ReportIndex As Integer, ByVal pFundID As Integer, ByVal pFundName As String, ByVal pLegalEntity As String, ByVal pDetailStartDate As Date, ByVal pDetailEndDate As Date, ByVal pPriceAndFXDate As Date, ByVal pStatusGroupFilter As String, ByVal pAdministratorDatesFilter As Integer, ByRef pStatusBar As ToolStripProgressBar) As Boolean
    ' **************************************************************************************
    '
    '
    ' **************************************************************************************
    Dim ReportData As DataTable = Nothing
    Dim thisReport As C1Report = Nothing

    Try

      Select Case ReportIndex
        Case 0      ' High Detail
          ReportData = MainForm.MainReportHandler.GetData_rptAvailableCashPlus(pFundID, pLegalEntity, Renaissance_BaseDate, pDetailEndDate, pDetailStartDate, pPriceAndFXDate, Now.Date, 0, 0, pStatusGroupFilter, pAdministratorDatesFilter)
          thisReport = GetReportDefinition(pFundID, "rptCashAvailability3")

        Case 1      ' Settled (Available) Cash
          ReportData = MainForm.MainReportHandler.GetData_rptAvailableCash(pFundID, pLegalEntity, Renaissance_BaseDate, pDetailEndDate, pDetailStartDate, 0, pStatusGroupFilter, pAdministratorDatesFilter)
          thisReport = GetReportDefinition(pFundID, "rptCashAvailability2")

        Case 2
          ReportData = MainForm.MainReportHandler.GetData_rptAvailableCash(pFundID, pLegalEntity, Renaissance_BaseDate, pDetailEndDate, pDetailStartDate, 0, pStatusGroupFilter, pAdministratorDatesFilter)
          thisReport = GetReportDefinition(pFundID, "rptCashAvailability1")

        Case 3      ' High Detail
          ReportData = MainForm.MainReportHandler.GetData_rptAvailableCashPlus(pFundID, pLegalEntity, pDetailEndDate, Renaissance_BaseDate, pDetailStartDate, pPriceAndFXDate, Now.Date, 0, 0, pStatusGroupFilter, pAdministratorDatesFilter)
          thisReport = GetReportDefinition(pFundID, "rptCashValue3")

        Case 4       ' Valued Cash
          ReportData = MainForm.MainReportHandler.GetData_rptAvailableCash(pFundID, pLegalEntity, pDetailEndDate, Renaissance_BaseDate, pDetailStartDate, 0, pStatusGroupFilter, pAdministratorDatesFilter)
          thisReport = GetReportDefinition(pFundID, "rptCashValue2")

        Case 5       ' 
          ReportData = MainForm.MainReportHandler.GetData_rptAvailableCash(pFundID, pLegalEntity, pDetailEndDate, Renaissance_BaseDate, pDetailStartDate, 0, pStatusGroupFilter, pAdministratorDatesFilter)
          thisReport = GetReportDefinition(pFundID, "rptCashValue1")

        Case 6
          ReportData = MainForm.MainReportHandler.GetData_rptCashMove(pFundID, pDetailStartDate, pDetailEndDate, MainForm.Main_Knowledgedate, MainForm.Main_Knowledgedate, pStatusGroupFilter, pAdministratorDatesFilter, MainForm.Main_Knowledgedate)
          thisReport = GetReportDefinition(pFundID, "rptCashMovement")
          Try
            thisReport.Fields("Text_StartDate").Text = pDetailStartDate.ToString(REPORT_DATEFORMAT)
          Catch ex As Exception
          End Try
          Try
            thisReport.Fields("Text_EndDate").Text = pDetailEndDate.ToString(REPORT_DATEFORMAT)
          Catch ex As Exception
          End Try

        Case 7
          ReportData = MainForm.MainReportHandler.GetData_rptAvailableCashPlus(pFundID, pLegalEntity, Renaissance_BaseDate, pDetailEndDate, pDetailStartDate, pPriceAndFXDate, Now.Date, InstrumentTypes.Cash, FundType.Cash, pStatusGroupFilter, pAdministratorDatesFilter)
          thisReport = GetReportDefinition(pFundID, "rptCashMoveDetail_Settlement")

        Case 8
          ReportData = MainForm.MainReportHandler.GetData_rptAvailableCashPlus(pFundID, pLegalEntity, pDetailEndDate, Renaissance_BaseDate, pDetailStartDate, pPriceAndFXDate, Now.Date, InstrumentTypes.Cash, FundType.Cash, pStatusGroupFilter, pAdministratorDatesFilter)
          thisReport = GetReportDefinition(pFundID, "rptCashMoveDetail_Value")

      End Select

      If (ReportData Is Nothing) Then
        MainForm.LogError("ReportHandler, rptCashReports()", LOG_LEVELS.Warning, "", "Failed to get report data.", "", True)
        Return False
      End If

      If (thisReport Is Nothing) Then
        MainForm.LogError("ReportHandler, rptCashReports()", LOG_LEVELS.Warning, "", "Failed to get report definition.", "", True)
        Return False
      End If

      Try
        thisReport.DataSource.Recordset = ReportData
      Catch ex As Exception
      End Try

      ' modify Date Fields as necessary

      ' Text_FundWarningText
      If pFundID > 0 Then
        Try
          If thisReport.Fields.Contains("Text_FundWarningText") Then
            thisReport.Fields("Text_FundWarningText").Text = "Note : This report has been restricted to the " & pFundName & " fund"
          End If
        Catch ex As Exception
        End Try
      Else
        Try
          thisReport.Fields("Text_FundWarningText").Text = ""
        Catch ex As Exception
        End Try
      End If

      ' Apply ValueDate and KnowledgeDate
      Try
        If thisReport.Fields.Contains(REPORT_ValueDate_FieldName) Then thisReport.Fields(REPORT_ValueDate_FieldName).Text = pDetailStartDate.ToString(REPORT_DATEFORMAT)
      Catch ex As Exception
      End Try

      Try
        If thisReport.Fields.Contains("Text_SettlementDate") Then thisReport.Fields("Text_SettlementDate").Text = pDetailStartDate.ToString(REPORT_DATEFORMAT)
      Catch ex As Exception
      End Try

      Try
        If thisReport.Fields.Contains("Text_StartDate") Then thisReport.Fields("Text_StartDate").Text = pDetailStartDate.ToString(REPORT_DATEFORMAT)
      Catch ex As Exception
      End Try
      Try
        If thisReport.Fields.Contains("Text_EndDate") Then thisReport.Fields("Text_EndDate").Text = pDetailEndDate.ToString(REPORT_DATEFORMAT)
      Catch ex As Exception
      End Try


      Try
        If thisReport.Fields.Contains("Text_TransactionsThru") Then thisReport.Fields("Text_TransactionsThru").Text = "Transactions Thru " & pDetailEndDate.ToString(REPORT_DATEFORMAT)
      Catch ex As Exception
      End Try

      Try
        If MainForm.Main_Knowledgedate.CompareTo(KNOWLEDGEDATE_NOW) <= 0 Then
          thisReport.Fields(REPORT_KnowledgeDate_FieldName).Text = "Live"
        ElseIf MainForm.Main_Knowledgedate.TimeOfDay.TotalSeconds >= LAST_SECOND Then
          thisReport.Fields(REPORT_KnowledgeDate_FieldName).Text = MainForm.Main_Knowledgedate.ToString(REPORT_DATEFORMAT)
        Else
          thisReport.Fields(REPORT_KnowledgeDate_FieldName).Text = MainForm.Main_Knowledgedate.ToString(REPORT_LONGDATEFORMAT)
        End If
      Catch ex As Exception
      End Try

      DisplayReport(thisReport, pStatusBar)

      SaveReportData(ReportData, thisReport.ReportName)

    Catch ex As Exception
    End Try

    Return True

  End Function


  ''' <summary>
  ''' RPTs the futures notional reports.
  ''' </summary>
  ''' <param name="ReportIndex">Index of the report.</param>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pFundName">Name of the p fund.</param>
  ''' <param name="pValuationDate">The p valuation date.</param>
  ''' <param name="pStatusGroupFilter">The p status group filter.</param>
  ''' <param name="pAdministratorDatesFilter">The p administrator dates filter.</param>
  ''' <param name="pUnRealisedCalculationType">Type of the p un realised calculation.</param>
  ''' <param name="pStatusBar">The p status bar.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function rptFuturesNotionalReports(ByVal ReportIndex As Integer, ByVal pFundID As Integer, ByVal pFundName As String, ByVal pValuationDate As Date, ByVal pStatusGroupFilter As String, ByVal pAdministratorDatesFilter As Integer, ByVal pUnRealisedCalculationType As Integer, ByRef pStatusBar As ToolStripProgressBar) As Boolean
    ' **************************************************************************************
    '
    '
    ' **************************************************************************************
    Dim ReportData As DataTable = Nothing
    Dim thisReport As C1Report = Nothing

    Try

      Select Case ReportIndex

        Case 0      ' High Detail
          ReportData = MainForm.MainReportHandler.GetData_rptNotionalAllocationReport(pFundID, pValuationDate, pStatusGroupFilter, pAdministratorDatesFilter, pUnRealisedCalculationType)
          thisReport = GetReportDefinition(pFundID, "rptNotionalAllocationReport")

      End Select

      If (ReportData Is Nothing) Then
        MainForm.LogError("ReportHandler, rptFuturesNotionalReports()", LOG_LEVELS.Warning, "", "Failed to get report data.", "", True)
        Return False
      End If

      If (thisReport Is Nothing) Then
        MainForm.LogError("ReportHandler, rptFuturesNotionalReports()", LOG_LEVELS.Warning, "", "Failed to get report definition.", "", True)
        Return False
      End If

      Try
        thisReport.DataSource.Recordset = ReportData
      Catch ex As Exception
      End Try

      ' modify Date Fields as necessary

      ' Apply ValueDate and KnowledgeDate
      Try
        thisReport.Fields(REPORT_ValueDate_FieldName).Text = pValuationDate.ToString(REPORT_DATEFORMAT)
      Catch ex As Exception
      End Try

      Try
        If thisReport.Fields.Contains("Text_ReportDetails") Then
          Dim StatusText As String = ""

          If (pStatusGroupFilter.Length > 0) Then
            pStatusGroupFilter = "Trade Status Filter : `" & pStatusGroupFilter & "`  "
          Else
            pStatusGroupFilter = "Trade Status Filter : <none>  "
          End If

          If (pAdministratorDatesFilter <> 0) Then
            pStatusGroupFilter &= "Dates Filter : " & pAdministratorDatesFilter.ToString & "  "
          Else
            pStatusGroupFilter &= "Dates Filter : <None>  "
          End If

          thisReport.Fields("Text_ReportDetails").Text = pStatusGroupFilter & "Unrealised P&L Calculation type : " & CType(pUnRealisedCalculationType, CalculationTypes_Profit).ToString
        End If
      Catch ex As Exception
      End Try

      Try
        If thisReport.Fields.Contains(REPORT_KnowledgeDate_FieldName) Then
          If MainForm.Main_Knowledgedate.CompareTo(KNOWLEDGEDATE_NOW) <= 0 Then
            thisReport.Fields(REPORT_KnowledgeDate_FieldName).Text = "Live"
          ElseIf MainForm.Main_Knowledgedate.TimeOfDay.TotalSeconds >= LAST_SECOND Then
            thisReport.Fields(REPORT_KnowledgeDate_FieldName).Text = MainForm.Main_Knowledgedate.ToString(REPORT_DATEFORMAT)
          Else
            thisReport.Fields(REPORT_KnowledgeDate_FieldName).Text = MainForm.Main_Knowledgedate.ToString(REPORT_LONGDATEFORMAT)
          End If
        End If
      Catch ex As Exception
      End Try

      DisplayReport(thisReport, pStatusBar)

      SaveReportData(ReportData, thisReport.ReportName)

    Catch ex As Exception
    End Try

    Return True

  End Function

  ''' <summary>
  ''' RPTs the reconciliation reports.
  ''' </summary>
  ''' <param name="ReportIndex">Index of the report.</param>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pFundName">Name of the p fund.</param>
  ''' <param name="pValuationDate">The p valuation date.</param>
  ''' <param name="pStatusGroupFilter">The p status group filter.</param>
  ''' <param name="pAdministratorDatesFilter">The p administrator dates filter.</param>
  ''' <param name="pStatusBar">The p status bar.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function rptReconciliationReports(ByVal ReportIndex As Integer, ByVal pFundID As Integer, ByVal pFundName As String, ByVal pValuationDate As Date, ByVal pStatusGroupFilter As String, ByVal pAdministratorDatesFilter As RenaissanceGlobals.AdministratorDatesFilter, ByRef pStatusBar As ToolStripProgressBar) As Boolean
    ' **************************************************************************************
    '
    '
    ' **************************************************************************************
    Dim ReportData As DataTable = Nothing
    Dim thisReport As C1Report = Nothing

    Try

      Select Case ReportIndex

        Case 0      ' Inventory Reconciliation
          ReportData = MainForm.MainReportHandler.GetData_rptInventoryReconciliationReport(pFundID, pValuationDate, pStatusGroupFilter, pAdministratorDatesFilter)
          thisReport = GetReportDefinition(pFundID, "rptInventoryReconciliationReport")

      End Select

      If (ReportData Is Nothing) Then
        MainForm.LogError("ReportHandler, rptReconciliationReports()", LOG_LEVELS.Warning, "", "Failed to get report data.", "", True)
        Return False
      End If

      If (thisReport Is Nothing) Then
        MainForm.LogError("ReportHandler, rptReconciliationReports()", LOG_LEVELS.Warning, "", "Failed to get report definition.", "", True)
        Return False
      End If

      Try
        thisReport.DataSource.Recordset = ReportData
      Catch ex As Exception
      End Try

      ' modify Date Fields as necessary

      ' Apply ValueDate and KnowledgeDate
      Try
        thisReport.Fields(REPORT_ValueDate_FieldName).Text = pValuationDate.ToString(REPORT_DATEFORMAT)
      Catch ex As Exception
      End Try

      Try
        If thisReport.Fields.Contains("Text_ReportDetails") Then
          Dim StatusText As String = ""

          If (pStatusGroupFilter.Length > 0) Then
            pStatusGroupFilter = "Trade Status Filter : `" & pStatusGroupFilter & "`  "
          Else
            pStatusGroupFilter = "Trade Status Filter : <none>  "
          End If

          If (pAdministratorDatesFilter <> 0) Then
            pStatusGroupFilter &= "Dates Filter : " & pAdministratorDatesFilter.ToString & "  "
          Else
            pStatusGroupFilter &= "Dates Filter : <None>  "
          End If

          thisReport.Fields("Text_ReportDetails").Text = pStatusGroupFilter
        End If
      Catch ex As Exception
      End Try

      Try
        If thisReport.Fields.Contains(REPORT_KnowledgeDate_FieldName) Then
          If MainForm.Main_Knowledgedate.CompareTo(KNOWLEDGEDATE_NOW) <= 0 Then
            thisReport.Fields(REPORT_KnowledgeDate_FieldName).Text = "Live"
          ElseIf MainForm.Main_Knowledgedate.TimeOfDay.TotalSeconds >= LAST_SECOND Then
            thisReport.Fields(REPORT_KnowledgeDate_FieldName).Text = MainForm.Main_Knowledgedate.ToString(REPORT_DATEFORMAT)
          Else
            thisReport.Fields(REPORT_KnowledgeDate_FieldName).Text = MainForm.Main_Knowledgedate.ToString(REPORT_LONGDATEFORMAT)
          End If
        End If
      Catch ex As Exception
      End Try

      DisplayReport(thisReport, pStatusBar)

      SaveReportData(ReportData, thisReport.ReportName)

    Catch ex As Exception
    End Try

    Return True

  End Function

  ''' <summary>
  ''' RPTs the expense report.
  ''' </summary>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pFundName">Name of the p fund.</param>
  ''' <param name="pValueDate">The p value date.</param>
  ''' <param name="pStatusGroupFilter">The p status group filter.</param>
  ''' <param name="pAdministratorDatesFilter">The p administrator dates filter.</param>
  ''' <param name="pStatusBar">The p status bar.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function rptExpenseReport(ByVal pFundID As Integer, ByVal pFundName As String, ByVal pValueDate As Date, ByVal pStatusGroupFilter As String, ByVal pAdministratorDatesFilter As Integer, ByRef pStatusBar As ToolStripProgressBar) As Boolean

    Dim ReportDate As Date
    Dim YearEndDate As Date
    Dim ReportData As DataTable
    Dim ExpenseReport As C1Report

    Try
      ' Calculate the Date for Year End (Used to Select Expense transactions )

      YearEndDate = New Date(pValueDate.Year, 12, 31) ' Last day of the given year

      ' Get Related Expense Data

      ReportData = Me.GetData_ExpenseReport(pFundID, YearEndDate, pStatusGroupFilter, pAdministratorDatesFilter)
      ExpenseReport = Me.GetReportDefinition(pFundID, "rptExpenseByMonth")

      If (ReportData Is Nothing) Then
        MainForm.LogError("ReportHandler, rptExpenseReport()", LOG_LEVELS.Warning, "", "Failed to get report data.", "", True)
      End If
      If (ExpenseReport Is Nothing) Then
        MainForm.LogError("ReportHandler, rptExpenseReport()", LOG_LEVELS.Warning, "", "Failed to get report definition.", "", True)
      End If

      ' Establish the appropriate Report Date 
      ' Equal to the date of the latest selected Expense.

      ReportDate = New Date(pValueDate.Year, 1, 1)
      Dim thisDataRow As DataRow
      For Each thisDataRow In ReportData.Rows
        If ReportDate.CompareTo(CDate(thisDataRow("TransactionValueDate"))) < 0 Then
          ReportDate = CDate(thisDataRow("TransactionValueDate"))
        End If
      Next
      If ReportDate.CompareTo(YearEndDate) > 0 Then
        ReportDate = YearEndDate
      End If

      ' Calculate Fund Valuations 

      Dim MonthCounter As Integer
      Dim FundValuationDate As Date

      For MonthCounter = 1 To ReportDate.Month
        Try
          FundValuationDate = New Date(ReportDate.Year, MonthCounter, 1)
          FundValuationDate = FundValuationDate.AddMonths(1)
          FundValuationDate = FundValuationDate.AddDays(-1)   ' Last Day of the month

          ExpenseReport.Fields("Text_FundValue_" & FundValuationDate.ToString("MMM")).Value = (Get_FundValue_ExExpense(MainForm, pFundID, FundValuationDate, False, pStatusGroupFilter, pAdministratorDatesFilter) / 1000000.0)

        Catch ex As Exception

        End Try

      Next

      ' Make Unused Field invisible ' 
      ' Text_Expense_

      If (ReportDate.Month < 12) Then
        For MonthCounter = (ReportDate.Month + 1) To 12
          Try
            FundValuationDate = New Date(ReportDate.Year, MonthCounter, 1)
            FundValuationDate = FundValuationDate.AddMonths(1)
            FundValuationDate = FundValuationDate.AddDays(-1)   ' Last Day of the month

            ExpenseReport.Fields("Text_Expense_" & FundValuationDate.ToString("MMM")).Visible = False
            ExpenseReport.Fields("Text_SumExpense_" & FundValuationDate.ToString("MMM")).Visible = False
            ExpenseReport.Fields("Text_FeesEx_" & FundValuationDate.ToString("MMM")).Visible = False
            ExpenseReport.Fields("Text_FundValue_" & FundValuationDate.ToString("MMM")).Visible = False
            ExpenseReport.Fields("Text_TER_" & FundValuationDate.ToString("MMM")).Visible = False

          Catch ex As Exception
          End Try
        Next
      End If

      ' Year Field

      Try
        ExpenseReport.Fields("Text_ValueYear").Text = ReportDate.Year.ToString
      Catch ex As Exception
      End Try

      ' Fund Name 

      Try
        ExpenseReport.Fields("Text_FundName").Text = pFundName
      Catch ex As Exception
      End Try

      Call DisplayReport(ExpenseReport, ReportData, ReportDate, MainForm.Main_Knowledgedate, pStatusBar)

    Catch ex As Exception
      MainForm.LogError("ReportHandler, rptExpenseReport()", LOG_LEVELS.Error, ex.Message, "Error generating Fund Expense Report.", ex.StackTrace, True)
    End Try


  End Function

  ''' <summary>
  ''' RPTs the management fees report.
  ''' </summary>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pFundName">Name of the p fund.</param>
  ''' <param name="pYear">The p year.</param>
  ''' <param name="pReportCurrency">The p report currency.</param>
  ''' <param name="pStatusGroupFilter">The p status group filter.</param>
  ''' <param name="pAdministratorDatesFilter">The p administrator dates filter.</param>
  ''' <param name="pSummaryReport">if set to <c>true</c> [p summary report].</param>
  ''' <param name="pStatusBar">The p status bar.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function rptManagementFeesReport(ByVal pFundID As Integer, ByVal pFundName As String, ByVal pYear As Integer, ByVal pReportCurrency As Integer, ByVal pStatusGroupFilter As String, ByVal pAdministratorDatesFilter As Integer, ByVal pSummaryReport As Boolean, ByRef pStatusBar As ToolStripProgressBar) As Boolean

    Dim ReportDate As Date
    Dim YearEndDate As Date
    Dim ReportData As DataTable
    Dim ExpenseReport As C1Report

    Try
      ' Calculate the Date for Year End (Used to Select Expense transactions )

      YearEndDate = New Date(pYear, 12, 31) ' Last day of the given year

      ' Get Related Expense Data

      ReportData = Me.GetData_ManagementFeesReport(pFundID, pYear, pReportCurrency, pStatusGroupFilter, pAdministratorDatesFilter)
      ExpenseReport = Me.GetReportDefinition(pFundID, "rptManagementFeesByMonth")

      If (ReportData Is Nothing) Then
        MainForm.LogError("ReportHandler, rptExpenseReport()", LOG_LEVELS.Warning, "", "Failed to get report data.", "", True)
      End If
      If (ExpenseReport Is Nothing) Then
        MainForm.LogError("ReportHandler, rptExpenseReport()", LOG_LEVELS.Warning, "", "Failed to get report definition.", "", True)
      End If

      '
      If (pSummaryReport) Then
        ExpenseReport.Groups("Fund").SectionHeader.Visible = False
        ExpenseReport.Groups("FundID").SectionFooter.Visible = False
        ExpenseReport.Groups("FundReportGroup").SectionHeader.ForcePageBreak = ForcePageBreakEnum.None
        'ExpenseReport.Groups("FundReportGroup").SectionFooter.Visible = False

        ExpenseReport.Fields("Label_GroupFooter_Total").Visible = False
        ExpenseReport.Fields("Line_GroupFooter").Visible = False
        ExpenseReport.Fields("Label_GroupFooter_FundName").Visible = True


      End If

      ' Establish the appropriate Report Date 
      ' Equal to the date of the latest selected Fee.

      ReportDate = New Date(pYear, 1, 1)
      Dim thisDataRow As DataRow
      For Each thisDataRow In ReportData.Rows
        If CInt(thisDataRow("CarriedOver")) <= 0 Then ' Exclude Carried over months
          If ReportDate.Month < CInt(thisDataRow("FeeMonth")) Then
            ReportDate = New Date(pYear, CInt(thisDataRow("FeeMonth")), 1)
          End If
        End If
      Next
      If ReportDate.CompareTo(YearEndDate) > 0 Then
        ReportDate = YearEndDate
      End If
      ReportDate = RenaissanceUtilities.DatePeriodFunctions.FitDateToPeriod(DealingPeriod.Monthly, ReportDate, True)

      '' Calculate Fund Valuations 

      Dim MonthCounter As Integer
      Dim FundValuationDate As Date

      'For MonthCounter = 1 To ReportDate.Month
      '  Try
      '    FundValuationDate = New Date(ReportDate.Year, MonthCounter, 1)
      '    FundValuationDate = FundValuationDate.AddMonths(1)
      '    FundValuationDate = FundValuationDate.AddDays(-1)   ' Last Day of the month

      '    ExpenseReport.Fields("Text_FundValue_" & FundValuationDate.ToString("MMM")).Value = (Get_FundValue_ExExpense(MainForm, pFundID, FundValuationDate, False, pStatusGroupFilter, pAdministratorDatesFilter) / 1000000.0)

      '  Catch ex As Exception

      '  End Try

      'Next

      ' Make Unused Field invisible ' 
      ' Text_Expense_

      If (ReportDate.Month < 12) Then
        For MonthCounter = (ReportDate.Month + 1) To 12
          Try
            FundValuationDate = New Date(ReportDate.Year, MonthCounter, 1)
            FundValuationDate = FundValuationDate.AddMonths(1)
            FundValuationDate = FundValuationDate.AddDays(-1)   ' Last Day of the month

            ExpenseReport.Fields("Text_Expense_" & FundValuationDate.ToString("MMM") & "1").Visible = False
            ExpenseReport.Fields("Text_Expense_" & FundValuationDate.ToString("MMM") & "2").Visible = False
            ExpenseReport.Fields("Text_Expense_" & FundValuationDate.ToString("MMM") & "3").Visible = False
            ExpenseReport.Fields("Text_Expense_" & FundValuationDate.ToString("MMM") & "4").Visible = False
            ExpenseReport.Fields("Text_Expense_" & FundValuationDate.ToString("MMM") & "5").Visible = False
            ExpenseReport.Fields("Text_Expense_" & FundValuationDate.ToString("MMM") & "6").Visible = False
            ExpenseReport.Fields("Text_SumExpense_" & FundValuationDate.ToString("MMM")).Visible = False
            ExpenseReport.Fields("Text_SumExpense_" & FundValuationDate.ToString("MMM") & "1").Visible = False
            ExpenseReport.Fields("Text_SumExpense_" & FundValuationDate.ToString("MMM") & "2").Visible = False

          Catch ex As Exception
          End Try
        Next
      End If

      ' Year Field

      Try
        ExpenseReport.Fields("Text_ValueYear").Text = pYear.ToString
      Catch ex As Exception
      End Try

      ' Fund Name 

      Try
        ExpenseReport.Fields("Text_FundName").Text = pFundName
      Catch ex As Exception
      End Try

      Call DisplayReport(ExpenseReport, ReportData, ReportDate, MainForm.Main_Knowledgedate, pStatusBar)

    Catch ex As Exception
      MainForm.LogError("ReportHandler, rptManagementFeesReport()", LOG_LEVELS.Error, ex.Message, "Error generating Management Fees Report.", ex.StackTrace, True)
    End Try


  End Function


  ''' <summary>
  ''' RPTs the liquidity report.
  ''' </summary>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pValueDate">The p value date.</param>
  ''' <param name="pReportToRun">The p report to run.</param>
  ''' <param name="pStatusBar">The p status bar.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function rptLiquidityReport(ByVal pFundID As Integer, ByVal pValueDate As Date, ByVal pReportToRun As Integer, ByRef pStatusBar As ToolStripProgressBar) As Boolean
    ' ************************************************************************

    Dim ReportData As DataTable

    ' Get report data.

    ReportData = GetData_rptLiquidityReport(pFundID, pValueDate)

    If (ReportData Is Nothing) Then
      Exit Function
    End If

    Try
      Select Case pReportToRun
        Case 1 ' Dealing Date
          DisplayReport(0, "rptLiquidityReport", ReportData, pStatusBar)

        Case Else   ' Notice Date 
          DisplayReport(0, "rptNoticeReport", ReportData, pStatusBar)

      End Select
    Catch ex As Exception
    End Try
  End Function


  ''' <summary>
  ''' RPTs the notice report.
  ''' </summary>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pValueDate">The p value date.</param>
  ''' <param name="pStatusBar">The p status bar.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function rptNoticeReport(ByVal pFundID As Integer, ByVal pValueDate As Date, ByRef pStatusBar As ToolStripProgressBar) As Boolean
    ' ************************************************************************

    Dim ReportData As DataTable

    ' Get report data.

    ReportData = GetData_rptLiquidityReport(pFundID, pValueDate)

    If (ReportData Is Nothing) Then
      Exit Function
    End If

    Try
      DisplayReport(0, "rptNoticeReport", ReportData, pStatusBar)
    Catch ex As Exception
    End Try
  End Function


  ''' <summary>
  ''' RPTs the prices.
  ''' </summary>
  ''' <param name="InstrumentID">The instrument ID.</param>
  ''' <param name="pDateFrom">The p date from.</param>
  ''' <param name="pDateTo">The p date to.</param>
  ''' <param name="pStatusBar">The p status bar.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function rptPrices(ByVal InstrumentID As Integer, ByVal pDateFrom As Date, ByVal pDateTo As Date, ByRef pStatusBar As ToolStripProgressBar) As Boolean
    ' ************************************************************************

    Dim ReportData As DataTable

    ' Get report data.

    ReportData = GetData_rptPrices(InstrumentID, pDateFrom, pDateTo)

    If (ReportData Is Nothing) Then
      Exit Function
    End If

    Try
      DisplayReport("rptPrices", ReportData, 0, New classReportFieldValue() {New classReportFieldValue("RptFromDate", pDateFrom.ToString(REPORT_DATEFORMAT)), New classReportFieldValue("RptToDate", pDateTo.ToString(REPORT_DATEFORMAT)), New classReportFieldValue("Text_KnowledgeDate", MainForm.Main_Knowledgedate.ToString(REPORT_DATEFORMAT))}, pStatusBar)
    Catch ex As Exception
    End Try
  End Function


  ''' <summary>
  ''' RPTs the fund gearing.
  ''' </summary>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pFundName">Name of the p fund.</param>
  ''' <param name="pReportDate">The p report date.</param>
  ''' <param name="pStatusGroupFilter">The p status group filter.</param>
  ''' <param name="pAdministratorDatesFilter">The p administrator dates filter.</param>
  ''' <param name="pKnowledgeDate">The p knowledge date.</param>
  ''' <param name="pStatusBar">The p status bar.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function rptFundGearing(ByVal pFundID As Integer, ByVal pFundName As String, ByVal pReportDate As Date, ByVal pStatusGroupFilter As String, ByVal pAdministratorDatesFilter As Integer, ByVal pKnowledgeDate As Date, ByRef pStatusBar As ToolStripProgressBar) As Boolean
    ' ************************************************************************
    '
    ' ************************************************************************

    Dim ReportData As DataTable

    ' Get report data.

    ReportData = GetData_rptFundGearing(pFundID, pReportDate, pStatusGroupFilter, pAdministratorDatesFilter, pKnowledgeDate)

    If (ReportData Is Nothing) Then
      Exit Function
    End If

    Try
      DisplayReport("rptFundGearing", ReportData, pFundID, pReportDate, pKnowledgeDate, pStatusBar)
    Catch ex As Exception
    End Try
  End Function

  ''' <summary>
  ''' RPTs the fund fees.
  ''' </summary>
  ''' <param name="pReportName">Name of the p report.</param>
  ''' <param name="ReportData">The report data.</param>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pReportDate">The p report date.</param>
  ''' <param name="NetFees">if set to <c>true</c> [net fees].</param>
  ''' <param name="InUSD">if set to <c>true</c> [in USD].</param>
  ''' <param name="InGBP">if set to <c>true</c> [in GBP].</param>
  ''' <param name="pKnowledgeDate">The p knowledge date.</param>
  ''' <param name="pStatusBar">The p status bar.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function rptFundFees(ByVal pReportName As String, ByVal ReportData As Object, ByVal pFundID As Integer, ByVal pReportDate As Date, ByVal NetFees As Boolean, ByVal InUSD As Boolean, ByVal InGBP As Boolean, ByVal pKnowledgeDate As Date, ByRef pStatusBar As ToolStripProgressBar) As Boolean
    ' ************************************************************************
    '
    ' ************************************************************************

    Dim thisReport As C1Report

    ' Get report definition.

    thisReport = GetReportDefinition(pFundID, pReportName)

    If (thisReport Is Nothing) Then
      Exit Function
    End If

    ' Set Extra Custom Fields
    Dim thisFieldValue As classReportFieldValue
    Dim FieldValue As String
    Dim FieldName As String
    Dim TitleString As String

    Select Case pReportName

      Case "rptFundFeeReport"

        If (NetFees) Then
          FieldValue = "=[ManagementFees_Net]"
          TitleString = "NET Fund Fees"
        Else
          FieldValue = "=[ManagementFees_Gross]"
          TitleString = "GROSS Fund Fees"
        End If

        If (InUSD) Then
          FieldValue &= " * [FXtoUSD]"
          TitleString &= " in USD"
        ElseIf (InGBP) Then
          FieldValue &= " * [FXtoUSD] / [FXtoReferenceCcy]"
          TitleString &= " in GBP"
        Else
          TitleString &= " in local currency"
        End If

        thisFieldValue = New classReportFieldValue("Text_FundMgmtFeeValue", FieldValue)
        Call SetFieldValue(thisReport, thisFieldValue)

        thisFieldValue = New classReportFieldValue("Label_GroupHeader", TitleString)
        Call SetFieldValue(thisReport, thisFieldValue)

        If (NetFees) Then
          FieldValue = "=[PerformanceFees_Net]"
        Else
          FieldValue = "=[PerformanceFees_Gross]"
        End If
        If (InUSD) Then
          FieldValue &= " * [FXtoUSD]"
        ElseIf (InGBP) Then
          FieldValue &= " * [FXtoUSD] / [FXtoReferenceCcy]"
        End If

        thisFieldValue = New classReportFieldValue("Text_FundPerfFeeValue", FieldValue)
        Call SetFieldValue(thisReport, thisFieldValue)

      Case "rptFundFeeStatusReport"

        ' Title

        TitleString = "Primonial, Fund Fees Summary Report"

        If (InUSD) Then
          TitleString &= " in USD"
        ElseIf (InGBP) Then
          TitleString &= " in GBP"
        Else
          TitleString &= " in local currency"
        End If

        thisFieldValue = New classReportFieldValue("Label_ReportTitle", TitleString)
        Call SetFieldValue(thisReport, thisFieldValue)

        Dim FieldStrings(,) As String = _
         {{"Text_GrossMgtFees", "=([ManagementFees_Gross]-([ManagementFees_DebtRebate]+[ManagementFees_EquityRebate]))"}, _
         {"Text_MgtMarketingFees", "=([ManagementFees_MarketingFCAM]+[ManagementFees_MarketingOthers])"}, _
         {"Text_NetMgtFees", "=[ManagementFees_Net]"}, _
         {"Text_GrossPerfFees", "=([PerformanceFees_Gross]-([PerformanceFees_DebtRebate]+[PerformanceFees_EquityRebate]))"}, _
         {"Text_PerfMarketingFees", "=([PerformanceFees_MarketingFCAM]+[PerformanceFees_MarketingOthers])"}, _
         {"Text_NetPerfFees", "=[PerformanceFees_Net]"}, _
         {"Text_GrossFees", "=(([ManagementFees_Gross]+[PerformanceFees_Gross])-([ManagementFees_DebtRebate]+[ManagementFees_EquityRebate]+[PerformanceFees_DebtRebate]+[PerformanceFees_EquityRebate]))"}, _
         {"Text_MarketingFees", "=([ManagementFees_MarketingFCAM]+[ManagementFees_MarketingOthers]+[PerformanceFees_MarketingFCAM]+[PerformanceFees_MarketingOthers])"}, _
         {"Text_NetFees", "=([ManagementFees_Net]+[PerformanceFees_Net])"}}

        Dim FieldCounter As Integer

        For FieldCounter = 0 To UBound(FieldStrings, 1)
          FieldName = FieldStrings(FieldCounter, 0)
          FieldValue = FieldStrings(FieldCounter, 1)

          If (InUSD) Then
            FieldValue &= " * [FXtoUSD]"
          ElseIf (InGBP) Then
            FieldValue &= " * [FXtoUSD] / [FXtoReferenceCcy]"
          End If

          thisFieldValue = New classReportFieldValue(FieldName, FieldValue)
          Call SetFieldValue(thisReport, thisFieldValue)
        Next

      Case "rptFundFeeStatusDetailReport"

        ' Title

        TitleString = "Primonial, Fund Fees Counterparty Summary Report"

        If (InUSD) Then
          TitleString &= " in USD"
        ElseIf (InGBP) Then
          TitleString &= " in GBP"
        Else
          TitleString &= " in local currency"
        End If

        thisFieldValue = New classReportFieldValue("Label_ReportTitle", TitleString)
        Call SetFieldValue(thisReport, thisFieldValue)

        Dim FieldStrings(,) As String = _
         {{"Text_GrossMgtFees", "=[ManagementFees_Gross]"}, _
         {"Text_RebateMgmtFees", "=([ManagementFees_DebtRebate]+[ManagementFees_EquityRebate])"}, _
         {"Text_MgtMarketingFees", "=([ManagementFees_MarketingFCAM]+[ManagementFees_MarketingOthers])"}, _
         {"Text_NetMgtFees", "=[ManagementFees_Net]"}, _
         {"Text_GrossPerfFees", "=([PerformanceFees_Gross]-([PerformanceFees_DebtRebate]+[PerformanceFees_EquityRebate]))"}, _
         {"Text_PerfMarketingFees", "=([PerformanceFees_MarketingFCAM]+[PerformanceFees_MarketingOthers])"}, _
         {"Text_NetPerfFees", "=[PerformanceFees_Net]"}, _
         {"Text_GrossFees", "=(([ManagementFees_Gross]+[PerformanceFees_Gross])-([ManagementFees_DebtRebate]+[ManagementFees_EquityRebate]+[PerformanceFees_DebtRebate]+[PerformanceFees_EquityRebate]))"}, _
         {"Text_MarketingFees", "=([ManagementFees_MarketingFCAM]+[ManagementFees_MarketingOthers]+[PerformanceFees_MarketingFCAM]+[PerformanceFees_MarketingOthers])"}, _
         {"Text_NetFees", "=([ManagementFees_Net]+[PerformanceFees_Net])"}}

        Dim FieldCounter As Integer

        For FieldCounter = 0 To UBound(FieldStrings, 1)
          FieldName = FieldStrings(FieldCounter, 0)
          FieldValue = FieldStrings(FieldCounter, 1)

          If (InUSD) Then
            FieldValue &= " * [FXtoUSD]"
          ElseIf (InGBP) Then
            FieldValue &= " * [FXtoUSD] / [FXtoReferenceCcy]"
          End If

          thisFieldValue = New classReportFieldValue(FieldName, FieldValue)
          Call SetFieldValue(thisReport, thisFieldValue)
        Next





      Case Else

    End Select


    thisFieldValue = Nothing

    Try
      DisplayReport(thisReport, ReportData, pReportDate, pKnowledgeDate, pStatusBar)
    Catch ex As Exception
    End Try
  End Function

  ''' <summary>
  ''' RPTs the trade status.
  ''' </summary>
  ''' <param name="FundID">The fund ID.</param>
  ''' <param name="TradeStatusID">The trade status ID.</param>
  ''' <param name="pDateFrom">The p date from.</param>
  ''' <param name="pDateTo">The p date to.</param>
  ''' <param name="pStatusBar">The p status bar.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function rptTradeStatus(ByVal FundID As Integer, ByVal TradeStatusID As Integer, ByVal pDateFrom As Date, ByVal pDateTo As Date, ByRef pStatusBar As ToolStripProgressBar) As Boolean
    ' ************************************************************************
    '
    ' ************************************************************************

    Dim ReportData As DataTable
    Dim StatusName As String = CStr(Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblTradeStatus, TradeStatusID, "TradeStatusName"), "All Trade Status"))

    ' Get report data.

    ReportData = GetData_rptTradeStatus(FundID, TradeStatusID, pDateFrom, pDateTo)

    If (ReportData Is Nothing) Then
      Exit Function
    End If

    Try
      DisplayReport("rptTradeStatus", ReportData, 0, New classReportFieldValue() {New classReportFieldValue("RptTradeStatus", StatusName), New classReportFieldValue("RptFromDate", pDateFrom.ToString(REPORT_DATEFORMAT)), New classReportFieldValue("RptToDate", pDateTo.ToString(REPORT_DATEFORMAT)), New classReportFieldValue("Text_KnowledgeDate", MainForm.Main_Knowledgedate.ToString(REPORT_DATEFORMAT))}, pStatusBar)
    Catch ex As Exception
    End Try
  End Function

  ''' <summary>
  ''' RPTs the FX rates.
  ''' </summary>
  ''' <param name="CurrencyID">The currency ID.</param>
  ''' <param name="pDateFrom">The p date from.</param>
  ''' <param name="pDateTo">The p date to.</param>
  ''' <param name="pStatusBar">The p status bar.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function rptFXRates(ByVal CurrencyID As Integer, ByVal pDateFrom As Date, ByVal pDateTo As Date, ByRef pStatusBar As ToolStripProgressBar) As Boolean
    ' ************************************************************************
    '
    ' ************************************************************************

    Dim ReportData As DataTable

    ' Get report data.

    ReportData = GetData_rptFX(CurrencyID, pDateFrom, pDateTo)

    If (ReportData Is Nothing) Then
      Exit Function
    End If

    Try
      DisplayReport("rptFXRates", ReportData, 0, New classReportFieldValue() {New classReportFieldValue("RptFromDate", pDateFrom.ToString(REPORT_DATEFORMAT)), New classReportFieldValue("RptToDate", pDateTo.ToString(REPORT_DATEFORMAT)), New classReportFieldValue("Text_KnowledgeDate", MainForm.Main_Knowledgedate.ToString(REPORT_DATEFORMAT))}, pStatusBar)
    Catch ex As Exception
    End Try
  End Function


  ''' <summary>
  ''' RPTs the master fund allocation.
  ''' </summary>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pFundName">Name of the p fund.</param>
  ''' <param name="pReportDate">The p report date.</param>
  ''' <param name="pStatusGroupFilter">The p status group filter.</param>
  ''' <param name="pAdministratorDatesFilter">The p administrator dates filter.</param>
  ''' <param name="pKnowledgeDateFrom">The p knowledge date from.</param>
  ''' <param name="pKnowledgeDateTo">The p knowledge date to.</param>
  ''' <param name="pKnowledgeDateLinkedTables">The p knowledge date linked tables.</param>
  ''' <param name="pStatusBar">The p status bar.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function rptMasterFundAllocation( _
  ByVal pFundID As Integer, ByVal pFundName As String, ByVal pReportDate As Date, ByVal pStatusGroupFilter As String, ByVal pAdministratorDatesFilter As Integer, ByVal pKnowledgeDateFrom As Date, ByVal pKnowledgeDateTo As Date, ByVal pKnowledgeDateLinkedTables As Date, ByRef pStatusBar As ToolStripProgressBar) As Boolean
    ' *******************************************************************************************************
    '
    '
    '
    ' *******************************************************************************************************

    Dim ReportData As DataTable
    Dim ThisRow As DataRow
    Dim FundAllocationReport As C1Report

    Try

      ' Retrieve data underlying the Report

      ReportData = GetData_rptMasterFundAllocation(pFundID, pReportDate, pStatusGroupFilter, pAdministratorDatesFilter, pKnowledgeDateFrom, pKnowledgeDateTo, pKnowledgeDateLinkedTables)
      FundAllocationReport = Me.GetReportDefinition(pFundID, "rptMasterFundAllocation")

      If (ReportData Is Nothing) Then
        MainForm.LogError("ReportHandler, rptMasterFundAllocation()", LOG_LEVELS.Warning, "", "Failed to get report data.", "", True)
        Return False
        Exit Function
      End If
      If (FundAllocationReport Is Nothing) Then
        MainForm.LogError("ReportHandler, rptMasterFundAllocation()", LOG_LEVELS.Warning, "", "Failed to get report definition.", "", True)
        Return False
        Exit Function
      End If

      ' Populate Report Fields.

      Dim RowData As String
      Dim MainFundData As String
      Dim Fund1Data As String
      Dim Fund2Data As String
      Dim Fund3Data As String

      ' Iterate through the returned rows, setting report fields as appropriate
      MainFundData = "0"
      Fund1Data = "0"
      Fund2Data = "0"
      Fund3Data = "0"

      For Each ThisRow In ReportData.Rows
        Try
          RowData = CStr(ThisRow("RowData"))
          MainFundData = CStr(ThisRow("MainFund"))

          If (Not ThisRow.IsNull("Fund1")) Then
            Fund1Data = CStr(ThisRow("Fund1"))
          Else
            Fund1Data = ""
          End If

          If (Not ThisRow.IsNull("Fund2")) Then
            Fund2Data = CStr(ThisRow("Fund2"))
          Else
            Fund2Data = ""
          End If
          If (Not ThisRow.IsNull("Fund3")) Then
            Fund3Data = CStr(ThisRow("Fund3"))
          Else
            Fund3Data = ""
          End If

        Catch ex As Exception
          MainForm.LogError("ReportHandler, rptMasterFundAllocation()", LOG_LEVELS.Error, ex.Message, "Error getting field Values. ThisRow=" & ThisRow.ToString, ex.StackTrace, True)
          RowData = ""
        End Try

        Try
          If (Not (RowData Is Nothing)) AndAlso (RowData.Length > 0) Then
            Select Case RowData.ToUpper

              Case "FUNDID"
                If (IsNumeric(Fund1Data) = False) OrElse (CInt(Fund1Data) = 0) Then
                  Call MainForm.LogError("rptMasterFundAllocation", RenaissanceGlobals.LOG_LEVELS.Warning, "", "No sub-funds exist.", "", True)
                  Return False
                  Exit Function
                End If

                If (IsNumeric(Fund2Data) = False) OrElse (CInt(Fund2Data) = 0) Then
                  Try
                    FundAllocationReport.Fields("Text_SubFund2").Visible = False
                    FundAllocationReport.Fields("Text_Fund2_MasterShares").Visible = False
                    FundAllocationReport.Fields("Text_Fund2_StartAllocation").Visible = False
                    FundAllocationReport.Fields("Text_Fund2_A").Visible = False
                    FundAllocationReport.Fields("Text_Fund2_StartLocalNAV").Visible = False
                    FundAllocationReport.Fields("Text_Fund2_AdminFee").Visible = False
                    FundAllocationReport.Fields("Text_Fund2_ManagementFee").Visible = False
                    FundAllocationReport.Fields("Text_Fund2_PerformanceFee").Visible = False
                    FundAllocationReport.Fields("Text_Fund2_EqualisationFee").Visible = False
                    FundAllocationReport.Fields("Text_Fund2_Hedging").Visible = False
                    FundAllocationReport.Fields("Text_Fund2_WriteOff").Visible = False
                    FundAllocationReport.Fields("Text_Fund2_B").Visible = False
                    FundAllocationReport.Fields("Text_Fund2_PreviousPerformanceFee").Visible = False
                    FundAllocationReport.Fields("Text_Fund2_PreviousEqualisationFee").Visible = False
                    FundAllocationReport.Fields("Text_Fund2_C").Visible = False
                    FundAllocationReport.Fields("Text_Fund2_NetAssets").Visible = False
                    FundAllocationReport.Fields("Text_Fund2_SharesInIssue").Visible = False
                    FundAllocationReport.Fields("Text_Fund2_EndUSDperShare").Visible = False
                    FundAllocationReport.Fields("Text_Fund2_FX").Visible = False
                    FundAllocationReport.Fields("Text_Fund2_EndLocalNAV").Visible = False
                    FundAllocationReport.Fields("Text_Fund2_EndLocalNAVpershare").Visible = False
                    FundAllocationReport.Fields("Text_Fund2_ShareTrade").Visible = False
                    FundAllocationReport.Fields("Text_Fund2_NewMasterShares").Visible = False
                    FundAllocationReport.Fields("Text_Fund2_EndAllocation").Visible = False

                  Catch ex As Exception
                  End Try
                End If

                If (IsNumeric(Fund3Data) = False) OrElse (CInt(Fund3Data) = 0) Then
                  Try
                    FundAllocationReport.Fields("Text_SubFund3").Visible = False
                    FundAllocationReport.Fields("Text_Fund3_MasterShares").Visible = False
                    FundAllocationReport.Fields("Text_Fund3_StartAllocation").Visible = False
                    FundAllocationReport.Fields("Text_Fund3_A").Visible = False
                    FundAllocationReport.Fields("Text_Fund3_StartLocalNAV").Visible = False
                    FundAllocationReport.Fields("Text_Fund3_AdminFee").Visible = False
                    FundAllocationReport.Fields("Text_Fund3_ManagementFee").Visible = False
                    FundAllocationReport.Fields("Text_Fund3_PerformanceFee").Visible = False
                    FundAllocationReport.Fields("Text_Fund3_EqualisationFee").Visible = False
                    FundAllocationReport.Fields("Text_Fund3_Hedging").Visible = False
                    FundAllocationReport.Fields("Text_Fund3_WriteOff").Visible = False
                    FundAllocationReport.Fields("Text_Fund3_B").Visible = False
                    FundAllocationReport.Fields("Text_Fund3_PreviousPerformanceFee").Visible = False
                    FundAllocationReport.Fields("Text_Fund3_PreviousEqualisationFee").Visible = False
                    FundAllocationReport.Fields("Text_Fund3_C").Visible = False
                    FundAllocationReport.Fields("Text_Fund3_NetAssets").Visible = False
                    FundAllocationReport.Fields("Text_Fund3_SharesInIssue").Visible = False
                    FundAllocationReport.Fields("Text_Fund3_EndUSDperShare").Visible = False
                    FundAllocationReport.Fields("Text_Fund3_FX").Visible = False
                    FundAllocationReport.Fields("Text_Fund3_EndLocalNAV").Visible = False
                    FundAllocationReport.Fields("Text_Fund3_EndLocalNAVpershare").Visible = False
                    FundAllocationReport.Fields("Text_Fund3_ShareTrade").Visible = False
                    FundAllocationReport.Fields("Text_Fund3_NewMasterShares").Visible = False
                    FundAllocationReport.Fields("Text_Fund3_EndAllocation").Visible = False

                  Catch ex As Exception
                  End Try
                End If

              Case "FUNDCODE"
                FundAllocationReport.Fields("Label_FundName").Text = "=" & Chr(34) & MainFundData & Chr(34)
                FundAllocationReport.Fields("Label_FundName").Calculated = True
                FundAllocationReport.Fields("Text_MasterFund").Text = "=" & Chr(34) & MainFundData & Chr(34)
                FundAllocationReport.Fields("Text_SubFund1").Text = "=" & Chr(34) & Fund1Data & Chr(34)
                FundAllocationReport.Fields("Text_SubFund2").Text = "=" & Chr(34) & Fund2Data & Chr(34)
                FundAllocationReport.Fields("Text_SubFund3").Text = "=" & Chr(34) & Fund3Data & Chr(34)

              Case "STARTMASTERUNITS"  ' 0
                FundAllocationReport.Fields("Text_MasterFund_MasterShares").Text = "=" & MainFundData
                FundAllocationReport.Fields("Text_Fund1_MasterShares").Text = "=" & Fund1Data
                FundAllocationReport.Fields("Text_Fund2_MasterShares").Text = "=" & Fund2Data
                FundAllocationReport.Fields("Text_Fund3_MasterShares").Text = "=" & Fund3Data

              Case "LASTMONTH_STARTMASTERUNITS"   ' 0
                FundAllocationReport.Fields("Text_MasterFund_PriorMonthPre").Text = "=" & MainFundData
                FundAllocationReport.Fields("Text_Fund1_PriorMonthPre").Text = "=" & Fund1Data
                FundAllocationReport.Fields("Text_Fund2_PriorMonthPre").Text = "=" & Fund2Data
                FundAllocationReport.Fields("Text_Fund3_PriorMonthPre").Text = "=" & Fund3Data

              Case "LASTMONTH_SHARETRANSACTIONREQUIRED"   ' 0
                FundAllocationReport.Fields("Text_Fund1_PriorMonthClassSpecific").Text = "=" & Fund1Data
                FundAllocationReport.Fields("Text_Fund2_PriorMonthClassSpecific").Text = "=" & Fund2Data
                FundAllocationReport.Fields("Text_Fund3_PriorMonthClassSpecific").Text = "=" & Fund3Data

              Case "MASTERFUNDSUBSCRIPTIONSTHISMONTH"
                FundAllocationReport.Fields("Text_MasterFund_SR_ThisMonth").Text = "=" & MainFundData
                FundAllocationReport.Fields("Text_Fund1_SR_ThisMonth").Text = "=" & Fund1Data & "- Text_Fund1_PriorMonthClassSpecific.Value"
                FundAllocationReport.Fields("Text_Fund2_SR_ThisMonth").Text = "=" & Fund2Data & "- Text_Fund2_PriorMonthClassSpecific.Value"
                FundAllocationReport.Fields("Text_Fund3_SR_ThisMonth").Text = "=" & Fund3Data & "- Text_Fund3_PriorMonthClassSpecific.Value"

                FundAllocationReport.Fields("Text_MasterFund_InitialSharesError").Text = "=Text_MasterFund_MasterShares.Value - (Text_MasterFund_PriorMonthPre.Value & " & MainFundData & ")"
                FundAllocationReport.Fields("Text_Fund1_InitialSharesError").Text = "=Text_Fund1_MasterShares.Value -(Text_Fund1_PriorMonthPre.Value +" & Fund1Data & ")"
                FundAllocationReport.Fields("Text_Fund2_InitialSharesError").Text = "=Text_Fund2_MasterShares.Value -(Text_Fund2_PriorMonthPre.Value +" & Fund2Data & ")"
                FundAllocationReport.Fields("Text_Fund3_InitialSharesError").Text = "=Text_Fund3_MasterShares.Value -(Text_Fund3_PriorMonthPre.Value +" & Fund3Data & ")"

              Case "FUNDUNITSINISSUE"
                FundAllocationReport.Fields("Text_Fund1_SharesInIssue").Text = "=" & Fund1Data
                FundAllocationReport.Fields("Text_Fund2_SharesInIssue").Text = "=" & Fund2Data
                FundAllocationReport.Fields("Text_Fund3_SharesInIssue").Text = "=" & Fund3Data

              Case "FUNDNETASSETS"
                FundAllocationReport.Fields("Text_Fund1_NetAssets").Text = "=" & Fund1Data
                FundAllocationReport.Fields("Text_Fund2_NetAssets").Text = "=" & Fund2Data
                FundAllocationReport.Fields("Text_Fund3_NetAssets").Text = "=" & Fund3Data

              Case "FUNDUNITPRICE"
                FundAllocationReport.Fields("Text_MasterFundUnitPrice").Text = "=" & MainFundData

              Case "FUNDFXRATES"
                FundAllocationReport.Fields("Text_Fund1_FX").Text = "=" & Fund1Data
                FundAllocationReport.Fields("Text_Fund2_FX").Text = "=" & Fund2Data
                FundAllocationReport.Fields("Text_Fund3_FX").Text = "=" & Fund3Data

              Case "ADMINISTRATOR FEES"
                FundAllocationReport.Fields("Text_Fund1_AdminFee").Text = "=" & Fund1Data
                FundAllocationReport.Fields("Text_Fund2_AdminFee").Text = "=" & Fund2Data
                FundAllocationReport.Fields("Text_Fund3_AdminFee").Text = "=" & Fund3Data

              Case "MANAGEMENT FEES PROFIT"
                FundAllocationReport.Fields("Text_Fund1_ManagementFee").Text = "=" & Fund1Data
                FundAllocationReport.Fields("Text_Fund2_ManagementFee").Text = "=" & Fund2Data
                FundAllocationReport.Fields("Text_Fund3_ManagementFee").Text = "=" & Fund3Data

              Case "DISTRIBUTION FEES PROFIT"
                If (FundAllocationReport.Fields.Contains("Text_Fund1_DistributionFee")) Then
                  If Not IsNumeric(Fund1Data) Then
                    Fund1Data = "0"
                  End If
                  If Not IsNumeric(Fund2Data) Then
                    Fund2Data = "0"
                  End If
                  If Not IsNumeric(Fund3Data) Then
                    Fund3Data = "0"
                  End If

                  SetFieldValue(FundAllocationReport, New classReportFieldValue("Text_Fund1_DistributionFee", "=" & Fund1Data, "Text"))
                  SetFieldValue(FundAllocationReport, New classReportFieldValue("Text_Fund2_DistributionFee", "=" & Fund2Data, "Text"))
                  SetFieldValue(FundAllocationReport, New classReportFieldValue("Text_Fund3_DistributionFee", "=" & Fund3Data, "Text"))
                End If

              Case "PERFORMANCE FEES PROFIT"
                FundAllocationReport.Fields("Text_Fund1_PreviousPerformanceFee").Text = "=Text_Fund1_PerformanceFee.Value - " & Fund1Data
                FundAllocationReport.Fields("Text_Fund2_PreviousPerformanceFee").Text = "=Text_Fund2_PerformanceFee.Value - " & Fund2Data
                FundAllocationReport.Fields("Text_Fund3_PreviousPerformanceFee").Text = "=Text_Fund3_PerformanceFee.Value - " & Fund3Data

              Case "PERFORMANCE FEES VALUE"
                FundAllocationReport.Fields("Text_Fund1_PerformanceFee").Text = "=" & Fund1Data
                FundAllocationReport.Fields("Text_Fund2_PerformanceFee").Text = "=" & Fund2Data
                FundAllocationReport.Fields("Text_Fund3_PerformanceFee").Text = "=" & Fund3Data

              Case "EQUALISATION FACTOR PROFIT"
                FundAllocationReport.Fields("Text_Fund1_PreviousEqualisationFee").Text = "=Text_Fund1_EqualisationFee.Value - " & Fund1Data
                FundAllocationReport.Fields("Text_Fund2_PreviousEqualisationFee").Text = "=Text_Fund2_EqualisationFee.Value - " & Fund2Data
                FundAllocationReport.Fields("Text_Fund3_PreviousEqualisationFee").Text = "=Text_Fund3_EqualisationFee.Value - " & Fund3Data

              Case "EQUALISATION FACTOR VALUE"
                FundAllocationReport.Fields("Text_Fund1_EqualisationFee").Text = "=" & Fund1Data
                FundAllocationReport.Fields("Text_Fund2_EqualisationFee").Text = "=" & Fund2Data
                FundAllocationReport.Fields("Text_Fund3_EqualisationFee").Text = "=" & Fund3Data

              Case "WRITE OFF PROFIT"
                FundAllocationReport.Fields("Text_Fund1_WriteOff").Text = "=" & Fund1Data
                FundAllocationReport.Fields("Text_Fund2_WriteOff").Text = "=" & Fund2Data
                FundAllocationReport.Fields("Text_Fund3_WriteOff").Text = "=" & Fund3Data

              Case Else


            End Select
          End If


        Catch ex As Exception
          Dim ErrorMessage As String
          Try
            ErrorMessage = "Error setting field Values. RowData=`" & RowData.ToString & "`"
          Catch ex1 As Exception
            ErrorMessage = "Error setting field Values."
          End Try

          MainForm.LogError("ReportHandler, rptMasterFundAllocation()", LOG_LEVELS.Error, ex.Message, ErrorMessage, ex.StackTrace, True)
          Return False
          Exit Function
        End Try

      Next


      ' Set Fund Name 

      Try
        FundAllocationReport.Fields("Label_FundName").Text = pFundName
      Catch ex As Exception
      End Try

      ' Set Report Date

      Try
        FundAllocationReport.Fields(REPORT_ValueDate_FieldName).Text = pReportDate.ToString(REPORT_DATEFORMAT)
      Catch ex As Exception
      End Try

      Dim KDMessage As String = ""


      ' Set Knowledgedate Comment

      Try
        If (pKnowledgeDateFrom.CompareTo(KNOWLEDGEDATE_NOW) <= 0) AndAlso (pKnowledgeDateTo.CompareTo(KNOWLEDGEDATE_NOW) <= 0) Then
          KDMessage = "Start and Finish KDs are LIVE"
        Else
          If (pKnowledgeDateFrom.CompareTo(KNOWLEDGEDATE_NOW) <= 0) Then
            KDMessage = "Start KD is LIVE"
          ElseIf pKnowledgeDateFrom.TimeOfDay.TotalSeconds >= LAST_SECOND Then
            KDMessage = "Start KD is " & pKnowledgeDateFrom.ToString(REPORT_DATEFORMAT)
          Else
            KDMessage = "Start KD is " & pKnowledgeDateFrom.ToString(REPORT_LONGDATEFORMAT)
          End If

          If (pKnowledgeDateTo.CompareTo(KNOWLEDGEDATE_NOW) <= 0) Then
            KDMessage &= ", End KD is LIVE"
          ElseIf pKnowledgeDateTo.TimeOfDay.TotalSeconds >= LAST_SECOND Then
            KDMessage &= ", End KD is " & pKnowledgeDateTo.ToString(REPORT_DATEFORMAT)
          Else
            KDMessage &= ", End KD is " & pKnowledgeDateTo.ToString(REPORT_LONGDATEFORMAT)
          End If
        End If

        If (pKnowledgeDateLinkedTables.CompareTo(KNOWLEDGEDATE_NOW) <= 0) Then
          KDMessage &= ". Linked Table KD is LIVE"
        ElseIf pKnowledgeDateLinkedTables.TimeOfDay.TotalSeconds >= LAST_SECOND Then
          KDMessage &= ". Linked Table KD is " & pKnowledgeDateLinkedTables.ToString(REPORT_DATEFORMAT)
        Else
          KDMessage &= ". Linked Table KD is " & pKnowledgeDateLinkedTables.ToString(REPORT_LONGDATEFORMAT)
        End If
      Catch ex As Exception
      End Try

      Try
        FundAllocationReport.Fields("Text_KDMessage").Text = KDMessage
      Catch ex As Exception
      End Try

      ' Display Report

      Call DisplayReport(FundAllocationReport, pStatusBar)

    Catch ex As Exception
      MainForm.LogError("ReportHandler, rptMasterFundAllocation()", LOG_LEVELS.Error, ex.Message, "Error generating Master Fund Allocation Report.", ex.StackTrace, True)
      Return False
    End Try

    Return True

  End Function


  ''' <summary>
  ''' RPTs the risk exposures report.
  ''' </summary>
  ''' <param name="ReportData">The report data.</param>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pDetailedExposuresReport">The p detailed exposures report.</param>
  ''' <param name="pExposureDate">The p exposure date.</param>
  ''' <param name="pExposureLimitCategory">The p exposure limit category.</param>
  ''' <param name="pExposureLimitInstrument">The p exposure limit instrument.</param>
  ''' <param name="pExposureLimitMinUsage">The p exposure limit min usage.</param>
  ''' <param name="pExposureFlags">The p exposure flags.</param>
  ''' <param name="pExposureKnowledgeDate">The p exposure knowledge date.</param>
  ''' <param name="pKnowledgeDate">The p knowledge date.</param>
  ''' <param name="pCombineTheExposuresReports">if set to <c>true</c> [p combine the exposures reports].</param>
  ''' <param name="pStatusBar">The p status bar.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function rptRiskExposuresReport(ByRef ReportData As DataTable, _
   ByVal pFundID As Integer, _
   ByVal pDetailedExposuresReport As Integer, _
   ByVal pExposureDate As Date, _
   ByVal pExposureLimitCategory As Integer, _
   ByVal pExposureLimitInstrument As Integer, _
   ByVal pExposureLimitMinUsage As Double, _
   ByVal pExposureFlags As Integer, _
   ByVal pExposureKnowledgeDate As Date, _
   ByVal pKnowledgeDate As Date, ByVal pCombineTheExposuresReports As Boolean, ByRef pStatusBar As ToolStripProgressBar) As Boolean

    ' ************************************************************************
    '
    '
    ' ************************************************************************

    Dim thisReport As C1Report

    ' Get report definition.

    If (pDetailedExposuresReport <> 0) Then
      thisReport = GetReportDefinition(pFundID, "rptRiskExposuresDetailReport")
    Else
      thisReport = GetReportDefinition(pFundID, "rptRiskExposuresReport")
    End If

    If (thisReport Is Nothing) Then
      Exit Function
    End If

    ' If is a combined report, modify report definition.

    If pCombineTheExposuresReports Then
      'thisReport.Groups("LimitType").SectionHeader.Visible = False
    End If

    ' Apply the given datasource to the C1Report object.

    Try
      thisReport.DataSource.Recordset = ReportData
    Catch ex As Exception
      Exit Function
    End Try

    ' Apply ValueDate and KnowledgeDate
    Try
      thisReport.Fields(REPORT_ValueDate_FieldName).Text = pExposureDate.ToString(REPORT_DATEFORMAT)
    Catch ex As Exception
    End Try

    ' Display the populated C1Report in a preview window.

    Try
      DisplayReport(thisReport, pStatusBar)

      SaveReportData(ReportData, thisReport.ReportName)

    Catch ex As Exception
    End Try
  End Function



  ''' <summary>
  ''' RPTs the risk exposures change report.
  ''' </summary>
  ''' <param name="ReportData">The report data.</param>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pExposureStartDate">The p exposure start date.</param>
  ''' <param name="pExposureEndDate">The p exposure end date.</param>
  ''' <param name="pExposureLimitCategory">The p exposure limit category.</param>
  ''' <param name="pExposureLimitInstrument">The p exposure limit instrument.</param>
  ''' <param name="pExposureLimitMinUsage">The p exposure limit min usage.</param>
  ''' <param name="pExposureStartKnowledgeDate">The p exposure start knowledge date.</param>
  ''' <param name="pExposureEndKnowledgeDate">The p exposure end knowledge date.</param>
  ''' <param name="pKnowledgeDate">The p knowledge date.</param>
  ''' <param name="pStatusBar">The p status bar.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function rptRiskExposuresChangeReport(ByRef ReportData As DataTable, _
  ByVal pFundID As Integer, _
  ByVal pExposureStartDate As Date, _
  ByVal pExposureEndDate As Date, _
  ByVal pExposureLimitCategory As Integer, _
  ByVal pExposureLimitInstrument As Integer, _
  ByVal pExposureLimitMinUsage As Double, _
  ByVal pExposureStartKnowledgeDate As Date, _
  ByVal pExposureEndKnowledgeDate As Date, _
  ByVal pKnowledgeDate As Date, ByRef pStatusBar As ToolStripProgressBar) As Boolean

    ' ************************************************************************

    Dim thisReport As C1Report

    ' Get report definition.

    thisReport = GetReportDefinition(pFundID, "rptRiskExposuresChangeReport")

    If (thisReport Is Nothing) Then
      Exit Function
    End If

    ' Apply the given datasource to the C1Report object.

    Try
      thisReport.DataSource.Recordset = ReportData
    Catch ex As Exception
      Exit Function
    End Try

    ' Apply ValueDate and KnowledgeDate
    Try
      If (thisReport.Fields.Contains("Text_StartValueDate")) Then
        thisReport.Fields("Text_StartValueDate").Text = pExposureStartDate.ToString(REPORT_DATEFORMAT)
      End If
      If (thisReport.Fields.Contains("Text_EndValueDate")) Then
        thisReport.Fields("Text_EndValueDate").Text = pExposureEndDate.ToString(REPORT_DATEFORMAT)
      End If
    Catch ex As Exception
    End Try

    ' Display the populated C1Report in a preview window.

    Try
      DisplayReport(thisReport, pStatusBar)

      SaveReportData(ReportData, thisReport.ReportName)

    Catch ex As Exception
    End Try
  End Function




  ''' <summary>
  ''' RPTs the risk policy.
  ''' </summary>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pStatusBar">The p status bar.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function rptRiskPolicy(ByRef pFundID As Integer, ByRef pStatusBar As ToolStripProgressBar) As Boolean
    ' **************************************************************************************
    '
    '
    ' **************************************************************************************

    ' Configure Adaptor

    Dim rptAdaptor As New SqlDataAdapter
    Dim rptTable As New DataTable

    Try

      MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.GetVeniceConnection, rptAdaptor, "rptRiskPolicy")
      If (rptAdaptor.SelectCommand.Connection Is Nothing) Then
        Exit Function
      End If

      rptAdaptor.SelectCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate
      rptAdaptor.SelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      SyncLock rptAdaptor.SelectCommand.Connection
        rptAdaptor.Fill(rptTable)
      End SyncLock

    Catch ex As Exception
      MainForm.LogError("ReportHandler, rptRiskPolicy()", LOG_LEVELS.Error, ex.Message, "Error getting 'rptRiskPolicy' data.", ex.StackTrace, True)
      Return False

    Finally
      ' Close Connection
      Try
        If (Not (rptAdaptor.SelectCommand Is Nothing)) AndAlso (Not (rptAdaptor.SelectCommand.Connection Is Nothing)) Then
          rptAdaptor.SelectCommand.Connection.Close()
        End If
      Catch ex As Exception
      End Try
    End Try

    ' Show report

    If (pFundID = 0) Then
      Call DisplayReport(0, "rptRiskPolicy", rptTable, pStatusBar)
    Else
      Call DisplayReport(0, "rptRiskPolicy", New DataView(rptTable, "limFund=" & pFundID.ToString, "ltpSortOrder", DataViewRowState.CurrentRows), pStatusBar)
    End If

    Return True

  End Function


  ''' <summary>
  ''' RPTs the view portfolio.
  ''' </summary>
  ''' <param name="pPortfolioID">The p portfolio ID.</param>
  ''' <param name="FundID">The fund ID.</param>
  ''' <param name="pStatusGroupFilter">The p status group filter.</param>
  ''' <param name="pAdministratorDatesFilter">The p administrator dates filter.</param>
  ''' <param name="pStatusBar">The p status bar.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function rptViewPortfolio(ByVal pPortfolioID As Integer, ByVal FundID As Integer, ByVal pStatusGroupFilter As String, ByVal pAdministratorDatesFilter As Integer, ByRef pStatusBar As ToolStripProgressBar) As Boolean

    ' ************************************************************************

    Dim ReportData As DataTable

    ' Get report data.

    ReportData = GetData_rptViewPortfolio(pPortfolioID, FundID, pStatusGroupFilter, pAdministratorDatesFilter)

    If (ReportData Is Nothing) Then
      Exit Function
    End If

    Try
      DisplayReport(0, "rptViewPortfolio", ReportData, pStatusBar)
    Catch ex As Exception
    End Try
  End Function


  ''' <summary>
  ''' RPTs the view portfolio change.
  ''' </summary>
  ''' <param name="pStartPortfolioName">Name of the p start portfolio.</param>
  ''' <param name="pEndPortfolioName">Name of the p end portfolio.</param>
  ''' <param name="pStartPortfolioID">The p start portfolio ID.</param>
  ''' <param name="pEndPortfolioID">The p end portfolio ID.</param>
  ''' <param name="pStatusBar">The p status bar.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function rptViewPortfolioChange(ByVal pStartPortfolioName As String, ByVal pEndPortfolioName As String, ByVal pStartPortfolioID As Integer, ByVal pEndPortfolioID As Integer, ByRef pStatusBar As ToolStripProgressBar) As Boolean

    ' ************************************************************************

    Dim ReportData As DataTable

    ' Get report data.

    ReportData = GetData_rptViewPortfolioChange(pStartPortfolioID, pEndPortfolioID)

    If (ReportData Is Nothing) Then
      Exit Function
    End If

    'thisReport = Me.GetReportDefinition(0, "rptViewPortfolioChange")
    'If (thisReport Is Nothing) Then
    '  Return False
    'End If

    'thisReport.Fields("").Text = pStartPortfolioName
    'thisReport.Fields("").Text = pEndPortfolioName

    'thisReport.DataSource.Recordset = ReportData

    Try
      DisplayReport(0, "rptViewPortfolioChange", ReportData, pStatusBar)
    Catch ex As Exception
    End Try

  End Function

  ''' <summary>
  ''' RPTs the pending transactions report.
  ''' </summary>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pValueDate">The p value date.</param>
  ''' <param name="pShowValue">if set to <c>true</c> [p show value].</param>
  ''' <param name="pAddDaysToMonthEnd">The p add days to month end.</param>
  ''' <param name="pAggregateToParent">if set to <c>true</c> [p aggregate to parent].</param>
  ''' <param name="pAnthonyLookthroughReport">if set to <c>true</c> [p anthony lookthrough report].</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function rptPendingTransactionsReport(ByVal pFundID As Integer, ByVal pValueDate As Date, ByVal pShowValue As Boolean, ByVal pAddDaysToMonthEnd As Integer, ByVal pAggregateToParent As Boolean, ByVal pAnthonyLookthroughReport As Boolean) As Boolean
    ' ************************************************************************
    ' Orchestrate the rptPendingTransactionsReport.
    ' Get Data, Set Report fields, Display report.
    '
    ' NPP Jul 2008
    ' ************************************************************************
    Dim RVal As Boolean = False

    Try
      Dim ReportData As DataTable

      If Not pAnthonyLookthroughReport Then
        ReportData = MainForm.MainReportHandler.GetData_PendingTransactionsReport(pFundID, pValueDate, pShowValue, pAddDaysToMonthEnd, pAggregateToParent)

        If (Not (ReportData Is Nothing)) Then
          Dim thisReport As C1Report

          ' Get report definition.

          If (pShowValue) Then
            thisReport = MainForm.MainReportHandler.GetReportDefinition(pFundID, "rptPendingTransactionsReportValue", "rptPendingTransactionsReportValue")
          Else
            thisReport = MainForm.MainReportHandler.GetReportDefinition(pFundID, "rptPendingTransactionsReport", "rptPendingTransactionsReport")
          End If

          Try

            Call SetFieldValue(thisReport, New classReportFieldValue("Label_M0", "to end " & pValueDate.ToString("MMM")))
            Call SetFieldValue(thisReport, New classReportFieldValue("Label_M1", pValueDate.AddMonths(1).ToString("MMM yy")))
            Call SetFieldValue(thisReport, New classReportFieldValue("Label_M2", pValueDate.AddMonths(2).ToString("MMM yy")))
            Call SetFieldValue(thisReport, New classReportFieldValue("Label_M3", pValueDate.AddMonths(3).ToString("MMM yy")))
            Call SetFieldValue(thisReport, New classReportFieldValue("Label_M4", pValueDate.AddMonths(4).ToString("MMM yy")))
            Call SetFieldValue(thisReport, New classReportFieldValue("Label_M5", pValueDate.AddMonths(5).ToString("MMM yy")))
            Call SetFieldValue(thisReport, New classReportFieldValue("Label_M6Plus", "Thereafter"))

          Catch ex As Exception
          End Try

          Call MainForm.MainReportHandler.DisplayReport(thisReport, ReportData, pValueDate, MainForm.Main_Knowledgedate, Nothing)

          RVal = True

        End If

      Else

        ReportData = MainForm.MainReportHandler.GetData_PendingTransactionsAndValuesReport(pFundID, pValueDate, pShowValue, pAddDaysToMonthEnd, True, pAggregateToParent)

        If (Not (ReportData Is Nothing)) Then
          Dim thisReport As C1Report

          ' Get report definition.

          thisReport = MainForm.MainReportHandler.GetReportDefinition(pFundID, "rptPendingTransactionsReportLookthrough", "rptPendingTransactionsReportLookthrough")

          Try
            Dim TempDate As Date
            ' pAddDaysToMonthEnd
            TempDate = pValueDate

            TempDate = pValueDate

            '	Call SetFieldValue(thisReport, New classReportFieldValue("Label_M0", "to end " & TempDate.ToString("MMM")))

            If (TempDate.Month <> TempDate.AddDays(1).Month) AndAlso (pAddDaysToMonthEnd <= 0) Then
              TempDate = TempDate.AddMonths(1)
            End If

            Call SetFieldValue(thisReport, New classReportFieldValue("Label_M0", TempDate.ToString("MMM yy")))
            Call SetFieldValue(thisReport, New classReportFieldValue("Label_M1", TempDate.AddMonths(1).ToString("MMM yy")))
            Call SetFieldValue(thisReport, New classReportFieldValue("Label_M2", TempDate.AddMonths(2).ToString("MMM yy")))
            Call SetFieldValue(thisReport, New classReportFieldValue("Label_M3", TempDate.AddMonths(3).ToString("MMM yy")))
            Call SetFieldValue(thisReport, New classReportFieldValue("Label_M4", TempDate.AddMonths(4).ToString("MMM yy")))

          Catch ex As Exception
          End Try

          Call MainForm.MainReportHandler.DisplayReport(thisReport, ReportData, pValueDate, MainForm.Main_Knowledgedate, Nothing)

          RVal = True

        Else

          MainForm.LogError("ReportHandler(), rptPendingTransactionsReport", LOG_LEVELS.Error, "", "Error, failed to get report definition 'rptPendingTransactionsReportLookthrough'", "", True)
          RVal = False

        End If

      End If

    Catch ex As Exception
      MainForm.LogError("ReportHandler(), rptPendingTransactionsReport", LOG_LEVELS.Error, ex.Message, "Error running rptPendingTransactionsReport", ex.StackTrace, True)
      RVal = False
    End Try

    Return RVal

  End Function

  ''' <summary>
  ''' RPTs the table audit report.
  ''' </summary>
  ''' <param name="thisReportTable">The this report table.</param>
  ''' <param name="pStatusBar">The p status bar.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function rptTableAuditReport(ByRef thisReportTable As DataTable, ByRef pStatusBar As ToolStripProgressBar) As Boolean
    ' ************************************************************************
    '
    ' ************************************************************************

    Try
      DisplayReport(0, "rptTableAuditReport", thisReportTable, pStatusBar)
    Catch ex As Exception
    End Try

  End Function

  ''' <summary>
  ''' RPTs the trade blotter report.
  ''' </summary>
  ''' <param name="UpFrontFeeFunds">Up front fee funds.</param>
  ''' <param name="TradeBlotterTicketNumber">The trade blotter ticket number.</param>
  ''' <param name="thisReportTable">The this report table.</param>
  ''' <param name="pFinal">if set to <c>true</c> [p final].</param>
  ''' <param name="pStatusBar">The p status bar.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function rptTradeBlotterReport(ByVal UpFrontFeeFunds As String, ByVal TradeBlotterTicketNumber As String, ByRef thisReportTable As DataView, ByVal pFinal As Boolean, ByRef pStatusBar As ToolStripProgressBar) As Boolean
    ' ************************************************************************
    '
    ' ************************************************************************
    Dim rptTradeblotter As C1.C1Report.C1Report

    If (pFinal) Then
      SaveReportDetails("rptTransactionTradeBlotter", TradeBlotterTicketNumber, thisReportTable, "TransactionID")
    End If

    rptTradeblotter = Me.GetReportDefinition(0, "rptTransactionTradeBlotter")

    Try
      If (rptTradeblotter.Fields.Contains("Text_TicketReference")) Then
        rptTradeblotter.Fields("Text_TicketReference").Text = "Ref : TB" & TradeBlotterTicketNumber
      End If
      If (rptTradeblotter.Fields.Contains("Text_AdminField3")) Then
        rptTradeblotter.Fields("Text_AdminField3").Text = rptTradeblotter.Fields("Text_AdminField3").Text & UpFrontFeeFunds & "."
      End If
    Catch ex As Exception
    End Try

    Try
      DisplayReport(rptTradeblotter, thisReportTable, Now.Date, MainForm.Main_Knowledgedate, pStatusBar)
    Catch ex As Exception
    End Try

  End Function

  ''' <summary>
  ''' RPTs the trade blotter report_ UCITS.
  ''' </summary>
  ''' <param name="ReportFile">The report file.</param>
  ''' <param name="TradeBlotterTicketNumber">The trade blotter ticket number.</param>
  ''' <param name="thisReportTable">The this report table.</param>
  ''' <param name="pFinal">if set to <c>true</c> [p final].</param>
  ''' <param name="pStatusBar">The p status bar.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function rptTradeBlotterReport_UCITS(ByVal ReportFile As String, ByVal TradeBlotterTicketNumber As String, ByRef thisReportTable As DataView, ByVal pFinal As Boolean, ByRef pStatusBar As ToolStripProgressBar) As Boolean
    ' ************************************************************************
    '
    ' ************************************************************************
    Dim rptTradeblotter As C1.C1Report.C1Report

    If (pFinal) Then
      SaveReportDetails(ReportFile, TradeBlotterTicketNumber, thisReportTable, "TransactionID")
    End If

    rptTradeblotter = Me.GetReportDefinition(0, ReportFile)

    Try
      If (rptTradeblotter.Fields.Contains("Text_TicketReference")) Then
        rptTradeblotter.Fields("Text_TicketReference").Text = "Ref : TB" & TradeBlotterTicketNumber
      End If
      If (rptTradeblotter.Fields.Contains("Text_AdminField3")) Then
        rptTradeblotter.Fields("Text_AdminField3").Text = rptTradeblotter.Fields("Text_AdminField3").Text & "."
      End If
    Catch ex As Exception
    End Try

    Try
      DisplayReport(rptTradeblotter, thisReportTable, Now.Date, MainForm.Main_Knowledgedate, pStatusBar)
    Catch ex As Exception
    End Try

  End Function

  ''' <summary>
  ''' RPTs the trade revision report.
  ''' </summary>
  ''' <param name="TransactionDetailsView">The transaction details view.</param>
  ''' <param name="pFinal">if set to <c>true</c> [p final].</param>
  ''' <param name="pStatusBar">The p status bar.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function rptTradeRevisionReport(ByRef TransactionDetailsView As DataView, ByVal pFinal As Boolean, ByRef pStatusBar As ToolStripProgressBar) As Boolean
    ' ************************************************************************
    '
    '
    ' ************************************************************************
    Dim rptTradeblotter As C1.C1Report.C1Report
    Dim ReportTable As New DSSelectTransaction.tblSelectTransactionDataTable
    Dim NewTradeBlotterTicketNumber As String = ""
    Dim RowCount As Integer
    Dim UpFrontFeeFunds As String = ""


    ' Get Report Definition

    rptTradeblotter = Me.GetReportDefinition(0, "rptTransactionTradeRevisionBlotter")

    ' Get Report Details and Upfront Fee Funds

    For RowCount = 0 To (TransactionDetailsView.Count - 1)
      If (CDbl(TransactionDetailsView(RowCount)("InstrumentUpfrontFees")) > 0) Then
        If (UpFrontFeeFunds.Length > 0) Then
          UpFrontFeeFunds &= ", "
        End If
        UpFrontFeeFunds &= CStr(TransactionDetailsView(RowCount)("InstrumentDescription"))
      End If

      rptSetTradeRevisionRecord(ReportTable, TransactionDetailsView(RowCount).Row)
    Next

    If (UpFrontFeeFunds.Length > 0) Then
      If UpFrontFeeFunds.LastIndexOf(",") > 0 Then
        UpFrontFeeFunds = UpFrontFeeFunds.Substring(0, UpFrontFeeFunds.LastIndexOf(",")) & " and" & UpFrontFeeFunds.Substring(UpFrontFeeFunds.LastIndexOf(",") + 1)
      End If

      UpFrontFeeFunds = " other than for " & UpFrontFeeFunds
    End If

    ' Get Ticket number

    NewTradeBlotterTicketNumber = MainForm.Get_SystemString("TradeBlotterTicketNumber")
    If pFinal Then
      If (Not IsNumeric(NewTradeBlotterTicketNumber)) Then
        NewTradeBlotterTicketNumber = "0000001"
      End If
      NewTradeBlotterTicketNumber = (CInt(NewTradeBlotterTicketNumber) + 1).ToString("000000")
      MainForm.Set_SystemString("TradeBlotterTicketNumber", NewTradeBlotterTicketNumber)
    Else
      NewTradeBlotterTicketNumber = "<Preview>"
    End If

    ' Set Report Fields

    rptTradeblotter.Fields("Text_TicketReference").Text = "Ref : TB" & NewTradeBlotterTicketNumber
    If (rptTradeblotter.Fields.Contains("Text_AdminField3")) Then
      rptTradeblotter.Fields("Text_AdminField3").Text = rptTradeblotter.Fields("Text_AdminField3").Text & UpFrontFeeFunds & "."
    End If

    ' Save Report details

    If (pFinal) Then
      SaveReportDetails("rptTransactionTradeBlotter", NewTradeBlotterTicketNumber, ReportTable.DefaultView, "TransactionID")
    End If

    ' Run report. 

    Try
      DisplayReport(rptTradeblotter, ReportTable, Now.Date, MainForm.Main_Knowledgedate, pStatusBar)
    Catch ex As Exception
    End Try


  End Function

  ''' <summary>
  ''' RPTs the set trade revision record.
  ''' </summary>
  ''' <param name="ReportTable">The report table.</param>
  ''' <param name="TransactionDetailsRow">The transaction details row.</param>
  Private Sub rptSetTradeRevisionRecord(ByRef ReportTable As DataTable, ByRef TransactionDetailsRow As DSSelectTransaction.tblSelectTransactionRow)
    ' ************************************************************************
    ' Add a record to the given report table representing the changes made to the given trade.
    '
    ' ************************************************************************

    Try
      Dim ThisTransactionRN As Integer
      Dim ThisTransactionID As Integer
      Dim ThisTransactionParentID As Integer

      Dim tblPreviousBlottersForThisTrade As New DataTable
      Dim OldTradeBlotterDetailsTable As New DSSelectTransaction.tblSelectTransactionDataTable

      Dim OldTradeBlotterTicketNumber As String = ""
      Dim OldTradeBlotterRN As Integer
      Dim OldReportDate As Date

      ThisTransactionRN = CInt(TransactionDetailsRow("RN"))
      ThisTransactionID = CInt(TransactionDetailsRow("TransactionID"))
      ThisTransactionParentID = CInt(TransactionDetailsRow("TransactionParentID"))

      Dim ThisCommand As New SqlCommand
      Dim ThisConnection As SqlConnection = Nothing

      Try
        ' First, Select Trade Blotter entries reflecting trades against previous versions of the given transaction.
        ' 

        ThisConnection = MainForm.GetVeniceConnection
        ThisCommand.CommandType = CommandType.Text
        ThisCommand.CommandText = "SELECT RN, KeyField, ValueField, DateEntered FROM tblReportDetailLog WHERE (AppName = 'Venice') AND (ReportName = 'rptTransactionTradeBlotter') AND (ValueField IN (SELECT TransactionID FROM tblTransaction WHERE (TransactionParentID = " & ThisTransactionParentID.ToString & ") AND (RN < " & ThisTransactionRN.ToString & ") AND (TransactionDateDeleted is NULL))) ORDER BY RN DESC"
        ThisCommand.Connection = ThisConnection

        MainForm.LoadTable_Custom(tblPreviousBlottersForThisTrade, ThisCommand)
        ' tblPreviousBlottersForThisTrade.Load(ThisCommand.ExecuteReader)

        ThisCommand = Nothing

        If (tblPreviousBlottersForThisTrade IsNot Nothing) AndAlso (tblPreviousBlottersForThisTrade.Rows.Count > 0) Then

          ' Given a previous Trade Blotter number, retrieve all details for that trade blotter.

          OldTradeBlotterTicketNumber = CStr(tblPreviousBlottersForThisTrade.Rows(0)("KeyField"))
          OldReportDate = CDate(tblPreviousBlottersForThisTrade.Rows(0)("DateEntered"))
          OldTradeBlotterRN = CInt(tblPreviousBlottersForThisTrade.Rows(0)("ValueField"))

          ThisCommand = New SqlCommand("adp_tblSelectTransaction_BlotterRecreate", ThisConnection)
          ThisCommand.CommandType = CommandType.StoredProcedure

          ThisCommand.Parameters.Add("@ReportName", SqlDbType.NVarChar, 30).Value = "rptTransactionTradeBlotter"
          ThisCommand.Parameters.Add("@KeyField", SqlDbType.NVarChar, 50).Value = OldTradeBlotterTicketNumber
          ThisCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime, 8).Value = OldReportDate   ' MainForm.Main_Knowledgedate

          MainForm.LoadTable_Custom(OldTradeBlotterDetailsTable, ThisCommand)
          'OldTradeBlotterDetailsTable.Load(ThisCommand.ExecuteReader)

        End If

      Catch ex As Exception
      Finally
        Try
          If (ThisConnection IsNot Nothing) Then
            ThisConnection.Close()
          End If
        Catch ex As Exception
        End Try

        ThisCommand = Nothing
      End Try

      If (tblPreviousBlottersForThisTrade IsNot Nothing) AndAlso (tblPreviousBlottersForThisTrade.Rows.Count > 0) AndAlso (OldTradeBlotterDetailsTable.Rows.Count > 0) Then

        ' OK, Now create the Table and row entry to feed the report

        Dim ThisRptColumn As DataColumn
        Dim ReportRow As DataRow
        Dim OldBlotterRows() As DataRow = Nothing
        Dim OldBlotterRow As DataRow = Nothing

        ' Find the correct line in the old blotter collection

        OldBlotterRows = OldTradeBlotterDetailsTable.Select("RN=" & OldTradeBlotterRN)

        If (OldBlotterRows IsNot Nothing) AndAlso (OldBlotterRows.Length > 0) Then
          OldBlotterRow = OldBlotterRows(0)

          ' Ensure that the Report table contains all the necessary fields.

          For Each ThisRptColumn In OldTradeBlotterDetailsTable.Columns
            If (Not ReportTable.Columns.Contains("Old" & ThisRptColumn.ColumnName)) Then
              ReportTable.Columns.Add(New DataColumn("Old" & ThisRptColumn.ColumnName, ThisRptColumn.DataType))
            End If
          Next

          If (Not ReportTable.Columns.Contains("OldTicketNumber")) Then
            ReportTable.Columns.Add(New DataColumn("OldTicketNumber", GetType(System.String)))
          End If

          ' Create new Data Row.

          ReportRow = ReportTable.NewRow

          ' Copy in 'New' Data 

          For Each ThisRptColumn In TransactionDetailsRow.Table.Columns
            If ReportTable.Columns.Contains(ThisRptColumn.ColumnName) Then
              ReportRow(ThisRptColumn.ColumnName) = TransactionDetailsRow(ThisRptColumn.ColumnName)
            End If
          Next

          ' Copy in 'Old' Data 

          For Each ThisRptColumn In OldTradeBlotterDetailsTable.Columns
            ReportRow("Old" & ThisRptColumn.ColumnName) = OldBlotterRow(ThisRptColumn.ColumnName)
          Next

          ReportRow("OldTicketNumber") = "Original Trade Blotter Number : TB" & OldTradeBlotterTicketNumber

          ReportTable.Rows.Add(ReportRow)

        Else
          MainForm.LogError("", LOG_LEVELS.Warning, "", "Trade '" & CStr(TransactionDetailsRow("TransactionTicket")) & "' does not appear to have been shown on a previous trade blotter (RN not Found).", "", True)
        End If

      Else
        MainForm.LogError("", LOG_LEVELS.Warning, "", "Trade '" & CStr(TransactionDetailsRow("TransactionTicket")) & "' does not appear to have been shown on a previous trade blotter", "", True)
      End If

    Catch ex As Exception
      MainForm.LogError("rptSetTradeRevisionRecord()", LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
    End Try

  End Sub

  ''' <summary>
  ''' RPTs the FX trade blotter report.
  ''' </summary>
  ''' <param name="TradeBlotterTicketNumber">The trade blotter ticket number.</param>
  ''' <param name="thisReportTable">The this report table.</param>
  ''' <param name="pFinal">if set to <c>true</c> [p final].</param>
  ''' <param name="pStatusBar">The p status bar.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function rptFXTradeBlotterReport(ByVal TradeBlotterTicketNumber As String, ByVal thisReportTable As DataTable, ByVal pFinal As Boolean, ByRef pStatusBar As ToolStripProgressBar) As Boolean
    ' ************************************************************************
    '
    ' ************************************************************************
    Dim rptTradeblotter As C1.C1Report.C1Report

    If (pFinal) Then
      SaveReportDetails("rptFXTradeBlotter", TradeBlotterTicketNumber, thisReportTable.DefaultView, "TransactionID")
    End If

    rptTradeblotter = Me.GetReportDefinition(0, "rptFXTradeBlotter")

    Try
      rptTradeblotter.Fields("Text_TicketReference").Text = "Ref : FX" & TradeBlotterTicketNumber
    Catch ex As Exception
    End Try

    Try
      DisplayReport(rptTradeblotter, thisReportTable, Now.Date, MainForm.Main_Knowledgedate, pStatusBar)
    Catch ex As Exception
    End Try

  End Function

  ''' <summary>
  ''' RPTs the trade blotter reprint.
  ''' </summary>
  ''' <param name="ReportName">Name of the report.</param>
  ''' <param name="TradeBlotterTicketNumber">The trade blotter ticket number.</param>
  ''' <param name="pStatusBar">The p status bar.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function rptTradeBlotterReprint(ByVal ReportName As String, ByVal TradeBlotterTicketNumber As String, ByRef pStatusBar As ToolStripProgressBar) As Boolean
    ' ************************************************************************
    '
    ' ************************************************************************

    Dim thisCommand As SqlCommand = Nothing
    Dim ReportTable As New DSSelectTransaction.tblSelectTransactionDataTable
    Dim ThisConnection As SqlConnection = Nothing

    Dim ReportDate As Date

    Try
      ThisConnection = MainForm.GetVeniceConnection()

      ' Get Report Date
      thisCommand = New SqlCommand("SELECT MAX(DateEntered) FROM fn_tblReportDetailLog_SelectKD(@knowledgeDate) WHERE ReportName=@ReportName AND KeyField=@KeyField", ThisConnection)
      thisCommand.CommandType = CommandType.Text
      thisCommand.Parameters.Add("@ReportName", SqlDbType.NVarChar, 30).Value = ReportName
      thisCommand.Parameters.Add("@KeyField", SqlDbType.NVarChar, 50).Value = TradeBlotterTicketNumber
      thisCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime, 8).Value = MainForm.Main_Knowledgedate

      ReportDate = CDate(Nz(thisCommand.ExecuteScalar, Now().Date))

      thisCommand.Connection = Nothing
      thisCommand.Parameters.Clear()

      ' Get Report Transactions 

      thisCommand = New SqlCommand("adp_tblSelectTransaction_BlotterRecreate", ThisConnection)
      thisCommand.CommandType = CommandType.StoredProcedure

      thisCommand.Parameters.Add("@ReportName", SqlDbType.NVarChar, 30).Value = ReportName
      thisCommand.Parameters.Add("@KeyField", SqlDbType.NVarChar, 50).Value = TradeBlotterTicketNumber
      thisCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime, 8).Value = ReportDate ' MainForm.Main_Knowledgedate

      MainForm.LoadTable_Custom(ReportTable, thisCommand)
      'ReportTable.Load(thisCommand.ExecuteReader)

      thisCommand.Connection = Nothing
      thisCommand.Parameters.Clear()

    Catch ex As Exception
    Finally
      Try
        If (ThisConnection IsNot Nothing) Then
          ThisConnection.Close()
          ThisConnection = Nothing
        End If
      Catch ex As Exception
      End Try
    End Try

    Dim UpFrontFeeFunds As String = ""

    Try
      Dim Counter As Integer

      For Counter = 0 To (ReportTable.Rows.Count - 1)
        If (CDbl(ReportTable.Rows(Counter)("InstrumentUpfrontFees")) > 0) Then
          If (UpFrontFeeFunds.Length > 0) Then
            UpFrontFeeFunds &= ", "
          End If
          UpFrontFeeFunds &= CStr(ReportTable.Rows(Counter)("InstrumentDescription"))
        End If
      Next

      If (UpFrontFeeFunds.Length > 0) Then
        If UpFrontFeeFunds.LastIndexOf(",") > 0 Then
          UpFrontFeeFunds = UpFrontFeeFunds.Substring(0, UpFrontFeeFunds.LastIndexOf(",")) & " and" & UpFrontFeeFunds.Substring(UpFrontFeeFunds.LastIndexOf(",") + 1)
        End If

        UpFrontFeeFunds = " other than for " & UpFrontFeeFunds
      End If

    Catch ex As Exception
    End Try

    If (ReportTable IsNot Nothing) AndAlso (ReportTable.Rows.Count > 0) Then
      Dim rptTradeblotter As C1.C1Report.C1Report

      rptTradeblotter = Me.GetReportDefinition(0, ReportName)

      Try
        If ReportName = "rptFXTradeBlotter" Then
          rptTradeblotter.Fields("Text_TicketReference").Text = "Ref : FX" & TradeBlotterTicketNumber
        Else
          rptTradeblotter.Fields("Text_TicketReference").Text = "Ref : TB" & TradeBlotterTicketNumber
        End If

        rptTradeblotter.Fields("Text_AdminField3").Text = rptTradeblotter.Fields("Text_AdminField3").Text & UpFrontFeeFunds & "."
      Catch ex As Exception
      End Try

      Try
        DisplayReport(rptTradeblotter, ReportTable, ReportDate, MainForm.Main_Knowledgedate, pStatusBar)
      Catch ex As Exception
      End Try

    Else
      MainForm.LogError("ReportHandler, rptTradeBlotterReprint()", LOG_LEVELS.Error, "", "This Trade blotter does not appear to have any Transactions.", "", True)
    End If

  End Function


  ''' <summary>
  ''' Gets the data_ AUM.
  ''' </summary>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pValueDate">The p value date.</param>
  ''' <param name="pIncludeBorrowing">The p include borrowing.</param>
  ''' <param name="pPriceVariant">The p price variant.</param>
  ''' <param name="pShowNetFlowsReport">if set to <c>true</c> [p show net flows report].</param>
  ''' <param name="pShowAccountingReport">if set to <c>true</c> [p show accounting report].</param>
  ''' <param name="pUseMSFundValues">if set to <c>true</c> [p use MS fund values].</param>
  ''' <param name="ShowDetail">if set to <c>true</c> [show detail].</param>
  ''' <param name="IncludeInternalCounterparties">if set to <c>true</c> [include internal counterparties].</param>
  ''' <param name="pStatusBar">The p status bar.</param>
  ''' <returns>DataTable.</returns>
  Public Function GetData_AUM( _
  ByVal pFundID As Integer, ByVal pValueDate As Date, ByVal pIncludeBorrowing As Integer, ByVal pPriceVariant As Integer, ByVal pShowNetFlowsReport As Boolean, ByVal pShowAccountingReport As Boolean, ByVal pUseMSFundValues As Boolean, ByVal ShowDetail As Boolean, ByVal IncludeInternalCounterparties As Boolean, ByRef pStatusBar As ToolStripProgressBar) As DataTable
    ' **************************************************************************************
    '
    '
    ' **************************************************************************************

    ' Configure Adaptor

    Dim rptAdaptor As New SqlDataAdapter
    Dim rptTable As New DataTable

    Try

      ' rptUnitPrice
      If (pShowNetFlowsReport) Then
        MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.GetVeniceConnection, rptAdaptor, "rpt_Venice_AssetsUnderManagement_NetFlows")
      ElseIf (pShowAccountingReport) Then
        MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.GetVeniceConnection, rptAdaptor, "rpt_Venice_AssetsUnderManagementAccountingDetail")
      ElseIf (ShowDetail) Then
        MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.GetVeniceConnection, rptAdaptor, "rpt_Venice_AssetsUnderManagementDetail")
      Else
        MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.GetVeniceConnection, rptAdaptor, "rpt_Venice_AssetsUnderManagement")
      End If

      If (rptAdaptor.SelectCommand.Connection Is Nothing) Then
        Return Nothing
        Exit Function
      End If

      If (rptAdaptor.SelectCommand.Parameters.Contains("@FundID")) Then
        rptAdaptor.SelectCommand.Parameters("@FundID").Value = pFundID
      End If
      If (rptAdaptor.SelectCommand.Parameters.Contains("@ValueDate")) Then
        rptAdaptor.SelectCommand.Parameters("@ValueDate").Value = pValueDate
      End If
      If (rptAdaptor.SelectCommand.Parameters.Contains("@ValueYear")) Then
        rptAdaptor.SelectCommand.Parameters("@ValueYear").Value = pValueDate.Year
      End If
      If (rptAdaptor.SelectCommand.Parameters.Contains("@IncludeBorrowing")) Then
        rptAdaptor.SelectCommand.Parameters("@IncludeBorrowing").Value = pIncludeBorrowing
      End If
      If (rptAdaptor.SelectCommand.Parameters.Contains("@IncludeInternalSubscriptions")) Then
        If (IncludeInternalCounterparties) Then
          rptAdaptor.SelectCommand.Parameters("@IncludeInternalSubscriptions").Value = 1
        Else
          rptAdaptor.SelectCommand.Parameters("@IncludeInternalSubscriptions").Value = 0
        End If
      End If

      If (rptAdaptor.SelectCommand.Parameters.Contains("@PriceVariant")) Then
        rptAdaptor.SelectCommand.Parameters("@PriceVariant").Value = pPriceVariant
      End If

      If (rptAdaptor.SelectCommand.Parameters.Contains("@UseMilestoneFundValues")) Then
        If (pUseMSFundValues = False) Then
          rptAdaptor.SelectCommand.Parameters("@UseMilestoneFundValues").Value = 0
        Else
          rptAdaptor.SelectCommand.Parameters("@UseMilestoneFundValues").Value = 1
        End If
      End If

      If (rptAdaptor.SelectCommand.Parameters.Contains("@ReferenceCurrency")) Then
        rptAdaptor.SelectCommand.Parameters("@ReferenceCurrency").Value = REFERENCE_REPORT_CURRENCY
      End If

      rptAdaptor.SelectCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate
      rptAdaptor.SelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT


      Dim caller As New GetDataReader_Delegate(AddressOf GetDataReader)
      Dim result As IAsyncResult = Nothing
      Dim RptReader As SqlDataReader = Nothing

      Try
        result = caller.BeginInvoke(rptAdaptor.SelectCommand, Nothing, Nothing)

        While result.IsCompleted = False
          Thread.Sleep(25)
          MainForm.IncrementStatusBar(pStatusBar)
          If Not (MainForm.InvokeRequired) Then
            ' Only bother calling DoEvents() if we are on the UI Thread.
            Application.DoEvents()
          End If
        End While

        If (pStatusBar IsNot Nothing) Then
          pStatusBar.Visible = False
        End If

      Catch ex As Exception
        RptReader = Nothing
      Finally
        RptReader = caller.EndInvoke(rptAdaptor.SelectCommand, result)
      End Try

      If (RptReader IsNot Nothing) Then
        rptTable.Load(RptReader)
      End If

    Catch ex As Exception
      MainForm.LogError("ReportHandler, GetData_AUM()", LOG_LEVELS.Error, ex.Message, "Error getting AssetsUnderManagement data.", ex.StackTrace, True)
      Return Nothing
      Exit Function

    Finally
      ' Close Connection
      Try
        If (Not (rptAdaptor.SelectCommand Is Nothing)) AndAlso (Not (rptAdaptor.SelectCommand.Connection Is Nothing)) Then
          rptAdaptor.SelectCommand.Connection.Close()
        End If
      Catch ex As Exception
      End Try
    End Try

    Return rptTable

  End Function

  ''' <summary>
  ''' Gets the data_ AU m_ excluded.
  ''' </summary>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pValueDate">The p value date.</param>
  ''' <param name="pStatusGroupFilter">The p status group filter.</param>
  ''' <param name="pAdministratorDatesFilter">The p administrator dates filter.</param>
  ''' <param name="pStatusBar">The p status bar.</param>
  ''' <returns>System.Double.</returns>
  Public Function GetData_AUM_Excluded(ByVal pFundID As Integer, ByVal pValueDate As Date, ByVal pStatusGroupFilter As String, ByVal pAdministratorDatesFilter As Integer, ByRef pStatusBar As ToolStripProgressBar) As Double
    ' **************************************************************************************
    '
    '
    ' **************************************************************************************

    Dim RVal As Double = 0
    Dim SelectCommand As New SqlCommand

    Try
      SelectCommand.CommandType = CommandType.Text
      SelectCommand.CommandText = "SELECT SUM(USDValue) from fn_ValuationOfExcludedAUM(" & pFundID.ToString() & ", '', @ValueDate, 0, @StatusGroupFilter, @AdministratorDatesFilter, 0, Null, @KnowledgeDate)"
      SelectCommand.Parameters.Add("@ValueDate", SqlDbType.DateTime).Value = pValueDate
      SelectCommand.Parameters.Add("@StatusGroupFilter", SqlDbType.NVarChar).Value = pStatusGroupFilter
      SelectCommand.Parameters.Add("@AdministratorDatesFilter", SqlDbType.Int).Value = pAdministratorDatesFilter
      SelectCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime).Value = MainForm.Main_Knowledgedate

      SelectCommand.Connection = MainForm.GetVeniceConnection

      Dim caller As New GetDataReader_Delegate(AddressOf GetDataReader)
      Dim result As IAsyncResult = Nothing
      Dim RptReader As SqlDataReader = Nothing

      Try
        result = caller.BeginInvoke(SelectCommand, Nothing, Nothing)

        While result.IsCompleted = False
          Thread.Sleep(25)
          MainForm.IncrementStatusBar(pStatusBar)
          If Not (MainForm.InvokeRequired) Then
            ' Only bother calling DoEvents() if we are on the UI Thread.
            Application.DoEvents()
          End If
        End While

        If (pStatusBar IsNot Nothing) Then
          pStatusBar.Visible = False
        End If

      Catch ex As Exception
        RptReader = Nothing
      Finally
        RptReader = caller.EndInvoke(SelectCommand, result)
      End Try

      If (RptReader IsNot Nothing) AndAlso (RptReader.HasRows) Then
        RptReader.Read()
        RVal = CDbl(Nz(RptReader.Item(0), 0))
      End If

    Catch ex As Exception
      RVal = 0
    Finally
      Try
        If (SelectCommand.Connection IsNot Nothing) Then
          SelectCommand.Connection.Close()
          SelectCommand.Connection = Nothing
        End If
      Catch ex As Exception
      End Try
    End Try

    Return RVal

  End Function

  ''' <summary>
  ''' Gets the data_rpt available cash.
  ''' </summary>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pLegalEntity">The p legal entity.</param>
  ''' <param name="pValueDate">The p value date.</param>
  ''' <param name="pSettlementDate">The p settlement date.</param>
  ''' <param name="pDetailDate">The p detail date.</param>
  ''' <param name="pInstrumentType">Type of the p instrument.</param>
  ''' <param name="pStatusGroupFilter">The p status group filter.</param>
  ''' <param name="pAdministratorDatesFilter">The p administrator dates filter.</param>
  ''' <returns>DataTable.</returns>
  Public Function GetData_rptAvailableCash( _
  ByVal pFundID As Integer, ByVal pLegalEntity As String, ByVal pValueDate As Date, ByVal pSettlementDate As Date, ByVal pDetailDate As Date, ByVal pInstrumentType As Integer, ByVal pStatusGroupFilter As String, ByVal pAdministratorDatesFilter As Integer) As DataTable
    ' **************************************************************************************
    '
    '
    ' **************************************************************************************

    ' Configure Adaptor

    Dim rptAdaptor As New SqlDataAdapter
    Dim rptTable As New DataTable

    Try

      ' rptUnitPrice
      MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.GetVeniceConnection, rptAdaptor, "rptAvailableCash")
      If (rptAdaptor.SelectCommand.Connection Is Nothing) Then
        Return Nothing
        Exit Function
      End If

      rptAdaptor.SelectCommand.Parameters("@FundID").Value = pFundID
      rptAdaptor.SelectCommand.Parameters("@LegalEntity").Value = pLegalEntity
      rptAdaptor.SelectCommand.Parameters("@ValueDate").Value = pValueDate
      rptAdaptor.SelectCommand.Parameters("@SettlementDate").Value = pSettlementDate
      rptAdaptor.SelectCommand.Parameters("@DetailDate").Value = pDetailDate
      rptAdaptor.SelectCommand.Parameters("@InstrumentType").Value = pInstrumentType
      rptAdaptor.SelectCommand.Parameters("@StatusGroupFilter").Value = pStatusGroupFilter
      rptAdaptor.SelectCommand.Parameters("@AdministratorDatesFilter").Value = pAdministratorDatesFilter
      rptAdaptor.SelectCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate
      rptAdaptor.SelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      SyncLock rptAdaptor.SelectCommand.Connection
        rptAdaptor.Fill(rptTable)
      End SyncLock

    Catch ex As Exception
      MainForm.LogError("ReportHandler, GetData_rptAvailableCash()", LOG_LEVELS.Error, ex.Message, "Error getting GetData_rptAvailableCash data.", ex.StackTrace, True)
      Return Nothing
      Exit Function

    Finally
      ' Close Connection
      Try
        If (Not (rptAdaptor.SelectCommand Is Nothing)) AndAlso (Not (rptAdaptor.SelectCommand.Connection Is Nothing)) Then
          rptAdaptor.SelectCommand.Connection.Close()
        End If
      Catch ex As Exception
      End Try
    End Try

    Return rptTable

  End Function


  ''' <summary>
  ''' Gets the data_rpt available cash plus.
  ''' </summary>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pLegalEntity">The p legal entity.</param>
  ''' <param name="pValueDate">The p value date.</param>
  ''' <param name="pSettlementDate">The p settlement date.</param>
  ''' <param name="pDetailDate">The p detail date.</param>
  ''' <param name="pPriceAndFXDate">The p price and FX date.</param>
  ''' <param name="pNowDate">The p now date.</param>
  ''' <param name="pInstrumentType">Type of the p instrument.</param>
  ''' <param name="pFundType">Type of the p fund.</param>
  ''' <param name="pStatusGroupFilter">The p status group filter.</param>
  ''' <param name="pAdministratorDatesFilter">The p administrator dates filter.</param>
  ''' <returns>DataTable.</returns>
  Public Function GetData_rptAvailableCashPlus( _
  ByVal pFundID As Integer, ByVal pLegalEntity As String, ByVal pValueDate As Date, ByVal pSettlementDate As Date, ByVal pDetailDate As Date, ByVal pPriceAndFXDate As Date, ByVal pNowDate As Date, ByVal pInstrumentType As Integer, ByVal pFundType As Integer, ByVal pStatusGroupFilter As String, ByVal pAdministratorDatesFilter As Integer) As DataTable
    ' **************************************************************************************
    '
    '
    ' **************************************************************************************

    ' Configure Adaptor

    Dim rptAdaptor As New SqlDataAdapter
    Dim rptTable As New DataTable

    Try

      ' rptUnitPrice
      MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.GetVeniceConnection, rptAdaptor, "rptAvailableCashPlus")
      If (rptAdaptor.SelectCommand.Connection Is Nothing) Then
        Return Nothing
        Exit Function
      End If

      rptAdaptor.SelectCommand.Parameters("@FundID").Value = pFundID
      rptAdaptor.SelectCommand.Parameters("@LegalEntity").Value = pLegalEntity
      rptAdaptor.SelectCommand.Parameters("@ValueDate").Value = pValueDate
      rptAdaptor.SelectCommand.Parameters("@SettlementDate").Value = pSettlementDate
      rptAdaptor.SelectCommand.Parameters("@DetailDate").Value = pDetailDate
      rptAdaptor.SelectCommand.Parameters("@PriceAndFXDate").Value = pPriceAndFXDate
      rptAdaptor.SelectCommand.Parameters("@NowDate").Value = pNowDate
      rptAdaptor.SelectCommand.Parameters("@InstrumentType").Value = pInstrumentType
      rptAdaptor.SelectCommand.Parameters("@FundType").Value = pFundType
      rptAdaptor.SelectCommand.Parameters("@StatusGroupFilter").Value = pStatusGroupFilter
      rptAdaptor.SelectCommand.Parameters("@AdministratorDatesFilter").Value = pAdministratorDatesFilter
      rptAdaptor.SelectCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate

      rptAdaptor.SelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      SyncLock rptAdaptor.SelectCommand.Connection
        rptAdaptor.Fill(rptTable)
      End SyncLock

    Catch ex As Exception
      MainForm.LogError("ReportHandler, GetData_rptAvailableCashPlus()", LOG_LEVELS.Error, ex.Message, "Error getting GetData_rptAvailableCashPlus data.", ex.StackTrace, True)
      Return Nothing
      Exit Function

    Finally
      ' Close Connection
      Try
        If (Not (rptAdaptor.SelectCommand Is Nothing)) AndAlso (Not (rptAdaptor.SelectCommand.Connection Is Nothing)) Then
          rptAdaptor.SelectCommand.Connection.Close()
          rptAdaptor.SelectCommand.Connection = Nothing
        End If
      Catch ex As Exception
      End Try
    End Try

    Return rptTable

  End Function

  ''' <summary>
  ''' Gets the data_rpt notional allocation report.
  ''' </summary>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pValueDate">The p value date.</param>
  ''' <param name="pStatusGroupFilter">The p status group filter.</param>
  ''' <param name="pAdministratorDatesFilter">The p administrator dates filter.</param>
  ''' <param name="pUnRealisedCalculationType">Type of the p un realised calculation.</param>
  ''' <returns>DataTable.</returns>
  Public Function GetData_rptNotionalAllocationReport( _
  ByVal pFundID As Integer, ByVal pValueDate As Date, ByVal pStatusGroupFilter As String, ByVal pAdministratorDatesFilter As Integer, ByVal pUnRealisedCalculationType As Integer) As DataTable
    ' **************************************************************************************
    ' MainForm.MainReportHandler.GetData_rptNotionalAllocationReport(pFundID, pValuationDate)
    '
    '
    ' **************************************************************************************

    ' Configure Adaptor

    Dim rptAdaptor As New SqlDataAdapter
    Dim rptTable As New DataTable

    Try
      rptAdaptor.SelectCommand = New SqlCommand()

      rptAdaptor.SelectCommand.CommandText = "rpt_Venice_NotionalAllocationReport"
      rptAdaptor.SelectCommand.CommandType = CommandType.StoredProcedure
      rptAdaptor.SelectCommand.Connection = MainForm.GetVeniceConnection

      rptAdaptor.SelectCommand.Parameters.Add("@FundID", SqlDbType.Int).Value = pFundID
      rptAdaptor.SelectCommand.Parameters.Add("@ValueDate", SqlDbType.DateTime).Value = pValueDate
      rptAdaptor.SelectCommand.Parameters.Add("@StatusGroupFilter", SqlDbType.NVarChar).Value = pStatusGroupFilter
      rptAdaptor.SelectCommand.Parameters.Add("@AdministratorDatesFilter", SqlDbType.Int).Value = pAdministratorDatesFilter
      rptAdaptor.SelectCommand.Parameters.Add("@UnRealisedCalculationType", SqlDbType.Int).Value = pUnRealisedCalculationType

      rptAdaptor.SelectCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime).Value = MainForm.Main_Knowledgedate

      rptAdaptor.SelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      SyncLock rptAdaptor.SelectCommand.Connection
        rptAdaptor.Fill(rptTable)
      End SyncLock

    Catch ex As Exception
      MainForm.LogError("ReportHandler, GetData_rptNotionalAllocationReport()", LOG_LEVELS.Error, ex.Message, "Error getting rptNotionalAllocationReport data.", ex.StackTrace, True)
      Return Nothing
      Exit Function

    Finally
      ' Close Connection
      Try
        If (Not (rptAdaptor.SelectCommand Is Nothing)) AndAlso (Not (rptAdaptor.SelectCommand.Connection Is Nothing)) Then
          rptAdaptor.SelectCommand.Connection.Close()
          rptAdaptor.SelectCommand.Connection = Nothing
        End If

        rptAdaptor.SelectCommand = Nothing

      Catch ex As Exception
      End Try
    End Try

    Return rptTable

  End Function

  ''' <summary>
  ''' Gets the data_rpt inventory reconciliation report.
  ''' </summary>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pValueDate">The p value date.</param>
  ''' <param name="pStatusGroupFilter">The p status group filter.</param>
  ''' <param name="pAdministratorDatesFilter">The p administrator dates filter.</param>
  ''' <returns>DataTable.</returns>
  Public Function GetData_rptInventoryReconciliationReport( _
  ByVal pFundID As Integer, ByVal pValueDate As Date, ByVal pStatusGroupFilter As String, ByVal pAdministratorDatesFilter As Integer) As DataTable
    ' **************************************************************************************
    ' MainForm.MainReportHandler.GetData_rptNotionalAllocationReport(pFundID, pValuationDate)
    '
    '
    'PROCEDURE([dbo].[rpt_Venice_ReconcileValuation])
    '	(
    '		@FundID int,
    '		@LegalEntity nvarchar(100) = '',
    '		@ValueDate datetime,
    '		@UnitPriceVariant int = 0,
    '		@StatusGroupFilter nvarchar(50) = '',
    '		@AdministratorDatesFilter int = 0, 
    '		@DisregardWatermarkPoints int = 0,
    '		@AggregateToInstrumentParentID int = 0,
    '		@MaxCaptureID int = 0,
    '		@KnowledgeDate datetime = null
    '	)
    ' **************************************************************************************

    ' Configure Adaptor

    Dim rptAdaptor As New SqlDataAdapter
    Dim rptTable As New DataTable

    Try
      rptAdaptor.SelectCommand = New SqlCommand()

      rptAdaptor.SelectCommand.CommandText = "rpt_Venice_ReconcileValuation"
      rptAdaptor.SelectCommand.CommandType = CommandType.StoredProcedure
      rptAdaptor.SelectCommand.Connection = MainForm.GetVeniceConnection

      rptAdaptor.SelectCommand.Parameters.Add("@FundID", SqlDbType.Int).Value = pFundID
      rptAdaptor.SelectCommand.Parameters.Add("@LegalEntity", SqlDbType.NVarChar).Value = ""
      rptAdaptor.SelectCommand.Parameters.Add("@ValueDate", SqlDbType.DateTime).Value = pValueDate
      rptAdaptor.SelectCommand.Parameters.Add("@UnitPriceVariant", SqlDbType.Int).Value = 0
      rptAdaptor.SelectCommand.Parameters.Add("@StatusGroupFilter", SqlDbType.NVarChar).Value = pStatusGroupFilter
      rptAdaptor.SelectCommand.Parameters.Add("@AdministratorDatesFilter", SqlDbType.Int).Value = pAdministratorDatesFilter
      rptAdaptor.SelectCommand.Parameters.Add("@DisregardWatermarkPoints", SqlDbType.Int).Value = 0
      rptAdaptor.SelectCommand.Parameters.Add("@AggregateToInstrumentParentID", SqlDbType.Int).Value = 0
      rptAdaptor.SelectCommand.Parameters.Add("@UnRealisedCalculationType", SqlDbType.Int).Value = 0 ' Default to FIFO.
      rptAdaptor.SelectCommand.Parameters.Add("@MaxCaptureID", SqlDbType.Int).Value = 0

      rptAdaptor.SelectCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime).Value = MainForm.Main_Knowledgedate

      rptAdaptor.SelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      SyncLock rptAdaptor.SelectCommand.Connection
        rptAdaptor.Fill(rptTable)
      End SyncLock

    Catch ex As Exception
      MainForm.LogError("ReportHandler, GetData_rptInventoryReconciliationReport()", LOG_LEVELS.Error, ex.Message, "Error getting rptInventoryReconciliationReport data.", ex.StackTrace, True)
      Return Nothing
      Exit Function

    Finally
      ' Close Connection
      Try
        If (Not (rptAdaptor.SelectCommand Is Nothing)) AndAlso (Not (rptAdaptor.SelectCommand.Connection Is Nothing)) Then
          rptAdaptor.SelectCommand.Connection.Close()
          rptAdaptor.SelectCommand.Connection = Nothing
        End If

        rptAdaptor.SelectCommand = Nothing

      Catch ex As Exception
      End Try
    End Try

    Return rptTable

  End Function

  ''' <summary>
  ''' Gets the data_rpt cash move.
  ''' </summary>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pValueDateFrom">The p value date from.</param>
  ''' <param name="pValueDateTo">The p value date to.</param>
  ''' <param name="pKnowledgeDateFrom">The p knowledge date from.</param>
  ''' <param name="pKnowledgeDateTo">The p knowledge date to.</param>
  ''' <param name="pStatusGroupFilter">The p status group filter.</param>
  ''' <param name="pAdministratorDatesFilter">The p administrator dates filter.</param>
  ''' <param name="pKnowledgeDateLinkedTables">The p knowledge date linked tables.</param>
  ''' <returns>DataTable.</returns>
  Public Function GetData_rptCashMove( _
  ByVal pFundID As Integer, ByVal pValueDateFrom As Date, ByVal pValueDateTo As Date, ByVal pKnowledgeDateFrom As Date, ByVal pKnowledgeDateTo As Date, ByVal pStatusGroupFilter As String, ByVal pAdministratorDatesFilter As Integer, ByVal pKnowledgeDateLinkedTables As Date) As DataTable
    ' **************************************************************************************
    '
    '
    ' **************************************************************************************

    ' Configure Adaptor

    Dim rptAdaptor As New SqlDataAdapter
    Dim rptTable As New DataTable

    Try

      ' rptUnitPrice
      MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.GetVeniceConnection, rptAdaptor, "rptCashMove")
      If (rptAdaptor.SelectCommand.Connection Is Nothing) Then
        Return Nothing
        Exit Function
      End If

      rptAdaptor.SelectCommand.Parameters("@FundID").Value = pFundID
      rptAdaptor.SelectCommand.Parameters("@ValueDateFrom").Value = pValueDateFrom
      rptAdaptor.SelectCommand.Parameters("@KnowledgeDateFrom").Value = pKnowledgeDateFrom
      rptAdaptor.SelectCommand.Parameters("@ValueDateTo").Value = pValueDateTo
      rptAdaptor.SelectCommand.Parameters("@KnowledgeDateTo").Value = pKnowledgeDateTo
      rptAdaptor.SelectCommand.Parameters("@StatusGroupFilter").Value = pStatusGroupFilter
      rptAdaptor.SelectCommand.Parameters("@AdministratorDatesFilter").Value = pAdministratorDatesFilter
      rptAdaptor.SelectCommand.Parameters("@KnowledgeDateLinkedTables").Value = MainForm.Main_Knowledgedate
      rptAdaptor.SelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      SyncLock rptAdaptor.SelectCommand.Connection
        rptAdaptor.Fill(rptTable)
      End SyncLock

    Catch ex As Exception
      MainForm.LogError("ReportHandler, GetData_rptCashMove()", LOG_LEVELS.Error, ex.Message, "Error getting GetData_rptCashMove data.", ex.StackTrace, True)
      Return Nothing
      Exit Function

    Finally
      ' Close Connection
      Try
        If (Not (rptAdaptor.SelectCommand Is Nothing)) AndAlso (Not (rptAdaptor.SelectCommand.Connection Is Nothing)) Then
          rptAdaptor.SelectCommand.Connection.Close()
        End If
      Catch ex As Exception
      End Try
    End Try

    Return rptTable

  End Function

  ''' <summary>
  ''' Gets the data_ expense report.
  ''' </summary>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pValueDate">The p value date.</param>
  ''' <param name="pStatusGroupFilter">The p status group filter.</param>
  ''' <param name="pAdministratorDatesFilter">The p administrator dates filter.</param>
  ''' <returns>DataTable.</returns>
  Public Function GetData_ExpenseReport( _
  ByVal pFundID As Integer, ByVal pValueDate As Date, ByVal pStatusGroupFilter As String, ByVal pAdministratorDatesFilter As Integer) As DataTable
    ' **************************************************************************************
    '
    '
    ' **************************************************************************************

    ' Configure Adaptor

    Dim rptAdaptor As New SqlDataAdapter
    Dim rptTable As New DataTable
    Dim thisRow As DataRow

    Try

      MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.GetVeniceConnection, rptAdaptor, "rptExpenseReport")
      If (rptAdaptor.SelectCommand.Connection Is Nothing) Then
        Return Nothing
        Exit Function
      End If

      rptAdaptor.SelectCommand.Parameters("@FundID").Value = pFundID
      rptAdaptor.SelectCommand.Parameters("@ValueDate").Value = pValueDate
      rptAdaptor.SelectCommand.Parameters("@StatusGroupFilter").Value = pStatusGroupFilter
      rptAdaptor.SelectCommand.Parameters("@AdministratorDatesFilter").Value = pAdministratorDatesFilter
      rptAdaptor.SelectCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate
      rptAdaptor.SelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      SyncLock rptAdaptor.SelectCommand.Connection
        rptAdaptor.Fill(rptTable)
      End SyncLock

    Catch ex As Exception
      MainForm.LogError("ReportHandler, GetData_ExpenseReport()", LOG_LEVELS.Error, ex.Message, "Error getting ExpenseReport data.", ex.StackTrace, True)
      Return Nothing
      Exit Function

    Finally
      ' Close Connection
      Try
        If (Not (rptAdaptor.SelectCommand Is Nothing)) AndAlso (Not (rptAdaptor.SelectCommand.Connection Is Nothing)) Then
          rptAdaptor.SelectCommand.Connection.Close()
        End If
      Catch ex As Exception
      End Try
    End Try

    ' Add Calculated fields :-
    '	GetFXValue(Instruments.InstrumentCurrencyID,Transactions.TransactionValueDate,0) AS InstFX, 
    '	GetFXValue(Funds.FundBaseCurrency,Transactions.TransactionValueDate,0) AS FundFX, 
    '	(Transactions.TransactionSignedUnits*[InstFX])/[FundFX] AS ExpenseInFundCcy

    Try
      rptTable.Columns.Add("InstFX", GetType(Double))
      rptTable.Columns.Add("FundFX", GetType(Double))
      rptTable.Columns.Add("ExpenseInFundCcy", GetType(Double))

      For Each thisRow In rptTable.Rows
        Try
          thisRow("InstFX") = Get_FXRate(CInt(thisRow("InstrumentCurrencyID")), CDate(thisRow("TransactionValueDate")))
          If CDbl(thisRow("InstFX")) = 0 Then thisRow("InstFX") = 1.0
          thisRow("FundFX") = Get_FXRate(CInt(thisRow("FundBaseCurrency")), CDate(thisRow("TransactionValueDate")))
          If CDbl(thisRow("FundFX")) = 0 Then thisRow("FundFX") = 1.0
          thisRow("ExpenseInFundCcy") = (CDbl(thisRow("TransactionSignedSettlement")) * CDbl(thisRow("InstFX"))) / CDbl(thisRow("FundFX"))
        Catch ex As Exception
        End Try
      Next

    Catch ex As Exception
    End Try

    Return rptTable

  End Function

  ''' <summary>
  ''' Gets the data_ management fees report.
  ''' </summary>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pYear">The p year.</param>
  ''' <param name="pReportCurrency">The p report currency.</param>
  ''' <param name="pStatusGroupFilter">The p status group filter.</param>
  ''' <param name="pAdministratorDatesFilter">The p administrator dates filter.</param>
  ''' <returns>DataTable.</returns>
  Public Function GetData_ManagementFeesReport( _
  ByVal pFundID As Integer, ByVal pYear As Integer, ByVal pReportCurrency As Integer, ByVal pStatusGroupFilter As String, ByVal pAdministratorDatesFilter As Integer) As DataTable
    ' **************************************************************************************
    '
    'SELECT 
    ' FundID, 
    ' FeeYear, 
    ' Feemonth, 
    ' ManagementFee, 
    ' PerformanceFee, 
    ' CrystalisedFee, 
    ' FundCurrency,
    ' FundName
    'FROM [dbo].[fn_rptManagementFeesByMonth]
    '
    ' **************************************************************************************

    ' Configure Adaptor

    Dim rptAdaptor As New SqlDataAdapter
    Dim rptTable As New DataTable

    Try

      MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.GetVeniceConnection, rptAdaptor, "rptManagementFeesReport")
      If (rptAdaptor.SelectCommand.Connection Is Nothing) Then
        Return Nothing
        Exit Function
      End If

      rptAdaptor.SelectCommand.Parameters("@FundID").Value = pFundID
      rptAdaptor.SelectCommand.Parameters("@Year").Value = pYear
      rptAdaptor.SelectCommand.Parameters("@StatusGroupFilter").Value = pStatusGroupFilter
      rptAdaptor.SelectCommand.Parameters("@AdministratorDatesFilter").Value = pAdministratorDatesFilter
      rptAdaptor.SelectCommand.Parameters("@ReportingCurrencyID").Value = pReportCurrency
      rptAdaptor.SelectCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate
      rptAdaptor.SelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      SyncLock rptAdaptor.SelectCommand.Connection
        rptAdaptor.Fill(rptTable)
      End SyncLock

    Catch ex As Exception
      MainForm.LogError("ReportHandler, GetData_ManagementFeesReport()", LOG_LEVELS.Error, ex.Message, "Error getting Management Fees Report data.", ex.StackTrace, True)
      Return Nothing
      Exit Function

    Finally
      ' Close Connection
      Try
        If (Not (rptAdaptor.SelectCommand Is Nothing)) AndAlso (Not (rptAdaptor.SelectCommand.Connection Is Nothing)) Then
          rptAdaptor.SelectCommand.Connection.Close()
        End If
      Catch ex As Exception
      End Try
    End Try

    Return rptTable

  End Function

  ''' <summary>
  ''' Gets the data_ fund valuation.
  ''' </summary>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pLegalEntity">The p legal entity.</param>
  ''' <param name="pAggregateToParent">if set to <c>true</c> [p aggregate to parent].</param>
  ''' <param name="pValueDate">The p value date.</param>
  ''' <param name="pStatusGroupFilter">The p status group filter.</param>
  ''' <param name="pAdministratorDatesFilter">The p administrator dates filter.</param>
  ''' <returns>DataTable.</returns>
  Public Function GetData_FundValuation( _
  ByVal pFundID As Integer, ByVal pLegalEntity As String, ByVal pAggregateToParent As Boolean, ByVal pValueDate As Date, ByVal pStatusGroupFilter As String, ByVal pAdministratorDatesFilter As Integer) As DataTable
    ' **************************************************************************************
    '
    '
    ' **************************************************************************************

    ' Configure Adaptor

    Dim rptAdaptor As New SqlDataAdapter
    Dim rptTable As New DataTable

    Try

      MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.GetVeniceConnection, rptAdaptor, "rptFundValuation")
      If (rptAdaptor.SelectCommand.Connection Is Nothing) Then
        Return Nothing
        Exit Function
      End If

      rptAdaptor.SelectCommand.Parameters("@FundID").Value = pFundID
      rptAdaptor.SelectCommand.Parameters("@LegalEntity").Value = pLegalEntity

      If (pAggregateToParent) Then
        rptAdaptor.SelectCommand.Parameters("@AggregateToInstrumentParentID").Value = 2
      Else
        rptAdaptor.SelectCommand.Parameters("@AggregateToInstrumentParentID").Value = 0
      End If

      rptAdaptor.SelectCommand.Parameters("@ValueDate").Value = pValueDate
      rptAdaptor.SelectCommand.Parameters("@StatusGroupFilter").Value = pStatusGroupFilter
      rptAdaptor.SelectCommand.Parameters("@AdministratorDatesFilter").Value = pAdministratorDatesFilter
      rptAdaptor.SelectCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate
      rptAdaptor.SelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      ' AggregateToInstrumentParentID

      SyncLock rptAdaptor.SelectCommand.Connection
        rptAdaptor.Fill(rptTable)
      End SyncLock

    Catch ex As Exception
      MainForm.LogError("ReportHandler, GetData_FundValuation()", LOG_LEVELS.Error, ex.Message, "Error getting rptFundValuation data.", ex.StackTrace, True)
      Return Nothing
      Exit Function

    Finally
      ' Close Connection
      Try
        If (Not (rptAdaptor.SelectCommand Is Nothing)) AndAlso (Not (rptAdaptor.SelectCommand.Connection Is Nothing)) Then
          rptAdaptor.SelectCommand.Connection.Close()
        End If
      Catch ex As Exception
      End Try
    End Try

    Return rptTable

  End Function


  ''' <summary>
  ''' Gets the data_rpt liquidity report.
  ''' </summary>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pValueDate">The p value date.</param>
  ''' <returns>DataTable.</returns>
  Public Function GetData_rptLiquidityReport( _
  ByVal pFundID As Integer, ByVal pValueDate As Date) As DataTable
    ' **************************************************************************************
    '
    '
    ' **************************************************************************************

    ' Configure Adaptor

    Dim rptAdaptor As New SqlDataAdapter
    Dim rptTable As New DataTable
    Dim thisRow As DataRow
    Dim Counter As Integer

    Try

      ' rptUnitPrice
      MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.GetVeniceConnection, rptAdaptor, "rptLiquidityReport")
      If (rptAdaptor.SelectCommand.Connection Is Nothing) Then
        Return Nothing
        Exit Function
      End If

      rptAdaptor.SelectCommand.Parameters("@FundID").Value = pFundID
      rptAdaptor.SelectCommand.Parameters("@ValueDate").Value = pValueDate
      rptAdaptor.SelectCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate
      rptAdaptor.SelectCommand.Parameters("@KnowledgeDateLinkedTables").Value = MainForm.Main_Knowledgedate
      rptAdaptor.SelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      SyncLock rptAdaptor.SelectCommand.Connection
        rptAdaptor.Fill(rptTable)
      End SyncLock

      ' Add Calculated Columns

      ' nextdealingdate([DealingPeriodDays],[NoticeDays],[InstrumentDealingBaseDate],Get_LocalDate("rptLiquidityToDate")) AS DealDate, 
      ' [DealDate]-[NoticeDays] AS NoticeDate, 
      ' IIf([InstrumentLockupPeriod]+[MaxValueDate]<=Get_LocalDate("rptLiquidityToDate"),"Free",IIf([InstrumentLockupPeriod]+[MinValueDate]>Get_LocalDate("rptLiquidityToDate"),"Locked","Partial")) AS Status

      rptTable.Columns.Add(New DataColumn("DealDate", GetType(Date)))
      rptTable.Columns.Add(New DataColumn("NoticeDate", GetType(Date)))
      rptTable.Columns.Add(New DataColumn("Status", GetType(String)))

      Dim DealingPeriodDays As Integer
      Dim NoticeDays As Integer
      Dim InstrumentDealingBaseDate As Date
      Dim InstrumentLockupPeriod As Integer
      Dim MaxValueDate As Date
      Dim MinValueDate As Date



      For Counter = 0 To (rptTable.Rows.Count - 1)
        thisRow = rptTable.Rows(Counter)

        If thisRow.IsNull("DealingPeriodDays") Then
          DealingPeriodDays = 30
        Else
          DealingPeriodDays = CInt(thisRow("DealingPeriodDays"))
        End If

        If thisRow.IsNull("NoticeDays") Then
          NoticeDays = 30
        Else
          NoticeDays = CInt(thisRow("NoticeDays"))
        End If

        If thisRow.IsNull("InstrumentDealingBaseDate") Then
          InstrumentDealingBaseDate = CDate("31 Dec 2000")
        Else
          InstrumentDealingBaseDate = CDate(thisRow("InstrumentDealingBaseDate"))
        End If

        thisRow("DealDate") = NextDealingDate(DealingPeriodDays, NoticeDays, InstrumentDealingBaseDate, pValueDate)
        thisRow("NoticeDate") = CDate(thisRow("DealDate")).AddDays(-NoticeDays)

        InstrumentLockupPeriod = CInt(thisRow("InstrumentLockupPeriod"))
        MaxValueDate = CDate(thisRow("MaxValueDate"))
        MinValueDate = CDate(thisRow("MinValueDate"))

        ' IIf([InstrumentLockupPeriod]+[MaxValueDate]<=Get_LocalDate("rptLiquidityToDate"),"Free",IIf([InstrumentLockupPeriod]+[MinValueDate]>Get_LocalDate("rptLiquidityToDate"),"Locked","Partial")) AS Status
        If MaxValueDate.AddDays(InstrumentLockupPeriod) <= pValueDate Then
          thisRow("Status") = "Free"
        ElseIf MinValueDate.AddDays(InstrumentLockupPeriod) > pValueDate Then
          thisRow("Status") = "Locked"
        Else
          thisRow("Status") = "Partial"
        End If

      Next

    Catch ex As Exception
      MainForm.LogError("ReportHandler, GetData_rptLiquidityReport()", LOG_LEVELS.Error, ex.Message, "Error getting rptLiquidityReport data.", ex.StackTrace, True)
      Return Nothing
      Exit Function

    Finally
      ' Close Connection
      Try
        If (Not (rptAdaptor.SelectCommand Is Nothing)) AndAlso (Not (rptAdaptor.SelectCommand.Connection Is Nothing)) Then
          rptAdaptor.SelectCommand.Connection.Close()
        End If
      Catch ex As Exception
      End Try
    End Try

    Return rptTable

  End Function


  ''' <summary>
  ''' Gets the data_rpt fund gearing.
  ''' </summary>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pReportDate">The p report date.</param>
  ''' <param name="pStatusGroupFilter">The p status group filter.</param>
  ''' <param name="pAdministratorDatesFilter">The p administrator dates filter.</param>
  ''' <param name="pKnowledgeDate">The p knowledge date.</param>
  ''' <returns>DataTable.</returns>
  Public Function GetData_rptFundGearing( _
  ByVal pFundID As Integer, ByVal pReportDate As Date, ByVal pStatusGroupFilter As String, ByVal pAdministratorDatesFilter As Integer, ByVal pKnowledgeDate As Date) As DataTable
    ' **************************************************************************************
    '
    '
    ' **************************************************************************************

    ' Configure Adaptor

    Dim rptAdaptor As New SqlDataAdapter
    Dim rptTable As New DataTable

    Try

      MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.GetVeniceConnection, rptAdaptor, "rpt_Venice_FundGearing")
      If (rptAdaptor.SelectCommand.Connection Is Nothing) Then
        Return Nothing
        Exit Function
      End If

      rptAdaptor.SelectCommand.Parameters("@FundID").Value = pFundID
      rptAdaptor.SelectCommand.Parameters("@ValueDate").Value = pReportDate
      rptAdaptor.SelectCommand.Parameters("@StatusGroupFilter").Value = pStatusGroupFilter
      rptAdaptor.SelectCommand.Parameters("@AdministratorDatesFilter").Value = pAdministratorDatesFilter
      rptAdaptor.SelectCommand.Parameters("@KnowledgeDate").Value = pKnowledgeDate
      rptAdaptor.SelectCommand.Parameters("@KnowledgeDateLinkedTables").Value = pKnowledgeDate
      rptAdaptor.SelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      SyncLock rptAdaptor.SelectCommand.Connection
        rptAdaptor.Fill(rptTable)
      End SyncLock

    Catch ex As Exception
      MainForm.LogError("ReportHandler, GetData_rptFundGearing()", LOG_LEVELS.Error, ex.Message, "Error getting rptFundGearing data.", ex.StackTrace, True)
      Return Nothing
      Exit Function

    Finally
      ' Close Connection
      Try
        If (rptAdaptor.SelectCommand IsNot Nothing) AndAlso (rptAdaptor.SelectCommand.Connection IsNot Nothing) Then
          rptAdaptor.SelectCommand.Connection.Close()
        End If
      Catch ex As Exception
      End Try
    End Try

    Return rptTable

  End Function

  'GetFundFeesData(FundID, CounterpartyID, InvestorGroupID, ValueDate)
  ''' <summary>
  ''' Gets the fund fees data.
  ''' </summary>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pCounterpartyID">The p counterparty ID.</param>
  ''' <param name="pInvestorGroupID">The p investor group ID.</param>
  ''' <param name="pValueDate">The p value date.</param>
  ''' <returns>DataTable.</returns>
  Public Function GetFundFeesData( _
  ByVal pFundID As Integer, ByVal pCounterpartyID As Integer, ByVal pInvestorGroupID As Integer, ByVal pValueDate As Date) As DataTable

    ' **************************************************************************************
    '
    '
    ' **************************************************************************************

    ' Configure Adaptor

    Dim rptAdaptor As New SqlDataAdapter
    Dim rptTable As New DataTable

    Try

      MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.GetVeniceConnection, rptAdaptor, "RPTFundFeeRecords")
      If (rptAdaptor.SelectCommand.Connection Is Nothing) Then
        Return Nothing
        Exit Function
      End If

      rptAdaptor.SelectCommand.Parameters("@FundID").Value = pFundID
      rptAdaptor.SelectCommand.Parameters("@CounterpartyID").Value = pCounterpartyID
      rptAdaptor.SelectCommand.Parameters("@InvestorGroup").Value = pInvestorGroupID
      rptAdaptor.SelectCommand.Parameters("@FeeDate").Value = pValueDate
      rptAdaptor.SelectCommand.Parameters("@ReferenceCurrency").Value = Currency.GBP
      rptAdaptor.SelectCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate
      rptAdaptor.SelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      SyncLock rptAdaptor.SelectCommand.Connection
        rptAdaptor.Fill(rptTable)
      End SyncLock

    Catch ex As Exception
      MainForm.LogError("ReportHandler, GetFundFeesData()", LOG_LEVELS.Error, ex.Message, "Error getting Fee data.", ex.StackTrace, True)
      Return Nothing
      Exit Function

    Finally
      ' Close Connection
      Try
        If (Not (rptAdaptor.SelectCommand Is Nothing)) AndAlso (Not (rptAdaptor.SelectCommand.Connection Is Nothing)) Then
          rptAdaptor.SelectCommand.Connection.Close()
        End If
      Catch ex As Exception
      End Try
    End Try

    Return rptTable

  End Function

  ''' <summary>
  ''' Gets the fund fees data.
  ''' </summary>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pCounterpartyID">The p counterparty ID.</param>
  ''' <param name="pInvestorGroupID">The p investor group ID.</param>
  ''' <returns>DataTable.</returns>
  Public Function GetFundFeesData( _
  ByVal pFundID As Integer, ByVal pCounterpartyID As Integer, ByVal pInvestorGroupID As Integer) As DataTable
    Return GetFundFeesData(pFundID, pCounterpartyID, pInvestorGroupID, Renaissance_BaseDate)
  End Function

  ' GetData_rptFX
  ''' <summary>
  ''' Gets the data_rpt FX.
  ''' </summary>
  ''' <param name="pCurrencyID">The p currency ID.</param>
  ''' <param name="pDateFrom">The p date from.</param>
  ''' <param name="pDateTo">The p date to.</param>
  ''' <returns>DataTable.</returns>
  Public Function GetData_rptFX( _
  ByVal pCurrencyID As Integer, ByVal pDateFrom As Date, ByVal pDateTo As Date) As DataTable
    ' **************************************************************************************
    '
    '
    ' **************************************************************************************

    ' Configure Adaptor

    Dim rptAdaptor As New SqlDataAdapter
    Dim rptTable As New DataTable

    Try

      MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.GetVeniceConnection, rptAdaptor, "rptFXs")
      If (rptAdaptor.SelectCommand.Connection Is Nothing) Then
        Return Nothing
        Exit Function
      End If

      rptAdaptor.SelectCommand.Parameters("@CurrencyID").Value = pCurrencyID
      rptAdaptor.SelectCommand.Parameters("@DateFrom").Value = pDateFrom
      rptAdaptor.SelectCommand.Parameters("@DateTo").Value = pDateTo
      rptAdaptor.SelectCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate
      rptAdaptor.SelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      SyncLock rptAdaptor.SelectCommand.Connection
        rptAdaptor.Fill(rptTable)
      End SyncLock

    Catch ex As Exception
      MainForm.LogError("ReportHandler, GetData_rptFX()", LOG_LEVELS.Error, ex.Message, "Error getting rptFX data.", ex.StackTrace, True)
      Return Nothing
      Exit Function

    Finally
      ' Close Connection
      Try
        If (Not (rptAdaptor.SelectCommand Is Nothing)) AndAlso (Not (rptAdaptor.SelectCommand.Connection Is Nothing)) Then
          rptAdaptor.SelectCommand.Connection.Close()
        End If
      Catch ex As Exception
      End Try
    End Try

    Return rptTable

  End Function

  ' GetData_rptTradeStatus
  ''' <summary>
  ''' Gets the data_rpt trade status.
  ''' </summary>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pTradeStatusID">The p trade status ID.</param>
  ''' <param name="pDateFrom">The p date from.</param>
  ''' <param name="pDateTo">The p date to.</param>
  ''' <returns>DataTable.</returns>
  Public Function GetData_rptTradeStatus( _
  ByVal pFundID As Integer, ByVal pTradeStatusID As Integer, ByVal pDateFrom As Date, ByVal pDateTo As Date) As DataTable
    ' **************************************************************************************
    '
    '
    ' **************************************************************************************

    ' Configure Adaptor

    Dim rptAdaptor As New SqlDataAdapter
    Dim rptTable As New DataTable

    Try

      MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.GetVeniceConnection, rptAdaptor, "rpt_Venice_TransactionsHavingStatus")
      If (rptAdaptor.SelectCommand.Connection Is Nothing) Then
        Return Nothing
        Exit Function
      End If

      rptAdaptor.SelectCommand.Parameters("@FundID").Value = pFundID
      rptAdaptor.SelectCommand.Parameters("@StartDate").Value = pDateFrom
      rptAdaptor.SelectCommand.Parameters("@EndDate").Value = pDateTo
      rptAdaptor.SelectCommand.Parameters("@TradeStatus").Value = pTradeStatusID
      rptAdaptor.SelectCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate
      rptAdaptor.SelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      SyncLock rptAdaptor.SelectCommand.Connection
        rptAdaptor.Fill(rptTable)
      End SyncLock

    Catch ex As Exception
      MainForm.LogError("ReportHandler, GetData_rptTradeStatus()", LOG_LEVELS.Error, ex.Message, "Error getting transactions data.", ex.StackTrace, True)
      Return Nothing
      Exit Function

    Finally
      ' Close Connection
      Try
        If (Not (rptAdaptor.SelectCommand Is Nothing)) AndAlso (Not (rptAdaptor.SelectCommand.Connection Is Nothing)) Then
          rptAdaptor.SelectCommand.Connection.Close()
        End If
      Catch ex As Exception
      End Try
    End Try

    Return rptTable

  End Function


  ''' <summary>
  ''' Gets the data_rpt prices.
  ''' </summary>
  ''' <param name="pInstrumentID">The p instrument ID.</param>
  ''' <param name="pDateFrom">The p date from.</param>
  ''' <param name="pDateTo">The p date to.</param>
  ''' <returns>DataTable.</returns>
  Public Function GetData_rptPrices( _
  ByVal pInstrumentID As Integer, ByVal pDateFrom As Date, ByVal pDateTo As Date) As DataTable
    ' **************************************************************************************
    '
    '
    ' **************************************************************************************

    ' Configure Adaptor

    Dim rptAdaptor As New SqlDataAdapter
    Dim rptTable As New DataTable

    Try

      MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.GetVeniceConnection, rptAdaptor, "rptPrices")
      If (rptAdaptor.SelectCommand.Connection Is Nothing) Then
        Return Nothing
        Exit Function
      End If

      rptAdaptor.SelectCommand.Parameters("@InstrumentID").Value = pInstrumentID
      rptAdaptor.SelectCommand.Parameters("@DateFrom").Value = pDateFrom
      rptAdaptor.SelectCommand.Parameters("@DateTo").Value = pDateTo
      rptAdaptor.SelectCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate
      rptAdaptor.SelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      SyncLock rptAdaptor.SelectCommand.Connection
        rptAdaptor.Fill(rptTable)
      End SyncLock

    Catch ex As Exception
      MainForm.LogError("ReportHandler, GetData_rptPrices()", LOG_LEVELS.Error, ex.Message, "Error getting rptPrices data.", ex.StackTrace, True)
      Return Nothing
      Exit Function

    Finally
      ' Close Connection
      Try
        If (Not (rptAdaptor.SelectCommand Is Nothing)) AndAlso (Not (rptAdaptor.SelectCommand.Connection Is Nothing)) Then
          rptAdaptor.SelectCommand.Connection.Close()
        End If
      Catch ex As Exception
      End Try
    End Try

    Return rptTable

  End Function

  ''' <summary>
  ''' Gets the data_ pending transactions report.
  ''' </summary>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pReportDate">The p report date.</param>
  ''' <param name="pShowValue">if set to <c>true</c> [p show value].</param>
  ''' <param name="pAddDaysToMonthEnd">The p add days to month end.</param>
  ''' <param name="pAggregateToParent">if set to <c>true</c> [p aggregate to parent].</param>
  ''' <returns>DataTable.</returns>
  Public Function GetData_PendingTransactionsReport(ByVal pFundID As Integer, ByVal pReportDate As Date, ByVal pShowValue As Boolean, ByVal pAddDaysToMonthEnd As Integer, ByVal pAggregateToParent As Boolean) As DataTable
    ' **************************************************************************************
    '
    '
    ' **************************************************************************************

    '

    Dim rptCommand As New SqlCommand
    Dim rptTable As New DataTable

    Try

      rptCommand.CommandType = CommandType.StoredProcedure
      rptCommand.CommandText = "rpt_Venice_PendingTransactionsReport"

      rptCommand.Parameters.Add(New SqlParameter("@FundID", pFundID))
      rptCommand.Parameters.Add(New SqlParameter("@ValueDate", pReportDate))
      rptCommand.Parameters.Add(New SqlParameter("@ShowValue", CInt(pShowValue)))
      rptCommand.Parameters.Add(New SqlParameter("@AddDaysToMonthEnd", pAddDaysToMonthEnd))
      rptCommand.Parameters.Add(New SqlParameter("@AggregateToParentID", CInt(pAggregateToParent)))
      rptCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", MainForm.Main_Knowledgedate))
      rptCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      rptCommand.Connection = MainForm.GetVeniceConnection

      SyncLock rptCommand.Connection
        MainForm.LoadTable_Custom(rptTable, rptCommand)
        'rptTable.Load(rptCommand.ExecuteReader)
      End SyncLock

    Catch ex As Exception
      MainForm.LogError("ReportHandler, GetData_PendingTransactionsReport()", LOG_LEVELS.Error, ex.Message, "Error getting rpt_Venice_PendingTransactionsReport data.", ex.StackTrace, True)
      Return Nothing
      Exit Function

    Finally
      ' Close Connection
      Try
        If (Not (rptCommand Is Nothing)) AndAlso (Not (rptCommand.Connection Is Nothing)) Then
          rptCommand.Connection.Close()
        End If
      Catch ex As Exception
      End Try
      rptCommand.Connection = Nothing
    End Try

    Return rptTable

  End Function

  ''' <summary>
  ''' Gets the data_ pending transactions and values report.
  ''' </summary>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pReportDate">The p report date.</param>
  ''' <param name="pShowValue">if set to <c>true</c> [p show value].</param>
  ''' <param name="pAddDaysToMonthEnd">The p add days to month end.</param>
  ''' <param name="pLookthrough">if set to <c>true</c> [p lookthrough].</param>
  ''' <param name="pAggregateToParent">if set to <c>true</c> [p aggregate to parent].</param>
  ''' <returns>DataTable.</returns>
  Public Function GetData_PendingTransactionsAndValuesReport(ByVal pFundID As Integer, ByVal pReportDate As Date, ByVal pShowValue As Boolean, ByVal pAddDaysToMonthEnd As Integer, ByVal pLookthrough As Boolean, ByVal pAggregateToParent As Boolean) As DataTable
    ' **************************************************************************************
    ' Aaaarrrrggghhhh......
    '
    ' OK, now that's off my chest, here goes....
    '
    ' We are looking to produce a report showing Fund positions, Values (also as a % of fund value) and Transactions through time,
    ' on a Fund LOOKTHROUGH basis.
    '
    ' I intend to get the prevailing positions using the P&L query, because that is currently the only Lookthrough enabled query.
    ' Marry that with the (Milestoned if available) Fund and Unit prices for valuations and imply the transactions from the change in 
    ' Holdings.
    '
    ' **************************************************************************************

    Dim rptCommand As New SqlCommand
    Dim rptTable As New DataTable
    Dim PnLTable As DataTable

    Try

      ' OK, Things I'll need...

      Const Data_HOLDING As Integer = 0
      Const Data_PRICE As Integer = 1

      Const Static_InstrumentID As Integer = 0
      Const Static_InstrumentDescription As Integer = 1
      Const Static_InstrumentType As Integer = 2
      Const Static_FundType As Integer = 3
      Const Static_FundTypeDescription As Integer = 4
      Const Static_CurrencyID As Integer = 5

      Const Dates_Holding As Integer = 0
      Const Dates_Pricing As Integer = 1
      Const Dates_KnowledgeDate As Integer = 2

      Dim DatesArray(2, 6) As Date
      Dim InstrumentsDictionary As New Dictionary(Of Integer, Integer)
      Dim StaticValuesArray(5, 1000) As Object
      Dim DataArray(6, 2, 1000) As Double
      Dim FundNAVArray(6) As Double

      Dim MaxInstrumentIndex As Integer = (-1)

      Dim ThisInstrumentID As Integer
      Dim ThisInstrumentType As Integer
      Dim InstrumentIndex As Integer

      Dim FundFXID As Integer
      Dim FundUnitInstrumentID As Integer
      Dim thisFundCode As String = "<Missing>"
      Dim MilestoneCounter As Integer = 0
      Dim DateCounter As Integer = 0
      Dim InstrumentCounter As Integer

      ' Get Fund FX Code

      Try
        FundFXID = CInt(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblFund, pFundID, "FundBaseCurrency"))
        FundUnitInstrumentID = CInt(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblFund, pFundID, "FundUnit"))
        thisFundCode = CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblFund, pFundID, "FundCode"))
      Catch ex As Exception
        FundFXID = RenaissanceGlobals.Currency.USD
      End Try

      '1) Initialise Dates array
      '   End up with an array of planned Holding and Pricing Dates

      DatesArray(Dates_Pricing, 0) = pReportDate
      DatesArray(Dates_Holding, 0) = pReportDate
      DatesArray(Dates_KnowledgeDate, 0) = MainForm.Main_Knowledgedate

      For DateCounter = 1 To 5
        If (DateCounter = 1) Then
          ' If not at End-Of-Month, Move to End-Of-Month 
          ' ElseIf pAddDaysToMonthEnd > 0 use This EOM & Forward Holding Date
          ' Else Use next month end.

          If (pReportDate.Month = (pReportDate.AddDays(1).Month)) Then
            DatesArray(Dates_Pricing, DateCounter) = pReportDate.AddMonths(1)
            DatesArray(Dates_Pricing, DateCounter) = DatesArray(Dates_Pricing, DateCounter).AddDays(-DatesArray(Dates_Pricing, DateCounter).Day)
            DatesArray(Dates_Holding, DateCounter) = DatesArray(Dates_Pricing, DateCounter).AddDays(pAddDaysToMonthEnd)

          ElseIf (pAddDaysToMonthEnd > 0) Then
            DatesArray(Dates_Pricing, DateCounter) = pReportDate
            DatesArray(Dates_Holding, DateCounter) = pReportDate.AddDays(pAddDaysToMonthEnd)

          Else
            DatesArray(Dates_Pricing, DateCounter) = pReportDate.AddMonths(2)
            DatesArray(Dates_Pricing, DateCounter) = DatesArray(Dates_Pricing, DateCounter).AddDays(-DatesArray(Dates_Pricing, DateCounter).Day)
            DatesArray(Dates_Holding, DateCounter) = DatesArray(Dates_Pricing, DateCounter).AddDays(pAddDaysToMonthEnd)

          End If

        Else
          DatesArray(Dates_Pricing, DateCounter) = DatesArray(Dates_Pricing, DateCounter - 1).AddMonths(2)
          DatesArray(Dates_Pricing, DateCounter) = DatesArray(Dates_Pricing, DateCounter).AddDays(-DatesArray(Dates_Pricing, DateCounter).Day)
          DatesArray(Dates_Holding, DateCounter) = DatesArray(Dates_Pricing, DateCounter).AddDays(pAddDaysToMonthEnd)
        End If

        ' Get KnowledgeDate

        DatesArray(Dates_KnowledgeDate, DateCounter) = MainForm.Main_Knowledgedate

      Next

      ' OK. Lets Cycle through, using the P&L Query to retrieve Holdings.


      rptCommand.CommandType = CommandType.StoredProcedure
      rptCommand.CommandText = "rptProfitAndLoss"

      rptCommand.Parameters.Add(New SqlParameter("@FundID", pFundID))
      rptCommand.Parameters.Add(New SqlParameter("@ValueDateFrom", SqlDbType.DateTime))
      rptCommand.Parameters.Add(New SqlParameter("@ValueDateTo", SqlDbType.DateTime))
      rptCommand.Parameters.Add(New SqlParameter("@UnitPriceVariant", CInt(0)))

      rptCommand.Parameters.Add("@StatusGroupFilter", SqlDbType.NVarChar).Value = DEFAULT_STATUSGROUPFILTER
      rptCommand.Parameters.Add("@AdministratorDatesFilter", SqlDbType.Int).Value = DEFAULT_ADMINISTRATORDATESFILTER

      rptCommand.Parameters.Add(New SqlParameter("@DisregardWatermarkPoints", CInt(0)))
      rptCommand.Parameters.Add(New SqlParameter("@IsLookthrough", CInt(pLookthrough)))
      rptCommand.Parameters.Add(New SqlParameter("@MaintainConsistentFundPnLs", CInt(0)))
      rptCommand.Parameters.Add(New SqlParameter("@KnowledgeDateFrom", SqlDbType.DateTime))
      rptCommand.Parameters.Add(New SqlParameter("@KnowledgeDateTo", SqlDbType.DateTime))
      rptCommand.Parameters.Add(New SqlParameter("@KnowledgeDateLinkedTables", MainForm.Main_Knowledgedate))
      rptCommand.Parameters.Add(New SqlParameter("@AggregateToInstrumentParent", CInt(pAggregateToParent)))
      rptCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      rptCommand.Connection = MainForm.GetVeniceConnection

      For DateCounter = 0 To 4 Step 2 ' (Each P&L Query picks up Two (Start & End) Fund Values.)

        ' Get Holding Data (From P&L Query)

        rptCommand.Parameters("@ValueDateFrom").Value = DatesArray(Dates_Holding, DateCounter)
        rptCommand.Parameters("@ValueDateTo").Value = DatesArray(Dates_Holding, DateCounter + 1)
        rptCommand.Parameters("@KnowledgeDateFrom").Value = DatesArray(Dates_KnowledgeDate, DateCounter)
        rptCommand.Parameters("@KnowledgeDateTo").Value = DatesArray(Dates_KnowledgeDate, DateCounter + 1)
        rptCommand.Parameters("@KnowledgeDateLinkedTables").Value = MainForm.Main_Knowledgedate

        PnLTable = New DataTable

        SyncLock rptCommand.Connection
          MainForm.LoadTable_Custom(PnLTable, rptCommand)
          'PnLTable.Load(rptCommand.ExecuteReader)
        End SyncLock

        ' Get Data.

        If (PnLTable.Rows.Count > 0) Then
          ' Fund valuations
          ' if pAddDaysToMonthEnd = 0 then the prices picked off the P&L Query should be OK.
          'If (pAddDaysToMonthEnd <= 0) Then
          FundNAVArray(DateCounter) = CDbl(PnLTable.Rows(0)("NetFundValue1"))
          FundNAVArray(DateCounter + 1) = CDbl(PnLTable.Rows(0)("NetFundValue2"))
          'End If

          For Each ThisPnlRow As DataRow In PnLTable.Rows
            If (CInt(ThisPnlRow("InstrumentType")) = RenaissanceGlobals.InstrumentTypes.Fund) OrElse (CInt(ThisPnlRow("InstrumentType")) = RenaissanceGlobals.InstrumentTypes.Share) OrElse (CInt(ThisPnlRow("InstrumentType")) = RenaissanceGlobals.InstrumentTypes.Cash) OrElse (CInt(ThisPnlRow("InstrumentType")) = RenaissanceGlobals.InstrumentTypes.CashFund) OrElse (CInt(ThisPnlRow("InstrumentType")) = RenaissanceGlobals.InstrumentTypes.Margin) Then

              ' Has this Instrument been encountered before ? If not add the static data.

              ThisInstrumentID = CInt(ThisPnlRow("Instrument"))

              If Not InstrumentsDictionary.ContainsKey(ThisInstrumentID) Then
                InstrumentsDictionary.Add(ThisInstrumentID, InstrumentsDictionary.Count)

                InstrumentIndex = InstrumentsDictionary(ThisInstrumentID)
                MaxInstrumentIndex = Math.Max(MaxInstrumentIndex, InstrumentIndex)

                If (InstrumentIndex >= UBound(StaticValuesArray, 2)) Then
                  ReDim Preserve StaticValuesArray(UBound(StaticValuesArray, 1), UBound(StaticValuesArray, 2) + 1000)
                  ReDim Preserve DataArray(UBound(DataArray, 1), UBound(DataArray, 2), UBound(DataArray, 3) + 1000)
                End If

                StaticValuesArray(Static_InstrumentID, InstrumentIndex) = ThisInstrumentID
                StaticValuesArray(Static_InstrumentDescription, InstrumentIndex) = CStr(ThisPnlRow("InstrumentDescription"))
                StaticValuesArray(Static_InstrumentType, InstrumentIndex) = CInt(ThisPnlRow("InstrumentType"))
                StaticValuesArray(Static_FundType, InstrumentIndex) = CInt(ThisPnlRow("InstrumentFundType"))
                StaticValuesArray(Static_FundTypeDescription, InstrumentIndex) = CStr(ThisPnlRow("FundTypeDescription"))
                StaticValuesArray(Static_CurrencyID, InstrumentIndex) = CInt(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, ThisInstrumentID, "InstrumentCurrencyID"))

              Else
                InstrumentIndex = InstrumentsDictionary(ThisInstrumentID)
              End If

              ' Set Data
              ' What about Currency ? - All prices converted to USD later.

              DataArray(DateCounter, Data_HOLDING, InstrumentIndex) += CDbl(ThisPnlRow("StartUnits"))
              DataArray(DateCounter + 1, Data_HOLDING, InstrumentIndex) += CDbl(ThisPnlRow("FinalUnits"))
              DataArray(DateCounter, Data_PRICE, InstrumentIndex) = CDbl(Nz(ThisPnlRow("StartPrice"), 0))
              DataArray(DateCounter + 1, Data_PRICE, InstrumentIndex) = CDbl(ThisPnlRow("EndPrice"))

            End If

          Next ' DataRow
        End If ' Rows Exist

      Next ' DatCounter Step 2

      ' Get Fund Valuations, Unit Prices and Instrument Prices
      ' Apply FXs

      Dim PricingQuery As New SqlCommand
      Dim FXQuery As New SqlCommand

      PricingQuery.CommandType = CommandType.Text
      PricingQuery.CommandText = "SELECT * FROM dbo.fn_BestPrice(@ValueDate, 0, @KnowledgeDate)"
      PricingQuery.Parameters.Add(New SqlParameter("@ValueDate", SqlDbType.DateTime))
      PricingQuery.Parameters.Add(New SqlParameter("@KnowledgeDate", SqlDbType.DateTime))
      PricingQuery.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      FXQuery.CommandType = CommandType.Text
      FXQuery.CommandText = "SELECT * FROM dbo.fn_BestFX(@ValueDate, @KnowledgeDate)"
      FXQuery.Parameters.Add(New SqlParameter("@ValueDate", SqlDbType.DateTime))
      FXQuery.Parameters.Add(New SqlParameter("@KnowledgeDate", SqlDbType.DateTime))
      FXQuery.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      Try
        PricingQuery.Connection = rptCommand.Connection
        FXQuery.Connection = rptCommand.Connection

        Dim MonthFX As Dictionary(Of Integer, Double)
        Dim MonthPrices As Dictionary(Of Integer, Double)
        Dim FXTable As DataTable
        Dim PriceTable As DataTable
        Dim UnitsTable As DataTable

        For DateCounter = 0 To 5
          MonthFX = New Dictionary(Of Integer, Double)
          MonthPrices = New Dictionary(Of Integer, Double)
          FXTable = New DataTable
          PriceTable = New DataTable
          UnitsTable = New DataTable

          ' Get Prices
          If (pAddDaysToMonthEnd > 0) Then
            ' if pAddDaysToMonthEnd = 0 then the prices picked off the P&L Query should be OK.

            PricingQuery.Parameters("@ValueDate").Value = DatesArray(Dates_Pricing, DateCounter)
            PricingQuery.Parameters("@KnowledgeDate").Value = DatesArray(Dates_KnowledgeDate, DateCounter)

            MainForm.LoadTable_Custom(PriceTable, PricingQuery)
            'PriceTable.Load(PricingQuery.ExecuteReader)

            For Each PriceRow As DataRow In PriceTable.Rows
              MonthPrices.Add(CInt(PriceRow("InstrumentID")), CDbl(PriceRow("PriceLevel")))
            Next
            PriceTable.Clear()

          End If

          ' Get FXs

          FXQuery.Parameters("@ValueDate").Value = DatesArray(Dates_Pricing, DateCounter)
          FXQuery.Parameters("@KnowledgeDate").Value = DatesArray(Dates_KnowledgeDate, DateCounter)

          MainForm.LoadTable_Custom(FXTable, FXQuery)
          'FXTable.Load(FXQuery.ExecuteReader)

          For Each FXRow As DataRow In FXTable.Rows
            MonthFX.Add(CInt(FXRow("FXCurrencyCode")), CDbl(FXRow("FXRate")))
          Next
          FXTable.Clear()

          ' OK, Apply Pricing and FX to the Data Table

          Dim ThisPrice As Double
          Dim ThisFX As Double
          Dim ThisCurrencyCode As Integer

          For InstrumentCounter = 0 To MaxInstrumentIndex
            ThisInstrumentID = CInt(StaticValuesArray(Static_InstrumentID, InstrumentCounter))
            ThisInstrumentType = CInt(StaticValuesArray(Static_InstrumentType, InstrumentCounter))
            ThisCurrencyCode = CInt(StaticValuesArray(Static_CurrencyID, InstrumentCounter))
            ThisPrice = 0.0#
            ThisFX = 1.0#

            If (pAddDaysToMonthEnd > 0) Then

              If (ThisInstrumentType = RenaissanceGlobals.InstrumentTypes.Cash) OrElse (ThisInstrumentType = RenaissanceGlobals.InstrumentTypes.CashFund) OrElse (ThisInstrumentType = RenaissanceGlobals.InstrumentTypes.Margin) Then
                ThisPrice = 1.0#
              ElseIf (MonthPrices.ContainsKey(ThisInstrumentID)) Then
                ThisPrice = MonthPrices(ThisInstrumentID)

                If (MonthFX.ContainsKey(ThisCurrencyCode)) Then
                  ThisFX = MonthFX(ThisCurrencyCode)
                End If
              End If

            Else
              ' if pAddDaysToMonthEnd = 0 then the prices picked off the P&L Query should be OK.

              If (ThisInstrumentType = RenaissanceGlobals.InstrumentTypes.Cash) OrElse (ThisInstrumentType = RenaissanceGlobals.InstrumentTypes.CashFund) OrElse (ThisInstrumentType = RenaissanceGlobals.InstrumentTypes.Margin) Then
                ThisPrice = 1.0#
              Else
                ThisPrice = DataArray(DateCounter, Data_PRICE, InstrumentCounter)
              End If

              If (MonthFX.ContainsKey(ThisCurrencyCode)) Then
                ThisFX = MonthFX(ThisCurrencyCode)
              End If

            End If

            DataArray(DateCounter, Data_PRICE, InstrumentCounter) = ThisPrice * ThisFX
          Next

          ' Get FX to USD for the Fund and adjust FundNAV to USD.

          ThisFX = 1.0#
          If (MonthFX.ContainsKey(FundFXID)) Then
            ThisFX = MonthFX(FundFXID)
          End If

          FundNAVArray(DateCounter) *= ThisFX

          MonthFX.Clear()
          MonthPrices.Clear()
        Next

      Catch ex As Exception
      Finally
        PricingQuery.Connection = Nothing
        FXQuery.Connection = Nothing
      End Try

      ' OK, No we have all the Instrument details together with holding, Pricing and Fund NAV information for each month across our reporting period.
      ' Construct the report Table....

      rptTable.Columns.Add(New DataColumn("Fund", GetType(Integer)))
      rptTable.Columns.Add(New DataColumn("FundCode", GetType(String)))
      rptTable.Columns.Add(New DataColumn("Instrument", GetType(Integer)))
      rptTable.Columns.Add(New DataColumn("InstrumentDescription", GetType(String)))
      rptTable.Columns.Add(New DataColumn("InstrumentType", GetType(Integer)))
      rptTable.Columns.Add(New DataColumn("InstrumentFundType", GetType(Integer)))
      rptTable.Columns.Add(New DataColumn("FundTypeDescription", GetType(String)))
      rptTable.Columns.Add(New DataColumn("Month0_Value", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month1_Value", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month2_Value", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month3_Value", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month4_Value", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month5_Value", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month6_Value", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month0_Holding", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month1_Holding", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month2_Holding", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month3_Holding", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month4_Holding", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month5_Holding", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month6_Holding", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month0_Transaction", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month1_Transaction", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month2_Transaction", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month3_Transaction", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month4_Transaction", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month5_Transaction", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month6_Transaction", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month0_TransactionUSD", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month1_TransactionUSD", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month2_TransactionUSD", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month3_TransactionUSD", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month4_TransactionUSD", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month5_TransactionUSD", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month6_TransactionUSD", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month0_FundUsdNAV", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month1_FundUsdNAV", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month2_FundUsdNAV", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month3_FundUsdNAV", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month4_FundUsdNAV", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month5_FundUsdNAV", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month6_FundUsdNAV", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month0_Percentage", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month1_Percentage", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month2_Percentage", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month3_Percentage", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month4_Percentage", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month5_Percentage", GetType(Double)))
      rptTable.Columns.Add(New DataColumn("Month6_Percentage", GetType(Double)))

      Dim ThisReportRow As DataRow
      Dim IsEqualisation As Integer
      Dim HasHolding As Boolean

      ' FundNAVArray(DateCounter) 

      For InstrumentCounter = 0 To MaxInstrumentIndex
        ThisReportRow = rptTable.NewRow
        ThisInstrumentID = CInt(StaticValuesArray(Static_InstrumentID, InstrumentCounter))
        ThisInstrumentType = CInt(StaticValuesArray(Static_InstrumentType, InstrumentCounter))

        Try
          IsEqualisation = CInt(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, ThisInstrumentID, "InstrumentIsEqualisation"))
        Catch ex As Exception
          IsEqualisation = 0
        End Try

        HasHolding = False
        If (Math.Abs(DataArray(0, Data_HOLDING, InstrumentCounter)) > 0.001) OrElse (Math.Abs(DataArray(1, Data_HOLDING, InstrumentCounter)) > 0.001) OrElse (Math.Abs(DataArray(2, Data_HOLDING, InstrumentCounter)) > 0.001) OrElse (Math.Abs(DataArray(3, Data_HOLDING, InstrumentCounter)) > 0.001) OrElse (Math.Abs(DataArray(4, Data_HOLDING, InstrumentCounter)) > 0.001) OrElse (Math.Abs(DataArray(5, Data_HOLDING, InstrumentCounter)) > 0.001) Then
          HasHolding = True
        End If

        If (IsEqualisation = 0) AndAlso (HasHolding) Then

          ThisReportRow("Fund") = pFundID
          ThisReportRow("FundCode") = thisFundCode
          ThisReportRow("Instrument") = ThisInstrumentID
          ThisReportRow("InstrumentDescription") = StaticValuesArray(Static_InstrumentDescription, InstrumentCounter)
          ThisReportRow("InstrumentFundType") = StaticValuesArray(Static_FundType, InstrumentCounter)
          ThisReportRow("InstrumentType") = ThisInstrumentType
          ThisReportRow("FundTypeDescription") = StaticValuesArray(Static_FundTypeDescription, InstrumentCounter)
          ThisReportRow("Month0_Value") = DataArray(0, Data_HOLDING, InstrumentCounter) * DataArray(0, Data_PRICE, InstrumentCounter) / 1000000.0# ' Value in Millions
          ThisReportRow("Month1_Value") = DataArray(1, Data_HOLDING, InstrumentCounter) * DataArray(1, Data_PRICE, InstrumentCounter) / 1000000.0#
          ThisReportRow("Month2_Value") = DataArray(2, Data_HOLDING, InstrumentCounter) * DataArray(2, Data_PRICE, InstrumentCounter) / 1000000.0#
          ThisReportRow("Month3_Value") = DataArray(3, Data_HOLDING, InstrumentCounter) * DataArray(3, Data_PRICE, InstrumentCounter) / 1000000.0#
          ThisReportRow("Month4_Value") = DataArray(4, Data_HOLDING, InstrumentCounter) * DataArray(4, Data_PRICE, InstrumentCounter) / 1000000.0#
          ThisReportRow("Month5_Value") = DataArray(5, Data_HOLDING, InstrumentCounter) * DataArray(5, Data_PRICE, InstrumentCounter) / 1000000.0#
          ThisReportRow("Month6_Value") = 0
          ThisReportRow("Month0_Holding") = DataArray(0, Data_HOLDING, InstrumentCounter)
          ThisReportRow("Month1_Holding") = DataArray(1, Data_HOLDING, InstrumentCounter)
          ThisReportRow("Month2_Holding") = DataArray(2, Data_HOLDING, InstrumentCounter)
          ThisReportRow("Month3_Holding") = DataArray(3, Data_HOLDING, InstrumentCounter)
          ThisReportRow("Month4_Holding") = DataArray(4, Data_HOLDING, InstrumentCounter)
          ThisReportRow("Month5_Holding") = DataArray(5, Data_HOLDING, InstrumentCounter)
          ThisReportRow("Month6_Holding") = 0

          If (ThisInstrumentType = RenaissanceGlobals.InstrumentTypes.Cash) OrElse (ThisInstrumentType = RenaissanceGlobals.InstrumentTypes.CashFund) OrElse (ThisInstrumentType = RenaissanceGlobals.InstrumentTypes.Margin) Then

            ThisReportRow("Month0_Transaction") = 0
            ThisReportRow("Month1_Transaction") = 0
            ThisReportRow("Month2_Transaction") = 0
            ThisReportRow("Month3_Transaction") = 0
            ThisReportRow("Month4_Transaction") = 0
            ThisReportRow("Month5_Transaction") = 0
            ThisReportRow("Month6_Transaction") = 0
            ThisReportRow("Month0_TransactionUSD") = 0
            ThisReportRow("Month1_TransactionUSD") = 0
            ThisReportRow("Month2_TransactionUSD") = 0
            ThisReportRow("Month3_TransactionUSD") = 0
            ThisReportRow("Month4_TransactionUSD") = 0
            ThisReportRow("Month5_TransactionUSD") = 0
            ThisReportRow("Month6_TransactionUSD") = 0

          Else

            ThisReportRow("Month0_Transaction") = 0.0#
            ThisReportRow("Month1_Transaction") = CDbl(ThisReportRow("Month1_Holding")) - CDbl(ThisReportRow("Month0_Holding"))
            ThisReportRow("Month2_Transaction") = CDbl(ThisReportRow("Month2_Holding")) - CDbl(ThisReportRow("Month1_Holding"))
            ThisReportRow("Month3_Transaction") = CDbl(ThisReportRow("Month3_Holding")) - CDbl(ThisReportRow("Month2_Holding"))
            ThisReportRow("Month4_Transaction") = CDbl(ThisReportRow("Month4_Holding")) - CDbl(ThisReportRow("Month3_Holding"))
            ThisReportRow("Month5_Transaction") = CDbl(ThisReportRow("Month5_Holding")) - CDbl(ThisReportRow("Month4_Holding"))
            ThisReportRow("Month6_Transaction") = 0
            ThisReportRow("Month0_TransactionUSD") = 0
            ThisReportRow("Month1_TransactionUSD") = ((CDbl(ThisReportRow("Month1_Holding")) - CDbl(ThisReportRow("Month0_Holding"))) * DataArray(1, Data_PRICE, InstrumentCounter)) / 1000000.0#   ' Transaction Value in Millions
            ThisReportRow("Month2_TransactionUSD") = ((CDbl(ThisReportRow("Month2_Holding")) - CDbl(ThisReportRow("Month1_Holding"))) * DataArray(2, Data_PRICE, InstrumentCounter)) / 1000000.0#
            ThisReportRow("Month3_TransactionUSD") = ((CDbl(ThisReportRow("Month3_Holding")) - CDbl(ThisReportRow("Month2_Holding"))) * DataArray(3, Data_PRICE, InstrumentCounter)) / 1000000.0#
            ThisReportRow("Month4_TransactionUSD") = ((CDbl(ThisReportRow("Month4_Holding")) - CDbl(ThisReportRow("Month3_Holding"))) * DataArray(4, Data_PRICE, InstrumentCounter)) / 1000000.0#
            ThisReportRow("Month5_TransactionUSD") = ((CDbl(ThisReportRow("Month5_Holding")) - CDbl(ThisReportRow("Month4_Holding"))) * DataArray(5, Data_PRICE, InstrumentCounter)) / 1000000.0#
            ThisReportRow("Month6_TransactionUSD") = 0

          End If

          ThisReportRow("Month0_FundUsdNAV") = FundNAVArray(0) / 1000000.0#   ' NAV in Millions
          ThisReportRow("Month1_FundUsdNAV") = FundNAVArray(0) / 1000000.0#
          ThisReportRow("Month2_FundUsdNAV") = FundNAVArray(0) / 1000000.0#
          ThisReportRow("Month3_FundUsdNAV") = FundNAVArray(0) / 1000000.0#
          ThisReportRow("Month4_FundUsdNAV") = FundNAVArray(0) / 1000000.0#
          ThisReportRow("Month5_FundUsdNAV") = FundNAVArray(0) / 1000000.0#
          ThisReportRow("Month6_FundUsdNAV") = FundNAVArray(0) / 1000000.0#
          ThisReportRow("Month0_Percentage") = CDbl(ThisReportRow("Month0_Value")) / Math.Max(CDbl(ThisReportRow("Month0_FundUsdNAV")), 1)
          ThisReportRow("Month1_Percentage") = CDbl(ThisReportRow("Month1_Value")) / Math.Max(CDbl(ThisReportRow("Month1_FundUsdNAV")), 1)
          ThisReportRow("Month2_Percentage") = CDbl(ThisReportRow("Month2_Value")) / Math.Max(CDbl(ThisReportRow("Month2_FundUsdNAV")), 1)
          ThisReportRow("Month3_Percentage") = CDbl(ThisReportRow("Month3_Value")) / Math.Max(CDbl(ThisReportRow("Month3_FundUsdNAV")), 1)
          ThisReportRow("Month4_Percentage") = CDbl(ThisReportRow("Month4_Value")) / Math.Max(CDbl(ThisReportRow("Month4_FundUsdNAV")), 1)
          ThisReportRow("Month5_Percentage") = CDbl(ThisReportRow("Month5_Value")) / Math.Max(CDbl(ThisReportRow("Month5_FundUsdNAV")), 1)
          ThisReportRow("Month6_Percentage") = 0

          rptTable.Rows.Add(ThisReportRow)

        End If
      Next

    Catch ex As Exception
      MainForm.LogError("ReportHandler, GetData_PendingTransactionsReport_ADC_Monster()", LOG_LEVELS.Error, ex.Message, "Error getting rpt_Venice_PendingTransactionsAndValuesReport data.", ex.StackTrace, True)
      Return Nothing
      Exit Function
    Finally
      ' Close Connection
      Try
        If (Not (rptCommand Is Nothing)) AndAlso (Not (rptCommand.Connection Is Nothing)) Then
          rptCommand.Connection.Close()
        End If
      Catch ex As Exception
      End Try
      rptCommand.Connection = Nothing
    End Try

    Return rptTable

  End Function


  ''' <summary>
  ''' Gets the data_rpt master fund allocation.
  ''' </summary>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pReportDate">The p report date.</param>
  ''' <param name="pStatusGroupFilter">The p status group filter.</param>
  ''' <param name="pAdministratorDatesFilter">The p administrator dates filter.</param>
  ''' <param name="pKnowledgeDateFrom">The p knowledge date from.</param>
  ''' <param name="pKnowledgeDateTo">The p knowledge date to.</param>
  ''' <param name="pKnowledgeDateLinkedTables">The p knowledge date linked tables.</param>
  ''' <returns>DataTable.</returns>
  Public Function GetData_rptMasterFundAllocation( _
  ByVal pFundID As Integer, ByVal pReportDate As Date, ByVal pStatusGroupFilter As String, ByVal pAdministratorDatesFilter As Integer, ByVal pKnowledgeDateFrom As Date, ByVal pKnowledgeDateTo As Date, ByVal pKnowledgeDateLinkedTables As Date) As DataTable
    ' **************************************************************************************
    '
    '
    ' **************************************************************************************

    ' Configure Adaptor

    Dim rptAdaptor As New SqlDataAdapter
    Dim rptTable As New DataTable

    Try

      MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.GetVeniceConnection, rptAdaptor, "rptMasterFundAllocation")
      If (rptAdaptor.SelectCommand.Connection Is Nothing) Then
        Return Nothing
        Exit Function
      End If

      rptAdaptor.SelectCommand.Parameters("@MasterFundID").Value = pFundID
      rptAdaptor.SelectCommand.Parameters("@ReportDate").Value = pReportDate
      rptAdaptor.SelectCommand.Parameters("@StatusGroupFilter").Value = pStatusGroupFilter
      rptAdaptor.SelectCommand.Parameters("@AdministratorDatesFilter").Value = pAdministratorDatesFilter
      rptAdaptor.SelectCommand.Parameters("@KnowledgeDateFrom").Value = pKnowledgeDateFrom
      rptAdaptor.SelectCommand.Parameters("@KnowledgeDateTo").Value = pKnowledgeDateTo
      rptAdaptor.SelectCommand.Parameters("@KnowledgeDateLinkedTables").Value = pKnowledgeDateLinkedTables
      rptAdaptor.SelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      SyncLock rptAdaptor.SelectCommand.Connection
        rptAdaptor.Fill(rptTable)
      End SyncLock

    Catch ex As Exception
      MainForm.LogError("ReportHandler, GetData_rptMasterFundAllocation()", LOG_LEVELS.Error, ex.Message, "Error getting rptMasterFundAllocation data.", ex.StackTrace, True)
      Return Nothing
      Exit Function

    Finally
      ' Close Connection
      Try
        If (Not (rptAdaptor.SelectCommand Is Nothing)) AndAlso (Not (rptAdaptor.SelectCommand.Connection Is Nothing)) Then
          rptAdaptor.SelectCommand.Connection.Close()
        End If
      Catch ex As Exception
      End Try
    End Try

    Return rptTable

  End Function

  ''' <summary>
  ''' Gets the data_ PFPC reconciliation.
  ''' </summary>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pReportDate">The p report date.</param>
  ''' <param name="pDetail">if set to <c>true</c> [p detail].</param>
  ''' <returns>DataView.</returns>
  Public Function GetData_PFPCReconciliation(ByVal pFundID As Integer, ByVal pReportDate As Date, ByVal pDetail As Boolean) As DataView
    ' **************************************************************************************
    '
    '
    ' **************************************************************************************
    Dim SelectedRows As DataView

    ' Configure Adaptor

    Dim rptAdaptor As New SqlDataAdapter
    Dim rptTable As New DataTable

    ' Get the Prices table, for use in dating prices later.

    Dim tblPrice As DSPrice.tblPriceDataTable = Nothing
    Dim PriceRow As DSPrice.tblPriceRow
    Dim SelectedPrices() As DSPrice.tblPriceRow

    Try
      SelectedRows = Nothing

      MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.GetVeniceConnection, rptAdaptor, "rptPFPCPortfolioReconcilliation")
      If (rptAdaptor.SelectCommand.Connection Is Nothing) Then
        Return Nothing
        Exit Function
      End If

      rptAdaptor.SelectCommand.Parameters("@FundID").Value = pFundID
      rptAdaptor.SelectCommand.Parameters("@LegalEntity").Value = ""
      rptAdaptor.SelectCommand.Parameters("@ReportDate").Value = pReportDate
      rptAdaptor.SelectCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate
      rptAdaptor.SelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      SyncLock rptAdaptor.SelectCommand.Connection
        rptAdaptor.Fill(rptTable)
      End SyncLock

      ' Construct custom fields
      ' 1) Add a field to contain the 'Age' in days, of the price being used.

      ' =IIf(([FCP_PriceFinal]=0) And ([FCP_PriceAdministrator]=0),
      '   IIf([FCP_PriceDate]<=CDate("1 Jan 1900"),
      '       "",
      '       CDate(get_LocalDate("rptPFPCPortfolioReconcilliation_ValueDate"))-nz([FCP_FinalPriceOnlyDate],CDate(get_LocalDate("rptPFPCPortfolioReconcilliation_ValueDate"))-365))
      '   ,"")

      rptTable.Columns.Add(New DataColumn("FinalPriceAge", GetType(Double)))

      Dim RowCount As Integer
      Dim PriceAdaptor As New SqlDataAdapter

      Try
        MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION), PriceAdaptor, "TBLPRICEBYINSTRUMENT")

        PriceAdaptor.SelectCommand.Parameters("@InstrumentID").Value = (-1)
        PriceAdaptor.SelectCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

        For RowCount = 0 To (rptTable.Rows.Count - 1)
          rptTable.Rows(RowCount)("FinalPriceAge") = 0

          If IsNumeric(rptTable.Rows(RowCount)("FCPInstrumentID")) AndAlso CInt(rptTable.Rows(RowCount)("FCPInstrumentID")) > 0 Then
            If (CInt(rptTable.Rows(RowCount)("FCP_PriceFinal")) = 0) And (CInt(rptTable.Rows(RowCount)("FCP_PriceAdministrator")) = 0) Then

              ' Load Price Data

              Try
                PriceAdaptor.SelectCommand.Parameters("@InstrumentID").Value = CInt(rptTable.Rows(RowCount)("FCPInstrumentID"))
                PriceAdaptor.SelectCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

                If (tblPrice Is Nothing) Then
                  tblPrice = New RenaissanceDataClass.DSPrice.tblPriceDataTable
                Else
                  tblPrice.Clear()
                End If

                SyncLock PriceAdaptor.SelectCommand.Connection
                  MainForm.LoadTable_Custom(tblPrice, PriceAdaptor.SelectCommand)
                  'tblPrice.Load(PriceAdaptor.SelectCommand.ExecuteReader)
                End SyncLock

              Catch ex As Exception
                MainForm.LogError("GetData_PFPCReconciliation()", LOG_LEVELS.Error, ex.Message, "Error Loading / Selecting Instrument Prices.", ex.StackTrace, True)

                If (tblPrice Is Nothing) Then
                  tblPrice = New RenaissanceDataClass.DSPrice.tblPriceDataTable
                Else
                  tblPrice.Clear()
                End If
              End Try


              If (tblPrice IsNot Nothing) Then
                SelectedPrices = tblPrice.Select("(PRICEDate <= '" & CDate(rptTable.Rows(RowCount)("FCP_PriceDate")).ToString(QUERY_SHORTDATEFORMAT) & "') AND ((PriceIsAdministrator=1) OR (PriceIsMilestone<>0) OR (PriceIsMilestone=1))", "PriceDate")
                'PriceView.RowFilter = "(PriceInstrument=" & CInt(rptTable.Rows(RowCount)("FCPInstrumentID")).ToString & ") AND (PRICEDate <= '" & CDate(rptTable.Rows(RowCount)("FCP_PriceDate")).ToString(QUERY_SHORTDATEFORMAT) & "') AND ((PriceIsAdministrator=1) OR (PriceIsMilestone<>0) OR (PriceIsMilestone=1))"

                If SelectedPrices.Length > 0 Then
                  PriceRow = SelectedPrices(SelectedPrices.Length - 1)

                  rptTable.Rows(RowCount)("FinalPriceAge") = CInt(pReportDate.ToOADate - CDate(PriceRow.PriceDate).ToOADate)
                Else
                  rptTable.Rows(RowCount)("FinalPriceAge") = 365
                End If
              Else
                rptTable.Rows(RowCount)("FinalPriceAge") = 365
              End If

            Else
              ' Is a Final or Administrator Price.
              Try
                rptTable.Rows(RowCount)("FinalPriceAge") = CInt(pReportDate.ToOADate - CDate(rptTable.Rows(RowCount)("FCP_PriceDate")).ToOADate)
              Catch ex As Exception
                rptTable.Rows(RowCount)("FinalPriceAge") = 365
              End Try
            End If
          End If

        Next


      Catch ex As Exception
        MainForm.LogError("ReportHandler, GetData_PFPCReconciliation()", LOG_LEVELS.Error, ex.Message, "Error getting rptMasterFundAllocation data.", ex.StackTrace, True)
        Return Nothing
        Exit Function

      Finally
        Try
          ' Just in case, free the reference to the Main Venice Connection.
          ' Do not Close it.

          If (PriceAdaptor.SelectCommand IsNot Nothing) Then
            PriceAdaptor.SelectCommand.Connection = Nothing
          End If
          PriceAdaptor = Nothing

        Catch ex As Exception
        End Try
      End Try

      If pDetail Then
        SelectedRows = New DataView(rptTable, "(FCP_Holding<(-0.001)) OR (FCP_Holding>0.001) OR (Holding_Difference<(-0.01)) OR (Holding_Difference>(0.01)) OR (BookCcyValue_Difference < (-1)) OR (BookCcyValue_Difference > (1))", "", DataViewRowState.CurrentRows)
      Else
        SelectedRows = New DataView(rptTable, "(Holding_Difference<(-0.01)) OR (Holding_Difference>(0.01)) OR (BookCcyValue_Difference < (-1)) OR (BookCcyValue_Difference > (1))", "", DataViewRowState.CurrentRows)
      End If

    Catch ex As Exception
      MainForm.LogError("ReportHandler, GetData_PFPCReconciliation()", LOG_LEVELS.Error, ex.Message, "Error getting rptMasterFundAllocation data.", ex.StackTrace, True)
      Return Nothing
      Exit Function

    Finally
      ' Close Connection
      Try
        If (Not (rptAdaptor.SelectCommand Is Nothing)) AndAlso (Not (rptAdaptor.SelectCommand.Connection Is Nothing)) Then
          rptAdaptor.SelectCommand.Connection.Close()
        End If
      Catch ex As Exception
      End Try
    End Try

    Return SelectedRows

  End Function


    ''' <summary>
    ''' Gets the data_rpt transaction price difference.
    ''' </summary>
    ''' <param name="pFundID">The p fund ID.</param>
    ''' <returns>DataTable.</returns>
  Public Function GetData_rptTransactionPriceDifference(ByVal pFundID As Integer) As DataTable
    ' **************************************************************************************
    '
    '
    ' **************************************************************************************

    ' Configure Adaptor

    Dim rptAdaptor As New SqlDataAdapter
    Dim rptTable As New DataTable

    Try

      ' rptUnitPrice
      MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.GetVeniceConnection, rptAdaptor, "rptTransactionPriceDifference")
      If (rptAdaptor.SelectCommand.Connection Is Nothing) Then
        Return Nothing
        Exit Function
      End If

      rptAdaptor.SelectCommand.Parameters("@FundID").Value = pFundID
      rptAdaptor.SelectCommand.Parameters("@OnlyFinalPrices").Value = 0
      rptAdaptor.SelectCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate
      rptAdaptor.SelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      SyncLock rptAdaptor.SelectCommand.Connection
        rptAdaptor.Fill(rptTable)
      End SyncLock

    Catch ex As Exception
      MainForm.LogError("ReportHandler, GetData_rptTransactionPriceDifference()", LOG_LEVELS.Error, ex.Message, "Error getting rptTransactionPriceDifference data.", ex.StackTrace, True)
      Return Nothing
      Exit Function

    Finally
      ' Close Connection
      Try
        If (Not (rptAdaptor.SelectCommand Is Nothing)) AndAlso (Not (rptAdaptor.SelectCommand.Connection Is Nothing)) Then
          rptAdaptor.SelectCommand.Connection.Close()
        End If
      Catch ex As Exception
      End Try
    End Try

    Return rptTable

  End Function

    ''' <summary>
    ''' Gets the data_rpt unit holder profit.
    ''' </summary>
    ''' <param name="pFundID">The p fund ID.</param>
    ''' <param name="pCounterpartyID">The p counterparty ID.</param>
    ''' <param name="pInvestorGroupID">The p investor group ID.</param>
    ''' <param name="pValueDate">The p value date.</param>
    ''' <returns>DataTable.</returns>
  Public Function GetData_rptUnitHolderProfit( _
  ByVal pFundID As Integer, ByVal pCounterpartyID As Integer, ByVal pInvestorGroupID As Integer, ByVal pValueDate As Date) As DataTable
    ' **************************************************************************************
    '
    '
    ' **************************************************************************************

    ' Configure Adaptor

    Dim rptAdaptor As New SqlDataAdapter
    Dim rptTable As New DataTable

    Try

      MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.GetVeniceConnection, rptAdaptor, "rptUnitHolderProfit")
      If (rptAdaptor.SelectCommand.Connection Is Nothing) Then
        Return Nothing
        Exit Function
      End If

      rptAdaptor.SelectCommand.Parameters("@FundID").Value = pFundID
      rptAdaptor.SelectCommand.Parameters("@CounterpartyID").Value = pCounterpartyID
      rptAdaptor.SelectCommand.Parameters("@InvestorGroup").Value = pInvestorGroupID
      rptAdaptor.SelectCommand.Parameters("@ValueDate").Value = pValueDate
      rptAdaptor.SelectCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate
      rptAdaptor.SelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      SyncLock rptAdaptor.SelectCommand.Connection
        rptAdaptor.Fill(rptTable)
      End SyncLock

    Catch ex As Exception
      MainForm.LogError("ReportHandler, GetData_rptUnitHolderProfit()", LOG_LEVELS.Error, ex.Message, "Error getting rptUnitHolderProfit data.", ex.StackTrace, True)
      Return Nothing
      Exit Function

    Finally
      ' Close Connection
      Try
        If (Not (rptAdaptor.SelectCommand Is Nothing)) AndAlso (Not (rptAdaptor.SelectCommand.Connection Is Nothing)) Then
          rptAdaptor.SelectCommand.Connection.Close()
        End If
      Catch ex As Exception
      End Try
    End Try

    Return rptTable

  End Function

    ''' <summary>
    ''' Gets the data_rpt unit holder positions.
    ''' </summary>
    ''' <param name="pFundID">The p fund ID.</param>
    ''' <param name="pCounterpartyID">The p counterparty ID.</param>
    ''' <param name="pInvestorGroupID">The p investor group ID.</param>
    ''' <param name="pValueDate">The p value date.</param>
    ''' <returns>DataTable.</returns>
  Public Function GetData_rptUnitHolderPositions( _
  ByVal pFundID As Integer, ByVal pCounterpartyID As Integer, ByVal pInvestorGroupID As Integer, ByVal pValueDate As Date) As DataTable
    ' **************************************************************************************
    '
    '
    ' **************************************************************************************

    ' Configure Adaptor

    Dim rptAdaptor As New SqlDataAdapter
    Dim rptTable As New DataTable

    Try

      MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.GetVeniceConnection, rptAdaptor, "rptUnitHolderPositions")
      If (rptAdaptor.SelectCommand.Connection Is Nothing) Then
        Return Nothing
        Exit Function
      End If

      rptAdaptor.SelectCommand.Parameters("@FundID").Value = pFundID
      rptAdaptor.SelectCommand.Parameters("@CounterpartyID").Value = pCounterpartyID
      rptAdaptor.SelectCommand.Parameters("@InvestorGroup").Value = pInvestorGroupID
      rptAdaptor.SelectCommand.Parameters("@ValueDate").Value = pValueDate
      rptAdaptor.SelectCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate
      rptAdaptor.SelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      SyncLock rptAdaptor.SelectCommand.Connection
        rptAdaptor.Fill(rptTable)
      End SyncLock

    Catch ex As Exception
      MainForm.LogError("ReportHandler, GetData_rptUnitHolderPositions()", LOG_LEVELS.Error, ex.Message, "Error getting rptUnitHolderPositions data.", ex.StackTrace, True)
      Return Nothing
      Exit Function

    Finally
      ' Close Connection
      Try
        If (Not (rptAdaptor.SelectCommand Is Nothing)) AndAlso (Not (rptAdaptor.SelectCommand.Connection Is Nothing)) Then
          rptAdaptor.SelectCommand.Connection.Close()
        End If
      Catch ex As Exception
      End Try
    End Try

    Return rptTable

  End Function

    ''' <summary>
    ''' Gets the data_ unit price.
    ''' </summary>
    ''' <param name="pFundID">The p fund ID.</param>
    ''' <param name="pValueDate">The p value date.</param>
    ''' <param name="pStatusGroupFilter">The p status group filter.</param>
    ''' <param name="pAdministratorDatesFilter">The p administrator dates filter.</param>
    ''' <returns>DataTable.</returns>
  Public Function GetData_UnitPrice( _
  ByVal pFundID As Integer, ByVal pValueDate As Date, ByVal pStatusGroupFilter As String, ByVal pAdministratorDatesFilter As Integer) As DataTable
    ' **************************************************************************************
    '
    '
    ' **************************************************************************************

    ' Configure Adaptor

    Dim rptAdaptor As New SqlDataAdapter
    Dim rptTable As New DataTable

    Try

      ' rptUnitPrice
      MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.GetVeniceConnection, rptAdaptor, "rptUnitPrice")
      If (rptAdaptor.SelectCommand.Connection Is Nothing) Then
        Return Nothing
        Exit Function
      End If

      rptAdaptor.SelectCommand.Parameters("@FundID").Value = pFundID
      rptAdaptor.SelectCommand.Parameters("@ValueDate").Value = pValueDate
      rptAdaptor.SelectCommand.Parameters("@StatusGroupFilter").Value = pStatusGroupFilter
      rptAdaptor.SelectCommand.Parameters("@AdministratorDatesFilter").Value = pAdministratorDatesFilter
      rptAdaptor.SelectCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate
      rptAdaptor.SelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      SyncLock rptAdaptor.SelectCommand.Connection
        rptAdaptor.Fill(rptTable)
      End SyncLock

    Catch ex As Exception
      MainForm.LogError("ReportHandler, GetData_UnitPrice()", LOG_LEVELS.Error, ex.Message, "Error getting rptUnitPrice data.", ex.StackTrace, True)
      Return Nothing
      Exit Function

    Finally
      ' Close Connection
      Try
        If (Not (rptAdaptor.SelectCommand Is Nothing)) AndAlso (Not (rptAdaptor.SelectCommand.Connection Is Nothing)) Then
          rptAdaptor.SelectCommand.Connection.Close()
        End If
      Catch ex As Exception
      End Try
    End Try

    Return rptTable

  End Function

    ''' <summary>
    ''' Gets the data_rpt view portfolio.
    ''' </summary>
    ''' <param name="PortfolioList">The portfolio list.</param>
    ''' <param name="FundID">The fund ID.</param>
    ''' <param name="pStatusGroupFilter">The p status group filter.</param>
    ''' <param name="pAdministratorDatesFilter">The p administrator dates filter.</param>
    ''' <returns>DataTable.</returns>
  Public Function GetData_rptViewPortfolio(ByRef PortfolioList As ArrayList, ByVal FundID As Integer, ByVal pStatusGroupFilter As String, ByVal pAdministratorDatesFilter As Integer) As DataTable
    Dim PortfolioID As Integer

    PortfolioID = 0

    Try
      If (PortfolioList.Count = 1) AndAlso (IsNumeric(PortfolioList(0))) Then
        PortfolioID = CInt(PortfolioList(0))
      End If
    Catch ex As Exception
    End Try

    Return GetData_rptViewPortfolio(PortfolioID, FundID, pStatusGroupFilter, pAdministratorDatesFilter)

  End Function

    ''' <summary>
    ''' Gets the data_rpt view portfolio.
    ''' </summary>
    ''' <param name="pPortfolioID">The p portfolio ID.</param>
    ''' <param name="FundID">The fund ID.</param>
    ''' <param name="pStatusGroupFilter">The p status group filter.</param>
    ''' <param name="pAdministratorDatesFilter">The p administrator dates filter.</param>
    ''' <returns>DataTable.</returns>
  Public Function GetData_rptViewPortfolio(ByVal pPortfolioID As Integer, ByVal FundID As Integer, ByVal pStatusGroupFilter As String, ByVal pAdministratorDatesFilter As Integer) As DataTable
    ' **************************************************************************************
    '
    '
    ' **************************************************************************************

    ' Configure Adaptor

    Dim rptAdaptor As New SqlDataAdapter
    Dim rptTable As New DataTable
    Dim thisRow As DataRow
    Dim RowCounter As Integer
    Dim PortfolioCounter As Integer
    Dim thisPortfolioID As Integer

    Dim PortfolioIDOrdinal As Integer
    Dim PriceOrdinal As Integer
    Dim USDValueOrdinal As Integer
    Dim PortfolioWgtOrdinal As Integer
    Dim FundWgtOrdinal As Integer
    Dim TypeOrdinal As Integer
    Dim MarketInstrumentOrdinal As Integer
    Dim PortfolioLinkedInstrumentIDOrdinal As Integer
    Dim HoldingOrdinal As Integer
    Dim WeightOrdinal As Integer

    ' There may (Will) already be Price and Value colums returned by the ViewPortfolio Query.
    ' If they already exist then don't (always) re-create the data.
    ' 
    Dim PriceColumnExists As Boolean
    Dim ValueColumnExists As Boolean

    ' Portfolio
    Dim SumGrossItemValue As Double
    Dim SumNetItemValue As Double
    Dim SumGrossItemWeight As Double
    Dim SumNetItemWeight As Double
    Dim GrossedUpPortfolioValue As Double
    Dim PortfolioTicker As String
    Dim PortfolioFundWeight As Double   ' Weighting of this Portfolio within the  Reference Fund
    Dim AbsReferenceFundValue As Double

    PortfolioFundWeight = 0

    Try
      MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.GetVeniceConnection, rptAdaptor, "spu_ViewPortfolios")
      If (rptAdaptor.SelectCommand.Connection Is Nothing) Then
        Return Nothing
        Exit Function
      End If

      rptAdaptor.SelectCommand.Parameters("@PortfolioID").Value = pPortfolioID
      rptAdaptor.SelectCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate
      rptAdaptor.SelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      SyncLock rptAdaptor.SelectCommand.Connection
        rptAdaptor.Fill(rptTable)
      End SyncLock

      ' Add Additional fields and populate them....

      ' Price, USD Value, PortfolioWeight, Weight vs Balanced USD
      PriceColumnExists = True
      ValueColumnExists = True

      If rptTable.Columns.Contains("PriceValue") = False Then
        rptTable.Columns.Add(New DataColumn("Price", GetType(Double)))
        PriceColumnExists = False
      End If
      If rptTable.Columns.Contains("USDValue") = False Then
        rptTable.Columns.Add(New DataColumn("USDValue", GetType(Double)))
        ValueColumnExists = False
      End If
      If rptTable.Columns.Contains("PortfolioWeight") = False Then
        rptTable.Columns.Add(New DataColumn("PortfolioWeight", GetType(Double)))
      End If
      If rptTable.Columns.Contains("FundWeight") = False Then
        rptTable.Columns.Add(New DataColumn("FundWeight", GetType(Double)))
      End If


      ' Initialise Columns

      PortfolioIDOrdinal = rptTable.Columns.IndexOf("PortfolioID")
      PriceOrdinal = rptTable.Columns.IndexOf("PriceValue")
      USDValueOrdinal = rptTable.Columns.IndexOf("USDValue")
      PortfolioWgtOrdinal = rptTable.Columns.IndexOf("PortfolioWeight")
      FundWgtOrdinal = rptTable.Columns.IndexOf("FundWeight")
      TypeOrdinal = rptTable.Columns.IndexOf("PortfolioItemType")
      MarketInstrumentOrdinal = rptTable.Columns.IndexOf("MarketInstrumentID")
      PortfolioLinkedInstrumentIDOrdinal = rptTable.Columns.IndexOf("PortfolioLinkedInstrumentID")
      HoldingOrdinal = rptTable.Columns.IndexOf("Holding")
      WeightOrdinal = rptTable.Columns.IndexOf("Weight")

      ' Initialise new columns
      For Each thisRow In rptTable.Rows
        Try

          If (PriceColumnExists = False) Then
            thisRow(PriceOrdinal) = 0
          End If

          If (ValueColumnExists = False) Then
            thisRow(USDValueOrdinal) = 0
          End If

          thisRow(PortfolioWgtOrdinal) = 0
          thisRow(FundWgtOrdinal) = 0

        Catch ex As Exception
        End Try
      Next

      '
      '
      ' To Do. In order to make this work for the LargePositions report, 
      ' The Fund holding code must be made to work for multiple portfolios !
      '

      Dim ReturnedPortfolioIDs As New ArrayList
      Dim thisSelectedPortfolio As DataRow()
      Dim thisSelectedValuation As DataRow()
      Dim LastPortfolioID As Integer
      Dim ThisPortfolioFundValue As Double ' Value of the position the Reference Fund has in this portfolio

      ' If there is a reference fund, then calculate weightings etc.....
      If (FundID > 0) Then
        ' Get Fund Valuation.
        Dim ValueAdaptor As New SqlDataAdapter
        Dim tblValuation As New DataTable

        AbsReferenceFundValue = 0

        Try
          MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.GetVeniceConnection, ValueAdaptor, "spu_valuation")

          ValueAdaptor.SelectCommand.Parameters("@FundID").Value = FundID
          ValueAdaptor.SelectCommand.Parameters("@LegalEntity").Value = ""
          ValueAdaptor.SelectCommand.Parameters("@ValueDate").Value = Now.Date
          ValueAdaptor.SelectCommand.Parameters("@UnitPriceVariant").Value = 0
          ValueAdaptor.SelectCommand.Parameters("@StatusGroupFilter").Value = pStatusGroupFilter
          ValueAdaptor.SelectCommand.Parameters("@AdministratorDatesFilter").Value = pAdministratorDatesFilter
          ValueAdaptor.SelectCommand.Parameters("@DisregardWatermarkPoints").Value = 0
          ValueAdaptor.SelectCommand.Parameters("@AggregateToInstrumentParentID").Value = 1
          ValueAdaptor.SelectCommand.Parameters("@ShowDetailedTransactions").Value = 0
          ValueAdaptor.SelectCommand.Parameters("@DetailDate").Value = Now.Date
          ValueAdaptor.SelectCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate
          ValueAdaptor.SelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

          SyncLock ValueAdaptor.SelectCommand.Connection
            ValueAdaptor.Fill(tblValuation)
          End SyncLock

          For Each thisRow In tblValuation.Rows
            Try
              AbsReferenceFundValue += CDbl(thisRow("USDValue"))
            Catch ex As Exception
            End Try
          Next

          AbsReferenceFundValue = Math.Abs(AbsReferenceFundValue)

        Catch ex As Exception
          MainForm.LogError("", LOG_LEVELS.Error, ex.Message, "Failed to get Fund Valuation data. Continuing report without Fund Weightings.", ex.StackTrace, True)
          tblValuation = Nothing
        End Try


        ' Build a list of portfolioIDs

        thisSelectedPortfolio = rptTable.Select("True", "PortfolioID")
        LastPortfolioID = (-1)

        If (Not (thisSelectedPortfolio Is Nothing)) AndAlso (thisSelectedPortfolio.Length > 0) Then
          For RowCounter = 0 To (thisSelectedPortfolio.Length - 1)
            Try
              thisRow = thisSelectedPortfolio(RowCounter)

              If LastPortfolioID <> CInt(thisRow(PortfolioIDOrdinal)) Then
                ReturnedPortfolioIDs.Add(CInt(thisRow(PortfolioIDOrdinal)))
                LastPortfolioID = CInt(thisRow(PortfolioIDOrdinal))
              End If
            Catch ex As Exception
            End Try
          Next
        End If

        ' 
        'Dim PortfolioCounter As Integer
        'Dim thisPortfolioID As Integer
        If (ReturnedPortfolioIDs.Count > 0) Then
          For PortfolioCounter = 0 To (ReturnedPortfolioIDs.Count - 1)
            Try
              ThisPortfolioFundValue = 0
              PortfolioFundWeight = 0

              thisPortfolioID = CInt(ReturnedPortfolioIDs(PortfolioCounter))
              thisSelectedPortfolio = rptTable.Select("PortfolioID='" & thisPortfolioID.ToString & "'", "MarketInstrumentID")

              If (Not (thisSelectedPortfolio Is Nothing)) AndAlso (thisSelectedPortfolio.Length > 0) Then

                ' Get Portfolio Ticker
                PortfolioTicker = thisSelectedPortfolio(0)("PortfolioTicker").ToString

                If (PortfolioTicker.Length > 0) Then

                  ' First Try to resolve the InstrumentID related to this portfolio

                  Dim DStblInstrument As RenaissanceDataClass.DSInstrument
                  Dim SelectedInstruments As RenaissanceDataClass.DSInstrument.tblInstrumentRow() = Nothing
                  Dim InstrumentID As Integer

                  InstrumentID = 0

                  If IsNumeric(thisSelectedPortfolio(0)("PortfolioLinkedInstrumentID")) AndAlso (CInt(thisSelectedPortfolio(0)("PortfolioLinkedInstrumentID")) > 0) Then
                    InstrumentID = CInt(thisSelectedPortfolio(0)("PortfolioLinkedInstrumentID"))
                  End If

                  If (InstrumentID <= 0) Then
										DStblInstrument = MainForm.Load_Table(RenaissanceStandardDatasets.tblInstrument, False)
                    If (Not (DStblInstrument Is Nothing)) Then
                      SelectedInstruments = DStblInstrument.tblInstrument.Select("InstrumentPortfolioTicker='" & PortfolioTicker & "'", "InstrumentParent Desc")
                    End If

                    If (SelectedInstruments IsNot Nothing) AndAlso (SelectedInstruments.Length > 0) Then
                      InstrumentID = CInt(SelectedInstruments(0).InstrumentParent)
                    End If

                    SelectedInstruments = Nothing
                  End If

                  If (InstrumentID <= 0) Then
                    GoTo Continue_NoFundValuation
                  End If

                  ' Now Get the Weight of this Instrument (group) in the Selected Fund, And the Fund Value in USD

                  If (Not (tblValuation Is Nothing)) AndAlso (tblValuation.Rows.Count > 0) Then
                    thisSelectedValuation = tblValuation.Select("Instrument=" & InstrumentID.ToString)

                    If (Not (thisSelectedValuation Is Nothing)) AndAlso (thisSelectedValuation.Length > 0) Then
                      ThisPortfolioFundValue = CDbl(thisSelectedValuation(0)("USDValue"))
                    End If
                  End If
                  ' 

                  If (AbsReferenceFundValue > 0) Then
                    PortfolioFundWeight = ThisPortfolioFundValue / AbsReferenceFundValue
                  End If

                End If ' PortfolioTicker.Length > 0


Continue_NoFundValuation:

                SumGrossItemWeight = 0
                SumNetItemWeight = 0
                SumGrossItemValue = 0
                SumNetItemValue = 0

                ' Calculate Prices, Values and get SUM() information
                For Each thisRow In thisSelectedPortfolio   ' rptTable.Rows
                  Try
                    If (PriceColumnExists = False) OrElse (CDbl(thisRow(PriceOrdinal)) = 0) Then
                      thisRow(PriceOrdinal) = GetPortfolioItemPrice(CType(thisRow(TypeOrdinal), RenaissanceGlobals.PortfolioItemType), CInt(thisRow(MarketInstrumentOrdinal)))
                    End If

                    thisRow(USDValueOrdinal) = CDbl(thisRow(PriceOrdinal)) * CDbl(thisRow(HoldingOrdinal)) * GetPortfolioItemFX(CType(thisRow(TypeOrdinal), RenaissanceGlobals.PortfolioItemType), CInt(thisRow(MarketInstrumentOrdinal)))

                    If CDbl(thisRow(WeightOrdinal)) = 0 Then
                      SumGrossItemValue += Math.Abs(CDbl(thisRow(USDValueOrdinal)))
                      SumNetItemValue += CDbl(thisRow(USDValueOrdinal))
                    Else
                      SumGrossItemWeight += Math.Abs(CDbl(thisRow(WeightOrdinal)))
                      SumNetItemWeight += CDbl(thisRow(WeightOrdinal))
                    End If

                  Catch ex As Exception
                    thisRow(PriceOrdinal) = 0
                    thisRow(USDValueOrdinal) = 0
                  End Try
                Next

                ' Try to extrapolate PortfolioValue
                GrossedUpPortfolioValue = 0
                Try
                  If SumNetItemWeight = 0 Then
                    GrossedUpPortfolioValue = SumNetItemValue
                  ElseIf (1 - SumNetItemWeight) <> 0 Then
                    GrossedUpPortfolioValue = SumNetItemValue / (1 - SumNetItemWeight)
                  End If
                Catch ex As Exception
                End Try

                Dim ItemWeight As Double
                For Each thisRow In thisSelectedPortfolio   ' rptTable.Rows
                  Try
                    If CDbl(thisRow(WeightOrdinal)) = 0 Then
                      ItemWeight = 0
                      If Math.Abs(GrossedUpPortfolioValue) >= 1 Then
                        ItemWeight = CDbl(thisRow(USDValueOrdinal)) / GrossedUpPortfolioValue

                        If (ItemWeight > 1) Then
                          ItemWeight = 1
                        ElseIf (ItemWeight < (-1)) Then
                          ItemWeight = (-1)
                        End If
                      End If

                      thisRow(PortfolioWgtOrdinal) = ItemWeight
                    Else
                      thisRow(PortfolioWgtOrdinal) = thisRow(WeightOrdinal)
                    End If

                    ' Weight as % of Balanced USD

                    ItemWeight = 0
                    thisRow(FundWgtOrdinal) = 0
                    If (AbsReferenceFundValue <> 0) AndAlso (CDbl(thisRow(USDValueOrdinal)) <> 0.0) Then
                      ItemWeight = CDbl(thisRow(USDValueOrdinal)) * PortfolioFundWeight / AbsReferenceFundValue
                      thisRow(FundWgtOrdinal) = ItemWeight
                    ElseIf (CDbl(thisRow(PortfolioWgtOrdinal)) <> 0) Then
                      ItemWeight = CDbl(thisRow(PortfolioWgtOrdinal)) * PortfolioFundWeight
                      thisRow(FundWgtOrdinal) = ItemWeight
                      thisRow(USDValueOrdinal) = ItemWeight * AbsReferenceFundValue
                    End If

                  Catch ex As Exception

                  End Try
                Next

              End If ' SelectePortfolio.Length > 0

            Catch ex As Exception
            End Try
          Next
        End If



        ' Free resource
        Try
          tblValuation = Nothing
          ValueAdaptor = Nothing
        Catch ex As Exception
        End Try

      End If


    Catch ex As Exception
      ' Free resource

      MainForm.LogError("ReportHandler, GetData_rptViewPortfolio()", LOG_LEVELS.Error, ex.Message, "Error getting Portfolio data.", ex.StackTrace, True)
      Return Nothing
      Exit Function

    Finally
      ' Close Connection
      Try
        If (Not (rptAdaptor.SelectCommand Is Nothing)) AndAlso (Not (rptAdaptor.SelectCommand.Connection Is Nothing)) Then
          rptAdaptor.SelectCommand.Connection.Close()
        End If
      Catch ex As Exception
      End Try
    End Try

    Return rptTable

  End Function


    ''' <summary>
    ''' Gets the data_rpt view portfolio change.
    ''' </summary>
    ''' <param name="pStartPortfolioID">The p start portfolio ID.</param>
    ''' <param name="pEndPortfolioID">The p end portfolio ID.</param>
    ''' <returns>DataTable.</returns>
  Public Function GetData_rptViewPortfolioChange(ByVal pStartPortfolioID As Integer, ByVal pEndPortfolioID As Integer) As DataTable
    ' **************************************************************************************
    '
    '
    ' **************************************************************************************

    ' Configure Adaptor

    Dim rptAdaptor As New SqlDataAdapter
    Dim rptTable As New DataTable

    Try

      MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.GetVeniceConnection, rptAdaptor, "spu_ViewPortfolioChange")
      If (rptAdaptor.SelectCommand.Connection Is Nothing) Then
        Return Nothing
        Exit Function
      End If

      rptAdaptor.SelectCommand.Parameters("@StartingPortfolioID").Value = pStartPortfolioID
      rptAdaptor.SelectCommand.Parameters("@EndingPortfolioID").Value = pEndPortfolioID
      rptAdaptor.SelectCommand.Parameters("@StartingKnowledgeDate").Value = MainForm.Main_Knowledgedate
      rptAdaptor.SelectCommand.Parameters("@EndingKnowledgeDate").Value = MainForm.Main_Knowledgedate
      rptAdaptor.SelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      SyncLock rptAdaptor.SelectCommand.Connection
        rptAdaptor.Fill(rptTable)
      End SyncLock

    Catch ex As Exception
      MainForm.LogError("ReportHandler, GetData_rptViewPortfolioChange()", LOG_LEVELS.Error, ex.Message, "Error getting Portfolio data.", ex.StackTrace, True)
      Return Nothing
      Exit Function

    Finally
      ' Close Connection
      Try
        If (Not (rptAdaptor.SelectCommand Is Nothing)) AndAlso (Not (rptAdaptor.SelectCommand.Connection Is Nothing)) Then
          rptAdaptor.SelectCommand.Connection.Close()
        End If
      Catch ex As Exception
      End Try
    End Try


    ' In-Fill some Null details
    Dim FieldValue As Object
    Dim FieldOrdinal As Integer
    Dim Counter As Integer

    ' StartPortfolioDescription
    FieldValue = "<Start Portfolio Description>"
    FieldOrdinal = rptTable.Columns("StartPortfolioDescription").Ordinal
    For Counter = 0 To (rptTable.Rows.Count - 1)
      If (rptTable.Rows(Counter).IsNull(FieldOrdinal) = False) AndAlso (CStr(rptTable.Rows(Counter).Item(FieldOrdinal)).Length > 0) Then
        FieldValue = (rptTable.Rows(Counter).Item(FieldOrdinal))
        Exit For
      End If
    Next
    For Counter = 0 To (rptTable.Rows.Count - 1)
      If rptTable.Rows(Counter).IsNull(FieldOrdinal) OrElse (CStr(rptTable.Rows(Counter).Item(FieldOrdinal)).Length = 0) Then
        rptTable.Rows(Counter).Item(FieldOrdinal) = FieldValue
      End If
    Next

    ' EndPortfolioDescription
    FieldValue = "<End Portfolio Description>"
    FieldOrdinal = rptTable.Columns("EndPortfolioDescription").Ordinal
    For Counter = 0 To (rptTable.Rows.Count - 1)
      If (rptTable.Rows(Counter).IsNull(FieldOrdinal) = False) AndAlso (CStr(rptTable.Rows(Counter).Item(FieldOrdinal)).Length > 0) Then
        FieldValue = (rptTable.Rows(Counter).Item(FieldOrdinal))
        Exit For
      End If
    Next
    For Counter = 0 To (rptTable.Rows.Count - 1)
      If rptTable.Rows(Counter).IsNull(FieldOrdinal) OrElse (CStr(rptTable.Rows(Counter).Item(FieldOrdinal)).Length = 0) Then
        rptTable.Rows(Counter).Item(FieldOrdinal) = FieldValue
      End If
    Next

    ' StartPortfolioTicker
    FieldValue = "<Start Portfolio Ticker>"
    FieldOrdinal = rptTable.Columns("StartPortfolioTicker").Ordinal
    For Counter = 0 To (rptTable.Rows.Count - 1)
      If (rptTable.Rows(Counter).IsNull(FieldOrdinal) = False) AndAlso (CStr(rptTable.Rows(Counter).Item(FieldOrdinal)).Length > 0) Then
        FieldValue = (rptTable.Rows(Counter).Item(FieldOrdinal))
        Exit For
      End If
    Next
    For Counter = 0 To (rptTable.Rows.Count - 1)
      If rptTable.Rows(Counter).IsNull(FieldOrdinal) OrElse (CStr(rptTable.Rows(Counter).Item(FieldOrdinal)).Length = 0) Then
        rptTable.Rows(Counter).Item(FieldOrdinal) = FieldValue
      End If
    Next

    ' EndPortfolioTicker
    FieldValue = "<End Portfolio Ticker>"
    FieldOrdinal = rptTable.Columns("EndPortfolioTicker").Ordinal
    For Counter = 0 To (rptTable.Rows.Count - 1)
      If (rptTable.Rows(Counter).IsNull(FieldOrdinal) = False) AndAlso (CStr(rptTable.Rows(Counter).Item(FieldOrdinal)).Length > 0) Then
        FieldValue = (rptTable.Rows(Counter).Item(FieldOrdinal))
        Exit For
      End If
    Next
    For Counter = 0 To (rptTable.Rows.Count - 1)
      If rptTable.Rows(Counter).IsNull(FieldOrdinal) OrElse (CStr(rptTable.Rows(Counter).Item(FieldOrdinal)).Length = 0) Then
        rptTable.Rows(Counter).Item(FieldOrdinal) = FieldValue
      End If
    Next


    ' StartPortfolioTypeName
    FieldValue = "<Start Portfolio TypeName>"
    FieldOrdinal = rptTable.Columns("StartPortfolioTypeName").Ordinal
    For Counter = 0 To (rptTable.Rows.Count - 1)
      If (rptTable.Rows(Counter).IsNull(FieldOrdinal) = False) AndAlso (CStr(rptTable.Rows(Counter).Item(FieldOrdinal)).Length > 0) Then
        FieldValue = (rptTable.Rows(Counter).Item(FieldOrdinal))
        Exit For
      End If
    Next
    For Counter = 0 To (rptTable.Rows.Count - 1)
      If rptTable.Rows(Counter).IsNull(FieldOrdinal) OrElse (CStr(rptTable.Rows(Counter).Item(FieldOrdinal)).Length = 0) Then
        rptTable.Rows(Counter).Item(FieldOrdinal) = FieldValue
      End If
    Next

    ' EndPortfolioTypeName
    FieldValue = "<End Portfolio TypeName>"
    FieldOrdinal = rptTable.Columns("EndPortfolioTypeName").Ordinal
    For Counter = 0 To (rptTable.Rows.Count - 1)
      If (rptTable.Rows(Counter).IsNull(FieldOrdinal) = False) AndAlso (CStr(rptTable.Rows(Counter).Item(FieldOrdinal)).Length > 0) Then
        FieldValue = (rptTable.Rows(Counter).Item(FieldOrdinal))
        Exit For
      End If
    Next
    For Counter = 0 To (rptTable.Rows.Count - 1)
      If rptTable.Rows(Counter).IsNull(FieldOrdinal) OrElse (CStr(rptTable.Rows(Counter).Item(FieldOrdinal)).Length = 0) Then
        rptTable.Rows(Counter).Item(FieldOrdinal) = FieldValue
      End If
    Next


    ' StartPortfolioFilingDate
    FieldValue = #1/1/1900#
    FieldOrdinal = rptTable.Columns("StartPortfolioFilingDate").Ordinal
    For Counter = 0 To (rptTable.Rows.Count - 1)
      If (rptTable.Rows(Counter).IsNull(FieldOrdinal) = False) AndAlso (CDate(rptTable.Rows(Counter).Item(FieldOrdinal)) > #1/1/1900#) Then
        FieldValue = (rptTable.Rows(Counter).Item(FieldOrdinal))
        Exit For
      End If
    Next
    For Counter = 0 To (rptTable.Rows.Count - 1)
      If rptTable.Rows(Counter).IsNull(FieldOrdinal) OrElse (CDate(rptTable.Rows(Counter).Item(FieldOrdinal)) <= #1/1/1900#) Then
        rptTable.Rows(Counter).Item(FieldOrdinal) = FieldValue
      End If
    Next

    ' EndPortfolioFilingDate
    FieldValue = #1/1/1900#
    FieldOrdinal = rptTable.Columns("EndPortfolioFilingDate").Ordinal
    For Counter = 0 To (rptTable.Rows.Count - 1)
      If (rptTable.Rows(Counter).IsNull(FieldOrdinal) = False) AndAlso (CDate(rptTable.Rows(Counter).Item(FieldOrdinal)) > #1/1/1900#) Then
        FieldValue = (rptTable.Rows(Counter).Item(FieldOrdinal))
        Exit For
      End If
    Next
    For Counter = 0 To (rptTable.Rows.Count - 1)
      If rptTable.Rows(Counter).IsNull(FieldOrdinal) OrElse (CDate(rptTable.Rows(Counter).Item(FieldOrdinal)) <= #1/1/1900#) Then
        rptTable.Rows(Counter).Item(FieldOrdinal) = FieldValue
      End If
    Next


    Return rptTable

  End Function

    ''' <summary>
    ''' Gets the data_rpt risk exposures report.
    ''' </summary>
    ''' <param name="pFundID">The p fund ID.</param>
    ''' <param name="pExposureDetail">The p exposure detail.</param>
    ''' <param name="pExposureDate">The p exposure date.</param>
    ''' <param name="pExposureLimitCategory">The p exposure limit category.</param>
    ''' <param name="pExposureLimitInstrument">The p exposure limit instrument.</param>
    ''' <param name="pExposureLimitMinUsage">The p exposure limit min usage.</param>
    ''' <param name="pExposureFlags">The p exposure flags.</param>
    ''' <param name="pExposureKnowledgeDate">The p exposure knowledge date.</param>
    ''' <param name="pKnowledgeDate">The p knowledge date.</param>
    ''' <returns>DataTable.</returns>
  Public Function GetData_rptRiskExposuresReport( _
   ByVal pFundID As Integer, _
   ByVal pExposureDetail As Integer, _
   ByVal pExposureDate As Date, _
   ByVal pExposureLimitCategory As Integer, _
   ByVal pExposureLimitInstrument As Integer, _
   ByVal pExposureLimitMinUsage As Double, _
   ByVal pExposureFlags As Integer, _
   ByVal pExposureKnowledgeDate As Date, _
   ByVal pKnowledgeDate As Date) As DataTable

    ' **************************************************************************************
    ' Call the GetData_rptRiskExposuresReport() function for a single FundID
    '
    ' **************************************************************************************

    Try

      Return GetData_rptRiskExposuresReport(New Integer() {pFundID}, pExposureDetail, pExposureDate, pExposureLimitCategory, pExposureLimitInstrument, pExposureLimitMinUsage, pExposureFlags, pExposureKnowledgeDate, pKnowledgeDate)

    Catch ex As Exception
    End Try

    Return Nothing

  End Function

    ''' <summary>
    ''' Gets the data_rpt risk exposures report.
    ''' </summary>
    ''' <param name="pFundIDs">The p fund I ds.</param>
    ''' <param name="pExposureDetail">The p exposure detail.</param>
    ''' <param name="pExposureDate">The p exposure date.</param>
    ''' <param name="pExposureLimitCategory">The p exposure limit category.</param>
    ''' <param name="pExposureLimitInstrument">The p exposure limit instrument.</param>
    ''' <param name="pExposureLimitMinUsage">The p exposure limit min usage.</param>
    ''' <param name="pExposureFlags">The p exposure flags.</param>
    ''' <param name="pExposureKnowledgeDate">The p exposure knowledge date.</param>
    ''' <param name="pKnowledgeDate">The p knowledge date.</param>
    ''' <returns>DataTable.</returns>
  Public Function GetData_rptRiskExposuresReport( _
   ByVal pFundIDs() As Integer, _
   ByVal pExposureDetail As Integer, _
   ByVal pExposureDate As Date, _
   ByVal pExposureLimitCategory As Integer, _
   ByVal pExposureLimitInstrument As Integer, _
   ByVal pExposureLimitMinUsage As Double, _
   ByVal pExposureFlags As Integer, _
   ByVal pExposureKnowledgeDate As Date, _
   ByVal pKnowledgeDate As Date) As DataTable

    ' **************************************************************************************
    ' Gather Exposures data for all funds in the pFundIDs() array.
    '
    ' Note that the Adaptor.Fill() method used in GetData_rptRiskExposuresReport() is cumulative.
    '
    ' **************************************************************************************

    ' Configure Adaptor

    Dim rptAdaptor As New SqlDataAdapter
    Dim rptTable As New DataTable
    Dim ThisFundID As Integer

    Try

      ' rptRiskExposuresReport
      MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.GetVeniceConnection, rptAdaptor, "rptRiskExposuresReport")
      If (rptAdaptor.SelectCommand.Connection Is Nothing) Then
        Return Nothing
        Exit Function
      End If

      For Each ThisFundID In pFundIDs
        Call GetData_rptRiskExposuresReport(rptAdaptor, rptTable, ThisFundID, pExposureDetail, pExposureDate, pExposureLimitCategory, pExposureLimitInstrument, pExposureLimitMinUsage, pExposureFlags, pExposureKnowledgeDate, pKnowledgeDate)
      Next

    Catch ex As Exception
    Finally
      ' Close Connection
      Try
        If (Not (rptAdaptor.SelectCommand Is Nothing)) AndAlso (Not (rptAdaptor.SelectCommand.Connection Is Nothing)) Then
          rptAdaptor.SelectCommand.Connection.Close()
        End If
      Catch ex As Exception
      End Try
    End Try

    ' Apply Fixes : 

    If (rptTable IsNot Nothing) AndAlso (rptTable.Rows.Count > 0) Then
      Dim EXP_LimitType As Integer
      Dim EXP_Instrument As Integer
      Dim EXP_Description As Integer
      Dim RowCount As Integer
      Dim ThisRow As DataRow

      EXP_LimitType = rptTable.Columns("expLimitType").Ordinal
      EXP_Instrument = rptTable.Columns("expLimitInstrument").Ordinal
      EXP_Description = rptTable.Columns("InstrumentDescription").Ordinal

      '1) Adjust Instrument Description for Currency Limits to reflect tblCurrency rather than tblInstrument
      For RowCount = 0 To (rptTable.Rows.Count - 1)
        ThisRow = rptTable.Rows(RowCount)

        If (CInt(ThisRow(EXP_LimitType)) = CInt(RenaissanceGlobals.LimitType.CurrencyExposure)) Then
          ThisRow(EXP_Description) = LookupTableValue(MainForm, RenaissanceGlobals.RenaissanceStandardDatasets.tblCurrency, CInt(ThisRow(EXP_Instrument)), "CurrencyDescription")
        End If
      Next
      rptTable.AcceptChanges()
    End If

    Return rptTable

  End Function

    ''' <summary>
    ''' Gets the data_rpt risk exposures report.
    ''' </summary>
    ''' <param name="rptAdaptor">The RPT adaptor.</param>
    ''' <param name="rptTable">The RPT table.</param>
    ''' <param name="pFundID">The p fund ID.</param>
    ''' <param name="pExposureDetail">The p exposure detail.</param>
    ''' <param name="pExposureDate">The p exposure date.</param>
    ''' <param name="pExposureLimitCategory">The p exposure limit category.</param>
    ''' <param name="pExposureLimitInstrument">The p exposure limit instrument.</param>
    ''' <param name="pExposureLimitMinUsage">The p exposure limit min usage.</param>
    ''' <param name="pExposureFlags">The p exposure flags.</param>
    ''' <param name="pExposureKnowledgeDate">The p exposure knowledge date.</param>
    ''' <param name="pKnowledgeDate">The p knowledge date.</param>
    ''' <returns>DataTable.</returns>
  Public Function GetData_rptRiskExposuresReport( _
   ByRef rptAdaptor As SqlDataAdapter, ByVal rptTable As DataTable, _
   ByVal pFundID As Integer, _
   ByVal pExposureDetail As Integer, _
   ByVal pExposureDate As Date, _
   ByVal pExposureLimitCategory As Integer, _
   ByVal pExposureLimitInstrument As Integer, _
   ByVal pExposureLimitMinUsage As Double, _
   ByVal pExposureFlags As Integer, _
   ByVal pExposureKnowledgeDate As Date, _
   ByVal pKnowledgeDate As Date) As DataTable

    ' **************************************************************************************
    '
    '
    ' **************************************************************************************

    Try

      If (pExposureDetail <> 0) Then
        pExposureDetail = 1
      End If

      rptAdaptor.SelectCommand.Parameters("@FundID").Value = pFundID
      rptAdaptor.SelectCommand.Parameters("@IsDetailRow").Value = pExposureDetail
      rptAdaptor.SelectCommand.Parameters("@ExposureDate").Value = pExposureDate
      rptAdaptor.SelectCommand.Parameters("@ExposureLimitCategory").Value = pExposureLimitCategory
      rptAdaptor.SelectCommand.Parameters("@ExposureLimitInstrument").Value = pExposureLimitInstrument
      rptAdaptor.SelectCommand.Parameters("@ExposureLimitMinUsage").Value = pExposureLimitMinUsage
      rptAdaptor.SelectCommand.Parameters("@ExposureFlags").Value = pExposureFlags
      rptAdaptor.SelectCommand.Parameters("@ExposureKnowledgeDate").Value = pExposureKnowledgeDate
      rptAdaptor.SelectCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate
      rptAdaptor.SelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      SyncLock rptAdaptor.SelectCommand.Connection
        rptAdaptor.Fill(rptTable)
      End SyncLock

    Catch ex As Exception
      MainForm.LogError("ReportHandler, GetData_rptRiskExposuresReport()", LOG_LEVELS.Error, ex.Message, "Error getting rptRiskExposuresReport data.", ex.StackTrace, True)
      Return Nothing
      Exit Function

    End Try

    Return rptTable

  End Function



    ''' <summary>
    ''' Gets the data_rpt risk exposures change report.
    ''' </summary>
    ''' <param name="pFundID">The p fund ID.</param>
    ''' <param name="pExposureStartDate">The p exposure start date.</param>
    ''' <param name="pExposureEndDate">The p exposure end date.</param>
    ''' <param name="pExposureLimitCategory">The p exposure limit category.</param>
    ''' <param name="pExposureLimitInstrument">The p exposure limit instrument.</param>
    ''' <param name="pExposureLimitMinUsage">The p exposure limit min usage.</param>
    ''' <param name="pExposureFlagsStart">The p exposure flags start.</param>
    ''' <param name="pExposureFlagsEnd">The p exposure flags end.</param>
    ''' <param name="pExposureStartKnowledgeDate">The p exposure start knowledge date.</param>
    ''' <param name="pExposureEndKnowledgeDate">The p exposure end knowledge date.</param>
    ''' <param name="pKnowledgeDate">The p knowledge date.</param>
    ''' <returns>DataTable.</returns>
  Public Function GetData_rptRiskExposuresChangeReport( _
   ByVal pFundID As Integer, _
   ByVal pExposureStartDate As Date, _
   ByVal pExposureEndDate As Date, _
   ByVal pExposureLimitCategory As Integer, _
   ByVal pExposureLimitInstrument As Integer, _
   ByVal pExposureLimitMinUsage As Double, _
   ByVal pExposureFlagsStart As Integer, _
   ByVal pExposureFlagsEnd As Integer, _
   ByVal pExposureStartKnowledgeDate As Date, _
   ByVal pExposureEndKnowledgeDate As Date, _
   ByVal pKnowledgeDate As Date) As DataTable

    ' **************************************************************************************
    '
    '
    ' **************************************************************************************

    ' Configure Adaptor

    Dim rptAdaptor As New SqlDataAdapter
    Dim rptTable As New DataTable

    Try

      ' rptRiskExposuresChangeReport
      MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.GetVeniceConnection, rptAdaptor, "rptRiskExposuresChangeReport")
      If (rptAdaptor.SelectCommand.Connection Is Nothing) Then
        Return Nothing
        Exit Function
      End If

      rptAdaptor.SelectCommand.Parameters("@FundID").Value = pFundID
      rptAdaptor.SelectCommand.Parameters("@FromExposureDate").Value = pExposureStartDate
      rptAdaptor.SelectCommand.Parameters("@FromExposureFlags").Value = pExposureFlagsStart
      rptAdaptor.SelectCommand.Parameters("@FromExposureKnowledgeDate").Value = pExposureStartKnowledgeDate
      rptAdaptor.SelectCommand.Parameters("@FromSystemKnowledgeDate").Value = pExposureStartKnowledgeDate
      rptAdaptor.SelectCommand.Parameters("@ToExposureDate").Value = pExposureEndDate
      rptAdaptor.SelectCommand.Parameters("@ToExposureFlags").Value = pExposureFlagsEnd
      rptAdaptor.SelectCommand.Parameters("@ToExposureKnowledgeDate").Value = pExposureEndKnowledgeDate
      rptAdaptor.SelectCommand.Parameters("@ToSystemKnowledgeDate").Value = pExposureEndKnowledgeDate
      rptAdaptor.SelectCommand.Parameters("@ExposureLimitCategory").Value = pExposureLimitCategory
      rptAdaptor.SelectCommand.Parameters("@ExposureLimitInstrument").Value = pExposureLimitInstrument
      rptAdaptor.SelectCommand.Parameters("@ExposureLimitMinUsage").Value = pExposureLimitMinUsage
      rptAdaptor.SelectCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate
      rptAdaptor.SelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      SyncLock rptAdaptor.SelectCommand.Connection
        rptAdaptor.Fill(rptTable)
      End SyncLock

    Catch ex As Exception
      MainForm.LogError("ReportHandler, GetData_rptRiskExposuresChangeReport()", LOG_LEVELS.Error, ex.Message, "Error getting rptRiskExposuresChangeReport data.", ex.StackTrace, True)
      Return Nothing
      Exit Function

    Finally
      ' Close Connection
      Try
        If (Not (rptAdaptor.SelectCommand Is Nothing)) AndAlso (Not (rptAdaptor.SelectCommand.Connection Is Nothing)) Then
          rptAdaptor.SelectCommand.Connection.Close()
        End If
      Catch ex As Exception
      End Try
    End Try

    Return rptTable

  End Function



#End Region

#Region " Equalisation Calculations and Watermark Code"

    ''' <summary>
    ''' Gets the equalisation calculations.
    ''' </summary>
    ''' <param name="pFundID">The p fund ID.</param>
    ''' <param name="pCounterpartyID">The p counterparty ID.</param>
    ''' <param name="pInvestorGroup">The p investor group.</param>
    ''' <param name="pValueDate">The p value date.</param>
    ''' <param name="MainKnowledgeDate">The main knowledge date.</param>
    ''' <returns>DataTable.</returns>
  Public Function GetEqualisationCalculations(ByVal pFundID As Integer, ByVal pCounterpartyID As Integer, ByVal pInvestorGroup As Integer, ByVal pValueDate As Date, ByVal MainKnowledgeDate As Date) As DataTable
    ' **************************************************************************************
    ' Function to return a table containing the requested Fund Subscription and Redemption 
    ' transactions with the appropriate Equalisation, Contingent Redemption and 
    ' Incentive Fee calculations.
    '
    ' The Basic details are returned from the SQL Server 'spu_BasicEqualisationCalcs' stored procedure, 
    ' and the involved calculations are then applied to the returned table.
    '
    ' **************************************************************************************
    ' **************************************************************************************
    '
    ' Please note that the Equalisation & Contingent redemption calculations in this Function
    ' should mirror the calculations in the GetSingleMonthCompleteFundCalculations() Function.
    ' Changes to one should be mirrored in the other.
    '
    ' **************************************************************************************

    ' Configure Adaptor, retrieve basic data.

    Dim rptAdaptor As New SqlDataAdapter
    Dim tblEqualisationData As New DataTable
    Dim thisEqualisationRow As DataRow
    Dim nextEqualisationRow As DataRow

    Try

      MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.GetVeniceConnection, rptAdaptor, "rptBasicEqualisation")
      If (rptAdaptor.SelectCommand.Connection Is Nothing) Then
        Return Nothing
        Exit Function
      End If

      rptAdaptor.SelectCommand.Parameters("@FundID").Value = Max(pFundID, 0)
      rptAdaptor.SelectCommand.Parameters("@CounterpartyID").Value = Max(pCounterpartyID, 0)
      rptAdaptor.SelectCommand.Parameters("@InvestorGroup").Value = pInvestorGroup
      rptAdaptor.SelectCommand.Parameters("@ValueDate").Value = pValueDate
      rptAdaptor.SelectCommand.Parameters("@KnowledgeDate").Value = MainKnowledgeDate
      rptAdaptor.SelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      SyncLock rptAdaptor.SelectCommand.Connection
        rptAdaptor.Fill(tblEqualisationData)
      End SyncLock

    Catch ex As Exception
      MainForm.LogError("ReportHandler, GetEqualisationCalculations()", LOG_LEVELS.Error, ex.Message, "Error loading Equalisation Data", ex.StackTrace, True)
      Return Nothing
      Exit Function

    Finally
      ' Close Connection
      Try
        If (Not (rptAdaptor.SelectCommand Is Nothing)) AndAlso (Not (rptAdaptor.SelectCommand.Connection Is Nothing)) Then
          rptAdaptor.SelectCommand.Connection.Close()
        End If
      Catch ex As Exception
      End Try
    End Try

    ' Now Seek to Update fields

    ' Original Transact SQL code :-
    '
    '	AccIncentiveFee             -- Accrued Incentive Fee (Since the last Watermark)
    ' ContingentRedemptionFactor  -- Contingent Redemption due for this position
    ' InitialEqualisationFactor   -- Equalisation amount due to this position at the time of Subscription
    ' CurrentEqualisationFactor   -- Current Equalisation due on this position
    ' SumOfContingentRedemption   -- Total Contingent Redemption Due to all Subscriptions, after Redemptions, for this Cpty / Fund / Instrument
    ' SumOFCurrentEqualisation    -- Total Current Equalisation Due to all Subscriptions, after Redemptions,  for this Cpty / Fund / Instrument
    ' SumOfContingentRedemptionExRedemption   -- Total Contingent Redemption Due to all Subscriptions, ingoring Redemptions, for this Cpty / Fund / Instrument
    ' SumOFCurrentEqualisationExRedemption    -- Total Current Equalisation Due to all Subscriptions, ingoring Redemptions, for this Cpty / Fund / Instrument

    ' UPDATE @EqualisationTransactions
    ' SET InitialEqualisationFactor = SignedUnits * [dbo].[fn_CalcInitialEqualisationFactor](Fund, Watermark, WatermarkDate, Price, ValueDate, @SubsPriceIsGNAV, NAV_Price, 0, PerformanceFeesPercent), 
    '     CurrentEqualisationFactor = SignedUnits * [dbo].[fn_CalcCurrentEqualisationFactor](Fund, Watermark, WatermarkDate, Price, ValueDate, @SubsPriceIsGNAV, NAV_Price, 0, PerformanceFeesPercent)
    ' WHERE 	(TransactionType = @SubscribeTransactionType) AND
    ' 	(PendingTransaction = 0)

    ' UPDATE @EqualisationTransactions
    ' SET AccIncentiveFee = SignedUnits * [dbo].[fn_CalcIncentiveFee](InitialEqualisationFactor, Fund, Watermark, WatermarkDate, Price, ValueDate, @SubsPriceIsGNAV, NAV_Price, 0, PerformanceFeesPercent), 
    '     ContingentRedemptionFactor = SignedUnits * [dbo].[fn_CalcContingentRedemptionFactor](Fund, Watermark, WatermarkDate, Price, ValueDate, @SubsPriceIsGNAV, NAV_Price, 0, PerformanceFeesPercent)
    ' WHERE 	(TransactionType = @SubscribeTransactionType) AND
    ' 	(PendingTransaction = 0)

    ' INSERT @TempEqualisationTotals (TempET_Counterparty, TempET_Fund, TempET_Instrument, 
    ' 	TempET_SumOfContingentRed, TempET_SumOFCurrentEqu, TempET_SumOfContingentRedExRedemption, TempET_SumOFCurrentEquExRedemption)
    ' SELECT Counterparty, Fund, Instrument, 
    ' 	Sum(ContingentRedemptionFactor * RedemptionRatio), Sum(CurrentEqualisationFactor * RedemptionRatio),
    ' 	Sum(ContingentRedemptionFactor), Sum(CurrentEqualisationFactor)
    ' FROM @EqualisationTransactions
    ' GROUP BY Counterparty, Fund, Instrument

    ' UPDATE @EqualisationTransactions
    ' SET SumOfContingentRedemption = (SELECT (b.TempET_SumOfContingentRed) FROM @TempEqualisationTotals b WHERE ((b.TempET_Fund = Fund) AND (b.TempET_Instrument = Instrument) AND (b.TempET_Counterparty = Counterparty))),
    '     SumOFCurrentEqualisation = (SELECT (b.TempET_SumOFCurrentEqu) FROM @TempEqualisationTotals b WHERE ((b.TempET_Fund = Fund) AND (b.TempET_Instrument = Instrument) AND (b.TempET_Counterparty = Counterparty))),  
    '     SumOfContingentRedemptionExRedemption = (SELECT (b.TempET_SumOfContingentRedExRedemption) FROM @TempEqualisationTotals b WHERE ((b.TempET_Fund = Fund) AND (b.TempET_Instrument = Instrument) AND (b.TempET_Counterparty = Counterparty))),
    '     SumOFCurrentEqualisationExRedemption = (SELECT (b.TempET_SumOFCurrentEquExRedemption) FROM @TempEqualisationTotals b WHERE ((b.TempET_Fund = Fund) AND (b.TempET_Instrument = Instrument) AND (b.TempET_Counterparty = Counterparty)))

    ' Collect Milestone Data

    Dim dsWatermark As New DSFundPerformanceFeePoints
    Dim tblWatermarks As DSFundPerformanceFeePoints.tblFundPerformanceFeePointsDataTable
    Dim WatermarkAdaptor As New SqlDataAdapter
    Dim FundWatermarkRows As DSFundPerformanceFeePoints.tblFundPerformanceFeePointsRow() = Nothing
    Dim CurrentFundID As Integer

    MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.GetVeniceConnection, WatermarkAdaptor, RenaissanceStandardDatasets.tblFundPerformanceFeePoints.TableName)
    If (WatermarkAdaptor.SelectCommand.Connection Is Nothing) Then
      Return Nothing
      Exit Function
    End If

    Try
      WatermarkAdaptor.SelectCommand.Parameters("@KnowledgeDate").Value = MainKnowledgeDate
      WatermarkAdaptor.SelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      SyncLock WatermarkAdaptor.SelectCommand.Connection
        WatermarkAdaptor.Fill(dsWatermark.tblFundPerformanceFeePoints)
      End SyncLock

      tblWatermarks = dsWatermark.tblFundPerformanceFeePoints
    Catch ex As Exception
      MainForm.LogError("ReportHandler, GetEqualisationCalculations()", LOG_LEVELS.Error, ex.Message, "Error loading tblFundPerformanceFeePoints", ex.StackTrace, True)
      Return Nothing
      Exit Function

    Finally
      ' Close Connection
      Try
        If (Not (rptAdaptor.SelectCommand Is Nothing)) AndAlso (Not (rptAdaptor.SelectCommand.Connection Is Nothing)) Then
          rptAdaptor.SelectCommand.Connection.Close()
        End If
      Catch ex As Exception
      End Try
    End Try

    CurrentFundID = (-1)

    Dim IsTransfer As Boolean
    Dim SignedUnits As Double
    Dim InitialWatermarkLevel As Double
    Dim InitialWatermarkDate As Date
    Dim LatestWatermarkLevel As Double
    Dim LatestWatermarkDate As Date
    Dim HighestWatermarkLevel As Double
    Dim HighestWatermarkDate As Date
    Dim SubscriptionValueDate As Date
    Dim TransferValueDate As Date
    Dim SubscriptionGNAV As Double
    Dim FundNAV As Double
    Dim GrossedUpFundGNAV As Double
    Dim PerformanceFeesPercent As Double
    ' Dim PerformanceFeesThresholdPercent As Double ' To Do.
    Dim RedemptionRatio As Double

    Dim EffectiveWatermark As Double
    Dim GrossedUpLatestWatermark As Double
    Dim GrossedUpHighestWatermarkAfterSubscription As Double
    Dim InitialEqualisation As Double
    Dim CurrentEqualisation As Double
    Dim LostEqualisation As Double
    Dim ContingentRedemptionFactor As Double
    'Dim InitialContingentRedemptionFactor As Double
    Dim AccruedIncentiveFee As Double

    Dim SumOfContingentRedemption As Double
    Dim SumOFCurrentEqualisation As Double
    Dim SumOfContingentRedemptionExRedemption As Double
    Dim SumOFCurrentEqualisationExRedemption As Double
    Dim LastCounterparty As Integer
    Dim LastFundID As Integer
    Dim LastInstrumentID As Integer

    Dim ForwardCounter As Integer
    Dim BackfillCounter As Integer
    Dim GroupStartCounter As Integer

    Dim SortedEqualisationRows() As DataRow

    Try
      ' The SQL Stored procedure should return these rows already sorted, so this
      ' sort selection should not take long.

      SortedEqualisationRows = tblEqualisationData.Select("True", "Counterparty, Fund, Instrument")
    Catch ex As Exception
      MainForm.LogError("ReportHandler, GetEqualisationCalculations()", LOG_LEVELS.Error, ex.Message, "Error sorting Equalisation transactions for SumOf... calculations.", ex.StackTrace, True)
      Return Nothing
      Exit Function
    End Try

    '
    If (Not (SortedEqualisationRows Is Nothing)) AndAlso (SortedEqualisationRows.GetLength(0) > 0) Then

      ' Initialise Values

      LastCounterparty = (-1)
      LastFundID = (-1)
      LastInstrumentID = (-1)

      SumOfContingentRedemption = 0
      SumOFCurrentEqualisation = 0
      SumOfContingentRedemptionExRedemption = 0
      SumOFCurrentEqualisationExRedemption = 0
      GroupStartCounter = 0

      ' ***************************************************************************
      '
      ' Loop through the sorted Equalisation records calculating first the Equalisation, 
      ' Contingent Redemption and Incentive Fee figures and then the SumOf... figures
      ' grouped by Counterparty, Fund and Instrument.
      '
      ' ***************************************************************************

      For ForwardCounter = 0 To (SortedEqualisationRows.GetLength(0) - 1)

        ' Get This record and the next record.

        thisEqualisationRow = SortedEqualisationRows(ForwardCounter)
        If (ForwardCounter < (SortedEqualisationRows.GetLength(0) - 1)) Then
          nextEqualisationRow = SortedEqualisationRows(ForwardCounter + 1)
        Else
          nextEqualisationRow = SortedEqualisationRows(ForwardCounter)
        End If

        ' ***************************************************************************
        ' Calculate Equalisation , Contingent Redemption and Incentive Fees
        ' ***************************************************************************

        Try
          IsTransfer = CBool(thisEqualisationRow("TransactionIsTransfer"))
          SignedUnits = CDbl(thisEqualisationRow("SignedUnits"))

          If (IsTransfer) Then
            SubscriptionValueDate = CDate(thisEqualisationRow("TransactionEffectiveValueDate"))
            SubscriptionGNAV = CDbl(thisEqualisationRow("TransactionEffectivePrice"))
            TransferValueDate = CDate(thisEqualisationRow("ValueDate"))
          Else
            SubscriptionValueDate = CDate(thisEqualisationRow("ValueDate"))
            SubscriptionGNAV = CDbl(thisEqualisationRow("Price"))
            TransferValueDate = SubscriptionValueDate
          End If

          FundNAV = CDbl(thisEqualisationRow("NAV_Price"))
          PerformanceFeesPercent = CDbl(Nz(thisEqualisationRow("CounterpartyPerformanceFee"), 0))
          ' To DO :- PerformanceFeesThresholdPercent = CDbl(thisEqualisationRow("CounterpartyPerformanceFeeThreshold"))
          'PerformanceFeesPercent = CDbl(thisEqualisationRow("PerformanceFeesPercent"))
          RedemptionRatio = CDbl(thisEqualisationRow("RedemptionRatio"))

          ' If this Transaction is not 'Pending'....
          ' (Subscription Date <= ValueDate)
          If (CInt(thisEqualisationRow("TransactionType")) = TransactionTypes.Subscribe) AndAlso (SubscriptionValueDate.CompareTo(pValueDate) <= 0) Then

            ' Select Watermark Rows
            If (CurrentFundID <> CInt(thisEqualisationRow("Fund"))) Then
              FundWatermarkRows = CType(tblWatermarks.Select("FeeFund=" & CInt(thisEqualisationRow("Fund")), "FeeDate"), DSFundPerformanceFeePoints.tblFundPerformanceFeePointsRow())
              CurrentFundID = CInt(thisEqualisationRow("Fund"))
            End If

            '	AccIncentiveFee float DEFAULT 0, 	-- Accrued Incentive Fee (Since the last Watermark)
            ' ContingentRedemptionFactor float DEFAULT 0,	-- Contingent Redemption due for this position
            ' InitialEqualisationFactor float DEFAULT 0, 	-- Equalisation amount due to this position at the time of Subscription
            ' CurrentEqualisationFactor float DEFAULT 0, 	-- Current Equalisation due on this position

            Call WatermarkAtTime(SubscriptionValueDate, FundWatermarkRows, InitialWatermarkLevel, InitialWatermarkDate)
            Call WatermarkAtTime(pValueDate, FundWatermarkRows, LatestWatermarkLevel, LatestWatermarkDate)
            Call HighestWatermarkAfterSubscriptionDate(SubscriptionValueDate, pValueDate, FundWatermarkRows, HighestWatermarkLevel, HighestWatermarkDate)

            EffectiveWatermark = 0

            If (IsTransfer) AndAlso (CDbl(thisEqualisationRow("TransactionEffectiveWatermark")) > 0) Then
              ' For Transfer Transactions, Allow an explicit Watermark to be specified

              EffectiveWatermark = CDbl(thisEqualisationRow("TransactionEffectiveWatermark"))

              If (InitialWatermarkLevel > EffectiveWatermark) Then
                InitialWatermarkLevel = EffectiveWatermark
              End If

              If (LatestWatermarkDate < TransferValueDate) OrElse (LatestWatermarkLevel < EffectiveWatermark) Then
                LatestWatermarkLevel = EffectiveWatermark
              End If

              If (HighestWatermarkDate < TransferValueDate) OrElse (HighestWatermarkLevel < EffectiveWatermark) Then
                HighestWatermarkLevel = EffectiveWatermark
              End If

            End If

            ' GrossedUpFundGNAV
            GrossedUpFundGNAV = ((FundNAV - InitialWatermarkLevel) / (1.0 - PerformanceFeesPercent)) + InitialWatermarkLevel

            If LatestWatermarkLevel > InitialWatermarkLevel Then
              GrossedUpLatestWatermark = ((LatestWatermarkLevel - InitialWatermarkLevel) / (1.0 - PerformanceFeesPercent)) + InitialWatermarkLevel
            Else
              GrossedUpLatestWatermark = InitialWatermarkLevel
            End If

            If HighestWatermarkLevel >= 0 Then
              GrossedUpHighestWatermarkAfterSubscription = ((HighestWatermarkLevel - InitialWatermarkLevel) / (1.0 - PerformanceFeesPercent)) + InitialWatermarkLevel
            Else
              GrossedUpHighestWatermarkAfterSubscription = (-1)        ' No Subsequent Watermark.
            End If

            If (SubscriptionGNAV <= InitialWatermarkLevel) Or (SubscriptionGNAV <= GrossedUpHighestWatermarkAfterSubscription) Then
              ' No Equalisation is Due
              ' Either Subscription was made at-or-below the Watermark point, or Subsequent watermarks have been
              ' recorded above the Subscription point.
              InitialEqualisation = 0
              CurrentEqualisation = 0
              LostEqualisation = 0
            Else
              ' InitialEqualisation
              ' CurrentEqualisation

              If (IsTransfer) AndAlso CBool(thisEqualisationRow("TransactionSpecificInitialEqualisationFlag")) Then
                InitialEqualisation = CDbl(thisEqualisationRow("TransactionSpecificInitialEqualisationValue"))
              Else
                InitialEqualisation = (SubscriptionGNAV - Max(InitialWatermarkLevel, GrossedUpHighestWatermarkAfterSubscription)) * PerformanceFeesPercent * SignedUnits
              End If

              LostEqualisation = (SubscriptionGNAV - Max(GrossedUpLatestWatermark, GrossedUpFundGNAV)) * PerformanceFeesPercent * SignedUnits

              If (LostEqualisation < 0) Then
                LostEqualisation = 0
                CurrentEqualisation = InitialEqualisation
              ElseIf (LostEqualisation > InitialEqualisation) Then
                LostEqualisation = InitialEqualisation
                CurrentEqualisation = 0
              Else
                CurrentEqualisation = InitialEqualisation - LostEqualisation
              End If
            End If

            ' Contingent Redemption ?
            ContingentRedemptionFactor = 0

            If (GrossedUpHighestWatermarkAfterSubscription >= InitialWatermarkLevel) Then
              ' If there has been a Watermark at-or-above the 
              ' Initial Watermark level, then Contingent redemption is Zero.

              ContingentRedemptionFactor = 0
            Else
              ' Contingent redemption is the performance fee due on the GNAV differential between the
              ' Watermark applicable at time of subscription and the Max of 'Current GNAV' OR 'Highest Watermark since Subscription',
              ' Given that the Current CR can not exceed the Initial CR and can not be negative.

              ContingentRedemptionFactor = (Min(GrossedUpFundGNAV, InitialWatermarkLevel) - Max(SubscriptionGNAV, GrossedUpHighestWatermarkAfterSubscription)) * PerformanceFeesPercent * SignedUnits
              If (ContingentRedemptionFactor < 0) Then
                ContingentRedemptionFactor = 0
              End If
            End If

            'If (GrossedUpFundGNAV >= InitialWatermarkLevel) Or (GrossedUpHighestWatermarkAfterSubscription >= InitialWatermarkLevel) Then
            '  ' If Fund Price > Initial Watermark OR There has been a Watermark at-or-above the 
            '  ' Initial Watermark level, then Contingent redemption is Zero.

            '  ContingentRedemptionFactor = 0
            'Else
            '  ' Contingent redemption is the performance fee due on the GNAV differential between the
            '  ' Watermark applicable at time of subscription and the Max of 'Current GNAV' OR 'Highest Watermark since Sunscription',
            '  ' Given that the Current CR can not exceed the Initial CR and can not be negative.

            '  InitialContingentRedemptionFactor = (InitialWatermarkLevel - SubscriptionGNAV) * PerformanceFeesPercent * SignedUnits
            '  ContingentRedemptionFactor = (InitialWatermarkLevel - Max(GrossedUpFundGNAV, GrossedUpHighestWatermarkAfterSubscription)) * PerformanceFeesPercent * SignedUnits
            '  If (ContingentRedemptionFactor < 0) Then
            '    ContingentRedemptionFactor = 0
            '  ElseIf (ContingentRedemptionFactor > InitialContingentRedemptionFactor) Then
            '    ContingentRedemptionFactor = InitialContingentRedemptionFactor
            '  End If
            'End If

            ' Accrued Incentive Fee

            ' AccruedIncentiveFee = (GrossedUpFundGNAV - Max(Max(InitialWatermarkLevel, GrossedUpHighestWatermarkAfterSubscription), SubscriptionGNAV)) * PerformanceFeesPercent * SignedUnits
            AccruedIncentiveFee = (GrossedUpFundGNAV - Max(GrossedUpHighestWatermarkAfterSubscription, SubscriptionGNAV)) * PerformanceFeesPercent * SignedUnits
            If (AccruedIncentiveFee < 0) Then
              AccruedIncentiveFee = 0
            End If

            thisEqualisationRow("InitialEqualisationFactor") = InitialEqualisation
            thisEqualisationRow("CurrentEqualisationFactor") = CurrentEqualisation
            thisEqualisationRow("ContingentRedemptionFactor") = ContingentRedemptionFactor
            thisEqualisationRow("AccIncentiveFee") = AccruedIncentiveFee

          End If
        Catch ex As Exception
          MainForm.LogError("ReportHandler, GetEqualisationCalculations()", LOG_LEVELS.Error, ex.Message, "Error calculating Equalisation, Contingent Redemption and Incentive Fees.", ex.StackTrace, True)
          Return Nothing
          Exit Function
        End Try

        ' ***************************************************************************
        ' Update SumOf... Fields
        ' ***************************************************************************

        LastCounterparty = CInt(thisEqualisationRow("Counterparty"))
        LastFundID = CInt(thisEqualisationRow("Fund"))
        LastInstrumentID = CInt(thisEqualisationRow("Instrument"))

        SumOfContingentRedemption += (ContingentRedemptionFactor * RedemptionRatio)
        SumOFCurrentEqualisation += (CurrentEqualisation * RedemptionRatio)
        SumOfContingentRedemptionExRedemption += ContingentRedemptionFactor
        SumOFCurrentEqualisationExRedemption += CurrentEqualisation

        ' BackFill Sum Figures ?
        ' If this is the last record or one of the grouping criteria has changed then
        ' Backfill the summary figures.

        Try
          If (CInt(nextEqualisationRow("Counterparty")) <> LastCounterparty) OrElse _
          (CInt(nextEqualisationRow("Fund")) <> LastFundID) OrElse _
          (CInt(nextEqualisationRow("Instrument")) <> LastInstrumentID) OrElse _
          (ForwardCounter = (SortedEqualisationRows.GetLength(0) - 1)) Then

            ' BackFill SumOf... Values

            For BackfillCounter = GroupStartCounter To ForwardCounter
              SortedEqualisationRows(BackfillCounter)("SumOfContingentRedemption") = SumOfContingentRedemption
              SortedEqualisationRows(BackfillCounter)("SumOFCurrentEqualisation") = SumOFCurrentEqualisation
              SortedEqualisationRows(BackfillCounter)("SumOfContingentRedemptionExRedemption") = SumOfContingentRedemptionExRedemption
              SortedEqualisationRows(BackfillCounter)("SumOFCurrentEqualisationExRedemption") = SumOFCurrentEqualisationExRedemption
            Next BackfillCounter

            ' Reset SumOf... Counters

            SumOfContingentRedemption = 0
            SumOFCurrentEqualisation = 0
            SumOfContingentRedemptionExRedemption = 0
            SumOFCurrentEqualisationExRedemption = 0

            GroupStartCounter = (ForwardCounter + 1)        ' The Next row marks the start of the next group
          End If

        Catch ex As Exception
          MainForm.LogError("ReportHandler, GetEqualisationCalculations()", LOG_LEVELS.Error, ex.Message, "Error backfilling summary figures.", ex.StackTrace, True)
          Return Nothing
          Exit Function
        End Try


      Next ForwardCounter
    End If

    Return tblEqualisationData

  End Function


    ''' <summary>
    ''' Watermarks at time.
    ''' </summary>
    ''' <param name="ValueDate">The value date.</param>
    ''' <param name="FundWatermarkRows">The fund watermark rows.</param>
    ''' <param name="WatermarkLevel">The watermark level.</param>
    ''' <param name="WatermarkDate">The watermark date.</param>
    ''' <returns>System.Double.</returns>
  Public Function WatermarkAtTime(ByVal ValueDate As Date, ByVal FundWatermarkRows As DSFundPerformanceFeePoints.tblFundPerformanceFeePointsRow(), ByRef WatermarkLevel As Double, ByRef WatermarkDate As Date) As Double
    ' ***************************************************************************
    ' Function to return the Watermark applicable upto a given Date.
    ' Returns 100 as a default.
    ' NOTE : Assumes array contains data only for the correct Fund AND that the 
    ' array is sorted by FeeDate ascending. 
    ' ***************************************************************************

    Dim Counter As Integer

    WatermarkLevel = VENICE_STARTINGFUNDPRICE
    WatermarkDate = Renaissance_BaseDate

    ' Basic Validation Scenarios

    If (FundWatermarkRows Is Nothing) Then
      Return WatermarkLevel
    End If
    If (FundWatermarkRows.GetLength(0) <= 0) Then
      Return WatermarkLevel
    End If

    ' Work through the Fee Point array in reverse order. (Latest Date First)
    ' Return the First Watermark that occurs before the given ValueDate
    ' If no Date is found before the given ValueDate then fall out of the FOR loop and
    ' return default values.
    ' The loop is done this way decause in the long run I hope it will be quicker as the 
    ' number of Watermarks increases and the average transaction duration remains short.

    For Counter = (FundWatermarkRows.GetLength(0) - 1) To 0 Step -1
      If IsDate(FundWatermarkRows(Counter).FeeDate) Then
        WatermarkLevel = FundWatermarkRows(Counter).FeeFundPrice
        WatermarkDate = FundWatermarkRows(Counter).FeeDate

        If FundWatermarkRows(Counter).FeeDate.CompareTo(ValueDate) <= 0 Then
          Return WatermarkLevel
        End If
      End If
    Next

    WatermarkLevel = VENICE_STARTINGFUNDPRICE
    WatermarkDate = Renaissance_BaseDate
    Return WatermarkLevel

  End Function

    ''' <summary>
    ''' Highests the watermark after subscription date.
    ''' </summary>
    ''' <param name="StartDate">The start date.</param>
    ''' <param name="EndDate">The end date.</param>
    ''' <param name="FundWatermarkRows">The fund watermark rows.</param>
    ''' <param name="WatermarkLevel">The watermark level.</param>
    ''' <param name="WatermarkDate">The watermark date.</param>
    ''' <returns>System.Double.</returns>
  Public Function HighestWatermarkAfterSubscriptionDate(ByVal StartDate As Date, ByVal EndDate As Date, ByVal FundWatermarkRows As DSFundPerformanceFeePoints.tblFundPerformanceFeePointsRow(), ByRef WatermarkLevel As Double, ByRef WatermarkDate As Date) As Double
    ' ***************************************************************************
    ' Function to return the highest Watermark applicable between the SubsriptionDate and a given (Now) Date.
    ' Returns (-1) as a default.
    ' NOTE : Assumes array contains data only for the correct Fund AND that the 
    ' array is sorted by FeeDate ascending. 
    ' Asumes Fund prices can not go below Zero.
    ' ***************************************************************************

    Dim Counter As Integer

    WatermarkLevel = (-1)
    WatermarkDate = Renaissance_BaseDate

    ' Basic Validation Scenarios

    If (FundWatermarkRows Is Nothing) Then
      Return WatermarkLevel
    End If
    If (FundWatermarkRows.GetLength(0) <= 0) Then
      Return WatermarkLevel
    End If

    For Counter = (FundWatermarkRows.GetLength(0) - 1) To 0 Step -1
      If IsDate(FundWatermarkRows(Counter).FeeDate) Then
        If (FundWatermarkRows(Counter).FeeDate.CompareTo(EndDate) <= 0) AndAlso (FundWatermarkRows(Counter).FeeDate.CompareTo(StartDate) >= 0) Then
          If (WatermarkLevel < FundWatermarkRows(Counter).FeeFundPrice) Then
            WatermarkLevel = FundWatermarkRows(Counter).FeeFundPrice
            WatermarkDate = FundWatermarkRows(Counter).FeeDate
          End If
        End If

        If FundWatermarkRows(Counter).FeeDate.CompareTo(StartDate) <= 0 Then
          Return WatermarkLevel
        End If
      End If
    Next

    Return WatermarkLevel

  End Function


#End Region

    ''' <summary>
    ''' Saves the report details.
    ''' </summary>
    ''' <param name="ThisReportName">Name of the this report.</param>
    ''' <param name="KeyText">The key text.</param>
    ''' <param name="ReportDataView">The report data view.</param>
    ''' <param name="KeyValueName">Name of the key value.</param>
  Private Sub SaveReportDetails(ByVal ThisReportName As String, ByVal KeyText As String, ByRef ReportDataView As DataView, ByVal KeyValueName As String)
    ' ************************************************************************
    '
    ' ************************************************************************

    ' Clear any records for the given Ticket number
    ' Save Record for eand TransactionID.

    Dim thisCommand As SqlCommand = Nothing
    ' Dim ThisReportName As String = "rptTransactionTradeBlotter"

    Try
      thisCommand = New SqlCommand("adp_tblReportDetailLog_ClearReportDetails", MainForm.GetVeniceConnection())
      thisCommand.CommandType = CommandType.StoredProcedure

      thisCommand.Parameters.Add("@ReportName", SqlDbType.NVarChar, 30).Value = ThisReportName
      thisCommand.Parameters.Add("@KeyField", SqlDbType.NVarChar, 50).Value = KeyText

      thisCommand.ExecuteNonQuery()

    Catch ex As Exception
    Finally
      Try
        If (thisCommand IsNot Nothing) AndAlso (thisCommand.Connection IsNot Nothing) Then
          thisCommand.Connection.Close()
          thisCommand.Connection = Nothing
        End If
      Catch ex As Exception
      End Try
    End Try

    ' Exit if no records.

    If (ReportDataView Is Nothing) OrElse (ReportDataView.Count <= 0) Then
      Exit Sub
    End If

    ' Add New Rercords.

    Dim ThisDetailTable As New DSReportDetailLog.tblReportDetailLogDataTable
    Dim ThisDetailRow As DSReportDetailLog.tblReportDetailLogRow
    Dim ThisDataRowView As DataRowView
    Dim DataRowCounter As Integer

    Try
      For DataRowCounter = 0 To (ReportDataView.Count - 1)
        ThisDataRowView = ReportDataView.Item(DataRowCounter)

        ThisDetailRow = ThisDetailTable.NewtblReportDetailLogRow
        ThisDetailRow.ReportName = ThisReportName
        ThisDetailRow.KeyField = KeyText
        ThisDetailRow.ValueField = CInt(ThisDataRowView(KeyValueName))

        Try
          ThisDetailTable.Rows.Add(ThisDetailRow)
        Catch ex As Exception
        End Try
      Next
    Catch ex As Exception
    End Try

    Try
      If (ThisDetailTable.Rows.Count > 0) Then
        Dim UpdateAdaptor As New SqlDataAdapter

        MainForm.AdaptorUpdate("ReportHandler, SaveFinalTradeBlotterDetails()", RenaissanceStandardDatasets.tblReportDetailLog, ThisDetailTable.Select())

      End If
    Catch ex As Exception
      MainForm.LogError("ReportHandler, SaveFinalTradeBlotterDetails()", LOG_LEVELS.Error, ex.Message, "Error saving Trade Blotter Details.", ex.StackTrace, True)
    End Try

		'Call(MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblReportDetailLog)))
		Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblReportDetailLog), True)

  End Sub

#Region " Get Price And FX Functions"

    ''' <summary>
    ''' Get_s the FX rate.
    ''' </summary>
    ''' <param name="pCurrencyCode">The p currency code.</param>
    ''' <param name="pFXDate">The p FX date.</param>
    ''' <returns>System.Double.</returns>
  Public Function Get_FXRate(ByVal pCurrencyCode As Integer, ByVal pFXDate As Date) As Double

    ' ********************************************************************
    ' Function to return the FX Rate applicable for a given currency on a given day.
    '
    ' This is a quick-and-easy solution and should be improved in the future. NPP.
    '
    ' ********************************************************************

    Dim dsFX As RenaissanceDataClass.DSFX
    Dim FXtable As RenaissanceDataClass.DSFX.tblFXDataTable
    Dim SelectedRows As RenaissanceDataClass.DSFX.tblFXRow()

    Dim ThisRow As RenaissanceDataClass.DSFX.tblFXRow

    Try
      dsFX = CType(MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblFX, False), RenaissanceDataClass.DSFX)
    Catch ex As Exception
      Return 1
    End Try

    FXtable = dsFX.tblFX
    SelectedRows = CType(FXtable.Select("(FXCurrencyCode=" & pCurrencyCode.ToString & ") AND (FXDate<=#" & pFXDate.ToString(QUERY_SHORTDATEFORMAT) & "#)", "FXDate Desc"), RenaissanceDataClass.DSFX.tblFXRow())

    If (SelectedRows Is Nothing) Then
      Return 1
    End If
    If (SelectedRows.Length <= 0) Then
      Return 1
    End If

    ThisRow = SelectedRows(0)
    Return ThisRow.FXRate

  End Function

    ''' <summary>
    ''' Gets the portfolio item price.
    ''' </summary>
    ''' <param name="ItemType">Type of the item.</param>
    ''' <param name="ItemID">The item ID.</param>
    ''' <returns>System.Double.</returns>
  Private Function GetPortfolioItemPrice(ByVal ItemType As RenaissanceGlobals.PortfolioItemType, ByVal ItemID As Integer) As Double
    Static Dim DStblBestMarketPrice As RenaissanceDataClass.DSBestMarketPrice

    ' DStblBestMarketPrice

    Try

      Select Case ItemType
        Case PortfolioItemType.MarketInstrument

          Try
            ' First Get Market Prices
            If (DStblBestMarketPrice Is Nothing) Then
							DStblBestMarketPrice = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblBestMarketPrice, False), RenaissanceDataClass.DSBestMarketPrice)
            End If

            If (DStblBestMarketPrice Is Nothing) Then
              ' Some kind of error ?
              Return 0
            End If

            ' Select appropriate Data Row(s)

            Dim SelectedRows As RenaissanceDataClass.DSBestMarketPrice.tblBestMarketPriceRow()
            SelectedRows = DStblBestMarketPrice.tblBestMarketPrice.Select("MarketInstrumentID=" & ItemID.ToString)
            If (SelectedRows Is Nothing) OrElse (SelectedRows.Length <= 0) Then
              Return 0
            End If

            ' Return Value.

            Return SelectedRows(0).PriceValue

          Catch ex As Exception
            Return 0
          End Try

        Case PortfolioItemType.VeniceInstrument

          ' Get Best price for the given Instrument.

          Dim PriceQuery As New SqlCommand
          Dim RVal As Object

          Try

            PriceQuery.CommandType = CommandType.Text
            PriceQuery.CommandText = "SELECT dbo.fn_SingleInstrumentBestPrice(" & ItemID.ToString & ", '" & KNOWLEDGEDATE_NOW.ToString(QUERY_SHORTDATEFORMAT) & "', 0, '" & MainForm.Main_Knowledgedate.ToString(QUERY_SHORTDATEFORMAT) & "')"
            PriceQuery.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT
            PriceQuery.Connection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)

            SyncLock PriceQuery.Connection
              RVal = PriceQuery.ExecuteScalar
            End SyncLock

          Catch ex As Exception
            Return 0
          Finally
            PriceQuery.Connection = Nothing
          End Try

          If (RVal Is Nothing) OrElse (Not IsNumeric(RVal)) Then
            Return 0
          End If

          Return CDbl(RVal)

          '' Select appropriate Data Row(s)

          'Dim SelectedRows As RenaissanceDataClass.DSPrice.tblPriceRow()
          'SelectedRows = DStblPrice.tblPrice.Select("PriceInstrument=" & ItemID.ToString, "PriceDate Desc")
          'If (SelectedRows Is Nothing) OrElse (SelectedRows.Length <= 0) Then
          '	Return 0
          'End If

          '' Return Value.

          'Return SelectedRows(0).PriceLevel

        Case PortfolioItemType.Portfolio
          ' Lots to do here LATER.
          ' Return 0 for now

          Return 0

        Case Else

          Return 0

      End Select

    Catch ex As Exception
      ' Fail silently ?
    End Try

    Return 0

  End Function

    ''' <summary>
    ''' Gets the portfolio item FX.
    ''' </summary>
    ''' <param name="ItemType">Type of the item.</param>
    ''' <param name="ItemID">The item ID.</param>
    ''' <returns>System.Double.</returns>
  Private Function GetPortfolioItemFX(ByVal ItemType As RenaissanceGlobals.PortfolioItemType, ByVal ItemID As Integer) As Double
    ' DStblBestFX
    Static Dim DStblBestFX As RenaissanceDataClass.DSBestFX
    Static Dim DStblMarketInstruments As RenaissanceDataClass.DSMarketInstruments
    Static Dim DStblInstrument As RenaissanceDataClass.DSInstrument

    Dim CurrencyID As Integer
    CurrencyID = RenaissanceGlobals.Currency.USD ' Default

    Select Case ItemType
      Case PortfolioItemType.MarketInstrument

        Try
          ' Get FXs
          If (DStblBestFX Is Nothing) Then
						DStblBestFX = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblBestFX, False), RenaissanceDataClass.DSBestFX)
          End If

          If (DStblBestFX Is Nothing) Then
            ' Some kind of error ?
            Return 1
          End If

          ' Get MarketInstrument Currency ID
          If (DStblMarketInstruments Is Nothing) Then
						DStblMarketInstruments = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblMarketInstruments, False), RenaissanceDataClass.DSMarketInstruments)
          End If

          If (DStblMarketInstruments Is Nothing) Then
            ' Some kind of error ?
            Return 1
          End If

          ' Select appropriate Data Row(s)
          Dim InstrumentRows As RenaissanceDataClass.DSMarketInstruments.tblMarketInstrumentsRow()
          InstrumentRows = DStblMarketInstruments.tblMarketInstruments.Select("MarketInstrumentID=" & ItemID.ToString)

          If InstrumentRows.Length > 0 Then
            CurrencyID = InstrumentRows(0).InstrumentCurrencyID
          End If

          If (CurrencyID = RenaissanceGlobals.Currency.USD) Then
            Return 1
          Else
            Dim FXRows As RenaissanceDataClass.DSBestFX.tblBestFXRow()
            FXRows = DStblBestFX.tblBestFX.Select("FXCurrencyCode=" & CurrencyID.ToString)

            If (FXRows Is Nothing) OrElse (FXRows.Length <= 0) Then
              Return 1
            End If

            Return FXRows(0).FXRate
          End If

        Catch ex As Exception
          Return 1
        End Try

      Case PortfolioItemType.VeniceInstrument

        Try
          ' Get Instrument Currency ID
          If (DStblInstrument Is Nothing) Then
						DStblInstrument = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblInstrument, False), RenaissanceDataClass.DSInstrument)
          End If

          If (DStblInstrument Is Nothing) Then
            ' Some kind of error ?
            Return 1
          End If

          ' Select appropriate Data Row(s)
          Dim InstrumentRows As RenaissanceDataClass.DSInstrument.tblInstrumentRow()
          InstrumentRows = CType(DStblInstrument.tblInstrument.Select("InstrumentID= " & ItemID.ToString), RenaissanceDataClass.DSInstrument.tblInstrumentRow())

          If InstrumentRows.Length > 0 Then
            CurrencyID = InstrumentRows(0).InstrumentCurrencyID
          End If

          If (CurrencyID = RenaissanceGlobals.Currency.USD) Then
            Return 1
          Else
            Return Get_FXRate(CurrencyID, Now().Date)
          End If

        Catch ex As Exception
          Return 1
        End Try

      Case PortfolioItemType.Portfolio
        ' Lots to do here LATER.
        ' Return 1 for now

        Return 1

      Case Else

        Return 1

    End Select

    Return 1
  End Function

#End Region

#Region " GetData Delegate Functions"

    ''' <summary>
    ''' Delegate GetDataReader_Delegate
    ''' </summary>
    ''' <param name="RptSQLCommand">The RPT SQL command.</param>
    ''' <returns>SqlDataReader.</returns>
  Friend Delegate Function GetDataReader_Delegate(ByRef RptSQLCommand As SqlCommand) As SqlDataReader

    ''' <summary>
    ''' Gets the data reader.
    ''' </summary>
    ''' <param name="RptSQLCommand">The RPT SQL command.</param>
    ''' <returns>SqlDataReader.</returns>
  Friend Function GetDataReader(ByRef RptSQLCommand As SqlCommand) As SqlDataReader
    Try
      SyncLock RptSQLCommand.Connection
        Return RptSQLCommand.ExecuteReader()
      End SyncLock
    Catch ex As Exception
      Return Nothing
    End Try
  End Function

#End Region

#Region " Form Control Disable Routines"

    ''' <summary>
    ''' Disables the form controls.
    ''' </summary>
    ''' <param name="thisForm">The this form.</param>
    ''' <returns>ArrayList.</returns>
  Friend Function DisableFormControls(ByRef thisForm As Form) As ArrayList
    Dim thisControl As Control
    Dim ReturnArray As New ArrayList

    For Each thisControl In thisForm.Controls
      If (thisControl.Visible = True) AndAlso (thisControl.Enabled = True) Then

        If Not (TypeOf thisControl Is StatusStrip) Then
          ReturnArray.Add(thisControl)
          thisControl.Enabled = False
        End If

      End If
    Next

    Return ReturnArray
  End Function

    ''' <summary>
    ''' Enables the form controls.
    ''' </summary>
    ''' <param name="ControlArray">The control array.</param>
  Friend Sub EnableFormControls(ByVal ControlArray As ArrayList)
    Dim thisControl As Control

    If (ControlArray Is Nothing) Then
      Exit Sub
    End If

    For Each thisControl In ControlArray
      If (thisControl IsNot Nothing) Then
        If (thisControl.IsDisposed = False) Then
          thisControl.Enabled = True
        End If
      End If
    Next
  End Sub

#End Region

End Class
