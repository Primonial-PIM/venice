' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="UtilityFunctions.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports System.Security.Principal
Imports RenaissanceGlobals.Globals
Imports System.Globalization

''' <summary>
''' Class UtilityFunctions
''' </summary>
Module UtilityFunctions

  ''' <summary>
  ''' Gets the name of the user.
  ''' </summary>
  ''' <returns>System.String.</returns>
  Function GetUserName() As String
    Dim RVal As String

    RVal = WindowsIdentity.GetCurrent.Name

    If RVal.Contains("\") Then
      Dim parts() As String = Split(RVal, "\")
      Dim username As String = parts(1)
      Return username
    End If

    Return RVal
  End Function

  ''' <summary>
  ''' Returns the Maximum of two dates.
  ''' </summary>
  ''' <param name="date1">The date1.</param>
  ''' <param name="date2">The date2.</param>
  ''' <returns>System.DateTime.</returns>
  Friend Function MaxDate(ByVal date1 As Date, ByVal date2 As Date) As Date
    Try
      If (date1.CompareTo(date2) < 0) Then
        Return date2
      End If
    Catch ex As Exception
    End Try

    Return date1
  End Function

  ''' <summary>
  ''' Returns the Minimum of two dates.
  ''' </summary>
  ''' <param name="date1">The date1.</param>
  ''' <param name="date2">The date2.</param>
  ''' <returns>System.DateTime.</returns>
  Friend Function MinDate(ByVal date1 As Date, ByVal date2 As Date) As Date
    Try
      If (date1.CompareTo(date2) < 0) Then
        Return date1
      End If
    Catch ex As Exception
    End Try

    Return date2
  End Function

  ''' <summary>
  ''' Takes two parameters, returns the first parameter if it is not Null orDBNull, else returns the second parameter.
  ''' </summary>
  ''' <param name="FirstObject">The first object.</param>
  ''' <param name="SecondObject">The second object.</param>
  ''' <returns>System.Object.</returns>
  Friend Function Nz(ByVal FirstObject As Object, ByVal SecondObject As Object) As Object
    If (FirstObject Is Nothing) OrElse (FirstObject Is DBNull.Value) Then
      Return SecondObject
    Else
      Return FirstObject
    End If
  End Function

  ''' <summary>
  ''' Converts a number to a string with given precision.
  ''' </summary>
  ''' <param name="myValue">My value.</param>
  ''' <param name="myPrecision">My precision.</param>
  ''' <param name="withCommas">if set to <c>true</c> [with commas].</param>
  ''' <param name="thisCultureInfo">The this culture info.</param>
  ''' <returns>System.String.</returns>
  Public Function ToStringWithPrecision(ByVal myValue As Double, ByVal myPrecision As Integer, Optional ByVal withCommas As Boolean = False, Optional ByVal thisCultureInfo As Globalization.CultureInfo = Nothing) As String
    Dim RVal As String = "0"

    Try
      Dim FormatString As String

      If (thisCultureInfo Is Nothing) Then
        thisCultureInfo = New Globalization.CultureInfo("")
      End If

      If (withCommas) Then
        FormatString = "#,##0"
      Else
        FormatString = "###0"
      End If

      If (myPrecision > 0) Then
        FormatString &= "." & New String("0"c, myPrecision)
      End If

      RVal = myValue.ToString(FormatString, thisCultureInfo)

    Catch ex As Exception
    End Try

    Return RVal

  End Function

  ''' <summary>
  ''' Compares the value.
  ''' </summary>
  ''' <param name="val1">The val1.</param>
  ''' <param name="val2">The val2.</param>
  ''' <returns>System.Int32.</returns>
  Public Function CompareValue(ByVal val1 As IComparable, _
  ByVal val2 As IComparable) As Integer

    Try
      If (val1 Is Nothing) OrElse (val2 Is Nothing) Then
        Return 0
      Else
        Return val1.CompareTo(val2)
      End If

    Catch ex As Exception
      If (val1 Is Nothing) OrElse (val2 Is Nothing) Then
        Return 0
      End If
    End Try

    If IsNumeric(val1) AndAlso IsNumeric(val2) Then
      Try
        Return CDbl(val1).CompareTo(CDbl(val2))
      Catch ex As Exception
      End Try
    End If

    If IsDate(val1) AndAlso IsDate(val2) Then
      Try
        Return CDate(val1).CompareTo(CDate(val2))
      Catch ex As Exception
      End Try
    End If

    Return CStr(val1).ToUpper.CompareTo(CStr(val2).ToUpper)

  End Function

  ''' <summary>
  ''' Converts the value.
  ''' </summary>
  ''' <param name="FirstObject">The first object.</param>
  ''' <param name="ReturnType">Type of the return.</param>
  ''' <returns>System.Object.</returns>
  Friend Function ConvertValue(ByVal FirstObject As Object, ByVal ReturnType As System.Type) As Object
    ' *************************************************************************************
    '
    '
    ' *************************************************************************************
    Dim RVal As Double = 0.0#

    ' Return Default Value if FirstObject is Nothing
    Try
      If (FirstObject Is Nothing) Then
        Return Nz(Nothing, ReturnType)
      End If
    Catch ex As Exception
    End Try

    Try
      Dim Multiplier As Double = 1.0#
      Dim TempString As String

      If (ReturnType.Name = "String") Then
        Return CStr(FirstObject)
      ElseIf (ReturnType.Name = "Double") Then
        If IsNumeric(FirstObject) Then
          Return CDbl(FirstObject)
        Else
          TempString = FirstObject.ToString.ToUpper

          While (TempString.Length > 0) AndAlso (IsNumeric(TempString.Substring(TempString.Length - 1)) = False)
            If (TempString.EndsWith("%")) Then
              Multiplier /= 100.0#
            ElseIf (TempString.EndsWith("K")) Then
              Multiplier *= 1000.0#
            ElseIf (TempString.EndsWith("M")) Then
              Multiplier *= 1000000.0#
            ElseIf (TempString.EndsWith("MIL")) Then
              Multiplier *= 1000000.0#
              TempString = TempString.Substring(0, TempString.Length - 2)
            End If

            TempString = TempString.Substring(0, TempString.Length - 1)
          End While

          If IsNumeric(TempString) Then

            Try
              RVal = Double.Parse(TempString, CultureInfo.CurrentCulture)
            Catch ex As Exception
              Try
                RVal = Double.Parse(TempString, CultureInfo.InvariantCulture)
              Catch ex1 As Exception
                RVal = 0.0#
              End Try
            End Try

            Return RVal * Multiplier
          End If

          Return 0
        End If

        'If (FirstObject.ToString.EndsWith("%")) AndAlso (IsNumeric(FirstObject.ToString.Substring(0, FirstObject.ToString.Length - 1))) Then
        '	Return (CDbl(FirstObject.ToString.Substring(0, FirstObject.ToString.Length - 1)) / 100.0#)
        'Else
        '	Return 0
        'End If
      ElseIf (ReturnType.Name.StartsWith("Int")) Then
        If IsNumeric(FirstObject) Then
          Return CInt(FirstObject)
        Else
          TempString = FirstObject.ToString.ToUpper

          While (TempString.Length > 0) AndAlso (IsNumeric(TempString.Substring(TempString.Length - 1)) = False)
            If (TempString.EndsWith("%")) Then
              Multiplier /= 100.0#
            ElseIf (TempString.EndsWith("K")) Then
              Multiplier *= 1000.0#
            ElseIf (TempString.EndsWith("M")) Then
              Multiplier *= 1000000.0#
            ElseIf (TempString.EndsWith("MIL")) Then
              Multiplier *= 1000000.0#
              TempString = TempString.Substring(0, TempString.Length - 2)
            End If

            TempString = TempString.Substring(0, TempString.Length - 1)
          End While

          If IsNumeric(TempString) Then
            Try
              RVal = Integer.Parse(TempString, CultureInfo.CurrentCulture)
            Catch ex As Exception
              Try
                RVal = Integer.Parse(TempString, CultureInfo.InvariantCulture)
              Catch ex1 As Exception
                RVal = 0.0#
              End Try
            End Try

            Return CInt(RVal * Multiplier)
          End If

          Return 0
        End If

        'If IsNumeric(FirstObject) Then
        '	Return CInt(FirstObject)
        'ElseIf (FirstObject.ToString.EndsWith("%")) AndAlso (IsNumeric(FirstObject.ToString.Substring(0, FirstObject.ToString.Length - 1))) Then
        '	Return (CInt(FirstObject.ToString.Substring(0, FirstObject.ToString.Length - 1)) / 100.0#)
        'Else
        '	Return 0
        'End If
      ElseIf (ReturnType.Name = "Date") Then
        If IsDate(FirstObject) Then
          Return CDate(FirstObject)
        Else
          Return 0
        End If
      ElseIf (ReturnType.Name = "Boolean") Then
        Return CBool(FirstObject)
      End If

      If (FirstObject Is Nothing) OrElse (FirstObject Is DBNull.Value) Then
        Return ReturnType.Assembly.CreateInstance(ReturnType.Name)
      Else
        Return FirstObject
      End If
    Catch ex As Exception
      Return Nothing
    End Try
  End Function

  ''' <summary>
  ''' Converts the is numeric.
  ''' </summary>
  ''' <param name="FirstObject">The first object.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Friend Function ConvertIsNumeric(ByVal FirstObject As Object) As Boolean
    ' *************************************************************************************
    ' Test to see if the given value is convertible to a numeric value.
    '
    ' *************************************************************************************

    Try
      If (FirstObject Is Nothing) Then
        Return False
      End If
    Catch ex As Exception
    End Try

    Try
      Dim Multiplier As Double = 1.0#
      Dim TempString As String

      If IsNumeric(FirstObject) Then
        Return True
      Else
        TempString = FirstObject.ToString.ToUpper

        While (TempString.Length > 0) AndAlso (IsNumeric(TempString.Substring(TempString.Length - 1)) = False)
          If (TempString.EndsWith("%")) Then
            Multiplier /= 100.0#
          ElseIf (TempString.EndsWith("K")) Then
            Multiplier *= 1000.0#
          ElseIf (TempString.EndsWith("M")) Then
            Multiplier *= 1000000.0#
          ElseIf (TempString.EndsWith("MIL")) Then
            Multiplier *= 1000000.0#
            TempString = TempString.Substring(0, TempString.Length - 2)
          End If

          TempString = TempString.Substring(0, TempString.Length - 1)
        End While

        If IsNumeric(TempString) Then
          Return True
        End If

        Return False
      End If
    Catch ex As Exception
    End Try

    Return False

  End Function

  ''' <summary>
  ''' Get_s the fund value_ ex expense.
  ''' </summary>
  ''' <param name="MainForm">The main form.</param>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pValuationDate">The p valuation date.</param>
  ''' <param name="pDontUseCachedFundValues">if set to <c>true</c> [p dont use cached fund values].</param>
  ''' <param name="pStatusGroupFilter">The p status group filter.</param>
  ''' <param name="pAdministratorDatesFilter">The p administrator dates filter.</param>
  ''' <returns>System.Double.</returns>
  Public Function Get_FundValue_ExExpense(ByRef MainForm As VeniceMain, ByVal pFundID As Long, ByVal pValuationDate As Date, ByVal pDontUseCachedFundValues As Boolean, ByVal pStatusGroupFilter As String, ByVal pAdministratorDatesFilter As Integer) As Double

    ' Validate
    Try
      If (pFundID <= 0) Then Return 0

      If (pValuationDate <= Renaissance_BaseDate) Then Return 0

    Catch ex As Exception
      MainForm.LogError("Get_FundValue_ExExpense()", RenaissanceGlobals.LOG_LEVELS.Error, ex.Message, "Error getting Fund Value Ex-Expense.", ex.StackTrace, True)
      Return 0
    End Try

    ' Check for values cached in tblSystemStrings

    If (MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And (pDontUseCachedFundValues = False) Then   ' i.e. Live KD ...
      Dim StringName As String
      Dim TempString As String

      ' Check for MS saved Value

      Try
        StringName = "ExExpense_FundValue_MS_" & pFundID.ToString & "_" & pValuationDate.ToString("mmmyyyy")
        TempString = MainForm.Get_SystemString(StringName)
        If IsNumeric(TempString) Then
          Return CDbl(TempString)
          Exit Function
        End If

      Catch ex As Exception
      End Try

      ' Check for Non-MS saved Value (If the Value Date is more than 3 months ago, i.e. always re-calculate 
      ' It date is in the last 3 mopnths.

      Try
        If pValuationDate.CompareTo(Now.Date.AddMonths(-3)) <= 0 Then
          StringName = "ExExpense_FundValue_NonMS_KD0_" & pValuationDate.ToString("mmmyyyy")
          TempString = MainForm.Get_SystemString(StringName)
          If IsNumeric(TempString) Then
            Return CDbl(TempString)
            Exit Function
          End If
        End If

      Catch ex As Exception
      End Try

    End If

    ' OK get calculated fund Value

    Dim ValueCommand As New SqlCommand
    Dim tblFundValuations As New DataTable
    Dim FundValue As Double

    Try
      ValueCommand.Connection = MainForm.GetVeniceConnection

      If (ValueCommand.Connection Is Nothing) Then
        Return 0
      End If

      ValueCommand.CommandType = CommandType.Text
      ValueCommand.CommandText = "SELECT [Value] FROM dbo.fn_rptExpenseFundValuation(@FundID, @ValueDate, @StatusGroupFilter, @AdministratorDatesFilter, @KnowledgeDate)"
      ValueCommand.CommandTimeout = 0

      ValueCommand.Parameters.Clear()

      ValueCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FundID", System.Data.SqlDbType.Int)).Value = pFundID
      ValueCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValueDate", System.Data.SqlDbType.DateTime)).Value = pValuationDate
      ValueCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@StatusGroupFilter", System.Data.SqlDbType.NVarChar)).Value = pStatusGroupFilter
      ValueCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AdministratorDatesFilter", System.Data.SqlDbType.Int)).Value = pAdministratorDatesFilter
      ValueCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime)).Value = MainForm.Main_Knowledgedate

      SyncLock ValueCommand.Connection
        FundValue = ValueCommand.ExecuteScalar
      End SyncLock

      Return FundValue

    Catch ex As Exception

    Finally
      If (Not (ValueCommand.Connection Is Nothing)) Then
        ValueCommand.Connection.Close()
      End If
    End Try

    Return 0
  End Function

  ''' <summary>
  ''' Get_s the fund value.
  ''' </summary>
  ''' <param name="MainForm">The main form.</param>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pValuationDate">The p valuation date.</param>
  ''' <param name="pKnowledgedate">The p knowledgedate.</param>
  ''' <param name="pDontUseCachedFundValues">if set to <c>true</c> [p dont use cached fund values].</param>
  ''' <param name="pStatusGroupFilter">The p status group filter.</param>
  ''' <param name="pAdministratorDatesFilter">The p administrator dates filter.</param>
  ''' <returns>System.Double.</returns>
  Public Function Get_FundValue(ByRef MainForm As VeniceMain, ByVal pFundID As Integer, ByVal pValuationDate As Date, ByVal pKnowledgedate As Date, ByVal pDontUseCachedFundValues As Boolean, ByVal pStatusGroupFilter As String, ByVal pAdministratorDatesFilter As Integer) As Double

    ' Validate
    Try
      If (pFundID <= 0) Then Return 0

      If (pValuationDate <= Renaissance_BaseDate) Then Return 0

    Catch ex As Exception
      MainForm.LogError("Get_FundValue()", RenaissanceGlobals.LOG_LEVELS.Error, ex.Message, "Error getting Fund Value Ex-Expense.", ex.StackTrace, True)
      Return 0
    End Try

    '' Check for values cached in tblSystemStrings

    'If (MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And (pDontUseCachedFundValues = False) Then   ' i.e. Live KD ...
    '  Dim StringName As String
    '  Dim TempString As String

    '  ' Check for MS saved Value

    '  Try
    '    StringName = "FundValue_MS_" & pFundID.ToString & "_" & pValuationDate.ToString("mmmyyyy")
    '    TempString = MainForm.Get_SystemString(StringName)
    '    If IsNumeric(TempString) Then
    '      Return CDbl(TempString)
    '      Exit Function
    '    End If

    '  Catch ex As Exception
    '  End Try


    '  ' Check for Non-MS saved Value (If the Value Date is more than 3 months ago, i.e. always re-calculate 
    '  ' It date is in the last 3 mopnths.

    '  Try
    '    If pValuationDate.CompareTo(Now.Date.AddMonths(-3)) <= 0 Then
    '      StringName = "FundValue_NonMS_KD0_" & pValuationDate.ToString("mmmyyyy")
    '      TempString = MainForm.Get_SystemString(StringName)
    '      If IsNumeric(TempString) Then
    '        Return CDbl(TempString)
    '        Exit Function
    '      End If
    '    End If

    '  Catch ex As Exception
    '  End Try

    'End If

    ' OK get calculated fund Value

    Dim ValueCommand As New SqlCommand
    Dim tblFundValuations As New DataTable
    Dim FundValue As Double

    Try
      ValueCommand.Connection = MainForm.GetVeniceConnection

      If (ValueCommand.Connection Is Nothing) Then
        Return 0
      End If

      ValueCommand.CommandType = CommandType.Text
      ValueCommand.CommandText = "SELECT SUM(Value) FROM dbo.fn_Valuation(@FundID, Null, @ValueDate, Null, @StatusGroupFilter, @AdministratorDatesFilter, Null, Null, Null, Null, @KnowledgeDate)"
      ValueCommand.CommandTimeout = 0

      ValueCommand.Parameters.Clear()

      ValueCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FundID", System.Data.SqlDbType.Int)).Value = pFundID
      ValueCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValueDate", System.Data.SqlDbType.DateTime)).Value = pValuationDate
      ValueCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@StatusGroupFilter", System.Data.SqlDbType.NVarChar)).Value = pStatusGroupFilter
      ValueCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AdministratorDatesFilter", System.Data.SqlDbType.Int)).Value = pAdministratorDatesFilter
      ValueCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime)).Value = pKnowledgedate

      SyncLock ValueCommand.Connection
        FundValue = ValueCommand.ExecuteScalar
      End SyncLock

      Return FundValue

    Catch ex As Exception

    Finally
      If (Not (ValueCommand.Connection Is Nothing)) Then
        ValueCommand.Connection.Close()
      End If
      ValueCommand = Nothing
    End Try

    Return 0
  End Function

  ''' <summary>
  ''' Get_s the USD fund value.
  ''' </summary>
  ''' <param name="MainForm">The main form.</param>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pValuationDate">The p valuation date.</param>
  ''' <param name="pKnowledgedate">The p knowledgedate.</param>
  ''' <param name="pDontUseCachedFundValues">if set to <c>true</c> [p dont use cached fund values].</param>
  ''' <param name="pStatusGroupFilter">The p status group filter.</param>
  ''' <param name="pAdministratorDatesFilter">The p administrator dates filter.</param>
  ''' <returns>System.Double.</returns>
  Public Function Get_USDFundValue(ByRef MainForm As VeniceMain, ByVal pFundID As Long, ByVal pValuationDate As Date, ByVal pKnowledgedate As Date, ByVal pDontUseCachedFundValues As Boolean, ByVal pStatusGroupFilter As String, ByVal pAdministratorDatesFilter As Integer) As Double

    ' Validate
    Try
      If (pFundID <= 0) Then Return 0

      If (pValuationDate <= Renaissance_BaseDate) Then Return 0

    Catch ex As Exception
      MainForm.LogError("Get_USDFundValue()", RenaissanceGlobals.LOG_LEVELS.Error, ex.Message, "Error getting Fund Value Ex-Expense.", ex.StackTrace, True)
      Return 0
    End Try

    ' OK get calculated fund Value

    Dim ValueCommand As New SqlCommand
    Dim tblFundValuations As New DataTable
    Dim FundValue As Double

    Try
      ValueCommand.Connection = MainForm.GetVeniceConnection

      If (ValueCommand.Connection Is Nothing) Then
        Return 0
      End If

      ValueCommand.CommandType = CommandType.Text
      ValueCommand.CommandText = "SELECT SUM(USDValue) FROM dbo.fn_Valuation(@FundID, Null, @ValueDate, Null, @StatusGroupFilter, @AdministratorDatesFilter, Null, Null, Null, Null, @KnowledgeDate)"
      ValueCommand.CommandTimeout = 0

      ValueCommand.Parameters.Clear()

      ValueCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FundID", System.Data.SqlDbType.Int)).Value = pFundID
      ValueCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValueDate", System.Data.SqlDbType.DateTime)).Value = pValuationDate
      ValueCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@StatusGroupFilter", System.Data.SqlDbType.NVarChar)).Value = pStatusGroupFilter
      ValueCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AdministratorDatesFilter", System.Data.SqlDbType.Int)).Value = pAdministratorDatesFilter
      ValueCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime)).Value = pKnowledgedate

      SyncLock ValueCommand.Connection
        FundValue = ValueCommand.ExecuteScalar
      End SyncLock

      Return FundValue

    Catch ex As Exception

    Finally
      If (Not (ValueCommand.Connection Is Nothing)) Then
        ValueCommand.Connection.Close()
      End If
      ValueCommand = Nothing
    End Try

    Return 0
  End Function

  ''' <summary>
  ''' Nexts the dealing date.
  ''' </summary>
  ''' <param name="p_PeriodDays">The p_ period days.</param>
  ''' <param name="p_Notice">The p_ notice.</param>
  ''' <param name="p_BaseDate">The p_ base date.</param>
  ''' <param name="p_CurrentDate">The p_ current date.</param>
  ''' <returns>System.DateTime.</returns>
  Public Function NextDealingDate(ByVal p_PeriodDays As Integer, ByVal p_Notice As Integer, ByVal p_BaseDate As Date, ByVal p_CurrentDate As Date) As Date
    ' Purpose:  To calcuate the next date on which a fund could be redeemed given the period
    '           that it deals (eg weekly, monthly, quarterly), the notice period that must be given
    '           (in days), the base date (an historical dealing date) and the current date.
    ' Accepts:  Period - the dealing period as determined in the table tblDealingPeriod
    '           Notice - the number of days notice required
    '           p_BaseDate - an historic dealing day.  Used as a base to cycle forward
    '           CurrentDate - the current date
    ' Returns:  The next date on which the fund could be redeemed

    Dim nextdate As Date
    Dim Basedate As Date
    Dim CurrentDate As Date

    Dim PeriodDays As Long
    Dim Notice As Integer

    If (p_BaseDate < #1/1/2000#) Then Basedate = #12/31/2000# Else Basedate = p_BaseDate

    If (p_PeriodDays > 0) Then
      PeriodDays = p_PeriodDays
    Else
      PeriodDays = 30
    End If

    If p_Notice >= 0 Then
      Notice = p_Notice
    Else
      Notice = 30
    End If

    If p_CurrentDate > #1/1/2000# Then
      CurrentDate = p_CurrentDate
    Else
      CurrentDate = Now().Date
    End If

    'set the base for cycling foward to future dealing dates
    nextdate = Basedate

    'Loop forward through dealing date until next one for which notice can be given is found
    Do
      Select Case PeriodDays
        Case 0, 1
          nextdate = nextdate.AddDays(1)
        Case 7
          nextdate = nextdate.AddDays(7)
        Case 14
          nextdate = nextdate.AddDays(14)
        Case 30, 31
          nextdate = nextdate.AddMonths(1)
        Case 90, 91, 92
          nextdate = nextdate.AddMonths(3)
        Case 182, 183
          nextdate = nextdate.AddMonths(6)
        Case 365, 366
          nextdate = nextdate.AddYears(1)
        Case Else
          nextdate = nextdate.AddMonths(2)
      End Select
    Loop While nextdate < (CurrentDate.AddDays(Notice))

    NextDealingDate = nextdate

  End Function

End Module
