' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 02-28-2013
' ***********************************************************************
' <copyright file="TableLookupFunctions.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
''' <summary>
''' Class TableLookupFunctions
''' </summary>
Module TableLookupFunctions

    ''' <summary>
    ''' Lookups the table row.
    ''' </summary>
    ''' <param name="Mainform">The mainform.</param>
    ''' <param name="StdTableDef">The STD table def.</param>
    ''' <param name="ThisAuditID">The this audit ID.</param>
    ''' <param name="ReturnField">The return field.</param>
    ''' <param name="SortString">The sort string.</param>
    ''' <param name="SearchString">The search string.</param>
    ''' <returns>DataRow.</returns>
	Friend Function LookupTableRow(ByRef Mainform As RenaissanceGlobals.StandardRenaissanceMainForm, ByVal StdTableDef As RenaissanceGlobals.StandardDataset, ByVal ThisAuditID As Integer, Optional ByVal ReturnField As String = "", Optional ByVal SortString As String = "RN", Optional ByVal SearchString As String = "") As DataRow
		' *************************************************************
		' Return the DataRow for the given ID from the Given table.
		' If a value is given for 'ReturnField' then this function will
		' return 'Nothing' if that Column does not exist.
		' *************************************************************

		Dim ThisDS As DataSet
		Dim ThisTable As DataTable
		Dim SelectedRows() As DataRow

		Try
			ThisDS = Mainform.Load_Table(StdTableDef)
			If (ThisDS Is Nothing) OrElse (ThisDS.Tables.Count < 1) Then
				Return Nothing
			End If

			ThisTable = ThisDS.Tables(0)
			If (ReturnField.Length > 0) AndAlso (ThisTable.Columns.Contains(ReturnField) = False) Then
				Return Nothing
			End If

			If (ThisAuditID > 0) Then
				SyncLock ThisTable
					SelectedRows = ThisTable.Select("AuditID=" & ThisAuditID.ToString, SortString)
				End SyncLock

				If (SelectedRows.Length > 0) Then
					Return SelectedRows(0)
				Else
					Return Nothing
				End If
			ElseIf (SearchString.Length > 0) Then
				SyncLock ThisTable
					SelectedRows = ThisTable.Select(SearchString, SortString)
				End SyncLock

				If (SelectedRows.Length > 0) Then
					Return SelectedRows(0)
				Else
					Return Nothing
				End If
			Else
				Return Nothing
			End If

		Catch ex As Exception
			Return Nothing
		End Try

	End Function

    ''' <summary>
    ''' Lookups the table row.
    ''' </summary>
    ''' <param name="ThisTable">The this table.</param>
    ''' <param name="SearchField">The search field.</param>
    ''' <param name="SearchValue">The search value.</param>
    ''' <param name="ReturnField">The return field.</param>
    ''' <param name="SortString">The sort string.</param>
    ''' <param name="SearchString">The search string.</param>
    ''' <returns>DataRow.</returns>
	Friend Function LookupTableRow(ByVal ThisTable As DataTable, ByVal SearchField As String, ByVal SearchValue As Integer, Optional ByVal ReturnField As String = "", Optional ByVal SortString As String = "RN", Optional ByVal SearchString As String = "") As DataRow
		' *************************************************************
		' Return the DataRow for the given ID from the Given table.
		' If a value is given for 'ReturnField' then this function will
		' return 'Nothing' if that Column does not exist.
		' *************************************************************

		Dim SelectedRows() As DataRow

		Try

			If (ReturnField.Length > 0) AndAlso (ThisTable.Columns.Contains(ReturnField) = False) Then
				Return Nothing
			End If


			If (SearchString.Length > 0) Then
				SyncLock ThisTable
					SelectedRows = ThisTable.Select(SearchString, SortString)
				End SyncLock

				If (SelectedRows.Length > 0) Then
					Return SelectedRows(0)
				Else
					Return Nothing
				End If
			ElseIf (SearchValue > 0) Then
				SyncLock ThisTable
					SelectedRows = ThisTable.Select(SearchField & "=" & SearchValue.ToString, SortString)
				End SyncLock

				If (SelectedRows.Length > 0) Then
					Return SelectedRows(0)
				Else
					Return Nothing
				End If
			End If

		Catch ex As Exception
			Return Nothing
		End Try

		Return Nothing

	End Function

    ''' <summary>
    ''' Lookups the table row.
    ''' </summary>
    ''' <param name="ThisTable">The this table.</param>
    ''' <param name="SearchField">The search field.</param>
    ''' <param name="SearchValue">The search value.</param>
    ''' <param name="ReturnField">The return field.</param>
    ''' <param name="SortString">The sort string.</param>
    ''' <param name="SearchString">The search string.</param>
    ''' <returns>DataRow.</returns>
	Friend Function LookupTableRow(ByVal ThisTable As DataTable, ByVal SearchField As String, ByVal SearchValue As String, Optional ByVal ReturnField As String = "", Optional ByVal SortString As String = "RN", Optional ByVal SearchString As String = "") As DataRow
		' *************************************************************
		' Return the DataRow for the given ID from the Given table.
		' If a value is given for 'ReturnField' then this function will
		' return 'Nothing' if that Column does not exist.
		' *************************************************************

		Dim SelectedRows() As DataRow

		Try

			If (ReturnField.Length > 0) AndAlso (ThisTable.Columns.Contains(ReturnField) = False) Then
				Return Nothing
			End If


			If (SearchString.Length > 0) Then
				SyncLock ThisTable
					SelectedRows = ThisTable.Select(SearchString, SortString)
				End SyncLock

				If (SelectedRows.Length > 0) Then
					Return SelectedRows(0)
				Else
					Return Nothing
				End If
			ElseIf (SearchValue.Length > 0) Then
				SyncLock ThisTable
					SelectedRows = ThisTable.Select(SearchField & "='" & SearchValue.ToString & "'", SortString)
				End SyncLock

				If (SelectedRows.Length > 0) Then
					Return SelectedRows(0)
				Else
					Return Nothing
				End If
			End If

		Catch ex As Exception
			Return Nothing
		End Try

		Return Nothing

	End Function

    ''' <summary>
    ''' Lookups the table value.
    ''' </summary>
    ''' <param name="Mainform">The mainform.</param>
    ''' <param name="StdTableDef">The STD table def.</param>
    ''' <param name="ThisAuditID">The this audit ID.</param>
    ''' <param name="ReturnField">The return field.</param>
    ''' <param name="SortString">The sort string.</param>
    ''' <param name="SearchString">The search string.</param>
    ''' <returns>System.Object.</returns>
	Friend Function LookupTableValue(ByRef Mainform As RenaissanceGlobals.StandardRenaissanceMainForm, ByVal StdTableDef As RenaissanceGlobals.StandardDataset, ByVal ThisAuditID As Integer, ByVal ReturnField As String, Optional ByVal SortString As String = "RN", Optional ByVal SearchString As String = "") As Object
		' *************************************************************
		' Return the given Data Value for the given ID from the Given table.
		' If a value is given for 'ReturnField' then this function will
		' return 'Nothing' if that Column does not exist.
		' *************************************************************
		Dim SelectedRow As DataRow

		Try
			SelectedRow = LookupTableRow(Mainform, StdTableDef, ThisAuditID, ReturnField, SortString, SearchString)

			If (SelectedRow IsNot Nothing) Then
				If (SelectedRow.Table.Columns.Contains(ReturnField)) Then
					Return SelectedRow(ReturnField)
				End If
			End If

		Catch ex As Exception
			Return Nothing
		End Try

		Return Nothing

	End Function

	'Friend Function LookupTableValue(ByRef Mainform As VeniceMain, ByVal StdTableDef As RenaissanceGlobals.StandardDataset, ByVal ThisAuditID As Integer, ByVal ReturnField As String, Optional ByVal SortString As String = "RN", Optional ByVal SearchString As String = "") As Object
	'	Dim ThisDS As DataSet
	'	Dim ThisTable As DataTable
	'	Dim SelectedRows() As DataRow

	'	Try
	'		ThisDS = Mainform.Load_Table(StdTableDef)
	'		If (ThisDS Is Nothing) OrElse (ThisDS.Tables.Count < 1) Then
	'			Return Nothing
	'		End If

	'		ThisTable = ThisDS.Tables(0)
	'		If (ThisTable.Columns.Contains(ReturnField) = False) Then
	'			Return Nothing
	'		End If

	'		If (ThisAuditID > 0) Then
	'			SelectedRows = ThisTable.Select("AuditID=" & ThisAuditID.ToString, SortString)
	'			If (SelectedRows.Length > 0) Then
	'				Return SelectedRows(0)(ReturnField)
	'			Else
	'				Return Nothing
	'			End If
	'		ElseIf (SearchString.Length > 0) Then
	'			SelectedRows = ThisTable.Select(SearchString, SortString)
	'			If (SelectedRows.Length > 0) Then
	'				Return SelectedRows(0)(ReturnField)
	'			Else
	'				Return Nothing
	'			End If
	'		Else
	'			Return Nothing
	'		End If

	'	Catch ex As Exception
	'		Return Nothing
	'	End Try

	'End Function

End Module
