﻿' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 08-15-2013
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="BackgroundTableLoader.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Option Strict On

Imports RenaissanceGlobals.Globals
Imports RenaissanceGlobals
Imports System.Data.SqlClient


''' <summary>
''' Class BackgroundTableLoaderClass
''' </summary>
Friend Class BackgroundTableLoaderClass
	' *****************************************************************************************************
	' BackgroundTableLoaderClass 
	' ---------------------------
	'
	' 
	'
	' *****************************************************************************************************

	Inherits NaplesGlobals.ThreadWrapperBase

    ''' <summary>
    ''' The _ close down flag
    ''' </summary>
	Private _CloseDownFlag As Boolean
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
	Private _MainForm As VeniceMain
    ''' <summary>
    ''' The _ update queue
    ''' </summary>
	Private _UpdateQueue As Queue(Of BackgroupWorkItemClass)
    ''' <summary>
    ''' The update queue lock
    ''' </summary>
	Private UpdateQueueLock As New Object
    ''' <summary>
    ''' The _ background data handler
    ''' </summary>
	Private _BackgroundDataHandler As RenaissanceDataClass.DataHandler ' Used to instantiate Datasets of the correct type.

    ''' <summary>
    ''' The PARTIA l_ UPDAT e_ TABL e_ ITE m_ LIMIT
    ''' </summary>
	Private Const PARTIAL_UPDATE_TABLE_ITEM_LIMIT As Integer = 30

    ''' <summary>
    ''' Occurs when [table loaded].
    ''' </summary>
	Public Event TableLoaded(ByVal sender As Object, ByVal ChangeID As RenaissanceChangeID, ByRef myDataset As DataSet)

    ''' <summary>
    ''' Enum BackgroundTaskType
    ''' </summary>
	Public Enum BackgroundTaskType As Integer
        ''' <summary>
        ''' The none
        ''' </summary>
		None = 0
        ''' <summary>
        ''' The load table
        ''' </summary>
		LoadTable = 1
        ''' <summary>
        ''' The refresh table
        ''' </summary>
		RefreshTable = 2

        ''' <summary>
        ''' The send update
        ''' </summary>
		SendUpdate = 16
	End Enum

    ''' <summary>
    ''' Class BackgroupWorkItemClass
    ''' </summary>
	Public Class BackgroupWorkItemClass

        ''' <summary>
        ''' The _ task type
        ''' </summary>
		Private _TaskType As BackgroundTaskType
        ''' <summary>
        ''' The _ update event args
        ''' </summary>
		Private _UpdateEventArgs As RenaissanceUpdateEventArgs
        ''' <summary>
        ''' The _ change ID
        ''' </summary>
		Private _ChangeID As RenaissanceChangeID
        ''' <summary>
        ''' The _ force refresh
        ''' </summary>
		Private _ForceRefresh As Boolean

        ''' <summary>
        ''' Prevents a default instance of the <see cref="BackgroupWorkItemClass"/> class from being created.
        ''' </summary>
		Private Sub New()
			_TaskType = 0
			_UpdateEventArgs = Nothing
			_ChangeID = Nothing
			_ForceRefresh = False
		End Sub

		'Public Sub New(ByVal TaskType As BackgroundTaskType, ByVal ChangeID As RenaissanceChangeID, ByVal ForceRefresh As Boolean)
		'	Me.New()
		'	_TaskType = TaskType
		'	_ChangeID = ChangeID
		'	_ForceRefresh = ForceRefresh
		'End Sub

        ''' <summary>
        ''' Initializes a new instance of the <see cref="BackgroupWorkItemClass"/> class.
        ''' </summary>
        ''' <param name="TaskType">Type of the task.</param>
        ''' <param name="UpdateEventArgs">The <see cref="RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
		Public Sub New(ByVal TaskType As BackgroundTaskType, ByVal UpdateEventArgs As RenaissanceUpdateEventArgs)
			Me.New()
			_TaskType = TaskType
			_UpdateEventArgs = UpdateEventArgs
		End Sub

        ''' <summary>
        ''' Initializes a new instance of the <see cref="BackgroupWorkItemClass"/> class.
        ''' </summary>
        ''' <param name="TaskType">Type of the task.</param>
        ''' <param name="ChangeID">The change ID.</param>
        ''' <param name="ForceRefresh">if set to <c>true</c> [force refresh].</param>
        ''' <param name="UpdateEventArgs">The <see cref="RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
		Public Sub New(ByVal TaskType As BackgroundTaskType, ByVal ChangeID As RenaissanceChangeID, ByVal ForceRefresh As Boolean, ByVal UpdateEventArgs As RenaissanceUpdateEventArgs)
			Me.New()
			_TaskType = TaskType
			_ChangeID = ChangeID
			_ForceRefresh = ForceRefresh
			_UpdateEventArgs = UpdateEventArgs
		End Sub

        ''' <summary>
        ''' Gets the type of the task.
        ''' </summary>
        ''' <value>The type of the task.</value>
		Public ReadOnly Property TaskType() As BackgroundTaskType
			Get
				Return _TaskType
			End Get
		End Property

        ''' <summary>
        ''' Gets the change ID.
        ''' </summary>
        ''' <value>The change ID.</value>
		Public ReadOnly Property ChangeID() As RenaissanceChangeID
			Get
				Return _ChangeID
			End Get
		End Property

        ''' <summary>
        ''' Gets the update event args.
        ''' </summary>
        ''' <value>The update event args.</value>
		Public ReadOnly Property UpdateEventArgs() As RenaissanceUpdateEventArgs
			Get
				Return _UpdateEventArgs
			End Get
		End Property

        ''' <summary>
        ''' Gets a value indicating whether [force refresh].
        ''' </summary>
        ''' <value><c>true</c> if [force refresh]; otherwise, <c>false</c>.</value>
		Public ReadOnly Property ForceRefresh() As Boolean
			Get
				Return _ForceRefresh
			End Get
		End Property
	End Class

    ''' <summary>
    ''' Prevents a default instance of the <see cref="BackgroundTableLoaderClass"/> class from being created.
    ''' </summary>
	Private Sub New()
	End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="BackgroundTableLoaderClass"/> class.
    ''' </summary>
    ''' <param name="MainForm">The main form.</param>
	Public Sub New(ByVal MainForm As VeniceMain)
		MyBase.New()
		Me.Thread.Name = "BackgroundTableLoader"

		Try
			_Mainform = MainForm

			_UpdateQueue = New Queue(Of BackgroupWorkItemClass)

			_BackgroundDataHandler = New RenaissanceDataClass.DataHandler

		Catch ex As Exception

		End Try

	End Sub

    ''' <summary>
    ''' Gets or sets a value indicating whether [close down].
    ''' </summary>
    ''' <value><c>true</c> if [close down]; otherwise, <c>false</c>.</value>
	Public Property CloseDown() As Boolean
		Get
			Return _CloseDownFlag
		End Get
		Set(ByVal Value As Boolean)
			_CloseDownFlag = Value
		End Set
	End Property

    ''' <summary>
    ''' Does the task.
    ''' </summary>
	Protected Overrides Sub DoTask()

		Initialise()

		RunServer()

		TidyUp()

	End Sub

    ''' <summary>
    ''' Initialises this instance.
    ''' </summary>
	Private Sub Initialise()
		' Initialise Data Server

		_CloseDownFlag = False


	End Sub

    ''' <summary>
    ''' Tidies up.
    ''' </summary>
	Private Sub TidyUp()

		' On ending the Server

		Try
			If (_UpdateQueue.Count > 0) Then
				_UpdateQueue.Clear()
			End If
		Catch ex As Exception
		End Try

	End Sub

    ''' <summary>
    ''' Runs the server.
    ''' </summary>
	Private Sub RunServer()

		' ******************************************************
		' Work loop.
		'
		' ******************************************************
		Dim ThisWorkItem As BackgroupWorkItemClass
		Dim QueueCount As Integer

		Try

			While (Not _CloseDownFlag)

				SyncLock UpdateQueueLock
					QueueCount = _UpdateQueue.Count
				End SyncLock

				If (QueueCount > 0) Then

					SyncLock UpdateQueueLock
						If (_UpdateQueue.Count > 0) Then
							ThisWorkItem = _UpdateQueue.Dequeue
						Else
							ThisWorkItem = Nothing
						End If
					End SyncLock

          If (ThisWorkItem IsNot Nothing) AndAlso (Not _CloseDownFlag) Then

            Select Case ThisWorkItem.TaskType

              Case BackgroundTaskType.LoadTable

								Load_Table(ThisWorkItem.ChangeID, ThisWorkItem.UpdateEventArgs, ThisWorkItem.ForceRefresh)

                If (_CloseDownFlag) Then
                  Exit Sub
                End If

                ' Post Table Refresh Actions

                Select Case ThisWorkItem.ChangeID

                  Case RenaissanceChangeID.tblLocations

                    Call _MainForm.RefreshLocationsDictionary()

                  Case RenaissanceChangeID.Mastername

                    Call _MainForm.RefreshMasternameDictionary()

                  Case RenaissanceChangeID.Information

                    Try
                      ' The PertracData object caches Instrument Information Data and is shared between forms, thus
                      ' it makes sense to clear the cache at this central point.

                      SyncLock _MainForm.PertracData
                        _MainForm.PertracData.ClearInformationCache()
                      End SyncLock
                    Catch ex As Exception
                    End Try

                  Case RenaissanceChangeID.Performance

                    Try
                      ' The StatFunctions object caches Instrument Statistics Data and is shared between forms, thus
                      ' it makes sense to clear the cache at this central point.

                      SyncLock _MainForm.StatFunctions
                        _MainForm.StatFunctions.ClearCache()
                      End SyncLock

                    Catch ex As Exception
                    End Try

                    Try
                      ' The PertracData object caches Instrument Returns Data and is shared between forms, thus
                      ' it makes sense to clear the cache at this central point.

                      SyncLock _MainForm.PertracData
                        _MainForm.PertracData.ClearDataCache()
                      End SyncLock
                    Catch ex As Exception
                    End Try

                End Select


              Case BackgroundTaskType.RefreshTable

								ReloadTable(ThisWorkItem.ChangeID, ThisWorkItem.UpdateEventArgs)

              Case BackgroundTaskType.SendUpdate

                _MainForm.TriggerEvent(ThisWorkItem.UpdateEventArgs)

            End Select

          End If

				Else
					Threading.Thread.Sleep(100)
				End If

        ' Exit loop if Venice has exited. (Crashed for example)

        If (_MainForm.IsDisposed) Then
          _CloseDownFlag = True
        End If

			End While

		Catch ex As Exception
		End Try

	End Sub

    ''' <summary>
    ''' Reloads the table.
    ''' </summary>
    ''' <param name="pTableID">The p table ID.</param>
    ''' <param name="UpdateEvent">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	Private Function ReloadTable(ByVal pTableID As RenaissanceChangeID, ByVal UpdateEvent As RenaissanceGlobals.RenaissanceUpdateEventArgs) As Boolean
		' **********************************************************************
		' Function to Re-Load a standard dataset if it is already loaded.
		'
		' If the dataset is not already loaded, then the call to 'Get_Dataset' will return
		' Nothing since insufficient of the constructor parameters are given.
		' If a dataset IS returned, then the Load_Dataset funcion is used to refresh it.
		' **********************************************************************
		Dim ThisStandardDataset As RenaissanceGlobals.StandardDataset = RenaissanceStandardDatasets.GetStandardDataset(pTableID)
		Dim myDataset As DataSet

		Try
			If (ThisStandardDataset IsNot Nothing) Then
				myDataset = _MainForm.MainDataHandler.Get_Dataset(ThisStandardDataset.DatasetName)

				If (myDataset IsNot Nothing) Then
					If (Load_Table(pTableID, UpdateEvent, True) Is Nothing) Then
						Return False
					Else
						Return True
					End If
				End If
			End If
		Catch ex As Exception
		End Try

		Return False
	End Function

    ''' <summary>
    ''' Load_s the table.
    ''' </summary>
    ''' <param name="pTableID">The p table ID.</param>
    ''' <param name="UpdateEvent">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
    ''' <param name="pForceRefresh">if set to <c>true</c> [p force refresh].</param>
    ''' <returns>DataSet.</returns>
	Private Function Load_Table(ByVal pTableID As RenaissanceChangeID, ByVal UpdateEvent As RenaissanceGlobals.RenaissanceUpdateEventArgs, ByVal pForceRefresh As Boolean) As DataSet
		' **********************************************************************
		' Routine to Load a standard dataset or retrieve an existing one if it already
		' exists.
		'
		' If UpdateEvent is present and contains relevant UpdateDetail, then,
		' if the table is already loaded, consider doing a smart (partial) update.
		' **********************************************************************

		Dim myDataset As DataSet = Nothing
		Dim myConnection As SqlConnection = Nothing
		Dim tempDataset As DataSet = Nothing
    Dim TempTable As DataTable = Nothing


		Try

			Dim pDatasetDetails As RenaissanceGlobals.StandardDataset = RenaissanceStandardDatasets.GetStandardDataset(pTableID)

			Dim myAdaptor As SqlDataAdapter

			If (pDatasetDetails Is Nothing) Then
				Return Nothing
			End If

			Dim THIS_TABLENAME As String = pDatasetDetails.TableName
			'Dim THIS_ADAPTORNAME As String = pDatasetDetails.Adaptorname
			Dim THIS_DATASETNAME As String = pDatasetDetails.DatasetName

			tempDataset = _BackgroundDataHandler.Instantiate_Dataset(THIS_DATASETNAME)
			TempTable = tempDataset.Tables(0)

			' StandardDatasetNames

			myConnection = _MainForm.GetVeniceConnection()
			If myConnection Is Nothing Then
				Return Nothing
			End If

			' Use Different connection to the main venice one.
			myAdaptor = New SqlDataAdapter
			_MainForm.MainAdaptorHandler.Set_AdaptorCommands(myConnection, myAdaptor, THIS_TABLENAME)
      myDataset = _MainForm.MainDataHandler.Get_Dataset(THIS_DATASETNAME)

			'AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
			'AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
			'AddHandler myAdaptor.FillError, AddressOf OnRowFillError

      'Lotfi : if forceRefresh free memory of existing dataset
      If myDataset IsNot Nothing Then
        If pForceRefresh Then

          Try
            If (myDataset.Tables.Count > 0) Then
              'myDataset.Tables(0).Rows.Clear()
              myDataset.Tables(0).Clear()
            End If
          Catch ex As Exception
          End Try
          myDataset = Nothing
        End If
      End If

      If myDataset Is Nothing Then

        myDataset = _MainForm.MainDataHandler.Get_Dataset(THIS_DATASETNAME, True)

        Try

          _MainForm.SetToolStripText(_MainForm.VeniceStatusBar, "Loading " & THIS_TABLENAME)

          If (_MainForm.Extra_Debug) Then
            Call _MainForm.LogError("Background, Load_Table", LOG_LEVELS.Audit, "", "Full Background Update,  " & THIS_TABLENAME, "", False)
          End If

          Try
            If (myAdaptor.SelectCommand.Parameters.Contains("@Knowledgedate")) Then
              myAdaptor.SelectCommand.Parameters("@Knowledgedate").Value = _MainForm.Main_Knowledgedate
            End If
          Catch ex As Exception
          End Try

          Try
            myAdaptor.Fill(TempTable)
          Catch ex As Exception
            myAdaptor.SelectCommand.Connection.Close()
            SqlConnection.ClearPool(myAdaptor.SelectCommand.Connection)
            myAdaptor.SelectCommand.Connection.Open()

            TempTable.Clear()
            myAdaptor.Fill(TempTable)
          End Try

          SyncLock myDataset

            _MainForm.MoveTable(TempTable, myDataset.Tables(0))

          End SyncLock

        Catch ex As Exception
          If (_CloseDownFlag = False) Then
            If (myDataset.Tables.Count > 0) Then
              Call _MainForm.LogError("BackgroundTableLoader, Load_Table", LOG_LEVELS.Error, ex.Message, "Error Filling Table " & myDataset.Tables(0).TableName, ex.StackTrace, False)
            Else
              Call _MainForm.LogError("BackgroundTableLoader, Load_Table", LOG_LEVELS.Error, ex.Message, "Error Filling Table ", ex.StackTrace, False)
            End If
          End If

        Finally
          _MainForm.SetToolStripText(_MainForm.VeniceStatusBar, "")
        End Try

        ' Reset the IDs have Changed Flag, as the table has been re-loaded.
        pDatasetDetails.NotedIDsHaveChanged = False

      ElseIf (pForceRefresh = True) Or (pDatasetDetails.NotedIDsHaveChanged = True) Then

        Dim DoPartialUpdate As Boolean = True
        Dim UpdateString As String = ""
        Dim StringArray(-1) As String
        Dim UpdateCounter As Integer

        ' Establish if a partial update is possible.

        If (UpdateEvent Is Nothing) Then
          ' Partial Update is not possible
          DoPartialUpdate = False

        ElseIf (myAdaptor.SelectCommand.Parameters.Contains("@AuditID") = False) Then
          ' Partial Update is not possible
          DoPartialUpdate = False

        Else
          UpdateString = UpdateEvent.UpdateDetail(pTableID)

          If (UpdateString.Length <= 0) Then
            DoPartialUpdate = False
          Else
            StringArray = UpdateString.Split(New Char() {","c, "|"c}, StringSplitOptions.RemoveEmptyEntries)

            If (StringArray.Length > 0) AndAlso (StringArray.Length <= PARTIAL_UPDATE_TABLE_ITEM_LIMIT) Then  ' Arbitrary limit of XX items, after which a complete update makes as much sense.

              ' Validate, To be expanded upon, as necessary.

              Select Case pTableID

                Case Else
                  ' Just check all entries are Numeric

                  For UpdateCounter = 0 To (StringArray.Length - 1)

                    If (ConvertIsNumeric(StringArray(UpdateCounter)) = False) OrElse (CInt(ConvertValue(StringArray(UpdateCounter), GetType(Integer))) = 0) Then
                      DoPartialUpdate = False
                    End If

                  Next

              End Select

            Else
              DoPartialUpdate = False
            End If

          End If
        End If

        Try

          _MainForm.SetToolStripText(_MainForm.VeniceStatusBar, "Loading " & THIS_TABLENAME)

          Try
            If (myAdaptor.SelectCommand.Parameters.Contains("@Knowledgedate")) Then
              myAdaptor.SelectCommand.Parameters("@Knowledgedate").Value = _MainForm.Main_Knowledgedate
            End If
          Catch ex As Exception
          End Try

          If (DoPartialUpdate) Then

            Dim SelectedRows() As DataRow
            Dim thisRow As DataRow
            Dim RowCounter As Integer
            Dim ColumnCount As Integer

            If (_MainForm.Extra_Debug) Then
              Call _MainForm.LogError("Background, Load_Table", LOG_LEVELS.Audit, "", "Partial Background Update,  " & THIS_TABLENAME & ", " & UpdateString, "", False)
            End If

            ' Get Updated rows, one at a time and process....

            For UpdateCounter = 0 To (StringArray.Length - 1)

              Select Case pTableID

                Case Else
                  ' Default

                  myAdaptor.SelectCommand.Parameters("@AuditID").Value = ConvertValue(StringArray(UpdateCounter), GetType(Integer))

              End Select

              ' 

              TempTable.Clear()

              SyncLock myDataset
                SyncLock myDataset.Tables(0)

                  'myAdaptor.Fill(TempTable)
                  Try
                    myAdaptor.Fill(TempTable)
                  Catch ex As Exception
                    myAdaptor.SelectCommand.Connection.Close()
                    SqlConnection.ClearPool(myAdaptor.SelectCommand.Connection)
                    myAdaptor.SelectCommand.Connection.Open()

                    TempTable.Clear()
                    myAdaptor.Fill(TempTable)
                  End Try

                  ' Merge values in...

                  If (TempTable.Rows.Count = 0) Then
                    ' Row Deleted....


                    SelectedRows = myDataset.Tables(0).Select("AuditID=" & StringArray(UpdateCounter))

                    If (SelectedRows.Length > 0) Then

                      Try
                        For RowCounter = 0 To (SelectedRows.Length - 1)
                          thisRow = SelectedRows(RowCounter)
                          myDataset.Tables(0).Rows.Remove(thisRow)
                        Next
                      Catch ex As Exception
                      End Try

                    End If


                  Else ' (TempTable.Rows.Count > 0)
                    ' Add / Merge new Row(s).


                    Try
                      myDataset.Tables(0).BeginLoadData()

                      For RowCounter = 0 To (TempTable.Rows.Count - 1)

                        thisRow = TempTable.Rows(RowCounter)

                        Select Case pTableID

                          Case Else
                            ' Default.

                            SelectedRows = myDataset.Tables(0).Select("AuditID=" & thisRow("AuditID").ToString())

                        End Select


                        If (SelectedRows Is Nothing) OrElse (SelectedRows.Length <= 0) Then
                          ' Add

                          myDataset.Tables(0).ImportRow(thisRow)
                        Else
                          ' Merge
                          Dim tempItemArray() As Object = thisRow.ItemArray

                          ' Check for ReadOnly Columns (try not to have any !!!)
                          For ColumnCount = 0 To (myDataset.Tables(0).Columns.Count - 1)
                            If (myDataset.Tables(0).Columns(ColumnCount).ReadOnly) Then
                              tempItemArray(myDataset.Tables(0).Columns(ColumnCount).Ordinal) = Nothing
                            End If
                          Next

                          SelectedRows(0).BeginEdit()

                          SelectedRows(0).ItemArray = tempItemArray

                          SelectedRows(0).EndEdit()
                          SelectedRows(0).AcceptChanges()

                        End If


                      Next

                    Catch ex As Exception
                    Finally
                      myDataset.Tables(0).EndLoadData()
                    End Try

                  End If

                End SyncLock
              End SyncLock

            Next

          Else
            ' Full table update.

            If (_MainForm.Extra_Debug) Then
              Call _MainForm.LogError("Background, Load_Table", LOG_LEVELS.Audit, "", "Full Background Update,  " & THIS_TABLENAME, "", False)
            End If

            'myAdaptor.Fill(TempTable)
            Try
              myAdaptor.Fill(TempTable)
            Catch ex As Exception
              myAdaptor.SelectCommand.Connection.Close()
              SqlConnection.ClearPool(myAdaptor.SelectCommand.Connection)
              myAdaptor.SelectCommand.Connection.Open()

              TempTable.Clear()
              myAdaptor.Fill(TempTable)
            End Try

            SyncLock myDataset

              If (myDataset.Tables.Count = 0) Then
                myDataset.Tables.Add(pDatasetDetails.TableName)
              Else
                _MainForm.MoveTable(TempTable, myDataset.Tables(0))
              End If

              ' Reset the IDs have Changed Flag, as the table has been re-loaded.
              pDatasetDetails.NotedIDsHaveChanged = False

            End SyncLock

          End If

        Catch ex As Exception
          If (_CloseDownFlag = False) Then
            Call _MainForm.LogError("BackgroundTableLoaderClass, Load_Table", LOG_LEVELS.Error, ex.Message, "Error Filling (Refresh) Table " & myDataset.Tables(0).TableName, ex.StackTrace, False)
          End If
        Finally
          _MainForm.SetToolStripText(_MainForm.VeniceStatusBar, "")
        End Try

        ' Reset the IDs have Changed Flag, as the table has been re-loaded.
        pDatasetDetails.NotedIDsHaveChanged = False
      End If

			RaiseEvent TableLoaded(Me, pTableID, myDataset)

		Catch ex As Exception
		Finally
      If (myConnection IsNot Nothing) AndAlso CBool(myConnection.State And ConnectionState.Open) Then
        Try
          myConnection.Close()
        Catch ex As Exception
        End Try
      End If
		End Try

    'lotfi, free TempTable memory
    If TempTable IsNot Nothing Then
      TempTable.Clear()
      TempTable = Nothing
      'should i call the gc ?
      GC.Collect()
    End If

    Return myDataset
  End Function

    ''' <summary>
    ''' Enqueues the event.
    ''' </summary>
    ''' <param name="UpdateEventArgs">The <see cref="RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
	Public Sub EnqueueEvent(ByVal UpdateEventArgs As RenaissanceUpdateEventArgs)
		Try
			SyncLock UpdateQueueLock
				_UpdateQueue.Enqueue(New BackgroupWorkItemClass(BackgroundTaskType.SendUpdate, UpdateEventArgs))
			End SyncLock
		Catch ex As Exception
		End Try
	End Sub

    ''' <summary>
    ''' Enqueues the load table.
    ''' </summary>
    ''' <param name="ChangeID">The change ID.</param>
    ''' <param name="UpdateEventArgs">The <see cref="RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
    ''' <param name="ForceRefresh">if set to <c>true</c> [force refresh].</param>
	Public Sub EnqueueLoadTable(ByVal ChangeID As RenaissanceChangeID, ByVal UpdateEventArgs As RenaissanceUpdateEventArgs, ByVal ForceRefresh As Boolean)
		Try
			SyncLock UpdateQueueLock
				_UpdateQueue.Enqueue(New BackgroupWorkItemClass(BackgroundTaskType.LoadTable, ChangeID, ForceRefresh, UpdateEventArgs))
			End SyncLock
		Catch ex As Exception
		End Try
	End Sub

    ''' <summary>
    ''' Enqueues the reload table.
    ''' </summary>
    ''' <param name="ChangeID">The change ID.</param>
    ''' <param name="UpdateEventArgs">The <see cref="RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
	Public Sub EnqueueReloadTable(ByVal ChangeID As RenaissanceChangeID, ByVal UpdateEventArgs As RenaissanceUpdateEventArgs)
		Try
			SyncLock UpdateQueueLock
				_UpdateQueue.Enqueue(New BackgroupWorkItemClass(BackgroundTaskType.RefreshTable, ChangeID, False, UpdateEventArgs))
			End SyncLock
		Catch ex As Exception
		End Try
	End Sub

End Class