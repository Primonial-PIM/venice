' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="RiskExposureFunctions.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals
Imports RenaissanceUtilities.DatePeriodFunctions

Imports System.Data.SqlClient

''' <summary>
''' Class RiskExposureFunctions
''' </summary>
Module RiskExposureFunctions

    ''' <summary>
    ''' Risk_s the process all limits.
    ''' </summary>
    ''' <param name="MainForm">The main form.</param>
    ''' <param name="TS_StatusLabel">The T s_ status label.</param>
    ''' <param name="pFundID">The p fund ID.</param>
    ''' <param name="pFundName">Name of the p fund.</param>
    ''' <param name="pDateToPertrac">The p date to pertrac.</param>
    ''' <param name="pDateToRisk">The p date to risk.</param>
    ''' <param name="pPreciseExposureKnowledgedate">The p precise exposure knowledgedate.</param>
    ''' <param name="pExposureFlags">The p exposure flags.</param>
    ''' <param name="pStatusGroupFilter">The p status group filter.</param>
    ''' <param name="pAdministratorDatesFilter">The p administrator dates filter.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Friend Function Risk_ProcessAllLimits(ByRef MainForm As VeniceMain, ByRef TS_StatusLabel As ToolStripStatusLabel, ByVal pFundID() As Integer, ByVal pFundName As String, ByVal pDateToPertrac As Date, ByVal pDateToRisk As Date, ByVal pPreciseExposureKnowledgedate As Date, ByVal pExposureFlags As Integer, ByVal pStatusGroupFilter As String, ByVal pAdministratorDatesFilter As Integer) As Boolean
    '**************************************************************************************************
    ' Given an array of FundIDs, call Risk_ProcessAllLimits() for each one.
    '**************************************************************************************************
    Dim RVal As Boolean = True

    Try
      Dim ThisFundID As Integer

      For Each ThisFundID In pFundID
        RVal = RVal And Risk_ProcessAllLimits(MainForm, TS_StatusLabel, ThisFundID, pFundName, pDateToPertrac, pDateToRisk, pPreciseExposureKnowledgedate, pExposureFlags, pStatusGroupFilter, pAdministratorDatesFilter)
      Next

    Catch ex As Exception
      RVal = False
    End Try

    Return RVal

  End Function


  ''' <summary>
  ''' Process all risk limits.
  ''' </summary>
  ''' <param name="MainForm">The main form.</param>
  ''' <param name="TS_StatusLabel">The T s_ status label.</param>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pFundName">Name of the fund.</param>
  ''' <param name="pDateToPertrac">The p date to pertrac.</param>
  ''' <param name="pDateToRisk">The p date to risk.</param>
  ''' <param name="pPreciseExposureKnowledgedate">The p precise exposure knowledgedate.</param>
  ''' <param name="pExposureFlags">The exposure flags.</param>
  ''' <param name="pStatusGroupFilter">The status group filter.</param>
  ''' <param name="pAdministratorDatesFilter">The administrator dates filter.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Friend Function Risk_ProcessAllLimits(ByRef MainForm As VeniceMain, ByRef TS_StatusLabel As ToolStripStatusLabel, ByVal pFundID As Integer, ByVal pFundName As String, ByVal pDateToPertrac As Date, ByVal pDateToRisk As Date, ByVal pPreciseExposureKnowledgedate As Date, ByVal pExposureFlags As Integer, ByVal pStatusGroupFilter As String, ByVal pAdministratorDatesFilter As Integer) As Boolean
    '**************************************************************************************************
    ' Purpose : Manage the whole Limits calculation process :
    '           1) Establish Underlying Index Tables and Query definitions.
    '              Specifically, allow for Knowledgedate details and ensure the validity of the `rptqryProfitandLoss`
    '              query which is used in the Drawdown Calculations.
    '           2) Call each function in turn to handle the groups of Specific Risk monitoring Limits
    '
    ' Accepts : 
    '         : Integer- pFundID, Fund Code of Fund to calculate Exposures for.
    '         : Date   - pDateToPertrac, Latest date to select performance data for.
    '         : Date   - pDateToRisk, Date the Risk Report is bing Run to. (1 month after DateToPertrac)
    '         : Date   - pPreciseExposureKnowledgedate, KnowledgeDate to use
    '         : Integer- pExposureFlags
    '
    ' Returns : Boolean - To indicate success or failure of this function.
    '
    '**************************************************************************************************
    Dim ExposureFromDate As Date
    Dim ExposureToDate As Date

    Dim PreciseExposureKnowledgedate As Date

    Dim ExposureFlags As Integer
    Dim AggregatePositions As Integer
    Dim LookthroughRisk As Integer

    Dim CalculationDate As Date

    Dim FundID As Integer

    Dim Temp_Str As String
    Dim SQL_String As String

    Dim Fund_Value As Double


    ' *************************************************************************************************

    CalculationDate = Now()
    ExposureFlags = pExposureFlags

    ' *************************************************************************************************
    '
    ' Establish Underlying Index Tables and Queries
    '
    ' *************************************************************************************************

    PreciseExposureKnowledgedate = pPreciseExposureKnowledgedate

    ' AggregatePositions
    AggregatePositions = 0
    If (ExposureFlags And RenaissanceGlobals.RiskExposureFlags.AggregatedRisk) > 0 Then
      AggregatePositions = 2
    End If

    LookthroughRisk = 0
    If (ExposureFlags And RenaissanceGlobals.RiskExposureFlags.FundLookthrough) > 0 Then
      LookthroughRisk = 1
    End If

    ' Validate and get Report Dates
    ExposureFromDate = pDateToPertrac
    ExposureToDate = pDateToRisk

    ' Validate and get FundID
    FundID = pFundID

    ' Audit log : Record the fact that this user ran this function.
    Temp_Str = "FundID = " & FundID.ToString & _
     ", ExposureFromDate = " & ExposureFromDate & ", ExposureToDate = " & ExposureToDate & _
     ", ExposureReportKnowledgeDate = " & PreciseExposureKnowledgedate
    Call MainForm.LogError("Risk_ProcessAllLimits()", LOG_LEVELS.Audit, "Activity", Temp_Str, "", False)



    ' *************************************************************************************************
    '
    ' Clear the Exposures table for this calculation Date
    '
    ' *************************************************************************************************

    MainForm.SetToolStripText(TS_StatusLabel, "Clearing existing Exposures : " & pFundName)

    Dim DeleteCommand As New SqlCommand
    Try
      SQL_String = "DELETE FROM tblExposure " & _
       "WHERE (expFund = " & FundID.ToString() & ") AND (expDate = '" & Format(pDateToRisk, QUERY_SHORTDATEFORMAT) & "') AND " & _
        "(expKnowledgeDate = '" & Format(PreciseExposureKnowledgedate, QUERY_LONGDATEFORMAT) & "') AND " & _
        "(expSystemKnowledgeDate = '" & Format(MainForm.Main_Knowledgedate, QUERY_LONGDATEFORMAT) & "') AND " & _
        "(expFlags = " & ExposureFlags & ")"
      DeleteCommand.CommandType = CommandType.Text
      DeleteCommand.CommandText = SQL_String
      DeleteCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT
      DeleteCommand.Connection = MainForm.GetVeniceConnection

      SyncLock DeleteCommand.Connection
        DeleteCommand.ExecuteNonQuery()
      End SyncLock

    Catch ex As Exception
      MainForm.LogError("Risk_ProcessAllLimits, ClearExistingExposures", LOG_LEVELS.Error, ex.Message, "Error Clearing Exposure Records.", ex.StackTrace, True)
      Return False

    Finally
      Try
        If (Not (DeleteCommand.Connection Is Nothing)) Then
          DeleteCommand.Connection.Close()
        End If
      Catch ex As Exception
      End Try
    End Try


    ' *************************************************************************************************
    '
    ' Calculate Limits Data
    '
    ' *************************************************************************************************

    MainForm.SetToolStripText(TS_StatusLabel, "Generating Fund Value : " & pFundName)

    ' Get Fund Valuation
    Fund_Value = Get_FundValue(MainForm, FundID, ExposureToDate, PreciseExposureKnowledgedate, False, pStatusGroupFilter, pAdministratorDatesFilter)

    MainForm.SetToolStripText(TS_StatusLabel, "Collecting Instrument Exposures : " & pFundName)

    Dim tblInstrumentExposures As New DataTable("tblInstrumentExposures")
    Dim tempAdaptor As New SqlDataAdapter

    Try
      MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.GetVeniceConnection(), tempAdaptor, "spu_RiskInstrumentExposures")
      tempAdaptor.SelectCommand.Parameters("@FundID").Value = FundID
      tempAdaptor.SelectCommand.Parameters("@ValueDateFrom").Value = ExposureFromDate
      tempAdaptor.SelectCommand.Parameters("@ValueDateTo").Value = ExposureToDate
      tempAdaptor.SelectCommand.Parameters("@UnitPriceVariant").Value = 0
      tempAdaptor.SelectCommand.Parameters("@StatusGroupFilter").Value = pStatusGroupFilter
      tempAdaptor.SelectCommand.Parameters("@AdministratorDatesFilter").Value = pAdministratorDatesFilter
      tempAdaptor.SelectCommand.Parameters("@DisregardWatermarkPoints").Value = 0
      tempAdaptor.SelectCommand.Parameters("@KnowledgeDateFrom").Value = PreciseExposureKnowledgedate
      tempAdaptor.SelectCommand.Parameters("@KnowledgeDateTo").Value = PreciseExposureKnowledgedate
      tempAdaptor.SelectCommand.Parameters("@KnowledgeDateLinkedTables").Value = MainForm.Main_Knowledgedate
      tempAdaptor.SelectCommand.Parameters("@KnowledgeDateLimitsTable").Value = MainForm.Main_Knowledgedate
      tempAdaptor.SelectCommand.Parameters("@AggregateToInstrumentParentID").Value = AggregatePositions
      tempAdaptor.SelectCommand.Parameters("@FundLookthrough").Value = LookthroughRisk
      SyncLock tempAdaptor.SelectCommand.Connection
        tempAdaptor.Fill(tblInstrumentExposures)
      End SyncLock

      MainForm.SetToolStripText(TS_StatusLabel, "Collecting fund Valuation : " & pFundName)
    Catch ex As Exception
      MainForm.LogError("Risk_ProcessAllLimits", LOG_LEVELS.Error, ex.Message, "Error getting Risk Instrument Exposures", ex.StackTrace, True)
      'lotfi: return false to exist the calling RunReport_OnDate sub
      Return False
    Finally
      Try
        If (tempAdaptor.SelectCommand IsNot Nothing) AndAlso (tempAdaptor.SelectCommand.Connection IsNot Nothing) Then
          tempAdaptor.SelectCommand.Connection.Close()
          tempAdaptor.SelectCommand.Connection = Nothing
        End If
      Catch ex As Exception
      End Try

    End Try

    ' Is the P&L info required ?
    Dim tblFundPnL As New DataTable("tblFundPnL")
    Dim myLimitRows() As RenaissanceDataClass.DSLimits.tblLimitsRow

    Dim DSLimits As RenaissanceDataClass.DSLimits
		Dim tblLimits As RenaissanceDataClass.DSLimits.tblLimitsDataTable

		' Not the background function as I want to guarantee having the latest Limits table.
		DSLimits = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblLimits, True, True)

    If Not (DSLimits Is Nothing) Then
      tblLimits = DSLimits.tblLimits
      myLimitRows = tblLimits.Select("(limType=" & CInt(LimitType.RealisedDrawdown).ToString & ") AND ((limFund=0) OR (LimFund=" & FundID.ToString & ") OR (" & FundID.ToString & "=0))")
    Else
      tblLimits = Nothing
      ReDim myLimitRows(0)
    End If

    If (myLimitRows.Length > 0) Then

      tempAdaptor = New SqlDataAdapter

      Try
        MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.GetVeniceConnection(), tempAdaptor, "spu_ProfitAndLoss")
        tempAdaptor.SelectCommand.Parameters("@FundID").Value = FundID
        tempAdaptor.SelectCommand.Parameters("@ValueDateFrom").Value = ExposureFromDate
        tempAdaptor.SelectCommand.Parameters("@ValueDateTo").Value = ExposureToDate
        tempAdaptor.SelectCommand.Parameters("@UnitPriceVariant").Value = 0
        tempAdaptor.SelectCommand.Parameters("@StatusGroupFilter").Value = pStatusGroupFilter
        tempAdaptor.SelectCommand.Parameters("@AdministratorDatesFilter").Value = pAdministratorDatesFilter
        tempAdaptor.SelectCommand.Parameters("@DisregardWatermarkPoints").Value = 0
        tempAdaptor.SelectCommand.Parameters("@KnowledgeDateFrom").Value = PreciseExposureKnowledgedate
        tempAdaptor.SelectCommand.Parameters("@KnowledgeDateTo").Value = PreciseExposureKnowledgedate
        tempAdaptor.SelectCommand.Parameters("@KnowledgeDateLinkedTables").Value = MainForm.Main_Knowledgedate
        tempAdaptor.SelectCommand.Parameters("@AggregateToInstrumentParent").Value = AggregatePositions
        SyncLock tempAdaptor.SelectCommand.Connection
          tempAdaptor.Fill(tblFundPnL)
        End SyncLock

      Catch ex As Exception
        MainForm.LogError("Risk_ProcessAllLimits", LOG_LEVELS.Error, ex.Message, "Error getting P&L for Risk Calculations.", ex.StackTrace, True)
        'lotfi: return false to exist the calling RunReport_OnDate sub
        Return False
      Finally
        Try
          If (tempAdaptor.SelectCommand IsNot Nothing) AndAlso (tempAdaptor.SelectCommand.Connection IsNot Nothing) Then
            tempAdaptor.SelectCommand.Connection.Close()
            tempAdaptor.SelectCommand.Connection = Nothing
          End If
        Catch ex As Exception
        End Try
      End Try


    End If

    Try
      ' 1) DrawDown Limits (LimType 4)

      MainForm.SetToolStripText(TS_StatusLabel, "Generating DrawDown Exposures : " & pFundName)

      If Risk_ProcessDrawDownLimits(MainForm, tblFundPnL, tblLimits, FundID, Fund_Value, pDateToPertrac, pDateToRisk, CalculationDate, PreciseExposureKnowledgedate, ExposureFlags) = False Then
        Return False
      End If


      ' 2) Instrument Specific Capital Usage and Geneneral Instrument Capital Usage (LimType 1 and 10)

      MainForm.SetToolStripText(TS_StatusLabel, "Generating Instrument Capital Exposures : " & pFundName)

      If ProcessInstrumentImpactLimits(MainForm, tblInstrumentExposures, FundID, Fund_Value, pDateToPertrac, pDateToRisk, CalculationDate, PreciseExposureKnowledgedate, ExposureFlags) = False Then
        Return False
      End If

      ' 3) Sector Specific Capital Usage and Geneneral Instrument Capital Usage (LimType 2 and 11)

      MainForm.SetToolStripText(TS_StatusLabel, "Generating Sector Capital Exposures : " & pFundName)

      If ProcessSectorImpactLimits(MainForm, tblInstrumentExposures, FundID, Fund_Value, pDateToPertrac, pDateToRisk, CalculationDate, PreciseExposureKnowledgedate, ExposureFlags) = False Then
        Return False
      End If

      ' 4) Total Fund Capital Usage (LimType 3)

      MainForm.SetToolStripText(TS_StatusLabel, "Generating Fund Total Capital Exposures : " & pFundName)

      If ProcessTotalImpactLimits(MainForm, tblInstrumentExposures, FundID, Fund_Value, pDateToPertrac, pDateToRisk, CalculationDate, PreciseExposureKnowledgedate, ExposureFlags) = False Then
        Return False
      End If


      ' 5) Instrument Specific Weights and Geneneral Instrument Weights (LimType 5 and 7)

      MainForm.SetToolStripText(TS_StatusLabel, "Generating Instrument Weight Exposures : " & pFundName)

      If ProcessInstrumentWeightLimits(MainForm, tblInstrumentExposures, FundID, Fund_Value, pDateToPertrac, pDateToRisk, CalculationDate, PreciseExposureKnowledgedate, ExposureFlags) = False Then
        Return False
      End If

      ' 6) Sector Specific Weights and Geneneral Instrument Weights (LimType 6 and 8)

      MainForm.SetToolStripText(TS_StatusLabel, "Generating Sector Weight Exposures : " & pFundName)

      If ProcessSectorWeightLimits(MainForm, tblInstrumentExposures, FundID, Fund_Value, pDateToPertrac, pDateToRisk, CalculationDate, PreciseExposureKnowledgedate, ExposureFlags) = False Then
        Return False
      End If

      ' 6A) ProcessSectorGroupWeightLimits 12, 13

      MainForm.SetToolStripText(TS_StatusLabel, "Generating Sector Group Exposures : " & pFundName)

      If ProcessSectorGroupWeightLimits(MainForm, tblInstrumentExposures, FundID, Fund_Value, pDateToPertrac, pDateToRisk, CalculationDate, PreciseExposureKnowledgedate, ExposureFlags) = False Then
        Return False
      End If

      ' 6B) Dealing Period Exposures (LimType 14)

      MainForm.SetToolStripText(TS_StatusLabel, "Generating Dealing Period Exposures : " & pFundName)

      If ProcessDealingPeriodLimits(MainForm, tblInstrumentExposures, FundID, Fund_Value, pDateToPertrac, pDateToRisk, CalculationDate, PreciseExposureKnowledgedate, ExposureFlags) = False Then
        Return False
      End If

      ' 7) Fund Instrument Count Limits (LimType 9)

      MainForm.SetToolStripText(TS_StatusLabel, "Generating Instrument Count Exposures : " & pFundName)

      If ProcessInstrumentCountLimits(MainForm, tblInstrumentExposures, FundID, Fund_Value, pDateToPertrac, pDateToRisk, CalculationDate, PreciseExposureKnowledgedate, ExposureFlags) = False Then
        Return False
      End If

      ' 8) Currency Exposure Limits (LimType 15)

      MainForm.SetToolStripText(TS_StatusLabel, "Generating Currency Exposures : " & pFundName)

      If ProcessCurrencyExposureLimits(MainForm, tblInstrumentExposures, FundID, Fund_Value, pDateToPertrac, pDateToRisk, CalculationDate, PreciseExposureKnowledgedate, ExposureFlags) = False Then
        Return False
      End If

      ' 9) Category Limits (LimType 16)

      MainForm.SetToolStripText(TS_StatusLabel, "Generating Category Exposures : " & pFundName)

      If ProcessCategoryExposureLimits(MainForm, tblInstrumentExposures, FundID, Fund_Value, pDateToPertrac, pDateToRisk, CalculationDate, PreciseExposureKnowledgedate, ExposureFlags) = False Then
        Return False
      End If

      'Lotfi: 02/05/2014 for 5-10-40 instrument weight rule
      ' 10) 5-10-40 Category Limit (LimType 16)

      MainForm.SetToolStripText(TS_StatusLabel, "Generating 5-10-40 ratios Exposure : " & pFundName)

      If ProcessCinqDixQuarenteExposureLimits(MainForm, tblInstrumentExposures, FundID, Fund_Value, pDateToPertrac, pDateToRisk, CalculationDate, PreciseExposureKnowledgedate, ExposureFlags) = False Then
        Return False
      End If
    Catch ex As Exception

      MainForm.LogError("Risk_ProcessDrawDownLimits", LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)

    Finally

      MainForm.SetToolStripText(TS_StatusLabel, "Exposures Done : " & pFundName)

    End Try

    Return True

  End Function



  ''' <summary>
  ''' Risk_s the process draw down limits.
  ''' </summary>
  ''' <param name="MainForm">The main form.</param>
  ''' <param name="tblFundPnL">The TBL fund pn L.</param>
  ''' <param name="tblLimits">The TBL limits.</param>
  ''' <param name="FundId">The fund id.</param>
  ''' <param name="Fund_Value">The fund_ value.</param>
  ''' <param name="pDateToPertrac">The p date to pertrac.</param>
  ''' <param name="pDateToRisk">The p date to risk.</param>
  ''' <param name="CalculationDate">The calculation date.</param>
  ''' <param name="pPreciseExposureKnowledgedate">The p precise exposure knowledgedate.</param>
  ''' <param name="ExposureFlags">The exposure flags.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function Risk_ProcessDrawDownLimits(ByRef MainForm As VeniceMain, ByRef tblFundPnL As DataTable, ByRef tblLimits As RenaissanceDataClass.DSLimits.tblLimitsDataTable, ByVal FundId As Integer, ByVal Fund_Value As Double, ByVal pDateToPertrac As Date, ByVal pDateToRisk As Date, ByVal CalculationDate As Date, ByVal pPreciseExposureKnowledgedate As Date, ByVal ExposureFlags As Integer) As Boolean
    '**************************************************************************************************
    ' Purpose : Manage the whole Drawdown Limits calculation process :
    '
    '**************************************************************************************************

    Dim SelectedLimits As RenaissanceDataClass.DSLimits.tblLimitsRow()
    Dim ThisLimit As RenaissanceDataClass.DSLimits.tblLimitsRow
    Dim SelectedPnLLines As DataRow()
    Dim thisPnLLine As DataRow

    Dim Counter As Integer

    Dim DSExposure As New RenaissanceDataClass.DSExposure
    Dim tblExposure As RenaissanceDataClass.DSExposure.tblExposureDataTable
    Dim ExposureRow As RenaissanceDataClass.DSExposure.tblExposureRow
    Dim AdpExposure As New SqlDataAdapter

    Try
      tblExposure = DSExposure.tblExposure
    Catch ex As Exception
      MainForm.LogError("Risk_ProcessDrawDownLimits", LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
      Return False
    End Try

    Try
      MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION), AdpExposure, "tblExposure")
    Catch ex As Exception
      MainForm.LogError("Risk_ProcessDrawDownLimits", LOG_LEVELS.Error, ex.Message, "Error configuring tblExposure Adaptor.", ex.StackTrace, True)
      Return False
    End Try

    ' Select appropriate Exposure Records
    Try
      SelectedLimits = tblLimits.Select("(limFund = " & FundId.ToString & ") AND (limType = " & LimitType.RealisedDrawdown & ")")

      ' Any Limits returned ?

      If (SelectedLimits Is Nothing) OrElse (SelectedLimits.Length <= 0) Then
        Return True
      End If
    Catch ex As Exception
      MainForm.LogError("Risk_ProcessDrawDownLimits", LOG_LEVELS.Error, ex.Message, "Error selecting tblLimits rows.", ex.StackTrace, True)
      Return False
    End Try

    Dim StartPrice As Double
    Dim EndPrice As Double
    Dim DrawDown As Double
    Dim CurrentDrawDown As Double
    Dim thisStatsDatePeriod As DealingPeriod

    For Counter = 0 To (SelectedLimits.Length - 1)
      Try
        ThisLimit = SelectedLimits(Counter)

        If IsNumeric(ThisLimit.limInstrument) Then
          SelectedPnLLines = tblFundPnL.Select("Instrument=" & ThisLimit.limInstrument.ToString)

          If (Not (SelectedPnLLines Is Nothing)) AndAlso (SelectedPnLLines.Length > 0) Then
            For Each thisPnLLine In SelectedPnLLines
              ExposureRow = tblExposure.NewtblExposureRow

              If Math.Abs(CDbl(thisPnLLine("Value2"))) >= 1 Then

                ExposureRow.expDate = pDateToRisk
                ExposureRow.expFund = FundId
                ExposureRow.expLimitID = ThisLimit.limID
                ExposureRow.expIsDetailRow = 0
                ExposureRow.expLimitType = LimitType.RealisedDrawdown
                ExposureRow.expLimitInstrument = ThisLimit.limInstrument
                ExposureRow.expLimitSector = CInt(thisPnLLine("InstrumentFundType"))
                ExposureRow.expLimitSectorName = CStr(thisPnLLine("FundTypeDescription"))
                ExposureRow.expLimitDealingPeriod = 0
                ExposureRow.expLimitGreaterOrLessThan = ThisLimit.limGreaterOrLessThan
                ExposureRow.expLimit = ThisLimit.limLimit

                'IIf((([Adjustedstartprice]<>0) And ([Adjustedendprice]<>0)),IIf(Exp([LastOfLogDrawdown])*([AdjustedEndPrice]/[AdjustedStartPrice])-1>0,0,Exp([lastofLogDrawdown])*([AdjustedEndPrice]/[AdjustedStartPrice]) -1),Exp([LastOfLogDrawdown])-1) AS CurrentDrawdown, 
                '((-[CurrentDrawdown])/IIf([LimLimit]=0,0.00000001,[LimLimit])) AS DrawDownLimitUsage
                If thisPnLLine.IsNull("StartPrice") Then
                  StartPrice = 0
                Else
                  StartPrice = CDbl(thisPnLLine("StartPrice"))
                End If
                If thisPnLLine.IsNull("EndPrice") Then
                  EndPrice = 0
                Else
                  EndPrice = CDbl(thisPnLLine("EndPrice"))
                End If


                ' Get Pertrac data Drawdown.

                Try
                  If thisPnLLine.IsNull("PertracCode") Then
                    DrawDown = 1
                  Else
                    DrawDown = MainForm.StatFunctions.GetCurrentDrawdown(MainForm.PertracData.GetPertracDataPeriod(CULng(thisPnLLine("PertracCode"))), CULng(thisPnLLine("PertracCode")), False, Renaissance_BaseDate, pDateToPertrac, 1.0#)
                    ' DrawDown = MainForm.Pertrac.PertracDrawdown(CInt(thisPnLLine("PertracCode")), pDateToPertrac)
                  End If
                Catch ex As Exception
                  DrawDown = 1
                End Try

                ' Is the Pertrac End Date before the PnL StartPriceDate ?
                ' If so, then try to fetch a better Venice 'Start Price'.

                Try
                  Dim PertracDateSeries() As Date
                  thisStatsDatePeriod = MainForm.PertracData.GetPertracDataPeriod(CULng(thisPnLLine("PertracCode")))

                  PertracDateSeries = MainForm.StatFunctions.DateSeries(thisStatsDatePeriod, CULng(thisPnLLine("PertracCode")), False, 12, 1, False, Renaissance_BaseDate, pDateToPertrac)

                  If IsDate(thisPnLLine("StartPriceDate")) AndAlso (PertracDateSeries IsNot Nothing) AndAlso (PertracDateSeries.Length > 0) Then

                    If FitDateToPeriod(thisStatsDatePeriod, PertracDateSeries(PertracDateSeries.Length - 1), True) < FitDateToPeriod(thisStatsDatePeriod, CDate(thisPnLLine("StartPriceDate")), True) Then
                      ' Get Best Venice price relating to the Pertrac End Date.

                      Dim GetPriceCommand As New SqlCommand
                      Dim SQL_String As String
                      Dim TempStartPrice As Double

                      Try
                        SQL_String = "SELECT dbo.fn_BestSinglePrice(@InstrumentID, @ValueDate, 0, @KnowledgeDate)"
                        GetPriceCommand.CommandType = CommandType.Text
                        GetPriceCommand.CommandText = SQL_String
                        GetPriceCommand.Parameters.Add("@InstrumentID", SqlDbType.Int).Value = ThisLimit.limInstrument
                        GetPriceCommand.Parameters.Add("@ValueDate", System.Data.SqlDbType.DateTime).Value = FitDateToPeriod(thisStatsDatePeriod, PertracDateSeries(PertracDateSeries.Length - 1), True)
                        GetPriceCommand.Parameters.Add("@KnowledgeDate", System.Data.SqlDbType.DateTime).Value = MainForm.Main_Knowledgedate
                        GetPriceCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT
                        GetPriceCommand.Connection = MainForm.GetVeniceConnection

                        SyncLock GetPriceCommand.Connection
                          TempStartPrice = CDbl(MainForm.ExecuteScalar(GetPriceCommand))
                          'TempStartPrice = CDbl(GetPriceCommand.ExecuteScalar)
                        End SyncLock

                        If (TempStartPrice > 0) Then
                          StartPrice = TempStartPrice
                        End If

                      Catch ex As Exception
                        TempStartPrice = StartPrice

                      Finally
                        Try
                          If (Not (GetPriceCommand.Connection Is Nothing)) Then
                            GetPriceCommand.Connection.Close()
                          End If
                        Catch ex As Exception
                        End Try
                      End Try

                    End If

                  End If

                Catch ex As Exception
                End Try

                ' Apply residual Drawdown based on Venice price data.

                CurrentDrawDown = DrawDown
                If (StartPrice <> 0) And (EndPrice <> 0) Then
                  If ((DrawDown * (EndPrice / StartPrice)) - 1) >= 0 Then
                    CurrentDrawDown = 0
                    ExposureRow.expExposure = 0
                  Else
                    CurrentDrawDown = ((DrawDown * (EndPrice / StartPrice)) - 1)
                    ExposureRow.expExposure = -CurrentDrawDown
                  End If
                Else
                  ExposureRow.expExposure = -DrawDown
                End If

                ' Set Usage figures.

                If (ThisLimit.limLimit = 0) Then
                  ExposureRow.expUsage = ExposureRow.expExposure / 0.000000001
                Else
                  ExposureRow.expUsage = ExposureRow.expExposure / ThisLimit.limLimit
                End If

                If ExposureRow.expUsage < 1 Then
                  If ExposureRow.expUsage > 0.75 Then
                    ExposureRow.expStatus = "Warning"
                  Else
                    ExposureRow.expStatus = "OK"
                  End If
                Else
                  ExposureRow.expStatus = "Break"
                End If
                ExposureRow.expDateCalculated = CalculationDate

                ExposureRow.expKnowledgeDate = pPreciseExposureKnowledgedate
                If (pPreciseExposureKnowledgedate = KNOWLEDGEDATE_NOW) OrElse (pPreciseExposureKnowledgedate.TimeOfDay.TotalSeconds >= LAST_SECOND) Then
                  ExposureRow.expKnowledgeDateWholeday = (-1)
                Else
                  ExposureRow.expKnowledgeDateWholeday = 0
                End If

                ExposureRow.expSystemKnowledgeDate = MainForm.Main_Knowledgedate
                If (MainForm.Main_Knowledgedate = KNOWLEDGEDATE_NOW) OrElse (MainForm.Main_Knowledgedate.TimeOfDay.TotalSeconds >= LAST_SECOND) Then
                  ExposureRow.expSystemKnowledgeDateWholeday = (-1)
                Else
                  ExposureRow.expSystemKnowledgeDateWholeday = 0
                End If

                ExposureRow.expFlags = ExposureFlags

                If (ExposureRow.expUsage <> 0) Then
                  tblExposure.Rows.Add(ExposureRow)
                End If
              End If
            Next

          End If
        End If
      Catch ex As Exception
        MainForm.LogError("Risk_ProcessDrawDownLimits", LOG_LEVELS.Error, ex.Message, "Error calculating DrawDown Limits.", ex.StackTrace, True)
        Return False
      End Try
    Next


    ' Save New Exposure Records.
    Try
      If tblExposure.Rows.Count > 0 Then
        MainForm.AdaptorUpdate("RiskExposureFunctions", AdpExposure, tblExposure)
      End If
    Catch ex As Exception
      MainForm.LogError("Risk_ProcessDrawDownLimits", LOG_LEVELS.Error, ex.Message, "Error updating Drawdown Exposures.", ex.StackTrace, True)
      Return False
    End Try

    Return True

  End Function


  ''' <summary>
  ''' Processes the instrument impact limits.
  ''' </summary>
  ''' <param name="MainForm">The main form.</param>
  ''' <param name="tblInstrumentExposures">The TBL instrument exposures.</param>
  ''' <param name="FundId">The fund id.</param>
  ''' <param name="Fund_Value">The fund_ value.</param>
  ''' <param name="pDateToPertrac">The p date to pertrac.</param>
  ''' <param name="pDateToRisk">The p date to risk.</param>
  ''' <param name="CalculationDate">The calculation date.</param>
  ''' <param name="pPreciseExposureKnowledgedate">The p precise exposure knowledgedate.</param>
  ''' <param name="ExposureFlags">The exposure flags.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function ProcessInstrumentImpactLimits(ByRef MainForm As VeniceMain, ByRef tblInstrumentExposures As DataTable, ByVal FundId As Integer, ByVal Fund_Value As Double, ByVal pDateToPertrac As Date, ByVal pDateToRisk As Date, ByVal CalculationDate As Date, ByVal pPreciseExposureKnowledgedate As Date, ByVal ExposureFlags As Integer) As Boolean

    ' Exit if no Exposure Data
    Try
      If (tblInstrumentExposures Is Nothing) OrElse (tblInstrumentExposures.Rows.Count <= 0) Then
        Return True
      End If
    Catch ex As Exception
      Return False
    End Try

    ' 
    Dim SelectedInstrumentExposures As DataRow()
    Dim thisInstrumentExposure As DataRow
    Dim Counter As Integer
    Dim NewExposureRecords As New ArrayList

    Dim DSExposure As New RenaissanceDataClass.DSExposure
    Dim tblExposure As RenaissanceDataClass.DSExposure.tblExposureDataTable
    Dim ExposureRow As RenaissanceDataClass.DSExposure.tblExposureRow
    Dim AdpExposure As New SqlDataAdapter
    Dim ThisInstrumentValue As Double

    Try
      tblExposure = DSExposure.tblExposure
    Catch ex As Exception
      MainForm.LogError("ProcessInstrumentImpactLimits", LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
      Return False
    End Try

    Dim RISK_TransactionFund As Integer
    Dim RISK_TransactionInstrument As Integer
    Dim RISK_InstrumentFundType As Integer
    Dim RISK_FundTypeDescription As Integer
    Dim RISK_InstrumentValue As Integer
    Dim RISK_InstrumentWorstCase As Integer
    Dim RISK_limType As Integer
    Dim RISK_limitID As Integer
    Dim RISK_limGreaterOrLessThan As Integer
    Dim RISK_limLimit As Integer

    Dim RISK_limOnPremium As Integer
    Dim RISK_InstrumentHasDelta As Integer
    Dim RISK_InstrumentDelta As Integer
    Dim RISK_InstrumentContractSize As Integer
    Dim RISK_InstrumentMultiplier As Integer
    Dim RISK_InstrumentUnderlyingPrice As Integer
    Dim RISK_InstrumentSharesPerOption As Integer
    Dim RISK_FXRate2 As Integer
    Dim RISK_FinalUnits As Integer

    Dim RISK_Weight As Double
    Dim RISK_Impact As Double
    Dim RISK_Usage As Double
    Dim RISK_Status As String


    ' Establish Ordinals
    Try
      RISK_TransactionFund = tblInstrumentExposures.Columns.IndexOf("Fund")
      RISK_limitID = tblInstrumentExposures.Columns.IndexOf("limID")
      RISK_TransactionInstrument = tblInstrumentExposures.Columns.IndexOf("Instrument")
      RISK_InstrumentFundType = tblInstrumentExposures.Columns.IndexOf("InstrumentFundType")
      RISK_FundTypeDescription = tblInstrumentExposures.Columns.IndexOf("FundTypeDescription")
      RISK_InstrumentValue = tblInstrumentExposures.Columns.IndexOf("Value2")
      RISK_InstrumentWorstCase = tblInstrumentExposures.Columns.IndexOf("InstrumentWorstCase")
      RISK_limType = tblInstrumentExposures.Columns.IndexOf("limType")
      RISK_limGreaterOrLessThan = tblInstrumentExposures.Columns.IndexOf("limGreaterOrLessThan")
      RISK_limLimit = tblInstrumentExposures.Columns.IndexOf("limLimit")

      RISK_limOnPremium = tblInstrumentExposures.Columns.IndexOf("limOnPremium")
      RISK_InstrumentHasDelta = tblInstrumentExposures.Columns.IndexOf("InstrumentTypeHasDelta")
      RISK_InstrumentDelta = tblInstrumentExposures.Columns.IndexOf("BestDelta")
      RISK_InstrumentContractSize = tblInstrumentExposures.Columns.IndexOf("InstrumentContractSize")
      RISK_InstrumentMultiplier = tblInstrumentExposures.Columns.IndexOf("InstrumentMultiplier")
      RISK_InstrumentUnderlyingPrice = tblInstrumentExposures.Columns.IndexOf("UnderlyingPrice")
      RISK_InstrumentSharesPerOption = tblInstrumentExposures.Columns.IndexOf("InstrumentSharesPerOption")
      RISK_FXRate2 = tblInstrumentExposures.Columns.IndexOf("FXRate2") ' Compound FX Rate Instrument to Fund FX
      RISK_FinalUnits = tblInstrumentExposures.Columns.IndexOf("FinalUnits") ' SignedUnits from 2nd PnL Valuation query.

    Catch ex As Exception
      MainForm.LogError("ProcessInstrumentImpactLimits", LOG_LEVELS.Error, ex.Message, "Error getting Field Ordinals from tblInstrumentExposures", ex.StackTrace, True)
      Return False
    End Try

    Try
      MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION), AdpExposure, "tblExposure")
    Catch ex As Exception
      MainForm.LogError("ProcessInstrumentImpactLimits", LOG_LEVELS.Error, ex.Message, "Error configuring tblExposure Adaptor.", ex.StackTrace, True)
      Return False
    End Try

    ' Select appropriate Exposure Records
    Try
      SelectedInstrumentExposures = tblInstrumentExposures.Select("((limType = " & CInt(RenaissanceGlobals.LimitType.InstrumentCapitalUsage).ToString & ") OR ((limType = " & CInt(RenaissanceGlobals.LimitType.SpecificInstrumentCapitalUsage).ToString & ") AND (limInstrument = Instrument)))", "Fund, limType, Instrument")

      If (SelectedInstrumentExposures Is Nothing) OrElse (SelectedInstrumentExposures.Length <= 0) Then
        Return True
      End If
    Catch ex As Exception
      MainForm.LogError("ProcessInstrumentImpactLimits", LOG_LEVELS.Error, ex.Message, "Error selecting tblInstrumentExposures rows.", ex.StackTrace, True)
      Return False
    End Try


    ' Process selected Exposures
    ' 
    For Counter = 0 To (SelectedInstrumentExposures.Length - 1)
      Try
        thisInstrumentExposure = SelectedInstrumentExposures(Counter)

        If (CDbl(thisInstrumentExposure(RISK_InstrumentWorstCase)) <> 0) OrElse _
         (Not ((CInt(thisInstrumentExposure(RISK_InstrumentFundType)) = FundType.Cash) OrElse _
         (CInt(thisInstrumentExposure(RISK_InstrumentFundType)) = FundType.Loan) OrElse _
         (CInt(thisInstrumentExposure(RISK_InstrumentFundType)) = FundType.Fee))) Then

          ExposureRow = tblExposure.NewtblExposureRow

          ThisInstrumentValue = CDbl(thisInstrumentExposure(RISK_InstrumentValue))

          ' Calculate Instrument Underlying, if appropriate.
          If (CBool(thisInstrumentExposure(RISK_limOnPremium)) = False) AndAlso CBool(thisInstrumentExposure(RISK_InstrumentHasDelta)) Then
            ThisInstrumentValue = CDbl(thisInstrumentExposure(RISK_FinalUnits)) * CDbl(thisInstrumentExposure(RISK_InstrumentDelta)) * CDbl(thisInstrumentExposure(RISK_InstrumentContractSize)) * CDbl(thisInstrumentExposure(RISK_InstrumentMultiplier)) * CDbl(thisInstrumentExposure(RISK_InstrumentSharesPerOption)) * CDbl(thisInstrumentExposure(RISK_InstrumentUnderlyingPrice)) * CDbl(thisInstrumentExposure(RISK_FXRate2))
          End If

          If Fund_Value = 0 Then
            RISK_Weight = 1
          Else
            RISK_Weight = CDbl(ThisInstrumentValue) / Fund_Value
          End If

          RISK_Impact = RISK_Weight * (CDbl(thisInstrumentExposure(RISK_InstrumentWorstCase)) * 10000)

          If CInt(thisInstrumentExposure(RISK_limGreaterOrLessThan)) = RenaissanceGlobals.Relationship.LessThan Then
            RISK_Usage = RISK_Impact / CDbl(thisInstrumentExposure(RISK_limLimit))
          Else
            RISK_Usage = CDbl(thisInstrumentExposure(RISK_limLimit)) / RISK_Impact
          End If

          If (RISK_Usage >= 1) Then
            RISK_Status = "Break"
          ElseIf (RISK_Usage >= 0.75) Then
            RISK_Status = "Warning"
          Else
            RISK_Status = "OK"
          End If

          ExposureRow.expFund = FundId
          ExposureRow.expLimitID = CInt(thisInstrumentExposure(RISK_limitID))
          ExposureRow.expIsDetailRow = 0
          ExposureRow.expLimitSector = CInt(thisInstrumentExposure(RISK_InstrumentFundType))
          ExposureRow.expLimitSectorName = CStr(thisInstrumentExposure(RISK_FundTypeDescription))
          ExposureRow.expLimitInstrument = CInt(thisInstrumentExposure(RISK_TransactionInstrument))
          ExposureRow.expLimitType = CInt(thisInstrumentExposure(RISK_limType))
          ExposureRow.expLimitGreaterOrLessThan = CInt(thisInstrumentExposure(RISK_limGreaterOrLessThan))
          ExposureRow.expLimit = CDbl(thisInstrumentExposure(RISK_limLimit))
          ExposureRow.expExposure = RISK_Impact
          ExposureRow.expUsage = RISK_Usage
          ExposureRow.expStatus = RISK_Status
          ExposureRow.expDate = pDateToRisk
          ExposureRow.expDateCalculated = CalculationDate

          ExposureRow.expKnowledgeDate = pPreciseExposureKnowledgedate
          If (pPreciseExposureKnowledgedate = KNOWLEDGEDATE_NOW) OrElse (pPreciseExposureKnowledgedate.TimeOfDay.TotalSeconds >= LAST_SECOND) Then
            ExposureRow.expKnowledgeDateWholeday = (-1)
          Else
            ExposureRow.expKnowledgeDateWholeday = 0
          End If

          ExposureRow.expSystemKnowledgeDate = MainForm.Main_Knowledgedate
          If (MainForm.Main_Knowledgedate = KNOWLEDGEDATE_NOW) OrElse (MainForm.Main_Knowledgedate.TimeOfDay.TotalSeconds >= LAST_SECOND) Then
            ExposureRow.expSystemKnowledgeDateWholeday = (-1)
          Else
            ExposureRow.expSystemKnowledgeDateWholeday = 0
          End If

          ExposureRow.expFlags = ExposureFlags

          NewExposureRecords.Add(ExposureRow)

        End If

      Catch ex As Exception
        MainForm.LogError("ProcessInstrumentImpactLimits", LOG_LEVELS.Error, ex.Message, "Error calculating Instrument Impact Limits.", ex.StackTrace, True)
        Return False
      End Try
    Next

    ' Now Add the specific exposures to the Exposures table, then add those general exposures which 
    ' are not covered by a specific limit.

    Dim InnerCount As Integer
    Dim OuterCount As Integer
    Dim CheckExposureRow As RenaissanceDataClass.DSExposure.tblExposureRow
    Dim FoundFlag As Boolean

    For OuterCount = 0 To (NewExposureRecords.Count - 1)
      Try
        ExposureRow = NewExposureRecords(OuterCount)

        If ExposureRow.expLimitType = CInt(RenaissanceGlobals.LimitType.SpecificInstrumentCapitalUsage) Then
          If ExposureRow.expUsage >= RISK_MINIMUMEXPOSURETOSAVE Then
            ' Specific Limit, Add to table
            tblExposure.Rows.Add(ExposureRow)
          End If

        Else
          ' General Limit, Only add if a specific limit does not exist for this Instrument
          FoundFlag = False

          For InnerCount = 0 To (NewExposureRecords.Count - 1)
            ' Check for a specific Exposure for the current Instrument
            ' Exit without saving this record if one exists
            CheckExposureRow = NewExposureRecords(InnerCount)

            If (CheckExposureRow.expLimitType = CInt(RenaissanceGlobals.LimitType.SpecificInstrumentCapitalUsage)) AndAlso (CheckExposureRow.expLimitInstrument = ExposureRow.expLimitInstrument) Then
              FoundFlag = True
              Exit For
            End If
          Next

          If FoundFlag = False Then
            If ExposureRow.expUsage >= RISK_MINIMUMEXPOSURETOSAVE Then
              ' no specific limit, add general limit
              tblExposure.Rows.Add(ExposureRow)
            End If
          End If
        End If
      Catch ex As Exception
        MainForm.LogError("ProcessInstrumentImpactLimits", LOG_LEVELS.Error, ex.Message, "Error saving Instrument Impact Limits.", ex.StackTrace, True)
        Return False
      End Try
    Next

    ' Save New Exposure Records.
    Try
      If tblExposure.Rows.Count > 0 Then
        MainForm.AdaptorUpdate("RiskExposureFunctions", AdpExposure, tblExposure)
      End If
    Catch ex As Exception
      MainForm.LogError("ProcessInstrumentImpactLimits", LOG_LEVELS.Error, ex.Message, "Error updating Instrument Impact Limits.", ex.StackTrace, True)
      Return False
    End Try

    Return True

  End Function


  ''' <summary>
  ''' Processes the sector impact limits.
  ''' </summary>
  ''' <param name="MainForm">The main form.</param>
  ''' <param name="tblInstrumentExposures">The TBL instrument exposures.</param>
  ''' <param name="FundId">The fund id.</param>
  ''' <param name="Fund_Value">The fund_ value.</param>
  ''' <param name="pDateToPertrac">The p date to pertrac.</param>
  ''' <param name="pDateToRisk">The p date to risk.</param>
  ''' <param name="CalculationDate">The calculation date.</param>
  ''' <param name="pPreciseExposureKnowledgedate">The p precise exposure knowledgedate.</param>
  ''' <param name="ExposureFlags">The exposure flags.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function ProcessSectorImpactLimits(ByRef MainForm As VeniceMain, ByRef tblInstrumentExposures As DataTable, ByVal FundId As Integer, ByVal Fund_Value As Double, ByVal pDateToPertrac As Date, ByVal pDateToRisk As Date, ByVal CalculationDate As Date, ByVal pPreciseExposureKnowledgedate As Date, ByVal ExposureFlags As Integer) As Boolean

    ' Exit if no Exposure Data
    Try
      If (tblInstrumentExposures Is Nothing) OrElse (tblInstrumentExposures.Rows.Count <= 0) Then
        Return True
      End If
    Catch ex As Exception
      Return False
    End Try

    ' 
    Dim SelectedInstrumentExposures As DataRow()
    Dim thisInstrumentExposure As DataRow
    Dim nextInstrumentExposure As DataRow
    Dim Counter As Integer
    Dim NewExposureRecords As New ArrayList
    Dim ThisInstrumentValue As Double


    Dim DSExposure As New RenaissanceDataClass.DSExposure
    Dim tblExposure As RenaissanceDataClass.DSExposure.tblExposureDataTable
    Dim ExposureRow As RenaissanceDataClass.DSExposure.tblExposureRow
    Dim ExposureDetailRow As RenaissanceDataClass.DSExposure.tblExposureRow
    Dim AdpExposure As New SqlDataAdapter

    Try
      tblExposure = DSExposure.tblExposure
    Catch ex As Exception
      MainForm.LogError("ProcessSectorImpactLimits", LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
      Return False
    End Try

    Dim RISK_TransactionFund As Integer
    Dim RISK_TransactionInstrument As Integer
    Dim RISK_InstrumentFundType As Integer
    Dim RISK_FundTypeDescription As Integer
    Dim RISK_InstrumentValue As Integer
    Dim RISK_InstrumentWorstCase As Integer
    Dim RISK_limType As Integer
    Dim RISK_limitID As Integer
    Dim RISK_limGreaterOrLessThan As Integer
    Dim RISK_limLimit As Integer

    Dim RISK_limOnPremium As Integer
    Dim RISK_InstrumentHasDelta As Integer
    Dim RISK_InstrumentDelta As Integer
    Dim RISK_InstrumentContractSize As Integer
    Dim RISK_InstrumentMultiplier As Integer
    Dim RISK_InstrumentUnderlyingPrice As Integer
    Dim RISK_InstrumentSharesPerOption As Integer
    Dim RISK_FXRate2 As Integer
    Dim RISK_FinalUnits As Integer

    Dim RISK_Weight As Double
    Dim RISK_Impact As Double
    Dim RISK_Usage As Double
    Dim RISK_Status As String = ""
    Dim RISK_SumOfWeight As Double
    Dim RISK_SumOfImpact As Double
    Dim RISK_SumOfUsage As Double


    ' Establish Ordinals
    Try
      RISK_TransactionFund = tblInstrumentExposures.Columns.IndexOf("Fund")
      RISK_limitID = tblInstrumentExposures.Columns.IndexOf("limID")
      RISK_TransactionInstrument = tblInstrumentExposures.Columns.IndexOf("Instrument")
      RISK_InstrumentFundType = tblInstrumentExposures.Columns.IndexOf("InstrumentFundType")
      RISK_FundTypeDescription = tblInstrumentExposures.Columns.IndexOf("FundTypeDescription")
      RISK_InstrumentValue = tblInstrumentExposures.Columns.IndexOf("Value2")
      RISK_InstrumentWorstCase = tblInstrumentExposures.Columns.IndexOf("InstrumentWorstCase")
      RISK_limType = tblInstrumentExposures.Columns.IndexOf("limType")
      RISK_limGreaterOrLessThan = tblInstrumentExposures.Columns.IndexOf("limGreaterOrLessThan")
      RISK_limLimit = tblInstrumentExposures.Columns.IndexOf("limLimit")

      RISK_limOnPremium = tblInstrumentExposures.Columns.IndexOf("limOnPremium")
      RISK_InstrumentHasDelta = tblInstrumentExposures.Columns.IndexOf("InstrumentTypeHasDelta")
      RISK_InstrumentDelta = tblInstrumentExposures.Columns.IndexOf("BestDelta")
      RISK_InstrumentContractSize = tblInstrumentExposures.Columns.IndexOf("InstrumentContractSize")
      RISK_InstrumentMultiplier = tblInstrumentExposures.Columns.IndexOf("InstrumentMultiplier")
      RISK_InstrumentUnderlyingPrice = tblInstrumentExposures.Columns.IndexOf("UnderlyingPrice")
      RISK_InstrumentSharesPerOption = tblInstrumentExposures.Columns.IndexOf("InstrumentSharesPerOption")
      RISK_FXRate2 = tblInstrumentExposures.Columns.IndexOf("FXRate2") ' Compound FX Rate Instrument to Fund FX
      RISK_FinalUnits = tblInstrumentExposures.Columns.IndexOf("FinalUnits") ' SignedUnits from 2nd PnL Valuation query.

    Catch ex As Exception
      MainForm.LogError("ProcessSectorImpactLimits", LOG_LEVELS.Error, ex.Message, "Error getting Field Ordinals from tblInstrumentExposures", ex.StackTrace, True)
      Return False
    End Try

    Try
      MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION), AdpExposure, "tblExposure")
    Catch ex As Exception
      MainForm.LogError("ProcessSectorImpactLimits", LOG_LEVELS.Error, ex.Message, "Error configuring tblExposure Adaptor.", ex.StackTrace, True)
      Return False
    End Try

    ' Select appropriate Exposure Records
    Try
      SelectedInstrumentExposures = tblInstrumentExposures.Select("((limType = " & CInt(RenaissanceGlobals.LimitType.SectorCapitalUsage).ToString & ") OR ((limType = " & CInt(RenaissanceGlobals.LimitType.SpecificSectorCapitalUsage).ToString & ") AND (limSector = InstrumentFundType)))", "Fund, limType, limGreaterOrLessThan, limLimit, InstrumentFundType")

      If (SelectedInstrumentExposures Is Nothing) OrElse (SelectedInstrumentExposures.Length <= 0) Then
        Return True
      End If
    Catch ex As Exception
      MainForm.LogError("ProcessSectorImpactLimits", LOG_LEVELS.Error, ex.Message, "Error selecting tblInstrumentExposures rows.", ex.StackTrace, True)
      Return False
    End Try


    ' Process selected Exposures
    ' 
    ' Calculate Calculated Fields :  Instrument Weight, Impact, Usage and Status
    '           Weight = [InstrumentValue] / [Fund Value]
    '           Impact = [Weight] * [WorstCase] * 10000    (Note : * 10000 to convert to bps)
    '           Usage  = [Impact] / [Limit]                (or inverse depending on Limit definition)
    '           Status = "OK", "Warning" or "Break"

    RISK_SumOfWeight = 0
    RISK_SumOfImpact = 0

    For Counter = 0 To (SelectedInstrumentExposures.Length - 1)
      Try
        thisInstrumentExposure = SelectedInstrumentExposures(Counter)

        ThisInstrumentValue = CDbl(thisInstrumentExposure(RISK_InstrumentValue))

        If (CBool(thisInstrumentExposure(RISK_limOnPremium)) = False) AndAlso CBool(thisInstrumentExposure(RISK_InstrumentHasDelta)) Then
          ThisInstrumentValue = CDbl(thisInstrumentExposure(RISK_FinalUnits)) * CDbl(thisInstrumentExposure(RISK_InstrumentDelta)) * CDbl(thisInstrumentExposure(RISK_InstrumentContractSize)) * CDbl(thisInstrumentExposure(RISK_InstrumentMultiplier)) * CDbl(thisInstrumentExposure(RISK_InstrumentSharesPerOption)) * CDbl(thisInstrumentExposure(RISK_InstrumentUnderlyingPrice)) * CDbl(thisInstrumentExposure(RISK_FXRate2))
        End If

        ExposureRow = tblExposure.NewtblExposureRow

        If Fund_Value = 0 Then
          RISK_Weight = 1.0#
        Else
          RISK_Weight = ThisInstrumentValue / Fund_Value
        End If

        RISK_Impact = RISK_Weight * (CDbl(thisInstrumentExposure(RISK_InstrumentWorstCase)) * 10000.0#)

        ' Roll Up Sector Totals
        RISK_SumOfWeight += RISK_Weight
        RISK_SumOfImpact += RISK_Impact

        If CInt(thisInstrumentExposure(RISK_limGreaterOrLessThan)) = RenaissanceGlobals.Relationship.LessThan Then
          RISK_SumOfUsage = RISK_SumOfImpact / CDbl(thisInstrumentExposure(RISK_limLimit))
          RISK_Usage = RISK_Impact / CDbl(thisInstrumentExposure(RISK_limLimit))
        Else
          If (RISK_SumOfImpact <> 0.0#) Then
            RISK_SumOfUsage = CDbl(thisInstrumentExposure(RISK_limLimit)) / RISK_SumOfImpact
          End If

          If (RISK_Impact <> 0.0#) Then
            RISK_Usage = CDbl(thisInstrumentExposure(RISK_limLimit)) / RISK_Impact
          End If
        End If

        If (RISK_SumOfUsage >= 1) Then
          RISK_Status = "Break"
        ElseIf (RISK_SumOfUsage >= 0.75) Then
          RISK_Status = "Warning"
        Else
          RISK_Status = "OK"
        End If

        ExposureRow.expFund = FundId
        ExposureRow.expLimitID = CInt(thisInstrumentExposure(RISK_limitID))
        ExposureRow.expIsDetailRow = 0
        ExposureRow.expLimitSector = CInt(thisInstrumentExposure(RISK_InstrumentFundType))
        ExposureRow.expLimitSectorName = CStr(thisInstrumentExposure(RISK_FundTypeDescription))
        ExposureRow.expLimitInstrument = 0
        ExposureRow.expLimitType = CInt(thisInstrumentExposure(RISK_limType))
        ExposureRow.expLimitGreaterOrLessThan = CInt(thisInstrumentExposure(RISK_limGreaterOrLessThan))
        ExposureRow.expLimit = CDbl(thisInstrumentExposure(RISK_limLimit))
        ExposureRow.expExposure = RISK_SumOfImpact
        ExposureRow.expUsage = RISK_SumOfUsage
        ExposureRow.expStatus = RISK_Status
        ExposureRow.expDate = pDateToRisk
        ExposureRow.expDateCalculated = CalculationDate

        ExposureRow.expKnowledgeDate = pPreciseExposureKnowledgedate
        If (pPreciseExposureKnowledgedate = KNOWLEDGEDATE_NOW) OrElse (pPreciseExposureKnowledgedate.TimeOfDay.TotalSeconds >= LAST_SECOND) Then
          ExposureRow.expKnowledgeDateWholeday = (-1)
        Else
          ExposureRow.expKnowledgeDateWholeday = 0
        End If

        ExposureRow.expSystemKnowledgeDate = MainForm.Main_Knowledgedate
        If (MainForm.Main_Knowledgedate = KNOWLEDGEDATE_NOW) OrElse (MainForm.Main_Knowledgedate.TimeOfDay.TotalSeconds >= LAST_SECOND) Then
          ExposureRow.expSystemKnowledgeDateWholeday = (-1)
        Else
          ExposureRow.expSystemKnowledgeDateWholeday = 0
        End If

        ExposureRow.expFlags = ExposureFlags

        ' Detail Row.

        If (Math.Abs(RISK_Weight) > 0) Then

          ExposureDetailRow = tblExposure.NewtblExposureRow

          ExposureDetailRow.expFund = ExposureRow.expFund
          ExposureDetailRow.expLimitID = CInt(thisInstrumentExposure(RISK_limitID))
          ExposureDetailRow.expIsDetailRow = 1
          ExposureDetailRow.expLimitSector = ExposureRow.expLimitSector
          ExposureDetailRow.expLimitSectorName = ExposureRow.expLimitSectorName
          ExposureDetailRow.expLimitInstrument = ExposureRow.expLimitInstrument
          ExposureDetailRow.expLimitType = ExposureRow.expLimitType
          ExposureDetailRow.expLimitCharacteristic = ExposureRow.expLimitCharacteristic
          ExposureDetailRow.expLimitGreaterOrLessThan = ExposureRow.expLimitGreaterOrLessThan
          ExposureDetailRow.expLimit = ExposureRow.expLimit
          ExposureDetailRow.expDetailInstrument = CInt(thisInstrumentExposure(RISK_TransactionInstrument))
          ExposureDetailRow.expExposure = RISK_Weight
          ExposureDetailRow.expUsage = RISK_Usage
          ExposureDetailRow.expStatus = RISK_Status
          ExposureDetailRow.expDate = ExposureRow.expDate
          ExposureDetailRow.expDateCalculated = ExposureRow.expDateCalculated
          ExposureDetailRow.expKnowledgeDate = ExposureRow.expKnowledgeDate
          ExposureDetailRow.expKnowledgeDateWholeday = ExposureRow.expKnowledgeDateWholeday
          ExposureDetailRow.expSystemKnowledgeDate = ExposureRow.expSystemKnowledgeDate
          ExposureDetailRow.expSystemKnowledgeDateWholeday = ExposureRow.expSystemKnowledgeDateWholeday
          ExposureDetailRow.expFlags = ExposureRow.expFlags

          ' Save Detail Row

          If (Not (ExposureDetailRow Is Nothing)) Then
            NewExposureRecords.Add(ExposureDetailRow)
            ExposureDetailRow = Nothing
          End If

        End If

        ' Save This Row ?
        ' Save if this is the last Exposure Row OR 
        ' If we have reached the end of a section (Fund, Limit Type, Limit or Condition has changed)

        If (Counter >= (SelectedInstrumentExposures.Length - 1)) Then
          NewExposureRecords.Add(ExposureRow)

          RISK_SumOfWeight = 0.0#
          RISK_SumOfImpact = 0.0#
        Else
          nextInstrumentExposure = SelectedInstrumentExposures(Counter + 1)

          If (CInt(thisInstrumentExposure(RISK_TransactionFund)) <> CInt(nextInstrumentExposure(RISK_TransactionFund))) OrElse _
           (CInt(thisInstrumentExposure(RISK_limType)) <> CInt(nextInstrumentExposure(RISK_limType))) OrElse _
           (CInt(thisInstrumentExposure(RISK_limGreaterOrLessThan)) <> CInt(nextInstrumentExposure(RISK_limGreaterOrLessThan))) OrElse _
           (CDbl(thisInstrumentExposure(RISK_limLimit)) <> CDbl(nextInstrumentExposure(RISK_limLimit))) OrElse _
           (CInt(thisInstrumentExposure(RISK_InstrumentFundType)) <> CInt(nextInstrumentExposure(RISK_InstrumentFundType))) Then
            NewExposureRecords.Add(ExposureRow)

            RISK_SumOfWeight = 0.0#
            RISK_SumOfImpact = 0.0#
          End If
        End If

      Catch ex As Exception
        MainForm.LogError("ProcessSectorImpactLimits", LOG_LEVELS.Error, ex.Message, "Error calculating Sector Impact Limits.", ex.StackTrace, True)
        Return False
      End Try
    Next

    ' Now Add the specific exposures to the Exposures table, then add those general exposures which 
    ' are not covered by a specific limit.

    Dim InnerCount As Integer
    Dim OuterCount As Integer
    Dim CheckExposureRow As RenaissanceDataClass.DSExposure.tblExposureRow
    Dim FoundFlag As Boolean

    For OuterCount = 0 To (NewExposureRecords.Count - 1)
      Try
        ExposureRow = NewExposureRecords(OuterCount)

        If ExposureRow.expLimitType = CInt(RenaissanceGlobals.LimitType.SpecificSectorCapitalUsage) Then
          ' Ignore any Sectors with negligible impact
          If Math.Abs(ExposureRow.expExposure) >= RISK_MINIMUMEXPOSURETOSAVE Then
            ' Specific Limit, Add to table
            tblExposure.Rows.Add(ExposureRow)
          End If
        Else
          ' General Limit, Only add if a specific limit does not exist for this Instrument
          FoundFlag = False

          For InnerCount = 0 To (NewExposureRecords.Count - 1)
            ' Check for a specific Exposure for the current Instrument
            ' Exit without saving this record if one exists
            CheckExposureRow = NewExposureRecords(InnerCount)

            If (CheckExposureRow.expLimitType = CInt(RenaissanceGlobals.LimitType.SpecificSectorCapitalUsage)) AndAlso (CheckExposureRow.expLimitSector = ExposureRow.expLimitSector) Then
              FoundFlag = True
              Exit For
            End If
          Next

          If FoundFlag = False Then
            ' Ignore any Sectors with negligible impact
            If Math.Abs(ExposureRow.expExposure) >= RISK_MINIMUMEXPOSURETOSAVE Then
              ' Specific Limit, Add to table
              tblExposure.Rows.Add(ExposureRow)
            End If
          End If
        End If
      Catch ex As Exception
        MainForm.LogError("ProcessSectorImpactLimits", LOG_LEVELS.Error, ex.Message, "Error saving Sector Capital Usage Limits.", ex.StackTrace, True)
        Return False
      End Try
    Next

    ' Save New Exposure Records.
    Try
      If tblExposure.Rows.Count > 0 Then
        MainForm.AdaptorUpdate("RiskExposureFunctions", AdpExposure, tblExposure)
      End If
    Catch ex As Exception
      MainForm.LogError("ProcessSectorImpactLimits", LOG_LEVELS.Error, ex.Message, "Error updating Sector Capital Usage Limits.", ex.StackTrace, True)
      Return False
    End Try

    Return True

  End Function


  ''' <summary>
  ''' Processes the total impact limits.
  ''' </summary>
  ''' <param name="MainForm">The main form.</param>
  ''' <param name="tblInstrumentExposures">The TBL instrument exposures.</param>
  ''' <param name="FundId">The fund id.</param>
  ''' <param name="Fund_Value">The fund_ value.</param>
  ''' <param name="pDateToPertrac">The p date to pertrac.</param>
  ''' <param name="pDateToRisk">The p date to risk.</param>
  ''' <param name="CalculationDate">The calculation date.</param>
  ''' <param name="pPreciseExposureKnowledgedate">The p precise exposure knowledgedate.</param>
  ''' <param name="ExposureFlags">The exposure flags.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function ProcessTotalImpactLimits(ByRef MainForm As VeniceMain, ByRef tblInstrumentExposures As DataTable, ByVal FundId As Integer, ByVal Fund_Value As Double, ByVal pDateToPertrac As Date, ByVal pDateToRisk As Date, ByVal CalculationDate As Date, ByVal pPreciseExposureKnowledgedate As Date, ByVal ExposureFlags As Integer) As Boolean

    ' Exit if no Exposure Data
    Try
      If (tblInstrumentExposures Is Nothing) OrElse (tblInstrumentExposures.Rows.Count <= 0) Then
        Return True
      End If
    Catch ex As Exception
      Return False
    End Try

    ' 
    Dim SelectedInstrumentExposures As DataRow()
    Dim thisInstrumentExposure As DataRow
    Dim nextInstrumentExposure As DataRow
    Dim Counter As Integer
    Dim NewExposureRecords As New ArrayList


    Dim DSExposure As New RenaissanceDataClass.DSExposure
    Dim tblExposure As RenaissanceDataClass.DSExposure.tblExposureDataTable
    Dim ExposureRow As RenaissanceDataClass.DSExposure.tblExposureRow
    Dim ExposureDetailRow As RenaissanceDataClass.DSExposure.tblExposureRow
    Dim AdpExposure As New SqlDataAdapter
    Dim ThisInstrumentValue As Double

    Try
      tblExposure = DSExposure.tblExposure
    Catch ex As Exception
      MainForm.LogError("ProcessTotalImpactLimits", LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
      Return False
    End Try

    Dim RISK_TransactionFund As Integer
    Dim RISK_TransactionInstrument As Integer
    Dim RISK_InstrumentFundType As Integer
    Dim RISK_FundTypeDescription As Integer
    Dim RISK_InstrumentValue As Integer
    Dim RISK_InstrumentWorstCase As Integer
    Dim RISK_limType As Integer
    Dim RISK_limitID As Integer
    Dim RISK_limGreaterOrLessThan As Integer
    Dim RISK_limLimit As Integer

    Dim RISK_limOnPremium As Integer
    Dim RISK_InstrumentHasDelta As Integer
    Dim RISK_InstrumentDelta As Integer
    Dim RISK_InstrumentContractSize As Integer
    Dim RISK_InstrumentMultiplier As Integer
    Dim RISK_InstrumentUnderlyingPrice As Integer
    Dim RISK_InstrumentSharesPerOption As Integer
    Dim RISK_FXRate2 As Integer
    Dim RISK_FinalUnits As Integer

    Dim RISK_Weight As Double
    Dim RISK_Impact As Double
    Dim RISK_Usage As Double
    Dim RISK_Status As String = ""
    Dim RISK_SumOfWeight As Double
    Dim RISK_SumOfImpact As Double
    Dim RISK_SumOfUsage As Double


    ' Establish Ordinals
    Try
      RISK_TransactionFund = tblInstrumentExposures.Columns.IndexOf("Fund")
      RISK_limitID = tblInstrumentExposures.Columns.IndexOf("limID")
      RISK_TransactionInstrument = tblInstrumentExposures.Columns.IndexOf("Instrument")
      RISK_InstrumentFundType = tblInstrumentExposures.Columns.IndexOf("InstrumentFundType")
      RISK_FundTypeDescription = tblInstrumentExposures.Columns.IndexOf("FundTypeDescription")
      RISK_InstrumentValue = tblInstrumentExposures.Columns.IndexOf("Value2")
      RISK_InstrumentWorstCase = tblInstrumentExposures.Columns.IndexOf("InstrumentWorstCase")
      RISK_limType = tblInstrumentExposures.Columns.IndexOf("limType")
      RISK_limGreaterOrLessThan = tblInstrumentExposures.Columns.IndexOf("limGreaterOrLessThan")
      RISK_limLimit = tblInstrumentExposures.Columns.IndexOf("limLimit")

      RISK_limOnPremium = tblInstrumentExposures.Columns.IndexOf("limOnPremium")
      RISK_InstrumentHasDelta = tblInstrumentExposures.Columns.IndexOf("InstrumentTypeHasDelta")
      RISK_InstrumentDelta = tblInstrumentExposures.Columns.IndexOf("BestDelta")
      RISK_InstrumentContractSize = tblInstrumentExposures.Columns.IndexOf("InstrumentContractSize")
      RISK_InstrumentMultiplier = tblInstrumentExposures.Columns.IndexOf("InstrumentMultiplier")
      RISK_InstrumentUnderlyingPrice = tblInstrumentExposures.Columns.IndexOf("UnderlyingPrice")
      RISK_InstrumentSharesPerOption = tblInstrumentExposures.Columns.IndexOf("InstrumentSharesPerOption")
      RISK_FXRate2 = tblInstrumentExposures.Columns.IndexOf("FXRate2") ' Compound FX Rate Instrument to Fund FX
      RISK_FinalUnits = tblInstrumentExposures.Columns.IndexOf("FinalUnits") ' SignedUnits from 2nd PnL Valuation query.

    Catch ex As Exception
      MainForm.LogError("ProcessTotalImpactLimits", LOG_LEVELS.Error, ex.Message, "Error getting Field Ordinals from tblInstrumentExposures", ex.StackTrace, True)
      Return False
    End Try

    Try
      MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION), AdpExposure, "tblExposure")
    Catch ex As Exception
      MainForm.LogError("ProcessTotalImpactLimits", LOG_LEVELS.Error, ex.Message, "Error configuring tblExposure Adaptor.", ex.StackTrace, True)
      Return False
    End Try

    ' Select appropriate Exposure Records
    Try
      SelectedInstrumentExposures = tblInstrumentExposures.Select("limType = " & CInt(RenaissanceGlobals.LimitType.TotalCapitalUsage).ToString, "Fund, limType, limGreaterOrLessThan, limLimit")

      If (SelectedInstrumentExposures Is Nothing) OrElse (SelectedInstrumentExposures.Length <= 0) Then
        Return True
      End If
    Catch ex As Exception
      MainForm.LogError("ProcessTotalImpactLimits", LOG_LEVELS.Error, ex.Message, "Error selecting tblInstrumentExposures rows.", ex.StackTrace, True)
      Return False
    End Try


    ' Process selected Exposures
    ' 
    ' Calculate Calculated Fields :  Instrument Weight, Impact, Usage and Status
    '           Weight = [InstrumentValue] / [Fund Value]
    '           Impact = [Weight] * [WorstCase] * 10000    (Note : * 10000 to convert to bps)
    '           Usage  = [Impact] / [Limit]                (or inverse depending on Limit definition)
    '           Status = "OK", "Warning" or "Break"

    RISK_SumOfWeight = 0
    RISK_SumOfImpact = 0

    For Counter = 0 To (SelectedInstrumentExposures.Length - 1)
      Try
        thisInstrumentExposure = SelectedInstrumentExposures(Counter)

        ThisInstrumentValue = CDbl(thisInstrumentExposure(RISK_InstrumentValue))

        If (CBool(thisInstrumentExposure(RISK_limOnPremium)) = False) AndAlso CBool(thisInstrumentExposure(RISK_InstrumentHasDelta)) Then
          ThisInstrumentValue = CDbl(thisInstrumentExposure(RISK_FinalUnits)) * CDbl(thisInstrumentExposure(RISK_InstrumentDelta)) * CDbl(thisInstrumentExposure(RISK_InstrumentContractSize)) * CDbl(thisInstrumentExposure(RISK_InstrumentMultiplier)) * CDbl(thisInstrumentExposure(RISK_InstrumentSharesPerOption)) * CDbl(thisInstrumentExposure(RISK_InstrumentUnderlyingPrice)) * CDbl(thisInstrumentExposure(RISK_FXRate2))
        End If

        ExposureRow = tblExposure.NewtblExposureRow

        If Fund_Value = 0 Then
          RISK_Weight = 1
        Else
          RISK_Weight = ThisInstrumentValue / Fund_Value
        End If

        RISK_Impact = RISK_Weight * (CDbl(thisInstrumentExposure(RISK_InstrumentWorstCase)) * 10000)

        ' Roll Up Sector Totals
        RISK_SumOfWeight += RISK_Weight
        RISK_SumOfImpact += RISK_Impact

        If CInt(thisInstrumentExposure(RISK_limGreaterOrLessThan)) = RenaissanceGlobals.Relationship.LessThan Then
          RISK_SumOfUsage = RISK_SumOfImpact / CDbl(thisInstrumentExposure(RISK_limLimit))
          RISK_Usage = RISK_Impact / CDbl(thisInstrumentExposure(RISK_limLimit))
        Else
          If (RISK_SumOfImpact <> 0) Then
            RISK_SumOfUsage = CDbl(thisInstrumentExposure(RISK_limLimit)) / RISK_SumOfImpact
          End If
          If (RISK_Impact <> 0) Then
            RISK_Usage = CDbl(thisInstrumentExposure(RISK_limLimit)) / RISK_Impact
          End If
        End If

        If (RISK_SumOfUsage >= 1) Then
          RISK_Status = "Break"
        ElseIf (RISK_SumOfUsage >= 0.75) Then
          RISK_Status = "Warning"
        Else
          RISK_Status = "OK"
        End If

        ExposureRow.expFund = FundId
        ExposureRow.expLimitID = CInt(thisInstrumentExposure(RISK_limitID))
        ExposureRow.expIsDetailRow = 0
        ExposureRow.expLimitSector = 0
        ExposureRow.expLimitSectorName = ""
        ExposureRow.expLimitInstrument = 0
        ExposureRow.expLimitType = CInt(thisInstrumentExposure(RISK_limType))
        ExposureRow.expLimitGreaterOrLessThan = CInt(thisInstrumentExposure(RISK_limGreaterOrLessThan))
        ExposureRow.expLimit = CDbl(thisInstrumentExposure(RISK_limLimit))
        ExposureRow.expExposure = RISK_SumOfImpact
        ExposureRow.expUsage = RISK_SumOfUsage
        ExposureRow.expStatus = RISK_Status
        ExposureRow.expDate = pDateToRisk
        ExposureRow.expDateCalculated = CalculationDate

        ExposureRow.expKnowledgeDate = pPreciseExposureKnowledgedate
        If (pPreciseExposureKnowledgedate = KNOWLEDGEDATE_NOW) OrElse (pPreciseExposureKnowledgedate.TimeOfDay.TotalSeconds >= LAST_SECOND) Then
          ExposureRow.expKnowledgeDateWholeday = (-1)
        Else
          ExposureRow.expKnowledgeDateWholeday = 0
        End If

        ExposureRow.expSystemKnowledgeDate = MainForm.Main_Knowledgedate
        If (MainForm.Main_Knowledgedate = KNOWLEDGEDATE_NOW) OrElse (MainForm.Main_Knowledgedate.TimeOfDay.TotalSeconds >= LAST_SECOND) Then
          ExposureRow.expSystemKnowledgeDateWholeday = (-1)
        Else
          ExposureRow.expSystemKnowledgeDateWholeday = 0
        End If

        ExposureRow.expFlags = ExposureFlags

        ' Detail Row.

        If (Math.Abs(RISK_Weight) > 0) Then

          ExposureDetailRow = tblExposure.NewtblExposureRow

          ExposureDetailRow.expFund = ExposureRow.expFund
          ExposureDetailRow.expLimitID = CInt(thisInstrumentExposure(RISK_limitID))
          ExposureDetailRow.expIsDetailRow = 1
          ExposureDetailRow.expLimitSector = ExposureRow.expLimitSector
          ExposureDetailRow.expLimitSectorName = ExposureRow.expLimitSectorName
          ExposureDetailRow.expLimitInstrument = ExposureRow.expLimitInstrument
          ExposureDetailRow.expLimitType = ExposureRow.expLimitType
          ExposureDetailRow.expLimitCharacteristic = ExposureRow.expLimitCharacteristic
          ExposureDetailRow.expLimitGreaterOrLessThan = ExposureRow.expLimitGreaterOrLessThan
          ExposureDetailRow.expLimit = ExposureRow.expLimit
          ExposureDetailRow.expDetailInstrument = CInt(thisInstrumentExposure(RISK_TransactionInstrument))
          ExposureDetailRow.expExposure = RISK_Weight
          ExposureDetailRow.expUsage = RISK_Usage
          ExposureDetailRow.expStatus = RISK_Status
          ExposureDetailRow.expDate = ExposureRow.expDate
          ExposureDetailRow.expDateCalculated = ExposureRow.expDateCalculated
          ExposureDetailRow.expKnowledgeDate = ExposureRow.expKnowledgeDate
          ExposureDetailRow.expKnowledgeDateWholeday = ExposureRow.expKnowledgeDateWholeday
          ExposureDetailRow.expSystemKnowledgeDate = ExposureRow.expSystemKnowledgeDate
          ExposureDetailRow.expSystemKnowledgeDateWholeday = ExposureRow.expSystemKnowledgeDateWholeday
          ExposureDetailRow.expFlags = ExposureRow.expFlags

          ' Save Detail Row

          If (Not (ExposureDetailRow Is Nothing)) Then
            NewExposureRecords.Add(ExposureDetailRow)
            ExposureDetailRow = Nothing
          End If

        End If

        ' Save This Row ?
        ' Save if this is the last Exposure Row OR 
        ' If we have reached the end of a section (Fund, Limit Type, Limit or Condition has changed)

        If (Counter >= (SelectedInstrumentExposures.Length - 1)) Then
          NewExposureRecords.Add(ExposureRow)

          RISK_SumOfWeight = 0
          RISK_SumOfImpact = 0
        Else
          nextInstrumentExposure = SelectedInstrumentExposures(Counter + 1)

          If (CInt(thisInstrumentExposure(RISK_TransactionFund)) <> CInt(nextInstrumentExposure(RISK_TransactionFund))) OrElse _
           (CInt(thisInstrumentExposure(RISK_limType)) <> CInt(nextInstrumentExposure(RISK_limType))) OrElse _
           (CInt(thisInstrumentExposure(RISK_limGreaterOrLessThan)) <> CInt(nextInstrumentExposure(RISK_limGreaterOrLessThan))) OrElse _
           (CDbl(thisInstrumentExposure(RISK_limLimit)) <> CDbl(nextInstrumentExposure(RISK_limLimit))) Then
            NewExposureRecords.Add(ExposureRow)

            RISK_SumOfWeight = 0
            RISK_SumOfImpact = 0
          End If
        End If

      Catch ex As Exception
        MainForm.LogError("ProcessTotalImpactLimits", LOG_LEVELS.Error, ex.Message, "Error calculating Total Impact Limits.", ex.StackTrace, True)
        Return False
      End Try
    Next

    ' Now Add the specific exposures to the Exposures table, then add those general exposures which 
    ' are not covered by a specific limit.

    Dim OuterCount As Integer

    For OuterCount = 0 To (NewExposureRecords.Count - 1)
      Try
        ExposureRow = NewExposureRecords(OuterCount)

        If ExposureRow.expLimitType = CInt(RenaissanceGlobals.LimitType.TotalCapitalUsage) Then
          ' Ignore any Sectors with negligible impact
          If Math.Abs(ExposureRow.expExposure) >= RISK_MINIMUMEXPOSURETOSAVE Then
            ' Specific Limit, Add to table
            tblExposure.Rows.Add(ExposureRow)
          End If
        End If
      Catch ex As Exception
        MainForm.LogError("ProcessTotalImpactLimits", LOG_LEVELS.Error, ex.Message, "Error saving Total Capital Usage Limits.", ex.StackTrace, True)
        Return False
      End Try
    Next

    ' Save New Exposure Records.
    Try
      If tblExposure.Rows.Count > 0 Then
        MainForm.AdaptorUpdate("RiskExposureFunctions", AdpExposure, tblExposure)
      End If
    Catch ex As Exception
      MainForm.LogError("ProcessTotalImpactLimits", LOG_LEVELS.Error, ex.Message, "Error updating Total Capital Usage Limits.", ex.StackTrace, True)
      Return False
    End Try

    Return True

  End Function


  ''' <summary>
  ''' Processes the instrument weight limits.
  ''' </summary>
  ''' <param name="MainForm">The main form.</param>
  ''' <param name="tblInstrumentExposures">The TBL instrument exposures.</param>
  ''' <param name="FundId">The fund id.</param>
  ''' <param name="Fund_Value">The fund_ value.</param>
  ''' <param name="pDateToPertrac">The p date to pertrac.</param>
  ''' <param name="pDateToRisk">The p date to risk.</param>
  ''' <param name="CalculationDate">The calculation date.</param>
  ''' <param name="pPreciseExposureKnowledgedate">The p precise exposure knowledgedate.</param>
  ''' <param name="ExposureFlags">The exposure flags.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function ProcessInstrumentWeightLimits(ByRef MainForm As VeniceMain, ByRef tblInstrumentExposures As DataTable, ByVal FundId As Integer, ByVal Fund_Value As Double, ByVal pDateToPertrac As Date, ByVal pDateToRisk As Date, ByVal CalculationDate As Date, ByVal pPreciseExposureKnowledgedate As Date, ByVal ExposureFlags As Integer) As Boolean
    ' **********************************************************************************************
    '
    '
    ' **********************************************************************************************

    ' Exit if no Exposure Data
    Try
      If (tblInstrumentExposures Is Nothing) OrElse (tblInstrumentExposures.Rows.Count <= 0) Then
        Return True
      End If
    Catch ex As Exception
      Return False
    End Try

    ' 
    Dim SelectedInstrumentExposures As DataRow()
    Dim thisInstrumentExposure As DataRow
    Dim CheckInstrumentExposure As DataRow
    Dim Counter As Integer
    Dim CheckLimitCounter As Integer
    Dim ThisLimitDepricated As Boolean
    Dim NewExposureRecords As New ArrayList
    Dim thisLimitValue As Double
    Dim thisInstrumentValue As Double
    Dim ThisFundID As Integer
    Dim ThisInstrumentID As Integer

    Dim DSExposure As New RenaissanceDataClass.DSExposure
    Dim tblExposure As RenaissanceDataClass.DSExposure.tblExposureDataTable
    Dim ExposureRow As RenaissanceDataClass.DSExposure.tblExposureRow
    Dim AdpExposure As New SqlDataAdapter

    Try
      tblExposure = DSExposure.tblExposure
    Catch ex As Exception
      MainForm.LogError("ProcessInstrumentWeightLimits", LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
      Return False
    End Try

    Dim RISK_TransactionFund As Integer
    Dim RISK_TransactionInstrument As Integer
    Dim RISK_InstrumentFundType As Integer
    Dim RISK_FundTypeDescription As Integer
    Dim RISK_InstrumentValue As Integer
    Dim RISK_InstrumentWorstCase As Integer
    Dim RISK_limType As Integer
    Dim RISK_limInstrument As Integer
    Dim RISK_limitID As Integer
    Dim RISK_limFund As Integer
    Dim RISK_limOnPremium As Integer
    Dim RISK_limGreaterOrLessThan As Integer
    Dim RISK_limLimit As Integer

    Dim RISK_InstrumentHasDelta As Integer
    Dim RISK_InstrumentDelta As Integer
    Dim RISK_InstrumentContractSize As Integer
    Dim RISK_InstrumentMultiplier As Integer
    Dim RISK_InstrumentUnderlyingPrice As Integer
    Dim RISK_InstrumentSharesPerOption As Integer
    Dim RISK_FXRate2 As Integer
    Dim RISK_FinalUnits As Integer

    Dim RISK_Weight As Double
    Dim RISK_Usage As Double
    Dim RISK_Status As String
    ' Dim RISK_SumOfWeight As Double
    ' Dim RISK_SumOfImpact As Double

    Dim RISK_InstrumentType As Integer

    ' Establish Ordinals
    Try
      RISK_TransactionFund = tblInstrumentExposures.Columns.IndexOf("Fund")
      RISK_limitID = tblInstrumentExposures.Columns.IndexOf("limID")
      RISK_limFund = tblInstrumentExposures.Columns.IndexOf("limFund")
      RISK_limOnPremium = tblInstrumentExposures.Columns.IndexOf("limOnPremium")
      RISK_TransactionInstrument = tblInstrumentExposures.Columns.IndexOf("Instrument")
      RISK_InstrumentFundType = tblInstrumentExposures.Columns.IndexOf("InstrumentFundType")
      RISK_FundTypeDescription = tblInstrumentExposures.Columns.IndexOf("FundTypeDescription")
      RISK_InstrumentValue = tblInstrumentExposures.Columns.IndexOf("Value2")
      RISK_InstrumentWorstCase = tblInstrumentExposures.Columns.IndexOf("InstrumentWorstCase")
      RISK_limType = tblInstrumentExposures.Columns.IndexOf("limType")
      RISK_limInstrument = tblInstrumentExposures.Columns.IndexOf("limInstrument")
      RISK_limGreaterOrLessThan = tblInstrumentExposures.Columns.IndexOf("limGreaterOrLessThan")
      RISK_limLimit = tblInstrumentExposures.Columns.IndexOf("limLimit")

      RISK_InstrumentHasDelta = tblInstrumentExposures.Columns.IndexOf("InstrumentTypeHasDelta")
      RISK_InstrumentDelta = tblInstrumentExposures.Columns.IndexOf("BestDelta")
      RISK_InstrumentContractSize = tblInstrumentExposures.Columns.IndexOf("InstrumentContractSize")
      RISK_InstrumentMultiplier = tblInstrumentExposures.Columns.IndexOf("InstrumentMultiplier")
      RISK_InstrumentUnderlyingPrice = tblInstrumentExposures.Columns.IndexOf("UnderlyingPrice")
      RISK_InstrumentSharesPerOption = tblInstrumentExposures.Columns.IndexOf("InstrumentSharesPerOption")
      RISK_FXRate2 = tblInstrumentExposures.Columns.IndexOf("FXRate2") ' Compound FX Rate Instrument to Fund FX
      RISK_FinalUnits = tblInstrumentExposures.Columns.IndexOf("FinalUnits") ' SignedUnits from 2nd PnL Valuation query.

      RISK_InstrumentType = tblInstrumentExposures.Columns.IndexOf("InstrumentType")

    Catch ex As Exception
      MainForm.LogError("ProcessInstrumentWeightLimits", LOG_LEVELS.Error, ex.Message, "Error getting Field Ordinals from tblInstrumentExposures", ex.StackTrace, True)
      Return False
    End Try

    Try
      MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION), AdpExposure, "tblExposure")
    Catch ex As Exception
      MainForm.LogError("ProcessInstrumentWeightLimits", LOG_LEVELS.Error, ex.Message, "Error configuring tblExposure Adaptor.", ex.StackTrace, True)
      Return False
    End Try

    ' Select appropriate Exposure Records
    Try
      SelectedInstrumentExposures = tblInstrumentExposures.Select("((limType = " & CInt(RenaissanceGlobals.LimitType.InstrumentWeight).ToString & ") OR ((limType = " & CInt(RenaissanceGlobals.LimitType.SpecificInstrumentWeight).ToString & ") AND (limInstrument = Instrument)))", "Fund, limType, Instrument")

      If (SelectedInstrumentExposures Is Nothing) OrElse (SelectedInstrumentExposures.Length <= 0) Then
        Return True
      End If
    Catch ex As Exception
      MainForm.LogError("ProcessInstrumentWeightLimits", LOG_LEVELS.Error, ex.Message, "Error selecting tblInstrumentExposures rows.", ex.StackTrace, True)
      Return False
    End Try


    ' Process selected Exposures
    ' 
    ' Calculate Calculated Fields :  Instrument Weight, Impact, Usage and Status
    '           Weight = [InstrumentValue] / [Fund Value]
    '           Impact = [Weight] * [WorstCase] * 10000    (Note : * 10000 to convert to bps)
    '           Usage  = [Weight] / [Limit]                (or inverse depending on Limit definition)
    '           Status = "OK", "Warning" or "Break"

    'RISK_SumOfWeight = 0
    'RISK_SumOfImpact = 0

    For Counter = 0 To (SelectedInstrumentExposures.Length - 1)
      Try
        thisInstrumentExposure = SelectedInstrumentExposures(Counter)
        thisLimitValue = CDbl(thisInstrumentExposure(RISK_limLimit))
        ThisFundID = CInt(thisInstrumentExposure(RISK_TransactionFund))

        'lotfi: exclude options ( instrumentId=8) and futures(InstrumentId=15) from the specific instrument weight limit
        ' they will be controlled by other limits
        ThisInstrumentID = CInt(Nz(thisInstrumentExposure(RISK_InstrumentType), 0))
        If (ThisInstrumentID = 8 Or ThisInstrumentID = 15) Then
          Continue For
        End If


        ' Ignore Small Value Exposures
        ' Ignore Instruments with Fund Type of CASH, LOAD or FEE
        ' "(NOT ((FundTypeDescription = 'Cash') OR (FundTypeDescription = 'LOAN') OR (FundTypeDescription = 'Fee')))"

        thisInstrumentValue = CDbl(thisInstrumentExposure(RISK_InstrumentValue))

        If (CBool(thisInstrumentExposure(RISK_limOnPremium)) = False) AndAlso CBool(thisInstrumentExposure(RISK_InstrumentHasDelta)) Then
          thisInstrumentValue = CDbl(thisInstrumentExposure(RISK_FinalUnits)) * CDbl(thisInstrumentExposure(RISK_InstrumentDelta)) * CDbl(thisInstrumentExposure(RISK_InstrumentContractSize)) * CDbl(thisInstrumentExposure(RISK_InstrumentMultiplier)) * CDbl(thisInstrumentExposure(RISK_InstrumentSharesPerOption)) * CDbl(thisInstrumentExposure(RISK_InstrumentUnderlyingPrice)) * CDbl(thisInstrumentExposure(RISK_FXRate2))
        End If

        If Math.Abs(thisInstrumentValue) >= 1.0# Then
          'If Not ((CInt(thisInstrumentExposure(RISK_InstrumentFundType)) = FundType.Cash) Or _
          ' (CInt(thisInstrumentExposure(RISK_InstrumentFundType)) = FundType.Loan) Or _
          ' (CInt(thisInstrumentExposure(RISK_InstrumentFundType)) = FundType.Fee)) Then

          ' Check for more specific limits.

          If (CInt(thisInstrumentExposure(RISK_limFund))) = 0 Then
            ThisLimitDepricated = False

            For CheckLimitCounter = 0 To (SelectedInstrumentExposures.Length - 1)
              CheckInstrumentExposure = SelectedInstrumentExposures(CheckLimitCounter)

              If (CheckInstrumentExposure(RISK_limFund) = ThisFundID) Then

                If CInt(CheckInstrumentExposure(RISK_limType)) = CInt(thisInstrumentExposure(RISK_limType)) And CInt(CheckInstrumentExposure(RISK_limInstrument)) = CInt(thisInstrumentExposure(RISK_limInstrument)) And CInt(CheckInstrumentExposure(RISK_limType)) = CInt(thisInstrumentExposure(RISK_limType)) Then

                  ThisLimitDepricated = True

                End If

              End If
            Next CheckLimitCounter

            If (ThisLimitDepricated) Then
              Continue For
            End If
          End If

          ExposureRow = tblExposure.NewtblExposureRow

          If Fund_Value = 0 Then
            RISK_Weight = 1
          Else
            RISK_Weight = thisInstrumentValue / Fund_Value
          End If

          If thisLimitValue = 0.0# Then

            If CInt(thisInstrumentExposure(RISK_limGreaterOrLessThan)) = RenaissanceGlobals.Relationship.LessThan Then
              'RISK_Usage = RISK_Weight / CDbl(thisInstrumentExposure(RISK_limLimit))

              RISK_Usage = Math.Min(2.0#, Math.Max(-2.0#, (RISK_Weight / 0.05#) + 1.0#))

              ' It has been requested that Zero Weight for Zero limits represents Zero Exposure NPP 15 May 2013)

              If (RISK_Weight <= 0.0#) Then
                RISK_Usage = 0.0#
              End If

            Else
              'RISK_Usage = CDbl(thisInstrumentExposure(RISK_limLimit)) / RISK_Weight

              RISK_Usage = Math.Min(2.0#, Math.Max(-2.0#, ((-RISK_Weight) / 0.05#) + 1.0#))

              ' It has been requested that Zero Weight for Zero limits represents Zero Exposure NPP 15 May 2013)

              If (RISK_Weight >= 0.0#) Then
                RISK_Usage = 0.0#
              End If

            End If

          Else

            If CInt(thisInstrumentExposure(RISK_limGreaterOrLessThan)) = RenaissanceGlobals.Relationship.LessThan Then
              'RISK_Usage = RISK_Weight / CDbl(thisInstrumentExposure(RISK_limLimit))

              RISK_Usage = Math.Min(2.0#, Math.Max(-2.0#, ((RISK_Weight - thisLimitValue) / Math.Abs(thisLimitValue)) + 1.0#))
            Else
              'RISK_Usage = CDbl(thisInstrumentExposure(RISK_limLimit)) / RISK_Weight

              RISK_Usage = Math.Min(2.0#, Math.Max(-2.0#, ((thisLimitValue - RISK_Weight) / Math.Abs(thisLimitValue)) + 1.0#))
            End If

          End If

          If (RISK_Usage >= 1) Then
            RISK_Status = "Break"
          ElseIf (RISK_Usage >= 0.75) Then
            RISK_Status = "Warning"
          Else
            RISK_Status = "OK"
          End If

          ExposureRow.expFund = FundId
          ExposureRow.expLimitID = CInt(thisInstrumentExposure(RISK_limitID))
          ExposureRow.expIsDetailRow = 0
          ExposureRow.expLimitSector = CInt(thisInstrumentExposure(RISK_InstrumentFundType))
          ExposureRow.expLimitSectorName = CStr(thisInstrumentExposure(RISK_FundTypeDescription))
          ExposureRow.expLimitInstrument = CInt(thisInstrumentExposure(RISK_TransactionInstrument))
          ExposureRow.expLimitType = CInt(thisInstrumentExposure(RISK_limType))
          ExposureRow.expLimitGreaterOrLessThan = CInt(thisInstrumentExposure(RISK_limGreaterOrLessThan))
          ExposureRow.expLimit = CDbl(thisInstrumentExposure(RISK_limLimit))
          ExposureRow.expExposure = RISK_Weight
          ExposureRow.expUsage = RISK_Usage
          ExposureRow.expStatus = RISK_Status
          ExposureRow.expDate = pDateToRisk
          ExposureRow.expDateCalculated = CalculationDate

          ExposureRow.expKnowledgeDate = pPreciseExposureKnowledgedate
          If (pPreciseExposureKnowledgedate = KNOWLEDGEDATE_NOW) OrElse (pPreciseExposureKnowledgedate.TimeOfDay.TotalSeconds >= LAST_SECOND) Then
            ExposureRow.expKnowledgeDateWholeday = (-1)
          Else
            ExposureRow.expKnowledgeDateWholeday = 0
          End If

          ExposureRow.expSystemKnowledgeDate = MainForm.Main_Knowledgedate
          If (MainForm.Main_Knowledgedate = KNOWLEDGEDATE_NOW) OrElse (MainForm.Main_Knowledgedate.TimeOfDay.TotalSeconds >= LAST_SECOND) Then
            ExposureRow.expSystemKnowledgeDateWholeday = (-1)
          Else
            ExposureRow.expSystemKnowledgeDateWholeday = 0
          End If

          ExposureRow.expFlags = ExposureFlags


          ' Save This Row 

          NewExposureRecords.Add(ExposureRow)

          'End If
        End If

      Catch ex As Exception
        MainForm.LogError("ProcessInstrumentWeightLimits", LOG_LEVELS.Error, ex.Message, "Error calculating Instrument Weight Limits.", ex.StackTrace, True)
        Return False
      End Try
    Next

    ' Now Add the specific exposures to the Exposures table, then add those general exposures which 
    ' are not covered by a specific limit.

    Dim InnerCount As Integer
    Dim OuterCount As Integer
    Dim CheckExposureRow As RenaissanceDataClass.DSExposure.tblExposureRow
    Dim FoundFlag As Boolean

    For OuterCount = 0 To (NewExposureRecords.Count - 1)
      Try
        ExposureRow = NewExposureRecords(OuterCount)

        If ExposureRow.expLimitType = CInt(RenaissanceGlobals.LimitType.SpecificInstrumentWeight) Then
          ' Ignore any Instruments with negligible impact
          If Math.Abs(ExposureRow.expExposure) >= RISK_MINIMUMEXPOSURETOSAVE Then
            ' Specific Limit, Add to table
            tblExposure.Rows.Add(ExposureRow)
          End If
        Else
          ' General Limit, Only add if a specific limit does not exist for this Instrument
          FoundFlag = False

          For InnerCount = 0 To (NewExposureRecords.Count - 1)
            ' Check for a specific Exposure for the current Instrument
            ' Exit without saving this record if one exists
            CheckExposureRow = NewExposureRecords(InnerCount)

            If (CheckExposureRow.expLimitType = CInt(RenaissanceGlobals.LimitType.SpecificInstrumentWeight)) AndAlso (CheckExposureRow.expLimitInstrument = ExposureRow.expLimitInstrument) Then
              FoundFlag = True
              Exit For
            End If
          Next

          If FoundFlag = False Then
            ' Ignore any Instruments with negligible impact
            If Math.Abs(ExposureRow.expExposure) >= RISK_MINIMUMEXPOSURETOSAVE Then
              ' Specific Limit, Add to table
              tblExposure.Rows.Add(ExposureRow)
            End If
          End If
        End If
      Catch ex As Exception
        MainForm.LogError("ProcessInstrumentWeightLimits", LOG_LEVELS.Error, ex.Message, "Error saving Instrument Weight Usage Limits.", ex.StackTrace, True)
        Return False
      End Try
    Next

    ' Save New Exposure Records.
    Try
      If tblExposure.Rows.Count > 0 Then
        MainForm.AdaptorUpdate("RiskExposureFunctions", AdpExposure, tblExposure)
      End If
    Catch ex As Exception
      MainForm.LogError("ProcessInstrumentWeightLimits", LOG_LEVELS.Error, ex.Message, "Error updating Instrument Weight Usage Limits.", ex.StackTrace, True)
      Return False
    End Try

    Return True

  End Function


  ''' <summary>
  ''' Processes the sector weight limits.
  ''' </summary>
  ''' <param name="MainForm">The main form.</param>
  ''' <param name="tblInstrumentExposures">The TBL instrument exposures.</param>
  ''' <param name="FundId">The fund id.</param>
  ''' <param name="Fund_Value">The fund_ value.</param>
  ''' <param name="pDateToPertrac">The p date to pertrac.</param>
  ''' <param name="pDateToRisk">The p date to risk.</param>
  ''' <param name="CalculationDate">The calculation date.</param>
  ''' <param name="pPreciseExposureKnowledgedate">The p precise exposure knowledgedate.</param>
  ''' <param name="ExposureFlags">The exposure flags.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function ProcessSectorWeightLimits(ByRef MainForm As VeniceMain, ByRef tblInstrumentExposures As DataTable, ByVal FundId As Integer, ByVal Fund_Value As Double, ByVal pDateToPertrac As Date, ByVal pDateToRisk As Date, ByVal CalculationDate As Date, ByVal pPreciseExposureKnowledgedate As Date, ByVal ExposureFlags As Integer) As Boolean

    ' Exit if no Exposure Data
    Try
      If (tblInstrumentExposures Is Nothing) OrElse (tblInstrumentExposures.Rows.Count <= 0) Then
        Return True
      End If
    Catch ex As Exception
      Return False
    End Try

    ' 
    Dim SelectedInstrumentExposures As DataRow()
    Dim thisInstrumentExposure As DataRow
    Dim nextInstrumentExposure As DataRow
    Dim Counter As Integer
    Dim NewExposureRecords As New ArrayList


    Dim DSExposure As New RenaissanceDataClass.DSExposure
    Dim tblExposure As RenaissanceDataClass.DSExposure.tblExposureDataTable
    Dim ExposureRow As RenaissanceDataClass.DSExposure.tblExposureRow
    Dim ExposureDetailRow As RenaissanceDataClass.DSExposure.tblExposureRow
    Dim AdpExposure As New SqlDataAdapter
    Dim ThisInstrumentValue As Double

    ExposureRow = Nothing

    Try
      tblExposure = DSExposure.tblExposure
    Catch ex As Exception
      MainForm.LogError("ProcessSectorWeightLimits", LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
      Return False
    End Try

    Dim RISK_TransactionFund As Integer
    Dim RISK_TransactionInstrument As Integer
    Dim RISK_InstrumentFundType As Integer
    Dim RISK_FundTypeDescription As Integer
    Dim RISK_InstrumentValue As Integer
    Dim RISK_InstrumentWorstCase As Integer
    Dim RISK_limType As Integer
    Dim RISK_limitID As Integer
    Dim RISK_limGreaterOrLessThan As Integer
    Dim RISK_limLimit As Integer

    Dim RISK_limOnPremium As Integer
    Dim RISK_InstrumentHasDelta As Integer
    Dim RISK_InstrumentDelta As Integer
    Dim RISK_InstrumentContractSize As Integer
    Dim RISK_InstrumentMultiplier As Integer
    Dim RISK_InstrumentUnderlyingPrice As Integer
    Dim RISK_InstrumentSharesPerOption As Integer
    Dim RISK_FXRate2 As Integer
    Dim RISK_FinalUnits As Integer

    Dim RISK_Weight As Double
    Dim RISK_Usage As Double
    Dim RISK_Status As String = ""
    Dim RISK_SumOfWeight As Double
    Dim RISK_SumOfUsage As Double


    ' Establish Ordinals
    Try
      RISK_TransactionFund = tblInstrumentExposures.Columns.IndexOf("Fund")
      RISK_limitID = tblInstrumentExposures.Columns.IndexOf("limID")
      RISK_TransactionInstrument = tblInstrumentExposures.Columns.IndexOf("Instrument")
      RISK_InstrumentFundType = tblInstrumentExposures.Columns.IndexOf("InstrumentFundType")
      RISK_FundTypeDescription = tblInstrumentExposures.Columns.IndexOf("FundTypeDescription")
      RISK_InstrumentValue = tblInstrumentExposures.Columns.IndexOf("Value2")
      RISK_InstrumentWorstCase = tblInstrumentExposures.Columns.IndexOf("InstrumentWorstCase")
      RISK_limType = tblInstrumentExposures.Columns.IndexOf("limType")
      RISK_limGreaterOrLessThan = tblInstrumentExposures.Columns.IndexOf("limGreaterOrLessThan")
      RISK_limLimit = tblInstrumentExposures.Columns.IndexOf("limLimit")

      RISK_limOnPremium = tblInstrumentExposures.Columns.IndexOf("limOnPremium")
      RISK_InstrumentHasDelta = tblInstrumentExposures.Columns.IndexOf("InstrumentTypeHasDelta")
      RISK_InstrumentDelta = tblInstrumentExposures.Columns.IndexOf("BestDelta")
      RISK_InstrumentContractSize = tblInstrumentExposures.Columns.IndexOf("InstrumentContractSize")
      RISK_InstrumentMultiplier = tblInstrumentExposures.Columns.IndexOf("InstrumentMultiplier")
      RISK_InstrumentUnderlyingPrice = tblInstrumentExposures.Columns.IndexOf("UnderlyingPrice")
      RISK_InstrumentSharesPerOption = tblInstrumentExposures.Columns.IndexOf("InstrumentSharesPerOption")
      RISK_FXRate2 = tblInstrumentExposures.Columns.IndexOf("FXRate2") ' Compound FX Rate Instrument to Fund FX
      RISK_FinalUnits = tblInstrumentExposures.Columns.IndexOf("FinalUnits") ' SignedUnits from 2nd PnL Valuation query.

    Catch ex As Exception
      MainForm.LogError("ProcessSectorWeightLimits", LOG_LEVELS.Error, ex.Message, "Error getting Field Ordinals from tblInstrumentExposures", ex.StackTrace, True)
      Return False
    End Try

    Try
      MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION), AdpExposure, "tblExposure")
    Catch ex As Exception
      MainForm.LogError("ProcessSectorWeightLimits", LOG_LEVELS.Error, ex.Message, "Error configuring tblExposure Adaptor.", ex.StackTrace, True)
      Return False
    End Try

    ' Select appropriate Exposure Records
    Try
      SelectedInstrumentExposures = tblInstrumentExposures.Select("((limType = " & CInt(RenaissanceGlobals.LimitType.SectorWeight).ToString & ") OR ((limType = " & CInt(RenaissanceGlobals.LimitType.SpecificSectorWeight).ToString & ") AND (limSector = InstrumentFundType)))", "Fund, limType, limGreaterOrLessThan, limLimit, InstrumentFundType")

      If (SelectedInstrumentExposures Is Nothing) OrElse (SelectedInstrumentExposures.Length <= 0) Then
        Return True
      End If
    Catch ex As Exception
      MainForm.LogError("ProcessSectorWeightLimits", LOG_LEVELS.Error, ex.Message, "Error selecting tblInstrumentExposures rows.", ex.StackTrace, True)
      Return False
    End Try


    ' Process selected Exposures
    ' 
    ' Calculate Calculated Fields :  Instrument Weight, Impact, Usage and Status
    '           Weight = [InstrumentValue] / [Fund Value]
    '           Impact = [Weight] * [WorstCase] * 10000    (Note : * 10000 to convert to bps)
    '           Usage  = [Weight] / [Limit]                (or inverse depending on Limit definition)
    '           Status = "OK", "Warning" or "Break"

    RISK_SumOfWeight = 0

    For Counter = 0 To (SelectedInstrumentExposures.Length - 1)
      Try
        thisInstrumentExposure = SelectedInstrumentExposures(Counter)

        ThisInstrumentValue = CDbl(thisInstrumentExposure(RISK_InstrumentValue))

        If (CBool(thisInstrumentExposure(RISK_limOnPremium)) = False) AndAlso CBool(thisInstrumentExposure(RISK_InstrumentHasDelta)) Then
          ThisInstrumentValue = CDbl(thisInstrumentExposure(RISK_FinalUnits)) * CDbl(thisInstrumentExposure(RISK_InstrumentDelta)) * CDbl(thisInstrumentExposure(RISK_InstrumentContractSize)) * CDbl(thisInstrumentExposure(RISK_InstrumentMultiplier)) * CDbl(thisInstrumentExposure(RISK_InstrumentSharesPerOption)) * CDbl(thisInstrumentExposure(RISK_InstrumentUnderlyingPrice)) * CDbl(thisInstrumentExposure(RISK_FXRate2))
        End If

        ' Ignore Small Value Exposures
        ' Ignore Instruments with Fund Type of CASH, LOAD or FEE
        ' "(NOT ((FundTypeDescription = 'Cash') OR (FundTypeDescription = 'LOAN') OR (FundTypeDescription = 'Fee')))"

        If Math.Abs(ThisInstrumentValue) >= 1.0# Then
          If Not ((CInt(thisInstrumentExposure(RISK_InstrumentFundType)) = FundType.Cash) OrElse _
           (CInt(thisInstrumentExposure(RISK_InstrumentFundType)) = FundType.Loan) OrElse _
           (CInt(thisInstrumentExposure(RISK_InstrumentFundType)) = FundType.Fee)) Then

            ExposureRow = tblExposure.NewtblExposureRow

            If Fund_Value = 0 Then
              RISK_Weight = 1
            Else
              RISK_Weight = ThisInstrumentValue / Fund_Value
            End If

            '' Roll Up Sector Totals
            RISK_SumOfWeight += RISK_Weight

            If CInt(thisInstrumentExposure(RISK_limGreaterOrLessThan)) = RenaissanceGlobals.Relationship.LessThan Then
              RISK_SumOfUsage = RISK_SumOfWeight / CDbl(thisInstrumentExposure(RISK_limLimit))
              RISK_Usage = RISK_Weight / CDbl(thisInstrumentExposure(RISK_limLimit))
              ' RISK_Usage = RISK_Weight / CDbl(thisInstrumentExposure(RISK_limLimit))
            Else
              If (RISK_SumOfWeight <> 0) Then
                RISK_SumOfUsage = CDbl(thisInstrumentExposure(RISK_limLimit)) / RISK_SumOfWeight
              End If
              If (RISK_Weight <> 0) Then
                RISK_Usage = CDbl(thisInstrumentExposure(RISK_limLimit)) / RISK_Weight
              End If
              ' RISK_Usage = CDbl(thisInstrumentExposure(RISK_limLimit)) / RISK_Weight
            End If

            If (RISK_SumOfUsage >= 1) Then
              RISK_Status = "Break"
            ElseIf (RISK_SumOfUsage >= 0.75) Then
              RISK_Status = "Warning"
            Else
              RISK_Status = "OK"
            End If

            ExposureRow.expFund = FundId
            ExposureRow.expLimitID = CInt(thisInstrumentExposure(RISK_limitID))
            ExposureRow.expIsDetailRow = 0
            ExposureRow.expLimitSector = CInt(thisInstrumentExposure(RISK_InstrumentFundType))
            ExposureRow.expLimitSectorName = CStr(thisInstrumentExposure(RISK_FundTypeDescription))
            ExposureRow.expLimitInstrument = 0
            ExposureRow.expLimitType = CInt(thisInstrumentExposure(RISK_limType))
            ExposureRow.expLimitGreaterOrLessThan = CInt(thisInstrumentExposure(RISK_limGreaterOrLessThan))
            ExposureRow.expLimit = CDbl(thisInstrumentExposure(RISK_limLimit))
            ExposureRow.expExposure = RISK_SumOfWeight
            ' ExposureRow.expExposure = RISK_Weight
            ExposureRow.expUsage = RISK_SumOfUsage
            ExposureRow.expStatus = RISK_Status
            ExposureRow.expDate = pDateToRisk
            ExposureRow.expDateCalculated = CalculationDate

            ExposureRow.expKnowledgeDate = pPreciseExposureKnowledgedate
            If (pPreciseExposureKnowledgedate = KNOWLEDGEDATE_NOW) OrElse (pPreciseExposureKnowledgedate.TimeOfDay.TotalSeconds >= LAST_SECOND) Then
              ExposureRow.expKnowledgeDateWholeday = (-1)
            Else
              ExposureRow.expKnowledgeDateWholeday = 0
            End If

            ExposureRow.expSystemKnowledgeDate = MainForm.Main_Knowledgedate
            If (MainForm.Main_Knowledgedate = KNOWLEDGEDATE_NOW) OrElse (MainForm.Main_Knowledgedate.TimeOfDay.TotalSeconds >= LAST_SECOND) Then
              ExposureRow.expSystemKnowledgeDateWholeday = (-1)
            Else
              ExposureRow.expSystemKnowledgeDateWholeday = 0
            End If

            ExposureRow.expFlags = ExposureFlags

          End If
        End If

        ' Detail Row.

        If (Math.Abs(RISK_Weight) > 0) Then

          ExposureDetailRow = tblExposure.NewtblExposureRow

          ExposureDetailRow.expFund = ExposureRow.expFund
          ExposureDetailRow.expLimitID = CInt(thisInstrumentExposure(RISK_limitID))
          ExposureDetailRow.expIsDetailRow = 1
          ExposureDetailRow.expLimitSector = ExposureRow.expLimitSector
          ExposureDetailRow.expLimitSectorName = ExposureRow.expLimitSectorName
          ExposureDetailRow.expLimitInstrument = ExposureRow.expLimitInstrument
          ExposureDetailRow.expLimitType = ExposureRow.expLimitType
          ExposureDetailRow.expLimitCharacteristic = ExposureRow.expLimitCharacteristic
          ExposureDetailRow.expLimitGreaterOrLessThan = ExposureRow.expLimitGreaterOrLessThan
          ExposureDetailRow.expLimit = ExposureRow.expLimit
          ExposureDetailRow.expDetailInstrument = CInt(thisInstrumentExposure(RISK_TransactionInstrument))
          ExposureDetailRow.expExposure = RISK_Weight
          ExposureDetailRow.expUsage = RISK_Usage
          ExposureDetailRow.expStatus = RISK_Status
          ExposureDetailRow.expDate = ExposureRow.expDate
          ExposureDetailRow.expDateCalculated = ExposureRow.expDateCalculated
          ExposureDetailRow.expKnowledgeDate = ExposureRow.expKnowledgeDate
          ExposureDetailRow.expKnowledgeDateWholeday = ExposureRow.expKnowledgeDateWholeday
          ExposureDetailRow.expSystemKnowledgeDate = ExposureRow.expSystemKnowledgeDate
          ExposureDetailRow.expSystemKnowledgeDateWholeday = ExposureRow.expSystemKnowledgeDateWholeday
          ExposureDetailRow.expFlags = ExposureRow.expFlags

          ' Save Detail Row

          If (Not (ExposureDetailRow Is Nothing)) Then
            NewExposureRecords.Add(ExposureDetailRow)
            ExposureDetailRow = Nothing
          End If

        End If

        ' Save This Row ?
        ' Save if this is the last Exposure Row OR 
        ' If we have reached the end of a section (Fund, Limit Type, Limit, Condition or FundType has changed)

        If (Not (ExposureRow Is Nothing)) Then
          If (Counter >= (SelectedInstrumentExposures.Length - 1)) Then
            NewExposureRecords.Add(ExposureRow)

            RISK_SumOfWeight = 0
            ExposureRow = Nothing

          Else
            nextInstrumentExposure = SelectedInstrumentExposures(Counter + 1)

            If (CInt(thisInstrumentExposure(RISK_TransactionFund)) <> CInt(nextInstrumentExposure(RISK_TransactionFund))) OrElse _
             (CInt(thisInstrumentExposure(RISK_limType)) <> CInt(nextInstrumentExposure(RISK_limType))) OrElse _
             (CInt(thisInstrumentExposure(RISK_limGreaterOrLessThan)) <> CInt(nextInstrumentExposure(RISK_limGreaterOrLessThan))) OrElse _
             (CDbl(thisInstrumentExposure(RISK_limLimit)) <> CDbl(nextInstrumentExposure(RISK_limLimit))) OrElse _
             (CInt(thisInstrumentExposure(RISK_InstrumentFundType)) <> CInt(nextInstrumentExposure(RISK_InstrumentFundType))) Then
              NewExposureRecords.Add(ExposureRow)

              RISK_SumOfWeight = 0
              ExposureRow = Nothing
            End If
          End If
        End If

      Catch ex As Exception
        MainForm.LogError("ProcessSectorWeightLimits", LOG_LEVELS.Error, ex.Message, "Error calculating Sector Weight Limits.", ex.StackTrace, True)
        Return False
      End Try
    Next

    ' Now Add the specific exposures to the Exposures table, then add those general exposures which 
    ' are not covered by a specific limit.

    Dim InnerCount As Integer
    Dim OuterCount As Integer
    Dim CheckExposureRow As RenaissanceDataClass.DSExposure.tblExposureRow
    Dim FoundFlag As Boolean

    For OuterCount = 0 To (NewExposureRecords.Count - 1)
      Try
        ExposureRow = NewExposureRecords(OuterCount)

        If ExposureRow.expLimitType = CInt(RenaissanceGlobals.LimitType.SpecificSectorWeight) Then
          ' Ignore any Instruments with negligible impact
          If Math.Abs(ExposureRow.expExposure) >= RISK_MINIMUMEXPOSURETOSAVE Then
            ' Specific Limit, Add to table
            tblExposure.Rows.Add(ExposureRow)
          End If
        Else
          ' General Limit, Only add if a specific limit does not exist for this Instrument
          FoundFlag = False

          For InnerCount = 0 To (NewExposureRecords.Count - 1)
            ' Check for a specific Exposure for the current Instrument
            ' Exit without saving this record if one exists
            CheckExposureRow = NewExposureRecords(InnerCount)

            If (CheckExposureRow.expLimitType = CInt(RenaissanceGlobals.LimitType.SpecificSectorWeight)) AndAlso (CheckExposureRow.expLimitSector = ExposureRow.expLimitSector) Then
              FoundFlag = True
              Exit For
            End If
          Next

          If FoundFlag = False Then
            ' Ignore any Instruments with negligible impact
            If Math.Abs(ExposureRow.expExposure) >= RISK_MINIMUMEXPOSURETOSAVE Then
              ' Specific Limit, Add to table
              tblExposure.Rows.Add(ExposureRow)
            End If
          End If
        End If
      Catch ex As Exception
        MainForm.LogError("ProcessSectorWeightLimits", LOG_LEVELS.Error, ex.Message, "Error saving Sector Weight Usage Limits.", ex.StackTrace, True)
        Return False
      End Try
    Next

    ' Save New Exposure Records.
    Try
      If tblExposure.Rows.Count > 0 Then
        MainForm.AdaptorUpdate("RiskExposureFunctions", AdpExposure, tblExposure)
      End If
    Catch ex As Exception
      MainForm.LogError("ProcessSectorWeightLimits", LOG_LEVELS.Error, ex.Message, "Error updating Sector Weight Usage Limits.", ex.StackTrace, True)
      Return False
    End Try

    Return True

  End Function

  ''' <summary>
  ''' Processes the sector group weight limits.
  ''' </summary>
  ''' <param name="MainForm">The main form.</param>
  ''' <param name="tblInstrumentExposures">The TBL instrument exposures.</param>
  ''' <param name="FundId">The fund id.</param>
  ''' <param name="Fund_Value">The fund_ value.</param>
  ''' <param name="pDateToPertrac">The p date to pertrac.</param>
  ''' <param name="pDateToRisk">The p date to risk.</param>
  ''' <param name="CalculationDate">The calculation date.</param>
  ''' <param name="pPreciseExposureKnowledgedate">The p precise exposure knowledgedate.</param>
  ''' <param name="ExposureFlags">The exposure flags.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function ProcessSectorGroupWeightLimits(ByRef MainForm As VeniceMain, ByRef tblInstrumentExposures As DataTable, ByVal FundId As Integer, ByVal Fund_Value As Double, ByVal pDateToPertrac As Date, ByVal pDateToRisk As Date, ByVal CalculationDate As Date, ByVal pPreciseExposureKnowledgedate As Date, ByVal ExposureFlags As Integer) As Boolean

    ' Exit if no Exposure Data
    Try
      If (tblInstrumentExposures Is Nothing) OrElse (tblInstrumentExposures.Rows.Count <= 0) Then
        Return True
      End If
    Catch ex As Exception
      Return False
    End Try

    ' 
    Dim SelectedInstrumentExposures As DataRow()
    Dim thisInstrumentExposure As DataRow
    Dim nextInstrumentExposure As DataRow
    Dim Counter As Integer
    Dim NewExposureRecords As New ArrayList


    Dim DSExposure As New RenaissanceDataClass.DSExposure
    Dim tblExposure As RenaissanceDataClass.DSExposure.tblExposureDataTable
    Dim ExposureRow As RenaissanceDataClass.DSExposure.tblExposureRow
    Dim ExposureDetailRow As RenaissanceDataClass.DSExposure.tblExposureRow
    Dim AdpExposure As New SqlDataAdapter
    Dim ThisInstrumentValue As Double

    ExposureRow = Nothing

    Try
      tblExposure = DSExposure.tblExposure
    Catch ex As Exception
      MainForm.LogError("ProcessSectorGroupWeightLimits", LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
      Return False
    End Try

    Dim RISK_TransactionFund As Integer
    Dim RISK_TransactionInstrument As Integer
    Dim RISK_InstrumentFundType As Integer
    Dim RISK_FundTypeDescription As Integer
    Dim RISK_FundTypeGroup As Integer
    Dim RISK_InstrumentValue As Integer
    Dim RISK_InstrumentWorstCase As Integer
    Dim RISK_limType As Integer
    Dim RISK_limitID As Integer
    Dim RISK_limGreaterOrLessThan As Integer
    Dim RISK_limLimit As Integer

    Dim RISK_limOnPremium As Integer
    Dim RISK_InstrumentHasDelta As Integer
    Dim RISK_InstrumentDelta As Integer
    Dim RISK_InstrumentContractSize As Integer
    Dim RISK_InstrumentMultiplier As Integer
    Dim RISK_InstrumentUnderlyingPrice As Integer
    Dim RISK_InstrumentSharesPerOption As Integer
    Dim RISK_FXRate2 As Integer
    Dim RISK_FinalUnits As Integer

    Dim RISK_Weight As Double
    Dim RISK_Impact As Double
    Dim RISK_Usage As Double
    Dim RISK_Status As String = ""
    Dim RISK_SumOfWeight As Double
    Dim RISK_SumOfImpact As Double
    Dim RISK_SumOfUsage As Double


    ' Establish Ordinals
    Try
      RISK_TransactionFund = tblInstrumentExposures.Columns.IndexOf("Fund")
      RISK_limitID = tblInstrumentExposures.Columns.IndexOf("limID")
      RISK_TransactionInstrument = tblInstrumentExposures.Columns.IndexOf("Instrument")
      RISK_InstrumentFundType = tblInstrumentExposures.Columns.IndexOf("InstrumentFundType")
      RISK_FundTypeDescription = tblInstrumentExposures.Columns.IndexOf("FundTypeDescription")
      RISK_FundTypeGroup = tblInstrumentExposures.Columns.IndexOf("FundTypeGroup")
      RISK_InstrumentValue = tblInstrumentExposures.Columns.IndexOf("Value2")
      RISK_InstrumentWorstCase = tblInstrumentExposures.Columns.IndexOf("InstrumentWorstCase")
      RISK_limType = tblInstrumentExposures.Columns.IndexOf("limType")
      RISK_limGreaterOrLessThan = tblInstrumentExposures.Columns.IndexOf("limGreaterOrLessThan")
      RISK_limLimit = tblInstrumentExposures.Columns.IndexOf("limLimit")

      RISK_limOnPremium = tblInstrumentExposures.Columns.IndexOf("limOnPremium")
      RISK_InstrumentHasDelta = tblInstrumentExposures.Columns.IndexOf("InstrumentTypeHasDelta")
      RISK_InstrumentDelta = tblInstrumentExposures.Columns.IndexOf("BestDelta")
      RISK_InstrumentContractSize = tblInstrumentExposures.Columns.IndexOf("InstrumentContractSize")
      RISK_InstrumentMultiplier = tblInstrumentExposures.Columns.IndexOf("InstrumentMultiplier")
      RISK_InstrumentUnderlyingPrice = tblInstrumentExposures.Columns.IndexOf("UnderlyingPrice")
      RISK_InstrumentSharesPerOption = tblInstrumentExposures.Columns.IndexOf("InstrumentSharesPerOption")
      RISK_FXRate2 = tblInstrumentExposures.Columns.IndexOf("FXRate2") ' Compound FX Rate Instrument to Fund FX
      RISK_FinalUnits = tblInstrumentExposures.Columns.IndexOf("FinalUnits") ' SignedUnits from 2nd PnL Valuation query.

    Catch ex As Exception
      MainForm.LogError("ProcessSectorGroupWeightLimits", LOG_LEVELS.Error, ex.Message, "Error getting Field Ordinals from tblInstrumentExposures", ex.StackTrace, True)
      Return False
    End Try

    Try
      MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION), AdpExposure, "tblExposure")
    Catch ex As Exception
      MainForm.LogError("ProcessSectorGroupWeightLimits", LOG_LEVELS.Error, ex.Message, "Error configuring tblExposure Adaptor.", ex.StackTrace, True)
      Return False
    End Try

    ' Select appropriate Exposure Records
    Try
      SelectedInstrumentExposures = tblInstrumentExposures.Select("((limType = " & CInt(RenaissanceGlobals.LimitType.SectorGroupWeight).ToString & ") OR ((limType = " & CInt(RenaissanceGlobals.LimitType.SpecificSectorGroupWeight).ToString & ") AND (limSectorGroup = FundTypeGroup)))", "Fund, limType, limGreaterOrLessThan, limLimit, FundTypeGroup")

      If (SelectedInstrumentExposures Is Nothing) OrElse (SelectedInstrumentExposures.Length <= 0) Then
        Return True
      End If
    Catch ex As Exception
      MainForm.LogError("ProcessSectorGroupWeightLimits", LOG_LEVELS.Error, ex.Message, "Error selecting tblInstrumentExposures rows.", ex.StackTrace, True)
      Return False
    End Try


    ' Process selected Exposures
    ' 
    ' Calculate Calculated Fields :  Instrument Weight, Impact, Usage and Status
    '           Weight = [InstrumentValue] / [Fund Value]
    '           Impact = [Weight] * [WorstCase] * 10000    (Note : * 10000 to convert to bps)
    '           Usage  = [Weight] / [Limit]                (or inverse depending on Limit definition)
    '           Status = "OK", "Warning" or "Break"

    RISK_SumOfWeight = 0
    RISK_SumOfImpact = 0

    For Counter = 0 To (SelectedInstrumentExposures.Length - 1)
      Try
        thisInstrumentExposure = SelectedInstrumentExposures(Counter)

        ' Ignore Small Value Exposures

        ThisInstrumentValue = CDbl(thisInstrumentExposure(RISK_InstrumentValue))

        If (CBool(thisInstrumentExposure(RISK_limOnPremium)) = False) AndAlso CBool(thisInstrumentExposure(RISK_InstrumentHasDelta)) Then
          ThisInstrumentValue = CDbl(thisInstrumentExposure(RISK_FinalUnits)) * CDbl(thisInstrumentExposure(RISK_InstrumentDelta)) * CDbl(thisInstrumentExposure(RISK_InstrumentContractSize)) * CDbl(thisInstrumentExposure(RISK_InstrumentMultiplier)) * CDbl(thisInstrumentExposure(RISK_InstrumentSharesPerOption)) * CDbl(thisInstrumentExposure(RISK_InstrumentUnderlyingPrice)) * CDbl(thisInstrumentExposure(RISK_FXRate2))
        End If

        If Math.Abs(ThisInstrumentValue) >= 1.0# Then

          ExposureRow = tblExposure.NewtblExposureRow

          If Fund_Value = 0 Then
            RISK_Weight = 1
          Else
            RISK_Weight = ThisInstrumentValue / Fund_Value
          End If

          RISK_Impact = RISK_Weight * (CDbl(thisInstrumentExposure(RISK_InstrumentWorstCase)) * 10000)

          '' Roll Up Sector Totals
          RISK_SumOfWeight += RISK_Weight
          RISK_SumOfImpact += RISK_Impact

          If CInt(thisInstrumentExposure(RISK_limGreaterOrLessThan)) = RenaissanceGlobals.Relationship.LessThan Then
            If (CDbl(thisInstrumentExposure(RISK_limLimit)) <> 0) Then
              RISK_SumOfUsage = RISK_SumOfWeight / CDbl(thisInstrumentExposure(RISK_limLimit))
              RISK_Usage = RISK_Weight / CDbl(thisInstrumentExposure(RISK_limLimit))
              ' RISK_Usage = RISK_Weight / CDbl(thisInstrumentExposure(RISK_limLimit))
            End If
          Else
            RISK_SumOfUsage = CDbl(thisInstrumentExposure(RISK_limLimit)) / RISK_SumOfWeight
            RISK_Usage = CDbl(thisInstrumentExposure(RISK_limLimit)) / RISK_Weight
            ' RISK_Usage = CDbl(thisInstrumentExposure(RISK_limLimit)) / RISK_Weight
          End If

          If (RISK_SumOfUsage >= 1) Then
            RISK_Status = "Break"
          ElseIf (RISK_SumOfUsage >= 0.75) Then
            RISK_Status = "Warning"
          Else
            RISK_Status = "OK"
          End If

          ExposureRow.expFund = FundId
          ExposureRow.expLimitID = CInt(thisInstrumentExposure(RISK_limitID))
          ExposureRow.expIsDetailRow = 0
          ExposureRow.expLimitSector = CInt(thisInstrumentExposure(RISK_InstrumentFundType))

          If (thisInstrumentExposure.IsNull(RISK_FundTypeGroup)) OrElse (CStr(thisInstrumentExposure(RISK_FundTypeGroup)).Length <= 0) Then
            ExposureRow.expLimitSectorName = "<No Group>"
          Else
            ExposureRow.expLimitSectorName = CStr(thisInstrumentExposure(RISK_FundTypeGroup))
          End If

          ExposureRow.expLimitInstrument = 0
          ExposureRow.expLimitType = CInt(thisInstrumentExposure(RISK_limType))
          ExposureRow.expLimitGreaterOrLessThan = CInt(thisInstrumentExposure(RISK_limGreaterOrLessThan))
          ExposureRow.expLimit = CDbl(thisInstrumentExposure(RISK_limLimit))
          ExposureRow.expExposure = RISK_SumOfWeight
          ExposureRow.expUsage = RISK_SumOfUsage
          ExposureRow.expStatus = RISK_Status
          ExposureRow.expDate = pDateToRisk
          ExposureRow.expDateCalculated = CalculationDate

          ExposureRow.expKnowledgeDate = pPreciseExposureKnowledgedate
          If (pPreciseExposureKnowledgedate = KNOWLEDGEDATE_NOW) OrElse (pPreciseExposureKnowledgedate.TimeOfDay.TotalSeconds >= LAST_SECOND) Then
            ExposureRow.expKnowledgeDateWholeday = (-1)
          Else
            ExposureRow.expKnowledgeDateWholeday = 0
          End If

          ExposureRow.expSystemKnowledgeDate = MainForm.Main_Knowledgedate
          If (MainForm.Main_Knowledgedate = KNOWLEDGEDATE_NOW) OrElse (MainForm.Main_Knowledgedate.TimeOfDay.TotalSeconds >= LAST_SECOND) Then
            ExposureRow.expSystemKnowledgeDateWholeday = (-1)
          Else
            ExposureRow.expSystemKnowledgeDateWholeday = 0
          End If

          ExposureRow.expFlags = ExposureFlags

        End If

        ' Detail Row.

        If (Math.Abs(RISK_Weight) > 0) Then

          ExposureDetailRow = tblExposure.NewtblExposureRow

          ExposureDetailRow.expFund = ExposureRow.expFund
          ExposureDetailRow.expLimitID = CInt(thisInstrumentExposure(RISK_limitID))
          ExposureDetailRow.expIsDetailRow = 1
          ExposureDetailRow.expLimitSector = ExposureRow.expLimitSector
          ExposureDetailRow.expLimitSectorName = ExposureRow.expLimitSectorName
          ExposureDetailRow.expLimitInstrument = ExposureRow.expLimitInstrument
          ExposureDetailRow.expLimitType = ExposureRow.expLimitType
          ExposureDetailRow.expLimitCharacteristic = ExposureRow.expLimitCharacteristic
          ExposureDetailRow.expLimitGreaterOrLessThan = ExposureRow.expLimitGreaterOrLessThan
          ExposureDetailRow.expLimit = ExposureRow.expLimit
          ExposureDetailRow.expDetailInstrument = CInt(thisInstrumentExposure(RISK_TransactionInstrument))
          ExposureDetailRow.expExposure = RISK_Weight
          ExposureDetailRow.expUsage = RISK_Usage
          ExposureDetailRow.expStatus = RISK_Status
          ExposureDetailRow.expDate = ExposureRow.expDate
          ExposureDetailRow.expDateCalculated = ExposureRow.expDateCalculated
          ExposureDetailRow.expKnowledgeDate = ExposureRow.expKnowledgeDate
          ExposureDetailRow.expKnowledgeDateWholeday = ExposureRow.expKnowledgeDateWholeday
          ExposureDetailRow.expSystemKnowledgeDate = ExposureRow.expSystemKnowledgeDate
          ExposureDetailRow.expSystemKnowledgeDateWholeday = ExposureRow.expSystemKnowledgeDateWholeday
          ExposureDetailRow.expFlags = ExposureRow.expFlags

          ' Save Detail Row

          If (Not (ExposureDetailRow Is Nothing)) Then
            NewExposureRecords.Add(ExposureDetailRow)
            ExposureDetailRow = Nothing
          End If

        End If


        If (Not (ExposureRow Is Nothing)) Then
          ' Save This Row ?
          ' Save if this is the last Exposure Row OR 
          ' If we have reached the end of a section (Fund, Limit Type, Limit, Condition or FundType has changed)

          If (Counter >= (SelectedInstrumentExposures.Length - 1)) Then
            NewExposureRecords.Add(ExposureRow)

            RISK_SumOfWeight = 0
            RISK_SumOfImpact = 0
            ExposureRow = Nothing
          Else
            nextInstrumentExposure = SelectedInstrumentExposures(Counter + 1)

            If (CInt(thisInstrumentExposure(RISK_TransactionFund)) <> CInt(nextInstrumentExposure(RISK_TransactionFund))) OrElse _
             (CInt(thisInstrumentExposure(RISK_limType)) <> CInt(nextInstrumentExposure(RISK_limType))) OrElse _
             (CInt(thisInstrumentExposure(RISK_limGreaterOrLessThan)) <> CInt(nextInstrumentExposure(RISK_limGreaterOrLessThan))) OrElse _
             (CDbl(thisInstrumentExposure(RISK_limLimit)) <> CDbl(nextInstrumentExposure(RISK_limLimit))) OrElse _
             (CStr(thisInstrumentExposure(RISK_FundTypeGroup)) <> CStr(nextInstrumentExposure(RISK_FundTypeGroup))) Then
              NewExposureRecords.Add(ExposureRow)

              RISK_SumOfWeight = 0
              RISK_SumOfImpact = 0
              ExposureRow = Nothing
            End If
          End If
        End If
      Catch ex As Exception
        MainForm.LogError("ProcessSectorGroupWeightLimits", LOG_LEVELS.Error, ex.Message, "Error calculating Sector Group Weight Limits.", ex.StackTrace, True)
        Return False
      End Try
    Next

    ' Now Add the specific exposures to the Exposures table, then add those general exposures which 
    ' are not covered by a specific limit.

    Dim InnerCount As Integer
    Dim OuterCount As Integer
    Dim CheckExposureRow As RenaissanceDataClass.DSExposure.tblExposureRow
    Dim FoundFlag As Boolean

    For OuterCount = 0 To (NewExposureRecords.Count - 1)
      Try
        ExposureRow = NewExposureRecords(OuterCount)

        If ExposureRow.expLimitType = CInt(RenaissanceGlobals.LimitType.SpecificSectorGroupWeight) Then
          ' Ignore any Instruments with negligible impact
          'If Math.Abs(ExposureRow.expExposure) >= RISK_MINIMUMEXPOSURETOSAVE Then
          ' Specific Limit, Add to table
          tblExposure.Rows.Add(ExposureRow)
          'End If
        Else
          ' General Limit, Only add if a specific limit does not exist for this Instrument
          FoundFlag = False

          For InnerCount = 0 To (NewExposureRecords.Count - 1)
            ' Check for a specific Exposure for the current Instrument
            ' Exit without saving this record if one exists
            CheckExposureRow = NewExposureRecords(InnerCount)

            If (CheckExposureRow.expLimitType = CInt(RenaissanceGlobals.LimitType.SpecificSectorGroupWeight)) AndAlso (CheckExposureRow.expLimitSectorName = ExposureRow.expLimitSectorName) Then
              FoundFlag = True
              Exit For
            End If
          Next

          If FoundFlag = False Then
            ' Ignore any Instruments with negligible impact
            If Math.Abs(ExposureRow.expExposure) >= RISK_MINIMUMEXPOSURETOSAVE Then
              ' Specific Limit, Add to table
              tblExposure.Rows.Add(ExposureRow)
            End If
          End If
        End If
      Catch ex As Exception
        MainForm.LogError("ProcessSectorGroupWeightLimits", LOG_LEVELS.Error, ex.Message, "Error saving Sector Group Weight Usage Limits.", ex.StackTrace, True)
        Return False
      End Try
    Next

    ' Save New Exposure Records.
    Try
      If tblExposure.Rows.Count > 0 Then
        MainForm.AdaptorUpdate("RiskExposureFunctions", AdpExposure, tblExposure)
      End If
    Catch ex As Exception
      MainForm.LogError("ProcessSectorGroupWeightLimits", LOG_LEVELS.Error, ex.Message, "Error updating Sector Group Weight Usage Limits.", ex.StackTrace, True)
      Return False
    End Try

    Return True

  End Function

  ''' <summary>
  ''' Processes the dealing period limits.
  ''' </summary>
  ''' <param name="MainForm">The main form.</param>
  ''' <param name="tblInstrumentExposures">The TBL instrument exposures.</param>
  ''' <param name="FundId">The fund id.</param>
  ''' <param name="Fund_Value">The fund_ value.</param>
  ''' <param name="pDateToPertrac">The p date to pertrac.</param>
  ''' <param name="pDateToRisk">The p date to risk.</param>
  ''' <param name="CalculationDate">The calculation date.</param>
  ''' <param name="pPreciseExposureKnowledgedate">The p precise exposure knowledgedate.</param>
  ''' <param name="ExposureFlags">The exposure flags.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function ProcessDealingPeriodLimits(ByRef MainForm As VeniceMain, ByRef tblInstrumentExposures As DataTable, ByVal FundId As Integer, ByVal Fund_Value As Double, ByVal pDateToPertrac As Date, ByVal pDateToRisk As Date, ByVal CalculationDate As Date, ByVal pPreciseExposureKnowledgedate As Date, ByVal ExposureFlags As Integer) As Boolean

    ' Exit if no Exposure Data
    Try
      If (tblInstrumentExposures Is Nothing) OrElse (tblInstrumentExposures.Rows.Count <= 0) Then
        Return True
      End If
    Catch ex As Exception
      Return False
    End Try

    ' 
    Dim SelectedInstrumentExposures As DataRow()
    Dim thisInstrumentExposure As DataRow
    Dim nextInstrumentExposure As DataRow
    Dim Counter As Integer
    Dim NewExposureRecords As New ArrayList


    Dim DSExposure As New RenaissanceDataClass.DSExposure
    Dim tblExposure As RenaissanceDataClass.DSExposure.tblExposureDataTable
    Dim ExposureRow As RenaissanceDataClass.DSExposure.tblExposureRow
    Dim ExposureDetailRow As RenaissanceDataClass.DSExposure.tblExposureRow
    Dim AdpExposure As New SqlDataAdapter

    ExposureRow = Nothing

    Try
      tblExposure = DSExposure.tblExposure
    Catch ex As Exception
      MainForm.LogError("ProcessDealingPeriodLimits", LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
      Return False
    End Try

    Dim RISK_TransactionFund As Integer
    Dim RISK_TransactionInstrument As Integer
    Dim RISK_InstrumentFundType As Integer
    Dim RISK_limDealingPeriod As Integer
    Dim RISK_limDealingPeriodDescription As Integer
    Dim RISK_limDealingPeriodDays As Integer
    Dim RISK_InstrumentDealingPeriodDays As Integer
    Dim RISK_InstrumentValue As Integer
    Dim RISK_InstrumentWorstCase As Integer
    Dim RISK_limType As Integer
    Dim RISK_limitID As Integer
    Dim RISK_limGreaterOrLessThan As Integer
    Dim RISK_limLimit As Integer

    Dim RISK_Weight As Double
    Dim RISK_Impact As Double
    Dim RISK_Usage As Double
    Dim RISK_Status As String = ""
    Dim RISK_SumOfWeight As Double
    Dim RISK_SumOfImpact As Double
    Dim RISK_SumOfUsage As Double


    ' Establish Ordinals
    Try
      RISK_TransactionFund = tblInstrumentExposures.Columns.IndexOf("Fund")
      RISK_limitID = tblInstrumentExposures.Columns.IndexOf("limID")
      RISK_TransactionInstrument = tblInstrumentExposures.Columns.IndexOf("Instrument")
      RISK_InstrumentFundType = tblInstrumentExposures.Columns.IndexOf("InstrumentFundType")
      RISK_limDealingPeriod = tblInstrumentExposures.Columns.IndexOf("limDealingPeriod")
      RISK_limDealingPeriodDescription = tblInstrumentExposures.Columns.IndexOf("LimitDealingPeriodDescription")
      RISK_limDealingPeriodDays = tblInstrumentExposures.Columns.IndexOf("LimitDealingPeriodDays")
      RISK_InstrumentDealingPeriodDays = tblInstrumentExposures.Columns.IndexOf("InstrumentDealingPeriodDays")
      RISK_InstrumentValue = tblInstrumentExposures.Columns.IndexOf("Value2")
      RISK_InstrumentWorstCase = tblInstrumentExposures.Columns.IndexOf("InstrumentWorstCase")
      RISK_limType = tblInstrumentExposures.Columns.IndexOf("limType")
      RISK_limGreaterOrLessThan = tblInstrumentExposures.Columns.IndexOf("limGreaterOrLessThan")
      RISK_limLimit = tblInstrumentExposures.Columns.IndexOf("limLimit")
    Catch ex As Exception
      MainForm.LogError("ProcessDealingPeriodLimits", LOG_LEVELS.Error, ex.Message, "Error getting Field Ordinals from tblInstrumentExposures", ex.StackTrace, True)
      Return False
    End Try

    Try
      MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION), AdpExposure, "tblExposure")
    Catch ex As Exception
      MainForm.LogError("ProcessDealingPeriodLimits", LOG_LEVELS.Error, ex.Message, "Error configuring tblExposure Adaptor.", ex.StackTrace, True)
      Return False
    End Try

    ' Select appropriate Exposure Records
    Try
      Try
        SelectedInstrumentExposures = tblInstrumentExposures.Select("limType = " & CInt(RenaissanceGlobals.LimitType.DealingPeriod).ToString, "Fund, limType, limDealingPeriod, limGreaterOrLessThan, limLimit")
      Catch ex As Exception
        ' Have been having a perverse error cropping up.
        ' On machines other than mine (Users other than me ?), The previous select has been throwing an error.
        ' When debugging, it seems as though if you perform the operation twice as below, then it works !
        Try
          SelectedInstrumentExposures = tblInstrumentExposures.Select("limType = " & CInt(RenaissanceGlobals.LimitType.DealingPeriod).ToString)
          SelectedInstrumentExposures = tblInstrumentExposures.Select("limType = " & CInt(RenaissanceGlobals.LimitType.DealingPeriod).ToString, "Fund, limType, limDealingPeriod, limGreaterOrLessThan, limLimit")
        Catch Inner_ex As Exception
          MainForm.LogError("ProcessDealingPeriodLimits", LOG_LEVELS.Error, Inner_ex.Message, "Error selecting tblInstrumentExposures rows.", Inner_ex.StackTrace, True)
          Return False
        End Try
      End Try

      If (SelectedInstrumentExposures Is Nothing) OrElse (SelectedInstrumentExposures.Length <= 0) Then
        Return True
      End If
    Catch ex As Exception
      MainForm.LogError("ProcessDealingPeriodLimits", LOG_LEVELS.Error, ex.Message, "Error selecting tblInstrumentExposures rows.", ex.StackTrace, True)
      Return False
    End Try


    ' Process selected Exposures
    ' 
    ' Calculate Calculated Fields :  Instrument Weight, Impact, Usage and Status
    '           Weight = [InstrumentValue] / [Fund Value]
    '           Impact = [Weight] * [WorstCase] * 10000    (Note : * 10000 to convert to bps)
    '           Usage  = [Weight] / [Limit]                (or inverse depending on Limit definition)
    '           Status = "OK", "Warning" or "Break"

    RISK_SumOfWeight = 0
    RISK_SumOfImpact = 0

    For Counter = 0 To (SelectedInstrumentExposures.Length - 1)
      Try
        thisInstrumentExposure = SelectedInstrumentExposures(Counter)

        ' Ignore Instruments with Fund Type of CASH, LOAD or FEE
        ' "(NOT ((FundTypeDescription = 'Cash') OR (FundTypeDescription = 'LOAN') OR (FundTypeDescription = 'Fee')))"

        If True Then
          If Not ((CInt(thisInstrumentExposure(RISK_InstrumentFundType)) = FundType.Cash) Or _
            (CInt(thisInstrumentExposure(RISK_InstrumentFundType)) = FundType.Loan) Or _
            (CInt(thisInstrumentExposure(RISK_InstrumentFundType)) = FundType.Fee)) Then

            ExposureRow = tblExposure.NewtblExposureRow

            If Fund_Value = 0 Then
              RISK_Weight = 1
            Else
              RISK_Weight = CDbl(thisInstrumentExposure(RISK_InstrumentValue)) / Fund_Value
            End If

            RISK_Impact = RISK_Weight * 10000

            ' Roll Up Sector Totals
            If CInt(thisInstrumentExposure(RISK_InstrumentDealingPeriodDays)) <= CInt(thisInstrumentExposure(RISK_limDealingPeriodDays)) Then
              RISK_SumOfWeight += RISK_Weight
              RISK_SumOfImpact += RISK_Impact
            End If

            If CInt(thisInstrumentExposure(RISK_limGreaterOrLessThan)) = RenaissanceGlobals.Relationship.LessThan Then
              If (CDbl(thisInstrumentExposure(RISK_limLimit)) <> 0) Then
                RISK_SumOfUsage = RISK_SumOfWeight / CDbl(thisInstrumentExposure(RISK_limLimit))
                RISK_Usage = RISK_Weight / CDbl(thisInstrumentExposure(RISK_limLimit))
                ' RISK_Usage = RISK_Weight / CDbl(thisInstrumentExposure(RISK_limLimit))
              End If
            Else
              If (RISK_SumOfWeight <> 0) Then
                RISK_SumOfUsage = CDbl(thisInstrumentExposure(RISK_limLimit)) / RISK_SumOfWeight
              End If
              If (RISK_Weight <> 0) Then
                RISK_Usage = CDbl(thisInstrumentExposure(RISK_limLimit)) / RISK_Weight
              End If
            End If

            If (RISK_SumOfUsage >= 1) Then
              RISK_Status = "Break"
            ElseIf (RISK_SumOfUsage >= 0.75) Then
              RISK_Status = "Warning"
            Else
              RISK_Status = "OK"
            End If

            ExposureRow.expFund = FundId
            ExposureRow.expLimitID = CInt(thisInstrumentExposure(RISK_limitID))
            ExposureRow.expIsDetailRow = 0
            ExposureRow.expLimitSector = 0
            ExposureRow.expLimitDealingPeriod = CInt(thisInstrumentExposure(RISK_limDealingPeriod))
            ExposureRow.expLimitSectorName = CStr(thisInstrumentExposure(RISK_limDealingPeriodDescription))
            ExposureRow.expLimitInstrument = 0
            ExposureRow.expLimitType = CInt(thisInstrumentExposure(RISK_limType))
            ExposureRow.expLimitGreaterOrLessThan = CInt(thisInstrumentExposure(RISK_limGreaterOrLessThan))
            ExposureRow.expLimit = CDbl(thisInstrumentExposure(RISK_limLimit))
            ExposureRow.expExposure = RISK_SumOfWeight
            ' ExposureRow.expExposure = RISK_Weight
            ExposureRow.expUsage = RISK_SumOfUsage
            ExposureRow.expStatus = RISK_Status
            ExposureRow.expDate = pDateToRisk
            ExposureRow.expDateCalculated = CalculationDate

            ExposureRow.expKnowledgeDate = pPreciseExposureKnowledgedate
            If (pPreciseExposureKnowledgedate = KNOWLEDGEDATE_NOW) OrElse (pPreciseExposureKnowledgedate.TimeOfDay.TotalSeconds >= LAST_SECOND) Then
              ExposureRow.expKnowledgeDateWholeday = (-1)
            Else
              ExposureRow.expKnowledgeDateWholeday = 0
            End If

            ExposureRow.expSystemKnowledgeDate = MainForm.Main_Knowledgedate
            If (MainForm.Main_Knowledgedate = KNOWLEDGEDATE_NOW) OrElse (MainForm.Main_Knowledgedate.TimeOfDay.TotalSeconds >= LAST_SECOND) Then
              ExposureRow.expSystemKnowledgeDateWholeday = (-1)
            Else
              ExposureRow.expSystemKnowledgeDateWholeday = 0
            End If

            ExposureRow.expFlags = ExposureFlags


          End If
        End If

        ' Detail Row.

        If (Math.Abs(RISK_Weight) > 0) Then

          ExposureDetailRow = tblExposure.NewtblExposureRow

          ExposureDetailRow.expFund = ExposureRow.expFund
          ExposureDetailRow.expLimitID = CInt(thisInstrumentExposure(RISK_limitID))
          ExposureDetailRow.expIsDetailRow = 1
          ExposureDetailRow.expLimitSector = ExposureRow.expLimitSector
          ExposureDetailRow.expLimitSectorName = ExposureRow.expLimitSectorName
          ExposureDetailRow.expLimitInstrument = ExposureRow.expLimitInstrument
          ExposureDetailRow.expLimitType = ExposureRow.expLimitType
          ExposureDetailRow.expLimitCharacteristic = ExposureRow.expLimitCharacteristic
          ExposureDetailRow.expLimitGreaterOrLessThan = ExposureRow.expLimitGreaterOrLessThan
          ExposureDetailRow.expLimit = ExposureRow.expLimit
          ExposureDetailRow.expDetailInstrument = CInt(thisInstrumentExposure(RISK_TransactionInstrument))
          ExposureDetailRow.expExposure = RISK_Weight
          ExposureDetailRow.expUsage = RISK_Usage
          ExposureDetailRow.expStatus = RISK_Status
          ExposureDetailRow.expDate = ExposureRow.expDate
          ExposureDetailRow.expDateCalculated = ExposureRow.expDateCalculated
          ExposureDetailRow.expKnowledgeDate = ExposureRow.expKnowledgeDate
          ExposureDetailRow.expKnowledgeDateWholeday = ExposureRow.expKnowledgeDateWholeday
          ExposureDetailRow.expSystemKnowledgeDate = ExposureRow.expSystemKnowledgeDate
          ExposureDetailRow.expSystemKnowledgeDateWholeday = ExposureRow.expSystemKnowledgeDateWholeday
          ExposureDetailRow.expFlags = ExposureRow.expFlags

          ' Save Detail Row

          If (Not (ExposureDetailRow Is Nothing)) Then
            NewExposureRecords.Add(ExposureDetailRow)
            ExposureDetailRow = Nothing
          End If

        End If



        If (Not (ExposureRow Is Nothing)) Then

          ' Save This Row ?
          ' Save if this is the last Exposure Row OR 
          ' If we have reached the end of a section (Fund, Limit Type, Limit, Condition or FundType has changed)

          If (Counter >= (SelectedInstrumentExposures.Length - 1)) Then
            NewExposureRecords.Add(ExposureRow)

            RISK_SumOfWeight = 0
            RISK_SumOfImpact = 0
            ExposureRow = Nothing
          Else
            nextInstrumentExposure = SelectedInstrumentExposures(Counter + 1)

            If (CInt(thisInstrumentExposure(RISK_TransactionFund)) <> CInt(nextInstrumentExposure(RISK_TransactionFund))) OrElse _
             (CInt(thisInstrumentExposure(RISK_limType)) <> CInt(nextInstrumentExposure(RISK_limType))) OrElse _
             (CInt(thisInstrumentExposure(RISK_limGreaterOrLessThan)) <> CInt(nextInstrumentExposure(RISK_limGreaterOrLessThan))) OrElse _
             (CDbl(thisInstrumentExposure(RISK_limLimit)) <> CDbl(nextInstrumentExposure(RISK_limLimit))) OrElse _
             (CInt(thisInstrumentExposure(RISK_limDealingPeriod)) <> CInt(nextInstrumentExposure(RISK_limDealingPeriod))) Then
              NewExposureRecords.Add(ExposureRow)

              RISK_SumOfWeight = 0
              RISK_SumOfImpact = 0
              ExposureRow = Nothing
            End If
          End If

        End If

      Catch ex As Exception
        MainForm.LogError("ProcessDealingPeriodLimits", LOG_LEVELS.Error, ex.Message, "Error calculating Dealing Period Limits.", ex.StackTrace, True)
        Return False
      End Try
    Next

    ' Now Add the specific exposures to the Exposures table, then add those general exposures which 
    ' are not covered by a specific limit.

    Dim OuterCount As Integer

    For OuterCount = 0 To (NewExposureRecords.Count - 1)
      Try
        ExposureRow = NewExposureRecords(OuterCount)

        If ExposureRow.expLimitType = CInt(RenaissanceGlobals.LimitType.DealingPeriod) Then
          ' Specific Limit, Add to table
          tblExposure.Rows.Add(ExposureRow)
        End If
      Catch ex As Exception
        MainForm.LogError("ProcessDealingPeriodLimits", LOG_LEVELS.Error, ex.Message, "Error saving Dealing Period Limits.", ex.StackTrace, True)
        Return False
      End Try
    Next

    ' Save New Exposure Records.
    Try
      If tblExposure.Rows.Count > 0 Then
        MainForm.AdaptorUpdate("RiskExposureFunctions", AdpExposure, tblExposure)
      End If
    Catch ex As Exception
      MainForm.LogError("ProcessDealingPeriodLimits", LOG_LEVELS.Error, ex.Message, "Error updating Dealing Period Limits.", ex.StackTrace, True)
      Return False
    End Try

    Return True

  End Function

  ''' <summary>
  ''' Processes the instrument count limits.
  ''' </summary>
  ''' <param name="MainForm">The main form.</param>
  ''' <param name="tblInstrumentExposures">The TBL instrument exposures.</param>
  ''' <param name="FundId">The fund id.</param>
  ''' <param name="Fund_Value">The fund_ value.</param>
  ''' <param name="pDateToPertrac">The p date to pertrac.</param>
  ''' <param name="pDateToRisk">The p date to risk.</param>
  ''' <param name="CalculationDate">The calculation date.</param>
  ''' <param name="pPreciseExposureKnowledgedate">The p precise exposure knowledgedate.</param>
  ''' <param name="ExposureFlags">The exposure flags.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function ProcessInstrumentCountLimits(ByRef MainForm As VeniceMain, ByRef tblInstrumentExposures As DataTable, ByVal FundId As Integer, ByVal Fund_Value As Double, ByVal pDateToPertrac As Date, ByVal pDateToRisk As Date, ByVal CalculationDate As Date, ByVal pPreciseExposureKnowledgedate As Date, ByVal ExposureFlags As Integer) As Boolean

    ' Exit if no Exposure Data
    Try
      If (tblInstrumentExposures Is Nothing) OrElse (tblInstrumentExposures.Rows.Count <= 0) Then
        Return True
      End If
    Catch ex As Exception
      Return False
    End Try

    ' 
    Dim SelectedInstrumentExposures As DataRow()
    Dim thisInstrumentExposure As DataRow
    Dim nextInstrumentExposure As DataRow
    Dim Counter As Integer
    Dim NewExposureRecords As New ArrayList


    Dim DSExposure As New RenaissanceDataClass.DSExposure
    Dim tblExposure As RenaissanceDataClass.DSExposure.tblExposureDataTable
    Dim ExposureRow As RenaissanceDataClass.DSExposure.tblExposureRow
    Dim AdpExposure As New SqlDataAdapter

    ExposureRow = Nothing

    Try
      tblExposure = DSExposure.tblExposure
    Catch ex As Exception
      MainForm.LogError("ProcessInstrumentCountLimits", LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
      Return False
    End Try

    Dim RISK_TransactionFund As Integer
    Dim RISK_TransactionInstrument As Integer
    Dim RISK_InstrumentFundType As Integer
    Dim RISK_FundTypeDescription As Integer
    Dim RISK_InstrumentValue As Integer
    Dim RISK_InstrumentWorstCase As Integer
    Dim RISK_limType As Integer
    Dim RISK_limitID As Integer
    Dim RISK_limGreaterOrLessThan As Integer
    Dim RISK_limLimit As Integer

    Dim RISK_Weight As Double
    Dim RISK_Impact As Double
    Dim RISK_Usage As Double
    Dim RISK_Status As String
    Dim RISK_SumOfWeight As Double
    Dim RISK_SumOfImpact As Double


    ' Establish Ordinals
    Try
      RISK_TransactionFund = tblInstrumentExposures.Columns.IndexOf("Fund")
      RISK_limitID = tblInstrumentExposures.Columns.IndexOf("limID")
      RISK_TransactionInstrument = tblInstrumentExposures.Columns.IndexOf("Instrument")
      RISK_InstrumentFundType = tblInstrumentExposures.Columns.IndexOf("InstrumentFundType")
      RISK_FundTypeDescription = tblInstrumentExposures.Columns.IndexOf("FundTypeDescription")
      RISK_InstrumentValue = tblInstrumentExposures.Columns.IndexOf("Value2")
      RISK_InstrumentWorstCase = tblInstrumentExposures.Columns.IndexOf("InstrumentWorstCase")
      RISK_limType = tblInstrumentExposures.Columns.IndexOf("limType")
      RISK_limGreaterOrLessThan = tblInstrumentExposures.Columns.IndexOf("limGreaterOrLessThan")
      RISK_limLimit = tblInstrumentExposures.Columns.IndexOf("limLimit")
    Catch ex As Exception
      MainForm.LogError("ProcessInstrumentCountLimits", LOG_LEVELS.Error, ex.Message, "Error getting Field Ordinals from tblInstrumentExposures", ex.StackTrace, True)
      Return False
    End Try

    Try
      MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION), AdpExposure, "tblExposure")
    Catch ex As Exception
      MainForm.LogError("ProcessInstrumentCountLimits", LOG_LEVELS.Error, ex.Message, "Error configuring tblExposure Adaptor.", ex.StackTrace, True)
      Return False
    End Try

    ' Select appropriate Exposure Records
    Try
      SelectedInstrumentExposures = tblInstrumentExposures.Select("limType = " & CInt(RenaissanceGlobals.LimitType.NumberOfInstruments).ToString, "Fund, limType, limGreaterOrLessThan, limLimit, InstrumentFundType")

      If (SelectedInstrumentExposures Is Nothing) OrElse (SelectedInstrumentExposures.Length <= 0) Then
        Return True
      End If
    Catch ex As Exception
      MainForm.LogError("ProcessInstrumentCountLimits", LOG_LEVELS.Error, ex.Message, "Error selecting tblInstrumentExposures rows.", ex.StackTrace, True)
      Return False
    End Try


    ' Process selected Exposures
    ' 
    ' Calculate Calculated Fields :  Instrument Weight, Impact, Usage and Status
    '           Weight = 1
    '           Impact = 1
    '           Usage  = [Weight] / [Limit]                (or inverse depending on Limit definition)
    '           Status = "OK", "Warning" or "Break"

    RISK_SumOfWeight = 0
    RISK_SumOfImpact = 0

    For Counter = 0 To (SelectedInstrumentExposures.Length - 1)
      Try
        thisInstrumentExposure = SelectedInstrumentExposures(Counter)

        ' Ignore Small Value Exposures
        ' Ignore Instruments with Fund Type of CASH, LOAD or FEE
        ' "(NOT ((FundTypeDescription = 'Cash') OR (FundTypeDescription = 'LOAN') OR (FundTypeDescription = 'Fee')))"

        If Math.Abs(CDbl(thisInstrumentExposure("Value2"))) >= 1 Then
          If Not ((CInt(thisInstrumentExposure(RISK_InstrumentFundType)) = FundType.Cash) Or _
            (CInt(thisInstrumentExposure(RISK_InstrumentFundType)) = FundType.Loan) Or _
            (CInt(thisInstrumentExposure(RISK_InstrumentFundType)) = FundType.Fee)) Then

            ExposureRow = tblExposure.NewtblExposureRow

            RISK_Weight = 1

            RISK_Impact = 1

            '' Roll Up Sector Totals
            RISK_SumOfWeight += RISK_Weight
            RISK_SumOfImpact += RISK_Impact

            If CInt(thisInstrumentExposure(RISK_limGreaterOrLessThan)) = RenaissanceGlobals.Relationship.LessThan Then
              RISK_Usage = RISK_SumOfWeight / CDbl(thisInstrumentExposure(RISK_limLimit))
              ' RISK_Usage = RISK_Weight / CDbl(thisInstrumentExposure(RISK_limLimit))
            Else
              RISK_Usage = CDbl(thisInstrumentExposure(RISK_limLimit)) / RISK_SumOfWeight
              ' RISK_Usage = CDbl(thisInstrumentExposure(RISK_limLimit)) / RISK_Weight
            End If

            If (RISK_Usage >= 1) Then
              RISK_Status = "Break"
            ElseIf (RISK_Usage >= 0.75) Then
              RISK_Status = "Warning"
            Else
              RISK_Status = "OK"
            End If

            ExposureRow.expFund = FundId
            ExposureRow.expLimitID = CInt(thisInstrumentExposure(RISK_limitID))
            ExposureRow.expIsDetailRow = 0
            ExposureRow.expLimitSector = 0
            ExposureRow.expLimitSectorName = ""
            ExposureRow.expLimitInstrument = 0
            ExposureRow.expLimitType = CInt(thisInstrumentExposure(RISK_limType))
            ExposureRow.expLimitGreaterOrLessThan = CInt(thisInstrumentExposure(RISK_limGreaterOrLessThan))
            ExposureRow.expLimit = CDbl(thisInstrumentExposure(RISK_limLimit))
            ExposureRow.expExposure = RISK_SumOfWeight
            ' ExposureRow.expExposure = RISK_Weight
            ExposureRow.expUsage = RISK_Usage
            ExposureRow.expStatus = RISK_Status
            ExposureRow.expDate = pDateToRisk
            ExposureRow.expDateCalculated = CalculationDate

            ExposureRow.expKnowledgeDate = pPreciseExposureKnowledgedate
            If (pPreciseExposureKnowledgedate = KNOWLEDGEDATE_NOW) OrElse (pPreciseExposureKnowledgedate.TimeOfDay.TotalSeconds >= LAST_SECOND) Then
              ExposureRow.expKnowledgeDateWholeday = (-1)
            Else
              ExposureRow.expKnowledgeDateWholeday = 0
            End If

            ExposureRow.expSystemKnowledgeDate = MainForm.Main_Knowledgedate
            If (MainForm.Main_Knowledgedate = KNOWLEDGEDATE_NOW) OrElse (MainForm.Main_Knowledgedate.TimeOfDay.TotalSeconds >= LAST_SECOND) Then
              ExposureRow.expSystemKnowledgeDateWholeday = (-1)
            Else
              ExposureRow.expSystemKnowledgeDateWholeday = 0
            End If

            ExposureRow.expFlags = ExposureFlags

          End If
        End If

        If (Not (ExposureRow Is Nothing)) Then
          ' Save This Row ?
          ' Save if this is the last Exposure Row OR 
          ' If we have reached the end of a section (Fund, Limit Type, Limit, Condition or FundType has changed)

          If (Counter >= (SelectedInstrumentExposures.Length - 1)) Then
            NewExposureRecords.Add(ExposureRow)

            RISK_SumOfWeight = 0
            RISK_SumOfImpact = 0
            ExposureRow = Nothing
          Else
            nextInstrumentExposure = SelectedInstrumentExposures(Counter + 1)

            If (CInt(thisInstrumentExposure(RISK_TransactionFund)) <> CInt(nextInstrumentExposure(RISK_TransactionFund))) OrElse _
             (CInt(thisInstrumentExposure(RISK_limType)) <> CInt(nextInstrumentExposure(RISK_limType))) OrElse _
             (CInt(thisInstrumentExposure(RISK_limGreaterOrLessThan)) <> CInt(nextInstrumentExposure(RISK_limGreaterOrLessThan))) OrElse _
             (CDbl(thisInstrumentExposure(RISK_limLimit)) <> CDbl(nextInstrumentExposure(RISK_limLimit))) Then
              NewExposureRecords.Add(ExposureRow)

              RISK_SumOfWeight = 0
              RISK_SumOfImpact = 0
              ExposureRow = Nothing
            End If
          End If
        End If

      Catch ex As Exception
        MainForm.LogError("ProcessInstrumentCountLimits", LOG_LEVELS.Error, ex.Message, "Error calculating Instrument Count Limits.", ex.StackTrace, True)
        Return False
      End Try
    Next

    ' Now Add the specific exposures to the Exposures table, then add those general exposures which 
    ' are not covered by a specific limit.

    Dim OuterCount As Integer

    For OuterCount = 0 To (NewExposureRecords.Count - 1)
      Try
        ExposureRow = NewExposureRecords(OuterCount)

        If ExposureRow.expLimitType = CInt(RenaissanceGlobals.LimitType.NumberOfInstruments) Then
          ' Specific Limit, Add to table
          tblExposure.Rows.Add(ExposureRow)
        End If
      Catch ex As Exception
        MainForm.LogError("ProcessInstrumentCountLimits", LOG_LEVELS.Error, ex.Message, "Error saving Instrument Count Limits.", ex.StackTrace, True)
        Return False
      End Try
    Next

    ' Save New Exposure Records.
    Try
      If tblExposure.Rows.Count > 0 Then
        MainForm.AdaptorUpdate("RiskExposureFunctions", AdpExposure, tblExposure)
      End If
    Catch ex As Exception
      MainForm.LogError("ProcessInstrumentCountLimits", LOG_LEVELS.Error, ex.Message, "Error updating Instrument Count Limits.", ex.StackTrace, True)
      Return False
    End Try

    Return True

  End Function

  ''' <summary>
  ''' Processes the currency exposure limits.
  ''' </summary>
  ''' <param name="MainForm">The main form.</param>
  ''' <param name="tblInstrumentExposures">The TBL instrument exposures.</param>
  ''' <param name="FundId">The fund id.</param>
  ''' <param name="Fund_Value">The fund_ value.</param>
  ''' <param name="pDateToPertrac">The p date to pertrac.</param>
  ''' <param name="pDateToRisk">The p date to risk.</param>
  ''' <param name="CalculationDate">The calculation date.</param>
  ''' <param name="pPreciseExposureKnowledgedate">The p precise exposure knowledgedate.</param>
  ''' <param name="ExposureFlags">The exposure flags.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function ProcessCurrencyExposureLimits(ByRef MainForm As VeniceMain, ByRef tblInstrumentExposures As DataTable, ByVal FundId As Integer, ByVal Fund_Value As Double, ByVal pDateToPertrac As Date, ByVal pDateToRisk As Date, ByVal CalculationDate As Date, ByVal pPreciseExposureKnowledgedate As Date, ByVal ExposureFlags As Integer) As Boolean

    ' Exit if no Exposure Data
    Try
      If (tblInstrumentExposures Is Nothing) OrElse (tblInstrumentExposures.Rows.Count <= 0) Then
        Return True
      End If
    Catch ex As Exception
      Return False
    End Try

    Dim SelectedInstrumentExposures As DataRow()
    Dim thisInstrumentExposure As DataRow
    Dim nextInstrumentExposure As DataRow
    Dim Counter As Integer
    Dim NewExposureRecords As New ArrayList


    Dim DSExposure As New RenaissanceDataClass.DSExposure
    Dim tblExposure As RenaissanceDataClass.DSExposure.tblExposureDataTable
    Dim ExposureRow As RenaissanceDataClass.DSExposure.tblExposureRow = Nothing
    Dim ExposureDetailRow As RenaissanceDataClass.DSExposure.tblExposureRow = Nothing
    Dim AdpExposure As New SqlDataAdapter

    ExposureRow = Nothing

    Try
      tblExposure = DSExposure.tblExposure
    Catch ex As Exception
      MainForm.LogError("ProcessInstrumentCountLimits", LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
      Return False
    End Try

    Dim RISK_TransactionFund As Integer
    Dim RISK_TransactionInstrument As Integer
    Dim RISK_InstrumentFundType As Integer
    Dim RISK_FundTypeDescription As Integer
    Dim RISK_InstrumentValue As Integer
    Dim RISK_InstrumentWorstCase As Integer
    Dim RISK_limType As Integer
    Dim RISK_limitID As Integer
    Dim RISK_limGreaterOrLessThan As Integer
    Dim RISK_limLimit As Integer
    Dim RISK_limCurrency As Integer
    Dim RISK_InstrumentCurrencyID As Integer
    Dim RISK_InstrumentCurrency As Integer
    Dim RISK_EndPrice As Integer
    Dim RISK_FinalUnits As Integer

    Dim RISK_limOnPremium As Integer
    Dim RISK_InstrumentHasDelta As Integer
    Dim RISK_InstrumentDelta As Integer
    Dim RISK_InstrumentContractSize As Integer
    Dim RISK_InstrumentMultiplier As Integer
    Dim RISK_InstrumentUnderlyingPrice As Integer
    Dim RISK_InstrumentSharesPerOption As Integer
    Dim RISK_FXRate2 As Integer

    Dim RISK_Weight As Double
    Dim RISK_Impact As Double
    Dim RISK_Usage As Double
    Dim RISK_Status As String = ""
    Dim RISK_SumOfWeight As Double
    Dim RISK_SumOfImpact As Double
    Dim RISK_SumOfUsage As Double
    Dim ThisInstrumentValue As Double
    Dim ThisRiskLimit As Double
    Dim ThisCurrencyID As Integer

    ' Establish Ordinals
    Try
      RISK_TransactionFund = tblInstrumentExposures.Columns.IndexOf("Fund")
      RISK_limitID = tblInstrumentExposures.Columns.IndexOf("limID")
      RISK_TransactionInstrument = tblInstrumentExposures.Columns.IndexOf("Instrument")
      RISK_InstrumentFundType = tblInstrumentExposures.Columns.IndexOf("InstrumentFundType")
      RISK_FundTypeDescription = tblInstrumentExposures.Columns.IndexOf("FundTypeDescription")
      RISK_InstrumentValue = tblInstrumentExposures.Columns.IndexOf("Value2")
      RISK_InstrumentWorstCase = tblInstrumentExposures.Columns.IndexOf("InstrumentWorstCase")
      RISK_limType = tblInstrumentExposures.Columns.IndexOf("limType")
      RISK_limGreaterOrLessThan = tblInstrumentExposures.Columns.IndexOf("limGreaterOrLessThan")
      RISK_limLimit = tblInstrumentExposures.Columns.IndexOf("limLimit")
      RISK_InstrumentCurrencyID = tblInstrumentExposures.Columns.IndexOf("InstrumentCurrencyID")
      RISK_InstrumentCurrency = tblInstrumentExposures.Columns.IndexOf("InstrumentCurrency")
      RISK_EndPrice = tblInstrumentExposures.Columns.IndexOf("EndPrice")
      RISK_FinalUnits = tblInstrumentExposures.Columns.IndexOf("FinalUnits")
      RISK_limCurrency = tblInstrumentExposures.Columns.IndexOf("limInstrument")

      RISK_limOnPremium = tblInstrumentExposures.Columns.IndexOf("limOnPremium")
      RISK_InstrumentHasDelta = tblInstrumentExposures.Columns.IndexOf("InstrumentTypeHasDelta")
      RISK_InstrumentDelta = tblInstrumentExposures.Columns.IndexOf("BestDelta")
      RISK_InstrumentContractSize = tblInstrumentExposures.Columns.IndexOf("InstrumentContractSize")
      RISK_InstrumentMultiplier = tblInstrumentExposures.Columns.IndexOf("InstrumentMultiplier")
      RISK_InstrumentUnderlyingPrice = tblInstrumentExposures.Columns.IndexOf("UnderlyingPrice")
      RISK_InstrumentSharesPerOption = tblInstrumentExposures.Columns.IndexOf("InstrumentSharesPerOption")
      RISK_FXRate2 = tblInstrumentExposures.Columns.IndexOf("FXRate2") ' Compound FX Rate Instrument to Fund FX
    Catch ex As Exception
      MainForm.LogError("ProcessInstrumentCountLimits", LOG_LEVELS.Error, ex.Message, "Error getting Field Ordinals from tblInstrumentExposures", ex.StackTrace, True)
      Return False
    End Try

    Try
      MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION), AdpExposure, "tblExposure")
    Catch ex As Exception
      MainForm.LogError("ProcessInstrumentCountLimits", LOG_LEVELS.Error, ex.Message, "Error configuring tblExposure Adaptor.", ex.StackTrace, True)
      Return False
    End Try

    ' Select appropriate Exposure Records
    Try
      SelectedInstrumentExposures = tblInstrumentExposures.Select("limType = " & CInt(RenaissanceGlobals.LimitType.CurrencyExposure).ToString, "Fund, limType, limGreaterOrLessThan, limLimit, limInstrument") ' limInstrument is the Limit currency

      If (SelectedInstrumentExposures Is Nothing) OrElse (SelectedInstrumentExposures.Length <= 0) Then
        Return True
      End If
    Catch ex As Exception
      MainForm.LogError("ProcessInstrumentCountLimits", LOG_LEVELS.Error, ex.Message, "Error selecting tblInstrumentExposures rows.", ex.StackTrace, True)
      Return False
    End Try

    ' Process selected Exposures
    ' 
    ' Calculate Calculated Fields :  Instrument Weight, Impact, Usage and Status
    '           Weight = 1
    '           Impact = 1
    '           Usage  = [Weight] / [Limit]                (or inverse depending on Limit definition)
    '           Status = "OK", "Warning" or "Break"

    RISK_SumOfWeight = 0
    RISK_SumOfImpact = 0

    For Counter = 0 To (SelectedInstrumentExposures.Length - 1)
      Try
        thisInstrumentExposure = SelectedInstrumentExposures(Counter)
        ThisCurrencyID = CInt(thisInstrumentExposure(RISK_InstrumentCurrencyID))

        If (ExposureRow Is Nothing) Then
          ExposureRow = tblExposure.NewtblExposureRow

          ExposureRow.expFund = FundId
          ExposureRow.expLimitID = CInt(thisInstrumentExposure(RISK_limitID))
          ExposureRow.expIsDetailRow = 0
          ExposureRow.expLimitSector = 0
          ExposureRow.expLimitSectorName = ""
          ExposureRow.expLimitInstrument = CInt(thisInstrumentExposure(RISK_limCurrency))
          ExposureRow.expLimitType = CInt(thisInstrumentExposure(RISK_limType))
          ExposureRow.expLimitGreaterOrLessThan = CInt(thisInstrumentExposure(RISK_limGreaterOrLessThan))
          ExposureRow.expLimit = CDbl(thisInstrumentExposure(RISK_limLimit))
          ExposureRow.expExposure = RISK_SumOfWeight
          ExposureRow.expUsage = RISK_SumOfUsage
          ExposureRow.expStatus = RISK_Status
          ExposureRow.expDate = pDateToRisk
          ExposureRow.expDateCalculated = CalculationDate

          ExposureRow.expKnowledgeDate = pPreciseExposureKnowledgedate
          If (pPreciseExposureKnowledgedate = KNOWLEDGEDATE_NOW) OrElse (pPreciseExposureKnowledgedate.TimeOfDay.TotalSeconds >= LAST_SECOND) Then
            ExposureRow.expKnowledgeDateWholeday = (-1)
          Else
            ExposureRow.expKnowledgeDateWholeday = 0
          End If

          ExposureRow.expSystemKnowledgeDate = MainForm.Main_Knowledgedate
          If (MainForm.Main_Knowledgedate = KNOWLEDGEDATE_NOW) OrElse (MainForm.Main_Knowledgedate.TimeOfDay.TotalSeconds >= LAST_SECOND) Then
            ExposureRow.expSystemKnowledgeDateWholeday = (-1)
          Else
            ExposureRow.expSystemKnowledgeDateWholeday = 0
          End If

          ExposureRow.expFlags = ExposureFlags
        End If

        ThisInstrumentValue = CDbl(thisInstrumentExposure(RISK_FinalUnits)) * CDbl(thisInstrumentExposure(RISK_EndPrice)) * CDbl(thisInstrumentExposure(RISK_InstrumentContractSize)) * CDbl(thisInstrumentExposure(RISK_InstrumentMultiplier))

        ' Calculate Underlying value if necessary
        If (CBool(thisInstrumentExposure(RISK_limOnPremium)) = False) AndAlso CBool(thisInstrumentExposure(RISK_InstrumentHasDelta)) Then
          ThisInstrumentValue = CDbl(thisInstrumentExposure(RISK_FinalUnits)) * CDbl(thisInstrumentExposure(RISK_InstrumentDelta)) * CDbl(thisInstrumentExposure(RISK_InstrumentContractSize)) * CDbl(thisInstrumentExposure(RISK_InstrumentMultiplier)) * CDbl(thisInstrumentExposure(RISK_InstrumentSharesPerOption)) * CDbl(thisInstrumentExposure(RISK_InstrumentUnderlyingPrice))
        End If

        If (ThisCurrencyID = CInt(thisInstrumentExposure(RISK_limCurrency))) Then
          RISK_Weight = ThisInstrumentValue
          RISK_Impact = ThisInstrumentValue
        Else
          RISK_Weight = 0
          RISK_Impact = 0
        End If

        '' Roll Up Sector Totals
        RISK_SumOfWeight += RISK_Weight
        RISK_SumOfImpact += RISK_Impact

        ThisRiskLimit = CDbl(thisInstrumentExposure(RISK_limLimit))

        If CInt(thisInstrumentExposure(RISK_limGreaterOrLessThan)) = RenaissanceGlobals.Relationship.LessThan Then

          If (ThisRiskLimit = 0.0#) Then
            RISK_SumOfUsage = Math.Min(2.0#, Math.Max(-2.0#, (RISK_SumOfWeight / 0.05#) + 1.0#))

            'If (RISK_SumOfWeight < ThisRiskLimit) Then
            '	RISK_SumOfUsage = 0.0#
            'Else
            '	RISK_SumOfUsage = 1.0#
            'End If

            If (RISK_Weight < ThisRiskLimit) Then
              RISK_Usage = 0.0#
            Else
              RISK_Usage = 1.0#
            End If
          Else
            RISK_SumOfUsage = 1.0# - ((ThisRiskLimit - RISK_SumOfWeight) / Math.Abs(ThisRiskLimit))
            RISK_Usage = 1.0# - ((ThisRiskLimit - RISK_Weight) / Math.Abs(ThisRiskLimit))
          End If

          ' RISK_Usage = RISK_SumOfWeight / CDbl(thisInstrumentExposure(RISK_limLimit))
          ' RISK_Usage = RISK_Weight / CDbl(thisInstrumentExposure(RISK_limLimit))
        Else ' Greater than
          If (ThisRiskLimit = 0.0#) Then

            RISK_SumOfUsage = Math.Min(2.0#, Math.Max(-2.0#, ((-RISK_SumOfWeight) / 0.05#) + 1.0#))

            'If (RISK_SumOfWeight > ThisRiskLimit) Then
            '	RISK_SumOfUsage = 0.0#
            'Else
            '	RISK_SumOfUsage = 1.0#
            'End If

            RISK_Usage = 0.0#

          Else ' (ThisRiskLimit != 0.0#)
            RISK_SumOfUsage = 1.0# - ((RISK_SumOfWeight - ThisRiskLimit) / Math.Abs(ThisRiskLimit))
            RISK_Usage = 1.0# - ((RISK_Weight - ThisRiskLimit) / Math.Abs(ThisRiskLimit))
          End If

        End If

        ExposureRow.expExposure = RISK_SumOfWeight
        ExposureRow.expUsage = Math.Max(RISK_SumOfUsage, 0.0#)
        ExposureRow.expStatus = RISK_Status

        ' Detail Row.

        If (Math.Abs(RISK_Weight) > 0) Then

          ExposureDetailRow = tblExposure.NewtblExposureRow

          ExposureDetailRow.expFund = ExposureRow.expFund
          ExposureDetailRow.expLimitID = CInt(thisInstrumentExposure(RISK_limitID))
          ExposureDetailRow.expIsDetailRow = 1
          ExposureDetailRow.expLimitSector = ExposureRow.expLimitSector
          ExposureDetailRow.expLimitSectorName = ExposureRow.expLimitSectorName
          ExposureDetailRow.expLimitInstrument = ExposureRow.expLimitInstrument
          ExposureDetailRow.expLimitType = ExposureRow.expLimitType
          ExposureDetailRow.expLimitCharacteristic = ExposureRow.expLimitCharacteristic
          ExposureDetailRow.expLimitGreaterOrLessThan = ExposureRow.expLimitGreaterOrLessThan
          ExposureDetailRow.expLimit = ExposureRow.expLimit
          ExposureDetailRow.expDetailInstrument = CInt(thisInstrumentExposure(RISK_TransactionInstrument))
          ExposureDetailRow.expExposure = RISK_Weight
          ExposureDetailRow.expUsage = RISK_Usage
          ExposureDetailRow.expStatus = RISK_Status
          ExposureDetailRow.expDate = ExposureRow.expDate
          ExposureDetailRow.expDateCalculated = ExposureRow.expDateCalculated
          ExposureDetailRow.expKnowledgeDate = ExposureRow.expKnowledgeDate
          ExposureDetailRow.expKnowledgeDateWholeday = ExposureRow.expKnowledgeDateWholeday
          ExposureDetailRow.expSystemKnowledgeDate = ExposureRow.expSystemKnowledgeDate
          ExposureDetailRow.expSystemKnowledgeDateWholeday = ExposureRow.expSystemKnowledgeDateWholeday
          ExposureDetailRow.expFlags = ExposureRow.expFlags

          ' Save Detail Row

          If (Not (ExposureDetailRow Is Nothing)) Then
            tblExposure.Rows.Add(ExposureDetailRow)
            ExposureDetailRow = Nothing
          End If

        End If

        If (Not (ExposureRow Is Nothing)) Then
          If (Counter >= (SelectedInstrumentExposures.Length - 1)) Then
            tblExposure.Rows.Add(ExposureRow)

            RISK_SumOfWeight = 0
            RISK_SumOfImpact = 0
            ExposureRow = Nothing

          Else
            nextInstrumentExposure = SelectedInstrumentExposures(Counter + 1)

            If (CInt(thisInstrumentExposure(RISK_TransactionFund)) <> CInt(nextInstrumentExposure(RISK_TransactionFund))) OrElse _
             (CInt(thisInstrumentExposure(RISK_limType)) <> CInt(nextInstrumentExposure(RISK_limType))) OrElse _
             (CInt(thisInstrumentExposure(RISK_limGreaterOrLessThan)) <> CInt(nextInstrumentExposure(RISK_limGreaterOrLessThan))) OrElse _
             (CDbl(thisInstrumentExposure(RISK_limLimit)) <> CDbl(nextInstrumentExposure(RISK_limLimit))) OrElse _
             (CInt(thisInstrumentExposure(RISK_limCurrency)) <> CInt(nextInstrumentExposure(RISK_limCurrency))) Then
              tblExposure.Rows.Add(ExposureRow)

              RISK_SumOfWeight = 0
              RISK_SumOfImpact = 0
              ExposureRow = Nothing
            End If
          End If
        End If
      Catch ex As Exception
      End Try
    Next

    ' Save New Exposure Records.
    Try
      If tblExposure.Rows.Count > 0 Then
        MainForm.AdaptorUpdate("RiskExposureFunctions", AdpExposure, tblExposure)
      End If
    Catch ex As Exception
      MainForm.LogError("ProcessSectorWeightLimits", LOG_LEVELS.Error, ex.Message, "Error updating Sector Weight Usage Limits.", ex.StackTrace, True)
      Return False
    End Try

    Return True

  End Function

  ''' <summary>
  ''' Processes the category exposure limits.
  ''' </summary>
  ''' <param name="MainForm">The main form.</param>
  ''' <param name="tblInstrumentExposures">The TBL instrument exposures.</param>
  ''' <param name="FundId">The fund id.</param>
  ''' <param name="Fund_Value">The fund_ value.</param>
  ''' <param name="pDateToPertrac">The p date to pertrac.</param>
  ''' <param name="pDateToRisk">The p date to risk.</param>
  ''' <param name="CalculationDate">The calculation date.</param>
  ''' <param name="pPreciseExposureKnowledgedate">The p precise exposure knowledgedate.</param>
  ''' <param name="ExposureFlags">The exposure flags.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function ProcessCategoryExposureLimits(ByRef MainForm As VeniceMain, ByRef tblInstrumentExposures As DataTable, ByVal FundId As Integer, ByVal Fund_Value As Double, ByVal pDateToPertrac As Date, ByVal pDateToRisk As Date, ByVal CalculationDate As Date, ByVal pPreciseExposureKnowledgedate As Date, ByVal ExposureFlags As Integer) As Boolean

    ' Exit if no Exposure Data

    Try
      If (tblInstrumentExposures Is Nothing) OrElse (tblInstrumentExposures.Rows.Count <= 0) Then
        Return True
      End If
    Catch ex As Exception
      Return False
    End Try

    ' 
    Dim SelectedCategoryExposures As DataRow()
    Dim thisCategoryExposure As DataRow
    Dim CheckCategoryExposure As DataRow
    Dim nextCategoryExposure As DataRow
    Dim Counter As Integer
    Dim NewExposureRecords As New ArrayList
    Dim thisLimitValue As Double

    Dim DSExposure As New RenaissanceDataClass.DSExposure
    Dim tblExposure As RenaissanceDataClass.DSExposure.tblExposureDataTable
    Dim ExposureRow As RenaissanceDataClass.DSExposure.tblExposureRow = Nothing
    Dim ExposureDetailRow As RenaissanceDataClass.DSExposure.tblExposureRow = Nothing
    Dim AdpExposure As New SqlDataAdapter

    Dim ThisFundID As Integer
    Dim ThisCharacteristicID As Integer
    Dim ThisLessThanOrGreaterThan As Integer
    Dim ThisLimitIDTested As Integer
    Dim ThisGeneralLimitDepricated As Boolean = False
    Dim CheckLimitCounter As Integer

    ExposureRow = Nothing

    Try
      tblExposure = DSExposure.tblExposure
    Catch ex As Exception
      MainForm.LogError("ProcessCategoryExposureLimits", LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
      Return False
    End Try

    Dim RISK_TransactionFund As Integer
    Dim RISK_TransactionInstrument As Integer
    Dim RISK_InstrumentFundType As Integer
    Dim RISK_FundTypeDescription As Integer
    Dim RISK_InstrumentValue As Integer
    Dim RISK_InstrumentWorstCase As Integer
    Dim RISK_limType As Integer
    Dim RISK_limitID As Integer
    Dim RISK_limFund As Integer
    Dim RISK_limCharacteristic As Integer
    Dim RISK_limGreaterOrLessThan As Integer
    Dim RISK_limLimit As Integer
    Dim RISK_limIsPercent As Integer
    Dim RISK_DataCategoryId As Integer
    Dim RISK_CategoryWeighting As Integer

    Dim RISK_limOnPremium As Integer
    Dim RISK_InstrumentHasDelta As Integer
    Dim RISK_InstrumentDelta As Integer
    Dim RISK_InstrumentContractSize As Integer
    Dim RISK_InstrumentMultiplier As Integer
    Dim RISK_InstrumentUnderlyingPrice As Integer
    Dim RISK_InstrumentSharesPerOption As Integer
    Dim RISK_FXRate2 As Integer
    Dim RISK_FinalUnits As Integer

    Dim RISK_Weight As Double
    Dim RISK_Usage As Double
    Dim RISK_SumOfUsage As Double
    Dim RISK_Status As String
    Dim RISK_SumOfWeight As Double

    Dim ThisInstrumentValue As Double

    ' Establish Ordinals
    Try
      RISK_TransactionFund = tblInstrumentExposures.Columns.IndexOf("Fund")
      RISK_limitID = tblInstrumentExposures.Columns.IndexOf("limID")
      RISK_limFund = tblInstrumentExposures.Columns.IndexOf("limFund")
      RISK_TransactionInstrument = tblInstrumentExposures.Columns.IndexOf("Instrument")
      RISK_InstrumentFundType = tblInstrumentExposures.Columns.IndexOf("InstrumentFundType")
      RISK_FundTypeDescription = tblInstrumentExposures.Columns.IndexOf("FundTypeDescription")
      RISK_InstrumentValue = tblInstrumentExposures.Columns.IndexOf("Value2")
      RISK_InstrumentWorstCase = tblInstrumentExposures.Columns.IndexOf("InstrumentWorstCase")
      RISK_limType = tblInstrumentExposures.Columns.IndexOf("limType")
      RISK_limCharacteristic = tblInstrumentExposures.Columns.IndexOf("limCharacteristic")
      RISK_limGreaterOrLessThan = tblInstrumentExposures.Columns.IndexOf("limGreaterOrLessThan")
      RISK_limLimit = tblInstrumentExposures.Columns.IndexOf("limLimit")
      RISK_limIsPercent = tblInstrumentExposures.Columns.IndexOf("limIsPercent")

      RISK_limOnPremium = tblInstrumentExposures.Columns.IndexOf("limOnPremium")
      RISK_InstrumentHasDelta = tblInstrumentExposures.Columns.IndexOf("InstrumentTypeHasDelta")
      RISK_InstrumentDelta = tblInstrumentExposures.Columns.IndexOf("BestDelta")
      RISK_InstrumentContractSize = tblInstrumentExposures.Columns.IndexOf("InstrumentContractSize")
      RISK_InstrumentMultiplier = tblInstrumentExposures.Columns.IndexOf("InstrumentMultiplier")
      RISK_InstrumentUnderlyingPrice = tblInstrumentExposures.Columns.IndexOf("UnderlyingPrice")
      RISK_InstrumentSharesPerOption = tblInstrumentExposures.Columns.IndexOf("InstrumentSharesPerOption")
      RISK_FXRate2 = tblInstrumentExposures.Columns.IndexOf("FXRate2") ' Compound FX Rate Instrument to Fund FX
      RISK_FinalUnits = tblInstrumentExposures.Columns.IndexOf("FinalUnits") ' SignedUnits from 2nd PnL Valuation query.

      RISK_DataCategoryId = tblInstrumentExposures.Columns.IndexOf("DataCategoryId")
      RISK_CategoryWeighting = tblInstrumentExposures.Columns.IndexOf("CategoryWeighting")

    Catch ex As Exception
      MainForm.LogError("ProcessCategoryExposureLimits", LOG_LEVELS.Error, ex.Message, "Error getting Field Ordinals from tblInstrumentExposures", ex.StackTrace, True)
      Return False
    End Try

    Try
      MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION), AdpExposure, "tblExposure")
    Catch ex As Exception
      MainForm.LogError("ProcessCategoryExposureLimits", LOG_LEVELS.Error, ex.Message, "Error configuring tblExposure Adaptor.", ex.StackTrace, True)
      Return False
    End Try

    ' Select appropriate Exposure Records
    Try
      SelectedCategoryExposures = tblInstrumentExposures.Select("(limType = " & CInt(RenaissanceGlobals.LimitType.Characteristic).ToString & ")", "Fund, limType, limCharacteristic, limGreaterOrLessThan, limLimit, limID, InstrumentParent, Instrument")

      If (SelectedCategoryExposures Is Nothing) OrElse (SelectedCategoryExposures.Length <= 0) Then
        Return True
      End If
    Catch ex As Exception
      MainForm.LogError("ProcessCategoryExposureLimits", LOG_LEVELS.Error, ex.Message, "Error selecting tblInstrumentExposures rows.", ex.StackTrace, True)
      Return False
    End Try


    ' Process selected Exposures
    ' 
    ' Calculate Calculated Fields :  Instrument Weight, Impact, Usage and Status
    '           Weight = [InstrumentValue] / [Fund Value]
    '           Impact = [Weight] * [WorstCase] * 10000    (Note : * 10000 to convert to bps)
    '           Usage  = [Weight] / [Limit]                (or inverse depending on Limit definition)
    '           Status = "OK", "Warning" or "Break"

    RISK_SumOfWeight = 0

    Dim limID As Integer
    For Counter = 0 To (SelectedCategoryExposures.Length - 1)
      Try
        thisCategoryExposure = SelectedCategoryExposures(Counter)
        thisLimitValue = CDbl(thisCategoryExposure(RISK_limLimit))
        ThisFundID = CInt(thisCategoryExposure(RISK_TransactionFund))
        ThisCharacteristicID = CInt(thisCategoryExposure(RISK_limCharacteristic))
        ThisLessThanOrGreaterThan = CInt(thisCategoryExposure(RISK_limGreaterOrLessThan))
        limID = CInt(thisCategoryExposure("limID"))
        ' Ignore 'All Fund' limits in favour of 'Specific Fund' Limits
        If (CInt(thisCategoryExposure(RISK_limFund)) = 0) Then
          If (ThisLimitIDTested <> limID) Then
            ' Check for Specific Fund Limit.
            ThisGeneralLimitDepricated = False

            For CheckLimitCounter = 0 To (SelectedCategoryExposures.Length - 1)
              CheckCategoryExposure = SelectedCategoryExposures(CheckLimitCounter)

              If (CheckCategoryExposure(RISK_limFund) = ThisFundID) Then

                If (CheckCategoryExposure(RISK_limCharacteristic) = ThisCharacteristicID) AndAlso _
                 (CheckCategoryExposure(RISK_limGreaterOrLessThan) = ThisLessThanOrGreaterThan) Then

                  ThisGeneralLimitDepricated = True
                  Exit For
                End If
              End If

            Next

            ThisLimitIDTested = CInt(thisCategoryExposure("limID"))

          End If
        Else
          ThisGeneralLimitDepricated = False
        End If

        If (ThisGeneralLimitDepricated) Then
          Continue For
        End If

        ' Ignore Small Value Exposures

        ThisInstrumentValue = CDbl(thisCategoryExposure(RISK_InstrumentValue))

        If (CBool(thisCategoryExposure(RISK_limOnPremium)) = False) AndAlso CBool(thisCategoryExposure(RISK_InstrumentHasDelta)) Then
          ThisInstrumentValue = CDbl(thisCategoryExposure(RISK_FinalUnits)) * CDbl(thisCategoryExposure(RISK_InstrumentDelta)) * CDbl(thisCategoryExposure(RISK_InstrumentContractSize)) * CDbl(thisCategoryExposure(RISK_InstrumentMultiplier)) * CDbl(thisCategoryExposure(RISK_InstrumentSharesPerOption)) * CDbl(thisCategoryExposure(RISK_InstrumentUnderlyingPrice)) * CDbl(thisCategoryExposure(RISK_FXRate2))
        End If

        If (Math.Abs(ThisInstrumentValue) >= 1) Then

          If (ExposureRow Is Nothing) Then
            ' Initial record.
            ExposureRow = tblExposure.NewtblExposureRow
          End If

          If Fund_Value = 0 Then
            RISK_Weight = 1
          Else
            If CInt(thisCategoryExposure(RISK_limIsPercent)) Then
              RISK_Weight = ThisInstrumentValue * CDbl(thisCategoryExposure(RISK_CategoryWeighting)) / Fund_Value
            Else
              RISK_Weight = ThisInstrumentValue * CDbl(thisCategoryExposure(RISK_CategoryWeighting))
            End If
          End If

          '' Roll Up Characteristic Totals
          RISK_SumOfWeight += RISK_Weight

          If thisLimitValue = 0.0# Then
            RISK_SumOfUsage = 0
            RISK_Usage = 0

            If ThisLessThanOrGreaterThan = RenaissanceGlobals.Relationship.LessThan Then

              RISK_SumOfUsage = Math.Min(2.0#, Math.Max(0.0#, (RISK_SumOfWeight / 0.05#) + 1.0#))
              RISK_Usage = Math.Min(2.0#, Math.Max(-2.0#, (RISK_Weight / 0.05#) + 1.0#))

              ' It has been requested that Zero Weight for Zero limits represents Zero Exposure
              If (RISK_SumOfWeight <= 0.0#) Then
                RISK_SumOfUsage = 0.0#
              End If

              If (RISK_Weight <= 0.0#) Then
                RISK_Usage = 0.0#
              End If

              'If (RISK_SumOfWeight > 0.0#) Then
              '	RISK_SumOfUsage = 2.0#
              'End If
              'If (RISK_Weight > 0.0#) Then
              '	RISK_Usage = 2.0#
              'End If

            Else ' Limit = 0, Type = GreaterThan

              RISK_SumOfUsage = Math.Min(2.0#, Math.Max(0.0#, ((-RISK_SumOfWeight) / 0.05#) + 1.0#))
              RISK_Usage = Math.Min(2.0#, Math.Max(-2.0#, ((-RISK_Weight) / 0.05#) + 1.0#))

              ' It has been requested that Zero Weight for Zero limits represents Zero Exposure
              If (RISK_SumOfWeight >= 0.0#) Then
                RISK_SumOfUsage = 0.0#
              End If

              If (RISK_Weight >= 0.0#) Then
                RISK_Usage = 0.0#
              End If

              'If (RISK_SumOfWeight < 0.0#) Then
              '	RISK_SumOfUsage = 2.0#
              'End If
              'If (RISK_Weight < 0.0#) Then
              '	RISK_Usage = 0.0#
              'End If

            End If
          Else ' thisLimitValue <> 0.0# 
            If ThisLessThanOrGreaterThan = RenaissanceGlobals.Relationship.LessThan Then

              'RISK_SumOfUsage = RISK_SumOfWeight / thisLimitValue
              'RISK_Usage = RISK_Weight / thisLimitValue

              RISK_SumOfUsage = Math.Min(2.0#, Math.Max(0.0#, ((RISK_SumOfWeight - thisLimitValue) / Math.Abs(thisLimitValue)) + 1.0#))
              RISK_Usage = Math.Min(2.0#, Math.Max(-2.0#, ((RISK_Weight - thisLimitValue) / Math.Abs(thisLimitValue)) + 1.0#))

            Else ' RenaissanceGlobals.Relationship.GreaterThan

              RISK_SumOfUsage = Math.Min(2.0#, Math.Max(0.0#, ((thisLimitValue - RISK_SumOfWeight) / Math.Abs(thisLimitValue)) + 1.0#))
              RISK_Usage = Math.Min(2.0#, Math.Max(-2.0#, ((thisLimitValue - RISK_Weight) / Math.Abs(thisLimitValue)) + 1.0#))

              'If (Math.Abs(RISK_SumOfWeight) > 0.000001) Then
              '	RISK_SumOfUsage = thisLimitValue / RISK_SumOfWeight

              '	If (Math.Abs(RISK_Weight) > 0.000001) Then
              '		RISK_Usage = RISK_Weight / thisLimitValue	' Note Different calc for detail risk.
              '	Else
              '		RISK_Usage = 0.0#
              '	End If
              'Else
              '	RISK_SumOfUsage = 2.0#
              '	RISK_Usage = 2.0#
              'End If

            End If
          End If



          If (RISK_SumOfUsage > 1) Then
            RISK_Status = "Break"
          ElseIf (RISK_SumOfUsage >= 0.75) Then
            RISK_Status = "Warning"
          Else
            RISK_Status = "OK"
          End If

          ExposureRow.expFund = CInt(thisCategoryExposure(RISK_TransactionFund))
          ExposureRow.expLimitID = CInt(thisCategoryExposure(RISK_limitID))
          ExposureRow.expIsDetailRow = 0
          ExposureRow.expLimitSector = 0
          ExposureRow.expLimitSectorName = ""
          ExposureRow.expLimitInstrument = 0
          ExposureRow.expLimitType = CInt(thisCategoryExposure(RISK_limType))
          ExposureRow.expLimitCharacteristic = ThisCharacteristicID
          ExposureRow.expLimitGreaterOrLessThan = ThisLessThanOrGreaterThan
          ExposureRow.expLimit = thisLimitValue
          ExposureRow.expDetailInstrument = 0
          ExposureRow.expExposure = RISK_SumOfWeight
          ExposureRow.expUsage = RISK_SumOfUsage
          ExposureRow.expStatus = RISK_Status
          ExposureRow.expDate = pDateToRisk
          ExposureRow.expDateCalculated = CalculationDate

          ExposureRow.expKnowledgeDate = pPreciseExposureKnowledgedate
          If (pPreciseExposureKnowledgedate = KNOWLEDGEDATE_NOW) OrElse (pPreciseExposureKnowledgedate.TimeOfDay.TotalSeconds >= LAST_SECOND) Then
            ExposureRow.expKnowledgeDateWholeday = (-1)
          Else
            ExposureRow.expKnowledgeDateWholeday = 0
          End If

          ExposureRow.expSystemKnowledgeDate = MainForm.Main_Knowledgedate
          If (MainForm.Main_Knowledgedate = KNOWLEDGEDATE_NOW) OrElse (MainForm.Main_Knowledgedate.TimeOfDay.TotalSeconds >= LAST_SECOND) Then
            ExposureRow.expSystemKnowledgeDateWholeday = (-1)
          Else
            ExposureRow.expSystemKnowledgeDateWholeday = 0
          End If

          ExposureRow.expFlags = ExposureFlags

          ' Detail Row.

          If (Math.Abs(RISK_Weight) > 0) Then

            ExposureDetailRow = tblExposure.NewtblExposureRow

            ExposureDetailRow.expFund = ExposureRow.expFund
            ExposureDetailRow.expLimitID = CInt(thisCategoryExposure(RISK_limitID))
            ExposureDetailRow.expIsDetailRow = 1
            ExposureDetailRow.expLimitSector = ExposureRow.expLimitSector
            ExposureDetailRow.expLimitSectorName = ExposureRow.expLimitSectorName
            ExposureDetailRow.expLimitInstrument = ExposureRow.expLimitInstrument
            ExposureDetailRow.expLimitType = ExposureRow.expLimitType
            ExposureDetailRow.expLimitCharacteristic = ExposureRow.expLimitCharacteristic
            ExposureDetailRow.expLimitGreaterOrLessThan = ExposureRow.expLimitGreaterOrLessThan
            ExposureDetailRow.expLimit = ExposureRow.expLimit
            ExposureDetailRow.expDetailInstrument = CInt(thisCategoryExposure(RISK_TransactionInstrument))
            ExposureDetailRow.expExposure = RISK_Weight
            ExposureDetailRow.expUsage = RISK_Usage
            ExposureDetailRow.expStatus = RISK_Status
            ExposureDetailRow.expDate = ExposureRow.expDate
            ExposureDetailRow.expDateCalculated = ExposureRow.expDateCalculated
            ExposureDetailRow.expKnowledgeDate = ExposureRow.expKnowledgeDate
            ExposureDetailRow.expKnowledgeDateWholeday = ExposureRow.expKnowledgeDateWholeday
            ExposureDetailRow.expSystemKnowledgeDate = ExposureRow.expSystemKnowledgeDate
            ExposureDetailRow.expSystemKnowledgeDateWholeday = ExposureRow.expSystemKnowledgeDateWholeday
            ExposureDetailRow.expFlags = ExposureRow.expFlags

            ' Save Detail Row

            If (Not (ExposureDetailRow Is Nothing)) Then
              NewExposureRecords.Add(ExposureDetailRow)
              ExposureDetailRow = Nothing
            End If

          End If


        End If ' Ignore small Exposures

        ' Save This Row ?
        ' Save if this is the last Exposure Row OR 
        ' If we have reached the end of a section (Fund, Limit Type, Limit, Condition or FundType has changed)

        If (Not (ExposureRow Is Nothing)) Then
          If (Counter >= (SelectedCategoryExposures.Length - 1)) Then
            NewExposureRecords.Add(ExposureRow)

            RISK_SumOfWeight = 0
            ExposureRow = Nothing

          Else
            nextCategoryExposure = SelectedCategoryExposures(Counter + 1)

            If (CInt(thisCategoryExposure(RISK_limitID)) <> CInt(nextCategoryExposure(RISK_limitID))) OrElse _
             (CInt(thisCategoryExposure(RISK_TransactionFund)) <> CInt(nextCategoryExposure(RISK_TransactionFund))) OrElse _
             (CInt(thisCategoryExposure(RISK_limType)) <> CInt(nextCategoryExposure(RISK_limType))) OrElse _
             (ThisCharacteristicID <> CInt(nextCategoryExposure(RISK_limCharacteristic))) OrElse _
             (ThisLessThanOrGreaterThan <> CInt(nextCategoryExposure(RISK_limGreaterOrLessThan))) OrElse _
             (thisLimitValue <> CDbl(nextCategoryExposure(RISK_limLimit))) Then

              NewExposureRecords.Add(ExposureRow)

              RISK_SumOfWeight = 0
              ThisLimitIDTested = False
              ThisGeneralLimitDepricated = False
              ExposureRow = Nothing
            End If
          End If
        End If ' (Not (ExposureRow Is Nothing)) Then

      Catch ex As Exception
        MainForm.LogError("ProcessCategoryExposureLimits", LOG_LEVELS.Error, ex.Message, "Error calculating Category Exposure Limits.", ex.StackTrace, True)
        Return False
      End Try
    Next

    ' Now Add the specific exposures to the Exposures table, then add those general exposures which 
    ' are not covered by a specific limit.

    Dim OuterCount As Integer

    For OuterCount = 0 To (NewExposureRecords.Count - 1)
      Try
        ExposureRow = NewExposureRecords(OuterCount)

        If Math.Abs(ExposureRow.expExposure) >= RISK_MINIMUMEXPOSURETOSAVE Then
          tblExposure.Rows.Add(ExposureRow)
        End If

      Catch ex As Exception
        MainForm.LogError("ProcessCategoryExposureLimits", LOG_LEVELS.Error, ex.Message, "Error saving Category Exposure Limits.", ex.StackTrace, True)
        Return False
      End Try
    Next

    ' Save New Exposure Records.
    Try
      If tblExposure.Rows.Count > 0 Then
        MainForm.AdaptorUpdate("RiskExposureFunctions", AdpExposure, tblExposure)
      End If
    Catch ex As Exception
      MainForm.LogError("ProcessCategoryExposureLimits", LOG_LEVELS.Error, ex.Message, "Error updating Category Exposure Limits.", ex.StackTrace, True)
      Return False
    End Try

    Return True

  End Function
  ''' <summary>
  ''' Lotfi: Processes the 5-10-40 exposure limits.
  ''' lists the instruments weights campared to the 5%, 10% limits 
  ''' and adds a row with the sum of weight bifgger thant 5%, it campares that sum to 40%
  ''' 
  ''' </summary>
  ''' <param name="MainForm">The main form.</param>
  ''' <param name="tblInstrumentExposures">The TBL instrument exposures.</param>
  ''' <param name="FundId">The fund id.</param>
  ''' <param name="Fund_Value">The fund_ value.</param>
  ''' <param name="pDateToPertrac">The p date to pertrac.</param>
  ''' <param name="pDateToRisk">The p date to risk.</param>
  ''' <param name="CalculationDate">The calculation date.</param>
  ''' <param name="pPreciseExposureKnowledgedate">The p precise exposure knowledgedate.</param>
  ''' <param name="ExposureFlags">The exposure flags.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function ProcessCinqDixQuarenteExposureLimits(ByRef MainForm As VeniceMain, ByRef tblInstrumentExposures As DataTable, ByVal FundId As Integer, ByVal Fund_Value As Double, ByVal pDateToPertrac As Date, ByVal pDateToRisk As Date, ByVal CalculationDate As Date, ByVal pPreciseExposureKnowledgedate As Date, ByVal ExposureFlags As Integer) As Boolean


    Try
      If (tblInstrumentExposures Is Nothing) OrElse (tblInstrumentExposures.Rows.Count <= 0) Then
        Return True
      End If
    Catch ex As Exception
      Return False
    End Try

    ' 
    Dim SelectedInstrumentExposures As DataRow()
    Dim thisInstrumentExposure As DataRow
    Dim Counter As Integer
    Dim NewExposureRecords As New ArrayList
    Dim thisLimitValue As Double

    Dim DSExposure As New RenaissanceDataClass.DSExposure
    Dim tblExposure As RenaissanceDataClass.DSExposure.tblExposureDataTable
    Dim ExposureRow As RenaissanceDataClass.DSExposure.tblExposureRow = Nothing
    Dim ExposureDetailRow As RenaissanceDataClass.DSExposure.tblExposureRow = Nothing
    Dim AdpExposure As New SqlDataAdapter

    Dim ThisFundID As Integer
    Dim ThisCharacteristicID As Integer
    Dim ThisLessThanOrGreaterThan As Integer
    Dim ThisGeneralLimitDepricated As Boolean = False

    ExposureRow = Nothing

    Try
      tblExposure = DSExposure.tblExposure
    Catch ex As Exception
      MainForm.LogError("ProcessCinqDixQuarenteExposureLimits", LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
      Return False
    End Try

    Dim RISK_TransactionFund As Integer
    Dim RISK_TransactionInstrument As Integer
    Dim RISK_InstrumentFundType As Integer
    Dim RISK_InstrumentType As Integer
    Dim RISK_FundTypeDescription As Integer
    Dim RISK_InstrumentValue As Integer
    Dim RISK_InstrumentWorstCase As Integer
    Dim RISK_limType As Integer
    Dim RISK_limitID As Integer
    Dim RISK_limFund As Integer
    Dim RISK_limCharacteristic As Integer
    Dim RISK_limGreaterOrLessThan As Integer
    Dim RISK_limLimit As Integer
    Dim RISK_limIsPercent As Integer
    Dim RISK_DataCategoryId As Integer
    Dim RISK_CategoryWeighting As Integer

    Dim RISK_limOnPremium As Integer
    Dim RISK_InstrumentHasDelta As Integer
    Dim RISK_InstrumentDelta As Integer
    Dim RISK_InstrumentContractSize As Integer
    Dim RISK_InstrumentMultiplier As Integer
    Dim RISK_InstrumentUnderlyingPrice As Integer
    Dim RISK_InstrumentSharesPerOption As Integer
    Dim RISK_FXRate2 As Integer
    Dim RISK_FinalUnits As Integer

    Dim RISK_Weight As Double
    Dim RISK_Usage As Double
    Dim RISK_SumOfUsage As Double
    Dim RISK_Status As String
    Dim RISK_SumOfWeight As Double

    Dim ThisInstrumentValue As Double

    ' Establish Ordinals
    Try
      RISK_TransactionFund = tblInstrumentExposures.Columns.IndexOf("Fund")
      RISK_limitID = tblInstrumentExposures.Columns.IndexOf("limID")
      RISK_limFund = tblInstrumentExposures.Columns.IndexOf("limFund")
      RISK_TransactionInstrument = tblInstrumentExposures.Columns.IndexOf("Instrument")
      RISK_InstrumentFundType = tblInstrumentExposures.Columns.IndexOf("InstrumentFundType")
      RISK_InstrumentType = tblInstrumentExposures.Columns.IndexOf("InstrumentType")
      RISK_FundTypeDescription = tblInstrumentExposures.Columns.IndexOf("FundTypeDescription")
      RISK_InstrumentValue = tblInstrumentExposures.Columns.IndexOf("Value2")
      RISK_InstrumentWorstCase = tblInstrumentExposures.Columns.IndexOf("InstrumentWorstCase")
      RISK_limType = tblInstrumentExposures.Columns.IndexOf("limType")
      RISK_limCharacteristic = tblInstrumentExposures.Columns.IndexOf("limCharacteristic")
      RISK_limGreaterOrLessThan = tblInstrumentExposures.Columns.IndexOf("limGreaterOrLessThan")
      RISK_limLimit = tblInstrumentExposures.Columns.IndexOf("limLimit")
      RISK_limIsPercent = tblInstrumentExposures.Columns.IndexOf("limIsPercent")

      RISK_limOnPremium = tblInstrumentExposures.Columns.IndexOf("limOnPremium")
      RISK_InstrumentHasDelta = tblInstrumentExposures.Columns.IndexOf("InstrumentTypeHasDelta")
      RISK_InstrumentDelta = tblInstrumentExposures.Columns.IndexOf("BestDelta")
      RISK_InstrumentContractSize = tblInstrumentExposures.Columns.IndexOf("InstrumentContractSize")
      RISK_InstrumentMultiplier = tblInstrumentExposures.Columns.IndexOf("InstrumentMultiplier")
      RISK_InstrumentUnderlyingPrice = tblInstrumentExposures.Columns.IndexOf("UnderlyingPrice")
      RISK_InstrumentSharesPerOption = tblInstrumentExposures.Columns.IndexOf("InstrumentSharesPerOption")
      RISK_FXRate2 = tblInstrumentExposures.Columns.IndexOf("FXRate2") ' Compound FX Rate Instrument to Fund FX
      RISK_FinalUnits = tblInstrumentExposures.Columns.IndexOf("FinalUnits") ' SignedUnits from 2nd PnL Valuation query.

      RISK_DataCategoryId = tblInstrumentExposures.Columns.IndexOf("DataCategoryId")
      RISK_CategoryWeighting = tblInstrumentExposures.Columns.IndexOf("CategoryWeighting")

    Catch ex As Exception
      MainForm.LogError("ProcessCinqDixQuarenteExposureLimits", LOG_LEVELS.Error, ex.Message, "Error getting Field Ordinals from tblInstrumentExposures", ex.StackTrace, True)
      Return False
    End Try

    Try
      MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION), AdpExposure, "tblExposure")
    Catch ex As Exception
      MainForm.LogError("ProcessCinqDixQuarenteExposureLimits", LOG_LEVELS.Error, ex.Message, "Error configuring tblExposure Adaptor.", ex.StackTrace, True)
      Return False
    End Try

    ' Select appropriate Exposure Records
    Try
      'TODO: add this enum  RenaissanceGlobals.LimitType.CinqQuarenteInstrumentWeight=17
      SelectedInstrumentExposures = tblInstrumentExposures.Select("(limType = " & CInt(RenaissanceGlobals.LimitType.Ratio_5_10_40).ToString & ")", "Fund, limType, limCharacteristic, limGreaterOrLessThan, limLimit, limID, InstrumentParent, Instrument")
      If (SelectedInstrumentExposures Is Nothing) OrElse (SelectedInstrumentExposures.Length <= 0) Then
        Return True
      End If
    Catch ex As Exception
      MainForm.LogError("ProcessCinqDixQuarenteExposureLimits", LOG_LEVELS.Error, ex.Message, "Error selecting tblInstrumentExposures rows.", ex.StackTrace, True)
      Return False
    End Try


    ' Process selected Exposures
    ' 
    ' Calculate Calculated Fields :  Instrument Weight, Impact, Usage and Status
    '           Weight = [InstrumentValue] / [Fund Value]
    '           Impact = [Weight] * [WorstCase] * 10000    (Note : * 10000 to convert to bps)
    '           Usage  = [Weight] / [Limit]                (or inverse depending on Limit definition)
    '           Status = "OK", "Warning" or "Break"

    RISK_SumOfWeight = 0

    Dim n As Integer = 0
    For Counter = 0 To (SelectedInstrumentExposures.Length - 1)
      Try
        thisInstrumentExposure = SelectedInstrumentExposures(Counter)
        thisLimitValue = CDbl(thisInstrumentExposure(RISK_limLimit))
        ThisFundID = CInt(thisInstrumentExposure(RISK_TransactionFund))
        ThisCharacteristicID = CInt(thisInstrumentExposure(RISK_limCharacteristic))
        ThisLessThanOrGreaterThan = CInt(thisInstrumentExposure(RISK_limGreaterOrLessThan))

        If (ExposureRow Is Nothing) Then
          ExposureRow = tblExposure.NewtblExposureRow
          ExposureRow.expFund = CInt(thisInstrumentExposure(RISK_TransactionFund))
          ExposureRow.expLimitID = CInt(thisInstrumentExposure(RISK_limitID))
          ExposureRow.expLimitType = CInt(thisInstrumentExposure(RISK_limType))
        End If

        ThisInstrumentValue = CDbl(thisInstrumentExposure(RISK_InstrumentValue))

        If (CBool(thisInstrumentExposure(RISK_limOnPremium)) = False) AndAlso CBool(thisInstrumentExposure(RISK_InstrumentHasDelta)) Then
          ThisInstrumentValue = CDbl(thisInstrumentExposure(RISK_FinalUnits)) * CDbl(thisInstrumentExposure(RISK_InstrumentDelta)) * CDbl(thisInstrumentExposure(RISK_InstrumentContractSize)) * CDbl(thisInstrumentExposure(RISK_InstrumentMultiplier)) * CDbl(thisInstrumentExposure(RISK_InstrumentSharesPerOption)) * CDbl(thisInstrumentExposure(RISK_InstrumentUnderlyingPrice)) * CDbl(thisInstrumentExposure(RISK_FXRate2))
        End If

        'Check only shares, bonds and convertible bonds
        If Not ((CInt(thisInstrumentExposure(RISK_InstrumentType)) = InstrumentTypes.Bond) Or _
         (CInt(thisInstrumentExposure(RISK_InstrumentType)) = InstrumentTypes.Share) Or _
         (CInt(thisInstrumentExposure(RISK_InstrumentType)) = InstrumentTypes.ConvertibleBond)) Then

          Continue For
        End If


        If (Math.Abs(ThisInstrumentValue) >= 1.0#) Then

          If Fund_Value = 0 Then
            RISK_Weight = 1
          Else
            RISK_Weight = ThisInstrumentValue / Fund_Value
          End If

          'Fot test, to de deleted
          'n = n + 1
          'If n = 2 Then
          '    RISK_Weight = 0.12#
          'ElseIf n = 3 Then
          '    RISK_Weight = 0.07#
          'ElseIf n = 4 Or n = 5 Then
          '    RISK_Weight = 0.048#
          'End If

          If (RISK_Weight <= 0.05#) Then
            Continue For
          End If
          '' Roll Up Characteristic Totals
          RISK_SumOfWeight += RISK_Weight

          'RISK_SumOfUsage = RISK_SumOfWeight / thisLimitValue
          'RISK_Usage = RISK_Weight / thisLimitValue

          RISK_SumOfUsage = Math.Min(2.0#, Math.Max(0.0#, ((RISK_SumOfWeight - thisLimitValue) / Math.Abs(thisLimitValue)) + 1.0#))
          RISK_Usage = Math.Min(2.0#, Math.Max(-2.0#, ((RISK_Weight - thisLimitValue) / Math.Abs(thisLimitValue)) + 1.0#))


        End If ' Ignore small Exposures

        ' Save This Row ?
        ' Save if this is the last Exposure Row OR 
        ' If we have reached the end of a section (Fund, Limit Type, Limit, Condition or FundType has changed)


      Catch ex As Exception
        MainForm.LogError("ProcessCinqDixQuarenteExposureLimits", LOG_LEVELS.Error, ex.Message, "Error calculating Category Exposure Limits.", ex.StackTrace, True)
        Return False
      End Try
    Next

    If Not (ExposureRow Is Nothing) Then
      If (RISK_SumOfUsage > 1) Then
        RISK_Status = "Break"
      ElseIf (RISK_SumOfUsage >= 0.75) Then
        RISK_Status = "Warning"
      Else
        RISK_Status = "OK"
      End If

      ExposureRow.expIsDetailRow = 0
      ExposureRow.expLimitSector = 0
      ExposureRow.expLimitSectorName = ""
      ExposureRow.expLimitInstrument = 0
      ExposureRow.expLimitCharacteristic = ThisCharacteristicID
      ExposureRow.expLimitGreaterOrLessThan = ThisLessThanOrGreaterThan
      ExposureRow.expLimit = thisLimitValue
      ExposureRow.expDetailInstrument = 0
      ExposureRow.expExposure = RISK_SumOfWeight
      ExposureRow.expUsage = RISK_SumOfUsage
      ExposureRow.expStatus = RISK_Status
      ExposureRow.expDate = pDateToRisk
      ExposureRow.expDateCalculated = CalculationDate

      ExposureRow.expKnowledgeDate = pPreciseExposureKnowledgedate
      If (pPreciseExposureKnowledgedate = KNOWLEDGEDATE_NOW) OrElse (pPreciseExposureKnowledgedate.TimeOfDay.TotalSeconds >= LAST_SECOND) Then
        ExposureRow.expKnowledgeDateWholeday = (-1)
      Else
        ExposureRow.expKnowledgeDateWholeday = 0
      End If

      ExposureRow.expSystemKnowledgeDate = MainForm.Main_Knowledgedate
      If (MainForm.Main_Knowledgedate = KNOWLEDGEDATE_NOW) OrElse (MainForm.Main_Knowledgedate.TimeOfDay.TotalSeconds >= LAST_SECOND) Then
        ExposureRow.expSystemKnowledgeDateWholeday = (-1)
      Else
        ExposureRow.expSystemKnowledgeDateWholeday = 0
      End If

      ExposureRow.expFlags = ExposureFlags

      NewExposureRecords.Add(ExposureRow)

    End If

    ' now add the details
    Dim lim_1 As Double = 0.05#
    Dim lim_2 As Double = 0.1#
    Dim lim_value As Double
    n = 0
    For Counter = 0 To (SelectedInstrumentExposures.Length - 1)
      Try
        thisInstrumentExposure = SelectedInstrumentExposures(Counter)
        thisLimitValue = CDbl(thisInstrumentExposure(RISK_limLimit))
        ThisFundID = CInt(thisInstrumentExposure(RISK_TransactionFund))
        ThisCharacteristicID = CInt(thisInstrumentExposure(RISK_limCharacteristic))
        ThisLessThanOrGreaterThan = CInt(thisInstrumentExposure(RISK_limGreaterOrLessThan))

        'Check only shares, bonds and convertible bonds
        If Not ((CInt(thisInstrumentExposure(RISK_InstrumentType)) = InstrumentTypes.Bond) Or _
         (CInt(thisInstrumentExposure(RISK_InstrumentType)) = InstrumentTypes.Share) Or _
         (CInt(thisInstrumentExposure(RISK_InstrumentType)) = InstrumentTypes.ConvertibleBond)) Then

          Continue For
        End If

        'TODO: Cr�er la lmimte n� 17 : Instrument Weight 5-10-40 et tester 

        ThisInstrumentValue = CDbl(thisInstrumentExposure(RISK_InstrumentValue))

        If (CBool(thisInstrumentExposure(RISK_limOnPremium)) = False) AndAlso CBool(thisInstrumentExposure(RISK_InstrumentHasDelta)) Then
          ThisInstrumentValue = CDbl(thisInstrumentExposure(RISK_FinalUnits)) * CDbl(thisInstrumentExposure(RISK_InstrumentDelta)) * CDbl(thisInstrumentExposure(RISK_InstrumentContractSize)) * CDbl(thisInstrumentExposure(RISK_InstrumentMultiplier)) * CDbl(thisInstrumentExposure(RISK_InstrumentSharesPerOption)) * CDbl(thisInstrumentExposure(RISK_InstrumentUnderlyingPrice)) * CDbl(thisInstrumentExposure(RISK_FXRate2))
        End If

        If (Math.Abs(ThisInstrumentValue) >= 1) Then
          If Fund_Value = 0 Then
            RISK_Weight = 1
          Else
            RISK_Weight = ThisInstrumentValue / Fund_Value
          End If

          'Fot test, to de deleted
          'n = n + 1
          'If n = 2 Then
          '    RISK_Weight = 0.12#
          'ElseIf n = 3 Then
          '    RISK_Weight = 0.07#
          'ElseIf n = 4 Or n = 5 Then
          '    RISK_Weight = 0.048#
          'End If


          If (RISK_Weight > lim_2) Then
            lim_value = lim_2
            RISK_Status = "Break"
          ElseIf (RISK_Weight > lim_1) Then
            lim_value = lim_1
            RISK_Status = "Warning"
            'If (RISK_SumOfUsage > 1) Then
            '    RISK_Status = "Break"
            'Else
            '    If (RISK_Usage > 0.75#) Then
            '        RISK_Status = "Warning"
            '    Else
            '        RISK_Status = "OK"
            '    End If

            'End If
          Else
            lim_value = lim_1
            RISK_Status = "OK"
          End If

          RISK_Usage = Math.Min(2.0#, Math.Max(-2.0#, ((RISK_Weight - lim_value) / Math.Abs(lim_value)) + 1.0#))

          If RISK_Status = "OK" And RISK_Usage > 0.75# Then
            RISK_Status = "Warning"
          End If


          ' There will be no details rows, 
          ' so here should we dispaly all the instruments or only the ones not OK or the ones with warning
          ' we can display all of them, or all the not OK or even if we 
          If (Math.Abs(RISK_Weight) > 0 _
              And ( _
              RISK_Status <> "OK" _
              ) _
          ) Then

            ExposureDetailRow = tblExposure.NewtblExposureRow

            ExposureDetailRow.expFund = ExposureRow.expFund
            ExposureDetailRow.expLimitID = CInt(thisInstrumentExposure(RISK_limitID))

            ' The instruments with risk_status not OK appear in the report even if the "show details" option is not checked                        
            ExposureDetailRow.expIsDetailRow = 0
            'If RISK_Status = "OK" Then
            '    ExposureDetailRow.expIsDetailRow = 1
            'Else
            '    ExposureDetailRow.expIsDetailRow = 0
            'End If

            'azerty
            ExposureDetailRow.expLimitSector = CInt(thisInstrumentExposure(RISK_InstrumentFundType))
            ExposureDetailRow.expLimitSectorName = CStr(thisInstrumentExposure(RISK_FundTypeDescription))
            ExposureDetailRow.expLimitInstrument = CInt(thisInstrumentExposure(RISK_TransactionInstrument))
            ExposureDetailRow.expLimitType = ExposureRow.expLimitType
            ExposureDetailRow.expLimitCharacteristic = ExposureRow.expLimitCharacteristic
            ExposureDetailRow.expLimitGreaterOrLessThan = ExposureRow.expLimitGreaterOrLessThan

            'ExposureDetailRow.expLimit = ExposureRow.expLimit
            ExposureDetailRow.expLimit = lim_value
            ExposureDetailRow.expDetailInstrument = CInt(thisInstrumentExposure(RISK_TransactionInstrument))
            ExposureDetailRow.expExposure = RISK_Weight
            ExposureDetailRow.expUsage = RISK_Usage
            ExposureDetailRow.expStatus = RISK_Status
            ExposureDetailRow.expDate = ExposureRow.expDate
            ExposureDetailRow.expDateCalculated = ExposureRow.expDateCalculated
            ExposureDetailRow.expKnowledgeDate = ExposureRow.expKnowledgeDate
            ExposureDetailRow.expKnowledgeDateWholeday = ExposureRow.expKnowledgeDateWholeday
            ExposureDetailRow.expSystemKnowledgeDate = ExposureRow.expSystemKnowledgeDate
            ExposureDetailRow.expSystemKnowledgeDateWholeday = ExposureRow.expSystemKnowledgeDateWholeday
            ExposureDetailRow.expFlags = ExposureRow.expFlags

            ExposureDetailRow.expKnowledgeDate = pPreciseExposureKnowledgedate
            If (pPreciseExposureKnowledgedate = KNOWLEDGEDATE_NOW) OrElse (pPreciseExposureKnowledgedate.TimeOfDay.TotalSeconds >= LAST_SECOND) Then
              ExposureDetailRow.expKnowledgeDateWholeday = (-1)
            Else
              ExposureDetailRow.expKnowledgeDateWholeday = 0
            End If

            ExposureDetailRow.expSystemKnowledgeDate = MainForm.Main_Knowledgedate
            If (MainForm.Main_Knowledgedate = KNOWLEDGEDATE_NOW) OrElse (MainForm.Main_Knowledgedate.TimeOfDay.TotalSeconds >= LAST_SECOND) Then
              ExposureDetailRow.expSystemKnowledgeDateWholeday = (-1)
            Else
              ExposureDetailRow.expSystemKnowledgeDateWholeday = 0
            End If

            ExposureDetailRow.expFlags = ExposureFlags

            NewExposureRecords.Add(ExposureDetailRow)
            ExposureDetailRow = Nothing

          End If

        End If


      Catch ex As Exception
        MainForm.LogError("ProcessCinqDixQuarenteExposureLimits", LOG_LEVELS.Error, ex.Message, "Error calculating CinqDixQuarente Exposure Limits.", ex.StackTrace, True)
        Return False
      End Try
    Next

    ExposureRow = Nothing

    ' Now Add the specific exposures to the Exposures table, then add those general exposures which 
    ' are not covered by a specific limit.

    Dim OuterCount As Integer

    For OuterCount = 0 To (NewExposureRecords.Count - 1)
      Try
        ExposureRow = NewExposureRecords(OuterCount)

        If Math.Abs(ExposureRow.expExposure) >= RISK_MINIMUMEXPOSURETOSAVE Then
          tblExposure.Rows.Add(ExposureRow)
        End If

      Catch ex As Exception
        MainForm.LogError("ProcessCinqDixQuarenteExposureLimits", LOG_LEVELS.Error, ex.Message, "Error saving CinqDixQuarente Exposure Limits.", ex.StackTrace, True)
        Return False
      End Try
    Next

    ' Save New Exposure Records.
    Try
      If tblExposure.Rows.Count > 0 Then
        MainForm.AdaptorUpdate("RiskExposureFunctions", AdpExposure, tblExposure)
      End If
    Catch ex As Exception
      MainForm.LogError("ProcessCinqDixQuarenteExposureLimits", LOG_LEVELS.Error, ex.Message, "Error updating CinqDixQuarente Exposure Limits.", ex.StackTrace, True)
      Return False
    End Try

    Return True

  End Function


  ''' <summary>
  ''' Checks the exposures exist.
  ''' </summary>
  ''' <param name="MainForm">The main form.</param>
  ''' <param name="TS_StatusLabel">The T s_ status label.</param>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pExposureDate">The p exposure date.</param>
  ''' <param name="pExposureKnowledgedate">The p exposure knowledgedate.</param>
  ''' <param name="pExposureFlags">The p exposure flags.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Friend Function CheckExposuresExist(ByRef MainForm As VeniceMain, ByRef TS_StatusLabel As ToolStripStatusLabel, ByVal pFundID As Integer, ByVal pExposureDate As Date, ByVal pExposureKnowledgedate As Date, ByVal pExposureFlags As Integer) As Boolean
    ' *********************************************************
    '
    ' Check Exposure records exist for given Fund, Date etc...
    ' This is simply achieved by calling the GetData function used by the Report
    ' And if Nothing or Zero Rows are returned then it is assumed that no data exists.
    '
    '
    ' *********************************************************

    Dim ReportData As DataTable

    Try
      ReportData = MainForm.MainReportHandler.GetData_rptRiskExposuresReport(pFundID, 0, pExposureDate, 0, 0, 0, pExposureFlags, pExposureKnowledgedate, MainForm.Main_Knowledgedate)
      If (Not (ReportData Is Nothing)) AndAlso (ReportData.Rows.Count > 0) Then
        Return True
      Else
        Return False
      End If
    Catch ex As Exception
    End Try
    Return False

  End Function

  ''' <summary>
  ''' Checks the exposures are current.
  ''' </summary>
  ''' <param name="MainForm">The main form.</param>
  ''' <param name="TS_StatusLabel">The T s_ status label.</param>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pExposureDate">The p exposure date.</param>
  ''' <param name="pExposureKnowledgeDate">The p exposure knowledge date.</param>
  ''' <param name="pExposureFlags">The p exposure flags.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Friend Function CheckExposuresAreCurrent(ByRef MainForm As VeniceMain, ByRef TS_StatusLabel As ToolStripStatusLabel, ByVal pFundID As Integer, ByVal pExposureDate As Date, ByVal pExposureKnowledgeDate As Date, ByVal pExposureFlags As Integer) As Boolean

    ' Configure Adaptor

    Dim checkAdaptor As New SqlDataAdapter
    Dim checkTable As New DataTable
    Dim RVal As Integer

    Try

      ' rptUnitPrice
      MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION), checkAdaptor, "spu_CheckRiskExposuresAreCurrent")
      If (checkAdaptor.SelectCommand Is Nothing) Then
        Return Nothing
        Exit Function
      End If

      checkAdaptor.SelectCommand.Connection = MainForm.GetVeniceConnection

      If (checkAdaptor.SelectCommand.Connection Is Nothing) Then
        Return Nothing
        Exit Function
      End If

      checkAdaptor.SelectCommand.Parameters("@FundID").Value = pFundID
      checkAdaptor.SelectCommand.Parameters("@IsDetailRow").Value = 0
      checkAdaptor.SelectCommand.Parameters("@ExposureDate").Value = pExposureDate
      checkAdaptor.SelectCommand.Parameters("@ExposureFlags").Value = pExposureFlags
      checkAdaptor.SelectCommand.Parameters("@ExposureKnowledgeDate").Value = pExposureKnowledgeDate
      checkAdaptor.SelectCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate

      SyncLock checkAdaptor.SelectCommand.Connection
        Try
          RVal = CInt(MainForm.ExecuteScalar(checkAdaptor.SelectCommand))
          'RVal = CInt(checkAdaptor.SelectCommand.ExecuteScalar)
        Catch ex As Exception
          RVal = 0
        End Try
      End SyncLock

      If RVal = 0 Then
        Return False
      Else
        Return True
      End If

    Catch ex As Exception
      MainForm.LogError("ReportHandler, CheckExposuresAreCurrent()", LOG_LEVELS.Error, ex.Message, "Error in CheckExposuresAreCurrent().", ex.StackTrace, True)
      Return False
      Exit Function

    Finally
      Try
        If (Not (checkAdaptor.SelectCommand.Connection Is Nothing)) Then
          checkAdaptor.SelectCommand.Connection.Close()
        End If
      Catch ex As Exception
      End Try
    End Try

    Return False

  End Function


End Module
