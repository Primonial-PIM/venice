' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 11-05-2012
' ***********************************************************************
' <copyright file="RegistryCode.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports Microsoft.Win32

''' <summary>
''' Class RegistryCode
''' </summary>
Module RegistryCode


    ''' <summary>
    ''' Get_s the registry item.
    ''' </summary>
    ''' <param name="pRegistryArea">The p registry area.</param>
    ''' <param name="pKey">The p key.</param>
    ''' <param name="pItem">The p item.</param>
    ''' <param name="pDefault">The p default.</param>
    ''' <returns>System.Object.</returns>
  Public Function Get_RegistryItem(ByRef pRegistryArea As RegistryKey, ByVal pKey As String, ByVal pItem As String, ByVal pDefault As Object) As Object
    ' **********************************************************************
    ' Retrieve an item from the registry.
    '
    ' **********************************************************************

    Dim Key As RegistryKey
    Dim ItemValue As Object

    Key = pRegistryArea.OpenSubKey(pKey, False)

    If Key Is Nothing Then
      ItemValue = pDefault
    Else
      ItemValue = Key.GetValue(pItem, pDefault)
    End If

    Return ItemValue

  End Function

    ''' <summary>
    ''' Get_s the registry item.
    ''' </summary>
    ''' <param name="pKey">The p key.</param>
    ''' <param name="pItem">The p item.</param>
    ''' <param name="pDefault">The p default.</param>
    ''' <returns>System.Object.</returns>
  Public Function Get_RegistryItem(ByVal pKey As String, ByVal pItem As String, ByVal pDefault As Object) As Object
    ' **********************************************************************
    ' Retrieve an item from the registry.
    '
    ' **********************************************************************

    Return Get_RegistryItem(Registry.CurrentUser, pKey, pItem, pDefault)

    'Dim Key As RegistryKey
    'Dim ItemValue As Object

    'Key = Registry.CurrentUser.OpenSubKey(pKey, False)

    'If Key Is Nothing Then
    '  ItemValue = pDefault
    'Else
    '  ItemValue = Key.GetValue(pItem, pDefault)
    'End If

    'Return ItemValue

  End Function

    ''' <summary>
    ''' Set_s the registry item.
    ''' </summary>
    ''' <param name="pRegistryArea">The p registry area.</param>
    ''' <param name="pKey">The p key.</param>
    ''' <param name="pItem">The p item.</param>
    ''' <param name="pItemValue">The p item value.</param>
    ''' <returns>System.Object.</returns>
	Public Function Set_RegistryItem(ByRef pRegistryArea As RegistryKey, ByVal pKey As String, ByVal pItem As String, ByVal pItemValue As Object) As Object
		' **********************************************************************
		' Save an item to the registry.
		'
		' **********************************************************************
		Dim Key As RegistryKey

		Try
			Key = pRegistryArea.OpenSubKey(pKey, True)

			If Key Is Nothing Then
				Key = pRegistryArea.CreateSubKey(pKey)
			End If

			Key.SetValue(pItem, pItemValue)

		Catch ex As Exception
			Return False
			Exit Function

		End Try

		Return True
	End Function

    ''' <summary>
    ''' Set_s the registry item.
    ''' </summary>
    ''' <param name="pKey">The p key.</param>
    ''' <param name="pItem">The p item.</param>
    ''' <param name="pItemValue">The p item value.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function Set_RegistryItem(ByVal pKey As String, ByVal pItem As String, ByVal pItemValue As Object) As Boolean
    ' **********************************************************************
    ' Save an item to the registry.
    '
    ' **********************************************************************

    Return Set_RegistryItem(Registry.CurrentUser, pKey, pItem, pItemValue)

    'Dim Key As RegistryKey
    'Dim ItemValue As Object

    'Try
    '  Key = Registry.CurrentUser.OpenSubKey(pKey, True)

    '  If Key Is Nothing Then
    '    Key = Registry.CurrentUser.CreateSubKey(pKey)
    '  End If

    '  Key.SetValue(pItem, pItemValue)

    'Catch ex As Exception
    '  Return False
    '  Exit Function

    'End Try

    'Return True

  End Function

End Module
