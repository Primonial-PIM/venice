﻿' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 08-15-2013
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="CustomC1NumericTextBox.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
''' <summary>
''' Class CustomC1NumericTextBox
''' </summary>
Public Class CustomC1NumericTextBox

  Inherits RenaissanceControls.NumericTextBox

  Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor

    ''' <summary>
    ''' Handles the GotFocus event of the NumericTextBox control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub NumericTextBox_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.GotFocus
    Try

    Catch ex As Exception

    End Try

  End Sub

    ''' <summary>
    ''' C1s the editor format.
    ''' </summary>
    ''' <param name="value">The value.</param>
    ''' <param name="mask">The mask.</param>
    ''' <returns>System.String.</returns>
  Public Function C1EditorFormat(ByVal value As Object, ByVal mask As String) As String Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorFormat
    Try
      Dim TempString As String = value.ToString()
      Dim Divisor As Double = 1.0

      If TempString.EndsWith("%") Then
        TempString = TempString.Substring(0, TempString.Length - 1)
        Divisor = 100.0
      End If

      If IsNumeric(TempString) Then
        Return CDbl(TempString).ToString(mask)
      End If

    Catch ex As Exception
    End Try

    Return CDbl(0.0#).ToString(mask)
  End Function

    ''' <summary>
    ''' C1s the editor get style.
    ''' </summary>
    ''' <returns>System.Drawing.Design.UITypeEditorEditStyle.</returns>
  Public Function C1EditorGetStyle() As System.Drawing.Design.UITypeEditorEditStyle Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorGetStyle
    Try
      Return Drawing.Design.UITypeEditorEditStyle.None
    Catch ex As Exception
    End Try

  End Function

    ''' <summary>
    ''' C1s the editor get value.
    ''' </summary>
    ''' <returns>System.Object.</returns>
  Public Function C1EditorGetValue() As Object Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorGetValue
    Try
      Return Me.Value
    Catch ex As Exception
    End Try

    Return 0.0#
  End Function

    ''' <summary>
    ''' C1s the editor initialize.
    ''' </summary>
    ''' <param name="value">The value.</param>
    ''' <param name="editorAttributes">The editor attributes.</param>
  Public Sub C1EditorInitialize(ByVal value As Object, ByVal editorAttributes As System.Collections.IDictionary) Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorInitialize
    Try
      Me.Value = CDbl(value)
      Me.SelectTextOnFocus = True
    Catch ex As Exception
    End Try
  End Sub

    ''' <summary>
    ''' C1s the editor key down finish edit.
    ''' </summary>
    ''' <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function C1EditorKeyDownFinishEdit(ByVal e As System.Windows.Forms.KeyEventArgs) As Boolean Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorKeyDownFinishEdit
    Try
      If (e.KeyValue = 13) OrElse (e.KeyValue = 9) Then
        Return True
      End If
      If (e.KeyValue = 27) Then
        Me.Text = Me.Value.ToString()
        Return True
      End If
    Catch ex As Exception
    End Try

    Return False
  End Function

    ''' <summary>
    ''' C1s the editor update bounds.
    ''' </summary>
    ''' <param name="rc">The rc.</param>
  Public Sub C1EditorUpdateBounds(ByVal rc As System.Drawing.Rectangle) Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorUpdateBounds
    Try
      Me.Location = rc.Location
      Me.Size = rc.Size
    Catch ex As Exception
    End Try
  End Sub

    ''' <summary>
    ''' C1s the editor value is valid.
    ''' </summary>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function C1EditorValueIsValid() As Boolean Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorValueIsValid
    Try
      Me.SetValueFromText()
    Catch ex As Exception
    End Try

    Return True
  End Function

End Class


''' <summary>
''' Class CustomC1PercentageTextBox
''' </summary>
Public Class CustomC1PercentageTextBox

  Inherits RenaissanceControls.PercentageTextBox

  Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor

    ''' <summary>
    ''' Handles the GotFocus event of the PercentageTextBox control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub PercentageTextBox_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.GotFocus
    Try

    Catch ex As Exception

    End Try


  End Sub

    ''' <summary>
    ''' C1s the editor format.
    ''' </summary>
    ''' <param name="value">The value.</param>
    ''' <param name="mask">The mask.</param>
    ''' <returns>System.String.</returns>
  Public Function C1EditorFormat(ByVal value As Object, ByVal mask As String) As String Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorFormat
    Try
      Dim TempString As String = value.ToString()
      Dim Divisor As Double = 1.0

      If TempString.EndsWith("%") Then
        TempString = TempString.Substring(0, TempString.Length - 1)
        Divisor = 100.0
      End If

      If IsNumeric(TempString) Then
        Return CDbl(TempString).ToString(mask)
      End If

    Catch ex As Exception
    End Try

    Return CDbl(0.0#).ToString(mask)
  End Function

    ''' <summary>
    ''' C1s the editor get style.
    ''' </summary>
    ''' <returns>System.Drawing.Design.UITypeEditorEditStyle.</returns>
  Public Function C1EditorGetStyle() As System.Drawing.Design.UITypeEditorEditStyle Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorGetStyle
    Try
      Return Drawing.Design.UITypeEditorEditStyle.None
    Catch ex As Exception
    End Try

  End Function

    ''' <summary>
    ''' C1s the editor get value.
    ''' </summary>
    ''' <returns>System.Object.</returns>
  Public Function C1EditorGetValue() As Object Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorGetValue
    Try
      Return Me.Value
    Catch ex As Exception
    End Try

    Return 0.0#
  End Function

    ''' <summary>
    ''' C1s the editor initialize.
    ''' </summary>
    ''' <param name="value">The value.</param>
    ''' <param name="editorAttributes">The editor attributes.</param>
  Public Sub C1EditorInitialize(ByVal value As Object, ByVal editorAttributes As System.Collections.IDictionary) Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorInitialize
    Try
      Me.Value = CDbl(value)
      Me.SelectTextOnFocus = True
    Catch ex As Exception
    End Try
  End Sub

    ''' <summary>
    ''' C1s the editor key down finish edit.
    ''' </summary>
    ''' <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function C1EditorKeyDownFinishEdit(ByVal e As System.Windows.Forms.KeyEventArgs) As Boolean Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorKeyDownFinishEdit
    Try
      If (e.KeyValue = 13) OrElse (e.KeyValue = 9) Then
        Return True
      End If
      If (e.KeyValue = 27) Then
        Me.Text = Me.Value.ToString()
        Return True
      End If
    Catch ex As Exception
    End Try

    Return False
  End Function

    ''' <summary>
    ''' C1s the editor update bounds.
    ''' </summary>
    ''' <param name="rc">The rc.</param>
  Public Sub C1EditorUpdateBounds(ByVal rc As System.Drawing.Rectangle) Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorUpdateBounds
    Try
      Me.Location = rc.Location
      Me.Size = rc.Size
    Catch ex As Exception
    End Try
  End Sub

    ''' <summary>
    ''' C1s the editor value is valid.
    ''' </summary>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function C1EditorValueIsValid() As Boolean Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorValueIsValid
    Try
      Me.SetValueFromText()
    Catch ex As Exception
    End Try

    Return True
  End Function
End Class
