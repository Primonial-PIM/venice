' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="AuditFunctions.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals


''' <summary>
''' Class AuditFunctions
''' </summary>
Module AuditFunctions


    ''' <summary>
    ''' Struct AUDIT_ParameterValues
    ''' </summary>
  Private Structure AUDIT_ParameterValues
        ''' <summary>
        ''' The record description
        ''' </summary>
    Dim RecordDescription As String
        ''' <summary>
        ''' The audit join
        ''' </summary>
    Dim AuditJoin As String
  End Structure

    ''' <summary>
    ''' Run_s the audit report_ ALL records.
    ''' </summary>
    ''' <param name="Mainform">The mainform.</param>
    ''' <param name="pSourceDataset">The p source dataset.</param>
    ''' <param name="pStatusBar">The p status bar.</param>
  Sub Run_AuditReport_ALLRecords(ByRef Mainform As VeniceMain, ByVal pSourceDataset As StandardDataset, ByRef pStatusBar As ToolStripProgressBar)
    '**************************************************************************************************
    ' Purpose : Run the Record changes Audit Report for the given Table. (Add/Edit form)
    '           All records.
    '
    ' Accepts <none>
    '
    ' Returns : <none>
    '
    '**************************************************************************************************
    If pSourceDataset Is Nothing Then
      Exit Sub
    End If

    Call Mainform.LogError("Run_AuditReport_AllRecord", LOG_LEVELS.Audit, "Activity", "", "", False)

    Call Run_AuditReport(Mainform, pSourceDataset, (-1), pStatusBar)
  End Sub

    ''' <summary>
    ''' Run_s the audit report_ this record.
    ''' </summary>
    ''' <param name="Mainform">The mainform.</param>
    ''' <param name="pSourceDataset">The p source dataset.</param>
    ''' <param name="pAuditID">The p audit ID.</param>
    ''' <param name="pStatusBar">The p status bar.</param>
  Sub Run_AuditReport_ThisRecord(ByRef Mainform As VeniceMain, ByVal pSourceDataset As StandardDataset, ByVal pAuditID As Integer, ByRef pStatusBar As ToolStripProgressBar)
    '**************************************************************************************************
    ' Purpose : Run the Record changes Audit Report for the given Table (Add/Edit form)
    '           And the given Item ID. (AuditID).
    '
    ' Accepts <none>
    '
    ' Returns : <none>
    '
    '**************************************************************************************************

    If pSourceDataset Is Nothing Then
      Exit Sub
    End If

    Call Mainform.LogError("Run_AuditReport_ThisRecord", LOG_LEVELS.Audit, "Activity", "", "", False)

    Call Run_AuditReport(Mainform, pSourceDataset, pAuditID, pStatusBar)
  End Sub


    ''' <summary>
    ''' Run_s the audit report.
    ''' </summary>
    ''' <param name="Mainform">The mainform.</param>
    ''' <param name="pSourceDataset">The p source dataset.</param>
    ''' <param name="pAuditID">The p audit ID.</param>
    ''' <param name="pStatusBar">The p status bar.</param>
  Sub Run_AuditReport(ByRef Mainform As VeniceMain, ByVal pSourceDataset As StandardDataset, ByVal pAuditID As Integer, ByRef pStatusBar As ToolStripProgressBar)
    '**************************************************************************************************
    ' Purpose : Run the Record changes Audit Report for the given Table (Add/Edit form)
    '           And all records.
    '
    ' Accepts <none>
    '
    ' Returns : <none>
    '
    '**************************************************************************************************
    Dim thisReportTable As DataTable

    thisReportTable = CreateRecordChange(Mainform, pSourceDataset, pAuditID)

    Call Mainform.MainReportHandler.rptTableAuditReport(thisReportTable, pStatusBar)

  End Sub



    ''' <summary>
    ''' Creates the record change.
    ''' </summary>
    ''' <param name="Mainform">The mainform.</param>
    ''' <param name="pSourceDataset">The p source dataset.</param>
    ''' <param name="pAuditID">The p audit ID.</param>
    ''' <returns>DataTable.</returns>
  Public Function CreateRecordChange(ByRef Mainform As VeniceMain, ByVal pSourceDataset As StandardDataset, Optional ByVal pAuditID As Integer = (-1)) As DataTable
    ' *******************************************************************************
    '
    ' Purpose:  To save field-by-field change log for the given table
    ' Accepts:  pSourceDataset : TableName to process
    '           pAuditID       : AuditID if item to track
    ' Returns:  DataTable.
    '
    ' *******************************************************************************

    Dim SourceTable As String                 ' Source Table name
    Dim SourceAuditID As Integer               ' Source AuditID, usually the same as InstrumentID, FXID etc...
    Dim SQL_String As String                  ' Temporary string used to build SQL Query / Execute statements
    Dim DescriptionField As String            ' Will hold the SQL phrase which describes the current record
    Dim JoinSQL As String                     ' Will hold the SQL fragment which defines any necessary table join.

    Dim This_Table As AUDIT_ParameterValues   '

    Dim Date_Entered_Fieldname As String      ' Will hold the name of the field in the Queried table which contains the Date Entered
    Dim Date_Entered_Ordinal As Integer
    Dim UserName_Fieldname As String          ' Will hold the name of the field in the Queried table which contains the name of the user that made the change.
    Dim UserName_Ordinal As Integer

    Dim AuditID_Ordinal As Integer

    Dim AuditSource_RS As DataTable                ' Recordsets to retrieve and compare Source Data
    Dim AuditRowCounter As Integer
    Dim RNCounter As Integer

    Dim AuditResults_RS As DataTable          ' 
    Dim newAuditRecord As DataRow
    Dim FieldCount As Integer                 ' Temporary variable

    Dim Has_Changed_Flag As Boolean


    ' Retrieve and validate parameters

    SourceTable = pSourceDataset.TableName
    SourceAuditID = pAuditID

    ' Clear existing contents of the Audit Results table
    ' (That is, create a new table in memory.)
    Try
      AuditResults_RS = New DataTable("tblAuditResults")

      AuditResults_RS.Columns.Add(New DataColumn("RN", GetType(Integer)))
      AuditResults_RS.Columns.Add(New DataColumn("ChangeTableName", GetType(String)))
      AuditResults_RS.Columns.Add(New DataColumn("ChangeID", GetType(Integer)))
      AuditResults_RS.Columns.Add(New DataColumn("ChangeRN", GetType(Integer)))
      AuditResults_RS.Columns.Add(New DataColumn("ChangeUser", GetType(String)))
      AuditResults_RS.Columns.Add(New DataColumn("ChangeDate", GetType(Date)))
      AuditResults_RS.Columns.Add(New DataColumn("ChangeField", GetType(String)))
      AuditResults_RS.Columns.Add(New DataColumn("ChangeFrom", GetType(String)))
      AuditResults_RS.Columns.Add(New DataColumn("ChangeTo", GetType(String)))
      AuditResults_RS.Columns.Add(New DataColumn("ChangeRecordDescription", GetType(String)))

    Catch ex As Exception
      Return Nothing
    End Try


    ' Get AUDIT parameters specific to this table
    This_Table = Audit_Preparation(pSourceDataset)
    DescriptionField = This_Table.RecordDescription
    JoinSQL = This_Table.AuditJoin

    ' Build SQL Query string, integrating any Table specific terms and making exceptions for the Transaction Table.

    ' SELECT FROM
    SQL_String = "SELECT " & DescriptionField & " AS RecordDescription, " & SourceTable & ".* " & _
                  "FROM " & SourceTable & " "

    ' Make JOIN if necessary
    If Len(JoinSQL) > 0 Then
      SQL_String = SQL_String & _
                   JoinSQL & " "
    End If

    ' WHERE Clauses
    If SourceAuditID >= 0 Then
      SQL_String = SQL_String & _
                  "WHERE (" & SourceTable & ".AuditID = " & SourceAuditID.ToString & ") "

      If pSourceDataset.ChangeID = RenaissanceChangeID.tblTransaction Then
        SQL_String = SQL_String & _
                  "AND (TransactionParentID > 0) AND (TransactionLeg = 1) "
      End If
    Else
      If pSourceDataset.ChangeID = RenaissanceChangeID.tblTransaction Then
        SQL_String = SQL_String & _
                  "WHERE (TransactionLeg = 1) AND (TransactionParentID > 0) "
      End If
    End If

    ' ORDER BY
    SQL_String = SQL_String & _
                  "ORDER BY " & SourceTable & ".AuditID ASC, " & SourceTable & ".RN ASC"

    ' OK, Get Source Data.

    Dim thisAdaptor As New SqlDataAdapter
    Dim thisSelectCommand As New SqlCommand

    Try

      thisSelectCommand.Connection = Mainform.GetVeniceConnection()
      thisSelectCommand.CommandType = CommandType.Text
      thisSelectCommand.CommandText = SQL_String
      thisSelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      thisAdaptor.SelectCommand = thisSelectCommand

      AuditSource_RS = New DataTable

      thisAdaptor.Fill(AuditSource_RS)
    Catch ex As Exception
      Mainform.LogError("Table Audit, CreateRecordChange()", LOG_LEVELS.Error, ex.Message, "Error Filling source table", ex.StackTrace, True)
      Return AuditResults_RS
    Finally
      Try
        If (thisSelectCommand IsNot Nothing) AndAlso (thisSelectCommand.Connection IsNot Nothing) Then
          thisSelectCommand.Connection.Close()
          thisSelectCommand.Connection = Nothing
        End If
      Catch ex As Exception
      End Try

    End Try


    ' Establish DateEntered and UserName fields
    Date_Entered_Fieldname = ""
    Date_Entered_Ordinal = (-1)
    UserName_Fieldname = ""
    UserName_Ordinal = (-1)
    For FieldCount = 0 To (AuditSource_RS.Columns.Count - 1)
      If (Date_Entered_Ordinal < 0) And (AuditSource_RS.Columns(FieldCount).ColumnName.EndsWith("DateEntered")) Then
        Date_Entered_Fieldname = AuditSource_RS.Columns(FieldCount).ColumnName
        Date_Entered_Ordinal = AuditSource_RS.Columns(FieldCount).Ordinal
      End If

      If AuditSource_RS.Columns(FieldCount).ColumnName.EndsWith("User") Or AuditSource_RS.Columns(FieldCount).ColumnName.EndsWith("UserEntered") Then
        UserName_Fieldname = AuditSource_RS.Columns(FieldCount).ColumnName
        UserName_Ordinal = AuditSource_RS.Columns(FieldCount).Ordinal
      End If

      If (Date_Entered_Ordinal >= 0) AndAlso (UserName_Ordinal >= 0) Then
        Exit For
      End If

    Next FieldCount

    AuditID_Ordinal = (-1)
    Try
      AuditID_Ordinal = AuditSource_RS.Columns("AuditID").Ordinal
    Catch ex As Exception
      Mainform.LogError("Table Audit, CreateRecordChange()", LOG_LEVELS.Error, ex.Message, "Error getting AuditID Column", ex.StackTrace, True)
      Return AuditResults_RS
    End Try

    ' OK, move through the source data, posting any differences to the results table.

    RNCounter = 0

    ' 
    Dim FirstValue As Object
    Dim SecondValue As Object

    AuditRowCounter = (0)

    While AuditRowCounter <= (AuditSource_RS.Rows.Count - 1)

      ' If this is the first Row, or else the Audit ID has changed, then record the full record values - A new Record.
      If (AuditRowCounter = 0) OrElse (CInt(AuditSource_RS.Rows(AuditRowCounter - 1)(AuditID_Ordinal)) <> CInt(AuditSource_RS.Rows(AuditRowCounter)(AuditID_Ordinal))) Then
        ' Record Initial Table Values.
        ' Cycle through the fields, Recording initial values.

        For FieldCount = 1 To (AuditSource_RS.Columns.Count - 1) ' Don't compare the first field as it is the RecordDescription.
          newAuditRecord = AuditResults_RS.NewRow

          RNCounter += 1
          newAuditRecord("RN") = RNCounter

          newAuditRecord("ChangeTableName") = SourceTable
          newAuditRecord("ChangeRN") = AuditSource_RS.Rows(AuditRowCounter)("RN") ' AuditSource_RS!RN
          newAuditRecord("ChangeID") = AuditSource_RS.Rows(AuditRowCounter)(AuditID_Ordinal) '  AuditSource_RS!AuditID

          If UserName_Ordinal >= 0 Then
            newAuditRecord("ChangeUser") = AuditSource_RS.Rows(AuditRowCounter)(UserName_Ordinal)
          Else
            newAuditRecord("ChangeUser") = "Not Known"
          End If

          If Date_Entered_Ordinal >= 0 Then
            newAuditRecord("ChangeDate") = AuditSource_RS.Rows(AuditRowCounter)(Date_Entered_Ordinal)
          Else
            newAuditRecord("ChangeDate") = #1/1/1900#
          End If

          newAuditRecord("ChangeField") = AuditSource_RS.Columns(FieldCount).ColumnName
          newAuditRecord("ChangeFrom") = "New Record"
          newAuditRecord("ChangeTo") = Nz(AuditSource_RS.Rows(AuditRowCounter)(FieldCount), "Null").ToString
          newAuditRecord("ChangeRecordDescription") = Nz(AuditSource_RS.Rows(AuditRowCounter)("RecordDescription"), ".").ToString

          AuditResults_RS.Rows.Add(newAuditRecord)
        Next

      Else
        ' If this is a conventional change comparison situation, compare ona field-by-field basis looking for changes.

        For FieldCount = 1 To (AuditSource_RS.Columns.Count - 1) ' Don't compare the first field as it is the RecordDescription.
          Has_Changed_Flag = False

          ' Get 'From' and 'To' values.
          FirstValue = Nz(AuditSource_RS.Rows(AuditRowCounter - 1)(FieldCount), "")
          SecondValue = Nz(AuditSource_RS.Rows(AuditRowCounter)(FieldCount), "")

          ' If the types are the same then compare them. (they may not be if one field is Null and the other isn't).

          If FirstValue.GetType Is SecondValue.GetType Then

            If CompareValue(FirstValue, SecondValue) <> 0 Then
              ' If the values do not match then, (ignoring RN and DateEntered fields), do a rounded comparison on Doubles
              ' doubles are compared to 12 signficant figures. If no rounding is used then unchanged values do not always
              ' compare as equal !

              If (AuditSource_RS.Columns(FieldCount).ColumnName <> "RN") And (FieldCount <> Date_Entered_Ordinal) Then
                Has_Changed_Flag = True

                ' Double check the comparison for Float & Double data types. Compare to 12 significant figures.

                If (AuditSource_RS.Columns(FieldCount).DataType Is GetType(System.Double)) Then
                  Dim FirstNumber As Double
                  Dim SecondNumber As Double
                  Dim Precision As Integer

                  FirstNumber = CDbl(Nz(AuditSource_RS.Rows(AuditRowCounter - 1)(FieldCount), 0))
                  SecondNumber = CDbl(Nz(AuditSource_RS.Rows(AuditRowCounter)(FieldCount), 0))

                  If (FirstNumber = 0) Then
                    Precision = 12
                  Else
                    Precision = 12 - Math.Max(0, Int(Math.Log10(Math.Abs(FirstNumber))))
                    If Precision < 0 Then Precision = 0
                  End If

                  If Math.Round(FirstNumber, Precision) = Math.Round(SecondNumber, Precision) Then
                    Has_Changed_Flag = False
                  End If

                ElseIf (AuditSource_RS.Columns(FieldCount).DataType Is GetType(System.Decimal)) Then
                  Dim FirstNumber As Decimal
                  Dim SecondNumber As Decimal
                  Dim Precision As Integer

                  FirstNumber = CDec(Nz(AuditSource_RS.Rows(AuditRowCounter - 1)(FieldCount), 0))
                  SecondNumber = CDec(Nz(AuditSource_RS.Rows(AuditRowCounter)(FieldCount), 0))

                  Precision = 12 - Math.Max(0, Int(Math.Log10(Math.Abs(FirstNumber))))
                  If Precision < 0 Then Precision = 0

                  If Math.Round(FirstNumber, Precision) = Math.Round(SecondNumber, Precision) Then
                    Has_Changed_Flag = False
                  End If

                End If
              End If

            End If
          Else
            ' Types were different, no need to compare.

            Has_Changed_Flag = True
          End If

          If Has_Changed_Flag = True Then
            ' Save Difference to Audit Table.

            newAuditRecord = AuditResults_RS.NewRow

            RNCounter += 1
            newAuditRecord("RN") = RNCounter

            newAuditRecord("ChangeTableName") = SourceTable
            newAuditRecord("ChangeRN") = AuditSource_RS.Rows(AuditRowCounter)("RN") ' AuditSource_RS!RN
            newAuditRecord("ChangeID") = AuditSource_RS.Rows(AuditRowCounter)(AuditID_Ordinal) '  AuditSource_RS!AuditID

            If UserName_Ordinal >= 0 Then
              newAuditRecord("ChangeUser") = AuditSource_RS.Rows(AuditRowCounter)(UserName_Ordinal)
            Else
              newAuditRecord("ChangeUser") = "Not Known"
            End If

            If Date_Entered_Ordinal >= 0 Then
              newAuditRecord("ChangeDate") = AuditSource_RS.Rows(AuditRowCounter)(Date_Entered_Ordinal)
            Else
              newAuditRecord("ChangeDate") = #1/1/1900#
            End If

            newAuditRecord("ChangeField") = AuditSource_RS.Columns(FieldCount).ColumnName
            newAuditRecord("ChangeFrom") = Nz(AuditSource_RS.Rows(AuditRowCounter - 1)(FieldCount), "Null").ToString
            newAuditRecord("ChangeTo") = Nz(AuditSource_RS.Rows(AuditRowCounter)(FieldCount), "Null").ToString
            newAuditRecord("ChangeRecordDescription") = Nz(AuditSource_RS.Rows(AuditRowCounter - 1)("RecordDescription"), ".").ToString

            AuditResults_RS.Rows.Add(newAuditRecord)
          End If

        Next FieldCount
      End If

      AuditRowCounter += 1
    End While

    Return AuditResults_RS

  End Function


    ''' <summary>
    ''' Audit_s the preparation.
    ''' </summary>
    ''' <param name="pSourceTable">The p source table.</param>
    ''' <returns>AUDIT_ParameterValues.</returns>
  Private Function Audit_Preparation(ByVal pSourceTable As StandardDataset) As AUDIT_ParameterValues
    ' *******************************************************************************
    '
    ' Purpose:  Return Table Specific information for the Table Change AUDIT Report
    '
    ' Accepts:  pSourceTable : TableName to process
    '
    ' Returns:  AUDIT_ParameterValues : RecordDescription - SQL fragment which returns descriptive
    '                                                       information about the changed records. Often a meaningful
    '                                                       field form the linked table. e.g. Instrument Name
    '                                 : AuditJoin         - SQL fragment to JOIN a second table to the query.
    '                                                       Used to enable the retrieval of normalised information,
    '                                                       e.g. Instrument Name, Fund Name etc.
    ' *******************************************************************************

    On Error Resume Next

    ' Defaulty Values.
    Audit_Preparation.RecordDescription = "('ID = ' & AuditID)"
    Audit_Preparation.AuditJoin = ""

    Select Case pSourceTable.ChangeID
      Case RenaissanceChangeID.tblBookmark  '  "tblBookmark"
        Audit_Preparation.RecordDescription = "('ID = ' + Convert(Varchar(200), AuditID))"
        Audit_Preparation.AuditJoin = ""

      Case RenaissanceChangeID.tblChangeControl ' "tblChangeControl"
        Audit_Preparation.RecordDescription = "('ID = ' + Convert(Varchar(200), AuditID) + ', Ref. = ' + Convert(Varchar(200), Change_ReferenceNumber)  )"
        Audit_Preparation.AuditJoin = ""

      Case RenaissanceChangeID.tblCompoundTransaction '  "tblCompoundTransaction"
        Audit_Preparation.RecordDescription = "('ID = ' + Convert(Varchar(200), AuditID))"
        Audit_Preparation.AuditJoin = ""

      Case RenaissanceChangeID.tblCounterparty ' "tblCounterparty"
        Audit_Preparation.RecordDescription = "('ID = ' + Convert(Varchar(200), AuditID))"
        Audit_Preparation.AuditJoin = ""

      Case RenaissanceChangeID.tblCurrency ' "tblCurrency"
        Audit_Preparation.RecordDescription = "('ID = ' + Convert(Varchar(200), AuditID) + ', Code = ' + Convert(Varchar(200), CurrencyCode))"
        Audit_Preparation.AuditJoin = ""

      Case RenaissanceChangeID.tblDataTask
        Audit_Preparation.RecordDescription = "('ID = ' + Convert(Varchar(200), AuditID))"
        Audit_Preparation.AuditJoin = ""

      Case RenaissanceChangeID.tblDealingPeriod ' "tblDealingPeriod"
        Audit_Preparation.RecordDescription = "('ID = ' + Convert(Varchar(200), AuditID))"
        Audit_Preparation.AuditJoin = ""

      Case RenaissanceChangeID.tblEstimate ' "tblEstimate"
        Audit_Preparation.RecordDescription = "('ID = ' + Convert(Varchar(200), AuditID) + ', Item = ' + Convert(Varchar(200), EstimateItem))"
        Audit_Preparation.AuditJoin = ""

      Case RenaissanceChangeID.tblFlorenceGroup
        Audit_Preparation.RecordDescription = "('ID = ' + Convert(Varchar(200), AuditID))"
        Audit_Preparation.AuditJoin = ""

      Case RenaissanceChangeID.tblFlorenceItems
        Audit_Preparation.RecordDescription = "('ID = ' + Convert(Varchar(200), AuditID))"
        Audit_Preparation.AuditJoin = ""

      Case RenaissanceChangeID.tblFlorenceStatus
        Audit_Preparation.RecordDescription = "('ID = ' + Convert(Varchar(200), AuditID))"
        Audit_Preparation.AuditJoin = ""

      Case RenaissanceChangeID.tblFlorenceStatusIDs
        Audit_Preparation.RecordDescription = "('ID = ' + Convert(Varchar(200), AuditID))"
        Audit_Preparation.AuditJoin = ""

      Case RenaissanceChangeID.tblFund ' "tblFund"
        Audit_Preparation.RecordDescription = "('ID = ' + Convert(Varchar(200), AuditID) + ', Code = ' + Convert(Varchar(200), FundCode))"
        Audit_Preparation.AuditJoin = ""

      Case RenaissanceChangeID.tblFundContactDetails
        Audit_Preparation.RecordDescription = "('ID = ' + Convert(Varchar(200), AuditID))"
        Audit_Preparation.AuditJoin = ""

      Case RenaissanceChangeID.tblFundType ' "tblFundType"
        Audit_Preparation.RecordDescription = "('ID = ' + Convert(Varchar(200), AuditID) + ', Desc = ' + Convert(Varchar(200), FundTypeDescription))"
        Audit_Preparation.AuditJoin = ""

      Case RenaissanceChangeID.tblFX ' "tblFX"
        Audit_Preparation.RecordDescription = "('Inst = ' + Convert(Varchar(200), Instruments.InstrumentDescription) + ', Date = ' + Convert(Varchar(200), FXDate))"
        Audit_Preparation.AuditJoin = " LEFT JOIN fn_tblInstrument_SelectKD(Null) Instruments on tblFX.FXCurrencyCode = Instruments.InstrumentID"

      Case RenaissanceChangeID.tblGroupList ' "tblGroupList"
        Audit_Preparation.RecordDescription = "('ID = ' + Convert(Varchar(200), AuditID))"
        Audit_Preparation.AuditJoin = ""

        '   Case "tblGroups"
        '     Audit_Preparation.RecordDescription = "('ID = ' + Convert(Varchar(200), AuditID))"
        '     Audit_Preparation.AuditJoin = ""

        '   Case "tblUserPermissions"
        '     Audit_Preparation.RecordDescription = "('ID = ' + Convert(Varchar(200), AuditID))"
        '     Audit_Preparation.AuditJoin = ""

      Case RenaissanceChangeID.tblIndexMapping
        Audit_Preparation.RecordDescription = "('ID = ' + Convert(Varchar(200), AuditID))"
        Audit_Preparation.AuditJoin = ""

      Case RenaissanceChangeID.tblInstrument ' "tblInstrument"
        Audit_Preparation.RecordDescription = "('Inst = ' + Convert(Varchar(200), Instruments.InstrumentDescription))"
        Audit_Preparation.AuditJoin = " LEFT JOIN fn_tblInstrument_SelectKD(Null) Instruments on tblInstrument.InstrumentID = Instruments.InstrumentID"

      Case RenaissanceChangeID.tblInstrumentType ' "tblInstrumentType"
        Audit_Preparation.RecordDescription = "('ID = ' + Convert(Varchar(200), AuditID))"
        Audit_Preparation.AuditJoin = ""

      Case RenaissanceChangeID.tblInvestorGroup ' "tblInvestorGroup"
        Audit_Preparation.RecordDescription = "('ID = ' + Convert(Varchar(200), AuditID))"
        Audit_Preparation.AuditJoin = ""

      Case RenaissanceChangeID.tblLimits ' "tblLimits"
        Audit_Preparation.RecordDescription = "('ID = ' + Convert(Varchar(200), AuditID))"
        Audit_Preparation.AuditJoin = ""

      Case RenaissanceChangeID.tblLimitType  ' "tblLimitType"
        Audit_Preparation.RecordDescription = "('ID = ' + Convert(Varchar(200), AuditID))"
        Audit_Preparation.AuditJoin = ""

      Case RenaissanceChangeID.tblLiquidityLimit ' "tblLiquidityLimit"
        Audit_Preparation.RecordDescription = "('ID = ' + Convert(Varchar(200), AuditID))"
        Audit_Preparation.AuditJoin = ""

      Case RenaissanceChangeID.tblMarketInstruments
        Audit_Preparation.RecordDescription = "('ID = ' + Convert(Varchar(200), AuditID))"
        Audit_Preparation.AuditJoin = ""

      Case RenaissanceChangeID.tblMarketPrices
        Audit_Preparation.RecordDescription = "('ID = ' + Convert(Varchar(200), AuditID))"
        Audit_Preparation.AuditJoin = ""

      Case RenaissanceChangeID.tblPerson ' "tblPerson"
        Audit_Preparation.RecordDescription = "('Name = ' + Convert(Varchar(200), People.Person))"
        Audit_Preparation.AuditJoin = " LEFT JOIN fn_tblPerson_SelectKD(Null) People on tblPerson.PersonID = People.PersonID"

      Case RenaissanceChangeID.tblPortfolioData
        Audit_Preparation.RecordDescription = "('ID = ' + Convert(Varchar(200), AuditID))"
        Audit_Preparation.AuditJoin = ""

      Case RenaissanceChangeID.tblPortfolioIndex
        Audit_Preparation.RecordDescription = "('ID = ' + Convert(Varchar(200), AuditID))"
        Audit_Preparation.AuditJoin = ""

      Case RenaissanceChangeID.tblPrice ' "tblPrice"
        Audit_Preparation.RecordDescription = "('Inst = ' + Convert(Varchar(200), Instruments.InstrumentDescription) + ', Date = ' + Convert(Varchar(200), PriceDate))"
        Audit_Preparation.AuditJoin = " LEFT JOIN fn_tblInstrument_SelectKD(Null) Instruments on tblPrice.PriceInstrument = Instruments.InstrumentID"

      Case RenaissanceChangeID.tblReferentialIntegrity ' "tblReferentialIntegrity"
        Audit_Preparation.RecordDescription = "('ID = ' + Convert(Varchar(200), AuditID))"
        Audit_Preparation.AuditJoin = ""

      Case RenaissanceChangeID.tblSectorLimit ' "tblSectorLimit"
        Audit_Preparation.RecordDescription = "('ID = ' + Convert(Varchar(200), AuditID))"
        Audit_Preparation.AuditJoin = ""

      Case RenaissanceChangeID.tblSubFund ' "tblSubfund"
        Audit_Preparation.RecordDescription = "('ID = ' + Convert(Varchar(200), AuditID))"
        Audit_Preparation.AuditJoin = ""

      Case RenaissanceChangeID.tblSystemDates ' "tblSystemDates"
        Audit_Preparation.RecordDescription = "('ID = ' + Convert(Varchar(200), AuditID))"
        Audit_Preparation.AuditJoin = ""

			Case RenaissanceChangeID.tblTransaction	' "tblTransaction"
				Audit_Preparation.RecordDescription = "('Ticket = ' + CAST(tblTransaction.TransactionTicket AS varchar) COLLATE Latin1_General_CI_AS + ', Inst = ' + CAST(Instruments.InstrumentDescription AS varchar) COLLATE Latin1_General_CI_AS)"
				' Audit_Preparation.RecordDescription = "('Ticket = ' + Convert(Varchar(200), tblTransaction.TransactionTicket) + ', Inst = ' + Convert(Varchar(200), Instruments.InstrumentDescription))"
				Audit_Preparation.AuditJoin = " LEFT JOIN fn_tblInstrument_SelectKD(Null) Instruments on tblTransaction.TransactionInstrument = Instruments.InstrumentID"

      Case RenaissanceChangeID.tblTransactionType ' "tblTransactionType"
        Audit_Preparation.RecordDescription = "('ID = ' + Convert(Varchar(200), AuditID))"
        Audit_Preparation.AuditJoin = ""

      Case Else
        Audit_Preparation.RecordDescription = "('RN = ' + Convert(Varchar(200), RN))"
        Audit_Preparation.AuditJoin = ""

    End Select

    On Error GoTo 0

  End Function



End Module
