' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="Globals.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
''' <summary>
''' Class Globals
''' </summary>
Module Globals

  ''' <summary>
  ''' The SPLAS h_ DISPLAY
  ''' </summary>
  Public Const SPLASH_DISPLAY As Integer = 3 ' Minimum Duration of Splash Display

  ' Base registry key for program settings

  ''' <summary>
  ''' The REGISTR y_ BASE
  ''' </summary>
  Public Const REGISTRY_BASE As String = "Software\Primonial\Venice"

  ' Standard Connection Name, as used with the DataHandler object.

  ''' <summary>
  ''' The VENIC e_ CONNECTION
  ''' </summary>
  Public Const VENICE_CONNECTION As String = "cnnVenice"

  ''' <summary>
  ''' The VENIC e_ custom report_ benchmark_ application name
  ''' </summary>
  Public Const VENICE_CustomReport_Benchmark_ApplicationName As String = "VeniceBenchmark"

  ' Standard Form Caching Counters

  ''' <summary>
  ''' The STANDAR d_ entry form_ CACH e_ COUNT
  ''' </summary>
  Public Const STANDARD_EntryForm_CACHE_COUNT As Integer = 0 ' 2
  ''' <summary>
  ''' The STANDAR d_ report form_ CACH e_ COUNT
  ''' </summary>
  Public Const STANDARD_ReportForm_CACHE_COUNT As Integer = 0 ' 1
  ''' <summary>
  ''' The STANDAR d_ single form_ CACH e_ COUNT
  ''' </summary>
  Public Const STANDARD_SingleForm_CACHE_COUNT As Integer = 0 ' 1

  ' Standard reference Dates & timings.
  ' LAST_Second is used to convert KDs to a precise 'Whole Day' value, also used to
  ' evaluate the 'Whole Day' status of a given Date.
  ' Now in RenaissanceGlobals - NPP 1 Oct 2007

  'Public Const KNOWLEDGEDATE_NOW As Date = #1/1/1900#
  'Public Const Renaissance_BaseDate As Date = #1/1/1900#
  'Public Const LAST_SECOND As Integer = 86399

  ' Default Starting Fund price.

  ''' <summary>
  ''' The VENIC e_ STARTINGFUNDPRICE
  ''' </summary>
  Public Const VENICE_STARTINGFUNDPRICE As Double = 100.0

  ' Standard SQL Command Timeouts

  ''' <summary>
  ''' The DEFAUL t_ SQLCOMMAN d_ TIMEOUT
  ''' </summary>
  Public Const DEFAULT_SQLCOMMAND_TIMEOUT As Integer = 600
  ''' <summary>
  ''' The ATTRIBUTIO n_ SQLCOMMAN d_ TIMEOUT
  ''' </summary>
  Public Const ATTRIBUTION_SQLCOMMAND_TIMEOUT As Integer = 900
  ''' <summary>
  ''' The TRANSACTIO n_ SQLCOMMAN d_ TIMEOUT
  ''' </summary>
  Public Const TRANSACTION_SQLCOMMAND_TIMEOUT As Integer = 900

  ' Risk

  ''' <summary>
  ''' The RIS k_ MINIMUMEXPOSURETOSAVE
  ''' </summary>
  Public Const RISK_MINIMUMEXPOSURETOSAVE As Double = 0

  ' Standard Date Formats
  ' Now in RenaissanceGlobals - NPP 1 Oct 2007

  ' Public Const DISPLAYMEMBER_DATEFORMAT As String = "dd MMM yyyy"
  ' Public Const DISPLAYMEMBER_LONGDATEFORMAT As String = "dd MMM yyyy HH:mm:ss"

  ' Public Const REPORT_DATEFORMAT As String = "dd MMMM yyyy"
  ' Public Const REPORT_LONGDATEFORMAT As String = "dd MMM yyyy HH:mm:ss"
  ' Public Const REPORT_MonthAndYear_DATEFORMAT As String = "MMMM yyyy"

  ' Public Const QUERY_SHORTDATEFORMAT As String = "dd MMM yyyy"
  ' Public Const QUERY_LONGDATEFORMAT As String = "dd MMM yyyy HH:mm:ss"

  'Public Const FILENAME_DATEFORMAT As String = "yyyyMMdd_HHmmss"

  ' Public Const TIMEFORMAT As String = "HH:mm:ss"

  ' Reference currency for reporting. Not used widely yet.

  ''' <summary>
  ''' The REFERENC e_ REPOR t_ CURRENCY
  ''' </summary>
  Public Const REFERENCE_REPORT_CURRENCY As Integer = 3 ' EUR

  ' Reference 'Period' Hardly used at present.

  ''' <summary>
  ''' The REFERENCE time PERIOD
  ''' </summary>
  Public Const REFERENCE_PERIOD As RenaissanceGlobals.DealingPeriod = RenaissanceGlobals.DealingPeriod.Daily ' 

  ' 

  ''' <summary>
  ''' The CUSTO m_ BACKGROUN d_ UNCHANGED
  ''' </summary>
  Public CUSTOM_BACKGROUND_UNCHANGED As System.Drawing.Color = System.Drawing.SystemColors.GradientInactiveCaption
  ''' <summary>
  ''' The CUSTO m_ BACKGROUN d_ CHANGED
  ''' </summary>
  Public CUSTOM_BACKGROUND_CHANGED As System.Drawing.Color = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))

  ' Temporary Fudge for Index
  ''' <summary>
  ''' The DEFAUL t_ BENCHMAR k_ INDEX
  ''' </summary>
  Public Const DEFAULT_BENCHMARK_INDEX As Integer = 1

  ' Query 'WHERE' to determine an incomplete Transaction.
  ' Must be able to integrate this with an existing WHERE clause.

  ''' <summary>
  ''' The INCOMPLET e_ TRANSACTIO n_ QUERY
  ''' </summary>
  Public Const INCOMPLETE_TRANSACTION_QUERY As String = "((TransactionInstructionFlag=0) OR (TransactionBankConfirmation=0) OR (TransactionInitialDataEntry=0) OR (TransactionAmountConfirmed=0) OR (TransactionSettlementConfirmed=0) OR (TransactionFinalDataEntry=0))"

  ' Query 'WHERE' to determine a 'Real' Characteristic entry. 

  ''' <summary>
  ''' The ENTERE d_ CHARACTERISTIC s_ QUERY
  ''' </summary>
  Public Const ENTERED_CHARACTERISTICS_QUERY As String = "(RiskDataID > 0) OR (DataWeighting <> 0)"

  ' Default value for StatusGroupFilter (Sophis Trade Status group) and AdministratorDatesFilter
  ' Mostly these constants are intended as a place holder to be changed later.

  ''' <summary>
  ''' The DEFAUL t_ STATUSGROUPFILTER
  ''' </summary>
  Public Const DEFAULT_STATUSGROUPFILTER As String = ""
  ''' <summary>
  ''' The DEFAUL t_ ADMINISTRATORDATESFILTER
  ''' </summary>
  Public Const DEFAULT_ADMINISTRATORDATESFILTER As Integer = 0
  ''' <summary>
  ''' The DEFAUL t_ STATUSGROUPFILTE r_ EOD
  ''' </summary>
  Public Const DEFAULT_STATUSGROUPFILTER_EOD As String = "EOD"

  ''' <summary>
  ''' The DEFAUL t_ EO d_ TRADESTATUS
  ''' </summary>
  Public Const DEFAULT_EOD_TRADESTATUS As Integer = 20 ' (Reconciled) EG, for notional balance and fee transactions.

  ' Standard Venice form interface.
  ' Must be implemented by any form that is cached.

  ''' <summary>
  ''' Interface StandardVeniceForm
  ''' </summary>
  Interface StandardVeniceForm
    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
    Property IsOverCancelButton() As Boolean
    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
    ReadOnly Property IsInPaint() As Boolean
    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
    ReadOnly Property InUse() As Boolean
    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
    ReadOnly Property FormOpenFailed() As Boolean
    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
    ReadOnly Property MainForm() As VeniceMain
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
    Sub ResetForm()
    ''' <summary>
    ''' Closes the form.
    ''' </summary>
    Sub CloseForm()
  End Interface

  ''' <summary>
  ''' The DEFAUL t_ F x_ FO r_ INSTRUMEN t_ TYPES
  ''' </summary>
  Public DEFAULT_FX_FOR_INSTRUMENT_TYPES() As RenaissanceGlobals.InstrumentTypes = {RenaissanceGlobals.InstrumentTypes.Fund, RenaissanceGlobals.InstrumentTypes.Share}

  ' Form ID Enumeration. Intended to uniquely identify each Form Type

  ''' <summary>
  ''' Enum VeniceFormID : Enumerates a unique ID for all Venice forms.
  ''' 
  ''' Note that permissions refer to these IDs by name. The actual number is not important, thus there is no consequence to inserting new entries.
  ''' </summary>
  Public Enum VeniceFormID
    ' **************************************************************************************
    '
    ' Enumeration member name MUST be the same as the associated form object.
    ' this is so that the generic 'New_VeniceForm()' function in the Venice Main form will 
    ' be able to instantiate the correct object.
    '
    ' **************************************************************************************

    ''' <summary>
    ''' The none
    ''' </summary>
    None = 0
    ''' <summary>
    ''' The FRM venice main
    ''' </summary>
    frmVeniceMain

    ''' <summary>
    ''' The FRM about
    ''' </summary>
    frmAbout
    ''' <summary>
    ''' The FRM approve transactions
    ''' </summary>
    frmApproveTransactions
    ''' <summary>
    ''' The FRM attributions reports
    ''' </summary>
    frmAttributionsReports
    ''' <summary>
    ''' The FRM AUM report
    ''' </summary>
    frmAUMReport
    ''' <summary>
    ''' The FRM benckmark attribution report
    ''' </summary>
    frmBenckmarkAttributionReport
    ''' <summary>
    ''' The FRM blotter reprints
    ''' </summary>
    frmBlotterReprints
    ''' <summary>
    ''' The FRM bookmark
    ''' </summary>
    frmBookmark
    ''' <summary>
    ''' The FRM Brokers
    ''' </summary>
    frmBroker
    ''' <summary>
    ''' The FRM Brokers Accounts
    ''' </summary>
    frmBrokerAccount
    ''' <summary>
    ''' The FRM calculate attributions
    ''' </summary>
    frmCalculateAttributions
    ''' <summary>
    ''' The FRM cash ladder
    ''' </summary>
    frmCashLadder
    ''' <summary>
    ''' The FRM cash reports
    ''' </summary>
    frmCashReports
    ''' <summary>
    ''' The FRM change control review
    ''' </summary>
    frmChangeControlReview ' To Do
    ''' <summary>
    ''' The FRM characteristics
    ''' </summary>
    frmCharacteristics
    ''' <summary>
    ''' The FRM compound transaction
    ''' </summary>
    frmCompoundTransaction
    ''' <summary>
    ''' The FRM connection
    ''' </summary>
    frmConnection
    ''' <summary>
    ''' The FRM counterparty
    ''' </summary>
    frmCounterparty
    ''' <summary>
    ''' The FRM currency
    ''' </summary>
    frmCurrency
    ''' <summary>
    ''' The FRM data task
    ''' </summary>
    frmDataTask
    ''' <summary>
    ''' The FRM delta grid
    ''' </summary>
    frmDeltaGrid
    ''' <summary>
    ''' The FRM expenses report
    ''' </summary>
    frmExpensesReport
    ''' <summary>
    ''' The Fees Add / Edit Form ID.
    ''' </summary>
    frmFees
    ''' <summary>
    ''' The FRM fund
    ''' </summary>
    frmFund
    ''' <summary>
    ''' The FRM fund fee reports
    ''' </summary>
    frmFundFeeReports
    ''' <summary>
    ''' The FRM fund type
    ''' </summary>
    frmFundType
    ''' <summary>
    ''' The FRM fund gearing report
    ''' </summary>
    frmFundGearingReport
    ''' <summary>
    ''' The FRM FX
    ''' </summary>
    frmFX
    ''' <summary>
    ''' The FRM FX grid
    ''' </summary>
    frmFXGrid
    ''' <summary>
    ''' The frmImportTransactions
    ''' </summary>
    frmImportTransactions
    ''' <summary>
    ''' The FRM instrument
    ''' </summary>
    frmInstrument
    ''' <summary>
    ''' The FRM instrument parent
    ''' </summary>
    frmInstrumentParent
    ''' <summary>
    ''' The FRM instrument type
    ''' </summary>
    frmInstrumentType
    ''' <summary>
    ''' The FRM investor group
    ''' </summary>
    frmInvestorGroup
    ''' <summary>
    ''' The FRM limits
    ''' </summary>
    frmLimits
    ''' <summary>
    ''' The FRM liquidity report
    ''' </summary>
    frmLiquidityReport
    ''' <summary>
    ''' The FRM load PFPC data
    ''' </summary>
    frmLoadPacVL
    ''' <summary>
    ''' The FRM load PFPC data
    ''' </summary>
    frmLoadPFPCData
    ''' <summary>
    ''' The FRM manage sub funds
    ''' </summary>
    frmManageSubFunds
    ''' <summary>
    ''' The FRM market instruments
    ''' </summary>
    frmMarketInstruments
    ''' <summary>
    ''' The FRM master fund allocation report
    ''' </summary>
    frmMasterFundAllocationReport
    'frmNoticeReport
    ''' <summary>
    ''' The FRM notional management reports
    ''' </summary>
    frmNotionalManagementReports
    ''' <summary>
    ''' The FRM pending transactions report
    ''' </summary>
    frmPendingTransactionsReport
    ''' <summary>
    ''' The FRM person
    ''' </summary>
    frmPerson
    ''' <summary>
    ''' The FRM portfolio edit
    ''' </summary>
    frmPortfolioEdit
    ''' <summary>
    ''' The FRM portfolio index
    ''' </summary>
    frmPortfolioIndex
    ''' <summary>
    ''' The FRM portfolio report
    ''' </summary>
    frmPortfolioReport
    ''' <summary>
    ''' The FRM portfolio change report
    ''' </summary>
    frmPortfolioChangeReport
    ''' <summary>
    ''' The FRM portfolio large positions report
    ''' </summary>
    frmPortfolioLargePositionsReport
    ''' <summary>
    ''' The FRM PFPC reconciliation report
    ''' </summary>
    frmPFPCReconciliationReport
    ''' <summary>
    ''' The FRM price
    ''' </summary>
    frmPrice
    ''' <summary>
    ''' The FRM price grid
    ''' </summary>
    frmPriceGrid
    ''' <summary>
    ''' The FRM price report
    ''' </summary>
    frmPriceReport
    ''' <summary>
    ''' The FRM profitand loss report
    ''' </summary>
    frmProfitandLossReport
    ''' <summary>
    ''' The FRM reconciliation process
    ''' </summary>
    frmReconciliationProcess
    ''' <summary>
    ''' The FRM reconciliation reports
    ''' </summary>
    frmReconciliationReports
    ''' <summary>
    ''' The FRM risk browser
    ''' </summary>
    frmRiskBrowser
    ''' <summary>
    ''' The FRM risk characteristics
    ''' </summary>
    frmRiskCharacteristics
    ''' <summary>
    ''' The FRM risk exposure report
    ''' </summary>
    frmRiskExposureReport
    ''' <summary>
    ''' The FRM risk exposure change report
    ''' </summary>
    frmRiskExposureChangeReport
    ''' <summary>
    ''' The FRM select change control
    ''' </summary>
    frmSelectChangeControl
    ''' <summary>
    ''' The FRM select compound transaction
    ''' </summary>
    frmSelectCompoundTransaction
    ''' <summary>
    ''' The FRM select futures notional
    ''' </summary>
    frmSelectFuturesNotional
    ''' <summary>
    ''' The FRM select instruments
    ''' </summary>
    frmSelectInstruments
    ''' <summary>
    ''' The FRM select transaction
    ''' </summary>
    frmSelectTransaction
    ''' <summary>
    ''' The FRM select transaction list
    ''' </summary>
    frmSelectTransactionList
    ''' <summary>
    ''' The FRM set fund milestone
    ''' </summary>
    frmSetFundMilestone
    ''' <summary>
    ''' The FRM set knowledge date
    ''' </summary>
    frmSetKnowledgeDate
    ''' <summary>
    ''' The FRM sub fund
    ''' </summary>
    frmSubFund
    ''' <summary>
    ''' The FRM sub fund parent
    ''' </summary>
    frmSubFundParent
    ''' <summary>
    ''' The FRM system dates
    ''' </summary>
    frmSystemDates              ' To Do
    ''' <summary>
    ''' The FRM transaction status report
    ''' </summary>
    frmTransactionStatusReport
    ''' <summary>
    ''' The FRM transaction type
    ''' </summary>
    frmTransactionType
    ''' <summary>
    ''' The FRM transaction
    ''' </summary>
    frmTransaction
    ''' <summary>
    ''' The FRM unit holder statement
    ''' </summary>
    frmUnitHolderStatement
    ''' <summary>
    ''' The FRM unit price report
    ''' </summary>
    frmUnitPriceReport
    ''' <summary>
    ''' The FRM update transactions
    ''' </summary>
    frmUpdateTransactions
    ''' <summary>
    ''' The FRM user permissions
    ''' </summary>
    frmUserPermissions
    ''' <summary>
    ''' The FRM valuation report
    ''' </summary>
    frmValuationReport
    ''' <summary>
    ''' The FRM view report
    ''' </summary>
    frmViewReport

    ' Permissioning for Trade Approval

    ''' <summary>
    ''' The trades_ fund manager approval
    ''' </summary>
    Trades_FundManagerApproval
    ''' <summary>
    ''' The trades_ middle office approval
    ''' </summary>
    Trades_MiddleOfficeApproval

    ' Non-Form permisioning for Change Control roles.

    'ChangeCtrlAddNew
    ''' <summary>
    ''' The change CTRL IT review
    ''' </summary>
    ChangeCtrlITReview
    ''' <summary>
    ''' The change CTRL authorise IT
    ''' </summary>
    ChangeCtrlAuthoriseIT
    ''' <summary>
    ''' The change CTRL authorise owner
    ''' </summary>
    ChangeCtrlAuthoriseOwner
    ''' <summary>
    ''' The change CTRL authorise business
    ''' </summary>
    ChangeCtrlAuthoriseBusiness
    ''' <summary>
    ''' The change CTRL accept IT
    ''' </summary>
    ChangeCtrlAcceptIT
    ''' <summary>
    ''' The change CTRL accept owner
    ''' </summary>
    ChangeCtrlAcceptOwner
    ''' <summary>
    ''' The change CTRL accept business
    ''' </summary>
    ChangeCtrlAcceptBusiness

    ''' <summary>
    ''' The max value
    ''' </summary>
    MaxValue
  End Enum

  ''' <summary>
  ''' Enum VeniceReportID
  ''' </summary>
  Public Enum VeniceReportID
    ' **************************************************************************************
    '
    ' Enumeration member name MUST be the same as the associated report.
    ' this is so that report permissioning can be performed.
    '
    ' **************************************************************************************

    ''' <summary>
    ''' The none
    ''' </summary>
    None = 0

    ''' <summary>
    ''' The RPT asset allocation
    ''' </summary>
    rptAssetAllocation
    ''' <summary>
    ''' The RPT asset allocation summary
    ''' </summary>
    rptAssetAllocationSummary
    ''' <summary>
    ''' The RPT assets under management
    ''' </summary>
    rptAssetsUnderManagement
    ''' <summary>
    ''' The RPT assets under management detail
    ''' </summary>
    rptAssetsUnderManagementDetail
    ''' <summary>
    ''' The RPT assets under management accounting detail
    ''' </summary>
    rptAssetsUnderManagementAccountingDetail
    ''' <summary>
    ''' The RPT assets under management_ net flows
    ''' </summary>
    rptAssetsUnderManagement_NetFlows
    ''' <summary>
    ''' The RPT attribution
    ''' </summary>
    rptAttribution
    ''' <summary>
    ''' The RPT attribution best worst
    ''' </summary>
    rptAttributionBestWorst
    ''' <summary>
    ''' The RPT attribution money
    ''' </summary>
    rptAttributionMoney
    ''' <summary>
    ''' The RPT benchmark attribution
    ''' </summary>
    rptBenchmarkAttribution
    ''' <summary>
    ''' The RPT cash availability1
    ''' </summary>
    rptCashAvailability1
    ''' <summary>
    ''' The RPT cash availability2
    ''' </summary>
    rptCashAvailability2
    ''' <summary>
    ''' The RPT cash availability3
    ''' </summary>
    rptCashAvailability3
    ''' <summary>
    ''' The RPT cash move detail_ settlement
    ''' </summary>
    rptCashMoveDetail_Settlement
    ''' <summary>
    ''' The RPT cash move detail_ value
    ''' </summary>
    rptCashMoveDetail_Value
    ''' <summary>
    ''' The RPT cash movement
    ''' </summary>
    rptCashMovement
    ''' <summary>
    ''' The RPT cash value1
    ''' </summary>
    rptCashValue1
    ''' <summary>
    ''' The RPT cash value2
    ''' </summary>
    rptCashValue2
    ''' <summary>
    ''' The RPT cash value3
    ''' </summary>
    rptCashValue3
    ''' <summary>
    ''' The RPT CC form
    ''' </summary>
    rptCCForm
    ''' <summary>
    ''' The RPT CC summary
    ''' </summary>
    rptCCSummary
    ''' <summary>
    ''' The RPT expense by month
    ''' </summary>
    rptExpenseByMonth
    ''' <summary>
    ''' The RPT fund fee report
    ''' </summary>
    rptFundFeeReport
    ''' <summary>
    ''' The RPT fund fee status report
    ''' </summary>
    rptFundFeeStatusReport
    ''' <summary>
    ''' The RPT fund fee status detail report
    ''' </summary>
    rptFundFeeStatusDetailReport
    ''' <summary>
    ''' The RPT fund gearing
    ''' </summary>
    rptFundGearing
    ''' <summary>
    ''' The RPT fund performance report
    ''' </summary>
    rptFundPerformanceReport
    ''' <summary>
    ''' The RPT fund valuation
    ''' </summary>
    rptFundValuation
    ''' <summary>
    ''' The RPT incomplete transactions
    ''' </summary>
    rptIncompleteTransactions
    ''' <summary>
    ''' The RPT instrument positions report
    ''' </summary>
    rptInstrumentPositionsReport
    ''' <summary>
    ''' The RPT inventory reconciliation report
    ''' </summary>
    rptInventoryReconciliationReport
    ''' <summary>
    ''' The RPT liquidity report
    ''' </summary>
    rptLiquidityReport
    ''' <summary>
    ''' The RPT management fees by month
    ''' </summary>
    rptManagementFeesByMonth
    ''' <summary>
    ''' The RPT master fund allocation
    ''' </summary>
    rptMasterFundAllocation
    ''' <summary>
    ''' The RPT notice report
    ''' </summary>
    rptNoticeReport
    ''' <summary>
    ''' The RPT notional allocation report
    ''' </summary>
    rptNotionalAllocationReport
    ''' <summary>
    ''' The RPT pending transactions report
    ''' </summary>
    rptPendingTransactionsReport
    ''' <summary>
    ''' The RPT pending transactions report value
    ''' </summary>
    rptPendingTransactionsReportValue
    ''' <summary>
    ''' The RPT pending transactions report lookthrough
    ''' </summary>
    rptPendingTransactionsReportLookthrough
    ''' <summary>
    ''' The RPT PFPC portfolio reconcilliation
    ''' </summary>
    rptPFPCPortfolioReconcilliation
    ''' <summary>
    ''' The RPT PFPC portfolio reconcilliation detail
    ''' </summary>
    rptPFPCPortfolioReconcilliationDetail
    ''' <summary>
    ''' The RPT portfolio large positions
    ''' </summary>
    rptPortfolioLargePositions
    ''' <summary>
    ''' The RPT portfolio aggregated large positions
    ''' </summary>
    rptPortfolioAggregatedLargePositions
    ''' <summary>
    ''' The RPT prices
    ''' </summary>
    rptPrices
    ''' <summary>
    ''' The RPT profit and loss
    ''' </summary>
    rptProfitAndLoss
    ''' <summary>
    ''' The RPT profitand loss sector summary
    ''' </summary>
    rptProfitandLossSectorSummary
    ''' <summary>
    ''' The RPT pn L detail report
    ''' </summary>
    rptPnLDetailReport
    ''' <summary>
    ''' The RPT risk exposures change report
    ''' </summary>
    rptRiskExposuresChangeReport
    ''' <summary>
    ''' The RPT risk exposures report
    ''' </summary>
    rptRiskExposuresReport
    ''' <summary>
    ''' The RPT risk exposures detail report
    ''' </summary>
    rptRiskExposuresDetailReport
    ''' <summary>
    ''' The RPT risk policy
    ''' </summary>
    rptRiskPolicy
    ''' <summary>
    ''' The RPT risk policy master balanced
    ''' </summary>
    rptRiskPolicyMasterBalanced
    ''' <summary>
    ''' The RPT risk policy master event
    ''' </summary>
    rptRiskPolicyMasterEvent
    ''' <summary>
    ''' The RPT risk policy all funds
    ''' </summary>
    rptRiskPolicyAllFunds
    ''' <summary>
    ''' The RPT sector bips
    ''' </summary>
    rptSectorBips
    ''' <summary>
    ''' The RPT sector returns
    ''' </summary>
    rptSectorReturns
    ''' <summary>
    ''' The RPT sector weights
    ''' </summary>
    rptSectorWeights
    ''' <summary>
    ''' The RPT share holder statement
    ''' </summary>
    rptShareHolderStatement
    ''' <summary>
    ''' The RPT table audit report
    ''' </summary>
    rptTableAuditReport
    ''' <summary>
    ''' The RPT trade status
    ''' </summary>
    rptTradeStatus
    ''' <summary>
    ''' The RPT transaction by date
    ''' </summary>
    rptTransactionByDate
    ''' <summary>
    ''' The RPT transaction by entry date
    ''' </summary>
    rptTransactionByEntryDate
    ''' <summary>
    ''' The RPT transaction by group
    ''' </summary>
    rptTransactionByGroup
    ''' <summary>
    ''' The RPT transaction by stock
    ''' </summary>
    rptTransactionByStock
    ''' <summary>
    ''' The RPT transaction price difference
    ''' </summary>
    rptTransactionPriceDifference
    ''' <summary>
    ''' The RPT transaction ticket
    ''' </summary>
    rptTransactionTicket
    ''' <summary>
    ''' The RPT transaction trade blotter
    ''' </summary>
    rptTransactionTradeBlotter
    ''' <summary>
    ''' The RPT transaction trade revision blotter
    ''' </summary>
    rptTransactionTradeRevisionBlotter
    ''' <summary>
    ''' The RPT transaction trade UCITSFAX
    ''' </summary>
    rptTransactionTradeUCITSFAX
    ''' <summary>
    ''' The RPT unit holders combined profit
    ''' </summary>
    rptUnitHoldersCombinedProfit
    ''' <summary>
    ''' The RPT unit holders profit
    ''' </summary>
    rptUnitHoldersProfit
    ''' <summary>
    ''' The RPT unit holders register
    ''' </summary>
    rptUnitHoldersRegister
    ''' <summary>
    ''' The RPT unit holder statement
    ''' </summary>
    rptUnitHolderStatement
    ''' <summary>
    ''' The RPT unit price
    ''' </summary>
    rptUnitPrice
    ''' <summary>
    ''' The RPT unit price_ detail
    ''' </summary>
    rptUnitPrice_Detail
    ''' <summary>
    ''' The RPT user permissions
    ''' </summary>
    rptUserPermissions ' To Do
    ''' <summary>
    ''' The RPT view portfolio
    ''' </summary>
    rptViewPortfolio
    ''' <summary>
    ''' The RPT view portfolio change
    ''' </summary>
    rptViewPortfolioChange
    ''' <summary>
    ''' The RPT FX trade blotter
    ''' </summary>
    rptFXTradeBlotter

    ' For permissioning Venice One.

    ''' <summary>
    ''' The RPT best price report
    ''' </summary>
    rptBestPriceReport
    ''' <summary>
    ''' The RPT check transaction price
    ''' </summary>
    rptCheckTransactionPrice
    ''' <summary>
    ''' The RPT counterparty audit
    ''' </summary>
    rptCounterpartyAudit
    ''' <summary>
    ''' The RPT fund valuation_ portfolio
    ''' </summary>
    rptFundValuation_Portfolio
    ''' <summary>
    ''' The RPT FX exposure
    ''' </summary>
    rptFXExposure
    ''' <summary>
    ''' The RPT FX rates
    ''' </summary>
    rptFXRates
    ''' <summary>
    ''' The RPT LOG activity report
    ''' </summary>
    rptLOGActivityReport
    ''' <summary>
    ''' The RPT LOG error report
    ''' </summary>
    rptLOGErrorReport
    ''' <summary>
    ''' The RPT LOG warning report
    ''' </summary>
    rptLOGWarningReport
    ''' <summary>
    ''' The RPT transaction audit
    ''' </summary>
    rptTransactionAudit
    ''' <summary>
    ''' The RPT transaction FX tickets
    ''' </summary>
    rptTransactionFXTickets
    ''' <summary>
    ''' The RPT unit holders
    ''' </summary>
    rptUnitHolders
    ''' <summary>
    ''' The RPT unit holder fees
    ''' </summary>
    rptUnitHolderFees
    ''' <summary>
    ''' The RPT unit holder fees aggregate
    ''' </summary>
    rptUnitHolderFeesAggregate


    ' End Stop

    ''' <summary>
    ''' The max value
    ''' </summary>
    MaxValue
  End Enum

  ''' <summary>
  ''' Class FXForwardPosition
  ''' </summary>
  Public Class FXForwardPosition
    ''' <summary>
    ''' The transaction parent ID
    ''' </summary>
    Public TransactionParentID As Integer
    ''' <summary>
    ''' The ISIN
    ''' </summary>
    Public ISIN As String
    ''' <summary>
    ''' The value date
    ''' </summary>
    Public ValueDate As Date
    ''' <summary>
    ''' The settlement date
    ''' </summary>
    Public SettlementDate As Date
    ''' <summary>
    ''' The currency leg1
    ''' </summary>
    Public CurrencyLeg1 As Integer
    ''' <summary>
    ''' The currency leg2
    ''' </summary>
    Public CurrencyLeg2 As Integer
    ''' <summary>
    ''' The currency code leg1
    ''' </summary>
    Public CurrencyCodeLeg1 As String
    ''' <summary>
    ''' The currency code leg2
    ''' </summary>
    Public CurrencyCodeLeg2 As String
    ''' <summary>
    ''' The instrument leg1
    ''' </summary>
    Public InstrumentLeg1 As Integer
    ''' <summary>
    ''' The instrument leg2
    ''' </summary>
    Public InstrumentLeg2 As Integer
    ''' <summary>
    ''' The units leg1
    ''' </summary>
    Public UnitsLeg1 As Double
    ''' <summary>
    ''' The units leg2
    ''' </summary>
    Public UnitsLeg2 As Double
    ''' <summary>
    ''' The value leg1
    ''' </summary>
    Public ValueLeg1 As Double
    ''' <summary>
    ''' The value leg2
    ''' </summary>
    Public ValueLeg2 As Double
    ''' <summary>
    ''' The FX rate
    ''' </summary>
    Public FXRate As Double
    ''' <summary>
    ''' The admin row leg1
    ''' </summary>
    Public AdminRowLeg1 As DataRow
    ''' <summary>
    ''' The admin row leg2
    ''' </summary>
    Public AdminRowLeg2 As DataRow

    ''' <summary>
    ''' Initializes a new instance of the <see cref="FXForwardPosition"/> class.
    ''' </summary>
    Public Sub New()
      TransactionParentID = 0
      ISIN = ":-)" ' Unlikely to occur naturally!
      InstrumentLeg1 = 0
      InstrumentLeg2 = 0
      CurrencyLeg1 = 0
      CurrencyLeg2 = 0
      CurrencyCodeLeg1 = ""
      CurrencyCodeLeg2 = ""
      InstrumentLeg1 = 0
      InstrumentLeg2 = 0
      UnitsLeg1 = 0.0#
      UnitsLeg2 = 0.0#
      ValueLeg1 = 0.0#
      ValueLeg2 = 0.0#
      FXRate = 1.0#
      AdminRowLeg1 = Nothing
      AdminRowLeg2 = Nothing
    End Sub
  End Class


End Module

