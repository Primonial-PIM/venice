' No longer Used.
' Converted to use RenaissanceStatFunctions.
' NPP 30 Jun 2008.
'
'Option Explicit On
'Option Strict On

'Imports System.IO
'Imports System.Data.SqlClient
'Imports System.Math
'Imports RenaissanceDataClass

'Public Class PertracFunctions
'	Private WithEvents _MainForm As VeniceMain

'	Private _ReloadPerformanceTables As Boolean

'	Private PertracReturnSelectedRows() As RenaissanceDataClass.DSInstrumentPerformance.tblInstrumentPerformanceRow

'#Region " Properties"

'	Private ReadOnly Property MainForm() As VeniceMain
'		Get
'			Return _MainForm
'		End Get
'	End Property

'	Private Property ReloadPerformanceTables() As Boolean
'		Get
'			Return _ReloadPerformanceTables
'		End Get
'		Set(ByVal Value As Boolean)
'			_ReloadPerformanceTables = Value
'		End Set
'	End Property

'#End Region

'#Region " Constructors"


'	Private Sub New()

'	End Sub

'	Public Sub New(ByRef pMainform As VeniceMain)
'		_MainForm = pMainform
'		ReloadPerformanceTables = True
'		PertracReturnSelectedRows = Nothing

'	End Sub

'#End Region

'#Region " AutoUpdate"

'	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs) Handles _MainForm.VeniceAutoUpdate
'		Dim KnowledgeDateChanged As Boolean

'		KnowledgeDateChanged = False

'		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
'			KnowledgeDateChanged = True
'		End If

'		' ****************************************************************
'		' Check for changes relevant to this form
'		' ****************************************************************

'		' Changes to the Performance table :-
'		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Performance) = True) Then
'			ReloadPerformanceTables = True
'			PertracReturnSelectedRows = Nothing
'		End If

'		' Changes to the tblInstrument table :-
'		' Instrument Pertrac Indices may have changed.
'		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrument) = True) Then
'			ReloadPerformanceTables = True
'			PertracReturnSelectedRows = Nothing
'		End If


'	End Sub

'#End Region

'#Region " Pertrac Return Functions"

'	Public Function GetPertracReturn(ByVal pPertracCode As Integer, ByVal pYear As Long, ByVal pMonth As Long) As Double

'		Dim dsPerformance As RenaissanceDataClass.DSInstrumentPerformance
'		Static LastPertracCode As Integer

'		Dim ThisRow As RenaissanceDataClass.DSInstrumentPerformance.tblInstrumentPerformanceRow

'		Try
'			If Me.ReloadPerformanceTables Then
'				dsPerformance = CType(MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.InstrumentPerformance, True), RenaissanceDataClass.DSInstrumentPerformance)
'				Me.ReloadPerformanceTables = False
'				LastPertracCode = (-1)
'			Else
'				dsPerformance = CType(MainForm.MainDataHandler.Get_Dataset(RenaissanceGlobals.RenaissanceStandardDatasets.InstrumentPerformance.DatasetName), RenaissanceDataClass.DSInstrumentPerformance)
'				If (dsPerformance Is Nothing) Then
'					dsPerformance = CType(MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.InstrumentPerformance, False), RenaissanceDataClass.DSInstrumentPerformance)
'					LastPertracCode = (-1)
'				End If
'			End If

'			If (pPertracCode <> LastPertracCode) OrElse (LastPertracCode < 0) OrElse (PertracReturnSelectedRows Is Nothing) Then
'				SyncLock dsPerformance
'					PertracReturnSelectedRows = CType(dsPerformance.tblInstrumentPerformance.Select("(ID=" & pPertracCode & ")", "PerformanceDate DESC"), RenaissanceDataClass.DSInstrumentPerformance.tblInstrumentPerformanceRow())
'				End SyncLock
'				LastPertracCode = pPertracCode
'			End If
'		Catch ex As Exception
'			Return 0
'		End Try

'		Try
'			For Each ThisRow In PertracReturnSelectedRows
'				If (ThisRow.PerformanceDate.Year = pYear) AndAlso (ThisRow.PerformanceDate.Month = pMonth) Then
'					Return ThisRow.PerformanceReturn
'				End If

'				' Since the dates are ordered Date descending, Exit if Year is less than the required year.
'				' Saves a little time !
'				If (ThisRow.PerformanceDate.Year < pYear) Then
'					Exit For
'				End If
'			Next
'		Catch ex As Exception
'		End Try

'		Return (0)
'	End Function

'	Public Function GetPertracYearReturn(ByVal pPertracCode As Integer, ByVal pYear As Long) As Double
'		Return GetPertracYearReturn(pPertracCode, pYear, 12)
'	End Function

'	Public Function GetPertracYearReturn(ByVal pPertracCode As Integer, ByVal pYear As Long, ByVal pEndMonth As Integer) As Double

'		Dim dsPerformance As RenaissanceDataClass.DSInstrumentPerformance
'		Static LastPertracCode As Integer

'		Dim ThisRow As RenaissanceDataClass.DSInstrumentPerformance.tblInstrumentPerformanceRow

'		If Me.ReloadPerformanceTables Then
'			Try
'				dsPerformance = CType(MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.InstrumentPerformance, True), RenaissanceDataClass.DSInstrumentPerformance)
'				Me.ReloadPerformanceTables = False
'				LastPertracCode = (-1)
'			Catch ex As Exception
'				Return 0
'			End Try
'		Else
'			Try
'				dsPerformance = CType(MainForm.MainDataHandler.Get_Dataset(RenaissanceGlobals.RenaissanceStandardDatasets.InstrumentPerformance.DatasetName), RenaissanceDataClass.DSInstrumentPerformance)
'				If (dsPerformance Is Nothing) Then
'					dsPerformance = CType(MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.InstrumentPerformance, False), RenaissanceDataClass.DSInstrumentPerformance)
'					LastPertracCode = (-1)
'				End If
'			Catch ex As Exception
'				Return 0
'			End Try
'		End If

'		Try
'			If (pPertracCode <> LastPertracCode) OrElse (LastPertracCode < 0) OrElse (PertracReturnSelectedRows Is Nothing) Then
'				SyncLock dsPerformance
'					PertracReturnSelectedRows = CType(dsPerformance.tblInstrumentPerformance.Select("(ID=" & pPertracCode & ")", "PerformanceDate DESC"), RenaissanceDataClass.DSInstrumentPerformance.tblInstrumentPerformanceRow())
'				End SyncLock
'				LastPertracCode = pPertracCode
'			End If
'		Catch ex As Exception
'			Return 0
'		End Try

'		Dim LastMonth As Integer
'		Dim CumulativeReturn As Double

'		CumulativeReturn = 1
'		LastMonth = (-1)

'		Try
'			For Each ThisRow In PertracReturnSelectedRows
'				If (ThisRow.PerformanceDate.Year = pYear) AndAlso (ThisRow.PerformanceDate.Month <= pEndMonth) AndAlso (ThisRow.PerformanceDate.Month <> LastMonth) Then
'					LastMonth = ThisRow.PerformanceDate.Month
'					CumulativeReturn *= (1.0 + ThisRow.PerformanceReturn)
'				End If

'				' Since the dates are ordered Date descending, Exit if Year is less than the required year.
'				' Saves a little time !
'				If (ThisRow.PerformanceDate.Year < pYear) Then
'					Exit For
'				End If
'			Next
'		Catch ex As Exception
'		End Try

'		Return (CumulativeReturn - 1.0)

'	End Function

'#End Region

'#Region " DrawDown Function"

'	Public Function PertracDrawdown(ByVal pPertracCode As Integer, ByVal ValueDate As Date) As Double
'		Dim dsPerformance As RenaissanceDataClass.DSInstrumentPerformance
'		Static LastPertracCode As Integer

'		Dim ThisRow As RenaissanceDataClass.DSInstrumentPerformance.tblInstrumentPerformanceRow

'		If Me.ReloadPerformanceTables Then
'			Try
'				dsPerformance = CType(MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.InstrumentPerformance, True), RenaissanceDataClass.DSInstrumentPerformance)
'				Me.ReloadPerformanceTables = False
'				LastPertracCode = (-1)
'			Catch ex As Exception
'				Return 1
'			End Try
'		Else
'			Try
'				dsPerformance = CType(MainForm.MainDataHandler.Get_Dataset(RenaissanceGlobals.RenaissanceStandardDatasets.InstrumentPerformance.DatasetName), RenaissanceDataClass.DSInstrumentPerformance)
'				If (dsPerformance Is Nothing) Then
'					dsPerformance = CType(MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.InstrumentPerformance, False), RenaissanceDataClass.DSInstrumentPerformance)
'					LastPertracCode = (-1)
'				End If
'			Catch ex As Exception
'				Return 1
'			End Try
'		End If

'		Try
'			If (pPertracCode <> LastPertracCode) OrElse (LastPertracCode < 0) OrElse (PertracReturnSelectedRows Is Nothing) Then
'				SyncLock dsPerformance
'					PertracReturnSelectedRows = CType(dsPerformance.tblInstrumentPerformance.Select("(ID=" & pPertracCode & ")", "PerformanceDate DESC"), RenaissanceDataClass.DSInstrumentPerformance.tblInstrumentPerformanceRow())
'				End SyncLock
'				LastPertracCode = pPertracCode
'			End If
'		Catch ex As Exception
'			Return 0
'		End Try

'		Dim CumulativeLogReturn As Double
'		Dim MinimusCumulativeLogReturn As Double
'		Dim LastDate As Date

'		CumulativeLogReturn = 0
'		MinimusCumulativeLogReturn = 0
'		LastDate = #1/1/1900#

'		Try
'			For Each ThisRow In PertracReturnSelectedRows
'				If (ThisRow.PerformanceDate.CompareTo(ValueDate) <= 0) Then
'					If (ThisRow.PerformanceDate.Year <> LastDate.Year) Or (ThisRow.PerformanceDate.Month <> LastDate.Month) Then
'						CumulativeLogReturn += Math.Log(1 + ThisRow.PerformanceReturn)
'						If (CumulativeLogReturn < MinimusCumulativeLogReturn) Then
'							MinimusCumulativeLogReturn = CumulativeLogReturn
'						End If

'						LastDate = ThisRow.PerformanceDate
'					End If
'				End If
'			Next
'		Catch ex As Exception
'		End Try

'		Return Math.Exp(MinimusCumulativeLogReturn)
'	End Function
'#End Region







'End Class
