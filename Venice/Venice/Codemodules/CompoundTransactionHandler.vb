' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 01-17-2013
' ***********************************************************************
' <copyright file="CompoundTransactionHandler.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Option Explicit On 
Option Strict On

Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals
Imports RenaissanceDataClass

''' <summary>
''' Class CompoundTransactionHandler
''' </summary>
Public Class CompoundTransactionHandler

    ''' <summary>
    ''' Class CT_Field
    ''' </summary>
	Private Class CT_Field
		' ******************************************************************************
		' Field Value Class.
		'
		' This Class is designed to hold values from the Compound Transaction Template 
		' and CT Parameters Tables.
		' The Constructors are designed to enforce the given DataType in the Given DataValue.
		'
		' ******************************************************************************

        ''' <summary>
        ''' The _ field name
        ''' </summary>
		Private _FieldName As String
        ''' <summary>
        ''' The _ field value
        ''' </summary>
		Private _FieldValue As Object
        ''' <summary>
        ''' The _ field type
        ''' </summary>
		Private _FieldType As SqlDbType

        ''' <summary>
        ''' Gets the name of the field.
        ''' </summary>
        ''' <value>The name of the field.</value>
		Public ReadOnly Property FieldName() As String
			Get
				Return _FieldName
			End Get
		End Property

        ''' <summary>
        ''' Gets the field value.
        ''' </summary>
        ''' <value>The field value.</value>
		Public ReadOnly Property FieldValue() As Object
			Get
				Return _FieldValue
			End Get
		End Property

        ''' <summary>
        ''' Gets the type of the field.
        ''' </summary>
        ''' <value>The type of the field.</value>
		Public ReadOnly Property FieldType() As Integer
			Get
				Return _FieldType
			End Get
		End Property

        ''' <summary>
        ''' Initializes a new instance of the <see cref="CT_Field"/> class.
        ''' </summary>
        ''' <param name="pFieldName">Name of the p field.</param>
        ''' <param name="pFieldValue">The p field value.</param>
        ''' <param name="pFieldType">Type of the p field.</param>
		Public Sub New(ByVal pFieldName As String, ByVal pFieldValue As Object, ByVal pFieldType As Integer)
			Me._FieldName = pFieldName

			Select Case CType(pFieldType, SqlDbType)
				Case SqlDbType.BigInt, SqlDbType.Int, SqlDbType.SmallInt, SqlDbType.TinyInt

					Me._FieldType = SqlDbType.BigInt
					Try
						If IsNumeric(pFieldValue) Then
							Me._FieldValue = CLng(pFieldValue)
						Else
							Me._FieldValue = 0
						End If
					Catch ex As Exception
						Me._FieldValue = 0
					End Try

				Case SqlDbType.Bit

					Me._FieldType = SqlDbType.Bit

					Try
						Me._FieldValue = CBool(pFieldValue)
					Catch ex As Exception
						Me._FieldValue = False
					End Try

				Case SqlDbType.Decimal, SqlDbType.Float, SqlDbType.Money, SqlDbType.Real, SqlDbType.SmallMoney

					Me._FieldType = SqlDbType.Decimal

					Try
						If IsNumeric(pFieldValue) Then
							Me._FieldValue = CDec(pFieldValue)
						ElseIf InStr(CStr(pFieldValue), "%") > 1 Then
							If IsNumeric(CStr(pFieldValue).Substring(0, InStr(CStr(pFieldValue), "%") - 1)) Then
								Me._FieldValue = CDec(CStr(pFieldValue).Substring(0, InStr(CStr(pFieldValue), "%") - 1)) / 100.0#
							Else
								Me._FieldValue = 0
							End If
						Else
							Me._FieldValue = 0
						End If
					Catch ex As Exception
						Me._FieldValue = 0
					End Try

				Case SqlDbType.DateTime, SqlDbType.SmallDateTime

					Me._FieldType = SqlDbType.DateTime

					Try
						If IsDate(pFieldValue) Then
							Me._FieldValue = CDate(pFieldValue)
						Else
							Me._FieldValue = Renaissance_BaseDate
						End If
					Catch ex As Exception
						Me._FieldValue = Renaissance_BaseDate
					End Try

				Case Else

					Me._FieldType = SqlDbType.VarChar
					Try
						Me._FieldValue = CStr(pFieldValue)
					Catch ex As Exception
						Me._FieldValue = ""
					End Try

			End Select

		End Sub

        ''' <summary>
        ''' Initializes a new instance of the <see cref="CT_Field"/> class.
        ''' </summary>
        ''' <param name="pFieldName">Name of the p field.</param>
        ''' <param name="pFieldValue">The p field value.</param>
        ''' <param name="pFieldType">Type of the p field.</param>
		Public Sub New(ByVal pFieldName As String, ByVal pFieldValue As Object, ByVal pFieldType As System.Type)
			Me._FieldName = pFieldName

			Select Case pFieldType.Name
				Case GetType(System.Int16).Name, _
				 GetType(System.Int32).Name, _
				 GetType(System.Int64).Name, _
				 GetType(System.Byte).Name, _
				 GetType(System.Char).Name

					Me._FieldType = SqlDbType.BigInt
					Try
						If IsNumeric(pFieldValue) Then
							Me._FieldValue = CLng(pFieldValue)
						Else
							Me._FieldValue = 0
						End If
					Catch ex As Exception
						Me._FieldValue = 0
					End Try

				Case GetType(System.Boolean).Name

					Me._FieldType = SqlDbType.Bit

					Try
						Me._FieldValue = CBool(pFieldValue)
					Catch ex As Exception
						Me._FieldValue = False
					End Try

				Case GetType(System.DateTime).Name

					Me._FieldType = SqlDbType.DateTime

					Try
						If IsDate(pFieldValue) Then
							Me._FieldValue = CDate(pFieldValue)
						Else
							Me._FieldValue = Renaissance_BaseDate
						End If
					Catch ex As Exception
						Me._FieldValue = Renaissance_BaseDate
					End Try

				Case GetType(System.Decimal).Name, _
				 GetType(System.Double).Name, _
				 GetType(System.Single).Name

					Me._FieldType = SqlDbType.Decimal

					Try
						If IsNumeric(pFieldValue) Then
							Me._FieldValue = CDec(pFieldValue)
						ElseIf InStr(CStr(pFieldValue), "%") > 1 Then
							If IsNumeric(CStr(pFieldValue).Substring(0, InStr(CStr(pFieldValue), "%") - 1)) Then
								Me._FieldValue = CDec(CStr(pFieldValue).Substring(0, InStr(CStr(pFieldValue), "%") - 1)) / CDec(100)
							Else
								Me._FieldValue = 0
							End If
						Else
							Me._FieldValue = 0
						End If
					Catch ex As Exception
						Me._FieldValue = 0
					End Try

				Case GetType(System.String).Name

					Me._FieldType = SqlDbType.VarChar
					Try
						Me._FieldValue = CStr(pFieldValue)
					Catch ex As Exception
						Me._FieldValue = ""
					End Try

				Case GetType(DBNull).Name

					Me._FieldValue = ""

				Case Else

					Me._FieldType = SqlDbType.VarChar
					Try
						Me._FieldValue = CStr(pFieldValue)
					Catch ex As Exception
						Me._FieldValue = ""
					End Try

			End Select

		End Sub

	End Class

    ''' <summary>
    ''' The C t_ terms_ array
    ''' </summary>
	Private CT_Terms_Array() As CT_Field
    ''' <summary>
    ''' The C t_ parameters
    ''' </summary>
	Private CT_Parameters() As CT_Field

    ''' <summary>
    ''' The C t_ template_ data set
    ''' </summary>
	Dim CT_Template_DataSet As DSCompoundTransactionTemplate
    ''' <summary>
    ''' The C t_ template_ table
    ''' </summary>
	Dim CT_Template_Table As DSCompoundTransactionTemplate.tblCompoundTransactionTemplateDataTable

	' CT Resolved Transactions :-
    ''' <summary>
    ''' The C t_ transactions_ array
    ''' </summary>
	Private CT_Transactions_Array(,) As Object
    ''' <summary>
    ''' The pending_ transactions_ array
    ''' </summary>
	Private Pending_Transactions_Array() As RenaissanceDataClass.DSTransaction.tblTransactionRow
    ''' <summary>
    ''' The C t_ transactions mask_ array
    ''' </summary>
	Private CT_TransactionsMask_Array(,) As Boolean

	' In Concept :
	'
	' The CompoundTransaction (CT) table provides all the information required to post the CT series,
	' This need not include calculated fields or field values which can be derived from information
	' on the CT table.
	'
	' The CompountTransactionTemplate table includes a selection of fields used to post a transaction,
	' Between the CT and CTTemplate tables all the fields necessary to post a transaction must be
	' presented or calculable.
	' The fields in the CTTemplate table are populated on an as-needed basis, by default when a compound
	' transaction is processed field values are copied down from the CT record to the Template Records on
	' a matching fieldname basis unless the template field already contains something.
	' The Template fields which were not copied from the CT record are then processed to resolve any
	' formulas, functions or references to other fields.
	' All the fields on the Template records are processed so that fields which do not directly derive from
	' the CT record can be used and additional or intermediate calculations can be used on each template
	' record. There are two fields (R1 & R2) specifically intended for the calculation of intermediate
	' values.
	'
	' On the template records formulas should begin with the equals (=) character, dates should be
	' delimited with the hash (#) character and strings delimited with either the single or double quote
	' character (", '), a string opened with a given quote character must be closed with the same quote character.
	' references to fields from the CT record are made by using the CT record field name enclosed in curley
	' brackets {}, references to fields on the same or other template rows are in the format :
	' {Row.FieldName}, again in curley brackets. The Row specifies the template row, the field specifies the
	' template field name. Templates must be created with sequentialy numbered rows starting with row one.
	' CT and Template field references may also be delimited with square brackets [] unless used within a SQL
	' SELECT statement, square brackets already forming part of the SQL language.
	'
	' 'Parameter' values may be specified on the CT record or within the CTParameters table.
	' Any unresolved token in a formula wether surrounded by brackets or not will be checked against the
	' available parameters. After this the user will be prompted for any unresolved tokens.
	' Parameter names must not contain the full stop (. period) character, otherwise they will be treated as
	' references to a field on the Transaction template, and probably fail to resolve and generate an error.
	'
	' SQL SELECT statements may form part or all of a function, if forming only part of a function, the SQL
	' statement should be enclosed in round brackets so that it can easily be isolated from the rest of the
	' function. Only the first returned value is used.
	'
	' Arithmetical and comparative operators (+,-,/,*,^,%,&,=,<,>,<=,>=,<>,!=), the use use of round brackets
	' and operator precedence follow normal conventions (General, Microsoft and specifically Access).
	'
	' The use of a limited selection of functions is supported :
	'
	'   NOW()                               : Returns double representing current time, usual Access / MS form.
	'   INT()                               : Returns Integer.
	'   CDBL(), CINT(), CSTR(), CDATE()     : Usual style Conversion functions.
	'   IIF(), IF()                         : IF(Condition, True_Result, False_Result).
	'                                       : Returns one of two values based on the True / False evaluation of the
	'                                       : first parameter.
	'   MAX(), MIN()                        : Return highest / lowest of the parameter list. No limit on the
	'                                       : number of parameters supplied. May not match string with non-string values.
	'   ROUND(Value, Precision)             : Round a given value to a given decimal precision.
	'   LOG(), LN()                         : Log base 10 and natural Log functions.
	'   EXP()                               : Converse of natural log.
	'   SQLSELECT()                         : Used to construct SQL Select statements and to return a value. The parameter
	'                                       : must begin with "SELECT " and as with all SQL SELECT terms, only the first
	'                                       : returned value will be used.
	'   FORMAT(Value, Format String)        : Usual style Format function.
	'   GETTICKET(Class, [Increment])       : Return the next-in-sequence Transaction Ticket for the given Transaction 'Class'.
	'                                       : Common 'Classes' are  'AD' for Administrative (Mgmt & Perf Fees)
	'                                       :                       'SR' for SUBSCRIBE and REDEEM trades, unless they are part of an INternal cross
	'                                       :                       'FX' for BUY FX and SELL FX trades
	'                                       :                       'BS' for BUY and SELL Transactions
	'                                       :                       'IN' for INternal trades, e.g. Select fund buying units from Balanced fund.
	'                                       :                       'OT' All ther trade types.
	'                                       : [Increment] represents an optional parameter. An Integer value to be
	'                                       : added to the Ticket Count. Allows a series of Ticket IDs to be generated.

    ''' <summary>
    ''' Processes the compound transaction.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  ''' <param name="Permission_Area">The permission_ area.</param>
  ''' <param name="This_CT">The this_ CT.</param>
  ''' <param name="pPreviousCTRN">The p previous CTRN.</param>
  ''' <param name="p_ValidateOnly">if set to <c>true</c> [p_ validate only].</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function ProcessCompoundTransaction(ByRef pMainForm As VeniceMain, ByVal Permission_Area As String, ByRef This_CT As DSCompoundTransaction.tblCompoundTransactionRow, ByVal pPreviousCTRN As Integer, Optional ByVal p_ValidateOnly As Boolean = False) As Boolean

    '**************************************************************************************************
    ' Public function returning Boolean
    '
    ' Purpose:  Given a CompoundTransaction record, process and post the consequent transaction chain.
    '
    ' Accepts:  Permission_Area as String            : Location of Error (for debugging and logging purposes)
    '           CT_RS As dao.Recordset               : Recordset positioned at an appropriate record oth the
    '                                                : compoundtransactions table.
    '
    ' Returns:  True / False for Successfull operation or failure with Error.
    '**************************************************************************************************

    Dim Template_RS() As DSCompoundTransactionTemplate.tblCompoundTransactionTemplateRow

    Dim ValidateOnlyFlag As Boolean

    Dim Counter As Integer
    Dim Template_Line As Integer
    Dim Template_Field As Integer
    Dim Field_Value As Object
    Dim String_Value As String

    Dim ErrorLevel As LOG_LEVELS = LOG_LEVELS.Warning
    Dim ErrorMessage As String = ""
    Dim SystemErrorMessage As String = ""
    Dim ErrorStack As String = ""
    Dim ErrorShow As Boolean = True

    ' Sequence of events :
    '
    ' 1) Retrieve the contents of the Compound Transaction record to the CT_Terms_Array
    ' 2) Create the Transaction Parameters array and retrieve additional parameters from the
    '    CompoundTransactionParameters table if necessary.
    ' 3) Retrieve the correct CT Template.
    ' 4) Initialise CT_Transactions_Array
    ' 5) Copy Values from the Template array to the Transactions Array, substituting blank values
    '    with values from the CT record if a matching field exists.
    ' 6) Resolve References, Formulas and Functions
    ' 7) Post process values in the transactions array.
    ' 8) Post Transactions
    ' 9) Mark Compound Transaction Record as posted.
    '

    If This_CT.CompoundTransactionPosted <> 0 Then
      ErrorMessage = "Compound Transaction " & This_CT.TransactionCompoundID & " has already been posted."
      GoTo ProcessCompoundTransaction_ExitMessage
    End If

    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    '  Validate Parameters
    '
    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    If (pMainForm Is Nothing) Then
      ErrorMessage = "pMainForm not Set (Is Nothing)"
      ErrorShow = False
      GoTo ProcessCompoundTransaction_ExitMessage
    End If

    If (This_CT Is Nothing) Then
      ErrorMessage = "CT_RS - CompoundTransaction Row is nothing."
      ErrorShow = False
      GoTo ProcessCompoundTransaction_ExitMessage
    End If

    ValidateOnlyFlag = p_ValidateOnly

    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    ' 1) Populate CT_Terms_Array with Compound Transaction Terms. :-
    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    ReDim CT_Terms_Array(This_CT.Table.Columns.Count - 1)
    For Counter = 0 To (This_CT.Table.Columns.Count - 1)
      CT_Terms_Array(Counter) = New CT_Field(This_CT.Table.Columns(Counter).ColumnName, This_CT.Item(Counter), This_CT.Item(Counter).GetType)
    Next

    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    ' 2) Establish the Parameters Array
    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    Try
      If This_CT.ParameterCount > 0 Then
        ReDim CT_Parameters(This_CT.ParameterCount)
      Else
        ReDim CT_Parameters(0)
        CT_Parameters(0) = New CT_Field("", 0, 0)
      End If

    Catch ex As Exception
      ErrorLevel = LOG_LEVELS.Error
      SystemErrorMessage = ex.Message
      ErrorStack = ex.StackTrace
      ErrorMessage = ""
      GoTo ProcessCompoundTransaction_ExitMessage
    End Try

    If (This_CT.ParameterCount >= 1) Then
      CT_Parameters(0) = New CT_Field(This_CT.Parameter1_Name, This_CT.Parameter1_Value, This_CT.Parameter1_Type)
    End If
    If (This_CT.ParameterCount >= 2) Then
      CT_Parameters(1) = New CT_Field(This_CT.Parameter2_Name, This_CT.Parameter2_Value, This_CT.Parameter2_Type)
    End If
    If (This_CT.ParameterCount >= 3) Then
      CT_Parameters(2) = New CT_Field(This_CT.Parameter3_Name, This_CT.Parameter3_Value, This_CT.Parameter3_Type)
    End If
    If (This_CT.ParameterCount >= 4) Then
      CT_Parameters(3) = New CT_Field(This_CT.Parameter4_Name, This_CT.Parameter4_Value, This_CT.Parameter4_Type)
    End If
    If (This_CT.ParameterCount >= 5) Then
      CT_Parameters(4) = New CT_Field(This_CT.Parameter5_Name, This_CT.Parameter5_Value, This_CT.Parameter5_Type)
    End If

    If This_CT.ParameterCount > 5 Then
      ' Retrieve additional Parameters from the Parameters table.

      ' MORE CODE HERE !!!

    End If

    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    ' 3) Retrieve Template
    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    Try
      CT_Template_DataSet = CType(pMainForm.Load_Table(RenaissanceStandardDatasets.tblCompoundTransactionTemplate, False), DSCompoundTransactionTemplate)
      CT_Template_Table = CT_Template_DataSet.tblCompoundTransactionTemplate
      Template_RS = CType(CT_Template_Table.Select("TemplateID=" & This_CT.TransactionTemplateID, "TemplateStep"), DSCompoundTransactionTemplate.tblCompoundTransactionTemplateRow())
    Catch ex As Exception
      ErrorLevel = LOG_LEVELS.Error
      SystemErrorMessage = ex.Message
      ErrorStack = ex.StackTrace
      ErrorMessage = "Error retrieving Compound Transaction Template."
      GoTo ProcessCompoundTransaction_ExitMessage
    End Try

    If (Template_RS.Length) <= 0 Then
      ErrorMessage = "There are no records in the Transaction Template : `" & This_CT.TransactionTemplateID & "`"
      GoTo ProcessCompoundTransaction_ExitMessage
    End If

    ReDim CT_Transactions_Array(Template_RS.Length - 1, (CT_Template_Table.Columns.Count - 1))
    ReDim CT_TransactionsMask_Array(Template_RS.Length - 1, (CT_Template_Table.Columns.Count - 1))

    ' Save Template Values to CT_Transactions_Array

    SyncLock CT_Template_DataSet
      For Template_Line = 0 To (Template_RS.Length - 1)

        ' Verify that the Template Steps start at one and are continuous.
        If Template_RS(Template_Line).TemplateStep <> (Template_Line + 1) Then
          ErrorMessage = "Some Transaction Steps seem to be missing in Transaction Template : `" & This_CT.TransactionTemplateID & "`"
          GoTo ProcessCompoundTransaction_ExitMessage
        End If

        ' Copy Template Data to the CT_Transactions_Array
        For Counter = 0 To (Template_RS(0).Table.Columns.Count - 1)
          If Template_RS(Template_Line).IsNull(Counter) Then
            CT_Transactions_Array(Template_Line, Counter) = Nothing
          Else
            CT_Transactions_Array(Template_Line, Counter) = Template_RS(Template_Line).Item(Counter)
          End If

          ' Set CT_TransactionsMask_Array to FALSE if the cell needs decoding, else set to TRUE

          If (CT_Transactions_Array(Template_Line, Counter) IsNot Nothing) AndAlso (CT_Transactions_Array(Template_Line, Counter).GetType Is GetType(String)) AndAlso CStr(CT_Transactions_Array(Template_Line, Counter)).Trim.StartsWith("=") Then
            CT_TransactionsMask_Array(Template_Line, Counter) = False
          Else
            CT_TransactionsMask_Array(Template_Line, Counter) = True
          End If
        Next Counter
      Next
    End SyncLock

    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    ' 5) Replace any blank fields in CT_Transactions_Array with
    '    Values from the CT Terms record if a matching field exists.
    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    Dim thisTermsField As Integer

    For Template_Field = 0 To (CT_Template_Table.Columns.Count - 1)
      thisTermsField = (-1)

      For Counter = 0 To UBound(CT_Terms_Array)
        If CT_Terms_Array(Counter).FieldName = CT_Template_Table.Columns(Template_Field).ColumnName Then
          thisTermsField = Counter
          Exit For
        End If
      Next

      If thisTermsField >= 0 Then
        For Template_Line = 0 To UBound(CT_Transactions_Array)
          Try
            If (CT_Transactions_Array(Template_Line, Template_Field) Is Nothing) OrElse (CStr(CT_Transactions_Array(Template_Line, Template_Field)).Trim.Length = 0) Then
              CT_Transactions_Array(Template_Line, Template_Field) = CT_Terms_Array(thisTermsField).FieldValue
              CT_TransactionsMask_Array(Template_Line, Template_Field) = True
            End If
          Catch ex As Exception
          End Try
        Next
      End If
    Next

    ' OK, Now The 'CT_Transactions_Array' has the Template values copied down to it, Any Null values
    ' Have Been replaced by values brought down from the Compound Transaction Line.
    ' All completed cells, i.e. those not requiring further processing (not starting '='), have been
    ' marked on the Mask as complete (True).


    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    ' 6) Resolve each Cell that requires processing.
    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    For Template_Line = 0 To UBound(CT_Transactions_Array)
      For Template_Field = 0 To (CT_Template_Table.Columns.Count - 1)
        If CT_TransactionsMask_Array(Template_Line, Template_Field) = False Then
          Call ResolveCell(pMainForm, Permission_Area, "|", Template_Line, Template_Field)
          CT_TransactionsMask_Array(Template_Line, Template_Field) = True
        End If
      Next Template_Field
    Next Template_Line

    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    ' 7) Post process values in the transactions array.
    '    remove Quotes from around strings, Convert dates delimited by '#' characters
    '    to normal Date values, Convert Numeric valued strings to numeric values.
    '    Exit if any cells contain an error value (delimited by curley brackets {})
    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    For Template_Line = 0 To UBound(CT_Transactions_Array)
      For Template_Field = 0 To (CT_Template_Table.Columns.Count - 1)
        Field_Value = CT_Transactions_Array(Template_Line, Template_Field)
        If (Field_Value.GetType Is GetType(String)) Then
          String_Value = CStr(Field_Value).Trim

          If ((String_Value.StartsWith("'")) And (String_Value.EndsWith("'"))) OrElse _
          ((String_Value.StartsWith("`")) And (String_Value.EndsWith("`"))) OrElse _
          ((String_Value.StartsWith(CStr(Chr(34)))) And (String_Value.EndsWith(CStr(Chr(34))))) Then

            If (String_Value.Length > 2) Then
              CT_Transactions_Array(Template_Line, Template_Field) = String_Value.Substring(1, String_Value.Length - 2)
            Else
              CT_Transactions_Array(Template_Line, Template_Field) = ""
            End If

          ElseIf ((String_Value.StartsWith("#")) And (String_Value.EndsWith("#"))) Then
            If (String_Value.Length > 2) AndAlso (IsDate(String_Value.Substring(1, String_Value.Length - 2))) Then

              CT_Transactions_Array(Template_Line, Template_Field) = CDate(String_Value.Substring(1, String_Value.Length - 2))

            End If

          ElseIf (IsDate(String_Value) = True) Then

            CT_Transactions_Array(Template_Line, Template_Field) = CDate(String_Value)

          ElseIf (IsNumeric(String_Value) = True) Then

            If InStr(String_Value, ".") > 0 Then
              CT_Transactions_Array(Template_Line, Template_Field) = CDec(String_Value)
            Else
              CT_Transactions_Array(Template_Line, Template_Field) = CLng(String_Value)
            End If
          ElseIf String_Value.StartsWith("{") Then
            ErrorMessage = "Error with Template " & This_CT.TransactionTemplateID & ": " & String_Value
            GoTo ProcessCompoundTransaction_ExitMessage
          End If

        End If

      Next Template_Field
    Next Template_Line


    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    ' Validate the transactions, post as Validate only.
    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    Dim TestTransactionsDS As New RenaissanceDataClass.DSTransaction
    Dim TestTransactionDataRow As RenaissanceDataClass.DSTransaction.tblTransactionRow
    Dim TestTransactionsAdaptor As SqlDataAdapter
    Dim TestTransactionsTable As RenaissanceDataClass.DSTransaction.tblTransactionDataTable
    TestTransactionsTable = TestTransactionsDS.tblTransaction

    TestTransactionsAdaptor = pMainForm.MainDataHandler.Add_Adaptor("TestTransaction", VENICE_CONNECTION, "tblTransaction")
    If (TestTransactionsAdaptor Is Nothing) Then
      ErrorMessage = "Error creating `TestTransaction` adaptor."
      GoTo ProcessCompoundTransaction_ExitMessage
    End If

    TestTransactionsAdaptor.InsertCommand.Parameters("@KnowledgeDate").Value = KNOWLEDGEDATE_NOW
    TestTransactionsAdaptor.InsertCommand.Parameters("@ValidationOnly").Value = 1

    Try

      For Template_Line = 0 To UBound(CT_Transactions_Array)
        TestTransactionDataRow = TestTransactionsDS.tblTransaction.NewtblTransactionRow

        TestTransactionDataRow.TransactionTicket = _
         CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionTicket")).ToString

        TestTransactionDataRow.TransactionFund = _
         CInt(CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionFund")))

        TestTransactionDataRow.TransactionSubFund = 0

        TestTransactionDataRow.TransactionInstrument = CInt(CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionInstrument")))
        TestTransactionDataRow.TransactionType = CInt(CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionType")))
        TestTransactionDataRow.TransactionValueorAmount = CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionValueorAmount")).ToString
        TestTransactionDataRow.TransactionUnits = CDbl(CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionUnits")))
        TestTransactionDataRow.TransactionPrice = CDbl(CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("Transactionprice")))
        TestTransactionDataRow.TransactionCosts = CDbl(CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionCosts")))
        TestTransactionDataRow.TransactionCostIsPercent = CBool(CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionCostIsPercent")))
        TestTransactionDataRow.TransactionCostPercent = CDbl(CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionCostPercent")))

        TestTransactionDataRow.TransactionFXRate = 1

        TestTransactionDataRow.TransactionCounterparty = CInt(CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionCounterparty")))

        TestTransactionDataRow.TransactionInvestment = CStr(CT_Terms_Array(Get_CT_TermsFieldColumn("TransactionInvestment")).FieldValue)
        TestTransactionDataRow.TransactionExecution = CStr(CT_Terms_Array(Get_CT_TermsFieldColumn("TransactionExecution")).FieldValue)
        TestTransactionDataRow.TransactionDataEntry = CStr(CT_Terms_Array(Get_CT_TermsFieldColumn("TransactionDataEntry")).FieldValue)

        TestTransactionDataRow.TransactionTradeStatusID = CInt(CT_Terms_Array(Get_CT_TermsFieldColumn("TransactionTradeStatusID")).FieldValue)

        Dim DateValue As Object

        DateValue = CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionDecisionDate"))
        If IsDate(DateValue) Then
          TestTransactionDataRow.TransactionDecisionDate = CDate(DateValue)
        ElseIf IsNumeric(DateValue) Then
          TestTransactionDataRow.TransactionDecisionDate = Date.FromOADate(CDbl(DateValue))
        Else
          ErrorMessage = "Error : DecisionDate invalid."
          GoTo ProcessCompoundTransaction_ExitMessage
        End If

        DateValue = CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionValueDate"))
        If IsDate(DateValue) Then
          TestTransactionDataRow.TransactionValueDate = CDate(DateValue)
          TestTransactionDataRow.TransactionFIFOValueDate = CDate(DateValue)
        ElseIf IsNumeric(DateValue) Then
          TestTransactionDataRow.TransactionValueDate = Date.FromOADate(CDbl(DateValue))
          TestTransactionDataRow.TransactionFIFOValueDate = Date.FromOADate(CDbl(DateValue))
        Else
          ErrorMessage = "Error : TransactionValueDate invalid."
          GoTo ProcessCompoundTransaction_ExitMessage
        End If

        DateValue = CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionSettlementDate"))
        If IsDate(DateValue) Then
          TestTransactionDataRow.TransactionSettlementDate = CDate(DateValue)
        ElseIf IsNumeric(DateValue) Then
          TestTransactionDataRow.TransactionSettlementDate = Date.FromOADate(CDbl(DateValue))
        Else
          ErrorMessage = "Error : TransactionSettlementDate invalid."
          GoTo ProcessCompoundTransaction_ExitMessage
        End If

        DateValue = CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionConfirmationDate"))
        If IsDate(DateValue) Then
          TestTransactionDataRow.TransactionConfirmationDate = CDate(DateValue)
        ElseIf IsNumeric(DateValue) Then
          TestTransactionDataRow.TransactionConfirmationDate = Date.FromOADate(CDbl(DateValue))
        Else
          ErrorMessage = "Error : TransactionConfirmationDate invalid."
          GoTo ProcessCompoundTransaction_ExitMessage
        End If

        ' Transfer related parameters :

        TestTransactionDataRow.TransactionIsTransfer = CBool(CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionIsTransfer")))

        If (TestTransactionDataRow.TransactionIsTransfer) Then
          If (TestTransactionDataRow.TransactionType = RenaissanceGlobals.TransactionTypes.Subscribe) Then
            ' Set all the specified transfer flags for a Subscription.

            TestTransactionDataRow.TransactionEffectivePrice = CDbl(CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionEffectivePrice")))

            DateValue = CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionEffectiveValueDate"))

            If IsDate(DateValue) Then
              TestTransactionDataRow.TransactionEffectiveValueDate = CDate(DateValue)
            ElseIf IsNumeric(DateValue) Then
              TestTransactionDataRow.TransactionEffectiveValueDate = Date.FromOADate(CDbl(DateValue))
            Else
              TestTransactionDataRow.TransactionEffectiveValueDate = TestTransactionDataRow.TransactionValueDate
            End If

            If (CBool(CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionSpecificInitialEqualisationFlag")))) Then
              TestTransactionDataRow.TransactionSpecificInitialEqualisationFlag = True
              TestTransactionDataRow.TransactionSpecificInitialEqualisationValue = CDbl(CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionSpecificInitialEqualisationValue")))
            Else
              TestTransactionDataRow.TransactionSpecificInitialEqualisationFlag = False
              TestTransactionDataRow.TransactionSpecificInitialEqualisationValue = 0
            End If

            TestTransactionDataRow.TransactionEffectiveWatermark = CDbl(CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionEffectiveWatermark")))

          ElseIf (TestTransactionDataRow.TransactionType = RenaissanceGlobals.TransactionTypes.Redeem) Then
            ' For Redemptions, only the 'IsTransfer' flag is needed, the other transfer valued do not apply.

            TestTransactionDataRow.TransactionEffectivePrice = TestTransactionDataRow.TransactionPrice
            TestTransactionDataRow.TransactionEffectiveValueDate = TestTransactionDataRow.TransactionValueDate
            TestTransactionDataRow.TransactionSpecificInitialEqualisationFlag = False
            TestTransactionDataRow.TransactionSpecificInitialEqualisationValue = 0
            TestTransactionDataRow.TransactionEffectiveWatermark = 0

          Else
            ' If the Transaction is neither Subscribe or Redeem, then none of the Transfer fields apply.

            TestTransactionDataRow.TransactionIsTransfer = False
            TestTransactionDataRow.TransactionEffectivePrice = TestTransactionDataRow.TransactionPrice
            TestTransactionDataRow.TransactionEffectiveValueDate = TestTransactionDataRow.TransactionValueDate
            TestTransactionDataRow.TransactionSpecificInitialEqualisationFlag = False
            TestTransactionDataRow.TransactionSpecificInitialEqualisationValue = 0
            TestTransactionDataRow.TransactionEffectiveWatermark = 0
          End If
        Else
          TestTransactionDataRow.TransactionIsTransfer = False
          TestTransactionDataRow.TransactionEffectivePrice = TestTransactionDataRow.TransactionPrice
          TestTransactionDataRow.TransactionEffectiveValueDate = TestTransactionDataRow.TransactionValueDate
          TestTransactionDataRow.TransactionSpecificInitialEqualisationFlag = False
          TestTransactionDataRow.TransactionSpecificInitialEqualisationValue = 0
          TestTransactionDataRow.TransactionEffectiveWatermark = 0
        End If

        ' Transaction 'ExemptFromUpdate' flag 

        If CBool(CT_Terms_Array(Get_CT_TermsFieldColumn("TransactionExemptFromUpdate")).FieldValue) Then
          TestTransactionDataRow.TransactionExemptFromUpdate = True
        Else
          TestTransactionDataRow.TransactionExemptFromUpdate = False
        End If

        ' Transaction progress flags.

        If CInt(CT_Terms_Array(Get_CT_TermsFieldColumn("TransactionFinalAmount")).FieldValue) = 0 Then
          TestTransactionDataRow.TransactionFinalAmount = 0
        Else
          TestTransactionDataRow.TransactionFinalAmount = (-1)
        End If

        If CInt(CT_Terms_Array(Get_CT_TermsFieldColumn("TransactionFinalPrice")).FieldValue) = 0 Then
          TestTransactionDataRow.TransactionFinalPrice = 0
        Else
          TestTransactionDataRow.TransactionFinalPrice = (-1)
        End If

        If CInt(CT_Terms_Array(Get_CT_TermsFieldColumn("TransactionFinalCosts")).FieldValue) = 0 Then
          TestTransactionDataRow.TransactionFinalCosts = 0
        Else
          TestTransactionDataRow.TransactionFinalCosts = (-1)
        End If

        If CInt(CT_Terms_Array(Get_CT_TermsFieldColumn("TransactionFinalSettlement")).FieldValue) = 0 Then
          TestTransactionDataRow.TransactionFinalSettlement = 0
        Else
          TestTransactionDataRow.TransactionFinalSettlement = (-1)
        End If

        If CInt(CT_Terms_Array(Get_CT_TermsFieldColumn("TransactionInstructionFlag")).FieldValue) = 0 Then
          TestTransactionDataRow.TransactionInstructionFlag = 0
        Else
          TestTransactionDataRow.TransactionInstructionFlag = (-1)
        End If

        If CInt(CT_Terms_Array(Get_CT_TermsFieldColumn("TransactionBankConfirmation")).FieldValue) = 0 Then
          TestTransactionDataRow.TransactionBankConfirmation = 0
        Else
          TestTransactionDataRow.TransactionBankConfirmation = (-1)
        End If

        If CInt(CT_Terms_Array(Get_CT_TermsFieldColumn("TransactionInitialDataEntry")).FieldValue) = 0 Then
          TestTransactionDataRow.TransactionInitialDataEntry = 0
        Else
          TestTransactionDataRow.TransactionInitialDataEntry = (-1)
        End If

        If CInt(CT_Terms_Array(Get_CT_TermsFieldColumn("TransactionAmountConfirmed")).FieldValue) = 0 Then
          TestTransactionDataRow.TransactionAmountConfirmed = 0
        Else
          TestTransactionDataRow.TransactionAmountConfirmed = (-1)
        End If

        If CInt(CT_Terms_Array(Get_CT_TermsFieldColumn("TransactionSettlementConfirmed")).FieldValue) = 0 Then
          TestTransactionDataRow.TransactionSettlementConfirmed = 0
        Else
          TestTransactionDataRow.TransactionSettlementConfirmed = (-1)
        End If

        If CInt(CT_Terms_Array(Get_CT_TermsFieldColumn("TransactionFinalDataEntry")).FieldValue) = 0 Then
          TestTransactionDataRow.TransactionFinalDataEntry = 0
        Else
          TestTransactionDataRow.TransactionFinalDataEntry = (-1)
        End If

        TestTransactionDataRow.TransactionComment = _
         CStr(CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionComment")))

        TestTransactionDataRow.TransactionParentID = 0

        If (CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionGroup")) Is Nothing) Then
          TestTransactionDataRow.TransactionGroup = ""
        Else
          TestTransactionDataRow.TransactionGroup = _
           CStr(CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionGroup")))
        End If

        TestTransactionDataRow.TransactionAdminStatus = _
         CInt(CT_Terms_Array(Get_CT_TermsFieldColumn("AdminStatus")).FieldValue)

        TestTransactionDataRow.TransactionCTFLAGS = _
         CInt(CT_Terms_Array(Get_CT_TermsFieldColumn("CompoundTransactionFlags")).FieldValue)

        TestTransactionDataRow.TransactionCompoundRN = This_CT.RN

        TestTransactionsTable.Rows.Add(TestTransactionDataRow)
        TestTransactionsAdaptor.InsertCommand.Parameters("@RETURN_VALUE").Value = 0

        Try
          pMainForm.AdaptorUpdate("ProcessCompoundTransactions", TestTransactionsAdaptor, (New DataRow() {TestTransactionDataRow}))
          ' TestTransactionsAdaptor.Update(New DataRow() {TestTransactionDataRow})
        Catch ex As Exception
        End Try

        If CInt(TestTransactionsAdaptor.InsertCommand.Parameters("@RETURN_VALUE").Value) = 0 Then
          ' An Error
          ErrorMessage = "Transaction Validation failed, " & CStr(TestTransactionsAdaptor.InsertCommand.Parameters("@rvStatusString").Value)
          GoTo ProcessCompoundTransaction_ExitMessage
        End If
      Next

    Catch ex As Exception
      ErrorMessage = "Transaction Validation failed, " & ex.Message
      GoTo ProcessCompoundTransaction_ExitMessage
    End Try

    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    ' retrieve existing Transactions (if they exist)
    ' in order to re-use the Tickets and ParentIDs
    ' In order to provide an accurate transaction audit for CTs
    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    Dim tblTransactionDataset As RenaissanceDataClass.DSTransaction    ' Form Specific !!!!
    Dim tblTransactionTable As RenaissanceDataClass.DSTransaction.tblTransactionDataTable
    Dim tblTransactionAdaptor As SqlDataAdapter
    Dim ExistingCTTransactions() As RenaissanceDataClass.DSTransaction.tblTransactionRow = Nothing

    Try
      tblTransactionAdaptor = pMainForm.MainDataHandler.Get_Adaptor(RenaissanceStandardDatasets.tblTransaction.Adaptorname, VENICE_CONNECTION, RenaissanceStandardDatasets.tblTransaction.TableName)
      tblTransactionDataset = CType(pMainForm.Load_Table(RenaissanceStandardDatasets.tblTransaction, False), RenaissanceDataClass.DSTransaction)
      If Not (tblTransactionDataset Is Nothing) Then
        tblTransactionTable = tblTransactionDataset.tblTransaction
      Else
        tblTransactionTable = Nothing
      End If
    Catch ex As Exception
      ErrorMessage = "Error loading Transactions, " & ex.Message
      GoTo ProcessCompoundTransaction_ExitMessage
    End Try

    If Not (tblTransactionTable Is Nothing) Then
      ExistingCTTransactions = CType(tblTransactionTable.Select("(TransactionCompoundRN > 0) AND (TransactionCompoundRN = " & pPreviousCTRN.ToString & ")", "RN"), RenaissanceDataClass.DSTransaction.tblTransactionRow())
    End If


    Dim DeleteCommand As New SqlCommand
    Try
      DeleteCommand.Connection = pMainForm.GetVeniceConnection
      DeleteCommand.CommandType = CommandType.Text
      DeleteCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      DeleteCommand.CommandText = "DELETE FROM tblPendingTransactions WHERE TransactionCompoundRN = " & This_CT.RN.ToString
      SyncLock DeleteCommand.Connection
        DeleteCommand.ExecuteNonQuery()
      End SyncLock
    Catch ex As Exception
      ErrorMessage = "Error deleting old Pending Transactions, " & ex.Message
      GoTo ProcessCompoundTransaction_ExitMessage
    Finally
      Try
        If (Not (DeleteCommand.Connection Is Nothing)) Then
          DeleteCommand.Connection.Close()
        End If
      Catch ex As Exception
      End Try
    End Try

    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    ' Post to the Pending Transactions Table.
    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    Dim PendingTransactionsDS As New RenaissanceDataClass.DSPendingTransactions
    Dim PendingTransactionDataRow As RenaissanceDataClass.DSPendingTransactions.tblPendingTransactionsRow
    Dim PendingTransactionsAdaptor As SqlDataAdapter
    Dim PendingTransactionsTable As RenaissanceDataClass.DSPendingTransactions.tblPendingTransactionsDataTable
    PendingTransactionsTable = PendingTransactionsDS.tblPendingTransactions

    PendingTransactionsAdaptor = pMainForm.MainDataHandler.Add_Adaptor("PendingTransactions", VENICE_CONNECTION, "tblPendingTransactions")
    If (PendingTransactionsAdaptor Is Nothing) Then
      ErrorMessage = "Error creating `PendingTransactionsAdaptor` adaptor."
      GoTo ProcessCompoundTransaction_ExitMessage
    End If

    PendingTransactionsAdaptor.InsertCommand.Parameters("@KnowledgeDate").Value = KNOWLEDGEDATE_NOW

    Try

      For Template_Line = 0 To UBound(CT_Transactions_Array)
        PendingTransactionDataRow = PendingTransactionsDS.tblPendingTransactions.NewtblPendingTransactionsRow

        If (Not (ExistingCTTransactions Is Nothing)) AndAlso (ExistingCTTransactions.Length > Template_Line) Then
          PendingTransactionDataRow.TransactionTicket = ExistingCTTransactions(Template_Line).TransactionTicket
        Else
          PendingTransactionDataRow.TransactionTicket = ""
        End If

        PendingTransactionDataRow.TransactionFund = _
         CInt(CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionFund")))

        PendingTransactionDataRow.TransactionSubFund = 0

        PendingTransactionDataRow.TransactionInstrument = CInt(CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionInstrument")))
        PendingTransactionDataRow.TransactionType = CInt(CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionType")))
        PendingTransactionDataRow.TransactionValueorAmount = CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionValueorAmount")).ToString
        PendingTransactionDataRow.TransactionUnits = CDbl(CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionUnits")))
        PendingTransactionDataRow.TransactionPrice = CDbl(CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("Transactionprice")))
        PendingTransactionDataRow.TransactionCosts = CDbl(CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionCosts")))

        PendingTransactionDataRow.TransactionCostIsPercent = CBool(CT_Terms_Array(Get_CT_TermsFieldColumn("TransactionCostIsPercent")).FieldValue)
        PendingTransactionDataRow.TransactionCostPercent = CDbl(CT_Terms_Array(Get_CT_TermsFieldColumn("TransactionCostPercent")).FieldValue)

        PendingTransactionDataRow.TransactionFXRate = 1

        PendingTransactionDataRow.TransactionCounterparty = CInt(CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionCounterparty")))
        PendingTransactionDataRow.TransactionInvestment = CStr(CT_Terms_Array(Get_CT_TermsFieldColumn("TransactionInvestment")).FieldValue)
        PendingTransactionDataRow.TransactionExecution = CStr(CT_Terms_Array(Get_CT_TermsFieldColumn("TransactionExecution")).FieldValue)
        PendingTransactionDataRow.TransactionDataEntry = CStr(CT_Terms_Array(Get_CT_TermsFieldColumn("TransactionDataEntry")).FieldValue)

        PendingTransactionDataRow.TransactionTradeStatusID = CInt(CT_Terms_Array(Get_CT_TermsFieldColumn("TransactionTradeStatusID")).FieldValue)

        Dim DateValue As Object

        DateValue = CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionDecisionDate"))
        If IsDate(DateValue) Then
          PendingTransactionDataRow.TransactionDecisionDate = CDate(DateValue)
        ElseIf IsNumeric(DateValue) Then
          PendingTransactionDataRow.TransactionDecisionDate = Date.FromOADate(CDbl(DateValue))
        Else
          ErrorMessage = "Error : DecisionDate invalid."
          GoTo ProcessCompoundTransaction_ExitMessage
        End If

        DateValue = CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionValueDate"))
        If IsDate(DateValue) Then
          PendingTransactionDataRow.TransactionValueDate = CDate(DateValue)
          PendingTransactionDataRow.TransactionFIFOValueDate = CDate(DateValue)
        ElseIf IsNumeric(DateValue) Then
          PendingTransactionDataRow.TransactionValueDate = Date.FromOADate(CDbl(DateValue))
          PendingTransactionDataRow.TransactionFIFOValueDate = Date.FromOADate(CDbl(DateValue))
        Else
          ErrorMessage = "Error : TransactionValueDate invalid."
          GoTo ProcessCompoundTransaction_ExitMessage
        End If

        DateValue = CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionSettlementDate"))
        If IsDate(DateValue) Then
          PendingTransactionDataRow.TransactionSettlementDate = CDate(DateValue)
        ElseIf IsNumeric(DateValue) Then
          PendingTransactionDataRow.TransactionSettlementDate = Date.FromOADate(CDbl(DateValue))
        Else
          ErrorMessage = "Error : TransactionSettlementDate invalid."
          GoTo ProcessCompoundTransaction_ExitMessage
        End If

        DateValue = CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionConfirmationDate"))
        If IsDate(DateValue) Then
          PendingTransactionDataRow.TransactionConfirmationDate = CDate(DateValue)
        ElseIf IsNumeric(DateValue) Then
          PendingTransactionDataRow.TransactionConfirmationDate = Date.FromOADate(CDbl(DateValue))
        Else
          ErrorMessage = "Error : TransactionConfirmationDate invalid."
          GoTo ProcessCompoundTransaction_ExitMessage
        End If

        ' Transfer related parameters :

        PendingTransactionDataRow.TransactionIsTransfer = CBool(CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionIsTransfer")))

        If (PendingTransactionDataRow.TransactionIsTransfer) Then
          If (PendingTransactionDataRow.TransactionType = RenaissanceGlobals.TransactionTypes.Subscribe) Then
            ' Set all the specified transfer flags for a Subscription.

            PendingTransactionDataRow.TransactionEffectivePrice = CDbl(CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionEffectivePrice")))

            DateValue = CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionEffectiveValueDate"))

            If IsDate(DateValue) Then
              PendingTransactionDataRow.TransactionEffectiveValueDate = CDate(DateValue)
            ElseIf IsNumeric(DateValue) Then
              PendingTransactionDataRow.TransactionEffectiveValueDate = Date.FromOADate(CDbl(DateValue))
            Else
              PendingTransactionDataRow.TransactionEffectiveValueDate = PendingTransactionDataRow.TransactionValueDate
            End If

            If (CBool(CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionSpecificInitialEqualisationFlag")))) Then
              PendingTransactionDataRow.TransactionSpecificInitialEqualisationFlag = True
              PendingTransactionDataRow.TransactionSpecificInitialEqualisationValue = CDbl(CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionSpecificInitialEqualisationValue")))
            Else
              PendingTransactionDataRow.TransactionSpecificInitialEqualisationFlag = False
              PendingTransactionDataRow.TransactionSpecificInitialEqualisationValue = 0
            End If

            PendingTransactionDataRow.TransactionEffectiveWatermark = CDbl(CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionEffectiveWatermark")))

          ElseIf (PendingTransactionDataRow.TransactionType = RenaissanceGlobals.TransactionTypes.Redeem) Then
            ' For Redemptions, only the 'IsTransfer' flag is needed, the other transfer valued do not apply.

            PendingTransactionDataRow.TransactionEffectivePrice = PendingTransactionDataRow.TransactionPrice
            PendingTransactionDataRow.TransactionEffectiveValueDate = PendingTransactionDataRow.TransactionValueDate
            PendingTransactionDataRow.TransactionSpecificInitialEqualisationFlag = False
            PendingTransactionDataRow.TransactionSpecificInitialEqualisationValue = 0
            PendingTransactionDataRow.TransactionEffectiveWatermark = 0

          Else
            ' If the Transaction is neither Subscribe or Redeem, then none of the Transfer fields apply.

            PendingTransactionDataRow.TransactionIsTransfer = False
            PendingTransactionDataRow.TransactionEffectivePrice = PendingTransactionDataRow.TransactionPrice
            PendingTransactionDataRow.TransactionEffectiveValueDate = PendingTransactionDataRow.TransactionValueDate
            PendingTransactionDataRow.TransactionSpecificInitialEqualisationFlag = False
            PendingTransactionDataRow.TransactionSpecificInitialEqualisationValue = 0
            PendingTransactionDataRow.TransactionEffectiveWatermark = 0
          End If

        Else

          PendingTransactionDataRow.TransactionIsTransfer = False
          PendingTransactionDataRow.TransactionEffectivePrice = PendingTransactionDataRow.TransactionPrice
          PendingTransactionDataRow.TransactionEffectiveValueDate = PendingTransactionDataRow.TransactionValueDate
          PendingTransactionDataRow.TransactionSpecificInitialEqualisationFlag = False
          PendingTransactionDataRow.TransactionSpecificInitialEqualisationValue = 0
          PendingTransactionDataRow.TransactionEffectiveWatermark = 0

        End If

        ' Transaction 'ExemptFromUpdate' flag 

        If CBool(CT_Terms_Array(Get_CT_TermsFieldColumn("TransactionExemptFromUpdate")).FieldValue) Then
          PendingTransactionDataRow.TransactionExemptFromUpdate = True
        Else
          PendingTransactionDataRow.TransactionExemptFromUpdate = False
        End If

        If CInt(CT_Terms_Array(Get_CT_TermsFieldColumn("TransactionFinalAmount")).FieldValue) = 0 Then
          PendingTransactionDataRow.TransactionFinalAmount = 0
        Else
          PendingTransactionDataRow.TransactionFinalAmount = (-1)
        End If

        If CInt(CT_Terms_Array(Get_CT_TermsFieldColumn("TransactionFinalPrice")).FieldValue) = 0 Then
          PendingTransactionDataRow.TransactionFinalPrice = 0
        Else
          PendingTransactionDataRow.TransactionFinalPrice = (-1)
        End If

        If CInt(CT_Terms_Array(Get_CT_TermsFieldColumn("TransactionFinalCosts")).FieldValue) = 0 Then
          PendingTransactionDataRow.TransactionFinalCosts = 0
        Else
          PendingTransactionDataRow.TransactionFinalCosts = (-1)
        End If

        If CInt(CT_Terms_Array(Get_CT_TermsFieldColumn("TransactionFinalSettlement")).FieldValue) = 0 Then
          PendingTransactionDataRow.TransactionFinalSettlement = 0
        Else
          PendingTransactionDataRow.TransactionFinalSettlement = (-1)
        End If

        If CInt(CT_Terms_Array(Get_CT_TermsFieldColumn("TransactionInstructionFlag")).FieldValue) = 0 Then
          PendingTransactionDataRow.TransactionInstructionFlag = 0
        Else
          PendingTransactionDataRow.TransactionInstructionFlag = (-1)
        End If

        If CInt(CT_Terms_Array(Get_CT_TermsFieldColumn("TransactionBankConfirmation")).FieldValue) = 0 Then
          PendingTransactionDataRow.TransactionBankConfirmation = 0
        Else
          PendingTransactionDataRow.TransactionBankConfirmation = (-1)
        End If

        If CInt(CT_Terms_Array(Get_CT_TermsFieldColumn("TransactionInitialDataEntry")).FieldValue) = 0 Then
          PendingTransactionDataRow.TransactionInitialDataEntry = 0
        Else
          PendingTransactionDataRow.TransactionInitialDataEntry = (-1)
        End If

        If CInt(CT_Terms_Array(Get_CT_TermsFieldColumn("TransactionAmountConfirmed")).FieldValue) = 0 Then
          PendingTransactionDataRow.TransactionAmountConfirmed = 0
        Else
          PendingTransactionDataRow.TransactionAmountConfirmed = (-1)
        End If

        If CInt(CT_Terms_Array(Get_CT_TermsFieldColumn("TransactionSettlementConfirmed")).FieldValue) = 0 Then
          PendingTransactionDataRow.TransactionSettlementConfirmed = 0
        Else
          PendingTransactionDataRow.TransactionSettlementConfirmed = (-1)
        End If

        If CInt(CT_Terms_Array(Get_CT_TermsFieldColumn("TransactionFinalDataEntry")).FieldValue) = 0 Then
          PendingTransactionDataRow.TransactionFinalDataEntry = 0
        Else
          PendingTransactionDataRow.TransactionFinalDataEntry = (-1)
        End If

        PendingTransactionDataRow.TransactionComment = _
         CStr(CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionComment")))

        If (Not (ExistingCTTransactions Is Nothing)) AndAlso (ExistingCTTransactions.Length > Template_Line) Then
          PendingTransactionDataRow.TransactionParentID = ExistingCTTransactions(Template_Line).TransactionParentID
        Else
          PendingTransactionDataRow.TransactionParentID = 0
        End If

        If (CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionGroup")) Is Nothing) Then
          PendingTransactionDataRow.TransactionGroup = ""
        Else
          PendingTransactionDataRow.TransactionGroup = _
           CStr(CT_Transactions_Array(Template_Line, Get_Template_FieldColumn("TransactionGroup")))
        End If

        PendingTransactionDataRow.TransactionAdminStatus = _
         CInt(CT_Terms_Array(Get_CT_TermsFieldColumn("AdminStatus")).FieldValue)

        PendingTransactionDataRow.TransactionCTFLAGS = _
         CInt(CT_Terms_Array(Get_CT_TermsFieldColumn("CompoundTransactionFlags")).FieldValue)

        PendingTransactionDataRow.TransactionCompoundRN = This_CT.RN
        PendingTransactionsTable.Rows.Add(PendingTransactionDataRow)

        pMainForm.AdaptorUpdate("ProcessCompoundTransaction", PendingTransactionsAdaptor, (New DataRow() {PendingTransactionDataRow}))
        ' PendingTransactionsAdaptor.Update(New DataRow() {PendingTransactionDataRow})
        If CInt(PendingTransactionsAdaptor.InsertCommand.Parameters("@RETURN_VALUE").Value) = 0 Then
          ' An Error
          ErrorMessage = "Pending Transaction Insert failed, " & CStr(PendingTransactionsAdaptor.InsertCommand.Parameters("@rvStatusString").Value)
          GoTo ProcessCompoundTransaction_ExitMessage
        End If
      Next

    Catch ex As Exception
      ErrorMessage = "Pending Transaction Insert failed, " & ex.Message
      GoTo ProcessCompoundTransaction_ExitMessage
    End Try

    ' Clear excess existing transactions.
    If ExistingCTTransactions.Length > UBound(CT_Transactions_Array) Then
      tblTransactionAdaptor.InsertCommand.Parameters("@KnowledgeDate").Value = KNOWLEDGEDATE_NOW
      tblTransactionAdaptor.UpdateCommand.Parameters("@KnowledgeDate").Value = KNOWLEDGEDATE_NOW

      For Template_Line = (UBound(CT_Transactions_Array) + 1) To (ExistingCTTransactions.Length - 1)
        ExistingCTTransactions(Template_Line).TransactionUnits = 0
        ExistingCTTransactions(Template_Line).TransactionPrice = 0
        ExistingCTTransactions(Template_Line).TransactionCosts = 0

        pMainForm.AdaptorUpdate("ProcessCompoundTransaction", tblTransactionAdaptor, (New RenaissanceDataClass.DSTransaction.tblTransactionRow() {ExistingCTTransactions(Template_Line)}))
        ' tblTransactionAdaptor.Update(New tblTransaction.tblTransactionRow() {ExistingCTTransactions(Template_Line)})
      Next
    End If


    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    ' Activate CT Row and 'Copy' Pending transactions to the live Transactions Table.
    ' Perform as an atomic operation.
    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    Dim CommitCommand As New SqlCommand
    Dim RVal As Int32
    Try
      CommitCommand.Parameters.Clear()

      CommitCommand.CommandType = CommandType.Text
      CommitCommand.CommandText = "EXEC CommitCompoundTransaction @TransactionCompoundRN = " & This_CT.RN.ToString
      CommitCommand.CommandTimeout = TRANSACTION_SQLCOMMAND_TIMEOUT

      CommitCommand.Connection = pMainForm.GetVeniceConnection
      CommitCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))

      If (CommitCommand.Connection Is Nothing) Then
        ErrorMessage = "Error getting connection."
        GoTo ProcessCompoundTransaction_ExitMessage
      End If

      SyncLock CommitCommand.Connection
        RVal = CInt(CommitCommand.ExecuteScalar())
      End SyncLock

    Catch ex As Exception
      ErrorMessage = "Error Commiting Pending Transactions, " & ex.Message
      GoTo ProcessCompoundTransaction_ExitMessage
    Finally
      Try
        If (Not (CommitCommand.Connection Is Nothing)) Then
          CommitCommand.Connection.Close()
        End If
      Catch ex As Exception
      End Try
    End Try

    If (RVal <= 0) Then
      ErrorMessage = "Error Commiting Pending Transactions, @RVal = " & CInt(CommitCommand.Parameters("@RETURN_VALUE").Value).ToString
      GoTo ProcessCompoundTransaction_ExitMessage
    End If



ProcessCompoundTransaction_ExitTrue:
    Return True
    Exit Function

ProcessCompoundTransaction_ExitMessage:
    Call pMainForm.LogError(Permission_Area & ", ProcessCompoundTransaction()", ErrorLevel, SystemErrorMessage, ErrorMessage, ErrorStack, ErrorShow)
    Return False
    Exit Function

  End Function

  ''' <summary>
  ''' Resolves the cell.
  ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  ''' <param name="Permission_Area">The permission_ area.</param>
  ''' <param name="p_Stack">The p_ stack.</param>
  ''' <param name="p_TransactionLine">The p_ transaction line.</param>
  ''' <param name="p_TransactionField">The p_ transaction field.</param>
  ''' <returns>System.Object.</returns>
  Private Function ResolveCell(ByRef pMainForm As VeniceMain, ByVal Permission_Area As String, ByVal p_Stack As String, ByVal p_TransactionLine As Integer, ByVal p_TransactionField As Integer) As Object
    Dim ResolvedValue As Object
    Dim ResolvedString As String
    Dim StackValue As String
    Dim TokenString As String

    If (p_TransactionLine < LBound(CT_Transactions_Array, 1)) Or (p_TransactionField < LBound(CT_Transactions_Array, 2)) Or _
     (p_TransactionLine > UBound(CT_Transactions_Array, 1)) Or (p_TransactionField > UBound(CT_Transactions_Array, 2)) Then
      Return "{Invalid Cell Reference}"
      Exit Function
    End If

    ' Return Value if this cell has already been resolved.
    ' If it is a string, ensure that it is enclosed in quotes.
    If CT_TransactionsMask_Array(p_TransactionLine, p_TransactionField) = True Then
      ResolvedValue = CT_Transactions_Array(p_TransactionLine, p_TransactionField)

      If (ResolvedValue.GetType Is GetType(String)) Then
        ResolvedString = CStr(ResolvedValue)

        If Not (ResolvedString.StartsWith("#") And ResolvedString.EndsWith("#")) Then
          If (IsNumeric(ResolvedString) = False) And (IsDate(ResolvedString) = False) And (ResolvedString <> "True") And (ResolvedString <> "False") Then
            ResolvedValue = Chr(34) & ResolvedString & Chr(34)
          End If
        End If
      End If

      Return ResolvedValue
      Exit Function
    End If

    ' Circular Reference ?
    If InStr(p_Stack, ("|" & p_TransactionLine & "_" & p_TransactionField & "|")) > 0 Then
      ' Circular Reference !!
      ResolvedValue = "{Circular " & p_TransactionLine & "." & CStr(CT_Transactions_Array(0, p_TransactionField)) & "}"
      GoTo ResolveCell_Exit
    End If

    StackValue = p_Stack & p_TransactionLine & "_" & p_TransactionField & "|"
    TokenString = CStr(CT_Transactions_Array(p_TransactionLine, p_TransactionField))

    If TokenString.Length = 0 Then
      ResolvedValue = ""
      GoTo ResolveCell_Exit
    End If

    ' Is if a formula
    If TokenString.Trim.StartsWith("=") Then
      ResolvedValue = ResolveToken(pMainForm, Permission_Area, StackValue, TokenString)

      If Left(CStr(ResolvedValue), 1) = "{" Then
        ResolvedValue = CStr(ResolvedValue).Substring(0, CStr(ResolvedValue).Length - 1) & _
        p_TransactionLine & "." & CStr(CT_Transactions_Array(0, p_TransactionField)) & _
        "}"
      End If
    Else
      ResolvedValue = CT_Transactions_Array(p_TransactionLine, p_TransactionField)
    End If

ResolveCell_Exit:

    CT_Transactions_Array(p_TransactionLine, p_TransactionField) = ResolvedValue
    CT_TransactionsMask_Array(p_TransactionLine, p_TransactionField) = True

    Return ResolvedValue
  End Function

  ''' <summary>
  ''' Resolves the token.
  ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  ''' <param name="Permission_Area">The permission_ area.</param>
  ''' <param name="p_Stack">The p_ stack.</param>
  ''' <param name="ValueString">The value string.</param>
  ''' <returns>System.Object.</returns>
  Private Function ResolveToken(ByRef pMainForm As VeniceMain, ByVal Permission_Area As String, ByVal p_Stack As String, ByVal ValueString As Object) As Object
    ' ********************************************************************************************
    ' Purpose:  Resolve a string down to a resultant value
    '
    ' Accepts:  <String>  - Permission_Area :
    '           <String>  - p_Stack         : String representation of the Template cell stack traversed to this point.
    '                                       : Used to detect circular references.
    '           <Variant> - ValueString     : The string to resolve.
    '
    ' Returns:  <Variant> - The resolved Value
    '
    ' The Resolve function works recursively by splitting up the recieved formula into it's component parts
    ' and calling 'ResolveToken' to resolve them down to single values.
    ' Eventually, after resolving all References, Function calls and bracketed blocks, the Valuestring is
    ' presented as a simple sequence of values and operators which are then resolved according to the
    ' rules of operator precedence.
    '
    ' ********************************************************************************************

    Dim ParseArray(,) As Object

    Const FORMULA_OPERATORS As String = "*/+-&^=<>!"
    Dim ResolvedValue As Object
    Dim StackValue As String
    Dim TokenString As String

    Dim TokenStart As Integer
    Dim TokenEnd As Integer
    Dim WordStart As Integer
    Dim FunctionStart As Integer

    Dim CurrentChar As Integer
    Dim BracketCount As Integer
    Dim FunctionBracketCount As Integer

    Dim ElementValue As Object
    Dim IsSQLSELECT As Boolean
    Dim SQLSELECT_BracketCount As Integer

    Dim Has_Operator As Boolean
    Dim OperandCount As Integer
    Dim OperatorString As String
    Dim OperandString As String

    Dim FieldReference As String
    Dim Field_Delimiter As String
    Dim Field_ResolveCount As Integer

    Dim Reference_Row As Integer
    Dim Reference_Field As Integer
    Dim Counter As Integer
    Dim MoveCounter As Integer

    Dim Loop_Flag As Boolean

    Dim Temp_Param1 As Object
    Dim Temp_Param2 As Object
    Dim Param1_IsString As Boolean
    Dim Param2_IsString As Boolean


    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    ' Retrieve the String to resolve and perform some initial checks :-
    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    ' Return the object if it is not a String. Other value types can only be themselves.
    If (Not (ValueString.GetType Is GetType(String))) Then
      Return ValueString
    End If


    StackValue = p_Stack
    TokenString = CStr(ValueString).Trim

    ' Return empty strings :-
    If TokenString.Length <= 0 Then
      ResolvedValue = ""
      GoTo ResolveToken_Exit
    End If

    If TokenString.StartsWith("=") Then
      TokenString = TokenString.Substring(1, TokenString.Length - 1)
    End If


    ' Is it a simple date ?
    If TokenString.Length > 2 Then
      If (TokenString.StartsWith("#")) And (TokenString.EndsWith("#")) And IsDate(TokenString.Substring(1, TokenString.Length - 2)) Then
        ResolvedValue = CDate(TokenString.Substring(1, TokenString.Length - 2)).ToOADate
        GoTo ResolveToken_Exit
      End If
    End If

    ' Is it a 'SELECT' statement ?
    IsSQLSELECT = False
    If TokenString.ToUpper.StartsWith("SELECT ") Then
      IsSQLSELECT = True
      GoTo ResolveToken_Evaluate
    End If

    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    ' OK, Parse the string for bracketed blocks, use a call to 'ResolveToken()' to resolve down to a single value.
    '     Also, resolve any function calls in a similar fasion via a call to the 'ResolveFunction()' function.
    '
    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    BracketCount = 0
    FunctionBracketCount = 0
    SQLSELECT_BracketCount = (-1)

    CurrentChar = 1
    WordStart = 0

    While CurrentChar <= TokenString.Length

      ' Skip Through Quoted sections and dates.

      If TokenString.Substring(CurrentChar - 1, 1) = Chr(34) Then
        CurrentChar += 1
        While TokenString.Substring(CurrentChar - 1, 1) <> Chr(34)
          CurrentChar += 1
          If CurrentChar > TokenString.Length Then
            ' Unmatched Quote
            ResolvedValue = "{Unmatched quote : " & Chr(34) & "}"
            GoTo ResolveToken_Exit
          End If
        End While
      ElseIf TokenString.Substring(CurrentChar - 1, 1) = "'" Then
        CurrentChar += 1
        While TokenString.Substring(CurrentChar - 1, 1) <> "'"
          CurrentChar += 1
          If CurrentChar > TokenString.Length Then
            ' Unmatched Quote
            ResolvedValue = "{Unmatched quote : '}"
            GoTo ResolveToken_Exit
          End If
        End While
      ElseIf TokenString.Substring(CurrentChar - 1, 1) = "#" Then
        CurrentChar += 1
        While TokenString.Substring(CurrentChar - 1, 1) <> "#"
          CurrentChar += 1
          If CurrentChar > TokenString.Length Then
            ' Unmatched Quote
            ResolvedValue = "{Unmatched #}"
            GoTo ResolveToken_Exit
          End If
        End While
      Else

        ' If an open round bracket is found :-
        If TokenString.Substring(CurrentChar - 1, 1) = "(" Then

          ' Check for a preceeding function name.
          If (FunctionBracketCount = 0) And (SQLSELECT_BracketCount < 0) Then
            If (CurrentChar - (WordStart + 1)) > 0 Then

              'Select Case UCase(Mid(TokenString, (WordStart + 1), (CurrentChar - (WordStart + 1))))
              Select Case TokenString.Substring(WordStart, (CurrentChar - (WordStart + 1))).ToUpper()
                Case "NOW", "INT", "CDBL", "CINT", "CSTR", _
                 "CDATE", "IIF", "IF", "MAX", "MIN", "ROUND", _
                 "LOG", "LN", "EXP", "SQLSELECT", "FORMAT", "GETTICKET"

                  FunctionBracketCount = BracketCount + 1
                  FunctionStart = WordStart + 1

                Case Else
                  ' Unknown Function !
                  pMainForm.LogError(Permission_Area & ", ResolveCell()", LOG_LEVELS.Warning, "", "Unknown Function : " & UCase(Mid(TokenString, (WordStart + 1), (CurrentChar - (WordStart + 1)))), "", True)
                  ResolvedValue = "{Unknown Function `" & TokenString.Substring(WordStart, (CurrentChar - (WordStart + 1))).ToUpper() & "`}"
                  GoTo ResolveToken_Exit
              End Select
            End If
          End If

          ' Increment standard Bracket counter

          BracketCount = BracketCount + 1
          If (BracketCount = 1) Then
            TokenStart = CurrentChar
          End If

          ' If a Close bracket is found :-
        ElseIf TokenString.Substring(CurrentChar - 1, 1) = ")" Then

          ' If this closing bracket matches a previously identified function start.....
          If (BracketCount = FunctionBracketCount) And (BracketCount > 0) Then

            ' Resolve Function

            ElementValue = ResolveFunction(pMainForm, Permission_Area, StackValue, TokenString.Substring(FunctionStart - 1, (CurrentChar - FunctionStart) + 1))
            If Left(CStr(ElementValue), 1) = "{" Then    ' ERROR
              ResolvedValue = ElementValue
              GoTo ResolveToken_Exit
            End If
            TokenString = TokenString.Substring(0, FunctionStart - 1) & CStr(ElementValue) & TokenString.Substring(CurrentChar)
            'TokenString = Left(TokenString, FunctionStart - 1) & ElementValue & Right(TokenString, Len(TokenString) - CurrentChar)
            CurrentChar = (FunctionStart - 1) + CStr(ElementValue).Length

            FunctionStart = 0
            FunctionBracketCount = 0

          ElseIf (BracketCount = 1) Then
            ' Natural Close of Bracket block, Call ResolveToken()

            TokenEnd = CurrentChar

            If (TokenEnd - (TokenStart + 1)) = 0 Then
              ' Error () with no contents
              ' Does it matter ?


            End If

            ElementValue = ResolveToken(pMainForm, Permission_Area, StackValue, Mid(TokenString, TokenStart + 1, (TokenEnd - (TokenStart + 1))))

            If CStr(ElementValue).StartsWith("{") Then      ' ERROR
              ResolvedValue = ElementValue
              GoTo ResolveToken_Exit
            End If

            ' Insert resolved value into the Token String.

            TokenString = TokenString.Substring(0, TokenStart - 1) & CStr(ElementValue) & TokenString.Substring(TokenEnd)
            'TokenString = Left(TokenString, TokenStart - 1) & ElementValue & Right(TokenString, Len(TokenString) - TokenEnd)
            CurrentChar = (TokenStart - 1) + CStr(ElementValue).Length

            ' Check that there is an operator before the next token or bracket block.

            Has_Operator = True
            For Counter = (CurrentChar + 1) To TokenString.Length
              If InStr("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.#({['" & Chr(36), TokenString.Substring(Counter - 1, 1)) > 0 Then
                Has_Operator = False
                Exit For
              End If
              If InStr(FORMULA_OPERATORS, TokenString.Substring(Counter - 1, 1)) > 0 Then
                Exit For
              End If
            Next Counter
            If (Has_Operator = False) Then
              ' Error, Expected operator.
              pMainForm.LogError(Permission_Area & ", ResolveCell()", LOG_LEVELS.Warning, "", "Expected Operator", "", True)
              ResolvedValue = "{Expected Operator}"
              GoTo ResolveToken_Exit
            End If

          ElseIf (BracketCount = 0) Then
            ' Unmatched close bracket
            pMainForm.LogError(Permission_Area & ", ResolveCell()", LOG_LEVELS.Warning, "", "Unmatched Close Bracket", "", True)
            ResolvedValue = "{Unmatched Close Bracket}"
            GoTo ResolveToken_Exit
          End If

          BracketCount = BracketCount - 1
          If (SQLSELECT_BracketCount > BracketCount) Then
            SQLSELECT_BracketCount = (-1)
          End If

        ElseIf TokenString.Substring(CurrentChar - 1, 1) = " " Then
          'If Mid(TokenString, (WordStart + 1), (CurrentChar - (WordStart + 1))) = "SELECT" Then
          If TokenString.Substring(WordStart, (CurrentChar - (WordStart + 1))) = "SELECT" Then
            SQLSELECT_BracketCount = BracketCount
          End If
        End If


        'Maintain 'WordStart' - Intended to record the start of the current word (function name)
        If TokenString.Length > 0 Then
          If InStr("ABCDEFGHIJKLMNOPQRSTUVWXYZ_#", TokenString.Substring(CurrentChar - 1, 1).ToUpper) = 0 Then
            WordStart = CurrentChar
          End If
        End If
      End If

      CurrentChar = CurrentChar + 1
    End While

    If (BracketCount <> 0) Then
      ' Unmatched Brackets
      pMainForm.LogError(Permission_Area & ", ResolveCell()", LOG_LEVELS.Warning, "", "Unmatched Open Bracket", "", True)
      ResolvedValue = "{Unmatched Open Bracket}"
      GoTo ResolveToken_Exit
    End If

ResolveToken_Evaluate:

    If Len(TokenString) = 0 Then
      ResolvedValue = ""
      GoTo ResolveToken_Exit
    End If

    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    ' OK, We now have a string with no brackets, no functions, just operators.
    ' It May be a SQL SELECT Statement though.
    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    ' Resolve delimited Field references
    ' Loop through twice.
    ' First Resolve Curley bracket field references
    ' Second, if this is not a SQL SELECT statement, resolve square bracket references.
    ' Square bracket references may not be used in SQL statements since square brackets form part
    ' of the SQL syntax.
    ' This process does not resolve functions for Select statements as it assumes that they are SQL functions.
    ' Use the SQLSELECT function to build SELECT Statements using our functions.

    OperandCount = 0
    For Field_ResolveCount = 1 To 2
      If Field_ResolveCount = 1 Then
        Field_Delimiter = "{}"
      Else
        If IsSQLSELECT = False Then
          Field_Delimiter = "[]"
        Else
          Exit For
        End If
      End If

      BracketCount = 0

      CurrentChar = 1
      WordStart = 0

      While CurrentChar <= TokenString.Length
        ' Skip Through Quoted sections.
        If TokenString.Substring(CurrentChar - 1, 1) = Chr(34) Then
          CurrentChar += 1
          While TokenString.Substring(CurrentChar - 1, 1) <> Chr(34)
            CurrentChar += 1
            If CurrentChar > TokenString.Length Then
              ' Unmatched Quote
              ResolvedValue = "{Unmatched quote : " & Chr(34) & "}"
              GoTo ResolveToken_Exit
            End If
          End While
        ElseIf TokenString.Substring(CurrentChar - 1, 1) = "'" Then
          CurrentChar += 1
          While TokenString.Substring(CurrentChar - 1, 1) <> "'"
            CurrentChar += 1
            If CurrentChar > TokenString.Length Then
              ' Unmatched Quote
              ResolvedValue = "{Unmatched quote : '}"
              GoTo ResolveToken_Exit
            End If
          End While
        ElseIf TokenString.Substring(CurrentChar - 1, 1) = "#" Then
          CurrentChar += 1
          While TokenString.Substring(CurrentChar - 1, 1) <> "#"
            CurrentChar += 1
            If CurrentChar > TokenString.Length Then
              ' Unmatched Quote
              ResolvedValue = "{Unmatched Date Delimeter : #}"
              GoTo ResolveToken_Exit
            End If
          End While
        Else

          If TokenString.Substring(CurrentChar - 1, 1) = Field_Delimiter.Substring(0, 1) Then   ' {

            BracketCount = BracketCount + 1
            If (BracketCount = 1) Then
              TokenStart = CurrentChar
            End If

          ElseIf TokenString.Substring(CurrentChar - 1, 1) = Field_Delimiter.Substring(Field_Delimiter.Length - 1) Then  ' }
            If (BracketCount = 1) Then
              ' Natural Close of Bracket block

              TokenEnd = CurrentChar

              FieldReference = TokenString.Substring(TokenStart, (TokenEnd - (TokenStart + 1)))

              If InStr(FieldReference, ".") <= 0 Then
                If Get_CT_TermsFieldType(FieldReference) >= 0 Then
                  ElementValue = Get_CT_TermsFieldValue(FieldReference)
                ElseIf Get_CT_ParameterType(FieldReference) >= 0 Then
                  ElementValue = Get_CT_ParameterValue(FieldReference)
                Else
                  ElementValue = ResolveToken(pMainForm, Permission_Area, StackValue, FieldReference)
                End If
              Else
                If IsNumeric(FieldReference.Substring(0, InStr(FieldReference, ".") - 1)) Then
                  ' Reduce reference Row by One to account for CTTransactions array now starting at Zero.
                  Reference_Row = CInt(FieldReference.Substring(0, InStr(FieldReference, ".") - 1)) - 1
                Else
                  Reference_Row = (-999)
                End If

                Reference_Field = Get_Template_FieldColumn(FieldReference.Substring(InStr(FieldReference, ".")))

                ElementValue = ResolveCell(pMainForm, Permission_Area, StackValue, Reference_Row, Reference_Field)

                If (ElementValue.GetType Is GetType(String)) AndAlso (CStr(ElementValue).StartsWith("{")) Then
                  ' Error
                  ResolvedValue = ElementValue
                  GoTo ResolveToken_Exit
                End If
              End If

              TokenString = TokenString.Substring(0, TokenStart - 1) & CStr(ElementValue) & TokenString.Substring(TokenEnd)
              CurrentChar = (TokenStart - 1) + CStr(ElementValue).Length
            ElseIf (BracketCount = 0) Then
              ' Unmatched close bracket
              pMainForm.LogError(Permission_Area & ", ResolveCell()", LOG_LEVELS.Warning, "", "Unmatched Close Bracket", "", True)
              ResolvedValue = "{Unmatched Close Bracket}"
              GoTo ResolveToken_Exit
            End If

            BracketCount = BracketCount - 1
          End If

          If Field_ResolveCount = 1 Then    ' Only Count operands and strip spaces during the first pass.
            If (BracketCount = 0) And (IsSQLSELECT = False) Then
              ' Count Operands and Strip out spaces if not in a bracket block.
              If InStr(FORMULA_OPERATORS, TokenString.Substring(CurrentChar - 1, 1)) > 0 Then
                If CurrentChar > 1 Then
                  If InStr(FORMULA_OPERATORS, TokenString.Substring(CurrentChar - 2, 1)) > 0 Then
                    ' Don't Doublecount multiple operator characters appearing together.
                    OperandCount = OperandCount - 1
                  End If
                End If
                OperandCount += 1
              ElseIf TokenString.Substring(CurrentChar - 1, 1) = " " Then
                TokenString = TokenString.Substring(0, CurrentChar - 1) & TokenString.Substring(CurrentChar)
                CurrentChar -= 1
              End If
            End If
          End If
        End If

        CurrentChar += 1
      End While

      If (BracketCount <> 0) Then
        ' Unmatched Brackets
        pMainForm.LogError(Permission_Area & ", ResolveCell()", LOG_LEVELS.Warning, "", "Unmatched Open Bracket", "", True)
        ResolvedValue = "{Unmatched Open Bracket}"
        GoTo ResolveToken_Exit
      End If
    Next Field_ResolveCount


    ' Resolve SQL SELECT statements.
    If (IsSQLSELECT = True) Then
      If (OperandCount = 0) Then

        Dim SelectCommand As New SqlCommand

        ' Fudge for Access->DotNet Migration
        '
        ' The AccessDB uses Queries such as qrytblTransaction in order to reference the underlying tables.
        ' These queries do not exist in SQL Server and these references must be translated into 
        ' fn_tblXXXX_SelectKD(Null) calls.
        '
        If (InStr(TokenString.ToUpper, "FROM QRYTBL") > 1) Then
          Dim FirstPart As String
          Dim TableName As String
          Dim Lastpart As String

          FirstPart = TokenString.Substring(0, InStr(TokenString.ToUpper, "FROM QRYTBL") - 1)
          TableName = TokenString.Substring(InStr(TokenString.ToUpper, "FROM QRYTBL") + 7)
          If InStr(TableName, " ") > 1 Then
            Lastpart = TableName.Substring(InStr(TableName, " "))
            TableName = TableName.Substring(0, InStr(TableName, " ") - 1)
          Else
            Lastpart = ""
          End If

          TokenString = FirstPart & "FROM fn_" & TableName & "_SelectKD(Null) " & Lastpart
        End If

        Try

          SelectCommand.Parameters.Clear()
          SelectCommand.Connection = pMainForm.GetVeniceConnection
          SelectCommand.CommandType = System.Data.CommandType.Text
          SelectCommand.CommandText = TokenString
          SelectCommand.CommandTimeout = TRANSACTION_SQLCOMMAND_TIMEOUT

          SyncLock SelectCommand.Connection
            ResolvedValue = SelectCommand.ExecuteScalar
          End SyncLock
          If (ResolvedValue Is Nothing) Then ResolvedValue = ""
        Catch ex As Exception
          Call pMainForm.LogError(Permission_Area & ", ResolveCell()", LOG_LEVELS.Error, ex.Message, "SELECT statement Error : " & TokenString, ex.StackTrace, False)
          ResolvedValue = "{SELECT Error}"
          GoTo ResolveToken_Exit

        Finally
          Try
            If (Not (SelectCommand.Connection Is Nothing)) Then
              SelectCommand.Connection.Close()
            End If
          Catch ex As Exception
          End Try
        End Try

        SelectCommand = Nothing

        ' Post process returned value to ensure formatting
        If (IsDate(ResolvedValue) = True) Then
          ResolvedValue = CDate(ResolvedValue).ToOADate
        ElseIf (IsNumeric(ResolvedValue) = False) Then
          If (CStr(ResolvedValue) <> "True") And (CStr(ResolvedValue) <> "False") Then
            ResolvedValue = Chr(34) & CStr(ResolvedValue) & Chr(34)
          End If
        End If

        GoTo ResolveToken_Exit
      Else
        ' Error.
        pMainForm.LogError(Permission_Area & ", ResolveCell()", LOG_LEVELS.Warning, "", "Syntax Error, Mixed SELECT with operands directly.", "", True)
        ResolvedValue = "{Syntax Error, Near SELECT}"
        GoTo ResolveToken_Exit
      End If
    End If

    ' OK, Not a SELECT Statement, Resolve Token String :-

    If OperandCount = 0 Then
      ' No operands. This is either a literal or a Cell or Field Reference or a parameter.

      FieldReference = TokenString

      If TokenString.EndsWith("%") Then
        If IsNumeric(TokenString.Substring(0, TokenString.Length - 1)) Then
          ResolvedValue = CDec(TokenString.Substring(0, TokenString.Length - 1)) / CDec(100)
          GoTo ResolveToken_Exit
        Else
          ResolvedValue = "{Syntax Error, Near %}"
          GoTo ResolveToken_Exit
        End If
      End If

      If TokenString.StartsWith(Chr(34)) Or TokenString.StartsWith("'") Then
        ' String Literal
        ElementValue = TokenString
      ElseIf TokenString.StartsWith("#") And (TokenString.Substring(TokenString.Length - 1) = "#") Then
        If IsDate(TokenString.Substring(1, TokenString.Length - 2)) Then
          ElementValue = CDate(TokenString.Substring(1, TokenString.Length - 2)).ToOADate
        Else
          ResolvedValue = "{Unrecognisable Date : " & TokenString & "}"
          GoTo ResolveToken_Exit
        End If
      ElseIf IsNumeric(TokenString) Then
        ElementValue = CDec(TokenString)

      ElseIf IsDate(TokenString) Then
        ElementValue = CDate(TokenString).ToOADate

      ElseIf (TokenString = "True") Or (TokenString = "False") Then
        ' Number, Date or Logical
        ElementValue = TokenString

      ElseIf InStr(FieldReference, ".") <= 0 Then
        If (Get_CT_TermsFieldColumn(FieldReference) >= 0) Then
          ' Appears to be a valid field reference
          ElementValue = Get_CT_TermsFieldValue(FieldReference)

        ElseIf Get_CT_ParameterType(FieldReference) >= 0 Then
          ' Appears to be a valid Parameter value
          ElementValue = Get_CT_ParameterValue(FieldReference)

        Else

          ' Not a String literal, Number, Date, True, False, Parameter or field value....

          ' Prompt for Parameter Value

          ElementValue = InputBox("Please enter value for : " & FieldReference, "Enter Parameter Value", "")
          If Set_CT_Parameter(pMainForm, Permission_Area, FieldReference, 0, ElementValue) = False Then
            ElementValue = "{Error Saving Parameter `" & FieldReference & "`}"
          End If
        End If

      Else
        ' Field Reference has '.'

        If IsNumeric(FieldReference.Substring(0, InStr(FieldReference, ".") - 1)) Then
          ' Reduce reference Row by One to account for CTTransactions array now starting at Zero.
          Reference_Row = CInt(FieldReference.Substring(0, InStr(FieldReference, ".") - 1)) - 1
          Reference_Field = Get_Template_FieldColumn(FieldReference.Substring(InStr(FieldReference, ".")))

          If (Reference_Field >= 0) Then
            ' Looks like a valid Cell Reference
            ElementValue = ResolveCell(pMainForm, Permission_Area, StackValue, Reference_Row, Reference_Field)

          Else
            ' not a valid Field Reference
            ElementValue = "{Unrecognised Token `" & FieldReference & "`}"

            ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            ' Prompt for Resolution ????
            ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
          End If

        Else

          ' not a valid Field Reference
          ElementValue = "{Unrecognised Token `" & FieldReference & "`}"

          ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
          ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
          ' Prompt for Resolution ????
          ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
          ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        End If
      End If

      ResolvedValue = ElementValue
      GoTo ResolveToken_Exit
    End If

    ' OK We have operands ......

    OperandCount = OperandCount + 2 ' just in case
    ReDim ParseArray(1, (OperandCount + 1))
    For Counter = 0 To (OperandCount + 1)
      ParseArray(0, Counter) = ""
      ParseArray(1, Counter) = ""
    Next Counter
    Counter = 0
    CurrentChar = 1
    While CurrentChar <= TokenString.Length

      ' Split out operator
      Loop_Flag = True
      While (CurrentChar <= TokenString.Length) And (Loop_Flag = True)
        If (InStr(FORMULA_OPERATORS, TokenString.Substring(CurrentChar - 1, 1)) > 0) Then
          ParseArray(1, Counter) = CStr(ParseArray(1, Counter)) & TokenString.Substring(CurrentChar - 1, 1)
          CurrentChar = CurrentChar + 1
        Else
          Loop_Flag = False
        End If
      End While

      Counter = Counter + 1

      ' Split out operand
      Loop_Flag = True
      While (CurrentChar <= TokenString.Length) And (Loop_Flag = True)
        If (InStr(FORMULA_OPERATORS, TokenString.Substring(CurrentChar - 1, 1)) = 0) Then
          ParseArray(0, Counter) = CStr(ParseArray(0, Counter)) & TokenString.Substring(CurrentChar - 1, 1)

          ' Skip Through Quoted sections.
          If TokenString.Substring(CurrentChar - 1, 1) = Chr(34) Then
            CurrentChar += 1
            ParseArray(0, Counter) = CStr(ParseArray(0, Counter)) & TokenString.Substring(CurrentChar - 1, 1)
            While TokenString.Substring(CurrentChar - 1, 1) <> Chr(34)
              CurrentChar += 1
              ParseArray(0, Counter) = CStr(ParseArray(0, Counter)) & TokenString.Substring(CurrentChar - 1, 1)
              If CurrentChar > TokenString.Length Then
                ' Unmatched Quote
                ResolvedValue = "{Unmatched quote : " & Chr(34) & "}"
                GoTo ResolveToken_Exit
              End If
            End While
          ElseIf TokenString.Substring(CurrentChar - 1, 1) = "'" Then
            CurrentChar += 1
            ParseArray(0, Counter) = CStr(ParseArray(0, Counter)) & TokenString.Substring(CurrentChar - 1, 1)
            While Mid(TokenString, CurrentChar, 1) <> "'"
              CurrentChar += 1
              ParseArray(0, Counter) = CStr(ParseArray(0, Counter)) & TokenString.Substring(CurrentChar - 1, 1)
              If CurrentChar > TokenString.Length Then
                ' Unmatched Quote
                ResolvedValue = "{Unmatched quote : '}"
                GoTo ResolveToken_Exit
              End If
            End While
          ElseIf TokenString.Substring(CurrentChar - 1, 1) = "#" Then
            CurrentChar += 1
            While (TokenString.Substring(CurrentChar - 1, 1) <> "#") And (CurrentChar <= TokenString.Length)
              ParseArray(0, Counter) = CStr(ParseArray(0, Counter)) & TokenString.Substring(CurrentChar - 1, 1)
              CurrentChar += 1
            End While
            If (CurrentChar > TokenString.Length) Then
              ' Error Unmatched '#'
              ResolvedValue = "{Syntax Error, Unmatched `#`}"
              GoTo ResolveToken_Exit
            End If
            ParseArray(0, Counter) = CStr(ParseArray(0, Counter)) & TokenString.Substring(CurrentChar - 1, 1)
          End If

          CurrentChar += 1
        Else
          Loop_Flag = False
        End If
      End While

      If CStr(ParseArray(0, Counter)).Length > 0 Then
        OperandString = CStr(ParseArray(0, Counter))

        If OperandString.EndsWith("%") Then
          If IsNumeric(OperandString.Substring(0, OperandString.Length - 1)) Then
            ParseArray(0, Counter) = CDec(OperandString.Substring(0, OperandString.Length - 1)) / CDec(100)
          Else
            ResolvedValue = "{Syntax Error, near %}"
            GoTo ResolveToken_Exit
          End If
        End If

        ' Convert Date Parameters to Numbers
        If OperandString.Length > 2 Then
          If (OperandString.StartsWith("#")) And _
          (OperandString.Substring(OperandString.Length - 1) = "#") And _
          IsDate(OperandString.Substring(1, OperandString.Length - 2)) Then
            ParseArray(0, Counter) = CDate(OperandString.Substring(1, OperandString.Length - 2)).ToOADate
          End If
        End If

        ' The operator may be a Parameter. Resolve if it seems likely.
        If (IsNumeric(OperandString) = True) Then
          ParseArray(0, Counter) = CDec(OperandString)
        ElseIf (IsDate(OperandString) = True) Then
          ParseArray(0, Counter) = CDate(OperandString).ToOADate
        Else
          If Not (((OperandString.StartsWith("'")) And (OperandString.Substring(OperandString.Length - 1) = "'")) Or ((OperandString.StartsWith(Chr(34))) And (OperandString.Substring(OperandString.Length - 1) = Chr(34)))) Then
            ' Not Numeric, Date or delimited string
            If (OperandString <> "True") And (OperandString <> "False") Then
              '
              ParseArray(0, Counter) = ResolveToken(pMainForm, Permission_Area, StackValue, ParseArray(0, Counter))
              If CStr(ParseArray(0, Counter)).StartsWith("{") Then
                ResolvedValue = ParseArray(0, Counter)
                GoTo ResolveToken_Exit
              End If
            End If
          End If
        End If
      End If
    End While

    ' OK Operator and operand array is prepared

    ' Rationalise the operators
    If CStr(ParseArray(1, OperandCount + 1)).Length > 0 Then
      ' Error, Operand on the Last Line

      ResolvedValue = "{Runtime : Operator Count Error}"
      GoTo ResolveToken_Exit

    End If

    For Counter = 0 To (OperandCount + 1)
      OperatorString = CStr(ParseArray(1, Counter))
      If (OperatorString.Length > 0) Then
        Loop_Flag = True
        While (Loop_Flag = True)
          Loop_Flag = False

          CurrentChar = InStr(OperatorString, "--")
          While CurrentChar > 0
            OperatorString = OperatorString.Substring(0, CurrentChar - 1) & "+" & _
            OperatorString.Substring(CurrentChar + 1)

            CurrentChar = InStr(OperatorString, "--")
            Loop_Flag = True
          End While

          CurrentChar = InStr(OperatorString, "++")
          While CurrentChar > 0
            OperatorString = OperatorString.Substring(0, CurrentChar - 1) & "+" & _
            OperatorString.Substring(CurrentChar + 1)

            CurrentChar = InStr(OperatorString, "++")
            Loop_Flag = True
          End While

          CurrentChar = InStr(OperatorString, "+-")
          While CurrentChar > 0
            OperatorString = OperatorString.Substring(0, CurrentChar - 1) & "-" & _
            OperatorString.Substring(CurrentChar + 1)

            CurrentChar = InStr(OperatorString, "+-")
            Loop_Flag = True
          End While

          CurrentChar = InStr(OperatorString, "-+")
          While CurrentChar > 0
            OperatorString = OperatorString.Substring(0, CurrentChar - 1) & "-" & _
            OperatorString.Substring(CurrentChar + 1)

            CurrentChar = InStr(OperatorString, "-+")
            Loop_Flag = True
          End While
        End While

        ' Attend to trailing '+' or '-' operators where there is more than one character.
        If (OperatorString.Length >= 2) Or (Counter = 0) Then
          If OperatorString.EndsWith("+") Then

            ' Drop a single trailing '+'
            OperatorString = OperatorString.Substring(0, OperatorString.Length - 1)

          ElseIf OperatorString.EndsWith("-") Then

            ' Impose a single trailing '-' on the subsequent operand
            If Counter < (OperandCount + 1) Then
              If IsNumeric(ParseArray(0, Counter + 1)) = True Then
                ParseArray(0, Counter + 1) = (-CDec(ParseArray(0, Counter + 1)))
                OperatorString = OperatorString.Substring(0, OperatorString.Length - 1)
              Else
                ' Type mismatch Error
                ResolvedValue = "{Operator Type mismatch}"
                GoTo ResolveToken_Exit
              End If
            Else
              ' Error, Array overflow ? !
              ResolvedValue = "{Runtime : Operator Array Overflow}"
              GoTo ResolveToken_Exit
            End If

          End If
        End If

        ParseArray(1, Counter) = OperatorString

      End If
    Next Counter

    If CStr(ParseArray(1, 0)).Length > 0 Then
      ResolvedValue = "{Leading Operator Error, Invalid Format}"
      GoTo ResolveToken_Exit
    End If

    ' OK, Now process each operator in order

    ' ^
    For Counter = 1 To (OperandCount)
      OperatorString = CStr(ParseArray(1, Counter))

      While OperatorString = "^"
        ' Process
        ' Needs Two Numeric Parameters....
        If IsNumeric(ParseArray(0, Counter)) And IsNumeric(ParseArray(0, Counter + 1)) Then
          ParseArray(0, Counter) = CDec(ParseArray(0, Counter)) ^ CDec(ParseArray(0, Counter + 1))
        Else
          ResolvedValue = "{Operator ^ Type mismatch}"
          GoTo ResolveToken_Exit
        End If

        ' Move rest up
        For MoveCounter = Counter To (OperandCount - 1)
          ParseArray(0, MoveCounter + 1) = ParseArray(0, MoveCounter + 2)
          ParseArray(1, MoveCounter) = ParseArray(1, MoveCounter + 1)
        Next MoveCounter
        ParseArray(0, UBound(ParseArray, 2)) = ""
        ParseArray(1, UBound(ParseArray, 2)) = ""

        OperatorString = CStr(ParseArray(1, Counter))
      End While
    Next Counter

    ' * /
    For Counter = 1 To (OperandCount)
      OperatorString = CStr(ParseArray(1, Counter))

      While (InStr("*|/", OperatorString) > 0) And (OperatorString.Length > 0)

        ' Process
        ' Needs Two Numeric Parameters....
        If IsNumeric(ParseArray(0, Counter)) And IsNumeric(ParseArray(0, Counter + 1)) Then
          If OperatorString = "*" Then
            ParseArray(0, Counter) = CDec(ParseArray(0, Counter)) * CDec(ParseArray(0, Counter + 1))
          ElseIf OperatorString = "/" Then
            If CDec(ParseArray(0, Counter + 1)) = 0 Then
              ResolvedValue = "{Error, Divide by zero}"
              GoTo ResolveToken_Exit
            Else
              ParseArray(0, Counter) = CDec(ParseArray(0, Counter)) / CDec(ParseArray(0, Counter + 1))
            End If
          Else
            ' Error
            ResolvedValue = "{Operator " & OperatorString & " Type mismatch}"
            GoTo ResolveToken_Exit
          End If
        Else
          ResolvedValue = "{Operator " & OperatorString & " Type mismatch}"
          GoTo ResolveToken_Exit
        End If

        ' Move rest up
        For MoveCounter = Counter To (OperandCount - 1)
          ParseArray(0, MoveCounter + 1) = ParseArray(0, MoveCounter + 2)
          ParseArray(1, MoveCounter) = ParseArray(1, MoveCounter + 1)
        Next MoveCounter
        ParseArray(0, UBound(ParseArray, 2)) = ""
        ParseArray(1, UBound(ParseArray, 2)) = ""

        OperatorString = CStr(ParseArray(1, Counter))
      End While
    Next Counter

    ' * /
    For Counter = 1 To (OperandCount)
      OperatorString = CStr(ParseArray(1, Counter))

      While (InStr("+|-", OperatorString) > 0) And (OperatorString.Length > 0)
        ' Process
        ' Needs Two Numeric or Date Parameters....
        If (IsNumeric(ParseArray(0, Counter)) Or IsDate(ParseArray(0, Counter))) And _
         (IsNumeric(ParseArray(0, Counter + 1)) Or IsDate(ParseArray(0, Counter + 1))) Then
          If IsNumeric(ParseArray(0, Counter)) Then
            Temp_Param1 = CDec(ParseArray(0, Counter))
          ElseIf IsDate(ParseArray(0, Counter)) Then
            Temp_Param1 = CDate(ParseArray(0, Counter)).ToOADate
          Else
            ' Error
            ResolvedValue = "{Operator " & OperatorString & " Type mismatch}"
            GoTo ResolveToken_Exit
          End If

          If IsNumeric(ParseArray(0, Counter + 1)) Then
            Temp_Param2 = CDec(ParseArray(0, Counter + 1))
          ElseIf IsDate(ParseArray(0, Counter + 1)) Then
            Temp_Param2 = CDate(ParseArray(0, Counter + 1)).ToOADate
          Else
            ' Error
            ResolvedValue = "{Operator " & OperatorString & " Type mismatch}"
            GoTo ResolveToken_Exit
          End If

          If OperatorString = "+" Then
            ParseArray(0, Counter) = CDec(Temp_Param1) + CDec(Temp_Param2)
          ElseIf OperatorString = "-" Then
            ParseArray(0, Counter) = CDec(Temp_Param1) - CDec(Temp_Param2)
          Else
            ' Error
            ResolvedValue = "{Operator " & OperatorString & " Type mismatch}"
            GoTo ResolveToken_Exit
          End If
        Else
          ResolvedValue = "{Operator " & OperatorString & " Type mismatch}"
          GoTo ResolveToken_Exit
        End If

        ' Move rest up
        For MoveCounter = Counter To (OperandCount - 1)
          ParseArray(0, MoveCounter + 1) = ParseArray(0, MoveCounter + 2)
          ParseArray(1, MoveCounter) = ParseArray(1, MoveCounter + 1)
        Next MoveCounter
        ParseArray(0, UBound(ParseArray, 2)) = ""
        ParseArray(1, UBound(ParseArray, 2)) = ""

        OperatorString = CStr(ParseArray(1, Counter))
      End While
    Next Counter


    ' &
    For Counter = 1 To (OperandCount)
      OperatorString = CStr(ParseArray(1, Counter))

      While (InStr("|&|", "|" & OperatorString & "|") > 0) And (OperatorString.Length > 0)
        ' Process
        ' Two parameters of any type
        Param1_IsString = False
        Param2_IsString = False

        Temp_Param1 = ParseArray(0, Counter)
        Temp_Param2 = ParseArray(0, Counter + 1)

        If CStr(Temp_Param1).Length >= 2 Then
          If (InStr("'" & Chr(34), CStr(Temp_Param1).Substring(0, 1)) > 0) And (InStr("'" & Chr(34), CStr(Temp_Param1).Substring(CStr(Temp_Param1).Length - 1)) > 0) Then
            If CStr(Temp_Param1).Length = 2 Then
              Temp_Param1 = ""
            Else
              Temp_Param1 = CStr(Temp_Param1).Substring(1, CStr(Temp_Param1).Length - 2)
            End If
          End If
        End If

        If CStr(Temp_Param2).Length >= 2 Then
          If (InStr("'" & Chr(34), CStr(Temp_Param2).Substring(0, 1)) > 0) And (InStr("'" & Chr(34), CStr(Temp_Param2).Substring(CStr(Temp_Param2).Length - 1)) > 0) Then
            If CStr(Temp_Param2).Length = 2 Then
              Temp_Param2 = ""
            Else
              Temp_Param2 = CStr(Temp_Param2).Substring(1, CStr(Temp_Param2).Length - 2)
            End If
          End If
        End If

        ParseArray(0, Counter) = Chr(34) & CStr(Temp_Param1) & CStr(Temp_Param2) & Chr(34)

        ' Move rest up
        For MoveCounter = Counter To (OperandCount - 1)
          ParseArray(0, MoveCounter + 1) = ParseArray(0, MoveCounter + 2)
          ParseArray(1, MoveCounter) = ParseArray(1, MoveCounter + 1)
        Next MoveCounter
        ParseArray(0, UBound(ParseArray, 2)) = ""
        ParseArray(1, UBound(ParseArray, 2)) = ""

        OperatorString = CStr(ParseArray(1, Counter))
      End While
    Next Counter


    ' =, ==,
    For Counter = 1 To (OperandCount)
      OperatorString = CStr(ParseArray(1, Counter))

      While (InStr("|=|==|<|>|<=|>=|<>|!=|", "|" & OperatorString & "|") > 0) And (OperatorString.Length > 0)
        ' Process
        ' Two parameters of any type
        Param1_IsString = False
        Param2_IsString = False

        If IsNumeric(ParseArray(0, Counter)) Then
          Temp_Param1 = CDec(ParseArray(0, Counter))
        ElseIf IsDate(ParseArray(0, Counter)) Then
          Temp_Param1 = CDate(ParseArray(0, Counter)).ToOADate
        Else
          Temp_Param1 = ParseArray(0, Counter)

          If CStr(Temp_Param1).Length >= 2 Then
            If (InStr("'" & Chr(34), CStr(Temp_Param1).Substring(0, 1)) > 0) And (InStr("'" & Chr(34), CStr(Temp_Param1).Substring(CStr(Temp_Param1).Length - 1)) > 0) Then
              If CStr(Temp_Param1).Length = 2 Then
                Temp_Param1 = ""
              Else
                Temp_Param1 = CStr(Temp_Param1).Substring(1, CStr(Temp_Param1).Length - 2)
              End If
            End If
          End If

          Param1_IsString = True
        End If

        If IsNumeric(ParseArray(0, Counter + 1)) Then
          Temp_Param2 = CDec(ParseArray(0, Counter + 1))
        ElseIf IsDate(ParseArray(0, Counter + 1)) Then
          Temp_Param2 = CDate(ParseArray(0, Counter + 1)).ToOADate
        Else
          Temp_Param2 = ParseArray(0, Counter + 1)
          Param2_IsString = True

          If CStr(Temp_Param2).Length >= 2 Then
            If (InStr("'" & Chr(34), CStr(Temp_Param2).Substring(0, 1)) > 0) And (InStr("'" & Chr(34), CStr(Temp_Param2).Substring(CStr(Temp_Param2).Length - 1)) > 0) Then
              If CStr(Temp_Param2).Length = 2 Then
                Temp_Param2 = ""
              Else
                Temp_Param2 = CStr(Temp_Param2).Substring(1, CStr(Temp_Param2).Length - 2)
              End If
            End If
          End If

        End If

        If (Param1_IsString = Param2_IsString) And (Param1_IsString = False) Then
          Try
            If (OperatorString = "=") Or (OperatorString = "==") Then
              ParseArray(0, Counter) = (CDec(Temp_Param1) = CDec(Temp_Param2))
            ElseIf OperatorString = "<" Then
              ParseArray(0, Counter) = (CDec(Temp_Param1) < CDec(Temp_Param2))
            ElseIf OperatorString = ">" Then
              ParseArray(0, Counter) = (CDec(Temp_Param1) > CDec(Temp_Param2))
            ElseIf OperatorString = "<=" Then
              ParseArray(0, Counter) = (CDec(Temp_Param1) <= CDec(Temp_Param2))
            ElseIf OperatorString = ">=" Then
              ParseArray(0, Counter) = (CDec(Temp_Param1) >= CDec(Temp_Param2))
            ElseIf (OperatorString = "<>") Or (OperatorString = "!=") Then
              ParseArray(0, Counter) = (CDec(Temp_Param1) <> CDec(Temp_Param2))
            Else
              ' Error
              ResolvedValue = "{Unknown Operator `" & OperatorString & "`}"
              GoTo ResolveToken_Exit
            End If
          Catch ex As Exception
            ResolvedValue = "{Error evaluating Operator " & OperatorString & "}"
            GoTo ResolveToken_Exit
          End Try
        Else
          Try
            If (OperatorString = "=") Or (OperatorString = "==") Then
              ParseArray(0, Counter) = (CStr(Temp_Param1) = CStr(Temp_Param2))
            ElseIf OperatorString = "<" Then
              ParseArray(0, Counter) = (CStr(Temp_Param1) < CStr(Temp_Param2))
            ElseIf OperatorString = ">" Then
              ParseArray(0, Counter) = (CStr(Temp_Param1) > CStr(Temp_Param2))
            ElseIf OperatorString = "<=" Then
              ParseArray(0, Counter) = (CStr(Temp_Param1) <= CStr(Temp_Param2))
            ElseIf OperatorString = ">=" Then
              ParseArray(0, Counter) = (CStr(Temp_Param1) >= CStr(Temp_Param2))
            ElseIf (OperatorString = "<>") Or (OperatorString = "!=") Then
              ParseArray(0, Counter) = (CStr(Temp_Param1) <> CStr(Temp_Param2))
            Else
              ' Error
              ResolvedValue = "{Unknown Operator `" & OperatorString & "`}"
              GoTo ResolveToken_Exit
            End If
          Catch ex As Exception
            ResolvedValue = "{Error evaluating Operator " & OperatorString & "}"
            GoTo ResolveToken_Exit
          End Try
        End If

        ' Move rest up
        For MoveCounter = Counter To (OperandCount - 1)
          ParseArray(0, MoveCounter + 1) = ParseArray(0, MoveCounter + 2)
          ParseArray(1, MoveCounter) = ParseArray(1, MoveCounter + 1)
        Next MoveCounter
        ParseArray(0, UBound(ParseArray, 2)) = ""
        ParseArray(1, UBound(ParseArray, 2)) = ""

        OperatorString = CStr(ParseArray(1, Counter))
      End While
    Next Counter

    If Len(ParseArray(1, 1)) > 0 Then
      ' Unresolved Operator :
      ResolvedValue = "{Unresolved Operator " & CStr(ParseArray(1, 1)) & "}"
      GoTo ResolveToken_Exit
    End If

    ResolvedValue = ParseArray(0, 1)

ResolveToken_Exit:

    ResolveToken = ResolvedValue
    Exit Function

  End Function

  ''' <summary>
  ''' Resolves the function.
  ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  ''' <param name="Permission_Area">The permission_ area.</param>
  ''' <param name="p_Stack">The p_ stack.</param>
  ''' <param name="ValueString">The value string.</param>
  ''' <returns>System.Object.</returns>
  Public Function ResolveFunction(ByRef pMainForm As VeniceMain, ByVal Permission_Area As String, ByVal p_Stack As String, ByVal ValueString As String) As Object
    '**************************************************************************************************
    ' Private function returning Variant
    '
    ' Purpose:  Given a function String, Resolve it
    '
    ' Accepts:  Permission_Area as String       : Location of Error (for debugging and logging purposes)
    '           p_Stack As String               : Stack used to detect circular references.
    '           ValueString As String           : Function string.
    '
    ' Returns: Resolved value of the function.
    '
    ' This process works by isolating the parameters to the function, resolving them via a calls
    ' to the 'ResolveToken()' function and then processing them according to the given function name.
    '**************************************************************************************************
    Dim ReturnValue As Object

    Dim FunctionName As String
    Dim FunctionArguments() As Object = Nothing
    Dim ParameterList As String
    Dim ParameterCount As Integer

    Dim Counter As Integer
    Dim CurrentChar As Integer
    Dim Param1 As Object
    Dim Param2 As Object
    Dim ThisParam As Object
    Dim Holder As Object
    Dim Holder_IsString As Boolean
    Dim FirstParamIndex As Integer
    Dim Modifier As Integer

    ' Split out the Function Name and the Parameter string.

    FunctionName = ValueString.Substring(0, InStr(ValueString, "(") - 1).ToUpper
    ParameterList = ValueString.Substring(InStr(ValueString, "(")).Trim
    If ParameterList.EndsWith(")") Then
      ParameterList = ParameterList.Substring(0, ParameterList.Length - 1)
    End If

    ' Split out the individual parameters and save them to the 'FunctionArguments' array.

    ParameterCount = 0
    If ParameterList.Length > 0 Then
      ReDim FunctionArguments(10)
      For Counter = 1 To 10
        FunctionArguments(Counter) = ""
      Next Counter

      ' Seperate out the parameters
      While ParameterList.Length > 0
        CurrentChar = 1
        While (CurrentChar <= ParameterList.Length) AndAlso (ParameterList.Substring(CurrentChar - 1, 1) <> ",")

          ' Skip Through Quoted sections.
          If ParameterList.Substring(CurrentChar - 1, 1) = Chr(34) Then
            CurrentChar += 1
            While ParameterList.Substring(CurrentChar - 1, 1) <> Chr(34)
              CurrentChar += 1
              If CurrentChar > ParameterList.Length Then
                ' Unmatched Quote
                ReturnValue = "{Unmatched quote : " & Chr(34) & "}"
                GoTo ResolveFunction_Exit
              End If
            End While
          ElseIf ParameterList.Substring(CurrentChar - 1, 1) = "'" Then
            CurrentChar += 1
            While ParameterList.Substring(CurrentChar - 1, 1) <> "'"
              CurrentChar += 1
              If CurrentChar > ParameterList.Length Then
                ' Unmatched Quote
                ReturnValue = "{Unmatched quote : '}"
                GoTo ResolveFunction_Exit
              End If
            End While
          ElseIf ParameterList.Substring(CurrentChar - 1, 1) = "(" Then
            CurrentChar += 1
            While ParameterList.Substring(CurrentChar - 1, 1) <> ")"
              If ParameterList.Substring(CurrentChar - 1, 1) = Chr(34) Then
                CurrentChar += 1
                While ParameterList.Substring(CurrentChar - 1, 1) <> Chr(34)
                  CurrentChar += 1
                  If CurrentChar > ParameterList.Length Then
                    ' Unmatched Quote
                    ReturnValue = "{Unmatched quote : " & Chr(34) & "}"
                    GoTo ResolveFunction_Exit
                  End If
                End While
              ElseIf ParameterList.Substring(CurrentChar - 1, 1) = "'" Then
                CurrentChar += 1
                While ParameterList.Substring(CurrentChar - 1, 1) <> "'"
                  CurrentChar += 1
                  If CurrentChar > ParameterList.Length Then
                    ' Unmatched Quote
                    ReturnValue = "{Unmatched quote : '}"
                    GoTo ResolveFunction_Exit
                  End If
                End While
              End If

              CurrentChar += 1

              If CurrentChar > ParameterList.Length Then
                ' Unmatched Quote
                ReturnValue = "{Unmatched bracket : `)`}"
                GoTo ResolveFunction_Exit
              End If
            End While
          End If

          CurrentChar += 1
        End While

        ParameterCount += 1

        ' Expand Parameter list if necessary
        Try
          If ParameterCount > UBound(FunctionArguments) Then
            ReDim Preserve FunctionArguments(UBound(FunctionArguments) + 10)
          End If
        Catch ex As Exception
          Call pMainForm.LogError(Permission_Area & ", ReturnValue()", LOG_LEVELS.Error, ex.Message, "Error extending Parameter array", ex.StackTrace, False)
          ReturnValue = "{Error in Function " & FunctionName & "}"
          GoTo ResolveFunction_Exit
        End Try

        FunctionArguments(ParameterCount) = ParameterList.Substring(0, (CurrentChar - 1))
        If (CurrentChar < ParameterList.Length) Then
          ParameterList = ParameterList.Substring(CurrentChar)
        Else
          ParameterList = ""
        End If

      End While

    End If

    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    ' Resolve Parameters down to single values.
    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    If ParameterCount > 0 Then
      For Counter = LBound(FunctionArguments) To UBound(FunctionArguments)
        If (Not (FunctionArguments(Counter) Is Nothing)) AndAlso CStr(FunctionArguments(Counter)).Length > 0 Then
          FunctionArguments(Counter) = ResolveToken(pMainForm, Permission_Area, p_Stack, FunctionArguments(Counter))

          If CStr(FunctionArguments(Counter)).StartsWith("{") Then
            ' Error returned from parameter resolution.
            ReturnValue = FunctionArguments(Counter)
            GoTo ResolveFunction_Exit
          End If

          ' Strip Quotes
          Param1 = FunctionArguments(Counter)

          If CStr(Param1).Length >= 2 Then
            If (InStr("'" & Chr(34), CStr(Param1).Substring(0, 1)) > 0) And (InStr("'" & Chr(34), CStr(Param1).Substring(CStr(Param1).Length - 1)) > 0) Then
              If CStr(Param1).Length = 2 Then
                Param1 = ""
              Else
                Param1 = CStr(Param1).Substring(1, CStr(Param1).Length - 2)
              End If
              FunctionArguments(Counter) = Param1
            End If
          End If

        End If
      Next Counter

    End If
    FirstParamIndex = 1

    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    ' Resolve Function
    ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    Select Case FunctionName

      Case "NOW"
        ReturnValue = CDbl(Now().ToOADate)
        GoTo ResolveFunction_Exit

      Case "INT", "CINT"
        If (ParameterCount <> 1) Then
          ReturnValue = "{Incorrect number of parameters : " & FunctionName & "}"
          GoTo ResolveFunction_Exit
        End If

        Param1 = FunctionArguments(FirstParamIndex)

        If IsNumeric(Param1) Then
          ReturnValue = CLng(Param1)
          GoTo ResolveFunction_Exit
        ElseIf IsDate(Param1) Then
          ReturnValue = Int(CDate(Param1).ToOADate)
          GoTo ResolveFunction_Exit
        Else
          ReturnValue = "{Type mismatch in function " & FunctionName & "}"
          GoTo ResolveFunction_Exit
        End If

      Case "CDBL"
        If (ParameterCount <> 1) Then
          ReturnValue = "{Incorrect number of parameters : " & FunctionName & "}"
          GoTo ResolveFunction_Exit
        End If

        Param1 = FunctionArguments(FirstParamIndex)
        If IsNumeric(Param1) Then
          ReturnValue = CDbl(Param1)
          GoTo ResolveFunction_Exit
        ElseIf IsDate(Param1) Then
          ReturnValue = CDbl(CDate(Param1).ToOADate)
          GoTo ResolveFunction_Exit
        Else
          ReturnValue = "{Type mismatch in function " & FunctionName & "}"
          GoTo ResolveFunction_Exit
        End If

      Case "CSTR"
        If (ParameterCount <> 1) Then
          ReturnValue = "{Incorrect number of parameters : " & FunctionName & "}"
          GoTo ResolveFunction_Exit
        End If

        Param1 = FunctionArguments(FirstParamIndex)

        If IsDate(Param1) Then
          ReturnValue = Chr(34) & CDate(Param1).ToOADate.ToString & Chr(34)
        Else
          ReturnValue = Chr(34) & CStr(Param1) & Chr(34)
        End If

        GoTo ResolveFunction_Exit

      Case "CDATE"
        If (ParameterCount <> 1) Then
          ReturnValue = "{Incorrect number of parameters : " & FunctionName & "}"
          GoTo ResolveFunction_Exit
        End If

        Param1 = FunctionArguments(FirstParamIndex)

        If IsDate(Param1) Then
          ReturnValue = CDate(Param1).ToOADate
          GoTo ResolveFunction_Exit
        ElseIf IsNumeric(Param1) Then
          ReturnValue = CDbl(Param1)
          GoTo ResolveFunction_Exit
        Else
          ReturnValue = "{Type mismatch in function " & FunctionName & "}"
          GoTo ResolveFunction_Exit
        End If

      Case "ROUND"
        If ParameterCount <> 2 Then
          ReturnValue = "{Wrong number of arguments, ROUND Expected 2, have " & ParameterCount & "}"
          GoTo ResolveFunction_Exit
        End If
        Param1 = FunctionArguments(FirstParamIndex)
        Param2 = FunctionArguments(FirstParamIndex + 1)

        If (IsNumeric(Param1) = False) Or (IsNumeric(Param2) = False) Then
          ReturnValue = "{Type mismatch in function " & FunctionName & "}"
          GoTo ResolveFunction_Exit
        End If
        If CInt(Param2) < 0 Then Param2 = 0

        ReturnValue = Math.Round(CDbl(Param1), CInt(Param2))
        GoTo ResolveFunction_Exit

      Case "IIF", "IF"
        If ParameterCount <> 3 Then
          ReturnValue = "{Wrong number of arguments, IIF Expected 3, have " & ParameterCount & "}"
          GoTo ResolveFunction_Exit
        End If

        Param1 = FunctionArguments(FirstParamIndex)
        If CBool(Param1) = True Then
          ReturnValue = FunctionArguments(FirstParamIndex + 1)
        Else
          ReturnValue = FunctionArguments(FirstParamIndex + 2)
          GoTo ResolveFunction_Exit
        End If

      Case "MAX"
        If (ParameterCount < 1) Then
          ReturnValue = 0
          GoTo ResolveFunction_Exit
        End If

        Holder = FunctionArguments(LBound(FunctionArguments))
        If (IsNumeric(Holder) = True) Then
          Holder_IsString = False
          Holder = CDec(Holder)
        ElseIf (IsDate(Holder) = True) Then
          Holder_IsString = False
          Holder = CDate(Holder)
        Else
          Holder_IsString = True

          If CStr(Holder).Length >= 2 Then
            If (InStr("'" & Chr(34), CStr(Holder).Substring(0, 1)) > 0) And (InStr("'" & Chr(34), CStr(Holder).Substring(CStr(Holder).Length - 1)) > 0) Then
              If CStr(Holder).Length = 2 Then
                Holder = ""
              Else
                Holder = CStr(Holder).Substring(1, CStr(Holder).Length - 2)
              End If
            End If
          End If

        End If

        For Counter = (LBound(FunctionArguments) + 1) To (LBound(FunctionArguments) + ParameterCount - 1)
          ThisParam = FunctionArguments(Counter)

          If (IsDate(ThisParam) = False) And (IsNumeric(ThisParam) = False) Then
            ' String
            If Holder_IsString = False Then
              ReturnValue = "{Type mismatch in function " & FunctionName & "}"
              GoTo ResolveFunction_Exit
            Else

              If CStr(ThisParam).Length >= 2 Then
                If (InStr("'" & Chr(34), CStr(ThisParam).Substring(0, 1)) > 0) And (InStr("'" & Chr(34), CStr(ThisParam).Substring(CStr(ThisParam).Length - 1)) > 0) Then
                  If CStr(ThisParam).Length = 2 Then
                    ThisParam = ""
                  Else
                    ThisParam = CStr(ThisParam).Substring(1, CStr(ThisParam).Length - 2)
                  End If
                End If
              End If

              If CStr(ThisParam) > CStr(Holder) Then
                Holder = ThisParam
              End If
            End If
          Else
            If Holder_IsString = True Then
              ReturnValue = "{Type mismatch in function " & FunctionName & "}"
              GoTo ResolveFunction_Exit
            ElseIf (IsDate(ThisParam) And IsDate(Holder)) Then
              If CDate(ThisParam).ToOADate > CDate(Holder).ToOADate Then
                Holder = CDate(ThisParam)
              End If
            ElseIf (IsNumeric(ThisParam) And IsNumeric(Holder)) Then
              If CDec(ThisParam) > CDec(Holder) Then
                Holder = CDec(ThisParam)
              End If
            Else
              ReturnValue = "{Type mismatch in function " & FunctionName & "}"
              GoTo ResolveFunction_Exit
            End If
          End If
        Next Counter

        ReturnValue = Holder
        GoTo ResolveFunction_Exit

      Case "MIN"
        If (ParameterCount < 1) Then
          ReturnValue = 0
          GoTo ResolveFunction_Exit
        End If

        Holder = FunctionArguments(LBound(FunctionArguments))
        If (IsNumeric(Holder) = True) Then
          Holder_IsString = False
          Holder = CDec(Holder)
        ElseIf (IsDate(Holder) = True) Then
          Holder_IsString = False
          Holder = CDate(Holder)
        Else
          Holder_IsString = True

          If CStr(Holder).Length >= 2 Then
            If (InStr("'" & Chr(34), CStr(Holder).Substring(0, 1)) > 0) And (InStr("'" & Chr(34), CStr(Holder).Substring(CStr(Holder).Length - 1)) > 0) Then
              If CStr(Holder).Length = 2 Then
                Holder = ""
              Else
                Holder = CStr(Holder).Substring(1, CStr(Holder).Length - 2)
              End If
            End If
          End If

        End If

        For Counter = (LBound(FunctionArguments) + 1) To (LBound(FunctionArguments) + ParameterCount - 1)
          ThisParam = FunctionArguments(Counter)

          If (IsDate(ThisParam) = False) And (IsNumeric(ThisParam) = False) Then
            ' String
            If Holder_IsString = False Then
              ReturnValue = "{Type mismatch in function " & FunctionName & "}"
              GoTo ResolveFunction_Exit
            Else

              If CStr(ThisParam).Length >= 2 Then
                If (InStr("'" & Chr(34), CStr(ThisParam).Substring(0, 1)) > 0) And (InStr("'" & Chr(34), CStr(ThisParam).Substring(CStr(ThisParam).Length - 1)) > 0) Then
                  If CStr(ThisParam).Length = 2 Then
                    ThisParam = ""
                  Else
                    ThisParam = CStr(ThisParam).Substring(1, CStr(ThisParam).Length - 2)
                  End If
                End If
              End If

              If CStr(ThisParam) < CStr(Holder) Then
                Holder = ThisParam
              End If
            End If
          Else
            If Holder_IsString = True Then
              ReturnValue = "{Type mismatch in function " & FunctionName & "}"
              GoTo ResolveFunction_Exit
            ElseIf (IsDate(ThisParam) And IsDate(Holder)) Then
              If CDate(ThisParam).ToOADate < CDate(Holder).ToOADate Then
                Holder = CDate(ThisParam)
              End If
            ElseIf (IsNumeric(ThisParam) And IsNumeric(Holder)) Then
              If CDec(ThisParam) < CDec(Holder) Then
                Holder = CDec(ThisParam)
              End If
            Else
              ReturnValue = "{Type mismatch in function " & FunctionName & "}"
              GoTo ResolveFunction_Exit
            End If
          End If
        Next Counter

        ReturnValue = Holder
        GoTo ResolveFunction_Exit

      Case "LOG"
        If (ParameterCount <> 1) Then
          ReturnValue = "{Wrong number of arguments, IIF Expected 3, have " & ParameterCount & "}"
          GoTo ResolveFunction_Exit
        End If

        Param1 = FunctionArguments(FirstParamIndex)
        If IsNumeric(Param1) Then
          Param1 = CDbl(Param1)
          If (CDbl(Param1) > 0) Then
            ReturnValue = Math.Log10(CDbl(Param1))
          Else
            ReturnValue = "{Invalid parameter value for LOG10}"
          End If
          GoTo ResolveFunction_Exit

        ElseIf IsDate(Param1) Then

          Param1 = CDbl(CDate(Param1).ToOADate)
          If (CDbl(Param1) > 0) Then
            ReturnValue = Math.Log10(CDbl(Param1))
          Else
            ReturnValue = "{Invalid parameter value for LOG10}"
          End If
          GoTo ResolveFunction_Exit

        Else

          ReturnValue = "{Type mismatch in function " & FunctionName & "}"
          GoTo ResolveFunction_Exit
        End If

      Case "LN"
        If (ParameterCount <> 1) Then
          ReturnValue = "{Wrong number of arguments, IIF Expected 3, have " & ParameterCount & "}"
          GoTo ResolveFunction_Exit
        End If

        Param1 = FunctionArguments(FirstParamIndex)
        If IsNumeric(Param1) Then
          Param1 = CDbl(Param1)
          If (CDbl(Param1) > 0) Then
            ReturnValue = Math.Log(CDbl(Param1))
          Else
            ReturnValue = "{Invalid parameter value for LN}"
          End If
          GoTo ResolveFunction_Exit
        ElseIf IsDate(Param1) Then
          Param1 = CDbl(CDate(Param1).ToOADate)
          If (CDbl(Param1) > 0) Then
            ReturnValue = Math.Log(CDbl(Param1))
          Else
            ReturnValue = "{Invalid parameter value for LN}"
          End If
          GoTo ResolveFunction_Exit
        Else
          ReturnValue = "{Type mismatch in function " & FunctionName & "}"
          GoTo ResolveFunction_Exit
        End If

      Case "EXP"
        If (ParameterCount <> 1) Then
          ReturnValue = "{Wrong number of arguments, EXP Expected 1, has " & ParameterCount & "}"
          GoTo ResolveFunction_Exit
        End If

        Param1 = FunctionArguments(FirstParamIndex)

        If (IsNumeric(Param1) = False) Then
          ReturnValue = "{Type mismatch in function " & FunctionName & "}"
          GoTo ResolveFunction_Exit
        End If

        ReturnValue = Math.Exp(CDbl(Param1))
        GoTo ResolveFunction_Exit

      Case "SQLSELECT"
        If (ParameterCount <> 1) Then
          ReturnValue = "{Wrong number of arguments, SQLSELECT Expected 1, has " & ParameterCount & "}"
          GoTo ResolveFunction_Exit
        End If

        Param1 = CStr(FunctionArguments(FirstParamIndex)).Trim

        If CStr(Param1).Length >= 2 Then
          If (InStr("'" & Chr(34), CStr(Param1).Substring(0, 1)) > 0) And (InStr("'" & Chr(34), CStr(Param1).Substring(CStr(Param1).Length - 1)) > 0) Then
            If CStr(Param1).Length = 2 Then
              Param1 = ""
            Else
              Param1 = CStr(Param1).Substring(1, CStr(Param1).Length - 2)
            End If
          End If
        End If

        If (Not (CStr(Param1).ToUpper.StartsWith("SELECT "))) Then
          ReturnValue = "{Error SQLSELECT parameter does not begin `SELECT `}"
          GoTo ResolveFunction_Exit
        Else
          ReturnValue = ResolveToken(pMainForm, Permission_Area, p_Stack, Param1)
        End If

      Case "FORMAT"
        If (ParameterCount <> 2) Then
          ReturnValue = "{Wrong number of arguments, FORMAT Expected 2, has " & ParameterCount & "}"
          GoTo ResolveFunction_Exit
        End If

        Param1 = FunctionArguments(FirstParamIndex)
        Param2 = FunctionArguments(FirstParamIndex + 1)

        If CStr(Param1).Length >= 2 Then
          If (InStr("'" & Chr(34), CStr(Param1).Substring(0, 1)) > 0) And (InStr("'" & Chr(34), CStr(Param1).Substring(CStr(Param1).Length - 1)) > 0) Then
            If CStr(Param1).Length = 2 Then
              Param1 = ""
            Else
              Param1 = CStr(Param1).Substring(1, CStr(Param1).Length - 2)
            End If
          End If
        End If

        If CStr(Param2).Length >= 2 Then
          If (InStr("'" & Chr(34), CStr(Param2).Substring(0, 1)) > 0) And (InStr("'" & Chr(34), CStr(Param2).Substring(CStr(Param2).Length - 1)) > 0) Then
            If CStr(Param2).Length = 2 Then
              Param2 = ""
            Else
              Param2 = CStr(Param2).Substring(1, CStr(Param2).Length - 2)
            End If
          End If
        End If

        If IsNumeric(Param1) Then
          Param1 = CDbl(Param1)
        ElseIf IsDate(Param1) Then
          Param1 = CDate(Param1)
        End If

        Try
          ReturnValue = Format(Param1, CStr(Param2))
        Catch ex As Exception
          ReturnValue = Param1
        End Try

        If IsNumeric(ReturnValue) = False Then
          ReturnValue = Chr(34) & CStr(ReturnValue) & Chr(34)
        End If

      Case "GETTICKET"
        If (ParameterCount < 1) Or (ParameterCount > 2) Then
          ReturnValue = "{Wrong number of arguments, GETTICKET Expected 1 or 2, has " & ParameterCount & "}"
          GoTo ResolveFunction_Exit
        End If

        Modifier = 0
        Param1 = CStr(FunctionArguments(FirstParamIndex)).Trim

        If (ParameterCount = 2) Then
          If IsNumeric(FunctionArguments(FirstParamIndex + 1)) Then
            Modifier = CInt(FunctionArguments(FirstParamIndex + 1))
          End If
        End If

        If CStr(Param1).Length >= 2 Then
          If (InStr("'" & Chr(34), CStr(Param1).Substring(0, 1)) > 0) And (InStr("'" & Chr(34), CStr(Param1).Substring(CStr(Param1).Length - 1)) > 0) Then
            If CStr(Param1).Length = 2 Then
              Param1 = ""
            Else
              Param1 = CStr(Param1).Substring(1, CStr(Param1).Length - 2)
            End If
          End If
        End If

        Dim ReferenceCommand As New SqlCommand

        Try
          ReferenceCommand.Parameters.Clear()
          ReferenceCommand.Connection = pMainForm.GetVeniceConnection
          ReferenceCommand.CommandType = System.Data.CommandType.Text
          ReferenceCommand.CommandTimeout = TRANSACTION_SQLCOMMAND_TIMEOUT
          ReferenceCommand.CommandText = "SELECT dbo.fn_NewTransactionTicket(@Header, @Modifier)"

          ReferenceCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Header", System.Data.SqlDbType.VarChar, 50, ParameterDirection.Input, False, CByte(0), CByte(0), Nothing, DataRowVersion.Current, CStr(Param1)))
          ReferenceCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Modifier", System.Data.SqlDbType.Int, 4, ParameterDirection.Input, False, CByte(0), CByte(0), Nothing, DataRowVersion.Current, Modifier))

          SyncLock ReferenceCommand.Connection
            ReturnValue = ReferenceCommand.ExecuteScalar
          End SyncLock

          If ReturnValue Is Nothing Then
            ReturnValue = "ERROR"
          End If

        Catch ex As Exception
          ReturnValue = "ERROR"

        Finally
          Try
            If (Not (ReferenceCommand.Connection Is Nothing)) Then
              ReferenceCommand.Connection.Close()
            End If
          Catch ex As Exception
          End Try
          ReferenceCommand = Nothing
        End Try

      Case Else

        ReturnValue = "{Unknown Function : " & FunctionName & "}"
        GoTo ResolveFunction_Exit

    End Select

    ' Wrap Strings in Quotes.
    If (IsNumeric(ReturnValue) = False) Then
      If (IsDate(ReturnValue) = True) Then
        ReturnValue = CDate(ReturnValue).ToOADate
      Else
        If (Not (CStr(ReturnValue).StartsWith("{"))) Then

          If CStr(ReturnValue).Length >= 2 Then
            If (InStr("'" & Chr(34), CStr(ReturnValue).Substring(0, 1)) > 0) And (InStr("'" & Chr(34), CStr(ReturnValue).Substring(CStr(ReturnValue).Length - 1)) > 0) Then
              If CStr(ReturnValue).Length = 2 Then
                ReturnValue = ""
              Else
                ReturnValue = CStr(ReturnValue).Substring(1, CStr(ReturnValue).Length - 2)
              End If
            End If
          End If

          ReturnValue = Chr(34) & CStr(ReturnValue) & Chr(34)
        End If
      End If
    End If

ResolveFunction_Exit:

    Return ReturnValue
    Exit Function

  End Function

  ''' <summary>
  ''' Set_s the C t_ parameter.
  ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  ''' <param name="Permission_Area">The permission_ area.</param>
  ''' <param name="p_FieldName">Name of the p_ field.</param>
  ''' <param name="p_FieldType">Type of the p_ field.</param>
  ''' <param name="p_FieldValue">The p_ field value.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	Private Function Set_CT_Parameter(ByRef pMainForm As VeniceMain, ByVal Permission_Area As String, ByVal p_FieldName As String, ByVal p_FieldType As Integer, ByVal p_FieldValue As Object) As Boolean
		'**************************************************************************************************
		' Private function returning variant
		'
		' Purpose:  Set a value in the Transaction Parameter Array
		'
		' Accepts:  p_Fieldname as String      : Parameter Name
		'           p_FieldType as integer        : Parameter Type
		'           p_FieldValue as Variant    : Parameter Value
		'
		' Returns:  Boolean - True for Success, False for Error or failure.
		'**************************************************************************************************
		Dim Counter As Integer

		Try

			For Counter = LBound(CT_Parameters) To UBound(CT_Parameters)
				If (CT_Parameters(Counter) Is Nothing) OrElse (CT_Parameters(Counter).FieldName = p_FieldName) Then
					CT_Parameters(Counter) = New CT_Field(p_FieldName, p_FieldValue, p_FieldType)
				End If

			Next Counter

			ReDim Preserve CT_Parameters(UBound(CT_Parameters) + 5)

			Counter = UBound(CT_Parameters) - 4
			CT_Parameters(Counter) = New CT_Field(p_FieldName, p_FieldValue, p_FieldType)

			Return True
			Exit Function

		Catch ex As Exception
			Call pMainForm.LogError(Permission_Area & ", Set_CT_Parameter()", LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
		End Try

		Return False
		Exit Function

	End Function

    ''' <summary>
    ''' Get_s the C t_ parameter value.
    ''' </summary>
    ''' <param name="p_FieldName">Name of the p_ field.</param>
    ''' <returns>System.Object.</returns>
	Private Function Get_CT_ParameterValue(ByVal p_FieldName As String) As Object
		'**************************************************************************************************
		' Private function returning variant
		'
		' Purpose:  Given a Parameter name, Return the Parameter Value
		'
		' Accepts:  p_Fieldname as String      : Parameter name
		'
		' Returns:  Parameter value, ("") if no matching field is found.
		'**************************************************************************************************

		Dim Counter As Integer
		Dim ReturnValue As Object

		On Error Resume Next

		For Counter = LBound(CT_Parameters) To UBound(CT_Parameters)
			If CT_Parameters(Counter).FieldName = p_FieldName Then
				ReturnValue = CT_Parameters(Counter).FieldValue
				GoTo Get_CT_ParameterValue_Exit
			End If
		Next Counter

		ReturnValue = ("")

Get_CT_ParameterValue_Exit:

		' Present the parameter according to it's apparent type.

		If IsNumeric(ReturnValue) Then
			ReturnValue = CDec(ReturnValue)
		ElseIf IsDate(ReturnValue) Then
			ReturnValue = CDate(ReturnValue).ToOADate
		ElseIf Not (ReturnValue.GetType Is GetType(Boolean)) Then

			If CStr(ReturnValue).Length >= 2 Then
				If (InStr("'" & Chr(34), CStr(ReturnValue).Substring(0, 1)) > 0) And (InStr("'" & Chr(34), CStr(ReturnValue).Substring(CStr(ReturnValue).Length - 1)) > 0) Then
					If CStr(ReturnValue).Length = 2 Then
						ReturnValue = ""
					Else
						ReturnValue = CStr(ReturnValue).Substring(1, CStr(ReturnValue).Length - 2)
					End If
				End If
			End If

			ReturnValue = Chr(34) & CStr(ReturnValue) & Chr(34)
		End If

		Return ReturnValue

		On Error GoTo 0
	End Function

    ''' <summary>
    ''' Get_s the type of the C t_ parameter.
    ''' </summary>
    ''' <param name="p_FieldName">Name of the p_ field.</param>
    ''' <returns>System.Int32.</returns>
	Private Function Get_CT_ParameterType(ByVal p_FieldName As String) As Integer
		'**************************************************************************************************
		' Private function returning variant
		'
		' Purpose:  Given a Parameter name, Return the Parameter type
		'
		' Accepts:  p_Fieldname as String      : Parameter name
		'
		' Returns:  Parameter type, (-1) if no matching field is found.
		'**************************************************************************************************

		Dim Counter As Integer

		On Error Resume Next

		For Counter = LBound(CT_Parameters) To UBound(CT_Parameters)
			If CT_Parameters(Counter).FieldName = p_FieldName Then
				Return CT_Parameters(Counter).FieldType
				On Error GoTo 0
				Exit Function
			End If
		Next Counter

		Return (-1)

		On Error GoTo 0

	End Function

    ''' <summary>
    ''' Get_s the C t_ terms field value.
    ''' </summary>
    ''' <param name="p_FieldName">Name of the p_ field.</param>
    ''' <returns>System.Object.</returns>
	Private Function Get_CT_TermsFieldValue(ByVal p_FieldName As String) As Object
		'**************************************************************************************************
		' Private function returning variant
		'
		' Purpose:  Given a Terms name, Return the Terms Value
		'
		' Accepts:  p_Fieldname as String      : Terms name
		'
		' Returns:  Terms value, ("") if no matching field is found.
		'**************************************************************************************************

		Dim Counter As Integer
		Dim ReturnValue As Object

		On Error Resume Next

		For Counter = LBound(CT_Terms_Array) To UBound(CT_Terms_Array)
			If CT_Terms_Array(Counter).FieldName = p_FieldName Then
				ReturnValue = CT_Terms_Array(Counter).FieldValue
				GoTo Get_CT_TermsFieldValue_Exit
			End If
		Next Counter

		ReturnValue = ("")

Get_CT_TermsFieldValue_Exit:

		' Present the Terms according to it's apparent type.

		If IsNumeric(ReturnValue) Then
			ReturnValue = CDec(ReturnValue)
		ElseIf IsDate(ReturnValue) Then
			ReturnValue = CDate(ReturnValue).ToOADate
		ElseIf Not (ReturnValue.GetType Is GetType(Boolean)) Then

			If CStr(ReturnValue).Length >= 2 Then
				If (InStr("'" & Chr(34), CStr(ReturnValue).Substring(0, 1)) > 0) And (InStr("'" & Chr(34), CStr(ReturnValue).Substring(CStr(ReturnValue).Length - 1)) > 0) Then
					If CStr(ReturnValue).Length = 2 Then
						ReturnValue = ""
					Else
						ReturnValue = CStr(ReturnValue).Substring(1, CStr(ReturnValue).Length - 2)
					End If
				End If
			End If

			ReturnValue = Chr(34) & CStr(ReturnValue) & Chr(34)
		End If

		Return ReturnValue

		On Error GoTo 0
	End Function

    ''' <summary>
    ''' Get_s the type of the C t_ terms field.
    ''' </summary>
    ''' <param name="p_FieldName">Name of the p_ field.</param>
    ''' <returns>System.Int32.</returns>
	Private Function Get_CT_TermsFieldType(ByVal p_FieldName As String) As Integer
		'**************************************************************************************************
		' Private function returning variant
		'
		' Purpose:  Given a Terms name, Return the Terms type
		'
		' Accepts:  p_Fieldname as String      : Terms name
		'
		' Returns:  Terms type, (-1) if no matching field is found.
		'**************************************************************************************************

		Dim Counter As Integer

		On Error Resume Next

		For Counter = LBound(CT_Terms_Array) To UBound(CT_Terms_Array)
			If CT_Terms_Array(Counter).FieldName = p_FieldName Then
				Return CT_Terms_Array(Counter).FieldType
				On Error GoTo 0
				Exit Function
			End If
		Next Counter

		Return (-1)

		On Error GoTo 0

	End Function

    ''' <summary>
    ''' Get_s the C t_ terms field column.
    ''' </summary>
    ''' <param name="p_FieldName">Name of the p_ field.</param>
    ''' <returns>System.Int32.</returns>
	Private Function Get_CT_TermsFieldColumn(ByVal p_FieldName As String) As Integer
		'**************************************************************************************************
		' Private function returning variant
		'
		' Purpose:  Given a Terms name, Return the Terms Column
		'
		' Accepts:  p_Fieldname as String      : Terms name
		'
		' Returns:  Terms type, (-1) if no matching field is found.
		'**************************************************************************************************

		Dim Counter As Integer

		On Error Resume Next

		For Counter = LBound(CT_Terms_Array) To UBound(CT_Terms_Array)
			If CT_Terms_Array(Counter).FieldName = p_FieldName Then
				Return Counter
				On Error GoTo 0
				Exit Function
			End If
		Next Counter

		Return (-1)

		On Error GoTo 0

	End Function

    ''' <summary>
    ''' Get_s the template_ field column.
    ''' </summary>
    ''' <param name="p_FieldName">Name of the p_ field.</param>
    ''' <returns>System.Int32.</returns>
	Private Function Get_Template_FieldColumn(ByVal p_FieldName As String) As Integer
		'**************************************************************************************************
		' Private function returning variant
		'
		' Purpose:  Given a Template Field name, Return the Template Field Column
		'
		' Accepts:  p_Fieldname as String      : Template Field name
		'
		' Returns:  Template Field Column, (-1) if no matching field is found.
		'**************************************************************************************************

		Dim Counter As Integer

		Try
			p_FieldName = p_FieldName.ToUpper

			For Counter = 0 To (CT_Template_Table.Columns.Count - 1)

				If CT_Template_Table.Columns(Counter).ColumnName.ToUpper = p_FieldName Then
					Return Counter
					Exit Function
				End If
			Next Counter

		Catch ex As Exception
		End Try

		Return (0)


	End Function

    ''' <summary>
    ''' Called when [row updating].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
	Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
		Dim a As Integer

		If Not e.Command Is Nothing Then
			For a = 0 To (e.Command.Parameters.Count - 1)
				If (e.Command.Parameters(a).Value Is Nothing) Then
					Debug.WriteLine(a.ToString & ", Nothing, " & e.Command.Parameters(a).ParameterName)
				Else
					Debug.WriteLine(a.ToString & ", " & e.Command.Parameters(a).Value.ToString & ", " & e.Command.Parameters(a).ParameterName)
				End If
			Next
		End If

		If Not e.Errors Is Nothing Then
			a = 2
		End If
	End Sub

    ''' <summary>
    ''' Called when [row updated].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
	Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
		Dim a As Integer

		If Not e.Command Is Nothing Then
			a = 1
		End If

		If Not e.Errors Is Nothing Then
			a = 2
		End If
	End Sub

    ''' <summary>
    ''' Called when [row fill error].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
	Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
		Dim a As Integer

		a = 1
	End Sub

End Class
