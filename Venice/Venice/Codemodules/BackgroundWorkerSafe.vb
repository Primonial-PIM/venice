﻿' ***********************************************************************
' Assembly         : Venice
' ***********************************************************************
' <summary></summary>
' ***********************************************************************
Imports System.Threading

' Acknowledgement : http://www.codeproject.com/Articles/558372/Building-a-Class-that-Raises-Events-in-a-Specific

''' <summary>
''' Class BackgroundWorkerThreadSafe
''' </summary>
Public Class BackgroundWorkerThreadSafe
	'System.Windows.Threading.Dispatcher may require you
	'to Add a Reference to "WindowsBase" (yes that is a Dot.Net library)

	Inherits System.ComponentModel.BackgroundWorker
 	Public Shadows Event ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs)
  Public Shadows Event RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs)

  Private m_ExplicitDispatcher As System.Windows.Threading.Dispatcher = Nothing

  Protected Overrides Sub OnProgressChanged(ByVal e As System.ComponentModel.ProgressChangedEventArgs)
    RaiseEventAndExecuteItInAnExplicitOrUIThread(ProgressChangedEvent, New Object() {Me, e}, m_ExplicitDispatcher)
  End Sub

  Protected Overrides Sub OnRunWorkerCompleted(ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs)
    RaiseEventAndExecuteItInAnExplicitOrUIThread(RunWorkerCompletedEvent, New Object() {Me, e}, m_ExplicitDispatcher)
  End Sub

  Public Sub New(ByVal ExplicitDispatcher As System.Windows.Threading.Dispatcher)
    MyBase.New()
    m_ExplicitDispatcher = ExplicitDispatcher
  End Sub

  Private Shared Sub RaiseEventAndExecuteItInAnExplicitOrUIThread(ByVal _event As System.MulticastDelegate, ByVal _ParamArray_args() As Object, ByVal ExplicitThreadSynchronizationDispatcher As System.Windows.Threading.Dispatcher)

    If Not _event Is Nothing Then
      If _event.GetInvocationList().Length > 0 Then

        Dim _sync As System.ComponentModel.ISynchronizeInvoke = Nothing

        For Each _delegate As System.MulticastDelegate In _event.GetInvocationList()

          If ((ExplicitThreadSynchronizationDispatcher Is Nothing) AndAlso (_sync Is Nothing) AndAlso (GetType(System.ComponentModel.ISynchronizeInvoke).IsAssignableFrom(_delegate.Target.GetType())) AndAlso (Not _delegate.Target.GetType().IsAbstract)) Then
            Try
              _sync = CType(_delegate.Target, System.ComponentModel.ISynchronizeInvoke)
            Catch ex As Exception
              Diagnostics.Debug.WriteLine(ex.ToString())
              _sync = Nothing
            End Try
          End If

          If Not ExplicitThreadSynchronizationDispatcher Is Nothing Then
            Try
              ExplicitThreadSynchronizationDispatcher.Invoke(_delegate, _ParamArray_args)
            Catch ex As Exception
              Diagnostics.Debug.WriteLine(ex.ToString())
            End Try
          Else
            If _sync Is Nothing Then
              Try
                _delegate.DynamicInvoke(_ParamArray_args)
              Catch ex As Exception
                Diagnostics.Debug.WriteLine(ex.ToString())
              End Try
            Else
              Try
                _sync.Invoke(_delegate, _ParamArray_args)
              Catch ex As Exception
                Diagnostics.Debug.WriteLine(ex.ToString())
              End Try
            End If
          End If
        Next
      End If
    End If
  End Sub

End Class