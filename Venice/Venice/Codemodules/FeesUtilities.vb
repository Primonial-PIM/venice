﻿Imports RenaissanceGlobals
Imports RenaissanceUtilities.DatePeriodFunctions
Imports RenaissanceDataClass

Module FeesUtilities


  Public Function GetFeeTransactions(ByVal MainForm As VeniceMain, ByVal FundID As Integer, ByVal FeeFromDate As Date, ByVal FeeToDate As Date) As DataTable
    ' *******************************************************************************
    ' Fees are not charged for 'FeeFromDate', it is assumed that this is the last date that was
    ' processed and that fees have already been charged.
    '
    ' *******************************************************************************

    Dim RVal As New DataTable
    Dim RVal_Row As DataRow
    Dim FeeDS As RenaissanceDataClass.DSFeeDefinitions
    Dim FeeTable As RenaissanceDataClass.DSFeeDefinitions.tblFeeDefinitionsDataTable
    Dim FeeRows() As RenaissanceDataClass.DSFeeDefinitions.tblFeeDefinitionsRow
    Dim thisFeeRow As RenaissanceDataClass.DSFeeDefinitions.tblFeeDefinitionsRow
    Dim thisFromDate As Date
    Dim thisToDate As Date
    Dim thisPeriodDays As Double

    Dim thisFundID As Integer
    Dim thisSubFundID As Integer
    Dim thisInstrumentID As Integer
    Dim thisShareClassID As Integer
    Dim thisValueDate As Date
    Dim periodValueDate As Date
    Dim thisSettlementDate As Date
    Dim thisAmount As Double
    Dim thisFeeRateType As Integer
    Dim thisFeeCalculationMethod As Integer

    Try

      RVal.Columns.Add(New DataColumn("FeeID", GetType(Integer)))
      RVal.Columns.Add(New DataColumn("TransactionFundID", GetType(Integer)))
      RVal.Columns.Add(New DataColumn("TransactionSubFundID", GetType(Integer)))
      RVal.Columns.Add(New DataColumn("TransactionInstrumentID", GetType(Integer)))
      RVal.Columns.Add(New DataColumn("TransactionSpecificShareClassID", GetType(Integer)))
      RVal.Columns.Add(New DataColumn("TransactionValueDate", GetType(Date)))
      RVal.Columns.Add(New DataColumn("TransactionSettlementDate", GetType(Date)))
      RVal.Columns.Add(New DataColumn("TransactionAmount", GetType(Double)))
      RVal.Columns.Add(New DataColumn("FeeRateType", GetType(Integer)))
      RVal.Columns.Add(New DataColumn("FeeCalculationMethod", GetType(Integer)))

      FeeDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblFeeDefinitions)
      If (FeeDS Is Nothing) Then
        Return RVal
        Exit Function
      End If
      FeeTable = FeeDS.tblFeeDefinitions

      If (FundID <= 0) Then
        FeeRows = FeeTable.Select
      Else
        FeeRows = FeeTable.Select("FundID=" & FundID.ToString)
      End If

      For Each thisFeeRow In FeeRows

        If (thisFeeRow.FeeStartDate > FeeToDate) OrElse ((thisFeeRow.FeeEndDate > RenaissanceGlobals.Globals.Renaissance_BaseDate) AndAlso (thisFeeRow.FeeEndDate < FeeFromDate)) Then
          Continue For
        End If

        thisFundID = thisFeeRow.FundID
        thisSubFundID = IIf(thisFeeRow.SubFundID <= 0, thisFeeRow.FundID, thisFeeRow.SubFundID)
        thisInstrumentID = thisFeeRow.FeeInstrumentID
        thisShareClassID = Math.Max(0, thisFeeRow.ShareClassID)
        thisFeeRateType = thisFeeRow.FeeRateType
        thisFeeCalculationMethod = thisFeeRow.FeeCalculationMethod

        thisFromDate = MaxDate(FeeFromDate, AddPeriodToDate(DealingPeriod.Daily, thisFeeRow.FeeStartDate, -1))
        thisToDate = MinDate(FeeToDate, IIf(thisFeeRow.FeeEndDate > RenaissanceGlobals.Globals.Renaissance_BaseDate, thisFeeRow.FeeEndDate, RenaissanceGlobals.Globals.Renaissance_EndDate_Data))

        If (thisFromDate >= thisToDate) Then
          Continue For
        End If

        ' Payment Period ?

        If (True) OrElse (CType(thisFeeRow.FeePaymentPeriod, DealingPeriod) <> DealingPeriod.Daily) Then

          If (False) AndAlso (CType(thisFeeRow.FeePaymentPeriod, DealingPeriod) = DealingPeriod.Daily) Then
            periodValueDate = thisToDate
          Else
            periodValueDate = FitDateToPeriod(DealingPeriod.Daily, FitDateToPeriod(CType(thisFeeRow.FeePaymentPeriod, DealingPeriod), AddPeriodToDate(DealingPeriod.Daily, thisFromDate, 1), True))

            ' Continue For If this Value date does not fall inside the current period
            If (periodValueDate > thisToDate) Then
              Continue For
            End If
          End If

          ' OK, Now Process each period

          If (thisFeeRow.FeeDaysPerPeriod > 0) Then
            thisPeriodDays = thisFeeRow.FeeDaysPerPeriod
          Else
            thisPeriodDays = (FitDateToPeriod(CType(thisFeeRow.FeeCalculationPeriod, DealingPeriod), periodValueDate) - AddPeriodToDate(CType(thisFeeRow.FeeCalculationPeriod, DealingPeriod), FitDateToPeriod(CType(thisFeeRow.FeeCalculationPeriod, DealingPeriod), periodValueDate), -1)).TotalDays
          End If

          While periodValueDate <= thisToDate

            If (False) AndAlso (CType(thisFeeRow.FeePaymentPeriod, DealingPeriod) = DealingPeriod.Daily) Then
              ' For daily calculation period, just calculate the whole lot.
              thisAmount = ((thisToDate - thisFromDate).TotalDays / thisPeriodDays) * thisFeeRow.FeeRate
            Else
              ' For non-Day calculation periods calculate per period
              thisAmount = ((periodValueDate - AddPeriodToDate(CType(thisFeeRow.FeePaymentPeriod, DealingPeriod), periodValueDate, -1)).TotalDays / thisPeriodDays) * thisFeeRow.FeeRate
            End If

            thisValueDate = periodValueDate

            If (CType(thisFeeRow.FeeSettlementPeriod, DealingPeriod) = DealingPeriod.Annually) AndAlso (thisFeeRow.FeeUseFundYearEnd <> 0) Then
              ' Fit to Fund Accounting year.

              Dim FundRow As DSFund.tblFundRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, thisFeeRow.FundID)
              Dim FundYearEnd As Date

              If (FundRow Is Nothing) OrElse (FundRow.IsFundYearEndNull) Then
                FundYearEnd = New Date(thisToDate.Year, 12, 31) ' Default to calendar year
              Else
                FundYearEnd = FitDateToPeriod(DealingPeriod.Monthly, New Date(thisToDate.Year, FundRow.FundYearEnd.Month, 28), True)

                If (FundYearEnd < thisToDate) Then
                  FundYearEnd = FundYearEnd.AddYears(1)
                End If
              End If

              ' Should not be necessry for normal fees.
              'If (Not FundRow.IsFundInvestmentStartDateNull) Then
              '  If (FundYearEnd < FundRow.FundInvestmentStartDate.AddYears(1)) Then
              '    FundYearEnd = FundYearEnd.AddYears(1)
              '  End If
              'End If

              thisSettlementDate = AddPeriodToDate(DealingPeriod.Daily, FundYearEnd, thisFeeRow.FeeSettlementTPlusDays)
            Else
              thisSettlementDate = AddPeriodToDate(DealingPeriod.Daily, FitDateToPeriod(CType(thisFeeRow.FeeSettlementPeriod, DealingPeriod), thisValueDate), thisFeeRow.FeeSettlementTPlusDays)
            End If

            ' Add Row.

            RVal_Row = RVal.NewRow

            RVal_Row("FeeID") = thisFeeRow.FeeID
            RVal_Row("TransactionFundID") = thisFundID
            RVal_Row("TransactionSubFundID") = thisSubFundID
            RVal_Row("TransactionInstrumentID") = thisInstrumentID
            RVal_Row("TransactionSpecificShareClassID") = thisShareClassID
            RVal_Row("TransactionValueDate") = thisValueDate
            RVal_Row("TransactionSettlementDate") = thisSettlementDate
            RVal_Row("TransactionAmount") = thisAmount
            RVal_Row("FeeRateType") = thisFeeRateType
            RVal_Row("FeeCalculationMethod") = thisFeeCalculationMethod

            RVal.Rows.Add(RVal_Row)

            periodValueDate = AddPeriodToDate(CType(thisFeeRow.FeePaymentPeriod, DealingPeriod), periodValueDate, 1)
          End While

        Else
          ' Paid Daily.

          If (thisFeeRow.FeeDaysPerPeriod > 0) Then
            thisPeriodDays = thisFeeRow.FeeDaysPerPeriod
          Else
            thisPeriodDays = (FitDateToPeriod(CType(thisFeeRow.FeeCalculationPeriod, DealingPeriod), periodValueDate) - AddPeriodToDate(CType(thisFeeRow.FeeCalculationPeriod, DealingPeriod), FitDateToPeriod(CType(thisFeeRow.FeeCalculationPeriod, DealingPeriod), periodValueDate), -1)).TotalDays
          End If

          thisAmount = ((thisToDate - thisFromDate).TotalDays / thisPeriodDays) * thisFeeRow.FeeRate

          thisValueDate = FeeToDate

          If (CType(thisFeeRow.FeeSettlementPeriod, DealingPeriod) = DealingPeriod.Annually) AndAlso (thisFeeRow.FeeUseFundYearEnd <> 0) Then
            ' Fit to Fund Accounting year.

            Dim FundRow As DSFund.tblFundRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, thisFeeRow.FundID)
            Dim FundYearEnd As Date

            If FundRow.IsFundYearEndNull Then
              FundYearEnd = New Date(thisToDate.Year, 12, 31) ' Default to calendar year
            Else
              FundYearEnd = FitDateToPeriod(DealingPeriod.Monthly, New Date(thisToDate.Year, FundRow.FundYearEnd.Month, 28), True)

              If (FundYearEnd < thisToDate) Then
                FundYearEnd = FundYearEnd.AddYears(1)
              End If
            End If

            ' Should not be necessry for normal fees.
            'If (Not FundRow.IsFundInvestmentStartDateNull) Then
            '  If (FundYearEnd < FundRow.FundInvestmentStartDate.AddYears(1)) Then
            '    FundYearEnd = FundYearEnd.AddYears(1)
            '  End If
            'End If

            thisSettlementDate = AddPeriodToDate(DealingPeriod.Daily, FundYearEnd, thisFeeRow.FeeSettlementTPlusDays)
          Else
            thisSettlementDate = AddPeriodToDate(DealingPeriod.Daily, FitDateToPeriod(CType(thisFeeRow.FeeSettlementPeriod, DealingPeriod), thisValueDate), thisFeeRow.FeeSettlementTPlusDays)
          End If

          ' Add Row.

          RVal_Row = RVal.NewRow

          RVal_Row("FeeID") = thisFeeRow.FeeID
          RVal_Row("TransactionFundID") = thisFundID
          RVal_Row("TransactionSubFundID") = thisSubFundID
          RVal_Row("TransactionInstrumentID") = thisInstrumentID
          RVal_Row("TransactionSpecificShareClassID") = thisShareClassID
          RVal_Row("TransactionValueDate") = thisValueDate
          RVal_Row("TransactionSettlementDate") = thisSettlementDate
          RVal_Row("TransactionAmount") = thisAmount
          RVal_Row("FeeRateType") = thisFeeRateType
          RVal_Row("FeeCalculationMethod") = thisFeeCalculationMethod

          RVal.Rows.Add(RVal_Row)

        End If

        ' ---------------------------------------------------------------------------------

        '  If (thisToDate > thisFromDate) Then
        '    Select Case CType(thisFeeRow.FeeSettlementPeriod, DealingPeriod)

        '      Case DealingPeriod.Daily

        '        If (thisFeeRow.FeeDaysPerPeriod > 0) Then
        '          thisPeriodDays = thisFeeRow.FeeDaysPerPeriod
        '        Else
        '          thisPeriodDays = 1.0#
        '        End If

        '        thisAmount = ((thisToDate - thisFromDate).TotalDays / thisPeriodDays) * thisFeeRow.FeeRate

        '      Case DealingPeriod.Weekly

        '        If (thisFeeRow.FeeDaysPerPeriod > 0) Then
        '          thisPeriodDays = thisFeeRow.FeeDaysPerPeriod
        '        Else
        '          thisPeriodDays = 7.0#
        '        End If

        '        thisAmount = ((thisToDate - thisFromDate).TotalDays / thisPeriodDays) * thisFeeRow.FeeRate

        '      Case DealingPeriod.Fortnightly

        '        If (thisFeeRow.FeeDaysPerPeriod > 0) Then
        '          thisPeriodDays = thisFeeRow.FeeDaysPerPeriod
        '        Else
        '          thisPeriodDays = 14.0#
        '        End If

        '        thisAmount = ((thisToDate - thisFromDate).TotalDays / thisPeriodDays) * thisFeeRow.FeeRate

        '      Case DealingPeriod.Monthly, DealingPeriod.Quarterly, DealingPeriod.SemiAnnually, DealingPeriod.Annually

        '        ' OK Need to allow that not all months / quarters / Half years / years are the same length

        '        If (thisFeeRow.FeeDaysPerPeriod > 0) Then
        '          thisPeriodDays = thisFeeRow.FeeDaysPerPeriod

        '          thisAmount = ((thisToDate - thisFromDate).TotalDays / thisPeriodDays) * thisFeeRow.FeeRate
        '        Else

        '          Dim ThisFeeDay As Date = thisFromDate.AddDays(1)
        '          Dim ThisMonthStartDate As Date
        '          Dim ThisMonthEndDate As Date = RenaissanceGlobals.Globals.Renaissance_BaseDate
        '          Dim thisMonthDays As Double
        '          thisAmount = 0.0#

        '          While ThisFeeDay <= thisToDate

        '            If (ThisFeeDay > ThisMonthEndDate) Then
        '              ThisMonthStartDate = FitDateToPeriod(CType(thisFeeRow.FeeSettlementPeriod, DealingPeriod), ThisFeeDay, False)
        '              ThisMonthEndDate = FitDateToPeriod(CType(thisFeeRow.FeeSettlementPeriod, DealingPeriod), ThisFeeDay)
        '              thisMonthDays = (ThisMonthEndDate - ThisMonthStartDate).TotalDays + 1
        '            End If

        '            thisAmount += ((MinDate(thisToDate, ThisMonthEndDate) - MaxDate(ThisFeeDay, ThisMonthStartDate)).TotalDays + 1.0#) * thisFeeRow.FeeRate
        '            ThisFeeDay = ThisMonthEndDate

        '            ThisFeeDay = ThisFeeDay.AddDays(1)
        '          End While

        '        End If

        '    End Select
        '  Else
        '    thisAmount = 0.0#
        '    Continue For
        '  End If

        '  thisValueDate = FeeToDate

        '  If (CType(thisFeeRow.FeeSettlementPeriod, DealingPeriod) = DealingPeriod.Annually) AndAlso (thisFeeRow.FeeUseFundYearEnd <> 0) Then
        '    ' Fit to Fund Accounting year.

        '    Dim FundRow As DSFund.tblFundRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, FundID)
        '    Dim FundYearEnd As Date

        '    If FundRow.IsFundYearEndNull Then
        '      FundYearEnd = New Date(thisToDate.Year, 12, 31) ' Default to calendar year
        '    Else
        '      FundYearEnd = FitDateToPeriod(DealingPeriod.Monthly, New Date(thisToDate.Year, FundRow.FundYearEnd.Month, 28), True)

        '      If (FundYearEnd < thisToDate) Then
        '        FundYearEnd = FundYearEnd.AddYears(1)
        '      End If
        '    End If

        '    ' Should not be necessry for normal fees.
        '    'If (Not FundRow.IsFundInvestmentStartDateNull) Then
        '    '  If (FundYearEnd < FundRow.FundInvestmentStartDate.AddYears(1)) Then
        '    '    FundYearEnd = FundYearEnd.AddYears(1)
        '    '  End If
        '    'End If

        '    thisSettlementDate = AddPeriodToDate(DealingPeriod.Daily, FitDateToPeriod(CType(thisFeeRow.FeeSettlementPeriod, DealingPeriod), FundYearEnd), thisFeeRow.FeeSettlementTPlusDays)
        '  Else
        '    thisSettlementDate = AddPeriodToDate(DealingPeriod.Daily, FitDateToPeriod(CType(thisFeeRow.FeeSettlementPeriod, DealingPeriod), thisValueDate), thisFeeRow.FeeSettlementTPlusDays)
        '  End If

        '  '

        '  RVal_Row = RVal.NewRow

        '  RVal_Row("FeeID") = thisFeeRow.FeeID
        '  RVal_Row("TransactionFundID") = thisFundID
        '  RVal_Row("TransactionSubFundID") = thisSubFundID
        '  RVal_Row("TransactionInstrumentID") = thisInstrumentID
        '  RVal_Row("TransactionSpecificShareClassID") = thisShareClassID
        '  RVal_Row("TransactionValueDate") = thisValueDate
        '  RVal_Row("TransactionSettlementDate") = thisSettlementDate
        '  RVal_Row("TransactionAmount") = thisAmount
        '  RVal_Row("FeeRateType") = thisFeeRateType
        '  RVal_Row("FeeCalculationMethod") = thisFeeCalculationMethod

        '  RVal.Rows.Add(RVal_Row)

      Next

      RVal.AcceptChanges()

    Catch ex As Exception
      Call MainForm.LogError("GetFeeTransactions Module, GetFeeTransactions()", LOG_LEVELS.Error, ex.Message, "Error in GetFeeTransactions()", ex.StackTrace, True)
    End Try

    Return RVal

  End Function


End Module
