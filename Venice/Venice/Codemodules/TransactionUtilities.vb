' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="TransactionUtilities.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals
Imports System.Data.SqlClient
Imports System.Globalization


''' <summary>
''' Class TransactionUtilities
''' </summary>
Module TransactionUtilities

  ''' <summary>
  ''' Interface ITradeFileEntry
  ''' </summary>
  Public Interface ITradeFileEntry

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is valid.
    ''' </summary>
    ''' <value><c>true</c> if this instance is valid; otherwise, <c>false</c>.</value>
    Property isValid() As Boolean
    ''' <summary>
    ''' Gets or sets the status string.
    ''' </summary>
    ''' <value>The status string.</value>
    Property StatusString() As String
    ''' <summary>
    ''' Gets or sets the trade string.
    ''' </summary>
    ''' <value>The trade string.</value>
    Property TradeString() As String

    ''' <summary>
    ''' Gets or sets the ISIN.
    ''' </summary>
    ''' <value>The ISIN.</value>
    Property ISIN() As String
    ''' <summary>
    ''' Gets or sets the quantity.
    ''' </summary>
    ''' <value>The quantity.</value>
    Property Quantity() As String
    ''' <summary>
    ''' Gets or sets the amount.
    ''' </summary>
    ''' <value>The amount.</value>
    Property Amount() As String
    ''' <summary>
    ''' Gets or sets the amount currency.
    ''' </summary>
    ''' <value>The amount currency.</value>
    Property AmountCurrency() As String
    ''' <summary>
    ''' Gets or sets the customer reference.
    ''' </summary>
    ''' <value>The customer reference.</value>
    Property CustomerReference() As String
    ''' <summary>
    ''' Gets or sets the type of the transaction.
    ''' </summary>
    ''' <value>The type of the transaction.</value>
    Property TransactionType() As RenaissanceGlobals.TransactionTypes
    ''' <summary>
    ''' Gets or sets a value indicating whether this order is for an EMSX broker.
    ''' </summary>
    ''' <value><c>true</c> if this order is for an EMSX broker; otherwise, <c>false</c>.</value>
    Property isEMSX() As Boolean


  End Interface

  ''' <summary>
  ''' Class BNP_Topic_Order_Class
  ''' </summary>
  Public Class BNP_Topic_Order_Class
    Implements ITradeFileEntry


    ' Not these three fields : BQUE_DTIT_CD, GUICH_DTIT_CD & DOSS_TI_NO (Bank Code, Agency Code and Account Number)
    ' Padd to the left with spaces and take the right 'n' characters. Unlike all the other fields.

    ' 15 Mar 2013, Re-Written to use StringBuilder. Just better practice than constantly chopping strings.

    ''' <summary>
    ''' The _ culture name
    ''' </summary>
    Private _CultureName As String = ""
    ''' <summary>
    ''' The _ culture info
    ''' </summary>
    Private _CultureInfo As Globalization.CultureInfo
    ''' <summary>
    ''' The _ numeric precision
    ''' </summary>
    Private _NumericPrecision As Integer
    ''' <summary>
    ''' The double format string
    ''' </summary>
    Private DoubleFormatString As String = "###0.0#####"
    ''' <summary>
    ''' The _ is valid
    ''' </summary>
    Private _IsValid As Boolean = True ' Information only.
    ''' <summary>
    ''' The _ status string
    ''' </summary>
    Private _StatusString As String = "" ' Information only.

    ' An order with this date will be executed on the next available day, not a specific day.
    ''' <summary>
    ''' The order rollover date
    ''' </summary>
    Private Const OrderRolloverDate As Date = #12/31/2999#

    ''' <summary>
    ''' Enum TOPIC_FieldPositions
    ''' </summary>
    Private Enum TOPIC_FieldPositions As Integer
      ''' <summary>
      ''' The EME t_ GE n_ ID
      ''' </summary>
      EMET_GEN_ID = 0
      ''' <summary>
      ''' The OR d_ OPCV m_ DAT
      ''' </summary>
      ORD_OPCVM_DAT = 11
      ''' <summary>
      ''' The OP e_ VA l_ REFEXT
      ''' </summary>
      OPE_VAL_REFEXT = 19
      ''' <summary>
      ''' The BQU e_ DTI t_ CD
      ''' </summary>
      BQUE_DTIT_CD = 35
      ''' <summary>
      ''' The GUIC h_ DTI t_ CD
      ''' </summary>
      GUICH_DTIT_CD = 40
      ''' <summary>
      ''' The DOS s_ T i_ NO
      ''' </summary>
      DOSS_TI_NO = 45
      ''' <summary>
      ''' The DOS s_ CD n_ T
      ''' </summary>
      DOSS_CDN_T = 56
      ''' <summary>
      ''' The VA l_ TYP
      ''' </summary>
      VAL_TYP = 57
      ''' <summary>
      ''' The AC t_ ELM t_ CD
      ''' </summary>
      ACT_ELMT_CD = 61
      ''' <summary>
      ''' The AC t_ LT a_ LIB
      ''' </summary>
      ACT_LTA_LIB = 73
      ''' <summary>
      ''' The OR d_ OPCV m_ CDN
      ''' </summary>
      ORD_OPCVM_CDN = 103
      ''' <summary>
      ''' The OR d_ OPCV m_ QTE
      ''' </summary>
      ORD_OPCVM_QTE = 105
      ''' <summary>
      ''' The CON t_ CO m_ REF
      ''' </summary>
      CONT_COM_REF = 121
      ''' <summary>
      ''' The AC t_ ELM t_ SOU s_ C
      ''' </summary>
      ACT_ELMT_SOUS_C = 137
      ''' <summary>
      ''' The AC t_ LT a_ SOU s_ LIB
      ''' </summary>
      ACT_LTA_SOUS_LIB = 149
      ''' <summary>
      ''' The PAY s_ DE p_ CD
      ''' </summary>
      PAYS_DEP_CD = 179
      ''' <summary>
      ''' The OR d_ OPCV m_ MT
      ''' </summary>
      ORD_OPCVM_MT = 181
      ''' <summary>
      ''' The DE v_ RE g_ OR d_ CD
      ''' </summary>
      DEV_REG_ORD_CD = 197
      ''' <summary>
      ''' The INI t_ NORD
      ''' </summary>
      INIT_NORD = 200
      ''' <summary>
      ''' The OR d_ PRE a_ IND
      ''' </summary>
      ORD_PREA_IND = 216
      ''' <summary>
      ''' The T x_ COM
      ''' </summary>
      TX_COM = 217
      ''' <summary>
      ''' The T x_ ACQ
      ''' </summary>
      TX_ACQ = 222
      ''' <summary>
      ''' The CH g_ OR d_ M t_ IND
      ''' </summary>
      CHG_ORD_MT_IND = 227
      ''' <summary>
      ''' The FILLER
      ''' </summary>
      FILLER = 228
    End Enum

    ''' <summary>
    ''' Enum TOPIC_FieldSizes
    ''' </summary>
    Private Enum TOPIC_FieldSizes As Integer
      ''' <summary>
      ''' The EME t_ GE n_ ID
      ''' </summary>
      EMET_GEN_ID = 11
      ''' <summary>
      ''' The OR d_ OPCV m_ DAT
      ''' </summary>
      ORD_OPCVM_DAT = 8
      ''' <summary>
      ''' The OP e_ VA l_ REFEXT
      ''' </summary>
      OPE_VAL_REFEXT = 16
      ''' <summary>
      ''' The BQU e_ DTI t_ CD
      ''' </summary>
      BQUE_DTIT_CD = 5
      ''' <summary>
      ''' The GUIC h_ DTI t_ CD
      ''' </summary>
      GUICH_DTIT_CD = 5
      ''' <summary>
      ''' The DOS s_ T i_ NO
      ''' </summary>
      DOSS_TI_NO = 11
      ''' <summary>
      ''' The DOS s_ CD n_ T
      ''' </summary>
      DOSS_CDN_T = 1
      ''' <summary>
      ''' The VA l_ TYP
      ''' </summary>
      VAL_TYP = 4
      ''' <summary>
      ''' The AC t_ ELM t_ CD
      ''' </summary>
      ACT_ELMT_CD = 12
      ''' <summary>
      ''' The AC t_ LT a_ LIB
      ''' </summary>
      ACT_LTA_LIB = 30
      ''' <summary>
      ''' The OR d_ OPCV m_ CDN
      ''' </summary>
      ORD_OPCVM_CDN = 2
      ''' <summary>
      ''' The OR d_ OPCV m_ QTE
      ''' </summary>
      ORD_OPCVM_QTE = 16
      ''' <summary>
      ''' The CON t_ CO m_ REF
      ''' </summary>
      CONT_COM_REF = 16
      ''' <summary>
      ''' The AC t_ ELM t_ SOU s_ C
      ''' </summary>
      ACT_ELMT_SOUS_C = 12
      ''' <summary>
      ''' The AC t_ LT a_ SOU s_ LIB
      ''' </summary>
      ACT_LTA_SOUS_LIB = 30
      ''' <summary>
      ''' The PAY s_ DE p_ CD
      ''' </summary>
      PAYS_DEP_CD = 2
      ''' <summary>
      ''' The OR d_ OPCV m_ MT
      ''' </summary>
      ORD_OPCVM_MT = 16
      ''' <summary>
      ''' The DE v_ RE g_ OR d_ CD
      ''' </summary>
      DEV_REG_ORD_CD = 3
      ''' <summary>
      ''' The INI t_ NORD
      ''' </summary>
      INIT_NORD = 16
      ''' <summary>
      ''' The OR d_ PRE a_ IND
      ''' </summary>
      ORD_PREA_IND = 1
      ''' <summary>
      ''' The T x_ COM
      ''' </summary>
      TX_COM = 5
      ''' <summary>
      ''' The T x_ ACQ
      ''' </summary>
      TX_ACQ = 5
      ''' <summary>
      ''' The CH g_ OR d_ M t_ IND
      ''' </summary>
      CHG_ORD_MT_IND = 1
      ''' <summary>
      ''' The FILLER
      ''' </summary>
      FILLER = 272
      ''' <summary>
      ''' The TOTAL
      ''' </summary>
      TOTAL = 500
    End Enum

    ''' <summary>
    ''' The _ trade string
    ''' </summary>
    Private _TradeString As System.Text.StringBuilder

    ''' <summary>
    ''' Initializes a new instance of the <see cref="BNP_Topic_Order_Class"/> class.
    ''' </summary>
    Public Sub New()
      _IsValid = True
      _StatusString = ""

      _TradeString = New System.Text.StringBuilder(TOPIC_FieldSizes.TOTAL + 10)
      _TradeString.Append(" "c, TOPIC_FieldSizes.TOTAL)

      _CultureName = ""
      _NumericPrecision = 6
      _CultureInfo = New Globalization.CultureInfo(_CultureName)

      If (_NumericPrecision > 0) Then
        DoubleFormatString = "###0.0" & (New String("#"c, Math.Min(19, _NumericPrecision - 1)))
      Else
        DoubleFormatString = "###0"
      End If

      ' Defaults

      Me.EMET_GEN_ID = PRIMONIAL_BNP_TOPIC_DEFAULT_SENDER_ID
      Me.ORD_OPCVM_DAT = OrderRolloverDate ' Special meaning date for BNP
      Me.DOSS_CDN_T = "C"
      Me.VAL_TYP = "ISIN"
      Me.BQUE_DTIT_CD = PRIMONIAL_BNP_TOPIC_DEFAULT_BANK_CODE
      Me.GUICH_DTIT_CD = PRIMONIAL_BNP_TOPIC_DEFAULT_AGENCY_CODE

    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="BNP_Topic_Order_Class"/> class.
    ''' </summary>
    ''' <param name="CultureName">Name of the culture.</param>
    ''' <param name="NumericPrecision">The numeric precision.</param>
    Public Sub New(ByVal CultureName As String, ByVal NumericPrecision As Integer)

      Me.New()

      _CultureName = CultureName
      _NumericPrecision = NumericPrecision
      _CultureInfo = New Globalization.CultureInfo(_CultureName)

      If (NumericPrecision < 1) Then
        DoubleFormatString = "###0"
      Else
        DoubleFormatString = "###0.0" & (New String("#"c, Math.Min(19, _NumericPrecision - 1)))
      End If

    End Sub

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is valid.
    ''' </summary>
    ''' <value><c>true</c> if this instance is valid; otherwise, <c>false</c>.</value>
    Public Property IsValid() As Boolean Implements ITradeFileEntry.isValid
      Get
        Return _IsValid
      End Get
      Set(ByVal value As Boolean)
        _IsValid = value
      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the status string.
    ''' </summary>
    ''' <value>The status string.</value>
    Public Property StatusString() As String Implements ITradeFileEntry.StatusString
      Get
        Return _StatusString
      End Get
      Set(ByVal value As String)
        _StatusString = value
      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the trade string.
    ''' </summary>
    ''' <value>The trade string.</value>
    Public Property TradeString() As String Implements ITradeFileEntry.TradeString
      Get
        Return Left(_TradeString.ToString(), TOPIC_FieldSizes.TOTAL)
      End Get
      Set(ByVal value As String)
        _TradeString.Remove(0, _TradeString.ToString.Length)
        _TradeString.Append(Left(value & CStr(New String(" ", TOPIC_FieldSizes.TOTAL)), TOPIC_FieldSizes.TOTAL))
      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the EME t_ GE n_ ID.
    ''' </summary>
    ''' <value>The EME t_ GE n_ ID.</value>
    Public Property EMET_GEN_ID() As String
      Get

        Return _TradeString.ToString.Substring(TOPIC_FieldPositions.EMET_GEN_ID, TOPIC_FieldSizes.EMET_GEN_ID)

      End Get
      Set(ByVal value As String)

        Dim CleanedValue As String = Left(value & CStr(New String(" ", TOPIC_FieldSizes.EMET_GEN_ID)), TOPIC_FieldSizes.EMET_GEN_ID)

        _TradeString.Remove(TOPIC_FieldPositions.EMET_GEN_ID, TOPIC_FieldSizes.EMET_GEN_ID)
        _TradeString.Insert(TOPIC_FieldPositions.EMET_GEN_ID, CleanedValue, 1)

      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the OR d_ OPCV m_ DAT.
    ''' </summary>
    ''' <value>The OR d_ OPCV m_ DAT.</value>
    Public Property ORD_OPCVM_DAT() As Date
      Get
        Dim RVal As Date = Renaissance_BaseDate
        Dim DateString As String


        Try
          DateString = _TradeString.ToString.Substring(TOPIC_FieldPositions.ORD_OPCVM_DAT, TOPIC_FieldSizes.ORD_OPCVM_DAT)
          RVal = DateTime.ParseExact(DateString, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None)
        Catch ex As Exception
        End Try

        Return RVal

      End Get
      Set(ByVal DateValue As Date)
        Dim value As String = DateValue.ToString("yyyyMMdd")

        Dim CleanedValue As String = Left(value & CStr(New String(" ", TOPIC_FieldSizes.ORD_OPCVM_DAT)), TOPIC_FieldSizes.ORD_OPCVM_DAT)

        _TradeString.Remove(TOPIC_FieldPositions.ORD_OPCVM_DAT, TOPIC_FieldSizes.ORD_OPCVM_DAT)
        _TradeString.Insert(TOPIC_FieldPositions.ORD_OPCVM_DAT, CleanedValue, 1)

      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the OP e_ VA l_ REFEXT.
    ''' </summary>
    ''' <value>The OP e_ VA l_ REFEXT.</value>
    Public Property OPE_VAL_REFEXT() As String Implements ITradeFileEntry.CustomerReference
      Get

        Return _TradeString.ToString.Substring(TOPIC_FieldPositions.OPE_VAL_REFEXT, TOPIC_FieldSizes.OPE_VAL_REFEXT)

      End Get
      Set(ByVal value As String)

        Dim CleanedValue As String = Left(value & CStr(New String(" ", TOPIC_FieldSizes.OPE_VAL_REFEXT)), TOPIC_FieldSizes.OPE_VAL_REFEXT)

        _TradeString.Remove(TOPIC_FieldPositions.OPE_VAL_REFEXT, TOPIC_FieldSizes.OPE_VAL_REFEXT)
        _TradeString.Insert(TOPIC_FieldPositions.OPE_VAL_REFEXT, CleanedValue, 1)

      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the BQU e_ DTI t_ CD.
    ''' </summary>
    ''' <value>The BQU e_ DTI t_ CD.</value>
    Public Property BQUE_DTIT_CD() As String
      ' note, unlike most other fields, this one pads left and takes right.
      Get

        Return _TradeString.ToString.Substring(TOPIC_FieldPositions.BQUE_DTIT_CD, TOPIC_FieldSizes.BQUE_DTIT_CD)

      End Get
      Set(ByVal value As String)

        Dim CleanedValue As String = Right(CStr(New String(" ", TOPIC_FieldSizes.BQUE_DTIT_CD)) & value, TOPIC_FieldSizes.BQUE_DTIT_CD)

        _TradeString.Remove(TOPIC_FieldPositions.BQUE_DTIT_CD, TOPIC_FieldSizes.BQUE_DTIT_CD)
        _TradeString.Insert(TOPIC_FieldPositions.BQUE_DTIT_CD, CleanedValue, 1)

      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the GUIC h_ DTI t_ CD.
    ''' </summary>
    ''' <value>The GUIC h_ DTI t_ CD.</value>
    Public Property GUICH_DTIT_CD() As String
      ' note, unlike most other fields, this one pads left and takes right.
      Get

        Return _TradeString.ToString.Substring(TOPIC_FieldPositions.GUICH_DTIT_CD, TOPIC_FieldSizes.GUICH_DTIT_CD)

      End Get
      Set(ByVal value As String)

        Dim CleanedValue As String = Right(CStr(New String(" ", TOPIC_FieldSizes.GUICH_DTIT_CD)) & value, TOPIC_FieldSizes.GUICH_DTIT_CD)

        _TradeString.Remove(TOPIC_FieldPositions.GUICH_DTIT_CD, TOPIC_FieldSizes.GUICH_DTIT_CD)
        _TradeString.Insert(TOPIC_FieldPositions.GUICH_DTIT_CD, CleanedValue, 1)

      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the DOS s_ T i_ NO.
    ''' </summary>
    ''' <value>The DOS s_ T i_ NO.</value>
    Public Property DOSS_TI_NO() As String
      ' note, unlike most other fields, this one pads left and takes right.
      Get

        Return _TradeString.ToString.Substring(TOPIC_FieldPositions.DOSS_TI_NO, TOPIC_FieldSizes.DOSS_TI_NO)

      End Get
      Set(ByVal value As String)

        Dim CleanedValue As String = Right(CStr(New String(" ", TOPIC_FieldSizes.DOSS_TI_NO)) & value, TOPIC_FieldSizes.DOSS_TI_NO)

        _TradeString.Remove(TOPIC_FieldPositions.DOSS_TI_NO, TOPIC_FieldSizes.DOSS_TI_NO)
        _TradeString.Insert(TOPIC_FieldPositions.DOSS_TI_NO, CleanedValue, 1)

      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the DOS s_ CD n_ T.
    ''' </summary>
    ''' <value>The DOS s_ CD n_ T.</value>
    Public Property DOSS_CDN_T() As String
      Get

        Return _TradeString.ToString.Substring(TOPIC_FieldPositions.DOSS_CDN_T, TOPIC_FieldSizes.DOSS_CDN_T)

      End Get
      Set(ByVal value As String)

        Dim CleanedValue As String = Left(value & CStr(New String(" ", TOPIC_FieldSizes.DOSS_CDN_T)), TOPIC_FieldSizes.DOSS_CDN_T)

        _TradeString.Remove(TOPIC_FieldPositions.DOSS_CDN_T, TOPIC_FieldSizes.DOSS_CDN_T)
        _TradeString.Insert(TOPIC_FieldPositions.DOSS_CDN_T, CleanedValue, 1)

      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the VA l_ TYP.
    ''' </summary>
    ''' <value>The VA l_ TYP.</value>
    Public Property VAL_TYP() As String
      Get

        Return _TradeString.ToString.Substring(TOPIC_FieldPositions.VAL_TYP, TOPIC_FieldSizes.VAL_TYP)

      End Get
      Set(ByVal value As String)

        Dim CleanedValue As String = Left(value & CStr(New String(" ", TOPIC_FieldSizes.VAL_TYP)), TOPIC_FieldSizes.VAL_TYP)

        _TradeString.Remove(TOPIC_FieldPositions.VAL_TYP, TOPIC_FieldSizes.VAL_TYP)
        _TradeString.Insert(TOPIC_FieldPositions.VAL_TYP, CleanedValue, 1)

      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the AC t_ ELM t_ CD.
    ''' </summary>
    ''' <value>The AC t_ ELM t_ CD.</value>
    Public Property ACT_ELMT_CD() As String Implements ITradeFileEntry.ISIN
      Get

        Return _TradeString.ToString.Substring(TOPIC_FieldPositions.ACT_ELMT_CD, TOPIC_FieldSizes.ACT_ELMT_CD)

      End Get
      Set(ByVal value As String)

        Dim CleanedValue As String = Left(value & CStr(New String(" ", TOPIC_FieldSizes.ACT_ELMT_CD)), TOPIC_FieldSizes.ACT_ELMT_CD)

        _TradeString.Remove(TOPIC_FieldPositions.ACT_ELMT_CD, TOPIC_FieldSizes.ACT_ELMT_CD)
        _TradeString.Insert(TOPIC_FieldPositions.ACT_ELMT_CD, CleanedValue, 1)

      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the AC t_ LT a_ LIB.
    ''' </summary>
    ''' <value>The AC t_ LT a_ LIB.</value>
    Public Property ACT_LTA_LIB() As String
      Get

        Return _TradeString.ToString.Substring(TOPIC_FieldPositions.ACT_LTA_LIB, TOPIC_FieldSizes.ACT_LTA_LIB)

      End Get
      Set(ByVal value As String)

        Dim CleanedValue As String = Left(value & CStr(New String(" ", TOPIC_FieldSizes.ACT_LTA_LIB)), TOPIC_FieldSizes.ACT_LTA_LIB)

        _TradeString.Remove(TOPIC_FieldPositions.ACT_LTA_LIB, TOPIC_FieldSizes.ACT_LTA_LIB)
        _TradeString.Insert(TOPIC_FieldPositions.ACT_LTA_LIB, CleanedValue, 1)

      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the OR d_ OPCV m_ CDN.
    ''' </summary>
    ''' <value>The OR d_ OPCV m_ CDN.</value>
    Public Property ORD_OPCVM_CDN() As String
      ' 
      'SO : Subsription,
      'RA : Redemption,
      'AR : Cross Order
      'CC : Switch order
      'A1 : Subscription Cancellation
      'A2 : Redemption Cancellation
      'A4 : Cross order cancellation
      'A5 : Switch order cancellation
      '
      Get

        Return _TradeString.ToString.Substring(TOPIC_FieldPositions.ORD_OPCVM_CDN, TOPIC_FieldSizes.ORD_OPCVM_CDN)

      End Get

      Set(ByVal value As String)
        Dim AcceptableValues() As String = {"SO", "RA", "AR", "CC", "A1", "A2", "A4", "A5"}
        Dim CleanedValue As String = Left(value & CStr(New String(" ", TOPIC_FieldSizes.ORD_OPCVM_CDN)), TOPIC_FieldSizes.ORD_OPCVM_CDN).ToUpper

        If Array.IndexOf(AcceptableValues, CleanedValue) >= 0 Then
          _TradeString.Remove(TOPIC_FieldPositions.ORD_OPCVM_CDN, TOPIC_FieldSizes.ORD_OPCVM_CDN)
          _TradeString.Insert(TOPIC_FieldPositions.ORD_OPCVM_CDN, CleanedValue, 1)
        End If

      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the OR d_ OPCV m_ CD n_ value.
    ''' </summary>
    ''' <value>The OR d_ OPCV m_ CD n_ value.</value>
    Public Property ORD_OPCVM_CDN_Value() As RenaissanceGlobals.TransactionTypes Implements ITradeFileEntry.TransactionType
      Get
        Select Case Me.ORD_OPCVM_CDN.ToUpper
          Case "SO"
            Return TransactionTypes.Buy

          Case "RA"
            Return TransactionTypes.Sell

          Case Else
            Return 0

        End Select
      End Get
      Set(ByVal value As RenaissanceGlobals.TransactionTypes)
        Dim CleanedValue As String = ""

        Select Case value
          Case TransactionTypes.Buy
            CleanedValue = "SO"

          Case TransactionTypes.Sell
            CleanedValue = "RA"

        End Select

        Me.ORD_OPCVM_CDN = CleanedValue
      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the OR d_ OPCV m_ QTE.
    ''' </summary>
    ''' <value>The OR d_ OPCV m_ QTE.</value>
    Public Property ORD_OPCVM_QTE() As String Implements ITradeFileEntry.Quantity
      ' Quantity and Amount are exclusive

      Get

        Return _TradeString.ToString.Substring(TOPIC_FieldPositions.ORD_OPCVM_QTE, TOPIC_FieldSizes.ORD_OPCVM_QTE)

      End Get
      Set(ByVal value As String)

        Dim CleanedValue As String = Left(value & CStr(New String(" ", TOPIC_FieldSizes.ORD_OPCVM_QTE)), TOPIC_FieldSizes.ORD_OPCVM_QTE)
        _TradeString.Remove(TOPIC_FieldPositions.ORD_OPCVM_QTE, TOPIC_FieldSizes.ORD_OPCVM_QTE)
        _TradeString.Insert(TOPIC_FieldPositions.ORD_OPCVM_QTE, CleanedValue, 1)

        If (Len(Trim(value)) > 0) AndAlso (Trim(Me.ORD_OPCVM_MT) <> "") Then
          Me.ORD_OPCVM_MT = ""
        End If
      End Set
    End Property

    ''' <summary>
    ''' Sets the OR d_ OPCV m_ QT e_ value.
    ''' </summary>
    ''' <value>The OR d_ OPCV m_ QT e_ value.</value>
    Public WriteOnly Property ORD_OPCVM_QTE_Value() As Double
      Set(ByVal value As Double)
        Dim CleanedValue As String

        If Math.Round(value, 0) = Math.Round(value, 8) Then ' Integer
          CleanedValue = value.ToString("###0", _CultureInfo)
        Else
          CleanedValue = Math.Round(value, _NumericPrecision).ToString(DoubleFormatString, _CultureInfo)
        End If

        Me.ORD_OPCVM_QTE = CleanedValue

      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the CON t_ CO m_ REF.
    ''' </summary>
    ''' <value>The CON t_ CO m_ REF.</value>
    Public Property CONT_COM_REF() As String
      Get

        Return _TradeString.ToString.Substring(TOPIC_FieldPositions.CONT_COM_REF, TOPIC_FieldSizes.CONT_COM_REF)

      End Get
      Set(ByVal value As String)

        Dim CleanedValue As String = Left(value & CStr(New String(" ", TOPIC_FieldSizes.CONT_COM_REF)), TOPIC_FieldSizes.CONT_COM_REF)
        _TradeString.Remove(TOPIC_FieldPositions.CONT_COM_REF, TOPIC_FieldSizes.CONT_COM_REF)
        _TradeString.Insert(TOPIC_FieldPositions.CONT_COM_REF, CleanedValue, 1)

      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the AC t_ ELM t_ SOU s_ C.
    ''' </summary>
    ''' <value>The AC t_ ELM t_ SOU s_ C.</value>
    Public Property ACT_ELMT_SOUS_C() As String
      Get

        Return _TradeString.ToString.Substring(TOPIC_FieldPositions.ACT_ELMT_SOUS_C, TOPIC_FieldSizes.ACT_ELMT_SOUS_C)

      End Get
      Set(ByVal value As String)

        Dim CleanedValue As String = Left(value & CStr(New String(" ", TOPIC_FieldSizes.ACT_ELMT_SOUS_C)), TOPIC_FieldSizes.ACT_ELMT_SOUS_C)

        _TradeString.Remove(TOPIC_FieldPositions.ACT_ELMT_SOUS_C, TOPIC_FieldSizes.ACT_ELMT_SOUS_C)
        _TradeString.Insert(TOPIC_FieldPositions.ACT_ELMT_SOUS_C, CleanedValue, 1)

      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the AC t_ LT a_ SOU s_ LIB.
    ''' </summary>
    ''' <value>The AC t_ LT a_ SOU s_ LIB.</value>
    Public Property ACT_LTA_SOUS_LIB() As String
      Get

        Return _TradeString.ToString.Substring(TOPIC_FieldPositions.ACT_LTA_SOUS_LIB, TOPIC_FieldSizes.ACT_LTA_SOUS_LIB)

      End Get
      Set(ByVal value As String)

        Dim CleanedValue As String = Left(value & CStr(New String(" ", TOPIC_FieldSizes.ACT_LTA_SOUS_LIB)), TOPIC_FieldSizes.ACT_LTA_SOUS_LIB)

        _TradeString.Remove(TOPIC_FieldPositions.ACT_LTA_SOUS_LIB, TOPIC_FieldSizes.ACT_LTA_SOUS_LIB)
        _TradeString.Insert(TOPIC_FieldPositions.ACT_LTA_SOUS_LIB, CleanedValue, 1)

      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the PAY s_ DE p_ CD.
    ''' </summary>
    ''' <value>The PAY s_ DE p_ CD.</value>
    Public Property PAYS_DEP_CD() As String
      Get

        Return _TradeString.ToString.Substring(TOPIC_FieldPositions.PAYS_DEP_CD, TOPIC_FieldSizes.PAYS_DEP_CD)

      End Get
      Set(ByVal value As String)

        Dim CleanedValue As String = Left(value & CStr(New String(" ", TOPIC_FieldSizes.PAYS_DEP_CD)), TOPIC_FieldSizes.PAYS_DEP_CD)

        _TradeString.Remove(TOPIC_FieldPositions.PAYS_DEP_CD, TOPIC_FieldSizes.PAYS_DEP_CD)
        _TradeString.Insert(TOPIC_FieldPositions.PAYS_DEP_CD, CleanedValue, 1)

      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the OR d_ OPCV m_ MT.
    ''' </summary>
    ''' <value>The OR d_ OPCV m_ MT.</value>
    Public Property ORD_OPCVM_MT() As String Implements ITradeFileEntry.Amount
      ' Quantity and Amount are exclusive

      Get

        Return _TradeString.ToString.Substring(TOPIC_FieldPositions.ORD_OPCVM_MT, TOPIC_FieldSizes.ORD_OPCVM_MT)

      End Get
      Set(ByVal value As String)

        Dim CleanedValue As String = Left(value & CStr(New String(" ", TOPIC_FieldSizes.ORD_OPCVM_MT)), TOPIC_FieldSizes.ORD_OPCVM_MT)

        _TradeString.Remove(TOPIC_FieldPositions.ORD_OPCVM_MT, TOPIC_FieldSizes.ORD_OPCVM_MT)
        _TradeString.Insert(TOPIC_FieldPositions.ORD_OPCVM_MT, CleanedValue, 1)

        If (Len(Trim(value)) > 0) AndAlso (Trim(Me.ORD_OPCVM_QTE) <> "") Then
          Me.ORD_OPCVM_QTE = ""
        End If
      End Set
    End Property

    ''' <summary>
    ''' Sets the OR d_ OPCV m_ M t_ value.
    ''' </summary>
    ''' <value>The OR d_ OPCV m_ M t_ value.</value>
    Public WriteOnly Property ORD_OPCVM_MT_Value() As Double
      Set(ByVal value As Double)
        Dim CleanedValue As String

        If Math.Round(value, 0) = Math.Round(value, 4) Then ' Integer
          CleanedValue = value.ToString("###0", _CultureInfo)
        Else
          CleanedValue = value.ToString(DoubleFormatString, _CultureInfo)
        End If

        Me.ORD_OPCVM_MT = CleanedValue
      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the DE v_ RE g_ OR d_ CD.
    ''' </summary>
    ''' <value>The DE v_ RE g_ OR d_ CD.</value>
    Public Property DEV_REG_ORD_CD() As String Implements ITradeFileEntry.AmountCurrency
      Get

        Return _TradeString.ToString.Substring(TOPIC_FieldPositions.DEV_REG_ORD_CD, TOPIC_FieldSizes.DEV_REG_ORD_CD)

      End Get
      Set(ByVal value As String)

        Dim CleanedValue As String = Left(value & CStr(New String(" ", TOPIC_FieldSizes.DEV_REG_ORD_CD)), TOPIC_FieldSizes.DEV_REG_ORD_CD)

        _TradeString.Remove(TOPIC_FieldPositions.DEV_REG_ORD_CD, TOPIC_FieldSizes.DEV_REG_ORD_CD)
        _TradeString.Insert(TOPIC_FieldPositions.DEV_REG_ORD_CD, CleanedValue, 1)

      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the INI t_ NORD.
    ''' </summary>
    ''' <value>The INI t_ NORD.</value>
    Public Property INIT_NORD() As String
      Get

        Return _TradeString.ToString.Substring(TOPIC_FieldPositions.INIT_NORD, TOPIC_FieldSizes.INIT_NORD)

      End Get
      Set(ByVal value As String)

        Dim CleanedValue As String = Left(value & CStr(New String(" ", TOPIC_FieldSizes.INIT_NORD)), TOPIC_FieldSizes.INIT_NORD)

        _TradeString.Remove(TOPIC_FieldPositions.INIT_NORD, TOPIC_FieldSizes.INIT_NORD)
        _TradeString.Insert(TOPIC_FieldPositions.INIT_NORD, CleanedValue, 1)

      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the OR d_ PRE a_ IND.
    ''' </summary>
    ''' <value>The OR d_ PRE a_ IND.</value>
    Public Property ORD_PREA_IND() As String
      Get

        Return _TradeString.ToString.Substring(TOPIC_FieldPositions.ORD_PREA_IND, TOPIC_FieldSizes.ORD_PREA_IND)

      End Get
      Set(ByVal value As String)

        Dim CleanedValue As String = Left(value & CStr(New String(" ", TOPIC_FieldSizes.ORD_PREA_IND)), TOPIC_FieldSizes.ORD_PREA_IND)

        _TradeString.Remove(TOPIC_FieldPositions.ORD_PREA_IND, TOPIC_FieldSizes.ORD_PREA_IND)
        _TradeString.Insert(TOPIC_FieldPositions.ORD_PREA_IND, CleanedValue, 1)

      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the T x_ COM.
    ''' </summary>
    ''' <value>The T x_ COM.</value>
    Public Property TX_COM() As String
      Get

        Return _TradeString.ToString.Substring(TOPIC_FieldPositions.TX_COM, TOPIC_FieldSizes.TX_COM)

      End Get
      Set(ByVal value As String)

        Dim CleanedValue As String = Left(value & CStr(New String(" ", TOPIC_FieldSizes.TX_COM)), TOPIC_FieldSizes.TX_COM)

        _TradeString.Remove(TOPIC_FieldPositions.TX_COM, TOPIC_FieldSizes.TX_COM)
        _TradeString.Insert(TOPIC_FieldPositions.TX_COM, CleanedValue, 1)

      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the T x_ ACQ.
    ''' </summary>
    ''' <value>The T x_ ACQ.</value>
    Public Property TX_ACQ() As String
      Get

        Return _TradeString.ToString.Substring(TOPIC_FieldPositions.TX_ACQ, TOPIC_FieldSizes.TX_ACQ)

      End Get
      Set(ByVal value As String)

        Dim CleanedValue As String = Left(value & CStr(New String(" ", TOPIC_FieldSizes.TX_ACQ)), TOPIC_FieldSizes.TX_ACQ)

        _TradeString.Remove(TOPIC_FieldPositions.TX_ACQ, TOPIC_FieldSizes.TX_ACQ)
        _TradeString.Insert(TOPIC_FieldPositions.TX_ACQ, CleanedValue, 1)

      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the CH g_ OR d_ M t_ IND.
    ''' </summary>
    ''' <value>The CH g_ OR d_ M t_ IND.</value>
    Public Property CHG_ORD_MT_IND() As String
      Get

        Return _TradeString.ToString.Substring(TOPIC_FieldPositions.CHG_ORD_MT_IND, TOPIC_FieldSizes.CHG_ORD_MT_IND)

      End Get
      Set(ByVal value As String)

        Dim CleanedValue As String = Left(value & CStr(New String(" ", TOPIC_FieldSizes.CHG_ORD_MT_IND)), TOPIC_FieldSizes.CHG_ORD_MT_IND)

        _TradeString.Remove(TOPIC_FieldPositions.CHG_ORD_MT_IND, TOPIC_FieldSizes.CHG_ORD_MT_IND)
        _TradeString.Insert(TOPIC_FieldPositions.CHG_ORD_MT_IND, CleanedValue, 1)

      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the FILLER.
    ''' </summary>
    ''' <value>The FILLER.</value>
    Public Property FILLER() As String
      Get

        Return _TradeString.ToString.Substring(TOPIC_FieldPositions.FILLER, TOPIC_FieldSizes.FILLER)

      End Get
      Set(ByVal value As String)

        Dim CleanedValue As String = Left(value & CStr(New String(" ", TOPIC_FieldSizes.FILLER)), TOPIC_FieldSizes.FILLER)

        _TradeString.Remove(TOPIC_FieldPositions.FILLER, TOPIC_FieldSizes.FILLER)
        _TradeString.Insert(TOPIC_FieldPositions.FILLER, CleanedValue, 1)

      End Set
    End Property

    Public Property isEMSX() As Boolean Implements ITradeFileEntry.isEMSX
      Get
        Return False
      End Get
      Set(ByVal value As Boolean)

      End Set
    End Property

  End Class

  ''' <summary>
  ''' Class BNP_Topic_Acknowledgement_Class
  ''' </summary>
  Public Class BNP_Topic_Acknowledgement_Class

    ''' <summary>
    ''' Enum TOPIC_FieldPositions
    ''' </summary>
    Private Enum TOPIC_FieldPositions As Integer
      ''' <summary>
      ''' The EME t_ GE n_ ID
      ''' </summary>
      EMET_GEN_ID = 0
      ''' <summary>
      ''' The B p2 s_ MESSAG e_ ID
      ''' </summary>
      BP2S_MESSAGE_ID = 11
      ''' <summary>
      ''' The B p2 s_ ORDE r_ ID
      ''' </summary>
      BP2S_ORDER_ID = 27
      ''' <summary>
      ''' The TRANSACTIO n_ ID
      ''' </summary>
      TRANSACTION_ID = 39
      ''' <summary>
      ''' The ORDE r_ STATUS
      ''' </summary>
      ORDER_STATUS = 55
      ''' <summary>
      ''' The ANOMAL y_ CODE
      ''' </summary>
      ANOMALY_CODE = 59
      ''' <summary>
      ''' The ANOMAL y_ DESCRIPTION
      ''' </summary>
      ANOMALY_DESCRIPTION = 62
    End Enum

    ''' <summary>
    ''' Enum TOPIC_FieldSizes
    ''' </summary>
    Private Enum TOPIC_FieldSizes As Integer
      ''' <summary>
      ''' The EME t_ GE n_ ID
      ''' </summary>
      EMET_GEN_ID = 11
      ''' <summary>
      ''' The B p2 s_ MESSAG e_ ID
      ''' </summary>
      BP2S_MESSAGE_ID = 16
      ''' <summary>
      ''' The B p2 s_ ORDE r_ ID
      ''' </summary>
      BP2S_ORDER_ID = 12
      ''' <summary>
      ''' The TRANSACTIO n_ ID
      ''' </summary>
      TRANSACTION_ID = 16
      ''' <summary>
      ''' The ORDE r_ STATUS
      ''' </summary>
      ORDER_STATUS = 4
      ''' <summary>
      ''' The ANOMAL y_ CODE
      ''' </summary>
      ANOMALY_CODE = 3
      ''' <summary>
      ''' The ANOMAL y_ DESCRIPTION
      ''' </summary>
      ANOMALY_DESCRIPTION = 38
      ''' <summary>
      ''' The TOTAL
      ''' </summary>
      TOTAL = 100
    End Enum

    ''' <summary>
    ''' The _ acknowledgement string
    ''' </summary>
    Private _AcknowledgementString As String

    ''' <summary>
    ''' Initializes a new instance of the <see cref="BNP_Topic_Acknowledgement_Class"/> class.
    ''' </summary>
    Public Sub New()

    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="BNP_Topic_Acknowledgement_Class"/> class.
    ''' </summary>
    ''' <param name="Acknowledgement">The acknowledgement.</param>
    Public Sub New(ByVal Acknowledgement As String)
      AcknowledgementString = Acknowledgement
    End Sub

    ''' <summary>
    ''' Gets or sets the acknowledgement string.
    ''' </summary>
    ''' <value>The acknowledgement string.</value>
    Public Property AcknowledgementString() As String
      Get
        Return Left(_AcknowledgementString, TOPIC_FieldSizes.TOTAL)
      End Get
      Set(ByVal value As String)
        _AcknowledgementString = Left(value & CStr(New String(" ", TOPIC_FieldSizes.TOTAL)), TOPIC_FieldSizes.TOTAL)
      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the B p2 s_ MESSAG e_ ID.
    ''' </summary>
    ''' <value>The B p2 s_ MESSAG e_ ID.</value>
    Public Property BP2S_MESSAGE_ID() As String
      Get

        Return _AcknowledgementString.Substring(TOPIC_FieldPositions.BP2S_MESSAGE_ID, TOPIC_FieldSizes.BP2S_MESSAGE_ID)

      End Get
      Set(ByVal value As String)

        Dim CleanedValue As String = Left(value & CStr(New String(" ", TOPIC_FieldSizes.BP2S_MESSAGE_ID)), TOPIC_FieldSizes.BP2S_MESSAGE_ID)

        _AcknowledgementString = Left(_AcknowledgementString, TOPIC_FieldPositions.BP2S_MESSAGE_ID) & CleanedValue & Right(_AcknowledgementString, Len(_AcknowledgementString) - (TOPIC_FieldPositions.BP2S_MESSAGE_ID + TOPIC_FieldSizes.BP2S_MESSAGE_ID))

      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the B p2 s_ ORDE r_ ID.
    ''' </summary>
    ''' <value>The B p2 s_ ORDE r_ ID.</value>
    Public Property BP2S_ORDER_ID() As String
      Get

        Return _AcknowledgementString.Substring(TOPIC_FieldPositions.BP2S_ORDER_ID, TOPIC_FieldSizes.BP2S_ORDER_ID)

      End Get
      Set(ByVal value As String)

        Dim CleanedValue As String = Left(value & CStr(New String(" ", TOPIC_FieldSizes.BP2S_ORDER_ID)), TOPIC_FieldSizes.BP2S_ORDER_ID)
        _AcknowledgementString = Left(_AcknowledgementString, TOPIC_FieldPositions.BP2S_ORDER_ID) & CleanedValue & Right(_AcknowledgementString, Len(_AcknowledgementString) - (TOPIC_FieldPositions.BP2S_ORDER_ID + TOPIC_FieldSizes.BP2S_ORDER_ID))

      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the TRANSACTIO n_ ID.
    ''' </summary>
    ''' <value>The TRANSACTIO n_ ID.</value>
    Public Property TRANSACTION_ID() As String
      ' note, unlike most other fields, this one pads left and takes right.
      Get

        Return _AcknowledgementString.Substring(TOPIC_FieldPositions.TRANSACTION_ID, TOPIC_FieldSizes.TRANSACTION_ID)

      End Get
      Set(ByVal value As String)

        Dim CleanedValue As String = Right(CStr(New String(" ", TOPIC_FieldSizes.TRANSACTION_ID)) & value, TOPIC_FieldSizes.TRANSACTION_ID)
        _AcknowledgementString = Left(_AcknowledgementString, TOPIC_FieldPositions.TRANSACTION_ID) & CleanedValue & Right(_AcknowledgementString, Len(_AcknowledgementString) - (TOPIC_FieldPositions.TRANSACTION_ID + TOPIC_FieldSizes.TRANSACTION_ID))

      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the ORDE r_ STATUS.
    ''' </summary>
    ''' <value>The ORDE r_ STATUS.</value>
    Public Property ORDER_STATUS() As String
      ' note, unlike most other fields, this one pads left and takes right.
      Get

        Return _AcknowledgementString.Substring(TOPIC_FieldPositions.ORDER_STATUS, TOPIC_FieldSizes.ORDER_STATUS)

      End Get
      Set(ByVal value As String)

        Dim CleanedValue As String = Right(CStr(New String(" ", TOPIC_FieldSizes.ORDER_STATUS)) & value, TOPIC_FieldSizes.ORDER_STATUS)
        _AcknowledgementString = Left(_AcknowledgementString, TOPIC_FieldPositions.ORDER_STATUS) & CleanedValue & Right(_AcknowledgementString, Len(_AcknowledgementString) - (TOPIC_FieldPositions.ORDER_STATUS + TOPIC_FieldSizes.ORDER_STATUS))

      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the ANOMAL y_ CODE.
    ''' </summary>
    ''' <value>The ANOMAL y_ CODE.</value>
    Public Property ANOMALY_CODE() As String
      ' note, unlike most other fields, this one pads left and takes right.
      Get

        Return _AcknowledgementString.Substring(TOPIC_FieldPositions.ANOMALY_CODE, TOPIC_FieldSizes.ANOMALY_CODE)

      End Get
      Set(ByVal value As String)

        Dim CleanedValue As String = Right(CStr(New String(" ", TOPIC_FieldSizes.ANOMALY_CODE)) & value, TOPIC_FieldSizes.ANOMALY_CODE)
        _AcknowledgementString = Left(_AcknowledgementString, TOPIC_FieldPositions.ANOMALY_CODE) & CleanedValue & Right(_AcknowledgementString, Len(_AcknowledgementString) - (TOPIC_FieldPositions.ANOMALY_CODE + TOPIC_FieldSizes.ANOMALY_CODE))

      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the ANOMAL y_ DESCRIPTION.
    ''' </summary>
    ''' <value>The ANOMAL y_ DESCRIPTION.</value>
    Public Property ANOMALY_DESCRIPTION() As String
      Get

        Return _AcknowledgementString.Substring(TOPIC_FieldPositions.ANOMALY_DESCRIPTION, TOPIC_FieldSizes.ANOMALY_DESCRIPTION)

      End Get
      Set(ByVal value As String)

        Dim CleanedValue As String = Left(value & CStr(New String(" ", TOPIC_FieldSizes.ANOMALY_DESCRIPTION)), TOPIC_FieldSizes.ANOMALY_DESCRIPTION)
        _AcknowledgementString = Left(_AcknowledgementString, TOPIC_FieldPositions.ANOMALY_DESCRIPTION) & CleanedValue & Right(_AcknowledgementString, Len(_AcknowledgementString) - (TOPIC_FieldPositions.ANOMALY_DESCRIPTION + TOPIC_FieldSizes.ANOMALY_DESCRIPTION))

      End Set
    End Property

  End Class

  ''' <summary>
  ''' Class BNP_FDS_Order_Class
  ''' </summary>
  Public Class BNP_FDS_Order_Class
    Implements ITradeFileEntry

    ''' <summary>
    ''' The _ culture name
    ''' </summary>
    Private _CultureName As String = ""
    ''' <summary>
    ''' The _ culture info
    ''' </summary>
    Private _CultureInfo As Globalization.CultureInfo
    ''' <summary>
    ''' The _ numeric precision
    ''' </summary>
    Private _NumericPrecision As Integer
    ''' <summary>
    ''' The double format string
    ''' </summary>
    Private DoubleFormatString As String = "###0.0#####"
    ''' <summary>
    ''' The _ is valid
    ''' </summary>
    Private _IsValid As Boolean = True ' Information only.
    ''' <summary>
    ''' The _ status string
    ''' </summary>
    Private _StatusString As String = "" ' Information only.

    ''' <summary>
    ''' The trade terms
    ''' </summary>
    Private TradeTerms(14) As String

    ''' <summary>
    ''' Enum FDS_FieldPositions
    ''' </summary>
    Private Enum FDS_FieldPositions As Integer
      ''' <summary>
      ''' The product code
      ''' </summary>
      ProductCode = 0
      ''' <summary>
      ''' The order reference
      ''' </summary>
      OrderReference = 1
      ''' <summary>
      ''' The account number
      ''' </summary>
      AccountNumber = 2
      ''' <summary>
      ''' The order type
      ''' </summary>
      OrderType = 3
      ''' <summary>
      ''' The security type
      ''' </summary>
      SecurityType = 4
      ''' <summary>
      ''' The ISIN
      ''' </summary>
      ISIN = 5
      ''' <summary>
      ''' The exceptional fee
      ''' </summary>
      ExceptionalFee = 6
      ''' <summary>
      ''' The security type2
      ''' </summary>
      SecurityType2 = 7
      ''' <summary>
      ''' The ISI n2
      ''' </summary>
      ISIN2 = 8
      ''' <summary>
      ''' The amount
      ''' </summary>
      Amount = 9
      ''' <summary>
      ''' The payment currency
      ''' </summary>
      PaymentCurrency = 10
      ''' <summary>
      ''' The quotation currency
      ''' </summary>
      QuotationCurrency = 11
      ''' <summary>
      ''' The quantity
      ''' </summary>
      Quantity = 12
      ''' <summary>
      ''' The transfer agent note
      ''' </summary>
      TransferAgentNote = 13
      ''' <summary>
      ''' The code bank
      ''' </summary>
      CodeBank = 14
    End Enum

    ''' <summary>
    ''' Enum FDS_FieldSizes
    ''' </summary>
    Private Enum FDS_FieldSizes As Integer
      ''' <summary>
      ''' The product code
      ''' </summary>
      ProductCode = 3
      ''' <summary>
      ''' The order reference
      ''' </summary>
      OrderReference = 16
      ''' <summary>
      ''' The account number LUX
      ''' </summary>
      AccountNumberLUX = 21
      ''' <summary>
      ''' The account number GB
      ''' </summary>
      AccountNumberGB = 7
      ''' <summary>
      ''' The order type
      ''' </summary>
      OrderType = 4
      ''' <summary>
      ''' The security type
      ''' </summary>
      SecurityType = 8
      ''' <summary>
      ''' The ISIN
      ''' </summary>
      ISIN = 12
      ''' <summary>
      ''' The exceptional fee
      ''' </summary>
      ExceptionalFee = 23
      ''' <summary>
      ''' The security type2
      ''' </summary>
      SecurityType2 = 8
      ''' <summary>
      ''' The ISI n2
      ''' </summary>
      ISIN2 = 12
      ''' <summary>
      ''' The amount
      ''' </summary>
      Amount = 23
      ''' <summary>
      ''' The payment currency
      ''' </summary>
      PaymentCurrency = 3
      ''' <summary>
      ''' The quotation currency
      ''' </summary>
      QuotationCurrency = 3
      ''' <summary>
      ''' The quantity
      ''' </summary>
      Quantity = 39
      ''' <summary>
      ''' The transfer agent noter
      ''' </summary>
      TransferAgentNoter = 300
      ''' <summary>
      ''' The code bank
      ''' </summary>
      CodeBank = 6
    End Enum

    ''' <summary>
    ''' Initializes a new instance of the <see cref="BNP_FDS_Order_Class"/> class.
    ''' </summary>
    Public Sub New()
      _IsValid = True
      _StatusString = ""

      _CultureName = ""
      _NumericPrecision = 2
      _CultureInfo = New Globalization.CultureInfo(_CultureName)

      DoubleFormatString = "###0.0" & (New String("#"c, Math.Min(19, _NumericPrecision - 1)))

      ' Defaults

      TradeTerms(FDS_FieldPositions.ProductCode) = "ORD" ' Always ORD
      TradeTerms(FDS_FieldPositions.SecurityType) = "ISIN" ' Always ISIN
      TradeTerms(FDS_FieldPositions.CodeBank) = "PARBLU" ' Luxembourg, default

    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="BNP_FDS_Order_Class"/> class.
    ''' </summary>
    ''' <param name="CultureName">Name of the culture.</param>
    ''' <param name="NumericPrecision">The numeric precision.</param>
    Public Sub New(ByVal CultureName As String, ByVal NumericPrecision As Integer)

      Me.New()

      _CultureName = CultureName
      _NumericPrecision = NumericPrecision
      _CultureInfo = New Globalization.CultureInfo(_CultureName)

      If (NumericPrecision < 1) Then
        DoubleFormatString = "###0"
      Else
        DoubleFormatString = "###0.0" & (New String("#"c, Math.Min(19, _NumericPrecision - 1)))
      End If

    End Sub

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is valid.
    ''' </summary>
    ''' <value><c>true</c> if this instance is valid; otherwise, <c>false</c>.</value>
    Public Property IsValid() As Boolean Implements ITradeFileEntry.isValid
      Get
        Return _IsValid
      End Get
      Set(ByVal value As Boolean)
        _IsValid = value
      End Set
    End Property

    ''' <summary>
    ''' Gets the product code.
    ''' </summary>
    ''' <value>The product code.</value>
    Public ReadOnly Property ProductCode() As String
      Get
        Return TradeTerms(FDS_FieldPositions.ProductCode)
      End Get
    End Property

    ''' <summary>
    ''' Gets or sets the customer reference.
    ''' </summary>
    ''' <value>The customer reference.</value>
    Public Property CustomerReference() As String Implements ITradeFileEntry.CustomerReference
      Get
        Return TradeTerms(FDS_FieldPositions.OrderReference)
      End Get
      Set(ByVal value As String)
        TradeTerms(FDS_FieldPositions.OrderReference) = Left(value, FDS_FieldSizes.OrderReference)
      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the account number.
    ''' </summary>
    ''' <value>The account number.</value>
    Public Property AccountNumber() As String
      Get
        Return TradeTerms(FDS_FieldPositions.AccountNumber)
      End Get
      Set(ByVal value As String)
        If TradeTerms(FDS_FieldSizes.CodeBank) = "PARBGB" Then
          TradeTerms(FDS_FieldPositions.AccountNumber) = Left(value, FDS_FieldSizes.AccountNumberGB)
        Else
          TradeTerms(FDS_FieldPositions.AccountNumber) = Left(value, FDS_FieldSizes.AccountNumberLUX)
        End If
      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the type of the order.
    ''' </summary>
    ''' <value>The type of the order.</value>
    Public Property OrderType() As String
      Get
        Return TradeTerms(FDS_FieldPositions.OrderType)
      End Get
      Set(ByVal value As String)
        TradeTerms(FDS_FieldPositions.OrderType) = Left(value, FDS_FieldSizes.OrderType).ToUpper
      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the type of the transaction.
    ''' </summary>
    ''' <value>The type of the transaction.</value>
    Public Property TransactionType() As RenaissanceGlobals.TransactionTypes Implements ITradeFileEntry.TransactionType
      Get
        If (TradeTerms(FDS_FieldPositions.OrderType) = "SUBS") Then
          Return TransactionTypes.Buy
        ElseIf (TradeTerms(FDS_FieldPositions.OrderType) = "REDE") Then
          Return TransactionTypes.Sell
        Else
          Return 0
        End If
      End Get
      Set(ByVal value As RenaissanceGlobals.TransactionTypes)
        Select Case value
          Case TransactionTypes.Buy
            TradeTerms(FDS_FieldPositions.OrderType) = "SUBS"

          Case TransactionTypes.Sell
            TradeTerms(FDS_FieldPositions.OrderType) = "REDE"

          Case Else
            TradeTerms(FDS_FieldPositions.OrderType) = ""
            ' Fail.
        End Select
      End Set
    End Property

    ''' <summary>
    ''' Gets the type of the security.
    ''' </summary>
    ''' <value>The type of the security.</value>
    Public ReadOnly Property SecurityType() As String
      Get
        Return TradeTerms(FDS_FieldPositions.SecurityType)
      End Get
    End Property

    ''' <summary>
    ''' Gets or sets the ISIN.
    ''' </summary>
    ''' <value>The ISIN.</value>
    Public Property ISIN() As String Implements ITradeFileEntry.ISIN
      Get
        Return TradeTerms(FDS_FieldPositions.ISIN)
      End Get
      Set(ByVal value As String)
        TradeTerms(FDS_FieldPositions.ISIN) = Left(Trim(value), FDS_FieldSizes.ISIN)
      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the exceptional fees.
    ''' </summary>
    ''' <value>The exceptional fees.</value>
    Public Property ExceptionalFees() As Double
      Get
        Return CDbl(TradeTerms(FDS_FieldPositions.ExceptionalFee))
      End Get
      Set(ByVal value As Double)
        TradeTerms(FDS_FieldPositions.ExceptionalFee) = value.ToString(DoubleFormatString)
      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the amount.
    ''' </summary>
    ''' <value>The amount.</value>
    Public Property Amount() As String Implements ITradeFileEntry.Amount
      Get

        Return TradeTerms(FDS_FieldPositions.Amount)

      End Get
      Set(ByVal value As String)

        TradeTerms(FDS_FieldPositions.Amount) = value

      End Set
    End Property

    ''' <summary>
    ''' Sets the amount_ value.
    ''' </summary>
    ''' <value>The amount_ value.</value>
    Public WriteOnly Property Amount_Value() As Double
      Set(ByVal value As Double)
        Dim CleanedValue As String

        If Math.Round(value, 0) = Math.Round(value, 4) Then ' Integer
          CleanedValue = value.ToString("###0", _CultureInfo)
        Else
          CleanedValue = Math.Round(value, 2).ToString("###0.00", _CultureInfo)
        End If

        TradeTerms(FDS_FieldPositions.Amount) = CleanedValue
      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the amount currency.
    ''' </summary>
    ''' <value>The amount currency.</value>
    Public Property AmountCurrency() As String Implements ITradeFileEntry.AmountCurrency
      Get
        Return Me.PaymentCurrency
      End Get
      Set(ByVal value As String)
        Me.PaymentCurrency = value
      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the payment currency.
    ''' </summary>
    ''' <value>The payment currency.</value>
    Public Property PaymentCurrency() As String
      Get
        Return TradeTerms(FDS_FieldPositions.PaymentCurrency)
      End Get
      Set(ByVal value As String)
        TradeTerms(FDS_FieldPositions.PaymentCurrency) = Left(Trim(value), FDS_FieldSizes.PaymentCurrency)
      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the quotation currency.
    ''' </summary>
    ''' <value>The quotation currency.</value>
    Public Property QuotationCurrency() As String
      Get
        Return TradeTerms(FDS_FieldPositions.QuotationCurrency)
      End Get
      Set(ByVal value As String)
        TradeTerms(FDS_FieldPositions.QuotationCurrency) = Left(Trim(value), FDS_FieldSizes.QuotationCurrency)
      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the quantity.
    ''' </summary>
    ''' <value>The quantity.</value>
    Public Property Quantity() As String Implements ITradeFileEntry.Quantity
      Get

        Return TradeTerms(FDS_FieldPositions.Quantity)

      End Get
      Set(ByVal value As String)

        TradeTerms(FDS_FieldPositions.Quantity) = value

      End Set

    End Property

    ''' <summary>
    ''' Sets the quantity_ value.
    ''' </summary>
    ''' <value>The quantity_ value.</value>
    Public WriteOnly Property Quantity_Value() As Double
      Set(ByVal value As Double)
        Dim CleanedValue As String

        If Math.Round(value, 0) = Math.Round(value, 8) Then ' Integer
          CleanedValue = Math.Round(value).ToString("###0", _CultureInfo)
        Else
          CleanedValue = Math.Round(value, _NumericPrecision).ToString(DoubleFormatString, _CultureInfo)
        End If

        TradeTerms(FDS_FieldPositions.Quantity) = CleanedValue
      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the transfer agent note.
    ''' </summary>
    ''' <value>The transfer agent note.</value>
    Public Property TransferAgentNote() As String
      Get
        Return TradeTerms(FDS_FieldPositions.TransferAgentNote)
      End Get
      Set(ByVal value As String)
        TradeTerms(FDS_FieldPositions.TransferAgentNote) = value
      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the code bank.
    ''' </summary>
    ''' <value>The code bank.</value>
    Public Property CodeBank() As String
      Get
        Return TradeTerms(FDS_FieldPositions.CodeBank)
      End Get
      Set(ByVal value As String)
        TradeTerms(FDS_FieldPositions.CodeBank) = value
      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the status string.
    ''' </summary>
    ''' <value>The status string.</value>
    Public Property StatusString() As String Implements ITradeFileEntry.StatusString
      Get
        Return _StatusString
      End Get
      Set(ByVal value As String)
        _StatusString = value
      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the trade string.
    ''' </summary>
    ''' <value>The trade string.</value>
    Public Property TradeString() As String Implements ITradeFileEntry.TradeString
      Get
        Return String.Join(";", TradeTerms)
      End Get
      Set(ByVal value As String)
        Dim TempArray() As String
        Dim Counter As Integer

        TempArray = value.Split(New Char() {";"c}, StringSplitOptions.None)

        If (TempArray.Length >= 13) Then
          For Counter = 0 To (TradeTerms.Length - 1)
            If (TempArray.Length > Counter) Then
              If (Counter <> FDS_FieldPositions.ProductCode) AndAlso (Counter <> FDS_FieldPositions.SecurityType) Then
                TradeTerms(Counter) = TempArray(Counter)
              End If
            End If
          Next

        End If
      End Set
    End Property

    Public Property isEMSX() As Boolean Implements ITradeFileEntry.isEMSX
      Get
        Return False
      End Get
      Set(ByVal value As Boolean)

      End Set
    End Property

  End Class


  ''' <summary>
  ''' Class EMSX_Order_Class
  ''' </summary>
  Public Class EMSX_Order_Class
    Implements ITradeFileEntry

    Private _Amount As String
    Private _AmountCurrency As String
    Private _CustomerReference As String
    Private _ISIN As String
    Private _isValid As Boolean
    Private _Quantity As String
    Private _StatusString As String
    Private _TradeString As String
    Private _TransactionType As RenaissanceGlobals.TransactionTypes

    Public Sub New()

      Try
        _Amount = "0"
        _AmountCurrency = ""
        _CustomerReference = ""
        _ISIN = ""
        _isValid = True
        _Quantity = "0"
        _StatusString = ""
        _TradeString = ""
        _TransactionType = 0

      Catch ex As Exception
      End Try
    End Sub

    Public Property Amount() As String Implements ITradeFileEntry.Amount
      Get
        Return _Amount
      End Get
      Set(ByVal value As String)
        _Amount = value
      End Set
    End Property

    Public Property AmountCurrency() As String Implements ITradeFileEntry.AmountCurrency
      Get
        Return _AmountCurrency
      End Get
      Set(ByVal value As String)
        _AmountCurrency = value
      End Set
    End Property

    Public Property CustomerReference() As String Implements ITradeFileEntry.CustomerReference
      Get
        Return _CustomerReference
      End Get
      Set(ByVal value As String)
        _CustomerReference = value
      End Set
    End Property

    Public Property ISIN() As String Implements ITradeFileEntry.ISIN
      Get
        Return _ISIN
      End Get
      Set(ByVal value As String)
        _ISIN = value
      End Set
    End Property

    Public Property isValid() As Boolean Implements ITradeFileEntry.isValid
      Get
        Return _isValid
      End Get
      Set(ByVal value As Boolean)
        _isValid = value
      End Set
    End Property

    Public Property Quantity() As String Implements ITradeFileEntry.Quantity
      Get
        Return _Quantity
      End Get
      Set(ByVal value As String)
        _Quantity = value
      End Set
    End Property

    Public Property StatusString() As String Implements ITradeFileEntry.StatusString
      Get
        Return _StatusString
      End Get
      Set(ByVal value As String)
        _StatusString = value
      End Set
    End Property

    Public Property TradeString() As String Implements ITradeFileEntry.TradeString
      Get
        Return _TradeString
      End Get
      Set(ByVal value As String)

      End Set
    End Property

    Public Property TransactionType() As RenaissanceGlobals.TransactionTypes Implements ITradeFileEntry.TransactionType
      Get
        Return _TransactionType
      End Get
      Set(ByVal value As RenaissanceGlobals.TransactionTypes)
        _TransactionType = value
      End Set
    End Property

    Public Property isEMSX() As Boolean Implements ITradeFileEntry.isEMSX
      Get
        Return True
      End Get
      Set(ByVal value As Boolean)

      End Set
    End Property

  End Class


  ''' <summary>
  ''' Gets the transaction units or settlement.
  ''' </summary>
  ''' <param name="pValueOrAmount">The p value or amount.</param>
  ''' <param name="pUnits">The p units.</param>
  ''' <param name="pPrice">The p price.</param>
  ''' <param name="Multiplier">The multiplier.</param>
  ''' <param name="pCosts">The p costs.</param>
  ''' <param name="pSettlement">The p settlement.</param>
  ''' <param name="pCashMovement">The p cash movement.</param>
  ''' <returns>System.Double.</returns>
  Public Function GetTransactionUnitsOrSettlement(ByVal pValueOrAmount As String, ByVal pUnits As Double, ByVal pPrice As Double, ByVal Multiplier As Double, ByVal BrokerFees As Double, ByVal pCosts As Double, ByVal pSettlement As Double, ByVal pCashMovement As Double) As Double
    Dim RVal As Double

    Try
      If pValueOrAmount.ToUpper.StartsWith("V") Then
        ' Value - Calculate Units
        If pPrice = 0 Then
          RVal = 0
        Else
          RVal = (((pSettlement) + ((pCosts + BrokerFees) * pCashMovement)) / (Math.Abs(pPrice) * Multiplier))
        End If

      Else
        ' Amount - Calculate Settlement

        RVal = ((pUnits) * Math.Abs(pPrice) * Multiplier) - ((pCosts + BrokerFees) * pCashMovement)
      End If
    Catch ex As Exception
      RVal = 0
    End Try

    Return RVal

  End Function

  ''' <summary>
  ''' Gets the cash move.
  ''' </summary>
  ''' <param name="Permission_Area">The permission_ area.</param>
  ''' <param name="pMainform">The p mainform.</param>
  ''' <param name="pTransactionTypeID">The p transaction type ID.</param>
  ''' <returns>System.Int32.</returns>
  Public Function GetCashMove(ByVal Permission_Area As String, ByRef pMainform As VeniceMain, ByVal pTransactionTypeID As Integer) As Integer
    Dim CashMovement As Integer
    Dim thisTable As RenaissanceDataClass.DSTransactionType.tblTransactionTypeDataTable
    Dim SelectedRows() As RenaissanceDataClass.DSTransactionType.tblTransactionTypeRow

    ' Return Cash Movement associated with a given Transaction type.
    ' Default Return Value of 1 (One).

    Try
      CashMovement = 1

      If (pMainform IsNot Nothing) Then

        thisTable = CType(pMainform.Load_Table(RenaissanceStandardDatasets.tblTransactionType, False), RenaissanceDataClass.DSTransactionType).tblTransactionType

        If thisTable Is Nothing Then
          pMainform.LogError(Permission_Area & ",GetCashMove()", LOG_LEVELS.Warning, "", "Failed to load TransactionType Table", "", True)
        End If
        If thisTable.Rows.Count > 0 Then
          SelectedRows = thisTable.Select("TransactionTypeID=" & pTransactionTypeID)
          If SelectedRows.Length > 0 Then
            CashMovement = CInt(SelectedRows(0)("TransactionTypeCashMove"))
          End If
        End If

      End If

    Catch ex As Exception
      pMainform.LogError(Permission_Area & ",GetCashMove()", LOG_LEVELS.Error, ex.Message, "Error in GetCashMove().", ex.StackTrace, True)
    End Try

    Return CashMovement

  End Function

  ''' <summary>
  ''' Gets the size of the instrument contract.
  ''' </summary>
  ''' <param name="Permission_Area">The permission_ area.</param>
  ''' <param name="pMainform">The p mainform.</param>
  ''' <param name="pInstrumentID">The p instrument ID.</param>
  ''' <returns>System.Double.</returns>
  Public Function GetInstrumentContractSize(ByVal Permission_Area As String, ByRef pMainform As VeniceMain, ByVal pInstrumentID As Integer) As Double
    Dim InstrumentContractSize As Double
    Dim thisTable As RenaissanceDataClass.DSInstrument.tblInstrumentDataTable
    Dim SelectedRows() As RenaissanceDataClass.DSInstrument.tblInstrumentRow

    ' Return combined InstrumentContractSize & Multiplier associated with a given Instrument.
    ' Default Return Value of 1.0

    Try

      InstrumentContractSize = 1.0#

      If (pMainform IsNot Nothing) Then ' MainForm may be nothing during the initial Form initialisation.

        thisTable = CType(pMainform.Load_Table(RenaissanceStandardDatasets.tblInstrument, False), RenaissanceDataClass.DSInstrument).tblInstrument

        If thisTable Is Nothing Then
          pMainform.LogError(Permission_Area & ",GetInstrumentContractSize()", LOG_LEVELS.Warning, "", "Failed to load the Instruments Table", "", True)
        End If
        If thisTable.Rows.Count > 0 Then
          SelectedRows = thisTable.Select("InstrumentID=" & pInstrumentID)
          If SelectedRows.Length > 0 Then
            InstrumentContractSize = SelectedRows(0).InstrumentContractSize * SelectedRows(0).InstrumentMultiplier
          End If
        End If

      End If

    Catch ex As Exception
      pMainform.LogError(Permission_Area & ",GetInstrumentContractSize()", LOG_LEVELS.Error, ex.Message, "Error in GetInstrumentContractSize().", ex.StackTrace, True)
      InstrumentContractSize = 1.0#
    End Try

    Return InstrumentContractSize

  End Function

  ''' <summary>
  ''' Gets the expected workflow rule.
  ''' </summary>
  ''' <param name="pMainform">The p mainform.</param>
  ''' <param name="FundID">The fund ID.</param>
  ''' <param name="InstrumentType">Type of the instrument.</param>
  ''' <param name="Exchange">The exchange.</param>
  ''' <param name="Currency">The currency.</param>
  ''' <param name="InstrumentID">The instrument ID.</param>
  ''' <param name="CurrentStatus">The current status.</param>
  ''' <param name="IsNewTrade">if set to <c>true</c> [is new trade].</param>
  ''' <param name="HasApproval">if set to <c>true</c> [has approval].</param>
  ''' <returns>RenaissanceDataClass.DSWorkflowRules.tblWorkflowRulesRow.</returns>
  Public Function GetExpectedWorkflowRule(ByRef pMainform As VeniceMain, ByVal FundID As Integer, ByVal InstrumentType As Integer, ByVal Exchange As Integer, ByVal Currency As Integer, ByVal InstrumentID As Integer, ByVal CurrentStatus As Integer, ByVal IsNewTrade As Boolean, ByVal HasApproval As Boolean) As RenaissanceDataClass.DSWorkflowRules.tblWorkflowRulesRow
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Try
      Dim dsRules As RenaissanceDataClass.DSWorkflowRules
      Dim tblRules As RenaissanceDataClass.DSWorkflowRules.tblWorkflowRulesDataTable
      Dim SelectedRows() As RenaissanceDataClass.DSWorkflowRules.tblWorkflowRulesRow
      Dim SelectString As String

      dsRules = CType(pMainform.Load_Table(RenaissanceStandardDatasets.tblWorkflowRules, False), RenaissanceDataClass.DSWorkflowRules)

      If (dsRules Is Nothing) Then
        Return Nothing
      End If

      tblRules = dsRules.tblWorkflowRules

      SelectString = "((Fund=0) OR (Fund=" & CInt(Nz(FundID, 0)).ToString() & ")) AND " & _
      "((InstrumentType=0) OR (InstrumentType=" & CInt(Nz(InstrumentType, 0)).ToString() & ")) AND " & _
      "((Exchange=0) OR (Exchange=" & CInt(Nz(Exchange, 0)).ToString() & ")) AND " & _
      "((Currency=0) OR (Currency=" & CInt(Nz(Currency, 0)).ToString() & ")) AND " & _
      "((InstrumentID=0) OR (InstrumentID=" & CInt(Nz(InstrumentID, 0)).ToString() & ")) AND " & _
      "((CurrentStatus=0) OR (CurrentStatus=" & CInt(Nz(CurrentStatus, 0)).ToString() & ")) "

      If CBool(Nz(HasApproval, False)) = True Then
        SelectString &= "AND (IsApprovalRule=0) "
      End If

      If (CBool(Nz(IsNewTrade, True)) = False) Then
        SelectString &= "AND (IsNewTrade=0) "
      Else
        SelectString &= "AND (IsNewTrade<>0) "
      End If

      ' Get Rows

      SelectedRows = tblRules.Select(SelectString, "WorkflowPriority DESC")

      '

      If SelectedRows.Length > 0 Then
        Return SelectedRows(0)
      Else
        Return Nothing
      End If

    Catch ex As Exception
      Call pMainform.LogError("GetExpectedWorkflow()", LOG_LEVELS.Error, ex.Message, "Error in GetExpectedWorkflow()", ex.StackTrace, True)
    End Try

    Return Nothing

  End Function

  ''' <summary>
  ''' Gets the expected broker account.
  ''' </summary>
  ''' <param name="pMainform">The p mainform.</param>
  ''' <param name="FundID">The fund ID.</param>
  ''' <param name="InstrumentType">Type of the instrument.</param>
  ''' <param name="Exchange">The exchange.</param>
  ''' <param name="Currency">The currency.</param>
  ''' <param name="InstrumentID">The instrument ID.</param>
  ''' <returns>RenaissanceDataClass.DSBrokerAccounts.tblBrokerAccountsRow.</returns>
  Public Function GetExpectedBrokerAccount(ByRef pMainform As VeniceMain, ByVal FundID As Integer, ByVal InstrumentType As Integer, ByVal BrokerID As Integer, ByVal Exchange As Integer, ByVal Currency As Integer, ByVal InstrumentID As Integer) As RenaissanceDataClass.DSBrokerAccounts.tblBrokerAccountsRow
    ' *******************************************************************************
    ' Get 'Best' broker account for the given transaction details.
    '
    ' *******************************************************************************

    Try
      Dim dsAccounts As RenaissanceDataClass.DSBrokerAccounts
      Dim tblAccounts As RenaissanceDataClass.DSBrokerAccounts.tblBrokerAccountsDataTable
      Dim SelectedAccounts() As RenaissanceDataClass.DSBrokerAccounts.tblBrokerAccountsRow
      Dim SelectString As String

      dsAccounts = CType(pMainform.Load_Table(RenaissanceStandardDatasets.tblBrokerAccounts, False), RenaissanceDataClass.DSBrokerAccounts)

      If (dsAccounts Is Nothing) Then
        Return Nothing
      End If

      tblAccounts = dsAccounts.tblBrokerAccounts

      SelectString = IIf(BrokerID = 0, "", "(BrokerID=" & CInt(Nz(BrokerID, 0)).ToString() & ") AND ") & _
        "((FundID=0) OR (FundID=" & CInt(Nz(FundID, 0)).ToString() & ")) AND " & _
        "((InstrumentTypeID=0) OR (InstrumentTypeID=" & CInt(Nz(InstrumentType, 0)).ToString() & ")) AND " & _
        "((Exchange=0) OR (Exchange=" & CInt(Nz(Exchange, 0)).ToString() & ")) AND " & _
        "((CurrencyID=0) OR (CurrencyID=" & CInt(Nz(Currency, 0)).ToString() & ")) AND " & _
        "((InstrumentID=0) OR (InstrumentID=" & CInt(Nz(InstrumentID, 0)).ToString() & "))"

      ' Get Rows

      SelectedAccounts = tblAccounts.Select(SelectString, "BrokerPriority DESC")

      '

      If SelectedAccounts.Length > 0 Then
        Return SelectedAccounts(0)
      Else
        Return Nothing
      End If

    Catch ex As Exception
      Call pMainform.LogError("GetExpectedBrokerAccount()", LOG_LEVELS.Error, ex.Message, "Error in GetExpectedBrokerAccount()", ex.StackTrace, True)
    End Try

    Return Nothing

  End Function

  ''' <summary>
  ''' Trades the status is in group.
  ''' </summary>
  ''' <param name="Mainform">The mainform.</param>
  ''' <param name="TransactionTradeStatusID">The transaction trade status ID.</param>
  ''' <param name="GroupID">The group ID.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function TradeStatusIsInGroup(ByRef Mainform As VeniceMain, ByVal TransactionTradeStatusID As Integer, ByVal GroupID As Integer) As Boolean
    ' *******************************************************************************
    ' Return True / False depending on whether or not the given Trade Status ID is in the given Trade Status group.
    ' *******************************************************************************

    Try
      Dim StatusItemsDS As RenaissanceDataClass.DSTradeStatusGroupItems
      Dim SelectedRows() As RenaissanceDataClass.DSTradeStatusGroupItems.tblTradeStatusGroupItemsRow = Nothing

      StatusItemsDS = CType(Mainform.Load_Table(RenaissanceStandardDatasets.tblTradeStatusGroupItems, False), RenaissanceDataClass.DSTradeStatusGroupItems)

      If (StatusItemsDS IsNot Nothing) Then
        SelectedRows = StatusItemsDS.tblTradeStatusGroupItems.Select("GroupID=" & GroupID.ToString & " AND StatusID=" & TransactionTradeStatusID.ToString)
      End If

      If (SelectedRows IsNot Nothing) AndAlso (SelectedRows.Length > 0) Then
        Return True
      Else
        Return False
      End If

    Catch ex As Exception
      Call Mainform.LogError("TradeStatusIsInGroup()", LOG_LEVELS.Error, ex.Message, "Error checking Trade Status Group.", ex.StackTrace, True)
    End Try

    Return False

  End Function

  ''' <summary>
  ''' Sets the transaction status.
  ''' </summary>
  ''' <param name="Mainform">The mainform.</param>
  ''' <param name="TransactionParentID">The transaction parent ID.</param>
  ''' <param name="StatusID">The status ID.</param>
  ''' <param name="CancelTrade">if set to <c>true</c> [cancel trade].</param>
  ''' <param name="Confirmed">if set to <c>true</c> [confirmed].</param>
  ''' <param name="ConfirmationDate">The confirmation date.</param>
  ''' <param name="Comment">The comment.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Friend Function SetTransactionStatus(ByRef Mainform As VeniceMain, ByVal TransactionParentID As Integer, ByVal StatusID As Integer, ByVal CancelTrade As Boolean, ByVal Confirmed As Boolean, ByVal ConfirmationDate As Date, ByVal Comment As String) As Boolean
    ' *******************************************************************************
    ' Function to call the SQL adp_tblTransaction_SetTradeStatus procedure
    '
    ' This function is a quick call to alter just the trade status (and possibly to cancel) a transaction.
    ' *******************************************************************************
    Dim ThisCommand As SqlCommand = Nothing
    Dim ThisConnection As SqlConnection = Nothing
    Dim Rval As Boolean = False
    Dim pConfirmationDate As Date = RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW

    Try
      If (Confirmed) Then
        pConfirmationDate = ConfirmationDate
      End If

      ThisConnection = Mainform.GetVeniceConnection

      ThisCommand = New SqlCommand("adp_tblTransaction_SetTradeStatus", ThisConnection)
      ThisCommand.CommandType = CommandType.StoredProcedure

      ThisCommand.Parameters.Add("@TransactionParentID", SqlDbType.Int, 4).Value = TransactionParentID
      ThisCommand.Parameters.Add("@TransactionTradeStatusID", SqlDbType.Int, 4).Value = StatusID
      ThisCommand.Parameters.Add("@SetToZero", SqlDbType.Int, 4).Value = CInt(CancelTrade)
      ThisCommand.Parameters.Add("@ConfirmedTrade", SqlDbType.Int, 4).Value = CInt(Confirmed)
      ThisCommand.Parameters.Add("@ConfirmationDate", SqlDbType.Date).Value = pConfirmationDate
      ThisCommand.Parameters.Add("@Comment", SqlDbType.VarChar, 500).Value = Comment
      ThisCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime, 8).Value = Mainform.Main_Knowledgedate

      If (ThisConnection IsNot Nothing) Then
        Call ThisCommand.ExecuteNonQuery()
      End If

      Rval = True

    Catch ex As Exception
      Call Mainform.LogError("SetTransactionStatus()", LOG_LEVELS.Error, ex.Message, "Error setting Transaction status.", ex.StackTrace, True)
    Finally
      Try
        If (ThisConnection IsNot Nothing) Then
          ThisConnection.Close()
        End If
      Catch ex As Exception
      End Try

      ThisCommand = Nothing
    End Try

    Return Rval

  End Function

  Friend Function SetTransactionStatusAndValueDate(ByRef Mainform As VeniceMain, ByVal TransactionParentID As Integer, ByVal StatusID As Integer, ByVal CancelTrade As Boolean, ByVal Confirmed As Boolean, ByVal ConfirmationDate As Date, ByVal Comment As String, ByVal newValueDate As Date, ByVal newSettlementDate As Date) As Boolean
    ' *******************************************************************************
    ' Function to call the SQL adp_tblTransaction_SetTradeStatus procedure
    '
    ' This function is a quick call to alter just the trade status (and possibly to cancel) a transaction.
    ' *******************************************************************************
    Dim ThisCommand As SqlCommand = Nothing
    Dim ThisConnection As SqlConnection = Nothing
    Dim Rval As Boolean = False
    Dim pConfirmationDate As Date = RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW

    Try
      If (Confirmed) Then
        pConfirmationDate = ConfirmationDate
      End If

      ThisConnection = Mainform.GetVeniceConnection

      ThisCommand = New SqlCommand("adp_tblTransaction_SetTradeStatusAndValueDate", ThisConnection)
      ThisCommand.CommandType = CommandType.StoredProcedure

      ThisCommand.Parameters.Add("@TransactionParentID", SqlDbType.Int, 4).Value = TransactionParentID
      ThisCommand.Parameters.Add("@TransactionTradeStatusID", SqlDbType.Int, 4).Value = StatusID
      ThisCommand.Parameters.Add("@SetToZero", SqlDbType.Int, 4).Value = CInt(CancelTrade)
      ThisCommand.Parameters.Add("@ConfirmedTrade", SqlDbType.Int, 4).Value = CInt(Confirmed)
      ThisCommand.Parameters.Add("@ConfirmationDate", SqlDbType.Date).Value = pConfirmationDate
      ThisCommand.Parameters.Add("@Comment", SqlDbType.VarChar, 500).Value = Comment
      ThisCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime, 8).Value = Mainform.Main_Knowledgedate

      ThisCommand.Parameters.Add("@newValueDate", SqlDbType.DateTime, 8).Value = newValueDate
      ThisCommand.Parameters.Add("@newSettlementDate", SqlDbType.DateTime, 8).Value = newSettlementDate

      If (ThisConnection IsNot Nothing) Then
        Call ThisCommand.ExecuteNonQuery()
      End If

      Rval = True

    Catch ex As Exception
      Call Mainform.LogError("SetTransactionStatus()", LOG_LEVELS.Error, ex.Message, "Error setting Transaction status.", ex.StackTrace, True)
    Finally
      Try
        If (ThisConnection IsNot Nothing) Then
          ThisConnection.Close()
        End If
      Catch ex As Exception
      End Try

      ThisCommand = Nothing
    End Try

    Return Rval

  End Function

  ''' <summary>
  ''' Gets the trade object.
  ''' </summary>
  ''' <param name="Mainform">The mainform.</param>
  ''' <param name="TransactionParentID">The transaction parent ID.</param>
  ''' <param name="TransactionRow">The transaction row.</param>
  ''' <param name="WorkflowRuleID">The workflow rule ID.</param>
  ''' <param name="WorkflowID">The workflow ID.</param>
  ''' <param name="BrokerID">The broker ID.</param>
  ''' <param name="BrokerAccountDescription">The broker account description.</param>
  ''' <param name="BrokerAccountNumber">The broker account number.</param>
  ''' <returns>ITradeFileEntry.</returns>
  Friend Function GetTradeObject(ByRef Mainform As VeniceMain, ByVal TransactionParentID As Integer, ByVal TransactionRow As DataRow, ByVal WorkflowRuleID As Integer, ByVal WorkflowID As Integer, ByRef BrokerID As Integer, ByRef VeniceAccountID As Integer, ByRef BrokerAccountDescription As String, ByRef BrokerAccountNumber As String) As ITradeFileEntry
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Dim tempOrderClass As ITradeFileEntry
    Dim ThisBroker As RenaissanceDataClass.DSBroker.tblBrokerRow = Nothing
    Dim ThisBrokerAccount As RenaissanceDataClass.DSBrokerAccounts.tblBrokerAccountsRow = Nothing
    Dim TransactionFund As Integer = 0
    Dim TransactionInstrument As Integer = 0
    Dim thisInstrumentRow As RenaissanceDataClass.DSInstrument.tblInstrumentRow = Nothing
    Dim TransactionExchangeID As Integer = 0
    Dim CounterpartyID As Integer
    Dim BrokerStrategy As String
    Try
      ' OK.
      If (TransactionRow Is Nothing) Then
        tempOrderClass = New BNP_Topic_Order_Class() ' Arbitrary class for failure.
        tempOrderClass.isValid = False
        tempOrderClass.StatusString = TransactionParentID.ToString() & " : Trade details not supplied, could not create file entry."
        Return tempOrderClass
        Exit Function
      End If

      ' Trade Details : 

      If (TransactionRow.Table.Columns.Contains("TransactionFund")) Then
        TransactionFund = CInt(Nz(TransactionRow("TransactionFund"), 0))
      ElseIf (TransactionRow.Table.Columns.Contains("FundID")) Then
        TransactionFund = CInt(Nz(TransactionRow("FundID"), 0))
      End If

      '

      If (TransactionRow.Table.Columns.Contains("TransactionBroker")) Then
        BrokerID = CInt(Nz(TransactionRow("TransactionBroker"), 0))
      Else
        BrokerID = 0
      End If

      '

      If (TransactionRow.Table.Columns.Contains("TransactionInstrument")) Then
        TransactionInstrument = CInt(Nz(TransactionRow("TransactionInstrument"), 0))
      ElseIf (TransactionRow.Table.Columns.Contains("InstrumentID")) Then
        TransactionInstrument = CInt(Nz(TransactionRow("InstrumentID"), 0))
      End If

      thisInstrumentRow = CType(LookupTableRow(Mainform, RenaissanceStandardDatasets.tblInstrument, TransactionInstrument), RenaissanceDataClass.DSInstrument.tblInstrumentRow)


      'lotfi: if instrument is not a Fund ( for fund typed instruments the broker is bnp paris) then
      '   check if countreparty != MAARKET (1) and (BrokerId or brokerStyrategy) is not defined then return error

      If (TransactionRow.Table.Columns.Contains("TransactionCounterparty")) Then
        CounterpartyID = CInt(Nz(TransactionRow("TransactionCounterparty"), 0))
      Else
        CounterpartyID = 0
      End If

      If (TransactionRow.Table.Columns.Contains("TransactionBrokerStrategy")) Then
        BrokerStrategy = TransactionRow("TransactionBrokerStrategy")
      Else
        BrokerStrategy = ""
      End If

      If thisInstrumentRow.InstrumentType <> 1 Then

        If BrokerID = 0 Then
          If CounterpartyID = 0 Or CounterpartyID = 1 Then ' no counterparty or counterparty is MARKET
            tempOrderClass = New BNP_Topic_Order_Class()
            tempOrderClass.isValid = False
            tempOrderClass.StatusString = TransactionParentID.ToString() & " : Neither Broker nor specific Couterparty is defined !."
            Return tempOrderClass
            Exit Function
          End If
        ElseIf BrokerStrategy = "" Then
          tempOrderClass = New BNP_Topic_Order_Class()
          tempOrderClass.isValid = False
          tempOrderClass.StatusString = TransactionParentID.ToString() & " : Broker Strategy is not defined."
          Return tempOrderClass
          Exit Function
        End If

      End If

      ' Which Account

      ThisBrokerAccount = GetExpectedBrokerAccount(Mainform, TransactionFund, thisInstrumentRow.InstrumentType, BrokerID, TransactionExchangeID, thisInstrumentRow.InstrumentCurrencyID, TransactionInstrument)

      ' OK, Get Account Details

      If (ThisBrokerAccount Is Nothing) Then
        tempOrderClass = New BNP_Topic_Order_Class()
        tempOrderClass.isValid = False
        tempOrderClass.StatusString = TransactionParentID.ToString() & " : Could not resolve Broker account Details."
        Return tempOrderClass
        Exit Function
      End If

      BrokerID = ThisBrokerAccount.BrokerID
      BrokerAccountDescription = ThisBrokerAccount.AccountDescription
      BrokerAccountNumber = ThisBrokerAccount.AccountNumber
      VeniceAccountID = ThisBrokerAccount.VeniceAccountID

      Select Case CType(ThisBrokerAccount.BrokerID, ExecutionBrokers)

        Case ExecutionBrokers.BNP_Paris

          Return GetTradeObject_BNP_Paris_Topic(Mainform, TransactionParentID, TransactionRow, ThisBrokerAccount)

        Case ExecutionBrokers.BNP_Luxembourg

          Return GetTradeObject_BNP_Luxembourg_FDS(Mainform, TransactionParentID, TransactionRow, ThisBrokerAccount)

        Case Else

          ThisBroker = CType(LookupTableRow(Mainform, RenaissanceStandardDatasets.tblBroker, BrokerID), RenaissanceDataClass.DSBroker.tblBrokerRow)

          If (ThisBroker IsNot Nothing) AndAlso (ThisBroker.IsEMSX) Then

            Return GetTradeObject_EMSX(Mainform, TransactionParentID, TransactionRow, ThisBroker, ThisBrokerAccount)

          End If

      End Select

    Catch ex As Exception
      Mainform.LogError("GetTradeObject()", LOG_LEVELS.Error, "Error building Trade file entry.", ex.Message, ex.StackTrace)

      tempOrderClass = New BNP_Topic_Order_Class() ' Arbitrary class for failure.
      tempOrderClass.isValid = False
      tempOrderClass.StatusString = TransactionParentID.ToString() & " : Error : " & ex.Message
      Return tempOrderClass
    End Try

    tempOrderClass = New BNP_Topic_Order_Class() ' Arbitrary class for failure.
    tempOrderClass.isValid = False
    tempOrderClass.StatusString = TransactionParentID.ToString() & " : Error, Unhandled Broker / Transaction details. "
    Return tempOrderClass


  End Function

  ''' <summary>
  ''' Gets the trade object_ BN p_ paris_ topic.
  ''' </summary>
  ''' <param name="Mainform">The mainform.</param>
  ''' <param name="TransactionParentID">The transaction parent ID.</param>
  ''' <param name="TransactionRow">The transaction row.</param>
  ''' <param name="ThisBrokerAccount">The this broker account.</param>
  ''' <returns>ITradeFileEntry.</returns>
  Friend Function GetTradeObject_BNP_Paris_Topic(ByRef Mainform As VeniceMain, ByVal TransactionParentID As Integer, ByVal TransactionRow As DataRow, ByVal ThisBrokerAccount As RenaissanceDataClass.DSBrokerAccounts.tblBrokerAccountsRow) As ITradeFileEntry
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Dim tempOrderClass As BNP_Topic_Order_Class
    Dim thisTradeObject As BNP_Topic_Order_Class = Nothing

    Dim ThisBroker As RenaissanceDataClass.DSBroker.tblBrokerRow = Nothing
    Dim thisInstrumentRow As RenaissanceDataClass.DSInstrument.tblInstrumentRow = Nothing
    Dim thisFundRow As RenaissanceDataClass.DSFund.tblFundRow = Nothing

    Dim TransactionFund As Integer = 0
    Dim TransactionAmountOrValue As String = ""
    Dim TransactionType As RenaissanceGlobals.TransactionTypes
    Dim TransactionSettlementCurrencyID As Integer = 0
    Dim TransactionInstrument As Integer = 0
    Dim TransactionExchangeID As Integer = 0
    Dim FundCurrencyID As Integer = 0
    Dim InstrumentCurrencyID As Integer = 0
    Dim InstrumentTypeID As Integer = 0
    Dim TradeCurrency As String = ""
    Dim InstrumentCurrency As String = ""
    Dim InstrumentISIN As String = ""
    Dim InstrumentDescription As String = ""
    Dim IsValueOrAmount As String = ""
    Dim TransactionUnits As Double = 0.0#
    Dim TransactionSettlement As Double = 0.0#
    Dim TransactionFXRate As Double = 1.0#

    Dim CultureName As String = ""
    Dim NumericPrecision As Integer = 6
    ' Dim ImpliedFX As Boolean = False

    Dim TradeFileEntry As String = ""

    Try
      ' OK.

      If (TransactionRow Is Nothing) Then
        tempOrderClass = New BNP_Topic_Order_Class()
        tempOrderClass.IsValid = False
        tempOrderClass.StatusString = TransactionParentID.ToString() & " : Trade details not supplied, could not create file entry."
        Return tempOrderClass
        Exit Function
      End If

      ' ThisWorkflow = LookupTableRow(Mainform, RenaissanceStandardDatasets.tblWorkflows, CInt(Nz(WorkflowID, 0)))

      ' Trade Details : 

      If (TransactionRow.Table.Columns.Contains("TransactionFund")) Then
        TransactionFund = CInt(Nz(TransactionRow("TransactionFund"), 0))
      ElseIf (TransactionRow.Table.Columns.Contains("FundID")) Then
        TransactionFund = CInt(Nz(TransactionRow("FundID"), 0))
      End If

      If (TransactionFund <= 0) Then
        tempOrderClass = New BNP_Topic_Order_Class()
        tempOrderClass.IsValid = False
        tempOrderClass.StatusString = TransactionParentID.ToString() & " : Fund ID = 0, Something wrong with the supplied Transaction details."
        Return tempOrderClass
        Exit Function
      End If

      thisFundRow = CType(LookupTableRow(Mainform, RenaissanceStandardDatasets.tblFund, TransactionFund), RenaissanceDataClass.DSFund.tblFundRow)
      If (thisFundRow IsNot Nothing) Then
        FundCurrencyID = thisFundRow.FundBaseCurrency
      End If

      If (TransactionRow.Table.Columns.Contains("TransactionInstrument")) Then
        TransactionInstrument = CInt(Nz(TransactionRow("TransactionInstrument"), 0))
      ElseIf (TransactionRow.Table.Columns.Contains("InstrumentID")) Then
        TransactionInstrument = CInt(Nz(TransactionRow("InstrumentID"), 0))
      End If

      If (TransactionInstrument <= 0) Then
        tempOrderClass = New BNP_Topic_Order_Class()
        tempOrderClass.IsValid = False
        tempOrderClass.StatusString = TransactionParentID.ToString() & " : Instrument ID = 0, Something wrong with the supplied Transaction details."
        Return tempOrderClass
        Exit Function
      End If

      If (TransactionRow.Table.Columns.Contains("TransactionType")) Then
        TransactionType = CType(CInt(Nz(TransactionRow("TransactionType"), 0)), RenaissanceGlobals.TransactionTypes)
      End If

      If (CInt(TransactionType) <= 0) Then
        tempOrderClass = New BNP_Topic_Order_Class()
        tempOrderClass.IsValid = False
        tempOrderClass.StatusString = TransactionParentID.ToString() & " : Transaction Type ID = 0, Something wrong with the supplied Transaction details."
        Return tempOrderClass
        Exit Function
      End If

      ' TransactionSettlementCurrencyID

      If (TransactionRow.Table.Columns.Contains("TransactionSettlementCurrencyID")) Then
        TransactionSettlementCurrencyID = CInt(Nz(TransactionRow("TransactionSettlementCurrencyID"), 0))
      End If

      ' IsValueOrAmount

      If (TransactionRow.Table.Columns.Contains("TransactionValueorAmount")) Then
        IsValueOrAmount = CStr(Nz(TransactionRow("TransactionValueorAmount"), ""))
      End If

      If (IsValueOrAmount.Length() <= 0) Then
        tempOrderClass = New BNP_Topic_Order_Class()
        tempOrderClass.IsValid = False
        tempOrderClass.StatusString = TransactionParentID.ToString() & " : Transaction Type (Value Or Amount) = '', Something wrong with the supplied Transaction details."
        Return tempOrderClass
        Exit Function
      End If

      If (Left(IsValueOrAmount.ToUpper, 1) <> "V") AndAlso (Left(IsValueOrAmount.ToUpper, 1) <> "A") Then
        tempOrderClass = New BNP_Topic_Order_Class()
        tempOrderClass.IsValid = False
        tempOrderClass.StatusString = TransactionParentID.ToString() & " : Transaction Type is not `Value` Or `Amount`, Something wrong with the supplied Transaction details."
        Return tempOrderClass
        Exit Function
      End If

      ' TransactionUnits : number of shares (or whatever) to trade.
      If (TransactionRow.Table.Columns.Contains("TransactionSignedUnits")) Then
        TransactionUnits = Math.Abs(CDbl(Nz(TransactionRow("TransactionSignedUnits"), 0.0#)))
      End If

      ' TransactionSettlement will be in Settlement Currency
      If (TransactionRow.Table.Columns.Contains("TransactionSignedSettlement")) Then
        TransactionSettlement = Math.Abs(CDbl(Nz(TransactionRow("TransactionSignedSettlement"), 0.0#)))
      End If

      If (TransactionRow.Table.Columns.Contains("TransactionFXRate")) Then
        TransactionFXRate = Math.Abs(CDbl(Nz(TransactionRow("TransactionFXRate"), 1.0#)))
      End If
      If (TransactionFXRate = 0.0#) Then TransactionFXRate = 1.0#

      ' 

      thisInstrumentRow = CType(LookupTableRow(Mainform, RenaissanceStandardDatasets.tblInstrument, TransactionInstrument), RenaissanceDataClass.DSInstrument.tblInstrumentRow)

      If (thisInstrumentRow Is Nothing) Then
        tempOrderClass = New BNP_Topic_Order_Class()
        tempOrderClass.IsValid = False
        tempOrderClass.StatusString = TransactionParentID.ToString() & " : Could not get Instrument details from Instrument ID = " & TransactionInstrument.ToString & "."
        Return tempOrderClass
        Exit Function
      End If

      InstrumentTypeID = thisInstrumentRow.InstrumentType
      InstrumentISIN = Nz(thisInstrumentRow.InstrumentISIN, "")
      InstrumentCurrencyID = Nz(thisInstrumentRow.InstrumentCurrencyID, 0)
      InstrumentDescription = Nz(thisInstrumentRow.InstrumentDescription, "")

      If (InstrumentTypeID <= 0) Then
        tempOrderClass = New BNP_Topic_Order_Class()
        tempOrderClass.IsValid = False
        tempOrderClass.StatusString = TransactionParentID.ToString() & " : Instrument Type ID = 0, Something wrong with the supplied Instrument details."
        Return tempOrderClass
        Exit Function
      End If

      If (Len(InstrumentISIN) <= 0) Then
        tempOrderClass = New BNP_Topic_Order_Class()
        tempOrderClass.IsValid = False
        tempOrderClass.StatusString = TransactionParentID.ToString() & " : Instrument ISIN is blank for " & Nz(thisInstrumentRow.InstrumentDescription, "") & ", " & TransactionInstrument.ToString & "."
        Return tempOrderClass
        Exit Function
      End If

      If (InstrumentCurrencyID <= 0) Then
        tempOrderClass = New BNP_Topic_Order_Class()
        tempOrderClass.IsValid = False
        tempOrderClass.StatusString = TransactionParentID.ToString() & " : Instrument Currency ID = 0, Something wrong with the supplied Transaction details."
        Return tempOrderClass
        Exit Function
      End If

      If (TransactionSettlementCurrencyID <= 0) Then
        TransactionSettlementCurrencyID = InstrumentCurrencyID
      End If

      InstrumentCurrency = CStr(Nz(LookupTableValue(Mainform, RenaissanceStandardDatasets.tblCurrency, InstrumentCurrencyID, "CurrencyCode"), ""))
      TradeCurrency = CStr(Nz(LookupTableValue(Mainform, RenaissanceStandardDatasets.tblCurrency, TransactionSettlementCurrencyID, "CurrencyCode"), ""))

      If (Len(TradeCurrency) <= 0) Then
        tempOrderClass = New BNP_Topic_Order_Class()
        tempOrderClass.IsValid = False
        tempOrderClass.StatusString = TransactionParentID.ToString() & " : Settlement Currency Code = ``, Could not get details from Currency ID = " & TransactionSettlementCurrencyID.ToString & "."
        Return tempOrderClass
        Exit Function
      End If

      ' Broker Details (For culture info and precision)

      ThisBroker = CType(LookupTableRow(Mainform, RenaissanceStandardDatasets.tblBroker, ThisBrokerAccount.BrokerID), RenaissanceDataClass.DSBroker.tblBrokerRow)

      If (ThisBroker IsNot Nothing) Then
        CultureName = ThisBroker.NLS_Culture
        NumericPrecision = ThisBroker.NumericPrecision
      End If

      If (thisInstrumentRow.InstrumentOrderPrecision < NumericPrecision) Then
        NumericPrecision = thisInstrumentRow.InstrumentOrderPrecision
      End If

      ' Generate object

      Select Case CType(ThisBrokerAccount.BrokerID, ExecutionBrokers)

        Case ExecutionBrokers.BNP_Paris

          ' Default values used in the class New() Function. 

          'Public Const PRIMONIAL_BNP_TOPIC_DEFAULT_SENDER_ID As String = "TROPICPFQ"
          'Public Const PRIMONIAL_BNP_TOPIC_DEFAULT_BANK_CODE As String = "30026"
          'Public Const PRIMONIAL_BNP_TOPIC_DEFAULT_AGENCY_CODE As String = "01000"

          ' Re TROPICfile format : 
          ' If trade is expressed as a number of shares (Field 12), then the Currency field (18) specifies settlement currency.
          ' If the Trade is expressed as an amount (Field 17) , then the amount and the currency (18) must be the Instrument currency.
          ' If the Trade is expressed as an amount and Automatic FX to base currency is required, then field 23 must be set to 'O'(Oui).

          thisTradeObject = New BNP_Topic_Order_Class(CultureName, NumericPrecision)

          ' 
          thisTradeObject.OPE_VAL_REFEXT = TransactionParentID.ToString()
          thisTradeObject.DOSS_TI_NO = ThisBrokerAccount.AccountNumber
          thisTradeObject.ACT_ELMT_CD = InstrumentISIN
          thisTradeObject.ACT_LTA_LIB = InstrumentDescription
          thisTradeObject.ORD_OPCVM_CDN_Value = TransactionType   ' Only recognises BUY and SELL types.

          If Left(IsValueOrAmount.ToUpper, 1) = "V" Then ' Value
            ' OK, Value style trades : 
            ' Remember Quantity is cash in Instrument Currency.

            ' Trade by Value (Instrument Value!)
            If (TransactionFXRate = 0.0#) OrElse (TransactionFXRate = 1.0#) Then
              thisTradeObject.ORD_OPCVM_MT_Value = TransactionSettlement
            Else
              thisTradeObject.ORD_OPCVM_MT_Value = TransactionSettlement / TransactionFXRate
            End If

            ' Currency
            thisTradeObject.DEV_REG_ORD_CD = InstrumentCurrency

            ' Auto FX 

            ' Currency is always Instrument currency for 'Amount' trades with BNP.
            If (InstrumentCurrencyID <> TransactionSettlementCurrencyID) Then
              thisTradeObject.CHG_ORD_MT_IND = "O"

              If (TransactionSettlementCurrencyID <> FundCurrencyID) And (FundCurrencyID > 0) Then
                Mainform.LogError("GetTradeObject_BNP_Paris_Topic()", LOG_LEVELS.Warning, "Transaction for Amount has settlement currency other than Fund or Instrument currency. BNP will settle in Fund currency.")
              End If
            Else
              thisTradeObject.CHG_ORD_MT_IND = "N"
            End If

          Else
            ' Trade by Quantity
            thisTradeObject.ORD_OPCVM_QTE_Value = TransactionUnits

            ' Currency
            thisTradeObject.DEV_REG_ORD_CD = TradeCurrency

            ' Auto FX 
            ' N/A
          End If

          TradeFileEntry = thisTradeObject.TradeString

        Case ExecutionBrokers.BNP_Luxembourg

          ' TradeFileEntry = Get_BNP_Luxembourg_TradeEntry(XX)

      End Select


    Catch ex As Exception
      thisTradeObject = New BNP_Topic_Order_Class()
      thisTradeObject.IsValid = False
      thisTradeObject.StatusString = ex.Message

      Call Mainform.LogError("GetTradeObject_BNP_Paris_Topic()", LOG_LEVELS.Error, ex.Message, "Error saving trade details for Electronic Market.", ex.StackTrace, True)
    End Try

    If (thisTradeObject Is Nothing) Then
      thisTradeObject = New BNP_Topic_Order_Class()
      thisTradeObject.IsValid = False
      thisTradeObject.StatusString = "Unknown problem. No error, but no trade object either!"
    End If

    Return thisTradeObject

  End Function

  ''' <summary>
  ''' Gets the trade object_ BN p_ luxembourg_ FDS.
  ''' </summary>
  ''' <param name="Mainform">The mainform.</param>
  ''' <param name="TransactionParentID">The transaction parent ID.</param>
  ''' <param name="TransactionRow">The transaction row.</param>
  ''' <param name="ThisBrokerAccount">The this broker account.</param>
  ''' <returns>ITradeFileEntry.</returns>
  Friend Function GetTradeObject_BNP_Luxembourg_FDS(ByRef Mainform As VeniceMain, ByVal TransactionParentID As Integer, ByVal TransactionRow As DataRow, ByVal ThisBrokerAccount As RenaissanceDataClass.DSBrokerAccounts.tblBrokerAccountsRow) As ITradeFileEntry
    ' *******************************************************************************
    '
    ' *******************************************************************************
    Dim thisTradeObject As BNP_FDS_Order_Class = Nothing
    Dim tempOrderClass As BNP_FDS_Order_Class

    Try
      Dim TransactionInstrument As Integer = 0
      Dim thisInstrumentRow As RenaissanceDataClass.DSInstrument.tblInstrumentRow = Nothing
      Dim ThisBroker As RenaissanceDataClass.DSBroker.tblBrokerRow = Nothing

      Dim TransactionUnits As Double = 0.0#
      Dim TransactionSettlement As Double = 0.0#
      Dim IsValueOrAmount As String = ""
      Dim InstrumentTypeID As Integer = 0
      Dim InstrumentISIN As String = ""
      Dim InstrumentCurrencyID As Integer = 0
      Dim InstrumentDescription As String = ""
      Dim TransactionSettlementCurrencyID As Integer
      Dim TradeCurrency As String = ""
      Dim InstrumentCurrency As String = ""
      Dim TransactionType As RenaissanceGlobals.TransactionTypes

      Dim CultureName As String = ""
      Dim NumericPrecision As Integer = 6

      ' IsValueOrAmount

      If (TransactionRow.Table.Columns.Contains("TransactionValueorAmount")) Then
        IsValueOrAmount = CStr(Nz(TransactionRow("TransactionValueorAmount"), ""))
      End If

      If (IsValueOrAmount.Length() <= 0) Then
        tempOrderClass = New BNP_FDS_Order_Class()
        tempOrderClass.IsValid = False
        tempOrderClass.StatusString = TransactionParentID.ToString() & " : Transaction Type (Value Or Amount) = '', Something wrong with the supplied Transaction details."
        Return tempOrderClass
        Exit Function
      End If

      If (Left(IsValueOrAmount.ToUpper, 1) <> "V") AndAlso (Left(IsValueOrAmount.ToUpper, 1) <> "A") Then
        tempOrderClass = New BNP_FDS_Order_Class()
        tempOrderClass.IsValid = False
        tempOrderClass.StatusString = TransactionParentID.ToString() & " : Transaction Type is not `Value` Or `Amount`, Something wrong with the supplied Transaction details."
        Return tempOrderClass
        Exit Function
      End If

      ' TransactionUnits : number of shares (or whatever) to trade.
      If (TransactionRow.Table.Columns.Contains("TransactionSignedUnits")) Then
        TransactionUnits = Math.Abs(CDbl(Nz(TransactionRow("TransactionSignedUnits"), 0.0#)))
      End If

      ' TransactionSettlement will be in Settlement Currency
      If (TransactionRow.Table.Columns.Contains("TransactionSignedSettlement")) Then
        TransactionSettlement = Math.Abs(CDbl(Nz(TransactionRow("TransactionSignedSettlement"), 0.0#)))
      End If

      '

      If (TransactionRow.Table.Columns.Contains("TransactionInstrument")) Then
        TransactionInstrument = CInt(Nz(TransactionRow("TransactionInstrument"), 0))
      ElseIf (TransactionRow.Table.Columns.Contains("InstrumentID")) Then
        TransactionInstrument = CInt(Nz(TransactionRow("InstrumentID"), 0))
      End If

      thisInstrumentRow = CType(LookupTableRow(Mainform, RenaissanceStandardDatasets.tblInstrument, TransactionInstrument), RenaissanceDataClass.DSInstrument.tblInstrumentRow)

      If (thisInstrumentRow Is Nothing) Then
        tempOrderClass = New BNP_FDS_Order_Class()
        tempOrderClass.IsValid = False
        tempOrderClass.StatusString = TransactionParentID.ToString() & " : Could not get Instrument details from Instrument ID = " & TransactionInstrument.ToString & "."
        Return tempOrderClass
        Exit Function
      End If


      InstrumentTypeID = thisInstrumentRow.InstrumentType
      InstrumentISIN = Nz(thisInstrumentRow.InstrumentISIN, "")
      InstrumentCurrencyID = Nz(thisInstrumentRow.InstrumentCurrencyID, 0)
      InstrumentDescription = Nz(thisInstrumentRow.InstrumentDescription, "")

      If (Len(InstrumentISIN) <= 0) Then
        tempOrderClass = New BNP_FDS_Order_Class()
        tempOrderClass.IsValid = False
        tempOrderClass.StatusString = TransactionParentID.ToString() & " : Instrument ISIN is blank for " & Nz(thisInstrumentRow.InstrumentDescription, "") & ", " & TransactionInstrument.ToString & "."
        Return tempOrderClass
        Exit Function
      End If

      If (TransactionRow.Table.Columns.Contains("TransactionType")) Then
        TransactionType = CType(CInt(Nz(TransactionRow("TransactionType"), 0)), RenaissanceGlobals.TransactionTypes)
      End If

      ' TransactionSettlementCurrencyID

      If (TransactionRow.Table.Columns.Contains("TransactionSettlementCurrencyID")) Then
        TransactionSettlementCurrencyID = CInt(Nz(TransactionRow("TransactionSettlementCurrencyID"), 0))
      End If

      If (TransactionSettlementCurrencyID <= 0) Then
        TransactionSettlementCurrencyID = InstrumentCurrencyID
      End If

      InstrumentCurrency = CStr(Nz(LookupTableValue(Mainform, RenaissanceStandardDatasets.tblCurrency, InstrumentCurrencyID, "CurrencyCode"), ""))
      TradeCurrency = CStr(Nz(LookupTableValue(Mainform, RenaissanceStandardDatasets.tblCurrency, TransactionSettlementCurrencyID, "CurrencyCode"), ""))

      ' Broker Details (For culture info and precision)

      ThisBroker = CType(LookupTableRow(Mainform, RenaissanceStandardDatasets.tblBroker, ThisBrokerAccount.BrokerID), RenaissanceDataClass.DSBroker.tblBrokerRow)

      If (ThisBroker IsNot Nothing) Then
        CultureName = ThisBroker.NLS_Culture
        NumericPrecision = ThisBroker.NumericPrecision
      End If

      If (thisInstrumentRow.InstrumentOrderPrecision < NumericPrecision) Then
        NumericPrecision = thisInstrumentRow.InstrumentOrderPrecision
      End If

      ' OK, Here we go....

      thisTradeObject = New BNP_FDS_Order_Class(CultureName, NumericPrecision)

      thisTradeObject.CodeBank = "PARBLU"
      thisTradeObject.CustomerReference = TransactionParentID.ToString()
      thisTradeObject.AccountNumber = ThisBrokerAccount.AccountNumber
      thisTradeObject.ISIN = InstrumentISIN
      thisTradeObject.TransactionType = TransactionType

      If Left(IsValueOrAmount.ToUpper, 1) = "V" Then ' Value
        thisTradeObject.Amount_Value = TransactionSettlement
      Else
        thisTradeObject.Quantity_Value = TransactionUnits
      End If

      thisTradeObject.PaymentCurrency = TradeCurrency ' Amount currency
      thisTradeObject.QuotationCurrency = InstrumentCurrency
      thisTradeObject.TransferAgentNote = ""

    Catch ex As Exception
      tempOrderClass = New BNP_FDS_Order_Class()
      tempOrderClass.IsValid = False
      tempOrderClass.StatusString = TransactionParentID.ToString() & " : Error : " & ex.Message
      Return tempOrderClass
      Exit Function
    End Try

    Return thisTradeObject

  End Function


  Friend Function GetTradeObject_EMSX(ByRef Mainform As VeniceMain, ByVal TransactionParentID As Integer, ByVal TransactionRow As DataRow, ByVal ThisBroker As RenaissanceDataClass.DSBroker.tblBrokerRow, ByVal ThisBrokerAccount As RenaissanceDataClass.DSBrokerAccounts.tblBrokerAccountsRow) As ITradeFileEntry
    ' *******************************************************************************
    '
    ' *******************************************************************************
    Dim ReturnOrder As New EMSX_Order_Class

    Try
      Dim TransactionInstrument As Integer = 0
      Dim thisInstrumentRow As RenaissanceDataClass.DSInstrument.tblInstrumentRow = Nothing

      Dim CultureName As String = ""
      Dim NumericPrecision As Integer = 6
      Dim TransactionUnits As Double = 0.0#
      Dim TransactionSettlement As Double = 0.0#
      Dim IsValueOrAmount As String = ""
      Dim InstrumentTypeID As Integer = 0
      Dim InstrumentISIN As String = ""
      Dim InstrumentCurrencyID As Integer = 0
      Dim InstrumentDescription As String = ""
      Dim TradeCurrency As String = ""
      Dim InstrumentCurrency As String = ""
      Dim InstrumentExchangeCode As String = ""

      Dim TransactionType As RenaissanceGlobals.TransactionTypes

      If (TransactionRow.Table.Columns.Contains("TransactionValueorAmount")) Then
        IsValueOrAmount = CStr(Nz(TransactionRow("TransactionValueorAmount"), ""))
      End If

      If (IsValueOrAmount.Length() <= 0) Then
        ReturnOrder.isValid = False
        ReturnOrder.StatusString = TransactionParentID.ToString() & " : Transaction Type (Value Or Amount) = '', Something wrong with the supplied Transaction details."
        Return ReturnOrder
        Exit Function
      End If

      ' TransactionUnits : number of shares (or whatever) to trade.
      If (TransactionRow.Table.Columns.Contains("TransactionSignedUnits")) Then
        TransactionUnits = Math.Abs(CDbl(Nz(TransactionRow("TransactionSignedUnits"), 0.0#)))
      End If

      If (TransactionRow.Table.Columns.Contains("TransactionInstrument")) Then
        TransactionInstrument = CInt(Nz(TransactionRow("TransactionInstrument"), 0))
      ElseIf (TransactionRow.Table.Columns.Contains("InstrumentID")) Then
        TransactionInstrument = CInt(Nz(TransactionRow("InstrumentID"), 0))
      End If

      thisInstrumentRow = CType(LookupTableRow(Mainform, RenaissanceStandardDatasets.tblInstrument, TransactionInstrument), RenaissanceDataClass.DSInstrument.tblInstrumentRow)

      If (thisInstrumentRow Is Nothing) Then
        ReturnOrder.isValid = False
        ReturnOrder.StatusString = TransactionParentID.ToString() & " : Could not get Instrument details from Instrument ID = " & TransactionInstrument.ToString & "."
        Return ReturnOrder
        Exit Function
      End If


      InstrumentTypeID = thisInstrumentRow.InstrumentType
      InstrumentISIN = Nz(thisInstrumentRow.InstrumentISIN, "")
      InstrumentCurrencyID = Nz(thisInstrumentRow.InstrumentCurrencyID, 0)
      InstrumentDescription = Nz(thisInstrumentRow.InstrumentDescription, "")
      InstrumentCurrency = CStr(Nz(LookupTableValue(Mainform, RenaissanceStandardDatasets.tblCurrency, InstrumentCurrencyID, "CurrencyCode"), ""))
      InstrumentExchangeCode = Nz(thisInstrumentRow.InstrumentExchangeCode, "")
      'lotfi: if ticker is empty return immediatly error
      If (InstrumentExchangeCode.Trim().Length() = 0) Then
        ReturnOrder.isValid = False
        ReturnOrder.StatusString = TransactionParentID.ToString() & " : Instrument '" & InstrumentDescription & "' Ticker is empty !"
        Return ReturnOrder
        Exit Function
      End If

      'If (Len(InstrumentISIN) <= 0) Then
      '  ReturnOrder.isValid = False
      '  ReturnOrder.StatusString = TransactionParentID.ToString() & " : Instrument ISIN is blank for " & Nz(thisInstrumentRow.InstrumentDescription, "") & ", " & TransactionInstrument.ToString & "."
      '  Return ReturnOrder
      '  Exit Function
      'End If

      If (TransactionRow.Table.Columns.Contains("TransactionType")) Then
        TransactionType = CType(CInt(Nz(TransactionRow("TransactionType"), 0)), RenaissanceGlobals.TransactionTypes)
      End If

      If (ThisBroker IsNot Nothing) Then
        CultureName = ThisBroker.NLS_Culture
        NumericPrecision = ThisBroker.NumericPrecision
      End If

      If (thisInstrumentRow.InstrumentOrderPrecision < NumericPrecision) Then
        NumericPrecision = thisInstrumentRow.InstrumentOrderPrecision
      End If

      ' OK, Here we go....

      ReturnOrder.CustomerReference = TransactionParentID.ToString()
      ReturnOrder.ISIN = InstrumentISIN
      ReturnOrder.TransactionType = TransactionType

      If Left(IsValueOrAmount.ToUpper, 1) = "V" Then ' Value
        ReturnOrder.Amount = TransactionSettlement.ToString
      Else
        ReturnOrder.Quantity = TransactionUnits.ToString
      End If

      ReturnOrder.AmountCurrency = TradeCurrency ' Amount currency

      Return ReturnOrder

    Catch ex As Exception
      ReturnOrder = New EMSX_Order_Class()
      ReturnOrder.isValid = False
      ReturnOrder.StatusString = TransactionParentID.ToString() & " : Error : " & ex.Message
      Return ReturnOrder
    End Try

  End Function

  ''' <summary>
  ''' Saves the trade file details.
  ''' </summary>
  ''' <param name="Mainform">The mainform.</param>
  ''' <param name="TransactionParentID">The transaction parent ID.</param>
  ''' <param name="TransactionRow">The transaction row.</param>
  ''' <param name="WorkflowRuleID">The workflow rule ID.</param>
  ''' <param name="WorkflowID">The workflow ID.</param>
  ''' <param name="TriggerDate">The trigger date.</param>
  ''' <param name="SuccessFlag">if set to <c>true</c> [success flag].</param>
  ''' <returns>System.String.</returns>
  Friend Function SaveTradeFileDetails(ByRef Mainform As VeniceMain, ByVal TransactionParentID As Integer, ByVal TransactionRow As DataRow, ByVal WorkflowRuleID As Integer, ByVal WorkflowID As Integer, ByVal TriggerDate As Date, ByRef SuccessFlag As Boolean) As String
    ' *******************************************************************************
    '
    ' *******************************************************************************
    SuccessFlag = False

    Dim RVal As String = TransactionParentID.ToString() & " : <None>"

    Try
      ' OK.

      Dim thisTradeObject As ITradeFileEntry
      Dim BrokerID As Integer = 0
      Dim VeniceAccountID As Integer = 0
      Dim BrokerAccountDescription As String = ""
      Dim BrokerAccountNumber As String = ""

      thisTradeObject = GetTradeObject(Mainform, TransactionParentID, TransactionRow, WorkflowRuleID, WorkflowID, BrokerID, VeniceAccountID, BrokerAccountDescription, BrokerAccountNumber)

      If (thisTradeObject Is Nothing) Then
        Return TransactionParentID.ToString() & " : Problem creating Trade Object."
      ElseIf (thisTradeObject.isValid = False) Then
        Return thisTradeObject.StatusString
      End If

      ' Write Trade Entry to table.

      Dim PendingTradeTable As New RenaissanceDataClass.DSTradeFilePending.tblTradeFilePendingDataTable
      Dim PendingTrade As RenaissanceDataClass.DSTradeFilePending.tblTradeFilePendingRow

      PendingTrade = PendingTradeTable.NewtblTradeFilePendingRow

      PendingTrade.BrokerID = BrokerID
      PendingTrade.VeniceAccountID = VeniceAccountID
      PendingTrade.BrokerSpecific = Left(BrokerAccountDescription, 50)
      PendingTrade.TransactionParentID = TransactionParentID
      PendingTrade.WorkflowID = WorkflowID

      ' They (FPascal) did not like this, so I took it out again.....

      '' If the ValueDate is after the trigger date, then delay the trade until the value Date
      'If (TransactionRow.Table.Columns.Contains("TransactionValueDate")) Then
      '  Dim ThisValueDate As Date

      '  ThisValueDate = CDate(TransactionRow("TransactionValueDate"))

      '  If (ThisValueDate.CompareTo(TriggerDate) > 0) Then
      '    PendingTrade.TriggerDate = ThisValueDate.Date.AddHours(6)
      '  Else
      '    PendingTrade.TriggerDate = TriggerDate
      '  End If
      'Else
      '  PendingTrade.TriggerDate = TriggerDate
      'End If

      PendingTrade.TriggerDate = TriggerDate
      PendingTrade.TradefileText = thisTradeObject.TradeString

      PendingTradeTable.Rows.Add(PendingTrade)

      Mainform.AdaptorUpdate("SaveTradeFileDetails()", RenaissanceStandardDatasets.tblTradeFilePending, PendingTradeTable.Select())

      ' EMSX ?

      If (thisTradeObject.isEMSX) Then

        Try
          Dim thisTradeFileEmsxStatusTable As New RenaissanceDataClass.DSTradeFileEmsxStatus.tblTradeFileEmsxStatusDataTable
          Dim OrderDetailsRow As RenaissanceDataClass.DSTradeFileEmsxStatus.tblTradeFileEmsxStatusRow = thisTradeFileEmsxStatusTable.NewtblTradeFileEmsxStatusRow
          Dim ThisBroker As RenaissanceDataClass.DSBroker.tblBrokerRow = Nothing

          ThisBroker = CType(LookupTableRow(Mainform, RenaissanceStandardDatasets.tblBroker, BrokerID), RenaissanceDataClass.DSBroker.tblBrokerRow)

          OrderDetailsRow.TransactionParentID = TransactionParentID
          OrderDetailsRow.SequenceNumber = 0
          OrderDetailsRow.OrderStatus = EMSX_OrderStatus.NotPlaced
          OrderDetailsRow.CorrelationID = 0
          OrderDetailsRow.StatusString = ""
          OrderDetailsRow.EMSX_BrokerID = ThisBroker.EMSX_Broker_ID
          OrderDetailsRow.EMSX_Ticker = ""
          OrderDetailsRow.RouteIDs = ""
          OrderDetailsRow.OrderAmount = Math.Abs(CDbl(Nz(TransactionRow("TransactionSignedUnits"), 0.0#)))
          OrderDetailsRow.FilledAmount = 0

          Mainform.AdaptorUpdate("TransactionUtilities, SaveTradeFileDetails.", RenaissanceStandardDatasets.tblTradeFileEmsxStatus, New DataRow() {OrderDetailsRow})

        Catch ex As Exception
          Call Mainform.LogError("SaveTradeFileDetails()", LOG_LEVELS.Error, ex.Message, "Error saving trade details to TradeFileEmsxStatus table.", ex.StackTrace, True)
        End Try

      End If

      If (PendingTrade.RN <= 0) Then
        RVal = TransactionParentID.ToString() & " : Problem saving to pending trades table. Table Update failed."
      Else
        RVal = TransactionParentID.ToString() & " : Trade saved to pending trades table, (" & PendingTrade.RN.ToString() & ")"
        SuccessFlag = True
      End If

    Catch ex As Exception
      Call Mainform.LogError("SaveTradeFileDetails()", LOG_LEVELS.Error, ex.Message, "Error saving trade details for Electronic Market.", ex.StackTrace, True)
    End Try

    Return RVal

  End Function


  ''' <summary>
  ''' Revokes the trade approval.
  ''' </summary>
  ''' <param name="Mainform">The mainform.</param>
  ''' <param name="TradeFileRN">The trade file RN.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Friend Function RevokeTradeApproval(ByRef Mainform As VeniceMain, ByVal TradeFileRN As Integer) As Boolean
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Dim ThisCommand As SqlCommand = Nothing
    Dim ThisConnection As SqlConnection = Nothing
    Dim Rval As Boolean = False

    Try

      ' adp_tblTradeFilePending_DeleteCommand


      ThisConnection = Mainform.GetVeniceConnection

      ThisCommand = New SqlCommand("adp_tblTradeFilePending_DeleteCommand", ThisConnection)
      ThisCommand.CommandType = CommandType.StoredProcedure

      ThisCommand.Parameters.Add("@RN", SqlDbType.Int, 4).Value = TradeFileRN

      If (ThisConnection IsNot Nothing) Then
        Call ThisCommand.ExecuteNonQuery()
      End If

      Rval = True

    Catch ex As Exception

      Call Mainform.LogError("RevokeTradeApproval()", LOG_LEVELS.Error, ex.Message, "Error Revoving Transaction approval.", ex.StackTrace, True)

    Finally
      Try
        If (ThisConnection IsNot Nothing) Then
          ThisConnection.Close()
        End If
      Catch ex As Exception
      End Try

      ThisCommand = Nothing
    End Try

    Return Rval

  End Function

  ''' <summary>
  ''' Marks the trade as sent.
  ''' </summary>
  ''' <param name="Mainform">The mainform.</param>
  ''' <param name="TradeFileRN">The trade file RN.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Friend Function MarkTradeAsSent(ByRef Mainform As VeniceMain, ByVal TradeFileRN As Integer) As Boolean
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Dim ThisCommand As SqlCommand = Nothing
    Dim ThisConnection As SqlConnection = Nothing
    Dim Rval As Boolean = False

    Try

      If (TradeFileRN > 0) Then

        ' adp_tblTradeFilePending_DeleteCommand

        ThisConnection = Mainform.GetVeniceConnection

        ThisCommand = New SqlCommand("UPDATE [dbo].[tblTradeFilePending] SET [DateDeleted] = getdate(), [Comment] = 'Mark as sent : ' + user_name() WHERE [RN] = " & TradeFileRN.ToString(), ThisConnection)
        ThisCommand.CommandType = CommandType.Text

        If (ThisConnection IsNot Nothing) Then
          Call ThisCommand.ExecuteNonQuery()
        End If

        Rval = True

      End If

    Catch ex As Exception

      Call Mainform.LogError("MarkTradeAsSent()", LOG_LEVELS.Error, ex.Message, "Error in MarkTradeAsSent().", ex.StackTrace, True)

    Finally
      Try
        If (ThisConnection IsNot Nothing) Then
          ThisConnection.Close()
        End If
      Catch ex As Exception
      End Try

      ThisCommand = Nothing
    End Try

    Return Rval

  End Function

End Module
