' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="PriceFunctions.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals

Imports RenaissanceUtilities.DatePeriodFunctions

''' <summary>
''' Class PriceFunctions
''' </summary>
Module PriceFunctions

    ''' <summary>
    ''' Class PriceClass
    ''' </summary>
	Private Class PriceClass
		' ***************************************************** 
		' Simple Class designed to facilitate the 'UpdateTransaction'
		' process.
		' This class is designed to hold Instrument Prices and to allow
		' an indication of whether ot not they have been changed.
		' ***************************************************** 

        ''' <summary>
        ''' The instrument ID
        ''' </summary>
		Public InstrumentID As Integer
        ''' <summary>
        ''' The price date
        ''' </summary>
		Public PriceDate As Date
        ''' <summary>
        ''' The price base date
        ''' </summary>
		Public PriceBaseDate As Date
        ''' <summary>
        ''' The price level
        ''' </summary>
		Public PriceLevel As Double
        ''' <summary>
        ''' The price GAV
        ''' </summary>
		Public PriceGAV As Double
        ''' <summary>
        ''' The is percent
        ''' </summary>
		Public IsPercent As Boolean
        ''' <summary>
        ''' The is milestone
        ''' </summary>
		Public IsMilestone As Boolean
        ''' <summary>
        ''' The price propagated
        ''' </summary>
		Public PricePropagated As Boolean
        ''' <summary>
        ''' The price affected
        ''' </summary>
		Public PriceAffected As Boolean

        ''' <summary>
        ''' Gets the price NAV.
        ''' </summary>
        ''' <value>The price NAV.</value>
		Public ReadOnly Property PriceNAV() As Double
			Get
				Return PriceLevel
			End Get
		End Property

        ''' <summary>
        ''' Initializes a new instance of the <see cref="PriceClass"/> class.
        ''' </summary>
        ''' <param name="pPriceRow">The p price row.</param>
		Public Sub New(ByVal pPriceRow As RenaissanceDataClass.DSPrice.tblPriceRow)
			Me.InstrumentID = pPriceRow.PriceInstrument
			Me.PriceDate = pPriceRow.PriceDate

			If pPriceRow.IsPriceBaseDateNull Then
				Me.PriceBaseDate = KNOWLEDGEDATE_NOW
			Else
				Me.PriceBaseDate = pPriceRow.PriceBaseDate
			End If

			Me.PriceLevel = pPriceRow.PriceNAV ' Transactions Updated using the NAV, not the price (Might be Discounted)
			Me.PriceGAV = pPriceRow.PriceGAV
			Me.IsPercent = pPriceRow.PriceIsPercent
			Me.IsMilestone = pPriceRow.PriceIsMilestone
			Me.PricePropagated = False
		End Sub

	End Class

    ''' <summary>
    ''' Update_s the dependent_ transactions_ milestone.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  ''' <param name="Permission_Area">The permission_ area.</param>
  ''' <param name="p_InstrumentID">The p_ instrument ID.</param>
  ''' <param name="p_PriceDate">The p_ price date.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function Update_Dependent_Transactions_Milestone(ByVal pMainForm As VeniceMain, ByVal Permission_Area As String, ByVal p_InstrumentID As Long, ByVal p_PriceDate As Date) As Boolean
    ' **************************************************************************************
    '
    '
    ' **************************************************************************************

    Try

      Return Update_Dependent_Transactions_All(pMainForm, Permission_Area, p_InstrumentID, New Date() {p_PriceDate}, False)

    Catch ex As Exception
    End Try

    Return False

  End Function

  ''' <summary>
  ''' Update_s the dependent_ transactions_ non milestone.
  ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  ''' <param name="Permission_Area">The permission_ area.</param>
  ''' <param name="p_InstrumentID">The p_ instrument ID.</param>
  ''' <param name="p_PriceDates">The p_ price dates.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function Update_Dependent_Transactions_NonMilestone(ByVal pMainForm As VeniceMain, ByVal Permission_Area As String, ByVal p_InstrumentID As Long, ByVal p_PriceDates() As Date) As Boolean
    ' **************************************************************************************
    '
    '
    ' **************************************************************************************

    Try

      Return Update_Dependent_Transactions_All(pMainForm, Permission_Area, p_InstrumentID, p_PriceDates, False)

    Catch ex As Exception
    End Try

    Return False

  End Function

  ''' <summary>
  ''' Update_s the dependent_ transactions_ all.
  ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  ''' <param name="Permission_Area">The permission_ area.</param>
  ''' <param name="p_InstrumentID">The p_ instrument ID.</param>
  ''' <param name="p_PriceDates">The p_ price dates.</param>
  ''' <param name="p_IsMilestone">if set to <c>true</c> [p_ is milestone].</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function Update_Dependent_Transactions_All(ByVal pMainForm As VeniceMain, ByVal Permission_Area As String, ByVal p_InstrumentID As Long, ByVal p_PriceDates() As Date, ByVal p_IsMilestone As Boolean) As Boolean
    ' **************************************************************************************
    '
    '
    ' **************************************************************************************

    Dim ErrMessage As String
    Dim ErrFlag As Boolean
    Dim ErrStack As String
    Dim ThisPriceDate As Date

    ErrMessage = ""
    ErrStack = ""

    ' Validate Parameters

    If (pMainForm Is Nothing) Then
      pMainForm.LogError(Permission_Area & ",UpdateDependantTransactions()", LOG_LEVELS.Warning, "", "MainForm parameter not set.", "", True)
      Return (False)
    End If

    If (p_InstrumentID <= 0) Then
      pMainForm.LogError(Permission_Area & ",UpdateDependantTransactions()", LOG_LEVELS.Warning, "", "Given InstrumentID <= 0.", "", True)
      Return False
    End If

    For Each ThisPriceDate In p_PriceDates
      If (ThisPriceDate <= KNOWLEDGEDATE_NOW) Then
        pMainForm.LogError(Permission_Area & ",UpdateDependantTransactions()", LOG_LEVELS.Warning, "", "Given Price date < `1 Jan 1900`.", "", True)
        Return False
      End If
    Next

    ' Get Data
    ' Retrieve Standard Adaptors, Datasets and Tables.

    Dim PriceTable As New RenaissanceDataClass.DSPrice.tblPriceDataTable

    Dim TransactionDataset As RenaissanceDataClass.DSTransaction
    Dim TransactionTable As RenaissanceDataClass.DSTransaction.tblTransactionDataTable
    Dim TransactionAdaptor As SqlDataAdapter

    Dim InstrumentDataset As RenaissanceDataClass.DSInstrument
    Dim InstrumentTable As RenaissanceDataClass.DSInstrument.tblInstrumentDataTable
    Dim InstrumentAdaptor As SqlDataAdapter

    TransactionAdaptor = pMainForm.MainDataHandler.Get_Adaptor(RenaissanceStandardDatasets.tblTransaction.Adaptorname, VENICE_CONNECTION, RenaissanceStandardDatasets.tblTransaction.TableName)
    TransactionDataset = pMainForm.Load_Table(RenaissanceStandardDatasets.tblTransaction, False)
    TransactionTable = TransactionDataset.tblTransaction

    If TransactionTable Is Nothing Then
      pMainForm.LogError(Permission_Area & ",UpdateDependantTransactions()", LOG_LEVELS.Warning, "", "Failed to load Transaction Table", "", True)
      Return False
    End If
    If TransactionTable.Rows.Count <= 0 Then
      Return True
    End If

    InstrumentAdaptor = pMainForm.MainDataHandler.Get_Adaptor(RenaissanceStandardDatasets.tblInstrument.Adaptorname, VENICE_CONNECTION, RenaissanceStandardDatasets.tblInstrument.TableName)
    InstrumentDataset = pMainForm.Load_Table(RenaissanceStandardDatasets.tblInstrument, False)
    InstrumentTable = InstrumentDataset.tblInstrument

    If InstrumentTable Is Nothing Then
      pMainForm.LogError(Permission_Area & ",UpdateDependantTransactions()", LOG_LEVELS.Warning, "", "Failed to load Instrument Table", "", True)
      Return False
    End If
    If InstrumentTable.Rows.Count <= 0 Then
      Return True
    End If

    ' Get the Instrument Type of the Instrument being Priced

    Dim PriceInstrument() As RenaissanceDataClass.DSInstrument.tblInstrumentRow
    Dim InstrumentType As Integer
    Dim InstrumentDescription As String

    Try
      PriceInstrument = InstrumentTable.Select("InstrumentID=" & p_InstrumentID.ToString)

      If (PriceInstrument.Length <= 0) Then
        pMainForm.LogError(Permission_Area & ",UpdateDependantTransactions()", LOG_LEVELS.Warning, "", "No Instrument, failed to get Instrument Type for ID=" & p_InstrumentID.ToString, "", True)
        Return True
      End If

      InstrumentType = PriceInstrument(0).InstrumentType
      InstrumentDescription = PriceInstrument(0).InstrumentDescription

    Catch ex As Exception
      pMainForm.LogError(Permission_Area & ",UpdateDependantTransactions()", LOG_LEVELS.Error, ex.Message, "Error Getting Instrument Type.", ex.StackTrace, True)
      Return False
    End Try

    ' Is this Instrument of a type for which we want to Update Transactions ? i.e. UNIT or FUND

    If (InstrumentType <> RenaissanceGlobals.InstrumentTypes.Fund) And (InstrumentType <> RenaissanceGlobals.InstrumentTypes.Unit) Then
      ' Fail Silently.
      Return True
    End If

    ' FIRST, Build a sorted array of prices and mark the prices if they (should) have changed.

    ' Get a sorted list of Prices for the affected Instrument

    Dim SortedPrices() As RenaissanceDataClass.DSPrice.tblPriceRow
    Dim PriceArray() As PriceClass
    Dim PriceCounter As Integer
    Dim UpdateCounter As Integer
    Dim PriceChangedFlag As Boolean
    Dim StartValueDate As Date
    Dim EndValueDate As Date

    ' Load Instrument Prices.
    ' Amended Feb '09 to remove use of entire Prices table.

    Dim myAdaptor As New SqlDataAdapter
    Try
      pMainForm.MainAdaptorHandler.Set_AdaptorCommands(pMainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION), myAdaptor, "TBLPRICEBYINSTRUMENT")

      myAdaptor.SelectCommand.Parameters("@InstrumentID").Value = CInt(p_InstrumentID)
      myAdaptor.SelectCommand.Parameters("@Knowledgedate").Value = pMainForm.Main_Knowledgedate

      If (PriceTable Is Nothing) Then
        PriceTable = New RenaissanceDataClass.DSPrice.tblPriceDataTable
      Else
        PriceTable.Clear()
      End If

      SyncLock myAdaptor.SelectCommand.Connection
        pMainForm.LoadTable_Custom(PriceTable, myAdaptor.SelectCommand)
        'PriceTable.Load(myAdaptor.SelectCommand.ExecuteReader)
      End SyncLock

      SortedPrices = PriceTable.Select("True", "PriceDate")
    Catch ex As Exception
      pMainForm.LogError(Permission_Area & ",UpdateDependantTransactions()", LOG_LEVELS.Error, ex.Message, "Error Loading / Selecting Instrument Prices.", ex.StackTrace, True)
      Return False
    Finally
      Try
        If (myAdaptor.SelectCommand IsNot Nothing) Then
          myAdaptor.SelectCommand.Connection = Nothing
        End If
      Catch ex As Exception
      End Try
    End Try

    If (SortedPrices.Length <= 0) Then
      pMainForm.LogError(Permission_Area & ",UpdateDependantTransactions()", LOG_LEVELS.Warning, "", "No Prices found for Instrument " & p_InstrumentID.ToString, "", True)
      Return True
    End If

    ' Build Price Array using the Selected Price Rows.

    ReDim PriceArray(SortedPrices.Length - 1)

    For PriceCounter = 0 To (SortedPrices.Length - 1)
      PriceArray(PriceCounter) = New PriceClass(SortedPrices(PriceCounter))
    Next

    ' Update PriceArray to Mark the Prices affected by the cascade

    ' Mark initial Price(s)

    For PriceCounter = 0 To (PriceArray.GetLength(0) - 1)
      For Each ThisPriceDate In p_PriceDates
        If (PriceArray(PriceCounter).PriceDate = ThisPriceDate) Then
          PriceArray(PriceCounter).PriceAffected = True
        End If
      Next
    Next

    ' Check each Percent based price to see if it is affected.
    ' Resolve which prices should have been updates by a price cascade.
    '
    ' Potentially pass through the array multiple times incase the chain of linked prices does not 
    ' progress linearly through time.

    PriceChangedFlag = True
    While (PriceChangedFlag = True)
      PriceChangedFlag = False

      For PriceCounter = 0 To (PriceArray.GetLength(0) - 1)

        If (PriceArray(PriceCounter).PriceAffected = True) And _
         (PriceArray(PriceCounter).PricePropagated = False) Then

          PriceArray(PriceCounter).PricePropagated = True

          For UpdateCounter = 0 To (PriceArray.GetLength(0) - 1)
            If (PriceArray(UpdateCounter).IsPercent = True) And _
             (PriceArray(UpdateCounter).PriceBaseDate = PriceArray(PriceCounter).PriceDate) Then

              PriceArray(UpdateCounter).PriceAffected = True
              PriceChangedFlag = True
            End If
          Next
        End If

      Next
    End While

    ' Get Earliest Affected Price Date

    StartValueDate = CDate("1 Jan 3000")
    For PriceCounter = 0 To (PriceArray.GetLength(0) - 1)
      If (PriceArray(PriceCounter).PriceAffected = True) And (PriceArray(PriceCounter).PriceDate < StartValueDate) Then
        StartValueDate = PriceArray(PriceCounter).PriceDate
      End If
    Next

    ' **************************************************************************************
    '
    ' OK, assuming that worked, we now have an array of prices for this Instrument where those prices
    ' affected by a price cascade have been marked.
    '
    ' Now Select those transactions which we wish to check.
    '	
    ' It is sometimes necessary to treat UNITs differently to FUNDs
    ' For re-pricing Transactions in our own Fund UNITs after a milestone event, we
    ' only want to consider MILESTONE Prices and we do not want the modifications to
    ' affect more than one month.
    ' This restriction does not necesarily apply to Funds in which we invest.
    '
    ' **************************************************************************************

    Dim SelectedTransactions() As RenaissanceDataClass.DSTransaction.tblTransactionRow
    Dim thisTransaction As RenaissanceDataClass.DSTransaction.tblTransactionRow
    Dim SelectString As String
    Dim CompoundTransactionRNs As New ArrayList
    Dim ThisCTAlreadyNoted As Boolean

    If (InstrumentType = RenaissanceGlobals.InstrumentTypes.Unit) And (p_IsMilestone = True) Then
      EndValueDate = p_PriceDates(0).AddMonths(1)
      EndValueDate = EndValueDate.AddDays(1 - EndValueDate.Day)
    Else
      EndValueDate = CDate("1 Jan 3000")
    End If

    SelectString = "(TransactionInstrument=" & p_InstrumentID & ") AND (TransactionValueDate>='" & StartValueDate.ToString(QUERY_SHORTDATEFORMAT) & "') AND (TransactionValueDate<'" & EndValueDate.ToString(QUERY_SHORTDATEFORMAT) & "') AND (TransactionExemptFromUpdate=0)"
    SelectedTransactions = TransactionTable.Select(SelectString, "TransactionValueDate")

    If (SelectedTransactions.Length <= 0) Then
      Return True
    End If

    ' Work Through the selected Transactions, Changing them as necessary.

    For Each thisTransaction In SelectedTransactions
      If ((thisTransaction.TransactionUnits <> 0) Or _
       (thisTransaction.TransactionSettlement <> 0)) And _
       ((thisTransaction.TransactionCTFLAGS And RenaissanceGlobals.CompoundTransactionFlags.ExcludeFromPriceCascadeUpdates) = 0) Then

        ' Has the price changed for this transaction, and if so what has it changed to ?
        ' Note that 'p_IsMilestone' is used to only select Milestone prices in the event of this
        ' being a Milestone Price Update.
        ' In the event of a Milestone price update, we want to ignore any non-MS intra-month
        ' prices for the fund.

        Dim PreviousPriceDate As Date
        Dim NewPrice As Double
        Dim NewGAVPrice As Double

        PriceChangedFlag = False
        PreviousPriceDate = KNOWLEDGEDATE_NOW

        For PriceCounter = 0 To (PriceArray.GetLength(0) - 1)
          If (PriceArray(PriceCounter).PriceDate > PreviousPriceDate) AndAlso _
           (PriceArray(PriceCounter).PriceDate <= thisTransaction.TransactionValueDate) AndAlso _
           ((PriceArray(PriceCounter).IsMilestone = True) Or ((p_IsMilestone = False))) Then

            PreviousPriceDate = PriceArray(PriceCounter).PriceDate
            NewPrice = PriceArray(PriceCounter).PriceLevel
            NewGAVPrice = PriceArray(PriceCounter).PriceGAV
            PriceChangedFlag = PriceArray(PriceCounter).PriceAffected

            ' Check NewGAVPrice has a value.
            ' At present the GAV value in the tblPrice table is only populated for Milestone prices.

            If (NewGAVPrice = 0) Then
              NewGAVPrice = NewPrice
            End If

          End If
        Next

        ' If this is a compound Transaction, then check to see if it has already been recorded.
        ' If it has not been noted, then Add the RN to the 'CompoundTransactionRNs' ArrayList.

        ThisCTAlreadyNoted = False
        If (PriceChangedFlag = True) And (thisTransaction.TransactionCompoundRN > 0) Then
          Dim CompoundTransaction_RN As Integer
          Try
            For Each CompoundTransaction_RN In CompoundTransactionRNs
              If CompoundTransaction_RN = thisTransaction.TransactionCompoundRN Then
                ThisCTAlreadyNoted = True
                Exit For
              End If
            Next
          Catch ex As Exception
          End Try
        End If
        If (PriceChangedFlag = True) And (thisTransaction.TransactionCompoundRN > 0) Then
          If (ThisCTAlreadyNoted = False) Then
            CompoundTransactionRNs.Add(thisTransaction.TransactionCompoundRN)
          End If
        End If

        ' Prompt to update Non-CompoundTransaction Transactions.

        If (PriceChangedFlag = True) And (thisTransaction.TransactionCompoundRN = 0) Then
          ' Ok Ask if this transaction is to be changed.
          Dim CounterpartyName As String
          Dim FundName As String
          Dim TransactionTypeName As String
          Dim MessageString As String
          Dim CashMovement As Double

          Dim thisTable As DataTable
          Dim SelectedRows() As DataRow

          ' CounterpartyName
          Try
            thisTable = CType(pMainForm.Load_Table(RenaissanceStandardDatasets.tblCounterparty, False), RenaissanceDataClass.DSCounterparty).tblCounterparty

            If thisTable Is Nothing Then
              pMainForm.LogError(Permission_Area & ",UpdateDependantTransactions()", LOG_LEVELS.Warning, "", "Failed to load Counterparty Table", "", True)
              Return False
            End If
            If thisTable.Rows.Count <= 0 Then
              CounterpartyName = thisTransaction.TransactionCounterparty.ToString
            Else
              SelectedRows = thisTable.Select("CounterpartyID=" & thisTransaction.TransactionCounterparty)
              If SelectedRows.Length <= 0 Then
                CounterpartyName = thisTransaction.TransactionCounterparty.ToString
              Else
                CounterpartyName = SelectedRows(0)("CounterpartyName")
              End If
            End If
          Catch ex As Exception
            pMainForm.LogError(Permission_Area & ",UpdateDependantTransactions()", LOG_LEVELS.Error, ex.Message, "Error Getting Counterparty Name.", ex.StackTrace, True)
            Return False
          End Try

          ' TransactionTypeName
          Try
            CashMovement = 1
            thisTable = CType(pMainForm.Load_Table(RenaissanceStandardDatasets.tblTransactionType, False), RenaissanceDataClass.DSTransactionType).tblTransactionType

            If thisTable Is Nothing Then
              pMainForm.LogError(Permission_Area & ",UpdateDependantTransactions()", LOG_LEVELS.Warning, "", "Failed to load TransactionType Table", "", True)
              Return False
            End If
            If thisTable.Rows.Count <= 0 Then
              TransactionTypeName = thisTransaction.TransactionType.ToString
            Else
              SelectedRows = thisTable.Select("TransactionTypeID=" & thisTransaction.TransactionType)
              If SelectedRows.Length <= 0 Then
                TransactionTypeName = thisTransaction.TransactionType.ToString
              Else
                TransactionTypeName = SelectedRows(0)("TransactionType")
                CashMovement = SelectedRows(0)("TransactionTypeCashMove")
              End If
            End If
          Catch ex As Exception
            pMainForm.LogError(Permission_Area & ",UpdateDependantTransactions()", LOG_LEVELS.Error, ex.Message, "Error Getting TransactionType Name.", ex.StackTrace, True)
            Return False
          End Try

          ' FundName
          Try
            thisTable = CType(pMainForm.Load_Table(RenaissanceStandardDatasets.tblFund, False), RenaissanceDataClass.DSFund).tblFund

            If thisTable Is Nothing Then
              pMainForm.LogError(Permission_Area & ",UpdateDependantTransactions()", LOG_LEVELS.Warning, "", "Failed to load Fund Table", "", True)
              Return False
            End If
            If thisTable.Rows.Count <= 0 Then
              FundName = thisTransaction.TransactionFund.ToString
            Else
              SelectedRows = thisTable.Select("FundID=" & thisTransaction.TransactionFund)
              If SelectedRows.Length <= 0 Then
                FundName = thisTransaction.TransactionFund.ToString
              Else
                FundName = SelectedRows(0)("FundName")
              End If
            End If
          Catch ex As Exception
            pMainForm.LogError(Permission_Area & ",UpdateDependantTransactions()", LOG_LEVELS.Error, ex.Message, "Error Getting Fund Name.", ex.StackTrace, True)
            Return False
          End Try

          MessageString = "This transaction may have been affected by the Price Change, Do you want to update it ?" & vbCrLf & _
           "(RN : " & thisTransaction.RN & ", ParentID : " & thisTransaction.TransactionParentID & ") " & vbCrLf & "Fund : " & FundName & vbCrLf & _
           TransactionTypeName & " " & thisTransaction.TransactionUnits & " " & InstrumentDescription & vbCrLf & _
           "Counterparty : " & CounterpartyName & vbCrLf & _
           "Price " & thisTransaction.TransactionPrice & vbCrLf & _
           "Settlement " & thisTransaction.TransactionSettlement & vbCrLf & _
           "ValueDate " & thisTransaction.TransactionValueDate.ToString(DISPLAYMEMBER_DATEFORMAT)

          If MessageBox.Show(MessageString, "Update Transaction ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then

            ErrFlag = False

            SyncLock TransactionTable

              Try
                ' Initiate Edit,
                thisTransaction.BeginEdit()

                If (InstrumentType = RenaissanceGlobals.InstrumentTypes.Unit) And (p_IsMilestone) Then
                  ' It is a transacion in our fund units.

                  ' Subscriptions are made at GAV, Redemptions at NAV

                  If (thisTransaction.TransactionType = RenaissanceGlobals.TransactionTypes.Subscribe) Then

                    thisTransaction.TransactionPrice = NewGAVPrice

                  Else
                    ' Not a subscription.

                    thisTransaction.TransactionPrice = NewPrice

                  End If

                Else
                  ' Default new price.

                  thisTransaction.TransactionPrice = NewPrice

                End If

                If thisTransaction.TransactionValueorAmount.ToUpper.StartsWith("V") Then
                  thisTransaction.TransactionUnits = GetTransactionUnitsOrSettlement(thisTransaction.TransactionValueorAmount, thisTransaction.TransactionUnits, NewPrice, GetInstrumentContractSize("UpdateDependentTransactions()", pMainForm, thisTransaction.TransactionInstrument), thisTransaction.TransactionBrokerFees, thisTransaction.TransactionCosts, thisTransaction.TransactionSettlement, CashMovement)
                Else
                  thisTransaction.TransactionSettlement = GetTransactionUnitsOrSettlement(thisTransaction.TransactionValueorAmount, thisTransaction.TransactionUnits, NewPrice, GetInstrumentContractSize("UpdateDependentTransactions()", pMainForm, thisTransaction.TransactionInstrument), thisTransaction.TransactionBrokerFees, thisTransaction.TransactionCosts, thisTransaction.TransactionSettlement, CashMovement)
                End If

                thisTransaction.EndEdit()

                Dim UpdateRows(0) As DataRow
                UpdateRows(0) = thisTransaction

                ' Post Additions / Updates to the underlying table :-
                ErrFlag = False

                TransactionAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = pMainForm.Main_Knowledgedate
                TransactionAdaptor.InsertCommand.Parameters("@ValidationOnly").Value = 0
                TransactionAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = pMainForm.Main_Knowledgedate
                TransactionAdaptor.UpdateCommand.Parameters("@ValidationOnly").Value = 0

                ' If TransactionAdaptor.Update(UpdateRows) = 0 Then
                If pMainForm.AdaptorUpdate("UpdateDependentTransactions", TransactionAdaptor, UpdateRows) = 0 Then
                  Try
                    pMainForm.LogError(Permission_Area & ",Update_Dependent_Transactions()", LOG_LEVELS.Error, TransactionAdaptor.UpdateCommand.Parameters("@rvStatusString").Value, "TransactionParentID=" & thisTransaction.TransactionParentID & ", Price=" & NewPrice, "", False)
                  Catch ex As Exception
                    ErrMessage = ex.Message
                    ErrFlag = True
                    ErrStack = ex.StackTrace
                  End Try
                Else
                  pMainForm.LogError(Permission_Area & ",Update_Dependent_Transactions()", LOG_LEVELS.Changes, "Add/Edit Record", "TransactionParentID=" & thisTransaction.TransactionParentID & ", Price=" & NewPrice, "", False)
                End If

              Catch ex As Exception
                ErrMessage = ex.Message
                ErrFlag = True
                ErrStack = ex.StackTrace
              End Try

            End SyncLock

            If (ErrFlag = True) Then
              Call pMainForm.LogError(Permission_Area & ",Update_Dependent_Transactions()", LOG_LEVELS.Error, ErrMessage, "Error Saving Data : TransactionParentID=" & thisTransaction.TransactionParentID & ", Price=" & NewPrice, ErrStack, True)
            End If

          End If ' MessageBox Prompt Says YES

        End If ' If PriceChangedFlag

      End If ' This Transaction is suitable for Updating : UNIT or FUND, etc.

    Next ' Transaction.

    ' Now work through the 'CompoundTransactionRNs' ArrayList prompting to modify Compound Transactions.

    If CompoundTransactionRNs.Count > 0 Then
      Dim CTTable As RenaissanceDataClass.DSCompoundTransaction.tblCompoundTransactionDataTable
      Dim CTRows() As RenaissanceDataClass.DSCompoundTransaction.tblCompoundTransactionRow
      Dim thisCTRow As RenaissanceDataClass.DSCompoundTransaction.tblCompoundTransactionRow
      Dim CTAdaptor As SqlDataAdapter
      Dim CT_Handler As New CompoundTransactionHandler

      CTAdaptor = pMainForm.MainDataHandler.Get_Adaptor(RenaissanceStandardDatasets.tblCompoundTransaction.Adaptorname, VENICE_CONNECTION, RenaissanceStandardDatasets.tblCompoundTransaction.TableName)

      Dim CompoundTransaction_RN As Integer
      Try
        For Each CompoundTransaction_RN In CompoundTransactionRNs

          ' Get Compound Transactions Table
          ' Get it on each loop as it May / Will have changed on each CT Update.

          CTTable = CType(pMainForm.Load_Table(RenaissanceStandardDatasets.tblCompoundTransaction, False), RenaissanceDataClass.DSCompoundTransaction).tblCompoundTransaction

          If (CTTable Is Nothing) Then
            Exit For
          End If

          ' Get the matching CT Record

          CTRows = CTTable.Select("RN=" & CompoundTransaction_RN.ToString)

          If CTRows.Length > 0 Then
            thisCTRow = CTRows(0)

            ' Has the price changed for this transaction, and if so what has it changed to ?
            ' Note that 'p_IsMilestone' is used to only select Milestone prices in the event of this
            ' being a Milestone Price Update.
            ' In the event of a Milestone price update, we want to ignore any non-MS intra-month
            ' prices for the fund.

            Dim PreviousPriceDate As Date
            Dim NewPrice As Double

            PriceChangedFlag = False
            PreviousPriceDate = KNOWLEDGEDATE_NOW

            For PriceCounter = 0 To (PriceArray.GetLength(0) - 1)
              If (PriceArray(PriceCounter).PriceDate > PreviousPriceDate) AndAlso _
               (PriceArray(PriceCounter).PriceDate <= thisCTRow.TransactionValueDate) AndAlso _
               ((PriceArray(PriceCounter).IsMilestone = True) Or ((p_IsMilestone = False))) Then

                PreviousPriceDate = PriceArray(PriceCounter).PriceDate
                NewPrice = PriceArray(PriceCounter).PriceLevel
                PriceChangedFlag = PriceArray(PriceCounter).PriceAffected
              End If
            Next

            If (PriceChangedFlag = True) Then

              ' Ok Ask if this transaction is to be changed.
              Dim CounterpartyName As String
              Dim FundName As String
              Dim TransactionTypeName As String
              Dim MessageString As String
              Dim CashMovement As Double

              Dim thisTable As DataTable
              Dim SelectedRows() As DataRow
              Dim UpdateRows(0) As DataRow
              Dim PreviousCTRN As Integer


              ' CounterpartyName
              Try
                thisTable = CType(pMainForm.Load_Table(RenaissanceStandardDatasets.tblCounterparty, False), RenaissanceDataClass.DSCounterparty).tblCounterparty

                If thisTable Is Nothing Then
                  pMainForm.LogError(Permission_Area & ",UpdateDependantTransactions()", LOG_LEVELS.Warning, "", "Failed to load Counterparty Table", "", True)
                  Return False
                End If
                If thisTable.Rows.Count <= 0 Then
                  CounterpartyName = thisCTRow.TransactionCounterparty.ToString
                Else
                  SelectedRows = thisTable.Select("CounterpartyID=" & thisCTRow.TransactionCounterparty)
                  If SelectedRows.Length <= 0 Then
                    CounterpartyName = thisCTRow.TransactionCounterparty.ToString
                  Else
                    CounterpartyName = SelectedRows(0)("CounterpartyName")
                  End If
                End If
              Catch ex As Exception
                pMainForm.LogError(Permission_Area & ",UpdateDependantTransactions()", LOG_LEVELS.Error, ex.Message, "Error Getting Counterparty Name.", ex.StackTrace, True)
                Return False
              End Try

              ' TransactionTypeName
              Try
                CashMovement = 1
                thisTable = CType(pMainForm.Load_Table(RenaissanceStandardDatasets.tblCompoundTransactionTemplate, False), RenaissanceDataClass.DSCompoundTransactionTemplate).tblCompoundTransactionTemplate

                If thisTable Is Nothing Then
                  pMainForm.LogError(Permission_Area & ",UpdateDependantTransactions()", LOG_LEVELS.Warning, "", "Failed to load CompoundTransactionTemplate Table", "", True)
                  Return False
                End If
                If thisTable.Rows.Count <= 0 Then
                  TransactionTypeName = thisCTRow.TransactionTemplateID.ToString
                Else
                  SelectedRows = thisTable.Select("TemplateID=" & thisCTRow.TransactionTemplateID)
                  If SelectedRows.Length <= 0 Then
                    TransactionTypeName = thisCTRow.TransactionTemplateID.ToString
                  Else
                    TransactionTypeName = SelectedRows(0)("TemplateDescription")
                  End If
                End If
              Catch ex As Exception
                pMainForm.LogError(Permission_Area & ",UpdateDependantTransactions()", LOG_LEVELS.Error, ex.Message, "Error Getting TransactionType Name.", ex.StackTrace, True)
                Return False
              End Try

              ' FundName
              Try
                thisTable = CType(pMainForm.Load_Table(RenaissanceStandardDatasets.tblFund, False), RenaissanceDataClass.DSFund).tblFund

                If thisTable Is Nothing Then
                  pMainForm.LogError(Permission_Area & ",UpdateDependantTransactions()", LOG_LEVELS.Warning, "", "Failed to load Fund Table", "", True)
                  Return False
                End If
                If thisTable.Rows.Count <= 0 Then
                  FundName = thisCTRow.TransactionFund.ToString
                Else
                  SelectedRows = thisTable.Select("FundID=" & thisCTRow.TransactionFund)
                  If SelectedRows.Length <= 0 Then
                    FundName = thisCTRow.TransactionFund.ToString
                  Else
                    FundName = SelectedRows(0)("FundName")
                  End If
                End If
              Catch ex As Exception
                pMainForm.LogError(Permission_Area & ",UpdateDependantTransactions()", LOG_LEVELS.Error, ex.Message, "Error Getting Fund Name.", ex.StackTrace, True)
                Return False
              End Try

              MessageString = "This Compound Transaction may have been affected by the Price Change, Do you want to update it ?" & vbCrLf & _
               "RN : " & thisCTRow.RN & vbCrLf & "Fund : " & FundName & vbCrLf & _
               TransactionTypeName & " " & thisCTRow.TransactionUnits & " " & InstrumentDescription & vbCrLf & _
               "Counterparty : " & CounterpartyName & vbCrLf & _
               "Price " & thisCTRow.TransactionPrice & vbCrLf & _
               "Units " & thisCTRow.TransactionUnits & vbCrLf & _
               "ValueDate " & thisCTRow.TransactionValueDate.ToString(DISPLAYMEMBER_DATEFORMAT)

              If MessageBox.Show(MessageString, "Update Compound Transaction ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then

                ErrFlag = False
                SyncLock CTTable

                  Try
                    ' Initiate Edit,
                    thisCTRow.BeginEdit()

                    If thisCTRow.TransactionValueorAmount.ToUpper.StartsWith("V") Then
                      thisCTRow.TransactionUnits = GetTransactionUnitsOrSettlement(thisCTRow.TransactionValueorAmount, thisCTRow.TransactionUnits, NewPrice, GetInstrumentContractSize("UpdateDependentTransactions()", pMainForm, thisCTRow.TransactionInstrument), thisCTRow.TransactionBrokerFees, thisCTRow.TransactionCosts, ((thisCTRow.TransactionUnits * thisCTRow.TransactionPrice) - thisCTRow.TransactionCosts), CashMovement)
                    End If

                    thisCTRow.TransactionPrice = NewPrice
                    thisCTRow.CompoundTransactionPosted = False

                    thisCTRow.EndEdit()

                    PreviousCTRN = thisCTRow.RN
                    UpdateRows(0) = thisCTRow

                    ' Post Additions / Updates to the underlying table :-
                    ErrFlag = False

                    CTAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = pMainForm.Main_Knowledgedate
                    CTAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = pMainForm.Main_Knowledgedate

                    ' If CTAdaptor.Update(UpdateRows) = 0 Then
                    If pMainForm.AdaptorUpdate("UpdateDependentTransactions", CTAdaptor, UpdateRows) = 0 Then
                      Try
                        pMainForm.LogError(Permission_Area & ",Update_Dependent_Transactions()", LOG_LEVELS.Error, "Updated Compound Transaction Failed. RN=" & thisCTRow.RN & ", Price=" & NewPrice, "", False)
                      Catch ex As Exception
                        ErrMessage = ex.Message
                        ErrFlag = True
                        ErrStack = ex.StackTrace
                      End Try
                    Else

                      If CT_Handler.ProcessCompoundTransaction(pMainForm, Permission_Area, thisCTRow, PreviousCTRN) = True Then
                        pMainForm.LogError(Permission_Area & ",Update_Dependent_Transactions()", LOG_LEVELS.Changes, "Updated Compound Transaction. RN=" & thisCTRow.RN & ", Price=" & NewPrice, "", False)
                      End If


                    End If

                  Catch ex As Exception
                    ErrMessage = ex.Message
                    ErrFlag = True
                    ErrStack = ex.StackTrace
                  End Try

                End SyncLock

                If (ErrFlag = True) Then
                  Call pMainForm.LogError(Permission_Area & ",Update_Dependent_Transactions()", LOG_LEVELS.Error, ErrMessage, "Error Saving Compound Transaction : RN=" & thisCTRow.RN & ", Price=" & NewPrice, ErrStack, True)
                End If

              End If ' MessageBox Prompt Says YES

            End If ' If PriceChangedFlag

          End If ' CTRows.Length > 0 Then

        Next ' For Each CompoundTransaction_RN In CompoundTransactionRNs

      Catch ex As Exception
        Call pMainForm.LogError(Permission_Area & ",Update_Dependent_Transactions()", LOG_LEVELS.Error, ex.Message, "Error loading CompoundTransactions Table.", ex.StackTrace, True)
      End Try

      ' Update Tables, propogate Events.


      'Call pMainForm.Load_Table(RenaissanceStandardDatasets.tblTransaction, True)
      'Call pMainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblTransaction))
      'Call pMainForm.Load_Table(RenaissanceStandardDatasets.tblCompoundTransaction, True)
      'Call pMainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblCompoundTransaction))

      Call pMainForm.Load_Table_Background(RenaissanceStandardDatasets.tblTransaction, "", True, True)
      Call pMainForm.Load_Table_Background(RenaissanceStandardDatasets.tblCompoundTransaction, "", True, True)

    End If ' If CompoundTransactionRNs.Count > 0 Then

  End Function


  ''' <summary>
  ''' Saves the price.
  ''' </summary>
  ''' <param name="MainForm">The main form.</param>
  ''' <param name="PriceInstrument">The price instrument.</param>
  ''' <param name="PriceDate">The price date.</param>
  ''' <param name="PriceLevel">The price level.</param>
  ''' <param name="IsPercent">if set to <c>true</c> [is percent].</param>
  ''' <param name="Percent">The percent.</param>
  ''' <param name="PriceBaseDate">The price base date.</param>
  ''' <param name="PriceIsFinal">The price is final.</param>
  ''' <param name="PriceComment">The price comment.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function SavePrice(ByVal MainForm As VeniceMain, ByVal PriceInstrument As Integer, ByVal PriceDate As Date, ByVal PriceLevel As Double, ByVal IsPercent As Boolean, ByVal Percent As Double, ByVal PriceBaseDate As Date, ByVal PriceIsFinal As Integer, ByVal PriceComment As String) As Boolean
    Return SavePrice(MainForm, PriceInstrument, PriceDate, PriceLevel, 0, 0, 0, IsPercent, Percent, PriceBaseDate, PriceIsFinal, 0, 0, Now(), PriceComment, 0, 0, 1, 0, 0)
  End Function

  ''' <summary>
  ''' Saves the price.
  ''' </summary>
  ''' <param name="MainForm">The main form.</param>
  ''' <param name="PriceInstrument">The price instrument.</param>
  ''' <param name="PriceDate">The price date.</param>
  ''' <param name="PriceLevel">The price level.</param>
  ''' <param name="BNAV">The BNAV.</param>
  ''' <param name="GNAV">The GNAV.</param>
  ''' <param name="GAV">The GAV.</param>
  ''' <param name="IsPercent">if set to <c>true</c> [is percent].</param>
  ''' <param name="Percent">The percent.</param>
  ''' <param name="PriceBaseDate">The price base date.</param>
  ''' <param name="PriceIsFinal">The price is final.</param>
  ''' <param name="PriceIsAdministrator">if set to <c>true</c> [price is administrator].</param>
  ''' <param name="PriceIsMilestone">if set to <c>true</c> [price is milestone].</param>
  ''' <param name="DateEntered">The date entered.</param>
  ''' <param name="PriceComment">The price comment.</param>
  ''' <param name="DontReplaceExistingPrices">The dont replace existing prices.</param>
  ''' <param name="DontReplaceExistingFinalPrices">The dont replace existing final prices.</param>
  ''' <param name="DontReplaceExistingMilestonePrices">The dont replace existing milestone prices.</param>
  ''' <param name="DontUpdateDependentPrices">The dont update dependent prices.</param>
  ''' <param name="DontPostPricesAutoUpdate">The dont post prices auto update.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function SavePrice(ByVal MainForm As VeniceMain, ByVal PriceInstrument As Integer, ByVal PriceDate As Date, ByVal PriceLevel As Double, ByVal BNAV As Double, ByVal GNAV As Double, ByVal GAV As Double, _
   ByVal IsPercent As Boolean, ByVal Percent As Double, ByVal PriceBaseDate As Date, ByVal PriceIsFinal As Integer, ByVal PriceIsAdministrator As Boolean, ByVal PriceIsMilestone As Boolean, _
   ByVal DateEntered As Date, ByVal PriceComment As String, ByVal DontReplaceExistingPrices As Integer, ByVal DontReplaceExistingFinalPrices As Integer, ByVal DontReplaceExistingMilestonePrices As Integer, _
   ByVal DontUpdateDependentPrices As Integer, ByVal DontPostPricesAutoUpdate As Integer) As Boolean

    ' 
    ' Save Price

    Dim thisCommand As SqlCommand = Nothing
    Try
      thisCommand = New SqlCommand()
      thisCommand.CommandType = CommandType.StoredProcedure
      thisCommand.CommandText = "spu_SavePrice"
      thisCommand.Parameters.Add("@Instrument_ID", SqlDbType.Int).Value = PriceInstrument
      thisCommand.Parameters.Add("@PriceDate", SqlDbType.DateTime).Value = PriceDate
      thisCommand.Parameters.Add("@PriceLevel", SqlDbType.Float).Value = PriceLevel
      thisCommand.Parameters.Add("@PriceBasicNAV", SqlDbType.Float).Value = BNAV
      thisCommand.Parameters.Add("@PriceGNAV", SqlDbType.Float).Value = GNAV
      thisCommand.Parameters.Add("@PriceGAV", SqlDbType.Float).Value = GAV
      thisCommand.Parameters.Add("@PriceIsPercent", SqlDbType.Bit).Value = IsPercent
      thisCommand.Parameters.Add("@PricePercent", SqlDbType.Float).Value = Percent
      thisCommand.Parameters.Add("@PriceBaseDate", SqlDbType.DateTime).Value = PriceBaseDate
      thisCommand.Parameters.Add("@PriceIsFinal", SqlDbType.Int).Value = PriceIsFinal
      thisCommand.Parameters.Add("@PriceIsAdministrator", SqlDbType.Bit).Value = PriceIsAdministrator
      thisCommand.Parameters.Add("@PriceIsMilestone", SqlDbType.Bit).Value = PriceIsMilestone
      thisCommand.Parameters.Add("@DateEntered", SqlDbType.DateTime).Value = DateEntered
      thisCommand.Parameters.Add("@PriceComment", SqlDbType.VarChar, 100).Value = PriceComment
      thisCommand.Parameters.Add("@DontReplaceExistingPrices", SqlDbType.Int).Value = DontReplaceExistingPrices
      thisCommand.Parameters.Add("@DontReplaceExistingFinalPrices", SqlDbType.Int).Value = DontReplaceExistingFinalPrices
      thisCommand.Parameters.Add("@DontReplaceExistingMilestonePrices", SqlDbType.Int).Value = DontReplaceExistingMilestonePrices
      thisCommand.Parameters.Add("@DontUpdateDependentPrices", SqlDbType.Int).Value = DontUpdateDependentPrices
      thisCommand.Parameters.Add("@DontPostPricesAutoUpdate", SqlDbType.Int).Value = DontPostPricesAutoUpdate
      thisCommand.Parameters.Add(New SqlParameter("@rvStatusString", SqlDbType.VarChar, 200, ParameterDirection.Output, False, CType(0, Byte), CType(0, Byte), "", DataRowVersion.Current, Nothing))

      thisCommand.Connection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)

      SyncLock thisCommand.Connection
        thisCommand.ExecuteNonQuery()
      End SyncLock

    Catch ex As Exception
      MainForm.LogError("SavePrice()", LOG_LEVELS.Error, ex.Message, "Error saving price.", ex.StackTrace, True)
      Return False
    Finally
      If (thisCommand IsNot Nothing) Then
        thisCommand.Connection = Nothing
        thisCommand.Parameters.Clear()
        thisCommand = Nothing
      End If
    End Try

    Return True
  End Function


  ''' <summary>
  ''' Prices the exists.
  ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  ''' <param name="pPriceInstrument">The p price instrument.</param>
  ''' <param name="pPriceDate">The p price date.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function PriceExists(ByVal pMainForm As VeniceMain, ByVal pPriceInstrument As Integer, ByVal pPriceDate As Date) As Boolean
    ' ***************************************************************
    ' Function to return the price for a given Instrument on a Given Date
    ' 
    ' Price is returned from the 'myTable' table. The assumption is that this is the 
    ' frmPrice Form !
    ' ***************************************************************

    Dim SelectQuery As New SqlCommand
    Dim RVal As Object = Nothing

    If (pPriceInstrument <= 0) Then
      Return 0
    End If

    Try
      SelectQuery.CommandType = CommandType.Text
      SelectQuery.CommandText = "SELECT TOP 1 PriceInstrument FROM tblPrice WHERE (DateDeleted IS NULL) AND (PriceInstrument=" & pPriceInstrument.ToString & ") AND (PriceDate='" & pPriceDate.ToString(QUERY_SHORTDATEFORMAT) & "')"
      SelectQuery.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      SelectQuery.Connection = pMainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)

      SyncLock SelectQuery.Connection
        RVal = SelectQuery.ExecuteScalar
      End SyncLock

    Catch ex As Exception

      RVal = Nothing

    Finally

      If (SelectQuery IsNot Nothing) Then
        SelectQuery.Connection = Nothing ' Main Venice Connection is used, do not close.
      End If

    End Try

    If (RVal IsNot Nothing) AndAlso (IsNumeric(RVal)) AndAlso (CInt(RVal) > 0) Then
      Return True
    End If

    Return False

  End Function

  ''' <summary>
  ''' Missings the instruments.
  ''' </summary>
  ''' <param name="MainForm">The main form.</param>
  ''' <param name="Permission_Area">The permission_ area.</param>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pLegalEntity">The p legal entity.</param>
  ''' <param name="pValueDate">The p value date.</param>
  ''' <returns>ArrayList.</returns>
  Public Function MissingInstruments(ByVal MainForm As VeniceMain, ByVal Permission_Area As String, ByVal pFundID As Integer, ByVal pLegalEntity As String, ByVal pValueDate As Date) As ArrayList
    ' ***************************************************** 
    ' Interface function to the SQL function that returns a list
    ' of Instruments with Transactions but without ant price.
    '
    ' Simply returns the 'InstrumentDescription' field from the 
    ' spu_checkinstrumentprices SP return set as an ArrayList.
    '
    ' ***************************************************** 

    Dim thisAdaptor As New SqlDataAdapter
    Dim thisArraylist As New ArrayList
    Dim MissingInstrumentsTable As New DataTable
    Dim RowCounter As Integer

    Try

      If (MainForm Is Nothing) Then
        Return thisArraylist
      End If

      Call MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.GetVeniceConnection(), thisAdaptor, "spu_checkinstrumentprices")

      thisAdaptor.SelectCommand.Parameters("@FundID").Value = pFundID
      thisAdaptor.SelectCommand.Parameters("@LegalEntity").Value = pLegalEntity
      thisAdaptor.SelectCommand.Parameters("@ValueDate").Value = pValueDate
      thisAdaptor.SelectCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate

      SyncLock thisAdaptor.SelectCommand.Connection
        thisAdaptor.Fill(MissingInstrumentsTable)
      End SyncLock

      If MissingInstrumentsTable.Rows.Count > 0 Then
        For RowCounter = 0 To (MissingInstrumentsTable.Rows.Count - 1)
          thisArraylist.Add(MissingInstrumentsTable.Rows(RowCounter)("InstrumentDescription"))
        Next
      End If

    Catch ex As Exception

    Finally
      Try
        If (thisAdaptor.SelectCommand IsNot Nothing) AndAlso (thisAdaptor.SelectCommand.Connection IsNot Nothing) Then
          thisAdaptor.SelectCommand.Connection.Close()
          thisAdaptor.SelectCommand.Connection = Nothing
        End If
      Catch ex As Exception
      End Try
    End Try

    Return thisArraylist

  End Function

  ''' <summary>
  ''' Gets the price child instrument I ds.
  ''' </summary>
  ''' <param name="Mainform">The mainform.</param>
  ''' <param name="pInstrumentID">The p instrument ID.</param>
  ''' <returns>ArrayList.</returns>
  Public Function GetPriceChildInstrumentIDs(ByRef Mainform As VeniceMain, ByVal pInstrumentID As Integer) As ArrayList
    ' *************************************************************
    ' Function to return an Arraylist of Sibling or Child instruments to which
    ' Price changes may be cascaded.
    ' 
    ' *************************************************************

    Dim DSInstruments As RenaissanceDataClass.DSInstrument
    Dim TblInstruments As RenaissanceDataClass.DSInstrument.tblInstrumentDataTable
    Dim SelectedInstruments() As RenaissanceDataClass.DSInstrument.tblInstrumentRow

    Dim QuestionString As String
    Dim ItemCount As Integer

    Dim RVal As New ArrayList

    Try

      DSInstruments = Mainform.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblInstrument, False)
      If DSInstruments Is Nothing Then
        Return New ArrayList
        Exit Function
      End If
      TblInstruments = DSInstruments.tblInstrument

      QuestionString = "Do you want to propagate the Percentage Price change" & vbCrLf & "to this Instrument's Child / Parent & Sibling Instruments ?" & vbCrLf & vbCrLf

      ' 1) Check for a Parent to this Instrument

      SelectedInstruments = TblInstruments.Select("(InstrumentID=" & pInstrumentID.ToString & ")")
      If (Not (SelectedInstruments Is Nothing)) AndAlso (SelectedInstruments.Length > 0) Then
        Dim thisParentInstrument As Integer
        thisParentInstrument = SelectedInstruments(0).InstrumentParent

        If (thisParentInstrument <> pInstrumentID) Then
          SelectedInstruments = TblInstruments.Select("(InstrumentParent=" & thisParentInstrument.ToString & ") AND (InstrumentID<>" & pInstrumentID.ToString & ") AND (InstrumentIsEqualisation=0)", "InstrumentDescription")
          If (Not (SelectedInstruments Is Nothing)) AndAlso (SelectedInstruments.Length > 0) Then
            For ItemCount = 0 To (SelectedInstruments.Length - 1)
              RVal.Add(SelectedInstruments(ItemCount).InstrumentID)
              QuestionString &= SelectedInstruments(ItemCount).InstrumentDescription & vbCrLf
            Next
          End If
        End If
      End If

      ' 2) Check for Child Instruments

      SelectedInstruments = TblInstruments.Select("(InstrumentParent=" & pInstrumentID.ToString & ") AND (InstrumentID<>" & pInstrumentID.ToString & ") AND (InstrumentIsEqualisation=0)", "InstrumentDescription")
      If (Not (SelectedInstruments Is Nothing)) AndAlso (SelectedInstruments.Length > 0) Then
        For ItemCount = 0 To (SelectedInstruments.Length - 1)
          If Not RVal.Contains(SelectedInstruments(ItemCount).InstrumentID) Then
            RVal.Add(SelectedInstruments(ItemCount).InstrumentID)
          End If
          QuestionString &= SelectedInstruments(ItemCount).InstrumentDescription & vbCrLf
        Next
      End If

      If RVal.Count > 0 Then
        If MessageBox.Show(QuestionString, "Propagate Prices", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
          Return New ArrayList
          Exit Function
        End If
      End If

      Return RVal

    Catch ex As Exception
      Call Mainform.LogError("CascadePercentChangesToChildInstruments", LOG_LEVELS.Error, ex.Message, "Error in CascadePercentChangesToChildInstruments", ex.StackTrace, True)
    End Try

    Return New ArrayList

  End Function

  ''' <summary>
  ''' Gets the unit price.
  ''' </summary>
  ''' <param name="MainForm">The main form.</param>
  ''' <param name="Permission_Area">The permission_ area.</param>
  ''' <param name="pFundID">The p fund ID.</param>
  ''' <param name="pValueDate">The p value date.</param>
  ''' <param name="pStatusGroupFilter">The p status group filter.</param>
  ''' <param name="pAdministratorDatesFilter">The p administrator dates filter.</param>
  ''' <param name="pKnowledgeDate">The p knowledge date.</param>
  ''' <param name="UnitPriceVariant">The unit price variant.</param>
  ''' <returns>System.Double.</returns>
  Public Function GetUnitPrice(ByVal MainForm As VeniceMain, ByVal Permission_Area As String, ByVal pFundID As Integer, ByVal pValueDate As Date, ByVal pStatusGroupFilter As String, ByVal pAdministratorDatesFilter As Integer, ByVal pKnowledgeDate As Date, Optional ByVal UnitPriceVariant As Integer = UnitPriceType.NAV) As Double
    ' ***************************************************** 
    ' Interface function to the SQL SP that returns a set of calculated
    ' Fund unit Prices.
    ' 
    ' Using the Given FundID, only a single value should be calculated
    ' and returned.
    ' ***************************************************** 

    Dim thisAdaptor As New SqlDataAdapter
    Dim PriceTable As New DataTable


    Try
      If (MainForm Is Nothing) Then
        Return 0
      End If

      If (pFundID <= 0) Then
        Return (0)
      End If

      Call MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.GetVeniceConnection(), thisAdaptor, "spu_UnitPrice")
      thisAdaptor.SelectCommand.Parameters("@FundID").Value = pFundID
      thisAdaptor.SelectCommand.Parameters("@ValueDate").Value = pValueDate
      thisAdaptor.SelectCommand.Parameters("@UnitPriceVariant").Value = UnitPriceVariant
      thisAdaptor.SelectCommand.Parameters("@StatusGroupFilter").Value = pStatusGroupFilter
      thisAdaptor.SelectCommand.Parameters("@AdministratorDatesFilter").Value = pAdministratorDatesFilter
      thisAdaptor.SelectCommand.Parameters("@DisregardWatermarkPoints").Value = 0
      thisAdaptor.SelectCommand.Parameters("@KnowledgeDate").Value = pKnowledgeDate

      SyncLock thisAdaptor.SelectCommand.Connection
        thisAdaptor.Fill(PriceTable)
      End SyncLock

      If PriceTable.Rows.Count > 0 Then
        Return CDbl(PriceTable.Rows(0)("UnitPrice"))
      End If
    Catch ex As Exception
    Finally
      Try
        If (thisAdaptor.SelectCommand IsNot Nothing) AndAlso (thisAdaptor.SelectCommand.Connection IsNot Nothing) Then
          thisAdaptor.SelectCommand.Connection.Close()
          thisAdaptor.SelectCommand.Connection = Nothing
        End If
      Catch ex As Exception
      End Try
    End Try

    Return 0

  End Function

  ''' <summary>
  ''' Class clsInstrumentPrice
  ''' </summary>
  Public Class clsInstrumentPrice
    ''' <summary>
    ''' The _ price date
    ''' </summary>
    Private _PriceDate As Date
    ''' <summary>
    ''' The _ price value
    ''' </summary>
    Private _PriceValue As Double
    ''' <summary>
    ''' The _ weight value
    ''' </summary>
    Private _WeightValue As Double
    ''' <summary>
    ''' The _ price return
    ''' </summary>
    Private _PriceReturn As Double
    ''' <summary>
    ''' The _ bips
    ''' </summary>
    Private _Bips As Double
    ''' <summary>
    ''' The _ date is set
    ''' </summary>
    Private _DateIsSet As Boolean
    ''' <summary>
    ''' The _ value is set
    ''' </summary>
    Private _ValueIsSet As Boolean
    ''' <summary>
    ''' The _ weight is set
    ''' </summary>
    Private _WeightIsSet As Boolean
    ''' <summary>
    ''' The _ return is set
    ''' </summary>
    Private _ReturnIsSet As Boolean

    ''' <summary>
    ''' Prevents a default instance of the <see cref="clsInstrumentPrice"/> class from being created.
    ''' </summary>
    Private Sub New()
      _PriceDate = #1/1/1900#
      _PriceValue = 0
      _WeightValue = 0
      _PriceReturn = 0
      _Bips = 0
      _DateIsSet = False
      _ValueIsSet = False
      _WeightIsSet = False
      _ReturnIsSet = False
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="clsInstrumentPrice"/> class.
    ''' </summary>
    ''' <param name="pDate">The p date.</param>
    Public Sub New(ByVal pDate As Date)
      Me.PriceDate = pDate
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="clsInstrumentPrice"/> class.
    ''' </summary>
    ''' <param name="pDate">The p date.</param>
    ''' <param name="pValue">The p value.</param>
    Public Sub New(ByVal pDate As Date, ByVal pValue As Double)
      Me.PriceDate = pDate
      Me.PriceValue = pValue
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="clsInstrumentPrice"/> class.
    ''' </summary>
    ''' <param name="pDate">The p date.</param>
    ''' <param name="pValue">The p value.</param>
    ''' <param name="pReturn">The p return.</param>
    Public Sub New(ByVal pDate As Date, ByVal pValue As Double, ByVal pReturn As Double)
      Me.PriceDate = pDate
      Me.PriceValue = pValue
      Me.PriceReturn = pReturn
    End Sub

    ''' <summary>
    ''' Gets a value indicating whether [date is set].
    ''' </summary>
    ''' <value><c>true</c> if [date is set]; otherwise, <c>false</c>.</value>
    Public ReadOnly Property DateIsSet() As Boolean
      Get
        Return _DateIsSet
      End Get
    End Property

    ''' <summary>
    ''' Gets a value indicating whether [value is set].
    ''' </summary>
    ''' <value><c>true</c> if [value is set]; otherwise, <c>false</c>.</value>
    Public ReadOnly Property ValueIsSet() As Boolean
      Get
        Return _ValueIsSet
      End Get
    End Property

    ''' <summary>
    ''' Gets a value indicating whether [return is set].
    ''' </summary>
    ''' <value><c>true</c> if [return is set]; otherwise, <c>false</c>.</value>
    Public ReadOnly Property ReturnIsSet() As Boolean
      Get
        Return _ReturnIsSet
      End Get
    End Property

    ''' <summary>
    ''' Gets or sets the price date.
    ''' </summary>
    ''' <value>The price date.</value>
    Public Property PriceDate() As Date
      Get
        Return _PriceDate
      End Get
      Set(ByVal value As Date)
        _DateIsSet = True
        _PriceDate = value
      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the price value.
    ''' </summary>
    ''' <value>The price value.</value>
    Public Property PriceValue() As Double
      Get
        Return _PriceValue
      End Get
      Set(ByVal value As Double)
        _ValueIsSet = True
        _PriceValue = value
      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the weight value.
    ''' </summary>
    ''' <value>The weight value.</value>
    Public Property WeightValue() As Double
      Get
        Return _WeightValue
      End Get
      Set(ByVal value As Double)
        _WeightIsSet = True
        _WeightValue = value
      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the price return.
    ''' </summary>
    ''' <value>The price return.</value>
    Public Overloads Property PriceReturn() As Double
      Get
        Return _PriceReturn
      End Get
      Set(ByVal value As Double)
        _ReturnIsSet = True
        _PriceReturn = value
      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the return value.
    ''' </summary>
    ''' <value>The return value.</value>
    Public Overloads Property ReturnValue() As Double
      ' Alternative property to access the PriceReturn value.

      Get
        Return _PriceReturn
      End Get
      Set(ByVal value As Double)
        _ReturnIsSet = True
        _PriceReturn = value
      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the bips.
    ''' </summary>
    ''' <value>The bips.</value>
    Public Property Bips() As Double
      Get
        Return _Bips
      End Get
      Set(ByVal value As Double)
        _Bips = value
      End Set
    End Property

    ''' <summary>
    ''' Sets the price return.
    ''' </summary>
    ''' <param name="value">The value.</param>
    ''' <returns>System.Double.</returns>
    Public Function SetPriceReturn(ByVal value As clsInstrumentPrice) As Double
      If (value Is Nothing) Then
        Return _PriceReturn
      End If

      Try
        If (value.PriceValue <> 0) Then
          _PriceReturn = _PriceValue / value.PriceValue
        End If
      Catch ex As Exception
      End Try

      Return _PriceReturn
    End Function

  End Class

  ''' <summary>
  ''' Class clsInstrumentDetails
  ''' </summary>
  Public Class clsInstrumentDetails
    ''' <summary>
    ''' The _ instrument item type
    ''' </summary>
    Private _InstrumentItemType As RenaissanceGlobals.PortfolioItemType
    ''' <summary>
    ''' The _ instrument ID
    ''' </summary>
    Private _InstrumentID As Integer
    ''' <summary>
    ''' The _ instrument description
    ''' </summary>
    Private _InstrumentDescription As String
    ''' <summary>
    ''' The _ pertrac ID
    ''' </summary>
    Private _PertracID As Integer

    ''' <summary>
    ''' The _ pricing data
    ''' </summary>
    Private _PricingData As clsInstrumentPrice()

    ''' <summary>
    ''' Initializes a new instance of the <see cref="clsInstrumentDetails"/> class.
    ''' </summary>
    Public Sub New()
      _InstrumentItemType = RenaissanceGlobals.PortfolioItemType.Default
      _InstrumentID = 0
      _InstrumentDescription = ""
      _PertracID = 0

      ReDim _PricingData(-1)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="clsInstrumentDetails"/> class.
    ''' </summary>
    ''' <param name="pItemType">Type of the p item.</param>
    ''' <param name="pInstrumentID">The p instrument ID.</param>
    Public Sub New(ByVal pItemType As RenaissanceGlobals.PortfolioItemType, ByVal pInstrumentID As Integer)
      Me.New()

      _InstrumentItemType = pItemType
      _InstrumentID = pInstrumentID
      _InstrumentDescription = ""
      _PertracID = 0

      ReDim _PricingData(-1)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="clsInstrumentDetails"/> class.
    ''' </summary>
    ''' <param name="pItemType">Type of the p item.</param>
    ''' <param name="pInstrumentID">The p instrument ID.</param>
    ''' <param name="pPertracID">The p pertrac ID.</param>
    Public Sub New(ByVal pItemType As RenaissanceGlobals.PortfolioItemType, ByVal pInstrumentID As Integer, ByVal pPertracID As Integer)
      Me.New()

      _InstrumentItemType = pItemType
      _InstrumentID = pInstrumentID
      _InstrumentDescription = ""
      _PertracID = pPertracID

      ReDim _PricingData(-1)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="clsInstrumentDetails"/> class.
    ''' </summary>
    ''' <param name="pItemType">Type of the p item.</param>
    ''' <param name="pInstrumentID">The p instrument ID.</param>
    ''' <param name="pInstrumentDescription">The p instrument description.</param>
    Public Sub New(ByVal pItemType As RenaissanceGlobals.PortfolioItemType, ByVal pInstrumentID As Integer, ByVal pInstrumentDescription As String)
      Me.New()

      _InstrumentItemType = pItemType
      _InstrumentID = pInstrumentID
      _InstrumentDescription = pInstrumentDescription
      _PertracID = 0

      ReDim _PricingData(-1)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="clsInstrumentDetails"/> class.
    ''' </summary>
    ''' <param name="pItemType">Type of the p item.</param>
    ''' <param name="pInstrumentID">The p instrument ID.</param>
    ''' <param name="pInstrumentDescription">The p instrument description.</param>
    ''' <param name="pPertracID">The p pertrac ID.</param>
    Public Sub New(ByVal pItemType As RenaissanceGlobals.PortfolioItemType, ByVal pInstrumentID As Integer, ByVal pInstrumentDescription As String, ByVal pPertracID As Integer)
      Me.New()

      _InstrumentItemType = pItemType
      _InstrumentID = pInstrumentID
      _InstrumentDescription = pInstrumentDescription
      _PertracID = pPertracID

      ReDim _PricingData(-1)
    End Sub

    ''' <summary>
    ''' Initialises the pricing array.
    ''' </summary>
    ''' <param name="PricePeriod">The price period.</param>
    ''' <param name="StartDate">The start date.</param>
    ''' <param name="EndDate">The end date.</param>
    Public Sub InitialisePricingArray(ByVal PricePeriod As RenaissanceGlobals.DealingPeriod, ByVal StartDate As Date, ByVal EndDate As Date)
      Dim ElementCounter As Integer
      Dim thisDate As Date

      ReDim Me.PricingData(GetPeriodCount(PricePeriod, StartDate, EndDate))

      thisDate = FitDateToPeriod(PricePeriod, StartDate, True)
      For ElementCounter = 0 To (Me.PricingData.Length - 1)
        Me.PricingData(ElementCounter) = New clsInstrumentPrice(thisDate)

        thisDate = AddPeriodToDate(PricePeriod, thisDate, 1)
      Next
    End Sub

    ''' <summary>
    ''' Gets or sets the type of the instrument item.
    ''' </summary>
    ''' <value>The type of the instrument item.</value>
    Public Property InstrumentItemType() As RenaissanceGlobals.PortfolioItemType
      Get
        Return (_InstrumentItemType)
      End Get
      Set(ByVal value As RenaissanceGlobals.PortfolioItemType)
        _InstrumentItemType = value
      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the instrument ID.
    ''' </summary>
    ''' <value>The instrument ID.</value>
    Public Property InstrumentID() As Integer
      Get
        Return _InstrumentID
      End Get
      Set(ByVal value As Integer)
        _InstrumentID = value
      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the instrument description.
    ''' </summary>
    ''' <value>The instrument description.</value>
    Public Property InstrumentDescription() As String
      Get
        Return _InstrumentDescription
      End Get
      Set(ByVal value As String)
        _InstrumentDescription = value
      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the pertrac ID.
    ''' </summary>
    ''' <value>The pertrac ID.</value>
    Public Property PertracID() As Integer
      Get
        Return _PertracID
      End Get
      Set(ByVal value As Integer)
        _PertracID = value
      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the pricing data.
    ''' </summary>
    ''' <value>The pricing data.</value>
    Public Property PricingData() As clsInstrumentPrice()
      Get
        Return _PricingData
      End Get
      Set(ByVal value As clsInstrumentPrice())
        If value Is Nothing Then
          ReDim _PricingData(-1)
        Else
          _PricingData = value
        End If
      End Set
    End Property

    ''' <summary>
    ''' Gets or sets the pricing data.
    ''' </summary>
    ''' <value>The pricing data.</value>
    Public Property PricingData(ByVal Index As Integer) As clsInstrumentPrice
      Get
        If (Index >= 0) AndAlso (Not (_PricingData Is Nothing)) Then
          If (Index < _PricingData.Length) Then
            Return _PricingData(Index)
          End If
        End If

        Return Nothing
      End Get
      Set(ByVal value As clsInstrumentPrice)
        If (_PricingData Is Nothing) Then
          ReDim _PricingData(Index + 100)
        End If

        If (_PricingData.Length <= Index) Then
          ReDim Preserve _PricingData(Index + 100)
        End If

        _PricingData(Index) = value
      End Set
    End Property
  End Class

  '  Public Function GetFundPriceArray(ByVal pMainForm As Venice.VeniceMain, _
  '                ByVal PricePeriod As RenaissanceGlobals.DealingPeriod, _
  '                ByVal pStartDate As Date, _
  '                ByVal pEndDate As Date, _
  '                ByVal pInitialFundID As Integer, _
  '                ByVal pFundID As Integer, _
  '                ByVal pKnowledgedate As Date) As clsInstrumentPrice()

  '    ' **********************************************************************************
  '    '
  '    ' Returns array of InstrumentPrices, ordered in Ascending Date order
  '    '
  '    ' **********************************************************************************

  '    Dim ReturnArray() As clsInstrumentPrice = Nothing

  '    ' First establish the required number of months
  '    Dim PeriodCount As Integer = 0
  '    Dim StartDate As Date
  '    Dim EndDate As Date

  '    Dim InstrumentID As Integer

  '    If (pStartDate.CompareTo(pEndDate) <= 0) Then
  '      StartDate = pStartDate
  '      EndDate = pEndDate
  '    Else
  '      EndDate = pStartDate
  '      StartDate = pEndDate
  '    End If

  '    ' Set StartDate to the first of the month, Set LastDate to the end of the month

  '    StartDate = FitDateToPeriod(PricePeriod, StartDate, False)
  '    EndDate = FitDateToPeriod(PricePeriod, EndDate, True)
  '    'StartDate = StartDate.AddDays(1 - StartDate.Day)
  '    'EndDate = EndDate.AddMonths(1)
  '    'EndDate = EndDate.AddDays(0 - EndDate.Day)

  '    Try
  '      Dim thisdate As Date

  '      PeriodCount = GetPeriodCount(PricePeriod, StartDate, EndDate)

  '      'thisdate = StartDate
  '      'PeriodCount = 0
  '      'While thisdate.CompareTo(EndDate) <= 0 '  (thisdate.Year < EndDate.Year) Or (thisdate.Month <= EndDate.Month)
  '      '  PeriodCount += 1

  '      '  thisdate = AddPeriodToDate(PricePeriod, thisdate)
  '      '  'thisdate = thisdate.AddMonths(1)
  '      'End While

  '      If (PeriodCount <= 0) Then
  '        GoTo Exit_GetFundPriceArray
  '      End If

  '      ' Size the return array
  '      ReDim ReturnArray(PeriodCount - 1)

  '      ' Initialise the return array

  '      thisdate = StartDate
  '      PeriodCount = 0
  '      Dim PeriodEndDate As Date
  '      While thisdate.CompareTo(EndDate) <= 0 ' (thisdate.Year < EndDate.Year) Or (thisdate.Month <= EndDate.Month)

  '        PeriodEndDate = FitDateToPeriod(PricePeriod, thisdate, True)

  '        'MonthEndDate = thisdate.AddMonths(1)
  '        'MonthEndDate = MonthEndDate.AddDays(0 - MonthEndDate.Day)

  '        ReturnArray(PeriodCount) = New clsInstrumentPrice(PeriodEndDate)

  '        PeriodCount += 1
  '        thisdate = AddPeriodToDate(PricePeriod, thisdate) '   thisdate.AddMonths(1)
  '      End While

  '    Catch ex As Exception
  '      GoTo Exit_GetFundPriceArray
  '    End Try

  '    ' Resolve Instrument ID

  '    Try
  '      Dim DS_Fund As RenaissanceDataClass.DSFund
  '      Dim tbl_Fund As RenaissanceDataClass.DSFund.tblFundDataTable
  '      Dim SelectedFundRows As RenaissanceDataClass.DSFund.tblFundRow() = Nothing

  '      DS_Fund = pMainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblFund)
  '      If (DS_Fund Is Nothing) Then GoTo Exit_GetFundPriceArray
  '      tbl_Fund = DS_Fund.tblFund

  '      SelectedFundRows = tbl_Fund.Select("FundID=" & pFundID.ToString)
  '      If (SelectedFundRows Is Nothing) Then GoTo Exit_GetFundPriceArray

  '      If (SelectedFundRows(0).FundUnit <= 0) Then GoTo Exit_GetFundPriceArray
  '      InstrumentID = SelectedFundRows(0).FundUnit
  '    Catch ex As Exception
  '      GoTo Exit_GetFundPriceArray
  '    End Try

  '    ' Get Pricing Data

  '    Dim DS_Price As RenaissanceDataClass.DSPrice
  '    Dim tbl_Price As RenaissanceDataClass.DSPrice.tblPriceDataTable
  '    Dim SelectedPriceRows As RenaissanceDataClass.DSPrice.tblPriceRow()
  '    Dim thisPriceRow As RenaissanceDataClass.DSPrice.tblPriceRow

  '    DS_Price = pMainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblPrice)
  '    If (DS_Price Is Nothing) Then GoTo Exit_GetFundPriceArray
  '    tbl_Price = DS_Price.tblPrice

  '    SelectedPriceRows = tbl_Price.Select("PriceInstrument=" & InstrumentID.ToString, "PriceDate Desc, RN Desc")
  '    If (SelectedPriceRows Is Nothing) OrElse (SelectedPriceRows.Length <= 0) Then
  '      ' What if there is no data returned ?
  '      ' OK, Check for an InFill Fund Instrument, otherwise GOTO the end of he function and return Nothing.

  '      Dim FillInFundID As String
  '      Try
  '        FillInFundID = Get_ReportCustomisationAttribute(pMainForm.MainAdaptorHandler, pMainForm.MainDataHandler, VENICE_CustomReport_Benchmark_ApplicationName, pInitialFundID, "FundAttribute", "BackFill_" & pFundID.ToString)

  '        If IsNumeric(FillInFundID) AndAlso (CInt(FillInFundID) > 0) Then
  '          Return GetFundPriceArray(pMainForm, PricePeriod, StartDate, EndDate, pInitialFundID, CInt(FillInFundID), pKnowledgedate)
  '        End If
  '      Catch ex As Exception
  '      End Try

  '      GoTo Exit_GetFundPriceArray

  '    End If


  '    ' Set values in the Price Array
  '    Try
  '      Dim RowCounter As Integer
  '      Dim LastDate As Date
  '      Dim MilestoneDateSet As Boolean
  '      Dim PriceIndex As Integer
  '      Dim ThisDate As Date
  '      Dim FirstDateFlag As Boolean = True

  '      LastDate = #1/1/1900#
  '      MilestoneDateSet = False

  '      For RowCounter = 0 To (SelectedPriceRows.Length - 1)
  '        thisPriceRow = SelectedPriceRows(RowCounter)
  '        ThisDate = thisPriceRow.PriceDate

  '        If (ThisDate.CompareTo(StartDate) >= 0) And (ThisDate.CompareTo(EndDate) <= 0) Then
  '          If AreDatesEquivalent(PricePeriod, ThisDate, LastDate) = False Then
  '            ' If (ThisDate.Year <> LastDate.Year) Or (ThisDate.Month <> LastDate.Month) Then
  '            ' New Date 

  '            PriceIndex = GetPriceIndex(PricePeriod, StartDate, ThisDate) ' (ThisDate.Month - StartDate.Month) + ((ThisDate.Year - StartDate.Year) * 12)
  '            ReturnArray(PriceIndex).PriceValue = thisPriceRow.PriceLevel

  '            LastDate = ThisDate

  '            ' Is this price a Milestone price ?
  '            If (thisPriceRow.PriceIsMilestone = True) Then
  '              MilestoneDateSet = True
  '            Else
  '              MilestoneDateSet = False
  '            End If
  '          ElseIf (MilestoneDateSet = False) AndAlso (thisPriceRow.PriceIsMilestone = True) AndAlso (AreDatesEquivalent(PricePeriod, ThisDate, LastDate) = True) Then
  '            ' Set if Milestone

  '            PriceIndex = GetPriceIndex(PricePeriod, StartDate, ThisDate) ' (ThisDate.Month - StartDate.Month) + ((ThisDate.Year - StartDate.Year) * 12)
  '            ReturnArray(PriceIndex).PriceValue = thisPriceRow.PriceLevel

  '            MilestoneDateSet = True
  '          End If
  '        End If

  '      Next

  '      ' Infill Fund Data

  '      ' At this point 'LastDate' should be the earliest date (or at least in the same month as) retrieved for the
  '      ' current Fund Instrument.
  '      ' If this date is later than the requested start date, then try to Infill with an alternative Fund Instrument.

  '      ' (ThisDate.Year <> LastDate.Year) Or (ThisDate.Month <> LastDate.Month)
  '      ' StartDate

  '      If AreDatesEquivalent(PricePeriod, LastDate, StartDate) = False Then ' (LastDate.Year <> StartDate.Year) OrElse (LastDate.Month > StartDate.Month) Then
  '        Dim FillInFundID As String
  '        Try
  '          FillInFundID = Get_ReportCustomisationAttribute(pMainForm.MainAdaptorHandler, pMainForm.MainDataHandler, VENICE_CustomReport_Benchmark_ApplicationName, pInitialFundID, "FundAttribute", "BackFill_" & pFundID.ToString)

  '          If IsNumeric(FillInFundID) AndAlso (CInt(FillInFundID) > 0) Then
  '            Dim InFillReturnArray() As clsInstrumentPrice = Nothing

  '            If (LastDate = #1/1/1900#) Or (LastDate.CompareTo(EndDate) > 0) Then
  '              ' Lastdate is outside of Start/End Date range. This means that there were no dates returned in the Start/End Range
  '              ' Thus just return the In-Fill array using original Start/End Dates.
  '              Return GetFundPriceArray(pMainForm, PricePeriod, StartDate, EndDate, pInitialFundID, CInt(FillInFundID), pKnowledgedate)
  '            End If

  '            InFillReturnArray = GetFundPriceArray(pMainForm, PricePeriod, StartDate, LastDate, pInitialFundID, CInt(FillInFundID), pKnowledgedate)

  '            If (Not (InFillReturnArray Is Nothing)) AndAlso (InFillReturnArray.Length > 1) Then
  '              ' Note Look for length > 1 as the last element should overlap and an array length of
  '              ' One is of no use.
  '              '
  '              ' Does the End of the In-Fill table match the start of the higher level table ?
  '              ' i.e. Is a Price Value set for the last array element ?

  '              If InFillReturnArray(InFillReturnArray.Length - 1).ValueIsSet = True Then
  '                ' OK, Stitch Fund Value Table Together.
  '                ' Note. The 'Get_ReportCustomisationAttribute()' function will return an array for 
  '                ' each month between start and end dates, with element Zero representing the month of the start
  '                ' date. This means that the in-fill elements match the higher-level array elements.
  '                ' e.g. Element 0 represents 'StartDate' in both arrays, thus there is no need to work out
  '                ' which element matches which element.

  '                Dim ContinuityMultiplier As Double = 1.0#
  '                Dim InFillIndex As Integer

  '                ' Adjust InFill NAV to be consistent with Higher-Level data

  '                InFillIndex = (InFillReturnArray.Length - 1)

  '                ' ReturnArray
  '                If (InFillReturnArray(InFillIndex).PriceValue <> 0) Then
  '                  ContinuityMultiplier = ReturnArray(InFillIndex).PriceValue / InFillReturnArray(InFillIndex).PriceValue
  '                End If

  '                For InFillIndex = 0 To (InFillReturnArray.Length - 2) ' Note : Don't use the last element, The arrays overlap by one element.
  '                  ReturnArray(InFillIndex).PriceValue = InFillReturnArray(InFillIndex).PriceValue * ContinuityMultiplier
  '                Next

  '              Else
  '                pMainForm.LogError("GetFundPriceArray()", RenaissanceGlobals.LOG_LEVELS.Warning, "", "Mis-Match in In-Fill End and Base-Fund Start Dates." & vbCrLf & "Fund " & pFundID.ToString & " vs Fund " & FillInFundID.ToString, "", True)
  '              End If

  '            End If ' InFill data returned OK

  '          End If ' InFill FundID OK 


  '        Catch Inner_Ex As Exception
  '          pMainForm.LogError("GetFundPriceArray()", RenaissanceGlobals.LOG_LEVELS.Error, Inner_Ex.Message, "Error Getting InFill Fund pricing.", Inner_Ex.StackTrace, True)
  '        End Try

  '      End If

  '    Catch ex As Exception
  '      pMainForm.LogError("GetFundPriceArray()", RenaissanceGlobals.LOG_LEVELS.Error, ex.Message, "Error Getting Fund prices.", ex.StackTrace, True)
  '    End Try

  'Exit_GetFundPriceArray:

  '    ' ForwardFill / Backfill / Calculate Returns.

  '    Try
  '      If (Not (ReturnArray Is Nothing)) Then
  '        Dim PortfolioIndex As Integer

  '        ' ForwardFill 
  '        For PortfolioIndex = 1 To (ReturnArray.Length - 1)
  '          If (ReturnArray(PortfolioIndex).ValueIsSet = False) AndAlso (ReturnArray(PortfolioIndex - 1).ValueIsSet = True) Then
  '            ReturnArray(PortfolioIndex).PriceValue = ReturnArray(PortfolioIndex - 1).PriceValue
  '          End If
  '        Next

  '        ' Backfill
  '        For PortfolioIndex = (ReturnArray.Length - 2) To 0 Step -1
  '          If (ReturnArray(PortfolioIndex).ValueIsSet = False) AndAlso (ReturnArray(PortfolioIndex + 1).ValueIsSet = True) Then
  '            ReturnArray(PortfolioIndex).PriceValue = ReturnArray(PortfolioIndex + 1).PriceValue
  '          End If
  '        Next

  '        ' Return 
  '        For PortfolioIndex = 1 To (ReturnArray.Length - 1)
  '          If (ReturnArray(PortfolioIndex).ValueIsSet = True) AndAlso (ReturnArray(PortfolioIndex - 1).ValueIsSet = True) Then
  '            If (ReturnArray(PortfolioIndex - 1).PriceValue = 0) Then
  '              ReturnArray(PortfolioIndex).PriceReturn = 0.0#
  '            Else
  '              ReturnArray(PortfolioIndex).PriceReturn = (ReturnArray(PortfolioIndex).PriceValue / ReturnArray(PortfolioIndex - 1).PriceValue) - 1.0#
  '            End If
  '          Else
  '            ReturnArray(PortfolioIndex).PriceReturn = 0.0#
  '          End If
  '        Next
  '      End If ' Not (ReturnArray Is Nothing)
  '    Catch ex As Exception
  '    End Try

  '    Return ReturnArray
  '  End Function

  ''' <summary>
  ''' Gets the last instrument price date.
  ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  ''' <param name="pInstrumentID">The p instrument ID.</param>
  ''' <returns>System.DateTime.</returns>
  Public Function GetLastInstrumentPriceDate(ByVal pMainForm As Venice.VeniceMain, ByVal pInstrumentID As Integer) As Date
    ' **********************************************************************************
    '
    ' Returns date of last Pertrac Data item.
    '
    ' **********************************************************************************

    Dim RVal As Object = Nothing
    Dim thisCommand As SqlCommand = Nothing

    Try

      Dim Query As String
      Query = "Select MAX(PriceDate) FROM dbo.fn_tblPrice_SelectByInstrument(" & pInstrumentID.ToString & ", '" & KNOWLEDGEDATE_NOW.ToString(QUERY_SHORTDATEFORMAT) & "')"

      thisCommand = New SqlCommand(Query, pMainForm.GetVeniceConnection())

      RVal = thisCommand.ExecuteScalar

    Catch ex As Exception
      Return Renaissance_BaseDate
    Finally
      Try
        If (thisCommand IsNot Nothing) AndAlso (thisCommand.Connection IsNot Nothing) Then
          thisCommand.Connection.Close()
          thisCommand.Connection = Nothing
        End If
      Catch ex As Exception
      End Try
    End Try

    Try
      If IsDate(RVal) Then
        Return CDate(RVal)
      Else
        Return Renaissance_BaseDate
      End If
    Catch ex As Exception
      Return Renaissance_BaseDate
    End Try
  End Function

  ''' <summary>
  ''' Gets the instrument price array.
  ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  ''' <param name="PricePeriod">The price period.</param>
  ''' <param name="pStartDate">The p start date.</param>
  ''' <param name="pEndDate">The p end date.</param>
  ''' <param name="pInstrumentID">The p instrument ID.</param>
  ''' <param name="pKnowledgedate">The p knowledgedate.</param>
  ''' <returns>clsInstrumentPrice[][].</returns>
  Public Function GetInstrumentPriceArray(ByVal pMainForm As Venice.VeniceMain, _
   ByVal PricePeriod As RenaissanceGlobals.DealingPeriod, _
   ByVal pStartDate As Date, _
   ByVal pEndDate As Date, _
   ByVal pInstrumentID As Integer, _
   ByVal pKnowledgedate As Date) As clsInstrumentPrice()

    ' **********************************************************************************
    '
    ' Returns array of InstrumentPrices, ordered in Ascending Date order
    ' The Prices returned are the Unadjusted Instrument Prices. i.e. PriceNAV rather then PriceLevel.
    '
    ' **********************************************************************************

    Dim ReturnArray() As clsInstrumentPrice = Nothing

    ' First establish the required number of months
    Dim PeriodCount As Integer = 0
    Dim StartDate As Date
    Dim EndDate As Date

    Dim InstrumentID As Integer

    If (pStartDate.CompareTo(pEndDate) <= 0) Then
      StartDate = pStartDate
      EndDate = pEndDate
    Else
      EndDate = pStartDate
      StartDate = pEndDate
    End If

    ' Set StartDate to the first of the month, Set LastDate to the end of the month

    StartDate = FitDateToPeriod(PricePeriod, StartDate, False)
    EndDate = FitDateToPeriod(PricePeriod, EndDate, True)

    Try
      Dim thisdate As Date

      PeriodCount = GetPeriodCount(PricePeriod, StartDate, EndDate)

      If (PeriodCount <= 0) Then
        GoTo Exit_GetFundPriceArray
      End If

      ' Size the return array
      ReDim ReturnArray(PeriodCount - 1)

      ' Initialise the return array

      thisdate = StartDate
      PeriodCount = 0
      While thisdate.CompareTo(EndDate) <= 0 ' (thisdate.Year < EndDate.Year) Or (thisdate.Month <= EndDate.Month)
        Dim PeriodEndDate As Date

        PeriodEndDate = FitDateToPeriod(PricePeriod, thisdate, True)

        ReturnArray(PeriodCount) = New clsInstrumentPrice(PeriodEndDate)

        PeriodCount += 1
        thisdate = AddPeriodToDate(PricePeriod, thisdate) '   thisdate.AddMonths(1)
      End While

    Catch ex As Exception
      GoTo Exit_GetFundPriceArray
    End Try

    ' Resolve Instrument ID

    InstrumentID = pInstrumentID

    ' Get Pricing Data

    Dim tbl_Price As New RenaissanceDataClass.DSPrice.tblPriceDataTable
    Dim SelectedPriceRows As RenaissanceDataClass.DSPrice.tblPriceRow()
    Dim thisPriceRow As RenaissanceDataClass.DSPrice.tblPriceRow

    Dim myAdaptor As New SqlDataAdapter
    Try
      pMainForm.MainAdaptorHandler.Set_AdaptorCommands(pMainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION), myAdaptor, "TBLPRICEBYINSTRUMENT")

      myAdaptor.SelectCommand.Parameters("@InstrumentID").Value = CInt(InstrumentID)
      myAdaptor.SelectCommand.Parameters("@Knowledgedate").Value = pMainForm.Main_Knowledgedate

      If (tbl_Price Is Nothing) Then
        tbl_Price = New RenaissanceDataClass.DSPrice.tblPriceDataTable
      Else
        tbl_Price.Clear()
      End If

      SyncLock myAdaptor.SelectCommand.Connection
        pMainForm.LoadTable_Custom(tbl_Price, myAdaptor.SelectCommand)
        'tbl_Price.Load(myAdaptor.SelectCommand.ExecuteReader)
      End SyncLock

    Catch ex As Exception
      pMainForm.LogError("PriceFunctions() ,GetInstrumentPriceArray()", LOG_LEVELS.Error, ex.Message, "Error Loading / Selecting Instrument Prices.", ex.StackTrace, True)

      GoTo Exit_GetFundPriceArray
    Finally
      Try
        If (myAdaptor.SelectCommand IsNot Nothing) Then
          myAdaptor.SelectCommand.Connection = Nothing
        End If
      Catch ex As Exception
      End Try
    End Try

    SelectedPriceRows = tbl_Price.Select("True", "PriceDate Desc, RN Desc")
    If (SelectedPriceRows Is Nothing) OrElse (SelectedPriceRows.Length <= 0) Then
      ' What if there is no data returned ?

      GoTo Exit_GetFundPriceArray

    End If


    ' Set values in the Price Array
    Try
      Dim RowCounter As Integer
      Dim LastDate As Date
      Dim MilestoneDateSet As Boolean
      Dim PriceIndex As Integer
      Dim ThisDate As Date
      Dim FirstDateFlag As Boolean = True

      LastDate = #1/1/1900#
      MilestoneDateSet = False

      For RowCounter = 0 To (SelectedPriceRows.Length - 1)
        thisPriceRow = SelectedPriceRows(RowCounter)
        ThisDate = thisPriceRow.PriceDate

        If (ThisDate.CompareTo(StartDate) >= 0) And (ThisDate.CompareTo(EndDate) <= 0) Then
          If AreDatesEquivalent(PricePeriod, ThisDate, LastDate) = False Then
            ' If (ThisDate.Year <> LastDate.Year) Or (ThisDate.Month <> LastDate.Month) Then
            ' New Date 

            PriceIndex = GetPriceIndex(PricePeriod, StartDate, ThisDate) ' (ThisDate.Month - StartDate.Month) + ((ThisDate.Year - StartDate.Year) * 12)
            ReturnArray(PriceIndex).PriceValue = thisPriceRow.PriceNAV ' PriceLevel
            If (thisPriceRow.PriceIsPercent = True) Then
              ReturnArray(PriceIndex).ReturnValue = thisPriceRow.PricePercent
            End If
            LastDate = ThisDate

            ' Is this price a Milestone price ?
            If (thisPriceRow.PriceIsMilestone = True) Then
              MilestoneDateSet = True
            Else
              MilestoneDateSet = False
            End If
          ElseIf (MilestoneDateSet = False) AndAlso (thisPriceRow.PriceIsMilestone = True) AndAlso (AreDatesEquivalent(PricePeriod, ThisDate, LastDate) = True) Then
            ' Set if Milestone

            PriceIndex = GetPriceIndex(PricePeriod, StartDate, ThisDate) ' (ThisDate.Month - StartDate.Month) + ((ThisDate.Year - StartDate.Year) * 12)
            ReturnArray(PriceIndex).PriceValue = thisPriceRow.PriceNAV ' PriceLevel

            MilestoneDateSet = True
          End If
        End If

      Next

    Catch ex As Exception
      pMainForm.LogError("GetFundPriceArray()", RenaissanceGlobals.LOG_LEVELS.Error, ex.Message, "Error Getting Fund prices.", ex.StackTrace, True)
    End Try

Exit_GetFundPriceArray:

    ' ForwardFill / Backfill / Calculate Returns.

    Try
      If (Not (ReturnArray Is Nothing)) AndAlso (ReturnArray.Length > 0) Then
        Dim PortfolioIndex As Integer

        ' ForwardFill - Gives priority to Price Returns (if present)

        For PortfolioIndex = 1 To (ReturnArray.Length - 1)
          If (ReturnArray(PortfolioIndex - 1).ValueIsSet = True) Then
            If (ReturnArray(PortfolioIndex).ReturnIsSet = True) Then
              If (Math.Abs(ReturnArray(PortfolioIndex).PriceValue - ((ReturnArray(PortfolioIndex).PriceReturn + 1.0#) * ReturnArray(PortfolioIndex - 1).PriceValue)) > 0.0001) Then
                ReturnArray(PortfolioIndex).PriceValue = (ReturnArray(PortfolioIndex).PriceReturn + 1.0#) * ReturnArray(PortfolioIndex - 1).PriceValue
              End If
            ElseIf (ReturnArray(PortfolioIndex).ValueIsSet = True) AndAlso (ReturnArray(PortfolioIndex).PriceValue <> 0) Then
              ReturnArray(PortfolioIndex).ReturnValue = (ReturnArray(PortfolioIndex).PriceValue / ReturnArray(PortfolioIndex - 1).PriceValue) - 1.0#
            Else
              ReturnArray(PortfolioIndex).PriceValue = ReturnArray(PortfolioIndex - 1).PriceValue
              ReturnArray(PortfolioIndex).ReturnValue = 0
            End If
          End If
        Next

        ' Backfill

        For PortfolioIndex = (ReturnArray.Length - 2) To 0 Step -1
          If (ReturnArray(PortfolioIndex + 1).ValueIsSet = True) Then
            If (ReturnArray(PortfolioIndex + 1).ReturnIsSet = True) Then
              If (ReturnArray(PortfolioIndex).ValueIsSet = True) Then
                If (Math.Abs(ReturnArray(PortfolioIndex).PriceValue - (ReturnArray(PortfolioIndex + 1).PriceValue / (ReturnArray(PortfolioIndex + 1).ReturnValue + 1.0#))) > 0.0001) Then
                  ReturnArray(PortfolioIndex).PriceValue = (ReturnArray(PortfolioIndex + 1).PriceValue / (ReturnArray(PortfolioIndex + 1).ReturnValue + 1.0#))
                End If
              Else
                ReturnArray(PortfolioIndex).PriceValue = (ReturnArray(PortfolioIndex + 1).PriceValue / (ReturnArray(PortfolioIndex + 1).ReturnValue + 1.0#))
              End If
            ElseIf (ReturnArray(PortfolioIndex).ValueIsSet = False) Then
              ReturnArray(PortfolioIndex).PriceValue = ReturnArray(PortfolioIndex + 1).PriceValue
            End If
          End If
        Next

        ' Return 

        For PortfolioIndex = 1 To (ReturnArray.Length - 1)
          If (ReturnArray(PortfolioIndex).ReturnIsSet = True) Then
            If (Math.Abs(ReturnArray(PortfolioIndex).PriceValue - ((ReturnArray(PortfolioIndex).PriceReturn + 1.0#) * ReturnArray(PortfolioIndex - 1).PriceValue)) > 0.0001) Then
              ReturnArray(PortfolioIndex).PriceValue = (ReturnArray(PortfolioIndex).PriceReturn + 1.0#) * ReturnArray(PortfolioIndex - 1).PriceValue
            End If
          ElseIf (ReturnArray(PortfolioIndex).ValueIsSet = False) AndAlso (ReturnArray(PortfolioIndex).ReturnIsSet = False) Then
            ReturnArray(PortfolioIndex).PriceValue = ReturnArray(PortfolioIndex - 1).PriceValue
          ElseIf (ReturnArray(PortfolioIndex).ValueIsSet = True) AndAlso (ReturnArray(PortfolioIndex - 1).ValueIsSet = True) Then
            If (ReturnArray(PortfolioIndex - 1).PriceValue = 0) Then
              ReturnArray(PortfolioIndex).PriceReturn = 0.0#
            Else
              ReturnArray(PortfolioIndex).PriceReturn = (ReturnArray(PortfolioIndex).PriceValue / ReturnArray(PortfolioIndex - 1).PriceValue) - 1.0#
            End If
          Else
            ReturnArray(PortfolioIndex).PriceReturn = 0.0#
          End If
        Next
      End If ' Not (ReturnArray Is Nothing)
    Catch ex As Exception
    End Try

    Return ReturnArray
  End Function

  ''' <summary>
  ''' Gets the last pertrac price date.
  ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  ''' <param name="pPertracID">The p pertrac ID.</param>
  ''' <returns>System.DateTime.</returns>
  Public Function GetLastPertracPriceDate(ByVal pMainForm As Venice.VeniceMain, ByVal pPertracID As Integer) As Date
    ' **********************************************************************************
    '
    ' Returns date of last Pertrac Data item.
    '
    ' **********************************************************************************

    Dim RVal As Object = Nothing
    Dim thisCommand As SqlCommand = Nothing

    Try

      Dim Query As String
      Query = "Select dbo.fn_tblPerformance_MaxDate(" & pPertracID.ToString & ")"

      thisCommand = New SqlCommand(Query, pMainForm.GetVeniceConnection())

      RVal = thisCommand.ExecuteScalar

    Catch ex As Exception
      Return Renaissance_BaseDate
    Finally
      Try
        If (thisCommand IsNot Nothing) AndAlso (thisCommand.Connection IsNot Nothing) Then
          thisCommand.Connection.Close()
          thisCommand.Connection = Nothing
        End If
      Catch ex As Exception
      End Try
    End Try

    Try
      If IsDate(RVal) Then
        Return CDate(RVal)
      Else
        Return Renaissance_BaseDate
      End If
    Catch ex As Exception
      Return Renaissance_BaseDate
    End Try

  End Function

  ''' <summary>
  ''' Gets the pertrac price array.
  ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  ''' <param name="PricePeriod">The price period.</param>
  ''' <param name="pStartDate">The p start date.</param>
  ''' <param name="pEndDate">The p end date.</param>
  ''' <param name="pInitialFundID">The p initial fund ID.</param>
  ''' <param name="pPertracID">The p pertrac ID.</param>
  ''' <param name="pKnowledgedate">The p knowledgedate.</param>
  ''' <returns>clsInstrumentPrice[][].</returns>
  Public Function GetPertracPriceArray(ByVal pMainForm As Venice.VeniceMain, _
   ByVal PricePeriod As RenaissanceGlobals.DealingPeriod, _
   ByVal pStartDate As Date, _
   ByVal pEndDate As Date, _
   ByVal pInitialFundID As Integer, _
   ByVal pPertracID As Integer, _
   ByVal pKnowledgedate As Date) As clsInstrumentPrice()

    ' **********************************************************************************
    '
    ' Returns array of InstrumentPrices, ordered in Ascending Date order
    '
    ' **********************************************************************************

    Dim ReturnArray() As clsInstrumentPrice = Nothing

    ' *******************************************************************
    ' First establish the required number of months
    ' *******************************************************************

    Dim PeriodCount As Integer = 0
    Dim StartDate As Date
    Dim EndDate As Date

    Dim PertracID As Integer

    Try

      If (pStartDate.CompareTo(pEndDate) <= 0) Then
        StartDate = pStartDate
        EndDate = pEndDate
      Else
        EndDate = pStartDate
        StartDate = pEndDate
      End If

      ' Set StartDate to the first of the month, Set LastDate to the end of the month

      StartDate = FitDateToPeriod(PricePeriod, StartDate, False)
      EndDate = FitDateToPeriod(PricePeriod, EndDate, True)
      PertracID = pPertracID

      PeriodCount = GetPeriodCount(PricePeriod, StartDate, EndDate)

      If (PeriodCount <= 0) Then
        GoTo Exit_GetPertracPriceArray
      End If

    Catch ex As Exception
      GoTo Exit_GetPertracPriceArray
    End Try

    ' Initialise the return array

    Try
      Dim thisdate As Date

      ' Size the return array
      ReDim ReturnArray(PeriodCount - 1)

      ' Initialise the return array

      thisdate = StartDate
      PeriodCount = 0

      Dim PeriodEndDate As Date
      While thisdate.CompareTo(EndDate) <= 0 ' (thisdate.Year < EndDate.Year) Or (thisdate.Month <= EndDate.Month)

        PeriodEndDate = FitDateToPeriod(PricePeriod, thisdate, True)

        ReturnArray(PeriodCount) = New clsInstrumentPrice(PeriodEndDate)

        PeriodCount += 1
        thisdate = AddPeriodToDate(PricePeriod, thisdate) '   thisdate.AddMonths(1)
      End While

    Catch ex As Exception
      GoTo Exit_GetPertracPriceArray
    End Try

    ' Get Pricing Data

    Dim adpPerformance As New SqlDataAdapter
    Dim tbl_Performance As New RenaissanceDataClass.DSInstrumentPerformance.tblInstrumentPerformanceDataTable
    Dim SelectedPerformanceRows As RenaissanceDataClass.DSInstrumentPerformance.tblInstrumentPerformanceRow()
    Dim thisPerformanceRow As RenaissanceDataClass.DSInstrumentPerformance.tblInstrumentPerformanceRow

    Try
      Call pMainForm.MainAdaptorHandler.Set_AdaptorCommands(pMainForm.GetVeniceConnection(), adpPerformance, "tblPerformance")
      If (adpPerformance.SelectCommand Is Nothing) OrElse (adpPerformance.SelectCommand.Connection Is Nothing) Then
        GoTo Exit_GetPertracPriceArray
      End If

      adpPerformance.SelectCommand.Parameters("@ID").Value = PertracID
      adpPerformance.SelectCommand.Parameters("@Date").Value = DBNull.Value

      SyncLock adpPerformance.SelectCommand.Connection
        SyncLock tbl_Performance
          adpPerformance.Fill(tbl_Performance)
        End SyncLock
      End SyncLock

    Catch ex As Exception
      pMainForm.LogError("GetPertracPriceArray()", LOG_LEVELS.Error, ex.Message, "Error getting data for Pertrac ID " & PertracID.ToString, ex.StackTrace, True)
    Finally
      Try
        If (adpPerformance.SelectCommand IsNot Nothing) AndAlso (adpPerformance.SelectCommand.Connection IsNot Nothing) Then
          adpPerformance.SelectCommand.Connection.Close()
          adpPerformance.SelectCommand.Connection = Nothing
        End If
      Catch ex As Exception
      End Try
    End Try

    If (tbl_Performance Is Nothing) Then GoTo Exit_GetPertracPriceArray

    SelectedPerformanceRows = tbl_Performance.Select("ID=" & PertracID.ToString, "PerformanceDate Desc")
    If (SelectedPerformanceRows Is Nothing) OrElse (SelectedPerformanceRows.Length <= 0) Then
      Dim FillInPertracID As String

      FillInPertracID = Get_ReportCustomisationAttribute(pMainForm.MainAdaptorHandler, pMainForm.MainDataHandler, VENICE_CustomReport_Benchmark_ApplicationName, pInitialFundID, "FundAttribute", "BackFillPertracID_" & PertracID.ToString)

      If IsNumeric(FillInPertracID) AndAlso (CInt(FillInPertracID) > 0) Then
        Return GetPertracPriceArray(pMainForm, PricePeriod, StartDate, EndDate, pInitialFundID, CInt(FillInPertracID), pKnowledgedate)
      End If

      GoTo Exit_GetPertracPriceArray
    End If

    ' **********************************************************
    ' Set values in the Price Array
    ' **********************************************************

    Try
      Dim RowCounter As Integer
      Dim LastDate As Date
      Dim MilestoneDateSet As Boolean
      Dim PriceIndex As Integer
      Dim ThisDate As Date
      Dim FirstDateFlag As Boolean = True

      LastDate = #1/1/1900#
      MilestoneDateSet = False

      For RowCounter = 0 To (SelectedPerformanceRows.Length - 1)
        thisPerformanceRow = SelectedPerformanceRows(RowCounter)
        ThisDate = thisPerformanceRow.PerformanceDate

        If (ThisDate.CompareTo(StartDate) >= 0) And (ThisDate.CompareTo(EndDate) <= 0) Then
          If AreDatesEquivalent(PricePeriod, ThisDate, LastDate) = False Then
            ' If (ThisDate.Year <> LastDate.Year) Or (ThisDate.Month <> LastDate.Month) Then
            ' New Date 

            PriceIndex = GetPriceIndex(PricePeriod, StartDate, ThisDate) ' (ThisDate.Month - StartDate.Month) + ((ThisDate.Year - StartDate.Year) * 12)

            If (thisPerformanceRow.NAV <> 0) Then
              ReturnArray(PriceIndex).PriceValue = thisPerformanceRow.NAV
            End If
            If (thisPerformanceRow.PerformanceReturn <> 0) Or (thisPerformanceRow.NAV = 0) Then
              ReturnArray(PriceIndex).PriceReturn = thisPerformanceRow.PerformanceReturn
            End If

            LastDate = ThisDate

          End If
        End If

      Next

      ' Check for inaccurate Returns. (As a result of finding some in the data!)
      ' Give precedence to NAVs if they are present
      Try
        Dim PortfolioIndex As Integer

        For PortfolioIndex = 1 To (ReturnArray.Length - 1)
          ' Copy forward prices
          If (ReturnArray(PortfolioIndex).ValueIsSet = False) AndAlso _
           (ReturnArray(PortfolioIndex).ReturnIsSet = False) AndAlso _
           (ReturnArray(PortfolioIndex - 1).ValueIsSet = True) AndAlso _
           (ReturnArray(PortfolioIndex - 1).PriceValue <> 0) Then

            ReturnArray(PortfolioIndex).PriceValue = ReturnArray(PortfolioIndex - 1).PriceValue
          End If

          If (ReturnArray(PortfolioIndex - 1).ValueIsSet) AndAlso _
           (ReturnArray(PortfolioIndex).ValueIsSet) AndAlso _
           (ReturnArray(PortfolioIndex).ReturnIsSet) Then

            ' See if the given return matches the return implied by the NAVs
            ' If not, then apply the smallest change, if the return is non zero
            If Math.Abs(ReturnArray(PortfolioIndex).PriceReturn - ((ReturnArray(PortfolioIndex).PriceValue / ReturnArray(PortfolioIndex - 1).PriceValue) - 1.0)) > 0.00005 Then

              If (Math.Abs(ReturnArray(PortfolioIndex).PriceReturn) <= 0.00005) Then
                ' If the given return is zero (or very small) then use the NAVs

                If (ReturnArray(PortfolioIndex - 1).PriceValue <> 0) AndAlso _
                 (ReturnArray(PortfolioIndex).PriceValue <> 0) Then

                  If (Math.Abs(((ReturnArray(PortfolioIndex).PriceValue / ReturnArray(PortfolioIndex - 1).PriceValue) - 1) - ReturnArray(PortfolioIndex).ReturnValue) > 0.00005) Then
                    ReturnArray(PortfolioIndex).ReturnValue = (ReturnArray(PortfolioIndex).PriceValue / ReturnArray(PortfolioIndex - 1).PriceValue) - 1
                  End If
                End If
              Else
                ' Use the smallest return

                If Math.Abs(ReturnArray(PortfolioIndex).PriceReturn) > Math.Abs((ReturnArray(PortfolioIndex).PriceValue / ReturnArray(PortfolioIndex - 1).PriceValue) - 1.0) Then
                  ReturnArray(PortfolioIndex).PriceReturn = ((ReturnArray(PortfolioIndex).PriceValue / ReturnArray(PortfolioIndex - 1).PriceValue) - 1.0)
                Else
                  ReturnArray(PortfolioIndex).PriceValue = ReturnArray(PortfolioIndex - 1).PriceValue * (1.0 + ReturnArray(PortfolioIndex).PriceReturn)
                End If
              End If
            End If

          ElseIf (ReturnArray(PortfolioIndex - 1).ValueIsSet) AndAlso _
           (ReturnArray(PortfolioIndex).ValueIsSet) Then

            If (ReturnArray(PortfolioIndex - 1).PriceValue <> 0) AndAlso _
             (ReturnArray(PortfolioIndex).PriceValue <> 0) Then

              If (Math.Abs(((ReturnArray(PortfolioIndex).PriceValue / ReturnArray(PortfolioIndex - 1).PriceValue) - 1) - ReturnArray(PortfolioIndex).ReturnValue) > 0.00005) Then
                ReturnArray(PortfolioIndex).ReturnValue = (ReturnArray(PortfolioIndex).PriceValue / ReturnArray(PortfolioIndex - 1).PriceValue) - 1
              End If
            End If
          End If
        Next
      Catch Inner_Ex As Exception
      End Try

      ' Infill Fund Data

      ' At this point 'LastDate' should be the earliest date (or at least in the same month as) retrieved for the
      ' current Fund Instrument.
      ' If this date is later than the requested start date, then try to Infill with an alternative Fund Instrument.

      ' (ThisDate.Year <> LastDate.Year) Or (ThisDate.Month <> LastDate.Month)
      ' StartDate

      If AreDatesEquivalent(PricePeriod, LastDate, StartDate) = False Then ' (LastDate.Year <> StartDate.Year) OrElse (LastDate.Month > StartDate.Month) Then
        Dim FillInPertracID As String
        Try
          FillInPertracID = Get_ReportCustomisationAttribute(pMainForm.MainAdaptorHandler, pMainForm.MainDataHandler, VENICE_CustomReport_Benchmark_ApplicationName, pInitialFundID, "FundAttribute", "BackFillPertracID_" & PertracID.ToString)

          If (LastDate = #1/1/1900#) Or (LastDate.CompareTo(EndDate) > 0) Then
            ' Lastdate is outside of Start/End Date range. This means that there were no dates returned in the Start/End Range
            ' Thus just return the In-Fill array using original Start/End Dates.
            Return GetPertracPriceArray(pMainForm, PricePeriod, StartDate, EndDate, pInitialFundID, CInt(FillInPertracID), pKnowledgedate)
          End If

          If IsNumeric(FillInPertracID) AndAlso (CInt(FillInPertracID) > 0) Then
            Dim InFillReturnArray() As clsInstrumentPrice = Nothing
            InFillReturnArray = GetPertracPriceArray(pMainForm, PricePeriod, StartDate, LastDate, pInitialFundID, CInt(FillInPertracID), pKnowledgedate)

            If (Not (InFillReturnArray Is Nothing)) AndAlso (InFillReturnArray.Length > 1) Then
              ' Note Look for length > 1 as the last element should overlap and an array length of
              ' One is of no use.
              '
              ' Does the End of the In-Fill table match the start of the higher level table ?
              ' i.e. Is a Price Value set for the last array element ?

              If InFillReturnArray(InFillReturnArray.Length - 1).ValueIsSet = True Then
                ' OK, Stitch Fund Value Table Together.
                ' Note. The 'Get_ReportCustomisationAttribute()' function will return an array for 
                ' each month between start and end dates, with element Zero representing the month of the start
                ' date. This means that the in-fill elements match the higher-level array elements.
                ' e.g. Element 0 represents 'StartDate' in both arrays, thus there is no need to work out
                ' which element matches which element.

                Dim ContinuityMultiplier As Double = 1.0#
                Dim InFillIndex As Integer

                ' Adjust InFill NAV to be consistent with Higher-Level data

                InFillIndex = (InFillReturnArray.Length - 1)

                ' ReturnArray
                If (InFillReturnArray(InFillIndex).ValueIsSet) AndAlso (InFillReturnArray(InFillIndex).PriceValue <> 0) Then
                  ContinuityMultiplier = ReturnArray(InFillIndex).PriceValue / InFillReturnArray(InFillIndex).PriceValue
                End If

                For InFillIndex = 0 To (InFillReturnArray.Length - 2) ' Note : Don't use the last element, The arrays overlap by one element.

                  If InFillReturnArray(InFillIndex).ValueIsSet Then
                    ReturnArray(InFillIndex).PriceValue = InFillReturnArray(InFillIndex).PriceValue * ContinuityMultiplier
                  End If

                  If InFillReturnArray(InFillIndex).ReturnIsSet Then
                    ReturnArray(InFillIndex).PriceReturn = InFillReturnArray(InFillIndex).PriceReturn
                  End If
                Next

              Else
                pMainForm.LogError("GetPertracPriceArray()", RenaissanceGlobals.LOG_LEVELS.Warning, "", "Mis-Match in In-Fill End and Base-Fund Start Dates." & vbCrLf & "Fund " & PertracID.ToString & " vs Fund " & FillInPertracID.ToString, "", True)
              End If

            End If ' InFill data returned OK

          End If ' InFill FundID OK 


        Catch Inner_Ex As Exception
          pMainForm.LogError("GetPertracPriceArray()", RenaissanceGlobals.LOG_LEVELS.Error, Inner_Ex.Message, "Error Getting InFill Fund pricing.", Inner_Ex.StackTrace, True)
        End Try

      End If

    Catch ex As Exception
      pMainForm.LogError("GetPertracPriceArray()", RenaissanceGlobals.LOG_LEVELS.Error, ex.Message, "Error Getting Fund prices.", ex.StackTrace, True)
    End Try

Exit_GetPertracPriceArray:

    ' ForwardFill / Backfill / Calculate Returns.

    Try
      If (Not (ReturnArray Is Nothing)) AndAlso (ReturnArray.Length > 0) Then
        Dim PortfolioIndex As Integer
        Dim ChangesMade As Boolean = True
        Dim PriceExists As Boolean = False

        While ChangesMade = True
          ChangesMade = False

          ' In fill Returns as possible
          For PortfolioIndex = 1 To (ReturnArray.Length - 1)
            If (ReturnArray(PortfolioIndex - 1).ValueIsSet) AndAlso (ReturnArray(PortfolioIndex).ValueIsSet) AndAlso (ReturnArray(PortfolioIndex).ReturnIsSet = False) Then
              If (ReturnArray(PortfolioIndex - 1).PriceValue <> 0) Then
                ReturnArray(PortfolioIndex).PriceReturn = (ReturnArray(PortfolioIndex).PriceValue / ReturnArray(PortfolioIndex - 1).PriceValue) - 1
                ChangesMade = True
              End If
            End If
          Next

          For PortfolioIndex = 1 To (ReturnArray.Length - 1)
            If (ReturnArray(PortfolioIndex).ReturnIsSet) AndAlso (ReturnArray(PortfolioIndex - 1).ValueIsSet) AndAlso (ReturnArray(PortfolioIndex).ValueIsSet = False) Then
              ReturnArray(PortfolioIndex).PriceValue = (ReturnArray(PortfolioIndex - 1).PriceValue * (ReturnArray(PortfolioIndex).PriceReturn + 1.0#))
              ChangesMade = True
            End If
          Next

          For PortfolioIndex = (ReturnArray.Length - 1) To 1 Step -1
            If (ReturnArray(PortfolioIndex).ReturnIsSet) AndAlso (ReturnArray(PortfolioIndex - 1).ValueIsSet = False) AndAlso (ReturnArray(PortfolioIndex).ValueIsSet) Then
              ReturnArray(PortfolioIndex - 1).PriceValue = (ReturnArray(PortfolioIndex).PriceValue / (ReturnArray(PortfolioIndex).PriceReturn + 1.0#))
              ChangesMade = True
            End If
          Next

        End While

        ' OK, Final passes.
        For PortfolioIndex = 0 To (ReturnArray.Length - 1)
          If (ReturnArray(PortfolioIndex).ValueIsSet) Then
            PriceExists = True
          End If
        Next

        If (PriceExists = False) Then
          ReturnArray(0).PriceValue = 100
        End If

        For PortfolioIndex = 1 To (ReturnArray.Length - 1)
          If (ReturnArray(PortfolioIndex).ReturnIsSet = False) Then
            ReturnArray(PortfolioIndex).PriceReturn = ReturnArray(PortfolioIndex).PriceReturn
          End If

          ReturnArray(PortfolioIndex).PriceValue = (ReturnArray(PortfolioIndex - 1).PriceValue * (ReturnArray(PortfolioIndex).PriceReturn + 1.0#))
        Next

      End If ' Not (ReturnArray Is Nothing)
    Catch ex As Exception
    End Try

    Return ReturnArray
  End Function

  ''' <summary>
  ''' Gets the pertrac prices array from venice instrument ID.
  ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  ''' <param name="PricePeriod">The price period.</param>
  ''' <param name="pStartDate">The p start date.</param>
  ''' <param name="pEndDate">The p end date.</param>
  ''' <param name="pInitialFundID">The p initial fund ID.</param>
  ''' <param name="pInstrumentID">The p instrument ID.</param>
  ''' <param name="pKnowledgedate">The p knowledgedate.</param>
  ''' <returns>clsInstrumentPrice[][].</returns>
	Public Function GetPertracPricesArrayFromVeniceInstrumentID(ByVal pMainForm As Venice.VeniceMain, _
			ByVal PricePeriod As RenaissanceGlobals.DealingPeriod, _
			ByVal pStartDate As Date, _
			ByVal pEndDate As Date, _
			ByVal pInitialFundID As Integer, _
			ByVal pInstrumentID As Integer, _
			ByVal pKnowledgedate As Date) As clsInstrumentPrice()

		' **********************************************************************************
		'
		'
		' **********************************************************************************
		Dim PertracID As Integer

		' Resolve Pertrac ID of the given Venice Instrument

		Try
			Dim DSInstrument As RenaissanceDataClass.DSInstrument
			Dim tblInstrument As RenaissanceDataClass.DSInstrument.tblInstrumentDataTable
			Dim SelectedInstrumentRows As RenaissanceDataClass.DSInstrument.tblInstrumentRow()

			DSInstrument = pMainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblInstrument, False)
			If (DSInstrument Is Nothing) Then
				Return Nothing
			End If

			tblInstrument = DSInstrument.tblInstrument
			SelectedInstrumentRows = tblInstrument.Select("InstrumentID=" & pInstrumentID.ToString)
			If (SelectedInstrumentRows Is Nothing) OrElse (SelectedInstrumentRows.Length <= 0) Then
				Return Nothing
			End If

			If (SelectedInstrumentRows(0).IsInstrumentPertracCodeNull) OrElse (SelectedInstrumentRows(0).InstrumentPertracCode <= 0) Then
				Return Nothing
			End If
			PertracID = SelectedInstrumentRows(0).InstrumentPertracCode

			' Return associated Price series.

			Return GetPertracPriceArray(pMainForm, PricePeriod, pStartDate, pEndDate, pInitialFundID, PertracID, pKnowledgedate)

		Catch ex As Exception
			Return Nothing
		End Try

		Return Nothing

	End Function

    ''' <summary>
    ''' Adds the price array returns.
    ''' </summary>
    ''' <param name="BasePriceArray">The base price array.</param>
    ''' <param name="SecondPriceArray">The second price array.</param>
    ''' <param name="PricePeriod">The price period.</param>
    ''' <returns>clsInstrumentPrice[][].</returns>
	Public Function AddPriceArrayReturns(ByRef BasePriceArray As clsInstrumentPrice(), ByRef SecondPriceArray As clsInstrumentPrice(), ByVal PricePeriod As RenaissanceGlobals.DealingPeriod) As clsInstrumentPrice()
		' ****************************************************************************************
		' Function to apply an additional array of price returns to a Base array of price returns.
		'
		' This function was written with the idea of applying Currency Hedge return series to base
		' price return series.
		' The Code should cope with scenarios of Null or Empty price arrays.
		' Operationally, the Series should have the same start and End dates, however the code should cope 
		' with this not being the case.
		' 
		' 
		' ****************************************************************************************

		Dim PriceCounter As Integer
		Dim BaseIndex As Integer = 0
		Dim SecondIndex As Integer = 0
		Dim BaseDate As Date
		Dim SecondDate As Date

		' Basic Array validation
		If (BasePriceArray Is Nothing) OrElse (BasePriceArray.Length <= 0) Then
			GoTo Exit_AddPriceArrayReturns
		End If
		If (SecondPriceArray Is Nothing) OrElse (SecondPriceArray.Length <= 0) Then
			GoTo Exit_AddPriceArrayReturns
		End If

		' Cycle through dates adding second return series to the first price series.
		Try
			For PriceCounter = 0 To (BasePriceArray.Length - 1)
				If (BaseIndex >= BasePriceArray.Length) Then
					GoTo Exit_AddPriceArrayReturns
				End If
				If (SecondIndex >= SecondPriceArray.Length) Then
					GoTo Exit_AddPriceArrayReturns
				End If

				BaseDate = BasePriceArray(BaseIndex).PriceDate
				SecondDate = SecondPriceArray(SecondIndex).PriceDate

				If AreDatesEquivalent(PricePeriod, BaseDate, SecondDate) Then	' (BaseDate.Year = SecondDate.Year) And (BaseDate.Month = SecondDate.Month) Then
					BasePriceArray(BaseIndex).PriceReturn += SecondPriceArray(SecondIndex).PriceReturn

					If (BaseIndex > 0) Then
						BasePriceArray(BaseIndex).PriceValue = BasePriceArray(BaseIndex - 1).PriceValue * (1.0 + BasePriceArray(BaseIndex).PriceReturn)
					End If

					BaseIndex += 1
					SecondIndex += 1
				ElseIf (BaseDate.CompareTo(SecondDate) < 0) Then
					BaseIndex += 1
				Else
					SecondIndex += 1
				End If

			Next
		Catch ex As Exception
		End Try

Exit_AddPriceArrayReturns:

		Return BasePriceArray
	End Function


#Region " Bug hunting "

	' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
	' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
	' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

    ''' <summary>
    ''' Called when [row updating transaction].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
	Private Sub OnRowUpdatingTransaction(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
		Dim a As Integer

		If Not e.Command Is Nothing Then
			a = 1
		End If

		If Not e.Errors Is Nothing Then
			a = 2
		End If
	End Sub

    ''' <summary>
    ''' Called when [row updated transaction].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
	Private Sub OnRowUpdatedTransaction(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
		Dim a As Integer

		If Not e.Command Is Nothing Then
			a = 1
		End If

		If Not e.Errors Is Nothing Then
			a = 2
		End If
	End Sub

    ''' <summary>
    ''' Called when [row fill error transaction].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
	Private Sub OnRowFillErrorTransaction(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
		Dim a As Integer

		a = 1
	End Sub

#End Region


End Module




