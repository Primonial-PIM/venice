﻿' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 11-15-2012
' ***********************************************************************
' <copyright file="CustomC1Combo.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System
Imports System.Collections
Imports System.Windows.Forms
Imports RenaissanceGlobals


''' <summary>
''' Class CustomC1Combo
''' </summary>
Public Class CustomC1Combo
	Inherits ComboBox
	Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor

    ''' <summary>
    ''' Refreshes the item contained at the specified location.
    ''' </summary>
    ''' <param name="index">The location of the item to refresh.</param>
	Protected Overrides Sub RefreshItem(ByVal index As Integer)
		MyBase.RefreshItem(index)
	End Sub

    ''' <summary>
    ''' When overridden in a derived class, sets the specified array of objects in a collection in the derived class.
    ''' </summary>
    ''' <param name="items">An array of items.</param>
	Protected Overrides Sub SetItemsCore(ByVal items As System.Collections.IList)
		MyBase.SetItemsCore(items)
	End Sub

    ''' <summary>
    ''' C1s the editor format.
    ''' </summary>
    ''' <param name="value">The value.</param>
    ''' <param name="mask">The mask.</param>
    ''' <returns>System.String.</returns>
	Public Function C1EditorFormat(ByVal value As Object, ByVal mask As String) As String Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorFormat
		Return value.ToString
	End Function

    ''' <summary>
    ''' C1s the editor get style.
    ''' </summary>
    ''' <returns>System.Drawing.Design.UITypeEditorEditStyle.</returns>
	Public Function C1EditorGetStyle() As System.Drawing.Design.UITypeEditorEditStyle Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorGetStyle
		Return System.Drawing.Design.UITypeEditorEditStyle.DropDown
	End Function

    ''' <summary>
    ''' C1s the editor get value.
    ''' </summary>
    ''' <returns>System.Object.</returns>
	Public Function C1EditorGetValue() As Object Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorGetValue
		Return Me.SelectedValue
	End Function

    ''' <summary>
    ''' C1s the editor initialize.
    ''' </summary>
    ''' <param name="value">The value.</param>
    ''' <param name="editorAttributes">The editor attributes.</param>
	Public Sub C1EditorInitialize(ByVal value As Object, ByVal editorAttributes As System.Collections.IDictionary) Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorInitialize
		Try
			If (value IsNot Nothing) Then
				Me.SelectedValue = value
			End If
		Catch ex As Exception
		End Try
	End Sub

    ''' <summary>
    ''' C1s the editor key down finish edit.
    ''' </summary>
    ''' <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	Public Function C1EditorKeyDownFinishEdit(ByVal e As System.Windows.Forms.KeyEventArgs) As Boolean Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorKeyDownFinishEdit

		Select Case e.KeyCode
			Case Keys.Enter, Keys.Tab
				Return True
		End Select

		Return False
	End Function

    ''' <summary>
    ''' C1s the editor update bounds.
    ''' </summary>
    ''' <param name="rc">The rc.</param>
	Public Sub C1EditorUpdateBounds(ByVal rc As System.Drawing.Rectangle) Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorUpdateBounds
		MyBase.FontHeight = rc.Height

		Bounds = rc
	End Sub

    ''' <summary>
    ''' C1s the editor value is valid.
    ''' </summary>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	Public Function C1EditorValueIsValid() As Boolean Implements C1.Win.C1FlexGrid.IC1EmbeddedEditor.C1EditorValueIsValid
		Return True
	End Function
End Class

