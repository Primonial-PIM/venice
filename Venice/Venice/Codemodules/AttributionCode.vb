' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="AttributionCode.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports System.Threading
Imports RenaissanceUtilities.DatePeriodFunctions
Imports RenaissanceGlobals

''' <summary>
''' Class AttributionCode
''' </summary>
Module AttributionCode

  ' PAM Asset Allocation stuff.

    ''' <summary>
    ''' Allocations the set returns.
    ''' </summary>
    ''' <param name="MainForm">The main form.</param>
    ''' <param name="pFundID">The p fund ID.</param>
    ''' <param name="pStartValueDate">The p start value date.</param>
    ''' <param name="pEndValueDate">The p end value date.</param>
    ''' <param name="pStatusBar">The p status bar.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Friend Function AllocationSetReturns(ByRef MainForm As VeniceMain, ByRef pFundID As Integer, ByVal pStartValueDate As Date, ByVal pEndValueDate As Date, ByVal SpecificInstrumentID As Integer, ByRef pStatusBar As ToolStripProgressBar) As Boolean
    ' **********************************************************************************************************************************
    ' Function to add attributions information to the Historic Valuations data.
    '
    '
    ' **********************************************************************************************************************************

    Dim DatesTable As New DataTable

    Try

      ' OK, Get Distinct Valuation Dates

      Dim StartIndex As Integer = 0
      Dim EndIndex As Integer = -1
      Dim DateCounter As Integer
      Dim BenchmarkDateSeries() As Date
      Dim BenchmarkNAVSeries() As Double

      Dim GetDatesCommand As New SqlCommand

      GetDatesCommand.CommandType = CommandType.Text
      GetDatesCommand.CommandText = "SELECT DISTINCT ValuationDate FROM tblHistoricFundValuations WHERE FundID = " & pFundID.ToString() & " ORDER BY ValuationDate"
      GetDatesCommand.CommandTimeout = ATTRIBUTION_SQLCOMMAND_TIMEOUT

      Try
        GetDatesCommand.Connection = MainForm.GetVeniceConnection

        MainForm.LoadTable_Custom(DatesTable, GetDatesCommand)
        'DatesTable.Load(GetDatesCommand.ExecuteReader)

      Catch ex As Exception
        MainForm.LogError("AllocationSetReturns()", RenaissanceGlobals.LOG_LEVELS.Error, "Error getting Valuation Dates.", ex.Message, ex.StackTrace, True)
        Return False
      Finally
        If (Not (GetDatesCommand.Connection Is Nothing)) Then
          Try
            GetDatesCommand.Connection.Close()
          Catch ex As Exception
          End Try
        End If
      End Try

      ' Anything to do (need at least two dates) ?

      If (DatesTable.Rows.Count <= 1) Then
        Return True
      End If

      ' Now Get the Returns series for the Fund

      ' TODO, Don't Think I need this yet......

      ' Narrow Down Start and End Dates

      For DateCounter = 0 To (DatesTable.Rows.Count - 1)
        If (CDate(DatesTable.Rows(DateCounter)(0)) < pStartValueDate) Then
          StartIndex = DateCounter
        End If

        If (CDate(DatesTable.Rows(DateCounter)(0)) <= pEndValueDate) Then
          EndIndex = DateCounter
        End If
      Next

      ' OK, Step Through the dates....

      Dim StartDate As Date
      Dim EndDate As Date
      Dim DataSeriesStartDate As Date
      Dim DataSeriesEndDate As Date

      Dim StartDateValuations As DataTable = Nothing
      Dim EndDateValuations As DataTable = Nothing
      Dim GetValuationsCommand As New SqlCommand
      Dim SaveAttributionCommand As New SqlCommand

      Dim FundStartValue As Double
      Dim FundEndValue As Double

      Dim StartValuationIndex As Integer
      Dim StartValuationCounter As Integer
      Dim EndValuationIndex As Integer

      Dim ThisStartingWeight As Double
      Dim ThisInstrumentReturn As Double
      Dim ThisBenchmarkReturn As Double
      Dim ThisInstrumentModifiedReturn As Double
      Dim ThisBenchmarkModifiedReturn As Double
      Dim ThisBenchmarkEndValue As Double
      Dim ThisBenchmarkModifiedProfit As Double

      Try

        '

        DataSeriesStartDate = CDate(DatesTable.Rows(StartIndex)(0))
        DataSeriesEndDate = CDate(DatesTable.Rows(EndIndex)(0))

        ' Initialise Valuations SQL Command

        GetValuationsCommand.Connection = MainForm.GetVeniceConnection
        GetValuationsCommand.CommandTimeout = ATTRIBUTION_SQLCOMMAND_TIMEOUT

        ' Initialise 'SaveAttribution' SQL Command

        SaveAttributionCommand.Connection = MainForm.GetVeniceConnection
        SaveAttributionCommand.CommandType = CommandType.StoredProcedure
        SaveAttributionCommand.CommandText = "[adp_tblHistoricFundValuations_UpdateAttributions]"
        SaveAttributionCommand.CommandTimeout = ATTRIBUTION_SQLCOMMAND_TIMEOUT

        SaveAttributionCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SaveAttributionCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RN", System.Data.SqlDbType.Int))
        SaveAttributionCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@InstrumentReturn", System.Data.SqlDbType.Float))
        SaveAttributionCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@InstrumentModifedReturn", System.Data.SqlDbType.Float))
        SaveAttributionCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BenchmarkReturn", System.Data.SqlDbType.Float))
        SaveAttributionCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BenchmarkModifiedReturn", System.Data.SqlDbType.Float))
        SaveAttributionCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@InstrumentBasisPoints", System.Data.SqlDbType.Float))
        SaveAttributionCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@InstrumentModifiedBasisPoints", System.Data.SqlDbType.Float))
        SaveAttributionCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BenchmarkBasisPoints", System.Data.SqlDbType.Float))
        SaveAttributionCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@BenchmarkModifiedBasisPoints", System.Data.SqlDbType.Float))
        SaveAttributionCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PreviousFundWeightPercent", System.Data.SqlDbType.Float))
        SaveAttributionCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime)).Value = RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW

        ' Here we go...

        For DateCounter = StartIndex To EndIndex

          EndDate = CDate(DatesTable.Rows(DateCounter)(0))

          ' Get ValuationData

          GetValuationsCommand.CommandType = CommandType.Text
          GetValuationsCommand.CommandText = "SELECT * FROM tblHistoricFundValuations WHERE (FundID = " & pFundID.ToString() & ") AND (ValuationDate = '" & EndDate.ToString("yyyy-MM-dd") & "') ORDER BY InstrumentID"

          Try
            EndDateValuations = New DataTable
            MainForm.LoadTable_Custom(EndDateValuations, GetValuationsCommand)
            'EndDateValuations.Load(GetValuationsCommand.ExecuteReader)

          Catch ex As Exception
            MainForm.LogError("AllocationSetReturns()", RenaissanceGlobals.LOG_LEVELS.Error, "Error getting Valuations for Date " & EndDate.ToString("yyyy-mm-dd"), ex.Message, ex.StackTrace, True)
            Return False
          End Try

          ' Get Fund Value 

          FundEndValue = 0.0#
          For EndValuationIndex = 0 To (EndDateValuations.Rows.Count - 1)
            FundEndValue += CDbl(EndDateValuations.Rows(EndValuationIndex)("Value")) ' Sum Value in Fund Currency.
          Next

          ' Don't calculate for the first date, not possible!

          If (DateCounter > StartIndex) Then

            Dim ThisInstrumentID As Integer
            Dim ThisBenchmarkID As Integer
            Dim InstrumentRow As RenaissanceDataClass.DSInstrument.tblInstrumentRow

            Dim Start_SignedUnits As Double
            Dim Start_SignedSettlement As Double
            Dim Start_Multiplier As Double
            Dim Start_PriceLevel As Double
            Dim Start_CompoundFX As Double
            Dim Start_LocalValue As Double

            Dim End_SignedUnits As Double
            Dim End_SignedSettlement As Double
            Dim End_Multiplier As Double
            Dim End_PriceLevel As Double
            Dim End_CompoundFX As Double
            Dim End_LocalValue As Double

            Dim LocalCurrencyProfit As Double
            Dim LocalCurrencyProfitInFundCurrency As Double
            Dim FundCurrencyProfit As Double
            Dim StartFundWeight As Double
            Dim StartLocalValue As Double
            Dim StartValueFundCcy As Double

            ' For Each Instrument line...

            For EndValuationIndex = 0 To (EndDateValuations.Rows.Count - 1)

              ' Get Instrument Details.

              ThisInstrumentID = CInt(EndDateValuations.Rows(EndValuationIndex)("InstrumentID"))

              ' Only recalculate specified instrument ?
              If (SpecificInstrumentID > 0) AndAlso (ThisInstrumentID = SpecificInstrumentID) Then
                Continue For
              End If

              InstrumentRow = CType(LookupTableRow(MainForm, RenaissanceGlobals.RenaissanceStandardDatasets.tblInstrument, ThisInstrumentID), RenaissanceDataClass.DSInstrument.tblInstrumentRow)
              ThisBenchmarkID = InstrumentRow.InstrumentBenchmark

              ThisStartingWeight = 0.0#
              ThisInstrumentReturn = 0.0#
              ThisBenchmarkReturn = 0.0#
              ThisInstrumentModifiedReturn = 0.0#
              ThisBenchmarkModifiedReturn = 0.0#

              ' Get Benchmark Series and return

              If (ThisBenchmarkID <> 0) Then
                BenchmarkDateSeries = MainForm.StatFunctions.DateSeries(RenaissanceGlobals.DealingPeriod.Daily, CULng(ThisBenchmarkID), False, 1, 1.0#, False, DataSeriesStartDate, DataSeriesEndDate)
                BenchmarkNAVSeries = MainForm.StatFunctions.NAVSeries(RenaissanceGlobals.DealingPeriod.Daily, CULng(ThisBenchmarkID), False, 1, 1.0#, False, DataSeriesStartDate, DataSeriesEndDate, 1.0#)

                If (BenchmarkDateSeries IsNot Nothing) AndAlso (EndDate <= BenchmarkDateSeries(BenchmarkDateSeries.Length - 1)) AndAlso (StartDate >= BenchmarkDateSeries(0)) Then

                  ThisBenchmarkReturn = (BenchmarkNAVSeries(GetPriceIndex(DealingPeriod.Daily, BenchmarkDateSeries(0), EndDate)) / BenchmarkNAVSeries(GetPriceIndex(DealingPeriod.Daily, BenchmarkDateSeries(0), StartDate))) - 1.0#
                End If
              End If

              ' Get Index for opening position

              StartValuationIndex = -1
              For StartValuationCounter = 0 To (StartDateValuations.Rows.Count - 1)
                If (ThisInstrumentID = CInt(StartDateValuations.Rows(StartValuationCounter)("InstrumentID"))) Then
                  StartValuationIndex = StartValuationCounter
                  Exit For
                End If
              Next

              ' Calculate Attributions. (If it Has starting or ending value or if the Settlement amount has changed).

              End_SignedUnits = CDbl(EndDateValuations.Rows(EndValuationIndex)("SignedUnits"))
              End_SignedSettlement = CDbl(EndDateValuations.Rows(EndValuationIndex)("SignedSettlement"))
              End_Multiplier = CDbl(EndDateValuations.Rows(EndValuationIndex)("InstrumentContractSize")) * CDbl(EndDateValuations.Rows(EndValuationIndex)("InstrumentMultiplier"))
              End_PriceLevel = CDbl(EndDateValuations.Rows(EndValuationIndex)("PriceLevel"))
              End_CompoundFX = CDbl(EndDateValuations.Rows(EndValuationIndex)("CompoundFXRate"))
              End_LocalValue = CDbl(EndDateValuations.Rows(EndValuationIndex)("LocalValue"))

              If (StartValuationIndex >= 0) Then
                ' Existing Position.

                Start_SignedUnits = CDbl(StartDateValuations.Rows(StartValuationIndex)("SignedUnits"))
                Start_SignedSettlement = CDbl(StartDateValuations.Rows(StartValuationIndex)("SignedSettlement"))
                Start_Multiplier = CDbl(StartDateValuations.Rows(StartValuationIndex)("InstrumentContractSize")) * CDbl(StartDateValuations.Rows(StartValuationIndex)("InstrumentMultiplier"))
                Start_PriceLevel = CDbl(StartDateValuations.Rows(StartValuationIndex)("PriceLevel"))
                Start_CompoundFX = CDbl(StartDateValuations.Rows(StartValuationIndex)("CompoundFXRate"))
                Start_LocalValue = CDbl(StartDateValuations.Rows(StartValuationIndex)("LocalValue"))
              Else
                ' Opening Position.

                Start_SignedUnits = 0.0#
                Start_SignedSettlement = 0.0#
                Start_Multiplier = CDbl(EndDateValuations.Rows(EndValuationIndex)("InstrumentContractSize")) * CDbl(EndDateValuations.Rows(EndValuationIndex)("InstrumentMultiplier"))
                Start_PriceLevel = CDbl(EndDateValuations.Rows(EndValuationIndex)("PriceLevel"))
                Start_CompoundFX = CDbl(EndDateValuations.Rows(EndValuationIndex)("CompoundFXRate"))
                Start_LocalValue = 0.0#
              End If

              ' If it has a value, or it is a new entry but cost something or if the settlement values have changed ......

              If (Math.Abs(End_LocalValue) > 1.0#) OrElse ((StartValuationIndex < 0) AndAlso (Math.Abs(End_SignedSettlement) > 1.0#)) OrElse ((StartValuationIndex >= 0) AndAlso ((Math.Abs(Start_LocalValue) > 1.0#) OrElse (Math.Abs(Start_SignedSettlement - End_SignedSettlement) > 0.01#))) Then ' Expenses do not have a value, check settlement value!

                ' Work out profit, return and bps

                ' ROUND((ISNULL(((VTo.SignedUnits * VTo.PriceLevel * VTo.InstrumentContractSize) - VTo.SignedSettlement), 0) - ISNULL(((VFrom.SignedUnits * VFrom.PriceLevel * VFrom.InstrumentContractSize) - VFrom.SignedSettlement) ,0)) * ISNULL(VTo.CompoundFXRate, VFrom.CompoundFXRate), 6) AS Profit,

                LocalCurrencyProfit = ((End_SignedUnits * End_PriceLevel * End_Multiplier) - End_SignedSettlement) - ((Start_SignedUnits * Start_PriceLevel * Start_Multiplier) - Start_SignedSettlement) ' Correct Profit
                LocalCurrencyProfitInFundCurrency = LocalCurrencyProfit * End_CompoundFX ' Correct Profit, excluding FX effects

                ' FundCurrencyProfit = (ValueNow - ValueThen) + (SettlementNow - SettlementThen) ' Position profit, trying to include FX effects
                FundCurrencyProfit = ((End_LocalValue * End_CompoundFX) - (Start_LocalValue * Start_CompoundFX)) - ((End_SignedSettlement - Start_SignedSettlement) * End_CompoundFX)

                If (ThisBenchmarkID <> 0) Then
                  ' Instrument has a benchmark

                  ThisBenchmarkEndValue = End_SignedUnits * (Start_PriceLevel * (1.0# + ThisBenchmarkReturn)) * Start_Multiplier
                  ThisBenchmarkModifiedProfit = ((ThisBenchmarkEndValue * End_CompoundFX) - (Start_LocalValue * Start_CompoundFX)) - ((End_SignedSettlement - Start_SignedSettlement) * End_CompoundFX)

                  '  Investment Return = R, FX Return = F : Compound return = (F + R + FR)
                  ThisBenchmarkModifiedReturn = (ThisBenchmarkReturn) + ((End_CompoundFX / Start_CompoundFX) - 1.0#) + (ThisBenchmarkReturn * ((End_CompoundFX / Start_CompoundFX) - 1.0#))

                End If

                Dim debug As Integer = 0

              Else

                ' No End Date Info or no Value
                ' Returns are Zero.
                ' 
                LocalCurrencyProfit = 0.0#
                LocalCurrencyProfitInFundCurrency = 0.0#
                FundCurrencyProfit = 0.0#
                ThisBenchmarkModifiedProfit = 0.0#
                ThisBenchmarkModifiedReturn = ThisBenchmarkReturn ' FX not important since the instrument weighting is zero anyway.

              End If

              ' Set Returns

              'CREATE PROCEDURE [dbo].[adp_tblHistoricFundValuations_UpdateAttributions]
              ' (
              ' @RN int,
              ' @InstrumentReturn float = 0,
              ' @InstrumentModifedReturn float = 0,
              ' @BenchmarkReturn float = 0,
              ' @BenchmarkModifiedReturn float = 0,
              ' @PeriodStartWeight float = 0,
              ' @InstrumentBasisPoints float = 0,
              ' @InstrumentModifiedBasisPoints float = 0,
              ' @BenchmarkBasisPoints float = 0,
              ' @BenchmarkModifiedBasisPoints float = 0,
              ' @PreviousFundWeightPercent float = 0,
              ' @KnowledgeDate datetime = null
              ' )

              Try

                SaveAttributionCommand.Parameters("@RN").Value = EndDateValuations.Rows(EndValuationIndex)("RN")

                If (StartValuationIndex >= 0) Then
                  StartFundWeight = CDbl(StartDateValuations.Rows(StartValuationIndex)("FundWeightPercent"))
                  StartLocalValue = CDbl(StartDateValuations.Rows(StartValuationIndex)("LocalValue"))
                  StartValueFundCcy = CDbl(StartDateValuations.Rows(StartValuationIndex)("Value"))
                Else
                  StartFundWeight = 0.0#
                  StartLocalValue = 0.0#
                  StartValueFundCcy = 0.0#
                End If

                If (Math.Abs(StartLocalValue) > 1.0#) Then
                  SaveAttributionCommand.Parameters("@InstrumentReturn").Value = LocalCurrencyProfit / StartLocalValue
                  SaveAttributionCommand.Parameters("@InstrumentModifedReturn").Value = FundCurrencyProfit / StartValueFundCcy
                Else
                  SaveAttributionCommand.Parameters("@InstrumentReturn").Value = 0.0#
                  SaveAttributionCommand.Parameters("@InstrumentModifedReturn").Value = 0.0#
                End If

                If (LocalCurrencyProfit <> 0.0#) AndAlso (StartFundWeight = 0.0#) AndAlso (Math.Abs(FundStartValue) > 1.0#) Then
                  ' Has a P&L, but no starting fund weight, either a new position or an expense. Anyway, can't use StartFundWeight to calculate bps attribution
                  SaveAttributionCommand.Parameters("@InstrumentBasisPoints").Value = LocalCurrencyProfitInFundCurrency / FundStartValue
                  SaveAttributionCommand.Parameters("@InstrumentModifiedBasisPoints").Value = FundCurrencyProfit / FundStartValue

                Else
                  SaveAttributionCommand.Parameters("@InstrumentBasisPoints").Value = CDbl(SaveAttributionCommand.Parameters("@InstrumentReturn").Value) * StartFundWeight
                  SaveAttributionCommand.Parameters("@InstrumentModifiedBasisPoints").Value = CDbl(SaveAttributionCommand.Parameters("@InstrumentModifedReturn").Value) * StartFundWeight
                End If

                If (ThisBenchmarkID <> 0) Then
                  ' Instrument has a benchmark

                  SaveAttributionCommand.Parameters("@BenchmarkReturn").Value = ThisBenchmarkReturn
                  SaveAttributionCommand.Parameters("@BenchmarkModifiedReturn").Value = ThisBenchmarkModifiedReturn
                  SaveAttributionCommand.Parameters("@BenchmarkBasisPoints").Value = ThisBenchmarkReturn * StartFundWeight
                  SaveAttributionCommand.Parameters("@BenchmarkModifiedBasisPoints").Value = ThisBenchmarkModifiedReturn * StartFundWeight

                Else
                  ' Instrument does not have a benchmark
                  ' If it's not fees, use the Instrument return values
                  Dim IsFees As Boolean = False

                  If (InstrumentRow IsNot Nothing) AndAlso ((InstrumentRow.InstrumentType = InstrumentTypes.Fee) OrElse (InstrumentRow.InstrumentType = InstrumentTypes.Expense)) Then
                    IsFees = True
                  End If

                  ' If it has no starting or ending value and no benchmark then it is either a closed position, in which case the Instrument returns etc are Zero anyway or it is a fee type in 
                  ' which case the benchmark returns etc should be zero.

                  If (StartValuationIndex > 0) Then
                    If (Math.Abs(CDbl(StartDateValuations.Rows(StartValuationIndex)("LocalValue"))) = 0.0#) AndAlso (Math.Abs(CDbl(EndDateValuations.Rows(EndValuationIndex)("LocalValue"))) = 0.0#) Then
                      IsFees = True
                    End If
                  Else
                    If (Math.Abs(CDbl(EndDateValuations.Rows(EndValuationIndex)("LocalValue"))) = 0.0#) Then
                      IsFees = True
                    End If
                  End If

                  If (IsFees) Then

                    SaveAttributionCommand.Parameters("@BenchmarkReturn").Value = 0.0#
                    SaveAttributionCommand.Parameters("@BenchmarkModifiedReturn").Value = 0.0#
                    SaveAttributionCommand.Parameters("@BenchmarkBasisPoints").Value = 0.0#
                    SaveAttributionCommand.Parameters("@BenchmarkModifiedBasisPoints").Value = 0.0#

                  Else

                    SaveAttributionCommand.Parameters("@BenchmarkReturn").Value = SaveAttributionCommand.Parameters("@InstrumentReturn").Value
                    SaveAttributionCommand.Parameters("@BenchmarkModifiedReturn").Value = SaveAttributionCommand.Parameters("@InstrumentModifedReturn").Value
                    SaveAttributionCommand.Parameters("@BenchmarkBasisPoints").Value = SaveAttributionCommand.Parameters("@InstrumentBasisPoints").Value
                    SaveAttributionCommand.Parameters("@BenchmarkModifiedBasisPoints").Value = SaveAttributionCommand.Parameters("@InstrumentModifiedBasisPoints").Value

                  End If


                End If


                SaveAttributionCommand.Parameters("@PreviousFundWeightPercent").Value = StartFundWeight

                If (Not (SaveAttributionCommand.Connection Is Nothing)) Then
                  SaveAttributionCommand.ExecuteNonQuery()
                End If
              Catch ex As Exception
                MainForm.LogError("AllocationSetReturns()", RenaissanceGlobals.LOG_LEVELS.Error, "Error saving allocation returns.", ex.Message, ex.StackTrace, True)
              End Try

            Next

          End If

          StartDate = EndDate

          If (StartDateValuations IsNot Nothing) Then
            StartDateValuations.Clear()
          End If

          StartDateValuations = EndDateValuations
          FundStartValue = FundEndValue

        Next

      Catch ex As Exception
        MainForm.LogError("AllocationSetReturns()", RenaissanceGlobals.LOG_LEVELS.Error, "Error in AllocationSetReturns().", ex.Message, ex.StackTrace, True)

      Finally
        If (Not (GetValuationsCommand.Connection Is Nothing)) Then
          Try
            GetValuationsCommand.Connection.Close()
          Catch ex As Exception
          End Try
        End If

        If (Not (SaveAttributionCommand.Connection Is Nothing)) Then
          Try
            SaveAttributionCommand.Connection.Close()
          Catch ex As Exception
          End Try
        End If

      End Try

    Catch ex As Exception
      MainForm.LogError("AllocationSetReturns()", RenaissanceGlobals.LOG_LEVELS.Error, "Error saving allocation returns.", ex.Message, ex.StackTrace, True)
    End Try

  End Function





  ' Attribution Code

    ''' <summary>
    ''' Attribution_s the save fund prices_ asynch.
    ''' </summary>
    ''' <param name="MainForm">The main form.</param>
    ''' <param name="pFundID">The p fund ID.</param>
    ''' <param name="pValueDate">The p value date.</param>
    ''' <param name="pStatusGroupFilter">The p status group filter.</param>
    ''' <param name="pAdministratorDatesFilter">The p administrator dates filter.</param>
    ''' <param name="pStatusBar">The p status bar.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Friend Function Attribution_SaveFundPrices_Asynch(ByRef MainForm As VeniceMain, ByRef pFundID As Integer, ByVal pValueDate As Date, ByVal pStatusGroupFilter As String, ByVal pAdministratorDatesFilter As Integer, ByRef pStatusBar As ToolStripProgressBar) As Boolean

    If (RenaissanceGlobals.RenaissanceStandardDatasets.tblPrice.ActivityCount > 0) Then
      MainForm.LogError("SaveAtributions", RenaissanceGlobals.LOG_LEVELS.Warning, "", "There appears to be an Attribution process already running, Attribution cancelled.", "", True)
      Exit Function
    End If

    Try
      RenaissanceGlobals.RenaissanceStandardDatasets.tblPrice.ActivityCount_Increment()

      Dim caller As New Attribution_SaveFundPrices_Delegate(AddressOf Attribution_SaveFundPrices)

      Dim result As IAsyncResult = caller.BeginInvoke(MainForm, pFundID, pValueDate, pStatusGroupFilter, pAdministratorDatesFilter, Nothing, Nothing)
      While result.IsCompleted = False
        Thread.Sleep(25)
        MainForm.IncrementStatusBar(pStatusBar)
        If Not (MainForm.InvokeRequired) Then
          Application.DoEvents()
        End If
      End While

      pStatusBar.Visible = False

      Return caller.EndInvoke(MainForm, result)

    Catch ex As Exception

      Return False

    Finally
      Try
        RenaissanceGlobals.RenaissanceStandardDatasets.tblPrice.ActivityCount_Decrement()
      Catch ex As Exception
      End Try

      Try
        ' If we have been Updating a ProgressBar from a Worker thread, re-build the Progress Bar.
        ' There seems to be a problem making them InVisible after Marshalled cross thread updates.

        If (pStatusBar IsNot Nothing) AndAlso (pStatusBar.Owner.InvokeRequired) Then
          MainForm.ResetProgressBar(pStatusBar.Owner, pStatusBar)
        End If
      Catch ex As Exception
      End Try

    End Try


  End Function

    ''' <summary>
    ''' Delegate Attribution_SaveFundPrices_Delegate
    ''' </summary>
    ''' <param name="MainForm">The main form.</param>
    ''' <param name="pFundID">The p fund ID.</param>
    ''' <param name="pValueDate">The p value date.</param>
    ''' <param name="pStatusGroupFilter">The p status group filter.</param>
    ''' <param name="pAdministratorDatesFilter">The p administrator dates filter.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Delegate Function Attribution_SaveFundPrices_Delegate(ByRef MainForm As VeniceMain, ByVal pFundID As Integer, ByVal pValueDate As Date, ByVal pStatusGroupFilter As String, ByVal pAdministratorDatesFilter As Integer) As Boolean

    ''' <summary>
    ''' Attribution_s the save fund prices.
    ''' </summary>
    ''' <param name="MainForm">The main form.</param>
    ''' <param name="pFundID">The p fund ID.</param>
    ''' <param name="pValueDate">The p value date.</param>
    ''' <param name="pStatusGroupFilter">The p status group filter.</param>
    ''' <param name="pAdministratorDatesFilter">The p administrator dates filter.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Friend Function Attribution_SaveFundPrices(ByRef MainForm As VeniceMain, ByVal pFundID As Integer, ByVal pValueDate As Date, ByVal pStatusGroupFilter As String, ByVal pAdministratorDatesFilter As Integer) As Boolean
    ' ************************************************************************************
    ' Interface routine to the spu_SaveFundPrices stored procedure
    '
    ' 
    ' ************************************************************************************

    Dim SavePricesCommand As New SqlCommand

    Try

      ' Construct Command object

      SavePricesCommand.CommandType = CommandType.StoredProcedure
      SavePricesCommand.CommandText = "[spu_SaveFundPrices]"
      SavePricesCommand.CommandTimeout = ATTRIBUTION_SQLCOMMAND_TIMEOUT

      SavePricesCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
      SavePricesCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FundID", System.Data.SqlDbType.Int, 4))
      SavePricesCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValueDate", System.Data.SqlDbType.DateTime, 8))
      SavePricesCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@StatusGroupFilter", System.Data.SqlDbType.NVarChar)).Value = pStatusGroupFilter
      SavePricesCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AdministratorDatesFilter", System.Data.SqlDbType.Int)).Value = pAdministratorDatesFilter
      SavePricesCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DontReplaceExistingPrices", System.Data.SqlDbType.Int, 4))
      SavePricesCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DontReplaceExistingFinalPrices", System.Data.SqlDbType.Int, 4))
      SavePricesCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@DontReplaceExistingMilestonePrices", System.Data.SqlDbType.Int, 4))
      SavePricesCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@rvStatusString", System.Data.SqlDbType.NVarChar, 200, System.Data.ParameterDirection.Output, True, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))

      ' Initialise Command object

      SavePricesCommand.Parameters("@FundID").Value = pFundID
      SavePricesCommand.Parameters("@ValueDate").Value = pValueDate.Date
      SavePricesCommand.Parameters("@DontReplaceExistingPrices").Value = 0
      SavePricesCommand.Parameters("@DontReplaceExistingFinalPrices").Value = 1
      SavePricesCommand.Parameters("@DontReplaceExistingMilestonePrices").Value = 1

      Try
        ' Log command.

        Dim MsgString As String
        Dim ThisParam As System.Data.SqlClient.SqlParameter

        MsgString = "Save Fund Price : "
        For Each ThisParam In SavePricesCommand.Parameters
          If (ThisParam.Value Is Nothing) Then
            MsgString = MsgString & ThisParam.ParameterName & "=<Nothing>, "
          Else
            MsgString = MsgString & ThisParam.ParameterName & "=`" & ThisParam.Value.ToString & "`, "
          End If
        Next
        MainForm.LogError("Attribution_SaveFundPrices()", RenaissanceGlobals.LOG_LEVELS.Changes, "SaveFundPrices", MsgString, "", False)

      Catch ex As Exception
      End Try

      ' Execute Command object.
      ' Save Fund Prices.

      Try
        SavePricesCommand.Connection = MainForm.GetVeniceConnection

        If (Not (SavePricesCommand.Connection Is Nothing)) Then
          SyncLock SavePricesCommand.Connection
            SavePricesCommand.ExecuteNonQuery()
          End SyncLock
        End If

      Catch ex As Exception

        MainForm.LogError("Attribution_SaveFundPrices()", RenaissanceGlobals.LOG_LEVELS.Error, "Error Saving Fund Price.", ex.Message, ex.StackTrace, True)
        Return False

      Finally

        If (Not (SavePricesCommand.Connection Is Nothing)) Then
          Try
            SavePricesCommand.Connection.Close()
          Catch ex As Exception
          End Try
        End If
      End Try


      ' Check for Error Return value.

      If CInt(SavePricesCommand.Parameters("@RETURN_VALUE").Value) = 0 Then
        MainForm.LogError("Attribution_SaveFundPrices()", RenaissanceGlobals.LOG_LEVELS.Error, "Error Saving Fund Price.", SavePricesCommand.Parameters("@rvStatusString").Value.ToString, "", True)
        Return False
      End If

      ' End.

      Return True

    Catch ex As Exception
      MainForm.LogError("Attribution_SaveFundPrices()", RenaissanceGlobals.LOG_LEVELS.Error, "Error Saving Fund Price.", ex.Message, ex.StackTrace, True)
      Return False
    End Try

  End Function

    ''' <summary>
    ''' Attribution_s the save atributions_ asynch.
    ''' </summary>
    ''' <param name="MainForm">The main form.</param>
    ''' <param name="pFundID">The p fund ID.</param>
    ''' <param name="pAttributionDate">The p attribution date.</param>
    ''' <param name="pAttribPeriod">The p attrib period.</param>
    ''' <param name="pIsMilestone">if set to <c>true</c> [p is milestone].</param>
    ''' <param name="pIsLookthrough">if set to <c>true</c> [p is lookthrough].</param>
    ''' <param name="pUseMSRecordsForSubFundLookthrough">if set to <c>true</c> [p use MS records for sub fund lookthrough].</param>
    ''' <param name="pUseMSRecordsForThisFundLookthrough">if set to <c>true</c> [p use MS records for this fund lookthrough].</param>
    ''' <param name="pStartKnowledgedate">The p start knowledgedate.</param>
    ''' <param name="pEndKnowledgedate">The p end knowledgedate.</param>
    ''' <param name="pKnowledgedate">The p knowledgedate.</param>
    ''' <param name="pStatusBar">The p status bar.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Friend Function Attribution_SaveAtributions_Asynch(ByRef MainForm As VeniceMain, _
      ByVal pFundID As Integer, _
      ByVal pAttributionDate As Date, _
      ByVal pAttribPeriod As RenaissanceGlobals.DealingPeriod, _
      ByVal pIsMilestone As Boolean, _
      ByVal pIsLookthrough As Boolean, _
      ByVal pUseMSRecordsForSubFundLookthrough As Boolean, _
      ByVal pUseMSRecordsForThisFundLookthrough As Boolean, _
      ByVal pStartKnowledgedate As Date, _
      ByVal pEndKnowledgedate As Date, _
      ByVal pKnowledgedate As Date, ByRef pStatusBar As ToolStripProgressBar) As Boolean

    If (RenaissanceGlobals.RenaissanceStandardDatasets.tblAttribution.ActivityCount > 0) Then
      MainForm.LogError("SaveAtributions", RenaissanceGlobals.LOG_LEVELS.Warning, "", "There appears to be an Attribution process already running, Attribution cancelled.", "", True)
      Exit Function
    End If

    Try
      Dim caller As New Attribution_SaveAtributions_Delegate(AddressOf Attribution_SaveAtributions)

      Dim result As IAsyncResult = caller.BeginInvoke(MainForm, pFundID, pAttributionDate, pAttribPeriod, pIsMilestone, pIsLookthrough, pUseMSRecordsForSubFundLookthrough, pUseMSRecordsForThisFundLookthrough, pStartKnowledgedate, pEndKnowledgedate, pKnowledgedate, Nothing, Nothing)
      While result.IsCompleted = False
        Thread.Sleep(25)
        MainForm.IncrementStatusBar(pStatusBar)
        If Not (MainForm.InvokeRequired) Then
          Application.DoEvents()
        End If
      End While

      Try
        If (pStatusBar IsNot Nothing) Then
          pStatusBar.Visible = False
        End If
      Catch ex As Exception
      End Try

      Return caller.EndInvoke(MainForm, result)

    Catch ex As Exception

      Return False

    Finally

      Try
        ' If we have been Updating a ProgressBar from a Worker thread, re-build the Progress Bar.
        ' There seems to be a problem making them InVisible after Marshalled cross thread updates.

        If (pStatusBar IsNot Nothing) AndAlso (pStatusBar.Owner.InvokeRequired) Then
          MainForm.ResetProgressBar(pStatusBar.Owner, pStatusBar)
        End If
      Catch ex As Exception
      End Try

    End Try


  End Function


    ''' <summary>
    ''' Delegate Attribution_SaveAtributions_Delegate
    ''' </summary>
    ''' <param name="MainForm">The main form.</param>
    ''' <param name="pFundID">The p fund ID.</param>
    ''' <param name="pAttributionDate">The p attribution date.</param>
    ''' <param name="pAttribPeriod">The p attrib period.</param>
    ''' <param name="pIsMilestone">if set to <c>true</c> [p is milestone].</param>
    ''' <param name="pIsLookthrough">if set to <c>true</c> [p is lookthrough].</param>
    ''' <param name="pUseMSRecordsForSubFundLookthrough">if set to <c>true</c> [p use MS records for sub fund lookthrough].</param>
    ''' <param name="pUseMSRecordsForThisFundLookthrough">if set to <c>true</c> [p use MS records for this fund lookthrough].</param>
    ''' <param name="pStartKnowledgedate">The p start knowledgedate.</param>
    ''' <param name="pEndKnowledgedate">The p end knowledgedate.</param>
    ''' <param name="pKnowledgedate">The p knowledgedate.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Delegate Function Attribution_SaveAtributions_Delegate(ByRef MainForm As VeniceMain, _
     ByVal pFundID As Integer, _
     ByVal pAttributionDate As Date, _
     ByVal pAttribPeriod As RenaissanceGlobals.DealingPeriod, _
     ByVal pIsMilestone As Boolean, _
     ByVal pIsLookthrough As Boolean, _
     ByVal pUseMSRecordsForSubFundLookthrough As Boolean, _
     ByVal pUseMSRecordsForThisFundLookthrough As Boolean, _
     ByVal pStartKnowledgedate As Date, _
     ByVal pEndKnowledgedate As Date, _
     ByVal pKnowledgedate As Date) As Boolean

    ''' <summary>
    ''' Attribution_s the save atributions.
    ''' </summary>
    ''' <param name="MainForm">The main form.</param>
    ''' <param name="pFundID">The p fund ID.</param>
    ''' <param name="pAttributionDate">The p attribution date.</param>
    ''' <param name="pAttribPeriod">The p attrib period.</param>
    ''' <param name="pIsMilestone">if set to <c>true</c> [p is milestone].</param>
    ''' <param name="pIsLookthrough">if set to <c>true</c> [p is lookthrough].</param>
    ''' <param name="pUseMSRecordsForSubFundLookthrough">if set to <c>true</c> [p use MS records for sub fund lookthrough].</param>
    ''' <param name="pUseMSRecordsForThisFundLookthrough">if set to <c>true</c> [p use MS records for this fund lookthrough].</param>
    ''' <param name="pStartKnowledgedate">The p start knowledgedate.</param>
    ''' <param name="pEndKnowledgedate">The p end knowledgedate.</param>
    ''' <param name="pKnowledgedate">The p knowledgedate.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function Attribution_SaveAtributions(ByRef MainForm As VeniceMain, _
     ByVal pFundID As Integer, _
     ByVal pAttributionDate As Date, _
     ByVal pAttribPeriod As RenaissanceGlobals.DealingPeriod, _
     ByVal pIsMilestone As Boolean, _
     ByVal pIsLookthrough As Boolean, _
     ByVal pUseMSRecordsForSubFundLookthrough As Boolean, _
     ByVal pUseMSRecordsForThisFundLookthrough As Boolean, _
     ByVal pStartKnowledgedate As Date, _
     ByVal pEndKnowledgedate As Date, _
     ByVal pKnowledgedate As Date) As Boolean

    ' ************************************************************************************
    ' Interface routine to the spu_SaveInstrumentAttributions stored procedure
    '
    ' This routine saves calculates and saves Fund Attrubutions.
    '
    ' ************************************************************************************

    Dim SaveAttrbutionsCommand As New SqlCommand

    If (RenaissanceGlobals.RenaissanceStandardDatasets.tblAttribution.ActivityCount > 0) Then
      MainForm.LogError("SaveAtributions", RenaissanceGlobals.LOG_LEVELS.Warning, "", "There appears to be an Attribution process already running, Attribution cancelled.", "", True)
      Exit Function
    End If

    Try
      RenaissanceGlobals.RenaissanceStandardDatasets.tblAttribution.ActivityCount_Increment()

      ' Create Command object

      SaveAttrbutionsCommand.CommandType = CommandType.StoredProcedure
      SaveAttrbutionsCommand.CommandText = "[spu_SaveInstrumentAttributions]"
      SaveAttrbutionsCommand.Connection = MainForm.GetVeniceConnection
      SaveAttrbutionsCommand.CommandTimeout = ATTRIBUTION_SQLCOMMAND_TIMEOUT

      SaveAttrbutionsCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
      SaveAttrbutionsCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FundID", System.Data.SqlDbType.Int, 4))
      SaveAttrbutionsCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AttributionEndDate", System.Data.SqlDbType.DateTime, 8))
      SaveAttrbutionsCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AttributionStartDate", System.Data.SqlDbType.DateTime, 8))
      SaveAttrbutionsCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@AttributionPeriod", System.Data.SqlDbType.Int, 4))
      SaveAttrbutionsCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@IsMilestone", System.Data.SqlDbType.Int, 4))
      SaveAttrbutionsCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@IsLookthrough", System.Data.SqlDbType.Int, 4))
      SaveAttrbutionsCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UseMSRecordsForSubFundLookthrough", System.Data.SqlDbType.Int, 4))
      SaveAttrbutionsCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UseMSRecordsForThisFundLookthrough", System.Data.SqlDbType.Int, 4))
      SaveAttrbutionsCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LookthroughFundPath", System.Data.SqlDbType.VarChar, 200))
      SaveAttrbutionsCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@StartKnowledgedate", System.Data.SqlDbType.DateTime, 8))
      SaveAttrbutionsCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EndKnowledgedate", System.Data.SqlDbType.DateTime, 8))
      SaveAttrbutionsCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))
      SaveAttrbutionsCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@rvStatusString", System.Data.SqlDbType.VarChar, 200, System.Data.ParameterDirection.Output, True, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))

      ' Initialise Command object.

      SaveAttrbutionsCommand.Parameters("@FundID").Value = pFundID
      SaveAttrbutionsCommand.Parameters("@AttributionEndDate").Value = FitDateToPeriod(pAttribPeriod, pAttributionDate.Date, True)
      SaveAttrbutionsCommand.Parameters("@AttributionStartDate").Value = FitDateToPeriod(pAttribPeriod, AddPeriodToDate(pAttribPeriod, pAttributionDate.Date, -1), True)
      SaveAttrbutionsCommand.Parameters("@AttributionPeriod").Value = CInt(pAttribPeriod)

      If pIsMilestone Then
        SaveAttrbutionsCommand.Parameters("@IsMilestone").Value = 1
      Else
        SaveAttrbutionsCommand.Parameters("@IsMilestone").Value = 0
      End If

      If pIsLookthrough Then
        SaveAttrbutionsCommand.Parameters("@IsLookthrough").Value = 1
      Else
        SaveAttrbutionsCommand.Parameters("@IsLookthrough").Value = 0
      End If

      If pUseMSRecordsForSubFundLookthrough Then
        SaveAttrbutionsCommand.Parameters("@UseMSRecordsForSubFundLookthrough").Value = 1
      Else
        SaveAttrbutionsCommand.Parameters("@UseMSRecordsForSubFundLookthrough").Value = 0
      End If

      If pUseMSRecordsForThisFundLookthrough Then
        SaveAttrbutionsCommand.Parameters("@UseMSRecordsForThisFundLookthrough").Value = 1
      Else
        SaveAttrbutionsCommand.Parameters("@UseMSRecordsForThisFundLookthrough").Value = 0
      End If

      SaveAttrbutionsCommand.Parameters("@LookthroughFundPath").Value = ""
      SaveAttrbutionsCommand.Parameters("@StartKnowledgedate").Value = pStartKnowledgedate
      SaveAttrbutionsCommand.Parameters("@EndKnowledgedate").Value = pEndKnowledgedate
      SaveAttrbutionsCommand.Parameters("@Knowledgedate").Value = pKnowledgedate

      Try
        ' Log command.

        Dim MsgString As String
        Dim ThisParam As System.Data.SqlClient.SqlParameter

        MsgString = "Save Attributions : "
        For Each ThisParam In SaveAttrbutionsCommand.Parameters
          If (ThisParam.Value Is Nothing) Then
            MsgString = MsgString & ThisParam.ParameterName & "=<Nothing>, "
          Else
            MsgString = MsgString & ThisParam.ParameterName & "=`" & ThisParam.Value.ToString & "`, "
          End If
        Next
        MainForm.LogError("Attribution_SaveAtributions()", RenaissanceGlobals.LOG_LEVELS.Changes, "SaveAtributions", MsgString, "", False)

      Catch ex As Exception
      End Try

      ' Execute Command to save Attributions.

      Try

        If (Not (SaveAttrbutionsCommand.Connection Is Nothing)) Then
          SyncLock SaveAttrbutionsCommand.Connection
            SaveAttrbutionsCommand.ExecuteNonQuery()
          End SyncLock
        End If

      Catch ex As Exception

        MainForm.LogError("Attribution_SaveAtributions()", RenaissanceGlobals.LOG_LEVELS.Error, "Error Saving Attributions.", ex.Message, ex.StackTrace, True)
        Return False

      End Try



      ' Check command return value.

      If CInt(SaveAttrbutionsCommand.Parameters("@RETURN_VALUE").Value) = 0 Then
        MainForm.LogError("Attribution_SaveAtributions()", RenaissanceGlobals.LOG_LEVELS.Error, "Error Saving Attributions.", SaveAttrbutionsCommand.Parameters("@rvStatusString").Value.ToString, "", True)
        Return False
      End If

      ' End.

      Return True


    Catch ex As Exception
      MainForm.LogError("Attribution_SaveAtributions()", RenaissanceGlobals.LOG_LEVELS.Error, "Error Saving Attributions.", ex.Message, ex.StackTrace, True)
      Return False

    Finally
      RenaissanceGlobals.RenaissanceStandardDatasets.tblAttribution.ActivityCount_Decrement()

      If (Not (SaveAttrbutionsCommand.Connection Is Nothing)) Then
        Try
          SaveAttrbutionsCommand.Connection.Close()
        Catch ex As Exception
        End Try
      End If
    End Try

  End Function

End Module
