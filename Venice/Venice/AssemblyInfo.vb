Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Venice")> 
<Assembly: AssemblyDescription("Venice")> 
<Assembly: AssemblyCompany("F&&C Partners LLP")> 
<Assembly: AssemblyProduct("Venice")> 
<Assembly: AssemblyCopyright("Copyright F&&C Partners LLP 2006")> 
<Assembly: AssemblyTrademark("")> 
<Assembly: CLSCompliant(True)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("7B910D31-B5DB-4192-9406-EC37DE7F8EDE")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("1.0.1.*")> 
