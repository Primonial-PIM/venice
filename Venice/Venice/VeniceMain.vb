' ***********************************************************************
  ' Assembly         : Venice
  ' Author           : Nicholas Pennington
  ' Created          : 11-28-2012
  '
  ' Last Modified By : Nicholas Pennington
  ' Last Modified On : 08-15-2013
  ' ***********************************************************************
  ' <copyright file="VeniceMain.vb" company="">
  '     Copyright (c) . All rights reserved.
  ' </copyright>
  ' <summary></summary>
  ' ***********************************************************************
  Imports Microsoft.Win32
  Imports System.IO
  Imports System.Data.SqlClient
  Imports System.Net
  Imports System.Net.Sockets
  Imports System.Reflection
  Imports System.ComponentModel
  Imports System.Globalization

  Imports TCPServer_Common
  Imports RenaissanceGlobals
  Imports RenaissanceGlobals.Globals

  Imports RenaissanceDataClass
  Imports RenaissanceControls
  Imports RenaissanceStatFunctions
  Imports RenaissancePertracDataClass


''' <summary>
''' Class VeniceMain
''' </summary>
Public Class VeniceMain
  Inherits System.Windows.Forms.Form
  Implements RenaissanceGlobals.StandardRenaissanceMainForm

  ''' <summary>
  ''' Declaration of the GetPrivateProfileString() Kernel function. Used to access INI files.
  ''' </summary>
  ''' <param name="lpAppName">Application Name.</param>
  ''' <param name="lpKeyName">Key Name.</param>
  ''' <param name="lpDefault">Default Value.</param>
  ''' <param name="lpReturnedString">Returned Value.</param>
  ''' <param name="nSize">Size of string buffer.</param>
  ''' <param name="lpFileName">.ini file name.</param>
  ''' <returns>System.Int32.</returns>
  Private Declare Auto Function GetPrivateProfileString Lib "kernel32" (ByVal lpAppName As String, _
  ByVal lpKeyName As String, _
  ByVal lpDefault As String, _
  ByVal lpReturnedString As System.Text.StringBuilder, _
  ByVal nSize As Integer, _
  ByVal lpFileName As String) As Integer


#Region " Windows Form Designer generated code "

  ''' <summary>
  ''' Initializes a new instance of the <see cref="VeniceMain"/> class.
  ''' </summary>
  Public Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  ''' <summary>
  ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
  ''' </summary>
  ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  ''' <summary>
  ''' The components
  ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  ''' <summary>
  ''' The main tool bar
  ''' </summary>
  Friend WithEvents MainToolBar As System.Windows.Forms.ToolBar
  ''' <summary>
  ''' The venice menu
  ''' </summary>
  Friend WithEvents VeniceMenu As System.Windows.Forms.MenuStrip
  ''' <summary>
  ''' The menu_ file
  ''' </summary>
  Friend WithEvents Menu_File As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ static
  ''' </summary>
  Friend WithEvents Menu_Static As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ file_ change connection
  ''' </summary>
  Friend WithEvents Menu_File_ChangeConnection As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ static_ counterparty
  ''' </summary>
  Friend WithEvents Menu_Static_Counterparty As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ static_ currency
  ''' </summary>
  Friend WithEvents Menu_Static_Currency As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ static_ funds
  ''' </summary>
  Friend WithEvents Menu_Static_Funds As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ static_ fund type
  ''' </summary>
  Friend WithEvents Menu_Static_FundType As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ static_ instrument
  ''' </summary>
  Friend WithEvents Menu_Static_Instrument As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ static_ instrument type
  ''' </summary>
  Friend WithEvents Menu_Static_InstrumentType As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ static_ people
  ''' </summary>
  Friend WithEvents Menu_Static_People As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ static_ transaction types
  ''' </summary>
  Friend WithEvents Menu_Static_TransactionTypes As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  '''The menu_ static_ brokers
  ''' </summary>
  Friend WithEvents Menu_Static_Brokers As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  '''The menu_ static_ brokers accounts
  ''' </summary>
  Friend WithEvents Menu_Static_BrokersAccounts As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The tool strip separator1
  ''' </summary>
  Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
  ''' <summary>
  ''' The menu_ file_ MDI
  ''' </summary>
  Friend WithEvents Menu_File_MDI As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The tool strip separator2
  ''' </summary>
  Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
  ''' <summary>
  ''' The menu_ file_ close
  ''' </summary>
  Friend WithEvents Menu_File_Close As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ transactions
  ''' </summary>
  Friend WithEvents Menu_Transactions As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ trans_ select transactions
  ''' </summary>
  Friend WithEvents Menu_Trans_SelectTransactions As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ trans_ select transactions list
  ''' </summary>
  Friend WithEvents Menu_Trans_SelectTransactionsList As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The tool strip separator3
  ''' </summary>
  Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
  ''' <summary>
  ''' The menu_ trans_ edit transactions
  ''' </summary>
  Friend WithEvents Menu_Trans_EditTransactions As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ prices
  ''' </summary>
  Friend WithEvents Menu_Prices As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ prices_ FX rates
  ''' </summary>
  Friend WithEvents Menu_Prices_FXRates As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ prices_ prices
  ''' </summary>
  Friend WithEvents Menu_Prices_Prices As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ risk
  ''' </summary>
  Friend WithEvents Menu_Risk As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ risk_ edit limits
  ''' </summary>
  Friend WithEvents Menu_Risk_EditLimits As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The tool strip separator4
  ''' </summary>
  Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
  ''' <summary>
  ''' The menu_ risk_ exposure report
  ''' </summary>
  Friend WithEvents Menu_Risk_ExposureReport As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ about
  ''' </summary>
  Friend WithEvents Menu_About As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ knowledge date
  ''' </summary>
  Friend WithEvents Menu_KnowledgeDate As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ K d_ set KD
  ''' </summary>
  Friend WithEvents Menu_KD_SetKD As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The tool strip separator6
  ''' </summary>
  Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
  ''' <summary>
  ''' The menu_ K d_ refresh KD
  ''' </summary>
  Friend WithEvents Menu_KD_RefreshKD As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ month end
  ''' </summary>
  Friend WithEvents Menu_MonthEnd As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The tool strip separator8
  ''' </summary>
  Friend WithEvents ToolStripSeparator8 As System.Windows.Forms.ToolStripSeparator
  ''' <summary>
  ''' The menu_ month end_ reports
  ''' </summary>
  Friend WithEvents Menu_MonthEnd_Reports As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ reports
  ''' </summary>
  Friend WithEvents Menu_Reports As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ RPT_ fund RPTS
  ''' </summary>
  Friend WithEvents Menu_Rpt_FundRpts As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ RPT_ AUM
  ''' </summary>
  Friend WithEvents Menu_Rpt_AUM As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ RPT_ fund expense
  ''' </summary>
  Friend WithEvents Menu_Rpt_FundExpense As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ RPT_ valuation
  ''' </summary>
  Friend WithEvents Menu_Rpt_Valuation As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ RPT_ fund pn L
  ''' </summary>
  Friend WithEvents Menu_Rpt_FundPnL As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ RPT_ cash report
  ''' </summary>
  Friend WithEvents Menu_Rpt_CashReport As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ RPT_ price reports
  ''' </summary>
  Friend WithEvents Menu_Rpt_PriceReports As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ K d_ display
  ''' </summary>
  Friend WithEvents Menu_KD_Display As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The tool strip separator12
  ''' </summary>
  Friend WithEvents ToolStripSeparator12 As System.Windows.Forms.ToolStripSeparator
  ''' <summary>
  ''' The menu_ windows
  ''' </summary>
  Friend WithEvents Menu_Windows As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ system
  ''' </summary>
  Friend WithEvents Menu_System As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ system_ user permissions
  ''' </summary>
  Friend WithEvents Menu_System_UserPermissions As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The venice status strip
  ''' </summary>
  Friend WithEvents VeniceStatusStrip As System.Windows.Forms.StatusStrip
  ''' <summary>
  ''' The venice status bar
  ''' </summary>
  Friend WithEvents VeniceStatusBar As System.Windows.Forms.ToolStripStatusLabel
  ''' <summary>
  ''' The venice progress bar
  ''' </summary>
  Friend WithEvents VeniceProgressBar As System.Windows.Forms.ToolStripProgressBar
  ''' <summary>
  ''' The menu_ use multithreaded reporting
  ''' </summary>
  Friend WithEvents Menu_UseMultithreadedReporting As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ RPT_ report data
  ''' </summary>
  Friend WithEvents Menu_Rpt_ReportData As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ RPT_ report data_ save data
  ''' </summary>
  Friend WithEvents Menu_Rpt_ReportData_SaveData As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ file_ enable report fade in
  ''' </summary>
  Friend WithEvents Menu_File_EnableReportFadeIn As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The venice command dock
  ''' </summary>
  Friend WithEvents VeniceCommandDock As C1.Win.C1Command.C1CommandDock
  ''' <summary>
  ''' The venice command holder
  ''' </summary>
  Friend WithEvents VeniceCommandHolder As C1.Win.C1Command.C1CommandHolder
  ''' <summary>
  ''' The c1 menu_ file
  ''' </summary>
  Friend WithEvents C1Menu_File As C1.Win.C1Command.C1CommandMenu
  ''' <summary>
  ''' The c1 link_ file_ command1
  ''' </summary>
  Friend WithEvents C1Link_File_Command1 As C1.Win.C1Command.C1CommandLink
  ''' <summary>
  ''' The c1 menu_ static data
  ''' </summary>
  Friend WithEvents C1Menu_StaticData As C1.Win.C1Command.C1CommandMenu
  ''' <summary>
  ''' The c1 link_ counterparties
  ''' </summary>
  Friend WithEvents C1Link_Counterparties As C1.Win.C1Command.C1CommandLink
  ''' <summary>
  ''' The c1 menu_ static_ counterparties
  ''' </summary>
  Friend WithEvents C1Menu_Static_Counterparties As C1.Win.C1Command.C1Command
  ''' <summary>
  ''' The c1 command link2
  ''' </summary>
  Friend WithEvents C1CommandLink2 As C1.Win.C1Command.C1CommandLink
  ''' <summary>
  ''' The c1 menu_ static_ currencies
  ''' </summary>
  Friend WithEvents C1Menu_Static_Currencies As C1.Win.C1Command.C1Command
  ''' <summary>
  ''' The tool bar_ test
  ''' </summary>
  Friend WithEvents ToolBar_Test As C1.Win.C1Command.C1ToolBar
  ''' <summary>
  ''' The button2
  ''' </summary>
  Friend WithEvents Button2 As System.Windows.Forms.Button
  ''' <summary>
  ''' The button3
  ''' </summary>
  Friend WithEvents Button3 As System.Windows.Forms.Button
  ''' <summary>
  ''' The menu_ reports_frm notional management reports
  ''' </summary>
  Friend WithEvents Menu_Reports_frmNotionalManagementReports As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ prices_ price grid
  ''' </summary>
  Friend WithEvents Menu_Prices_PriceGrid As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ risk_ characteristics
  ''' </summary>
  Friend WithEvents Menu_Risk_Characteristics As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ prices_ greeks grid
  ''' </summary>
  Friend WithEvents Menu_Prices_GreeksGrid As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ risk_ risk characteristics
  ''' </summary>
  Friend WithEvents Menu_Risk_RiskCharacteristics As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ month end_ select futures notional
  ''' </summary>
  Friend WithEvents Menu_MonthEnd_SelectFuturesNotional As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ EO d_ reconciliation form
  ''' </summary>
  Friend WithEvents Menu_EOD_ReconciliationForm As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The tool strip separator15
  ''' </summary>
  Friend WithEvents ToolStripSeparator15 As System.Windows.Forms.ToolStripSeparator
  ''' <summary>
  ''' The menu_ trans_ approve transactions
  ''' </summary>
  Friend WithEvents Menu_Trans_ApproveTransactions As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The button1
  ''' </summary>
  Friend WithEvents Button1 As System.Windows.Forms.Button
  ''' <summary>
  ''' The menu_ trans_ update transactions
  ''' </summary>
  Friend WithEvents Menu_Trans_UpdateTransactions As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ static_ sub fund
  ''' </summary>
  Friend WithEvents Menu_Static_SubFund As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The information reports tool strip menu item
  ''' </summary>
  Friend WithEvents InformationReportsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ report_ transaction status
  ''' </summary>
  Friend WithEvents Menu_Report_TransactionStatus As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ trans_frm manage sub funds
  ''' </summary>
  Friend WithEvents Menu_Trans_frmManageSubFunds As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ trans_ cash ladder
  ''' </summary>
  Friend WithEvents Menu_Trans_CashLadder As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ static_ sub fund heirarchy
  ''' </summary>
  Friend WithEvents Menu_Static_SubFundHeirarchy As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ prices_ FX grid
  ''' </summary>
  Friend WithEvents Menu_Prices_FXGrid As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The menu_ static_ select instrument
  ''' </summary>
  Friend WithEvents Menu_Static_SelectInstrument As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_EOD_LoadPackVL As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Trans_ImportTransactions As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents kansasIsAliveCheckTimer As System.Windows.Forms.Timer
  Friend WithEvents Menu_Static_Fees As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' Initializes the component.
  ''' </summary>
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(VeniceMain))
    Me.MainToolBar = New System.Windows.Forms.ToolBar
    Me.VeniceMenu = New System.Windows.Forms.MenuStrip
    Me.Menu_File = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_File_ChangeConnection = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
    Me.Menu_File_MDI = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_UseMultithreadedReporting = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_File_EnableReportFadeIn = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
    Me.Menu_File_Close = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Static = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Static_Counterparty = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Static_Currency = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Static_Funds = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Static_SubFund = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Static_SubFundHeirarchy = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Static_FundType = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Static_Instrument = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Static_SelectInstrument = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Static_InstrumentType = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Static_People = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Static_TransactionTypes = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Static_Brokers = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Static_BrokersAccounts = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Static_Fees = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Transactions = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Trans_SelectTransactions = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Trans_SelectTransactionsList = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Trans_frmManageSubFunds = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Trans_CashLadder = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripSeparator15 = New System.Windows.Forms.ToolStripSeparator
    Me.Menu_Trans_EditTransactions = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Trans_ImportTransactions = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
    Me.Menu_Trans_ApproveTransactions = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Trans_UpdateTransactions = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Prices = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Prices_FXRates = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Prices_FXGrid = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Prices_Prices = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Prices_PriceGrid = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Prices_GreeksGrid = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Risk = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Risk_EditLimits = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Risk_RiskCharacteristics = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Risk_Characteristics = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator
    Me.Menu_Risk_ExposureReport = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_About = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_KnowledgeDate = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_KD_SetKD = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator
    Me.Menu_KD_RefreshKD = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_KD_Display = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_MonthEnd = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_EOD_ReconciliationForm = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripSeparator8 = New System.Windows.Forms.ToolStripSeparator
    Me.Menu_MonthEnd_Reports = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_MonthEnd_SelectFuturesNotional = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_EOD_LoadPackVL = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Reports = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Rpt_FundRpts = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Rpt_AUM = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Rpt_FundExpense = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Reports_frmNotionalManagementReports = New System.Windows.Forms.ToolStripMenuItem
    Me.InformationReportsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Report_TransactionStatus = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Rpt_Valuation = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Rpt_FundPnL = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Rpt_CashReport = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Rpt_PriceReports = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripSeparator12 = New System.Windows.Forms.ToolStripSeparator
    Me.Menu_Rpt_ReportData = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Rpt_ReportData_SaveData = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Windows = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_System = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_System_UserPermissions = New System.Windows.Forms.ToolStripMenuItem
    Me.VeniceStatusStrip = New System.Windows.Forms.StatusStrip
    Me.VeniceProgressBar = New System.Windows.Forms.ToolStripProgressBar
    Me.VeniceStatusBar = New System.Windows.Forms.ToolStripStatusLabel
    Me.VeniceCommandDock = New C1.Win.C1Command.C1CommandDock
    Me.ToolBar_Test = New C1.Win.C1Command.C1ToolBar
    Me.VeniceCommandHolder = New C1.Win.C1Command.C1CommandHolder
    Me.C1Menu_File = New C1.Win.C1Command.C1CommandMenu
    Me.C1Link_File_Command1 = New C1.Win.C1Command.C1CommandLink
    Me.C1Menu_StaticData = New C1.Win.C1Command.C1CommandMenu
    Me.C1Link_Counterparties = New C1.Win.C1Command.C1CommandLink
    Me.C1Menu_Static_Counterparties = New C1.Win.C1Command.C1Command
    Me.C1CommandLink2 = New C1.Win.C1Command.C1CommandLink
    Me.C1Menu_Static_Currencies = New C1.Win.C1Command.C1Command
    Me.Button2 = New System.Windows.Forms.Button
    Me.Button3 = New System.Windows.Forms.Button
    Me.Button1 = New System.Windows.Forms.Button
    Me.kansasIsAliveCheckTimer = New System.Windows.Forms.Timer(Me.components)
    Me.VeniceMenu.SuspendLayout()
    Me.VeniceStatusStrip.SuspendLayout()
    CType(Me.VeniceCommandDock, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.VeniceCommandDock.SuspendLayout()
    CType(Me.VeniceCommandHolder, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'MainToolBar
    '
    Me.MainToolBar.Dock = System.Windows.Forms.DockStyle.None
    Me.MainToolBar.DropDownArrows = True
    Me.MainToolBar.Location = New System.Drawing.Point(48, 116)
    Me.MainToolBar.Name = "MainToolBar"
    Me.MainToolBar.ShowToolTips = True
    Me.MainToolBar.Size = New System.Drawing.Size(100, 42)
    Me.MainToolBar.TabIndex = 3
    '
    'VeniceMenu
    '
    Me.VeniceMenu.AllowMerge = False
    Me.VeniceMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_File, Me.Menu_Static, Me.Menu_Transactions, Me.Menu_Prices, Me.Menu_Risk, Me.Menu_About, Me.Menu_KnowledgeDate, Me.Menu_MonthEnd, Me.Menu_Reports, Me.Menu_Windows, Me.Menu_System})
    Me.VeniceMenu.Location = New System.Drawing.Point(0, 0)
    Me.VeniceMenu.Name = "VeniceMenu"
    Me.VeniceMenu.Size = New System.Drawing.Size(1093, 24)
    Me.VeniceMenu.TabIndex = 0
    Me.VeniceMenu.Text = "VeniceMenu"
    '
    'Menu_File
    '
    Me.Menu_File.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_File_ChangeConnection, Me.ToolStripSeparator1, Me.Menu_File_MDI, Me.Menu_UseMultithreadedReporting, Me.Menu_File_EnableReportFadeIn, Me.ToolStripSeparator2, Me.Menu_File_Close})
    Me.Menu_File.Name = "Menu_File"
    Me.Menu_File.Size = New System.Drawing.Size(37, 20)
    Me.Menu_File.Text = "&File"
    '
    'Menu_File_ChangeConnection
    '
    Me.Menu_File_ChangeConnection.Name = "Menu_File_ChangeConnection"
    Me.Menu_File_ChangeConnection.Size = New System.Drawing.Size(226, 22)
    Me.Menu_File_ChangeConnection.Text = "Change &DB Connection"
    '
    'ToolStripSeparator1
    '
    Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
    Me.ToolStripSeparator1.Size = New System.Drawing.Size(223, 6)
    '
    'Menu_File_MDI
    '
    Me.Menu_File_MDI.Name = "Menu_File_MDI"
    Me.Menu_File_MDI.Size = New System.Drawing.Size(226, 22)
    Me.Menu_File_MDI.Tag = ""
    Me.Menu_File_MDI.Text = "Toggle &MDI"
    '
    'Menu_UseMultithreadedReporting
    '
    Me.Menu_UseMultithreadedReporting.Name = "Menu_UseMultithreadedReporting"
    Me.Menu_UseMultithreadedReporting.Size = New System.Drawing.Size(226, 22)
    Me.Menu_UseMultithreadedReporting.Tag = "ToggleMultithreadedReports"
    Me.Menu_UseMultithreadedReporting.Text = "Use Multithreaded Reporting"
    '
    'Menu_File_EnableReportFadeIn
    '
    Me.Menu_File_EnableReportFadeIn.Name = "Menu_File_EnableReportFadeIn"
    Me.Menu_File_EnableReportFadeIn.Size = New System.Drawing.Size(226, 22)
    Me.Menu_File_EnableReportFadeIn.Tag = "ToggleReportFadeIn"
    Me.Menu_File_EnableReportFadeIn.Text = "Enable Report Fade-In"
    '
    'ToolStripSeparator2
    '
    Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
    Me.ToolStripSeparator2.Size = New System.Drawing.Size(223, 6)
    '
    'Menu_File_Close
    '
    Me.Menu_File_Close.Name = "Menu_File_Close"
    Me.Menu_File_Close.Size = New System.Drawing.Size(226, 22)
    Me.Menu_File_Close.Text = "&Close"
    '
    'Menu_Static
    '
    Me.Menu_Static.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_Static_Counterparty, Me.Menu_Static_Currency, Me.Menu_Static_Funds, Me.Menu_Static_SubFund, Me.Menu_Static_SubFundHeirarchy, Me.Menu_Static_FundType, Me.Menu_Static_Instrument, Me.Menu_Static_SelectInstrument, Me.Menu_Static_InstrumentType, Me.Menu_Static_People, Me.Menu_Static_TransactionTypes, Me.Menu_Static_Brokers, Me.Menu_Static_BrokersAccounts, Me.Menu_Static_Fees})
    Me.Menu_Static.Name = "Menu_Static"
    Me.Menu_Static.Size = New System.Drawing.Size(75, 20)
    Me.Menu_Static.Text = "&Static Data"
    '
    'Menu_Static_Counterparty
    '
    Me.Menu_Static_Counterparty.Name = "Menu_Static_Counterparty"
    Me.Menu_Static_Counterparty.Size = New System.Drawing.Size(181, 22)
    Me.Menu_Static_Counterparty.Tag = "frmCounterparty"
    Me.Menu_Static_Counterparty.Text = "&Counterparty"
    '
    'Menu_Static_Currency
    '
    Me.Menu_Static_Currency.Name = "Menu_Static_Currency"
    Me.Menu_Static_Currency.Size = New System.Drawing.Size(181, 22)
    Me.Menu_Static_Currency.Tag = "frmCurrency"
    Me.Menu_Static_Currency.Text = "&Currency"
    '
    'Menu_Static_Funds
    '
    Me.Menu_Static_Funds.Name = "Menu_Static_Funds"
    Me.Menu_Static_Funds.Size = New System.Drawing.Size(181, 22)
    Me.Menu_Static_Funds.Tag = "frmFund"
    Me.Menu_Static_Funds.Text = "&Funds"
    '
    'Menu_Static_SubFund
    '
    Me.Menu_Static_SubFund.Name = "Menu_Static_SubFund"
    Me.Menu_Static_SubFund.Size = New System.Drawing.Size(181, 22)
    Me.Menu_Static_SubFund.Tag = "frmSubFund"
    Me.Menu_Static_SubFund.Text = "Sub Fund"
    '
    'Menu_Static_SubFundHeirarchy
    '
    Me.Menu_Static_SubFundHeirarchy.Name = "Menu_Static_SubFundHeirarchy"
    Me.Menu_Static_SubFundHeirarchy.Size = New System.Drawing.Size(181, 22)
    Me.Menu_Static_SubFundHeirarchy.Tag = "frmSubFundParent"
    Me.Menu_Static_SubFundHeirarchy.Text = "SubFund Heirarchy"
    '
    'Menu_Static_FundType
    '
    Me.Menu_Static_FundType.Name = "Menu_Static_FundType"
    Me.Menu_Static_FundType.Size = New System.Drawing.Size(181, 22)
    Me.Menu_Static_FundType.Tag = "frmFundType"
    Me.Menu_Static_FundType.Text = "Fund &Type"
    '
    'Menu_Static_Instrument
    '
    Me.Menu_Static_Instrument.Name = "Menu_Static_Instrument"
    Me.Menu_Static_Instrument.Size = New System.Drawing.Size(181, 22)
    Me.Menu_Static_Instrument.Tag = "frmInstrument"
    Me.Menu_Static_Instrument.Text = "&Instrument"
    '
    'Menu_Static_SelectInstrument
    '
    Me.Menu_Static_SelectInstrument.Name = "Menu_Static_SelectInstrument"
    Me.Menu_Static_SelectInstrument.Size = New System.Drawing.Size(181, 22)
    Me.Menu_Static_SelectInstrument.Tag = "frmSelectInstruments"
    Me.Menu_Static_SelectInstrument.Text = "Select Instrument"
    '
    'Menu_Static_InstrumentType
    '
    Me.Menu_Static_InstrumentType.Name = "Menu_Static_InstrumentType"
    Me.Menu_Static_InstrumentType.Size = New System.Drawing.Size(181, 22)
    Me.Menu_Static_InstrumentType.Tag = "frmInstrumentType"
    Me.Menu_Static_InstrumentType.Text = "&Instrument &Type"
    '
    'Menu_Static_People
    '
    Me.Menu_Static_People.Name = "Menu_Static_People"
    Me.Menu_Static_People.Size = New System.Drawing.Size(181, 22)
    Me.Menu_Static_People.Tag = "frmPerson"
    Me.Menu_Static_People.Text = "&People"
    '
    'Menu_Static_TransactionTypes
    '
    Me.Menu_Static_TransactionTypes.Name = "Menu_Static_TransactionTypes"
    Me.Menu_Static_TransactionTypes.Size = New System.Drawing.Size(181, 22)
    Me.Menu_Static_TransactionTypes.Tag = "frmTransactionType"
    Me.Menu_Static_TransactionTypes.Text = "Transaction T&ypes"
    '
    'Menu_Static_Brokers
    '
    Me.Menu_Static_Brokers.Name = "Menu_Static_Brokers"
    Me.Menu_Static_Brokers.Size = New System.Drawing.Size(181, 22)
    Me.Menu_Static_Brokers.Tag = "frmBroker"
    Me.Menu_Static_Brokers.Text = "Brokers"
    '
    'Menu_Static_BrokersAccounts
    '
    Me.Menu_Static_BrokersAccounts.Name = "Menu_Static_BrokersAccounts"
    Me.Menu_Static_BrokersAccounts.Size = New System.Drawing.Size(181, 22)
    Me.Menu_Static_BrokersAccounts.Tag = "frmBrokerAccount"
    Me.Menu_Static_BrokersAccounts.Text = "Brokers Accounts"
    '
    'Menu_Static_Fees
    '
    Me.Menu_Static_Fees.Name = "Menu_Static_Fees"
    Me.Menu_Static_Fees.Size = New System.Drawing.Size(181, 22)
    Me.Menu_Static_Fees.Tag = "frmFees"
    Me.Menu_Static_Fees.Text = "Recurring Fund Fees"
    '
    'Menu_Transactions
    '
    Me.Menu_Transactions.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_Trans_SelectTransactions, Me.Menu_Trans_SelectTransactionsList, Me.Menu_Trans_frmManageSubFunds, Me.Menu_Trans_CashLadder, Me.ToolStripSeparator15, Me.Menu_Trans_EditTransactions, Me.Menu_Trans_ImportTransactions, Me.ToolStripSeparator3, Me.Menu_Trans_ApproveTransactions, Me.Menu_Trans_UpdateTransactions})
    Me.Menu_Transactions.Name = "Menu_Transactions"
    Me.Menu_Transactions.Size = New System.Drawing.Size(86, 20)
    Me.Menu_Transactions.Text = "&Transactions"
    '
    'Menu_Trans_SelectTransactions
    '
    Me.Menu_Trans_SelectTransactions.Name = "Menu_Trans_SelectTransactions"
    Me.Menu_Trans_SelectTransactions.Size = New System.Drawing.Size(236, 22)
    Me.Menu_Trans_SelectTransactions.Tag = "frmSelectTransaction"
    Me.Menu_Trans_SelectTransactions.Text = "&Select Transactions"
    '
    'Menu_Trans_SelectTransactionsList
    '
    Me.Menu_Trans_SelectTransactionsList.Name = "Menu_Trans_SelectTransactionsList"
    Me.Menu_Trans_SelectTransactionsList.Size = New System.Drawing.Size(236, 22)
    Me.Menu_Trans_SelectTransactionsList.Tag = "frmSelectTransactionList"
    Me.Menu_Trans_SelectTransactionsList.Text = "Holdings Drilldown"
    '
    'Menu_Trans_frmManageSubFunds
    '
    Me.Menu_Trans_frmManageSubFunds.Name = "Menu_Trans_frmManageSubFunds"
    Me.Menu_Trans_frmManageSubFunds.Size = New System.Drawing.Size(236, 22)
    Me.Menu_Trans_frmManageSubFunds.Tag = "frmManageSubFunds"
    Me.Menu_Trans_frmManageSubFunds.Text = "Strategy Allocation"
    '
    'Menu_Trans_CashLadder
    '
    Me.Menu_Trans_CashLadder.Name = "Menu_Trans_CashLadder"
    Me.Menu_Trans_CashLadder.Size = New System.Drawing.Size(236, 22)
    Me.Menu_Trans_CashLadder.Tag = "frmCashLadder"
    Me.Menu_Trans_CashLadder.Text = "Cash Ladder"
    '
    'ToolStripSeparator15
    '
    Me.ToolStripSeparator15.Name = "ToolStripSeparator15"
    Me.ToolStripSeparator15.Size = New System.Drawing.Size(233, 6)
    '
    'Menu_Trans_EditTransactions
    '
    Me.Menu_Trans_EditTransactions.Name = "Menu_Trans_EditTransactions"
    Me.Menu_Trans_EditTransactions.Size = New System.Drawing.Size(236, 22)
    Me.Menu_Trans_EditTransactions.Tag = "frmTransaction"
    Me.Menu_Trans_EditTransactions.Text = "Add Transactions"
    '
    'Menu_Trans_ImportTransactions
    '
    Me.Menu_Trans_ImportTransactions.Name = "Menu_Trans_ImportTransactions"
    Me.Menu_Trans_ImportTransactions.Size = New System.Drawing.Size(236, 22)
    Me.Menu_Trans_ImportTransactions.Tag = "frmImportTransactions"
    Me.Menu_Trans_ImportTransactions.Text = "Import Transactions"
    '
    'ToolStripSeparator3
    '
    Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
    Me.ToolStripSeparator3.Size = New System.Drawing.Size(233, 6)
    '
    'Menu_Trans_ApproveTransactions
    '
    Me.Menu_Trans_ApproveTransactions.Name = "Menu_Trans_ApproveTransactions"
    Me.Menu_Trans_ApproveTransactions.Size = New System.Drawing.Size(236, 22)
    Me.Menu_Trans_ApproveTransactions.Tag = "frmApproveTransactions"
    Me.Menu_Trans_ApproveTransactions.Text = "Approve Pending Transactions"
    '
    'Menu_Trans_UpdateTransactions
    '
    Me.Menu_Trans_UpdateTransactions.Name = "Menu_Trans_UpdateTransactions"
    Me.Menu_Trans_UpdateTransactions.Size = New System.Drawing.Size(236, 22)
    Me.Menu_Trans_UpdateTransactions.Tag = "frmUpdateTransactions"
    Me.Menu_Trans_UpdateTransactions.Text = "Update Transactions"
    '
    'Menu_Prices
    '
    Me.Menu_Prices.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_Prices_FXRates, Me.Menu_Prices_FXGrid, Me.Menu_Prices_Prices, Me.Menu_Prices_PriceGrid, Me.Menu_Prices_GreeksGrid})
    Me.Menu_Prices.Name = "Menu_Prices"
    Me.Menu_Prices.Size = New System.Drawing.Size(94, 20)
    Me.Menu_Prices.Text = "&Prices && &Rates"
    '
    'Menu_Prices_FXRates
    '
    Me.Menu_Prices_FXRates.Name = "Menu_Prices_FXRates"
    Me.Menu_Prices_FXRates.Size = New System.Drawing.Size(134, 22)
    Me.Menu_Prices_FXRates.Tag = "frmFX"
    Me.Menu_Prices_FXRates.Text = "F&X Rates"
    '
    'Menu_Prices_FXGrid
    '
    Me.Menu_Prices_FXGrid.Name = "Menu_Prices_FXGrid"
    Me.Menu_Prices_FXGrid.Size = New System.Drawing.Size(134, 22)
    Me.Menu_Prices_FXGrid.Tag = "frmFXGrid"
    Me.Menu_Prices_FXGrid.Text = "FX Grid"
    '
    'Menu_Prices_Prices
    '
    Me.Menu_Prices_Prices.Name = "Menu_Prices_Prices"
    Me.Menu_Prices_Prices.Size = New System.Drawing.Size(134, 22)
    Me.Menu_Prices_Prices.Tag = "frmPrice"
    Me.Menu_Prices_Prices.Text = "&Prices"
    '
    'Menu_Prices_PriceGrid
    '
    Me.Menu_Prices_PriceGrid.Name = "Menu_Prices_PriceGrid"
    Me.Menu_Prices_PriceGrid.Size = New System.Drawing.Size(134, 22)
    Me.Menu_Prices_PriceGrid.Tag = "frmPriceGrid"
    Me.Menu_Prices_PriceGrid.Text = "Price Grid"
    '
    'Menu_Prices_GreeksGrid
    '
    Me.Menu_Prices_GreeksGrid.Name = "Menu_Prices_GreeksGrid"
    Me.Menu_Prices_GreeksGrid.Size = New System.Drawing.Size(134, 22)
    Me.Menu_Prices_GreeksGrid.Tag = "frmDeltaGrid"
    Me.Menu_Prices_GreeksGrid.Text = "Greeks Grid"
    '
    'Menu_Risk
    '
    Me.Menu_Risk.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_Risk_EditLimits, Me.Menu_Risk_RiskCharacteristics, Me.Menu_Risk_Characteristics, Me.ToolStripSeparator4, Me.Menu_Risk_ExposureReport})
    Me.Menu_Risk.Name = "Menu_Risk"
    Me.Menu_Risk.Size = New System.Drawing.Size(40, 20)
    Me.Menu_Risk.Text = "&Risk"
    '
    'Menu_Risk_EditLimits
    '
    Me.Menu_Risk_EditLimits.Name = "Menu_Risk_EditLimits"
    Me.Menu_Risk_EditLimits.Size = New System.Drawing.Size(225, 22)
    Me.Menu_Risk_EditLimits.Tag = "frmLimits"
    Me.Menu_Risk_EditLimits.Text = "Add / Edit &Limits"
    '
    'Menu_Risk_RiskCharacteristics
    '
    Me.Menu_Risk_RiskCharacteristics.Name = "Menu_Risk_RiskCharacteristics"
    Me.Menu_Risk_RiskCharacteristics.Size = New System.Drawing.Size(225, 22)
    Me.Menu_Risk_RiskCharacteristics.Tag = "frmRiskCharacteristics"
    Me.Menu_Risk_RiskCharacteristics.Text = "Add / Edit Characteristics"
    '
    'Menu_Risk_Characteristics
    '
    Me.Menu_Risk_Characteristics.Name = "Menu_Risk_Characteristics"
    Me.Menu_Risk_Characteristics.Size = New System.Drawing.Size(225, 22)
    Me.Menu_Risk_Characteristics.Tag = "frmCharacteristics"
    Me.Menu_Risk_Characteristics.Text = "Edit Risk C&haracteristics Data"
    '
    'ToolStripSeparator4
    '
    Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
    Me.ToolStripSeparator4.Size = New System.Drawing.Size(222, 6)
    '
    'Menu_Risk_ExposureReport
    '
    Me.Menu_Risk_ExposureReport.Name = "Menu_Risk_ExposureReport"
    Me.Menu_Risk_ExposureReport.Size = New System.Drawing.Size(225, 22)
    Me.Menu_Risk_ExposureReport.Tag = "frmRiskExposureReport"
    Me.Menu_Risk_ExposureReport.Text = "Risk &Exposure Report"
    '
    'Menu_About
    '
    Me.Menu_About.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
    Me.Menu_About.Name = "Menu_About"
    Me.Menu_About.Size = New System.Drawing.Size(52, 20)
    Me.Menu_About.Tag = "frmAbout"
    Me.Menu_About.Text = "&About"
    '
    'Menu_KnowledgeDate
    '
    Me.Menu_KnowledgeDate.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_KD_SetKD, Me.ToolStripSeparator6, Me.Menu_KD_RefreshKD, Me.Menu_KD_Display})
    Me.Menu_KnowledgeDate.Name = "Menu_KnowledgeDate"
    Me.Menu_KnowledgeDate.Size = New System.Drawing.Size(102, 20)
    Me.Menu_KnowledgeDate.Text = "&KnowledgeDate"
    '
    'Menu_KD_SetKD
    '
    Me.Menu_KD_SetKD.Name = "Menu_KD_SetKD"
    Me.Menu_KD_SetKD.Size = New System.Drawing.Size(176, 22)
    Me.Menu_KD_SetKD.Tag = "frmSetKnowledgeDate"
    Me.Menu_KD_SetKD.Text = "&Set KnowledgeDate"
    '
    'ToolStripSeparator6
    '
    Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
    Me.ToolStripSeparator6.Size = New System.Drawing.Size(173, 6)
    '
    'Menu_KD_RefreshKD
    '
    Me.Menu_KD_RefreshKD.Name = "Menu_KD_RefreshKD"
    Me.Menu_KD_RefreshKD.Size = New System.Drawing.Size(176, 22)
    Me.Menu_KD_RefreshKD.Text = "&Refresh All Tables"
    '
    'Menu_KD_Display
    '
    Me.Menu_KD_Display.Name = "Menu_KD_Display"
    Me.Menu_KD_Display.Size = New System.Drawing.Size(176, 22)
    Me.Menu_KD_Display.Text = " "
    '
    'Menu_MonthEnd
    '
    Me.Menu_MonthEnd.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_EOD_ReconciliationForm, Me.ToolStripSeparator8, Me.Menu_MonthEnd_Reports, Me.Menu_MonthEnd_SelectFuturesNotional, Me.Menu_EOD_LoadPackVL})
    Me.Menu_MonthEnd.Name = "Menu_MonthEnd"
    Me.Menu_MonthEnd.Size = New System.Drawing.Size(78, 20)
    Me.Menu_MonthEnd.Text = "&End Of Day"
    '
    'Menu_EOD_ReconciliationForm
    '
    Me.Menu_EOD_ReconciliationForm.Name = "Menu_EOD_ReconciliationForm"
    Me.Menu_EOD_ReconciliationForm.Size = New System.Drawing.Size(195, 22)
    Me.Menu_EOD_ReconciliationForm.Tag = "frmReconciliationProcess"
    Me.Menu_EOD_ReconciliationForm.Text = "Reconciliation Process"
    '
    'ToolStripSeparator8
    '
    Me.ToolStripSeparator8.Name = "ToolStripSeparator8"
    Me.ToolStripSeparator8.Size = New System.Drawing.Size(192, 6)
    Me.ToolStripSeparator8.Visible = False
    '
    'Menu_MonthEnd_Reports
    '
    Me.Menu_MonthEnd_Reports.Name = "Menu_MonthEnd_Reports"
    Me.Menu_MonthEnd_Reports.Size = New System.Drawing.Size(195, 22)
    Me.Menu_MonthEnd_Reports.Tag = "frmReconciliationReports"
    Me.Menu_MonthEnd_Reports.Text = "&Reconcilliation Reports"
    '
    'Menu_MonthEnd_SelectFuturesNotional
    '
    Me.Menu_MonthEnd_SelectFuturesNotional.Name = "Menu_MonthEnd_SelectFuturesNotional"
    Me.Menu_MonthEnd_SelectFuturesNotional.Size = New System.Drawing.Size(195, 22)
    Me.Menu_MonthEnd_SelectFuturesNotional.Tag = "frmSelectFuturesNotional"
    Me.Menu_MonthEnd_SelectFuturesNotional.Text = "Sweep Futures P&&L"
    '
    'Menu_EOD_LoadPackVL
    '
    Me.Menu_EOD_LoadPackVL.Name = "Menu_EOD_LoadPackVL"
    Me.Menu_EOD_LoadPackVL.Size = New System.Drawing.Size(195, 22)
    Me.Menu_EOD_LoadPackVL.Tag = "frmLoadPacVL"
    Me.Menu_EOD_LoadPackVL.Text = "Load Pack VL"
    '
    'Menu_Reports
    '
    Me.Menu_Reports.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_Rpt_FundRpts, Me.InformationReportsToolStripMenuItem, Me.Menu_Rpt_Valuation, Me.Menu_Rpt_FundPnL, Me.Menu_Rpt_CashReport, Me.Menu_Rpt_PriceReports, Me.ToolStripSeparator12, Me.Menu_Rpt_ReportData})
    Me.Menu_Reports.Name = "Menu_Reports"
    Me.Menu_Reports.Size = New System.Drawing.Size(59, 20)
    Me.Menu_Reports.Text = "&Reports"
    '
    'Menu_Rpt_FundRpts
    '
    Me.Menu_Rpt_FundRpts.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_Rpt_AUM, Me.Menu_Rpt_FundExpense, Me.Menu_Reports_frmNotionalManagementReports})
    Me.Menu_Rpt_FundRpts.Name = "Menu_Rpt_FundRpts"
    Me.Menu_Rpt_FundRpts.Size = New System.Drawing.Size(182, 22)
    Me.Menu_Rpt_FundRpts.Text = "&Fund Reports"
    '
    'Menu_Rpt_AUM
    '
    Me.Menu_Rpt_AUM.Name = "Menu_Rpt_AUM"
    Me.Menu_Rpt_AUM.Size = New System.Drawing.Size(287, 22)
    Me.Menu_Rpt_AUM.Tag = "frmAUMReport"
    Me.Menu_Rpt_AUM.Text = "Assets Under Management"
    '
    'Menu_Rpt_FundExpense
    '
    Me.Menu_Rpt_FundExpense.Name = "Menu_Rpt_FundExpense"
    Me.Menu_Rpt_FundExpense.Size = New System.Drawing.Size(287, 22)
    Me.Menu_Rpt_FundExpense.Tag = "frmExpensesReport"
    Me.Menu_Rpt_FundExpense.Text = "Fund &Expense Report"
    '
    'Menu_Reports_frmNotionalManagementReports
    '
    Me.Menu_Reports_frmNotionalManagementReports.Name = "Menu_Reports_frmNotionalManagementReports"
    Me.Menu_Reports_frmNotionalManagementReports.Size = New System.Drawing.Size(287, 22)
    Me.Menu_Reports_frmNotionalManagementReports.Tag = "frmNotionalManagementReports"
    Me.Menu_Reports_frmNotionalManagementReports.Text = "Futures / Notional Management Reports"
    '
    'InformationReportsToolStripMenuItem
    '
    Me.InformationReportsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_Report_TransactionStatus})
    Me.InformationReportsToolStripMenuItem.Name = "InformationReportsToolStripMenuItem"
    Me.InformationReportsToolStripMenuItem.Size = New System.Drawing.Size(182, 22)
    Me.InformationReportsToolStripMenuItem.Text = "Information Reports"
    '
    'Menu_Report_TransactionStatus
    '
    Me.Menu_Report_TransactionStatus.Name = "Menu_Report_TransactionStatus"
    Me.Menu_Report_TransactionStatus.Size = New System.Drawing.Size(214, 22)
    Me.Menu_Report_TransactionStatus.Tag = "frmTransactionStatusReport"
    Me.Menu_Report_TransactionStatus.Text = "Transaction Status Reports"
    '
    'Menu_Rpt_Valuation
    '
    Me.Menu_Rpt_Valuation.Name = "Menu_Rpt_Valuation"
    Me.Menu_Rpt_Valuation.Size = New System.Drawing.Size(182, 22)
    Me.Menu_Rpt_Valuation.Tag = "frmValuationReport"
    Me.Menu_Rpt_Valuation.Text = "Valuation Report"
    '
    'Menu_Rpt_FundPnL
    '
    Me.Menu_Rpt_FundPnL.Name = "Menu_Rpt_FundPnL"
    Me.Menu_Rpt_FundPnL.Size = New System.Drawing.Size(182, 22)
    Me.Menu_Rpt_FundPnL.Tag = "frmProfitandLossReport"
    Me.Menu_Rpt_FundPnL.Text = "Fund &P&&L Report"
    '
    'Menu_Rpt_CashReport
    '
    Me.Menu_Rpt_CashReport.Name = "Menu_Rpt_CashReport"
    Me.Menu_Rpt_CashReport.Size = New System.Drawing.Size(182, 22)
    Me.Menu_Rpt_CashReport.Tag = "frmCashReports"
    Me.Menu_Rpt_CashReport.Text = "&Cash Reports"
    '
    'Menu_Rpt_PriceReports
    '
    Me.Menu_Rpt_PriceReports.Name = "Menu_Rpt_PriceReports"
    Me.Menu_Rpt_PriceReports.Size = New System.Drawing.Size(182, 22)
    Me.Menu_Rpt_PriceReports.Tag = "frmPriceReport"
    Me.Menu_Rpt_PriceReports.Text = "Price and FX Reports"
    '
    'ToolStripSeparator12
    '
    Me.ToolStripSeparator12.Name = "ToolStripSeparator12"
    Me.ToolStripSeparator12.Size = New System.Drawing.Size(179, 6)
    '
    'Menu_Rpt_ReportData
    '
    Me.Menu_Rpt_ReportData.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_Rpt_ReportData_SaveData})
    Me.Menu_Rpt_ReportData.Name = "Menu_Rpt_ReportData"
    Me.Menu_Rpt_ReportData.Size = New System.Drawing.Size(182, 22)
    Me.Menu_Rpt_ReportData.Text = "Report Data"
    '
    'Menu_Rpt_ReportData_SaveData
    '
    Me.Menu_Rpt_ReportData_SaveData.Name = "Menu_Rpt_ReportData_SaveData"
    Me.Menu_Rpt_ReportData_SaveData.Size = New System.Drawing.Size(181, 22)
    Me.Menu_Rpt_ReportData_SaveData.Text = "Save DataSets to File"
    '
    'Menu_Windows
    '
    Me.Menu_Windows.Name = "Menu_Windows"
    Me.Menu_Windows.Size = New System.Drawing.Size(68, 20)
    Me.Menu_Windows.Text = "&Windows"
    '
    'Menu_System
    '
    Me.Menu_System.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_System_UserPermissions})
    Me.Menu_System.Name = "Menu_System"
    Me.Menu_System.Size = New System.Drawing.Size(57, 20)
    Me.Menu_System.Text = "&System"
    '
    'Menu_System_UserPermissions
    '
    Me.Menu_System_UserPermissions.Name = "Menu_System_UserPermissions"
    Me.Menu_System_UserPermissions.Size = New System.Drawing.Size(163, 22)
    Me.Menu_System_UserPermissions.Tag = "frmUserPermissions"
    Me.Menu_System_UserPermissions.Text = "&User Permissions"
    '
    'VeniceStatusStrip
    '
    Me.VeniceStatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.VeniceProgressBar, Me.VeniceStatusBar})
    Me.VeniceStatusStrip.Location = New System.Drawing.Point(0, 152)
    Me.VeniceStatusStrip.Name = "VeniceStatusStrip"
    Me.VeniceStatusStrip.Size = New System.Drawing.Size(1093, 22)
    Me.VeniceStatusStrip.TabIndex = 5
    '
    'VeniceProgressBar
    '
    Me.VeniceProgressBar.Maximum = 20
    Me.VeniceProgressBar.Name = "VeniceProgressBar"
    Me.VeniceProgressBar.Size = New System.Drawing.Size(150, 17)
    Me.VeniceProgressBar.Step = 1
    Me.VeniceProgressBar.Visible = False
    '
    'VeniceStatusBar
    '
    Me.VeniceStatusBar.Name = "VeniceStatusBar"
    Me.VeniceStatusBar.Size = New System.Drawing.Size(10, 17)
    Me.VeniceStatusBar.Text = " "
    '
    'VeniceCommandDock
    '
    Me.VeniceCommandDock.AutoDockBottom = False
    Me.VeniceCommandDock.AutoDockLeft = False
    Me.VeniceCommandDock.AutoDockRight = False
    Me.VeniceCommandDock.AutoDockTop = False
    Me.VeniceCommandDock.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.VeniceCommandDock.Controls.Add(Me.ToolBar_Test)
    Me.VeniceCommandDock.Id = 1
    Me.VeniceCommandDock.Location = New System.Drawing.Point(0, 24)
    Me.VeniceCommandDock.MinimumSize = New System.Drawing.Size(24, 24)
    Me.VeniceCommandDock.Name = "VeniceCommandDock"
    Me.VeniceCommandDock.Size = New System.Drawing.Size(1093, 41)
    '
    'ToolBar_Test
    '
    Me.ToolBar_Test.AccessibleName = "Tool Bar"
    Me.ToolBar_Test.BackColor = System.Drawing.SystemColors.Control
    Me.ToolBar_Test.CommandHolder = Me.VeniceCommandHolder
    Me.ToolBar_Test.Location = New System.Drawing.Point(139, 0)
    Me.ToolBar_Test.MinButtonSize = 32
    Me.ToolBar_Test.Name = "ToolBar_Test"
    Me.ToolBar_Test.Size = New System.Drawing.Size(32, 32)
    Me.ToolBar_Test.Text = "C1ToolBar2"
    Me.ToolBar_Test.Visible = False
    Me.ToolBar_Test.VisualStyle = C1.Win.C1Command.VisualStyle.Custom
    Me.ToolBar_Test.VisualStyleBase = C1.Win.C1Command.VisualStyle.Office2003Blue
    '
    'VeniceCommandHolder
    '
    Me.VeniceCommandHolder.Commands.Add(Me.C1Menu_File)
    Me.VeniceCommandHolder.Commands.Add(Me.C1Menu_StaticData)
    Me.VeniceCommandHolder.Commands.Add(Me.C1Menu_Static_Counterparties)
    Me.VeniceCommandHolder.Commands.Add(Me.C1Menu_Static_Currencies)
    Me.VeniceCommandHolder.Owner = Me
    Me.VeniceCommandHolder.VisualStyle = C1.Win.C1Command.VisualStyle.Office2003Blue
    '
    'C1Menu_File
    '
    Me.C1Menu_File.CommandLinks.AddRange(New C1.Win.C1Command.C1CommandLink() {Me.C1Link_File_Command1})
    Me.C1Menu_File.HideNonRecentLinks = False
    Me.C1Menu_File.Name = "C1Menu_File"
    Me.C1Menu_File.ShowToolTips = True
    Me.C1Menu_File.Text = "&File"
    Me.C1Menu_File.VisualStyleBase = C1.Win.C1Command.VisualStyle.Office2003Blue
    '
    'C1Link_File_Command1
    '
    Me.C1Link_File_Command1.Text = "Command1"
    '
    'C1Menu_StaticData
    '
    Me.C1Menu_StaticData.CommandLinks.AddRange(New C1.Win.C1Command.C1CommandLink() {Me.C1Link_Counterparties, Me.C1CommandLink2})
    Me.C1Menu_StaticData.HideNonRecentLinks = False
    Me.C1Menu_StaticData.Name = "C1Menu_StaticData"
    Me.C1Menu_StaticData.Text = "&Static Data"
    Me.C1Menu_StaticData.VisualStyleBase = C1.Win.C1Command.VisualStyle.Office2003Blue
    '
    'C1Link_Counterparties
    '
    Me.C1Link_Counterparties.Command = Me.C1Menu_Static_Counterparties
    Me.C1Link_Counterparties.Text = "Counterparties"
    '
    'C1Menu_Static_Counterparties
    '
    Me.C1Menu_Static_Counterparties.Name = "C1Menu_Static_Counterparties"
    Me.C1Menu_Static_Counterparties.Text = "&Counterparties"
    Me.C1Menu_Static_Counterparties.UserData = "frmCounterparty"
    '
    'C1CommandLink2
    '
    Me.C1CommandLink2.Command = Me.C1Menu_Static_Currencies
    Me.C1CommandLink2.SortOrder = 1
    Me.C1CommandLink2.Text = "Currencies"
    '
    'C1Menu_Static_Currencies
    '
    Me.C1Menu_Static_Currencies.Name = "C1Menu_Static_Currencies"
    Me.C1Menu_Static_Currencies.Text = "Currencies"
    '
    'Button2
    '
    Me.Button2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Button2.Location = New System.Drawing.Point(89, 103)
    Me.Button2.Name = "Button2"
    Me.Button2.Size = New System.Drawing.Size(52, 32)
    Me.Button2.TabIndex = 6
    Me.Button2.Text = "Button2"
    Me.Button2.Visible = False
    '
    'Button3
    '
    Me.Button3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Button3.Location = New System.Drawing.Point(147, 103)
    Me.Button3.Name = "Button3"
    Me.Button3.Size = New System.Drawing.Size(52, 32)
    Me.Button3.TabIndex = 8
    Me.Button3.Text = "Button3"
    Me.Button3.Visible = False
    '
    'Button1
    '
    Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Button1.Location = New System.Drawing.Point(31, 103)
    Me.Button1.Name = "Button1"
    Me.Button1.Size = New System.Drawing.Size(52, 32)
    Me.Button1.TabIndex = 10
    Me.Button1.Text = "Button1"
    Me.Button1.Visible = False
    '
    'kansasIsAliveCheckTimer
    '
    Me.kansasIsAliveCheckTimer.Enabled = True
    Me.kansasIsAliveCheckTimer.Interval = 60000
    '
    'VeniceMain
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(1093, 174)
    Me.Controls.Add(Me.Button1)
    Me.Controls.Add(Me.Button3)
    Me.Controls.Add(Me.Button2)
    Me.Controls.Add(Me.VeniceCommandDock)
    Me.Controls.Add(Me.VeniceStatusStrip)
    Me.Controls.Add(Me.VeniceMenu)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
    Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
    Me.MainMenuStrip = Me.VeniceMenu
    Me.MaximizeBox = False
    Me.Name = "VeniceMain"
    Me.Text = "Venice"
    Me.VeniceMenu.ResumeLayout(False)
    Me.VeniceMenu.PerformLayout()
    Me.VeniceStatusStrip.ResumeLayout(False)
    Me.VeniceStatusStrip.PerformLayout()
    CType(Me.VeniceCommandDock, System.ComponentModel.ISupportInitialize).EndInit()
    Me.VeniceCommandDock.ResumeLayout(False)
    CType(Me.VeniceCommandHolder, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Class Globals and Declarations"

  ' Declaration of the AutoUpdate Event.
  ' Used to notify forms of changes.
  ''' <summary>
  ''' Triggered when the application need to notify of updates to data tables or the KnowledgeDate.
  ''' </summary>
  Public Event VeniceAutoUpdate(ByVal sender As Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs) Implements RenaissanceGlobals.StandardRenaissanceMainForm.RenaissanceAutoUpdate

  ' Collection of Venice Form Handles. One item for the Main-form and one item for each sub-Form.
  ''' <summary>
  ''' The _ venice forms
  ''' </summary>
  Private _VeniceForms As New RenaissanceFormCollection

  ''' <summary>
  ''' Flag to indicate Venice is closing.
  ''' </summary>
  Public IsClosing As Boolean = False

  ' Central Data Structures.
  ''' <summary>
  ''' The venice TCP client - Interfaces with the Message Server.
  ''' </summary>
  Private WithEvents VeniceTCPClient As TCPServer_Common.GenericTCPClient
  ''' <summary>
  ''' The update event control
  ''' </summary>
  Private UpdateEventControl As UpdateEventControlClass

  ' Generic Form-Update Timer
  ''' <summary>
  ''' Timer used by the central update process.
  ''' </summary>
  Private WithEvents FormUpdateDataTimer As System.Windows.Forms.Timer
  ''' <summary>
  ''' Collection of forms serviced by the central update process.
  ''' </summary>
  Private FormUpdateObjects As New Dictionary(Of System.Windows.Forms.Form, RenaissanceTimerUpdateClass)

  ' Application Knowledgedate
  ''' <summary>
  ''' Venice-wide Knowledge Date value.
  ''' </summary>
  Private _Main_Knowledgedate As Date

  ''' <summary>
  ''' Enable / Disable the use of report worker threads.
  ''' </summary>
  Private _UseReportWorkerThreads As Boolean = True
  ''' <summary>
  ''' Enable / Disable the background table loader process.
  ''' </summary>
  Private _UseBackgroundTableLoader As Boolean = True ' Enable / Disable use of the BackgroundTableLoader process.

  ' Central Data and Report handlers
  ''' <summary>
  ''' The _ main data handler
  ''' </summary>
  Private _MainDataHandler As New RenaissanceDataClass.DataHandler
  ''' <summary>
  ''' The _ main adaptor handler
  ''' </summary>
  Private _MainAdaptorHandler As New RenaissanceDataClass.AdaptorHandler
  ''' <summary>
  ''' The _ main report handler
  ''' </summary>
  Private _MainReportHandler As New ReportHandler(Me)
  ''' <summary>
  ''' The _ pertrac data
  ''' </summary>
  Private _PertracData As New RenaissancePertracDataClass.PertracDataClass(Me)
  ''' <summary>
  ''' The _ stat functions
  ''' </summary>
  Private _StatFunctions As New RenaissanceStatFunctions.StatFunctions(Me, _PertracData)

  ' Standard Message Server
  ''' <summary>
  ''' The _ message server name
  ''' </summary>
  Private _MessageServerName As String = ""
  ''' <summary>
  ''' The message server address
  ''' </summary>
  Private MessageServerAddress As IPAddress = IPAddress.None

  ' SQL Server
  ''' <summary>
  ''' The SQL server name
  ''' </summary>
  Private SQLServerName As String = "<Unknown>"
  ''' <summary>
	''' The SQL server instance name. Empty string for Default instance.
  ''' </summary>
  Private SQLServerInstance As String = ""
  ''' <summary>
  ''' The _ SQL connection string
  ''' </summary>
  Private _SQLConnectionString As String
  'Private _SQLAsynchConnectString As String
  ''' <summary>
  ''' The _ SQL trusted connection
  ''' </summary>
  Private _SQLTrustedConnection As Boolean = True
  ''' <summary>
  ''' The _ SQL user name
  ''' </summary>
  Private _SQLUserName As String = ""
  ''' <summary>
  ''' The _ SQL password
  ''' </summary>
  Private _SQLPassword As String = ""
  ''' <summary>
  ''' The _ entry form_ cache count
  ''' </summary>
  Private _EntryForm_CacheCount As Integer = 0 ' STANDARD_EntryForm_CACHE_COUNT
  ''' <summary>
  ''' The _ report form_ cache count
  ''' </summary>
  Private _ReportForm_CacheCount As Integer = 0 ' STANDARD_ReportForm_CACHE_COUNT
  ''' <summary>
  ''' The _ single cache_ cache count
  ''' </summary>
  Private _SingleCache_CacheCount As Integer = 0 ' STANDARD_SingleForm_CACHE_COUNT
  ''' <summary>
  ''' The _ no cache_ cache count
  ''' </summary>
  Private _NoCache_CacheCount As Integer = 0

  ' 
  ''' <summary>
  ''' The background table loader process
  ''' </summary>
  Private WithEvents BackgroundTableLoaderProcess As BackgroundTableLoaderClass

  ''' <summary>
  ''' The STANDAR d_ report form_ CACH e_ COUNT
  ''' </summary>
  Public Const STANDARD_ReportForm_CACHE_COUNT As Integer = 1

  ''' <summary>
  ''' The tables not to log changes for.
  ''' In order to prevent adding millions of un-necessary rows to the Log tables, it is possible to specify tables where changes are not automatically logged.
  ''' </summary>
  Public TablesNotToLogChangesFor() As String = {RenaissanceStandardDatasets.tblPertracCustomFieldData.TableName, RenaissanceStandardDatasets.tblExposure.TableName}
  ''' <summary>
  ''' The splash form.
  ''' </summary>
  Dim Splash As New VeniceSplash
  ''' <summary>
  ''' The splash time
  ''' </summary>
  Dim SplashTime As Date

  ''' <summary>
  ''' The _ mastername dictionary
  ''' </summary>
  Friend _MasternameDictionary As New LookupCollection(Of Integer, String)     ' Dictionary(Of Integer, String)
  ''' <summary>
  ''' The _ locations dictionary
  ''' </summary>
  Friend _LocationsDictionary As LookupCollection(Of Integer, String) = Nothing  ' Dictionary(Of Integer, String)
  ''' <summary>
  ''' The _ temp storage dictionary
  ''' </summary>
  Friend _TempStorageDictionary As New Dictionary(Of String, Object)

  Friend KansasStatusMessages As New Dictionary(Of String, String)

  ''' <summary>
  ''' The PARTIA l_ UPDAT e_ TABL e_ ITE m_ LIMIT
  ''' </summary>
  Private Const PARTIAL_UPDATE_TABLE_ITEM_LIMIT As Integer = 30

  Public Extra_Debug As Boolean = False

#End Region


#Region " Form Properties"

  ''' <summary>
  ''' Gets or sets the main_ knowledgedate.
  ''' </summary>
  ''' <value>The main_ knowledgedate.</value>
  Public Property Main_Knowledgedate() As Date Implements StandardRenaissanceMainForm.Main_Knowledgedate
    ' *******************************************************************************
    ' Public property to Set the system Knowledgedate
    '
    ' The update event is automatically triggered.
    '
    ' *******************************************************************************
    Get
      Return _Main_Knowledgedate
    End Get
    Set(ByVal Value As Date)
      If Value <> _Main_Knowledgedate Then
        _Main_Knowledgedate = Value

        If Value.CompareTo(KNOWLEDGEDATE_NOW) <= 0 Then
          SetToolStripText(VeniceStatusBar, "KnowledgeDate is Live.")
          Me.Menu_KD_Display.Text = ""
        Else
          If Value.TimeOfDay.TotalSeconds = 0 Then
            SetToolStripText(VeniceStatusBar, "KnowledgeDate is " & _Main_Knowledgedate.ToString(DISPLAYMEMBER_DATEFORMAT))
            Me.Menu_KD_Display.Text = "KD is " & _Main_Knowledgedate.ToString(DISPLAYMEMBER_DATEFORMAT)
          Else
            SetToolStripText(VeniceStatusBar, "KnowledgeDate is " & _Main_Knowledgedate.ToString(DISPLAYMEMBER_LONGDATEFORMAT))
            Me.Menu_KD_Display.Text = "KD is " & _Main_Knowledgedate.ToString(DISPLAYMEMBER_LONGDATEFORMAT)
          End If
        End If

        Call Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.KnowledgeDate))

      End If
    End Set
  End Property

  ''' <summary>
  ''' Gets the main data handler.
  ''' </summary>
  ''' <value>The main data handler.</value>
  Public ReadOnly Property MainDataHandler() As RenaissanceDataClass.DataHandler Implements StandardRenaissanceMainForm.MainDataHandler
    Get
      Return _MainDataHandler
    End Get
  End Property

  ''' <summary>
  ''' Gets the main adaptor handler.
  ''' </summary>
  ''' <value>The main adaptor handler.</value>
  Public ReadOnly Property MainAdaptorHandler() As RenaissanceDataClass.AdaptorHandler Implements StandardRenaissanceMainForm.MainAdaptorHandler
    Get
      Return _MainAdaptorHandler
    End Get
  End Property

  'Public ReadOnly Property Pertrac() As PertracFunctions
  '	Get
  '		Return _Pertrac
  '	End Get
  'End Property

  ''' <summary>
  ''' Gets the main report handler.
  ''' </summary>
  ''' <value>The main report handler.</value>
  Public ReadOnly Property MainReportHandler() As ReportHandler
    Get
      Return _MainReportHandler
    End Get
  End Property

  ''' <summary>
  ''' Gets a value indicating whether [use report worker threads].
  ''' </summary>
  ''' <value><c>true</c> if [use report worker threads]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property UseReportWorkerThreads() As Boolean
    Get
      Return _UseReportWorkerThreads
    End Get
  End Property

  ''' <summary>
  ''' Gets the venice forms.
  ''' </summary>
  ''' <value>The venice forms.</value>
  Public ReadOnly Property VeniceForms() As RenaissanceFormCollection
    Get
      Return _VeniceForms
    End Get
  End Property

  ''' <summary>
  ''' Gets the entry form_ cache count.
  ''' </summary>
  ''' <value>The entry form_ cache count.</value>
  Public ReadOnly Property EntryForm_CacheCount() As Integer
    Get
      Return _EntryForm_CacheCount
    End Get
  End Property

  ''' <summary>
  ''' Gets the no cache_ cache count.
  ''' </summary>
  ''' <value>The no cache_ cache count.</value>
  Public ReadOnly Property NoCache_CacheCount() As Integer
    Get
      Return _NoCache_CacheCount
    End Get
  End Property

  ''' <summary>
  ''' Gets the report form_ cache count.
  ''' </summary>
  ''' <value>The report form_ cache count.</value>
  Public ReadOnly Property ReportForm_CacheCount() As Integer
    Get
      Return _ReportForm_CacheCount
    End Get
  End Property

  ''' <summary>
  ''' Gets the single cache_ cache count.
  ''' </summary>
  ''' <value>The single cache_ cache count.</value>
  Public ReadOnly Property SingleCache_CacheCount() As Integer
    Get
      Return _SingleCache_CacheCount
    End Get
  End Property

  ''' <summary>
  ''' Gets the name of the message server.
  ''' </summary>
  ''' <value>The name of the message server.</value>
  Public ReadOnly Property MessageServerName() As String
    Get
      Return _MessageServerName
    End Get
  End Property

  ''' <summary>
  ''' Gets a value indicating whether [SQL trusted connection].
  ''' </summary>
  ''' <value><c>true</c> if [SQL trusted connection]; otherwise, <c>false</c>.</value>
  Private ReadOnly Property SQLTrustedConnection() As Boolean
    Get
      Return _SQLTrustedConnection
    End Get
  End Property

  ''' <summary>
  ''' Gets the name of the SQL user.
  ''' </summary>
  ''' <value>The name of the SQL user.</value>
  Private ReadOnly Property SQLUserName() As String
    Get
      Return _SQLUserName
    End Get
  End Property

  ''' <summary>
  ''' Gets the SQL password.
  ''' </summary>
  ''' <value>The SQL password.</value>
  Private ReadOnly Property SQLPassword() As String
    Get
      Return _SQLPassword
    End Get
  End Property

  ''' <summary>
  ''' Gets the SQL connect string.
  ''' </summary>
  ''' <value>The SQL connect string.</value>
  Public ReadOnly Property SQLConnectString() As String
    Get
      Return Me._SQLConnectionString
    End Get
  End Property

  'Public ReadOnly Property SQLAsynchConnectString() As String
  '	Get
  '		Return Me._SQLAsynchConnectString
  '	End Get
  'End Property

  ''' <summary>
  ''' Gets the stat functions.
  ''' </summary>
  ''' <value>The stat functions.</value>
  <CLSCompliant(False)> Public ReadOnly Property StatFunctions() As StatFunctions
    Get
      Return _StatFunctions
    End Get
  End Property

  ''' <summary>
  ''' Gets the pertrac data.
  ''' </summary>
  ''' <value>The pertrac data.</value>
  <CLSCompliant(False)> Public ReadOnly Property PertracData() As RenaissancePertracDataClass.PertracDataClass
    Get
      Return _PertracData
    End Get
  End Property

  ''' <summary>
  ''' Gets the mastername dictionary.
  ''' </summary>
  ''' <value>The mastername dictionary.</value>
  Public ReadOnly Property MasternameDictionary() As LookupCollection(Of Integer, String) Implements StandardRenaissanceMainForm.MasternameDictionary
    Get
      Return _MasternameDictionary
    End Get
  End Property

  ''' <summary>
  ''' Gets the locations dictionary.
  ''' </summary>
  ''' <value>The locations dictionary.</value>
  Public ReadOnly Property LocationsDictionary() As LookupCollection(Of Integer, String) Implements StandardRenaissanceMainForm.LocationsDictionary
    Get
      Return _LocationsDictionary
    End Get
  End Property

  ''' <summary>
  ''' Gets a value indicating whether [enable report fade in].
  ''' </summary>
  ''' <value><c>true</c> if [enable report fade in]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property EnableReportFadeIn() As Boolean
    Get
      Return Menu_File_EnableReportFadeIn.Checked
    End Get
  End Property

  ''' <summary>
  ''' Disables the report fade in.
  ''' </summary>
  Public Sub DisableReportFadeIn()
    Try
      Menu_File_EnableReportFadeIn.Checked = False
    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Gets the temp storage dictionary.
  ''' </summary>
  ''' <value>The temp storage dictionary.</value>
  Public ReadOnly Property TempStorageDictionary() As Dictionary(Of String, Object)
    Get
      Return _TempStorageDictionary
    End Get
  End Property

#End Region

#Region " Main, New, Load() and Closing() routines"


  ''' <summary>
  ''' Defines the entry point of the application.
  ''' </summary>
  ''' <param name="CmdArgs">The CMD args.</param>
  Shared Sub Main(ByVal CmdArgs() As String)
    ' *******************************************************************************
    ' Start Venice
    ' 
    ' *******************************************************************************

    Application.EnableVisualStyles()

    Dim RunningClass As VeniceMain
    Dim ErrorFlag As Boolean = True

    RunningClass = New VeniceMain(CmdArgs)

    While ErrorFlag
      ErrorFlag = False

      Try
        Application.Run(RunningClass)

      Catch ex As Exception
        If RunningClass.VeniceTCPClient IsNot Nothing Then
          RunningClass.VeniceTCPClient.CloseDown = True
        End If
      End Try
    End While
  End Sub

  ''' <summary>
  ''' Initializes a new instance of the <see cref="VeniceMain"/> class.
  ''' </summary>
  ''' <param name="CmdArgs">The CMD args.</param>
  Public Sub New(ByVal CmdArgs() As String)
    Me.New()

    Try
      If (Not (Splash Is Nothing)) Then
        SplashTime = Now
        Splash.Show()
      End If
    Catch ex As Exception
    End Try

    ' Get Computername

    Dim ComputerName As String
    Try
      ComputerName = Environment.GetEnvironmentVariable("COMPUTERNAME")
    Catch ex As Exception
      ComputerName = ""
    End Try

    ' Startup Parameters
    '
    ' MessageServer=<ServerName>

    _MessageServerName = ""
    _SQLTrustedConnection = True
    _SQLUserName = ""
    _SQLPassword = ""

    MessageServerAddress = IPAddress.None

    If Not (CmdArgs Is Nothing) Then
      If (CmdArgs.Length > 0) Then
        Dim ParameterString As String
        Dim ParameterName As String

        For Each ParameterString In CmdArgs
          If InStr(ParameterString, "=") > 0 Then
            ParameterName = ParameterString.Substring(0, InStr(ParameterString, "=") - 1)
            ParameterString = ParameterString.Substring(InStr(ParameterString, "="), (ParameterString.Length - InStr(ParameterString, "=")))

            Select Case ParameterName.ToUpper

              Case "MESSAGESERVER"
                _MessageServerName = ParameterString.Trim
                Try
                  MessageServerAddress = NetworkFunctions.GetServerAddress(_MessageServerName)
                Catch ex As Exception
                  _MessageServerName = ""
                  MessageServerAddress = IPAddress.None
                End Try

              Case "SQLSERVER"
                If ParameterString.Trim.Length > 0 Then

                  If (ParameterString.Contains("\")) Then
										Dim Parts As String() = ParameterString.Split(New Char() {"\"c}, StringSplitOptions.RemoveEmptyEntries)

                    If (Parts.Length > 0) Then SQLServerName = Parts(0)
                    If (Parts.Length > 1) Then SQLServerInstance = Parts(1)
                  Else
                    SQLServerName = ParameterString.Trim
                    SQLServerInstance = ""
                  End If
                End If

              Case "SQLINSTANCE"
                If ParameterString.Trim.Length > 0 Then
                  SQLServerInstance = ParameterString.Trim
                End If

              Case "SQLUSERNAME"
                If ParameterString.Trim.Length > 0 Then
                  _SQLUserName = ParameterString.Trim
                  _SQLTrustedConnection = False
                End If

              Case "SQLPASSWORD"
                If ParameterString.Trim.Length > 0 Then
                  _SQLPassword = ParameterString.Trim
                End If

              Case "USEREPORTWORKERTHREADS"
                If ParameterString.Trim.Length > 0 Then
                  Try
                    _UseReportWorkerThreads = CBool(ParameterString.Trim)
                  Catch ex As Exception
                    _UseReportWorkerThreads = False
                  End Try
                End If

              Case "MDIFORM"
                If ParameterString.Trim.Length > 0 Then
                  If ParameterString.Trim.ToUpper = "TRUE" Then
                    Me.Menu_File_MDI.Checked = True
                    Me.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
                    Me.MaximizeBox = True
                    Me.IsMdiContainer = True
                  ElseIf IsNumeric(ParameterString.Trim) Then
                    If CInt(ParameterString.Trim) <> 0 Then
                      Me.Menu_File_MDI.Checked = False
                      Me.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
                      Me.MaximizeBox = True
                      Me.IsMdiContainer = True
                    End If
                  End If
                End If

              Case "FORMCACHE"
                If ParameterString.Trim.Length > 0 Then
                  Dim Flag As Boolean

                  Flag = False
                  Try
                    Flag = CBool(ParameterString.Trim)
                  Catch ex As Exception
                    Flag = True
                  End Try

                  If (Flag = False) Then
                    _EntryForm_CacheCount = 0
                    _ReportForm_CacheCount = 0
                    _SingleCache_CacheCount = 0
                    _NoCache_CacheCount = 0
                  Else
                    _EntryForm_CacheCount = STANDARD_EntryForm_CACHE_COUNT
                    _ReportForm_CacheCount = STANDARD_ReportForm_CACHE_COUNT
                    _SingleCache_CacheCount = STANDARD_SingleForm_CACHE_COUNT
                    _NoCache_CacheCount = 0
                  End If
                End If

            End Select
          End If
        Next
      End If
    End If

    ' Check Registry if IPAddress = None

    If (IPAddress.None.Equals(MessageServerAddress)) Then
      ' Try to resolve an address from the Registry

      _MessageServerName = CType(Get_RegistryItem(Registry.CurrentUser, REGISTRY_BASE & "\MessageServer", "ServerName", ""), String)

      If (_MessageServerName = "") Then
        _MessageServerName = CType(Get_RegistryItem(Registry.LocalMachine, REGISTRY_BASE & "\MessageServer", "ServerName", System.Environment.MachineName), String)
      End If

      Try
        If (_MessageServerName.Length <= 0) Then
          _MessageServerName = System.Environment.MachineName
        End If

        MessageServerAddress = NetworkFunctions.GetServerAddress(_MessageServerName)

      Catch ex As Exception
        MessageServerAddress = IPAddress.None
        _MessageServerName = ""
      End Try
    End If

    Try
      UpdateEventControl = New UpdateEventControlClass(Me)
      UpdateEventControl.Visible = False
      Me.Controls.Add(Me.UpdateEventControl)

      '
      ' Initiate TCP Client

      VeniceTCPClient = New TCPServer_Common.GenericTCPClient(Me, _MessageServerName, MessageServerAddress, SQLServerInstance, FCP_Application.Venice, New FCP_Application() {FCP_Application.Venice, FCP_Application.Renaissance, FCP_Application.VeniceDataServer})

      ' Add TCP Client Message Handler
      AddHandler VeniceTCPClient.MessageReceivedEvent, AddressOf TCPMessageReceived
      AddHandler VeniceTCPClient.TCPErrorEvent, AddressOf TCPErrorReceived

      'VeniceTCPClient.Start()
    Catch ex As Exception
    End Try

    ' Process Venice Update requests
    ' Commented out as the code has been altered to call the VeniceMain AutoUpdate() procedure explicitly, to ensure that it happens first.
    ' AddHandler Me.VeniceAutoUpdate, AddressOf Me.AutoUpdate

    ' Simple Menu handlers

    ' Static
    AddHandler Menu_Static_Counterparty.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_Static_Currency.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_Static_Funds.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_Static_SubFund.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_Static_FundType.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_Static_Instrument.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_Static_SelectInstrument.Click, AddressOf Generic_FormMenu_Click
    'AddHandler Menu_Static_InstrumentParents.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_Static_InstrumentType.Click, AddressOf Generic_FormMenu_Click
    'AddHandler Menu_Static_InvestorGroups.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_Static_People.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_Static_TransactionTypes.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_Static_Brokers.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_Static_BrokersAccounts.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_Static_SubFundHeirarchy.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_Static_Fees.Click, AddressOf Generic_FormMenu_Click

    ' Add / Edit

    AddHandler Menu_Trans_SelectTransactions.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_Trans_SelectTransactionsList.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_Trans_EditTransactions.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_Trans_ImportTransactions.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_Trans_ApproveTransactions.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_Trans_UpdateTransactions.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_Trans_frmManageSubFunds.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_Trans_CashLadder.Click, AddressOf Generic_FormMenu_Click

    ' Prices

    AddHandler Menu_Prices_FXRates.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_Prices_FXGrid.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_Prices_Prices.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_Prices_PriceGrid.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_Prices_GreeksGrid.Click, AddressOf Generic_FormMenu_Click

    ' Risk

    AddHandler Menu_Risk_EditLimits.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_Risk_RiskCharacteristics.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_Risk_Characteristics.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_Risk_ExposureReport.Click, AddressOf Generic_FormMenu_Click

    ' Attributions

    'AddHandler Menu_Attributions.Click, AddressOf Generic_FormMenu_Click
    'AddHandler Menu_Attributions_Calculate.Click, AddressOf Generic_FormMenu_Click
    'AddHandler Menu_Attributions_Report.Click, AddressOf Generic_FormMenu_Click

    ' KnowledgeDate

    AddHandler Menu_KD_SetKD.Click, AddressOf Generic_FormMenu_Click
    'AddHandler Menu_KD_SetBookmark.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_KD_RefreshKD.Click, AddressOf Generic_FormMenu_Click

    ' Month End

    'AddHandler Menu_MonthEnd_SetMilestone.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_MonthEnd_Reports.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_MonthEnd_SelectFuturesNotional.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_EOD_ReconciliationForm.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_EOD_LoadPackVL.Click, AddressOf Generic_FormMenu_Click

    ' Reports

    AddHandler Menu_Rpt_FundRpts.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_Rpt_AUM.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_Rpt_FundExpense.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_Rpt_Valuation.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_Rpt_FundPnL.Click, AddressOf Generic_FormMenu_Click
    'AddHandler Menu_Rpt_Attribution.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_Rpt_CashReport.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_Rpt_PriceReports.Click, AddressOf Generic_FormMenu_Click
    'AddHandler Menu_Rpt_UnitHolder.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_Reports_frmNotionalManagementReports.Click, AddressOf Generic_FormMenu_Click
    AddHandler Menu_Report_TransactionStatus.Click, AddressOf Generic_FormMenu_Click

    ' User Menu

    AddHandler Menu_System_UserPermissions.Click, AddressOf Generic_FormMenu_Click
    'AddHandler Menu_System_ReviewCC.Click, AddressOf Generic_FormMenu_Click
    'AddHandler Menu_System_SelectChangeControls.Click, AddressOf Generic_FormMenu_Click

    ' About

    AddHandler Menu_About.Click, AddressOf Generic_FormMenu_Click

    If (_PertracData IsNot Nothing) AndAlso (_StatFunctions IsNot Nothing) Then
      _PertracData.StatFunctionsObject = _StatFunctions
    End If

  End Sub

  ''' <summary>
  ''' Handles the Load event of the VeniceMain control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub VeniceMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    ' *******************************************************************************
    ' Initialise Venice
    ' 
    ' *******************************************************************************

    Dim myConnection As SqlConnection

    Dim ComputerName As String
    Dim ConnectString As String
    'Dim ConnectionCount As Integer
    'Dim CurrentConnection As Integer
    'Dim ItemName As String

    ' Add the Main form to the Venice Forms Collection.
    VeniceForms.Add(New RenaissanceGlobals.RenaissanceFormHandle(Me, VeniceFormID.frmVeniceMain, True))

    Try
      If (Not (Splash Is Nothing)) Then
        Splash.Focus()
        Splash.TopMost = True
      End If
    Catch ex As Exception
    End Try

    ' **********************************************************************
    ' Initialise Databse Connection.
    ' 
    ' 1) Construct a reasonable default, then query the registry for a saved value.
    ' 2) Create a managed connection object 'cnnVenice'
    '
    ' **********************************************************************

    Try
      Me.Visible = True

      ' Get initial connection string(s)
      ' Default value will be determined by what machine this is running on.
      ' Is it at home, my development machine or AN Other ?

      ' Establish default connection string.
      SetToolStripText(VeniceStatusBar, "Initailising, SQL Connection.")
      Me.VeniceStatusStrip.Refresh()

      If SQLServerName.StartsWith("<") Then
        ComputerName = Environment.GetEnvironmentVariable("COMPUTERNAME")

        If ComputerName Is Nothing Then
          ComputerName = "<Unknown>"
        End If

        If (SQLServerInstance.Length > 0) Then
          ComputerName &= "\" & SQLServerInstance
        End If

        '_SQLTrustedConnection = True
        '_SQLUserName = ""
        '_SQLPassword = ""

        If ComputerName.StartsWith("EXXP") OrElse ComputerName.StartsWith("FCPXP") Then
          If ComputerName.EndsWith("XP9731") Then
            ConnectString = "SERVER=" & ComputerName & ";DATABASE=InvestMaster_Test1;Trusted_Connection=Yes;Application Name=" & Application.ProductName
            SQLServerName = ComputerName
          Else
            ConnectString = "SERVER=EXMSDB53;DATABASE=Renaissance;Trusted_Connection=Yes;Application Name=" & Application.ProductName
            SQLServerName = "EXMSDB53"
          End If
        Else
          ConnectString = "SERVER=" & ComputerName & ";DATABASE=InvestMaster_Test1;Trusted_Connection=Yes;Application Name=" & Application.ProductName
        End If
      Else
        If (SQLTrustedConnection) Then
          If (SQLServerInstance.Length > 0) Then
            ConnectString = "SERVER=" & SQLServerName & "\" & SQLServerInstance & ";DATABASE=Renaissance;Trusted_Connection=Yes;Application Name=" & Application.ProductName
          Else
            ConnectString = "SERVER=" & SQLServerName & ";DATABASE=Renaissance;Trusted_Connection=Yes;Application Name=" & Application.ProductName
          End If
        Else
          If (SQLServerInstance.Length > 0) Then
            ConnectString = "SERVER=" & SQLServerName & "\" & SQLServerInstance & ";DATABASE=Renaissance;Trusted_Connection=No;User ID=" & SQLUserName & ";Password=" & SQLPassword & ";Application Name=" & Application.ProductName
          Else
            ConnectString = "SERVER=" & SQLServerName & ";DATABASE=Renaissance;Trusted_Connection=No;User ID=" & SQLUserName & ";Password=" & SQLPassword & ";Application Name=" & Application.ProductName
          End If

        End If
      End If

      'ConnectString &= ";Pooling=true;Min Pool Size=1;Max Pool Size=100;Connection Timeout=5"
      ConnectString &= ";Pooling=false;Connection Timeout=5"

      '' Retrieve existing connection strings from the registry.

      'ConnectionCount = CType(Get_RegistryItem(REGISTRY_BASE & "\Connections", "ConnectionCount", (-1)), Integer)
      'If ConnectionCount > 0 Then
      '  CurrentConnection = CType(Get_RegistryItem(REGISTRY_BASE & "\Connections", "CurrentConnection", (-1)), Integer)

      '  If (CurrentConnection < ConnectionCount) And (CurrentConnection >= 0) Then
      '    ItemName = "C" & CurrentConnection.ToString

      '    ConnectString = CType(Get_RegistryItem(REGISTRY_BASE & "\Connections", ItemName, ConnectString), String)
      '  End If

      'End If

      ' create connection object in the DataHandler object.

      myConnection = MainDataHandler.Add_Connection(VENICE_CONNECTION, ConnectString)
      _SQLConnectionString = ConnectString
      '_SQLAsynchConnectString = ConnectString & ";async=true"

      If (myConnection Is Nothing) Then
        If MainDataHandler.ErrorMessage.Length > 0 Then
          Me.LogError(Me.Name, LOG_LEVELS.Error, MainDataHandler.ErrorMessage, "Failed to open Connection.", "", True)
          Exit Sub
        Else
          Me.LogError(Me.Name, LOG_LEVELS.Error, "", "Failed to open Connection.", "", True)
          Exit Sub
        End If
      End If

      ' Ensure that it is opened.

      Try
        If myConnection.State = ConnectionState.Closed Then
          myConnection.Open()
        End If
      Catch ex As Exception
        Me.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Failed to open Connection.", ex.StackTrace, True)
        Exit Sub
      End Try

    Catch ex As Exception
      Me.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Failed to open Database Connection.", ex.StackTrace, True)
      Exit Sub
    Finally
      SetToolStripText(VeniceStatusBar, "")
    End Try

    ' **********************************************************************
    ' Start Message Server connection.
    ' Done after the DB is opened so we can report errors.
    ' **********************************************************************

    Try
      VeniceTCPClient.Start()
    Catch ex As Exception
    End Try

    ' **********************************************************************

    ' FormUpdateDataTimer
    ' FormUpdateObjects

    If (FormUpdateDataTimer IsNot Nothing) Then
      Try
        FormUpdateDataTimer.Stop()
      Catch ex As Exception
      Finally
        FormUpdateDataTimer = Nothing
      End Try
    End If
    Try
      FormUpdateDataTimer = New System.Windows.Forms.Timer
      FormUpdateDataTimer.Interval = 200
      AddHandler FormUpdateDataTimer.Tick, AddressOf FormUpdateDataTimer_Tick
      FormUpdateDataTimer.Start()
    Catch ex As Exception
    End Try

    '

    If (BackgroundTableLoaderProcess Is Nothing) Then
      BackgroundTableLoaderProcess = New BackgroundTableLoaderClass(Me)
      BackgroundTableLoaderProcess.Start()
    End If

    ' **********************************************************************

    Main_Knowledgedate = KNOWLEDGEDATE_NOW

    SetToolStripText(VeniceStatusBar, "")
    Menu_UseMultithreadedReporting.Checked = _UseReportWorkerThreads
    Menu_File_EnableReportFadeIn.Checked = True

    Call SetMenuBarEnabled()

    ' Set Tool bars

    SetMenuCommand("", VeniceMenu.Items)
    SetCustomToolbar()
    AddToolbarsToDock()

    ' Title

    Me.Text = Me.Text & " : " & SQLServerName & "\" & SQLServerInstance

    ' Preload a couple of tables

    Call RefreshMasternameDictionary()
    ' Call RefreshLocationsDictionary()

    Try

      'Splash.Label_Message.Text = CInt((Now - SplashTime).TotalSeconds).ToString("###0") & " preloading Transactions..."
      Call Load_Table(RenaissanceStandardDatasets.tblTransaction)

      If (Now - SplashTime).TotalSeconds < 12.0# Then
        'Splash.Label_Message.Text = CInt((Now - SplashTime).TotalSeconds).ToString("###0") & " preloading Select-Transactions..."
        Call Load_Table(RenaissanceStandardDatasets.tblSelectTransaction)
      End If

      If (Now - SplashTime).TotalSeconds < 12.0# Then
        'Splash.Label_Message.Text = CInt((Now - SplashTime).TotalSeconds).ToString("###0") & " preloading additional tables..."
        Call Load_Table(RenaissanceStandardDatasets.tblFund)
        Call Load_Table(RenaissanceStandardDatasets.tblInstrument)
      End If

    Catch ex As Exception
    End Try

    ' force free memory
    GC.Collect()


    If (Not (Splash Is Nothing)) Then
      Try
        If (Now - SplashTime).TotalSeconds < SPLASH_DISPLAY Then
          Splash.StartTimer(SplashTime.AddSeconds(SPLASH_DISPLAY))
          Splash.Hide()
          Splash.ShowDialog()
        End If

        Splash.Hide()
        Splash.Close()
      Catch ex As Exception
      Finally
        If (Not (Splash Is Nothing)) Then
          If Splash.Visible Then Splash.Hide()
        End If
        Splash = Nothing
      End Try
    End If

  End Sub

  ''' <summary>
  ''' Handles the Closing event of the VeniceMain control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
  Private Sub VeniceMain_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    ' **********************************************************************
    ' Close the application  neatly.
    ' **********************************************************************

    Dim TimeCounter As Integer

    ' Tidy Up

    ' **********************************************************************

    Try
      Me.IsClosing = True

      Application.DoEvents()

      If (FormUpdateDataTimer IsNot Nothing) Then
        Try
          FormUpdateDataTimer.Stop()
        Catch ex As Exception
        Finally
          FormUpdateDataTimer = Nothing
        End Try
      End If

    Catch ex As Exception
    End Try

    ' **********************************************************************

    Try
      If (BackgroundTableLoaderProcess IsNot Nothing) Then
        BackgroundTableLoaderProcess.CloseDown = True

        TimeCounter = 50
        While (TimeCounter > 0) And (BackgroundTableLoaderProcess.IsCompleted = False)
          Threading.Thread.Sleep(100)
          TimeCounter -= 1
        End While
      End If
    Catch ex As Exception
    Finally
      If (BackgroundTableLoaderProcess IsNot Nothing) AndAlso (BackgroundTableLoaderProcess.IsCompleted = False) Then
        BackgroundTableLoaderProcess.Stop()
      End If
    End Try

    ' **********************************************************************

    Try
      SaveToolbarStatus()

    Catch ex As Exception
    End Try

    ' **********************************************************************
    ' ShutDown VeniceTCPClient
    ' **********************************************************************

    Try

      VeniceTCPClient.CloseDown = True
      TimeCounter = 40
      While (TimeCounter > 0) And (VeniceTCPClient.IsCompleted = False)
        Threading.Thread.Sleep(50)
        TimeCounter -= 1
      End While
    Catch ex As Exception
    Finally
      If (VeniceTCPClient IsNot Nothing) AndAlso (VeniceTCPClient.IsCompleted = False) Then
        VeniceTCPClient.Stop()
      End If
    End Try

    ' **********************************************************************
    ' Clear Data Handler
    ' **********************************************************************

    Try
      SyncLock MainDataHandler

        If (MainDataHandler IsNot Nothing) Then
          If (MainDataHandler.Adaptors.Count > 0) Then
            MainDataHandler.Adaptors.Clear()
          End If

          If (MainDataHandler.Datasets.Count > 0) Then
            MainDataHandler.Datasets.Clear()
          End If

          If (MainDataHandler.DBConnections.Count > 0) Then
            Dim ThisConnection As SqlConnection

            For Each ThisConnection In MainDataHandler.DBConnections.Values
              If (ThisConnection IsNot Nothing) Then
                If (ThisConnection.State And ConnectionState.Open) Then
                  ThisConnection.Close()
                End If
              End If
            Next

            MainDataHandler.DBConnections.Clear()
          End If
        End If

      End SyncLock

    Catch ex As Exception
    End Try

  End Sub

#End Region

#Region " Display Report "

  ''' <summary>
  ''' Delegate SetToolStripTextDelegate
  ''' </summary>
  ''' <param name="thisStripLabel">The this strip label.</param>
  ''' <param name="StripText">The strip text.</param>
  Private Delegate Sub SetToolStripTextDelegate(ByVal thisStripLabel As ToolStripLabel, ByVal StripText As String)

  ''' <summary>
  ''' Sets the tool strip text.
  ''' </summary>
  ''' <param name="thisStripLabel">The this strip label.</param>
  ''' <param name="StripText">The strip text.</param>
  Friend Sub SetToolStripText(ByVal thisStripLabel As ToolStripLabel, ByVal StripText As String)
    ' ********************************************************************
    ' InvokeRequired compares the thread ID of the
    ' calling thread to the thread ID of the creating thread.
    ' If these threads are different, it returns true.
    ' ********************************************************************

    Try

      If (thisStripLabel Is Nothing) OrElse (thisStripLabel.IsDisposed) OrElse (StripText Is Nothing) OrElse (Me.IsClosing) Then
        Exit Sub
      End If

      If thisStripLabel.Owner.InvokeRequired Then
        Dim d As New SetToolStripTextDelegate(AddressOf SetToolStripText)
        thisStripLabel.Owner.Invoke(d, New Object() {thisStripLabel, StripText})
      Else
        thisStripLabel.Text = StripText
        thisStripLabel.Owner.Refresh()
      End If

    Catch ex As Exception
      ' Fail silently, It's only a tooltip text after all...
    End Try

  End Sub

  ''' <summary>
  ''' Delegate SetControlTextDelegate
  ''' </summary>
  ''' <param name="thisControl">The this control.</param>
  ''' <param name="StripText">The strip text.</param>
  Private Delegate Sub SetControlTextDelegate(ByVal thisControl As Control, ByVal StripText As String)

  ''' <summary>
  ''' Sets the control text.
  ''' </summary>
  ''' <param name="thisControl">The this control.</param>
  ''' <param name="StripText">The strip text.</param>
  Friend Sub SetControlText(ByVal thisControl As Control, ByVal StripText As String)

    If (thisControl Is Nothing) OrElse (thisControl.IsDisposed) Then
      Exit Sub
    End If

    If thisControl.InvokeRequired Then
      Dim d As New SetControlTextDelegate(AddressOf SetControlText)
      thisControl.Invoke(d, New Object() {thisControl, StripText})
    Else
      thisControl.Text = StripText
    End If

  End Sub

  ''' <summary>
  ''' Delegate IncrementProgressBarDelegate
  ''' </summary>
  ''' <param name="thisBar">The this bar.</param>
  Private Delegate Sub IncrementProgressBarDelegate(ByVal thisBar As ToolStripProgressBar)


  ''' <summary>
  ''' Increments the status bar.
  ''' </summary>
  ''' <param name="thisProgressbar">The this progressbar.</param>
  Friend Sub IncrementStatusBar(ByVal thisProgressbar As ToolStripProgressBar)
    Static LastUpdateTime As Date = #1/1/1900#
    Dim LastObjectUpdateTime As Date

    If (thisProgressbar Is Nothing) OrElse (thisProgressbar.IsDisposed) Then
      Exit Sub
    End If

    If thisProgressbar.Owner.InvokeRequired Then
      ' InvokeRequired required compares the thread ID of the
      ' calling thread to the thread ID of the creating thread.
      ' If these threads are different, it returns true.
      Dim d As New IncrementProgressBarDelegate(AddressOf IncrementStatusBar)
      thisProgressbar.Owner.Invoke(d, New Object() {thisProgressbar})
      Exit Sub
    End If

    Try
      ' Make sure the bar is Visible.
      If (thisProgressbar.Visible = False) Then
        thisProgressbar.Visible = True
        thisProgressbar.Owner.PerformLayout()
      End If

      ' Impose a minimum time between status bar updates.
      If (thisProgressbar.Tag IsNot Nothing) AndAlso (TypeOf thisProgressbar.Tag Is Date) Then
        LastObjectUpdateTime = CType(thisProgressbar.Tag, Date)
      Else
        LastObjectUpdateTime = LastUpdateTime
      End If

      If (Now() - LastObjectUpdateTime).TotalSeconds >= 0.25 Then
        If (thisProgressbar.Tag IsNot Nothing) AndAlso (Not (TypeOf thisProgressbar.Tag Is Date)) Then
          LastUpdateTime = Now()
        Else
          thisProgressbar.Tag = Now()
        End If

        ' Progress the Status Bar....
        If (thisProgressbar.Value < thisProgressbar.Maximum) Then
          If (thisProgressbar.Value + thisProgressbar.Step) <= thisProgressbar.Maximum Then
            thisProgressbar.Value += thisProgressbar.Step
          Else
            thisProgressbar.Value = thisProgressbar.Maximum
          End If
        Else
          thisProgressbar.Value = thisProgressbar.Minimum
        End If
      End If
    Catch ex As Exception
    End Try

  End Sub


  ' This is the method to run when the timer is raised.
  ''' <summary>
  ''' Increments the status bar timer event.
  ''' </summary>
  ''' <param name="myObject">My object.</param>
  ''' <param name="myEventArgs">The <see cref="EventArgs"/> instance containing the event data.</param>
  Friend Sub IncrementStatusBarTimerEvent(ByVal myObject As Object, _
  ByVal myEventArgs As EventArgs)
    If (myObject Is Nothing) OrElse (Not (TypeOf myObject Is Windows.Forms.Timer)) Then
      Exit Sub
    End If

    Dim mytimer As Windows.Forms.Timer = CType(myObject, Windows.Forms.Timer)

    If (mytimer.Tag Is Nothing) OrElse (Not (TypeOf mytimer.Tag Is ToolStripProgressBar)) Then
      Exit Sub
    End If

    IncrementStatusBar(CType(mytimer.Tag, ToolStripProgressBar))
  End Sub

  ''' <summary>
  ''' Delegate GetComboSelectedIndexDelegate
  ''' </summary>
  ''' <param name="thiscombo">The thiscombo.</param>
  ''' <returns>System.Int32.</returns>
  Private Delegate Function GetComboSelectedIndexDelegate(ByVal thiscombo As Object) As Integer

  ''' <summary>
  ''' Gets the index of the combo selected.
  ''' </summary>
  ''' <param name="thiscombo">The thiscombo.</param>
  ''' <returns>System.Int32.</returns>
  Friend Function GetComboSelectedIndex(ByVal thiscombo As Object) As Integer

    If (thiscombo Is Nothing) OrElse (thiscombo.IsDisposed) Then
      Return -1
      Exit Function
    End If

    If (TypeOf thiscombo Is Windows.Forms.ComboBox) Then

      If CType(thiscombo, ComboBox).InvokeRequired Then
        Dim d As New GetComboSelectedIndexDelegate(AddressOf GetComboSelectedIndex)
        Return CType(thiscombo, ComboBox).Invoke(d, New Object() {thiscombo})
      Else
        Return CType(thiscombo, ComboBox).SelectedIndex
      End If

    ElseIf (TypeOf thiscombo Is Telerik.WinControls.UI.RadComboBox) Then

      If CType(thiscombo, Telerik.WinControls.UI.RadComboBox).InvokeRequired Then
        Dim d As New GetComboSelectedIndexDelegate(AddressOf GetComboSelectedIndex)
        Return CType(thiscombo, Telerik.WinControls.UI.RadComboBox).Invoke(d, New Object() {thiscombo})
      Else
        Return CType(thiscombo, Telerik.WinControls.UI.RadComboBox).SelectedIndex
      End If

    End If

  End Function

  ''' <summary>
  ''' Delegate GetComboSelectedItemDelegate
  ''' </summary>
  ''' <param name="thiscombo">The thiscombo.</param>
  ''' <returns>System.Object.</returns>
  Private Delegate Function GetComboSelectedItemDelegate(ByVal thiscombo As ComboBox) As Object

  ''' <summary>
  ''' Gets the combo selected item.
  ''' </summary>
  ''' <param name="thiscombo">The thiscombo.</param>
  ''' <returns>System.Object.</returns>
  Friend Function GetComboSelectedItem(ByVal thiscombo As ComboBox) As Object

    If (thiscombo Is Nothing) OrElse (thiscombo.IsDisposed) Then
      Return Nothing
      Exit Function
    End If

    If thiscombo.InvokeRequired Then
      Dim d As New GetComboSelectedItemDelegate(AddressOf GetComboSelectedItem)
      Return thiscombo.Invoke(d, New Object() {thiscombo})
    Else
      Return thiscombo.SelectedItem
    End If
  End Function

  ''' <summary>
  ''' Delegate GetComboSelectedValueDelegate
  ''' </summary>
  ''' <param name="thiscombo">The thiscombo.</param>
  ''' <returns>System.Object.</returns>
  Private Delegate Function GetComboSelectedValueDelegate(ByVal thiscombo As Object) As Object

  ''' <summary>
  ''' Gets the combo selected value.
  ''' </summary>
  ''' <param name="thiscombo">The thiscombo.</param>
  ''' <returns>System.Object.</returns>
  Friend Function GetComboSelectedValue(ByVal thiscombo As Object) As Object

    If (thiscombo Is Nothing) OrElse (thiscombo.IsDisposed) Then
      Return Nothing
      Exit Function
    End If

    If (TypeOf thiscombo Is Windows.Forms.ComboBox) Then

      If CType(thiscombo, ComboBox).InvokeRequired Then
        Dim d As New GetComboSelectedValueDelegate(AddressOf GetComboSelectedValue)
        Return CType(thiscombo, ComboBox).Invoke(d, New Object() {thiscombo})
      Else
        Return CType(thiscombo, ComboBox).SelectedValue
      End If

    ElseIf (TypeOf thiscombo Is Telerik.WinControls.UI.RadComboBox) Then

      If CType(thiscombo, Telerik.WinControls.UI.RadComboBox).InvokeRequired Then
        Dim d As New GetComboSelectedValueDelegate(AddressOf GetComboSelectedValue)
        Return CType(thiscombo, Telerik.WinControls.UI.RadComboBox).Invoke(d, New Object() {thiscombo})
      Else
        Return CType(thiscombo, Telerik.WinControls.UI.RadComboBox).SelectedValue
      End If
    End If

    Return Nothing
  End Function

  ''' <summary>
  ''' Delegate GetComboSelectedTextDelegate
  ''' </summary>
  ''' <param name="thiscombo">The thiscombo.</param>
  ''' <returns>System.String.</returns>
  Private Delegate Function GetComboSelectedTextDelegate(ByVal thiscombo As ComboBox) As String

  ''' <summary>
  ''' Gets the combo selected text.
  ''' </summary>
  ''' <param name="thiscombo">The thiscombo.</param>
  ''' <returns>System.String.</returns>
  Friend Function GetComboSelectedText(ByVal thiscombo As ComboBox) As String
    If (thiscombo Is Nothing) OrElse (thiscombo.IsDisposed) Then
      Return ""
      Exit Function
    End If

    If thiscombo.InvokeRequired Then
      Dim d As New GetComboSelectedTextDelegate(AddressOf GetComboSelectedText)
      Return thiscombo.Invoke(d, New Object() {thiscombo})
    Else
      Return thiscombo.SelectedText
    End If
  End Function

  ''' <summary>
  ''' Delegate GetControlTextDelegate
  ''' </summary>
  ''' <param name="thisControl">The this control.</param>
  ''' <returns>System.String.</returns>
  Private Delegate Function GetControlTextDelegate(ByVal thisControl As Control) As String

  ''' <summary>
  ''' Gets the control text.
  ''' </summary>
  ''' <param name="thisControl">The this control.</param>
  ''' <returns>System.String.</returns>
  Friend Function GetControlText(ByVal thisControl As Control) As String

    If (thisControl Is Nothing) OrElse (thisControl.IsDisposed) Then
      Return ""
      Exit Function
    End If

    If thisControl.InvokeRequired Then
      Dim d As New GetControlTextDelegate(AddressOf GetControlText)
      Return thisControl.Invoke(d, New Object() {thisControl})
    Else
      Return thisControl.Text
    End If
  End Function

  ''' <summary>
  ''' Delegate GetCheckBoxCheckedDelegate
  ''' </summary>
  ''' <param name="thisCheckBox">The this check box.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Delegate Function GetCheckBoxCheckedDelegate(ByVal thisCheckBox As CheckBox) As Boolean

  ''' <summary>
  ''' Gets the check box checked.
  ''' </summary>
  ''' <param name="thisCheckBox">The this check box.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Friend Function GetCheckBoxChecked(ByVal thisCheckBox As CheckBox) As Boolean

    If (thisCheckBox Is Nothing) OrElse (thisCheckBox.IsDisposed) Then
      Return False
      Exit Function
    End If

    If thisCheckBox.InvokeRequired Then
      Dim d As New GetCheckBoxCheckedDelegate(AddressOf GetCheckBoxChecked)
      Return thisCheckBox.Invoke(d, New Object() {thisCheckBox})
    Else
      Return thisCheckBox.Checked
    End If
  End Function

  ''' <summary>
  ''' Delegate GetRadioCheckedDelegate
  ''' </summary>
  ''' <param name="thisRadio">The this radio.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Delegate Function GetRadioCheckedDelegate(ByVal thisRadio As RadioButton) As Boolean

  ''' <summary>
  ''' Gets the radio checked.
  ''' </summary>
  ''' <param name="thisRadio">The this radio.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Friend Function GetRadioChecked(ByVal thisRadio As RadioButton) As Boolean

    If (thisRadio Is Nothing) OrElse (thisRadio.IsDisposed) Then
      Return False
      Exit Function
    End If

    If thisRadio.InvokeRequired Then
      Dim d As New GetRadioCheckedDelegate(AddressOf GetRadioChecked)
      Return thisRadio.Invoke(d, New Object() {thisRadio})
    Else
      Return thisRadio.Checked
    End If
  End Function

  ''' <summary>
  ''' Delegate GetMenuCheckedDelegate
  ''' </summary>
  ''' <param name="thisMenu">The this menu.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Delegate Function GetMenuCheckedDelegate(ByVal thisMenu As ToolStripMenuItem) As Boolean

  ''' <summary>
  ''' Gets the menu checked.
  ''' </summary>
  ''' <param name="thisMenu">The this menu.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Friend Function GetMenuChecked(ByVal thisMenu As ToolStripMenuItem) As Boolean
    '****************************************************************************
    '
    '
    '****************************************************************************

    Try

      If (thisMenu Is Nothing) OrElse (thisMenu.IsDisposed) Then
        Return False
        Exit Function
      End If

      If Me.InvokeRequired Then
        Dim d As New GetMenuCheckedDelegate(AddressOf GetMenuChecked)
        Return Me.Invoke(d, New Object() {thisMenu})
      Else
        Return thisMenu.Checked
      End If

    Catch ex As Exception
    End Try

    Return False

  End Function

  ''' <summary>
  ''' Delegate GetListBoxSelectedItemsDelegate
  ''' </summary>
  ''' <param name="thisList">The this list.</param>
  ''' <returns>System.Object[][].</returns>
  Private Delegate Function GetListBoxSelectedItemsDelegate(ByVal thisList As ListBox) As Object()

  ''' <summary>
  ''' Gets the list box selected items.
  ''' </summary>
  ''' <param name="thisList">The this list.</param>
  ''' <returns>System.Object[][].</returns>
  Friend Function GetListBoxSelectedItems(ByVal thisList As ListBox) As Object()
    '****************************************************************************
    '
    '
    '****************************************************************************

    Dim ThisObjectCollection As ListBox.SelectedObjectCollection
    Dim RVal(-1) As Object

    If (thisList Is Nothing) OrElse (thisList.IsDisposed) Then
      Return RVal
      Exit Function
    End If

    Try

      If thisList.InvokeRequired Then
        Dim d As New GetListBoxSelectedItemsDelegate(AddressOf GetListBoxSelectedItems)
        Return thisList.Invoke(d, New Object() {thisList})
      Else
        ThisObjectCollection = thisList.SelectedItems
      End If

      If (ThisObjectCollection Is Nothing) OrElse (ThisObjectCollection.Count <= 0) Then
        Return RVal
      End If

      ReDim RVal(ThisObjectCollection.Count - 1)
      Dim ThisObject As Object
      Dim ObjectCounter As Integer
      ObjectCounter = 0

      For Each ThisObject In ThisObjectCollection
        RVal(ObjectCounter) = ThisObject
        ObjectCounter += 1
      Next

    Catch ex As Exception
    End Try

    Return RVal

  End Function

  ' GetListBoxItems
  ''' <summary>
  ''' Delegate GetListBoxItemsDelegate
  ''' </summary>
  ''' <param name="thisList">The this list.</param>
  ''' <returns>System.Object[][].</returns>
  Private Delegate Function GetListBoxItemsDelegate(ByVal thisList As ListBox) As Object()

  ''' <summary>
  ''' Gets the list box items.
  ''' </summary>
  ''' <param name="thisList">The this list.</param>
  ''' <returns>System.Object[][].</returns>
  Friend Function GetListBoxItems(ByVal thisList As ListBox) As Object()
    Dim ThisObjectCollection As ListBox.ObjectCollection
    Dim RVal(-1) As Object

    If (thisList Is Nothing) OrElse (thisList.IsDisposed) Then
      Return RVal
      Exit Function
    End If

    If thisList.InvokeRequired Then
      Dim d As New GetListBoxItemsDelegate(AddressOf GetListBoxItems)
      Return thisList.Invoke(d, New Object() {thisList})
    Else
      ThisObjectCollection = thisList.Items
    End If

    If (ThisObjectCollection Is Nothing) OrElse (ThisObjectCollection.Count <= 0) Then
      Return RVal
    End If

    Try
      ReDim RVal(ThisObjectCollection.Count - 1)
      Dim ThisObject As Object
      Dim ObjectCounter As Integer
      ObjectCounter = 0
      For Each ThisObject In ThisObjectCollection
        RVal(ObjectCounter) = ThisObject
        ObjectCounter += 1
      Next
    Catch ex As Exception
    End Try

    Return RVal

  End Function

  ''' <summary>
  ''' Delegate GetDatetimeValueDelegate
  ''' </summary>
  ''' <param name="thisCheckBox">The this check box.</param>
  ''' <returns>System.DateTime.</returns>
  Private Delegate Function GetDatetimeValueDelegate(ByVal thisCheckBox As DateTimePicker) As Date

  ''' <summary>
  ''' Gets the datetime value.
  ''' </summary>
  ''' <param name="thisDateTimePicker">The this date time picker.</param>
  ''' <returns>System.DateTime.</returns>
  Friend Function GetDatetimeValue(ByVal thisDateTimePicker As DateTimePicker) As Date
    If thisDateTimePicker.InvokeRequired Then
      Dim d As New GetDatetimeValueDelegate(AddressOf GetDatetimeValue)
      Return thisDateTimePicker.Invoke(d, New Object() {thisDateTimePicker})
    Else
      Return thisDateTimePicker.Value
    End If
  End Function

  ''' <summary>
  ''' Delegate GetGridRowCountDelegate
  ''' </summary>
  ''' <param name="thisGrid">The this grid.</param>
  ''' <returns>System.Int32.</returns>
  Private Delegate Function GetGridRowCountDelegate(ByVal thisGrid As C1.Win.C1FlexGrid.C1FlexGrid) As Integer

  ''' <summary>
  ''' Gets the grid row count.
  ''' </summary>
  ''' <param name="thisGrid">The this grid.</param>
  ''' <returns>System.Int32.</returns>
  Friend Function GetGridRowCount(ByVal thisGrid As C1.Win.C1FlexGrid.C1FlexGrid) As Integer
    If thisGrid.InvokeRequired Then
      Dim d As New GetGridRowCountDelegate(AddressOf GetGridRowCount)
      Return thisGrid.Invoke(d, New Object() {thisGrid})
    Else
      Return thisGrid.Rows.Count
    End If
  End Function


  ''' <summary>
  ''' Delegate GetNumericUpDownValueDelegate
  ''' </summary>
  ''' <param name="thisNumeric">The this numeric.</param>
  ''' <returns>System.Int32.</returns>
  Private Delegate Function GetNumericUpDownValueDelegate(ByVal thisNumeric As NumericUpDown) As Integer

  ''' <summary>
  ''' Gets the numeric up down value.
  ''' </summary>
  ''' <param name="thisNumeric">The this numeric.</param>
  ''' <returns>System.Int32.</returns>
  Friend Function GetNumericUpDownValue(ByVal thisNumeric As NumericUpDown) As Integer
    If thisNumeric.InvokeRequired Then
      Dim d As New GetNumericUpDownValueDelegate(AddressOf GetNumericUpDownValue)
      Return thisNumeric.Invoke(d, New Object() {thisNumeric})
    Else
      Return thisNumeric.Value
    End If
  End Function

  ''' <summary>
  ''' Delegate GetNumericTextBoxValueDelegate
  ''' </summary>
  ''' <param name="thisNumeric">The this numeric.</param>
  ''' <returns>System.Double.</returns>
  Private Delegate Function GetNumericTextBoxValueDelegate(ByVal thisNumeric As NumericTextBox) As Double

  ''' <summary>
  ''' Gets the numeric text box value.
  ''' </summary>
  ''' <param name="thisNumeric">The this numeric.</param>
  ''' <returns>System.Double.</returns>
  Friend Function GetNumericTextBoxValue(ByVal thisNumeric As NumericTextBox) As Double
    If thisNumeric.InvokeRequired Then
      Dim d As New GetNumericTextBoxValueDelegate(AddressOf GetNumericTextBoxValue)
      Return thisNumeric.Invoke(d, New Object() {thisNumeric})
    Else
      Return thisNumeric.Value
    End If
  End Function


  ''' <summary>
  ''' Delegate GetGridItemByIndexDelegate
  ''' </summary>
  ''' <param name="thisGrid">The this grid.</param>
  ''' <param name="RowNumber">The row number.</param>
  ''' <param name="Colnumber">The colnumber.</param>
  ''' <returns>System.Object.</returns>
  Private Delegate Function GetGridItemByIndexDelegate(ByVal thisGrid As C1.Win.C1FlexGrid.C1FlexGrid, ByVal RowNumber As Integer, ByVal Colnumber As Integer) As Object

  ''' <summary>
  ''' Gets the index of the grid item by.
  ''' </summary>
  ''' <param name="thisGrid">The this grid.</param>
  ''' <param name="RowNumber">The row number.</param>
  ''' <param name="Colnumber">The colnumber.</param>
  ''' <returns>System.Object.</returns>
  Friend Function GetGridItemByIndex(ByVal thisGrid As C1.Win.C1FlexGrid.C1FlexGrid, ByVal RowNumber As Integer, ByVal Colnumber As Integer) As Object
    If thisGrid.InvokeRequired Then
      Dim d As New GetGridItemByIndexDelegate(AddressOf GetGridItemByIndex)
      Return thisGrid.Invoke(d, New Object() {thisGrid, RowNumber, Colnumber})
    Else
      Return thisGrid.Item(RowNumber, Colnumber)
    End If
  End Function

  ''' <summary>
  ''' Delegate GetGridItemByNameDelegate
  ''' </summary>
  ''' <param name="thisGrid">The this grid.</param>
  ''' <param name="RowNumber">The row number.</param>
  ''' <param name="ColName">Name of the col.</param>
  ''' <returns>System.Object.</returns>
  Private Delegate Function GetGridItemByNameDelegate(ByVal thisGrid As C1.Win.C1FlexGrid.C1FlexGrid, ByVal RowNumber As Integer, ByVal ColName As String) As Object

  ''' <summary>
  ''' Gets the name of the grid item by.
  ''' </summary>
  ''' <param name="thisGrid">The this grid.</param>
  ''' <param name="RowNumber">The row number.</param>
  ''' <param name="ColName">Name of the col.</param>
  ''' <returns>System.Object.</returns>
  Friend Function GetGridItemByName(ByVal thisGrid As C1.Win.C1FlexGrid.C1FlexGrid, ByVal RowNumber As Integer, ByVal ColName As String) As Object
    If thisGrid.InvokeRequired Then
      Dim d As New GetGridItemByNameDelegate(AddressOf GetGridItemByName)
      Return thisGrid.Invoke(d, New Object() {thisGrid, RowNumber, ColName})
    Else
      Return thisGrid.Item(RowNumber, ColName)
    End If
  End Function

  ''' <summary>
  ''' Delegate SetDocumentDelegate
  ''' </summary>
  ''' <param name="pPreview">The p preview.</param>
  ''' <param name="DocumentValue">The document value.</param>
  Private Delegate Sub SetDocumentDelegate(ByRef pPreview As C1.Win.C1Preview.C1PrintPreviewControl, ByRef DocumentValue As System.Drawing.Printing.PrintDocument)

  ''' <summary>
  ''' Sets the document.
  ''' </summary>
  ''' <param name="pPreview">The p preview.</param>
  ''' <param name="DocumentValue">The document value.</param>
  Public Sub SetDocument(ByRef pPreview As C1.Win.C1Preview.C1PrintPreviewControl, ByRef DocumentValue As System.Drawing.Printing.PrintDocument)
    If Me.IsHandleCreated = False Then
      Me.CreateControl()
    End If

    If Me.InvokeRequired Then
      Dim d As New SetDocumentDelegate(AddressOf SetDocument)
      Me.Invoke(d, New Object() {pPreview, DocumentValue})
    Else
      pPreview.Document = DocumentValue
    End If
  End Sub

  ''' <summary>
  ''' Delegate ActivateFormDelegate
  ''' </summary>
  ''' <param name="thisForm">The this form.</param>
  Private Delegate Sub ActivateFormDelegate(ByRef thisForm As Windows.Forms.Form)

  ''' <summary>
  ''' Activates the form.
  ''' </summary>
  ''' <param name="thisForm">The this form.</param>
  Public Sub ActivateForm(ByRef thisForm As Windows.Forms.Form)
    If Me.IsHandleCreated = False Then
      Me.CreateControl()
    End If

    If Me.InvokeRequired Then
      Dim d As New ActivateFormDelegate(AddressOf ActivateForm)
      Me.Invoke(d, New Object() {thisForm})
    Else
      thisForm.Activate()
    End If
  End Sub

  ''' <summary>
  ''' Resets the progress bar.
  ''' </summary>
  ''' <param name="pToolStrip">The p tool strip.</param>
  ''' <param name="pProgressBar">The p progress bar.</param>
  ''' <returns>ToolStripProgressBar.</returns>
  Friend Function ResetProgressBar(ByVal pToolStrip As ToolStrip, ByVal pProgressBar As ToolStripProgressBar) As ToolStripProgressBar
    ' *****************************************************************************
    ' For some reasont the ProgressBars were not becoming Invisible even when the Visible 
    ' property was set to false, Thus the only way I found to clear the item was to 
    ' destroy and re-create the ProgressBar.
    '
    ' *****************************************************************************

    Dim NewProgressBar As New ToolStripProgressBar

    Try

      NewProgressBar.Name = pProgressBar.Name
      NewProgressBar.Size = New System.Drawing.Size(pProgressBar.Size.Width, pProgressBar.Size.Height)
      NewProgressBar.Maximum = pProgressBar.Maximum
      NewProgressBar.Minimum = pProgressBar.Minimum
      NewProgressBar.Value = pProgressBar.Minimum
      NewProgressBar.Step = pProgressBar.Step
      NewProgressBar.Visible = False

      pToolStrip.Items.Remove(pProgressBar)
      pProgressBar.Dispose()
      pProgressBar = NewProgressBar
      pToolStrip.Items.Insert(0, pProgressBar)

    Catch ex As Exception
    End Try

    Return NewProgressBar
  End Function

#End Region

#Region " Update Event processes and Event Thread synchronisation class"

  ' **************************************************************************************
  ' When a Venice Update is received from the TCPServer, the message should be de-coded and 
  ' appropriate Update events should be triggered and processed by the appropriate threads.
  ' And this is what I thought was happening, but I was wrong !
  '
  '
  ' When a MessageReceivedEvent is triggered by the TCPClient object, it is the TCPClient thread that
  ' executes the associated handlers.
  ' Thus the 'TCPMessageReceived' process is (always?) executed by the TCPclient thread.
  ' That being the case, the subsequent VeniceUpdateEvents are also run within the TCPClient thread, which
  ' can cause problems when controls are updated on Venice forms, control update methods and properties not being 
  ' thread safe. 
  ' dotNet 1 did not seem to complain about this, though it is not thread safe and may have been the root cause
  ' of some strange bugs (Brian's mysterious hangings ?). Dot Net 2.0 does seem to complain vociferously
  ' about this which is what has brought it to my attention.
  '
  ' The simplest way of marshaling the update execution onto the main Venice UI thread seems to be to
  ' leverage the 'Invoke' method of the 'Control' class, which is designed to marshal execution onto the 
  ' Control's parent thread.
  ' 
  ' Thus the 'UpdateEventControlClass' exists for this sole purpose. It exposes a simple delegate which take parameters 
  ' identical to the ProcessTCPMessage() process. 
  '
  ' AddHandler VeniceTCPClient.MessageReceivedEvent, AddressOf Me.TCPMessageReceived
  ' **************************************************************************************

  ''' <summary>
  ''' Class UpdateEventControlClass
  ''' </summary>
  Friend Class UpdateEventControlClass
    ' **************************************************************************************
    ' Simple Control Class designed to Marshal Update Event processing back to the main Venice
    ' UserInterface thread so that subsequent control updates are conducted in a thread safe fashion.
    ' **************************************************************************************

    Inherits Control

    ''' <summary>
    ''' Delegate MessageReceivedDelegate
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="MessageReceivedEventArgs"/> instance containing the event data.</param>
    Delegate Sub MessageReceivedDelegate(ByVal sender As System.Object, ByVal e As MessageReceivedEventArgs)

    ''' <summary>
    ''' The _ message received delegate
    ''' </summary>
    Private _MessageReceivedDelegate As MessageReceivedDelegate
    ''' <summary>
    ''' The main form
    ''' </summary>
    Private MainForm As VeniceMain

    ''' <summary>
    ''' Gets the TCP message delegate.
    ''' </summary>
    ''' <value>The TCP message delegate.</value>
    Public ReadOnly Property TCPMessageDelegate() As MessageReceivedDelegate
      Get
        Return _MessageReceivedDelegate
      End Get
    End Property

    ''' <summary>
    ''' Prevents a default instance of the <see cref="UpdateEventControlClass"/> class from being created.
    ''' </summary>
    Private Sub New()
      MyBase.New()
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="UpdateEventControlClass"/> class.
    ''' </summary>
    ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
    Public Sub New(ByVal pMainForm As VeniceMain)
      MyBase.New()

      MainForm = pMainForm
      _MessageReceivedDelegate = New MessageReceivedDelegate(AddressOf Me.TCPMessageReceived)
    End Sub

    ''' <summary>
    ''' TCPs the message received.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="MessageReceivedEventArgs"/> instance containing the event data.</param>
    Private Sub TCPMessageReceived(ByVal sender As System.Object, ByVal e As MessageReceivedEventArgs)
      MainForm.ProcessTCPMessage(sender, e)
    End Sub

  End Class

  ''' <summary>
  ''' Delegate Main_TriggerEventDelegate
  ''' </summary>
  ''' <param name="pEventArgs">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
  Private Delegate Sub Main_TriggerEventDelegate(ByVal pEventArgs As RenaissanceGlobals.RenaissanceUpdateEventArgs)

  ''' <summary>
  ''' Triggers the event.
  ''' </summary>
  ''' <param name="UpdateEvent">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
  Friend Sub TriggerEvent(ByVal UpdateEvent As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    ' **********************************************************************
    ' Trigger an update event.
    ' Used initially by the BackgroundTableLoaderClass
    ' Event is marshalled back to the UI Thread as the listeners need to execute on this thread.
    ' **********************************************************************
    Try
      If Me.InvokeRequired Then
        Dim d As New Main_TriggerEventDelegate(AddressOf TriggerEvent)
        ' Use non-blocking Invoke method.
        Me.BeginInvoke(d, New Object() {UpdateEvent})
        Exit Sub
      End If

      ' Call AutoUpdate on this form first to ensure that it happens first !
      Me.AutoUpdate(Me, UpdateEvent)
      RaiseEvent VeniceAutoUpdate(Me, UpdateEvent)
    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Main_s the raise event.
  ''' </summary>
  ''' <param name="pEventArgs">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
  Public Sub Main_RaiseEvent(ByVal pEventArgs As RenaissanceGlobals.RenaissanceUpdateEventArgs) Implements StandardRenaissanceMainForm.Main_RaiseEvent
    ' **********************************************************************
    ' Propogate the Changed Event, Globally.
    ' **********************************************************************
    Call Main_RaiseEvent(pEventArgs, True)
  End Sub

  ''' <summary>
  ''' Delegate Main_RaiseEventDelegate
  ''' </summary>
  ''' <param name="pEventArgs">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
  ''' <param name="pGlobalUpdate">if set to <c>true</c> [p global update].</param>
  Private Delegate Sub Main_RaiseEventDelegate(ByVal pEventArgs As RenaissanceGlobals.RenaissanceUpdateEventArgs, ByVal pGlobalUpdate As Boolean)

  ''' <summary>
  ''' Main_s the raise event.
  ''' </summary>
  ''' <param name="pEventArgs">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
  ''' <param name="pGlobalUpdate">if set to <c>true</c> [p global update].</param>
  Public Sub Main_RaiseEvent(ByVal pEventArgs As RenaissanceGlobals.RenaissanceUpdateEventArgs, ByVal pGlobalUpdate As Boolean)
    ' **********************************************************************
    ' Propogate the local Changed Event
    '
    ' **********************************************************************
    If Me.InvokeRequired Then
      Dim d As New Main_RaiseEventDelegate(AddressOf Main_RaiseEvent)
      ' Use non-blocking Invoke method.
      Me.BeginInvoke(d, New Object() {pEventArgs, pGlobalUpdate})
      Exit Sub
    End If

    Dim ApplicationName As FCP_Application = (FCP_Application.Venice Or FCP_Application.Renaissance)
    Dim thisChangeID As RenaissanceGlobals.RenaissanceChangeID
    Dim thisUpdateDetail As String
    Dim ThisDataset As StandardDataset

    ' If this event includes the KnowledgeDate ID or the DB Connection ID, then re-load all the tables.
    ' If this is only a KnowledgeDate Update, then don't reload non-Knowledgedated tables.
    ' If this is a connection update, then re-load all standard tables.

    Dim TablesToReload As New ArrayList
    Dim TablesToReloadDetail As New Dictionary(Of RenaissanceChangeID, String)
    Dim UpdateID As RenaissanceChangeID

    Try
      Dim UpdateIsKnowledgeDate As Boolean = False
      Dim UpdateIsConnection As Boolean = False

      For Each thisChangeID In pEventArgs.TablesChanged
        If (thisChangeID = RenaissanceChangeID.KnowledgeDate) Then
          UpdateIsKnowledgeDate = True
        End If
        If (thisChangeID = RenaissanceChangeID.Connection) Then
          UpdateIsConnection = True
        End If

        thisUpdateDetail = pEventArgs.UpdateDetail(thisChangeID)

        ' Cycle through each standard dataset (via ChangeID enumeration)
        ' checking whether there should be a dependency based re-load.
        ' Do not bother re-loading the dataset if this is a Connection or KD Update
        ' as the DS will be re-loaded in the next step.

        For Each UpdateID In System.Enum.GetValues(GetType(RenaissanceChangeID))
          ThisDataset = RenaissanceStandardDatasets.GetStandardDataset(UpdateID)

          If (Not (ThisDataset Is Nothing)) Then
            ThisDataset.ISChangeIDToNote(thisChangeID)

            If Not (UpdateIsConnection Or (UpdateIsKnowledgeDate And ThisDataset.IsKnowledgeDated)) Then
              If ThisDataset.IsChangeIDToTriggerUpdate(thisChangeID) Then
                If Not TablesToReload.Contains(ThisDataset) Then
                  TablesToReload.Add(ThisDataset)

                  ' propogate this Update Detail to dependant tables ?

                  If ThisDataset.IsChangeIdToPropogateDetailsFrom(thisChangeID) Then
                    ReloadTable(UpdateID, thisUpdateDetail, False)

                    If (TablesToReloadDetail.ContainsKey(ThisDataset.ChangeID) = False) Then
                      TablesToReloadDetail.Add(ThisDataset.ChangeID, thisUpdateDetail)
                    Else
                      ' It already exists.
                      ' Either it is blank, in which case leave it alone, or it contains 'thisUpdateDetail', in which case leave it alone.
                    End If
                  Else
                    ReloadTable(UpdateID, "", False)

                    If (TablesToReloadDetail.ContainsKey(ThisDataset.ChangeID)) Then
                      TablesToReloadDetail(ThisDataset.ChangeID) = "" ' Just to be safe.
                    Else
                      TablesToReloadDetail.Add(ThisDataset.ChangeID, "")
                    End If
                  End If
                End If
              End If
            End If
          End If
        Next

        ' Post Table Refresh Actions

        Select Case thisChangeID

          Case RenaissanceChangeID.tblLocations

            Call RefreshLocationsDictionary()

          Case RenaissanceChangeID.Mastername

            Call RefreshMasternameDictionary()

          Case RenaissanceChangeID.Information

            Try
              ' The PertracData object caches Instrument Information Data and is shared between forms, thus
              ' it makes sense to clear the cache at this central point.

              SyncLock PertracData
                PertracData.ClearInformationCache()
              End SyncLock
            Catch ex As Exception
            End Try

          Case RenaissanceChangeID.Performance

            Try
              ' The StatFunctions object caches Instrument Statistics Data and is shared between forms, thus
              ' it makes sense to clear the cache at this central point.

              SyncLock StatFunctions
                StatFunctions.ClearCache()
              End SyncLock

            Catch ex As Exception
            End Try

            Try
              ' The PertracData object caches Instrument Returns Data and is shared between forms, thus
              ' it makes sense to clear the cache at this central point.

              SyncLock PertracData
                PertracData.ClearDataCache()
              End SyncLock
            Catch ex As Exception
            End Try

        End Select

      Next

      ' Re-load Datasets as appropriate to a connection or KnowledgeDate change.

      For Each UpdateID In System.Enum.GetValues(GetType(RenaissanceChangeID))
        ThisDataset = RenaissanceStandardDatasets.GetStandardDataset(UpdateID)

        If (Not (ThisDataset Is Nothing)) Then
          If (UpdateIsConnection Or (UpdateIsKnowledgeDate And ThisDataset.IsKnowledgeDated)) Then
            ReloadTable(ThisDataset.ChangeID, "", False)
          End If
        End If
      Next

    Catch ex As Exception
      Call LogError(Me.Name & ", Main_RaiseEvent", LOG_LEVELS.Error, ex.Message, "Error checking KnowledgeDate or Connection Update.", ex.StackTrace, True)
    End Try

    Try
      ' Call AutoUpdate on this form first to ensure that it happens first !
      Me.AutoUpdate(Me, pEventArgs)
      RaiseEvent VeniceAutoUpdate(Me, pEventArgs)
    Catch ex As Exception
      Call LogError(Me.Name & ", Main_RaiseEvent", LOG_LEVELS.Error, ex.Message, "Error raising VeniceAutoUpdate event.", ex.StackTrace, True)
    End Try

    ' Post 'Cascaded' Update messages
    ' This is structured so that Cascaded updates are broadcast after the primary Table update.

    If (Not (TablesToReload Is Nothing)) AndAlso (TablesToReload.Count > 0) Then

      TablesToReload.Sort(New RenaissanceGlobals.StandardDatasetsComparer)

      Dim Counter As Integer

      For Counter = 0 To (TablesToReload.Count - 1)
        ThisDataset = TablesToReload(Counter)
        thisUpdateDetail = TablesToReloadDetail(ThisDataset.ChangeID)

        Try
          ' Call AutoUpdate on this form first to ensure that it happens first !
          Me.AutoUpdate(Me, New RenaissanceGlobals.RenaissanceUpdateEventArgs(ThisDataset.ChangeID, thisUpdateDetail))
          RaiseEvent VeniceAutoUpdate(Me, New RenaissanceGlobals.RenaissanceUpdateEventArgs(ThisDataset.ChangeID, thisUpdateDetail))
        Catch ex As Exception
          Call LogError(Me.Name & ", Main_RaiseEvent", LOG_LEVELS.Error, ex.Message, "Error raising VeniceAutoUpdate event.", ex.StackTrace, True)
          Exit For
        End Try
      Next

      TablesToReload.Clear()
      TablesToReloadDetail.Clear()
    End If

    ' Pass Updates to the message server, if required

    If (pGlobalUpdate = True) Then
      If (Not (Me.VeniceTCPClient Is Nothing)) Then

        If pEventArgs.TablesChanged.GetLength(0) >= 1 Then
          ' Multiple Updates, 
          ' Construct a Venice Update object 

          Dim ChangeIDArray() As RenaissanceChangeID
          Dim UpdateDetailArray() As String

          ChangeIDArray = pEventArgs.TablesChanged
          UpdateDetailArray = pEventArgs.UpdateDetails

          Dim thisRenaissanceUpdateClass As New RenaissanceUpdateClass(ChangeIDArray, UpdateDetailArray)
          Dim ThisMessageHeader As New TCPServerHeader(TCP_ServerMessageType.Client_ApplicationUpdateMessage, ApplicationName, SQLServerInstance, "", 1, 0)

          ' Post Header and Update object to Queue for sending.

          Me.VeniceTCPClient.PostNewMessage(New MessageFolder(ThisMessageHeader, thisRenaissanceUpdateClass))

        Else
          ' Only a single Update Event.
          ' Post it as a simple Header based Update
          Dim UpdateDetail As String

          UpdateDetail = pEventArgs.UpdateDetail(thisChangeID)

          If (UpdateDetail IsNot Nothing) AndAlso (UpdateDetail.Length > 0) Then
            Me.VeniceTCPClient.PostNewMessage(New MessageFolder(New TCPServerHeader(TCP_ServerMessageType.Client_ApplicationUpdateMessage, ApplicationName, SQLServerInstance, thisChangeID.ToString & "|" & UpdateDetail, 0, 0)))
          Else
            Me.VeniceTCPClient.PostNewMessage(New MessageFolder(New TCPServerHeader(TCP_ServerMessageType.Client_ApplicationUpdateMessage, ApplicationName, SQLServerInstance, thisChangeID.ToString, 0, 0)))
          End If

        End If
      End If

    End If
  End Sub

  ''' <summary>
  ''' Delegate Main_RaiseEventBackgroundDelegate
  ''' </summary>
  ''' <param name="pEventArgs">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
  ''' <param name="pGlobalUpdate">if set to <c>true</c> [p global update].</param>
  Private Delegate Sub Main_RaiseEventBackgroundDelegate(ByVal pEventArgs As RenaissanceGlobals.RenaissanceUpdateEventArgs, ByVal pGlobalUpdate As Boolean)

  ''' <summary>
  ''' Main_s the raise event_ background.
  ''' </summary>
  ''' <param name="pEventArgs">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
  ''' <param name="pGlobalUpdate">if set to <c>true</c> [p global update].</param>
  Public Sub Main_RaiseEvent_Background(ByVal pEventArgs As RenaissanceGlobals.RenaissanceUpdateEventArgs, ByVal pGlobalUpdate As Boolean)
    ' **********************************************************************
    ' Propogate the local Changed Event
    '
    ' **********************************************************************
    If Me.InvokeRequired Then
      Dim d As New Main_RaiseEventBackgroundDelegate(AddressOf Main_RaiseEvent_Background)
      ' Use non-blocking Invoke method.
      Me.BeginInvoke(d, New Object() {pEventArgs, pGlobalUpdate})
      Exit Sub
    End If

    If (_UseBackgroundTableLoader = False) Then
      Main_RaiseEvent(pEventArgs, pGlobalUpdate)
      Exit Sub
    End If

    Dim ApplicationName As FCP_Application = (FCP_Application.Venice Or FCP_Application.Renaissance)
    Dim thisChangeID As RenaissanceGlobals.RenaissanceChangeID
    Dim thisUpdateDetail As String
    Dim ThisDataset As StandardDataset

    ' If this event includes the KnowledgeDate ID or the DB Connection ID, then re-load all the tables.
    ' If this is only a KnowledgeDate Update, then don't reload non-Knowledgedated tables.
    ' If this is a connection update, then re-load all standard tables.

    Dim TablesToReload As New ArrayList
    Dim TablesToReloadDetail As New Dictionary(Of RenaissanceChangeID, String)
    Dim UpdateID As RenaissanceChangeID

    Try
      Dim UpdateIsKnowledgeDate As Boolean = False
      Dim UpdateIsConnection As Boolean = False

      For Each thisChangeID In pEventArgs.TablesChanged
        If (thisChangeID = RenaissanceChangeID.KnowledgeDate) Then
          UpdateIsKnowledgeDate = True
        End If
        If (thisChangeID = RenaissanceChangeID.Connection) Then
          UpdateIsConnection = True
        End If

        thisUpdateDetail = pEventArgs.UpdateDetail(thisChangeID)

        ' Cycle through each standard dataset (via ChangeID enumeration)
        ' checking whether there should be a dependency based re-load.
        ' Do not bother re-loading the dataset if this is a Connection or KD Update
        ' as the DS will be re-loaded in the next step.

        For Each UpdateID In System.Enum.GetValues(GetType(RenaissanceChangeID))
          ThisDataset = RenaissanceStandardDatasets.GetStandardDataset(UpdateID)

          If (Not (ThisDataset Is Nothing)) Then
            ThisDataset.ISChangeIDToNote(thisChangeID)

            If Not (UpdateIsConnection Or (UpdateIsKnowledgeDate And ThisDataset.IsKnowledgeDated)) Then
              If ThisDataset.IsChangeIDToTriggerUpdate(thisChangeID) Then
                If Not TablesToReload.Contains(ThisDataset) Then
                  TablesToReload.Add(ThisDataset)

                  ' propogate this Update Detail to dependant tables ?

                  If ThisDataset.IsChangeIdToPropogateDetailsFrom(thisChangeID) Then
                    Me.BackgroundTableLoaderProcess.EnqueueReloadTable(UpdateID, New RenaissanceUpdateEventArgs(UpdateID, thisUpdateDetail))

                    If (TablesToReloadDetail.ContainsKey(ThisDataset.ChangeID) = False) Then
                      TablesToReloadDetail.Add(ThisDataset.ChangeID, thisUpdateDetail)
                    Else
                      ' It already exists.
                      ' Either it is blank, in which case leave it alone, or it contains 'thisUpdateDetail', in which case leave it alone.
                    End If
                  Else
                    Me.BackgroundTableLoaderProcess.EnqueueReloadTable(UpdateID, Nothing)

                    If (TablesToReloadDetail.ContainsKey(ThisDataset.ChangeID)) Then
                      TablesToReloadDetail(ThisDataset.ChangeID) = "" ' Just to be safe.
                    Else
                      TablesToReloadDetail.Add(ThisDataset.ChangeID, "")
                    End If
                  End If
                End If
              End If
            End If
          End If
        Next

      Next

      ' Re-load Datasets as appropriate to a connection or KnowledgeDate change.

      For Each UpdateID In System.Enum.GetValues(GetType(RenaissanceChangeID))
        ThisDataset = RenaissanceStandardDatasets.GetStandardDataset(UpdateID)

        If (Not (ThisDataset Is Nothing)) Then
          If (UpdateIsConnection Or (UpdateIsKnowledgeDate And ThisDataset.IsKnowledgeDated)) Then
            Me.BackgroundTableLoaderProcess.EnqueueReloadTable(ThisDataset.ChangeID, Nothing)
          End If
        End If
      Next

    Catch ex As Exception
      Call LogError(Me.Name & ", Main_RaiseEvent", LOG_LEVELS.Error, ex.Message, "Error checking KnowledgeDate or Connection Update.", ex.StackTrace, True)
    End Try

    Try
      BackgroundTableLoaderProcess.EnqueueEvent(pEventArgs)
      ' Call AutoUpdate on this form first to ensure that it happens first !
      ' Me.AutoUpdate(Me, pEventArgs)
      ' RaiseEvent VeniceAutoUpdate(Me, pEventArgs)
    Catch ex As Exception
      Call LogError(Me.Name & ", Main_RaiseEvent", LOG_LEVELS.Error, ex.Message, "Error raising VeniceAutoUpdate event.", ex.StackTrace, True)
    End Try

    ' Post 'Cascaded' Update messages
    ' This is structured so that Cascaded updates are broadcast after the primary Table update.

    If (Not (TablesToReload Is Nothing)) AndAlso (TablesToReload.Count > 0) Then

      TablesToReload.Sort(New RenaissanceGlobals.StandardDatasetsComparer)

      Dim Counter As Integer

      For Counter = 0 To (TablesToReload.Count - 1)
        ThisDataset = TablesToReload(Counter)
        thisUpdateDetail = TablesToReloadDetail(ThisDataset.ChangeID)

        Try
          BackgroundTableLoaderProcess.EnqueueEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(ThisDataset.ChangeID, thisUpdateDetail))
          ' Call AutoUpdate on this form first to ensure that it happens first !
          ' Me.AutoUpdate(Me, New RenaissanceGlobals.RenaissanceUpdateEventArgs(UpdateID, thisUpdateDetail))
          'RaiseEvent VeniceAutoUpdate(Me, New RenaissanceGlobals.RenaissanceUpdateEventArgs(UpdateID, thisUpdateDetail))
        Catch ex As Exception
          Call LogError(Me.Name & ", Main_RaiseEvent", LOG_LEVELS.Error, ex.Message, "Error raising VeniceAutoUpdate event.", ex.StackTrace, True)
          Exit For
        End Try

      Next

      TablesToReload.Clear()
      TablesToReloadDetail.Clear()
    End If

    ' Pass Updates to the message server, if required

    If (pGlobalUpdate = True) Then
      If (Not (Me.VeniceTCPClient Is Nothing)) Then

        If pEventArgs.TablesChanged.GetLength(0) >= 1 Then
          ' Multiple Updates, 
          ' Construct a Venice Update object 

          Dim ChangeIDArray() As RenaissanceChangeID
          Dim UpdateDetailArray() As String

          ChangeIDArray = pEventArgs.TablesChanged
          UpdateDetailArray = pEventArgs.UpdateDetails

          Dim thisRenaissanceUpdateClass As New RenaissanceUpdateClass(ChangeIDArray, UpdateDetailArray)
          Dim ThisMessageHeader As New TCPServerHeader(TCP_ServerMessageType.Client_ApplicationUpdateMessage, ApplicationName, SQLServerInstance, "", 1, 0)

          ' Post Header and Update object to Queue for sending.

          Me.VeniceTCPClient.PostNewMessage(New MessageFolder(ThisMessageHeader, thisRenaissanceUpdateClass))

        Else
          ' Only a single Update Event.
          ' Post it as a simple Header based Update
          Dim UpdateDetail As String

          For Each thisChangeID In pEventArgs.TablesChanged
            If (Not (thisChangeID = RenaissanceGlobals.RenaissanceChangeID.None)) Then

              UpdateDetail = pEventArgs.UpdateDetail(thisChangeID)

              If (UpdateDetail IsNot Nothing) AndAlso (UpdateDetail.Length > 0) Then
                Me.VeniceTCPClient.PostNewMessage(New MessageFolder(New TCPServerHeader(TCP_ServerMessageType.Client_ApplicationUpdateMessage, ApplicationName, SQLServerInstance, thisChangeID.ToString & "|" & UpdateDetail, 0, 0)))
              Else
                Me.VeniceTCPClient.PostNewMessage(New MessageFolder(New TCPServerHeader(TCP_ServerMessageType.Client_ApplicationUpdateMessage, ApplicationName, SQLServerInstance, thisChangeID.ToString, 0, 0)))
              End If

            End If
          Next
        End If
      End If

    End If
  End Sub

  ''' <summary>
  ''' TCPs the message received.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="TCPServer_Common.MessageReceivedEventArgs"/> instance containing the event data.</param>
  Private Sub TCPMessageReceived(ByVal sender As Object, ByVal e As TCPServer_Common.MessageReceivedEventArgs)
    ' **************************************************************************************
    ' 
    ' Event handler for the TCP Client MessageReceivedEvent Event.
    ' This Routine should use the UpdateEventControl to marshal the update execution back to 
    ' the Main Venice User Interface thread.
    '
    ' **************************************************************************************

    Try
      UpdateEventControl.BeginInvoke(UpdateEventControl.TCPMessageDelegate, New Object() {sender, e})
    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' TCPs the error received.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The e.</param>
  Private Sub TCPErrorReceived(ByVal sender As Object, ByVal e As System.Exception)
    ' **************************************************************************************
    ' 
    ' Event handler for the TCP Client MessageReceivedEvent Event.
    ' This Routine should use the UpdateEventControl to marshal the update execution back to 
    ' the Main Venice User Interface thread.
    '
    ' **************************************************************************************

    Try
      LogError(Me.Name, LOG_LEVELS.Error, e.Message, "TCP Server Error", e.StackTrace, False)
    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Processes the TCP message.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="MessageReceivedEventArgs"/> instance containing the event data.</param>
  Friend Sub ProcessTCPMessage(ByVal sender As System.Object, ByVal e As MessageReceivedEventArgs)
    ' **********************************************************************
    '
    ' Process MessageReceived events from the TCP Client.
    ' Usually this event is only trggered for Update Messages.
    '
    ' **********************************************************************
    Dim TerminateFlag As Integer = 0

    Select Case e.HeaderMessage.MessageType

      Case TCP_ServerMessageType.Client_KansasStatusMessage

        Try

          Dim StatusString As String = Nz(e.HeaderMessage.MessageString, "")
          Dim MessageParts() As String = StatusString.Split(":")
          Dim MachineName As String = MessageParts(0)
          lastKansasMsgDate = Now
          'System.Console.Out.WriteLine("***Client_KansasStatusMessage received at : " & CStr(Now) & " StatusString=" & StatusString)
          If (KansasStatusMessages IsNot Nothing) Then
            If (KansasStatusMessages.ContainsKey(MachineName)) Then
              If (KansasStatusMessages(MachineName) <> StatusString) Then
                KansasStatusMessages(MachineName) = StatusString
                Main_RaiseEvent_Background(New RenaissanceUpdateEventArgs(RenaissanceChangeID.KansasStatusMessage), False)
              End If
            Else
              KansasStatusMessages.Add(MachineName, StatusString)
              Main_RaiseEvent_Background(New RenaissanceUpdateEventArgs(RenaissanceChangeID.KansasStatusMessage), False)
            End If
          End If

        Catch ex As Exception
        End Try

      Case TCP_ServerMessageType.Server_ApplicationUpdateMessage, TCP_ServerMessageType.Client_ApplicationUpdateMessage

        ' **********************************************************************
        ' If this is an Update Message....
        ' **********************************************************************

        If (e.HeaderMessage.Application And (FCP_Application.Venice Or FCP_Application.Renaissance)) <> FCP_Application.None Then
          ' **********************************************************************
          ' And it is for Venice or Renaissance
          ' **********************************************************************

          If (e.HeaderMessage.FollowingMessageCount > 0) AndAlso _
          (e.FollowingObjects.Count > 0) AndAlso _
          (e.FollowingObjects(0).GetType Is GetType(RenaissanceUpdateClass)) Then

            ' **********************************************************************
            ' Multiple Updates have been sent in a 'RenaissanceUpdateClass' class
            ' **********************************************************************

            Dim thisRenaissanceUpdateEventArgs As New RenaissanceUpdateEventArgs
            Dim thisRenaissanceUpdateClass As New RenaissanceUpdateClass
            Dim thisChangeID As RenaissanceChangeID

            ' Construct the local UpdateEvent Argument class

            thisRenaissanceUpdateClass = CType(e.FollowingObjects(0), RenaissanceUpdateClass)

            For Each thisChangeID In thisRenaissanceUpdateClass.ChangeIDs

              ' Don't propogate Knowledgedate or DBConnection changes from other applications.

              If (thisChangeID <> RenaissanceChangeID.KnowledgeDate) And (thisChangeID <> RenaissanceChangeID.Connection) Then

                ' Add this ChangeID to the Update Class Object
                thisRenaissanceUpdateEventArgs.TableChanged(thisChangeID) = True
                thisRenaissanceUpdateEventArgs.UpdateDetail(thisChangeID) = thisRenaissanceUpdateClass.UpdateDetail(thisChangeID)

                ' ReLoad the related dataset
                If (_UseBackgroundTableLoader) Then
                  Me.BackgroundTableLoaderProcess.EnqueueReloadTable(thisChangeID, New RenaissanceUpdateEventArgs(thisChangeID, thisRenaissanceUpdateClass.UpdateDetail(thisChangeID)))
                Else
                  Call ReloadTable(thisChangeID, thisRenaissanceUpdateClass.UpdateDetail(thisChangeID), False)
                End If

              End If

              If thisChangeID = RenaissanceChangeID.Terminate1 Then
                TerminateFlag = (TerminateFlag Or 1)
              End If
              If thisChangeID = RenaissanceChangeID.Terminate2 Then
                TerminateFlag = (TerminateFlag Or 2)
              End If
            Next

            ' **********************************************************************
            ' Handle the Venice Shutdown Message.
            ' **********************************************************************
            If TerminateFlag = 3 Then
              Call Menu_File_Close_Click(Me, New System.EventArgs)

              Exit Sub
            End If

            ' **********************************************************************
            ' Trigger the local Changed Event
            ' **********************************************************************

            If (_UseBackgroundTableLoader) Then
              Main_RaiseEvent_Background(thisRenaissanceUpdateEventArgs, False)
            Else
              Main_RaiseEvent(thisRenaissanceUpdateEventArgs, False)
            End If
            ' Call AutoUpdate on this form first to ensure that it happens first !
            ' Me.AutoUpdate(Me, thisRenaissanceUpdateEventArgs)
            'RaiseEvent VeniceAutoUpdate(Me, thisRenaissanceUpdateEventArgs)

          Else

            ' **********************************************************************
            ' Only a single Update has been set, encoded in the MessageHeader String
            ' The Message string may either be an Integer relating to the RenaissanceChangeID Enum,
            ' OR the text representation of the Enum value, e.g. 'tblFund'.
            ' **********************************************************************

            Dim ChangeID As Integer    ' RenaissanceGlobals.RenaissanceChangeID
            Dim UpdateDetail As String
            ChangeID = e.UpdateMessage(FCP_Application.Venice Or FCP_Application.Renaissance)
            UpdateDetail = e.UpdateDetail(FCP_Application.Venice Or FCP_Application.Renaissance)

            If (ChangeID > 0) And (ChangeID <> RenaissanceChangeID.KnowledgeDate) Then
              Try
                ' **********************************************************************
                ' ReLoad the related dataset
                ' Trigger the local Changed Event
                '
                ' This code now assumes that the ChangeID given in this way might be a bitmap, thus
                ' it checks against every item in the RenaissanceChangeID enumeration.
                ' **********************************************************************

                If (_UseBackgroundTableLoader) Then
                  Me.BackgroundTableLoaderProcess.EnqueueReloadTable(ChangeID, New RenaissanceUpdateEventArgs(ChangeID, UpdateDetail))
                Else
                  Call ReloadTable(ChangeID, UpdateDetail, False)
                End If

                ' Raise Event.

                If (_UseBackgroundTableLoader) Then
                  Main_RaiseEvent_Background(New RenaissanceUpdateEventArgs(ChangeID, UpdateDetail), False)
                Else
                  Main_RaiseEvent(New RenaissanceUpdateEventArgs(ChangeID, UpdateDetail), False)
                End If

              Catch ex As Exception
              End Try
            End If
          End If
        End If

      Case TCP_ServerMessageType.Server_NaplesDataAnswer, TCP_ServerMessageType.Client_NaplesDataAnswer

      Case Else

    End Select

  End Sub

  ''' <summary>
  ''' Autoes the update.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    ' **************************************************************************************
    ' Eable / Disable menu items according to any changes made in the User Permissions table.
    '
    '
    ' **************************************************************************************

    If (Me.IsDisposed) Then Exit Sub

    If (Extra_Debug) Then
      Call LogError(Me.Name & ", Load_Table", LOG_LEVELS.Audit, "", "Update Event,  " & e.TablesChanged(0).ToString, "", False)
    End If

    Dim UpdateDetail As String

    ' Changes to the tblUserPermissions table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Then
        Call SetMenuBarEnabled()
      End If
    Catch ex As Exception
    End Try

    ' Changes to the tblPrice table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblPrice) = True) Then

        UpdateDetail = e.UpdateDetail(RenaissanceGlobals.RenaissanceChangeID.tblPrice)

        If (UpdateDetail.Length = 0) OrElse (UpdateDetail.Split(New Char() {",", "|"}, StringSplitOptions.RemoveEmptyEntries).Contains("0")) Then
          Me.PertracData.ClearDataCache()
          Me.StatFunctions.ClearCache()
        Else
          Me.PertracData.ClearDataCache(True, False, False, False, False, e.UpdateDetail(RenaissanceGlobals.RenaissanceChangeID.tblPrice))
          Me.StatFunctions.ClearCache(True, False, False, False, False, e.UpdateDetail(RenaissanceGlobals.RenaissanceChangeID.tblPrice))
        End If

      End If
    Catch ex As Exception
    End Try


  End Sub

#End Region

#Region " Venice Menu Code "

  ''' <summary>
  ''' Delegate New_VeniceFormDelegate
  ''' </summary>
  ''' <param name="pFormID">The p form ID.</param>
  ''' <returns>RenaissanceGlobals.RenaissanceFormHandle.</returns>
  Private Delegate Function New_VeniceFormDelegate(ByVal pFormID As VeniceFormID) As RenaissanceGlobals.RenaissanceFormHandle

  ''' <summary>
  ''' New_s the venice form.
  ''' </summary>
  ''' <param name="pFormID">The p form ID.</param>
  ''' <returns>RenaissanceGlobals.RenaissanceFormHandle.</returns>
  Friend Function New_VeniceForm(ByVal pFormID As VeniceFormID) As RenaissanceGlobals.RenaissanceFormHandle
    ' **********************************************************************
    ' Spawn new Form.
    ' **********************************************************************
    If Me.InvokeRequired Then
      Dim d As New New_VeniceFormDelegate(AddressOf New_VeniceForm)
      Return Me.Invoke(d, New Object() {pFormID})
      Exit Function
    End If

    Dim newFormHandle As RenaissanceGlobals.RenaissanceFormHandle
    Dim StartCursor As Cursor
    Dim IsNewForm As Boolean = False
    Dim ActiveItemCount As Integer = 0

    StartCursor = Me.Cursor
    Me.Cursor = Cursors.WaitCursor

    newFormHandle = VeniceForms.UnusedItem(pFormID)

    ' Parannoia check for disposed forms. Should never occur. (ha !)
    Try
      If (Not (newFormHandle Is Nothing)) AndAlso (Not (newFormHandle.Form Is Nothing)) Then
        If (newFormHandle.Form.IsDisposed) Then
          VeniceForms.Remove(newFormHandle)
          newFormHandle = Nothing
        End If
      End If
    Catch ex As Exception
      newFormHandle = Nothing
    End Try

    ' Create a New form if an existing one was not returned.

    If (newFormHandle Is Nothing) Then
      Try
        Dim ThisAssembly As System.Reflection.Assembly = System.Reflection.Assembly.GetExecutingAssembly
        Dim myForm As Form

        myForm = CType(ThisAssembly.CreateInstance("Venice." & System.Enum.GetName(GetType(VeniceFormID), pFormID), True, Reflection.BindingFlags.Default, Nothing, New Object() {Me}, Nothing, Nothing), Form)
        myForm.Icon = Me.Icon.Clone

        newFormHandle = New RenaissanceGlobals.RenaissanceFormHandle(myForm, pFormID)
        Application.DoEvents()
        If Not (newFormHandle.Form Is Nothing) Then
          VeniceForms.Add(newFormHandle)
          IsNewForm = True
        End If

      Catch ex As Exception
        newFormHandle = Nothing
        Call LogError(Me.Name & ", New_VeniceForm", LOG_LEVELS.Error, ex.Message, "Error Loading Form " & System.Enum.GetName(GetType(VeniceFormID), pFormID), ex.StackTrace, True)
      End Try

    Else
      ' Refurbish an existing form.

      ' newFormHandle.Form.Visible = True
      Try
        Dim StdForm As StandardVeniceForm
        StdForm = newFormHandle.Form
        StdForm.ResetForm()
      Catch ex As Exception
        newFormHandle = Nothing
        Call LogError(Me.Name & ", New_VeniceForm", LOG_LEVELS.Error, ex.Message, "Error Re-Setting Form " & System.Enum.GetName(GetType(VeniceFormID), pFormID), ex.StackTrace, True)
      End Try
    End If

    If (Me.IsMdiContainer) AndAlso (pFormID <> VeniceFormID.frmViewReport) Then
      Try
        newFormHandle.Form.MdiParent = Me
      Catch ex As Exception
      End Try
    End If

    Try
      If Not (newFormHandle Is Nothing) Then
        Dim StdForm As StandardVeniceForm
        Dim InitialLocation As Point

        Dim InitialSizeAndLocation As Rectangle

        ActiveItemCount = VeniceForms.ActiveItemsCount - 2
        newFormHandle.InUse = True
        newFormHandle.Form.ShowInTaskbar = True
        newFormHandle.Form.StartPosition = FormStartPosition.Manual

        If (Me.WindowState = FormWindowState.Minimized) Then ' Minimised
          InitialLocation = New Point(Math.Max(0, Me.RestoreBounds.X) + ((ActiveItemCount \ 10) * 100) + ((ActiveItemCount Mod 10) * 30) + 30, Math.Max(0, Me.RestoreBounds.Y) + ((ActiveItemCount Mod 10) * 30) + 30)
        Else
          InitialLocation = New Point(Math.Max(0, Me.Left) + ((ActiveItemCount \ 10) * 100) + ((ActiveItemCount Mod 10) * 30) + 30, Math.Max(0, Me.Top) + ((ActiveItemCount Mod 10) * 30) + 30)
        End If
        InitialSizeAndLocation = New Rectangle(InitialLocation, New Size(newFormHandle.Form.Width, newFormHandle.Form.Height))

        newFormHandle.Form.Bounds = InitialSizeAndLocation

        ' Don't immediately show the Report form.
        ' Show all other forms.

        If (pFormID = VeniceFormID.frmViewReport) Then
          CType(newFormHandle.Form, frmViewReport).Opacity = 0
        End If

        If newFormHandle.Form.Visible = False Then
          newFormHandle.Form.Show()
        Else
          newFormHandle.Form.WindowState = FormWindowState.Normal
        End If

        newFormHandle.Form.Bounds = InitialSizeAndLocation
        Application.DoEvents()

        StdForm = newFormHandle.Form
        If StdForm.FormOpenFailed Then
          newFormHandle.Form.Hide()
          RemoveFromFormsCollection(newFormHandle.Form)
        End If

        SetComboSelectionLengths(newFormHandle.Form)

      End If
    Catch ex As Exception
      Call LogError(Me.Name & ", New_VeniceForm", LOG_LEVELS.Error, ex.Message, "Error Showing Form " & System.Enum.GetName(GetType(VeniceFormID), pFormID), ex.StackTrace, True)
    End Try

    SetToolStripText(VeniceStatusBar, "")
    Me.Cursor = StartCursor
    Call BuildWindowsMenu()

    Return newFormHandle
  End Function

  ''' <summary>
  ''' Removes from forms collection.
  ''' </summary>
  ''' <param name="pForm">The p form.</param>
  Public Sub RemoveFromFormsCollection(ByRef pForm As Form)
    SyncLock VeniceForms
      VeniceForms.Remove(pForm)
    End SyncLock
    Call BuildWindowsMenu()
  End Sub

  ''' <summary>
  ''' Removes from forms collection.
  ''' </summary>
  ''' <param name="pVeniceFormHandle">The p venice form handle.</param>
  Public Sub RemoveFromFormsCollection(ByRef pVeniceFormHandle As RenaissanceGlobals.RenaissanceFormHandle)
    SyncLock VeniceForms
      VeniceForms.Remove(pVeniceFormHandle)
    End SyncLock
    Call BuildWindowsMenu()
  End Sub

  ''' <summary>
  ''' Hides the in forms collection.
  ''' </summary>
  ''' <param name="pForm">The p form.</param>
  Public Sub HideInFormsCollection(ByRef pForm As Form)
    SyncLock VeniceForms
      VeniceForms.Hide(pForm)
    End SyncLock
    Call BuildWindowsMenu()
  End Sub

  ''' <summary>
  ''' Hides the in forms collection.
  ''' </summary>
  ''' <param name="pVeniceFormHandle">The p venice form handle.</param>
  Public Sub HideInFormsCollection(ByRef pVeniceFormHandle As RenaissanceGlobals.RenaissanceFormHandle)
    SyncLock VeniceForms
      VeniceForms.Hide(pVeniceFormHandle)
    End SyncLock
    Call BuildWindowsMenu()
  End Sub

  ''' <summary>
  ''' Delegate BuildWindowsMenuDelegate
  ''' </summary>
  Private Delegate Sub BuildWindowsMenuDelegate()

  ''' <summary>
  ''' Builds the windows menu.
  ''' </summary>
  Friend Sub BuildWindowsMenu()
    ' ************************************************************************
    ' Routine to Build the 'Windows' Menu.
    '
    ' This routine is called from within the 'New_VaniceForm()' function and the
    ' various Hide or Remove Form functions, but it should be updated each time a 
    ' Form is added / Removed or Hidden in the Venice Forms collection.
    '
    ' At present this is done EXCLUSIVELY through this collection of functions
    ' but just be aware !
    '
    ' ************************************************************************
    If Me.InvokeRequired Then
      Dim d As New BuildWindowsMenuDelegate(AddressOf BuildWindowsMenu)
      Call Me.Invoke(d, New Object() {})
      Exit Sub
    End If

    Dim thisWindowsMenu As ToolStripMenuItem
    Dim ActiveWindows As ArrayList
    Dim WindowCount As Integer
    Dim ThisFormHandle As RenaissanceFormHandle

    Try
      SyncLock Menu_Windows
        SyncLock VeniceForms
          thisWindowsMenu = Menu_Windows
          If (thisWindowsMenu.DropDownItems.Count > 0) Then
            thisWindowsMenu.DropDownItems.Clear()
          End If

          ActiveWindows = Me.VeniceForms.AllActiveItems
          If (Not (ActiveWindows Is Nothing)) AndAlso (ActiveWindows.Count > 1) Then
            ' Index 0 should always be the Main Form, so skip it.
            Dim thisMenuItem As ToolStripMenuItem

            For WindowCount = 1 To (ActiveWindows.Count - 1)
              ThisFormHandle = ActiveWindows(WindowCount)

              thisMenuItem = New ToolStripMenuItem(ThisFormHandle.Form.Text, Nothing, AddressOf Me.WindowsMenuEvent)
              thisMenuItem.Tag = (WindowCount)

              thisWindowsMenu.DropDownItems.Add(thisMenuItem)
            Next
          End If
        End SyncLock
      End SyncLock
    Catch ex As Exception
      LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error building 'Windows' Menu.", ex.StackTrace, True)
    End Try

  End Sub

  ''' <summary>
  ''' Windowses the menu event.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
  Private Sub WindowsMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
    ' ************************************************************************
    ' Action Event for the Windows Menu.
    '
    ' ************************************************************************

    Dim SelectedMenuItem As ToolStripMenuItem
    Dim ActiveWindows As ArrayList
    Dim ThisFormHandle As RenaissanceFormHandle

    Try
      SelectedMenuItem = CType(sender, ToolStripMenuItem)
      SyncLock VeniceForms
        ActiveWindows = Me.VeniceForms.AllActiveItems

        ThisFormHandle = ActiveWindows(CInt(SelectedMenuItem.Tag))
        ThisFormHandle.Form.Activate()
      End SyncLock

    Catch ex As Exception
      LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error selecting 'Windows' Menu Item.", ex.StackTrace, True)
    End Try

  End Sub

  ''' <summary>
  ''' Delegate SetComboSelectionLengthsDelegate
  ''' </summary>
  ''' <param name="pForm">The p form.</param>
  Private Delegate Sub SetComboSelectionLengthsDelegate(ByRef pForm As Form)

  ''' <summary>
  ''' Sets the combo selection lengths.
  ''' </summary>
  ''' <param name="pForm">The p form.</param>
  Public Sub SetComboSelectionLengths(ByRef pForm As Form)
    ' ************************************************************************
    ' I have come accross an issue such that if an editable Combo box is used
    ' on a form, then when the form is drawn and GetFormData() is called, the
    ' text contents of the combo box appear as highlighted text. Most annoying.
    ' My initial solution involved setting the SelectionLength property to Zero
    ' which succeeded in clearing the text highlight, however it was found in
    ' operation that randomly and occasionally it would take several seconds for
    ' the "SelectionLength = 0" command to execute. Even More Annoying !
    ' Thus I have re-written this function to set focus to any appropriate Combos
    ' as this seems to be equally effective at clearing the highlighted text
    ' but will, I hope, avoid the aforementioned side affects.
    '
    ' ************************************************************************
    If pForm.InvokeRequired Then
      Dim d As New SetComboSelectionLengthsDelegate(AddressOf SetComboSelectionLengths)
      pForm.Invoke(d, New Object() {pForm})
      Exit Sub
    End If

    Dim thisControl As Control
    Dim thisCombo As ComboBox

    If (pForm Is Nothing) Then
      Exit Sub
    End If

    Try
      For Each thisControl In pForm.Controls
        If TypeOf thisControl Is ComboBox Then
          thisCombo = CType(thisControl, ComboBox)
          If thisCombo.DropDownStyle <> ComboBoxStyle.DropDownList Then
            thisCombo.Focus()
          End If
        End If
      Next
    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Generics the form resize handler.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Public Sub GenericFormResizeHandler(ByVal sender As Object, ByVal e As System.EventArgs)
    Try
      SetComboSelectionLengths(sender)
    Catch ex As Exception
    End Try
  End Sub

  ' **********************************************************************
  ' **********************************************************************
  ' 
  ' FORM Menu.
  '
  ' **********************************************************************
  ' **********************************************************************

  ''' <summary>
  ''' Handles the Click event of the Generic_FormMenu control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Generic_FormMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    ' **********************************************************************
    '
    ' Open New form of type relating to Menu Item's tag
    '
    ' **********************************************************************

    Dim thisFormID As VeniceFormID

    Try
      If (Me.Disposing) OrElse (Me.IsDisposed) Then
        Exit Sub
      End If

      If (TypeOf sender Is ToolStripMenuItem) Then
        Dim thisSender As ToolStripMenuItem
        thisSender = CType(sender, ToolStripMenuItem)

        If (thisSender.Tag Is Nothing) Then
          Exit Sub
        End If

        thisFormID = System.Enum.Parse(GetType(VeniceFormID), thisSender.Tag.ToString)

      ElseIf (TypeOf sender Is C1.Win.C1Command.C1Command) Then
        Dim thisSender As C1.Win.C1Command.C1Command

        thisSender = CType(sender, C1.Win.C1Command.C1Command)

        If (thisSender.UserData Is Nothing) Then
          Exit Sub
        End If

        thisFormID = System.Enum.Parse(GetType(VeniceFormID), thisSender.UserData.ToString)

      Else
        Exit Sub
      End If

      Call New_VeniceForm(thisFormID)

    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Handles the Click event of the Menu_File_ChangeConnection control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Menu_File_ChangeConnection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_File_ChangeConnection.Click
    ' **********************************************************************
    '
    ' Display a dialog to enable a user to change the connected Database.
    '
    ' **********************************************************************

    ' This routine was used to display a standard Database connection dialog
    ' box, this was sourced from the 'Microsoft OLE Database Servive Component IO Type library'
    ' Sadly this COM object is not strongly typed and thus created a number of compiler warnings.
    ' Whilst teh warnings were not serious, I have for the moment removed the reference and commented out this code.
    '
    ' Two solutions exist to this issue, 
    ' One, rebuild teh assembly with a strong name. See MS Article ID 313666
    ' Two, Write my own connection Dialog.
    '
    '

    'Dim Permissions As Integer

    'Permissions = CheckPermissions("SwitchODBCDSNlinks", PermissionFeatureType.TypeSystem)
    'If (Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) = 0 Then
    '  Call LogError(Me.Name & ", Menu_File_ChangeConnection_Click", LOG_LEVELS.Warning, "", "You do not have Permission to change the Database Connection.", "", True)
    '  Exit Sub
    'End If

    'Dim myConnection As SqlConnection

    'Dim Instance As DataLinksClass = New MSDASC.DataLinksClass
    'Dim connection As ConnectionClass = New ADODB.ConnectionClass

    'Dim Parameters() As String
    'Dim thisParameter As String
    'Dim ParameterName As String
    'Dim ParameterValue As String
    'Dim thisConnectString As String

    'myConnection = MainDataHandler.Get_Connection(VENICE_CONNECTION)
    'If (myConnection Is Nothing) Then
    '  connection.ConnectionString = "Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=Renaissance;Data Source=EXMSDB53"
    'Else
    '  ' Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=Renaissance;Data Source=EXMSDB53
    '  ' SERVER=EXMSDB53;DATABASE=Renaissance;Trusted_Connection=Yes

    '  ' Construct Connect String from existing Connection

    '  thisConnectString = myConnection.ConnectionString
    '  Parameters = myConnection.ConnectionString.Split(New Char() {";"})
    '  If (Not (Parameters Is Nothing)) AndAlso (Parameters.Length > 0) Then
    '    thisConnectString = "Provider=SQLOLEDB.1"

    '    For Each thisParameter In Parameters
    '      If InStr(thisParameter, "=") > 0 Then
    '        Try
    '          ParameterName = thisParameter.Substring(0, (InStr(thisParameter, "=") - 1))
    '          ParameterValue = thisParameter.Substring(InStr(thisParameter, "="))


    '          Select Case ParameterName.ToUpper
    '            Case "SERVER", "DATA SOURCE", "ADDRESS", "ADDR", "NETWORK ADDRESS"
    '              thisConnectString &= ";" & "Data Source=" & ParameterValue

    '            Case "INITIAL CATALOG", "DATABASE"
    '              thisConnectString &= ";" & "Initial Catalog=" & ParameterValue

    '            Case "INTEGRATED SECURITY", "TRUSTED_CONNECTION"
    '              thisConnectString &= ";" & "Integrated Security=" & ParameterValue

    '            Case "PERSIST SECURITY INFO"
    '              thisConnectString &= ";" & "Persist Security Info=" & ParameterValue

    '            Case "PROVIDER"

    '            Case Else
    '              thisConnectString &= ";" & thisParameter & "=" & ParameterValue

    '          End Select

    '        Catch ex As Exception
    '          Call LogError(Me.Name & ", Menu_File_ChangeConnection_Click", LOG_LEVELS.Error, ex.Message, "Error Changing Database Connection. ", ex.StackTrace, True)
    '          Exit Sub
    '        End Try

    '      End If

    '    Next
    '  End If

    '  connection.ConnectionString = thisConnectString
    'End If

    'If (Instance.PromptEdit(connection)) Then

    '  ' Construct appropriate SQLServer Connection string

    '  thisConnectString = connection.ConnectionString
    '  Parameters = thisConnectString.Split(New Char() {";"})

    '  If (Not (Parameters Is Nothing)) AndAlso (Parameters.Length > 0) Then
    '    thisConnectString = ""

    '    For Each thisParameter In Parameters
    '      If InStr(thisParameter, "=") > 0 Then
    '        Try
    '          ParameterName = thisParameter.Substring(0, (InStr(thisParameter, "=") - 1))
    '          ParameterValue = thisParameter.Substring(InStr(thisParameter, "="))


    '          Select Case ParameterName.ToUpper
    '            Case "SERVER", "DATA SOURCE", "ADDRESS", "ADDR", "NETWORK ADDRESS"
    '              thisConnectString &= ";" & "Data Source=" & ParameterValue

    '            Case "INITIAL CATALOG", "DATABASE"
    '              thisConnectString &= ";" & "Initial Catalog=" & ParameterValue

    '            Case "INTEGRATED SECURITY", "TRUSTED_CONNECTION"
    '              thisConnectString &= ";" & "Integrated Security=" & ParameterValue

    '            Case "PERSIST SECURITY INFO"
    '              thisConnectString &= ";" & "Persist Security Info=" & ParameterValue

    '            Case "PROVIDER"

    '            Case Else
    '              thisConnectString &= ";" & thisParameter & "=" & ParameterValue

    '          End Select

    '        Catch ex As Exception
    '          Call LogError(Me.Name & ", Menu_File_ChangeConnection_Click", LOG_LEVELS.Error, ex.Message, "Error Changing Database Connection. ", ex.StackTrace, True)
    '          Exit Sub
    '        End Try

    '      End If

    '    Next
    '  End If

    '  ' Apply new connection string 
    '  If thisConnectString.StartsWith(";") Then
    '    thisConnectString = thisConnectString.Substring(1, thisConnectString.Length - 1)
    '  End If

    '  If (myConnection Is Nothing) Then
    '    myConnection = MainDataHandler.Add_Connection(VENICE_CONNECTION, thisConnectString)
    '  Else
    '    Try
    '      myConnection.Close()
    '      myConnection.ConnectionString = thisConnectString
    '      myConnection.Open()

    '      Call Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.Connection))

    '    Catch ex As Exception
    '      Call LogError(Me.Name & ", Menu_File_ChangeConnection_Click", LOG_LEVELS.Error, ex.Message, "Error Changing Database Connection. ", ex.StackTrace, True)

    '    End Try

    '  End If

    '  If (Not (myConnection Is Nothing)) AndAlso ((myConnection.State And ConnectionState.Open) AND ConnectionState.Open) Then
    '    Me.VeniceStatusBar.Text = "Open : " & myConnection.DataSource & ", " & myConnection.Database
    '  Else
    '    Me.VeniceStatusBar.Text = "Connection failed."
    '  End If

    'End If


  End Sub

  ''' <summary>
  ''' Handles the Click event of the Menu_File_MDI control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Menu_File_MDI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_File_MDI.Click
    ' **********************************************************************
    '
    ' Toggle Venice as MDI vs N-n-MDI Application.
    '
    ' **********************************************************************

    Dim HandlesToDelete As New ArrayList

    If Menu_File_MDI.Checked = False Then
      Try
        Menu_File_MDI.Checked = True

        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
        Me.MaximizeBox = True
        Me.IsMdiContainer = True

        Dim thisFormHandle As RenaissanceGlobals.RenaissanceFormHandle
        For Each thisFormHandle In VeniceForms
          Try
            If (thisFormHandle.Form Is Nothing) Then
              HandlesToDelete.Add(thisFormHandle)
            ElseIf (thisFormHandle.FormType <> VeniceFormID.frmViewReport) AndAlso (thisFormHandle.FormType <> VeniceFormID.frmVeniceMain) Then

              Try
                thisFormHandle.Form.Hide()

                thisFormHandle.Form.MdiParent = Me
              Catch ex As Exception
              End Try

              If thisFormHandle.InUse Then
                thisFormHandle.Form.Show()
              End If
            End If
          Catch ex As Exception
          End Try
        Next
      Catch ex As Exception
      End Try
    Else
      Try
        Menu_File_MDI.Checked = False

        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False

        Dim thisFormHandle As RenaissanceGlobals.RenaissanceFormHandle
        For Each thisFormHandle In VeniceForms
          Try
            If (thisFormHandle.Form Is Nothing) Then
              HandlesToDelete.Add(thisFormHandle)

            Else

              thisFormHandle.Form.Hide()

              Try
                thisFormHandle.Form.MdiParent = Nothing
              Catch ex As Exception
              End Try

              If thisFormHandle.InUse Then
                thisFormHandle.Form.Show()
              End If

            End If

          Catch ex As Exception
          End Try
        Next

        Me.IsMdiContainer = False
        Me.Height = 196
        Me.Width = 884

      Catch ex As Exception
      End Try

    End If

    If HandlesToDelete.Count > 0 Then
      Dim DeleteCounter As Integer

      For DeleteCounter = 0 To (HandlesToDelete.Count - 1)
        Try
          VeniceForms.Remove(HandlesToDelete(DeleteCounter))
        Catch ex As Exception
        End Try
      Next

    End If

  End Sub

  ''' <summary>
  ''' Handles the Click event of the Menu_UseMultithreadedReporting control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Menu_UseMultithreadedReporting_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_UseMultithreadedReporting.Click
    Menu_UseMultithreadedReporting.Checked = Not Menu_UseMultithreadedReporting.Checked
    _UseReportWorkerThreads = Menu_UseMultithreadedReporting.Checked
  End Sub

  ''' <summary>
  ''' Handles the Click event of the Menu_File_EnableReportFadeIn control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Menu_File_EnableReportFadeIn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_File_EnableReportFadeIn.Click
    ' **********************************************************************
    '
    '
    ' **********************************************************************

    Try
      Menu_File_EnableReportFadeIn.Checked = (Not Menu_File_EnableReportFadeIn.Checked)
    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Handles the Click event of the Menu_File_Close control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Menu_File_Close_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_File_Close.Click
    ' **********************************************************************
    '
    ' **********************************************************************
    Dim FormHandle As RenaissanceGlobals.RenaissanceFormHandle
    Dim StdFormHandle As StandardVeniceForm

    While VeniceForms.Count > 0
      FormHandle = VeniceForms.Item(VeniceForms.Count - 1)
      VeniceForms.Remove(FormHandle)

      If TypeOf (FormHandle.Form) Is StandardVeniceForm Then
        StdFormHandle = FormHandle.Form
        StdFormHandle.CloseForm()
      Else
        FormHandle.Form.Close()
      End If
    End While

    Me.Close()
  End Sub

  ' **********************************************************************
  ' **********************************************************************
  ' 
  ' Static Data Menu.
  '
  ' **********************************************************************
  ' **********************************************************************


  ' **********************************************************************
  ' **********************************************************************
  ' 
  ' Transactions Menu.
  '
  ' **********************************************************************
  ' **********************************************************************


  ' **********************************************************************
  ' **********************************************************************
  ' 
  ' Prices Menu.
  '
  ' **********************************************************************
  ' **********************************************************************


  ' **********************************************************************
  ' **********************************************************************
  ' 
  ' Risk Menu.
  '
  ' **********************************************************************
  ' **********************************************************************

  ''' <summary>
  ''' Handles the Click event of the Menu_Risk_PolicyReport_MasterBalanced control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Menu_Risk_PolicyReport_MasterBalanced_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) 
    ' **********************************************************************
    ' Spawn new frmRiskPolicyReport.
    ' **********************************************************************

    Call Me.MainReportHandler.rptRiskPolicy(1, Me.VeniceProgressBar)

  End Sub

  ''' <summary>
  ''' Handles the Click event of the Menu_Risk_PolicyReport_MasterEvent control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Menu_Risk_PolicyReport_MasterEvent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) 
    ' **********************************************************************
    ' Spawn new frmRiskPolicyReport.
    ' **********************************************************************

    Call Me.MainReportHandler.rptRiskPolicy(13, Me.VeniceProgressBar)

  End Sub

  ''' <summary>
  ''' Handles the Click event of the Menu_Risk_PolicyReport_All control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Menu_Risk_PolicyReport_All_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) 
    ' **********************************************************************
    ' 
    ' **********************************************************************

    Call Me.MainReportHandler.rptRiskPolicy(0, Me.VeniceProgressBar)

  End Sub

  ' **********************************************************************
  ' **********************************************************************
  ' 
  ' Attributions Menu.
  '
  ' **********************************************************************
  ' **********************************************************************



  ''' <summary>
  ''' Handles the Click event of the Menu_Attributions_AssetAllocationReport control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Menu_Attributions_AssetAllocationReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) 
    ' **********************************************************************
    ' Spawn new frmProfitandLossReport.
    '
    ' I have combined the forms for the P&L and Asset Allocation (FCEV Board) reports, the choice of report
    ' to run is made via a Combo Box.
    '
    ' **********************************************************************

    Dim thisFormHandle As RenaissanceGlobals.RenaissanceFormHandle
    Dim thisPnLRptForm As frmProfitandLossReport

    thisFormHandle = New_VeniceForm(VeniceFormID.frmProfitandLossReport)
    If (Not (thisFormHandle Is Nothing)) AndAlso (Not (thisFormHandle.Form Is Nothing)) Then
      thisPnLRptForm = thisFormHandle.Form

      thisPnLRptForm.SelectedReport = 1 ' Asset Allocation report
    End If

  End Sub

  ''' <summary>
  ''' Handles the Click event of the Menu_Attributions_FundPerformanceReport control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Menu_Attributions_FundPerformanceReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) 
    ' **********************************************************************
    ' Spawn new frmProfitandLossReport.
    '
    ' I have combined the forms for the P&L and Asset Allocation (FCEV Board) reports, the choice of report
    ' to run is made via a Combo Box.
    '
    ' **********************************************************************

    Dim thisFormHandle As RenaissanceGlobals.RenaissanceFormHandle
    Dim thisAttributionRptForm As frmAttributionsReports

    thisFormHandle = New_VeniceForm(VeniceFormID.frmAttributionsReports)
    If (Not (thisFormHandle Is Nothing)) AndAlso (Not (thisFormHandle.Form Is Nothing)) Then
      thisAttributionRptForm = thisFormHandle.Form

      thisAttributionRptForm.SelectedReport = 7 ' Fund Performance Report
    End If

  End Sub

  ' **********************************************************************
  ' **********************************************************************
  ' 
  ' Reports Menu.
  '
  ' **********************************************************************
  ' **********************************************************************

  ''' <summary>
  ''' Handles the Click event of the Menu_Rpt_RiskPolicy_MasterBalanced control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Menu_Rpt_RiskPolicy_MasterBalanced_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) 
    ' **********************************************************************
    ' Spawn new frmRiskPolicyReport.
    ' **********************************************************************

    Call Me.MainReportHandler.rptRiskPolicy(1, Me.VeniceProgressBar)

  End Sub

  ''' <summary>
  ''' Handles the Click event of the Menu_Rpt_RiskPolicy_MasterEvent control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Menu_Rpt_RiskPolicy_MasterEvent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) 
    ' **********************************************************************
    ' Spawn new frmRiskPolicyReport.
    ' **********************************************************************

    Call Me.MainReportHandler.rptRiskPolicy(13, Me.VeniceProgressBar)

  End Sub

  ''' <summary>
  ''' Handles the Click event of the Menu_Rpt_RiskPolicy_All control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Menu_Rpt_RiskPolicy_All_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) 
    ' **********************************************************************
    ' Spawn new frmRiskPolicyReport.
    ' **********************************************************************

    Call Me.MainReportHandler.rptRiskPolicy(0, Me.VeniceProgressBar)

  End Sub

  ''' <summary>
  ''' Handles the Click event of the Menu_Reports_NoticeReport control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Menu_Reports_NoticeReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) 
    ' **********************************************************************
    ' Spawn new frmLiquidityReport.
    '
    ' I have combined the forms for the Liquidity and notice reports, the choice of report
    ' to run is made via a Combo Box. For the purposes of continuity, I have left the Notice Report
    ' menu item in place, but it simply starts the Liquidity Reports Form and changes the default
    ' for the report to run.
    ' **********************************************************************

    Dim thisFormHandle As RenaissanceGlobals.RenaissanceFormHandle
    Dim thisLiquidityRptForm As frmLiquidityReport

    thisFormHandle = New_VeniceForm(VeniceFormID.frmLiquidityReport)
    If (Not (thisFormHandle Is Nothing)) AndAlso (Not (thisFormHandle.Form Is Nothing)) Then
      thisLiquidityRptForm = thisFormHandle.Form

      thisLiquidityRptForm.ReportToRun = 1
    End If
  End Sub

  ''' <summary>
  ''' Handles the Click event of the Menu_Rpt_ReportData_SaveData control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Menu_Rpt_ReportData_SaveData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_Rpt_ReportData_SaveData.Click
    ' **********************************************************************
    '
    ' **********************************************************************

    Try
      Menu_Rpt_ReportData_SaveData.Checked = Not Menu_Rpt_ReportData_SaveData.Checked
    Catch ex As Exception
    End Try
  End Sub

  ' **********************************************************************
  ' **********************************************************************
  ' 
  ' KnowledgeDate Menu.
  '
  ' **********************************************************************
  ' **********************************************************************


  ''' <summary>
  ''' Handles the Click event of the Menu_KD_RefreshKD control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Menu_KD_RefreshKD_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_KD_RefreshKD.Click
    ' **********************************************************************
    ' Use the Connection Update Event to refresh all tables
    ' **********************************************************************

    Call Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.Connection))

  End Sub



  ' **********************************************************************
  ' **********************************************************************
  ' 
  ' Naples Menu.
  '
  ' **********************************************************************
  ' **********************************************************************


  ' **********************************************************************
  ' **********************************************************************
  ' 
  ' System Menu.
  '
  ' **********************************************************************
  ' **********************************************************************

  ''' <summary>
  ''' Sets the menu bar enabled.
  ''' </summary>
  Private Sub SetMenuBarEnabled()
    ' **********************************************************************
    ' 
    ' **********************************************************************

    Try
      If Me.VeniceMenu.Items.Count > 0 Then
        SetMenuItemEnabled(VeniceMenu.Items)
      End If
    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Sets the menu item enabled.
  ''' </summary>
  ''' <param name="ItemArray">The item array.</param>
  Private Sub SetMenuItemEnabled(ByRef ItemArray As ToolStripItemCollection)
    ' **********************************************************************
    ' 
    ' **********************************************************************

    Dim thisItem As ToolStripItem
    Dim thisMenuItem As ToolStripMenuItem
    Dim ThisFeatureName As String
    Dim ThisFeatureType As PermissionFeatureType

    For Each thisItem In ItemArray
      Try
        If (Not (thisItem Is Nothing)) Then
          If (TypeOf thisItem Is ToolStripMenuItem) Then
            thisMenuItem = CType(thisItem, ToolStripMenuItem)

            If (thisMenuItem.DropDownItems.Count > 0) Then
              SetMenuItemEnabled(thisMenuItem.DropDownItems)
            ElseIf (thisItem.Tag Is Nothing) Then
              thisItem.Visible = True
              thisItem.Enabled = True
            ElseIf (thisItem.Tag.ToString = "") Then
              thisItem.Visible = True
              thisItem.Enabled = True
            Else
              ThisFeatureName = thisItem.Tag.ToString
              If ThisFeatureName.StartsWith("rpt") Then
                ThisFeatureType = PermissionFeatureType.TypeReport
              Else
                ThisFeatureType = PermissionFeatureType.TypeForm
              End If

              If (CheckPermissions(ThisFeatureName, ThisFeatureType) > 0) Then
                thisItem.Visible = True
                thisItem.Enabled = True
              Else
                thisItem.Enabled = False
                thisItem.Visible = False ' Change to hide unpermissioned reports. NPP 5 Jun 2013
              End If
            End If

          End If

        End If

      Catch ex As Exception
      End Try
    Next
  End Sub

  ''' <summary>
  ''' Builds the standard form menu.
  ''' </summary>
  ''' <param name="RootMenu">The root menu.</param>
  ''' <param name="pThisTable">The p this table.</param>
  ''' <param name="SelectClick">The select click.</param>
  ''' <param name="SortClick">The sort click.</param>
  ''' <param name="AuditReportClick">The audit report click.</param>
  ''' <returns>MenuStrip.</returns>
  Public Function BuildStandardFormMenu( _
  ByRef RootMenu As MenuStrip, _
  ByVal pThisTable As DataTable, _
  ByRef SelectClick As System.EventHandler, _
  ByRef SortClick As System.EventHandler, _
  ByRef AuditReportClick As System.EventHandler) As MenuStrip

    ' **********************************************************************
    ' Standard routine to build the Standard Form menus - Select & Order
    ' **********************************************************************

    Dim ThisColumn As System.Data.DataColumn
    Dim SelectByMenuItem As New ToolStripMenuItem("&Select By")
    Dim OrderByItem As New ToolStripMenuItem("&Order By")
    Dim AuditReportItem As New ToolStripMenuItem("&Audit Reports")
    Dim newMenuItem As ToolStripMenuItem
    Dim Counter As Integer

    'SelectByMenuItem.Text = "&Select By"
    'OrderByItem.Text = "&Order By"
    'AuditReportItem.Text = "&Audit Reports"

    Counter = 0
    For Each ThisColumn In pThisTable.Columns

      If Not (SelectClick Is Nothing) Then
        newMenuItem = SelectByMenuItem.DropDownItems.Add(ThisColumn.ColumnName, Nothing, SelectClick)
        newMenuItem.Tag = Counter
      End If

      If Not (SortClick Is Nothing) Then
        newMenuItem = OrderByItem.DropDownItems.Add(ThisColumn.ColumnName, Nothing, SortClick)
        newMenuItem.Tag = Counter
      End If

      Counter += 1
    Next

    If Not (SelectClick Is Nothing) Then
      RootMenu.Items.Add(SelectByMenuItem)
    End If
    If Not (SortClick Is Nothing) Then
      RootMenu.Items.Add(OrderByItem)
    End If

    If Not (AuditReportClick Is Nothing) Then
      newMenuItem = AuditReportItem.DropDownItems.Add("This Record", Nothing, AuditReportClick)
      newMenuItem.Tag = 0

      'newMenuItem = AuditReportItem.DropDownItems.Add("All Records", Nothing, AuditReportClick)
      'newMenuItem.Tag = 1

      RootMenu.Items.Add(AuditReportItem)
    End If

    Return RootMenu

  End Function

#End Region

#Region " CheckPermissions()"

  ''' <summary>
  ''' Checks the permissions.
  ''' </summary>
  ''' <param name="PermissionArea">The permission area.</param>
  ''' <param name="PermissionType">Type of the permission.</param>
  ''' <returns>System.Int32.</returns>
  Public Function CheckPermissions(ByVal PermissionArea As String, ByVal PermissionType As RenaissanceGlobals.PermissionFeatureType) As Integer
    ' **********************************************************************
    ' Central Permissions checking Function.
    '
    ' Simply invokes the underlying SQL Strored procedure to check the available
    ' permissons of the current user.
    ' **********************************************************************

    Dim PermissionsCommand As New SqlCommand

    PermissionsCommand.CommandType = CommandType.StoredProcedure
    PermissionsCommand.CommandText = "[fn_Check_Permission]"
    PermissionsCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

    PermissionsCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UserName", System.Data.SqlDbType.VarChar, 100))
    PermissionsCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FunctionName", System.Data.SqlDbType.VarChar, 100))
    PermissionsCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FunctionType", System.Data.SqlDbType.Int, 4))
    PermissionsCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))

    PermissionsCommand.Parameters("@UserName").Value = DBNull.Value
    PermissionsCommand.Parameters("@FunctionName").Value = PermissionArea
    PermissionsCommand.Parameters("@FunctionType").Value = PermissionType

    Try
      PermissionsCommand.Connection = Me.GetVeniceConnection

      If (Not (PermissionsCommand.Connection Is Nothing)) Then
        SyncLock PermissionsCommand.Connection
          PermissionsCommand.ExecuteNonQuery()
        End SyncLock
      End If

    Catch ex As Exception
      Return 0

    Finally

      If (Not (PermissionsCommand.Connection Is Nothing)) Then
        Try
          PermissionsCommand.Connection.Close()
        Catch ex As Exception
        End Try
      End If
    End Try

    Return PermissionsCommand.Parameters("@RETURN_VALUE").Value

  End Function

#End Region

#Region " Standard Format checking handlers "

  ''' <summary>
  ''' Handles the NotNull event of the Text control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Public Sub Text_NotNull(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' **********************************************************************
    ' Standard File Format checking function.
    ' **********************************************************************

    Dim thisControl As System.Windows.Forms.Control
    Dim Parentform As StandardVeniceForm
    Dim ParentObject As Object

    'ParentObject = sender.TopLevelControl
    ParentObject = sender
    thisControl = Nothing

    While Not (TypeOf (ParentObject) Is StandardVeniceForm)
      Try
        thisControl = ParentObject
        ParentObject = thisControl.Parent
      Catch ex As Exception
        Exit Sub
      End Try
    End While

    Parentform = ParentObject

    If (Parentform.IsOverCancelButton = False) And (Parentform.IsInPaint = False) Then
      If thisControl.Text.Length = 0 Then
        MessageBox.Show("This field may not be left empty.", "Field Validation", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
        thisControl.Focus()
      End If
    End If
  End Sub





#End Region

#Region " Establish Standard table datasets "

  ''' <summary>
  ''' Delegate Main_MoveTableDelegate
  ''' </summary>
  ''' <param name="SourceTable">The source table.</param>
  ''' <param name="TargetTable">The target table.</param>
  Private Delegate Sub Main_MoveTableDelegate(ByVal SourceTable As DataTable, ByVal TargetTable As DataTable)

  ''' <summary>
  ''' Moves the table.
  ''' </summary>
  ''' <param name="SourceTable">The source table.</param>
  ''' <param name="TargetTable">The target table.</param>
  Friend Sub MoveTable(ByVal SourceTable As DataTable, ByVal TargetTable As DataTable)
    ' **********************************************************************
    ' Routine to move data from Source table to Target table.
    ' Called form the Background table loader.
    ' The reason that this is done, and done this way, is so that tables being used in the the 
    ' UI thread are not altered at exactly the same time on a background thread.
    ' This was causing 'invalid collection' type errors.
    ' 
    ' Also, the source table is cleared here rather than on the other thread because
    ' it seems that it might have been cleared (or disposed?) on the other thread before this one 
    ' finished using it.
    '
    ' Do not add DoEvents() in here! Could cause synclock issues.
    ' **********************************************************************
    Try
      If (TargetTable Is Nothing) OrElse (Me.IsClosing) Then
        Exit Sub
      End If

      ' Martial to UI Thread

      If Me.InvokeRequired Then
        Dim d As New Main_MoveTableDelegate(AddressOf MoveTable)
        ' Use non-blocking Invoke method.
        Me.BeginInvoke(d, New Object() {SourceTable, TargetTable})
        Exit Sub
      End If

      ' Copy Table.

      Call CopyTable(SourceTable, TargetTable)

      ' Clear Source.

      SourceTable.Rows.Clear()

    Catch ex As Exception
      Call LogError("MoveTable", LOG_LEVELS.Error, ex.Message, "Error Moving Table contents : " & TargetTable.TableName, ex.StackTrace, True)
    End Try
  End Sub

  ''' <summary>
  ''' Copies the table.
  ''' </summary>
  ''' <param name="SourceTable">The source table.</param>
  ''' <param name="TargetTable">The target table.</param>
  Friend Sub CopyTable(ByVal SourceTable As DataTable, ByVal TargetTable As DataTable)
    ' **********************************************************************
    ' Routine to copy data from Source table to Target table.
    '
    ' Do not add DoEvents() in here! Could cause synclock issues.
    ' **********************************************************************
    Try

      Dim RowCount As Integer

      TargetTable.Rows.Clear()

      If (TargetTable.DataSet IsNot Nothing) Then
        TargetTable.DataSet.EnforceConstraints = False
      End If

      If (SourceTable IsNot Nothing) Then
        For RowCount = 0 To (SourceTable.Rows.Count - 1)
          TargetTable.ImportRow(SourceTable.Rows(RowCount))
        Next
      End If

      If (TargetTable.DataSet IsNot Nothing) Then
        TargetTable.DataSet.EnforceConstraints = True
      End If

    Catch ex As Exception
      Call LogError("CopyTable", LOG_LEVELS.Error, ex.Message, "Error Copying Table contents : " & TargetTable.TableName, ex.StackTrace, True)
    End Try
  End Sub

  ''' <summary>
  ''' Load_s the table.
  ''' </summary>
  ''' <param name="pDatasetDetails">The p dataset details.</param>
  ''' <returns>DataSet.</returns>
  Public Function Load_Table(ByVal pDatasetDetails As RenaissanceGlobals.StandardDataset) As DataSet Implements RenaissanceGlobals.StandardRenaissanceMainForm.Load_Table
    ' Depricated, Don't use this function. Forms bad habits.
    ' Get Dataset, forcing a refresh if Noted IDs have changed.

    Return Load_Table(pDatasetDetails, "", pDatasetDetails.NotedIDsHaveChanged, True)
  End Function

  ''' <summary>
  ''' Load_s the table.
  ''' </summary>
  ''' <param name="pDatasetDetails">The p dataset details.</param>
  ''' <param name="pForceRefresh">if set to <c>true</c> [p force refresh].</param>
  ''' <returns>DataSet.</returns>
  Public Function Load_Table(ByVal pDatasetDetails As RenaissanceGlobals.StandardDataset, ByVal pForceRefresh As Boolean) As DataSet Implements RenaissanceGlobals.StandardRenaissanceMainForm.Load_Table
    Return Load_Table(pDatasetDetails, "", pForceRefresh, True)
  End Function

  ''' <summary>
  ''' Load_s the table.
  ''' </summary>
  ''' <param name="pDatasetDetails">The p dataset details.</param>
  ''' <param name="pForceRefresh">if set to <c>true</c> [p force refresh].</param>
  ''' <param name="pRaiseEvent">if set to <c>true</c> [p raise event].</param>
  ''' <returns>DataSet.</returns>
  Public Function Load_Table(ByVal pDatasetDetails As RenaissanceGlobals.StandardDataset, ByVal pForceRefresh As Boolean, ByVal pRaiseEvent As Boolean) As DataSet Implements RenaissanceGlobals.StandardRenaissanceMainForm.Load_Table
    Return Load_Table(pDatasetDetails, "", pForceRefresh, pRaiseEvent)
  End Function

  ''' <summary>
  ''' Load_s the table_ background.
  ''' </summary>
  ''' <param name="pDatasetDetails">The p dataset details.</param>
  ''' <param name="UpdateDetail">The update detail.</param>
  ''' <param name="pForceRefresh">if set to <c>true</c> [p force refresh].</param>
  ''' <param name="pRaiseEvent">if set to <c>true</c> [p raise event].</param>
  Public Sub Load_Table_Background(ByVal pDatasetDetails As RenaissanceGlobals.StandardDataset, ByVal UpdateDetail As String, ByVal pForceRefresh As Boolean, ByVal pRaiseEvent As Boolean)
    ' **********************************************************************
    '
    ' **********************************************************************

    Try
      If (_UseBackgroundTableLoader) Then
        If (UpdateDetail.Length > 0) Then
          Me.BackgroundTableLoaderProcess.EnqueueLoadTable(pDatasetDetails.ChangeID, New RenaissanceGlobals.RenaissanceUpdateEventArgs(pDatasetDetails.ChangeID, UpdateDetail), pForceRefresh)
        Else
          Me.BackgroundTableLoaderProcess.EnqueueLoadTable(pDatasetDetails.ChangeID, Nothing, pForceRefresh)
        End If
        If (pRaiseEvent) Then
          Call Me.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(pDatasetDetails.ChangeID, UpdateDetail), False)
        End If
      Else
        Load_Table(pDatasetDetails, UpdateDetail, pForceRefresh, pRaiseEvent)
      End If

    Catch ex As Exception

    End Try
  End Sub

  ''' <summary>
  ''' Load_s the table.
  ''' </summary>
  ''' <param name="pDatasetDetails">The p dataset details.</param>
  ''' <param name="pUpdateDetail">The p update detail.</param>
  ''' <param name="pForceRefresh">if set to <c>true</c> [p force refresh].</param>
  ''' <param name="pRaiseEvent">if set to <c>true</c> [p raise event].</param>
  ''' <returns>DataSet.</returns>
  Public Function Load_Table(ByVal pDatasetDetails As RenaissanceGlobals.StandardDataset, ByVal pUpdateDetail As String, ByVal pForceRefresh As Boolean, ByVal pRaiseEvent As Boolean) As DataSet Implements RenaissanceGlobals.StandardRenaissanceMainForm.Load_Table
    ' **********************************************************************
    ' Routine to Load a standard dataset or retrieve an existing one if it already
    ' exists.
    ' **********************************************************************

    Dim myDataset As DataSet
    Dim myConnection As SqlConnection
    Dim myAdaptor As SqlDataAdapter

    If (pDatasetDetails Is Nothing) Then
      Return Nothing
    End If

    Dim THIS_TABLENAME As String = pDatasetDetails.TableName
    Dim THIS_ADAPTORNAME As String = pDatasetDetails.Adaptorname
    Dim THIS_DATASETNAME As String = pDatasetDetails.DatasetName

    ' StandardDatasetNames

    myConnection = Me.MainDataHandler.Get_Connection(VENICE_CONNECTION)
    If myConnection Is Nothing Then
      Return Nothing
    End If

    myAdaptor = Me.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, VENICE_CONNECTION, THIS_TABLENAME)
    myDataset = Me.MainDataHandler.Get_Dataset(THIS_DATASETNAME)

    'AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
    'AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
    'AddHandler myAdaptor.FillError, AddressOf OnRowFillError

    'Lotfi : if forceRefresh free memory of existing dataset
    If myDataset IsNot Nothing Then
      If pForceRefresh Then

        Try
          If (myDataset.Tables.Count > 0) Then
            'myDataset.Tables(0).Rows.Clear()
            myDataset.Tables(0).Clear()
          End If
        Catch ex As Exception
        End Try
        myDataset = Nothing
      End If
    End If


    If myDataset Is Nothing Then

      myDataset = Me.MainDataHandler.Get_Dataset(THIS_DATASETNAME, True)

      Dim OrgText As String = ""
      OrgText = Me.VeniceStatusBar.Text

      SetToolStripText(VeniceStatusBar, "Loading Table " & pDatasetDetails.TableName)

      SyncLock myAdaptor.SelectCommand.Connection
        SyncLock myDataset

          Try
            If (myDataset.Tables.Count > 0) Then
              myDataset.Tables(0).Clear()
            End If
          Catch ex As Exception
          End Try
          Try
            If (myAdaptor.SelectCommand.Parameters.Contains("@Knowledgedate")) Then
              myAdaptor.SelectCommand.Parameters("@Knowledgedate").Value = Me.Main_Knowledgedate
            End If
          Catch ex As Exception
          End Try

          Try

            If (myDataset.Tables.Count > 0) Then

              If Not (Me.InvokeRequired) Then
                Me.VeniceStatusStrip.Refresh()
                'Application.DoEvents()
              End If

              myDataset.EnforceConstraints = False

              Try
                myAdaptor.Fill(myDataset.Tables(0))
              Catch ex As Exception
                myAdaptor.SelectCommand.Connection.Close()
                SqlConnection.ClearPool(myAdaptor.SelectCommand.Connection)
                myAdaptor.SelectCommand.Connection.Open()

                myDataset.Tables(0).Clear()
                myAdaptor.Fill(myDataset.Tables(0))
              End Try

              myDataset.EnforceConstraints = True

            Else

              If Not (Me.InvokeRequired) Then
                Me.VeniceStatusStrip.Refresh()
                'Application.DoEvents()
              End If

              myDataset.EnforceConstraints = False

              Try
                myAdaptor.Fill(myDataset, pDatasetDetails.TableName)
              Catch ex As Exception
                myAdaptor.SelectCommand.Connection.Close()
                SqlConnection.ClearPool(myAdaptor.SelectCommand.Connection)
                myAdaptor.SelectCommand.Connection.Open()

                myDataset.Tables(pDatasetDetails.TableName).Clear()
                myAdaptor.Fill(myDataset, pDatasetDetails.TableName)
              End Try

              myDataset.EnforceConstraints = True

            End If

            'Lotfi : force garbage collection to free memory
            ' commented after that because causes a stuck when opening risk menus forms !!
            GC.Collect()

            If (Extra_Debug) Then
              Call LogError(Me.Name & ", Load_Table", LOG_LEVELS.Audit, "", "Filling Table " & myDataset.Tables(0).TableName, "", False)
            End If

          Catch ex As Exception

            If (myDataset.Tables.Count > 0) Then
              Call LogError(Me.Name & ", Load_Table", LOG_LEVELS.Error, ex.Message, "Error Filling Table " & myDataset.Tables(0).TableName, ex.StackTrace, True)
            Else
              Call LogError(Me.Name & ", Load_Table", LOG_LEVELS.Error, ex.Message, "Error Filling Table ", ex.StackTrace, True)
            End If

          Finally

            SetToolStripText(VeniceStatusBar, OrgText)

          End Try

        End SyncLock
      End SyncLock


      ' Reset the IDs have Changed Flag, as the table has been re-loaded.
      pDatasetDetails.NotedIDsHaveChanged = False

    ElseIf (pForceRefresh = True) Or (pDatasetDetails.NotedIDsHaveChanged = True) Then

      Dim DoPartialUpdate As Boolean = True
      Dim UpdateString As String = ""
      Dim StringArray(-1) As String
      Dim UpdateCounter As Integer = 0

      ' Establish if a partial update is possible.

      If (pUpdateDetail Is Nothing) Then
        ' Partial Update is not possible
        DoPartialUpdate = False

      ElseIf (myAdaptor.SelectCommand.Parameters.Contains("@AuditID") = False) Then
        ' Partial Update is not possible
        DoPartialUpdate = False

      Else
        UpdateString = pUpdateDetail

        If (UpdateString.Length <= 0) Then
          DoPartialUpdate = False
        Else
          StringArray = UpdateString.Split(New Char() {","c, "|"c}, StringSplitOptions.RemoveEmptyEntries)

          If (StringArray.Length > 0) AndAlso (StringArray.Length <= PARTIAL_UPDATE_TABLE_ITEM_LIMIT) Then  ' Arbitrary limit of XX items, after which a complete update makes as much sense.

            ' Validate, To be expanded upon, as necessary.

            Select Case pDatasetDetails.ChangeID


              Case Else
                ' Just check all entries are Numeric

                For UpdateCounter = 0 To (StringArray.Length - 1)

                  If (ConvertIsNumeric(StringArray(UpdateCounter)) = False) OrElse (ConvertValue(StringArray(UpdateCounter), GetType(Integer)) = 0) Then
                    DoPartialUpdate = False
                  End If

                Next

            End Select

          Else
            DoPartialUpdate = False
          End If

        End If
      End If

      SyncLock myAdaptor.SelectCommand.Connection
        SyncLock myDataset

          If (DoPartialUpdate) Then

            ' Try partial update.
            ' note, this uses a new Adaptor.

            Dim SelectedRows() As DataRow
            Dim thisRow As DataRow
            Dim RowCounter As Integer
            Dim ColumnCount As Integer
            Dim tempDataset As DataSet = Nothing
            Dim TempTable As DataTable

            Try

              myConnection = GetVeniceConnection()
              myAdaptor = New SqlDataAdapter
              MainAdaptorHandler.Set_AdaptorCommands(myConnection, myAdaptor, THIS_TABLENAME)

              tempDataset = MainDataHandler.Instantiate_Dataset(THIS_DATASETNAME)
              TempTable = tempDataset.Tables(0)

              If (Extra_Debug) Then
                Call LogError(Me.Name & ", Load_Table", LOG_LEVELS.Audit, "", "Partial Update,  " & THIS_TABLENAME & " : " & UpdateString, "", False)
              End If

              Try
                If (myAdaptor.SelectCommand.Parameters.Contains("@Knowledgedate")) Then
                  myAdaptor.SelectCommand.Parameters("@Knowledgedate").Value = Me.Main_Knowledgedate
                End If
              Catch ex As Exception
              End Try

              For UpdateCounter = 0 To (StringArray.Length - 1)

                Select Case pDatasetDetails.ChangeID

                  Case Else
                    ' Default

                    myAdaptor.SelectCommand.Parameters("@AuditID").Value = ConvertValue(StringArray(UpdateCounter), GetType(Integer))

                End Select

                TempTable = tempDataset.Tables(0)
                TempTable.Clear()

                Try
                  myAdaptor.Fill(TempTable)
                Catch ex As Exception
                  myAdaptor.SelectCommand.Connection.Close()
                  SqlConnection.ClearPool(myAdaptor.SelectCommand.Connection)
                  myAdaptor.SelectCommand.Connection.Open()

                  TempTable.Clear()
                  myAdaptor.Fill(TempTable)
                End Try

                ' Merge values in...

                If (TempTable.Rows.Count = 0) Then

                  ' Row Deleted....

                  SelectedRows = myDataset.Tables(0).Select("AuditID=" & StringArray(UpdateCounter))

                  If (SelectedRows.Length > 0) Then

                    Try
                      For RowCounter = 0 To (SelectedRows.Length - 1)
                        thisRow = SelectedRows(RowCounter)
                        myDataset.Tables(0).Rows.Remove(thisRow)
                      Next
                    Catch ex As Exception
                    End Try

                  End If

                Else ' (TempTable.Rows.Count > 0)
                  ' Add / Merge new Row(s).

                  Try
                    myDataset.Tables(0).BeginLoadData()

                    For RowCounter = 0 To (TempTable.Rows.Count - 1)

                      thisRow = TempTable.Rows(RowCounter)

                      Select Case pDatasetDetails.ChangeID

                        Case Else
                          ' Default.

                          SelectedRows = myDataset.Tables(0).Select("AuditID=" & thisRow("AuditID").ToString())

                      End Select


                      If (SelectedRows Is Nothing) OrElse (SelectedRows.Length <= 0) Then
                        ' Add

                        myDataset.Tables(0).ImportRow(thisRow)
                      Else
                        ' Merge
                        Dim tempItemArray() As Object = thisRow.ItemArray

                        ' Check for ReadOnly Columns (try not to have any !!!)
                        For ColumnCount = 0 To (myDataset.Tables(0).Columns.Count - 1)
                          If (myDataset.Tables(0).Columns(ColumnCount).ReadOnly) Then
                            tempItemArray(myDataset.Tables(0).Columns(ColumnCount).Ordinal) = Nothing
                          End If
                        Next

                        SelectedRows(0).BeginEdit()

                        SelectedRows(0).ItemArray = tempItemArray

                        SelectedRows(0).EndEdit()
                        SelectedRows(0).AcceptChanges()

                      End If


                    Next

                  Catch ex As Exception
                  Finally
                    myDataset.Tables(0).EndLoadData()
                  End Try

                End If

                'Lotfi : free memory
                If TempTable IsNot Nothing Then
                  TempTable.Clear()
                  TempTable = Nothing
                End If


              Next ' UpdateCounter

            Catch ex As Exception

            End Try

          Else ' Not DoPartialUpdate
            '  Full table update.

            Try
              Try
                If (myDataset.Tables.Count > 0) Then
                  myDataset.Tables(0).Clear()
                End If
              Catch ex As Exception
              End Try

              If (Extra_Debug) Then
                Call LogError(Me.Name & ", Load_Table", LOG_LEVELS.Audit, "", "Full Update,  " & THIS_TABLENAME, "", False)
              End If

              Try
                If (myAdaptor.SelectCommand.Parameters.Contains("@Knowledgedate")) Then
                  myAdaptor.SelectCommand.Parameters("@Knowledgedate").Value = Me.Main_Knowledgedate
                End If
              Catch ex As Exception
              End Try

              myDataset.EnforceConstraints = False
              If (myDataset.Tables.Count > 0) Then

                Try
                  myAdaptor.Fill(myDataset.Tables(0))
                Catch ex As Exception
                  myAdaptor.SelectCommand.Connection.Close()
                  SqlConnection.ClearPool(myAdaptor.SelectCommand.Connection)
                  myAdaptor.SelectCommand.Connection.Open()

                  myDataset.Tables(0).Clear()
                  myAdaptor.Fill(myDataset.Tables(0))
                End Try

              Else

                Try
                  myAdaptor.Fill(myDataset, pDatasetDetails.TableName)
                Catch ex As Exception
                  myAdaptor.SelectCommand.Connection.Close()
                  SqlConnection.ClearPool(myAdaptor.SelectCommand.Connection)
                  myAdaptor.SelectCommand.Connection.Open()

                  myDataset.Tables(pDatasetDetails.TableName).Clear()
                  myAdaptor.Fill(myDataset, pDatasetDetails.TableName)
                End Try

              End If
              myDataset.EnforceConstraints = True

              'Lotfi : force garbage collection to free memory
              ' commented after that because causes a stuck when opening risk menus forms !!
              GC.Collect()

              ' Reset the IDs have Changed Flag, as the table has been re-loaded.
              pDatasetDetails.NotedIDsHaveChanged = False

              If (pRaiseEvent = True) Then
                If (UpdateString.Length = 0) Then
                  Call Me.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(pDatasetDetails.ChangeID), False)
                Else
                  Call Me.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(pDatasetDetails.ChangeID, UpdateString), False)
                End If
              End If
            Catch ex As Exception
              Call LogError(Me.Name & ", Load_Table", LOG_LEVELS.Error, ex.Message, "Error Filling (Refresh) Table " & myDataset.Tables(0).TableName, ex.StackTrace, True)
            End Try

          End If

        End SyncLock
      End SyncLock

      ' Reset the IDs have Changed Flag, as the table has been re-loaded.
      pDatasetDetails.NotedIDsHaveChanged = False
    End If



    Return myDataset
  End Function

  ''' <summary>
  ''' Load_s the table.
  ''' </summary>
  ''' <param name="pDatasetDetails">The p dataset details.</param>
  ''' <param name="pUpdateDetail">The p update detail.</param>
  ''' <param name="pForceRefresh">if set to <c>true</c> [p force refresh].</param>
  ''' <param name="pRaiseEvent">if set to <c>true</c> [p raise event].</param>
  ''' <returns>DataSet.</returns>
  Public Function Load_Table_ManageMemory(ByVal pDatasetDetails As RenaissanceGlobals.StandardDataset, ByVal pUpdateDetail As String, ByVal pForceRefresh As Boolean, ByVal pRaiseEvent As Boolean) As DataSet _
  'Implements RenaissanceGlobals.StandardRenaissanceMainForm.Load_Table
    ' **********************************************************************
    ' Routine to Load a standard dataset or retrieve an existing one if it already
    ' exists.
    ' **********************************************************************

    Dim myDataset As DataSet
    Dim myConnection As SqlConnection
    Dim myAdaptor As SqlDataAdapter

    If (pDatasetDetails Is Nothing) Then
      Return Nothing
    End If

    Dim THIS_TABLENAME As String = pDatasetDetails.TableName
    Dim THIS_ADAPTORNAME As String = pDatasetDetails.Adaptorname
    Dim THIS_DATASETNAME As String = pDatasetDetails.DatasetName

    ' StandardDatasetNames

    myConnection = Me.MainDataHandler.Get_Connection(VENICE_CONNECTION)
    If myConnection Is Nothing Then
      Return Nothing
    End If

    myAdaptor = Me.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, VENICE_CONNECTION, THIS_TABLENAME)
    myDataset = Me.MainDataHandler.Get_Dataset(THIS_DATASETNAME)

    'AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
    'AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
    'AddHandler myAdaptor.FillError, AddressOf OnRowFillError

    'Lotfi : if forceRefresh free memory of existing dataset
    If myDataset IsNot Nothing Then
      If pForceRefresh Then

        Try
          If (myDataset.Tables.Count > 0) Then
            'myDataset.Tables(0).Rows.Clear()
            myDataset.Tables(0).Clear()
          End If
        Catch ex As Exception
        End Try
        myDataset = Nothing
      End If
    End If


    If myDataset Is Nothing Then

      myDataset = Me.MainDataHandler.Get_Dataset(THIS_DATASETNAME, True)

      Dim OrgText As String = ""
      OrgText = Me.VeniceStatusBar.Text

      SetToolStripText(VeniceStatusBar, "Loading Table " & pDatasetDetails.TableName)

      SyncLock myAdaptor.SelectCommand.Connection
        SyncLock myDataset

          Try
            If (myDataset.Tables.Count > 0) Then
              myDataset.Tables(0).Clear()
            End If
          Catch ex As Exception
          End Try
          Try
            If (myAdaptor.SelectCommand.Parameters.Contains("@Knowledgedate")) Then
              myAdaptor.SelectCommand.Parameters("@Knowledgedate").Value = Me.Main_Knowledgedate
            End If
          Catch ex As Exception
          End Try

          Try

            If (myDataset.Tables.Count > 0) Then

              If Not (Me.InvokeRequired) Then
                Me.VeniceStatusStrip.Refresh()
                'Application.DoEvents()
              End If

              myDataset.EnforceConstraints = False

              Try
                myAdaptor.Fill(myDataset.Tables(0))
              Catch ex As Exception
                myAdaptor.SelectCommand.Connection.Close()
                SqlConnection.ClearPool(myAdaptor.SelectCommand.Connection)
                myAdaptor.SelectCommand.Connection.Open()

                myDataset.Tables(0).Clear()
                myAdaptor.Fill(myDataset.Tables(0))
              End Try

              myDataset.EnforceConstraints = True

            Else

              If Not (Me.InvokeRequired) Then
                Me.VeniceStatusStrip.Refresh()
                'Application.DoEvents()
              End If

              myDataset.EnforceConstraints = False

              Try
                myAdaptor.Fill(myDataset, pDatasetDetails.TableName)
              Catch ex As Exception
                myAdaptor.SelectCommand.Connection.Close()
                SqlConnection.ClearPool(myAdaptor.SelectCommand.Connection)
                myAdaptor.SelectCommand.Connection.Open()

                myDataset.Tables(pDatasetDetails.TableName).Clear()
                myAdaptor.Fill(myDataset, pDatasetDetails.TableName)
              End Try

              myDataset.EnforceConstraints = True

            End If

            If (Extra_Debug) Then
              Call LogError(Me.Name & ", Load_Table", LOG_LEVELS.Audit, "", "Filling Table " & myDataset.Tables(0).TableName, "", False)
            End If

          Catch ex As Exception

            If (myDataset.Tables.Count > 0) Then
              Call LogError(Me.Name & ", Load_Table", LOG_LEVELS.Error, ex.Message, "Error Filling Table " & myDataset.Tables(0).TableName, ex.StackTrace, True)
            Else
              Call LogError(Me.Name & ", Load_Table", LOG_LEVELS.Error, ex.Message, "Error Filling Table ", ex.StackTrace, True)
            End If

          Finally

            SetToolStripText(VeniceStatusBar, OrgText)

          End Try

        End SyncLock
      End SyncLock


      ' Reset the IDs have Changed Flag, as the table has been re-loaded.
      pDatasetDetails.NotedIDsHaveChanged = False

    ElseIf (pForceRefresh = True) Or (pDatasetDetails.NotedIDsHaveChanged = True) Then

      Dim DoPartialUpdate As Boolean = True
      Dim UpdateString As String = ""
      Dim StringArray(-1) As String
      Dim UpdateCounter As Integer = 0

      ' Establish if a partial update is possible.

      If (pUpdateDetail Is Nothing) Then
        ' Partial Update is not possible
        DoPartialUpdate = False

      ElseIf (myAdaptor.SelectCommand.Parameters.Contains("@AuditID") = False) Then
        ' Partial Update is not possible
        DoPartialUpdate = False

      Else
        UpdateString = pUpdateDetail

        If (UpdateString.Length <= 0) Then
          DoPartialUpdate = False
        Else
          StringArray = UpdateString.Split(New Char() {","c, "|"c}, StringSplitOptions.RemoveEmptyEntries)

          If (StringArray.Length > 0) AndAlso (StringArray.Length <= PARTIAL_UPDATE_TABLE_ITEM_LIMIT) Then  ' Arbitrary limit of XX items, after which a complete update makes as much sense.

            ' Validate, To be expanded upon, as necessary.

            Select Case pDatasetDetails.ChangeID


              Case Else
                ' Just check all entries are Numeric

                For UpdateCounter = 0 To (StringArray.Length - 1)

                  If (ConvertIsNumeric(StringArray(UpdateCounter)) = False) OrElse (ConvertValue(StringArray(UpdateCounter), GetType(Integer)) = 0) Then
                    DoPartialUpdate = False
                  End If

                Next

            End Select

          Else
            DoPartialUpdate = False
          End If

        End If
      End If

      SyncLock myAdaptor.SelectCommand.Connection
        SyncLock myDataset

          If (DoPartialUpdate) Then

            ' Try partial update.
            ' note, this uses a new Adaptor.

            Dim SelectedRows() As DataRow
            Dim thisRow As DataRow
            Dim RowCounter As Integer
            Dim ColumnCount As Integer
            Dim tempDataset As DataSet = Nothing
            Dim TempTable As DataTable

            Try

              myConnection = GetVeniceConnection()
              myAdaptor = New SqlDataAdapter
              MainAdaptorHandler.Set_AdaptorCommands(myConnection, myAdaptor, THIS_TABLENAME)

              tempDataset = MainDataHandler.Instantiate_Dataset(THIS_DATASETNAME)
              TempTable = tempDataset.Tables(0)

              If (Extra_Debug) Then
                Call LogError(Me.Name & ", Load_Table", LOG_LEVELS.Audit, "", "Partial Update,  " & THIS_TABLENAME & " : " & UpdateString, "", False)
              End If

              Try
                If (myAdaptor.SelectCommand.Parameters.Contains("@Knowledgedate")) Then
                  myAdaptor.SelectCommand.Parameters("@Knowledgedate").Value = Me.Main_Knowledgedate
                End If
              Catch ex As Exception
              End Try

              For UpdateCounter = 0 To (StringArray.Length - 1)

                Select Case pDatasetDetails.ChangeID

                  Case Else
                    ' Default

                    myAdaptor.SelectCommand.Parameters("@AuditID").Value = ConvertValue(StringArray(UpdateCounter), GetType(Integer))

                End Select

                TempTable = tempDataset.Tables(0)
                TempTable.Clear()

                Try
                  myAdaptor.Fill(TempTable)
                Catch ex As Exception
                  myAdaptor.SelectCommand.Connection.Close()
                  SqlConnection.ClearPool(myAdaptor.SelectCommand.Connection)
                  myAdaptor.SelectCommand.Connection.Open()

                  TempTable.Clear()
                  myAdaptor.Fill(TempTable)
                End Try

                ' Merge values in...

                If (TempTable.Rows.Count = 0) Then

                  ' Row Deleted....

                  SelectedRows = myDataset.Tables(0).Select("AuditID=" & StringArray(UpdateCounter))

                  If (SelectedRows.Length > 0) Then

                    Try
                      For RowCounter = 0 To (SelectedRows.Length - 1)
                        thisRow = SelectedRows(RowCounter)
                        myDataset.Tables(0).Rows.Remove(thisRow)
                      Next
                    Catch ex As Exception
                    End Try

                  End If

                Else ' (TempTable.Rows.Count > 0)
                  ' Add / Merge new Row(s).

                  Try
                    myDataset.Tables(0).BeginLoadData()

                    For RowCounter = 0 To (TempTable.Rows.Count - 1)

                      thisRow = TempTable.Rows(RowCounter)

                      Select Case pDatasetDetails.ChangeID

                        Case Else
                          ' Default.

                          SelectedRows = myDataset.Tables(0).Select("AuditID=" & thisRow("AuditID").ToString())

                      End Select


                      If (SelectedRows Is Nothing) OrElse (SelectedRows.Length <= 0) Then
                        ' Add

                        myDataset.Tables(0).ImportRow(thisRow)
                      Else
                        ' Merge
                        Dim tempItemArray() As Object = thisRow.ItemArray

                        ' Check for ReadOnly Columns (try not to have any !!!)
                        For ColumnCount = 0 To (myDataset.Tables(0).Columns.Count - 1)
                          If (myDataset.Tables(0).Columns(ColumnCount).ReadOnly) Then
                            tempItemArray(myDataset.Tables(0).Columns(ColumnCount).Ordinal) = Nothing
                          End If
                        Next

                        SelectedRows(0).BeginEdit()

                        SelectedRows(0).ItemArray = tempItemArray

                        SelectedRows(0).EndEdit()
                        SelectedRows(0).AcceptChanges()

                      End If


                    Next

                  Catch ex As Exception
                  Finally
                    myDataset.Tables(0).EndLoadData()
                  End Try

                End If

                'Lotfi : free memory
                If TempTable IsNot Nothing Then
                  TempTable.Clear()
                  TempTable = Nothing
                End If


              Next ' UpdateCounter

            Catch ex As Exception

            End Try

          Else ' Not DoPartialUpdate
            '  Full table update.

            Try
              Try
                If (myDataset.Tables.Count > 0) Then
                  myDataset.Tables(0).Clear()
                End If
              Catch ex As Exception
              End Try

              If (Extra_Debug) Then
                Call LogError(Me.Name & ", Load_Table", LOG_LEVELS.Audit, "", "Full Update,  " & THIS_TABLENAME, "", False)
              End If

              Try
                If (myAdaptor.SelectCommand.Parameters.Contains("@Knowledgedate")) Then
                  myAdaptor.SelectCommand.Parameters("@Knowledgedate").Value = Me.Main_Knowledgedate
                End If
              Catch ex As Exception
              End Try

              myDataset.EnforceConstraints = False
              If (myDataset.Tables.Count > 0) Then

                Try
                  myAdaptor.Fill(myDataset.Tables(0))
                Catch ex As Exception
                  myAdaptor.SelectCommand.Connection.Close()
                  SqlConnection.ClearPool(myAdaptor.SelectCommand.Connection)
                  myAdaptor.SelectCommand.Connection.Open()

                  myDataset.Tables(0).Clear()
                  myAdaptor.Fill(myDataset.Tables(0))
                End Try

              Else

                Try
                  myAdaptor.Fill(myDataset, pDatasetDetails.TableName)
                Catch ex As Exception
                  myAdaptor.SelectCommand.Connection.Close()
                  SqlConnection.ClearPool(myAdaptor.SelectCommand.Connection)
                  myAdaptor.SelectCommand.Connection.Open()

                  myDataset.Tables(pDatasetDetails.TableName).Clear()
                  myAdaptor.Fill(myDataset, pDatasetDetails.TableName)
                End Try

              End If
              myDataset.EnforceConstraints = True

              ' Reset the IDs have Changed Flag, as the table has been re-loaded.
              pDatasetDetails.NotedIDsHaveChanged = False

              If (pRaiseEvent = True) Then
                If (UpdateString.Length = 0) Then
                  Call Me.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(pDatasetDetails.ChangeID), False)
                Else
                  Call Me.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(pDatasetDetails.ChangeID, UpdateString), False)
                End If
              End If
            Catch ex As Exception
              Call LogError(Me.Name & ", Load_Table", LOG_LEVELS.Error, ex.Message, "Error Filling (Refresh) Table " & myDataset.Tables(0).TableName, ex.StackTrace, True)
            End Try

          End If

        End SyncLock
      End SyncLock

      ' Reset the IDs have Changed Flag, as the table has been re-loaded.
      pDatasetDetails.NotedIDsHaveChanged = False
    End If

    'Lotfi : force garbage collection
    ' commented after that because causes a stuck when openingg risk menus forms !!
    'GC.Collect()


    Return myDataset
  End Function

  ''' <summary>
  ''' Reloads the table.
  ''' </summary>
  ''' <param name="pTableID">The p table ID.</param>
  ''' <param name="pUpdateString">The p update string.</param>
  ''' <param name="pRaiseEvent">if set to <c>true</c> [p raise event].</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function ReloadTable(ByVal pTableID As RenaissanceChangeID, ByVal pUpdateString As String, ByVal pRaiseEvent As Boolean) As Boolean
    ' **********************************************************************
    ' Function to Re-Load a standard dataset if it is already loaded.
    '
    ' If the dataset is not already loaded, then the call to 'Get_Dataset' will return
    ' Nothing since insufficient of the constructor parameters are given.
    ' If a dataset IS returned, then the Load_Dataset funcion is used to refresh it.
    ' **********************************************************************
    Dim ThisStandardDataset As RenaissanceGlobals.StandardDataset = RenaissanceStandardDatasets.GetStandardDataset(pTableID)
    Dim myDataset As DataSet

    Try
      If (ThisStandardDataset IsNot Nothing) Then
        myDataset = Me.MainDataHandler.Get_Dataset(ThisStandardDataset.DatasetName)
        If (myDataset IsNot Nothing) Then
          If (Load_Table(ThisStandardDataset, pUpdateString, True, pRaiseEvent) Is Nothing) Then
            Return False
          Else
            Return True
          End If
        ElseIf pRaiseEvent Then
          ' If the table is not loaded, then just raise the event (If requested)

          If (pUpdateString IsNot Nothing) AndAlso (pUpdateString.Length > 0) Then
            Call Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(pTableID, pUpdateString))
          Else
            Call Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(pTableID))
          End If
          Return False
        End If
      End If
    Catch ex As Exception
    End Try

    Return False
  End Function

  ''' <summary>
  ''' Reloads the table_ background.
  ''' </summary>
  ''' <param name="pTableID">The p table ID.</param>
  ''' <param name="UpdateDetail">The update detail.</param>
  ''' <param name="pRaiseEvent">if set to <c>true</c> [p raise event].</param>
  Public Sub ReloadTable_Background(ByVal pTableID As RenaissanceChangeID, ByVal UpdateDetail As String, ByVal pRaiseEvent As Boolean)
    ' **********************************************************************
    ' Function to Re-Load a standard dataset if it is already loaded.
    '
    ' If the dataset is not already loaded, then the call to 'Get_Dataset' will return
    ' Nothing since insufficient of the constructor parameters are given.
    ' If a dataset IS returned, then the Load_Dataset funcion is used to refresh it.
    ' **********************************************************************
    Dim ThisStandardDataset As RenaissanceGlobals.StandardDataset = RenaissanceStandardDatasets.GetStandardDataset(pTableID)
    Dim myDataset As DataSet

    Try
      If (ThisStandardDataset IsNot Nothing) Then
        myDataset = Me.MainDataHandler.Get_Dataset(ThisStandardDataset.DatasetName)

        If (myDataset IsNot Nothing) Then
          Call Load_Table_Background(ThisStandardDataset, UpdateDetail, True, pRaiseEvent)
          Exit Sub
        ElseIf pRaiseEvent Then
          ' If the table is not loaded, then just raise the event (If requested)

          Call Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(pTableID, UpdateDetail), True)
          Exit Sub
        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Delegate Load_Table_Delegate
  ''' </summary>
  ''' <param name="pDatasetDetails">The p dataset details.</param>
  ''' <param name="pForceRefresh">if set to <c>true</c> [p force refresh].</param>
  ''' <param name="pRaiseEvent">if set to <c>true</c> [p raise event].</param>
  ''' <returns>DataSet.</returns>
  Public Delegate Function Load_Table_Delegate(ByVal pDatasetDetails As RenaissanceGlobals.StandardDataset, ByVal pForceRefresh As Boolean, ByVal pRaiseEvent As Boolean) As DataSet

#End Region

#Region " Standard 'Set' Functions : Combos, Tooltips etc. "

  ''' <summary>
  ''' Sets the TBL generic combo.
  ''' </summary>
  ''' <param name="pCombo">The p combo.</param>
  ''' <param name="pDataRows">The p data rows.</param>
  ''' <param name="pDisplayMember">The p display member.</param>
  ''' <param name="pValueMember">The p value member.</param>
  ''' <param name="pSelectString">The p select string.</param>
  ''' <param name="pSELECTDISTINCT">if set to <c>true</c> [p SELECTDISTINCT].</param>
  ''' <param name="pOrderAscending">if set to <c>true</c> [p order ascending].</param>
  ''' <param name="pLeadingBlank">if set to <c>true</c> [p leading blank].</param>
  ''' <param name="pLeadingBlankValue">The p leading blank value.</param>
  ''' <param name="pLeadingBlankText">The p leading blank text.</param>
  Public Sub SetTblGenericCombo(ByVal pCombo As System.Windows.Forms.ComboBox, ByVal pDataRows As DataRow(), ByVal pDisplayMember As String, ByVal pValueMember As String, ByVal pSelectString As String, Optional ByVal pSELECTDISTINCT As Boolean = False, Optional ByVal pOrderAscending As Boolean = True, Optional ByVal pLeadingBlank As Boolean = False, Optional ByVal pLeadingBlankValue As Object = "<DBNull>", Optional ByVal pLeadingBlankText As String = "")
    ' ************************************************************************************
    ' The process for setting standard combos has been split into two parts.
    ' The first part (the other function) takes a Venice 'StandardDataset' parameter
    ' and then creates a DataRow array in accordance with the other parameters.
    ' This DataRow array is then passed to this procedure and the Combo is populated.
    '
    ' It has been done this way so that the Selection Combos on Add/Edit forms can be
    ' populated using just this procedure and the standard form selectedrows array.
    '
    ' ************************************************************************************

    Dim thisSortedRows() As DataRow
    Dim thisRow As DataRow
    Dim lastRow As DataRow
    Dim newRow As DataRow
    Dim currentSelectedValue As Object
    Dim OrgText As String = ""
    Dim CurrentValueIsIndex As Boolean

    Dim DisplayFieldNames As String()
    Dim FormatStrings As String()
    Dim DCount As Integer

    Try
      If (pCombo Is Nothing) Then Exit Sub
      If (pDataRows Is Nothing) Then Exit Sub

      OrgText = Me.VeniceStatusBar.Text
      If (pDataRows.Length > 0) Then
        SetToolStripText(VeniceStatusBar, "Building " & pDataRows(0).Table.TableName & " Combo")
      Else
        SetToolStripText(VeniceStatusBar, "Building Combo")
      End If
      'If Not (Me.InvokeRequired) Then
      '	Me.VeniceStatusStrip.Refresh()
      '	Application.DoEvents()
      'End If

      ' Preserve Existing Combo Value

      CurrentValueIsIndex = True

      If pCombo.SelectedIndex >= 0 Then
        currentSelectedValue = pCombo.SelectedValue
      ElseIf pCombo.DropDownStyle = ComboBoxStyle.DropDown Then
        CurrentValueIsIndex = False
        currentSelectedValue = pCombo.Text
      Else
        currentSelectedValue = Nothing
      End If

      ' Resolve Display Field names

      DisplayFieldNames = pDisplayMember.Split(New Char() {",", "|"})
      ReDim FormatStrings(DisplayFieldNames.Length - 1)

      Dim SelectSortString As String = ""
      Try

        For DCount = 0 To (DisplayFieldNames.Length - 1)
          DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Trim(New Char() {" "})

          If (DisplayFieldNames(DCount).Length > 0) Then
            If (DCount > 0) AndAlso (SelectSortString.Length > 0) Then
              SelectSortString &= ", "
            End If

            If (DisplayFieldNames(DCount).ToUpper.EndsWith(" DESC")) Then
              DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Substring(0, DisplayFieldNames(DCount).Length - 5)
              SelectSortString &= DisplayFieldNames(DCount)
            ElseIf (DisplayFieldNames(DCount).ToUpper.EndsWith(" ASC")) Then
              DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Substring(0, DisplayFieldNames(DCount).Length - 4)
              SelectSortString &= DisplayFieldNames(DCount)
            ElseIf pOrderAscending Then
              SelectSortString &= DisplayFieldNames(DCount)
            Else
              SelectSortString &= (DisplayFieldNames(DCount) & " DESC")
            End If
          End If
        Next
      Catch ex As Exception
      End Try

      ' Get Source Dataset
      ' pDataRows
      If (pDataRows Is Nothing) Then
        Try
          ' Clear Existing Binding
          pCombo.DataSource = Nothing
          pCombo.DisplayMember = ""
          pCombo.ValueMember = ""
          pCombo.Items.Clear()
        Catch ex As Exception
        End Try

        Exit Sub
      End If

      thisSortedRows = pDataRows

      ' Clear Existing Binding
      pCombo.DataSource = Nothing
      pCombo.DisplayMember = ""
      pCombo.ValueMember = ""

      ' Build new Source table
      ' Linking directly to the source had odd side affects.
      Dim newTable As New DataTable

      newTable.Clear()

      Try
        newTable.Columns.Add(New DataColumn("DM", GetType(System.String)))
        If (thisSortedRows.Length <= 0) Then
          newTable.Columns.Add(New DataColumn("VM", pLeadingBlankValue.GetType))
        Else
          newTable.Columns.Add(New DataColumn("VM", thisSortedRows(0).Table.Columns(pValueMember).DataType))
        End If
      Catch ex As Exception
        Exit Sub
      End Try

      ' Set Date Format strings for Display fields as necessary

      For DCount = 0 To (DisplayFieldNames.Length - 1)
        FormatStrings(DCount) = ""
        If (thisSortedRows IsNot Nothing) AndAlso (thisSortedRows.Length > 0) Then
          If (DisplayFieldNames(DCount).Length > 0) Then
            If thisSortedRows(0).Table.Columns(DisplayFieldNames(DCount)).DataType Is GetType(System.DateTime) Then
              FormatStrings(DCount) = DISPLAYMEMBER_DATEFORMAT
            End If
          End If
        End If
      Next DCount

      ' Leading Blank.

      If (pLeadingBlank = True) Then
        Try
          newRow = newTable.NewRow

          newRow("DM") = pLeadingBlankText
          If (TypeOf pLeadingBlankValue Is String) AndAlso (CType(pLeadingBlankValue, String) = "<DBNull>") Then
            newRow("VM") = DBNull.Value
          Else
            newRow("VM") = pLeadingBlankValue
          End If

          newTable.Rows.Add(newRow)
        Catch ex As Exception
        End Try
      End If

      lastRow = Nothing
      If (Not (thisSortedRows Is Nothing)) AndAlso (thisSortedRows.Length > 0) Then
        Dim DisplayString As String

        For Each thisRow In thisSortedRows
          If (pSELECTDISTINCT) Then
            If (Not (lastRow Is Nothing)) AndAlso (CompareValue(thisRow(pValueMember), lastRow(pValueMember)) <> 0) Then
              lastRow = Nothing
            End If
          End If

          If (lastRow Is Nothing) Then
            newRow = newTable.NewRow

            Try
              DisplayString = ""
              For DCount = 0 To (DisplayFieldNames.Length - 1)
                If (DisplayFieldNames(DCount).Length > 0) Then
                  If DisplayString.Length > 0 Then
                    DisplayString &= ", "
                  End If

                  If thisRow.IsNull(DisplayFieldNames(DCount)) Then
                    DisplayString &= "<Null>"
                  Else
                    DisplayString &= Format(thisRow(DisplayFieldNames(DCount)), FormatStrings(DCount))
                  End If
                End If
              Next DCount

              newRow("DM") = DisplayString

              If thisRow.IsNull(pValueMember) Then
                newRow("VM") = 0
              Else
                newRow("VM") = thisRow(pValueMember)
              End If

              newTable.Rows.Add(newRow)

            Catch ex As Exception
            End Try
          End If

          If pSELECTDISTINCT Then
            lastRow = thisRow
          End If
        Next
      End If

      ' Link to new table
      Try
        pCombo.DisplayMember = "DM"
        pCombo.ValueMember = "VM"
        pCombo.DataSource = newTable
      Catch ex As Exception
      End Try

      ' Re-Size
      SetComboDropDownWidth(pCombo)

      ' Re-set existing value
      If Not (currentSelectedValue Is Nothing) Then
        Try
          If CurrentValueIsIndex Then
            pCombo.SelectedValue = currentSelectedValue
          Else
            pCombo.SelectedText = currentSelectedValue.ToString
            pCombo.Text = currentSelectedValue.ToString
          End If
        Catch ex As Exception
        End Try
      End If


    Catch ex As Exception
    Finally
      SetToolStripText(VeniceStatusBar, OrgText)
    End Try

  End Sub

  ''' <summary>
  ''' Sets the TBL generic combo.
  ''' </summary>
  ''' <param name="pCombo">The p combo.</param>
  ''' <param name="pDataRows">The p data rows.</param>
  ''' <param name="pDisplayMember">The p display member.</param>
  ''' <param name="pValueMember">The p value member.</param>
  ''' <param name="pSelectString">The p select string.</param>
  ''' <param name="pSELECTDISTINCT">if set to <c>true</c> [p SELECTDISTINCT].</param>
  ''' <param name="pOrderAscending">if set to <c>true</c> [p order ascending].</param>
  ''' <param name="pLeadingBlank">if set to <c>true</c> [p leading blank].</param>
  ''' <param name="pLeadingBlankValue">The p leading blank value.</param>
  ''' <param name="pLeadingBlankText">The p leading blank text.</param>
  Public Sub SetTblGenericCombo(ByVal pCombo As Telerik.WinControls.UI.RadComboBox, ByVal pDataRows As DataRow(), ByVal pDisplayMember As String, ByVal pValueMember As String, ByVal pSelectString As String, Optional ByVal pSELECTDISTINCT As Boolean = False, Optional ByVal pOrderAscending As Boolean = True, Optional ByVal pLeadingBlank As Boolean = False, Optional ByVal pLeadingBlankValue As Object = "<DBNull>", Optional ByVal pLeadingBlankText As String = "")
    ' ************************************************************************************
    ' The process for setting standard combos has been split into two parts.
    ' The first part (the other function) takes a Venice 'StandardDataset' parameter
    ' and then creates a DataRow array in accordance with the other parameters.
    ' This DataRow array is then passed to this procedure and the Combo is populated.
    '
    ' It has been done this way so that the Selection Combos on Add/Edit forms can be
    ' populated using just this procedure and the standard form selectedrows array.
    '
    ' ************************************************************************************

    Dim thisSortedRows() As DataRow
    Dim thisRow As DataRow
    Dim currentSelectedValue As Object
    Dim OrgText As String = ""
    Dim CurrentValueIsIndex As Boolean

    Dim DisplayFieldNames As String()
    Dim FormatStrings As String()
    Dim DCount As Integer

    Try
      If (pCombo Is Nothing) Then Exit Sub
      If (pDataRows Is Nothing) Then Exit Sub

      OrgText = Me.VeniceStatusBar.Text
      If (pDataRows.Length > 0) Then
        SetToolStripText(VeniceStatusBar, "Building " & pDataRows(0).Table.TableName & " Combo")
      Else
        SetToolStripText(VeniceStatusBar, "Building Combo")
      End If
      'If Not (Me.InvokeRequired) Then
      '	Me.VeniceStatusStrip.Refresh()
      '	Application.DoEvents()
      'End If

      ' Preserve Existing Combo Value

      CurrentValueIsIndex = True

      If pCombo.SelectedIndex >= 0 Then
        currentSelectedValue = pCombo.SelectedValue
      ElseIf pCombo.DropDownStyle = ComboBoxStyle.DropDown Then
        CurrentValueIsIndex = False
        currentSelectedValue = pCombo.Text
      Else
        currentSelectedValue = Nothing
      End If

      ' Resolve Display Field names

      DisplayFieldNames = pDisplayMember.Split(New Char() {",", "|"})
      ReDim FormatStrings(DisplayFieldNames.Length - 1)

      Dim SelectSortString As String = ""
      Try

        For DCount = 0 To (DisplayFieldNames.Length - 1)
          DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Trim(New Char() {" "})

          If (DisplayFieldNames(DCount).Length > 0) Then
            If (DCount > 0) AndAlso (SelectSortString.Length > 0) Then
              SelectSortString &= ", "
            End If

            If (DisplayFieldNames(DCount).ToUpper.EndsWith(" DESC")) Then
              DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Substring(0, DisplayFieldNames(DCount).Length - 5)
              SelectSortString &= DisplayFieldNames(DCount)
            ElseIf (DisplayFieldNames(DCount).ToUpper.EndsWith(" ASC")) Then
              DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Substring(0, DisplayFieldNames(DCount).Length - 4)
              SelectSortString &= DisplayFieldNames(DCount)
            ElseIf pOrderAscending Then
              SelectSortString &= DisplayFieldNames(DCount)
            Else
              SelectSortString &= (DisplayFieldNames(DCount) & " DESC")
            End If
          End If
        Next
      Catch ex As Exception
      End Try

      ' Clear Existing Binding
      pCombo.DataSource = Nothing
      pCombo.DisplayMember = ""
      pCombo.ValueMember = ""
      pCombo.Items.Clear()

      ' Get Source Dataset
      ' pDataRows
      If (pDataRows Is Nothing) Then
        Exit Sub
      End If

      thisSortedRows = pDataRows

      ' Build new Source 
      ' Linking directly to the source had odd side affects.

      ' Set Date Format strings for Display fields as necessary

      For DCount = 0 To (DisplayFieldNames.Length - 1)
        FormatStrings(DCount) = ""
        If (thisSortedRows IsNot Nothing) AndAlso (thisSortedRows.Length > 0) Then
          If (DisplayFieldNames(DCount).Length > 0) Then
            If thisSortedRows(0).Table.Columns(DisplayFieldNames(DCount)).DataType Is GetType(System.DateTime) Then
              FormatStrings(DCount) = DISPLAYMEMBER_DATEFORMAT
            End If
          End If
        End If
      Next DCount

      ' Leading Blank.

      If (pLeadingBlank = True) Then
        Try
          If (TypeOf pLeadingBlankValue Is String) AndAlso (CType(pLeadingBlankValue, String) = "<DBNull>") Then
            pCombo.Items.Add(New Telerik.WinControls.UI.RadComboBoxItem(pLeadingBlankText, DBNull.Value))
          Else
            pCombo.Items.Add(New Telerik.WinControls.UI.RadComboBoxItem(pLeadingBlankText, pLeadingBlankValue))
          End If

        Catch ex As Exception
        End Try
      End If

      If (Not (thisSortedRows Is Nothing)) AndAlso (thisSortedRows.Length > 0) Then
        Dim DisplayString As String
        Dim LastValueMember As Object = Nothing

        Try
          pCombo.BeginUpdate()

          For Each thisRow In thisSortedRows
            If (pSELECTDISTINCT) Then
              If (Not (LastValueMember IsNot Nothing)) AndAlso (CompareValue(thisRow(pValueMember), LastValueMember) <> 0) Then
                LastValueMember = Nothing
              End If
            End If

            If (LastValueMember Is Nothing) Then

              Try
                DisplayString = ""
                For DCount = 0 To (DisplayFieldNames.Length - 1)
                  If (DisplayFieldNames(DCount).Length > 0) Then
                    If DisplayString.Length > 0 Then
                      DisplayString &= ", "
                    End If

                    If thisRow.IsNull(DisplayFieldNames(DCount)) Then
                      DisplayString &= "<Null>"
                    Else
                      DisplayString &= Format(thisRow(DisplayFieldNames(DCount)), FormatStrings(DCount))
                    End If
                  End If
                Next DCount

                If thisRow.IsNull(pValueMember) Then
                  pCombo.Items.Add(New Telerik.WinControls.UI.RadComboBoxItem(DisplayString, 0))
                  If pSELECTDISTINCT Then
                    LastValueMember = 0
                  End If
                Else
                  pCombo.Items.Add(New Telerik.WinControls.UI.RadComboBoxItem(DisplayString, thisRow(pValueMember)))
                  If pSELECTDISTINCT Then
                    LastValueMember = thisRow(pValueMember)
                  End If
                End If

              Catch ex As Exception
              End Try
            End If
          Next

        Catch ex As Exception
        Finally
          pCombo.EndUpdate()
        End Try
      End If

      ' Re-set existing value
      If Not (currentSelectedValue Is Nothing) Then
        Try
          If CurrentValueIsIndex Then
            pCombo.SelectedValue = currentSelectedValue
          Else
            pCombo.SelectedText = currentSelectedValue.ToString
            pCombo.Text = currentSelectedValue.ToString
          End If
        Catch ex As Exception
        End Try
      End If


    Catch ex As Exception
    Finally

      SetToolStripText(VeniceStatusBar, OrgText)

    End Try

  End Sub

  ''' <summary>
  ''' Sets the TBL generic combo.
  ''' </summary>
  ''' <param name="pCombo">The p combo.</param>
  ''' <param name="pDataRows">The p data rows.</param>
  ''' <param name="DisplayFieldNames">The display field names.</param>
  ''' <param name="pValueMember">The p value member.</param>
  ''' <param name="DisplayFieldNames_DependencyTable">The display field names_ dependency table.</param>
  ''' <param name="DisplayFieldNames_DependencyField">The display field names_ dependency field.</param>
  ''' <param name="pSelectString">The p select string.</param>
  ''' <param name="pSELECTDISTINCT">if set to <c>true</c> [p SELECTDISTINCT].</param>
  ''' <param name="pOrderAscending">if set to <c>true</c> [p order ascending].</param>
  ''' <param name="pLeadingBlank">if set to <c>true</c> [p leading blank].</param>
  ''' <param name="pLeadingBlankValue">The p leading blank value.</param>
  ''' <param name="pLeadingBlankText">The p leading blank text.</param>
  Private Sub SetTblGenericCombo(ByVal pCombo As System.Windows.Forms.ComboBox, ByVal pDataRows As DataRow(), ByVal DisplayFieldNames As String(), ByVal pValueMember As String, ByVal DisplayFieldNames_DependencyTable As RenaissanceChangeID(), ByVal DisplayFieldNames_DependencyField As String(), ByVal pSelectString As String, Optional ByVal pSELECTDISTINCT As Boolean = False, Optional ByVal pOrderAscending As Boolean = True, Optional ByVal pLeadingBlank As Boolean = False, Optional ByVal pLeadingBlankValue As Object = "<DBNull>", Optional ByVal pLeadingBlankText As String = "")
    ' ************************************************************************************
    ' The process for setting standard combos has been split into two parts.
    ' The first part (the other function) takes a Renaissance 'StandardDataset' parameter
    ' and then creates a DataRow array in accordance with the other parameters.
    ' This DataRow array is then passed to this procedure and the Combo is populated.
    '
    ' It has been done this way so that the Selection Combos on Add/Edit forms can be
    ' populated using just this procedure and the standard form selectedrows() array.
    '
    ' ************************************************************************************

    Dim thisSortedRows() As DataRow
    Dim thisRow As DataRow
    Dim lastRow As DataRow
    Dim newRow As DataRow
    Dim currentSelectedValue As Object
    Dim OrgText As String = ""
    Dim CurrentValueIsIndex As Boolean

    'Dim DisplayFieldNames_DependencyTable As RenaissanceChangeID()
    'Dim DisplayFieldNames_DependencyField As String()
    Dim FormatStrings As String()
    Dim DCount As Integer

    Try
      If (pCombo Is Nothing) OrElse (pCombo.IsDisposed) Then Exit Sub
      If (pDataRows Is Nothing) Then Exit Sub

      OrgText = VeniceStatusBar.Text

      If (pDataRows.Length > 0) Then
        SetToolStripText(VeniceStatusBar, "Building " & pDataRows(0).Table.TableName & " Combo")
      Else
        SetToolStripText(VeniceStatusBar, "Building Combo")
      End If

      'Me.VeniceStatusStrip.Refresh()
      'Application.DoEvents()

      ' Preserve Existing Combo Value

      CurrentValueIsIndex = True
      If pCombo.DropDownStyle = ComboBoxStyle.DropDownList Then
        If pCombo.SelectedIndex >= 0 Then
          currentSelectedValue = pCombo.SelectedValue
        ElseIf pCombo.DropDownStyle = ComboBoxStyle.DropDown Then
          currentSelectedValue = pCombo.Text
        Else
          currentSelectedValue = Nothing
        End If
      ElseIf pCombo.DropDownStyle = ComboBoxStyle.DropDown Then
        If pCombo.SelectedIndex >= 0 Then
          currentSelectedValue = pCombo.SelectedValue
        Else
          CurrentValueIsIndex = False
          currentSelectedValue = pCombo.SelectedText
        End If
      End If

      If pCombo.SelectedIndex >= 0 Then
        currentSelectedValue = pCombo.SelectedValue
      ElseIf pCombo.DropDownStyle = ComboBoxStyle.DropDown Then
        currentSelectedValue = pCombo.Text
      Else
        currentSelectedValue = Nothing
      End If

      ' Resolve Display Field names

      ReDim FormatStrings(DisplayFieldNames.Length - 1)

      ' Get Source Dataset
      ' pDataRows
      If (pDataRows Is Nothing) Then
        Try
          ' Clear Existing Binding
          pCombo.DataSource = Nothing
          pCombo.DisplayMember = ""
          pCombo.ValueMember = ""
          pCombo.Items.Clear()
        Catch ex As Exception
        End Try

        Exit Sub
      End If

      thisSortedRows = pDataRows

      ' Clear Existing Binding
      pCombo.DataSource = Nothing
      pCombo.DisplayMember = ""
      pCombo.ValueMember = ""

      ' Build new Source table
      ' Linking directly to the source had odd side affects.
      Dim newTable As New DataTable

      newTable.Clear()

      Try
        newTable.Columns.Add(New DataColumn("DM", GetType(System.String)))
        If (thisSortedRows.Length <= 0) Then
          newTable.Columns.Add(New DataColumn("VM", pLeadingBlankValue.GetType))
        Else
          newTable.Columns.Add(New DataColumn("VM", thisSortedRows(0).Table.Columns(pValueMember).DataType))
        End If
      Catch ex As Exception
        SetToolStripText(VeniceStatusBar, OrgText)
        Exit Sub
      End Try

      ' Set Date Format strings for Display fields as necessary

      For DCount = 0 To (DisplayFieldNames.Length - 1)
        FormatStrings(DCount) = ""

        If (thisSortedRows IsNot Nothing) AndAlso (thisSortedRows.Length > 0) Then
          If (DisplayFieldNames(DCount).Length > 0) Then
            If thisSortedRows(0).Table.Columns(DisplayFieldNames(DCount)).DataType Is GetType(System.DateTime) Then
              FormatStrings(DCount) = DISPLAYMEMBER_DATEFORMAT
            End If
          End If
        End If
      Next DCount

      ' Leading Blank.

      If (pLeadingBlank = True) Then
        Try
          newRow = newTable.NewRow

          newRow("DM") = pLeadingBlankText
          If (TypeOf pLeadingBlankValue Is String) AndAlso (pLeadingBlankValue.ToString = "<DBNull>") Then
            newRow("VM") = DBNull.Value
          Else
            newRow("VM") = pLeadingBlankValue
          End If

          newTable.Rows.Add(newRow)
        Catch ex As Exception
        End Try
      End If

      lastRow = Nothing
      If (Not (thisSortedRows Is Nothing)) AndAlso (thisSortedRows.Length > 0) Then
        Dim DisplayString As String

        For Each thisRow In thisSortedRows
          If (pSELECTDISTINCT) Then
            If (Not (lastRow Is Nothing)) AndAlso (Not (thisRow(pValueMember).ToString = lastRow(pValueMember).ToString)) Then
              lastRow = Nothing
            End If
          End If

          If (lastRow Is Nothing) Then
            newRow = newTable.NewRow

            Try
              DisplayString = ""
              For DCount = 0 To (DisplayFieldNames.Length - 1)
                If (DisplayFieldNames(DCount).Length > 0) Then
                  If DisplayString.Length > 0 Then
                    DisplayString &= ", "
                  End If

                  If (DisplayFieldNames_DependencyTable(DCount) = RenaissanceChangeID.None) Then
                    If thisRow.IsNull(DisplayFieldNames(DCount)) Then
                      DisplayString &= "<Null>"
                    Else
                      DisplayString &= Format(thisRow(DisplayFieldNames(DCount)), FormatStrings(DCount))
                    End If
                  Else
                    If thisRow.IsNull(DisplayFieldNames(DCount)) Then
                      DisplayString &= "<Null>"
                    Else
                      Try
                        DisplayString &= LookupTableValue(Me, RenaissanceGlobals.RenaissanceStandardDatasets.GetStandardDataset(DisplayFieldNames_DependencyTable(DCount)), CInt(thisRow(DisplayFieldNames(DCount))), DisplayFieldNames_DependencyField(DCount)).ToString
                      Catch ex As Exception
                      End Try
                    End If
                  End If
                End If
              Next DCount

              newRow("DM") = DisplayString

              If thisRow.IsNull(pValueMember) Then
                newRow("VM") = 0
              Else
                newRow("VM") = thisRow(pValueMember)
              End If

              newTable.Rows.Add(newRow)

            Catch ex As Exception
            End Try
          End If

          If pSELECTDISTINCT Then
            lastRow = thisRow
          End If
        Next
      End If

      ' Link to new table
      pCombo.DataSource = newTable
      pCombo.DisplayMember = "DM"
      pCombo.ValueMember = "VM"

      ' Re-Size
      SetComboDropDownWidth(pCombo)

      ' Re-set existing value
      If Not (currentSelectedValue Is Nothing) Then
        Try
          If CurrentValueIsIndex Then
            pCombo.SelectedValue = currentSelectedValue
          Else
            pCombo.SelectedText = currentSelectedValue.ToString
            pCombo.Text = currentSelectedValue.ToString
          End If
        Catch ex As Exception
        End Try
      End If

    Catch ex As Exception
    Finally
      SetToolStripText(VeniceStatusBar, OrgText)
    End Try


  End Sub

  ''' <summary>
  ''' Sets the TBL generic combo.
  ''' </summary>
  ''' <param name="pCombo">The p combo.</param>
  ''' <param name="pDataRows">The p data rows.</param>
  ''' <param name="DisplayFieldNames">The display field names.</param>
  ''' <param name="pValueMember">The p value member.</param>
  ''' <param name="DisplayFieldNames_DependencyTable">The display field names_ dependency table.</param>
  ''' <param name="DisplayFieldNames_DependencyField">The display field names_ dependency field.</param>
  ''' <param name="pSelectString">The p select string.</param>
  ''' <param name="pSELECTDISTINCT">if set to <c>true</c> [p SELECTDISTINCT].</param>
  ''' <param name="pOrderAscending">if set to <c>true</c> [p order ascending].</param>
  ''' <param name="pLeadingBlank">if set to <c>true</c> [p leading blank].</param>
  ''' <param name="pLeadingBlankValue">The p leading blank value.</param>
  ''' <param name="pLeadingBlankText">The p leading blank text.</param>
  Private Sub SetTblGenericCombo(ByVal pCombo As Telerik.WinControls.UI.RadComboBox, ByVal pDataRows As DataRow(), ByVal DisplayFieldNames As String(), ByVal pValueMember As String, ByVal DisplayFieldNames_DependencyTable As RenaissanceChangeID(), ByVal DisplayFieldNames_DependencyField As String(), ByVal pSelectString As String, Optional ByVal pSELECTDISTINCT As Boolean = False, Optional ByVal pOrderAscending As Boolean = True, Optional ByVal pLeadingBlank As Boolean = False, Optional ByVal pLeadingBlankValue As Object = "<DBNull>", Optional ByVal pLeadingBlankText As String = "")
    ' ************************************************************************************
    ' The process for setting standard combos has been split into two parts.
    ' The first part (the other function) takes a Renaissance 'StandardDataset' parameter
    ' and then creates a DataRow array in accordance with the other parameters.
    ' This DataRow array is then passed to this procedure and the Combo is populated.
    '
    ' It has been done this way so that the Selection Combos on Add/Edit forms can be
    ' populated using just this procedure and the standard form selectedrows() array.
    '
    ' ************************************************************************************

    Dim thisSortedRows() As DataRow
    Dim thisRow As DataRow
    Dim currentSelectedValue As Object
    Dim OrgText As String = ""
    Dim CurrentValueIsIndex As Boolean

    'Dim DisplayFieldNames_DependencyTable As RenaissanceChangeID()
    'Dim DisplayFieldNames_DependencyField As String()
    Dim FormatStrings As String()
    Dim DCount As Integer

    Try
      If (pCombo Is Nothing) OrElse (pCombo.IsDisposed) Then Exit Sub
      If (pDataRows Is Nothing) Then Exit Sub

      OrgText = VeniceStatusBar.Text

      If (pDataRows.Length > 0) Then
        SetToolStripText(VeniceStatusBar, "Building " & pDataRows(0).Table.TableName & " Combo")
      Else
        SetToolStripText(VeniceStatusBar, "Building Combo")
      End If

      'Me.VeniceStatusStrip.Refresh()
      'Application.DoEvents()

      ' Preserve Existing Combo Value

      CurrentValueIsIndex = True
      If pCombo.DropDownStyle = ComboBoxStyle.DropDownList Then
        If pCombo.SelectedIndex >= 0 Then
          currentSelectedValue = pCombo.SelectedValue
        ElseIf pCombo.DropDownStyle = ComboBoxStyle.DropDown Then
          currentSelectedValue = pCombo.Text
        Else
          currentSelectedValue = Nothing
        End If
      ElseIf pCombo.DropDownStyle = ComboBoxStyle.DropDown Then
        If pCombo.SelectedIndex >= 0 Then
          currentSelectedValue = pCombo.SelectedValue
        Else
          CurrentValueIsIndex = False
          currentSelectedValue = pCombo.SelectedText
        End If
      End If

      If pCombo.SelectedIndex >= 0 Then
        currentSelectedValue = pCombo.SelectedValue
      ElseIf pCombo.DropDownStyle = ComboBoxStyle.DropDown Then
        currentSelectedValue = pCombo.Text
      Else
        currentSelectedValue = Nothing
      End If

      ' Resolve Display Field names

      ReDim FormatStrings(DisplayFieldNames.Length - 1)

      pCombo.DataSource = Nothing
      pCombo.DisplayMember = ""
      pCombo.ValueMember = ""
      pCombo.Items.Clear()

      ' Get Source Dataset
      ' pDataRows
      If (pDataRows Is Nothing) Then
        Exit Sub
      End If

      thisSortedRows = pDataRows

      ' Build new Source 
      ' Linking directly to the source had odd side affects.

      ' Set Date Format strings for Display fields as necessary

      For DCount = 0 To (DisplayFieldNames.Length - 1)
        FormatStrings(DCount) = ""

        If (thisSortedRows IsNot Nothing) AndAlso (thisSortedRows.Length > 0) Then
          If (DisplayFieldNames(DCount).Length > 0) Then
            If thisSortedRows(0).Table.Columns(DisplayFieldNames(DCount)).DataType Is GetType(System.DateTime) Then
              FormatStrings(DCount) = DISPLAYMEMBER_DATEFORMAT
            End If
          End If
        End If
      Next DCount

      ' Leading Blank.

      If (pLeadingBlank = True) Then
        Try
          If (TypeOf pLeadingBlankValue Is String) AndAlso (CType(pLeadingBlankValue, String) = "<DBNull>") Then
            pCombo.Items.Add(New Telerik.WinControls.UI.RadComboBoxItem(pLeadingBlankText, DBNull.Value))
          Else
            pCombo.Items.Add(New Telerik.WinControls.UI.RadComboBoxItem(pLeadingBlankText, pLeadingBlankValue))
          End If

        Catch ex As Exception
        End Try
      End If

      If (Not (thisSortedRows Is Nothing)) AndAlso (thisSortedRows.Length > 0) Then
        Dim DisplayString As String
        Dim LastValueMember As Object = Nothing

        Try
          pCombo.BeginUpdate()

          For Each thisRow In thisSortedRows
            If (pSELECTDISTINCT) Then
              If (Not (LastValueMember IsNot Nothing)) AndAlso (CompareValue(thisRow(pValueMember), LastValueMember) <> 0) Then
                LastValueMember = Nothing
              End If
            End If

            If (LastValueMember Is Nothing) Then

              Try
                DisplayString = ""
                For DCount = 0 To (DisplayFieldNames.Length - 1)
                  If (DisplayFieldNames(DCount).Length > 0) Then
                    If DisplayString.Length > 0 Then
                      DisplayString &= ", "
                    End If

                    If (DisplayFieldNames_DependencyTable(DCount) = RenaissanceChangeID.None) Then
                      If thisRow.IsNull(DisplayFieldNames(DCount)) Then
                        DisplayString &= "<Null>"
                      Else
                        DisplayString &= Format(thisRow(DisplayFieldNames(DCount)), FormatStrings(DCount))
                      End If
                    Else
                      If thisRow.IsNull(DisplayFieldNames(DCount)) Then
                        DisplayString &= "<Null>"
                      Else
                        Try
                          DisplayString &= LookupTableValue(Me, RenaissanceGlobals.RenaissanceStandardDatasets.GetStandardDataset(DisplayFieldNames_DependencyTable(DCount)), CInt(thisRow(DisplayFieldNames(DCount))), DisplayFieldNames_DependencyField(DCount)).ToString
                        Catch ex As Exception
                        End Try
                      End If
                    End If
                  End If
                Next DCount

                If thisRow.IsNull(pValueMember) Then
                  pCombo.Items.Add(New Telerik.WinControls.UI.RadComboBoxItem(DisplayString, 0))
                  If pSELECTDISTINCT Then
                    LastValueMember = 0
                  End If
                Else
                  pCombo.Items.Add(New Telerik.WinControls.UI.RadComboBoxItem(DisplayString, thisRow(pValueMember)))
                  If pSELECTDISTINCT Then
                    LastValueMember = thisRow(pValueMember)
                  End If
                End If

              Catch ex As Exception
              End Try
            End If

          Next

        Catch ex As Exception
        Finally
          pCombo.EndUpdate()
        End Try

      End If

      ' Re-set existing value
      If Not (currentSelectedValue Is Nothing) Then
        Try
          If CurrentValueIsIndex Then
            pCombo.SelectedValue = currentSelectedValue
          Else
            pCombo.SelectedText = currentSelectedValue.ToString
            pCombo.Text = currentSelectedValue.ToString
          End If
        Catch ex As Exception
        End Try
      End If

    Catch ex As Exception
    Finally
      SetToolStripText(VeniceStatusBar, OrgText)
    End Try

  End Sub

  ''' <summary>
  ''' Sets the TBL generic combo.
  ''' </summary>
  ''' <param name="pCombo">The p combo.</param>
  ''' <param name="pTableID">The p table ID.</param>
  ''' <param name="pDisplayMember">The p display member.</param>
  ''' <param name="pValueMember">The p value member.</param>
  ''' <param name="pSelectString">The p select string.</param>
  ''' <param name="pSELECTDISTINCT">if set to <c>true</c> [p SELECTDISTINCT].</param>
  ''' <param name="pOrderAscending">if set to <c>true</c> [p order ascending].</param>
  ''' <param name="pLeadingBlank">if set to <c>true</c> [p leading blank].</param>
  ''' <param name="pLeadingBlankValue">The p leading blank value.</param>
  ''' <param name="pLeadingBlankText">The p leading blank text.</param>
  Public Sub SetTblGenericCombo(ByVal pCombo As System.Windows.Forms.ComboBox, ByVal pTableID As RenaissanceGlobals.StandardDataset, ByVal pDisplayMember As String, ByVal pValueMember As String, ByVal pSelectString As String, Optional ByVal pSELECTDISTINCT As Boolean = False, Optional ByVal pOrderAscending As Boolean = True, Optional ByVal pLeadingBlank As Boolean = False, Optional ByVal pLeadingBlankValue As Object = "<DBNull>", Optional ByVal pLeadingBlankText As String = "", Optional ByVal pSortString As String = "")
    ' ************************************************************************************
    ' Wrapper for the  SetTblGenericCombo() Routine.
    ' ************************************************************************************

    Dim thisDataset As DataSet
    Dim thisSortedRows() As DataRow
    Dim SelectString As String
    Dim OrgText As String = ""

    Try
      ' SetMasterNameCombo / LocationsCombo

      If (pTableID.ChangeID = RenaissanceGlobals.RenaissanceStandardDatasets.Mastername.ChangeID) AndAlso ((pSelectString.Length = 0) OrElse (pSelectString.ToUpper = "TRUE")) AndAlso (pValueMember = "ID") Then
        If (_MasternameDictionary Is Nothing) Then
          _MasternameDictionary = New LookupCollection(Of Integer, String)
        End If

        Call SetMasterNameCombo(pCombo, pSelectString, pLeadingBlank, pLeadingBlankValue, pLeadingBlankText)
        Exit Sub
      End If

      If (pTableID.ChangeID = RenaissanceGlobals.RenaissanceStandardDatasets.tblLocations.ChangeID) AndAlso ((pSelectString.Length = 0) OrElse (pSelectString.ToUpper = "TRUE")) AndAlso (pValueMember = "CityID") Then
        If (_LocationsDictionary Is Nothing) Then
          ' This prevents the Locations collection being maintained unless a Locations Combo is actually requested.

          _LocationsDictionary = New LookupCollection(Of Integer, String)
        End If

        Call SetLocationsCombo(pCombo)
        Exit Sub
      End If
    Catch ex As Exception
    End Try

    Dim DisplayFieldNames As String()
    Dim DCount As Integer
    Dim ReferentialDataset As RenaissanceDataClass.DSReferentialIntegrity = Nothing
    Dim ReferentialTable As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityDataTable = Nothing
    Dim IntegrityChecks() As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityRow
    Dim DisplayFieldNames_DependencyTable As RenaissanceChangeID()
    Dim DisplayFieldNames_DependencyField As String()

    Try
      If (pCombo Is Nothing) Then Exit Sub
      If (pTableID Is Nothing) Then Exit Sub

      OrgText = VeniceStatusBar.Text
      SetToolStripText(VeniceStatusBar, "Building " & pTableID.TableName & " Combo")
      'Application.DoEvents()

      ' Resolve Display Field names
      DisplayFieldNames = pDisplayMember.Split(New Char() {",", "|"})

      ' ReDim FormatStrings(DisplayFieldNames.Length - 1)
      ReDim DisplayFieldNames_DependencyTable(DisplayFieldNames.Length - 1)
      ReDim DisplayFieldNames_DependencyField(DisplayFieldNames.Length - 1)

      Dim SelectSortString As String = ""
      Try

        For DCount = 0 To (DisplayFieldNames.Length - 1)
          DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Trim(New Char() {" "})
          DisplayFieldNames_DependencyTable(DCount) = RenaissanceChangeID.None
          DisplayFieldNames_DependencyField(DCount) = ""

          If (DisplayFieldNames(DCount).Length > 0) Then
            If (DCount > 0) AndAlso (SelectSortString.Length > 0) Then
              SelectSortString &= ", "
            End If

            If (DisplayFieldNames(DCount).ToUpper.EndsWith(" DESC")) Then
              DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Substring(0, DisplayFieldNames(DCount).Length - 5)
              SelectSortString &= DisplayFieldNames(DCount)
            ElseIf (DisplayFieldNames(DCount).ToUpper.EndsWith(" ASC")) Then
              DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Substring(0, DisplayFieldNames(DCount).Length - 4)
              SelectSortString &= DisplayFieldNames(DCount)
            ElseIf pOrderAscending Then
              SelectSortString &= DisplayFieldNames(DCount)
            Else
              SelectSortString &= (DisplayFieldNames(DCount) & " DESC")
            End If

            ' Check Referential Dependency
            If (ReferentialTable Is Nothing) Then
              ReferentialDataset = Load_Table(RenaissanceStandardDatasets.tblReferentialIntegrity, False)
              ReferentialTable = ReferentialDataset.tblReferentialIntegrity
            End If

            Try
              IntegrityChecks = ReferentialTable.Select("(FeedsTable = '" & pTableID.TableName & "') AND (FeedsField = '" & DisplayFieldNames(DCount) & "')")
              If (IntegrityChecks IsNot Nothing) AndAlso (IntegrityChecks.Length > 0) Then

                DisplayFieldNames_DependencyTable(DCount) = System.Enum.Parse(GetType(RenaissanceChangeID), IntegrityChecks(0).TableName)
                DisplayFieldNames_DependencyField(DCount) = IntegrityChecks(0).DescriptionField
              End If
            Catch ex As Exception
              DisplayFieldNames_DependencyTable(DCount) = RenaissanceChangeID.None
              DisplayFieldNames_DependencyField(DCount) = ""
            End Try

          End If
        Next
      Catch ex As Exception
      End Try

      If (pSortString.Length > 0) Then
        SelectSortString = pSortString
      End If

      ' Get Source Dataset
      thisDataset = Me.Load_Table(pTableID, False)
      If Not (thisDataset Is Nothing) Then
        If pSelectString Is Nothing Then
          SelectString = "True"
        ElseIf pSelectString.Length <= 0 Then
          SelectString = "True"
        Else
          SelectString = pSelectString
        End If

        Try
          thisSortedRows = thisDataset.Tables(0).Select(SelectString, SelectSortString)
        Catch ex As Exception
          thisSortedRows = Nothing
        End Try
      Else
        Try
          ' Clear Existing Binding
          pCombo.DataSource = Nothing
          pCombo.DisplayMember = ""
          pCombo.ValueMember = ""
          pCombo.Items.Clear()
        Catch ex As Exception
        End Try

        Exit Sub
      End If

      Call SetTblGenericCombo(pCombo, thisSortedRows, DisplayFieldNames, pValueMember, DisplayFieldNames_DependencyTable, DisplayFieldNames_DependencyField, pSelectString, pSELECTDISTINCT, pOrderAscending, pLeadingBlank, pLeadingBlankValue, pLeadingBlankText)

    Catch ex As Exception
    Finally
      SetToolStripText(VeniceStatusBar, OrgText)
    End Try

  End Sub

  ''' <summary>
  ''' Sets the TBL generic combo.
  ''' </summary>
  ''' <param name="pCombo">The Combo box to populate.</param>
  ''' <param name="pTableID">The Table ID.</param>
  ''' <param name="pDisplayMember">The Display member.</param>
  ''' <param name="pValueMember">The Value member.</param>
  ''' <param name="pSelectString">The Select string.</param>
  ''' <param name="pSELECTDISTINCT">if set to <c>true</c> [SELECTDISTINCT].</param>
  ''' <param name="pOrderAscending">if set to <c>true</c> [order ascending].</param>
  ''' <param name="pLeadingBlank">if set to <c>true</c> [leading blank].</param>
  ''' <param name="pLeadingBlankValue">The leading blank value.</param>
  ''' <param name="pLeadingBlankText">The leading blank text.</param>
  Public Sub SetTblGenericCombo(ByVal pCombo As Telerik.WinControls.UI.RadComboBox, ByVal pTableID As RenaissanceGlobals.StandardDataset, ByVal pDisplayMember As String, ByVal pValueMember As String, ByVal pSelectString As String, Optional ByVal pSELECTDISTINCT As Boolean = False, Optional ByVal pOrderAscending As Boolean = True, Optional ByVal pLeadingBlank As Boolean = False, Optional ByVal pLeadingBlankValue As Object = "<DBNull>", Optional ByVal pLeadingBlankText As String = "")
    ' ************************************************************************************
    ' Wrapper for the  SetTblGenericCombo() Routine.
    ' ************************************************************************************

    Dim thisDataset As DataSet
    Dim thisSortedRows() As DataRow
    Dim SelectString As String
    Dim OrgText As String = ""

    Dim DisplayFieldNames As String()
    Dim DCount As Integer
    Dim ReferentialDataset As RenaissanceDataClass.DSReferentialIntegrity = Nothing
    Dim ReferentialTable As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityDataTable = Nothing
    Dim IntegrityChecks() As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityRow
    Dim DisplayFieldNames_DependencyTable As RenaissanceChangeID()
    Dim DisplayFieldNames_DependencyField As String()

    Try
      If (pCombo Is Nothing) Then Exit Sub
      If (pTableID Is Nothing) Then Exit Sub

      OrgText = VeniceStatusBar.Text
      SetToolStripText(VeniceStatusBar, "Building " & pTableID.TableName & " Combo")
      'Application.DoEvents()

      ' Resolve Display Field names
      DisplayFieldNames = pDisplayMember.Split(New Char() {",", "|"})
      ' ReDim FormatStrings(DisplayFieldNames.Length - 1)
      ReDim DisplayFieldNames_DependencyTable(DisplayFieldNames.Length - 1)
      ReDim DisplayFieldNames_DependencyField(DisplayFieldNames.Length - 1)

      Dim SelectSortString As String = ""
      Try

        For DCount = 0 To (DisplayFieldNames.Length - 1)
          DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Trim(New Char() {" "})
          DisplayFieldNames_DependencyTable(DCount) = RenaissanceChangeID.None
          DisplayFieldNames_DependencyField(DCount) = ""

          If (DisplayFieldNames(DCount).Length > 0) Then
            If (DCount > 0) AndAlso (SelectSortString.Length > 0) Then
              SelectSortString &= ", "
            End If

            If (DisplayFieldNames(DCount).ToUpper.EndsWith(" DESC")) Then
              DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Substring(0, DisplayFieldNames(DCount).Length - 5)
              SelectSortString &= DisplayFieldNames(DCount)
            ElseIf (DisplayFieldNames(DCount).ToUpper.EndsWith(" ASC")) Then
              DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Substring(0, DisplayFieldNames(DCount).Length - 4)
              SelectSortString &= DisplayFieldNames(DCount)
            ElseIf pOrderAscending Then
              SelectSortString &= DisplayFieldNames(DCount)
            Else
              SelectSortString &= (DisplayFieldNames(DCount) & " DESC")
            End If

            ' Check Referential Dependency
            If (ReferentialTable Is Nothing) Then
              ReferentialDataset = Load_Table(RenaissanceStandardDatasets.tblReferentialIntegrity, False)
              ReferentialTable = ReferentialDataset.tblReferentialIntegrity
            End If

            Try
              IntegrityChecks = ReferentialTable.Select("(FeedsTable = '" & pTableID.TableName & "') AND (FeedsField = '" & DisplayFieldNames(DCount) & "')")
              If (IntegrityChecks IsNot Nothing) AndAlso (IntegrityChecks.Length > 0) Then

                DisplayFieldNames_DependencyTable(DCount) = System.Enum.Parse(GetType(RenaissanceChangeID), IntegrityChecks(0).TableName)
                DisplayFieldNames_DependencyField(DCount) = IntegrityChecks(0).DescriptionField
              End If
            Catch ex As Exception
              DisplayFieldNames_DependencyTable(DCount) = RenaissanceChangeID.None
              DisplayFieldNames_DependencyField(DCount) = ""
            End Try

          End If
        Next
      Catch ex As Exception
      End Try

      ' Get Source Dataset
      thisDataset = Me.Load_Table(pTableID, False)
      If Not (thisDataset Is Nothing) Then
        If pSelectString Is Nothing Then
          SelectString = "True"
        ElseIf pSelectString.Length <= 0 Then
          SelectString = "True"
        Else
          SelectString = pSelectString
        End If

        Try
          thisSortedRows = thisDataset.Tables(0).Select(SelectString, SelectSortString)
        Catch ex As Exception
          thisSortedRows = Nothing
        End Try
      Else
        Try
          ' Clear Existing Binding
          pCombo.DataSource = Nothing
          pCombo.DisplayMember = ""
          pCombo.ValueMember = ""
          pCombo.Items.Clear()
        Catch ex As Exception
        End Try

        Exit Sub
      End If

      Call SetTblGenericCombo(pCombo, thisSortedRows, DisplayFieldNames, pValueMember, DisplayFieldNames_DependencyTable, DisplayFieldNames_DependencyField, pSelectString, pSELECTDISTINCT, pOrderAscending, pLeadingBlank, pLeadingBlankValue, pLeadingBlankText)

    Catch ex As Exception
    Finally
      SetToolStripText(VeniceStatusBar, OrgText)
    End Try

  End Sub

  ''' <summary>
  ''' Sets the TBL generic combo.
  ''' </summary>
  ''' <param name="pCombo">The p combo.</param>
  ''' <param name="pEnumeration">The p enumeration.</param>
  ''' <param name="pLeadingBlank">if set to <c>true</c> [p leading blank].</param>
  ''' <param name="pLeadingBlankLabel">The p leading blank label.</param>
  ''' <param name="pLeadingBlankValue">The p leading blank value.</param>
  Public Sub SetTblGenericCombo(ByRef pCombo As System.Windows.Forms.ComboBox, ByRef pEnumeration As Type, Optional ByVal pLeadingBlank As Boolean = False, Optional ByVal pLeadingBlankLabel As String = "", Optional ByVal pLeadingBlankValue As Object = CInt(0))

    Dim newRow As DataRow
    Dim currentSelectedValue As Object
    Dim OrgText As String = ""

    Dim EnumNames As String()
    Dim EnumCounter As Integer
    Dim CharCounter As Integer
    Dim thischar As Char

    Dim ThisName As String
    Dim NewNameBuilder As System.Text.StringBuilder

    Try

      OrgText = Me.VeniceStatusBar.Text
      SetToolStripText(VeniceStatusBar, "Building " & pEnumeration.Name & " Combo")
      'If Not (Me.InvokeRequired) Then
      '	Me.VeniceStatusStrip.Refresh()
      '	Application.DoEvents()
      'End If

      ' Preserve Existing Combo Value

      If pCombo.SelectedIndex >= 0 Then
        currentSelectedValue = pCombo.SelectedValue
      Else
        currentSelectedValue = Nothing
      End If

      EnumNames = System.Enum.GetNames(pEnumeration)

      ' Clear Existing Binding
      pCombo.DataSource = Nothing
      pCombo.DisplayMember = ""
      pCombo.ValueMember = ""

      ' Build new Source table
      ' Linking directly to the source had odd side affects.
      Dim newTable As New DataTable

      newTable.Clear()

      Try
        newTable.Columns.Add(New DataColumn("DM", GetType(System.String)))
        newTable.Columns.Add(New DataColumn("VM", GetType(System.Int32)))
      Catch ex As Exception
        Exit Sub
      End Try

      If (pLeadingBlank = True) Then
        Try
          newRow = newTable.NewRow

          newRow("DM") = pLeadingBlankLabel
          newRow("VM") = pLeadingBlankValue

          newTable.Rows.Add(newRow)
        Catch ex As Exception
        End Try
      End If

      If EnumNames.Length > 0 Then
        For EnumCounter = 0 To (EnumNames.Length - 1)
          newRow = newTable.NewRow

          NewNameBuilder = New System.Text.StringBuilder
          ThisName = EnumNames(EnumCounter)
          For CharCounter = 0 To (ThisName.Length - 1)
            thischar = ThisName.Substring(CharCounter, 1)
            If (CharCounter > 0) AndAlso (Char.IsUpper(thischar)) Then
              NewNameBuilder.Append(" ")
            End If
            NewNameBuilder.Append(thischar)
          Next

          newRow("DM") = NewNameBuilder.ToString
          newRow("VM") = CInt(System.Enum.Parse(pEnumeration, EnumNames(EnumCounter)))

          newTable.Rows.Add(newRow)
        Next

      End If

      ' Link to new table
      Try
        pCombo.DisplayMember = "DM"
        pCombo.ValueMember = "VM"
      Catch ex As Exception
      End Try
      pCombo.DataSource = newTable
      Try
        pCombo.DisplayMember = "DM"
        pCombo.ValueMember = "VM"
      Catch ex As Exception
      End Try

      ' Re-Size
      SetComboDropDownWidth(pCombo)

      ' Re-set existing value
      If Not (currentSelectedValue Is Nothing) Then
        Try
          pCombo.SelectedValue = currentSelectedValue
        Catch ex As Exception
        End Try
      End If


    Catch ex As Exception
    Finally
      SetToolStripText(VeniceStatusBar, OrgText)
    End Try

  End Sub

  ''' <summary>
  ''' Sets the TBL generic combo.
  ''' </summary>
  ''' <param name="pCombo">The p combo.</param>
  ''' <param name="pEnumeration">The p enumeration.</param>
  ''' <param name="pLeadingBlank">if set to <c>true</c> [p leading blank].</param>
  ''' <param name="pLeadingBlankLabel">The p leading blank label.</param>
  ''' <param name="pLeadingBlankValue">The p leading blank value.</param>
  Public Sub SetTblGenericCombo(ByRef pCombo As Telerik.WinControls.UI.RadComboBox, ByRef pEnumeration As Type, Optional ByVal pLeadingBlank As Boolean = False, Optional ByVal pLeadingBlankLabel As String = "", Optional ByVal pLeadingBlankValue As Object = CInt(0))

    Dim currentSelectedValue As Object
    Dim OrgText As String = ""

    Dim EnumNames As String()
    Dim EnumCounter As Integer
    Dim CharCounter As Integer
    Dim thischar As Char

    Dim ThisName As String
    Dim NewNameBuilder As System.Text.StringBuilder

    Try

      OrgText = Me.VeniceStatusBar.Text
      SetToolStripText(VeniceStatusBar, "Building " & pEnumeration.Name & " Combo")
      'If Not (Me.InvokeRequired) Then
      '	Me.VeniceStatusStrip.Refresh()
      '	Application.DoEvents()
      'End If

      ' Preserve Existing Combo Value

      If pCombo.SelectedIndex >= 0 Then
        currentSelectedValue = pCombo.SelectedValue
      Else
        currentSelectedValue = Nothing
      End If

      EnumNames = System.Enum.GetNames(pEnumeration)

      ' Clear Existing Binding
      pCombo.DataSource = Nothing
      pCombo.DisplayMember = ""
      pCombo.ValueMember = ""
      pCombo.Items.Clear()

      ' Build new Source table
      ' Linking directly to the source had odd side affects.

      If (pLeadingBlank = True) Then
        Try
          pCombo.Items.Add(New Telerik.WinControls.UI.RadComboBoxItem(pLeadingBlankLabel, pLeadingBlankValue))

        Catch ex As Exception
        End Try
      End If

      If EnumNames.Length > 0 Then
        For EnumCounter = 0 To (EnumNames.Length - 1)

          NewNameBuilder = New System.Text.StringBuilder
          ThisName = EnumNames(EnumCounter)
          For CharCounter = 0 To (ThisName.Length - 1)
            thischar = ThisName.Substring(CharCounter, 1)
            If (CharCounter > 0) AndAlso (Char.IsUpper(thischar)) Then
              NewNameBuilder.Append(" ")
            End If
            NewNameBuilder.Append(thischar)
          Next

          pCombo.Items.Add(New Telerik.WinControls.UI.RadComboBoxItem(NewNameBuilder.ToString, CInt(System.Enum.Parse(pEnumeration, EnumNames(EnumCounter)))))
        Next

      End If

      ' Re-set existing value
      If Not (currentSelectedValue Is Nothing) Then
        Try
          pCombo.SelectedValue = currentSelectedValue
        Catch ex As Exception
        End Try
      End If

    Catch ex As Exception
    Finally

      SetToolStripText(VeniceStatusBar, OrgText)

    End Try

  End Sub

  ''' <summary>
  ''' Sets the master name combo.
  ''' </summary>
  ''' <param name="pCombo">The p combo.</param>
  ''' <param name="pSelectString">The p select string.</param>
  ''' <param name="pLeadingBlank">if set to <c>true</c> [p leading blank].</param>
  ''' <param name="pLeadingBlankValue">The p leading blank value.</param>
  ''' <param name="pLeadingBlankText">The p leading blank text.</param>
  Public Sub SetMasterNameCombo(ByVal pCombo As System.Windows.Forms.ComboBox, ByVal pSelectString As String, Optional ByVal pLeadingBlank As Boolean = False, Optional ByVal pLeadingBlankValue As Object = "<DBNull>", Optional ByVal pLeadingBlankText As String = "")
    ' ************************************************************************************
    '
    ' ************************************************************************************

    Dim currentSelectedValue As Object = Nothing
    Dim IsCurrentValue As Boolean = False
    Dim currentText As String = ""

    Dim OrgText As String = ""

    Try
      If (pCombo Is Nothing) Then Exit Sub
      If (_MasternameDictionary Is Nothing) Then Exit Sub

      OrgText = VeniceStatusBar.Text
      VeniceStatusBar.Text = "Building Mastername Combo"

      Try
        If (pCombo.SelectedIndex >= 0) Then
          currentSelectedValue = pCombo.SelectedValue
          IsCurrentValue = True
        ElseIf (pCombo.Text.Length > 0) Then
          currentText = pCombo.Text
        End If
      Catch ex As Exception
        currentSelectedValue = Nothing
      End Try

      Dim bs As BindingSource

      ' Clear existing BS ?

      If (pCombo.DataSource IsNot Nothing) Then
        If (GetType(BindingSource).IsInstanceOfType(pCombo.DataSource)) Then
          Try
            bs = CType(pCombo.DataSource, BindingSource)
            bs.Sort = ""
            bs.DataSource = Nothing
            bs.Clear()
            bs = Nothing

            pCombo.DisplayMember = ""
            pCombo.ValueMember = ""
            pCombo.DataSource = Nothing

          Catch ex As Exception
          End Try
        Else
          Try
            pCombo.DisplayMember = ""
            pCombo.ValueMember = ""
            pCombo.DataSource = Nothing
          Catch ex As Exception
          End Try
        End If
      End If

      ' Build Combo Bs.

      bs = New BindingSource

      bs.DataSource = _MasternameDictionary
      pCombo.DisplayMember = "Value"
      pCombo.ValueMember = "Key"
      pCombo.DataSource = bs

      If (IsCurrentValue) AndAlso (currentSelectedValue IsNot Nothing) Then
        pCombo.SelectedValue = currentSelectedValue
      ElseIf (currentText.Length > 0) Then
        pCombo.Text = currentText
      ElseIf (pCombo.Items.Count > 0) Then
        pCombo.SelectedIndex = 0
      End If

    Catch ex As Exception
      LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Setting Mastername Combo", ex.StackTrace, True)
    Finally
      SetToolStripText(VeniceStatusBar, OrgText)
    End Try


  End Sub

  ''' <summary>
  ''' Sets the locations combo.
  ''' </summary>
  ''' <param name="pCombo">The p combo.</param>
  ''' <param name="pLeadingBlank">if set to <c>true</c> [p leading blank].</param>
  Public Sub SetLocationsCombo(ByVal pCombo As System.Windows.Forms.ComboBox, Optional ByVal pLeadingBlank As Boolean = False)
    ' ************************************************************************************
    '
    ' ************************************************************************************

    Dim currentSelectedValue As Object = Nothing
    Dim IsCurrentValue As Boolean = False
    Dim currentText As String = ""

    Dim OrgText As String = ""

    Try
      If (pCombo Is Nothing) Then Exit Sub
      If (_LocationsDictionary Is Nothing) Then Exit Sub

      OrgText = VeniceStatusBar.Text
      SetToolStripText(VeniceStatusBar, "Building Locations Combo")

      If (pCombo.SelectedIndex >= 0) Then
        currentSelectedValue = pCombo.SelectedValue
        IsCurrentValue = True
      ElseIf (pCombo.Text.Length > 0) Then
        currentText = pCombo.Text
      End If

      Dim bs As BindingSource

      ' Clear existing BS ?

      If (pCombo.DataSource IsNot Nothing) AndAlso (GetType(BindingSource).IsInstanceOfType(pCombo.DataSource)) Then
        Try
          bs = CType(pCombo.DataSource, BindingSource)
          bs.Sort = ""
          bs.DataSource = Nothing
          bs.Clear()
          bs = Nothing
        Catch ex As Exception
        End Try
      End If

      ' Build Combo Bs.

      bs = New BindingSource

      bs.DataSource = _LocationsDictionary
      pCombo.DisplayMember = "Value"
      pCombo.ValueMember = "Key"
      pCombo.DataSource = bs

      If (IsCurrentValue) AndAlso (currentSelectedValue IsNot Nothing) Then
        pCombo.SelectedValue = currentSelectedValue
      ElseIf (currentText.Length > 0) Then
        pCombo.Text = currentText
      ElseIf (pCombo.Items.Count > 0) Then
        pCombo.SelectedIndex = 0
      End If

    Catch ex As Exception
      LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Setting Locations Combo", ex.StackTrace, True)
    Finally
      SetToolStripText(VeniceStatusBar, OrgText)
    End Try


  End Sub

  ''' <summary>
  ''' Refreshes the mastername dictionary.
  ''' </summary>
  Friend Sub RefreshMasternameDictionary()
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    If (_MasternameDictionary Is Nothing) OrElse (Me.IsClosing) Then Exit Sub

    Try

      Dim MasternameDataView As DataView = PertracData.GetPertracInstruments
      Dim ExistingKeys() As Integer
      Dim KeyCounter As Integer = 0
      Dim TableCounter As Integer = 0
      Dim ValueOrdinal As Integer = MasternameDataView.Table.Columns("PertracCode").Ordinal
      Dim DisplayOrdinal As Integer = MasternameDataView.Table.Columns("ListDescription").Ordinal

      ' Add Default Row to the Dictionary
      If (_MasternameDictionary.Count = 0) Then
        _MasternameDictionary.Add(0, "")
      End If

      MasternameDataView.Sort = "PertracCode"

      ExistingKeys = CType(Me._MasternameDictionary.KeyArray, Integer())
      Array.Sort(ExistingKeys)

      While (KeyCounter < ExistingKeys.Length) OrElse (TableCounter < MasternameDataView.Count)
        If (KeyCounter < ExistingKeys.Length) AndAlso (TableCounter < MasternameDataView.Count) Then
          Select Case (ExistingKeys(KeyCounter)).CompareTo(MasternameDataView(TableCounter)(ValueOrdinal))
            Case Is > 0
              ' Add Entry to Dictionary

              _MasternameDictionary.Add(CInt(MasternameDataView(TableCounter)(ValueOrdinal)), MasternameDataView(TableCounter)(DisplayOrdinal).ToString)
              TableCounter += 1

            Case 0
              ' Keys are the Same

              If ExistingKeys(KeyCounter) > 0 Then
                If CStr(_MasternameDictionary(ExistingKeys(KeyCounter))) <> (MasternameDataView(TableCounter)(DisplayOrdinal).ToString) Then
                  _MasternameDictionary(ExistingKeys(KeyCounter)) = (MasternameDataView(TableCounter)(DisplayOrdinal).ToString)
                End If
              End If

              TableCounter += 1
              KeyCounter += 1

            Case Is < 0
              ' Delete Dictionary Entry

              _MasternameDictionary.Remove(ExistingKeys(KeyCounter))
              KeyCounter += 1

          End Select

        ElseIf (KeyCounter < ExistingKeys.Length) Then
          ' Delete Dictionary Entry

          _MasternameDictionary.Remove(ExistingKeys(KeyCounter))
          KeyCounter += 1

        ElseIf (TableCounter < MasternameDataView.Count) Then
          ' Add Entry to Dictionary

          _MasternameDictionary.Add(CInt(MasternameDataView(TableCounter)(ValueOrdinal)), MasternameDataView(TableCounter)(DisplayOrdinal).ToString)
          TableCounter += 1

        Else
          Exit While
        End If
      End While

    Catch ex As Exception
    Finally
      _MasternameDictionary.Sort()
    End Try

  End Sub

  ''' <summary>
  ''' Refreshes the locations dictionary.
  ''' </summary>
  Friend Sub RefreshLocationsDictionary()
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    If (_LocationsDictionary Is Nothing) OrElse (Me.IsClosing) Then Exit Sub

    Try

      Dim tblLocations As DSLocations.tblLocationsDataTable = CType(Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblLocations, False).Tables(0), DSLocations.tblLocationsDataTable)
      Dim SelectedRows() As DSLocations.tblLocationsRow
      Dim ExistingKeys() As Integer
      Dim KeyCounter As Integer = 0
      Dim TableCounter As Integer = 0
      Dim ValueString As String

      ' Add Default Row to the Dictionary
      If (_LocationsDictionary.Count = 0) Then
        _LocationsDictionary.Add(0, "")
      End If

      SelectedRows = CType(tblLocations.Select("True", "CityID"), DSLocations.tblLocationsRow())
      ExistingKeys = CType(Me._LocationsDictionary.KeyArray, Integer())
      Array.Sort(ExistingKeys)

      While (KeyCounter < ExistingKeys.Length) OrElse (TableCounter < SelectedRows.Length)
        If (KeyCounter < ExistingKeys.Length) AndAlso (TableCounter < SelectedRows.Length) Then
          Select Case (ExistingKeys(KeyCounter)).CompareTo(SelectedRows(TableCounter).CityID)
            Case Is > 0
              ' Add Entry to Dictionary
              ValueString = SelectedRows(TableCounter).City & ", " & SelectedRows(TableCounter).RegionName

              _LocationsDictionary.Add(SelectedRows(TableCounter).CityID, ValueString)
              TableCounter += 1

            Case 0
              ' Keys are the Same
              ValueString = SelectedRows(TableCounter).City & ", " & SelectedRows(TableCounter).RegionName

              If CStr(_LocationsDictionary(ExistingKeys(KeyCounter))) <> ValueString Then
                _LocationsDictionary(ExistingKeys(KeyCounter)) = ValueString
              End If

              TableCounter += 1
              KeyCounter += 1

            Case Is < 0
              ' Delete Dictionary Entry

              If ExistingKeys(KeyCounter) > 0 Then
                _LocationsDictionary.Remove(ExistingKeys(KeyCounter))
              End If

              KeyCounter += 1

          End Select

        ElseIf (KeyCounter < ExistingKeys.Length) Then
          ' Delete Dictionary Entry

          _LocationsDictionary.Remove(ExistingKeys(KeyCounter))
          KeyCounter += 1

        ElseIf (TableCounter < SelectedRows.Length) Then
          ' Add Entry to Dictionary
          ValueString = SelectedRows(TableCounter).City & ", " & SelectedRows(TableCounter).RegionName

          _LocationsDictionary.Add(SelectedRows(TableCounter).CityID, ValueString)
          TableCounter += 1

        Else
          Exit While
        End If
      End While

    Catch ex As Exception
    Finally
      _LocationsDictionary.Sort()
    End Try

  End Sub

  ''' <summary>
  ''' Sets the width of the combo drop down.
  ''' </summary>
  ''' <param name="pCombo">The p combo.</param>
  Public Sub SetComboDropDownWidth(ByRef pCombo As System.Windows.Forms.ComboBox)
    ' ************************************************************************************
    ' 
    ' ************************************************************************************

    Dim thisDrowDownWidth As Integer
    Dim SizingBitmap As Bitmap
    Dim SizingGraphics As Graphics
    Dim stringSize As SizeF

    Dim ItemString As String
    Dim ItemObject As Object
    Dim ItemCount As Integer

    If (pCombo Is Nothing) Then
      Exit Sub
    End If

    If (pCombo.Items.Count <= 0) Then
      Exit Sub
    End If

    Try

      thisDrowDownWidth = pCombo.DropDownWidth
      SizingBitmap = New Bitmap(1, 1, System.Drawing.Imaging.PixelFormat.Format32bppArgb)
      SizingGraphics = Graphics.FromImage(SizingBitmap)

      For ItemCount = 0 To (pCombo.Items.Count - 1)
        ItemObject = pCombo.Items(ItemCount)
        ItemString = ""

        If (pCombo.DataSource Is Nothing) Then
          ItemString = ItemObject.ToString
        ElseIf GetType(DataRowView).IsInstanceOfType(ItemObject) Then
          ItemString = CType(ItemObject, DataRowView).Row(pCombo.DisplayMember).ToString()
        ElseIf GetType(DataRow).IsInstanceOfType(ItemObject) Then
          ItemString = CType(ItemObject, DataRow)(pCombo.DisplayMember).ToString()
        ElseIf GetType(KeyValuePair(Of Integer, String)).IsInstanceOfType(ItemObject) Then
          ItemString = CType(ItemObject, KeyValuePair(Of Integer, String)).Value
        ElseIf TypeOf ItemObject Is ILookup Then
          ItemString = CType(ItemObject, ILookup).Value.ToString.ToUpper
        Else
          Try
            ItemString = ItemObject.ToString()
          Catch ex As Exception
            ItemString = ""
          End Try
        End If

        If (ItemString.Length > 0) Then
          stringSize = SizingGraphics.MeasureString(ItemString, pCombo.Font)
          If (stringSize.Width) > thisDrowDownWidth Then
            thisDrowDownWidth = CInt(stringSize.Width)
          End If
        End If
      Next

      If (pCombo.DropDownWidth < thisDrowDownWidth) Then
        pCombo.DropDownWidth = thisDrowDownWidth
      End If

    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Clears the combo selection.
  ''' </summary>
  ''' <param name="pCombo">The p combo.</param>
  Public Sub ClearComboSelection(ByRef pCombo As Object)

    If (TypeOf pCombo Is ComboBox) Then
      Dim ThisCombo As ComboBox = CType(pCombo, ComboBox)

      Try
        If (TypeOf ThisCombo.FindForm Is StandardVeniceForm) Then
          If CType(ThisCombo.FindForm, StandardVeniceForm).InUse = False Then
            Exit Sub
          End If
        End If
      Catch ex As Exception
      End Try

      Try
        If ThisCombo.Items.Count > 0 Then
          ThisCombo.SelectedIndex = 0
          ThisCombo.SelectedIndex = -1
        End If
      Catch ex As Exception
      End Try

    ElseIf (TypeOf pCombo Is Telerik.WinControls.UI.RadComboBox) Then
      Try
        CType(pCombo, Telerik.WinControls.UI.RadComboBox).SelectedValue = Nothing
      Catch ex As Exception
      End Try
    End If

  End Sub

  ''' <summary>
  ''' Sets the form tool tip.
  ''' </summary>
  ''' <param name="thisForm">The this form.</param>
  ''' <param name="pFormTooltip">The p form tooltip.</param>
  Public Sub SetFormToolTip(ByRef thisForm As Form, ByRef pFormTooltip As ToolTip)
    ' ************************************************************************************
    ' 
    ' ************************************************************************************

    Dim ToolTipDS As DSVeniceFormTooltips
    Dim SelectedToolTipRows() As DSVeniceFormTooltips.tblVeniceFormTooltipsRow
    Dim ToolTipRow As DSVeniceFormTooltips.tblVeniceFormTooltipsRow

    pFormTooltip.InitialDelay = 2000

    Try
      pFormTooltip.RemoveAll()

      ToolTipDS = Me.Load_Table(RenaissanceStandardDatasets.tblVeniceFormTooltips, False)

      If (Not (ToolTipDS Is Nothing)) AndAlso (Not (ToolTipDS.tblVeniceFormTooltips Is Nothing)) Then
        SelectedToolTipRows = ToolTipDS.tblVeniceFormTooltips.Select("FormName='" & thisForm.Name & "'")

        If (Not (SelectedToolTipRows Is Nothing)) AndAlso (SelectedToolTipRows.Length > 0) Then
          For Each ToolTipRow In SelectedToolTipRows

            SetControlToolTip(thisForm, pFormTooltip, ToolTipRow.ControlName, ToolTipRow.ToolTip, True)

          Next

        End If
      End If

    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Sets the control tool tip.
  ''' </summary>
  ''' <param name="ParentControl">The parent control.</param>
  ''' <param name="pFormTooltip">The p form tooltip.</param>
  ''' <param name="TargetControlName">Name of the target control.</param>
  ''' <param name="ControlToolTipText">The control tool tip text.</param>
  ''' <param name="ExitOnOneFind">if set to <c>true</c> [exit on one find].</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function SetControlToolTip(ByVal ParentControl As Control, ByRef pFormTooltip As ToolTip, ByVal TargetControlName As String, ByVal ControlToolTipText As String, Optional ByVal ExitOnOneFind As Boolean = True) As Boolean
    ' ************************************************************************************
    ' 
    ' ************************************************************************************

    Try

      If (ParentControl.Name = TargetControlName) Then
        pFormTooltip.SetToolTip(ParentControl, ControlToolTipText)

        If (ExitOnOneFind) Then
          Return True
        End If
      End If

      For Each c As Control In ParentControl.Controls
        Try

          If (c.Name = TargetControlName) Then
            pFormTooltip.SetToolTip(c, ControlToolTipText)

            If (ExitOnOneFind) Then
              Return True
            End If
          End If

          If (c.Controls IsNot Nothing) AndAlso (c.Controls.Count > 0) Then
            If (SetControlToolTip(c, pFormTooltip, TargetControlName, ControlToolTipText, ExitOnOneFind)) And (ExitOnOneFind) Then
              Return True
            End If
          End If

        Catch ex As Exception
        End Try

      Next

    Catch ex As Exception
    End Try

  End Function

#End Region

#Region " Generic Form Update Code"

  ''' <summary>
  ''' Adds the form update.
  ''' </summary>
  ''' <param name="pForm">The p form.</param>
  ''' <param name="pUpdateProcess">The p update process.</param>
  ''' <param name="pUpdatePeriod">The p update period.</param>
  ''' <returns>RenaissanceTimerUpdateClass.</returns>
  Public Function AddFormUpdate(ByVal pForm As Windows.Forms.Form, ByRef pUpdateProcess As RenaissanceTimerUpdateClass.UpdateProcessDelegate, Optional ByVal pUpdatePeriod As Integer = 400) As RenaissanceTimerUpdateClass

    ' ************************************************************************************
    '
    '
    '
    ' ************************************************************************************

    Try
      If (pForm Is Nothing) OrElse (pUpdateProcess Is Nothing) Then
        Return Nothing
        Exit Function
      End If

      SyncLock FormUpdateObjects
        If (FormUpdateObjects.ContainsKey(pForm)) Then
          Try
            FormUpdateObjects(pForm).Clear()
          Catch ex As Exception
          End Try

          FormUpdateObjects.Remove(pForm)
        End If

        FormUpdateObjects.Add(pForm, New RenaissanceTimerUpdateClass(pUpdateProcess, pUpdatePeriod))

        Return FormUpdateObjects(pForm)

      End SyncLock

    Catch ex As Exception
    End Try

    Return Nothing

  End Function

  ''' <summary>
  ''' Removes the form update.
  ''' </summary>
  ''' <param name="pForm">The p form.</param>
  Public Sub RemoveFormUpdate(ByVal pForm As Windows.Forms.Form)

    ' ************************************************************************************
    '
    '
    '
    ' ************************************************************************************

    Try
      If (pForm Is Nothing) Then
        Exit Sub
      End If

      SyncLock FormUpdateObjects
        If (FormUpdateObjects.ContainsKey(pForm)) Then
          Try
            FormUpdateObjects(pForm).Clear()
          Catch ex As Exception
          End Try

          FormUpdateObjects.Remove(pForm)
        End If

      End SyncLock

    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Handles the Tick event of the FormUpdateDataTimer control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="Args">The <see cref="EventArgs"/> instance containing the event data.</param>
  Private Sub FormUpdateDataTimer_Tick(ByVal sender As Object, ByVal Args As EventArgs)
    ' ************************************************************************************
    '
    '
    '
    ' ************************************************************************************

    Try
      ' The main Application icon sometimes disappers, See if this helps.

      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        If (Not Me.ShowInTaskbar) Then
          Me.ShowInTaskbar = True
          Me.WindowState = FormWindowState.Normal
        End If
      End If
    Catch ex As Exception
    End Try

    Try

      Dim KeyForms() As Windows.Forms.Form = Nothing
      Dim ThisForm As Windows.Forms.Form = Nothing

      If (FormUpdateObjects.Count > 0) Then
        SyncLock FormUpdateObjects
          KeyForms = CType(Array.CreateInstance(GetType(Windows.Forms.Form), FormUpdateObjects.Count), Form())
          FormUpdateObjects.Keys.CopyTo(KeyForms, 0)
        End SyncLock
      Else
        Exit Sub
      End If

      If (KeyForms IsNot Nothing) AndAlso (KeyForms.Length > 0) Then
        For Each ThisForm In KeyForms
          If (ThisForm IsNot Nothing) Then
            If (ThisForm.IsDisposed) Then
              SyncLock FormUpdateObjects
                Try
                  FormUpdateObjects(ThisForm).Clear()
                Catch ex As Exception
                End Try

                FormUpdateObjects.Remove(ThisForm)
              End SyncLock
            ElseIf (ThisForm.Created) Then
              Try
                FormUpdateObjects(ThisForm).CheckUpdate()
              Catch ex As Exception
              End Try
            End If
          End If
        Next
      End If

    Catch ex As Exception
    End Try

  End Sub

#End Region

#Region " Generic Combo Select Code"

  ' There Is an issue with the ComboBox control whereby if you select the combobox and type 
  ' One or more characters to select an item and then leave the combo by pressing the TAB key,
  ' Then the SelectedIndex (and other) properties revert to their starting values.
  ' This seems to be a generic .NET problem and is not specific to any of the coding in Venice.
  ' After EXTENSIVE research, I found that the SelectedIndex property reverts somehow between
  ' the 'Leave' and 'LostFocus' events. Furthermore I found that you can reset the SelectedIndex 
  ' in the 'LostFocus' event and it sticks.
  ' So this is what I have done :- Use the 'Tag' property to persist the SelectedIndex value.
  ' The Tag is initialised in the 'GotFocus' Event, Updated in the 'SelectedIndexChanged'
  ' Event and Re-set in the 'LostFocus' Event.
  '

  ''' <summary>
  ''' Handles the GotFocus event of the GenericCombo control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Public Sub GenericCombo_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs)
    Try
      If TypeOf sender Is ComboBox Then
        Dim thisCombo As ComboBox
        thisCombo = CType(sender, ComboBox)
        thisCombo.Tag = CType(sender, ComboBox).SelectedIndex
      End If
    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Handles the SelectedIndexChanged event of the GenericCombo control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Public Sub GenericCombo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    Try
      If TypeOf sender Is ComboBox Then
        Dim thisCombo As ComboBox
        thisCombo = CType(sender, ComboBox)
        thisCombo.Tag = CType(sender, ComboBox).SelectedIndex
      End If
    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Handles the LostFocus event of the GenericCombo control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Public Sub GenericCombo_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs)
    Try
      If TypeOf sender Is ComboBox Then
        Dim thisCombo As ComboBox
        thisCombo = CType(sender, ComboBox)

        If (Not (thisCombo.Tag Is Nothing)) AndAlso IsNumeric(thisCombo.Tag) AndAlso (CInt(thisCombo.Tag) >= 0) AndAlso (thisCombo.Items.Count > 0) Then
          If thisCombo.SelectedIndex <> CInt(thisCombo.Tag) Then
            thisCombo.SelectedIndex = thisCombo.Tag
          End If
        End If
      End If
    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Comboes the type of the select as you.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
  Public Sub ComboSelectAsYouType(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
    ' *******************************************************************************
    ' This routine provides the 'Select-as-you-type' functionality on some of the combo boxes.
    ' 
    ' It does not allow you to type an item that is not in the combo items.
    '
    ' Note that it will only work for Combo Boxes derived from a Data Table or View.
    ' *******************************************************************************

    Dim SelectString As String
    'Dim selectRow As DataRow
    'Dim selectRowView As DataRowView
    'Dim IndexCounter As Integer
    Dim ThisComboBox As ComboBox
    Dim StartingIndex As Integer = 0

    Try
      SyncLock sender

        ' Cast the sender to type Combo Box.
        ' In the event of an error, exit silently.
        Try
          ThisComboBox = CType(sender, ComboBox)
        Catch ex As Exception
          Exit Sub
        End Try

        ' Check for <Enter>

        If e.KeyCode = System.Windows.Forms.Keys.Enter Then
          ThisComboBox.SelectionStart = 0
          ThisComboBox.SelectionLength = ThisComboBox.Text.Length
          Exit Sub
        End If

        SelectString = ThisComboBox.Text

        If (ThisComboBox.Tag IsNot Nothing) AndAlso IsNumeric(ThisComboBox.Tag) AndAlso (SelectString.Length > 1) Then
          StartingIndex = CInt(ThisComboBox.Tag)
        End If

        If e.KeyCode = System.Windows.Forms.Keys.Back Then
          If SelectString.Length > 0 Then
            SelectString = SelectString.Substring(0, SelectString.Length - 1)
          End If
        End If

        If e.KeyCode = System.Windows.Forms.Keys.Down Then
          Exit Sub
        End If

        If e.KeyCode = System.Windows.Forms.Keys.Up Then
          Exit Sub
        End If

startSearch:

        ' Search the Combo Data Items for an item starting with the typed text.

        If SelectString.Length <= 0 Then
          If ThisComboBox.Items.Count > 0 Then
            ThisComboBox.SelectedIndex = 0
            ThisComboBox.SelectionStart = 0
            ThisComboBox.SelectionLength = ThisComboBox.Text.Length
          Else
            ThisComboBox.SelectedIndex = -1
          End If

          Exit Sub
        End If

        Dim IndexString As String

        Try
          Dim RIndex As Integer

          If (StartingIndex <= 0) AndAlso (ThisComboBox.Items.Count > 1000) Then
            StartingIndex = BestGuessStartingIndex(ThisComboBox, SelectString, StartingIndex)
          End If

          RIndex = ThisComboBox.FindString(SelectString, Math.Max(0, StartingIndex - 1))
          If (RIndex >= 0) Then
            If (ThisComboBox.SelectedIndex <> RIndex) Then
              IndexString = GetComboItemStringValue(ThisComboBox.Items(RIndex), ThisComboBox.DisplayMember)

              'If GetType(DataRowView).IsInstanceOfType(ThisComboBox.Items(RIndex)) Then
              '	IndexString = CType(ThisComboBox.Items(RIndex), DataRowView)(ThisComboBox.DisplayMember).ToString.ToUpper
              'ElseIf GetType(DataRow).IsInstanceOfType(ThisComboBox.Items(RIndex)) Then
              '	IndexString = CType(ThisComboBox.Items(RIndex), DataRow)(ThisComboBox.DisplayMember).ToString.ToUpper
              'ElseIf GetType(KeyValuePair(Of Integer, String)).IsInstanceOfType(ThisComboBox.Items(RIndex)) Then
              '	IndexString = CType(ThisComboBox.Items(RIndex), KeyValuePair(Of Integer, String)).Value
              'ElseIf TypeOf ThisComboBox.Items(RIndex) Is ILookup Then
              '	IndexString = CType(ThisComboBox.Items(RIndex), ILookup).Value.ToString.ToUpper
              'Else
              '	Try
              '		IndexString = CStr(ThisComboBox.Items(RIndex)).ToUpper
              '	Catch ex As Exception
              '		Exit Sub
              '	End Try
              'End If

              ThisComboBox.SelectedIndex = RIndex
              ThisComboBox.SelectionStart = SelectString.Length
              ThisComboBox.SelectionLength = IndexString.Length - SelectString.Length
            End If
            Exit Sub
          End If
        Catch ex As Exception
        End Try

        ' String not found
        SelectString = SelectString.Substring(0, SelectString.Length - 1)
        GoTo startSearch

      End SyncLock

    Catch ex As Exception
    End Try


  End Sub

  ''' <summary>
  ''' Handles the wildcard event of the ComboSelectAsYouType control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
  Public Sub ComboSelectAsYouType_wildcard(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
    ' *******************************************************************************
    ' Specualtive, not used, under development.
    ' *******************************************************************************


    '    Dim SelectString As String
    '    Dim SourceTable As DataTable = Nothing
    '    'Dim selectRowView As DataRowView
    '    'Dim IndexCounter As Integer
    '    Dim ThisComboBox As ComboBox
    '    Dim StartingIndex As Integer = 0

    '    Try

    '      SyncLock sender

    '        ' Cast the sender to type Combo Box.
    '        ' In the event of an error, exit silently.
    '        Try
    '          ThisComboBox = CType(sender, ComboBox)
    '        Catch ex As Exception
    '          Exit Sub
    '        End Try

    '        Try
    '          If Not (TypeOf ThisComboBox.DataSource Is DataTable) Then
    '            ComboSelectAsYouType(sender, e)
    '            Exit Sub
    '          End If
    '          SourceTable = CType(ThisComboBox.DataSource, DataTable)

    '        Catch ex As Exception
    '          Exit Sub
    '        End Try


    '        ' Check for <Enter>

    '        If e.KeyCode = System.Windows.Forms.Keys.Enter Then
    '          ThisComboBox.SelectionStart = 0
    '          ThisComboBox.SelectionLength = ThisComboBox.Text.Length
    '          Exit Sub
    '        End If

    '        SelectString = ThisComboBox.Text

    '        If (ThisComboBox.Tag IsNot Nothing) AndAlso IsNumeric(ThisComboBox.Tag) AndAlso (SelectString.Length > 1) Then
    '          StartingIndex = CInt(ThisComboBox.Tag)
    '        End If

    '        If e.KeyCode = System.Windows.Forms.Keys.Back Then
    '          If SelectString.Length > 0 Then
    '            SelectString = SelectString.Substring(0, SelectString.Length - 1)
    '          End If
    '        End If

    '        If e.KeyCode = System.Windows.Forms.Keys.Down Then
    '          Exit Sub
    '        End If

    '        If e.KeyCode = System.Windows.Forms.Keys.Up Then
    '          Exit Sub
    '        End If

    'startSearch:

    '        ' Search the Combo Data Items for an item starting with the typed text.

    '        If SelectString.Length <= 0 Then
    '          If ThisComboBox.Items.Count > 0 Then
    '            ThisComboBox.SelectedIndex = 0
    '            ThisComboBox.SelectionStart = 0
    '            ThisComboBox.SelectionLength = ThisComboBox.Text.Length
    '          Else
    '            ThisComboBox.SelectedIndex = -1
    '          End If

    '          Exit Sub
    '        End If

    '        Dim IndexString As String

    '        Try
    '          Dim RIndex As Integer

    '          If (StartingIndex <= 0) AndAlso (ThisComboBox.Items.Count > 1000) Then
    '            StartingIndex = BestGuessStartingIndex(ThisComboBox, SelectString, StartingIndex)
    '          End If

    '          RIndex = ThisComboBox.FindString(SelectString, Math.Max(0, StartingIndex - 1))
    '          If (RIndex >= 0) Then
    '            If (ThisComboBox.SelectedIndex <> RIndex) Then
    '              IndexString = GetComboItemStringValue(ThisComboBox.Items(RIndex), ThisComboBox.DisplayMember)

    '              'If GetType(DataRowView).IsInstanceOfType(ThisComboBox.Items(RIndex)) Then
    '              '	IndexString = CType(ThisComboBox.Items(RIndex), DataRowView)(ThisComboBox.DisplayMember).ToString.ToUpper
    '              'ElseIf GetType(DataRow).IsInstanceOfType(ThisComboBox.Items(RIndex)) Then
    '              '	IndexString = CType(ThisComboBox.Items(RIndex), DataRow)(ThisComboBox.DisplayMember).ToString.ToUpper
    '              'ElseIf GetType(KeyValuePair(Of Integer, String)).IsInstanceOfType(ThisComboBox.Items(RIndex)) Then
    '              '	IndexString = CType(ThisComboBox.Items(RIndex), KeyValuePair(Of Integer, String)).Value
    '              'ElseIf TypeOf ThisComboBox.Items(RIndex) Is ILookup Then
    '              '	IndexString = CType(ThisComboBox.Items(RIndex), ILookup).Value.ToString.ToUpper
    '              'Else
    '              '	Try
    '              '		IndexString = CStr(ThisComboBox.Items(RIndex)).ToUpper
    '              '	Catch ex As Exception
    '              '		Exit Sub
    '              '	End Try
    '              'End If

    '              ThisComboBox.SelectedIndex = RIndex
    '              ThisComboBox.SelectionStart = SelectString.Length
    '              ThisComboBox.SelectionLength = IndexString.Length - SelectString.Length
    '            End If
    '            Exit Sub
    '          End If
    '        Catch ex As Exception
    '        End Try

    '        ' String not found
    '        SelectString = SelectString.Substring(0, SelectString.Length - 1)
    '        GoTo startSearch

    '      End SyncLock

    '    Catch ex As Exception
    '    End Try


  End Sub

  ''' <summary>
  ''' Handles the NoMatch event of the ComboSelectAsYouType control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
  Public Sub ComboSelectAsYouType_NoMatch(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
    ' *******************************************************************************
    ' This routine provides the 'Select-as-you-type' functionality on some of the combo boxes.
    ' 
    ' It does allow you to type an item that is not in the combo items.
    '
    ' Note that it will only work for Combo Boxes derived from a Data Table or View.
    ' *******************************************************************************

    Dim SelectString As String
    'Dim selectRow As DataRow
    'Dim selectRowView As DataRowView
    'Dim IndexCounter As Integer
    Dim ThisComboBox As ComboBox
    Dim StartingIndex As Integer = 0

    Try

      ' Cast the sender to type Combo Box.
      ' In the event of an error, exit silently.
      Try
        ThisComboBox = CType(sender, ComboBox)
      Catch ex As Exception
        Exit Sub
      End Try

      ' Check for <Enter>

      If e.KeyCode = System.Windows.Forms.Keys.Enter Then
        ThisComboBox.SelectionStart = 0
        ThisComboBox.SelectionLength = ThisComboBox.Text.Length
        Exit Sub
      End If

      SelectString = ThisComboBox.Text

      If (ThisComboBox.Tag IsNot Nothing) AndAlso IsNumeric(ThisComboBox.Tag) AndAlso (SelectString.Length > 1) Then
        StartingIndex = CInt(ThisComboBox.Tag)
      End If

      If e.KeyCode = System.Windows.Forms.Keys.Back Then
        If SelectString.Length > 0 Then
          SelectString = SelectString.Substring(0, SelectString.Length - 1)
        End If
      End If

      If e.KeyCode = System.Windows.Forms.Keys.Down Then
        Exit Sub
      End If

      If e.KeyCode = System.Windows.Forms.Keys.Down Then
        Exit Sub
      End If

startSearch:

      ' Search the Combo Data Items for an item starting with the typed text.

      If SelectString.Length <= 0 Then
        If ThisComboBox.Items.Count > 0 Then
          ThisComboBox.SelectedIndex = 0
          ThisComboBox.SelectionStart = 0
          ThisComboBox.SelectionLength = ThisComboBox.Text.Length
        Else
          ThisComboBox.SelectedIndex = -1
        End If

        Exit Sub
      End If

      Dim IndexString As String

      Try
        Dim RIndex As Integer

        If (StartingIndex <= 0) AndAlso (ThisComboBox.Items.Count > 1000) Then
          StartingIndex = BestGuessStartingIndex(ThisComboBox, SelectString, StartingIndex)
        End If

        RIndex = ThisComboBox.FindString(SelectString, Math.Max(0, StartingIndex - 1))
        If (RIndex >= 0) Then
          If (ThisComboBox.SelectedIndex <> RIndex) Then
            IndexString = GetComboItemStringValue(ThisComboBox.Items(RIndex), ThisComboBox.DisplayMember)

            'If GetType(DataRowView).IsInstanceOfType(ThisComboBox.Items(RIndex)) Then
            '	IndexString = CType(ThisComboBox.Items(RIndex), DataRowView)(ThisComboBox.DisplayMember).ToString.ToUpper
            'ElseIf GetType(DataRow).IsInstanceOfType(ThisComboBox.Items(RIndex)) Then
            '	IndexString = CType(ThisComboBox.Items(RIndex), DataRow)(ThisComboBox.DisplayMember).ToString.ToUpper
            'ElseIf GetType(KeyValuePair(Of Integer, String)).IsInstanceOfType(ThisComboBox.Items(RIndex)) Then
            '	IndexString = CType(ThisComboBox.Items(RIndex), KeyValuePair(Of Integer, String)).Value
            'ElseIf TypeOf ThisComboBox.Items(RIndex) Is ILookup Then
            '	IndexString = CType(ThisComboBox.Items(RIndex), ILookup).Value.ToString.ToUpper
            'Else
            '	Try
            '		IndexString = CStr(ThisComboBox.Items(RIndex)).ToUpper
            '	Catch ex As Exception
            '		Exit Sub
            '	End Try
            'End If

            ThisComboBox.SelectedIndex = RIndex
            ThisComboBox.SelectionStart = SelectString.Length
            ThisComboBox.SelectionLength = IndexString.Length - SelectString.Length
          End If
          Exit Sub
        End If
      Catch ex As Exception
      End Try

      ' String not found
      ThisComboBox.Tag = Nothing ' Stop LotFocus Event setting the SelectedIndex
      Exit Sub

    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Bests the index of the guess starting.
  ''' </summary>
  ''' <param name="ThisComboBox">The this combo box.</param>
  ''' <param name="SelectString">The select string.</param>
  ''' <param name="OriginalStartingIndex">Index of the original starting.</param>
  ''' <returns>System.Int32.</returns>
  Private Function BestGuessStartingIndex(ByVal ThisComboBox As ComboBox, ByVal SelectString As String, ByVal OriginalStartingIndex As Integer) As Integer

    Dim RVal As Integer = OriginalStartingIndex

    Try
      Dim TopIndex As Integer = 0
      Dim BottomIndex As Integer = ThisComboBox.Items.Count - 1
      Dim MidIndex As Integer
      Dim ThisString As String
      Dim CVal As Double

      While (BottomIndex - TopIndex) > 5
        MidIndex = CInt((TopIndex + BottomIndex) / 2)

        ThisString = GetComboItemStringValue(ThisComboBox.Items(MidIndex), ThisComboBox.DisplayMember)
        CVal = ThisString.CompareTo(SelectString)

        If (CVal = 0) Then
          RVal = MidIndex
          Exit While
        ElseIf (CVal > 0) Then
          BottomIndex = MidIndex
        Else
          TopIndex = MidIndex
        End If

        RVal = TopIndex
      End While

    Catch ex As Exception
    End Try

    Return RVal

  End Function

  ''' <summary>
  ''' Gets the combo item string value.
  ''' </summary>
  ''' <param name="pComboItem">The p combo item.</param>
  ''' <param name="DisplayMember">The display member.</param>
  ''' <returns>System.String.</returns>
  Private Function GetComboItemStringValue(ByVal pComboItem As Object, ByVal DisplayMember As String) As String

    If TypeOf pComboItem Is DataRowView Then ' GetType(DataRowView).IsInstanceOfType(ThisComboBox.Items(IndexCounter)) Then
      Return CType(pComboItem, DataRowView).Row(DisplayMember).ToString.ToUpper
    ElseIf TypeOf pComboItem Is DataRow Then ' GetType(DataRow).IsInstanceOfType(ThisComboBox.Items(IndexCounter)) Then
      Return CType(pComboItem, DataRow)(DisplayMember).ToString.ToUpper
    ElseIf TypeOf pComboItem Is KeyValuePair(Of Integer, String) Then ' GetType(KeyValuePair(Of Integer, String)).IsInstanceOfType(ThisComboBox.Items(IndexCounter)) Then
      Return CType(pComboItem, KeyValuePair(Of Integer, String)).Value.ToUpper
    ElseIf TypeOf pComboItem Is ILookup Then   'TypeOf ThisComboBox.Items(IndexCounter) Is ILookup Then
      Return CType(pComboItem, ILookup).Value.ToString.ToUpper
    Else
      Try
        Return CStr(pComboItem).ToUpper
      Catch ex As Exception
      End Try
    End If

    Return ""

  End Function

#End Region

#Region " tblSystemString Functions "

  ''' <summary>
  ''' Get_s the system string.
  ''' </summary>
  ''' <param name="pStringName">Name of the p string.</param>
  ''' <returns>System.String.</returns>
  Friend Function Get_SystemString(ByVal pStringName As String) As String

    Dim adpSystemStrings As SqlDataAdapter
    Dim tblSystemstrings As New DataTable

    adpSystemStrings = Me.MainDataHandler.Get_Adaptor("adp_GetSystemString", VENICE_CONNECTION, "tblSystemStrings")

    If (adpSystemStrings Is Nothing) Then
      Return ""
    End If

    Try
      SyncLock adpSystemStrings.SelectCommand.Connection
        SyncLock adpSystemStrings
          adpSystemStrings.SelectCommand.Parameters("@StringDescription").Value = pStringName
          adpSystemStrings.Fill(tblSystemstrings)
        End SyncLock
      End SyncLock
    Catch ex As Exception
      Me.LogError(Me.Name & ", Get_SystemString()", LOG_LEVELS.Error, ex.Message, "Failed to Get System String.", ex.StackTrace, True)

      Return ""
    End Try

    If (tblSystemstrings Is Nothing) OrElse (tblSystemstrings.Rows.Count <= 0) Then
      Return ""
    End If

    Try
      Return CStr(tblSystemstrings.Rows(0)("StringValue"))
    Catch ex As Exception
      Me.LogError(Me.Name & ", Get_SystemString()", LOG_LEVELS.Error, ex.Message, "Failed to Get System String.", ex.StackTrace, True)
      Return ""
    End Try

    Return ""

  End Function

  ''' <summary>
  ''' Set_s the system string.
  ''' </summary>
  ''' <param name="pStringName">Name of the p string.</param>
  ''' <param name="pStringValue">The p string value.</param>
  ''' <returns>System.String.</returns>
  Friend Function Set_SystemString(ByVal pStringName As String, ByVal pStringValue As String) As String

    Dim adpSystemStrings As SqlDataAdapter
    Dim tblSystemstrings As New DataTable

    adpSystemStrings = Me.MainDataHandler.Get_Adaptor("adp_GetSystemString", VENICE_CONNECTION, "tblSystemStrings")

    If (adpSystemStrings Is Nothing) Then
      Return ""
    End If

    Try
      SyncLock adpSystemStrings.InsertCommand.Connection
        SyncLock adpSystemStrings
          adpSystemStrings.InsertCommand.Parameters("@StringDescription").Value = pStringName
          adpSystemStrings.InsertCommand.Parameters("@StringValue").Value = pStringValue

          Call adpSystemStrings.InsertCommand.ExecuteNonQuery()

          Return pStringValue

        End SyncLock
      End SyncLock
    Catch ex As Exception
      Me.LogError(Me.Name & ", Set_SystemString()", LOG_LEVELS.Error, ex.Message, "Failed to Set System String.", ex.StackTrace, True)

      Return ""
    End Try

    Return ""

  End Function


#End Region

#Region " Event Handler code : List / Clear object event handlers."

  ''' <summary>
  ''' Clears the control event subscribers.
  ''' </summary>
  ''' <param name="ThisControl">The this control.</param>
  Public Sub ClearControlEventSubscribers(ByRef ThisControl As Control)


    Try
      ClearObjectEventSubscribers(ThisControl)
    Catch ex As Exception
    End Try

    If (ThisControl.HasChildren) Then
      Dim ChildControl As Control

      For Each ChildControl In ThisControl.Controls
        ClearControlEventSubscribers(ChildControl)
      Next
    End If
  End Sub

  ''' <summary>
  ''' Clears the object event subscribers.
  ''' </summary>
  ''' <param name="target">The target.</param>
  Public Sub ClearObjectEventSubscribers(ByRef target As Object)
    Try

      Dim TargetType As Type = target.GetType
      Dim evAll As Reflection.EventInfo() = TargetType.GetEvents
      Dim evThis As Reflection.EventInfo
      Dim thisDelegate As [Delegate]
      Dim ListenerList() As System.Delegate

      Dim EventName As String

      For Each evThis In evAll
        Try
          EventName = evThis.Name

          ListenerList = GetEventSubscribers(target, EventName)

          For Each thisDelegate In ListenerList
            evThis.RemoveEventHandler(target, thisDelegate)
          Next
        Catch ex As Exception
        End Try
      Next

    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Gets the event subscribers.
  ''' </summary>
  ''' <param name="target">The target.</param>
  ''' <param name="eventName">Name of the event.</param>
  ''' <returns>Delegate[][].</returns>
  Public Function GetEventSubscribers(ByVal target As Object, ByVal eventName As String) As [Delegate]()

    Dim WinFormsEventName1 As String = ("Event" & eventName)
    Dim WinFormsEventName2 As String = (eventName & "Event")
    Dim WinFormsEventName3 As String = (eventName)

    Dim type1 As Type = target.GetType

    Do

      Dim fiThis As Reflection.FieldInfo

      fiThis = type1.GetField(WinFormsEventName1, (BindingFlags.NonPublic Or (BindingFlags.Static Or BindingFlags.Instance)))
      'If fi Is Nothing Then
      '  fi = type1.GetField(WinFormsEventName2, (BindingFlags.NonPublic Or (BindingFlags.Static Or BindingFlags.Instance)))
      'End If
      'If fi Is Nothing Then
      '  fi = type1.GetField(WinFormsEventName3, (BindingFlags.NonPublic Or (BindingFlags.Static Or BindingFlags.Instance)))
      'End If

      If fiThis IsNot Nothing Then
        Dim ThisDelegate As MulticastDelegate

        If (fiThis.Name = WinFormsEventName1) Or (fiThis.Name = WinFormsEventName2) Or (fiThis.Name = WinFormsEventName3) Then

          ThisDelegate = TryCast(fiThis.GetValue(target), MulticastDelegate)

          If (Not ThisDelegate Is Nothing) Then

            Return ThisDelegate.GetInvocationList

          End If

        End If

        If (fiThis.Name = WinFormsEventName1) Or (fiThis.Name = WinFormsEventName2) Or (fiThis.Name = WinFormsEventName3) Then

          Dim ThisHandlerList As System.ComponentModel.EventHandlerList = DirectCast(target.GetType.GetProperty("Events", (BindingFlags.FlattenHierarchy Or (BindingFlags.NonPublic Or BindingFlags.Instance))).GetValue(target, Nothing), System.ComponentModel.EventHandlerList)

          ThisDelegate = ThisHandlerList.Item(fiThis.GetValue(target))

          If (Not ThisDelegate Is Nothing) Then

            Return ThisDelegate.GetInvocationList

          End If

        End If
      End If

      type1 = type1.BaseType

    Loop While (Not type1 Is Nothing)

    Return New [Delegate](-1) {}

  End Function

#End Region

#Region "DataReader"

  ' *****************************************************************************
  ' OK, what I wanted was a way to load data from the database without blocking the UI thread.
  ' The hope being that this will make Venice more responsive.
  ' 
  ' If instead of using the table.fill(sqlReader) construct, I get the reader first 
  ' (the time consuming bit) on a worker thread and then fill the table (the quick bit)
  ' on the UI thread then it all appears much better.
  '
  ' This pattern (?) passes an already initialised sqlCommand to a Worker thread which then 
  ' simply retuns the value of sqlCommand.ExecuteReader()
  '
  ' In order to get the sqlCommand in and the sqlReader out, the process accepts as its only 
  ' argument an instance of the GetDataReaderClass, pre-initialised with property 'MyCommand'
  ' set to the appropriate sqlCommand.
  ' When the function returns, the 'myReader' property will have been set to the return 
  ' value of MyCommand.ExecuteReader().
  ' 
  ' From the perspective of the calling process, the RunDataReader() function appears to block, 
  ' however the data function is executed on a BackgroundWorker thread and the UI thread sits in a 
  ' tight loop, calling DoEvents() repeatedly until the BackgroundWorker completes.
  ' 
  ' 
  ' *****************************************************************************

  ''' <summary>
  ''' Class GetDataReaderClass
  ''' </summary>
  Friend Class GetDataReaderClass
    ''' <summary>
    ''' My reader
    ''' </summary>
    Public myReader As SqlDataReader
    ''' <summary>
    ''' My command
    ''' </summary>
    Public MyCommand As SqlCommand

    ''' <summary>
    ''' Initializes a new instance of the <see cref="GetDataReaderClass"/> class.
    ''' </summary>
    Public Sub New()

    End Sub
  End Class

  ''' <summary>
  ''' Datas the reader do work.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="System.ComponentModel.DoWorkEventArgs"/> instance containing the event data.</param>
  Friend Sub DataReaderDoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) ' Handles backgroundWorker1.DoWork
    ' *****************************************************************************
    ' Simple pattern to get a SQLDataReader using a BackgroundWorker so as not
    ' to block the UI Thread.
    ' *****************************************************************************
    Dim thisGetReader As GetDataReaderClass = CType(e.Argument, GetDataReaderClass)

    Try
      thisGetReader.myReader = thisGetReader.MyCommand.ExecuteReader
    Catch ex As Exception
      thisGetReader.MyCommand.Connection.Close()
      SqlConnection.ClearPool(thisGetReader.MyCommand.Connection)
      thisGetReader.MyCommand.Connection.Open()

      thisGetReader.myReader = thisGetReader.MyCommand.ExecuteReader
    End Try

  End Sub

  ''' <summary>
  ''' Runs the data reader.
  ''' </summary>
  ''' <param name="DataReader">The data reader.</param>
  Friend Sub RunDataReader(ByRef DataReader As GetDataReaderClass)
    ' *****************************************************************************
    ' 
    ' *****************************************************************************

    Dim DataReaderBackgroundWorker As New BackgroundWorker()

    Try
      AddHandler DataReaderBackgroundWorker.DoWork, AddressOf DataReaderDoWork

      If (DataReader.MyCommand.Connection.State <> ConnectionState.Open) Then
        DataReader.MyCommand.Connection.Open()
      End If

      DataReaderBackgroundWorker.RunWorkerAsync(DataReader)

      Do
        Application.DoEvents()
        Threading.Thread.Sleep(20)
      Loop While DataReaderBackgroundWorker.IsBusy

    Catch ex As Exception
      LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in RunDataReader().", ex.StackTrace, False)
    Finally
      RemoveHandler DataReaderBackgroundWorker.DoWork, AddressOf DataReaderDoWork
    End Try

  End Sub


#End Region

#Region "Get Best NAV & FX"

  ''' <summary>
  ''' Gets the best NAV.
  ''' </summary>
  ''' <param name="pInstrumentID">The p instrument ID.</param>
  ''' <param name="pValueDate">The p value date.</param>
  ''' <returns>System.Double.</returns>
  Friend Function GetBestNAV(ByVal pInstrumentID As Integer, ByVal pValueDate As Date) As Double
    ' ****************************************************************************
    '
    '
    ' ****************************************************************************



    Dim myConnection As SqlConnection = Nothing
    Dim QueryCommand As New SqlCommand
    Dim InstrumentNAV As Object

    Try

      myConnection = GetVeniceConnection()

      QueryCommand.Connection = myConnection
      QueryCommand.CommandText = "SELECT dbo.fn_SingleInstrumentBestNAV(@InstrumentID, @ValueDate, 0, Null)"
      QueryCommand.Parameters.Clear()
      QueryCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@InstrumentID", System.Data.SqlDbType.Int, 4))
      QueryCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValueDate", System.Data.SqlDbType.DateTime, 8))
      QueryCommand.Parameters("@InstrumentID").Value = pInstrumentID
      QueryCommand.Parameters("@ValueDate").Value = pValueDate

      SyncLock QueryCommand.Connection
        InstrumentNAV = QueryCommand.ExecuteScalar
      End SyncLock

      If IsNumeric(InstrumentNAV) Then
        Return CDbl(InstrumentNAV)
      End If
    Catch ex As Exception
      LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error getting an Instrument Price.", ex.StackTrace, True)
    Finally
      If (myConnection IsNot Nothing) AndAlso (myConnection.State And ConnectionState.Open) Then
        myConnection.Close()
      End If
      QueryCommand.Connection = Nothing
    End Try

    Return 0
  End Function

  ''' <summary>
  ''' Gets the best NAV.
  ''' </summary>
  ''' <param name="pInstrumentID">The p instrument ID.</param>
  ''' <param name="pValueDate">The p value date.</param>
  ''' <returns>System.Double.</returns>
  Friend Function GetBestNAVandDate(ByVal pInstrumentID As Integer, ByVal pValueDate As Date, ByRef price As Double, ByRef priceDate As Date, ByRef instrumentType As String) As Boolean
    ' ****************************************************************************
    '
    '
    ' ****************************************************************************

    Dim command As New SqlCommand
    Dim DataReader As New VeniceMain.GetDataReaderClass
    Dim table As New DataTable

    Try
      command.CommandType = CommandType.StoredProcedure
      command.CommandText = "spu_GetSingleInstrumentBestNAVandDate"

      command.Parameters.Add(New System.Data.SqlClient.SqlParameter("@InstrumentID", System.Data.SqlDbType.Int, 4))
      command.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValueDate", System.Data.SqlDbType.DateTime, 8))
      command.Parameters.Add(New System.Data.SqlClient.SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime, 8))
      command.Parameters.Add(New System.Data.SqlClient.SqlParameter("@OnlyFinalPrices", System.Data.SqlDbType.Int, 4))
      command.Parameters("@InstrumentID").Value = pInstrumentID
      command.Parameters("@ValueDate").Value = pValueDate
      command.Parameters("@OnlyFinalPrices").Value = 0
      command.Parameters("@KnowledgeDate").Value = RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW

      command.Connection = GetVeniceConnection()

      DataReader.MyCommand = command

      RunDataReader(DataReader)

      If (DataReader.myReader Is Nothing) Then
        ' MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "DataReader.myReader is nothing in Check_ShowPercentOfNAV.", "", False)
        LoadTable_Custom(table, command)
        'NAVTable.Load(NAVCommand.ExecuteReader)
      Else
        'MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "DataReader.myReader OK in Check_ShowPercentOfNAV.", "", False)
        table.Load(DataReader.myReader)
      End If

      If (table.Rows.Count > 0) Then
        price = CDbl(table.Rows(0)("PriceNAV"))
        priceDate = CDate(table.Rows(0)("PriceDate"))
        instrumentType = CStr(table.Rows(0)("InstrumentTypeDescription"))
      Else
        price = Nothing
        priceDate = Nothing
        instrumentType = "#NOPRICEFOUND#" ' to say no price is defined

      End If

      table = Nothing

    Catch ex As Exception
      LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error getting price for instrument " + CStr(pInstrumentID), ex.StackTrace, False)
      Return False
    Finally
      If (command.Connection IsNot Nothing) Then
        If (command.Connection.State And ConnectionState.Open) Then
          command.Connection.Close()
        End If
        command.Connection = Nothing
      End If
    End Try

    Return True


  End Function

  ''' <summary>
  ''' Gets the best FX.
  ''' </summary>
  ''' <param name="pFXID">The p FXID.</param>
  ''' <param name="pValueDate">The p value date.</param>
  ''' <returns>System.Double.</returns>
  Friend Function GetBestFX(ByVal pFXID As Integer, ByVal pValueDate As Date) As Double
    ' ****************************************************************************
    '
    '
    ' ****************************************************************************

    Dim myConnection As SqlConnection = Nothing
    Dim QueryCommand As New SqlCommand
    Dim FXRate As Object

    Try

      myConnection = GetVeniceConnection()

      QueryCommand.Connection = myConnection
      QueryCommand.CommandText = "SELECT dbo.fn_SingleBestFX(@FXID, @ValueDate, Null)"
      QueryCommand.Parameters.Clear()
      QueryCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FXID", System.Data.SqlDbType.Int, 4))
      QueryCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValueDate", System.Data.SqlDbType.DateTime, 8))
      QueryCommand.Parameters("@FXID").Value = pFXID
      QueryCommand.Parameters("@ValueDate").Value = pValueDate

      SyncLock QueryCommand.Connection
        FXRate = QueryCommand.ExecuteScalar
      End SyncLock

      If IsNumeric(FXRate) Then
        Return CDbl(FXRate)
      End If
    Catch ex As Exception
      LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error getting an FX Rate.", ex.StackTrace, True)
    Finally
      If (myConnection IsNot Nothing) AndAlso (myConnection.State And ConnectionState.Open) Then
        myConnection.Close()
      End If
      QueryCommand.Connection = Nothing
    End Try

    Return 1.0#
  End Function

#End Region

#Region " AdaptorUpdate, SQL Connection Synchlock functions."

  '''' <summary>
  '''' Cycles the adaptor connection.
  '''' </summary>
  '''' <param name="pAdaptor">The p adaptor.</param>
  'Friend Sub CycleAdaptorConnection(ByVal pAdaptor As SqlDataAdapter)
  '  ' ************************************************************************************
  '  ' It has become increasingly necessary to verify the DB connection before using it.
  '  ' (Seemingly the remote network to the Production site is not always up and the TCP connections to the DB do not recover)
  '  '
  '  ' Historically, the principal DB connection in Venice is widely shared and never closed (assumed to be available).
  '  ' It has been causing problems that this connection sometimes fails.
  '  ' ************************************************************************************

  '  Try
  '    Dim myConnection As SqlConnection = Nothing
  '    If (pAdaptor.SelectCommand.Connection IsNot Nothing) Then
  '      myConnection = pAdaptor.SelectCommand.Connection
  '    ElseIf (pAdaptor.InsertCommand.Connection IsNot Nothing) Then
  '      myConnection = pAdaptor.InsertCommand.Connection
  '    ElseIf (pAdaptor.UpdateCommand.Connection IsNot Nothing) Then
  '      myConnection = pAdaptor.UpdateCommand.Connection
  '    ElseIf (pAdaptor.DeleteCommand.Connection IsNot Nothing) Then
  '      myConnection = pAdaptor.DeleteCommand.Connection
  '    End If

  '    ' Cycle Connection. To ensure it is connected.
  '    If (myConnection IsNot Nothing) AndAlso (myConnection.State And ConnectionState.Open) Then
  '      myConnection.Close()
  '    End If
  '    myConnection.Open()
  '  Catch ex As Exception
  '    LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error cycling DB Connection.", ex.StackTrace, False)
  '  End Try

  'End Sub

  'Friend Sub CycleAdaptorConnection(ByVal pCommand As SqlCommand)

  '  Try
  '    Dim myConnection As SqlConnection = Nothing

  '    If (pCommand IsNot Nothing) Then
  '      myConnection = pCommand.Connection

  '      ' Cycle Connection. To ensure it is connected.
  '      If (myConnection IsNot Nothing) AndAlso (myConnection.State And ConnectionState.Open) Then
  '        myConnection.Close()
  '      End If
  '      myConnection.Open()
  '    End If

  '  Catch ex As Exception
  '    LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error cycling DB Connection.", ex.StackTrace, False)
  '  End Try
  'End Sub
  ''' <summary>
  ''' Adaptors the update.
  ''' </summary>
  ''' <param name="FormName">Name of the form.</param>
  ''' <param name="pAdaptor">The p adaptor.</param>
  ''' <param name="pTable">The p table.</param>
  ''' <returns>System.Int32.</returns>
  Public Function AdaptorUpdate(ByVal FormName As String, ByVal pAdaptor As SqlDataAdapter, ByVal pTable As DataTable) As Integer
    ' ************************************************************************************
    ' 
    ' 
    ' ************************************************************************************

    Dim thisDataRow As DataRow
    Dim LogString As String
    Dim EditState As String

    Try
      If (Array.IndexOf(TablesNotToLogChangesFor, pTable.TableName) < 0) Then
        For Each thisDataRow In pTable.Rows
          If (thisDataRow.RowState = DataRowState.Added) OrElse (thisDataRow.RowState = DataRowState.Deleted) OrElse (thisDataRow.RowState = DataRowState.Modified) Then
            EditState = thisDataRow.RowState.ToString & " Record"
            LogString = Me.GetStandardLogString(thisDataRow)
            Me.LogError(FormName, LOG_LEVELS.Changes, EditState, LogString, "", False)
          End If
        Next
      End If
    Catch ex As Exception
    End Try

    Return MainDataHandler.AdaptorUpdate(pAdaptor, pTable)
  End Function

  ''' <summary>
  ''' Adaptors the update.
  ''' </summary>
  ''' <param name="FormName">Name of the form.</param>
  ''' <param name="pAdaptor">The p adaptor.</param>
  ''' <param name="pDataSet">The p data set.</param>
  ''' <returns>System.Int32.</returns>
  Public Function AdaptorUpdate(ByVal FormName As String, ByRef pAdaptor As SqlDataAdapter, ByRef pDataSet As DataSet) As Integer
    ' ************************************************************************************
    ' 
    ' 
    ' ************************************************************************************

    Dim thisTable As DataTable
    Dim thisDataRow As DataRow
    Dim LogString As String
    Dim EditState As String

    Try
      For Each thisTable In pDataSet.Tables
        If (Array.IndexOf(TablesNotToLogChangesFor, thisTable.TableName) < 0) Then
          For Each thisDataRow In thisTable.Rows
            If (thisDataRow.RowState = DataRowState.Added) OrElse (thisDataRow.RowState = DataRowState.Deleted) OrElse (thisDataRow.RowState = DataRowState.Modified) Then
              EditState = thisDataRow.RowState.ToString & " Record"
              LogString = Me.GetStandardLogString(thisDataRow)
              Me.LogError(FormName, LOG_LEVELS.Changes, EditState, LogString, "", False)
            End If
          Next
        End If
      Next
    Catch ex As Exception
    End Try

    Return MainDataHandler.AdaptorUpdate(pAdaptor, pDataSet)
  End Function

  ''' <summary>
  ''' Adaptors the update.
  ''' </summary>
  ''' <param name="FormName">Name of the form.</param>
  ''' <param name="pAdaptor">The p adaptor.</param>
  ''' <param name="pDataRows">The p data rows.</param>
  ''' <returns>System.Int32.</returns>
  Public Function AdaptorUpdate(ByVal FormName As String, ByVal pAdaptor As SqlDataAdapter, ByVal pDataRows() As DataRow) As Integer
    ' ************************************************************************************
    ' 
    ' 
    ' ************************************************************************************

    Dim thisDataRow As DataRow
    Dim LogString As String
    Dim EditState As String

    Try
      For Each thisDataRow In pDataRows
        If (thisDataRow.RowState = DataRowState.Added) OrElse (thisDataRow.RowState = DataRowState.Deleted) OrElse (thisDataRow.RowState = DataRowState.Modified) Then
          If (Array.IndexOf(TablesNotToLogChangesFor, thisDataRow.Table.TableName) < 0) Then
            EditState = thisDataRow.RowState.ToString & " Record"
            LogString = Me.GetStandardLogString(thisDataRow)
            Me.LogError(FormName, LOG_LEVELS.Changes, EditState, LogString, "", False)
          End If
        End If
      Next
    Catch ex As Exception
    End Try

    Return MainDataHandler.AdaptorUpdate(pAdaptor, pDataRows)
  End Function

  ''' <summary>
  ''' Adaptors the update.
  ''' </summary>
  ''' <param name="FormName">Name of the form.</param>
  ''' <param name="pAdaptor">The p adaptor.</param>
  ''' <param name="pDataSet">The p data set.</param>
  ''' <param name="pTableName">Name of the p table.</param>
  ''' <returns>System.Int32.</returns>
  Public Function AdaptorUpdate(ByVal FormName As String, ByRef pAdaptor As SqlDataAdapter, ByRef pDataSet As DataSet, ByVal pTableName As String) As Integer
    ' ************************************************************************************
    ' 
    ' 
    ' ************************************************************************************

    Dim thisDataRow As DataRow
    Dim LogString As String
    Dim EditState As String

    Try
      If (Array.IndexOf(TablesNotToLogChangesFor, pTableName) < 0) Then
        For Each thisDataRow In pDataSet.Tables(pTableName).Rows
          If (thisDataRow.RowState = DataRowState.Added) OrElse (thisDataRow.RowState = DataRowState.Deleted) OrElse (thisDataRow.RowState = DataRowState.Modified) Then
            EditState = thisDataRow.RowState.ToString & " Record"
            LogString = Me.GetStandardLogString(thisDataRow)
            Me.LogError(FormName, LOG_LEVELS.Changes, EditState, LogString, "", False)
          End If
        Next
      End If
    Catch ex As Exception
    End Try

    Return MainDataHandler.AdaptorUpdate(pAdaptor, pDataSet, pTableName)
  End Function

  ''' <summary>
  ''' Adaptors the update.
  ''' </summary>
  ''' <param name="FormName">Name of the form.</param>
  ''' <param name="pStdTable">The p STD table.</param>
  ''' <param name="pDataRows">The p data rows.</param>
  ''' <returns>System.Int32.</returns>
  Public Function AdaptorUpdate(ByVal FormName As String, ByVal pStdTable As RenaissanceGlobals.StandardDataset, ByVal pDataRows() As DataRow) As Integer
    ' ************************************************************************************
    ' Retrieve / Create the Standard table Adaptor and then Update the given DataRows.
    ' 
    ' ************************************************************************************

    Dim ThisAdaptor As SqlDataAdapter

    Try
      ThisAdaptor = Me.MainDataHandler.Get_Adaptor(pStdTable.Adaptorname, VENICE_CONNECTION, pStdTable.TableName)

      If ThisAdaptor IsNot Nothing Then
        Try
          If (ThisAdaptor.InsertCommand IsNot Nothing) AndAlso (ThisAdaptor.InsertCommand.Parameters.Contains("@Knowledgedate")) Then
            ThisAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = Main_Knowledgedate
          End If
        Catch ex As Exception
        End Try
        Try
          If (ThisAdaptor.UpdateCommand IsNot Nothing) AndAlso (ThisAdaptor.UpdateCommand.Parameters.Contains("@Knowledgedate")) Then
            ThisAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = Main_Knowledgedate
          End If
        Catch ex As Exception
        End Try

        Return AdaptorUpdate(FormName, ThisAdaptor, pDataRows)
      Else
        Return 0
      End If
    Catch ex As Exception
    End Try

    Return 0

  End Function

  ''' <summary>
  ''' Adaptors the update.
  ''' </summary>
  ''' <param name="FormName">Name of the form.</param>
  ''' <param name="pStdTable">The p STD table.</param>
  ''' <param name="pDataTable">The p data table.</param>
  ''' <returns>System.Int32.</returns>
  Public Function AdaptorUpdate(ByVal FormName As String, ByVal pStdTable As RenaissanceGlobals.StandardDataset, ByVal pDataTable As DataTable) As Integer
    ' ************************************************************************************
    ' 
    ' 
    ' ************************************************************************************

    Dim ThisAdaptor As SqlDataAdapter

    Try
      ThisAdaptor = Me.MainDataHandler.Get_Adaptor(pStdTable.Adaptorname, VENICE_CONNECTION, pStdTable.TableName)

      If ThisAdaptor IsNot Nothing Then
        Try
          If (ThisAdaptor.InsertCommand IsNot Nothing) AndAlso (ThisAdaptor.InsertCommand.Parameters.Contains("@Knowledgedate")) Then
            ThisAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = Main_Knowledgedate
          End If
        Catch ex As Exception
        End Try
        Try
          If (ThisAdaptor.UpdateCommand IsNot Nothing) AndAlso (ThisAdaptor.UpdateCommand.Parameters.Contains("@Knowledgedate")) Then
            ThisAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = Main_Knowledgedate
          End If
        Catch ex As Exception
        End Try

        Return AdaptorUpdate(FormName, ThisAdaptor, pDataTable)
      Else
        Return 0
      End If
    Catch ex As Exception
    End Try

    Return 0

  End Function

  ''' <summary>
  ''' Return new DB connection, using the normal Venice connection string.
  ''' Code will try up to two times on failure.
  ''' </summary>
  ''' <returns>SqlConnection.</returns>
  Public Function GetVeniceConnection() As SqlConnection Implements StandardRenaissanceMainForm.GetRenaissanceConnection
    ' ************************************************************************************
    ' Return new DB connection, using the normal Venice connection string.
    ' 
    ' Code will try up to two times on failure.
    ' ************************************************************************************

    Dim RVal As SqlConnection = Nothing
    Dim LoopCounter As Integer

    LoopCounter = 0

    If (Me.IsClosing) Then
      Return Nothing
    End If

    Try
      SqlConnection.ClearAllPools()

      Do
        LoopCounter += 1

        Try
          RVal = New SqlConnection(Me.SQLConnectString)

          If (RVal.State And ConnectionState.Open) <> ConnectionState.Open Then
            RVal.Open()
          End If

          Return RVal

        Catch ex As Exception
          Try
            If (RVal IsNot Nothing) Then
              RVal.Close()
              RVal = Nothing
            End If
          Catch Inner_Ex As Exception
            RVal = Nothing
          End Try
        End Try

      Loop Until (RVal IsNot Nothing OrElse LoopCounter > 2)

    Catch ex As Exception
    End Try

    Return Nothing

  End Function

  Public Sub LoadTable_Custom(ByRef myTable As DataTable, ByRef myCommand As SqlCommand)

    If (myTable Is Nothing) OrElse (myCommand Is Nothing) OrElse (myCommand.Connection Is Nothing) Then
      Exit Sub
    End If

    Try
      myTable.Load(myCommand.ExecuteReader)
    Catch ex As Exception
      myCommand.Connection.Close()
      SqlConnection.ClearPool(myCommand.Connection)
      myCommand.Connection.Open()

      myTable.Clear()
      myTable.Load(myCommand.ExecuteReader)
    End Try

  End Sub

  Public Function ExecuteScalar(ByRef myCommand As SqlCommand) As Object

    If (myCommand Is Nothing) OrElse (myCommand.Connection Is Nothing) Then
      Return Nothing
    End If

    Dim Rval As Object = Nothing

    Try
      Rval = myCommand.ExecuteScalar
    Catch ex As Exception
      myCommand.Connection.Close()
      SqlConnection.ClearPool(myCommand.Connection)
      myCommand.Connection.Open()

      Rval = myCommand.ExecuteScalar 
    End Try

    Return Rval

  End Function

  'Public Function GetVeniceAsynchConnection() As SqlConnection

  '	Dim RVal As SqlConnection = Nothing

  '	Try
  '		RVal = New SqlConnection(Me.SQLAsynchConnectString)

  '		If (RVal.State And ConnectionState.Open) <> ConnectionState.Open Then
  '			RVal.Open()
  '		End If

  '		Return RVal
  '	Catch ex As Exception
  '		Try
  '			If (RVal IsNot Nothing) Then
  '				RVal.Close()
  '			End If
  '		Catch Inner_Ex As Exception
  '		End Try
  '	End Try

  '	Return Nothing
  'End Function

#End Region

#Region " Grid Copy code"

  ''' <summary>
  ''' Copies the grid selection.
  ''' </summary>
  ''' <param name="ThisGrid">The this grid.</param>
  ''' <param name="IncludeColumnZero">if set to <c>true</c> [include column zero].</param>
  ''' <param name="PerformanceGridStyle">if set to <c>true</c> [performance grid style].</param>
  ''' <param name="CopyWholeGrid">if set to <c>true</c> [copy whole grid].</param>
  Public Sub CopyGridSelection(ByVal ThisGrid As C1.Win.C1FlexGrid.C1FlexGrid, ByVal IncludeColumnZero As Boolean, ByVal PerformanceGridStyle As Boolean, Optional ByVal CopyWholeGrid As Boolean = False)
    ' **************************************************************************
    '
    '
    ' **************************************************************************

    Try
      Dim ClipString As String = ThisGrid.Clip
      Dim HeaderString As String = ""
      Dim ColCount As Integer

      Dim PertracID As ULong = 0UL
      Dim PertracIDString As String
      Dim StartCol As Integer = ThisGrid.Selection.c1
      Dim EndCol As Integer = ThisGrid.Selection.c2
      Dim StartRow As Integer = ThisGrid.Selection.r1
      Dim EndRow As Integer = ThisGrid.Selection.r2

      Dim CSV_CultureInfo As Globalization.CultureInfo = New Globalization.CultureInfo("")

      If (CopyWholeGrid) Then
        StartCol = 0
        EndCol = ThisGrid.Cols.Count - 1

        If (IncludeColumnZero) Then
          StartRow = Math.Min(1, ThisGrid.Rows.Count - 1)
        Else
          StartRow = 0
        End If
        EndRow = ThisGrid.Rows.Count - 1

        ClipString = ThisGrid.GetCellRange(StartRow, StartCol, EndRow, EndCol).Clip
      End If

      If (ThisGrid.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Row) Or (ThisGrid.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.RowRange) Or (ThisGrid.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.ListBox) Then
        StartCol = 0
        EndCol = ThisGrid.Cols.Count - 1
      End If

      If PerformanceGridStyle Then
        If (IsNumeric(ThisGrid.Tag)) Then
          PertracID = CULng(ThisGrid.Tag)
        End If

        Dim ThisRow As Integer
        Dim ThisCol As Integer

        If StartCol <= 0 Then StartCol = 1
        If StartRow <= 0 Then StartRow = 1
        If EndCol = (ThisGrid.Cols.Count - 1) Then EndCol -= 1 ' Strip off YTD

        HeaderString = "ID" & Chr(9) & "Date" & Chr(9) & "Return" & Chr(13)
        ClipString = ""

        For ThisRow = StartRow To EndRow
          For ThisCol = StartCol To EndCol

            If (ThisRow < ThisGrid.Rows.Count) AndAlso (ThisGrid.Item(ThisRow, ThisCol) IsNot Nothing) Then
              If (CSV_CultureInfo Is Nothing) Then
                PertracIDString = PertracID.ToString("###0")
                If IsDate("1 " & ThisGrid.Item(0, ThisCol).ToString & " " & ThisGrid.Item(ThisRow, 0).ToString) And IsNumeric(ThisGrid.Item(ThisRow, ThisCol)) Then
                  ClipString &= PertracIDString & Chr(9) & RenaissanceUtilities.DatePeriodFunctions.FitDateToPeriod(DealingPeriod.Monthly, CDate("1 " & ThisGrid.Item(0, ThisCol).ToString & " " & CStr(ThisGrid.Item(ThisRow, 0))), True).ToString(QUERY_SHORTDATEFORMAT) & Chr(9) & CDbl(ThisGrid.Item(ThisRow, ThisCol)).ToString("###0.00##%") & Chr(13)
                Else
                  ClipString &= PertracIDString & Chr(9) & ThisGrid.Item(0, ThisCol).ToString & " " & ThisGrid.Item(ThisRow, 0).ToString & Chr(9) & ThisGrid.Item(ThisRow, ThisCol).ToString & Chr(13)
                End If
              Else
                PertracIDString = PertracID.ToString("###0", CSV_CultureInfo)

                If IsDate("1 " & ThisGrid.Item(0, ThisCol).ToString & " " & ThisGrid.Item(ThisRow, 0).ToString) And IsNumeric(ThisGrid.Item(ThisRow, ThisCol)) Then
                  ClipString &= PertracIDString & Chr(9) & RenaissanceUtilities.DatePeriodFunctions.FitDateToPeriod(DealingPeriod.Monthly, CDate("1 " & ThisGrid.Item(0, ThisCol).ToString & " " & CStr(ThisGrid.Item(ThisRow, 0))), True).ToString(QUERY_SHORTDATEFORMAT, CSV_CultureInfo) & Chr(9) & CDbl(ThisGrid.Item(ThisRow, ThisCol)).ToString("###0.00##%", CSV_CultureInfo) & Chr(13)
                Else
                  ClipString &= PertracIDString & Chr(9) & ThisGrid.Item(0, ThisCol).ToString() & " " & ThisGrid.Item(ThisRow, 0).ToString & Chr(9) & ThisGrid.Item(ThisRow, ThisCol).ToString & Chr(13)
                End If
              End If
            End If
          Next
        Next

      ElseIf (IncludeColumnZero = False) OrElse (ThisGrid.Selection.c1 = 0) Then

        For ColCount = StartCol To EndCol
          If ColCount > StartCol Then HeaderString &= Chr(9)
          HeaderString &= CStr(ThisGrid.Item(0, ColCount))
        Next
        HeaderString &= Chr(13)

      Else

        Dim ClipStrings() As String = ClipString.Split(New Char() {Chr(13)})
        Dim RowCount As Integer

        HeaderString = CStr(ThisGrid.Item(0, 0))
        For ColCount = StartCol To EndCol
          HeaderString &= Chr(9)
          HeaderString &= CStr(ThisGrid.Item(0, ColCount))
        Next
        HeaderString &= Chr(13)

        ClipString = ""
        For RowCount = StartRow To EndRow
          If (RowCount < ThisGrid.Rows.Count) Then
            ClipString &= CStr(ThisGrid.Item(RowCount, 0)) ' 1st column
            ClipString &= Chr(9) & ClipStrings(RowCount - StartRow) ' Selection

            ClipString &= Chr(13)
          End If
        Next
      End If

      Clipboard.Clear()
      Clipboard.SetData(System.Windows.Forms.DataFormats.Text, (HeaderString & ClipString))

    Catch ex As Exception
    End Try
  End Sub


#End Region

#Region " Error Logging "

  ''' <summary>
  ''' Delegate LogErrorDelegate
  ''' </summary>
  ''' <param name="p_Error_Location">The p_ error_ location.</param>
  ''' <param name="p_System_Error_Number">The p_ system_ error_ number.</param>
  ''' <param name="p_System_Error_Description">The p_ system_ error_ description.</param>
  ''' <param name="p_Error_Message">The p_ error_ message.</param>
  ''' <param name="p_Error_StackTrace">The p_ error_ stack trace.</param>
  ''' <param name="p_Show_Error">if set to <c>true</c> [p_ show_ error].</param>
  ''' <param name="p_MessageHeading">The p_ message heading.</param>
  ''' <param name="p_MessageButtons">The p_ message buttons.</param>
  ''' <param name="p_MessageBoxIcon">The p_ message box icon.</param>
  ''' <param name="p_Log_to">The p_ log_to.</param>
  ''' <returns>System.Int32.</returns>
  Private Delegate Function LogErrorDelegate(ByVal p_Error_Location As String, _
  ByVal p_System_Error_Number As Integer, _
  ByVal p_System_Error_Description As String, _
  ByVal p_Error_Message As String, _
  ByVal p_Error_StackTrace As String, _
  ByVal p_Show_Error As Boolean, _
  ByVal p_MessageHeading As String, _
  ByVal p_MessageButtons As System.Windows.Forms.MessageBoxButtons, _
  ByVal p_MessageBoxIcon As System.Windows.Forms.MessageBoxIcon, _
  ByVal p_Log_to As Integer) As Integer



  ''' <summary>
  ''' Logs the error.
  ''' </summary>
  ''' <param name="p_Error_Location">The p_ error_ location.</param>
  ''' <param name="p_System_Error_Number">The p_ system_ error_ number.</param>
  ''' <param name="p_System_Error_Description">The p_ system_ error_ description.</param>
  ''' <param name="p_Error_Message">The p_ error_ message.</param>
  ''' <param name="p_Error_StackTrace">The p_ error_ stack trace.</param>
  ''' <param name="p_Show_Error">if set to <c>true</c> [p_ show_ error].</param>
  ''' <param name="p_MessageHeading">The p_ message heading.</param>
  ''' <param name="p_MessageButtons">The p_ message buttons.</param>
  ''' <param name="p_MessageBoxIcon">The p_ message box icon.</param>
  ''' <param name="p_Log_to">The p_ log_to.</param>
  ''' <returns>System.Int32.</returns>
  Public Function LogError(ByVal p_Error_Location As String, _
   ByVal p_System_Error_Number As Integer, _
   ByVal p_System_Error_Description As String, _
   Optional ByVal p_Error_Message As String = "", _
   Optional ByVal p_Error_StackTrace As String = "", _
   Optional ByVal p_Show_Error As Boolean = True, _
   Optional ByVal p_MessageHeading As String = "", _
   Optional ByVal p_MessageButtons As System.Windows.Forms.MessageBoxButtons = MessageBoxButtons.OK, _
   Optional ByVal p_MessageBoxIcon As System.Windows.Forms.MessageBoxIcon = MessageBoxIcon.Exclamation, _
   Optional ByVal p_Log_to As Integer = 0) As Integer _
   Implements StandardRenaissanceMainForm.LogError

    '**************************************************************************************************
    ' Public function returning public variables
    '
    ' Purpose:  Display Error messages and log to Database / File if required
    '
    ' Accepts:  p_Error_Location as String            : Location of Error (for debugging and logging purposes)
    '           p_System_Error_Number As Integer      : System 'Err'
    '           p_System_Error_Description As String  : System 'Error'
    '           p_Error_Message as String (Optional)  : Meaningful User Error message
    '           p_Error_StackTrace as String (Optional)  : StackTrace Property of Error message
    '           p_Show_Error As Boolean (Optional)    : If TRUE then a MsgBox is diplayed
    '           p_MessageHeading as String (Optional) : Title for Error message box
    '           p_MessageButtons                      : Button(s) to show on the error message
    '           p_MessageBoxIcon                      : Icon to show on the Error message
    '           p_Log_to As Integer (Optional)        : Write log conditions
    '
    ' Returns:  0 or error number on fail, only returns an error if logged to File / Table and the update fails
    '**************************************************************************************************

    If (Me.IsClosing) Then
      Return 0
      Exit Function
    End If

    If Me.InvokeRequired Then
      Dim d As New LogErrorDelegate(AddressOf LogError)
      Return Me.Invoke(d, New Object() {p_Error_Location, p_System_Error_Number, p_System_Error_Description, p_Error_Message, p_Error_StackTrace, p_Show_Error, p_MessageHeading, p_MessageButtons, p_MessageBoxIcon, p_Log_to})
      Exit Function
    End If

    If (p_MessageHeading.Length <= 0) Then
      p_MessageHeading = "Venice Message"
    End If

    ' Display Error message if required.

    If p_Show_Error = True Then
      MessageBox.Show(p_System_Error_Number.ToString & vbCrLf & _
      p_System_Error_Description & vbCrLf & _
      p_Error_Message, p_MessageHeading, p_MessageButtons, p_MessageBoxIcon, MessageBoxDefaultButton.Button1)
    End If

    ' Log to DB, if possible.

    Dim InsertCommand As New SqlCommand

    Try
      InsertCommand.CommandText = "[adp_tblINVEST_ERROR_LOG_InsertCommand]"
      InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
      InsertCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      InsertCommand.Connection = Me.GetVeniceConnection()

      InsertCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
      InsertCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@WindowsUserName", System.Data.SqlDbType.NVarChar, 100))
      InsertCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ErrorNumber", System.Data.SqlDbType.Int, 4))
      InsertCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ErrorLocation", System.Data.SqlDbType.NVarChar, 250))
      InsertCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SystemErrorDescription", System.Data.SqlDbType.NVarChar, 250))
      InsertCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UserErrorDescription", System.Data.SqlDbType.NVarChar, 450))
      InsertCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CallStack", System.Data.SqlDbType.Text, 2147483647))

      InsertCommand.Parameters("@WindowsUserName").Value = Environment.UserName
      InsertCommand.Parameters("@ErrorNumber").Value = p_System_Error_Number
      InsertCommand.Parameters("@ErrorLocation").Value = p_Error_Location
      InsertCommand.Parameters("@SystemErrorDescription").Value = p_System_Error_Description
      InsertCommand.Parameters("@UserErrorDescription").Value = p_Error_Message
      InsertCommand.Parameters("@CallStack").Value = p_Error_StackTrace

      If (p_Error_Message.Length > 450) AndAlso (p_Error_StackTrace.Length = 0) Then
        InsertCommand.Parameters("@UserErrorDescription").Value = "See StackTrace."
        InsertCommand.Parameters("@CallStack").Value = p_Error_Message
      End If

      If (InsertCommand.Connection IsNot Nothing) Then
        SyncLock InsertCommand.Connection
          InsertCommand.ExecuteNonQuery()
        End SyncLock
      End If

    Catch ex As Exception
    Finally
      If (InsertCommand.Connection IsNot Nothing) Then
        Try
          InsertCommand.Connection.Close()
        Catch ex As Exception
        End Try
      End If
    End Try

    Return 0

  End Function

  ''' <summary>
  ''' Gets the standard log string.
  ''' </summary>
  ''' <param name="thisDataRow">The this data row.</param>
  ''' <returns>System.String.</returns>
  Public Function GetStandardLogString(ByRef thisDataRow As DataRow) As String

    Dim LogString As String
    LogString = ""

    Try
      Dim ItemCount As Integer
      Dim thisItem As Object
      Dim TableName As String
      Dim ItemName As String

      TableName = ""
      Try
        TableName = thisDataRow.Table.TableName.ToUpper
        If TableName.StartsWith("TBL") Then
          TableName = TableName.Substring(3)
        End If
      Catch ex As Exception
      End Try

      LogString = thisDataRow.RowState.ToString & ", " & thisDataRow.Table.TableName & ", "

      If (thisDataRow.RowState And DataRowState.Deleted) <> DataRowState.Deleted Then
        For ItemCount = 0 To (thisDataRow.ItemArray.Length - 1)
          thisItem = thisDataRow.ItemArray(ItemCount)
          ItemName = thisDataRow.Table.Columns(ItemCount).ColumnName
          If ItemName.ToUpper.StartsWith(TableName) Then
            ItemName = ItemName.Substring(TableName.Length)
          End If

          LogString &= ItemName & "="

          If (thisItem Is Nothing) Then
            LogString &= "Null,"
          Else
            LogString &= "`" & thisItem.ToString & "`,"
          End If
        Next
      End If
    Catch ex As Exception
    End Try

    Return LogString

  End Function

#End Region

#Region " Bug hunting "

  ' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
  ' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
  ' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

  ''' <summary>
  ''' Called when [row updating].
  ''' </summary>
  ''' <param name="Sender">The sender.</param>
  ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  ''' <summary>
  ''' Called when [row updated].
  ''' </summary>
  ''' <param name="Sender">The sender.</param>
  ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  ''' <summary>
  ''' Called when [row fill error].
  ''' </summary>
  ''' <param name="Sender">The sender.</param>
  ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
    Dim a As Integer

    a = 1
  End Sub

#End Region



  ''' <summary>
  ''' Handles the Click event of the C1Menu_Static_Counterparties control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="C1.Win.C1Command.ClickEventArgs"/> instance containing the event data.</param>
  Private Sub C1Menu_Static_Counterparties_Click(ByVal sender As System.Object, ByVal e As C1.Win.C1Command.ClickEventArgs) Handles C1Menu_Static_Counterparties.Click
    Call Generic_FormMenu_Click(sender, Nothing)
  End Sub

  ''' <summary>
  ''' Handles the Click event of the Button2 control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

 
  End Sub

  ''' <summary>
  ''' Handles the Click event of the Button3 control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click

    '' Select Distinct Dates

    'Dim DatesCommand As New SqlCommand
    'Dim DatesTable As New DataTable
    'Dim NavDate As Date
    'Dim DateRow As DataRow
    'Dim PortfolioCommand As New SqlCommand
    'Dim DeltaCommand As New SqlCommand
    'Dim DeltaTable As New DataTable
    'Dim DeltaRow As DataRow
    'Dim CashCommand As New SqlCommand
    'Dim SavePriceCommand As New SqlCommand

    'Dim thisPortfolio As DataTable
    'Dim thisPortfolioRow As DataRow
    'Dim thisPortfolioSelection() As DataRow
    'Dim LastPotfolio As DataTable
    'Dim LastPortfolioSelection() As DataRow
    'Dim LastPortfolioRow As DataRow
    'Dim InstrumentRow As DSInstrument.tblInstrumentRow

    'Dim AddTransaction As New SqlCommand

    'DatesCommand.Connection = GetVeniceConnection()
    'DatesCommand.CommandType = CommandType.Text
    'DatesCommand.CommandText = "Select Distinct NAVDATE FROM tbl_RBImport Order by NavDate"

    'DatesTable.Load(DatesCommand.ExecuteReader)

    'PortfolioCommand.Connection = DatesCommand.Connection
    'PortfolioCommand.CommandType = CommandType.Text
    'PortfolioCommand.CommandText = "Select * FROM tbl_RBImport WHERE NAVDate = @NAVDate"
    'PortfolioCommand.Parameters.Add(New SqlParameter("@NAVDATE", SqlDbType.Date))

    'SavePriceCommand.Connection = DatesCommand.Connection
    'SavePriceCommand.CommandType = CommandType.StoredProcedure
    'SavePriceCommand.CommandText = "spu_SavePrice"
    'SavePriceCommand.Parameters.Add(New SqlParameter("@Instrument_ID", SqlDbType.Int))
    'SavePriceCommand.Parameters.Add(New SqlParameter("@PriceDate", SqlDbType.Date))
    'SavePriceCommand.Parameters.Add(New SqlParameter("@PriceBaseDate", SqlDbType.Date)).Value = CDate("1900-01-01")
    'SavePriceCommand.Parameters.Add(New SqlParameter("@PriceNAV", SqlDbType.Float))
    'SavePriceCommand.Parameters.Add(New SqlParameter("@rvStatusString", SqlDbType.VarChar, 200, ParameterDirection.Output, False, CType(0, Byte), CType(0, Byte), "", DataRowVersion.Current, Nothing))

    'DeltaCommand.Connection = DatesCommand.Connection
    'DeltaCommand.CommandType = CommandType.Text
    'DeltaCommand.CommandText = "Select * FROM fn_RB_Import(@ValueDate) Order By Category, Name"
    'DeltaCommand.Parameters.Add(New SqlParameter("@ValueDate", SqlDbType.Date))

    'CashCommand.Connection = DatesCommand.Connection
    'CashCommand.CommandType = CommandType.Text
    'CashCommand.CommandText = "SELECT Sum(TransactionSignedUnits) FROM fn_tblTransaction_SelectKD(Null) WHERE (TransactionFund = 3) AND (TransactionValueDate <= @ValueDate) AND (TransactionInstrument = 3)"
    'CashCommand.Parameters.Add(New SqlParameter("@ValueDate", SqlDbType.Date))
    'CashCommand.CommandTimeout = 60

    'AddTransaction.Connection = DatesCommand.Connection
    'AddTransaction.CommandType = CommandType.StoredProcedure
    'AddTransaction.CommandText = "sp_QuickAddtransaction"

    'AddTransaction.Parameters.Add(New SqlParameter("@Fund", SqlDbType.Int)).Value = 3
    'AddTransaction.Parameters.Add(New SqlParameter("@Instrument", SqlDbType.Int))
    'AddTransaction.Parameters.Add(New SqlParameter("@Counterparty", SqlDbType.Int)).Value = 1
    'AddTransaction.Parameters.Add(New SqlParameter("@Type", SqlDbType.Int))
    'AddTransaction.Parameters.Add(New SqlParameter("@Units", SqlDbType.Float))
    'AddTransaction.Parameters.Add(New SqlParameter("@Price", SqlDbType.Float))
    'AddTransaction.Parameters.Add(New SqlParameter("@SettlementCurrencyID", SqlDbType.Int)).Value = 3
    'AddTransaction.Parameters.Add(New SqlParameter("@FXRate", SqlDbType.Float))
    'AddTransaction.Parameters.Add(New SqlParameter("@ValueDate", SqlDbType.Date))

    'Dim LastEuroValue As Double
    'Dim EuroValue As Double = 0.0#

    'For Each DateRow In DatesTable.Select("true", "NAVDate")

    '  NavDate = CDate(DateRow("NAVDate"))

    '  If NavDate < CDate("2011-07-08") Then
    '    Continue For
    '  End If

    '  Debug.Print(NavDate.ToString)

    '  'thisPortfolio = New DataTable

    '  'PortfolioCommand.Parameters("@NAVDATE").Value = NavDate
    '  'thisPortfolio.Load(PortfolioCommand.ExecuteReader)

    '  '
    '  DeltaCommand.Parameters("@ValueDate").Value = NavDate
    '  DeltaTable.Clear()
    '  DeltaTable.Load(DeltaCommand.ExecuteReader)

    '  Dim TradeSize As Double
    '  Dim TradePrice As Double
    '  Dim FXRate As Double

    '  ' 

    '  EuroValue = 0.0#

    '  For Each DeltaRow In DeltaTable.Rows

    '    InstrumentRow = LookupTableRow(Me, RenaissanceStandardDatasets.tblInstrument, 0, "", "", "InstrumentISIN='" & CStr(DeltaRow("ISIN")) & "'")
    '    TradeSize = 0.0#

    '    Select Case CStr(DeltaRow("Category"))

    '      Case ""
    '        Select Case CType(InstrumentRow.InstrumentType, InstrumentTypes)

    '          Case InstrumentTypes.Share, InstrumentTypes.Future, InstrumentTypes.[Option], InstrumentTypes.Unit, InstrumentTypes.Fund, InstrumentTypes.Bond, InstrumentTypes.ConvertibleBond, InstrumentTypes.Index

    '            If DeltaRow.IsNull("FromDate") Then
    '              ' New Position

    '              TradeSize = CDbl(DeltaRow("ToAmount"))
    '              TradePrice = CDbl(DeltaRow("ToPrice"))
    '              FXRate = CDbl(DeltaRow("ToFXRate"))

    '            ElseIf DeltaRow.IsNull("ToDate") Then
    '              ' Closed Position

    '              TradeSize = -CDbl(DeltaRow("FromAmount"))
    '              TradePrice = CDbl(DeltaRow("FromPrice"))
    '              FXRate = CDbl(DeltaRow("FromFXRate"))

    '            Else
    '              ' Changed ?
    '              If (CDbl(DeltaRow("FromAmount")) <> CDbl(DeltaRow("ToAmount"))) Then

    '                TradeSize = CDbl(DeltaRow("ToAmount")) - CDbl(DeltaRow("FromAmount"))
    '                TradePrice = CDbl(DeltaRow("ToPrice"))
    '                FXRate = CDbl(DeltaRow("ToFXRate"))

    '              End If
    '            End If

    '            If (Not DeltaRow.IsNull("ToPrice")) Then
    '              SavePriceCommand.Parameters("@Instrument_ID").Value = InstrumentRow.InstrumentID
    '              SavePriceCommand.Parameters("@PriceDate").Value = NavDate
    '              SavePriceCommand.Parameters("@PriceNAV").Value = CDbl(DeltaRow("ToPrice"))

    '              SavePriceCommand.ExecuteNonQuery()

    '              ' SavePrice(Me, InstrumentRow.InstrumentID, NavDate, CDbl(DeltaRow("ToPrice")), False, 0.0#, #1/1/1900#, 0, "")
    '            End If

    '          Case InstrumentTypes.Cash, InstrumentTypes.CashFund, InstrumentTypes.Fee, InstrumentTypes.Expense

    '            If DeltaRow.IsNull("FromDate") Then
    '              ' New Position

    '              TradeSize = CDbl(DeltaRow("ToAmount"))
    '              TradePrice = 0.0#
    '              FXRate = CDbl(DeltaRow("ToFXRate"))

    '            ElseIf DeltaRow.IsNull("ToDate") Then
    '              ' Closed Position

    '              TradeSize = -CDbl(DeltaRow("FromAmount"))
    '              TradePrice = 0.0#
    '              FXRate = CDbl(DeltaRow("FromFXRate"))

    '            Else
    '              ' Changed ?
    '              If (CDbl(DeltaRow("FromAmount")) <> CDbl(DeltaRow("ToAmount"))) Then

    '                TradeSize = CDbl(DeltaRow("ToAmount")) - CDbl(DeltaRow("FromAmount"))
    '                TradePrice = 0.0#
    '                FXRate = CDbl(DeltaRow("ToFXRate"))

    '              End If
    '            End If

    '          Case Else

    '            If DeltaRow.IsNull("FromDate") Then
    '              ' New Position

    '              TradeSize = CDbl(DeltaRow("ToAmount"))
    '              TradePrice = 0.0#
    '              FXRate = CDbl(DeltaRow("ToFXRate"))

    '            ElseIf DeltaRow.IsNull("ToDate") Then
    '              ' Closed Position

    '              TradeSize = -CDbl(DeltaRow("FromAmount"))
    '              TradePrice = 0.0#
    '              FXRate = CDbl(DeltaRow("FromFXRate"))

    '            Else
    '              ' Changed ?
    '              If (CDbl(DeltaRow("FromAmount")) <> CDbl(DeltaRow("ToAmount"))) Then

    '                TradeSize = CDbl(DeltaRow("ToAmount")) - CDbl(DeltaRow("FromAmount"))
    '                TradePrice = 0.0#
    '                FXRate = CDbl(DeltaRow("ToFXRate"))

    '              End If
    '            End If

    '        End Select

    '      Case "Liquidites"

    '        If DeltaRow.IsNull("FromDate") Then
    '          ' New Position

    '          TradeSize = CDbl(DeltaRow("ToAmount"))
    '          TradePrice = 0.0#
    '          FXRate = CDbl(DeltaRow("ToFXRate"))

    '        ElseIf DeltaRow.IsNull("ToDate") Then
    '          ' Closed Position

    '          TradeSize = -CDbl(DeltaRow("FromAmount"))
    '          TradePrice = 0.0#
    '          FXRate = CDbl(DeltaRow("FromFXRate"))

    '        Else
    '          ' Changed ?
    '          If (CDbl(DeltaRow("FromAmount")) <> CDbl(DeltaRow("ToAmount"))) Then

    '            TradeSize = CDbl(DeltaRow("ToAmount")) - CDbl(DeltaRow("FromAmount"))
    '            TradePrice = 0.0#
    '            FXRate = CDbl(DeltaRow("ToFXRate"))

    '          End If
    '        End If

    '        If (Not DeltaRow.IsNull("ToValue")) AndAlso (InstrumentRow.InstrumentID = 3) Then
    '          EuroValue += CDbl(DeltaRow("ToValue"))
    '        End If

    '      Case "Futures"

    '        If DeltaRow.IsNull("FromDate") Then
    '          ' New Position

    '          TradeSize = CDbl(DeltaRow("ToAmount"))
    '          TradePrice = CDbl(DeltaRow("ToPrice"))
    '          FXRate = CDbl(DeltaRow("ToFXRate"))

    '        ElseIf DeltaRow.IsNull("ToDate") Then
    '          ' Closed Position

    '          TradeSize = -CDbl(DeltaRow("FromAmount"))
    '          TradePrice = CDbl(DeltaRow("FromPrice"))
    '          FXRate = CDbl(DeltaRow("FromFXRate"))

    '        Else
    '          ' Changed ?
    '          If (CDbl(DeltaRow("FromAmount")) <> CDbl(DeltaRow("ToAmount"))) Then

    '            TradeSize = CDbl(DeltaRow("ToAmount")) - CDbl(DeltaRow("FromAmount"))
    '            TradePrice = CDbl(DeltaRow("ToPrice"))
    '            FXRate = CDbl(DeltaRow("ToFXRate"))

    '          End If
    '        End If

    '        If (Not DeltaRow.IsNull("ToPrice")) Then
    '          SavePriceCommand.Parameters("@Instrument_ID").Value = InstrumentRow.InstrumentID
    '          SavePriceCommand.Parameters("@PriceDate").Value = NavDate
    '          SavePriceCommand.Parameters("@PriceNAV").Value = CDbl(DeltaRow("ToPrice"))

    '          SavePriceCommand.ExecuteNonQuery()
    '        End If

    '      Case "Coupons"

    '        ' Ignore, Catch in Cash

    '        If Not DeltaRow.IsNull("ToValue") Then
    '          EuroValue += CDbl(DeltaRow("ToValue"))
    '        End If

    '      Case "Valeurs mobilieres"

    '        If DeltaRow.IsNull("FromDate") Then
    '          ' New Position

    '          TradeSize = CDbl(DeltaRow("ToAmount"))
    '          TradePrice = CDbl(DeltaRow("ToPrice"))
    '          FXRate = CDbl(DeltaRow("ToFXRate"))

    '        ElseIf DeltaRow.IsNull("ToDate") Then
    '          ' Closed Position

    '          TradeSize = -CDbl(DeltaRow("FromAmount"))
    '          TradePrice = CDbl(DeltaRow("FromPrice"))
    '          FXRate = CDbl(DeltaRow("FromFXRate"))

    '        Else
    '          ' Changed ?
    '          If (CDbl(DeltaRow("FromAmount")) <> CDbl(DeltaRow("ToAmount"))) Then

    '            TradeSize = CDbl(DeltaRow("ToAmount")) - CDbl(DeltaRow("FromAmount"))
    '            TradePrice = CDbl(DeltaRow("ToPrice"))
    '            FXRate = CDbl(DeltaRow("ToFXRate"))

    '          End If
    '        End If

    '        If (Not DeltaRow.IsNull("ToPrice")) Then
    '          SavePriceCommand.Parameters("@Instrument_ID").Value = InstrumentRow.InstrumentID
    '          SavePriceCommand.Parameters("@PriceDate").Value = NavDate
    '          SavePriceCommand.Parameters("@PriceNAV").Value = CDbl(DeltaRow("ToPrice"))

    '          SavePriceCommand.ExecuteNonQuery()

    '          ' SavePrice(Me, InstrumentRow.InstrumentID, NavDate, CDbl(DeltaRow("ToPrice")), False, 0.0#, #1/1/1900#, 0, "")
    '        End If

    '    End Select

    '    ' Trade 

    '    If (TradeSize <> 0.0#) AndAlso (InstrumentRow.InstrumentID <> 3) Then

    '      If (FXRate = 0.0#) Then FXRate = 1.0#

    '      AddTransaction.Parameters("@Instrument").Value = InstrumentRow.InstrumentID
    '      AddTransaction.Parameters("@Units").Value = Math.Abs(TradeSize)
    '      AddTransaction.Parameters("@Price").Value = TradePrice
    '      AddTransaction.Parameters("@FXRate").Value = 1.0# / FXRate
    '      AddTransaction.Parameters("@ValueDate").Value = NavDate

    '      Select Case CType(InstrumentRow.InstrumentType, InstrumentTypes)

    '        Case InstrumentTypes.Fee
    '          AddTransaction.Parameters("@Type").Value = TransactionTypes.Fee
    '          AddTransaction.Parameters("@Units").Value = (TradeSize)

    '        Case InstrumentTypes.Expense

    '          If (TradeSize < 0.0#) Then
    '            AddTransaction.Parameters("@Type").Value = TransactionTypes.Expense_Increase
    '          Else
    '            AddTransaction.Parameters("@Type").Value = TransactionTypes.Expense_Reduce
    '          End If
    '        Case Else

    '          If (TradeSize < 0.0#) Then
    '            AddTransaction.Parameters("@Type").Value = TransactionTypes.Sell
    '          Else
    '            AddTransaction.Parameters("@Type").Value = TransactionTypes.Buy
    '          End If

    '      End Select

    '      AddTransaction.ExecuteNonQuery()
    '    End If

    '  Next

    '  ' 3) Net Cash.

    '  Try
    '    CashCommand.Parameters("@ValueDate").Value = NavDate
    '    LastEuroValue = CDbl(CashCommand.ExecuteScalar)
    '  Catch ex As Exception
    '    Dim Crap As Integer
    '    Crap = 1
    '  End Try

    '  If (EuroValue <> LastEuroValue) Then

    '    TradeSize = EuroValue - LastEuroValue

    '    AddTransaction.Parameters("@Instrument").Value = 67683542
    '    AddTransaction.Parameters("@Units").Value = Math.Abs(TradeSize)
    '    AddTransaction.Parameters("@Price").Value = 1.0#
    '    AddTransaction.Parameters("@FXRate").Value = 1.0#
    '    AddTransaction.Parameters("@ValueDate").Value = NavDate

    '    If (TradeSize < 0.0#) Then
    '      AddTransaction.Parameters("@Type").Value = TransactionTypes.Expense_Increase
    '    Else
    '      AddTransaction.Parameters("@Type").Value = TransactionTypes.Expense_Reduce
    '    End If

    '    AddTransaction.ExecuteNonQuery()

    '  End If


    '  LastPotfolio = thisPortfolio
    '  LastEuroValue = EuroValue

    '  AddTransaction.Connection.Close()
    '  SqlConnection.ClearPool(AddTransaction.Connection)
    '  AddTransaction.Connection.Open()

    'Next ' Day


    'DatesCommand.Connection.Close()


  End Sub


  ''' <summary>
  ''' Handles the Click event of the Button1 control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

    'Call AllocationSetReturns(Me, 100002, #12/31/2011#, #2/28/2014#, 0, Nothing)
    'Call AllocationSetReturns(Me, 100003, #12/31/2011#, #2/28/2014#, 0, Nothing)
    'Call AllocationSetReturns(Me, 100004, #12/31/2011#, #2/28/2014#, 0, Nothing)
    'Call AllocationSetReturns(Me, 12548, #12/9/2013#, #1/10/2014#, 0, Nothing)
    'Call AllocationSetReturns(Me, 12549, #12/9/2013#, #1/10/2014#, 0, Nothing)

  End Sub

  'lotfi: add last kansas Msg date and a background thread that checks if kansas has sent a message or not since the las one
  ' if no msg is sent since 10 minutes the isKansasOk status is set to false and the error is shown in the approveFrom
  Public lastKansasMsgDate As Date = Now
  Public NoKansasMsg_MAX_PERIOD_SECOND As Integer = 60
  Public Event NoKansasMsgReceived(ByVal kansasMachine As String)

  Private Sub kansasIsAliveCheckTimer_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles kansasIsAliveCheckTimer.Tick
    'System.Console.Out.WriteLine("***kansasIsAliveCheckTimer_Tick received at : " & CStr(Now) & " lastMsgReceived at=" & CStr(lastKansasMsgDate))
    If DateDiff(DateInterval.Second, lastKansasMsgDate, Now) > 60 Then
      If KansasStatusMessages IsNot Nothing Then
        KansasStatusMessages.Clear()
      End If
      RaiseEvent NoKansasMsgReceived("")
    End If
  End Sub
End Class
