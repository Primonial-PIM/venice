' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmPortfolioEdit.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals

Imports System.Collections.Specialized

''' <summary>
''' Class frmPortfolioEdit
''' </summary>
Public Class frmPortfolioEdit

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmPortfolioEdit"/> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The edit audit ID
    ''' </summary>
  Friend WithEvents editAuditID As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The edit ticker
    ''' </summary>
  Friend WithEvents editTicker As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The BTN nav first
    ''' </summary>
  Friend WithEvents btnNavFirst As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN nav prev
    ''' </summary>
  Friend WithEvents btnNavPrev As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN nav next
    ''' </summary>
  Friend WithEvents btnNavNext As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN last
    ''' </summary>
  Friend WithEvents btnLast As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN add
    ''' </summary>
  Friend WithEvents btnAdd As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN delete
    ''' </summary>
  Friend WithEvents btnDelete As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN cancel
    ''' </summary>
  Friend WithEvents btnCancel As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN save
    ''' </summary>
  Friend WithEvents btnSave As System.Windows.Forms.Button
    ''' <summary>
    ''' The combo_ select portfolio
    ''' </summary>
  Friend WithEvents Combo_SelectPortfolio As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label1
    ''' </summary>
  Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The group box1
    ''' </summary>
  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The BTN close
    ''' </summary>
  Friend WithEvents btnClose As System.Windows.Forms.Button
    ''' <summary>
    ''' The LBL ticker
    ''' </summary>
  Friend WithEvents lblTicker As System.Windows.Forms.Label
    ''' <summary>
    ''' The LBL type
    ''' </summary>
  Friend WithEvents lblType As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ portfolio type
    ''' </summary>
  Friend WithEvents Combo_PortfolioType As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The grid_ portfolio data
    ''' </summary>
  Friend WithEvents Grid_PortfolioData As C1.Win.C1FlexGrid.C1FlexGrid
    ''' <summary>
    ''' The edit_ filing date
    ''' </summary>
  Friend WithEvents edit_FilingDate As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The label2
    ''' </summary>
  Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPortfolioEdit))
		Me.lblTicker = New System.Windows.Forms.Label
		Me.lblType = New System.Windows.Forms.Label
		Me.editAuditID = New System.Windows.Forms.TextBox
		Me.editTicker = New System.Windows.Forms.TextBox
		Me.btnNavFirst = New System.Windows.Forms.Button
		Me.btnNavPrev = New System.Windows.Forms.Button
		Me.btnNavNext = New System.Windows.Forms.Button
		Me.btnLast = New System.Windows.Forms.Button
		Me.btnAdd = New System.Windows.Forms.Button
		Me.btnDelete = New System.Windows.Forms.Button
		Me.btnCancel = New System.Windows.Forms.Button
		Me.btnSave = New System.Windows.Forms.Button
		Me.Combo_SelectPortfolio = New System.Windows.Forms.ComboBox
		Me.Label1 = New System.Windows.Forms.Label
		Me.GroupBox1 = New System.Windows.Forms.GroupBox
		Me.btnClose = New System.Windows.Forms.Button
		Me.Grid_PortfolioData = New C1.Win.C1FlexGrid.C1FlexGrid
		Me.Combo_PortfolioType = New System.Windows.Forms.ComboBox
		Me.edit_FilingDate = New System.Windows.Forms.TextBox
		Me.Label2 = New System.Windows.Forms.Label
		CType(Me.Grid_PortfolioData, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.SuspendLayout()
		'
		'lblTicker
		'
		Me.lblTicker.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.lblTicker.Location = New System.Drawing.Point(8, 60)
		Me.lblTicker.Name = "lblTicker"
		Me.lblTicker.Size = New System.Drawing.Size(100, 20)
		Me.lblTicker.TabIndex = 39
		Me.lblTicker.Text = "Ticker"
		'
		'lblType
		'
		Me.lblType.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.lblType.Location = New System.Drawing.Point(276, 60)
		Me.lblType.Name = "lblType"
		Me.lblType.Size = New System.Drawing.Size(100, 20)
		Me.lblType.TabIndex = 40
		Me.lblType.Text = "Portfolio Type"
		'
		'editAuditID
		'
		Me.editAuditID.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.editAuditID.Enabled = False
		Me.editAuditID.Location = New System.Drawing.Point(872, 12)
		Me.editAuditID.Name = "editAuditID"
		Me.editAuditID.Size = New System.Drawing.Size(64, 20)
		Me.editAuditID.TabIndex = 6
		'
		'editTicker
		'
		Me.editTicker.Enabled = False
		Me.editTicker.Location = New System.Drawing.Point(120, 56)
		Me.editTicker.Name = "editTicker"
		Me.editTicker.Size = New System.Drawing.Size(132, 20)
		Me.editTicker.TabIndex = 7
		'
		'btnNavFirst
		'
		Me.btnNavFirst.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnNavFirst.Location = New System.Drawing.Point(396, 8)
		Me.btnNavFirst.Name = "btnNavFirst"
		Me.btnNavFirst.Size = New System.Drawing.Size(40, 28)
		Me.btnNavFirst.TabIndex = 1
		Me.btnNavFirst.Text = "<<"
		'
		'btnNavPrev
		'
		Me.btnNavPrev.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnNavPrev.Location = New System.Drawing.Point(437, 8)
		Me.btnNavPrev.Name = "btnNavPrev"
		Me.btnNavPrev.Size = New System.Drawing.Size(35, 28)
		Me.btnNavPrev.TabIndex = 2
		Me.btnNavPrev.Text = "<"
		'
		'btnNavNext
		'
		Me.btnNavNext.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnNavNext.Location = New System.Drawing.Point(476, 8)
		Me.btnNavNext.Name = "btnNavNext"
		Me.btnNavNext.Size = New System.Drawing.Size(35, 28)
		Me.btnNavNext.TabIndex = 3
		Me.btnNavNext.Text = ">"
		'
		'btnLast
		'
		Me.btnLast.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnLast.Location = New System.Drawing.Point(512, 8)
		Me.btnLast.Name = "btnLast"
		Me.btnLast.Size = New System.Drawing.Size(40, 28)
		Me.btnLast.TabIndex = 4
		Me.btnLast.Text = ">>"
		'
		'btnAdd
		'
		Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnAdd.Location = New System.Drawing.Point(564, 8)
		Me.btnAdd.Name = "btnAdd"
		Me.btnAdd.Size = New System.Drawing.Size(75, 28)
		Me.btnAdd.TabIndex = 5
		Me.btnAdd.Text = "&New"
		'
		'btnDelete
		'
		Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnDelete.Location = New System.Drawing.Point(100, 584)
		Me.btnDelete.Name = "btnDelete"
		Me.btnDelete.Size = New System.Drawing.Size(75, 28)
		Me.btnDelete.TabIndex = 12
		Me.btnDelete.Text = "&Delete"
		'
		'btnCancel
		'
		Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
		Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnCancel.Location = New System.Drawing.Point(184, 584)
		Me.btnCancel.Name = "btnCancel"
		Me.btnCancel.Size = New System.Drawing.Size(75, 28)
		Me.btnCancel.TabIndex = 13
		Me.btnCancel.Text = "&Cancel"
		'
		'btnSave
		'
		Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnSave.Location = New System.Drawing.Point(12, 584)
		Me.btnSave.Name = "btnSave"
		Me.btnSave.Size = New System.Drawing.Size(75, 28)
		Me.btnSave.TabIndex = 11
		Me.btnSave.Text = "&Save"
		'
		'Combo_SelectPortfolio
		'
		Me.Combo_SelectPortfolio.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_SelectPortfolio.Location = New System.Drawing.Point(120, 12)
		Me.Combo_SelectPortfolio.Name = "Combo_SelectPortfolio"
		Me.Combo_SelectPortfolio.Size = New System.Drawing.Size(252, 21)
		Me.Combo_SelectPortfolio.TabIndex = 0
		'
		'Label1
		'
		Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label1.Location = New System.Drawing.Point(8, 16)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(100, 16)
		Me.Label1.TabIndex = 17
		Me.Label1.Text = "Select"
		'
		'GroupBox1
		'
		Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.GroupBox1.Location = New System.Drawing.Point(8, 44)
		Me.GroupBox1.Name = "GroupBox1"
		Me.GroupBox1.Size = New System.Drawing.Size(928, 4)
		Me.GroupBox1.TabIndex = 77
		Me.GroupBox1.TabStop = False
		Me.GroupBox1.Text = "GroupBox1"
		'
		'btnClose
		'
		Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnClose.Location = New System.Drawing.Point(268, 584)
		Me.btnClose.Name = "btnClose"
		Me.btnClose.Size = New System.Drawing.Size(75, 28)
		Me.btnClose.TabIndex = 14
		Me.btnClose.Text = "&Close"
		'
		'Grid_PortfolioData
		'
		Me.Grid_PortfolioData.AllowAddNew = True
		Me.Grid_PortfolioData.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.Both
		Me.Grid_PortfolioData.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
								Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Grid_PortfolioData.ColumnInfo = resources.GetString("Grid_PortfolioData.ColumnInfo")
		Me.Grid_PortfolioData.Cursor = System.Windows.Forms.Cursors.Default
		Me.Grid_PortfolioData.Location = New System.Drawing.Point(8, 88)
		Me.Grid_PortfolioData.Name = "Grid_PortfolioData"
		Me.Grid_PortfolioData.Rows.DefaultSize = 17
		Me.Grid_PortfolioData.Size = New System.Drawing.Size(928, 488)
		Me.Grid_PortfolioData.TabIndex = 10
		'
		'Combo_PortfolioType
		'
		Me.Combo_PortfolioType.Enabled = False
		Me.Combo_PortfolioType.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_PortfolioType.Location = New System.Drawing.Point(384, 56)
		Me.Combo_PortfolioType.Name = "Combo_PortfolioType"
		Me.Combo_PortfolioType.Size = New System.Drawing.Size(252, 21)
		Me.Combo_PortfolioType.TabIndex = 8
		'
		'edit_FilingDate
		'
		Me.edit_FilingDate.Enabled = False
		Me.edit_FilingDate.Location = New System.Drawing.Point(772, 56)
		Me.edit_FilingDate.Name = "edit_FilingDate"
		Me.edit_FilingDate.Size = New System.Drawing.Size(132, 20)
		Me.edit_FilingDate.TabIndex = 9
		'
		'Label2
		'
		Me.Label2.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label2.Location = New System.Drawing.Point(660, 60)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(100, 20)
		Me.Label2.TabIndex = 81
		Me.Label2.Text = "Filing Date"
		'
		'frmPortfolioEdit
		'
		Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
		Me.CancelButton = Me.btnCancel
		Me.ClientSize = New System.Drawing.Size(940, 618)
		Me.Controls.Add(Me.edit_FilingDate)
		Me.Controls.Add(Me.Grid_PortfolioData)
		Me.Controls.Add(Me.editAuditID)
		Me.Controls.Add(Me.editTicker)
		Me.Controls.Add(Me.Label2)
		Me.Controls.Add(Me.Combo_PortfolioType)
		Me.Controls.Add(Me.btnClose)
		Me.Controls.Add(Me.GroupBox1)
		Me.Controls.Add(Me.Label1)
		Me.Controls.Add(Me.Combo_SelectPortfolio)
		Me.Controls.Add(Me.btnSave)
		Me.Controls.Add(Me.lblTicker)
		Me.Controls.Add(Me.lblType)
		Me.Controls.Add(Me.btnDelete)
		Me.Controls.Add(Me.btnCancel)
		Me.Controls.Add(Me.btnAdd)
		Me.Controls.Add(Me.btnNavPrev)
		Me.Controls.Add(Me.btnNavNext)
		Me.Controls.Add(Me.btnNavFirst)
		Me.Controls.Add(Me.btnLast)
		Me.Name = "frmPortfolioEdit"
		Me.Text = "Add/Edit Portfolio"
		CType(Me.Grid_PortfolioData, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

#End Region

#Region " Form Locals and Constants "

  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
  Private WithEvents _MainForm As VeniceMain

  ' Form Constants, specific to the table being updated.

    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
  Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

  ' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
  Private FormTooltip As New ToolTip()

  ' Form Locals, initialised on 'New' defining what standard data items to use
    ''' <summary>
  ''' Variable holding the form's data table name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_TABLENAME As String
    ''' <summary>
  ''' Variable holding the form's data adaptor name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_ADAPTORNAME As String
    ''' <summary>
  ''' Variable holding the form's dataset name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_DATASETNAME As String


  ' The standard ChangeID for this form. e.g. tblPortfolioData
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

    ''' <summary>
  ''' Principal control used for selecting items on this form. 
    ''' </summary>
  Private THIS_FORM_SelectingCombo As ComboBox
    ''' <summary>
  ''' Control to select after proessing the "New" button.
    ''' </summary>
  Private THIS_FORM_NewMoveToControl As Control

  ' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
  Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
  Private THIS_FORM_FormID As VeniceFormID

  ' Data Structures

    ''' <summary>
    ''' My dataset
    ''' </summary>
	Private myDataset As RenaissanceDataClass.DSPortfolioData		 ' Form Specific !!!!
    ''' <summary>
    ''' My table
    ''' </summary>
	Private myTable As RenaissanceDataClass.DSPortfolioData.tblPortfolioDataDataTable
    ''' <summary>
    ''' My connection
    ''' </summary>
	Private myConnection As SqlConnection
    ''' <summary>
    ''' My adaptor
    ''' </summary>
	Private myAdaptor As SqlDataAdapter

    ''' <summary>
    ''' The this standard dataset
    ''' </summary>
	Private ThisStandardDataset As RenaissanceGlobals.StandardDataset

    ''' <summary>
    ''' The D STBL best FX
    ''' </summary>
	Dim DStblBestFX As RenaissanceDataClass.DSBestFX
    ''' <summary>
    ''' The D STBL best market price
    ''' </summary>
	Dim DStblBestMarketPrice As RenaissanceDataClass.DSBestMarketPrice
    ''' <summary>
    ''' The D STBL market instruments
    ''' </summary>
	Dim DStblMarketInstruments As RenaissanceDataClass.DSMarketInstruments
    ''' <summary>
    ''' The D STBL instrument
    ''' </summary>
	Dim DStblInstrument As RenaissanceDataClass.DSInstrument

  ' 
  ' Active Element.

    ''' <summary>
    ''' The __ is over cancel button
    ''' </summary>
  Private __IsOverCancelButton As Boolean
    ''' <summary>
    ''' The _ in use
    ''' </summary>
  Private _InUse As Boolean

  ' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
  Private FormIsValid As Boolean
    ''' <summary>
    ''' The form changed
    ''' </summary>
  Private FormChanged As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
  Private _FormOpenFailed As Boolean
    ''' <summary>
    ''' The in paint
    ''' </summary>
  Private InPaint As Boolean
    ''' <summary>
    ''' The add new record
    ''' </summary>
  Private AddNewRecord As Boolean

  ' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
  Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
  Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
  Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
  Private HasDeletePermission As Boolean

  ' Grid Dictionary objects

  ' 'Hashtable' is used in preference to 'LookupCollection' as 
  ' The selection is made using a seperate Combo and the Hashtable is quicker in 
  ' all other respects. If the Hashtable dictionary was used as the Editor then the 
  ' Items are not ordered correctly and a 'LookupCollection' object should be used instead.

    ''' <summary>
    ''' The portfolio item type dictionary
    ''' </summary>
  Dim PortfolioItemTypeDictionary As Hashtable '  LookupCollection
    ''' <summary>
    ''' The portfolio market instruments dictionary
    ''' </summary>
  Dim PortfolioMarketInstrumentsDictionary As Hashtable '  LookupCollection
    ''' <summary>
    ''' The portfolio portfolio list dictionary
    ''' </summary>
  Dim PortfolioPortfolioListDictionary As Hashtable '  LookupCollection
    ''' <summary>
    ''' The portfolio venice instruments dictionary
    ''' </summary>
  Dim PortfolioVeniceInstrumentsDictionary As Hashtable '  LookupCollection

    ''' <summary>
    ''' The combo_ portfolio item type dictionary
    ''' </summary>
  Dim WithEvents Combo_PortfolioItemTypeDictionary As New CustomC1Combo ' ComboBox
    ''' <summary>
    ''' The combo_ portfolio market instruments dictionary
    ''' </summary>
  Dim WithEvents Combo_PortfolioMarketInstrumentsDictionary As New CustomC1Combo ' ComboBox
    ''' <summary>
    ''' The combo_ portfolio portfolio list dictionary
    ''' </summary>
  Dim WithEvents Combo_PortfolioPortfolioListDictionary As New CustomC1Combo ' ComboBox
    ''' <summary>
    ''' The combo_ portfolio venice instruments dictionary
    ''' </summary>
  Dim WithEvents Combo_PortfolioVeniceInstrumentsDictionary As New CustomC1Combo ' ComboBox

  ' Grid Column Constants

    ''' <summary>
    ''' The grid_ type
    ''' </summary>
  Private Grid_Type As Integer
    ''' <summary>
    ''' The grid_ instrument
    ''' </summary>
  Private Grid_Instrument As Integer
    ''' <summary>
    ''' The grid_ ticker
    ''' </summary>
  Private Grid_Ticker As Integer
    ''' <summary>
    ''' The grid_ filing date
    ''' </summary>
  Private Grid_FilingDate As Integer
    ''' <summary>
    ''' The grid_ holding
    ''' </summary>
  Private Grid_Holding As Integer
    ''' <summary>
    ''' The grid_ weight
    ''' </summary>
  Private Grid_Weight As Integer
    ''' <summary>
    ''' The grid_ price
    ''' </summary>
  Private Grid_Price As Integer
    ''' <summary>
    ''' The grid_ USD value
    ''' </summary>
  Private Grid_USDValue As Integer
    ''' <summary>
    ''' The grid_ portfolio weight
    ''' </summary>
	Private Grid_PortfolioWeight As Integer
    ''' <summary>
    ''' The grid_ monitor item
    ''' </summary>
	Private Grid_MonitorItem As Integer
    ''' <summary>
    ''' The grid_ delete
    ''' </summary>
  Private Grid_Delete As Integer
    ''' <summary>
    ''' The grid_ portfolio item ID
    ''' </summary>
  Private Grid_PortfolioItemID As Integer
    ''' <summary>
    ''' The grid_ internal use
    ''' </summary>
  Private Grid_InternalUse As Integer

#End Region

    ''' <summary>
    ''' Init_s the grid columns class.
    ''' </summary>
  Private Sub Init_GridColumnsClass()
    Try
      Grid_Type = Me.Grid_PortfolioData.Cols("Type").SafeIndex
      Grid_Instrument = Me.Grid_PortfolioData.Cols("Instrument").SafeIndex
      Grid_Ticker = Me.Grid_PortfolioData.Cols("Ticker").SafeIndex
      Grid_FilingDate = Me.Grid_PortfolioData.Cols("FilingDate").SafeIndex
      Grid_Holding = Me.Grid_PortfolioData.Cols("Holding").SafeIndex
      Grid_Weight = Me.Grid_PortfolioData.Cols("Weight").SafeIndex
      Grid_Price = Me.Grid_PortfolioData.Cols("Price").SafeIndex
      Grid_USDValue = Me.Grid_PortfolioData.Cols("USDValue").SafeIndex
      Grid_PortfolioWeight = Me.Grid_PortfolioData.Cols("PortfolioWeight").SafeIndex
			Grid_MonitorItem = Me.Grid_PortfolioData.Cols("MonitorItem").SafeIndex
			Grid_Delete = Me.Grid_PortfolioData.Cols("Delete").SafeIndex
      Grid_PortfolioItemID = Me.Grid_PortfolioData.Cols("PortfolioItemID").SafeIndex
      Grid_InternalUse = Me.Grid_PortfolioData.Cols("InternalUse").SafeIndex
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error getting Grid column indices.", ex.StackTrace, True)
    End Try

  End Sub

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
  Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
    ' Public property to return handle to the 'Main' Venice form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
  Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
    ' Public property maintaining a value indicating if the cursor is over the 'Cancel'
    ' Button on this form.
    ' This property is specifically designed for use by the field formating Event functions
    ' In order that they do not impose format restrictions if the user is about to click the 
    ' 'Cancel' button.
    '
    Get
      Return __IsOverCancelButton
    End Get
    Set(ByVal Value As Boolean)
      __IsOverCancelButton = Value
    End Set
  End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
  Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
    Get
      Return InPaint
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
    Get
      Return _InUse
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmPortfolioEdit"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  Public Sub New(ByVal pMainForm As VeniceMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()

    _MainForm = pMainForm
    AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

    _FormOpenFailed = False
    _InUse = True

    ' ******************************************************
    ' Form Specific Settings :
    ' ******************************************************

    THIS_FORM_SelectingCombo = Me.Combo_SelectPortfolio
    THIS_FORM_NewMoveToControl = Me.editTicker

    ' Form Permissioning :-

    THIS_FORM_PermissionArea = Me.Name
    THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

    ' 'This' form ID

    THIS_FORM_FormID = VeniceFormID.frmPortfolioEdit

    ' This form's dataset type.

    ThisStandardDataset = RenaissanceStandardDatasets.tblPortfolioData ' This Defines the Form Data !!! 

    ' Format Event Handlers for form controls

    AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

    ' Form Control Changed events
    AddHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
    AddHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

    '     AddHandler editTicker.TextChanged, AddressOf Me.FormControlChanged

		AddHandler Combo_PortfolioItemTypeDictionary.keyup, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_PortfolioItemTypeDictionary.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_PortfolioItemTypeDictionary.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_PortfolioItemTypeDictionary.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

    AddHandler Combo_PortfolioMarketInstrumentsDictionary.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_PortfolioMarketInstrumentsDictionary.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_PortfolioMarketInstrumentsDictionary.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_PortfolioMarketInstrumentsDictionary.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

    AddHandler Combo_PortfolioPortfolioListDictionary.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_PortfolioPortfolioListDictionary.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_PortfolioPortfolioListDictionary.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_PortfolioPortfolioListDictionary.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

    AddHandler Combo_PortfolioVeniceInstrumentsDictionary.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_PortfolioVeniceInstrumentsDictionary.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_PortfolioVeniceInstrumentsDictionary.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_PortfolioVeniceInstrumentsDictionary.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

    ' Set up the ToolTip
    MainForm.SetFormToolTip(Me, FormTooltip)

    Combo_PortfolioItemTypeDictionary.DropDownStyle = ComboBoxStyle.DropDown
    Combo_PortfolioMarketInstrumentsDictionary.DropDownStyle = ComboBoxStyle.DropDown
    Combo_PortfolioPortfolioListDictionary.DropDownStyle = ComboBoxStyle.DropDown
    Combo_PortfolioVeniceInstrumentsDictionary.DropDownStyle = ComboBoxStyle.DropDown

    ' Configure Grid
    Call Init_GridColumnsClass()

    Grid_PortfolioData.Item(0, Grid_Type) = "Type"
    Grid_PortfolioData.Item(0, Grid_Instrument) = "Instrument"
    Grid_PortfolioData.Item(0, Grid_Ticker) = "Ticker"
    Grid_PortfolioData.Item(0, Grid_FilingDate) = "Filing Date"
    Grid_PortfolioData.Item(0, Grid_Holding) = "Holding"
    Grid_PortfolioData.Item(0, Grid_Weight) = "Weight"
    Grid_PortfolioData.Item(0, Grid_Price) = "Price"
    Grid_PortfolioData.Item(0, Grid_USDValue) = "USD Value"
		Grid_PortfolioData.Item(0, Grid_MonitorItem) = "Monitor Item"
		Grid_PortfolioData.Item(0, Grid_Delete) = "Delete"
    Grid_PortfolioData.Item(0, Grid_InternalUse) = "Internal Use"

    Grid_PortfolioData.Cols(Grid_Type).Width = 120
    Grid_PortfolioData.Cols(Grid_Instrument).Width = 250
    Grid_PortfolioData.Cols(Grid_Ticker).Width = 150
		Grid_PortfolioData.Cols(Grid_MonitorItem).Width = 50
		Grid_PortfolioData.Cols(Grid_Delete).Width = 50

    Dim thisEditor As C1.Win.C1Input.C1NumericEdit
    thisEditor = New C1.Win.C1Input.C1NumericEdit

    thisEditor.DisplayFormat.CustomFormat = "#,##0.0###"
    thisEditor.DisplayFormat.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat
    thisEditor.DisplayFormat.Inherit = CType((((C1.Win.C1Input.FormatInfoInheritFlags.NullText Or C1.Win.C1Input.FormatInfoInheritFlags.EmptyAsNull) _
                Or C1.Win.C1Input.FormatInfoInheritFlags.TrimStart) _
                Or C1.Win.C1Input.FormatInfoInheritFlags.TrimEnd), C1.Win.C1Input.FormatInfoInheritFlags)
    thisEditor.EditFormat.CustomFormat = "##########0.0#######"
    thisEditor.EditFormat.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat
    thisEditor.EditFormat.Inherit = CType((((C1.Win.C1Input.FormatInfoInheritFlags.NullText Or C1.Win.C1Input.FormatInfoInheritFlags.EmptyAsNull) _
                Or C1.Win.C1Input.FormatInfoInheritFlags.TrimStart) _
                Or C1.Win.C1Input.FormatInfoInheritFlags.TrimEnd), C1.Win.C1Input.FormatInfoInheritFlags)

    Grid_PortfolioData.Cols(Grid_Holding).Editor = thisEditor

    thisEditor = New C1.Win.C1Input.C1NumericEdit
    thisEditor.DisplayFormat.CustomFormat = "#,##0.0###"
    thisEditor.DisplayFormat.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat
    thisEditor.DisplayFormat.Inherit = CType((((C1.Win.C1Input.FormatInfoInheritFlags.NullText Or C1.Win.C1Input.FormatInfoInheritFlags.EmptyAsNull) _
                Or C1.Win.C1Input.FormatInfoInheritFlags.TrimStart) _
                Or C1.Win.C1Input.FormatInfoInheritFlags.TrimEnd), C1.Win.C1Input.FormatInfoInheritFlags)
    thisEditor.EditFormat.CustomFormat = "##########0.0#######"
    thisEditor.EditFormat.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat
    thisEditor.EditFormat.Inherit = CType((((C1.Win.C1Input.FormatInfoInheritFlags.NullText Or C1.Win.C1Input.FormatInfoInheritFlags.EmptyAsNull) _
                Or C1.Win.C1Input.FormatInfoInheritFlags.TrimStart) _
                Or C1.Win.C1Input.FormatInfoInheritFlags.TrimEnd), C1.Win.C1Input.FormatInfoInheritFlags)

    Grid_PortfolioData.Cols(Grid_Weight).Editor = New C1.Win.C1Input.C1NumericEdit


    ' ******************************************************
    ' End Form Specific.
    ' ******************************************************

    ' Data object names standard to this Form type.

    THIS_TABLENAME = ThisStandardDataset.TableName
    THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
    THIS_DATASETNAME = ThisStandardDataset.DatasetName

    THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

    ' Establish / Retrieve data objects for this form.

    myConnection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)
    myAdaptor = MainForm.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, VENICE_CONNECTION, THIS_TABLENAME)
    SyncLock myConnection
      myDataset = MainForm.Load_Table(ThisStandardDataset, False)
    End SyncLock
    myTable = myDataset.Tables(0)

  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
  Public Sub ResetForm() Implements StandardVeniceForm.ResetForm

    Call Form_Load(Me, New System.EventArgs)
  End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
  Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
    ALWAYS_CLOSE_THIS_FORM = True
    Me.Close()
  End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
    _FormOpenFailed = False
    _InUse = True

    ' Initialise Data structures. Connection, Adaptor and Dataset.

    If Not (MainForm Is Nothing) Then
      FormIsValid = True
    Else
      MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (myConnection Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (myAdaptor Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (myDataset Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    ' Initialse form

    InPaint = True
    IsOverCancelButton = False

    ' Check User permissions
    Call CheckPermissions()
    If (HasReadPermission = False) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      InPaint = False
      Exit Sub
    End If

    Try
      Grid_PortfolioData.Cols(Grid_Ticker).AllowEditing = False
      Grid_PortfolioData.Cols(Grid_FilingDate).AllowEditing = False

      If Not ((Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord)) Then
        Grid_PortfolioData.Cols(Grid_Type).AllowEditing = False
        Grid_PortfolioData.Cols(Grid_Instrument).AllowEditing = False
        Grid_PortfolioData.Cols(Grid_Holding).AllowEditing = False
        Grid_PortfolioData.Cols(Grid_Weight).AllowEditing = False
				Grid_PortfolioData.Cols(Grid_MonitorItem).AllowEditing = False
			Else
				Grid_PortfolioData.Cols(Grid_Type).AllowEditing = True
				Grid_PortfolioData.Cols(Grid_Instrument).AllowEditing = True
				Grid_PortfolioData.Cols(Grid_Holding).AllowEditing = True
				Grid_PortfolioData.Cols(Grid_Weight).AllowEditing = True
				Grid_PortfolioData.Cols(Grid_MonitorItem).AllowEditing = True
			End If

    Catch ex As Exception
    End Try

    Call SetPortfolioItemTypeDictionary()
    Call SetPortfolioMarketInstrumentsDictionary()
    Call SetPortfolioPortfolioListDictionary()
    Call SetPortfolioVeniceInstrumentsDictionary()

    Call SetPortfolioIndexCombo()
    Call SetPortfolioTypeCombo()

    Dim NewStyle As C1.Win.C1FlexGrid.CellStyle

    NewStyle = Grid_PortfolioData.Styles.Add("PortfolioType", Grid_PortfolioData.GetCellStyle(0, Grid_Type))
    NewStyle.DataMap = PortfolioItemTypeDictionary

    NewStyle = Grid_PortfolioData.Styles.Add("MarketInstrument", Grid_PortfolioData.GetCellStyle(0, Grid_Instrument))
    NewStyle.DataMap = PortfolioMarketInstrumentsDictionary

    NewStyle = Grid_PortfolioData.Styles.Add("PortfolioList", Grid_PortfolioData.GetCellStyle(0, Grid_Instrument))
    NewStyle.DataMap = PortfolioPortfolioListDictionary

    NewStyle = Grid_PortfolioData.Styles.Add("VeniceInstruments", Grid_PortfolioData.GetCellStyle(0, Grid_Instrument))
    NewStyle.DataMap = PortfolioVeniceInstrumentsDictionary

    Grid_PortfolioData.Cols(Grid_Type).Style = Grid_PortfolioData.Styles("PortfolioType")

    ' Display initial record.

    If THIS_FORM_SelectingCombo.Items.Count > 0 Then
      Me.THIS_FORM_SelectingCombo.SelectedIndex = 0

      Call GetFormData()

    Else
      Me.THIS_FORM_SelectingCombo.SelectedIndex = (-1)

      Call GetFormData()
    End If

    InPaint = False


  End Sub

    ''' <summary>
    ''' Handles the Closing event of the frmPortfolioEdit control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
  Private Sub frmPortfolioEdit_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    Dim HideForm As Boolean

    ' Hide or Close this form ?
    ' All depends on how many of this form type are Open or in Cache...

    _InUse = False

    If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
      HideForm = False
    Else
      If (FormChanged = True) Then
        Call SetFormData()
      End If

      HideForm = True
      If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
        HideForm = False
      End If
    End If

    If HideForm = True Then
      MainForm.HideInFormsCollection(Me)
      Me.Hide() ' NPP Fix

      e.Cancel = True
    Else
      Try
        MainForm.RemoveFromFormsCollection(Me)
        RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

        RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

        ' Form Control Changed events
        RemoveHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
        RemoveHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        '     removehandler editTicker.TextChanged, AddressOf Me.FormControlChanged

        RemoveHandler Combo_PortfolioItemTypeDictionary.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_PortfolioItemTypeDictionary.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_PortfolioItemTypeDictionary.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_PortfolioItemTypeDictionary.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

        RemoveHandler Combo_PortfolioMarketInstrumentsDictionary.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_PortfolioMarketInstrumentsDictionary.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_PortfolioMarketInstrumentsDictionary.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_PortfolioMarketInstrumentsDictionary.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

        RemoveHandler Combo_PortfolioPortfolioListDictionary.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_PortfolioPortfolioListDictionary.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_PortfolioPortfolioListDictionary.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_PortfolioPortfolioListDictionary.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

        RemoveHandler Combo_PortfolioVeniceInstrumentsDictionary.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_PortfolioVeniceInstrumentsDictionary.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_PortfolioVeniceInstrumentsDictionary.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_PortfolioVeniceInstrumentsDictionary.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

      Catch ex As Exception
      End Try
    End If

  End Sub


#End Region

  ' Routine to handle changes / updates to tables by this and other windows.
  ' If this, or any other, form posts a change to a table, then it will invoke an update event 
  ' detailing what tables have been altered.
  ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
  ' the 'VeniceAutoUpdate' event of the main Venice form.
  ' Each form may them react as appropriate to changes in any table that might impact it.
  '
    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    Dim OrgInPaint As Boolean
    Dim KnowledgeDateChanged As Boolean
    Dim SetButtonStatus_Flag As Boolean
    Dim RepaintForm As Boolean = False

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

    Try

      OrgInPaint = InPaint
      InPaint = True
      KnowledgeDateChanged = False
      SetButtonStatus_Flag = False

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
        KnowledgeDateChanged = True
      End If

      ' ****************************************************************
      ' Check for changes relevant to this form
      ' ****************************************************************

      ' Changes to the KnowledgeDate :-
      If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
        SetButtonStatus_Flag = True
        RepaintForm = True
      End If

      ' tblMarketPrices
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblMarketPrices) = True) Or KnowledgeDateChanged Then
        If (Not (DStblBestMarketPrice Is Nothing)) Then
					DStblBestMarketPrice = MainForm.Load_Table(RenaissanceStandardDatasets.tblBestMarketPrice, False)
          Call RefreshGridPrices(PortfolioItemType.MarketInstrument)
        End If
      End If

      ' tblFX
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFX) = True) Or KnowledgeDateChanged Then
        If (Not (DStblBestFX Is Nothing)) Then
					DStblBestFX = MainForm.Load_Table(RenaissanceStandardDatasets.tblFX, False)
          Call RefreshGridPrices(PortfolioItemType.MarketInstrument)
        End If
      End If

      ' Changes to the tblUserPermissions table :-
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

        ' Check ongoing permissions.

        Call CheckPermissions()
        If (HasReadPermission = False) Then
          Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

          FormIsValid = False
          InPaint = OrgInPaint
          Me.Close()
          Exit Sub
        End If

        Try

          If Not ((Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord)) Then
            Grid_PortfolioData.Cols(Grid_Type).AllowEditing = False
            Grid_PortfolioData.Cols(Grid_Instrument).AllowEditing = False
            Grid_PortfolioData.Cols(Grid_Holding).AllowEditing = False
            Grid_PortfolioData.Cols(Grid_Weight).AllowEditing = False
						Grid_PortfolioData.Cols(Grid_MonitorItem).AllowEditing = False
					Else
						Grid_PortfolioData.Cols(Grid_Type).AllowEditing = True
						Grid_PortfolioData.Cols(Grid_Instrument).AllowEditing = True
						Grid_PortfolioData.Cols(Grid_Holding).AllowEditing = True
						Grid_PortfolioData.Cols(Grid_Weight).AllowEditing = True
						Grid_PortfolioData.Cols(Grid_MonitorItem).AllowEditing = True
					End If

        Catch ex As Exception
        End Try

        SetButtonStatus_Flag = True

      End If


      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblPortfolioItemTypes) = True) Or KnowledgeDateChanged Then
        Call SetPortfolioItemTypeDictionary()

        Dim NewStyle As C1.Win.C1FlexGrid.CellStyle

        Call SetPortfolioTypeCombo()

        ' NewStyle = Grid_PortfolioData.Styles.Add("PortfolioType", Grid_PortfolioData.GetCellStyle(0, Grid_Type))
        NewStyle = Grid_PortfolioData.Styles("PortfolioType")
        NewStyle.DataMap = PortfolioItemTypeDictionary

      End If

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblMarketInstruments) = True) Or KnowledgeDateChanged Then
        Call SetPortfolioMarketInstrumentsDictionary()

        Dim NewStyle As C1.Win.C1FlexGrid.CellStyle

        ' NewStyle = Grid_PortfolioData.Styles.Add("MarketInstrument", Grid_PortfolioData.GetCellStyle(0, Grid_Instrument))
        NewStyle = Grid_PortfolioData.Styles("MarketInstrument")
        NewStyle.DataMap = PortfolioMarketInstrumentsDictionary
      End If

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblPortfolioIndex) = True) Or KnowledgeDateChanged Then
        Call Me.SetPortfolioIndexCombo()

        Call SetPortfolioPortfolioListDictionary()

        Dim NewStyle As C1.Win.C1FlexGrid.CellStyle

        ' NewStyle = Grid_PortfolioData.Styles.Add("PortfolioList", Grid_PortfolioData.GetCellStyle(0, Grid_Instrument))
        NewStyle = Grid_PortfolioData.Styles("PortfolioList")
        NewStyle.DataMap = PortfolioPortfolioListDictionary
      End If

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrument) = True) Or KnowledgeDateChanged Then
        Call SetPortfolioVeniceInstrumentsDictionary()

        Dim NewStyle As C1.Win.C1FlexGrid.CellStyle

        ' NewStyle = Grid_PortfolioData.Styles.Add("VeniceInstruments", Grid_PortfolioData.GetCellStyle(0, Grid_Instrument))
        NewStyle = Grid_PortfolioData.Styles("VeniceInstruments")
        NewStyle.DataMap = PortfolioVeniceInstrumentsDictionary
      End If

      ' ****************************************************************
      ' Changes to the Main FORM table :-
      ' ****************************************************************

      If (e.TableChanged(THIS_FORM_ChangeID) = True) Or KnowledgeDateChanged Then

        RepaintForm = True

      End If

      ' ****************************************************************
      ' Repaint if not currently in Edit Mode
      '
      ' ****************************************************************

    Catch ex As Exception
    Finally
      InPaint = OrgInPaint
    End Try


    ' Retrieve form data if appropriate, i.e. this data item is not already being edited.
    If (RepaintForm) AndAlso (FormChanged = False) AndAlso (AddNewRecord = False) Then
      GetFormData() ' Includes a call to 'SetButtonStatus()'
    Else
      If SetButtonStatus_Flag Then
        Call SetButtonStatus()
      End If
    End If

  End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

  ' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
  Private Sub CheckPermissions()

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

  End Sub

  ' Flag changes to form controls, Event Associations made in 'New' routine.
    ''' <summary>
  ''' Basic event handler, called when a form value is changed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    If InPaint = False Then
      If (Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord) Then
        FormChanged = True
        Me.btnSave.Enabled = True
        Me.btnCancel.Enabled = True
      End If
    End If

  End Sub



#End Region

#Region " Set Form Combos / Dictionary objects (Form Specific Code) "

    ''' <summary>
    ''' Sets the portfolio index combo.
    ''' </summary>
  Private Sub SetPortfolioIndexCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_SelectPortfolio, _
    RenaissanceStandardDatasets.tblPortfolioIndex, _
    "PortfolioDescription, PortfolioFilingDate", _
    "PortfolioID", _
    "", False, True, False)

  End Sub

    ''' <summary>
    ''' Sets the portfolio type combo.
    ''' </summary>
  Private Sub SetPortfolioTypeCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_PortfolioType, _
    RenaissanceStandardDatasets.tblPortfolioType, _
    "PortfolioTypeName", _
    "PortfolioTypeID", _
    "", False, True, False)

  End Sub


    ''' <summary>
    ''' Sets the portfolio item type combo only.
    ''' </summary>
    ''' <param name="pLimitPortfolioItemTypeDictionary">The p limit portfolio item type dictionary.</param>
  Private Sub SetPortfolioItemTypeComboOnly(Optional ByVal pLimitPortfolioItemTypeDictionary As Integer = (-1))

    Static LastPortfolioItemType As Integer

    ' In order or not repeatedly re-build the dictionary object when not needed, check to see if the Limit parameter
    ' has changed. A value of < (-1) will always refresh the Dictionary.
    If (pLimitPortfolioItemTypeDictionary = LastPortfolioItemType) And (LastPortfolioItemType >= (-1)) Then
      Exit Sub
    End If
    LastPortfolioItemType = pLimitPortfolioItemTypeDictionary

    Dim SelectString As String
    SelectString = "True"

    If (pLimitPortfolioItemTypeDictionary > 0) Then
      SelectString = ("PortfolioItemType=" & pLimitPortfolioItemTypeDictionary.ToString)
    End If

    Call MainForm.SetTblGenericCombo( _
      Combo_PortfolioItemTypeDictionary, _
      RenaissanceStandardDatasets.tblPortfolioItemTypes, _
      "PortfolioItemTypeDescription", _
      "PortfolioItemType", _
      SelectString, False, True, False)

  End Sub

    ''' <summary>
    ''' Sets the portfolio item type dictionary.
    ''' </summary>
    ''' <param name="pLimitPortfolioTypeDictionary">The p limit portfolio type dictionary.</param>
  Private Sub SetPortfolioItemTypeDictionary(Optional ByVal pLimitPortfolioTypeDictionary As Integer = (-1))
		Dim DSPortfolioType As RenaissanceDataClass.DSPortfolioItemTypes
		Dim SelectedTypeRows As RenaissanceDataClass.DSPortfolioItemTypes.tblPortfolioItemTypesRow()
		Dim thisTypeRow As RenaissanceDataClass.DSPortfolioItemTypes.tblPortfolioItemTypesRow
		Dim Counter As Integer

		PortfolioItemTypeDictionary = New Hashtable	'  LookupCollection

		DSPortfolioType = MainForm.Load_Table(RenaissanceStandardDatasets.tblPortfolioItemTypes, False)
		SelectedTypeRows = DSPortfolioType.tblPortfolioItemTypes.Select("True", "PortfolioItemTypeDescription")

		If (Not (SelectedTypeRows Is Nothing)) AndAlso (SelectedTypeRows.Length > 0) Then
			For Counter = 0 To (SelectedTypeRows.Length - 1)
				thisTypeRow = SelectedTypeRows(Counter)
				PortfolioItemTypeDictionary.Add(CInt(thisTypeRow.PortfolioItemType), thisTypeRow.PortfolioItemTypeDescription.ToString)
			Next
		End If

		If (Combo_PortfolioType.SelectedIndex < 0) OrElse (Combo_PortfolioType.SelectedValue Is Nothing) OrElse (CInt(Combo_PortfolioType.SelectedValue) <> CInt(PortfolioType.IndexWeighting)) Then
			Call SetPortfolioItemTypeComboOnly(-10)
		Else
			Call SetPortfolioItemTypeComboOnly(CInt(PortfolioItemType.VeniceInstrument))
		End If


	End Sub

    ''' <summary>
    ''' Sets the portfolio market instruments dictionary.
    ''' </summary>
	Private Sub SetPortfolioMarketInstrumentsDictionary()

		Dim DSMarketInstruments As RenaissanceDataClass.DSMarketInstruments
		Dim SelectedInstrumentRows As RenaissanceDataClass.DSMarketInstruments.tblMarketInstrumentsRow()
		Dim thisInstrumentRow As RenaissanceDataClass.DSMarketInstruments.tblMarketInstrumentsRow
		Dim Counter As Integer

		PortfolioMarketInstrumentsDictionary = New Hashtable '  LookupCollection

		DSMarketInstruments = MainForm.Load_Table(RenaissanceStandardDatasets.tblMarketInstruments, False)
		If (Not (DSMarketInstruments Is Nothing)) AndAlso (DSMarketInstruments.tblMarketInstruments.Rows.Count > 0) Then
			SelectedInstrumentRows = DSMarketInstruments.tblMarketInstruments.Select("True", "Description")

			For Counter = 0 To (SelectedInstrumentRows.Length - 1)
				thisInstrumentRow = SelectedInstrumentRows(Counter)
				PortfolioMarketInstrumentsDictionary.Add(thisInstrumentRow.MarketInstrumentID, thisInstrumentRow.Description)
			Next
		End If

		' PortfolioMarketInstrumentsDictionary.Sort()

		Call MainForm.SetTblGenericCombo( _
		Combo_PortfolioMarketInstrumentsDictionary, _
		RenaissanceStandardDatasets.tblMarketInstruments, _
		"Description", _
		"MarketInstrumentID", _
		"", False, True, False)

	End Sub

    ''' <summary>
    ''' Sets the portfolio portfolio list dictionary.
    ''' </summary>
  Private Sub SetPortfolioPortfolioListDictionary()

		Dim DSPortfolioList As RenaissanceDataClass.DSPortfolioIndex
		Dim SelectedRows As RenaissanceDataClass.DSPortfolioIndex.tblPortfolioIndexRow()
		Dim thisRow As RenaissanceDataClass.DSPortfolioIndex.tblPortfolioIndexRow
		Dim Counter As Integer

		PortfolioPortfolioListDictionary = New Hashtable '  LookupCollection

		DSPortfolioList = MainForm.Load_Table(RenaissanceStandardDatasets.tblPortfolioIndex, False)
		If (Not (DSPortfolioList Is Nothing)) AndAlso (DSPortfolioList.tblPortfolioIndex.Rows.Count > 0) Then
			SelectedRows = DSPortfolioList.tblPortfolioIndex.Select("True", "PortfolioDescription")

			For Counter = 0 To (SelectedRows.Length - 1)
				thisRow = SelectedRows(Counter)
				PortfolioPortfolioListDictionary.Add(thisRow.PortfolioID, thisRow.PortfolioDescription)
			Next
		End If

		' PortfolioPortfolioListDictionary.Sort()

		Call MainForm.SetTblGenericCombo( _
		Combo_PortfolioPortfolioListDictionary, _
		RenaissanceStandardDatasets.tblPortfolioIndex, _
		"PortfolioDescription", _
		"PortfolioID", _
		"", False, True, False)

	End Sub


    ''' <summary>
    ''' Sets the portfolio venice instruments combo only.
    ''' </summary>
    ''' <param name="pLimitInstrumentTypeDictionary">The p limit instrument type dictionary.</param>
	Private Sub SetPortfolioVeniceInstrumentsComboOnly(Optional ByVal pLimitInstrumentTypeDictionary As Integer = (-1))

		Static LastInstrumentItemType As Integer

		' In order not to repeatedly re-build the combo object when not needed, check to see if the Limit parameter
		' has changed. A value of < (-1) will always refresh the Dictionary.
		If (pLimitInstrumentTypeDictionary = LastInstrumentItemType) And (LastInstrumentItemType >= (-1)) Then
			Exit Sub
		End If
		LastInstrumentItemType = pLimitInstrumentTypeDictionary

		Dim SelectString As String = "True"

		If (pLimitInstrumentTypeDictionary > 0) Then
			SelectString = ("InstrumentType=" & pLimitInstrumentTypeDictionary.ToString)
		End If

		Call MainForm.SetTblGenericCombo( _
		Combo_PortfolioVeniceInstrumentsDictionary, _
		RenaissanceStandardDatasets.tblInstrument, _
		"InstrumentDescription", _
		"InstrumentID", _
		SelectString, False, True, False)

	End Sub

    ''' <summary>
    ''' Sets the portfolio venice instruments dictionary.
    ''' </summary>
	Private Sub SetPortfolioVeniceInstrumentsDictionary()
		Dim DSInstrumentsList As RenaissanceDataClass.DSInstrument
		Dim SelectedRows As RenaissanceDataClass.DSInstrument.tblInstrumentRow()
		Dim thisRow As RenaissanceDataClass.DSInstrument.tblInstrumentRow
		Dim Counter As Integer

		PortfolioVeniceInstrumentsDictionary = New Hashtable '  LookupCollection

		DSInstrumentsList = MainForm.Load_Table(RenaissanceStandardDatasets.tblInstrument, False)
		If (Not (DSInstrumentsList Is Nothing)) AndAlso (DSInstrumentsList.tblInstrument.Rows.Count > 0) Then
			SelectedRows = DSInstrumentsList.tblInstrument.Select("True", "InstrumentDescription")

			For Counter = 0 To (SelectedRows.Length - 1)
				thisRow = SelectedRows(Counter)
				PortfolioVeniceInstrumentsDictionary.Add(thisRow.InstrumentID, thisRow.InstrumentDescription)
			Next
		End If

		If (Combo_PortfolioType.SelectedIndex < 0) OrElse (Combo_PortfolioType.SelectedValue Is Nothing) OrElse (CInt(Combo_PortfolioType.SelectedValue) <> CInt(PortfolioType.IndexWeighting)) Then
			Call SetPortfolioVeniceInstrumentsComboOnly(-10)
		Else
			Call SetPortfolioVeniceInstrumentsComboOnly(CInt(InstrumentTypes.Index))
		End If

	End Sub

#End Region

#Region " Get & Set Form Data / SetButton / ValidateForm / btnCancel Events (Form Specific Code) "

    ''' <summary>
    ''' Gets the form data.
    ''' </summary>
  Private Sub GetFormData()
    ' Routine to populate form Controls given a data row.
    ' Form is cleared for invalid Datarows or Invalid Form.

    Dim OrgInpaint As Boolean

    ' Set 'Paint' flag to prevent event actions caused by changing the value of form fields.
    OrgInpaint = InPaint
    InPaint = True


    If (Me.Combo_SelectPortfolio.SelectedIndex < 0) Or (FormIsValid = False) Or (Me.InUse = False) Then
      ' Bad / New Datarow - Clear Form.

      Me.editAuditID.Text = ""
      Me.editTicker.Text = ""
      Me.edit_FilingDate.Text = ""
      MainForm.ClearComboSelection(Me.Combo_PortfolioType)

      Call SetPortfolioItemTypeComboOnly()
      Call SetPortfolioVeniceInstrumentsComboOnly()

      Grid_PortfolioData.Rows.Count = 1

      If AddNewRecord = True Then
        Me.btnCancel.Enabled = True
        Me.THIS_FORM_SelectingCombo.Enabled = False
      Else
        Me.btnCancel.Enabled = False
        Me.THIS_FORM_SelectingCombo.Enabled = True
      End If

    Else

      ' Populate Form with given data.
      Try

        ' First Get Portfolio Data

				Dim thisPortfolioIndexDS As RenaissanceDataClass.DSPortfolioIndex
				Dim SelectedPortfolioIndex As RenaissanceDataClass.DSPortfolioIndex.tblPortfolioIndexRow()
				Dim SelectedPortfolioData As RenaissanceDataClass.DSPortfolioData.tblPortfolioDataRow()
				Dim thisPortfolioDataRow As RenaissanceDataClass.DSPortfolioData.tblPortfolioDataRow

        Dim thisPortfiolioDataID As Integer
        Dim Counter As Integer

        SelectedPortfolioIndex = Nothing

				thisPortfolioIndexDS = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblPortfolioIndex, False)
        If (Not (thisPortfolioIndexDS Is Nothing)) Then
          SelectedPortfolioIndex = thisPortfolioIndexDS.tblPortfolioIndex.Select("PortfolioID=" & Combo_SelectPortfolio.SelectedValue.ToString)
        End If

        If (SelectedPortfolioIndex Is Nothing) OrElse (SelectedPortfolioIndex.Length <= 0) Then
          Me.editAuditID.Text = ""
          Me.editTicker.Text = ""
          Me.edit_FilingDate.Text = ""
          MainForm.ClearComboSelection(Me.Combo_PortfolioType)

          Grid_PortfolioData.Rows.Count = 1
          InPaint = OrgInpaint
          Exit Sub
        End If

        Me.editTicker.Text = SelectedPortfolioIndex(0).PortfolioTicker
        Me.edit_FilingDate.Text = SelectedPortfolioIndex(0).PortfolioFilingDate.ToString(DISPLAYMEMBER_DATEFORMAT)

        Me.Combo_PortfolioType.SelectedValue = SelectedPortfolioIndex(0).PortfolioType

        ' Specific Portfolio limitations.
        If (SelectedPortfolioIndex(0).PortfolioType = CInt(PortfolioType.IndexWeighting)) Then
          Call SetPortfolioItemTypeComboOnly(CInt(PortfolioItemType.VeniceInstrument))
          Call SetPortfolioVeniceInstrumentsComboOnly(CInt(InstrumentTypes.Index))
        Else
          Call SetPortfolioItemTypeComboOnly()
          Call SetPortfolioVeniceInstrumentsComboOnly()
        End If

        thisPortfiolioDataID = SelectedPortfolioIndex(0).PortfolioDataID


        SelectedPortfolioData = myTable.Select("PortfolioDataID=" & thisPortfiolioDataID.ToString, "PortfolioItemType,MarketInstrumentID")

        Grid_PortfolioData.Rows.Count = 1 + SelectedPortfolioData.Length

        For Counter = 0 To (SelectedPortfolioData.Length - 1)
          thisPortfolioDataRow = SelectedPortfolioData(Counter)

          Grid_PortfolioData.Item(Counter + 1, Grid_Type) = thisPortfolioDataRow.PortfolioItemType
          ' Grid_PortfolioData.SetCellStyle(Counter + 1, 0, Grid_PortfolioData.Styles("PortfolioType"))

          Grid_PortfolioData.Item(Counter + 1, Grid_Instrument) = thisPortfolioDataRow.MarketInstrumentID
          Select Case thisPortfolioDataRow.PortfolioItemType
            Case RenaissanceGlobals.PortfolioItemType.MarketInstrument
              Grid_PortfolioData.SetCellStyle(Counter + 1, Grid_Instrument, Grid_PortfolioData.Styles("MarketInstrument"))

            Case RenaissanceGlobals.PortfolioItemType.VeniceInstrument
              Grid_PortfolioData.SetCellStyle(Counter + 1, Grid_Instrument, Grid_PortfolioData.Styles("VeniceInstruments"))

            Case RenaissanceGlobals.PortfolioItemType.Portfolio
              Grid_PortfolioData.SetCellStyle(Counter + 1, Grid_Instrument, Grid_PortfolioData.Styles("PortfolioList"))

            Case Else
              Grid_PortfolioData.SetCellStyle(Counter + 1, Grid_Instrument, Grid_PortfolioData.GetCellStyle(0, Grid_Instrument))

          End Select

          Grid_PortfolioData.Item(Counter + 1, Grid_Ticker) = thisPortfolioDataRow.Ticker
          Grid_PortfolioData.Item(Counter + 1, Grid_FilingDate) = thisPortfolioDataRow.FilingDate
          Grid_PortfolioData.Item(Counter + 1, Grid_Holding) = thisPortfolioDataRow.Holding
          Grid_PortfolioData.Item(Counter + 1, Grid_Weight) = thisPortfolioDataRow.Weight
					Grid_PortfolioData.Item(Counter + 1, Grid_MonitorItem) = thisPortfolioDataRow.MonitorItem
					Grid_PortfolioData.Item(Counter + 1, Grid_Delete) = False
					Grid_PortfolioData.Item(Counter + 1, Grid_PortfolioItemID) = thisPortfolioDataRow.PortfolioItemID
          Grid_PortfolioData.Item(Counter + 1, Grid_InternalUse) = False

          Grid_PortfolioData.Item(Counter + 1, Grid_Price) = GetPortfolioItemPrice(CType(thisPortfolioDataRow.PortfolioItemType, PortfolioItemType), thisPortfolioDataRow.MarketInstrumentID)
          Grid_PortfolioData.Item(Counter + 1, Grid_USDValue) = CDbl(Grid_PortfolioData.Item(Counter + 1, Grid_Price)) * thisPortfolioDataRow.Holding * GetPortfolioItemFX(CType(thisPortfolioDataRow.PortfolioItemType, RenaissanceGlobals.PortfolioItemType), thisPortfolioDataRow.MarketInstrumentID)
        Next

        Call RefreshGridPortfolioWeights()

        AddNewRecord = False
				' MainForm.SetComboSelectionLengths(Me)

        Me.btnCancel.Enabled = False
        Me.THIS_FORM_SelectingCombo.Enabled = True

      Catch ex As Exception

        Call MainForm.LogError(Me.Name & ", GetFormData", 0, ex.Message, "Error Showing Data", ex.StackTrace, True)
        Me.Combo_SelectPortfolio.SelectedIndex = 0
        Me.Combo_SelectPortfolio.SelectedIndex = (-1)
        Call GetFormData()

      End Try

    End If

    ' Allow Field events to trigger before 'InPaint' Is re-set. 
    ' (Should) Prevent Validation errors during Form Draw.
    Application.DoEvents()

    ' Restore 'Paint' flag.
    InPaint = OrgInpaint
    FormChanged = False
    Me.btnSave.Enabled = False

    ' As it says on the can....
    Call SetButtonStatus()

  End Sub

    ''' <summary>
    ''' Sets the form data.
    ''' </summary>
    ''' <param name="pConfirm">if set to <c>true</c> [p confirm].</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function SetFormData(Optional ByVal pConfirm As Boolean = True) As Boolean
    ' *************************************************************
    '
    ' *************************************************************
    Dim ErrMessage As String
    Dim ErrFlag As Boolean
    Dim ErrStack As String
    Dim ProtectedItem As Boolean = False

    ErrMessage = ""
    ErrStack = ""

    ' *************************************************************
    ' Appropriate Save permission :-
    ' *************************************************************

    If (Me.HasUpdatePermission = False) And ((Me.HasInsertPermission = False) Or (Me.AddNewRecord = False)) Then
      MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "You do not have permission to save this record.", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Function
    End If

    ' *************************************************************
    ' If Save button is disabled then should not be able to save, exit silently.
    ' *************************************************************
    If Me.btnSave.Enabled = False Then
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Function
    End If

    ' *************************************************************
    ' KnowledgeDate OK :-
    ' *************************************************************

    If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
      MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Changes to Venice are not allowed when the Knowledgedate is not set to `NOW`", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Function
    End If

    ' Confirm Save, if required.

    If (pConfirm = True) Then
      If MessageBox.Show("Save Changes ?", "Save Changes ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
        Return True
        Exit Function
      End If
    End If

    ' *************************************************************
    ' Procedure to Save the current form information.
    ' *************************************************************

    Dim StatusString As String
    Dim UpdateRows(0) As DataRow

    If (FormChanged = False) Or (FormIsValid = False) Then
      Return False
      Exit Function
    End If

    ' Validation
    StatusString = ""
    If ValidateForm(StatusString) = False Then
      MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Validation Error." & vbCrLf & StatusString, "", True)
      Return False
      Exit Function
    End If



    ' Get Portfolio Index
    Dim thisPortfolioID As Integer
    Dim thisPortfolioDataID As Integer

		Dim DSPortfolio As RenaissanceDataClass.DSPortfolioIndex
		Dim tblPortfolio As RenaissanceDataClass.DSPortfolioIndex.tblPortfolioIndexDataTable
		Dim SelectedInst As RenaissanceDataClass.DSPortfolioIndex.tblPortfolioIndexRow()

		thisPortfolioID = (-1)
		thisPortfolioDataID = (-1)

		DSPortfolio = MainForm.Load_Table(RenaissanceStandardDatasets.tblPortfolioIndex, False)
		If Not (DSPortfolio Is Nothing) Then
			tblPortfolio = DSPortfolio.tblPortfolioIndex

			SelectedInst = Nothing
			Try
				SelectedInst = DSPortfolio.tblPortfolioIndex.Select("(PortfolioTicker='" & Me.editTicker.Text & "') AND (PortfolioFilingDate='" & Me.edit_FilingDate.Text & "')")
			Catch ex As Exception

			End Try
			If (Not (SelectedInst Is Nothing)) AndAlso (SelectedInst.Length > 0) Then
				thisPortfolioID = SelectedInst(0).PortfolioID
				thisPortfolioDataID = SelectedInst(0).PortfolioDataID
			End If
		End If

		If (thisPortfolioID < 0) Or (thisPortfolioDataID < 0) Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Failed to get PortfolioID or DataID.", "", True)
			Return False
			Exit Function
		End If

		' *************************************************************
		' Lock the Data Table, to prevent update conflicts.
		' *************************************************************

		' Set 'Paint' flag.
		InPaint = True

		Dim PortfolioDataSelectedRows As RenaissanceDataClass.DSPortfolioData.tblPortfolioDataRow()
		Dim newPortfolioDataRow As RenaissanceDataClass.DSPortfolioData.tblPortfolioDataRow

		' Grid_PortfolioData.Item(Counter + 1, Grid_PortfolioItemID) = thisPortfolioDataRow.PortfolioItemID

		SyncLock myTable
			Try

				Dim RowsCounter As Integer
				Dim GridCounter As Integer
				Dim FoundFlag As Boolean

				For GridCounter = 1 To (Me.Grid_PortfolioData.Rows.Count - 1)
					Me.Grid_PortfolioData.Item(GridCounter, Me.Grid_InternalUse) = False
				Next

				PortfolioDataSelectedRows = myTable.Select("PortfolioDataID=" & thisPortfolioDataID.ToString)

				' Delete Rows that are not present in the grid.

				If (PortfolioDataSelectedRows.Length > 0) Then
					For RowsCounter = 0 To (PortfolioDataSelectedRows.Length - 1)
						FoundFlag = False

						For GridCounter = 1 To (Me.Grid_PortfolioData.Rows.Count - 1)
							If (Not (Grid_PortfolioData.Item(GridCounter, Grid_PortfolioItemID) Is Nothing)) Then
								If (PortfolioDataSelectedRows(RowsCounter).PortfolioItemID = CInt(Grid_PortfolioData.Item(GridCounter, Grid_PortfolioItemID))) Then
									FoundFlag = True
									Exit For
								End If
							End If

						Next

						If FoundFlag = False Then
							PortfolioDataSelectedRows(RowsCounter).Delete()
						End If
					Next
				End If

				' Update existing Rows

				For GridCounter = 1 To (Me.Grid_PortfolioData.Rows.Count - 1)
					For RowsCounter = 0 To (PortfolioDataSelectedRows.Length - 1)

						' If this Row has not already been Updated
						If CType(Me.Grid_PortfolioData.Item(GridCounter, Me.Grid_InternalUse), Boolean) = False Then
							' And this row has not been deleted or modified
							If (PortfolioDataSelectedRows(RowsCounter).RowState = DataRowState.Unchanged) Then

								' If this Row matches this Grid line, then...
								If (Not (Grid_PortfolioData.Item(GridCounter, Grid_PortfolioItemID) Is Nothing)) AndAlso (PortfolioDataSelectedRows(RowsCounter).PortfolioItemID = CInt(Grid_PortfolioData.Item(GridCounter, Grid_PortfolioItemID))) Then
									'If (PortfolioDataSelectedRows(RowsCounter).PortfolioItemType = Grid_PortfolioData.Item(GridCounter, Grid_Type)) And (PortfolioDataSelectedRows(RowsCounter).MarketInstrumentID = Grid_PortfolioData.Item(GridCounter, Grid_Instrument)) Then

									If CType(Grid_PortfolioData.Item(GridCounter, Grid_Delete), Boolean) = True Then

										PortfolioDataSelectedRows(RowsCounter).Delete()

									Else
										' Check for changes
										If (Not (PortfolioDataSelectedRows(RowsCounter).Ticker = CType(Me.Grid_PortfolioData.Item(GridCounter, Me.Grid_Ticker), String))) OrElse _
										 (Not (PortfolioDataSelectedRows(RowsCounter).FilingDate = CType(Me.Grid_PortfolioData.Item(GridCounter, Me.Grid_FilingDate), Date))) OrElse _
										 (Not (PortfolioDataSelectedRows(RowsCounter).Holding = CDbl(Me.Grid_PortfolioData.Item(GridCounter, Me.Grid_Holding)))) OrElse _
										 (Not (PortfolioDataSelectedRows(RowsCounter).Weight = CDbl(Me.Grid_PortfolioData.Item(GridCounter, Me.Grid_Weight)))) OrElse _
										 (Not (PortfolioDataSelectedRows(RowsCounter).MarketInstrumentID = CInt(Me.Grid_PortfolioData.Item(GridCounter, Me.Grid_Instrument)))) OrElse _
										 (Not (PortfolioDataSelectedRows(RowsCounter).PortfolioItemType = CInt(Me.Grid_PortfolioData.Item(GridCounter, Me.Grid_Type)))) OrElse _
										 (Not (PortfolioDataSelectedRows(RowsCounter).MonitorItem = CBool(Me.Grid_PortfolioData.Item(GridCounter, Me.Grid_MonitorItem)))) Then

											PortfolioDataSelectedRows(RowsCounter).PortfolioItemType = CInt(Me.Grid_PortfolioData.Item(GridCounter, Me.Grid_Type))
											PortfolioDataSelectedRows(RowsCounter).MarketInstrumentID = CInt(Me.Grid_PortfolioData.Item(GridCounter, Me.Grid_Instrument))
											PortfolioDataSelectedRows(RowsCounter).Ticker = CStr(Nz(Me.Grid_PortfolioData.Item(GridCounter, Me.Grid_Ticker), ""))
											PortfolioDataSelectedRows(RowsCounter).FilingDate = CDate(Nz(Me.Grid_PortfolioData.Item(GridCounter, Me.Grid_FilingDate), #1/1/1900#))
											PortfolioDataSelectedRows(RowsCounter).Holding = CDbl(Nz(Me.Grid_PortfolioData.Item(GridCounter, Me.Grid_Holding), 0))
											PortfolioDataSelectedRows(RowsCounter).Weight = CDbl(Nz(Me.Grid_PortfolioData.Item(GridCounter, Me.Grid_Weight), 0))
											PortfolioDataSelectedRows(RowsCounter).MonitorItem = CBool(Nz(Me.Grid_PortfolioData.Item(GridCounter, Me.Grid_MonitorItem), 0))
										End If

									End If

									Me.Grid_PortfolioData.Item(GridCounter, Me.Grid_InternalUse) = True

									Exit For
								End If
							End If
						End If
					Next
				Next

				' Add new rows

				For GridCounter = 1 To (Me.Grid_PortfolioData.Rows.Count - 1)
					If CType(Me.Grid_PortfolioData.Item(GridCounter, Me.Grid_InternalUse), Boolean) = False Then
						If IsNumeric(Grid_PortfolioData.Item(GridCounter, Me.Grid_Type)) And IsNumeric(Grid_PortfolioData.Item(GridCounter, Me.Grid_Instrument)) Then
							newPortfolioDataRow = myTable.NewtblPortfolioDataRow

							newPortfolioDataRow.PortfolioDataID = thisPortfolioDataID
							newPortfolioDataRow.PortfolioItemType = CInt(Grid_PortfolioData.Item(GridCounter, Me.Grid_Type))
							newPortfolioDataRow.MarketInstrumentID = CInt(Grid_PortfolioData.Item(GridCounter, Me.Grid_Instrument))
							newPortfolioDataRow.Ticker = CStr(Nz(Grid_PortfolioData.Item(GridCounter, Me.Grid_Ticker), ""))
							newPortfolioDataRow.FilingDate = CDate(Nz(Grid_PortfolioData.Item(GridCounter, Me.Grid_FilingDate), #1/1/1900#))
							newPortfolioDataRow.Holding = CDbl(Nz(Grid_PortfolioData.Item(GridCounter, Me.Grid_Holding), 0))
							newPortfolioDataRow.Weight = CDbl(Nz(Grid_PortfolioData.Item(GridCounter, Me.Grid_Weight), 0))
							newPortfolioDataRow.MonitorItem = CBool(Nz(Me.Grid_PortfolioData.Item(GridCounter, Me.Grid_MonitorItem), 0))
							newPortfolioDataRow.XMLData = ""

							myTable.Rows.Add(newPortfolioDataRow)
						Else
							If GridCounter < (Me.Grid_PortfolioData.Rows.Count - 1) Then
								' Ignore missing details on the last grid line as that is probably the 
								' blank 'Add New' line.
								MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Invalid Type or Instrument. Row " & GridCounter.ToString, "", True)
							End If
						End If

					End If
				Next

				' Update Table
				myAdaptor.InsertCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate
				myAdaptor.UpdateCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate

				myAdaptor.Update(myTable)

        MainForm.ReloadTable(RenaissanceChangeID.tblPortfolioData, Nothing, False)
			Catch ex As Exception
				ErrFlag = True
				ErrMessage = ex.Message
				ErrStack = ex.StackTrace
			End Try

		End SyncLock

		' *************************************************************
		' Error report. Not done in the Try-Catch as that would serve to persist the SyncLock.
		' *************************************************************

		If (ErrFlag = True) Then
			Call MainForm.LogError(Me.Name & ", SetFormData", 0, ErrMessage, "Error Saving Data", ErrStack, True)
		End If

		' Finish off

		InPaint = False
		AddNewRecord = False
		FormChanged = False

		Me.THIS_FORM_SelectingCombo.Enabled = True

		' Propogate changes

		'Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID))
		Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID), True)

	End Function

    ''' <summary>
    ''' Sets the button status.
    ''' </summary>
	Private Sub SetButtonStatus()
		' Sets the status of the form controlls appropriate to the current users 
		'permissions and the 'Changed' or 'New' status of the form.

		' No Read permission :-

		If Me.HasReadPermission = False Then
			MainForm.LogError(Me.Name & ", SetButtonStatus()", 0, "", "You do not have Read permission for this Form", "", True)
			Me.Close()
			Exit Sub
		End If

		' Has Insert Permission.
		If Me.HasInsertPermission Then
			Me.btnAdd.Enabled = True
		Else
			Me.btnAdd.Enabled = False
		End If

		' Has Delete permission. 
		If (Me.HasDeletePermission) And (Me.AddNewRecord = False) Then
			Me.btnDelete.Enabled = True
		Else
			Me.btnDelete.Enabled = False
		End If

		If (Me.MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And _
			((Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord)) Then

			Me.editTicker.Enabled = True

		Else

			Me.editTicker.Enabled = False

		End If

	End Sub

    ''' <summary>
    ''' Validates the form.
    ''' </summary>
    ''' <param name="pReturnString">The p return string.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	Private Function ValidateForm(ByRef pReturnString As String) As Boolean
		' Form Validation code.
		' 
		' This code should be the final arbiter of what is allowed. no assumptions regarding 
		' prior validation should be made.
		' 
		' This Code is called by the SetFormData routine before position changes.
		'
		Dim RVal As Boolean

		RVal = True
		pReturnString = ""

		' Validate 

		If Me.editTicker.Text.Length <= 0 Then
			pReturnString = "Currency Code must not be left blank."
			RVal = False
		End If


		Return RVal

	End Function

    ''' <summary>
    ''' Handles the MouseEnter event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnCancel_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseEnter
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = True
	End Sub

    ''' <summary>
    ''' Handles the MouseLeave event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnCancel_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseLeave
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = False
	End Sub


#End Region

#Region " Navigation Code / GetPosition() (Generic Code) "

    ''' <summary>
    ''' Handles the SelectComboChanged event of the Combo control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_SelectComboChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
		' Selection Combo. SelectedItem changed.
		'

		' Don't react to changes made in paint routines etc.
		If InPaint = True Then Exit Sub

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		Call GetFormData()

	End Sub


    ''' <summary>
    ''' Handles the Click event of the btnNavPrev control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnNavPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavPrev.Click
		' 'Previous' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		If (THIS_FORM_SelectingCombo.Items.Count > 0) AndAlso (THIS_FORM_SelectingCombo.SelectedIndex > 0) Then
			THIS_FORM_SelectingCombo.SelectedIndex -= 1
		End If

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnNavNext control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnNavNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavNext.Click
		' 'Next' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		If (THIS_FORM_SelectingCombo.Items.Count > 0) AndAlso (THIS_FORM_SelectingCombo.SelectedIndex < (THIS_FORM_SelectingCombo.Items.Count - 1)) Then
			THIS_FORM_SelectingCombo.SelectedIndex += 1
		End If

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnNavFirst control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnNavFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavFirst.Click
		' 'First' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		If (THIS_FORM_SelectingCombo.Items.Count > 0) Then
			THIS_FORM_SelectingCombo.SelectedIndex = 0
		End If

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnLast control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLast.Click
		' 'Last' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		If (THIS_FORM_SelectingCombo.Items.Count > 0) AndAlso (THIS_FORM_SelectingCombo.SelectedIndex < (THIS_FORM_SelectingCombo.Items.Count - 1)) Then
			THIS_FORM_SelectingCombo.SelectedIndex = (THIS_FORM_SelectingCombo.Items.Count - 1)
		End If

	End Sub

#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

    ''' <summary>
    ''' Handles the Click event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
		' Cancel Changes, redisplay form.

		FormChanged = False
		AddNewRecord = False

		Me.THIS_FORM_SelectingCombo.Enabled = True

		Call GetFormData()

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnSave control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
		' Save Changes, if any, without prompting.

		If (FormChanged = True) Then
			Call SetFormData(False)
		End If

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnDelete control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click

		If (AddNewRecord = True) Then
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Sub
		End If

		' No Appropriate Save permission :-

		If (Me.HasDeletePermission = False) Then
			MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "You do not have permission to Delete this record.", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Sub
		End If

		' *************************************************************
		' KnowledgeDate OK :-
		' *************************************************************

		If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
			MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "Changes to Venice are not allowed when the Knowledgedate is not set to `NOW`", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Sub
		End If

		' *************************************************************
		' Confirm :-
		' *************************************************************
		If MsgBox("Are you Sure ?", MsgBoxStyle.OkCancel, "Confirm Delete") = MsgBoxResult.Cancel Then
			Exit Sub
		End If

		' Delete this Portfolio


		' Tidy Up.

		FormChanged = False

		'Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID))
		Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID), True)

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnAdd control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
		' Prepare form to Add a new record.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		MainForm.New_VeniceForm(VeniceFormID.frmPortfolioIndex)

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnClose control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
		' Close Form

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		Me.Close()

	End Sub

#End Region

#Region " Bug hunting "

	' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
	' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
	' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

    ''' <summary>
    ''' Called when [row updating].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
	Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
		Dim a As Integer

		If Not e.Command Is Nothing Then
			a = 1
		End If

		If Not e.Errors Is Nothing Then
			a = 2
		End If
	End Sub

    ''' <summary>
    ''' Called when [row updated].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
	Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
		Dim a As Integer

		If Not e.Command Is Nothing Then
			a = 1
		End If

		If Not e.Errors Is Nothing Then
			a = 2
		End If
	End Sub

    ''' <summary>
    ''' Called when [row fill error].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
	Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
		Dim a As Integer

		a = 1
	End Sub

#End Region

#Region " Grid Event Code"

    ''' <summary>
    ''' Handles the AfterDragColumn event of the Grid_Transactions control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="C1.Win.C1FlexGrid.DragRowColEventArgs"/> instance containing the event data.</param>
	Private Sub Grid_Transactions_AfterDragColumn(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.DragRowColEventArgs) Handles Grid_PortfolioData.AfterDragColumn
		' **********************************************************************************
		' Handle the Moving of Grid columns
		'
		' **********************************************************************************
		Call Init_GridColumnsClass()
	End Sub

    ''' <summary>
    ''' Handles the AfterEdit event of the Grid_PortfolioData control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="C1.Win.C1FlexGrid.RowColEventArgs"/> instance containing the event data.</param>
	Private Sub Grid_PortfolioData_AfterEdit(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles Grid_PortfolioData.AfterEdit
		If Me.InPaint = False Then

			If e.Col = Grid_Type Then

				Select Case CInt(Me.Grid_PortfolioData.Item(e.Row, e.Col))
					Case RenaissanceGlobals.PortfolioItemType.MarketInstrument
						Grid_PortfolioData.SetCellStyle(e.Row, Grid_Instrument, Grid_PortfolioData.Styles("MarketInstrument"))

					Case RenaissanceGlobals.PortfolioItemType.VeniceInstrument
						Grid_PortfolioData.SetCellStyle(e.Row, Grid_Instrument, Grid_PortfolioData.Styles("VeniceInstruments"))

					Case RenaissanceGlobals.PortfolioItemType.Portfolio
						Grid_PortfolioData.SetCellStyle(e.Row, Grid_Instrument, Grid_PortfolioData.Styles("PortfolioList"))

					Case Else
						Grid_PortfolioData.SetCellStyle(e.Row, Grid_Instrument, Grid_PortfolioData.GetCellStyle(0, Grid_Instrument))

				End Select

				' Set Default Instrument ID, First value in DataMap (or default of 1)
				Dim tempCollection As ICollection
				Dim tempKeyValue As Object
				Grid_PortfolioData.Item(e.Row, Grid_Instrument) = 1
				If Not (Grid_PortfolioData.GetCellStyle(e.Row, Grid_Instrument) Is Nothing) Then
					tempCollection = Grid_PortfolioData.GetCellStyle(e.Row, Grid_Instrument).DataMap.Keys
					For Each tempKeyValue In tempCollection
						If IsNumeric(tempKeyValue) Then
							Grid_PortfolioData.Item(e.Row, Grid_Instrument) = tempKeyValue
							Exit For
						End If
					Next
				End If

				Application.DoEvents()

				Grid_PortfolioData.Item(e.Row, Grid_Ticker) = ""
				Grid_PortfolioData.Item(e.Row, Grid_FilingDate) = #1/1/1900#
				Grid_PortfolioData.Item(e.Row, Grid_Holding) = 0
				Grid_PortfolioData.Item(e.Row, Grid_Weight) = 0
				Grid_PortfolioData.Item(e.Row, Grid_MonitorItem) = 0
				Grid_PortfolioData.Item(e.Row, Grid_Delete) = 0
				Grid_PortfolioData.Item(e.Row, Grid_InternalUse) = 0

			End If

			If (e.Col = Grid_Instrument) Or (e.Col = Grid_Type) Then
				' Run this code for (e.Col = Grid_Type) so that the Ticker is set for the New Instrument selected
				' If a New Type is selected...

				Grid_PortfolioData.Item(e.Row, Grid_Ticker) = ""

				If IsDate(Me.edit_FilingDate.Text) Then
					Grid_PortfolioData.Item(e.Row, Grid_FilingDate) = CDate(Me.edit_FilingDate.Text)
				Else
					Grid_PortfolioData.Item(e.Row, Grid_FilingDate) = Renaissance_BaseDate
				End If

				Grid_PortfolioData.Item(e.Row, Grid_Holding) = 0
				Grid_PortfolioData.Item(e.Row, Grid_Weight) = 0
				Grid_PortfolioData.Item(e.Row, Grid_MonitorItem) = 0
				Grid_PortfolioData.Item(e.Row, Grid_Delete) = 0
				Grid_PortfolioData.Item(e.Row, Grid_InternalUse) = 0

				Select Case CInt(Me.Grid_PortfolioData.Item(e.Row, Grid_Type))

					Case RenaissanceGlobals.PortfolioItemType.MarketInstrument
						Dim DSMktInst As RenaissanceDataClass.DSMarketInstruments
						Dim tblMktInst As RenaissanceDataClass.DSMarketInstruments.tblMarketInstrumentsDataTable
						Dim SelectedMktInst As RenaissanceDataClass.DSMarketInstruments.tblMarketInstrumentsRow()

						DSMktInst = MainForm.Load_Table(RenaissanceStandardDatasets.tblMarketInstruments, False)
						If Not (DSMktInst Is Nothing) Then
							tblMktInst = DSMktInst.tblMarketInstruments
							SelectedMktInst = tblMktInst.Select("MarketInstrumentID=" & CStr(Grid_PortfolioData.Item(e.Row, Grid_Instrument)))
							If (SelectedMktInst.Length > 0) Then
								Grid_PortfolioData.Item(e.Row, Grid_Ticker) = SelectedMktInst(0).Ticker
							End If
						End If

					Case RenaissanceGlobals.PortfolioItemType.VeniceInstrument
						Dim DSVeniceInst As RenaissanceDataClass.DSInstrument
						Dim tblVeniceInst As RenaissanceDataClass.DSInstrument.tblInstrumentDataTable
						Dim SelectedInst As RenaissanceDataClass.DSInstrument.tblInstrumentRow()

						DSVeniceInst = MainForm.Load_Table(RenaissanceStandardDatasets.tblInstrument, False)
						If Not (DSVeniceInst Is Nothing) Then
							tblVeniceInst = DSVeniceInst.tblInstrument
							SelectedInst = tblVeniceInst.Select("InstrumentID=" & CStr(Grid_PortfolioData.Item(e.Row, Grid_Instrument)))
							If (SelectedInst.Length > 0) Then
								Grid_PortfolioData.Item(e.Row, Grid_Ticker) = SelectedInst(0).InstrumentExchangeCode
							End If
						End If

					Case RenaissanceGlobals.PortfolioItemType.Portfolio
						Dim DSPortfolio As RenaissanceDataClass.DSPortfolioIndex
						Dim tblPortfolio As RenaissanceDataClass.DSPortfolioIndex.tblPortfolioIndexDataTable
						Dim SelectedInst As RenaissanceDataClass.DSPortfolioIndex.tblPortfolioIndexRow()

						DSPortfolio = MainForm.Load_Table(RenaissanceStandardDatasets.tblPortfolioIndex, False)
						If Not (DSPortfolio Is Nothing) Then
							tblPortfolio = DSPortfolio.tblPortfolioIndex
							SelectedInst = tblPortfolio.Select("PortfolioID=" & CStr(Grid_PortfolioData.Item(e.Row, Grid_Instrument)))
							If (SelectedInst.Length > 0) Then
								Grid_PortfolioData.Item(e.Row, Grid_Ticker) = SelectedInst(0).PortfolioTicker
							End If
						End If

					Case Else

				End Select

				' Get Price if Type or Instrument changes, USD Value Should cascade automatically

				Grid_PortfolioData.Item(e.Row, Grid_Price) = GetPortfolioItemPrice(CType(Grid_PortfolioData.Item(e.Row, Grid_Type), RenaissanceGlobals.PortfolioItemType), CInt(Grid_PortfolioData.Item(e.Row, Grid_Instrument)))
				Grid_PortfolioData.Item(e.Row, Grid_USDValue) = CDbl(Grid_PortfolioData.Item(e.Row, Grid_Price)) * CDbl(Grid_PortfolioData.Item(e.Row, Grid_Holding)) * GetPortfolioItemFX(CType(Grid_PortfolioData.Item(e.Row, Grid_Type), RenaissanceGlobals.PortfolioItemType), CInt(Grid_PortfolioData.Item(e.Row, Grid_Instrument)))
				Call RefreshGridPortfolioWeights()
			ElseIf e.Col = Grid_Ticker Then

			ElseIf e.Col = Grid_FilingDate Then

			ElseIf (e.Col = Grid_Holding) Or (e.Col = Grid_Weight) Then
				Try
					If Not (Grid_PortfolioData.Cols(e.Col).Editor Is Nothing) Then

						If (TypeOf Grid_PortfolioData.Cols(e.Col).Editor Is C1.Win.C1Input.C1NumericEdit) Then
							Grid_PortfolioData.Item(e.Row, e.Col) = CType(Grid_PortfolioData.Cols(e.Col).Editor, C1.Win.C1Input.C1NumericEdit).Value
						End If

						'If IsNumeric(Grid_PortfolioData.Cols(e.Col).Editor.Text) Then
						'  Grid_PortfolioData.Item(e.Row, e.Col) = CDbl(Grid_PortfolioData.Cols(e.Col).Editor.Text)
						'End If
					End If
				Catch ex As Exception
				End Try

				Try
					Grid_PortfolioData.Item(e.Row, Grid_USDValue) = CDbl(Grid_PortfolioData.Item(e.Row, Grid_Price)) * CDbl(Grid_PortfolioData.Item(e.Row, Grid_Holding)) * GetPortfolioItemFX(CType(Grid_PortfolioData.Item(e.Row, Grid_Type), PortfolioItemType), CInt(Grid_PortfolioData.Item(e.Row, Grid_Instrument)))
					Call RefreshGridPortfolioWeights()

				Catch ex As Exception
				End Try

			ElseIf e.Col = Grid_MonitorItem Then

			ElseIf e.Col = Grid_Delete Then

			ElseIf e.Col = Grid_Price Then
				Grid_PortfolioData.Item(e.Row, Grid_USDValue) = CDbl(Grid_PortfolioData.Item(e.Row, Grid_Price)) * CDbl(Grid_PortfolioData.Item(e.Row, Grid_Holding)) * GetPortfolioItemFX(CType(Grid_PortfolioData.Item(e.Row, Grid_Type), PortfolioItemType), CInt(Grid_PortfolioData.Item(e.Row, Grid_Instrument)))
				Call RefreshGridPortfolioWeights()

			ElseIf e.Col = Grid_USDValue Then

			ElseIf e.Col = Grid_InternalUse Then
				Exit Sub
			End If

			If (Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord) Then
				Call Me.FormControlChanged(Me, New EventArgs)
			End If
		End If
	End Sub

    ''' <summary>
    ''' Handles the ValidateEdit event of the Grid_PortfolioData control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="C1.Win.C1FlexGrid.ValidateEditEventArgs"/> instance containing the event data.</param>
	Private Sub Grid_PortfolioData_ValidateEdit(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.ValidateEditEventArgs) Handles Grid_PortfolioData.ValidateEdit
		If Me.InPaint = False Then


		End If
	End Sub

    ''' <summary>
    ''' Handles the StartEdit event of the Grid_PortfolioData control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="C1.Win.C1FlexGrid.RowColEventArgs"/> instance containing the event data.</param>
	Private Sub Grid_PortfolioData_StartEdit(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles Grid_PortfolioData.StartEdit

		If e.Col = Me.Grid_Type Then
			Grid_PortfolioData.Editor = Combo_PortfolioItemTypeDictionary
		End If

		If e.Col = Me.Grid_Instrument Then
			Select Case CInt(Grid_PortfolioData.Item(e.Row, Grid_Type))
				Case RenaissanceGlobals.PortfolioItemType.MarketInstrument
					Grid_PortfolioData.Editor = Combo_PortfolioMarketInstrumentsDictionary

				Case RenaissanceGlobals.PortfolioItemType.Portfolio
					Grid_PortfolioData.Editor = Combo_PortfolioPortfolioListDictionary

				Case RenaissanceGlobals.PortfolioItemType.VeniceInstrument
					Grid_PortfolioData.Editor = Combo_PortfolioVeniceInstrumentsDictionary

				Case Else

			End Select
		End If

	End Sub

#End Region

#Region " Portfolio Instrument Pricing and Weight functions"

    ''' <summary>
    ''' Gets the portfolio item price.
    ''' </summary>
    ''' <param name="ItemType">Type of the item.</param>
    ''' <param name="ItemID">The item ID.</param>
    ''' <returns>System.Double.</returns>
  Private Function GetPortfolioItemPrice(ByVal ItemType As RenaissanceGlobals.PortfolioItemType, ByVal ItemID As Integer) As Double

    ' DStblBestMarketPrice
    Select Case ItemType
      Case PortfolioItemType.MarketInstrument

        Try
          ' First Get Market Prices
          If (DStblBestMarketPrice Is Nothing) Then
						DStblBestMarketPrice = MainForm.Load_Table(RenaissanceStandardDatasets.tblBestMarketPrice, False)
          End If

          If (DStblBestMarketPrice Is Nothing) Then
            ' Some kind of error ?
            Return 0
          End If

          ' Select appropriate Data Row(s)

					Dim SelectedRows As RenaissanceDataClass.DSBestMarketPrice.tblBestMarketPriceRow()
					SelectedRows = DStblBestMarketPrice.tblBestMarketPrice.Select("MarketInstrumentID=" & ItemID.ToString)
					If (SelectedRows Is Nothing) OrElse (SelectedRows.Length <= 0) Then
						Return 0
					End If

					' Return Value.

					Return SelectedRows(0).PriceValue

				Catch ex As Exception
					Return 0
				End Try

			Case PortfolioItemType.VeniceInstrument

				Try

					' Get Best price for the given Instrument.

					Dim PriceQuery As New SqlCommand
					Dim RVal As Object

					Try

						PriceQuery.CommandType = CommandType.Text
						PriceQuery.CommandText = "SELECT dbo.fn_SingleInstrumentBestPrice(" & ItemID.ToString & ", '" & KNOWLEDGEDATE_NOW.ToString(QUERY_SHORTDATEFORMAT) & "', 0, '" & MainForm.Main_Knowledgedate.ToString(QUERY_SHORTDATEFORMAT) & "')"
						PriceQuery.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT
						PriceQuery.Connection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)

						SyncLock PriceQuery.Connection
							RVal = PriceQuery.ExecuteScalar
						End SyncLock

					Catch ex As Exception
						Return 0
					Finally
						PriceQuery.Connection = Nothing
					End Try

					If (RVal Is Nothing) OrElse (Not IsNumeric(RVal)) Then
						Return 0
					End If

					Return CDbl(RVal)

				Catch ex As Exception
					Return 0
				End Try

      Case PortfolioItemType.Portfolio
        ' Lots to do here LATER.
        ' Return 0 for now

        Return 0

      Case Else

        Return 0

    End Select

    Return 0

  End Function

    ''' <summary>
    ''' Gets the portfolio item FX.
    ''' </summary>
    ''' <param name="ItemType">Type of the item.</param>
    ''' <param name="ItemID">The item ID.</param>
    ''' <returns>System.Double.</returns>
  Private Function GetPortfolioItemFX(ByVal ItemType As RenaissanceGlobals.PortfolioItemType, ByVal ItemID As Integer) As Double
    ' DStblBestFX
    Dim CurrencyID As Integer
    CurrencyID = RenaissanceGlobals.Currency.USD ' Default

    Select Case ItemType
      Case PortfolioItemType.MarketInstrument

        Try
          ' Get FXs
          If (DStblBestFX Is Nothing) Then
						DStblBestFX = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblBestFX, False), RenaissanceDataClass.DSBestFX)
					End If

					If (DStblBestFX Is Nothing) Then
						' Some kind of error ?
						Return 1
					End If

					' Get MarketInstrument Currency ID
					If (DStblMarketInstruments Is Nothing) Then
						DStblMarketInstruments = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblMarketInstruments, False), RenaissanceDataClass.DSMarketInstruments)
					End If

          If (DStblMarketInstruments Is Nothing) Then
            ' Some kind of error ?
            Return 1
          End If

          ' Select appropriate Data Row(s)
					Dim InstrumentRows As RenaissanceDataClass.DSMarketInstruments.tblMarketInstrumentsRow()
					InstrumentRows = DStblMarketInstruments.tblMarketInstruments.Select("MarketInstrumentID=" & ItemID.ToString)

					If InstrumentRows.Length > 0 Then
						CurrencyID = InstrumentRows(0).InstrumentCurrencyID
					End If

					If (CurrencyID = RenaissanceGlobals.Currency.USD) Then
						Return 1
					Else
						Dim FXRows As RenaissanceDataClass.DSBestFX.tblBestFXRow()
						FXRows = DStblBestFX.tblBestFX.Select("FXCurrencyCode=" & CurrencyID.ToString)

						If (FXRows Is Nothing) OrElse (FXRows.Length <= 0) Then
							Return 1
						End If

						Return FXRows(0).FXRate
					End If

        Catch ex As Exception
          Return 1
        End Try

      Case PortfolioItemType.VeniceInstrument

        Try
          ' First Get FXs
          If (DStblBestFX Is Nothing) Then
						DStblBestFX = MainForm.Load_Table(RenaissanceStandardDatasets.tblBestFX, False)
          End If

          If (DStblBestFX Is Nothing) Then
            ' Some kind of error ?
            Return 1
          End If

          ' Get Instrument Currency ID
          If (DStblInstrument Is Nothing) Then
						DStblInstrument = MainForm.Load_Table(RenaissanceStandardDatasets.tblInstrument, False)
          End If

          If (DStblInstrument Is Nothing) Then
            ' Some kind of error ?
            Return 1
          End If

          ' Select appropriate Data Row(s)
					Dim InstrumentRows As RenaissanceDataClass.DSInstrument.tblInstrumentRow()
					InstrumentRows = DStblInstrument.tblInstrument.Select("InstrumentID= " & ItemID.ToString)

					If InstrumentRows.Length > 0 Then
						CurrencyID = InstrumentRows(0).InstrumentCurrencyID
					End If

					If (CurrencyID = RenaissanceGlobals.Currency.USD) Then
						Return 1
					Else
						Dim FXRows As RenaissanceDataClass.DSBestFX.tblBestFXRow()
						FXRows = DStblBestFX.tblBestFX.Select("FXCurrencyCode=" & CurrencyID.ToString)

						If (FXRows Is Nothing) OrElse (FXRows.Length <= 0) Then
							Return 1
						End If

						Return FXRows(0).FXRate
					End If

        Catch ex As Exception
          Return 1
        End Try

      Case PortfolioItemType.Portfolio
        ' Lots to do here LATER.
        ' Return 1 for now

        Return 1

      Case Else

        Return 1

    End Select

    Return 1
  End Function

    ''' <summary>
    ''' Refreshes the grid prices.
    ''' </summary>
    ''' <param name="ItemType">Type of the item.</param>
  Private Sub RefreshGridPrices(ByVal ItemType As RenaissanceGlobals.PortfolioItemType)
    ' **********************************************************************************
    ' 
    '
    ' **********************************************************************************

    Dim GridCounter As Integer

    For GridCounter = 1 To (Me.Grid_PortfolioData.Rows.Count - 1)
      Try
        If (CType(Grid_PortfolioData.Item(GridCounter, Grid_Type), PortfolioItemType) = ItemType) Or (ItemType = PortfolioItemType.Default) Then
          Grid_PortfolioData.Item(GridCounter, Grid_Price) = GetPortfolioItemPrice(ItemType, CInt(Grid_PortfolioData.Item(GridCounter, Grid_Instrument)))
        End If
      Catch ex As Exception
      End Try
    Next
  End Sub

    ''' <summary>
    ''' Refreshes the grid portfolio weights.
    ''' </summary>
  Private Sub RefreshGridPortfolioWeights()
    ' **********************************************************************************
    ' 
    '
    ' **********************************************************************************

    Dim GridCounter As Integer
    Dim SumGrossItemValue As Double
    Dim SumNetItemValue As Double
    Dim SumGrossItemWeight As Double
    Dim SumNetItemWeight As Double
    Dim GrossedUpPortfolioValue As Double

    Dim ItemWeight As Double

    ' First Get Portfolio Values and weights
    SumGrossItemWeight = 0
    SumNetItemWeight = 0
    SumGrossItemValue = 0
    SumNetItemValue = 0

    Try
      For GridCounter = 1 To (Me.Grid_PortfolioData.Rows.Count - 2)
        If CDbl(Grid_PortfolioData.Item(GridCounter, Grid_Weight)) = 0 Then
          SumGrossItemValue += Math.Abs(CDbl(Grid_PortfolioData.Item(GridCounter, Grid_USDValue)))
          SumNetItemValue += CDbl(Grid_PortfolioData.Item(GridCounter, Grid_USDValue))
        Else
          SumGrossItemWeight += Math.Abs(CDbl(Grid_PortfolioData.Item(GridCounter, Grid_Weight)))
          SumNetItemWeight += CDbl(Grid_PortfolioData.Item(GridCounter, Grid_Weight))
        End If
      Next
    Catch ex As Exception
    End Try

    ' Try to extrapolate PortfolioValue
    GrossedUpPortfolioValue = 0
    Try
      If SumNetItemWeight = 0 Then
        GrossedUpPortfolioValue = SumNetItemValue
      ElseIf (1 - SumNetItemWeight) <> 0 Then
        GrossedUpPortfolioValue = SumNetItemValue / (1 - SumNetItemWeight)
      End If
    Catch ex As Exception
    End Try

    ' Set Portfolio Weights
    For GridCounter = 1 To (Me.Grid_PortfolioData.Rows.Count - 2)
      Try
        If CDbl(Grid_PortfolioData.Item(GridCounter, Grid_Weight)) = 0 Then
          ItemWeight = 0
          If Math.Abs(GrossedUpPortfolioValue) >= 1 Then
            ItemWeight = CDbl(Grid_PortfolioData.Item(GridCounter, Grid_USDValue)) / GrossedUpPortfolioValue

            If (ItemWeight > 1) Then
              ItemWeight = 1
            ElseIf (ItemWeight < (-1)) Then
              ItemWeight = (-1)
            End If
          End If
          Grid_PortfolioData.Item(GridCounter, Grid_PortfolioWeight) = ItemWeight
        Else
          Grid_PortfolioData.Item(GridCounter, Grid_PortfolioWeight) = Grid_PortfolioData.Item(GridCounter, Grid_Weight)
        End If
      Catch ex As Exception
      End Try
    Next

  End Sub


#End Region

  
End Class
