' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmDataTask.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.IO
Imports System.Data.SqlClient
Imports System.Xml
Imports System.Xml.Serialization
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals

Imports NaplesGlobals
Imports RenaissanceControls

''' <summary>
''' Class frmDataTask
''' </summary>
Public Class frmDataTask

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmDataTask"/> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The LBL task description
    ''' </summary>
  Friend WithEvents lblTaskDescription As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit audit ID
    ''' </summary>
  Friend WithEvents editAuditID As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The edit task description
    ''' </summary>
  Friend WithEvents editTaskDescription As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The BTN nav first
    ''' </summary>
  Friend WithEvents btnNavFirst As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN nav prev
    ''' </summary>
  Friend WithEvents btnNavPrev As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN nav next
    ''' </summary>
  Friend WithEvents btnNavNext As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN last
    ''' </summary>
  Friend WithEvents btnLast As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN add
    ''' </summary>
  Friend WithEvents btnAdd As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN delete
    ''' </summary>
  Friend WithEvents btnDelete As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN cancel
    ''' </summary>
  Friend WithEvents btnCancel As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN save
    ''' </summary>
  Friend WithEvents btnSave As System.Windows.Forms.Button
    ''' <summary>
    ''' The combo_ select data task code
    ''' </summary>
  Friend WithEvents Combo_SelectDataTaskCode As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The panel1
    ''' </summary>
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The label1
    ''' </summary>
  Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The group box1
    ''' </summary>
  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The BTN close
    ''' </summary>
  Friend WithEvents btnClose As System.Windows.Forms.Button
    ''' <summary>
    ''' The label3
    ''' </summary>
  Friend WithEvents Label3 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ task type
    ''' </summary>
  Friend WithEvents Combo_TaskType As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label2
    ''' </summary>
  Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ fields
    ''' </summary>
  Friend WithEvents Combo_Fields As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label4
    ''' </summary>
  Friend WithEvents Label4 As System.Windows.Forms.Label
    ''' <summary>
    ''' The panel_ scheduling
    ''' </summary>
  Friend WithEvents Panel_Scheduling As System.Windows.Forms.Panel
    ''' <summary>
    ''' The label5
    ''' </summary>
  Friend WithEvents Label5 As System.Windows.Forms.Label
    ''' <summary>
    ''' The check box_ sun
    ''' </summary>
  Friend WithEvents CheckBox_Sun As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check box_ sat
    ''' </summary>
  Friend WithEvents CheckBox_Sat As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check box_ fri
    ''' </summary>
  Friend WithEvents CheckBox_Fri As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check box_ thu
    ''' </summary>
  Friend WithEvents CheckBox_Thu As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check box_ wed
    ''' </summary>
  Friend WithEvents CheckBox_Wed As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check box_ tue
    ''' </summary>
  Friend WithEvents CheckBox_Tue As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check box_ mon
    ''' </summary>
  Friend WithEvents CheckBox_Mon As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The label7
    ''' </summary>
  Friend WithEvents Label7 As System.Windows.Forms.Label
    ''' <summary>
    ''' The D t_ occurs once
    ''' </summary>
  Friend WithEvents DT_OccursOnce As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The radio_ occurs every
    ''' </summary>
  Friend WithEvents Radio_OccursEvery As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The radio_ occurs once
    ''' </summary>
  Friend WithEvents Radio_OccursOnce As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The label8
    ''' </summary>
  Friend WithEvents Label8 As System.Windows.Forms.Label
    ''' <summary>
    ''' The D t_ time starts
    ''' </summary>
  Friend WithEvents DT_TimeStarts As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The label9
    ''' </summary>
  Friend WithEvents Label9 As System.Windows.Forms.Label
    ''' <summary>
    ''' The D t_ time ends
    ''' </summary>
  Friend WithEvents DT_TimeEnds As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The combo_ ticker
    ''' </summary>
  Friend WithEvents Combo_Ticker As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The root menu
    ''' </summary>
  Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
    ''' <summary>
    ''' The check_ aggregate notification emails
    ''' </summary>
  Friend WithEvents Check_AggregateNotificationEmails As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The tab control_ data task
    ''' </summary>
  Friend WithEvents TabControl_DataTask As System.Windows.Forms.TabControl
    ''' <summary>
    ''' The tab page_ scheduling
    ''' </summary>
  Friend WithEvents TabPage_Scheduling As System.Windows.Forms.TabPage
    ''' <summary>
    ''' The tab page_ email addresses
    ''' </summary>
  Friend WithEvents TabPage_EmailAddresses As System.Windows.Forms.TabPage
    ''' <summary>
    ''' The panel_ email addresses
    ''' </summary>
  Friend WithEvents Panel_EmailAddresses As System.Windows.Forms.Panel
    ''' <summary>
    ''' The list box_ email people
    ''' </summary>
  Friend WithEvents ListBox_EmailPeople As System.Windows.Forms.CheckedListBox
    ''' <summary>
    ''' The tab page_ XML
    ''' </summary>
  Friend WithEvents TabPage_XML As System.Windows.Forms.TabPage
    ''' <summary>
    ''' The edit XML
    ''' </summary>
  Friend WithEvents editXML As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The edit_ price move trigger
    ''' </summary>
  Friend WithEvents edit_PriceMoveTrigger As RenaissanceControls.PercentageTextBox
    ''' <summary>
    ''' The label6
    ''' </summary>
	Friend WithEvents Label6 As System.Windows.Forms.Label
    ''' <summary>
    ''' The check_ portfolio monitor items
    ''' </summary>
  Friend WithEvents Check_PortfolioMonitorItems As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The text_ task comment
    ''' </summary>
  Friend WithEvents Text_TaskComment As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The label10
    ''' </summary>
  Friend WithEvents Label10 As System.Windows.Forms.Label
    ''' <summary>
    ''' The numeric_ minute count
    ''' </summary>
  Friend WithEvents Numeric_MinuteCount As System.Windows.Forms.NumericUpDown
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.lblTaskDescription = New System.Windows.Forms.Label
    Me.editAuditID = New System.Windows.Forms.TextBox
    Me.editTaskDescription = New System.Windows.Forms.TextBox
    Me.btnNavFirst = New System.Windows.Forms.Button
    Me.btnNavPrev = New System.Windows.Forms.Button
    Me.btnNavNext = New System.Windows.Forms.Button
    Me.btnLast = New System.Windows.Forms.Button
    Me.btnAdd = New System.Windows.Forms.Button
    Me.btnDelete = New System.Windows.Forms.Button
    Me.btnCancel = New System.Windows.Forms.Button
    Me.btnSave = New System.Windows.Forms.Button
    Me.Combo_SelectDataTaskCode = New System.Windows.Forms.ComboBox
    Me.Panel1 = New System.Windows.Forms.Panel
    Me.Label1 = New System.Windows.Forms.Label
    Me.GroupBox1 = New System.Windows.Forms.GroupBox
    Me.btnClose = New System.Windows.Forms.Button
    Me.Combo_TaskType = New System.Windows.Forms.ComboBox
    Me.Label3 = New System.Windows.Forms.Label
    Me.Combo_Ticker = New System.Windows.Forms.ComboBox
    Me.Label2 = New System.Windows.Forms.Label
    Me.Combo_Fields = New System.Windows.Forms.ComboBox
    Me.Label4 = New System.Windows.Forms.Label
    Me.Panel_Scheduling = New System.Windows.Forms.Panel
    Me.Label9 = New System.Windows.Forms.Label
    Me.DT_TimeEnds = New System.Windows.Forms.DateTimePicker
    Me.Label8 = New System.Windows.Forms.Label
    Me.DT_TimeStarts = New System.Windows.Forms.DateTimePicker
    Me.Label7 = New System.Windows.Forms.Label
    Me.Numeric_MinuteCount = New System.Windows.Forms.NumericUpDown
    Me.DT_OccursOnce = New System.Windows.Forms.DateTimePicker
    Me.Radio_OccursEvery = New System.Windows.Forms.RadioButton
    Me.Radio_OccursOnce = New System.Windows.Forms.RadioButton
    Me.CheckBox_Sun = New System.Windows.Forms.CheckBox
    Me.CheckBox_Sat = New System.Windows.Forms.CheckBox
    Me.CheckBox_Fri = New System.Windows.Forms.CheckBox
    Me.CheckBox_Thu = New System.Windows.Forms.CheckBox
    Me.CheckBox_Wed = New System.Windows.Forms.CheckBox
    Me.CheckBox_Tue = New System.Windows.Forms.CheckBox
    Me.CheckBox_Mon = New System.Windows.Forms.CheckBox
    Me.Label5 = New System.Windows.Forms.Label
    Me.RootMenu = New System.Windows.Forms.MenuStrip
    Me.Check_AggregateNotificationEmails = New System.Windows.Forms.CheckBox
    Me.TabControl_DataTask = New System.Windows.Forms.TabControl
    Me.TabPage_Scheduling = New System.Windows.Forms.TabPage
    Me.TabPage_EmailAddresses = New System.Windows.Forms.TabPage
    Me.Panel_EmailAddresses = New System.Windows.Forms.Panel
    Me.ListBox_EmailPeople = New System.Windows.Forms.CheckedListBox
    Me.TabPage_XML = New System.Windows.Forms.TabPage
    Me.editXML = New System.Windows.Forms.TextBox
    Me.edit_PriceMoveTrigger = New RenaissanceControls.PercentageTextBox
    Me.Label6 = New System.Windows.Forms.Label
    Me.Check_PortfolioMonitorItems = New System.Windows.Forms.CheckBox
    Me.Text_TaskComment = New System.Windows.Forms.TextBox
    Me.Label10 = New System.Windows.Forms.Label
    Me.Panel1.SuspendLayout()
    Me.Panel_Scheduling.SuspendLayout()
    CType(Me.Numeric_MinuteCount, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.TabControl_DataTask.SuspendLayout()
    Me.TabPage_Scheduling.SuspendLayout()
    Me.TabPage_EmailAddresses.SuspendLayout()
    Me.Panel_EmailAddresses.SuspendLayout()
    Me.TabPage_XML.SuspendLayout()
    Me.SuspendLayout()
    '
    'lblTaskDescription
    '
    Me.lblTaskDescription.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.lblTaskDescription.Location = New System.Drawing.Point(8, 75)
    Me.lblTaskDescription.Name = "lblTaskDescription"
    Me.lblTaskDescription.Size = New System.Drawing.Size(100, 23)
    Me.lblTaskDescription.TabIndex = 40
    Me.lblTaskDescription.Text = "Description"
    Me.lblTaskDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'editAuditID
    '
    Me.editAuditID.Enabled = False
    Me.editAuditID.Location = New System.Drawing.Point(308, 32)
    Me.editAuditID.Name = "editAuditID"
    Me.editAuditID.Size = New System.Drawing.Size(64, 20)
    Me.editAuditID.TabIndex = 1
    '
    'editTaskDescription
    '
    Me.editTaskDescription.Location = New System.Drawing.Point(120, 70)
    Me.editTaskDescription.Name = "editTaskDescription"
    Me.editTaskDescription.Size = New System.Drawing.Size(252, 20)
    Me.editTaskDescription.TabIndex = 2
    '
    'btnNavFirst
    '
    Me.btnNavFirst.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNavFirst.Location = New System.Drawing.Point(8, 8)
    Me.btnNavFirst.Name = "btnNavFirst"
    Me.btnNavFirst.Size = New System.Drawing.Size(40, 28)
    Me.btnNavFirst.TabIndex = 0
    Me.btnNavFirst.Text = "<<"
    '
    'btnNavPrev
    '
    Me.btnNavPrev.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNavPrev.Location = New System.Drawing.Point(49, 8)
    Me.btnNavPrev.Name = "btnNavPrev"
    Me.btnNavPrev.Size = New System.Drawing.Size(35, 28)
    Me.btnNavPrev.TabIndex = 1
    Me.btnNavPrev.Text = "<"
    '
    'btnNavNext
    '
    Me.btnNavNext.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNavNext.Location = New System.Drawing.Point(88, 8)
    Me.btnNavNext.Name = "btnNavNext"
    Me.btnNavNext.Size = New System.Drawing.Size(35, 28)
    Me.btnNavNext.TabIndex = 2
    Me.btnNavNext.Text = ">"
    '
    'btnLast
    '
    Me.btnLast.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnLast.Location = New System.Drawing.Point(124, 8)
    Me.btnLast.Name = "btnLast"
    Me.btnLast.Size = New System.Drawing.Size(40, 28)
    Me.btnLast.TabIndex = 3
    Me.btnLast.Text = ">>"
    '
    'btnAdd
    '
    Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnAdd.Location = New System.Drawing.Point(176, 8)
    Me.btnAdd.Name = "btnAdd"
    Me.btnAdd.Size = New System.Drawing.Size(75, 28)
    Me.btnAdd.TabIndex = 4
    Me.btnAdd.Text = "&New"
    '
    'btnDelete
    '
    Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnDelete.Location = New System.Drawing.Point(123, 572)
    Me.btnDelete.Name = "btnDelete"
    Me.btnDelete.Size = New System.Drawing.Size(75, 28)
    Me.btnDelete.TabIndex = 13
    Me.btnDelete.Text = "&Delete"
    '
    'btnCancel
    '
    Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnCancel.Location = New System.Drawing.Point(207, 572)
    Me.btnCancel.Name = "btnCancel"
    Me.btnCancel.Size = New System.Drawing.Size(75, 28)
    Me.btnCancel.TabIndex = 14
    Me.btnCancel.Text = "&Cancel"
    '
    'btnSave
    '
    Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnSave.Location = New System.Drawing.Point(35, 572)
    Me.btnSave.Name = "btnSave"
    Me.btnSave.Size = New System.Drawing.Size(75, 28)
    Me.btnSave.TabIndex = 12
    Me.btnSave.Text = "&Save"
    '
    'Combo_SelectDataTaskCode
    '
    Me.Combo_SelectDataTaskCode.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectDataTaskCode.Location = New System.Drawing.Point(120, 32)
    Me.Combo_SelectDataTaskCode.Name = "Combo_SelectDataTaskCode"
    Me.Combo_SelectDataTaskCode.Size = New System.Drawing.Size(180, 21)
    Me.Combo_SelectDataTaskCode.TabIndex = 0
    '
    'Panel1
    '
    Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Panel1.Controls.Add(Me.btnNavFirst)
    Me.Panel1.Controls.Add(Me.btnNavPrev)
    Me.Panel1.Controls.Add(Me.btnNavNext)
    Me.Panel1.Controls.Add(Me.btnLast)
    Me.Panel1.Controls.Add(Me.btnAdd)
    Me.Panel1.Location = New System.Drawing.Point(71, 518)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(260, 48)
    Me.Panel1.TabIndex = 11
    '
    'Label1
    '
    Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label1.Location = New System.Drawing.Point(8, 32)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(100, 23)
    Me.Label1.TabIndex = 17
    Me.Label1.Text = "Select"
    Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'GroupBox1
    '
    Me.GroupBox1.Location = New System.Drawing.Point(8, 59)
    Me.GroupBox1.Name = "GroupBox1"
    Me.GroupBox1.Size = New System.Drawing.Size(364, 4)
    Me.GroupBox1.TabIndex = 77
    Me.GroupBox1.TabStop = False
    Me.GroupBox1.Text = "GroupBox1"
    '
    'btnClose
    '
    Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnClose.Location = New System.Drawing.Point(291, 572)
    Me.btnClose.Name = "btnClose"
    Me.btnClose.Size = New System.Drawing.Size(75, 28)
    Me.btnClose.TabIndex = 15
    Me.btnClose.Text = "&Close"
    '
    'Combo_TaskType
    '
    Me.Combo_TaskType.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_TaskType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_TaskType.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_TaskType.Location = New System.Drawing.Point(120, 97)
    Me.Combo_TaskType.Name = "Combo_TaskType"
    Me.Combo_TaskType.Size = New System.Drawing.Size(269, 21)
    Me.Combo_TaskType.TabIndex = 3
    '
    'Label3
    '
    Me.Label3.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label3.Location = New System.Drawing.Point(8, 102)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(108, 20)
    Me.Label3.TabIndex = 85
    Me.Label3.Text = "Task Type"
    Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'Combo_Ticker
    '
    Me.Combo_Ticker.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Ticker.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Ticker.Location = New System.Drawing.Point(120, 124)
    Me.Combo_Ticker.Name = "Combo_Ticker"
    Me.Combo_Ticker.Size = New System.Drawing.Size(269, 21)
    Me.Combo_Ticker.TabIndex = 4
    '
    'Label2
    '
    Me.Label2.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label2.Location = New System.Drawing.Point(8, 129)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(108, 20)
    Me.Label2.TabIndex = 87
    Me.Label2.Text = "Ticker / Portfolio"
    Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'Combo_Fields
    '
    Me.Combo_Fields.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Fields.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Fields.Location = New System.Drawing.Point(120, 151)
    Me.Combo_Fields.Name = "Combo_Fields"
    Me.Combo_Fields.Size = New System.Drawing.Size(269, 21)
    Me.Combo_Fields.TabIndex = 5
    '
    'Label4
    '
    Me.Label4.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label4.Location = New System.Drawing.Point(8, 156)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(108, 20)
    Me.Label4.TabIndex = 89
    Me.Label4.Text = "Fields"
    Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'Panel_Scheduling
    '
    Me.Panel_Scheduling.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Panel_Scheduling.Controls.Add(Me.Label9)
    Me.Panel_Scheduling.Controls.Add(Me.DT_TimeEnds)
    Me.Panel_Scheduling.Controls.Add(Me.Label8)
    Me.Panel_Scheduling.Controls.Add(Me.DT_TimeStarts)
    Me.Panel_Scheduling.Controls.Add(Me.Label7)
    Me.Panel_Scheduling.Controls.Add(Me.Numeric_MinuteCount)
    Me.Panel_Scheduling.Controls.Add(Me.DT_OccursOnce)
    Me.Panel_Scheduling.Controls.Add(Me.Radio_OccursEvery)
    Me.Panel_Scheduling.Controls.Add(Me.Radio_OccursOnce)
    Me.Panel_Scheduling.Controls.Add(Me.CheckBox_Sun)
    Me.Panel_Scheduling.Controls.Add(Me.CheckBox_Sat)
    Me.Panel_Scheduling.Controls.Add(Me.CheckBox_Fri)
    Me.Panel_Scheduling.Controls.Add(Me.CheckBox_Thu)
    Me.Panel_Scheduling.Controls.Add(Me.CheckBox_Wed)
    Me.Panel_Scheduling.Controls.Add(Me.CheckBox_Tue)
    Me.Panel_Scheduling.Controls.Add(Me.CheckBox_Mon)
    Me.Panel_Scheduling.Controls.Add(Me.Label5)
    Me.Panel_Scheduling.Location = New System.Drawing.Point(2, 2)
    Me.Panel_Scheduling.Name = "Panel_Scheduling"
    Me.Panel_Scheduling.Size = New System.Drawing.Size(372, 180)
    Me.Panel_Scheduling.TabIndex = 0
    '
    'Label9
    '
    Me.Label9.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label9.Location = New System.Drawing.Point(48, 144)
    Me.Label9.Name = "Label9"
    Me.Label9.Size = New System.Drawing.Size(60, 16)
    Me.Label9.TabIndex = 15
    Me.Label9.Text = "Ending"
    Me.Label9.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'DT_TimeEnds
    '
    Me.DT_TimeEnds.CustomFormat = ""
    Me.DT_TimeEnds.Format = System.Windows.Forms.DateTimePickerFormat.Time
    Me.DT_TimeEnds.Location = New System.Drawing.Point(112, 140)
    Me.DT_TimeEnds.Name = "DT_TimeEnds"
    Me.DT_TimeEnds.ShowUpDown = True
    Me.DT_TimeEnds.Size = New System.Drawing.Size(88, 20)
    Me.DT_TimeEnds.TabIndex = 16
    '
    'Label8
    '
    Me.Label8.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label8.Location = New System.Drawing.Point(48, 116)
    Me.Label8.Name = "Label8"
    Me.Label8.Size = New System.Drawing.Size(60, 16)
    Me.Label8.TabIndex = 13
    Me.Label8.Text = "Starting"
    Me.Label8.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'DT_TimeStarts
    '
    Me.DT_TimeStarts.CustomFormat = ""
    Me.DT_TimeStarts.Format = System.Windows.Forms.DateTimePickerFormat.Time
    Me.DT_TimeStarts.Location = New System.Drawing.Point(112, 112)
    Me.DT_TimeStarts.Name = "DT_TimeStarts"
    Me.DT_TimeStarts.ShowUpDown = True
    Me.DT_TimeStarts.Size = New System.Drawing.Size(88, 20)
    Me.DT_TimeStarts.TabIndex = 14
    '
    'Label7
    '
    Me.Label7.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label7.Location = New System.Drawing.Point(208, 88)
    Me.Label7.Name = "Label7"
    Me.Label7.Size = New System.Drawing.Size(40, 16)
    Me.Label7.TabIndex = 12
    Me.Label7.Text = "minutes"
    '
    'Numeric_MinuteCount
    '
    Me.Numeric_MinuteCount.Location = New System.Drawing.Point(112, 84)
    Me.Numeric_MinuteCount.Maximum = New Decimal(New Integer() {3600, 0, 0, 0})
    Me.Numeric_MinuteCount.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
    Me.Numeric_MinuteCount.Name = "Numeric_MinuteCount"
    Me.Numeric_MinuteCount.Size = New System.Drawing.Size(88, 20)
    Me.Numeric_MinuteCount.TabIndex = 11
    Me.Numeric_MinuteCount.Value = New Decimal(New Integer() {1, 0, 0, 0})
    '
    'DT_OccursOnce
    '
    Me.DT_OccursOnce.CustomFormat = ""
    Me.DT_OccursOnce.Format = System.Windows.Forms.DateTimePickerFormat.Time
    Me.DT_OccursOnce.Location = New System.Drawing.Point(112, 56)
    Me.DT_OccursOnce.Name = "DT_OccursOnce"
    Me.DT_OccursOnce.ShowUpDown = True
    Me.DT_OccursOnce.Size = New System.Drawing.Size(88, 20)
    Me.DT_OccursOnce.TabIndex = 9
    '
    'Radio_OccursEvery
    '
    Me.Radio_OccursEvery.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Radio_OccursEvery.Location = New System.Drawing.Point(8, 88)
    Me.Radio_OccursEvery.Name = "Radio_OccursEvery"
    Me.Radio_OccursEvery.Size = New System.Drawing.Size(96, 16)
    Me.Radio_OccursEvery.TabIndex = 10
    Me.Radio_OccursEvery.Text = "Occurs every"
    '
    'Radio_OccursOnce
    '
    Me.Radio_OccursOnce.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Radio_OccursOnce.Location = New System.Drawing.Point(8, 60)
    Me.Radio_OccursOnce.Name = "Radio_OccursOnce"
    Me.Radio_OccursOnce.Size = New System.Drawing.Size(100, 16)
    Me.Radio_OccursOnce.TabIndex = 8
    Me.Radio_OccursOnce.Text = "Occurs once at "
    '
    'CheckBox_Sun
    '
    Me.CheckBox_Sun.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.CheckBox_Sun.Location = New System.Drawing.Point(316, 28)
    Me.CheckBox_Sun.Name = "CheckBox_Sun"
    Me.CheckBox_Sun.Size = New System.Drawing.Size(44, 20)
    Me.CheckBox_Sun.TabIndex = 7
    Me.CheckBox_Sun.Text = "Sun"
    '
    'CheckBox_Sat
    '
    Me.CheckBox_Sat.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.CheckBox_Sat.Location = New System.Drawing.Point(264, 28)
    Me.CheckBox_Sat.Name = "CheckBox_Sat"
    Me.CheckBox_Sat.Size = New System.Drawing.Size(48, 20)
    Me.CheckBox_Sat.TabIndex = 6
    Me.CheckBox_Sat.Text = "Sat"
    '
    'CheckBox_Fri
    '
    Me.CheckBox_Fri.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.CheckBox_Fri.Location = New System.Drawing.Point(212, 28)
    Me.CheckBox_Fri.Name = "CheckBox_Fri"
    Me.CheckBox_Fri.Size = New System.Drawing.Size(48, 20)
    Me.CheckBox_Fri.TabIndex = 5
    Me.CheckBox_Fri.Text = "Fri"
    '
    'CheckBox_Thu
    '
    Me.CheckBox_Thu.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.CheckBox_Thu.Location = New System.Drawing.Point(160, 28)
    Me.CheckBox_Thu.Name = "CheckBox_Thu"
    Me.CheckBox_Thu.Size = New System.Drawing.Size(48, 20)
    Me.CheckBox_Thu.TabIndex = 4
    Me.CheckBox_Thu.Text = "Thu"
    '
    'CheckBox_Wed
    '
    Me.CheckBox_Wed.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.CheckBox_Wed.Location = New System.Drawing.Point(108, 28)
    Me.CheckBox_Wed.Name = "CheckBox_Wed"
    Me.CheckBox_Wed.Size = New System.Drawing.Size(48, 20)
    Me.CheckBox_Wed.TabIndex = 3
    Me.CheckBox_Wed.Text = "Wed"
    '
    'CheckBox_Tue
    '
    Me.CheckBox_Tue.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.CheckBox_Tue.Location = New System.Drawing.Point(56, 28)
    Me.CheckBox_Tue.Name = "CheckBox_Tue"
    Me.CheckBox_Tue.Size = New System.Drawing.Size(48, 20)
    Me.CheckBox_Tue.TabIndex = 2
    Me.CheckBox_Tue.Text = "Tue"
    '
    'CheckBox_Mon
    '
    Me.CheckBox_Mon.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.CheckBox_Mon.Location = New System.Drawing.Point(4, 28)
    Me.CheckBox_Mon.Name = "CheckBox_Mon"
    Me.CheckBox_Mon.Size = New System.Drawing.Size(48, 20)
    Me.CheckBox_Mon.TabIndex = 1
    Me.CheckBox_Mon.Text = "Mon"
    '
    'Label5
    '
    Me.Label5.Location = New System.Drawing.Point(5, 3)
    Me.Label5.Name = "Label5"
    Me.Label5.Size = New System.Drawing.Size(232, 20)
    Me.Label5.TabIndex = 0
    Me.Label5.Text = "Scheduling"
    Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'RootMenu
    '
    Me.RootMenu.Location = New System.Drawing.Point(0, 0)
    Me.RootMenu.Name = "RootMenu"
    Me.RootMenu.Size = New System.Drawing.Size(397, 24)
    Me.RootMenu.TabIndex = 16
    Me.RootMenu.Text = " "
    '
    'Check_AggregateNotificationEmails
    '
    Me.Check_AggregateNotificationEmails.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_AggregateNotificationEmails.Location = New System.Drawing.Point(8, 203)
    Me.Check_AggregateNotificationEmails.Name = "Check_AggregateNotificationEmails"
    Me.Check_AggregateNotificationEmails.Size = New System.Drawing.Size(360, 20)
    Me.Check_AggregateNotificationEmails.TabIndex = 7
    Me.Check_AggregateNotificationEmails.Text = "Aggregate Notification Emails to a scheduled time."
    '
    'TabControl_DataTask
    '
    Me.TabControl_DataTask.Controls.Add(Me.TabPage_Scheduling)
    Me.TabControl_DataTask.Controls.Add(Me.TabPage_EmailAddresses)
    Me.TabControl_DataTask.Controls.Add(Me.TabPage_XML)
    Me.TabControl_DataTask.Location = New System.Drawing.Point(8, 247)
    Me.TabControl_DataTask.Name = "TabControl_DataTask"
    Me.TabControl_DataTask.SelectedIndex = 0
    Me.TabControl_DataTask.Size = New System.Drawing.Size(384, 211)
    Me.TabControl_DataTask.TabIndex = 9
    '
    'TabPage_Scheduling
    '
    Me.TabPage_Scheduling.Controls.Add(Me.Panel_Scheduling)
    Me.TabPage_Scheduling.Location = New System.Drawing.Point(4, 22)
    Me.TabPage_Scheduling.Name = "TabPage_Scheduling"
    Me.TabPage_Scheduling.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage_Scheduling.Size = New System.Drawing.Size(376, 185)
    Me.TabPage_Scheduling.TabIndex = 0
    Me.TabPage_Scheduling.Text = "Scheduling"
    Me.TabPage_Scheduling.UseVisualStyleBackColor = True
    '
    'TabPage_EmailAddresses
    '
    Me.TabPage_EmailAddresses.Controls.Add(Me.Panel_EmailAddresses)
    Me.TabPage_EmailAddresses.Location = New System.Drawing.Point(4, 22)
    Me.TabPage_EmailAddresses.Name = "TabPage_EmailAddresses"
    Me.TabPage_EmailAddresses.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage_EmailAddresses.Size = New System.Drawing.Size(376, 185)
    Me.TabPage_EmailAddresses.TabIndex = 1
    Me.TabPage_EmailAddresses.Text = "Email Addresses"
    Me.TabPage_EmailAddresses.UseVisualStyleBackColor = True
    '
    'Panel_EmailAddresses
    '
    Me.Panel_EmailAddresses.Controls.Add(Me.ListBox_EmailPeople)
    Me.Panel_EmailAddresses.Location = New System.Drawing.Point(2, 2)
    Me.Panel_EmailAddresses.Name = "Panel_EmailAddresses"
    Me.Panel_EmailAddresses.Size = New System.Drawing.Size(372, 181)
    Me.Panel_EmailAddresses.TabIndex = 0
    '
    'ListBox_EmailPeople
    '
    Me.ListBox_EmailPeople.CheckOnClick = True
    Me.ListBox_EmailPeople.FormattingEnabled = True
    Me.ListBox_EmailPeople.Location = New System.Drawing.Point(2, 2)
    Me.ListBox_EmailPeople.Name = "ListBox_EmailPeople"
    Me.ListBox_EmailPeople.ScrollAlwaysVisible = True
    Me.ListBox_EmailPeople.Size = New System.Drawing.Size(368, 169)
    Me.ListBox_EmailPeople.Sorted = True
    Me.ListBox_EmailPeople.TabIndex = 0
    '
    'TabPage_XML
    '
    Me.TabPage_XML.Controls.Add(Me.editXML)
    Me.TabPage_XML.Location = New System.Drawing.Point(4, 22)
    Me.TabPage_XML.Name = "TabPage_XML"
    Me.TabPage_XML.Size = New System.Drawing.Size(376, 185)
    Me.TabPage_XML.TabIndex = 2
    Me.TabPage_XML.Text = "XML"
    Me.TabPage_XML.UseVisualStyleBackColor = True
    '
    'editXML
    '
    Me.editXML.Location = New System.Drawing.Point(3, 3)
    Me.editXML.Multiline = True
    Me.editXML.Name = "editXML"
    Me.editXML.ScrollBars = System.Windows.Forms.ScrollBars.Both
    Me.editXML.Size = New System.Drawing.Size(370, 179)
    Me.editXML.TabIndex = 0
    Me.editXML.WordWrap = False
    '
    'edit_PriceMoveTrigger
    '
    Me.edit_PriceMoveTrigger.Location = New System.Drawing.Point(120, 178)
    Me.edit_PriceMoveTrigger.Name = "edit_PriceMoveTrigger"
    Me.edit_PriceMoveTrigger.RenaissanceTag = Nothing
    Me.edit_PriceMoveTrigger.Size = New System.Drawing.Size(252, 20)
    Me.edit_PriceMoveTrigger.TabIndex = 6
    Me.edit_PriceMoveTrigger.Text = "0.00%"
    Me.edit_PriceMoveTrigger.TextFormat = "#,##0.00%"
    Me.edit_PriceMoveTrigger.Value = 0
    '
    'Label6
    '
    Me.Label6.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label6.Location = New System.Drawing.Point(9, 182)
    Me.Label6.Name = "Label6"
    Me.Label6.Size = New System.Drawing.Size(108, 21)
    Me.Label6.TabIndex = 92
    Me.Label6.Text = "Price move Trigger"
    Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'Check_PortfolioMonitorItems
    '
    Me.Check_PortfolioMonitorItems.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_PortfolioMonitorItems.Location = New System.Drawing.Point(8, 223)
    Me.Check_PortfolioMonitorItems.Name = "Check_PortfolioMonitorItems"
    Me.Check_PortfolioMonitorItems.Size = New System.Drawing.Size(360, 20)
    Me.Check_PortfolioMonitorItems.TabIndex = 8
    Me.Check_PortfolioMonitorItems.Text = "Restrict Portfolio tracking to 'Monitor Items'"
    '
    'Text_TaskComment
    '
    Me.Text_TaskComment.Location = New System.Drawing.Point(120, 464)
    Me.Text_TaskComment.Multiline = True
    Me.Text_TaskComment.Name = "Text_TaskComment"
    Me.Text_TaskComment.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.Text_TaskComment.Size = New System.Drawing.Size(269, 45)
    Me.Text_TaskComment.TabIndex = 10
    '
    'Label10
    '
    Me.Label10.AutoSize = True
    Me.Label10.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label10.Location = New System.Drawing.Point(8, 469)
    Me.Label10.Name = "Label10"
    Me.Label10.Size = New System.Drawing.Size(78, 13)
    Me.Label10.TabIndex = 94
    Me.Label10.Text = "Task Comment"
    Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'frmDataTask
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.CancelButton = Me.btnCancel
    Me.ClientSize = New System.Drawing.Size(397, 605)
    Me.Controls.Add(Me.Text_TaskComment)
    Me.Controls.Add(Me.Label10)
    Me.Controls.Add(Me.Check_PortfolioMonitorItems)
    Me.Controls.Add(Me.Label6)
    Me.Controls.Add(Me.edit_PriceMoveTrigger)
    Me.Controls.Add(Me.TabControl_DataTask)
    Me.Controls.Add(Me.Check_AggregateNotificationEmails)
    Me.Controls.Add(Me.RootMenu)
    Me.Controls.Add(Me.Combo_Fields)
    Me.Controls.Add(Me.Label4)
    Me.Controls.Add(Me.Combo_Ticker)
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.Combo_TaskType)
    Me.Controls.Add(Me.Label3)
    Me.Controls.Add(Me.btnClose)
    Me.Controls.Add(Me.GroupBox1)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.Panel1)
    Me.Controls.Add(Me.Combo_SelectDataTaskCode)
    Me.Controls.Add(Me.btnSave)
    Me.Controls.Add(Me.editAuditID)
    Me.Controls.Add(Me.editTaskDescription)
    Me.Controls.Add(Me.lblTaskDescription)
    Me.Controls.Add(Me.btnDelete)
    Me.Controls.Add(Me.btnCancel)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frmDataTask"
    Me.Text = "Add/Edit DataTask"
    Me.Panel1.ResumeLayout(False)
    Me.Panel_Scheduling.ResumeLayout(False)
    CType(Me.Numeric_MinuteCount, System.ComponentModel.ISupportInitialize).EndInit()
    Me.TabControl_DataTask.ResumeLayout(False)
    Me.TabPage_Scheduling.ResumeLayout(False)
    Me.TabPage_EmailAddresses.ResumeLayout(False)
    Me.Panel_EmailAddresses.ResumeLayout(False)
    Me.TabPage_XML.ResumeLayout(False)
    Me.TabPage_XML.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "


  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
  Private WithEvents _MainForm As VeniceMain

  ' Form Menu


  ' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
  Private FormTooltip As New ToolTip()

  ' Form Constants, specific to the table being updated.

    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
  Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

  ' Form Locals, initialised on 'New' defining what standard data items to use
    ''' <summary>
  ''' Variable holding the form's data table name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_TABLENAME As String
    ''' <summary>
  ''' Variable holding the form's data adaptor name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_ADAPTORNAME As String
    ''' <summary>
  ''' Variable holding the form's dataset name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_DATASETNAME As String


  ' The standard ChangeID for this form. e.g. tblDataTask
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

    ''' <summary>
  ''' Principal control used for selecting items on this form. 
    ''' </summary>
  Private THIS_FORM_SelectingCombo As ComboBox
    ''' <summary>
  ''' Control to select after proessing the "New" button.
    ''' </summary>
  Private THIS_FORM_NewMoveToControl As Control

  ' Form Specific Order fields
    ''' <summary>
  ''' Field Name to show in the Select combo.
    ''' </summary>
  Private THIS_FORM_SelectBy As String
    ''' <summary>
  ''' Field Name to order the Select combo by.
    ''' </summary>
  Private THIS_FORM_OrderBy As String

    ''' <summary>
  ''' The field name that holds the unique identifier for each entry. e.g. FundID for the tblFund table.
    ''' </summary>
  Private THIS_FORM_ValueMember As String

  ' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
  Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
  Private THIS_FORM_FormID As VeniceFormID

  ' Data Structures

    ''' <summary>
    ''' My dataset
    ''' </summary>
	Private myDataset As RenaissanceDataClass.DSDataTask		' Form Specific !!!!
    ''' <summary>
    ''' My table
    ''' </summary>
	Private myTable As RenaissanceDataClass.DSDataTask.tblDataTaskDataTable
    ''' <summary>
    ''' My connection
    ''' </summary>
  Private myConnection As SqlConnection
    ''' <summary>
    ''' My adaptor
    ''' </summary>
  Private myAdaptor As SqlDataAdapter

    ''' <summary>
    ''' The this standard dataset
    ''' </summary>
  Private ThisStandardDataset As RenaissanceGlobals.StandardDataset


  ' Active Element.

    ''' <summary>
    ''' The sorted rows
    ''' </summary>
  Private SortedRows() As DataRow
    ''' <summary>
    ''' The select by sorted rows
    ''' </summary>
  Private SelectBySortedRows() As DataRow
    ''' <summary>
    ''' The this data row
    ''' </summary>
	Private thisDataRow As RenaissanceDataClass.DSDataTask.tblDataTaskRow		' Form Specific !!!!
    ''' <summary>
    ''' The this audit ID
    ''' </summary>
	Private thisAuditID As Integer
    ''' <summary>
    ''' The this position
    ''' </summary>
	Private thisPosition As Integer
    ''' <summary>
    ''' The __ is over cancel button
    ''' </summary>
	Private __IsOverCancelButton As Boolean
    ''' <summary>
    ''' The _ in use
    ''' </summary>
	Private _InUse As Boolean

	' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
	Private FormIsValid As Boolean
    ''' <summary>
    ''' The form changed
    ''' </summary>
	Private FormChanged As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
	Private _FormOpenFailed As Boolean
    ''' <summary>
    ''' The in paint
    ''' </summary>
	Private InPaint As Boolean
    ''' <summary>
    ''' The add new record
    ''' </summary>
	Private AddNewRecord As Boolean

	' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
	Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
	Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
	Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
	Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
	Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
	Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return __IsOverCancelButton
		End Get
		Set(ByVal Value As Boolean)
			__IsOverCancelButton = Value
		End Set
	End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
	Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
		Get
			Return InPaint
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
		Get
			Return _InUse
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmDataTask"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
	Public Sub New(ByVal pMainForm As VeniceMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()

		_MainForm = pMainForm
		AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

		_FormOpenFailed = False

		' ******************************************************
		' Form Specific Settings :
		' ******************************************************

		THIS_FORM_SelectingCombo = Me.Combo_SelectDataTaskCode
		THIS_FORM_NewMoveToControl = Me.editTaskDescription

		' Default Select and Order fields.

		THIS_FORM_SelectBy = "TaskDescription"
		THIS_FORM_OrderBy = "TaskDescription"

		THIS_FORM_ValueMember = "TaskID"

		' Form Permissioning :-

		THIS_FORM_PermissionArea = Me.Name
		THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

		' 'This' form ID

		THIS_FORM_FormID = VeniceFormID.frmDataTask

		' This form's dataset type.

		ThisStandardDataset = RenaissanceStandardDatasets.tblDataTask	' This Defines the Form Data !!! 

		' Format Event Handlers for form controls

		AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler
		AddHandler editTaskDescription.LostFocus, AddressOf MainForm.Text_NotNull

		' Form Control Changed events
		AddHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
		AddHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		AddHandler editTaskDescription.TextChanged, AddressOf Me.FormControlChanged

		AddHandler Combo_TaskType.SelectedIndexChanged, AddressOf Me.FormControlChanged

		AddHandler Combo_Ticker.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_Ticker.SelectedIndexChanged, AddressOf Me.FormControlChanged

		AddHandler Combo_Fields.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_Fields.SelectedIndexChanged, AddressOf Me.FormControlChanged
		AddHandler edit_PriceMoveTrigger.ValueChanged, AddressOf Me.FormControlChanged
		AddHandler Check_AggregateNotificationEmails.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Check_PortfolioMonitorItems.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler ListBox_EmailPeople.ItemCheck, AddressOf Me.ListCheckChanged

		AddHandler CheckBox_Mon.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler CheckBox_Tue.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler CheckBox_Wed.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler CheckBox_Thu.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler CheckBox_Fri.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler CheckBox_Sat.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler CheckBox_Sun.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Radio_OccursOnce.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Radio_OccursEvery.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler DT_OccursOnce.ValueChanged, AddressOf Me.FormControlChanged
		AddHandler Numeric_MinuteCount.ValueChanged, AddressOf Me.FormControlChanged
		AddHandler DT_TimeStarts.ValueChanged, AddressOf Me.FormControlChanged
		AddHandler DT_TimeEnds.ValueChanged, AddressOf Me.FormControlChanged

		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

		' ******************************************************
		' End Form Specific.
		' ******************************************************

		' Data object names standard to this Form type.

		THIS_TABLENAME = ThisStandardDataset.TableName
		THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
		THIS_DATASETNAME = ThisStandardDataset.DatasetName

		THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

		' Establish / Retrieve data objects for this form.

		myConnection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)
		myAdaptor = MainForm.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, VENICE_CONNECTION, THIS_TABLENAME)
		SyncLock myConnection
			myDataset = MainForm.Load_Table(ThisStandardDataset, False)
		End SyncLock
		myTable = myDataset.Tables(0)

		MainForm.BuildStandardFormMenu(Me.RootMenu, myDataset.Tables(0), AddressOf SelectMenuEvent, AddressOf OrderMenuEvent, AddressOf AuditReportMenuEvent)

	End Sub

#Region " This Form Event handlers : FormLoad / FormClose / AutoUpdate"

	' Form Initialisation code.
	'
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
	Public Sub ResetForm() Implements StandardVeniceForm.ResetForm
		THIS_FORM_SelectBy = "TaskDescription"
		THIS_FORM_OrderBy = "TaskDescription"

		Call Form_Load(Me, New System.EventArgs)
	End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
	Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
		_FormOpenFailed = False
		_InUse = True

		' Initialise Data structures. Connection, Adaptor and Dataset.

		If Not (MainForm Is Nothing) Then
			FormIsValid = True
		Else
			MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myConnection Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myAdaptor Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myDataset Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		' Initialse form

		InPaint = True
		IsOverCancelButton = False

		' Check User permissions
		Call CheckPermissions()
		If (HasReadPermission = False) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		' Build Sorted data list from which this form operates
		Call SetSortedRows()

		' Display initial record.

		Call Me.SetTaskTypeCombo()
		Call Me.SetEmailAddressList()

		thisPosition = 0
		If THIS_FORM_SelectingCombo.Items.Count > 0 Then
			Me.THIS_FORM_SelectingCombo.SelectedIndex = 0
			thisDataRow = SortedRows(thisPosition)
			Call GetFormData(thisDataRow)
		Else
			Me.THIS_FORM_SelectingCombo.SelectedIndex = (-1)
			Call GetFormData(Nothing)
		End If

		InPaint = False

	End Sub

    ''' <summary>
    ''' Handles the Closing event of the frmDataTask control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
	Private Sub frmDataTask_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		Dim HideForm As Boolean

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...

		_InUse = False

		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else
			If (FormChanged = True) Then
				Call SetFormData()
			End If

			HideForm = True
			If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			e.Cancel = True
		Else
			Try
				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler
				RemoveHandler editTaskDescription.LostFocus, AddressOf MainForm.Text_NotNull

				' Form Control Changed events
				RemoveHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
				RemoveHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				RemoveHandler editTaskDescription.TextChanged, AddressOf Me.FormControlChanged

				RemoveHandler Combo_TaskType.SelectedIndexChanged, AddressOf Me.FormControlChanged

				RemoveHandler Combo_Ticker.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Ticker.SelectedIndexChanged, AddressOf Me.FormControlChanged

				RemoveHandler Combo_Fields.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Fields.SelectedIndexChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_PriceMoveTrigger.ValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_AggregateNotificationEmails.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_PortfolioMonitorItems.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler ListBox_EmailPeople.ItemCheck, AddressOf Me.ListCheckChanged

				RemoveHandler CheckBox_Mon.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler CheckBox_Tue.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler CheckBox_Wed.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler CheckBox_Thu.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler CheckBox_Fri.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler CheckBox_Sat.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler CheckBox_Sun.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Radio_OccursOnce.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Radio_OccursEvery.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler DT_OccursOnce.ValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Numeric_MinuteCount.ValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler DT_TimeStarts.ValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler DT_TimeEnds.ValueChanged, AddressOf Me.FormControlChanged

			Catch ex As Exception
			End Try
		End If

	End Sub

    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		' Routine to handle changes / updates to tables by this and other windows.
		' If this, or any other, form posts a change to a table, then it will invoke an update event 
		' detailing what tables have been altered.
		' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
		' the 'VeniceAutoUpdate' event of the main Venice form.
		' Each form may them react as appropriate to changes in any table that might impact it.
		'
		Dim OrgInPaint As Boolean
		Dim KnowledgeDateChanged As Boolean
		Dim SetButtonStatus_Flag As Boolean

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		OrgInPaint = InPaint
		InPaint = True
		KnowledgeDateChanged = False
		SetButtonStatus_Flag = False

		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
			KnowledgeDateChanged = True
		End If

		' ****************************************************************
		' Check for changes relevant to this form
		' ****************************************************************

		' Changes to the KnowledgeDate :-
		If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
			SetButtonStatus_Flag = True
		End If

		' Changes to the tblUserPermissions table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

			' Check ongoing permissions.

			Call CheckPermissions()
			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

				FormIsValid = False
				Me.Close()
				Exit Sub
			End If

			SetButtonStatus_Flag = True

		End If

		' Changes to the tblPerson table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblPerson) = True) Or KnowledgeDateChanged Then
			Call SetEmailAddressList()
		End If

		' ****************************************************************
		' Changes to the Main FORM table :-
		' ****************************************************************

		If (e.TableChanged(THIS_FORM_ChangeID) = True) Or KnowledgeDateChanged Then

			' Re-Set Controls etc.
			Call SetSortedRows()

			' Move again to the correct item
			thisPosition = Get_Position(thisAuditID)

			' Validate current position.
			If thisPosition >= 0 And thisPosition < Me.SortedRows.GetLength(0) Then
				thisDataRow = Me.SortedRows(thisPosition)
			Else
				If (Me.SortedRows.GetLength(0) > 0) And (AddNewRecord = False) Then
					thisPosition = 0
					thisDataRow = Me.SortedRows(thisPosition)
					FormChanged = False
				Else
					thisDataRow = Nothing
				End If
			End If

			' Set SelectingCombo Index.
			If (Me.THIS_FORM_SelectingCombo.Items.Count <= 0) Then
				Try
					Me.THIS_FORM_SelectingCombo.SelectedIndex = -1
				Catch ex As Exception
				End Try
			ElseIf (thisPosition < 0) Then
				Try
					MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
				Catch ex As Exception
				End Try
			Else
				Try
					Me.THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
				Catch ex As Exception
				End Try
			End If

		End If

		' ****************************************************************
		' Repaint if not currently in Edit Mode
		'
		' ****************************************************************

		InPaint = OrgInPaint

		' Retrieve form data if appropriate, i.e. this data item is not already being edited.
		If (FormChanged = False) And (AddNewRecord = False) Then
			GetFormData(thisDataRow) ' Includes a call to 'SetButtonStatus()'
		Else
			If SetButtonStatus_Flag Then
				Call SetButtonStatus()
			End If
		End If

	End Sub

#End Region

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

	' Build Sorted list from the Source dataset and update the Select Combo
    ''' <summary>
    ''' Sets the sorted rows.
    ''' </summary>
	Private Sub SetSortedRows()

		Dim OrgInPaint As Boolean

		Dim thisrow As DataRow
		Dim thisDrowDownWidth As Integer
		Dim SizingBitmap As Bitmap
		Dim SizingGraphics As Graphics

		' Form Specific Selection Combo :-
		If (THIS_FORM_SelectingCombo Is Nothing) Then Exit Sub

		' Code is pretty Generic form here on...

		' Set paint local so that changes to the selection combo do not trigger form updates.

		OrgInPaint = InPaint
		InPaint = True

		' Get selected Row sets, or exit if no data is present.
		If myDataset Is Nothing Then
			ReDim SortedRows(0)
			ReDim SelectBySortedRows(0)
		Else
			SortedRows = myTable.Select("RN >= 0", THIS_FORM_OrderBy)
			SelectBySortedRows = myTable.Select("RN >= 0", THIS_FORM_SelectBy)
		End If

		' Set Combo data source
		THIS_FORM_SelectingCombo.DataSource = SortedRows
		THIS_FORM_SelectingCombo.DisplayMember = THIS_FORM_SelectBy
		THIS_FORM_SelectingCombo.ValueMember = THIS_FORM_ValueMember

		' Ensure that the Selection Combo's DrowDownWidth is wide enough to take the required strings

		thisDrowDownWidth = THIS_FORM_SelectingCombo.Width
		For Each thisrow In SortedRows

			' Compute the string dimensions in the given font
			SizingBitmap = New Bitmap(1, 1, System.Drawing.Imaging.PixelFormat.Format32bppArgb)
			SizingGraphics = Graphics.FromImage(SizingBitmap)
			Dim stringSize As SizeF = SizingGraphics.MeasureString(thisrow(THIS_FORM_SelectBy).ToString, THIS_FORM_SelectingCombo.Font)
			If (stringSize.Width) > thisDrowDownWidth Then
				thisDrowDownWidth = CInt(stringSize.Width)
			End If
		Next

		THIS_FORM_SelectingCombo.DropDownWidth = thisDrowDownWidth

		InPaint = OrgInPaint
	End Sub


	' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
	Private Sub CheckPermissions()

		Dim Permissions As Integer

		Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

		HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
		HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
		HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
		HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

	End Sub

	' Flag changes to form controls, Event Associations made in 'New' routine.
    ''' <summary>
  ''' Basic event handler, called when a form value is changed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

		If InPaint = False Then
			If (Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord) Then
				FormChanged = True
				Me.btnSave.Enabled = True
				Me.btnCancel.Enabled = True
			End If
		End If

	End Sub

    ''' <summary>
    ''' Lists the check changed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.ItemCheckEventArgs"/> instance containing the event data.</param>
	Private Sub ListCheckChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs)
		If InPaint = False Then
			If (Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord) Then
				FormChanged = True
				Me.btnSave.Enabled = True
				Me.btnCancel.Enabled = True
			End If
		End If

	End Sub

    ''' <summary>
    ''' Selects the menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Public Sub SelectMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Select Menu
		' Event association is made during the dynamic menu creation

		THIS_FORM_SelectBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

		Call SetSortedRows()

	End Sub

    ''' <summary>
    ''' Orders the menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Public Sub OrderMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Order Menu
		' Event association is made during the dynamic menu creation

		THIS_FORM_OrderBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

		Call SetSortedRows()

	End Sub


    ''' <summary>
    ''' Audits the report menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Public Sub AuditReportMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Audit Report Menu
		' Event association is made during the dynamic menu creation

		Try
			Select Case CInt(CType(sender, System.Windows.Forms.ToolStripMenuItem).Tag)
				Case 0 ' This Record
					If (Me.AddNewRecord = False) AndAlso (thisAuditID >= 0) Then
						Run_AuditReport_ThisRecord(MainForm, ThisStandardDataset, thisAuditID, MainForm.VeniceProgressBar)
					End If

				Case 1 ' All Records
					Run_AuditReport_ALLRecords(MainForm, ThisStandardDataset, MainForm.VeniceProgressBar)

			End Select
		Catch ex As Exception
			Call MainForm.LogError(Me.Name & ", AuditReportMenuEvent()", 0, ex.Message, "Error calling Audit report.", ex.StackTrace, True)
		End Try

	End Sub


#End Region

#Region " Set Form Combos (Form Specific Code) "

    ''' <summary>
    ''' Sets the ticker combo.
    ''' </summary>
	Private Sub SetTickerCombo()
		Dim IsAPortfolioDataTask As Boolean

		IsAPortfolioDataTask = False
		If Me.Combo_TaskType.SelectedIndex >= 0 Then
			Dim ThisDataTaskType As DataTaskType

			ThisDataTaskType = CType(CInt(Me.Combo_TaskType.SelectedValue), DataTaskType)

			Select Case ThisDataTaskType

				Case DataTaskType.PortfolioCapture

					Call MainForm.SetTblGenericCombo( _
					Me.Combo_Ticker, _
					RenaissanceStandardDatasets.tblInstrument, _
					"InstrumentDescription, InstrumentPortfolioTicker", _
					"InstrumentID", _
					"InstrumentPortfolioTicker<>''", True, True, False)

				Case DataTaskType.PeriodicSecurityCapture, DataTaskType.RealtimeSecurityCapture

					Call MainForm.SetTblGenericCombo( _
					Me.Combo_Ticker, _
					RenaissanceStandardDatasets.tblInstrument, _
					"InstrumentDescription, InstrumentExchangeCode", _
					"InstrumentID", _
					"InstrumentExchangeCode<>''", True, True, False)

				Case DataTaskType.RealtimeMarketSecurityCapture, DataTaskType.RealtimeMarketSecurityCapture

					Call MainForm.SetTblGenericCombo( _
					Me.Combo_Ticker, _
					RenaissanceStandardDatasets.tblMarketInstruments, _
					"Description, Ticker", _
					"MarketInstrumentID", _
					"Ticker<>''", True, True, False)

				Case DataTaskType.PageCapture

					Combo_Ticker.Items.Clear()

				Case DataTaskType.EmailPortfolioStockChangeNotification
					Call MainForm.SetTblGenericCombo( _
					Me.Combo_Ticker, _
					RenaissanceStandardDatasets.tblPortfolioIndex, _
					"PortfolioDescription, PortfolioFilingDate, PortfolioTicker", _
					"PortfolioID", _
					"PortfolioTicker<>''", False, True, False)

				Case DataTaskType.EmailSingleStockChangeNotification
					Call MainForm.SetTblGenericCombo( _
					Me.Combo_Ticker, _
					RenaissanceStandardDatasets.tblMarketInstruments, _
					"Description, Ticker", _
					"MarketInstrumentID", _
					"Ticker<>''", True, True, False)

				Case Else

					Call MainForm.SetTblGenericCombo( _
					Me.Combo_Ticker, _
					RenaissanceStandardDatasets.tblInstrument, _
					"InstrumentDescription, InstrumentPortfolioTicker", _
					"InstrumentID", _
					"InstrumentPortfolioTicker<>''", True, True, False)

			End Select

		Else

			Call MainForm.SetTblGenericCombo( _
			 Me.Combo_Ticker, _
			 RenaissanceStandardDatasets.tblInstrument, _
			 "InstrumentDescription, InstrumentPortfolioTicker", _
			 "InstrumentID", _
			 "", True, True, False)

		End If


	End Sub

    ''' <summary>
    ''' Sets the task type combo.
    ''' </summary>
	Private Sub SetTaskTypeCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_TaskType, _
		GetType(DataTaskType), _
		False)

	End Sub

    ''' <summary>
    ''' Sets the email address list.
    ''' </summary>
	Private Sub SetEmailAddressList()
		Dim thisDataset As RenaissanceDataClass.DSPerson
		Dim thisTable As RenaissanceDataClass.DSPerson.tblPersonDataTable
		Dim ThisRow As RenaissanceDataClass.DSPerson.tblPersonRow
		Dim RowCounter As Integer
		Dim ListCounter As Integer
		Dim FindFlag As Boolean
		Dim thisEMailEntryObject As RenaissanceControls.EMailEntryObject

		Try
			thisDataset = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblPerson, False), RenaissanceDataClass.DSPerson)
			If (thisDataset Is Nothing) Then Exit Sub
			If (thisDataset.Tables.Count <= 0) Then Exit Sub
			thisTable = thisDataset.tblPerson
		Catch ex As Exception
			Exit Sub
		End Try

		Try
			ListBox_EmailPeople.SuspendLayout()

			If (thisTable.Rows.Count <= 0) Then
				ListBox_EmailPeople.Items.Clear()
				Exit Sub
			End If

			' First Clear List items that do not already exist
			For ListCounter = (ListBox_EmailPeople.Items.Count - 1) To 0 Step -1
				FindFlag = False

				If (ListBox_EmailPeople.Items(ListCounter) IsNot Nothing) AndAlso (TypeOf ListBox_EmailPeople.Items(ListCounter) Is RenaissanceControls.EMailEntryObject) Then
					thisEMailEntryObject = CType(ListBox_EmailPeople.Items(ListCounter), RenaissanceControls.EMailEntryObject)

					For RowCounter = 0 To (thisTable.Rows.Count - 1)
						ThisRow = thisTable.Rows(RowCounter)
						If thisEMailEntryObject.PersonID = ThisRow.PersonID Then
							FindFlag = True
							Exit For
						End If
					Next

					If (Not FindFlag) Then
						ListBox_EmailPeople.Items.RemoveAt(ListCounter)
					End If

				End If

			Next


			For RowCounter = 0 To (thisTable.Rows.Count - 1)
				FindFlag = False
				ThisRow = thisTable.Rows(RowCounter)

				If (ThisRow.PersonEmail.Length > 0) Then
					' thisEMailEntryObject = New RenaissanceControls.EMailEntryObject(ThisRow.PersonID, ThisRow.Person, ThisRow.PersonEmail)

					For ListCounter = 0 To (ListBox_EmailPeople.Items.Count - 1)
						If (ListBox_EmailPeople.Items(ListCounter) IsNot Nothing) AndAlso (TypeOf ListBox_EmailPeople.Items(ListCounter) Is RenaissanceControls.EMailEntryObject) Then
							thisEMailEntryObject = CType(ListBox_EmailPeople.Items(ListCounter), RenaissanceControls.EMailEntryObject)

							If thisEMailEntryObject.PersonID = ThisRow.PersonID Then
								' Check Name and EMail are upto date.

								thisEMailEntryObject.EMail = ThisRow.PersonEmail
								thisEMailEntryObject.PersonName = ThisRow.Person

								' Flag as Found
								FindFlag = True
								Exit For
							End If
						End If
					Next

					If (Not FindFlag) Then
						ListBox_EmailPeople.Items.Add(New RenaissanceControls.EMailEntryObject(ThisRow.PersonID, ThisRow.Person, ThisRow.PersonEmail))
					End If
				End If
			Next
		Catch ex As Exception
		Finally
			ListBox_EmailPeople.ResumeLayout(True)
		End Try

	End Sub

#End Region

#Region " Form Control Event Code"

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_TaskType control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_TaskType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_TaskType.SelectedIndexChanged
		If Me.InPaint = False Then
			Call SetTickerCombo()
		End If

		Me.Panel_Scheduling.Enabled = False
		Check_AggregateNotificationEmails.Enabled = False
		Check_PortfolioMonitorItems.Enabled = False

		ListBox_EmailPeople.Enabled = False

		If Combo_TaskType.SelectedIndex >= 0 Then
			If (Combo_TaskType.SelectedValue IsNot Nothing) Then
				Select Case CType(Combo_TaskType.SelectedValue, DataTaskType)
					Case NaplesGlobals.DataTaskType.PageCapture, _
					 NaplesGlobals.DataTaskType.PeriodicSecurityCapture, _
					 NaplesGlobals.DataTaskType.PeriodicMarketSecurityCapture, _
					 NaplesGlobals.DataTaskType.PortfolioCapture, _
					 NaplesGlobals.DataTaskType.RealtimeSecurityCapture, _
					 NaplesGlobals.DataTaskType.RealtimeMarketSecurityCapture

						Panel_Scheduling.Enabled = True

					Case NaplesGlobals.DataTaskType.EmailPeriodicInstrumentInformation, _
								DataTaskType.EmailPage

						Panel_Scheduling.Enabled = True
						ListBox_EmailPeople.Enabled = True

					Case NaplesGlobals.DataTaskType.EmailPortfolioStockChangeNotification

						Check_AggregateNotificationEmails.Enabled = True
						Check_PortfolioMonitorItems.Enabled = True
						ListBox_EmailPeople.Enabled = True

						If (Check_AggregateNotificationEmails.Checked) Then
							Panel_Scheduling.Enabled = True
						End If

					Case NaplesGlobals.DataTaskType.EmailSingleStockChangeNotification

						Check_AggregateNotificationEmails.Enabled = True
						ListBox_EmailPeople.Enabled = True

						If (Check_AggregateNotificationEmails.Checked) Then
							Panel_Scheduling.Enabled = True
						End If

				End Select
			End If
		End If

	End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the Check_AggregateNotificationEmails control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Check_AggregateNotificationEmails_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_AggregateNotificationEmails.CheckedChanged
		Panel_Scheduling.Enabled = Check_AggregateNotificationEmails.Checked
	End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the Radio_OccursOnce control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Radio_OccursOnce_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_OccursOnce.CheckedChanged
		If (Radio_OccursOnce.Checked) Then
			Radio_OccursEvery.Checked = False

			Me.DT_OccursOnce.Enabled = True
			Me.DT_TimeStarts.Enabled = False
			Me.DT_TimeEnds.Enabled = False
		End If
	End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the Radio_OccursEvery control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Radio_OccursEvery_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_OccursEvery.CheckedChanged
		If (Me.Radio_OccursEvery.Checked) Then
			Radio_OccursOnce.Checked = False

			Me.DT_OccursOnce.Enabled = False
			Me.DT_TimeStarts.Enabled = True
			Me.DT_TimeEnds.Enabled = True
		End If
	End Sub

#End Region

#Region " Get & Set Form Data / SetButton / ValidateForm / btnCancel Events (Form Specific Code) "

    ''' <summary>
    ''' Gets the form data.
    ''' </summary>
    ''' <param name="ThisRow">The this row.</param>
	Private Sub GetFormData(ByRef ThisRow As RenaissanceDataClass.DSDataTask.tblDataTaskRow)
		' Routine to populate form Controls given a data row.
		' Form is cleared for invalid Datarows or Invalid Form.

		Dim OrgInpaint As Boolean
		Dim ThisDataTaskType As DataTaskType

		' Set 'Paint' flag to prevent event actions caused by changing the value of form fields.
		OrgInpaint = InPaint
		InPaint = True

		Me.TabControl_DataTask.TabIndex = 0

		If (ThisRow Is Nothing) Or (FormIsValid = False) Or (Me.InUse = False) Then
			' Bad / New Datarow - Clear Form.

			thisAuditID = (-1)

			Me.editAuditID.Text = ""

			Me.editTaskDescription.Text = ""
			If Me.Combo_TaskType.Items.Count > 0 Then
				Me.Combo_TaskType.SelectedIndex = 0
			Else
				Me.Combo_TaskType.SelectedIndex = -1
			End If

			Me.Combo_Ticker.Text = ""
			Me.Combo_Fields.Text = ""
			edit_PriceMoveTrigger.Value = 0

			editXML.Text = ""
			Text_TaskComment.Text = ""

			Me.Check_AggregateNotificationEmails.Checked = False
			Me.Check_PortfolioMonitorItems.Checked = False

			Me.CheckBox_Mon.Checked = False
			Me.CheckBox_Tue.Checked = False
			Me.CheckBox_Wed.Checked = False
			Me.CheckBox_Thu.Checked = False
			Me.CheckBox_Fri.Checked = False
			Me.CheckBox_Sat.Checked = False
			Me.CheckBox_Sun.Checked = False

			Me.Radio_OccursOnce.Checked = True
			Me.DT_OccursOnce.Value = Renaissance_BaseDate.AddHours(7)
			Me.DT_TimeStarts.Value = Renaissance_BaseDate.AddHours(7)
			Me.DT_TimeEnds.Value = Renaissance_BaseDate.AddHours(19)

			Try
				Dim CheckIndex As Integer

				For Each CheckIndex In ListBox_EmailPeople.CheckedIndices
					ListBox_EmailPeople.SetItemChecked(CheckIndex, False)
				Next

			Catch ex As Exception
			End Try

			If AddNewRecord = True Then
				Me.btnCancel.Enabled = True
				Me.THIS_FORM_SelectingCombo.Enabled = False
			Else
				Me.btnCancel.Enabled = False
				Me.THIS_FORM_SelectingCombo.Enabled = True
			End If

			Call Me.SetTickerCombo()

		Else

			' Populate Form with given data.
			Try
				thisAuditID = thisDataRow.AuditID

				Me.editAuditID.Text = thisDataRow.AuditID.ToString

				Me.editTaskDescription.Text = thisDataRow.TaskDescription

				Dim XML_TaskDetails As XmlDocument
				Dim nodelist As XmlNodeList

				editXML.Text = thisDataRow.TaskDefinition

				XML_TaskDetails = New XmlDocument
				XML_TaskDetails.LoadXml(thisDataRow.TaskDefinition)

				' Task Type

				Try
					nodelist = XML_TaskDetails.GetElementsByTagName("RequestType")
					ThisDataTaskType = DataTaskType.NoTask
					If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) Then
						ThisDataTaskType = CType(System.Enum.Parse(GetType(DataTaskType), nodelist(0).InnerText), DataTaskType)
						Me.Combo_TaskType.SelectedValue = CType(System.Enum.Parse(GetType(DataTaskType), nodelist(0).InnerText), Integer)
					Else
						Me.Combo_TaskType.SelectedIndex = 0
					End If
				Catch ex As Exception
					Me.Combo_TaskType.SelectedIndex = 0
					ThisDataTaskType = DataTaskType.NoTask
				End Try

				'       edit_PriceMoveTrigger.Value = 0
				Try
					nodelist = XML_TaskDetails.GetElementsByTagName("PriceMoveTrigger")
					If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) AndAlso (IsNumeric(nodelist(0).InnerText)) Then
						edit_PriceMoveTrigger.Value = CDbl(nodelist(0).InnerText)
					Else
						edit_PriceMoveTrigger.Value = 0
					End If
				Catch ex As Exception
					edit_PriceMoveTrigger.Value = 0
				End Try

				' 		
				' LimitToMonitorItems
				'       

				Try
					nodelist = XML_TaskDetails.GetElementsByTagName("LimitToMonitorItems")
					If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) Then
						If CBool(nodelist(0).InnerText) Then
							Me.Check_PortfolioMonitorItems.Checked = True
						Else
							Me.Check_PortfolioMonitorItems.Checked = False
						End If
					Else
						Me.Check_PortfolioMonitorItems.Checked = False
					End If
				Catch ex As Exception
					Me.Check_PortfolioMonitorItems.Checked = False
				End Try

				' Aggregate Emails
				'       

				Try
					nodelist = XML_TaskDetails.GetElementsByTagName("AggregateEmails")
					If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) Then
						If CBool(nodelist(0).InnerText) Then
							Me.Check_AggregateNotificationEmails.Checked = True
						Else
							Me.Check_AggregateNotificationEmails.Checked = False
						End If
					Else
						Me.Check_AggregateNotificationEmails.Checked = False
					End If
				Catch ex As Exception
					Me.Check_AggregateNotificationEmails.Checked = False
				End Try

				' Email Addresses
				Try
					Dim CheckIndex As Integer
					Dim AddressCount As Integer = 0
					Dim AddressIndex As Integer = 0
					Dim EMailAddress As String
					Dim PersonID As Integer = 0
					Dim thisEMailEntryObject As RenaissanceControls.EMailEntryObject

					For Each CheckIndex In ListBox_EmailPeople.CheckedIndices
						ListBox_EmailPeople.SetItemChecked(CheckIndex, False)
					Next

					nodelist = XML_TaskDetails.GetElementsByTagName("EMail_AddressCount")
					If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) AndAlso (IsNumeric(nodelist(0).InnerText)) Then
						AddressCount = CInt(nodelist(0).InnerText)
					End If

					For AddressIndex = 1 To AddressCount
						EMailAddress = ""
						PersonID = 0

						nodelist = XML_TaskDetails.GetElementsByTagName("EMail_PersonID_" & AddressIndex.ToString)
						If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) AndAlso (IsNumeric(nodelist(0).InnerText)) AndAlso (CInt(nodelist(0).InnerText) > 0) Then
							PersonID = CInt(nodelist(0).InnerText)

							For CheckIndex = 0 To (ListBox_EmailPeople.Items.Count - 1)
								If (ListBox_EmailPeople.Items(CheckIndex) IsNot Nothing) AndAlso (TypeOf ListBox_EmailPeople.Items(CheckIndex) Is RenaissanceControls.EMailEntryObject) Then
									thisEMailEntryObject = CType(ListBox_EmailPeople.Items(CheckIndex), RenaissanceControls.EMailEntryObject)

									If thisEMailEntryObject.PersonID = PersonID Then
										ListBox_EmailPeople.SetItemChecked(CheckIndex, True)
										Exit For
									End If
								End If
							Next
						Else
							nodelist = XML_TaskDetails.GetElementsByTagName("EMail_Address_" & AddressIndex.ToString)
							If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) AndAlso (nodelist(0).InnerText.Length > 0) Then
								For CheckIndex = 0 To (ListBox_EmailPeople.Items.Count - 1)
									If (ListBox_EmailPeople.Items(CheckIndex) IsNot Nothing) AndAlso (TypeOf ListBox_EmailPeople.Items(CheckIndex) Is RenaissanceControls.EMailEntryObject) Then
										thisEMailEntryObject = CType(ListBox_EmailPeople.Items(CheckIndex), RenaissanceControls.EMailEntryObject)
										If (thisEMailEntryObject.EMail.ToLower = CStr(nodelist(0).InnerText).ToLower) Then
											ListBox_EmailPeople.SetItemChecked(CheckIndex, True)
											Exit For
										End If
									End If
								Next
							End If
						End If
					Next

				Catch ex As Exception
				End Try


				' SaveDays

				Dim DayMap As Integer
				Me.CheckBox_Mon.Checked = False
				Me.CheckBox_Tue.Checked = False
				Me.CheckBox_Wed.Checked = False
				Me.CheckBox_Thu.Checked = False
				Me.CheckBox_Fri.Checked = False
				Me.CheckBox_Sat.Checked = False
				Me.CheckBox_Sun.Checked = False

				nodelist = XML_TaskDetails.GetElementsByTagName("SaveDays")
				If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) AndAlso (IsNumeric(nodelist(0).InnerText)) Then
					DayMap = CInt(nodelist(0).InnerText)
				Else
					DayMap = DataTaskDay.WeekDays
				End If
				If (DayMap And DataTaskDay.Monday) Then
					Me.CheckBox_Mon.Checked = True
				End If
				If (DayMap And DataTaskDay.Tuesday) Then
					Me.CheckBox_Tue.Checked = True
				End If
				If (DayMap And DataTaskDay.Wednesday) Then
					Me.CheckBox_Wed.Checked = True
				End If
				If (DayMap And DataTaskDay.Thursday) Then
					Me.CheckBox_Thu.Checked = True
				End If
				If (DayMap And DataTaskDay.Friday) Then
					Me.CheckBox_Fri.Checked = True
				End If
				If (DayMap And DataTaskDay.Saturday) Then
					Me.CheckBox_Sat.Checked = True
				End If
				If (DayMap And DataTaskDay.Sunday) Then
					Me.CheckBox_Sun.Checked = True
				End If

				' Frequency - 'Once' or 'Interval'

				Try
					nodelist = XML_TaskDetails.GetElementsByTagName("Frequency")
					If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) Then
						If nodelist(0).InnerText.StartsWith("O") Then
							Me.Radio_OccursOnce.Checked = True
						Else
							If (CInt(Me.Combo_TaskType.SelectedValue) = CInt(DataTaskType.PortfolioCapture)) Then
								' Portfolio Capture is always 'Once-a-Day'
								Me.Radio_OccursOnce.Checked = True
							Else
								Me.Radio_OccursEvery.Checked = True
							End If
						End If
					Else
						Me.Radio_OccursOnce.Checked = True
					End If
				Catch ex As Exception
					Me.Radio_OccursOnce.Checked = True
				End Try

				' Start Time

				Try
					nodelist = XML_TaskDetails.GetElementsByTagName("StartTime")
					If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) AndAlso (IsDate(nodelist(0).InnerText)) Then
						Me.DT_TimeStarts.Value = CDate("1 Jan 1900 " & nodelist(0).InnerText)
					Else
						Me.DT_TimeStarts.Value = New Date(3000, 1, 1, 7, 0, 0)
					End If
				Catch ex As Exception
					Me.DT_TimeStarts.Value = New Date(3000, 1, 1, 7, 0, 0)
				End Try
				Me.DT_OccursOnce.Value = Me.DT_TimeStarts.Value

				' End Time

				Try
					nodelist = XML_TaskDetails.GetElementsByTagName("EndTime")
					If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) AndAlso (IsDate(nodelist(0).InnerText)) Then
						Me.DT_TimeEnds.Value = CDate("1 Jan 1900 " & nodelist(0).InnerText)
					Else
						Me.DT_TimeEnds.Value = New Date(3000, 1, 1, 19, 0, 0)
					End If
				Catch ex As Exception
					Me.DT_TimeEnds.Value = New Date(3000, 1, 1, 19, 0, 0)
				End Try

				' Interval

				Try
					nodelist = XML_TaskDetails.GetElementsByTagName("Interval")
					If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) AndAlso (IsNumeric(nodelist(0).InnerText)) Then
						Me.Numeric_MinuteCount.Value = CInt(nodelist(0).InnerText) / 60
					Else
						Me.Numeric_MinuteCount.Value = 60
					End If
				Catch ex As Exception
					Me.Numeric_MinuteCount.Value = 60
				End Try

				' Ticker
				Call Me.SetTickerCombo()

				Dim thisInstrumentID As Integer = 0
				Dim thisPortfolioID As Integer = 0
				Dim InstrumentTicker As String = ""
				Dim PortfolioTicker As String = ""
				Dim PageTicker As String = ""

				Try
					nodelist = XML_TaskDetails.GetElementsByTagName("Instrument")
					If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) Then
						InstrumentTicker = nodelist(0).InnerText
					Else
						nodelist = XML_TaskDetails.GetElementsByTagName("Security")
						If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) Then
							InstrumentTicker = nodelist(0).InnerText
						End If
					End If

					nodelist = XML_TaskDetails.GetElementsByTagName("InstrumentID")
					If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) AndAlso (IsNumeric(nodelist(0).InnerText)) Then
						thisInstrumentID = CInt(nodelist(0).InnerText)
					End If

					nodelist = XML_TaskDetails.GetElementsByTagName("Portfolio")
					If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) Then
						PortfolioTicker = nodelist(0).InnerText
					End If

					nodelist = XML_TaskDetails.GetElementsByTagName("PortfolioID")
					If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) AndAlso (IsNumeric(nodelist(0).InnerText)) Then
						thisPortfolioID = CInt(nodelist(0).InnerText)
					End If

					nodelist = XML_TaskDetails.GetElementsByTagName("PageCode")
					If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) Then
						PageTicker = (nodelist(0).InnerText)
					End If

				Catch ex As Exception
				End Try

				Try
					Select Case ThisDataTaskType

						Case DataTaskType.PortfolioCapture

							If (thisInstrumentID > 0) Then
								Combo_Ticker.SelectedValue = thisInstrumentID
							Else
								Combo_Ticker.Text = PortfolioTicker
							End If

						Case DataTaskType.PeriodicSecurityCapture, DataTaskType.RealtimeSecurityCapture

							If (thisInstrumentID > 0) Then
								Combo_Ticker.SelectedValue = thisInstrumentID
							Else
								Combo_Ticker.Text = InstrumentTicker
							End If

						Case DataTaskType.PeriodicMarketSecurityCapture, DataTaskType.RealtimeMarketSecurityCapture

							If (thisInstrumentID > 0) Then
								Combo_Ticker.SelectedValue = thisInstrumentID
							Else
								Combo_Ticker.Text = InstrumentTicker
							End If

						Case DataTaskType.PageCapture
							Combo_Ticker.Text = PageTicker

						Case DataTaskType.EmailPortfolioStockChangeNotification

							If (thisPortfolioID > 0) Then
								Combo_Ticker.SelectedValue = thisPortfolioID
							Else
								Combo_Ticker.Text = PortfolioTicker
							End If

						Case DataTaskType.EmailSingleStockChangeNotification

							If (thisInstrumentID > 0) Then
								Combo_Ticker.SelectedValue = thisInstrumentID
							Else
								Combo_Ticker.Text = InstrumentTicker
							End If

						Case Else

							Combo_Ticker.Text = InstrumentTicker

					End Select


				Catch ex As Exception

				End Try

				'Try
				'	nodelist = XML_TaskDetails.GetElementsByTagName("Security")
				'	If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) Then
				'		Me.Combo_Ticker.Text = nodelist(0).InnerText
				'	Else
				'		Me.Combo_Ticker.Text = ""
				'	End If
				'Catch ex As Exception
				'	Me.Combo_Ticker.Text = ""
				'End Try

				' Portfolio

				'Try
				'	If Me.Combo_Ticker.Text.Length <= 0 Then
				'		nodelist = XML_TaskDetails.GetElementsByTagName("Portfolio")
				'		If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) Then
				'			Me.Combo_Ticker.Text = nodelist(0).InnerText
				'		Else
				'			Me.Combo_Ticker.Text = ""
				'		End If
				'	End If
				'Catch ex As Exception
				'	Me.Combo_Ticker.Text = ""
				'End Try

				' PageCode

				'Try
				'	If Me.Combo_Ticker.Text.Length <= 0 Then
				'		nodelist = XML_TaskDetails.GetElementsByTagName("PageCode")
				'		If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) Then
				'			Me.Combo_Ticker.Text = nodelist(0).InnerText
				'		Else
				'			Me.Combo_Ticker.Text = ""
				'		End If
				'	End If
				'Catch ex As Exception
				'	Me.Combo_Ticker.Text = ""
				'End Try

				' Fields

				Try
					nodelist = XML_TaskDetails.GetElementsByTagName("Fields")
					If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) Then
						Me.Combo_Fields.Text = nodelist(0).InnerText
					Else
						Me.Combo_Fields.Text = ""
					End If
				Catch ex As Exception
					Me.Combo_Fields.Text = ""
				End Try

				' Text_TaskComment
				Try
					nodelist = XML_TaskDetails.GetElementsByTagName("TaskComment")
					If (Not (nodelist Is Nothing)) AndAlso (nodelist.Count > 0) Then
						Text_TaskComment.Text = nodelist(0).InnerText
					Else
						Text_TaskComment.Text = ""
					End If
				Catch ex As Exception
					Text_TaskComment.Text = ""
				End Try

				AddNewRecord = False
				' MainForm.SetComboSelectionLengths(Me)

				Me.btnCancel.Enabled = False
				Me.THIS_FORM_SelectingCombo.Enabled = True

			Catch ex As Exception

				Call MainForm.LogError(Me.Name & ", GetFormData", 0, ex.Message, "Error Showing Data", ex.StackTrace, True)
				Call GetFormData(Nothing)

			End Try

		End If

    ' Allow Field events to trigger before 'InPaint' Is re-set. 
    ' (Should) Prevent Validation errors during Form Draw.
    Application.DoEvents()

    ' Restore 'Paint' flag.
    InPaint = OrgInpaint
    FormChanged = False
    Me.btnSave.Enabled = False

    ' As it says on the can.... 
    Call SetButtonStatus()


	End Sub

    ''' <summary>
    ''' Sets the form data.
    ''' </summary>
    ''' <param name="pConfirm">if set to <c>true</c> [p confirm].</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	Private Function SetFormData(Optional ByVal pConfirm As Boolean = True) As Boolean
		' *************************************************************
		'
		' *************************************************************
		Dim ErrMessage As String
		Dim ErrFlag As Boolean
		Dim ErrStack As String

		ErrMessage = ""
		ErrStack = ""

		' *************************************************************
		' Appropriate Save permission :-
		' *************************************************************

		If (Me.HasUpdatePermission = False) And ((Me.HasInsertPermission = False) Or (Me.AddNewRecord = False)) Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "You do not have permission to save this record.", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' *************************************************************
		' If Save button is disabled then should not be able to save, exit silently.
		' *************************************************************
		If Me.btnSave.Enabled = False Then
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' *************************************************************
		' KnowledgeDate OK :-
		' *************************************************************

		If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Changes to Venice are not allowed when the Knowledgedate is not set to `NOW`", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' Confirm Save, if required.

		If (pConfirm = True) Then
			If MessageBox.Show("Save Changes ?", "Save Changes ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
				Return True
				Exit Function
			End If
		End If

		' *************************************************************
		' Procedure to Save the current form information.
		' *************************************************************

		Dim StatusString As String
		Dim LogString As String
		Dim UpdateRows(0) As DataRow
		Dim Position As Integer
		Dim thisInstrumentID As Integer = 0
		Dim thisPortfolioID As Integer = 0
		Dim ThisDataTaskType As DataTaskType

		If (FormChanged = False) Or (FormIsValid = False) Then
			Return False
			Exit Function
		End If

		' Validation
		StatusString = ""
		If ValidateForm(StatusString) = False Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Validation Error." & vbCrLf & StatusString, "", True)
			Return False
			Exit Function
		End If

		' Check current position in the table.
		Position = Get_Position(thisAuditID)

		' Allow for new or missing ID.
		If Position < 0 Then
			If (AddNewRecord = True) OrElse (SortedRows.Length <= 0) Then
				thisDataRow = myTable.NewRow
				LogString = "Add: "
				AddNewRecord = True
			Else
				Return False
				Exit Function
			End If
		Else
			' Row found OK.
			LogString = "Edit : AuditID = " & thisAuditID.ToString

			thisDataRow = Me.SortedRows(Position)

		End If

		' *************************************************************
		' Generate XML string 

		Dim node As XmlNode
		Dim newNode As XmlNode
		Dim doc As New XmlDocument
		Dim ms As New System.IO.MemoryStream(10000)
		Dim sr As New StreamReader(ms)
		Dim sw As New StreamWriter(ms)
		Dim TaskDefinition As String
		Dim ResolvedTicker As String = ""
		Dim InstrumentTicker As String = ""
		Dim PortfolioTicker As String = ""
		Dim PageTicker As String = ""

		If (doc.DocumentElement Is Nothing) Then
			doc.AppendChild(doc.CreateNode(XmlNodeType.XmlDeclaration, "", ""))
			doc.AppendChild(doc.CreateNode(XmlNodeType.Element, "root", ""))
		Else
			doc.DocumentElement.RemoveAll()
		End If

		ThisDataTaskType = CType(Combo_TaskType.SelectedValue, DataTaskType)
		node = doc.CreateNode(XmlNodeType.Element, "RequestType", "RequestType")
		node.InnerText = System.Enum.GetName(GetType(DataTaskType), ThisDataTaskType)
		doc.DocumentElement.AppendChild(node)

		node = doc.CreateNode(XmlNodeType.Element, "EMAIL_DETAILS", "EMAIL_DETAILS")
		doc.DocumentElement.AppendChild(node)

		newNode = doc.CreateNode(XmlNodeType.Element, "AggregateEmails", "AggregateEmails")
		newNode.InnerText = Check_AggregateNotificationEmails.Checked.ToString
		node.AppendChild(newNode)

		newNode = doc.CreateNode(XmlNodeType.Element, "LimitToMonitorItems", "LimitToMonitorItems")
		newNode.InnerText = Check_PortfolioMonitorItems.Checked.ToString
		node.AppendChild(newNode)

		newNode = doc.CreateNode(XmlNodeType.Element, "PriceMoveTrigger", "PriceMoveTrigger")
		newNode.InnerText = edit_PriceMoveTrigger.Value.ToString
		node.AppendChild(newNode)

		newNode = doc.CreateNode(XmlNodeType.Element, "TaskComment", "TaskComment")
		newNode.InnerText = Me.Text_TaskComment.Text
		node.AppendChild(newNode)

		newNode = doc.CreateNode(XmlNodeType.Element, "EMail_AddressCount", "EMail_AddressCount")
		newNode.InnerText = ListBox_EmailPeople.CheckedIndices.Count.ToString
		node.AppendChild(newNode)

		Try
			Dim ListIndex As Integer
			Dim ListCount As Integer = 1
			Dim thisEMailEntryObject As RenaissanceControls.EMailEntryObject

			For Each ListIndex In ListBox_EmailPeople.CheckedIndices
				If (ListBox_EmailPeople.Items(ListIndex) IsNot Nothing) AndAlso (TypeOf ListBox_EmailPeople.Items(ListIndex) Is RenaissanceControls.EMailEntryObject) Then
					thisEMailEntryObject = CType(ListBox_EmailPeople.Items(ListIndex), RenaissanceControls.EMailEntryObject)

					newNode = doc.CreateNode(XmlNodeType.Element, "EMail_Address_" & ListCount.ToString, "EMail_Address_" & ListCount.ToString)
					newNode.InnerText = thisEMailEntryObject.EMail
					node.AppendChild(newNode)

					newNode = doc.CreateNode(XmlNodeType.Element, "EMail_PersonID_" & ListCount.ToString, "EMail_PersonID_" & ListCount.ToString)
					newNode.InnerText = thisEMailEntryObject.PersonID.ToString
					node.AppendChild(newNode)
				End If

				ListCount += 1
			Next

		Catch ex As Exception
		End Try

		node = doc.CreateNode(XmlNodeType.Element, "SAVE_TIMES", "SAVE_TIMES")
		doc.DocumentElement.AppendChild(node)

		Dim SaveDays As Integer
		SaveDays = 0
		If Me.CheckBox_Mon.Checked Then
			SaveDays = SaveDays Or DataTaskDay.Monday
		End If
		If Me.CheckBox_Tue.Checked Then
			SaveDays = SaveDays Or DataTaskDay.Tuesday
		End If
		If Me.CheckBox_Wed.Checked Then
			SaveDays = SaveDays Or DataTaskDay.Wednesday
		End If
		If Me.CheckBox_Thu.Checked Then
			SaveDays = SaveDays Or DataTaskDay.Thursday
		End If
		If Me.CheckBox_Fri.Checked Then
			SaveDays = SaveDays Or DataTaskDay.Friday
		End If
		If Me.CheckBox_Sat.Checked Then
			SaveDays = SaveDays Or DataTaskDay.Saturday
		End If
		If Me.CheckBox_Sun.Checked Then
			SaveDays = SaveDays Or DataTaskDay.Sunday
		End If

		newNode = doc.CreateNode(XmlNodeType.Element, "SaveDays", "SaveDays")
		newNode.InnerText = SaveDays.ToString
		node.AppendChild(newNode)

		newNode = doc.CreateNode(XmlNodeType.Element, "Frequency", "Frequency")	' 'Once' or 'Interval'
		If Me.Radio_OccursEvery.Checked Then
			newNode.InnerText = "Interval"
		Else
			newNode.InnerText = "Once"
		End If
		node.AppendChild(newNode)


		newNode = doc.CreateNode(XmlNodeType.Element, "StartTime", "StartTime")
		If Me.Radio_OccursEvery.Checked Then
			newNode.InnerText = Me.DT_TimeStarts.Value.ToString(TIMEFORMAT)
		Else
			newNode.InnerText = Me.DT_OccursOnce.Value.ToString(TIMEFORMAT)
		End If

		node.AppendChild(newNode)
		newNode = doc.CreateNode(XmlNodeType.Element, "EndTime", "EndTime")
		If Me.Radio_OccursEvery.Checked Then
			newNode.InnerText = Me.DT_TimeEnds.Value.ToString(TIMEFORMAT)
		Else
			newNode.InnerText = Me.DT_OccursOnce.Value.AddMinutes(1).ToString(TIMEFORMAT)
		End If
		'newNode.InnerText = "18:00:00"
		node.AppendChild(newNode)

		newNode = doc.CreateNode(XmlNodeType.Element, "Interval", "Interval")
		newNode.InnerText = (Numeric_MinuteCount.Value * 60).ToString
		node.AppendChild(newNode)

		node = doc.CreateNode(XmlNodeType.Element, "TICKER", "TICKER")
		doc.DocumentElement.AppendChild(node)

		ResolvedTicker = ""
		PortfolioTicker = ""
		InstrumentTicker = ""
		PageTicker = ""

		Select Case ThisDataTaskType

			Case DataTaskType.PortfolioCapture
				If IsNumeric(Combo_Ticker.SelectedValue) Then
					thisInstrumentID = CInt(Combo_Ticker.SelectedValue)
					PortfolioTicker = LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, thisInstrumentID, "InstrumentPortfolioTicker")
				Else
					PortfolioTicker = Combo_Ticker.Text
				End If
				ResolvedTicker = PortfolioTicker
				InstrumentTicker = PortfolioTicker

			Case DataTaskType.PeriodicSecurityCapture, DataTaskType.RealtimeSecurityCapture
				If IsNumeric(Combo_Ticker.SelectedValue) Then
					thisInstrumentID = CInt(Combo_Ticker.SelectedValue)
					InstrumentTicker = LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, thisInstrumentID, "InstrumentExchangeCode")
				Else
					InstrumentTicker = Combo_Ticker.Text
				End If
				ResolvedTicker = InstrumentTicker

			Case DataTaskType.PeriodicMarketSecurityCapture, DataTaskType.RealtimeMarketSecurityCapture
				If IsNumeric(Combo_Ticker.SelectedValue) Then
					thisInstrumentID = CInt(Combo_Ticker.SelectedValue)
					InstrumentTicker = LookupTableValue(MainForm, RenaissanceStandardDatasets.tblMarketInstruments, thisInstrumentID, "Ticker")
				Else
					InstrumentTicker = Combo_Ticker.Text
				End If
				ResolvedTicker = InstrumentTicker

			Case DataTaskType.PageCapture
				PageTicker = Combo_Ticker.Text
				ResolvedTicker = PageTicker

			Case DataTaskType.EmailPortfolioStockChangeNotification
				If IsNumeric(Combo_Ticker.SelectedValue) Then
					thisPortfolioID = CInt(Combo_Ticker.SelectedValue)
					PortfolioTicker = LookupTableValue(MainForm, RenaissanceStandardDatasets.tblPortfolioIndex, thisPortfolioID, "PortfolioTicker")
				Else
					PortfolioTicker = Combo_Ticker.Text
				End If
				ResolvedTicker = PortfolioTicker

			Case DataTaskType.EmailSingleStockChangeNotification
				If IsNumeric(Combo_Ticker.SelectedValue) Then
					thisInstrumentID = CInt(Combo_Ticker.SelectedValue)
					InstrumentTicker = LookupTableValue(MainForm, RenaissanceStandardDatasets.tblMarketInstruments, thisInstrumentID, "Ticker")
				Else
					InstrumentTicker = Combo_Ticker.Text
				End If
				ResolvedTicker = InstrumentTicker

			Case Else
				If IsNumeric(Combo_Ticker.SelectedValue) Then
					thisInstrumentID = CInt(Combo_Ticker.SelectedValue)
				End If
				InstrumentTicker = Combo_Ticker.Text
				PortfolioTicker = Combo_Ticker.Text
				PageTicker = Combo_Ticker.Text
				ResolvedTicker = Combo_Ticker.Text

		End Select


		'If (Combo_Ticker.SelectedIndex >= 0) Then
		'  thisInstrumentID = CInt(Combo_Ticker.SelectedValue)
		'  If (CInt(Me.Combo_TaskType.SelectedValue) = CInt(DataTaskType.PortfolioCapture)) Then
		'    newNode.InnerText = LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, thisInstrumentID, "InstrumentPortfolioTicker")
		'  Else
		'    newNode.InnerText = LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, thisInstrumentID, "InstrumentExchangeCode")
		'  End If

		'  If (newNode.InnerText Is Nothing) OrElse (newNode.InnerText.Length <= 0) Then
		'    newNode.InnerText = Me.Combo_Ticker.Text
		'  End If
		'Else
		'  thisInstrumentID = 0
		'  newNode.InnerText = Me.Combo_Ticker.Text
		'End If
		'ResolvedTicker = newNode.InnerText


		newNode = doc.CreateNode(XmlNodeType.Element, "Security", "Security")	' Same as 'Security' (for backwards compatability)
		newNode.InnerText = InstrumentTicker
		node.AppendChild(newNode)

		newNode = doc.CreateNode(XmlNodeType.Element, "Instrument", "Instrument")
		newNode.InnerText = InstrumentTicker
		node.AppendChild(newNode)

		node.AppendChild(newNode)
		newNode = doc.CreateNode(XmlNodeType.Element, "InstrumentID", "InstrumentID")
		newNode.InnerText = thisInstrumentID.ToString
		node.AppendChild(newNode)

		newNode = doc.CreateNode(XmlNodeType.Element, "Portfolio", "Portfolio")
		newNode.InnerText = PortfolioTicker
		node.AppendChild(newNode)

		newNode = doc.CreateNode(XmlNodeType.Element, "PortfolioID", "PortfolioID")
		newNode.InnerText = thisPortfolioID.ToString
		node.AppendChild(newNode)

		newNode = doc.CreateNode(XmlNodeType.Element, "PageCode", "PageCode")
		newNode.InnerText = PageTicker
		node.AppendChild(newNode)

		newNode = doc.CreateNode(XmlNodeType.Element, "Fields", "Fields")
		newNode.InnerText = Me.Combo_Fields.Text
		node.AppendChild(newNode)

		doc.Save(sw)
		ms.Seek(0, SeekOrigin.Begin)
		TaskDefinition = sr.ReadToEnd

		' *************************************************************

		' Set 'Paint' flag.
		InPaint = True

		' *************************************************************
		' Lock the Data Table, to prevent update conflicts.
		' *************************************************************
		SyncLock myTable

			' Initiate Edit,
			thisDataRow.BeginEdit()


			thisDataRow.TaskDescription = Me.editTaskDescription.Text
			LogString &= ", TaskDescription = " & thisDataRow.TaskDescription

			thisDataRow.TaskType = Me.Combo_TaskType.SelectedValue
			thisDataRow.InstrumentID = thisInstrumentID
			thisDataRow.SecurityTicker = ResolvedTicker
			thisDataRow.TaskDefinition = TaskDefinition

			thisDataRow.EndEdit()
			InPaint = False

			' Add and Update DataRow. 

			ErrFlag = False

			If AddNewRecord = True Then
				Try
					myTable.Rows.Add(thisDataRow)
				Catch ex As Exception
					ErrFlag = True
					ErrMessage = ex.Message
					ErrStack = ex.StackTrace
				End Try
			End If

			UpdateRows(0) = thisDataRow

			' Post Additions / Updates to the underlying table :-
			Dim temp As Integer
			Try
				If (ErrFlag = False) Then
					myAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
					myAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

					temp = MainForm.AdaptorUpdate(Me.Name, myAdaptor, UpdateRows)

				End If

			Catch ex As Exception

				ErrMessage = ex.Message
				ErrFlag = True
				ErrStack = ex.StackTrace

			End Try

			thisAuditID = thisDataRow.AuditID


		End SyncLock

		' *************************************************************
		' Error report. Not done in the Try-Catch as that would serve to persist the SyncLock.
		' *************************************************************

		If (ErrFlag = True) Then
			Call MainForm.LogError(Me.Name & ", SetFormData", 0, ErrMessage, "Error Saving Data", ErrStack, True)
		End If

		' Finish off

		AddNewRecord = False
		FormChanged = False

		Me.THIS_FORM_SelectingCombo.Enabled = True

		' Propogate changes

		'Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID))
		Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID), True)

	End Function

    ''' <summary>
    ''' Sets the button status.
    ''' </summary>
	Private Sub SetButtonStatus()
		' Sets the status of the form controlls appropriate to the current users 
		'permissions and the 'Changed' or 'New' status of the form.

		' No Read permission :-

		If Me.HasReadPermission = False Then
			MainForm.LogError(Me.Name & ", SetButtonStatus()", 0, "", "You do not have Read permission for this Form", "", True)
			Me.Close()
			Exit Sub
		End If

		' Has Insert Permission.
		If Me.HasInsertPermission Then
			Me.btnAdd.Enabled = True
		Else
			Me.btnAdd.Enabled = False
		End If

		' Has Delete permission. 
		If (Me.HasDeletePermission) And (Me.AddNewRecord = False) And (thisPosition >= 0) Then
			Me.btnDelete.Enabled = True
		Else
			Me.btnDelete.Enabled = False
		End If

		If (Me.MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And _
			((Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord)) Then

			Me.editTaskDescription.Enabled = True
			editTaskDescription.Enabled = True
			Combo_TaskType.Enabled = True
			Combo_Ticker.Enabled = True
			Combo_Fields.Enabled = True
			CheckBox_Mon.Enabled = True
			CheckBox_Tue.Enabled = True
			CheckBox_Wed.Enabled = True
			CheckBox_Thu.Enabled = True
			CheckBox_Fri.Enabled = True
			CheckBox_Sat.Enabled = True
			CheckBox_Sun.Enabled = True
			Radio_OccursOnce.Enabled = True
			Radio_OccursEvery.Enabled = True
			DT_OccursOnce.Enabled = True
			Numeric_MinuteCount.Enabled = True
			DT_TimeStarts.Enabled = True
			DT_TimeEnds.Enabled = True

		Else

			Me.editTaskDescription.Enabled = False
			editTaskDescription.Enabled = False
			Combo_TaskType.Enabled = False
			Combo_Ticker.Enabled = False
			Combo_Fields.Enabled = False
			CheckBox_Mon.Enabled = False
			CheckBox_Tue.Enabled = False
			CheckBox_Wed.Enabled = False
			CheckBox_Thu.Enabled = False
			CheckBox_Fri.Enabled = False
			CheckBox_Sat.Enabled = False
			CheckBox_Sun.Enabled = False
			Radio_OccursOnce.Enabled = False
			Radio_OccursEvery.Enabled = False
			DT_OccursOnce.Enabled = False
			Numeric_MinuteCount.Enabled = False
			DT_TimeStarts.Enabled = False
			DT_TimeEnds.Enabled = False

		End If

	End Sub

    ''' <summary>
    ''' Validates the form.
    ''' </summary>
    ''' <param name="pReturnString">The p return string.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	Private Function ValidateForm(ByRef pReturnString As String) As Boolean
		' Form Validation code.
		' 
		' This code should be the final arbiter of what is allowed. no assumptions regarding 
		' prior validation should be made.
		' 
		' This Code is called by the SetFormData routine before position changes.
		'
		Dim RVal As Boolean

		RVal = True
		pReturnString = ""

		' Validate 

		If Me.editTaskDescription.Text.Length <= 0 Then
			pReturnString = "DataTask Description must not be left blank."
			RVal = False
		End If

		If (Me.Combo_TaskType.SelectedIndex < 0) AndAlso (RVal) Then
			pReturnString = "Task Type must not be left blank."
			RVal = False
		End If

		If (Me.Combo_Ticker.Text.Length <= 0) AndAlso (RVal) Then
			pReturnString = "Ticker / Portfolio must not be left blank."
			RVal = False
		End If




		Return RVal

	End Function

    ''' <summary>
    ''' Handles the MouseEnter event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnCancel_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseEnter
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = True
	End Sub

    ''' <summary>
    ''' Handles the MouseLeave event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnCancel_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseLeave
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = False
	End Sub


#End Region

#Region " Navigation Code / GetPosition() (Generic Code) "

    ''' <summary>
    ''' Handles the SelectComboChanged event of the Combo control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_SelectComboChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' Selection Combo. SelectedItem changed.
    '

    ' Don't react to changes made in paint routines etc.
    If InPaint = True Then Exit Sub

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    ' Find the correct data row, then show it...
    thisPosition = THIS_FORM_SelectingCombo.SelectedIndex

    If (thisPosition >= 0) Then
      thisDataRow = Me.SortedRows(thisPosition)
    Else
      thisDataRow = Nothing
    End If

    Call GetFormData(thisDataRow)

  End Sub


    ''' <summary>
    ''' Handles the Click event of the btnNavPrev control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnNavPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavPrev.Click
    ' 'Previous' Button.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    If thisPosition > 0 Then thisPosition -= 1
    If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
      thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
    End If

    THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

    Exit Sub

    If myTable.Rows.Count > thisPosition Then
      thisDataRow = myTable.Rows(thisPosition)
      Call GetFormData(thisDataRow)
    Else
      Call GetFormData(Nothing)
    End If

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnNavNext control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnNavNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavNext.Click
    ' 'Next' Button.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    If thisPosition < (myTable.Rows.Count - 1) Then thisPosition += 1
    If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
      thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
    End If

    THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

    Exit Sub

    If myTable.Rows.Count > thisPosition Then
      thisDataRow = myTable.Rows(thisPosition)
      Call GetFormData(thisDataRow)
    Else
      Call GetFormData(Nothing)
    End If

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnNavFirst control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnNavFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavFirst.Click
    ' 'First' Button.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    thisPosition = 0
    If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
      thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
    End If

    If (THIS_FORM_SelectingCombo.SelectedIndex = thisPosition) Then
      Combo_SelectComboChanged(THIS_FORM_SelectingCombo, New EventArgs)
    Else
      THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
    End If

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnLast control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLast.Click
    ' 'Last' Button.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    thisPosition = THIS_FORM_SelectingCombo.Items.Count - 1

    THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

  End Sub

    ''' <summary>
    ''' Get_s the position.
    ''' </summary>
    ''' <param name="pAuditID">The p audit ID.</param>
    ''' <returns>System.Int32.</returns>
  Private Function Get_Position(ByVal pAuditID As Integer) As Integer
    ' Returns the position in the current 'SortedRows' array of the entry corresponding to the given 
    ' AudidID.

    Dim MinIndex As Integer
    Dim MaxIndex As Integer
    Dim CurrentMin As Integer
    Dim CurrentMax As Integer
    Dim searchDataRow As DataRow

    Try
      ' SortedRows exists ?

      If SortedRows Is Nothing Then
        Return (-1)
        Exit Function
      End If

      ' Return (-1) if there are no rows in the 'SortedRows'

      If (SortedRows.GetLength(0) <= 0) Then
        Return (-1)
        Exit Function
      End If


      ' Use a modified Search moving outwards from the last returned value to locate the required value.
      ' Reflecting the fact that for the most part One looks for closely located records.

      MinIndex = 0
      MaxIndex = SortedRows.GetLength(0) - 1


      ' Check First and Last records (Incase 'First' or 'Last' was pressed).

      searchDataRow = SortedRows(MinIndex)

      If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
        Return MinIndex
        Exit Function
      End If

      searchDataRow = SortedRows(MaxIndex)
      If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
        Return MaxIndex
        Exit Function
      End If

      ' now search outwards from the last returned value.

      If (Me.thisPosition >= MinIndex) And (Me.thisPosition <= MaxIndex) Then
        CurrentMin = thisPosition
        CurrentMax = CurrentMin + 1
      Else
        CurrentMin = CInt((MinIndex + MaxIndex) / 2)
        CurrentMax = CurrentMin + 1
      End If

      While (CurrentMin >= MinIndex) Or (CurrentMax <= MaxIndex)
        If (CurrentMin >= MinIndex) Then
          searchDataRow = SortedRows(CurrentMin)

          If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
            Return CurrentMin
            Exit Function
          End If

          CurrentMin -= 1
        End If

        If (CurrentMax <= MaxIndex) Then
          searchDataRow = SortedRows(CurrentMax)

          If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
            Return CurrentMax
            Exit Function
          End If

          CurrentMax += 1
        End If

      End While

      Return (-1)
      Exit Function

    Catch ex As Exception
      MainForm.LogError(Me.Name & ", Get_Position()", LOG_LEVELS.Warning, ex.Message, "", ex.StackTrace, True)
      Return (-1)
      Exit Function
    End Try

  End Function

#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

    ''' <summary>
    ''' Handles the Click event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    ' Cancel Changes, redisplay form.

    FormChanged = False
    AddNewRecord = False

    Me.THIS_FORM_SelectingCombo.Enabled = True

    If (thisPosition >= 0) And (thisPosition < Me.SortedRows.GetLength(0)) Then
      thisDataRow = Me.SortedRows(thisPosition)
      Call GetFormData(thisDataRow)
    Else
      Call btnNavFirst_Click(Me, New System.EventArgs)
    End If

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnSave control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
    ' Save Changes, if any, without prompting.

    If (FormChanged = True) Then
      Call SetFormData(False)
    End If

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnDelete control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
    Dim Position As Integer
    Dim NextAuditID As Integer

    If (AddNewRecord = True) Then
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Sub
    End If

    ' No Appropriate Save permission :-

    If (Me.HasDeletePermission = False) Then
      MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "You do not have permission to Delete this record.", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Sub
    End If

    ' *************************************************************
    ' KnowledgeDate OK :-
    ' *************************************************************

    If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
      MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "Changes to Venice are not allowed when the Knowledgedate is not set to `NOW`", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Sub
    End If

    ' *************************************************************
    ' Confirm :-
    ' *************************************************************
    If MsgBox("Are you Sure ?", MsgBoxStyle.OkCancel, "Confirm Delete") = MsgBoxResult.Cancel Then
      Exit Sub
    End If

    ' Check Data position.

    Position = Get_Position(thisAuditID)

    If Position < 0 Then Exit Sub

    ' Check Referential Integrity 
    If CheckReferentialIntegrity(_MainForm, Me.SortedRows(Position)) = False Then
      MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "Deleting this record would voilate referential integrity.", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Sub
    End If

    ' Resolve row to show after deleting this one.

    NextAuditID = (-1)
    If (Position + 1) < Me.SortedRows.GetLength(0) Then
      NextAuditID = CInt(Me.SortedRows(Position + 1)("AuditID"))
    ElseIf (Position - 1) >= 0 Then
      NextAuditID = CInt(Me.SortedRows(Position - 1)("AuditID"))
    Else
      NextAuditID = (-1)
    End If

    ' Delete this row

    Try
      Me.SortedRows(Position).Delete()

      MainForm.AdaptorUpdate(Me.Name, myAdaptor, myTable)

    Catch ex As Exception

      Call MainForm.LogError(Me.Name & ", Delete()", LOG_LEVELS.Error, ex.Message, "Error Deleting Record.", ex.StackTrace, True)
    End Try

    ' Tidy Up.

    FormChanged = False

    thisAuditID = NextAuditID
		'Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID))
		Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID), True)

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnAdd control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
    ' Prepare form to Add a new record.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    thisPosition = -1
    InPaint = True
    MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
    InPaint = False

    GetFormData(Nothing)
    AddNewRecord = True
    Me.btnCancel.Enabled = True
    Me.THIS_FORM_SelectingCombo.Enabled = False

    Call SetButtonStatus()

    THIS_FORM_NewMoveToControl.Focus()

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnClose control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
    ' Close Form

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    Me.Close()

  End Sub

#End Region

#Region " Bug hunting "

  ' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
  ' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
  ' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

    ''' <summary>
    ''' Called when [row updating].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

    ''' <summary>
    ''' Called when [row updated].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

    ''' <summary>
    ''' Called when [row fill error].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
    Dim a As Integer

    a = 1
  End Sub

#End Region


  
End Class
