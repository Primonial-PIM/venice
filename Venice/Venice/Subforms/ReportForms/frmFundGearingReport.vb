' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmFundGearingReport.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals


''' <summary>
''' Class frmFundGearingReport
''' </summary>
Public Class frmFundGearingReport

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmFundGearingReport"/> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The BTN run report
    ''' </summary>
  Friend WithEvents btnRunReport As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN close
    ''' </summary>
  Friend WithEvents btnClose As System.Windows.Forms.Button
    ''' <summary>
    ''' The label2
    ''' </summary>
  Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ fund
    ''' </summary>
  Friend WithEvents Combo_Fund As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The panel5
    ''' </summary>
  Friend WithEvents Panel5 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The date_ report date
    ''' </summary>
  Friend WithEvents Date_ReportDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The label1
    ''' </summary>
  Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label3
    ''' </summary>
  Friend WithEvents Label3 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ milestone date
    ''' </summary>
  Friend WithEvents Combo_MilestoneDate As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label5
    ''' </summary>
  Friend WithEvents Label5 As System.Windows.Forms.Label
    ''' <summary>
    ''' The radio_ from precise time
    ''' </summary>
  Friend WithEvents Radio_FromPreciseTime As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The radio_ from whole day
    ''' </summary>
  Friend WithEvents Radio_FromWholeDay As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The date_ knowledge date from
    ''' </summary>
  Friend WithEvents Date_KnowledgeDateFrom As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The radio_ from live
    ''' </summary>
  Friend WithEvents Radio_FromLive As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The form_ status strip
    ''' </summary>
  Friend WithEvents Form_StatusStrip As System.Windows.Forms.StatusStrip
    ''' <summary>
    ''' The form_ progress bar
    ''' </summary>
  Friend WithEvents Form_ProgressBar As System.Windows.Forms.ToolStripProgressBar
    ''' <summary>
    ''' The group box1
    ''' </summary>
  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The check_ save unit prices
    ''' </summary>
  Friend WithEvents Check_SaveUnitPrices As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The label_ status
    ''' </summary>
  Friend WithEvents Label_Status As System.Windows.Forms.ToolStripStatusLabel
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.btnRunReport = New System.Windows.Forms.Button
    Me.btnClose = New System.Windows.Forms.Button
    Me.Label2 = New System.Windows.Forms.Label
    Me.Combo_Fund = New System.Windows.Forms.ComboBox
    Me.Panel5 = New System.Windows.Forms.Panel
    Me.Radio_FromLive = New System.Windows.Forms.RadioButton
    Me.Radio_FromPreciseTime = New System.Windows.Forms.RadioButton
    Me.Radio_FromWholeDay = New System.Windows.Forms.RadioButton
    Me.Date_KnowledgeDateFrom = New System.Windows.Forms.DateTimePicker
    Me.Date_ReportDate = New System.Windows.Forms.DateTimePicker
    Me.Label1 = New System.Windows.Forms.Label
    Me.Label3 = New System.Windows.Forms.Label
    Me.Combo_MilestoneDate = New System.Windows.Forms.ComboBox
    Me.Label5 = New System.Windows.Forms.Label
    Me.Form_StatusStrip = New System.Windows.Forms.StatusStrip
    Me.Form_ProgressBar = New System.Windows.Forms.ToolStripProgressBar
    Me.Label_Status = New System.Windows.Forms.ToolStripStatusLabel
    Me.GroupBox1 = New System.Windows.Forms.GroupBox
    Me.Check_SaveUnitPrices = New System.Windows.Forms.CheckBox
    Me.Panel5.SuspendLayout()
    Me.Form_StatusStrip.SuspendLayout()
    Me.GroupBox1.SuspendLayout()
    Me.SuspendLayout()
    '
    'btnRunReport
    '
    Me.btnRunReport.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnRunReport.Location = New System.Drawing.Point(50, 221)
    Me.btnRunReport.Name = "btnRunReport"
    Me.btnRunReport.Size = New System.Drawing.Size(148, 28)
    Me.btnRunReport.TabIndex = 4
    Me.btnRunReport.Text = "Run Report"
    '
    'btnClose
    '
    Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnClose.Location = New System.Drawing.Point(266, 221)
    Me.btnClose.Name = "btnClose"
    Me.btnClose.Size = New System.Drawing.Size(75, 28)
    Me.btnClose.TabIndex = 5
    Me.btnClose.Text = "&Close"
    '
    'Label2
    '
    Me.Label2.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label2.Location = New System.Drawing.Point(16, 16)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(120, 20)
    Me.Label2.TabIndex = 53
    Me.Label2.Text = "Fund Name"
    '
    'Combo_Fund
    '
    Me.Combo_Fund.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Fund.Location = New System.Drawing.Point(140, 12)
    Me.Combo_Fund.Name = "Combo_Fund"
    Me.Combo_Fund.Size = New System.Drawing.Size(252, 21)
    Me.Combo_Fund.TabIndex = 0
    '
    'Panel5
    '
    Me.Panel5.Controls.Add(Me.Radio_FromLive)
    Me.Panel5.Controls.Add(Me.Radio_FromPreciseTime)
    Me.Panel5.Controls.Add(Me.Radio_FromWholeDay)
    Me.Panel5.Location = New System.Drawing.Point(10, 43)
    Me.Panel5.Name = "Panel5"
    Me.Panel5.Size = New System.Drawing.Size(372, 24)
    Me.Panel5.TabIndex = 1
    '
    'Radio_FromLive
    '
    Me.Radio_FromLive.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Radio_FromLive.Checked = True
    Me.Radio_FromLive.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Radio_FromLive.Location = New System.Drawing.Point(12, 4)
    Me.Radio_FromLive.Name = "Radio_FromLive"
    Me.Radio_FromLive.Size = New System.Drawing.Size(104, 16)
    Me.Radio_FromLive.TabIndex = 0
    Me.Radio_FromLive.TabStop = True
    Me.Radio_FromLive.Text = "'Live' KD"
    '
    'Radio_FromPreciseTime
    '
    Me.Radio_FromPreciseTime.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Radio_FromPreciseTime.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Radio_FromPreciseTime.Location = New System.Drawing.Point(252, 4)
    Me.Radio_FromPreciseTime.Name = "Radio_FromPreciseTime"
    Me.Radio_FromPreciseTime.Size = New System.Drawing.Size(104, 16)
    Me.Radio_FromPreciseTime.TabIndex = 2
    Me.Radio_FromPreciseTime.Text = "Precise Time"
    '
    'Radio_FromWholeDay
    '
    Me.Radio_FromWholeDay.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Radio_FromWholeDay.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Radio_FromWholeDay.Location = New System.Drawing.Point(128, 4)
    Me.Radio_FromWholeDay.Name = "Radio_FromWholeDay"
    Me.Radio_FromWholeDay.Size = New System.Drawing.Size(104, 16)
    Me.Radio_FromWholeDay.TabIndex = 1
    Me.Radio_FromWholeDay.Text = "WholeDay"
    '
    'Date_KnowledgeDateFrom
    '
    Me.Date_KnowledgeDateFrom.CustomFormat = "dd MMM yyyy"
    Me.Date_KnowledgeDateFrom.Enabled = False
    Me.Date_KnowledgeDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom
    Me.Date_KnowledgeDateFrom.Location = New System.Drawing.Point(134, 19)
    Me.Date_KnowledgeDateFrom.Name = "Date_KnowledgeDateFrom"
    Me.Date_KnowledgeDateFrom.Size = New System.Drawing.Size(252, 20)
    Me.Date_KnowledgeDateFrom.TabIndex = 0
    '
    'Date_ReportDate
    '
    Me.Date_ReportDate.Location = New System.Drawing.Point(140, 41)
    Me.Date_ReportDate.Name = "Date_ReportDate"
    Me.Date_ReportDate.Size = New System.Drawing.Size(160, 20)
    Me.Date_ReportDate.TabIndex = 1
    '
    'Label1
    '
    Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label1.Location = New System.Drawing.Point(16, 45)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(116, 16)
    Me.Label1.TabIndex = 108
    Me.Label1.Text = "Report Date"
    '
    'Label3
    '
    Me.Label3.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label3.Location = New System.Drawing.Point(10, 23)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(116, 16)
    Me.Label3.TabIndex = 112
    Me.Label3.Text = "KnowledgeDate"
    '
    'Combo_MilestoneDate
    '
    Me.Combo_MilestoneDate.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_MilestoneDate.Location = New System.Drawing.Point(134, 81)
    Me.Combo_MilestoneDate.Name = "Combo_MilestoneDate"
    Me.Combo_MilestoneDate.Size = New System.Drawing.Size(252, 21)
    Me.Combo_MilestoneDate.TabIndex = 2
    '
    'Label5
    '
    Me.Label5.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label5.Location = New System.Drawing.Point(10, 85)
    Me.Label5.Name = "Label5"
    Me.Label5.Size = New System.Drawing.Size(100, 20)
    Me.Label5.TabIndex = 114
    Me.Label5.Text = "Milestone Dates"
    '
    'Form_StatusStrip
    '
    Me.Form_StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Form_ProgressBar, Me.Label_Status})
    Me.Form_StatusStrip.Location = New System.Drawing.Point(0, 262)
    Me.Form_StatusStrip.Name = "Form_StatusStrip"
    Me.Form_StatusStrip.Size = New System.Drawing.Size(402, 22)
    Me.Form_StatusStrip.TabIndex = 129
    Me.Form_StatusStrip.Text = " "
    '
    'Form_ProgressBar
    '
    Me.Form_ProgressBar.Maximum = 20
    Me.Form_ProgressBar.Name = "Form_ProgressBar"
    Me.Form_ProgressBar.Size = New System.Drawing.Size(150, 17)
    Me.Form_ProgressBar.Step = 1
    Me.Form_ProgressBar.Visible = False
    '
    'Label_Status
    '
    Me.Label_Status.Name = "Label_Status"
    Me.Label_Status.Size = New System.Drawing.Size(10, 17)
    Me.Label_Status.Text = " "
    '
    'GroupBox1
    '
    Me.GroupBox1.Controls.Add(Me.Date_KnowledgeDateFrom)
    Me.GroupBox1.Controls.Add(Me.Panel5)
    Me.GroupBox1.Controls.Add(Me.Combo_MilestoneDate)
    Me.GroupBox1.Controls.Add(Me.Label3)
    Me.GroupBox1.Controls.Add(Me.Label5)
    Me.GroupBox1.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.GroupBox1.Location = New System.Drawing.Point(6, 70)
    Me.GroupBox1.Name = "GroupBox1"
    Me.GroupBox1.Size = New System.Drawing.Size(392, 111)
    Me.GroupBox1.TabIndex = 2
    Me.GroupBox1.TabStop = False
    '
    'Check_SaveUnitPrices
    '
    Me.Check_SaveUnitPrices.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_SaveUnitPrices.Location = New System.Drawing.Point(16, 187)
    Me.Check_SaveUnitPrices.Name = "Check_SaveUnitPrices"
    Me.Check_SaveUnitPrices.Size = New System.Drawing.Size(352, 20)
    Me.Check_SaveUnitPrices.TabIndex = 3
    Me.Check_SaveUnitPrices.Text = "Save Unit prices before Attribution calculation"
    '
    'frmFundGearingReport
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(402, 284)
    Me.Controls.Add(Me.Check_SaveUnitPrices)
    Me.Controls.Add(Me.GroupBox1)
    Me.Controls.Add(Me.Form_StatusStrip)
    Me.Controls.Add(Me.Date_ReportDate)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.Combo_Fund)
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.btnClose)
    Me.Controls.Add(Me.btnRunReport)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frmFundGearingReport"
    Me.Text = "Fund Gearing Report"
    Me.Panel5.ResumeLayout(False)
    Me.Form_StatusStrip.ResumeLayout(False)
    Me.Form_StatusStrip.PerformLayout()
    Me.GroupBox1.ResumeLayout(False)
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "


  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
  Private WithEvents _MainForm As VeniceMain

  ' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
  Private FormTooltip As New ToolTip()

    ''' <summary>
    ''' The report worker
    ''' </summary>
	Private ReportWorker As BackgroundWorkerThreadSafe = Nothing
    ''' <summary>
    ''' The form controls
    ''' </summary>
  Private FormControls As ArrayList = Nothing
    ''' <summary>
    ''' The report timer
    ''' </summary>
  Private WithEvents ReportTimer As New Windows.Forms.Timer()

  ' Form Constants, specific to the table being updated.

    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
  Private ALWAYS_CLOSE_THIS_FORM As Boolean = True

  ' The standard ChangeID for this form. e.g. tblPerson
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

  ' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
  Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
  Private THIS_FORM_FormID As VeniceFormID

  ' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
  Private FormIsValid As Boolean
    ''' <summary>
    ''' The form changed
    ''' </summary>
  Private FormChanged As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
  Private _FormOpenFailed As Boolean

  ' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
  Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
  Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
  Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
  Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
  Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
    ' Public property to return handle to the 'Main' Venice form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
  Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
    ' Public property maintaining a value indicating if the cursor is over the 'Cancel'
    ' Button on this form.
    ' This property is specifically designed for use by the field formating Event functions
    ' In order that they do not impose format restrictions if the user is about to click the 
    ' 'Cancel' button.
    '
    Get
      Return False
    End Get
    Set(ByVal Value As Boolean)
    End Set
  End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
  Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
    Get
      Return False
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
    Get
      Return True
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmFundGearingReport"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  Public Sub New(ByVal pMainForm As VeniceMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()

    _MainForm = pMainForm
    AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate
    AddHandler ReportTimer.Tick, AddressOf MainForm.IncrementStatusBarTimerEvent

    _FormOpenFailed = False

    ' ******************************************************
    ' Form Specific Settings :
    ' ******************************************************

    ' Form Permissioning :-

    THIS_FORM_PermissionArea = Me.Name
    THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

    ' 'This' form ID

    THIS_FORM_FormID = VeniceFormID.frmFundGearingReport

    ' Format Event Handlers for form controls

    AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

    AddHandler Combo_Fund.KeyUp, AddressOf MainForm.ComboSelectAsYouType

    ' Set up the ToolTip
    MainForm.SetFormToolTip(Me, FormTooltip)

    ' ******************************************************
    ' End Form Specific.
    ' ******************************************************

    THIS_FORM_ChangeID = RenaissanceChangeID.KnowledgeDate

  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
  Public Sub ResetForm() Implements StandardVeniceForm.ResetForm

    Call Form_Load(Me, New System.EventArgs)
  End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
  Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
    ALWAYS_CLOSE_THIS_FORM = True
    Me.Close()
  End Sub

    ''' <summary>
    ''' Handles the FormClosing event of the frmFundGearingReport control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.FormClosingEventArgs"/> instance containing the event data.</param>
  Private Sub frmFundGearingReport_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
    If (ReportWorker IsNot Nothing) Then
      If ReportWorker.IsBusy Then
        e.Cancel = True
        Exit Sub
      End If

      Try
        RemoveHandler ReportTimer.Tick, AddressOf MainForm.IncrementStatusBarTimerEvent
        RemoveHandler ReportWorker.DoWork, AddressOf ReportWorker_DoWork
        RemoveHandler ReportWorker.RunWorkerCompleted, AddressOf ReportWorkerCompleted
        ReportWorker.Dispose()
        ReportWorker = Nothing
      Catch ex As Exception
      End Try
    End If

  End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
    _FormOpenFailed = False

    If Not (MainForm Is Nothing) Then
      FormIsValid = True
    Else
      MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If


    ' Check User permissions
    Call CheckPermissions()
    If (HasReadPermission = False) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    ' Build Combos

    Try
      Call SetFundCombo()
      MainForm.SetComboSelectionLengths(Me)

      Call SetMilestoneDateCombo()
    Catch ex As Exception
    End Try


    ' Default KnowledgeDate values

    Me.Radio_FromLive.Checked = True
    Me.Date_KnowledgeDateFrom.Value = Now.Date

    ' Default Report Date Values - End Of month dates for the 'Current'ish month .
    ' Set this after the default values above as the selection of a report date may set Report KD values.

    Dim TempDate As Date
    TempDate = Now.Date.AddDays(-10)

    TempDate = TempDate.AddDays(1 - TempDate.Day)
    TempDate = TempDate.AddMonths(1).AddDays(-1)

    If Me.Combo_Fund.Items.Count > 0 Then
      Me.Combo_Fund.SelectedIndex = 0
    End If
    Me.Date_ReportDate.Value = TempDate


  End Sub

    ''' <summary>
    ''' Handles the Closing event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
  Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    Dim HideForm As Boolean

    ' Hide or Close this form ?
    ' All depends on how many of this form type are Open or in Cache...

    If (btnClose.Enabled = False) Then
      e.Cancel = True
      Exit Sub
    End If

    If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
      HideForm = False
    Else
      HideForm = True
      If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.ReportForm_CacheCount Then
        HideForm = False
      End If
    End If

    If HideForm = True Then
      MainForm.HideInFormsCollection(Me)
      Me.Hide() ' NPP Fix

      e.Cancel = True
    Else
      Try
        MainForm.RemoveFromFormsCollection(Me)
        RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

        RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

        RemoveHandler Combo_Fund.KeyUp, AddressOf MainForm.ComboSelectAsYouType


      Catch ex As Exception
      End Try
    End If

  End Sub


#End Region

  ' Routine to handle changes / updates to tables by this and other windows.
  ' If this, or any other, form posts a change to a table, then it will invoke an update event 
  ' detailing what tables have been altered.
  ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
  ' the 'VeniceAutoUpdate' event of the main Venice form.
  ' Each form may them react as appropriate to changes in any table that might impact it.
  '
    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    Dim KnowledgeDateChanged As Boolean
    Dim SetButtonStatus_Flag As Boolean

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

    KnowledgeDateChanged = False
    SetButtonStatus_Flag = False

    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
      KnowledgeDateChanged = True
    End If

    ' ****************************************************************
    ' Check for changes relevant to this form
    ' ****************************************************************

    ' Changes to the KnowledgeDate :-
    If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
      SetButtonStatus_Flag = True
    End If

    ' Changes to the tblFund table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFund) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Call SetFundCombo()
    End If


    ' Changes to the tblUserPermissions table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

      ' Check ongoing permissions.

      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

        FormIsValid = False
        Me.Close()
        Exit Sub
      End If

      SetButtonStatus_Flag = True

    End If

    ' ****************************************************************
    '
    ' ****************************************************************


  End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

  ' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
  Private Sub CheckPermissions()
    ' *****************************************************************************
    '
    ' *****************************************************************************

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

  End Sub


#End Region

#Region " Control Events"

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_Fund control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_Fund_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_Fund.SelectedIndexChanged
    ' *****************************************************************************
    ' If a Fund is selected, Update the Milestone Date Combo.
    ' *****************************************************************************

    Call SetMilestoneDateCombo()

  End Sub



    ''' <summary>
    ''' Handles the CheckedChanged event of the FromRadios control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub FromRadios_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_FromLive.CheckedChanged, Radio_FromWholeDay.CheckedChanged, Radio_FromPreciseTime.CheckedChanged
    ' *******************************************************************************
    '
    '
    ' *******************************************************************************

    If Me.Radio_FromLive.Checked = True Then
      Me.Date_KnowledgeDateFrom.Enabled = False
      Me.Date_KnowledgeDateFrom.Value = Now.Date

      Me.Check_SaveUnitPrices.Enabled = True
    Else
      Me.Date_KnowledgeDateFrom.Enabled = True
      Me.Check_SaveUnitPrices.Enabled = False
      Me.Check_SaveUnitPrices.Checked = False

      If Me.Radio_FromWholeDay.Checked = True Then
        Me.Date_KnowledgeDateFrom.CustomFormat = DISPLAYMEMBER_DATEFORMAT
      Else
        Me.Date_KnowledgeDateFrom.CustomFormat = DISPLAYMEMBER_LONGDATEFORMAT
      End If
    End If

  End Sub


    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_MilestoneDate control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_MilestoneDate_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_MilestoneDate.SelectedIndexChanged
    ' *****************************************************************************
    ' If a Milestone Date is selected, update the Displayed Knowledgedate.
    ' *****************************************************************************

    If Combo_MilestoneDate.SelectedIndex > 0 Then
      If IsDate(Combo_MilestoneDate.SelectedValue) Then
        Me.Radio_FromPreciseTime.Checked = True
        Me.Date_KnowledgeDateFrom.Value = CDate(Combo_MilestoneDate.SelectedValue)
        Me.Date_ReportDate.Value = CDate(CType(Combo_MilestoneDate.SelectedItem, DataRowView)("DM"))
      End If
      Combo_MilestoneDate.SelectedIndex = 0
    End If

  End Sub

#End Region

#Region " Set Form Combos (Form Specific Code) "

    ''' <summary>
    ''' Sets the fund combo.
    ''' </summary>
  Private Sub SetFundCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_Fund, _
    RenaissanceStandardDatasets.tblFund, _
    "FundName", _
    "FundID", _
    "FundClosed=0", False, True, True, 0)  ' 

    Call SetMilestoneDateCombo()

  End Sub

    ''' <summary>
    ''' Sets the milestone date combo.
    ''' </summary>
  Private Sub SetMilestoneDateCombo()
    Dim SelectString As String = ""

    SelectString = False
    If (Combo_Fund.SelectedIndex >= 0) AndAlso (IsNumeric(Combo_Fund.SelectedValue)) Then
      SelectString = "MilestoneFundID=" & CInt(Combo_Fund.SelectedValue).ToString
    End If

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_MilestoneDate, _
    RenaissanceStandardDatasets.tblFundMilestones, _
    "MilestoneDate", _
    "MilestoneKnowledgeDate", _
    SelectString, False, False, True)    ' 

  End Sub



#End Region


#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "


    ''' <summary>
    ''' Handles the Click event of the btnRunReport control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnRunReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRunReport.Click
    ' *****************************************************************************
    ' Run Report
    ' *****************************************************************************

    Try
      FormControls = MainForm.MainReportHandler.DisableFormControls(Me)
      Form_ProgressBar.Value = Form_ProgressBar.Minimum
      MainForm.SetToolStripText(Label_Status, "Processing Report")

      If (ReportWorker Is Nothing) Then
				ReportWorker = New BackgroundWorkerThreadSafe(Nothing)
        AddHandler ReportWorker.DoWork, AddressOf ReportWorker_DoWork
        AddHandler ReportWorker.RunWorkerCompleted, AddressOf ReportWorkerCompleted
      End If

      ReportTimer.Interval = 25
      ReportTimer.Tag = Me.Form_ProgressBar
      ReportTimer.Start()


      If MainForm.UseReportWorkerThreads Then
        ReportWorker.RunWorkerAsync()
      Else
        RunReport()
        Call ReportWorkerCompleted(Nothing, Nothing)
      End If


    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
    End Try
  End Sub

    ''' <summary>
    ''' Handles the DoWork event of the ReportWorker control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.DoWorkEventArgs"/> instance containing the event data.</param>
  Private Sub ReportWorker_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) ' Handles backgroundWorker1.DoWork
    RunReport()
  End Sub

    ''' <summary>
    ''' Reports the worker completed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.RunWorkerCompletedEventArgs"/> instance containing the event data.</param>
  Private Sub ReportWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) ' Handles backgroundWorker1.RunWorkerCompleted
    Try
      ReportTimer.Stop()
      MainForm.MainReportHandler.EnableFormControls(FormControls)
      MainForm.SetToolStripText(Label_Status, "")

      'Form_ProgressBar = MainForm.ResetProgressBar(Form_StatusStrip, Form_ProgressBar)
      Form_ProgressBar.Value = Form_ProgressBar.Minimum
      Form_ProgressBar.Visible = False
      ReportTimer.Tag = Me.Form_ProgressBar
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
    End Try

  End Sub

    ''' <summary>
    ''' Runs the report.
    ''' </summary>
  Private Sub RunReport()
    ' *****************************************************************************
    ' Run Report
    ' *****************************************************************************

    Dim FundID As Integer
    Dim ReportDate As Date
    Dim KnowledgeDate As Date

    ' Validate Form Inputs

    FundID = 0
    Try
      If MainForm.GetComboSelectedIndex(Combo_Fund) >= 0 Then
        FundID = CInt(MainForm.GetComboSelectedValue(Combo_Fund))
      End If
    Catch ex As Exception
      FundID = 0
    End Try

    Try

      ' Report Dates

      ReportDate = MainForm.GetDatetimeValue(Date_ReportDate).Date

      ' KDs

      KnowledgeDate = KNOWLEDGEDATE_NOW

      If MainForm.GetRadioChecked(Radio_FromWholeDay) = True Then
        KnowledgeDate = CDate(CDate(MainForm.GetDatetimeValue(Date_KnowledgeDateFrom).Date).ToString(DISPLAYMEMBER_DATEFORMAT) & " 23:59:59")
      ElseIf Me.Radio_FromPreciseTime.Checked = True Then
        KnowledgeDate = MainForm.GetDatetimeValue(Date_KnowledgeDateFrom)
      End If
      If (KnowledgeDate <= KNOWLEDGEDATE_NOW) Then
        KnowledgeDate = KNOWLEDGEDATE_NOW
      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name & ", btnRunReport_Click()", LOG_LEVELS.Error, ex.Message, "Error Validating Fund Allocation Form inputs.", ex.StackTrace, True)
      Exit Sub
    End Try

    ' Save Unit Prices
    If MainForm.GetCheckBoxChecked(Me.Check_SaveUnitPrices) = True Then
      MainForm.SetToolStripText(Label_Status, "Saving Unit Prices.")
      If Not (MainForm.InvokeRequired) Then
        Application.DoEvents()
      End If

			Call Attribution_SaveFundPrices(MainForm, 0, ReportDate, DEFAULT_STATUSGROUPFILTER, DEFAULT_ADMINISTRATORDATESFILTER)
			Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblPrice), True)
    End If

    ' Run Report

    If Not (MainForm.InvokeRequired) Then
      Application.DoEvents()
    End If

    MainForm.SetToolStripText(Label_Status, "Getting Report Data.")
    If Not (MainForm.InvokeRequired) Then
      Application.DoEvents()
    End If

    Try

			Call MainForm.MainReportHandler.rptFundGearing(FundID, MainForm.GetControlText(Combo_Fund), ReportDate, DEFAULT_STATUSGROUPFILTER, DEFAULT_ADMINISTRATORDATESFILTER, KnowledgeDate, Nothing) ' Form_ProgressBar)

    Catch ex As Exception
      MainForm.LogError(Me.Name & ", btnRunReport_Click()", LOG_LEVELS.Error, ex.Message, "Error running Fund Allocation report.", ex.StackTrace, True)
    End Try

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnClose control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
    ' *****************************************************************************
    ' Close Form
    ' *****************************************************************************

    Me.Close()

  End Sub

#End Region




End Class
