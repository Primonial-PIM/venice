' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmProfitandLossReport.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals
Imports RenaissanceUtilities.DatePeriodFunctions

''' <summary>
''' Class frmProfitandLossReport
''' </summary>
Public Class frmProfitandLossReport

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmProfitandLossReport" /> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The BTN run report
    ''' </summary>
  Friend WithEvents btnRunReport As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN close
    ''' </summary>
  Friend WithEvents btnClose As System.Windows.Forms.Button
    ''' <summary>
    ''' The label2
    ''' </summary>
  Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ fund
    ''' </summary>
  Friend WithEvents Combo_Fund As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label_ trade date
    ''' </summary>
  Friend WithEvents Label_TradeDate As System.Windows.Forms.Label
    ''' <summary>
    ''' The date_ date from
    ''' </summary>
  Friend WithEvents Date_DateFrom As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The panel5
    ''' </summary>
  Friend WithEvents Panel5 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The panel1
    ''' </summary>
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The date_ date to
    ''' </summary>
  Friend WithEvents Date_DateTo As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The label1
    ''' </summary>
  Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The check_ aggregate to parent
    ''' </summary>
  Friend WithEvents Check_AggregateToParent As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ save unit prices
    ''' </summary>
  Friend WithEvents Check_SaveUnitPrices As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ lookthrough
    ''' </summary>
  Friend WithEvents Check_Lookthrough As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The label3
    ''' </summary>
  Friend WithEvents Label3 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label4
    ''' </summary>
  Friend WithEvents Label4 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ milestone date
    ''' </summary>
  Friend WithEvents Combo_MilestoneDate As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label5
    ''' </summary>
  Friend WithEvents Label5 As System.Windows.Forms.Label
    ''' <summary>
    ''' The radio_ from precise time
    ''' </summary>
	Friend WithEvents Radio_FromPreciseTime As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The radio_ from whole day
    ''' </summary>
	Friend WithEvents Radio_FromWholeDay As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The date_ knowledge date from
    ''' </summary>
	Friend WithEvents Date_KnowledgeDateFrom As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The radio_ to precise time
    ''' </summary>
	Friend WithEvents Radio_ToPreciseTime As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The radio_ to whole day
    ''' </summary>
	Friend WithEvents Radio_ToWholeDay As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The date_ knowledge date to
    ''' </summary>
	Friend WithEvents Date_KnowledgeDateTo As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The radio_ from live
    ''' </summary>
	Friend WithEvents Radio_FromLive As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The form_ status strip
    ''' </summary>
	Friend WithEvents Form_StatusStrip As System.Windows.Forms.StatusStrip
    ''' <summary>
    ''' The form_ progress bar
    ''' </summary>
	Friend WithEvents Form_ProgressBar As System.Windows.Forms.ToolStripProgressBar
    ''' <summary>
    ''' The label_ status
    ''' </summary>
	Friend WithEvents Label_Status As System.Windows.Forms.ToolStripStatusLabel
    ''' <summary>
    ''' The check_ sector summary
    ''' </summary>
	Friend WithEvents Check_SectorSummary As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The combo_ report name
    ''' </summary>
	Friend WithEvents Combo_ReportName As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label6
    ''' </summary>
	Friend WithEvents Label6 As System.Windows.Forms.Label
    ''' <summary>
    ''' The group_ report grouping
    ''' </summary>
	Friend WithEvents Group_ReportGrouping As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The check_ dont show others
    ''' </summary>
	Friend WithEvents Check_DontShowOthers As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The combo_ selected report group
    ''' </summary>
	Friend WithEvents Combo_SelectedReportGroup As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label7
    ''' </summary>
	Friend WithEvents Label7 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ report grouping option
    ''' </summary>
	Friend WithEvents Combo_ReportGroupingOption As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label8
    ''' </summary>
	Friend WithEvents Label8 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ start milestone
    ''' </summary>
	Friend WithEvents Combo_StartMilestone As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label9
    ''' </summary>
	Friend WithEvents Label9 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ end milestone
    ''' </summary>
	Friend WithEvents Combo_EndMilestone As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label10
    ''' </summary>
	Friend WithEvents Label10 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ transaction group
    ''' </summary>
	Friend WithEvents Combo_TransactionGroup As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label11
    ''' </summary>
  Friend WithEvents Label11 As System.Windows.Forms.Label
  Friend WithEvents Group_Removed As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The radio_ to live
    ''' </summary>
	Friend WithEvents Radio_ToLive As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.btnRunReport = New System.Windows.Forms.Button
    Me.btnClose = New System.Windows.Forms.Button
    Me.Label2 = New System.Windows.Forms.Label
    Me.Combo_Fund = New System.Windows.Forms.ComboBox
    Me.Date_DateFrom = New System.Windows.Forms.DateTimePicker
    Me.Label_TradeDate = New System.Windows.Forms.Label
    Me.Panel5 = New System.Windows.Forms.Panel
    Me.Combo_StartMilestone = New System.Windows.Forms.ComboBox
    Me.Label9 = New System.Windows.Forms.Label
    Me.Radio_FromLive = New System.Windows.Forms.RadioButton
    Me.Radio_FromPreciseTime = New System.Windows.Forms.RadioButton
    Me.Radio_FromWholeDay = New System.Windows.Forms.RadioButton
    Me.Date_KnowledgeDateFrom = New System.Windows.Forms.DateTimePicker
    Me.Panel1 = New System.Windows.Forms.Panel
    Me.Combo_EndMilestone = New System.Windows.Forms.ComboBox
    Me.Label10 = New System.Windows.Forms.Label
    Me.Radio_ToLive = New System.Windows.Forms.RadioButton
    Me.Radio_ToPreciseTime = New System.Windows.Forms.RadioButton
    Me.Radio_ToWholeDay = New System.Windows.Forms.RadioButton
    Me.Date_KnowledgeDateTo = New System.Windows.Forms.DateTimePicker
    Me.Date_DateTo = New System.Windows.Forms.DateTimePicker
    Me.Label1 = New System.Windows.Forms.Label
    Me.Check_AggregateToParent = New System.Windows.Forms.CheckBox
    Me.Check_SaveUnitPrices = New System.Windows.Forms.CheckBox
    Me.Check_Lookthrough = New System.Windows.Forms.CheckBox
    Me.Label3 = New System.Windows.Forms.Label
    Me.Label4 = New System.Windows.Forms.Label
    Me.Combo_MilestoneDate = New System.Windows.Forms.ComboBox
    Me.Label5 = New System.Windows.Forms.Label
    Me.Form_StatusStrip = New System.Windows.Forms.StatusStrip
    Me.Form_ProgressBar = New System.Windows.Forms.ToolStripProgressBar
    Me.Label_Status = New System.Windows.Forms.ToolStripStatusLabel
    Me.Check_SectorSummary = New System.Windows.Forms.CheckBox
    Me.Combo_ReportName = New System.Windows.Forms.ComboBox
    Me.Label6 = New System.Windows.Forms.Label
    Me.Group_ReportGrouping = New System.Windows.Forms.GroupBox
    Me.Check_DontShowOthers = New System.Windows.Forms.CheckBox
    Me.Combo_SelectedReportGroup = New System.Windows.Forms.ComboBox
    Me.Label7 = New System.Windows.Forms.Label
    Me.Combo_ReportGroupingOption = New System.Windows.Forms.ComboBox
    Me.Label8 = New System.Windows.Forms.Label
    Me.Combo_TransactionGroup = New System.Windows.Forms.ComboBox
    Me.Label11 = New System.Windows.Forms.Label
    Me.Group_Removed = New System.Windows.Forms.GroupBox
    Me.Panel5.SuspendLayout()
    Me.Panel1.SuspendLayout()
    Me.Form_StatusStrip.SuspendLayout()
    Me.Group_ReportGrouping.SuspendLayout()
    Me.Group_Removed.SuspendLayout()
    Me.SuspendLayout()
    '
    'btnRunReport
    '
    Me.btnRunReport.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnRunReport.Location = New System.Drawing.Point(50, 422)
    Me.btnRunReport.Name = "btnRunReport"
    Me.btnRunReport.Size = New System.Drawing.Size(148, 28)
    Me.btnRunReport.TabIndex = 15
    Me.btnRunReport.Text = "Run Report"
    '
    'btnClose
    '
    Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnClose.Location = New System.Drawing.Point(266, 422)
    Me.btnClose.Name = "btnClose"
    Me.btnClose.Size = New System.Drawing.Size(75, 28)
    Me.btnClose.TabIndex = 16
    Me.btnClose.Text = "&Close"
    '
    'Label2
    '
    Me.Label2.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label2.Location = New System.Drawing.Point(16, 38)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(120, 20)
    Me.Label2.TabIndex = 53
    Me.Label2.Text = "Fund Name"
    '
    'Combo_Fund
    '
    Me.Combo_Fund.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Fund.Location = New System.Drawing.Point(140, 34)
    Me.Combo_Fund.Name = "Combo_Fund"
    Me.Combo_Fund.Size = New System.Drawing.Size(252, 21)
    Me.Combo_Fund.TabIndex = 1
    '
    'Date_DateFrom
    '
    Me.Date_DateFrom.Location = New System.Drawing.Point(140, 90)
    Me.Date_DateFrom.Name = "Date_DateFrom"
    Me.Date_DateFrom.Size = New System.Drawing.Size(160, 20)
    Me.Date_DateFrom.TabIndex = 3
    '
    'Label_TradeDate
    '
    Me.Label_TradeDate.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label_TradeDate.Location = New System.Drawing.Point(16, 94)
    Me.Label_TradeDate.Name = "Label_TradeDate"
    Me.Label_TradeDate.Size = New System.Drawing.Size(116, 16)
    Me.Label_TradeDate.TabIndex = 102
    Me.Label_TradeDate.Text = "Date From"
    '
    'Panel5
    '
    Me.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Panel5.Controls.Add(Me.Combo_StartMilestone)
    Me.Panel5.Controls.Add(Me.Label9)
    Me.Panel5.Controls.Add(Me.Radio_FromLive)
    Me.Panel5.Controls.Add(Me.Radio_FromPreciseTime)
    Me.Panel5.Controls.Add(Me.Radio_FromWholeDay)
    Me.Panel5.Location = New System.Drawing.Point(12, 220)
    Me.Panel5.Name = "Panel5"
    Me.Panel5.Size = New System.Drawing.Size(372, 55)
    Me.Panel5.TabIndex = 11
    '
    'Combo_StartMilestone
    '
    Me.Combo_StartMilestone.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_StartMilestone.Location = New System.Drawing.Point(113, 27)
    Me.Combo_StartMilestone.Name = "Combo_StartMilestone"
    Me.Combo_StartMilestone.Size = New System.Drawing.Size(252, 21)
    Me.Combo_StartMilestone.TabIndex = 115
    '
    'Label9
    '
    Me.Label9.Location = New System.Drawing.Point(10, 30)
    Me.Label9.Name = "Label9"
    Me.Label9.Size = New System.Drawing.Size(100, 20)
    Me.Label9.TabIndex = 116
    Me.Label9.Text = "Milestone Date"
    '
    'Radio_FromLive
    '
    Me.Radio_FromLive.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Radio_FromLive.Checked = True
    Me.Radio_FromLive.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Radio_FromLive.Location = New System.Drawing.Point(12, 4)
    Me.Radio_FromLive.Name = "Radio_FromLive"
    Me.Radio_FromLive.Size = New System.Drawing.Size(104, 16)
    Me.Radio_FromLive.TabIndex = 0
    Me.Radio_FromLive.TabStop = True
    Me.Radio_FromLive.Text = "'Live' KD"
    '
    'Radio_FromPreciseTime
    '
    Me.Radio_FromPreciseTime.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Radio_FromPreciseTime.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Radio_FromPreciseTime.Location = New System.Drawing.Point(252, 4)
    Me.Radio_FromPreciseTime.Name = "Radio_FromPreciseTime"
    Me.Radio_FromPreciseTime.Size = New System.Drawing.Size(104, 16)
    Me.Radio_FromPreciseTime.TabIndex = 2
    Me.Radio_FromPreciseTime.Text = "Precise Time"
    '
    'Radio_FromWholeDay
    '
    Me.Radio_FromWholeDay.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Radio_FromWholeDay.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Radio_FromWholeDay.Location = New System.Drawing.Point(128, 4)
    Me.Radio_FromWholeDay.Name = "Radio_FromWholeDay"
    Me.Radio_FromWholeDay.Size = New System.Drawing.Size(104, 16)
    Me.Radio_FromWholeDay.TabIndex = 1
    Me.Radio_FromWholeDay.Text = "WholeDay"
    '
    'Date_KnowledgeDateFrom
    '
    Me.Date_KnowledgeDateFrom.CustomFormat = "dd MMM yyyy"
    Me.Date_KnowledgeDateFrom.Enabled = False
    Me.Date_KnowledgeDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom
    Me.Date_KnowledgeDateFrom.Location = New System.Drawing.Point(136, 196)
    Me.Date_KnowledgeDateFrom.Name = "Date_KnowledgeDateFrom"
    Me.Date_KnowledgeDateFrom.Size = New System.Drawing.Size(252, 20)
    Me.Date_KnowledgeDateFrom.TabIndex = 10
    '
    'Panel1
    '
    Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Panel1.Controls.Add(Me.Combo_EndMilestone)
    Me.Panel1.Controls.Add(Me.Label10)
    Me.Panel1.Controls.Add(Me.Radio_ToLive)
    Me.Panel1.Controls.Add(Me.Radio_ToPreciseTime)
    Me.Panel1.Controls.Add(Me.Radio_ToWholeDay)
    Me.Panel1.Location = New System.Drawing.Point(12, 306)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(372, 55)
    Me.Panel1.TabIndex = 13
    '
    'Combo_EndMilestone
    '
    Me.Combo_EndMilestone.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_EndMilestone.Location = New System.Drawing.Point(113, 26)
    Me.Combo_EndMilestone.Name = "Combo_EndMilestone"
    Me.Combo_EndMilestone.Size = New System.Drawing.Size(252, 21)
    Me.Combo_EndMilestone.TabIndex = 117
    '
    'Label10
    '
    Me.Label10.Location = New System.Drawing.Point(10, 29)
    Me.Label10.Name = "Label10"
    Me.Label10.Size = New System.Drawing.Size(100, 20)
    Me.Label10.TabIndex = 118
    Me.Label10.Text = "Milestone Date"
    '
    'Radio_ToLive
    '
    Me.Radio_ToLive.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Radio_ToLive.Checked = True
    Me.Radio_ToLive.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Radio_ToLive.Location = New System.Drawing.Point(12, 4)
    Me.Radio_ToLive.Name = "Radio_ToLive"
    Me.Radio_ToLive.Size = New System.Drawing.Size(104, 16)
    Me.Radio_ToLive.TabIndex = 0
    Me.Radio_ToLive.TabStop = True
    Me.Radio_ToLive.Text = "'Live' KD"
    '
    'Radio_ToPreciseTime
    '
    Me.Radio_ToPreciseTime.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Radio_ToPreciseTime.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Radio_ToPreciseTime.Location = New System.Drawing.Point(252, 4)
    Me.Radio_ToPreciseTime.Name = "Radio_ToPreciseTime"
    Me.Radio_ToPreciseTime.Size = New System.Drawing.Size(104, 16)
    Me.Radio_ToPreciseTime.TabIndex = 2
    Me.Radio_ToPreciseTime.Text = "Precise Time"
    '
    'Radio_ToWholeDay
    '
    Me.Radio_ToWholeDay.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Radio_ToWholeDay.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Radio_ToWholeDay.Location = New System.Drawing.Point(128, 4)
    Me.Radio_ToWholeDay.Name = "Radio_ToWholeDay"
    Me.Radio_ToWholeDay.Size = New System.Drawing.Size(104, 16)
    Me.Radio_ToWholeDay.TabIndex = 1
    Me.Radio_ToWholeDay.Text = "WholeDay"
    '
    'Date_KnowledgeDateTo
    '
    Me.Date_KnowledgeDateTo.CustomFormat = "dd MMM yyyy"
    Me.Date_KnowledgeDateTo.Enabled = False
    Me.Date_KnowledgeDateTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom
    Me.Date_KnowledgeDateTo.Location = New System.Drawing.Point(136, 282)
    Me.Date_KnowledgeDateTo.Name = "Date_KnowledgeDateTo"
    Me.Date_KnowledgeDateTo.Size = New System.Drawing.Size(252, 20)
    Me.Date_KnowledgeDateTo.TabIndex = 12
    '
    'Date_DateTo
    '
    Me.Date_DateTo.Location = New System.Drawing.Point(140, 118)
    Me.Date_DateTo.Name = "Date_DateTo"
    Me.Date_DateTo.Size = New System.Drawing.Size(160, 20)
    Me.Date_DateTo.TabIndex = 4
    '
    'Label1
    '
    Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label1.Location = New System.Drawing.Point(16, 122)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(116, 16)
    Me.Label1.TabIndex = 108
    Me.Label1.Text = "Date To"
    '
    'Check_AggregateToParent
    '
    Me.Check_AggregateToParent.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Check_AggregateToParent.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_AggregateToParent.Location = New System.Drawing.Point(18, 53)
    Me.Check_AggregateToParent.Name = "Check_AggregateToParent"
    Me.Check_AggregateToParent.Size = New System.Drawing.Size(51, 20)
    Me.Check_AggregateToParent.TabIndex = 5
    Me.Check_AggregateToParent.Text = "Aggregate positions to Parent Instrument"
    '
    'Check_SaveUnitPrices
    '
    Me.Check_SaveUnitPrices.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Check_SaveUnitPrices.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_SaveUnitPrices.Location = New System.Drawing.Point(18, 105)
    Me.Check_SaveUnitPrices.Name = "Check_SaveUnitPrices"
    Me.Check_SaveUnitPrices.Size = New System.Drawing.Size(51, 20)
    Me.Check_SaveUnitPrices.TabIndex = 6
    Me.Check_SaveUnitPrices.Text = "Save Unit Prices before running report"
    Me.Check_SaveUnitPrices.Visible = False
    '
    'Check_Lookthrough
    '
    Me.Check_Lookthrough.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Check_Lookthrough.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_Lookthrough.Location = New System.Drawing.Point(18, 79)
    Me.Check_Lookthrough.Name = "Check_Lookthrough"
    Me.Check_Lookthrough.Size = New System.Drawing.Size(51, 20)
    Me.Check_Lookthrough.TabIndex = 7
    Me.Check_Lookthrough.Text = "Look-Through P&&L"
    '
    'Label3
    '
    Me.Label3.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label3.Location = New System.Drawing.Point(12, 200)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(116, 16)
    Me.Label3.TabIndex = 112
    Me.Label3.Text = "Start KnowledgeDate"
    '
    'Label4
    '
    Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label4.Location = New System.Drawing.Point(12, 286)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(116, 20)
    Me.Label4.TabIndex = 113
    Me.Label4.Text = "End KnowledgeDate"
    '
    'Combo_MilestoneDate
    '
    Me.Combo_MilestoneDate.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_MilestoneDate.Location = New System.Drawing.Point(136, 373)
    Me.Combo_MilestoneDate.Name = "Combo_MilestoneDate"
    Me.Combo_MilestoneDate.Size = New System.Drawing.Size(252, 21)
    Me.Combo_MilestoneDate.TabIndex = 14
    '
    'Label5
    '
    Me.Label5.Location = New System.Drawing.Point(12, 377)
    Me.Label5.Name = "Label5"
    Me.Label5.Size = New System.Drawing.Size(132, 20)
    Me.Label5.TabIndex = 114
    Me.Label5.Text = "Single Milestone Period"
    '
    'Form_StatusStrip
    '
    Me.Form_StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Form_ProgressBar, Me.Label_Status})
    Me.Form_StatusStrip.Location = New System.Drawing.Point(0, 470)
    Me.Form_StatusStrip.Name = "Form_StatusStrip"
    Me.Form_StatusStrip.Size = New System.Drawing.Size(398, 22)
    Me.Form_StatusStrip.TabIndex = 130
    Me.Form_StatusStrip.Text = " "
    '
    'Form_ProgressBar
    '
    Me.Form_ProgressBar.Maximum = 20
    Me.Form_ProgressBar.Name = "Form_ProgressBar"
    Me.Form_ProgressBar.Size = New System.Drawing.Size(150, 17)
    Me.Form_ProgressBar.Step = 1
    Me.Form_ProgressBar.Visible = False
    '
    'Label_Status
    '
    Me.Label_Status.Name = "Label_Status"
    Me.Label_Status.Size = New System.Drawing.Size(10, 17)
    Me.Label_Status.Text = " "
    '
    'Check_SectorSummary
    '
    Me.Check_SectorSummary.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Check_SectorSummary.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_SectorSummary.Location = New System.Drawing.Point(16, 157)
    Me.Check_SectorSummary.Name = "Check_SectorSummary"
    Me.Check_SectorSummary.Size = New System.Drawing.Size(272, 20)
    Me.Check_SectorSummary.TabIndex = 8
    Me.Check_SectorSummary.Text = "Show as Sector Summary Report"
    '
    'Combo_ReportName
    '
    Me.Combo_ReportName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_ReportName.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_ReportName.Items.AddRange(New Object() {"Fund P&L Report"})
    Me.Combo_ReportName.Location = New System.Drawing.Point(140, 5)
    Me.Combo_ReportName.Name = "Combo_ReportName"
    Me.Combo_ReportName.Size = New System.Drawing.Size(252, 21)
    Me.Combo_ReportName.TabIndex = 0
    '
    'Label6
    '
    Me.Label6.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label6.Location = New System.Drawing.Point(16, 9)
    Me.Label6.Name = "Label6"
    Me.Label6.Size = New System.Drawing.Size(120, 20)
    Me.Label6.TabIndex = 133
    Me.Label6.Text = "ReportName"
    '
    'Group_ReportGrouping
    '
    Me.Group_ReportGrouping.Controls.Add(Me.Check_DontShowOthers)
    Me.Group_ReportGrouping.Controls.Add(Me.Combo_SelectedReportGroup)
    Me.Group_ReportGrouping.Controls.Add(Me.Label7)
    Me.Group_ReportGrouping.Controls.Add(Me.Combo_ReportGroupingOption)
    Me.Group_ReportGrouping.Controls.Add(Me.Label8)
    Me.Group_ReportGrouping.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Group_ReportGrouping.Location = New System.Drawing.Point(18, 21)
    Me.Group_ReportGrouping.Name = "Group_ReportGrouping"
    Me.Group_ReportGrouping.Size = New System.Drawing.Size(55, 26)
    Me.Group_ReportGrouping.TabIndex = 9
    Me.Group_ReportGrouping.TabStop = False
    Me.Group_ReportGrouping.Text = "Report Grouping"
    '
    'Check_DontShowOthers
    '
    Me.Check_DontShowOthers.Enabled = False
    Me.Check_DontShowOthers.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_DontShowOthers.Location = New System.Drawing.Point(121, 73)
    Me.Check_DontShowOthers.Name = "Check_DontShowOthers"
    Me.Check_DontShowOthers.Size = New System.Drawing.Size(245, 20)
    Me.Check_DontShowOthers.TabIndex = 2
    Me.Check_DontShowOthers.Text = "Don't show 'Others'"
    '
    'Combo_SelectedReportGroup
    '
    Me.Combo_SelectedReportGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_SelectedReportGroup.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectedReportGroup.Location = New System.Drawing.Point(121, 46)
    Me.Combo_SelectedReportGroup.Name = "Combo_SelectedReportGroup"
    Me.Combo_SelectedReportGroup.Size = New System.Drawing.Size(245, 21)
    Me.Combo_SelectedReportGroup.TabIndex = 1
    '
    'Label7
    '
    Me.Label7.Location = New System.Drawing.Point(5, 50)
    Me.Label7.Name = "Label7"
    Me.Label7.Size = New System.Drawing.Size(100, 17)
    Me.Label7.TabIndex = 76
    Me.Label7.Text = "Selected Group"
    '
    'Combo_ReportGroupingOption
    '
    Me.Combo_ReportGroupingOption.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_ReportGroupingOption.Items.AddRange(New Object() {"No additional Grouping", "Group by Fund Type Attribution Grouping", "Group by Selected fund Type Group", "Group by Instrument Type", "Group by Selected Instrument Type"})
    Me.Combo_ReportGroupingOption.Location = New System.Drawing.Point(121, 19)
    Me.Combo_ReportGroupingOption.Name = "Combo_ReportGroupingOption"
    Me.Combo_ReportGroupingOption.Size = New System.Drawing.Size(245, 21)
    Me.Combo_ReportGroupingOption.TabIndex = 0
    '
    'Label8
    '
    Me.Label8.Location = New System.Drawing.Point(5, 23)
    Me.Label8.Name = "Label8"
    Me.Label8.Size = New System.Drawing.Size(100, 17)
    Me.Label8.TabIndex = 74
    Me.Label8.Text = "Report Grouping"
    '
    'Combo_TransactionGroup
    '
    Me.Combo_TransactionGroup.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_TransactionGroup.Location = New System.Drawing.Point(140, 61)
    Me.Combo_TransactionGroup.Name = "Combo_TransactionGroup"
    Me.Combo_TransactionGroup.Size = New System.Drawing.Size(252, 21)
    Me.Combo_TransactionGroup.TabIndex = 2
    '
    'Label11
    '
    Me.Label11.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label11.Location = New System.Drawing.Point(16, 65)
    Me.Label11.Name = "Label11"
    Me.Label11.Size = New System.Drawing.Size(116, 16)
    Me.Label11.TabIndex = 136
    Me.Label11.Text = "Transaction Group"
    '
    'Group_Removed
    '
    Me.Group_Removed.Controls.Add(Me.Group_ReportGrouping)
    Me.Group_Removed.Controls.Add(Me.Check_AggregateToParent)
    Me.Group_Removed.Controls.Add(Me.Check_Lookthrough)
    Me.Group_Removed.Controls.Add(Me.Check_SaveUnitPrices)
    Me.Group_Removed.Location = New System.Drawing.Point(327, 90)
    Me.Group_Removed.Name = "Group_Removed"
    Me.Group_Removed.Size = New System.Drawing.Size(43, 32)
    Me.Group_Removed.TabIndex = 137
    Me.Group_Removed.TabStop = False
    Me.Group_Removed.Text = "Removed"
    Me.Group_Removed.Visible = False
    '
    'frmProfitandLossReport
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(398, 492)
    Me.Controls.Add(Me.Group_Removed)
    Me.Controls.Add(Me.Combo_TransactionGroup)
    Me.Controls.Add(Me.Label11)
    Me.Controls.Add(Me.Combo_MilestoneDate)
    Me.Controls.Add(Me.Combo_ReportName)
    Me.Controls.Add(Me.Label6)
    Me.Controls.Add(Me.Check_SectorSummary)
    Me.Controls.Add(Me.Form_StatusStrip)
    Me.Controls.Add(Me.Label5)
    Me.Controls.Add(Me.Label4)
    Me.Controls.Add(Me.Label3)
    Me.Controls.Add(Me.Date_DateTo)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.Panel1)
    Me.Controls.Add(Me.Date_KnowledgeDateTo)
    Me.Controls.Add(Me.Panel5)
    Me.Controls.Add(Me.Date_KnowledgeDateFrom)
    Me.Controls.Add(Me.Date_DateFrom)
    Me.Controls.Add(Me.Label_TradeDate)
    Me.Controls.Add(Me.Combo_Fund)
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.btnClose)
    Me.Controls.Add(Me.btnRunReport)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.MinimumSize = New System.Drawing.Size(0, 520)
    Me.Name = "frmProfitandLossReport"
    Me.Text = "Fund P&L Report"
    Me.Panel5.ResumeLayout(False)
    Me.Panel1.ResumeLayout(False)
    Me.Form_StatusStrip.ResumeLayout(False)
    Me.Form_StatusStrip.PerformLayout()
    Me.Group_ReportGrouping.ResumeLayout(False)
    Me.Group_Removed.ResumeLayout(False)
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "


	' Form 'Parent', the Main Venice form.
	' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
	Private WithEvents _MainForm As VeniceMain

	' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
	Private FormTooltip As New ToolTip()

    ''' <summary>
    ''' The report worker
    ''' </summary>
	Private ReportWorker As BackgroundWorkerThreadSafe = Nothing
    ''' <summary>
    ''' The form controls
    ''' </summary>
	Private FormControls As ArrayList = Nothing
    ''' <summary>
    ''' The report timer
    ''' </summary>
	Private WithEvents ReportTimer As New Windows.Forms.Timer()

	' Form Constants, specific to the table being updated.

    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
	Private ALWAYS_CLOSE_THIS_FORM As Boolean = True

	' The standard ChangeID for this form. e.g. tblPerson
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
	Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

	' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
	Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
	Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

	' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
	Private THIS_FORM_FormID As VeniceFormID

	' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
	Private FormIsValid As Boolean
    ''' <summary>
    ''' The form changed
    ''' </summary>
	Private FormChanged As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
	Private _FormOpenFailed As Boolean

	' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
	Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
	Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
	Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
	Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
	Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
	Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return False
		End Get
		Set(ByVal Value As Boolean)
		End Set
	End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
	Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
		Get
			Return False
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
		Get
			Return True
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

    ''' <summary>
    ''' Gets or sets the selected report.
    ''' </summary>
    ''' <value>The selected report.</value>
	Public Property SelectedReport() As Integer
		Get
			Try
				Return Combo_ReportName.SelectedIndex
			Catch ex As Exception
			End Try
		End Get
		Set(ByVal value As Integer)
			Try
				If (Combo_ReportName.Items.Count > value) AndAlso (Combo_ReportName.Items.Count > 0) Then
					Combo_ReportName.SelectedIndex = value
				End If
			Catch ex As Exception
			End Try
		End Set
	End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmProfitandLossReport" /> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
	Public Sub New(ByVal pMainForm As VeniceMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()

		_MainForm = pMainForm
		AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate
		AddHandler ReportTimer.Tick, AddressOf MainForm.IncrementStatusBarTimerEvent

		_FormOpenFailed = False

		' ******************************************************
		' Form Specific Settings :
		' ******************************************************

		' Form Permissioning :-

		THIS_FORM_PermissionArea = Me.Name
		THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

		' 'This' form ID

		THIS_FORM_FormID = VeniceFormID.frmProfitandLossReport

		' Format Event Handlers for form controls

		AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

		AddHandler Combo_Fund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_TransactionGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

		' ******************************************************
		' End Form Specific.
		' ******************************************************

		THIS_FORM_ChangeID = RenaissanceChangeID.KnowledgeDate

	End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

	' Form Initialisation code.
	'
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
	Public Sub ResetForm() Implements StandardVeniceForm.ResetForm

		Call Form_Load(Me, New System.EventArgs)
	End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
	Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub

    ''' <summary>
    ''' Handles the FormClosing event of the frmProfitandLossReport control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.FormClosingEventArgs" /> instance containing the event data.</param>
	Private Sub frmProfitandLossReport_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
		If (ReportWorker IsNot Nothing) Then
			If ReportWorker.IsBusy Then
				e.Cancel = True
				Exit Sub
			End If

			Try
				RemoveHandler ReportTimer.Tick, AddressOf MainForm.IncrementStatusBarTimerEvent
				RemoveHandler ReportWorker.DoWork, AddressOf ReportWorker_DoWork
				RemoveHandler ReportWorker.RunWorkerCompleted, AddressOf ReportWorkerCompleted
				ReportWorker.Dispose()
				ReportWorker = Nothing
			Catch ex As Exception
			End Try
		End If

	End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
		_FormOpenFailed = False

		If Not (MainForm Is Nothing) Then
			FormIsValid = True
		Else
			MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If


		' Check User permissions
		Call CheckPermissions()
		If (HasReadPermission = False) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		' Build Combos

		Try
			Call SetFundCombo()
			Call SetTransactionGroupCombo()

			MainForm.SetComboSelectionLengths(Me)
		Catch ex As Exception
		End Try

		' Default Report Date Values - End Of month dates for the 'Current'ish month .

    Dim TempDate As Date

    Select Case REFERENCE_PERIOD

      Case DealingPeriod.Daily

        TempDate = AddPeriodToDate(REFERENCE_PERIOD, Now.Date, -1)
        Me.Date_DateTo.Value = TempDate
        Me.Date_DateFrom.Value = AddPeriodToDate(REFERENCE_PERIOD, TempDate, -1)

      Case DealingPeriod.Weekly

        TempDate = FitDateToPeriod(REFERENCE_PERIOD, AddPeriodToDate(REFERENCE_PERIOD, Now.Date, -2), True)
        Me.Date_DateTo.Value = TempDate
        Me.Date_DateFrom.Value = AddPeriodToDate(REFERENCE_PERIOD, TempDate, -1)

      Case Else

        TempDate = Now.Date.AddDays(-10)

        TempDate = TempDate.AddDays(1 - TempDate.Day)
        TempDate = TempDate.AddMonths(1).AddDays(-1)

        Me.Date_DateTo.Value = TempDate

        TempDate = TempDate.AddDays(0 - TempDate.Day)

        Me.Date_DateFrom.Value = TempDate

    End Select

		' Default KnowledgeDate values

		Me.Radio_FromLive.Checked = True
		Me.Radio_ToLive.Checked = True
		Me.Date_KnowledgeDateFrom.Value = Now.Date
		Me.Date_KnowledgeDateTo.Value = Now.Date

		Me.SelectedReport = 0

	End Sub

    ''' <summary>
    ''' Handles the Closing event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs" /> instance containing the event data.</param>
	Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		Dim HideForm As Boolean

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...


		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else
			HideForm = True
			If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.ReportForm_CacheCount Then
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			e.Cancel = True
		Else
			Try
				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

				RemoveHandler Combo_Fund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_TransactionGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType

			Catch ex As Exception
			End Try
		End If

	End Sub


#End Region

    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs" /> instance containing the event data.</param>
	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		' Routine to handle changes / updates to tables by this and other windows.
		' If this, or any other, form posts a change to a table, then it will invoke an update event 
		' detailing what tables have been altered.
		' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
		' the 'VeniceAutoUpdate' event of the main Venice form.
		' Each form may them react as appropriate to changes in any table that might impact it.
		'

		Dim KnowledgeDateChanged As Boolean
		Dim SetButtonStatus_Flag As Boolean

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		KnowledgeDateChanged = False
		SetButtonStatus_Flag = False

		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
			KnowledgeDateChanged = True
		End If

		' ****************************************************************
		' Check for changes relevant to this form
		' ****************************************************************

		' Changes to the KnowledgeDate :-
		If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
			SetButtonStatus_Flag = True
		End If

		' Changes to the tblFund table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFund) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			Call SetFundCombo()
		End If

		' Changes to the tblSophisStatusGroups table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblSophisStatusGroups) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			Call SetTransactionGroupCombo()
		End If

		' Changes to the tblUserPermissions table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

			' Check ongoing permissions.

			Call CheckPermissions()
			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

				FormIsValid = False
				Me.Close()
				Exit Sub
			End If

			SetButtonStatus_Flag = True

		End If

		' ****************************************************************
		'
		' ****************************************************************


	End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

	' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
	Private Sub CheckPermissions()
		' *****************************************************************************
		'
		' *****************************************************************************

		Dim Permissions As Integer

		Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

		HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
		HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
		HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
		HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

	End Sub


#End Region

#Region " Control Events"

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_ReportName control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
	Private Sub Combo_ReportName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_ReportName.SelectedIndexChanged
		' *****************************************************************************
		'
		'
		' *****************************************************************************

		Try

			If (Me.Combo_ReportName.SelectedIndex = 0) Then
				Check_SectorSummary.Enabled = True
				Group_ReportGrouping.Enabled = False
			Else
				Check_SectorSummary.Enabled = False
				Group_ReportGrouping.Enabled = True
			End If

		Catch ex As Exception
		End Try

	End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_Fund control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
	Private Sub Combo_Fund_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_Fund.SelectedIndexChanged
		' *****************************************************************************
		' If a Fund is selected, Update the Milestone Date Combo.
		' *****************************************************************************

		If (Combo_Fund.SelectedIndex > 0) Then
			Call SetMilestoneDateCombo()
		End If

	End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the FromRadios control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
	Private Sub FromRadios_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_FromLive.CheckedChanged, Radio_FromWholeDay.CheckedChanged, Radio_FromPreciseTime.CheckedChanged
		' *******************************************************************************
		'
		'
		' *******************************************************************************

		If Me.Radio_FromLive.Checked = True Then
			Me.Date_KnowledgeDateFrom.Enabled = False
			Me.Date_KnowledgeDateFrom.Value = Now.Date
		Else
			Me.Date_KnowledgeDateFrom.Enabled = True

			If Me.Radio_FromWholeDay.Checked = True Then
				Me.Date_KnowledgeDateFrom.CustomFormat = DISPLAYMEMBER_DATEFORMAT
			Else
				Me.Date_KnowledgeDateFrom.CustomFormat = DISPLAYMEMBER_LONGDATEFORMAT
			End If
		End If

	End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the ToRadios control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
	Private Sub ToRadios_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_ToLive.CheckedChanged, Radio_ToWholeDay.CheckedChanged, Radio_ToPreciseTime.CheckedChanged
		' *******************************************************************************
		'
		'
		' *******************************************************************************

		If Me.Radio_ToLive.Checked = True Then
			Me.Date_KnowledgeDateTo.Enabled = False
			Me.Date_KnowledgeDateTo.Value = Now.Date

			Me.Check_SaveUnitPrices.Enabled = True
		Else
			Me.Date_KnowledgeDateTo.Enabled = True
			Me.Check_SaveUnitPrices.Enabled = False
			Me.Check_SaveUnitPrices.Checked = False

			If Me.Radio_ToWholeDay.Checked = True Then
				Me.Date_KnowledgeDateTo.CustomFormat = DISPLAYMEMBER_DATEFORMAT
			Else
				Me.Date_KnowledgeDateTo.CustomFormat = DISPLAYMEMBER_LONGDATEFORMAT
			End If
		End If

	End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_ReportGroupingOption control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
	Private Sub Combo_ReportGroupingOption_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_ReportGroupingOption.SelectedIndexChanged
		' *****************************************************************
		'
		'
		' *****************************************************************

		Try
			Try
				Combo_SelectedReportGroup.DataSource = Nothing
				Me.Combo_SelectedReportGroup.Items.Clear()
			Catch ex As Exception
			End Try

			Select Case Me.Combo_ReportGroupingOption.SelectedIndex

				Case 0
					'No additional Grouping

					Combo_SelectedReportGroup.Enabled = False
					Check_DontShowOthers.Enabled = False
					Check_DontShowOthers.Checked = False

				Case 1
					'Group by Fund Type Attribution Grouping

					Combo_SelectedReportGroup.Enabled = False
					Check_DontShowOthers.Enabled = False
					Check_DontShowOthers.Checked = False

				Case 2
					'Group by Selected fund Type Group

					Combo_SelectedReportGroup.Enabled = True
					Check_DontShowOthers.Enabled = True
					Check_DontShowOthers.Checked = False

					Call MainForm.SetTblGenericCombo( _
					 Me.Combo_SelectedReportGroup, _
					 RenaissanceStandardDatasets.tblFundType, _
					 "FundTypeAttributionGrouping", _
					 "FundTypeAttributionGrouping", _
					 "", True, True, False)			' 

				Case 3
					'Group by Instrument Type

					Combo_SelectedReportGroup.Enabled = False
					Check_DontShowOthers.Enabled = False
					Check_DontShowOthers.Checked = False

				Case 4
					'Group by Selected Instrument Type

					Combo_SelectedReportGroup.Enabled = True
					Check_DontShowOthers.Enabled = True
					Check_DontShowOthers.Checked = False

					Call MainForm.SetTblGenericCombo( _
					 Me.Combo_SelectedReportGroup, _
					 RenaissanceStandardDatasets.tblInstrumentType, _
					 "InstrumentTypeDescription", _
					 "InstrumentTypeID", _
					 "")		 ' 

				Case Else
					Combo_SelectedReportGroup.Enabled = False
					Check_DontShowOthers.Enabled = False
					Check_DontShowOthers.Checked = False

			End Select
		Catch ex As Exception
		End Try

	End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_MilestoneDate control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
	Private Sub Combo_MilestoneDate_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_MilestoneDate.SelectedIndexChanged
		' *****************************************************************************
		' If a Milestone Date is selected, update the Displayed Knowledgedate.
		' *****************************************************************************

		If Combo_MilestoneDate.SelectedIndex > 0 Then
			If IsDate(Combo_MilestoneDate.SelectedValue) Then
				Me.Radio_ToPreciseTime.Checked = True
				Me.Date_KnowledgeDateTo.Value = CDate(Combo_MilestoneDate.SelectedValue)
				Me.Date_DateTo.Value = CDate(CType(Combo_MilestoneDate.SelectedItem, DataRowView)("DM"))

				If (Combo_MilestoneDate.SelectedIndex < (Combo_MilestoneDate.Items.Count - 1)) Then
					' Date From is the previous Milestone
					Dim PreviousMS As DataRowView

					Me.Radio_FromPreciseTime.Checked = True

					Try

						PreviousMS = CType(Me.Combo_MilestoneDate.Items(Me.Combo_MilestoneDate.SelectedIndex + 1), DataRowView)

						Me.Date_KnowledgeDateFrom.Value = CDate(PreviousMS("VM"))
						Me.Date_DateFrom.Value = CDate(PreviousMS("DM"))

					Catch ex As Exception
						Me.Date_KnowledgeDateFrom.Value = CDate(Combo_MilestoneDate.SelectedValue)
						Me.Date_DateFrom.Value = Me.Date_DateFrom.Value.AddDays(0 - Me.Date_DateFrom.Value.Day)
					End Try

				Else
					Me.Radio_ToPreciseTime.Checked = True
					Me.Date_KnowledgeDateTo.Value = CDate(Combo_MilestoneDate.SelectedValue)
				End If



			End If
			Combo_MilestoneDate.SelectedIndex = 0
		End If

	End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_StartMilestone control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
	Private Sub Combo_StartMilestone_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_StartMilestone.SelectedIndexChanged
		' *****************************************************************************
		' If a Milestone Date is selected, update the Start Knowledgedate.
		' *****************************************************************************

		If Combo_StartMilestone.SelectedIndex > 0 Then
			If IsDate(Combo_StartMilestone.SelectedValue) Then
				Me.Radio_FromPreciseTime.Checked = True
				Me.Date_KnowledgeDateFrom.Value = CDate(Combo_StartMilestone.SelectedValue)
				Me.Date_DateFrom.Value = CDate(CType(Combo_StartMilestone.SelectedItem, DataRowView)("DM"))
			End If
			Combo_StartMilestone.SelectedIndex = 0
		End If
	End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_EndMilestone control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
	Private Sub Combo_EndMilestone_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_EndMilestone.SelectedIndexChanged
		' *****************************************************************************
		' If a Milestone Date is selected, update the End Knowledgedate.
		' *****************************************************************************

		If Combo_EndMilestone.SelectedIndex > 0 Then
			If IsDate(Combo_EndMilestone.SelectedValue) Then
				Me.Radio_ToPreciseTime.Checked = True
				Me.Date_KnowledgeDateTo.Value = CDate(Combo_EndMilestone.SelectedValue)
				Me.Date_DateTo.Value = CDate(CType(Combo_EndMilestone.SelectedItem, DataRowView)("DM"))
			End If
			Combo_EndMilestone.SelectedIndex = 0
		End If
	End Sub

#End Region

#Region " Set Form Combos (Form Specific Code) "

    ''' <summary>
    ''' Sets the fund combo.
    ''' </summary>
	Private Sub SetFundCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_Fund, _
    RenaissanceStandardDatasets.tblFund, _
    "FundName", _
    "FundID", _
    "FundClosed=0", False, True, True)  ' 

	End Sub

    ''' <summary>
    ''' Sets the transaction group combo.
    ''' </summary>
	Private Sub SetTransactionGroupCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_TransactionGroup, _
		RenaissanceStandardDatasets.tblSophisStatusGroups, _
		"GROUP_NAME", _
		"GROUP_NAME", _
		"", True, True, True)		' 

	End Sub

    ''' <summary>
    ''' Sets the milestone date combo.
    ''' </summary>
	Private Sub SetMilestoneDateCombo()
		Dim SelectString As String

		SelectString = False
		If (Combo_Fund.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Fund.SelectedValue)) Then
			SelectString = "MilestoneFundID=" & CInt(Combo_Fund.SelectedValue).ToString
		End If

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_MilestoneDate, _
		RenaissanceStandardDatasets.tblFundMilestones, _
		"MilestoneDate", _
		"MilestoneKnowledgeDate", _
		SelectString, False, False, True)		 ' 

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_StartMilestone, _
		RenaissanceStandardDatasets.tblFundMilestones, _
		"MilestoneDate", _
		"MilestoneKnowledgeDate", _
		SelectString, False, False, True)		 ' 

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_EndMilestone, _
		RenaissanceStandardDatasets.tblFundMilestones, _
		"MilestoneDate", _
		"MilestoneKnowledgeDate", _
		SelectString, False, False, True)		 ' 

	End Sub



#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "


    ''' <summary>
    ''' Handles the Click event of the btnRunReport control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
	Private Sub btnRunReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRunReport.Click
		' *****************************************************************************
		' Run Report
		' *****************************************************************************

		Try
			FormControls = MainForm.MainReportHandler.DisableFormControls(Me)
			Form_ProgressBar.Value = Form_ProgressBar.Minimum
			MainForm.SetToolStripText(Label_Status, "Processing Report")

			If (ReportWorker Is Nothing) Then
				ReportWorker = New BackgroundWorkerThreadSafe(Nothing)
				AddHandler ReportWorker.DoWork, AddressOf ReportWorker_DoWork
				AddHandler ReportWorker.RunWorkerCompleted, AddressOf ReportWorkerCompleted
			End If

			ReportTimer.Interval = 25
			ReportTimer.Tag = Me.Form_ProgressBar
			ReportTimer.Start()


			If MainForm.UseReportWorkerThreads Then
				ReportWorker.RunWorkerAsync()
			Else
				RunReport()
				Call ReportWorkerCompleted(Nothing, Nothing)
			End If


		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
		End Try
	End Sub

    ''' <summary>
    ''' Handles the DoWork event of the ReportWorker control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.DoWorkEventArgs" /> instance containing the event data.</param>
	Private Sub ReportWorker_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs)	' Handles backgroundWorker1.DoWork
		RunReport()
	End Sub

    ''' <summary>
    ''' Reports the worker completed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.RunWorkerCompletedEventArgs" /> instance containing the event data.</param>
	Private Sub ReportWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs)	' Handles backgroundWorker1.RunWorkerCompleted
		Try
			ReportTimer.Stop()
			MainForm.MainReportHandler.EnableFormControls(FormControls)
			MainForm.SetToolStripText(Label_Status, "")

			'Form_ProgressBar = MainForm.ResetProgressBar(Form_StatusStrip, Form_ProgressBar)
			Form_ProgressBar.Value = Form_ProgressBar.Minimum
			Form_ProgressBar.Visible = False
			ReportTimer.Tag = Me.Form_ProgressBar
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
		End Try

	End Sub

    ''' <summary>
    ''' Runs the report.
    ''' </summary>
	Private Sub RunReport()
		' *****************************************************************************
		' Run Report
		' *****************************************************************************

		Dim FundID As Integer
		Dim DateFrom As Date
		Dim DateTo As Date
		Dim KnowledgeDateFrom As Date
		Dim KnowledgeDateTo As Date
		Dim TransactionStatusGroup As String

		' Validate Form Inputs

		If (MainForm.GetComboSelectedValue(Combo_Fund) Is Nothing) OrElse (MainForm.GetComboSelectedIndex(Combo_Fund) <= 0) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Fund must be selected.", "", True)
			Exit Sub
		End If
		FundID = CInt(MainForm.GetComboSelectedValue(Combo_Fund))

		TransactionStatusGroup = ""
		If MainForm.GetComboSelectedIndex(Combo_TransactionGroup) > 0 Then
			TransactionStatusGroup = MainForm.GetComboSelectedValue(Combo_TransactionGroup).ToString
		End If

		Try

			' Report Dates

			DateFrom = MainForm.GetDatetimeValue(Date_DateFrom).Date
			DateTo = MainForm.GetDatetimeValue(Date_DateTo).Date

			' KDs

			KnowledgeDateFrom = KNOWLEDGEDATE_NOW
			KnowledgeDateTo = KNOWLEDGEDATE_NOW

			If MainForm.GetRadioChecked(Radio_FromWholeDay) = True Then
				KnowledgeDateFrom = CDate(CDate(MainForm.GetDatetimeValue(Date_KnowledgeDateFrom).Date).ToString(DISPLAYMEMBER_DATEFORMAT) & " 23:59:59")
			ElseIf MainForm.GetRadioChecked(Radio_FromPreciseTime) = True Then
				KnowledgeDateFrom = MainForm.GetDatetimeValue(Date_KnowledgeDateFrom)
			End If
			If (KnowledgeDateFrom <= KNOWLEDGEDATE_NOW) Then
				KnowledgeDateFrom = KNOWLEDGEDATE_NOW
			End If

			If MainForm.GetRadioChecked(Radio_ToWholeDay) = True Then
				KnowledgeDateTo = CDate(CDate(MainForm.GetDatetimeValue(Date_KnowledgeDateTo).Date).ToString(DISPLAYMEMBER_DATEFORMAT) & " 23:59:59")
			ElseIf MainForm.GetRadioChecked(Radio_ToPreciseTime) = True Then
				KnowledgeDateTo = MainForm.GetDatetimeValue(Date_KnowledgeDateTo)
			End If
			If (KnowledgeDateTo <= KNOWLEDGEDATE_NOW) Then
				KnowledgeDateTo = KNOWLEDGEDATE_NOW
			End If

		Catch ex As Exception
			MainForm.LogError(Me.Name & ", btnRunReport_Click()", LOG_LEVELS.Error, ex.Message, "Error Validating Profit and Loss Form inputs.", ex.StackTrace, True)
			Exit Sub
		End Try

		If MainForm.GetCheckBoxChecked(Check_SaveUnitPrices) Then
			Try

				Call Attribution_SaveFundPrices(MainForm, FundID, DateTo, TransactionStatusGroup, DEFAULT_ADMINISTRATORDATESFILTER)
				Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblPrice), True)

			Catch ex As Exception
				MainForm.LogError(Me.Name & ", btnRunReport_Click()", LOG_LEVELS.Error, ex.Message, "Error saving Fund Prices.", ex.StackTrace, True)
				Exit Sub
			End Try
		End If

		' Check Prices

		Dim MissingInstrumentList As ArrayList
		MissingInstrumentList = MissingInstruments(MainForm, Me.Name, FundID, "", DateTo)
		If MissingInstrumentList.Count > 0 Then
			Dim MessageString As String
			Dim thisCount As Integer

			MessageString = "Prices are missing for the following Instruments :" & vbCrLf

			For thisCount = 0 To (MissingInstrumentList.Count - 1)
				MessageString &= "  " & MissingInstrumentList(thisCount).ToString & vbCrLf
			Next
			MessageString &= vbCrLf & "Do you wish to continue running the report ?"

			If MessageBox.Show(MessageString, "Abort Report ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
				Exit Sub
			End If

		End If

		' Run Report

		If Not (MainForm.InvokeRequired) Then
			Application.DoEvents()
		End If

		Try
			Select Case MainForm.GetComboSelectedIndex(Me.Combo_ReportName)

				Case 0 ' Fund P&L Report

					MainForm.MainReportHandler.rptProfitAndLoss(FundID, DateFrom, DateTo, TransactionStatusGroup, DEFAULT_ADMINISTRATORDATESFILTER, KnowledgeDateFrom, KnowledgeDateTo, MainForm.GetCheckBoxChecked(Check_AggregateToParent), MainForm.GetCheckBoxChecked(Check_Lookthrough), MainForm.GetCheckBoxChecked(Check_SectorSummary), Nothing)	' Form_ProgressBar)

				Case 1 ' Asset Allocation Report

          'Dim ReportGroupingOption As Integer = MainForm.GetComboSelectedIndex(Me.Combo_ReportGroupingOption)
          'Dim ReportGroupingChoice As String = ""
          'Dim DontShowOthers As Boolean = MainForm.GetCheckBoxChecked(Check_DontShowOthers)

          'If (MainForm.GetComboSelectedValue(Combo_SelectedReportGroup) IsNot Nothing) Then
          '	ReportGroupingChoice = MainForm.GetComboSelectedValue(Combo_SelectedReportGroup).ToString
          'End If

          'MainForm.MainReportHandler.rptAssetAllocationReport(FundID, DateFrom, DateTo, TransactionStatusGroup, DEFAULT_ADMINISTRATORDATESFILTER, KnowledgeDateFrom, KnowledgeDateTo, MainForm.GetCheckBoxChecked(Check_AggregateToParent), MainForm.GetCheckBoxChecked(Check_Lookthrough), MainForm.GetCheckBoxChecked(Check_SectorSummary), ReportGroupingOption, ReportGroupingChoice, DontShowOthers, Nothing)	' Form_ProgressBar)

			End Select

		Catch ex As Exception
			MainForm.LogError(Me.Name & ", btnRunReport_Click()", LOG_LEVELS.Error, ex.Message, "Error running Profit and Loss report.", ex.StackTrace, True)
		End Try

	End Sub


    ''' <summary>
    ''' Handles the Click event of the btnClose control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
	Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
		' *****************************************************************************
		' Close Form
		' *****************************************************************************

		Me.Close()

	End Sub

#End Region




End Class
