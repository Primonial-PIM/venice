' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmRiskExposureReport.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals


''' <summary>
''' Class frmRiskExposureReport
''' </summary>
Public Class frmRiskExposureReport

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmRiskExposureReport"/> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The BTN run report
    ''' </summary>
  Friend WithEvents btnRunReport As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN close
    ''' </summary>
  Friend WithEvents btnClose As System.Windows.Forms.Button
    ''' <summary>
    ''' The label2
    ''' </summary>
  Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The date_ report start date
    ''' </summary>
  Friend WithEvents Date_ReportStartDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The label_ trade date
    ''' </summary>
  Friend WithEvents Label_TradeDate As System.Windows.Forms.Label
    ''' <summary>
    ''' The label3
    ''' </summary>
  Friend WithEvents Label3 As System.Windows.Forms.Label
    ''' <summary>
    ''' The panel5
    ''' </summary>
  Friend WithEvents Panel5 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The radio_ live
    ''' </summary>
  Friend WithEvents Radio_Live As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The radio_ precise time
    ''' </summary>
  Friend WithEvents Radio_PreciseTime As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The radio_ whole day
    ''' </summary>
  Friend WithEvents Radio_WholeDay As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The date_ knowledge date
    ''' </summary>
  Friend WithEvents Date_KnowledgeDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The combo_ bookmark
    ''' </summary>
  Friend WithEvents Combo_Bookmark As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label4
    ''' </summary>
  Friend WithEvents Label4 As System.Windows.Forms.Label
    ''' <summary>
    ''' The panel4
    ''' </summary>
  Friend WithEvents Panel4 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The panel6
    ''' </summary>
  Friend WithEvents Panel6 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The label5
    ''' </summary>
  Friend WithEvents Label5 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label6
    ''' </summary>
  Friend WithEvents Label6 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ minimum usage
    ''' </summary>
  Friend WithEvents Combo_MinimumUsage As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label7
    ''' </summary>
  Friend WithEvents Label7 As System.Windows.Forms.Label
    ''' <summary>
    ''' The panel1
    ''' </summary>
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The panel2
    ''' </summary>
  Friend WithEvents Panel2 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The combo_ limit category
    ''' </summary>
  Friend WithEvents Combo_LimitCategory As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ instrument
    ''' </summary>
  Friend WithEvents Combo_Instrument As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The check_ recreate exposures
    ''' </summary>
  Friend WithEvents Check_RecreateExposures As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ aggregate to parent
    ''' </summary>
  Friend WithEvents Check_AggregateToParent As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The form_ status strip
    ''' </summary>
  Friend WithEvents Form_StatusStrip As System.Windows.Forms.StatusStrip
    ''' <summary>
    ''' The form_ progress bar
    ''' </summary>
  Friend WithEvents Form_ProgressBar As System.Windows.Forms.ToolStripProgressBar
    ''' <summary>
    ''' The label_ status
    ''' </summary>
  Friend WithEvents Label_Status As System.Windows.Forms.ToolStripStatusLabel
    ''' <summary>
    ''' The check_ lookthrough risk
    ''' </summary>
  Friend WithEvents Check_LookthroughRisk As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ detail report
    ''' </summary>
  Friend WithEvents Check_DetailReport As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The combo_ transaction group
    ''' </summary>
  Friend WithEvents Combo_TransactionGroup As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label8
    ''' </summary>
  Friend WithEvents Label8 As System.Windows.Forms.Label
    ''' <summary>
    ''' The list_ fund
    ''' </summary>
  Friend WithEvents List_Fund As System.Windows.Forms.ListBox
    ''' <summary>
    ''' The check_ composite report
    ''' </summary>
  Friend WithEvents Check_CompositeReport As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The date_ report end date
    ''' </summary>
  Friend WithEvents Date_ReportEndDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The label1
    ''' </summary>
  Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ existing end dates
    ''' </summary>
  Friend WithEvents Combo_ExistingEndDates As System.Windows.Forms.ComboBox
  Friend WithEvents Check_SeparateFXForwards As System.Windows.Forms.CheckBox
  Friend WithEvents Check_SeparateFXTrades As System.Windows.Forms.CheckBox
  ''' <summary>
  ''' The combo_ existing start dates
  ''' </summary>
  Friend WithEvents Combo_ExistingStartDates As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' Initializes the component.
  ''' </summary>
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.btnRunReport = New System.Windows.Forms.Button
    Me.btnClose = New System.Windows.Forms.Button
    Me.Label2 = New System.Windows.Forms.Label
    Me.Date_ReportStartDate = New System.Windows.Forms.DateTimePicker
    Me.Label_TradeDate = New System.Windows.Forms.Label
    Me.Label3 = New System.Windows.Forms.Label
    Me.Panel5 = New System.Windows.Forms.Panel
    Me.Radio_Live = New System.Windows.Forms.RadioButton
    Me.Radio_PreciseTime = New System.Windows.Forms.RadioButton
    Me.Radio_WholeDay = New System.Windows.Forms.RadioButton
    Me.Date_KnowledgeDate = New System.Windows.Forms.DateTimePicker
    Me.Combo_Bookmark = New System.Windows.Forms.ComboBox
    Me.Label4 = New System.Windows.Forms.Label
    Me.Panel4 = New System.Windows.Forms.Panel
    Me.Panel6 = New System.Windows.Forms.Panel
    Me.Combo_LimitCategory = New System.Windows.Forms.ComboBox
    Me.Label5 = New System.Windows.Forms.Label
    Me.Combo_Instrument = New System.Windows.Forms.ComboBox
    Me.Label6 = New System.Windows.Forms.Label
    Me.Combo_MinimumUsage = New System.Windows.Forms.ComboBox
    Me.Label7 = New System.Windows.Forms.Label
    Me.Panel1 = New System.Windows.Forms.Panel
    Me.Panel2 = New System.Windows.Forms.Panel
    Me.Check_RecreateExposures = New System.Windows.Forms.CheckBox
    Me.Check_AggregateToParent = New System.Windows.Forms.CheckBox
    Me.Combo_ExistingStartDates = New System.Windows.Forms.ComboBox
    Me.Form_StatusStrip = New System.Windows.Forms.StatusStrip
    Me.Form_ProgressBar = New System.Windows.Forms.ToolStripProgressBar
    Me.Label_Status = New System.Windows.Forms.ToolStripStatusLabel
    Me.Check_LookthroughRisk = New System.Windows.Forms.CheckBox
    Me.Check_DetailReport = New System.Windows.Forms.CheckBox
    Me.Combo_TransactionGroup = New System.Windows.Forms.ComboBox
    Me.Label8 = New System.Windows.Forms.Label
    Me.List_Fund = New System.Windows.Forms.ListBox
    Me.Check_CompositeReport = New System.Windows.Forms.CheckBox
    Me.Date_ReportEndDate = New System.Windows.Forms.DateTimePicker
    Me.Label1 = New System.Windows.Forms.Label
    Me.Combo_ExistingEndDates = New System.Windows.Forms.ComboBox
    Me.Check_SeparateFXForwards = New System.Windows.Forms.CheckBox
    Me.Check_SeparateFXTrades = New System.Windows.Forms.CheckBox
    Me.Panel5.SuspendLayout()
    Me.Panel4.SuspendLayout()
    Me.Panel1.SuspendLayout()
    Me.Form_StatusStrip.SuspendLayout()
    Me.SuspendLayout()
    '
    'btnRunReport
    '
    Me.btnRunReport.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnRunReport.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnRunReport.Location = New System.Drawing.Point(48, 516)
    Me.btnRunReport.Name = "btnRunReport"
    Me.btnRunReport.Size = New System.Drawing.Size(148, 28)
    Me.btnRunReport.TabIndex = 19
    Me.btnRunReport.Text = "Run Report"
    '
    'btnClose
    '
    Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnClose.Location = New System.Drawing.Point(264, 516)
    Me.btnClose.Name = "btnClose"
    Me.btnClose.Size = New System.Drawing.Size(75, 28)
    Me.btnClose.TabIndex = 20
    Me.btnClose.Text = "&Close"
    '
    'Label2
    '
    Me.Label2.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label2.Location = New System.Drawing.Point(12, 16)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(120, 20)
    Me.Label2.TabIndex = 53
    Me.Label2.Text = "Fund Name"
    '
    'Date_ReportStartDate
    '
    Me.Date_ReportStartDate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Date_ReportStartDate.Location = New System.Drawing.Point(136, 120)
    Me.Date_ReportStartDate.Name = "Date_ReportStartDate"
    Me.Date_ReportStartDate.Size = New System.Drawing.Size(235, 20)
    Me.Date_ReportStartDate.TabIndex = 1
    '
    'Label_TradeDate
    '
    Me.Label_TradeDate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label_TradeDate.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label_TradeDate.Location = New System.Drawing.Point(12, 124)
    Me.Label_TradeDate.Name = "Label_TradeDate"
    Me.Label_TradeDate.Size = New System.Drawing.Size(100, 16)
    Me.Label_TradeDate.TabIndex = 102
    Me.Label_TradeDate.Text = "Valuation From Date"
    '
    'Label3
    '
    Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label3.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label3.Location = New System.Drawing.Point(12, 205)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(116, 16)
    Me.Label3.TabIndex = 115
    Me.Label3.Text = "KnowledgeDate"
    '
    'Panel5
    '
    Me.Panel5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Panel5.Controls.Add(Me.Radio_Live)
    Me.Panel5.Controls.Add(Me.Radio_PreciseTime)
    Me.Panel5.Controls.Add(Me.Radio_WholeDay)
    Me.Panel5.Location = New System.Drawing.Point(12, 225)
    Me.Panel5.Name = "Panel5"
    Me.Panel5.Size = New System.Drawing.Size(379, 24)
    Me.Panel5.TabIndex = 7
    '
    'Radio_Live
    '
    Me.Radio_Live.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Radio_Live.Checked = True
    Me.Radio_Live.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Radio_Live.Location = New System.Drawing.Point(12, 4)
    Me.Radio_Live.Name = "Radio_Live"
    Me.Radio_Live.Size = New System.Drawing.Size(104, 16)
    Me.Radio_Live.TabIndex = 0
    Me.Radio_Live.TabStop = True
    Me.Radio_Live.Text = "'Live' KD"
    '
    'Radio_PreciseTime
    '
    Me.Radio_PreciseTime.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Radio_PreciseTime.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Radio_PreciseTime.Location = New System.Drawing.Point(252, 4)
    Me.Radio_PreciseTime.Name = "Radio_PreciseTime"
    Me.Radio_PreciseTime.Size = New System.Drawing.Size(104, 16)
    Me.Radio_PreciseTime.TabIndex = 2
    Me.Radio_PreciseTime.Text = "Precise Time"
    '
    'Radio_WholeDay
    '
    Me.Radio_WholeDay.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Radio_WholeDay.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Radio_WholeDay.Location = New System.Drawing.Point(128, 4)
    Me.Radio_WholeDay.Name = "Radio_WholeDay"
    Me.Radio_WholeDay.Size = New System.Drawing.Size(104, 16)
    Me.Radio_WholeDay.TabIndex = 1
    Me.Radio_WholeDay.Text = "WholeDay"
    '
    'Date_KnowledgeDate
    '
    Me.Date_KnowledgeDate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Date_KnowledgeDate.CustomFormat = "dd MMM yyyy"
    Me.Date_KnowledgeDate.Enabled = False
    Me.Date_KnowledgeDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
    Me.Date_KnowledgeDate.Location = New System.Drawing.Point(136, 201)
    Me.Date_KnowledgeDate.Name = "Date_KnowledgeDate"
    Me.Date_KnowledgeDate.Size = New System.Drawing.Size(255, 20)
    Me.Date_KnowledgeDate.TabIndex = 6
    '
    'Combo_Bookmark
    '
    Me.Combo_Bookmark.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Bookmark.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Bookmark.Location = New System.Drawing.Point(136, 257)
    Me.Combo_Bookmark.Name = "Combo_Bookmark"
    Me.Combo_Bookmark.Size = New System.Drawing.Size(255, 21)
    Me.Combo_Bookmark.TabIndex = 8
    '
    'Label4
    '
    Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label4.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label4.Location = New System.Drawing.Point(12, 261)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(100, 20)
    Me.Label4.TabIndex = 116
    Me.Label4.Text = "Bookmark"
    '
    'Panel4
    '
    Me.Panel4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Panel4.Controls.Add(Me.Panel6)
    Me.Panel4.Location = New System.Drawing.Point(12, 286)
    Me.Panel4.Name = "Panel4"
    Me.Panel4.Size = New System.Drawing.Size(379, 2)
    Me.Panel4.TabIndex = 118
    '
    'Panel6
    '
    Me.Panel6.Location = New System.Drawing.Point(0, 116)
    Me.Panel6.Name = "Panel6"
    Me.Panel6.Size = New System.Drawing.Size(428, 2)
    Me.Panel6.TabIndex = 48
    '
    'Combo_LimitCategory
    '
    Me.Combo_LimitCategory.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_LimitCategory.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_LimitCategory.Location = New System.Drawing.Point(135, 295)
    Me.Combo_LimitCategory.Name = "Combo_LimitCategory"
    Me.Combo_LimitCategory.Size = New System.Drawing.Size(255, 21)
    Me.Combo_LimitCategory.TabIndex = 9
    '
    'Label5
    '
    Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label5.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label5.Location = New System.Drawing.Point(11, 299)
    Me.Label5.Name = "Label5"
    Me.Label5.Size = New System.Drawing.Size(120, 20)
    Me.Label5.TabIndex = 121
    Me.Label5.Text = "Limit Category"
    '
    'Combo_Instrument
    '
    Me.Combo_Instrument.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Instrument.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Instrument.Location = New System.Drawing.Point(135, 324)
    Me.Combo_Instrument.Name = "Combo_Instrument"
    Me.Combo_Instrument.Size = New System.Drawing.Size(255, 21)
    Me.Combo_Instrument.TabIndex = 10
    '
    'Label6
    '
    Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label6.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label6.Location = New System.Drawing.Point(11, 328)
    Me.Label6.Name = "Label6"
    Me.Label6.Size = New System.Drawing.Size(120, 20)
    Me.Label6.TabIndex = 123
    Me.Label6.Text = "Limit Instrument"
    '
    'Combo_MinimumUsage
    '
    Me.Combo_MinimumUsage.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_MinimumUsage.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_MinimumUsage.Items.AddRange(New Object() {"0%", "5%", "10%", "20%", "25%", "50%", "75%", "100%"})
    Me.Combo_MinimumUsage.Location = New System.Drawing.Point(136, 353)
    Me.Combo_MinimumUsage.Name = "Combo_MinimumUsage"
    Me.Combo_MinimumUsage.Size = New System.Drawing.Size(255, 21)
    Me.Combo_MinimumUsage.TabIndex = 11
    '
    'Label7
    '
    Me.Label7.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label7.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label7.Location = New System.Drawing.Point(12, 357)
    Me.Label7.Name = "Label7"
    Me.Label7.Size = New System.Drawing.Size(120, 20)
    Me.Label7.TabIndex = 125
    Me.Label7.Text = "Minimum Usage"
    '
    'Panel1
    '
    Me.Panel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Panel1.Controls.Add(Me.Panel2)
    Me.Panel1.Location = New System.Drawing.Point(13, 382)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(375, 3)
    Me.Panel1.TabIndex = 126
    '
    'Panel2
    '
    Me.Panel2.Location = New System.Drawing.Point(0, 116)
    Me.Panel2.Name = "Panel2"
    Me.Panel2.Size = New System.Drawing.Size(428, 2)
    Me.Panel2.TabIndex = 48
    '
    'Check_RecreateExposures
    '
    Me.Check_RecreateExposures.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Check_RecreateExposures.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_RecreateExposures.Location = New System.Drawing.Point(12, 393)
    Me.Check_RecreateExposures.Name = "Check_RecreateExposures"
    Me.Check_RecreateExposures.Size = New System.Drawing.Size(323, 20)
    Me.Check_RecreateExposures.TabIndex = 12
    Me.Check_RecreateExposures.Text = "Recreate Exposures regardless of existing data."
    '
    'Check_AggregateToParent
    '
    Me.Check_AggregateToParent.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Check_AggregateToParent.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_AggregateToParent.Location = New System.Drawing.Point(357, 393)
    Me.Check_AggregateToParent.Name = "Check_AggregateToParent"
    Me.Check_AggregateToParent.Size = New System.Drawing.Size(31, 20)
    Me.Check_AggregateToParent.TabIndex = 17
    Me.Check_AggregateToParent.Text = "Aggregate risk to Parent Instrument"
    Me.Check_AggregateToParent.Visible = False
    '
    'Combo_ExistingStartDates
    '
    Me.Combo_ExistingStartDates.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_ExistingStartDates.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_ExistingStartDates.Location = New System.Drawing.Point(136, 120)
    Me.Combo_ExistingStartDates.Name = "Combo_ExistingStartDates"
    Me.Combo_ExistingStartDates.Size = New System.Drawing.Size(255, 21)
    Me.Combo_ExistingStartDates.TabIndex = 2
    '
    'Form_StatusStrip
    '
    Me.Form_StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Form_ProgressBar, Me.Label_Status})
    Me.Form_StatusStrip.Location = New System.Drawing.Point(0, 547)
    Me.Form_StatusStrip.Name = "Form_StatusStrip"
    Me.Form_StatusStrip.Size = New System.Drawing.Size(401, 22)
    Me.Form_StatusStrip.TabIndex = 133
    Me.Form_StatusStrip.Text = " "
    '
    'Form_ProgressBar
    '
    Me.Form_ProgressBar.Maximum = 20
    Me.Form_ProgressBar.Name = "Form_ProgressBar"
    Me.Form_ProgressBar.Size = New System.Drawing.Size(150, 17)
    Me.Form_ProgressBar.Step = 1
    Me.Form_ProgressBar.Visible = False
    '
    'Label_Status
    '
    Me.Label_Status.Name = "Label_Status"
    Me.Label_Status.Size = New System.Drawing.Size(10, 17)
    Me.Label_Status.Text = " "
    '
    'Check_LookthroughRisk
    '
    Me.Check_LookthroughRisk.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Check_LookthroughRisk.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_LookthroughRisk.Location = New System.Drawing.Point(357, 417)
    Me.Check_LookthroughRisk.Name = "Check_LookthroughRisk"
    Me.Check_LookthroughRisk.Size = New System.Drawing.Size(31, 20)
    Me.Check_LookthroughRisk.TabIndex = 18
    Me.Check_LookthroughRisk.Text = "Lookthrough Fund Risk"
    Me.Check_LookthroughRisk.Visible = False
    '
    'Check_DetailReport
    '
    Me.Check_DetailReport.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Check_DetailReport.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_DetailReport.Location = New System.Drawing.Point(12, 461)
    Me.Check_DetailReport.Name = "Check_DetailReport"
    Me.Check_DetailReport.Size = New System.Drawing.Size(323, 20)
    Me.Check_DetailReport.TabIndex = 15
    Me.Check_DetailReport.Text = "Detailed Exposure Report, show individual exposures."
    '
    'Combo_TransactionGroup
    '
    Me.Combo_TransactionGroup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_TransactionGroup.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_TransactionGroup.Location = New System.Drawing.Point(136, 174)
    Me.Combo_TransactionGroup.Name = "Combo_TransactionGroup"
    Me.Combo_TransactionGroup.Size = New System.Drawing.Size(255, 21)
    Me.Combo_TransactionGroup.TabIndex = 5
    '
    'Label8
    '
    Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label8.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label8.Location = New System.Drawing.Point(12, 178)
    Me.Label8.Name = "Label8"
    Me.Label8.Size = New System.Drawing.Size(116, 16)
    Me.Label8.TabIndex = 135
    Me.Label8.Text = "Transaction Group"
    '
    'List_Fund
    '
    Me.List_Fund.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.List_Fund.FormattingEnabled = True
    Me.List_Fund.Location = New System.Drawing.Point(135, 7)
    Me.List_Fund.Name = "List_Fund"
    Me.List_Fund.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
    Me.List_Fund.Size = New System.Drawing.Size(255, 108)
    Me.List_Fund.TabIndex = 0
    '
    'Check_CompositeReport
    '
    Me.Check_CompositeReport.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Check_CompositeReport.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_CompositeReport.Location = New System.Drawing.Point(12, 483)
    Me.Check_CompositeReport.Name = "Check_CompositeReport"
    Me.Check_CompositeReport.Size = New System.Drawing.Size(323, 20)
    Me.Check_CompositeReport.TabIndex = 16
    Me.Check_CompositeReport.Text = "Combine the Fund exposures reports"
    '
    'Date_ReportEndDate
    '
    Me.Date_ReportEndDate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Date_ReportEndDate.Location = New System.Drawing.Point(136, 147)
    Me.Date_ReportEndDate.Name = "Date_ReportEndDate"
    Me.Date_ReportEndDate.Size = New System.Drawing.Size(235, 20)
    Me.Date_ReportEndDate.TabIndex = 3
    '
    'Label1
    '
    Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label1.Location = New System.Drawing.Point(12, 151)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(100, 16)
    Me.Label1.TabIndex = 139
    Me.Label1.Text = "Valuation To Date"
    '
    'Combo_ExistingEndDates
    '
    Me.Combo_ExistingEndDates.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_ExistingEndDates.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_ExistingEndDates.Location = New System.Drawing.Point(136, 147)
    Me.Combo_ExistingEndDates.Name = "Combo_ExistingEndDates"
    Me.Combo_ExistingEndDates.Size = New System.Drawing.Size(255, 21)
    Me.Combo_ExistingEndDates.TabIndex = 4
    '
    'Check_SeparateFXForwards
    '
    Me.Check_SeparateFXForwards.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Check_SeparateFXForwards.Checked = True
    Me.Check_SeparateFXForwards.CheckState = System.Windows.Forms.CheckState.Checked
    Me.Check_SeparateFXForwards.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_SeparateFXForwards.Location = New System.Drawing.Point(12, 416)
    Me.Check_SeparateFXForwards.Name = "Check_SeparateFXForwards"
    Me.Check_SeparateFXForwards.Size = New System.Drawing.Size(323, 20)
    Me.Check_SeparateFXForwards.TabIndex = 13
    Me.Check_SeparateFXForwards.Text = "Show unsettled Forward FX trades as separate Forward FX"
    '
    'Check_SeparateFXTrades
    '
    Me.Check_SeparateFXTrades.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Check_SeparateFXTrades.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_SeparateFXTrades.Location = New System.Drawing.Point(12, 438)
    Me.Check_SeparateFXTrades.Name = "Check_SeparateFXTrades"
    Me.Check_SeparateFXTrades.Size = New System.Drawing.Size(323, 20)
    Me.Check_SeparateFXTrades.TabIndex = 14
    Me.Check_SeparateFXTrades.Text = "Show unsettled Spot FX trades as separate Forward FX"
    '
    'frmRiskExposureReport
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(401, 569)
    Me.Controls.Add(Me.Check_SeparateFXTrades)
    Me.Controls.Add(Me.Check_SeparateFXForwards)
    Me.Controls.Add(Me.Date_ReportEndDate)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.Combo_ExistingEndDates)
    Me.Controls.Add(Me.Check_CompositeReport)
    Me.Controls.Add(Me.List_Fund)
    Me.Controls.Add(Me.Combo_TransactionGroup)
    Me.Controls.Add(Me.Label8)
    Me.Controls.Add(Me.Check_DetailReport)
    Me.Controls.Add(Me.Check_LookthroughRisk)
    Me.Controls.Add(Me.Form_StatusStrip)
    Me.Controls.Add(Me.Check_AggregateToParent)
    Me.Controls.Add(Me.Check_RecreateExposures)
    Me.Controls.Add(Me.Panel1)
    Me.Controls.Add(Me.Combo_MinimumUsage)
    Me.Controls.Add(Me.Label7)
    Me.Controls.Add(Me.Combo_Instrument)
    Me.Controls.Add(Me.Label6)
    Me.Controls.Add(Me.Combo_LimitCategory)
    Me.Controls.Add(Me.Label5)
    Me.Controls.Add(Me.Panel4)
    Me.Controls.Add(Me.Combo_Bookmark)
    Me.Controls.Add(Me.Label4)
    Me.Controls.Add(Me.Label3)
    Me.Controls.Add(Me.Panel5)
    Me.Controls.Add(Me.Date_KnowledgeDate)
    Me.Controls.Add(Me.Date_ReportStartDate)
    Me.Controls.Add(Me.Label_TradeDate)
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.btnClose)
    Me.Controls.Add(Me.btnRunReport)
    Me.Controls.Add(Me.Combo_ExistingStartDates)
    Me.Name = "frmRiskExposureReport"
    Me.Text = "Risk Exposure Report"
    Me.Panel5.ResumeLayout(False)
    Me.Panel4.ResumeLayout(False)
    Me.Panel1.ResumeLayout(False)
    Me.Form_StatusStrip.ResumeLayout(False)
    Me.Form_StatusStrip.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "


  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
  ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
  ''' </summary>
  Private WithEvents _MainForm As VeniceMain

  ' Form ToolTip
  ''' <summary>
  ''' Control to provide tooltip functionality.
  ''' </summary>
  Private FormTooltip As New ToolTip()

  ''' <summary>
  ''' The report worker
  ''' </summary>
  Private ReportWorker As BackgroundWorkerThreadSafe = Nothing
  ''' <summary>
  ''' The form controls
  ''' </summary>
  Private FormControls As ArrayList = Nothing
  ''' <summary>
  ''' The report timer
  ''' </summary>
  Private WithEvents ReportTimer As New Windows.Forms.Timer()

  ' Form Constants, specific to the table being updated.

  ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
  ''' </summary>
  Private ALWAYS_CLOSE_THIS_FORM As Boolean = True

  ' The standard ChangeID for this form. e.g. tblPerson
  ''' <summary>
  ''' The standard ChangeID for this form.
  ''' </summary>
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

  ' Form specific Permissioning variables
  ''' <summary>
  ''' The THI s_ FOR m_ permission area
  ''' </summary>
  Private THIS_FORM_PermissionArea As String
  ''' <summary>
  ''' The THI s_ FOR m_ permission type
  ''' </summary>
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
  ''' <summary>
  ''' The THI s_ FOR m_ form ID
  ''' </summary>
  Private THIS_FORM_FormID As VeniceFormID

  ' Form Status Flags

  ''' <summary>
  ''' The form is valid
  ''' </summary>
  Private FormIsValid As Boolean
  ''' <summary>
  ''' The form changed
  ''' </summary>
  Private FormChanged As Boolean
  ''' <summary>
  ''' The _ form open failed
  ''' </summary>
  Private _FormOpenFailed As Boolean

  ' User Permission Flags

  ''' <summary>
  ''' The has read permission
  ''' </summary>
  Private HasReadPermission As Boolean
  ''' <summary>
  ''' The has update permission
  ''' </summary>
  Private HasUpdatePermission As Boolean
  ''' <summary>
  ''' The has insert permission
  ''' </summary>
  Private HasInsertPermission As Boolean
  ''' <summary>
  ''' The has delete permission
  ''' </summary>
  Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

  ''' <summary>
  ''' Gets the main form.
  ''' </summary>
  ''' <value>The main form.</value>
  Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
    ' Public property to return handle to the 'Main' Venice form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

  ''' <summary>
  ''' Gets or sets a value indicating whether this instance is over cancel button.
  ''' </summary>
  ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
  Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
    ' Public property maintaining a value indicating if the cursor is over the 'Cancel'
    ' Button on this form.
    ' This property is specifically designed for use by the field formating Event functions
    ' In order that they do not impose format restrictions if the user is about to click the 
    ' 'Cancel' button.
    '
    Get
      Return False
    End Get
    Set(ByVal Value As Boolean)
    End Set
  End Property

  ''' <summary>
  ''' Gets a value indicating whether this instance is in paint.
  ''' </summary>
  ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
  Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
    Get
      Return False
    End Get
  End Property

  ''' <summary>
  ''' Gets a value indicating whether [in use].
  ''' </summary>
  ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
    Get
      Return True
    End Get
  End Property

  ''' <summary>
  ''' Gets a value indicating whether [form open failed].
  ''' </summary>
  ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

#End Region

  ''' <summary>
  ''' Initializes a new instance of the <see cref="frmRiskExposureReport"/> class.
  ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  Public Sub New(ByVal pMainForm As VeniceMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()

    _MainForm = pMainForm
    AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate
    AddHandler ReportTimer.Tick, AddressOf MainForm.IncrementStatusBarTimerEvent

    _FormOpenFailed = False

    ' ******************************************************
    ' Form Specific Settings :
    ' ******************************************************

    ' Form Permissioning :-

    THIS_FORM_PermissionArea = Me.Name
    THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

    ' 'This' form ID

    THIS_FORM_FormID = VeniceFormID.frmRiskExposureReport

    ' Format Event Handlers for form controls

    AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

    AddHandler Combo_TransactionGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_Instrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_LimitCategory.KeyUp, AddressOf MainForm.ComboSelectAsYouType

    ' Set up the ToolTip
    MainForm.SetFormToolTip(Me, FormTooltip)

    ' Initialise List_GroupList
    Dim FundsDS As RenaissanceDataClass.DSFund = MainForm.Load_Table(RenaissanceStandardDatasets.tblFund, False)

    Me.List_Fund.DataSource = New DataView(FundsDS.tblFund, "FundClosed=0", "FundName", DataViewRowState.CurrentRows)
    List_Fund.DisplayMember = "FundName"
    List_Fund.ValueMember = "FundID"

    ' ******************************************************
    ' End Form Specific.
    ' ******************************************************

    THIS_FORM_ChangeID = RenaissanceChangeID.KnowledgeDate

  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
  ''' <summary>
  ''' Resets the form.
  ''' </summary>
  Public Sub ResetForm() Implements StandardVeniceForm.ResetForm

    Call Form_Load(Me, New System.EventArgs)
  End Sub

  ''' <summary>
  ''' Closes the form.
  ''' </summary>
  Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
    ALWAYS_CLOSE_THIS_FORM = True
    Me.Close()
  End Sub

  ''' <summary>
  ''' Handles the FormClosing event of the frmRiskExposureReport control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.Windows.Forms.FormClosingEventArgs"/> instance containing the event data.</param>
  Private Sub frmRiskExposureReport_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
    If (ReportWorker IsNot Nothing) Then
      If ReportWorker.IsBusy Then
        e.Cancel = True
        Exit Sub
      End If

      Try
        RemoveHandler ReportTimer.Tick, AddressOf MainForm.IncrementStatusBarTimerEvent
        RemoveHandler ReportWorker.DoWork, AddressOf ReportWorker_DoWork
        RemoveHandler ReportWorker.RunWorkerCompleted, AddressOf ReportWorkerCompleted
        ReportWorker.Dispose()
        ReportWorker = Nothing
      Catch ex As Exception
      End Try
    End If

  End Sub

  ''' <summary>
  ''' Handles the Load event of the Form control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
    _FormOpenFailed = False

    If Not (MainForm Is Nothing) Then
      FormIsValid = True
    Else
      MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If


    ' Check User permissions
    Call CheckPermissions()
    If (HasReadPermission = False) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    ' Build Combos

    Try

      Call SetBookmarkCombo()
      Call SetLimitCategoryCombo()
      Call SetInstrumentCombo()
      Call SetTransactionGroupCombo()

      MainForm.SetComboSelectionLengths(Me)

      Me.Radio_Live.Checked = True
      Me.Combo_MinimumUsage.SelectedIndex = 0
      Me.Check_RecreateExposures.Checked = True
      Me.Check_SeparateFXForwards.Checked = True
      Me.Check_SeparateFXTrades.Checked = False

    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Handles the Closing event of the Form control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
  Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    Dim HideForm As Boolean

    ' Hide or Close this form ?
    ' All depends on how many of this form type are Open or in Cache...


    If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
      HideForm = False
    Else
      HideForm = True
      If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.ReportForm_CacheCount Then
        HideForm = False
      End If
    End If

    If HideForm = True Then
      MainForm.HideInFormsCollection(Me)
      Me.Hide() ' NPP Fix

      e.Cancel = True
    Else
      Try
        MainForm.RemoveFromFormsCollection(Me)
        RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

        RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

        RemoveHandler Combo_TransactionGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_Instrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_LimitCategory.KeyUp, AddressOf MainForm.ComboSelectAsYouType

      Catch ex As Exception
      End Try
    End If

  End Sub


#End Region

  ' Routine to handle changes / updates to tables by this and other windows.
  ' If this, or any other, form posts a change to a table, then it will invoke an update event 
  ' detailing what tables have been altered.
  ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
  ' the 'VeniceAutoUpdate' event of the main Venice form.
  ' Each form may them react as appropriate to changes in any table that might impact it.
  '
  ''' <summary>
  ''' Autoes the update.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    Dim KnowledgeDateChanged As Boolean
    Dim SetButtonStatus_Flag As Boolean

    If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

    KnowledgeDateChanged = False
    SetButtonStatus_Flag = False

    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
      KnowledgeDateChanged = True
    End If

    ' ****************************************************************
    ' Check for changes relevant to this form
    ' ****************************************************************

    ' Changes to the KnowledgeDate :-
    If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
      SetButtonStatus_Flag = True
    End If

    ' Changes to the tblFund table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFund) = True) Or KnowledgeDateChanged Then

      Dim FundsDS As RenaissanceDataClass.DSFund = MainForm.Load_Table(RenaissanceStandardDatasets.tblFund, False)

      Me.List_Fund.DataSource = New DataView(FundsDS.tblFund, "FundClosed=0", "FundName", DataViewRowState.CurrentRows)
      List_Fund.DisplayMember = "FundName"
      List_Fund.ValueMember = "FundID"

    End If

    ' Changes to the tblSophisStatusGroups table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblSophisStatusGroups) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Call SetTransactionGroupCombo()
    End If

    ' Changes to the tblBookmark table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblBookmark) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Call SetBookmarkCombo()
    End If

    ' Changes to the tblLimitType table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblLimitType) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Call SetLimitCategoryCombo()
    End If

    ' Changes to the tblInstrument table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrument) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Call SetInstrumentCombo()
    End If

    ' Changes to the tblExposure table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblExposure) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Call Set_ExistingDateCombo()
    End If

    ' Changes to the tblUserPermissions table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

      ' Check ongoing permissions.

      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

        FormIsValid = False
        Me.Close()
        Exit Sub
      End If

      SetButtonStatus_Flag = True

    End If

    ' ****************************************************************
    '
    ' ****************************************************************


  End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

  ' Check User permissions
  ''' <summary>
  ''' Checks the permissions.
  ''' </summary>
  Private Sub CheckPermissions()
    ' *****************************************************************************
    '
    ' *****************************************************************************

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

  End Sub


#End Region

#Region " Set Form Combos (Form Specific Code) "

  ''' <summary>
  ''' Sets the transaction group combo.
  ''' </summary>
  Private Sub SetTransactionGroupCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_TransactionGroup, _
    RenaissanceStandardDatasets.tblSophisStatusGroups, _
    "GROUP_NAME", _
    "GROUP_NAME", _
    "", True, True, True)   ' 

  End Sub

  ''' <summary>
  ''' Sets the bookmark combo.
  ''' </summary>
  Private Sub SetBookmarkCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_Bookmark, _
    RenaissanceStandardDatasets.tblBookmark, _
    "BookMarkDescription", _
    "BookmarkDate", _
    "", False, True, True)    ' 

  End Sub

  ''' <summary>
  ''' Sets the limit category combo.
  ''' </summary>
  Private Sub SetLimitCategoryCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_LimitCategory, _
    RenaissanceStandardDatasets.tblLimitType, _
    "ltpLimitType", _
    "ltpID", _
    "", False, True, True)    ' 

  End Sub

  ''' <summary>
  ''' Sets the instrument combo.
  ''' </summary>
  Private Sub SetInstrumentCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_Instrument, _
    RenaissanceStandardDatasets.tblInstrument, _
    "InstrumentDescription", _
    "InstrumentID", _
    "", False, True, True)    ' 

  End Sub

  ''' <summary>
  ''' Set_s the existing date combo.
  ''' </summary>
  Private Sub Set_ExistingDateCombo()
    Dim SelectString As String = ""
    Dim thisFundID As Integer

    If Me.List_Fund.SelectedItems.Count > 0 Then
      Dim SelectedRow As DataRowView

      For Each SelectedRow In List_Fund.SelectedItems
        thisFundID = CType(SelectedRow.Row, RenaissanceDataClass.DSFund.tblFundRow).FundID

        If (SelectString.Length > 0) Then
          SelectString &= "," & thisFundID.ToString
        Else
          SelectString = thisFundID.ToString
        End If
      Next

      Dim ThisCommand As New SqlCommand
      Dim ThisConnection As SqlConnection = Nothing
      Dim tblExistingExposureDates As New DataTable

      Try
        ' First, Select Trade Blotter entries reflecting trades against previous versions of the given transaction.
        ' 

        ThisConnection = MainForm.GetVeniceConnection
        ThisCommand.CommandType = CommandType.Text
        ThisCommand.CommandText = "SELECT DISTINCT expDate FROM dbo.fn_tblExposure_SelectKD(null) WHERE expFund IN (" & SelectString & ") ORDER BY expDate DESC"
        ThisCommand.Connection = ThisConnection

        MainForm.LoadTable_Custom(tblExistingExposureDates, ThisCommand)
        'tblExistingExposureDates.Load(ThisCommand.ExecuteReader)

        Call MainForm.SetTblGenericCombo( _
        Me.Combo_ExistingStartDates, _
        tblExistingExposureDates.Select, _
        "expDate", _
        "expDate", _
        "", False, False, True)       ' 

        Call MainForm.SetTblGenericCombo( _
        Me.Combo_ExistingEndDates, _
        tblExistingExposureDates.Select, _
        "expDate", _
        "expDate", _
        "", False, False, True)       ' 

      Catch ex As Exception
      Finally
        Try
          If (ThisConnection IsNot Nothing) Then
            ThisConnection.Close()
          End If
        Catch ex As Exception
        End Try

        ThisCommand = Nothing
      End Try

    Else
      Combo_ExistingStartDates.Items.Clear()
    End If



  End Sub
#End Region

#Region " Control Events"

  ''' <summary>
  ''' Handles the CheckedChanged event of the FromRadios control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub FromRadios_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_Live.CheckedChanged, Radio_WholeDay.CheckedChanged, Radio_PreciseTime.CheckedChanged
    ' *******************************************************************************
    '
    '
    ' *******************************************************************************

    If Me.Radio_Live.Checked = True Then
      Me.Date_KnowledgeDate.Enabled = False
      Me.Date_KnowledgeDate.Value = Now.Date
    Else
      Me.Date_KnowledgeDate.Enabled = True

      If Me.Radio_WholeDay.Checked = True Then
        Me.Date_KnowledgeDate.CustomFormat = DISPLAYMEMBER_DATEFORMAT
      Else
        Me.Date_KnowledgeDate.CustomFormat = DISPLAYMEMBER_LONGDATEFORMAT
      End If
    End If

  End Sub


  ''' <summary>
  ''' Handles the SelectedIndexChanged event of the Combo_Bookmark control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_Bookmark_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_Bookmark.SelectedIndexChanged
    ' *****************************************************************************
    ' If a Bookmark Date is selected, update the Displayed Knowledgedate.
    ' *****************************************************************************

    If Combo_Bookmark.SelectedIndex > 0 Then
      If IsDate(Combo_Bookmark.SelectedValue) Then
        If CDate(Combo_Bookmark.SelectedValue).TimeOfDay.TotalSeconds >= LAST_SECOND Then
          Me.Radio_PreciseTime.Checked = False
          Me.Radio_WholeDay.Checked = True
          Me.Date_KnowledgeDate.Value = CDate(Combo_Bookmark.SelectedValue)
        Else
          Me.Radio_PreciseTime.Checked = True
          Me.Date_KnowledgeDate.Value = CDate(Combo_Bookmark.SelectedValue)
        End If
      End If
      Combo_Bookmark.SelectedIndex = 0
    End If

  End Sub

  ''' <summary>
  ''' Handles the LostFocus event of the Combo_MinimumUsage control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_MinimumUsage_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Combo_MinimumUsage.LostFocus

    Dim ValueText As String

    Try
      ValueText = Combo_MinimumUsage.Text.Trim

      If ValueText.EndsWith("%") AndAlso (ValueText.Length > 1) Then
        ValueText = ValueText.Substring(0, ValueText.Length - 1)
      End If
      If IsNumeric(ValueText) Then
        If CDbl(ValueText) <= 1 Then
          ' Form a percentage
          Combo_MinimumUsage.Text = (CDbl(ValueText) * 100).ToString & "%"
        Else
          ' Is a Percentage
          Combo_MinimumUsage.Text = ValueText & "%"
        End If
      End If
    Catch ex As Exception
    End Try

  End Sub


  ''' <summary>
  ''' Handles the SelectedIndexChanged event of the Combo_ExistingDates control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_ExistingDates_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_ExistingStartDates.SelectedIndexChanged, Combo_ExistingEndDates.SelectedIndexChanged
    Try
      If (Combo_ExistingStartDates.Focused) Then
        If (Combo_ExistingStartDates.SelectedIndex > 0) AndAlso (IsDate(Combo_ExistingStartDates.SelectedValue)) Then
          Me.Date_ReportStartDate.Value = CDate(Combo_ExistingStartDates.SelectedValue)
        End If
      End If

      If (Combo_ExistingEndDates.Focused) Then
        If (Combo_ExistingEndDates.SelectedIndex > 0) AndAlso (IsDate(Combo_ExistingEndDates.SelectedValue)) Then
          Me.Date_ReportEndDate.Value = CDate(Combo_ExistingEndDates.SelectedValue)
        End If
      End If
    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Handles the SelectedIndexChanged event of the List_Fund control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub List_Fund_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles List_Fund.SelectedIndexChanged
    Try
      If (List_Fund.Focused) Then
        If (List_Fund.SelectedItems.Count > 0) Then
          Call Set_ExistingDateCombo()
        End If
      End If

    Catch ex As Exception
    End Try
  End Sub

#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "


  ''' <summary>
  ''' Handles the Click event of the btnRunReport control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnRunReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRunReport.Click
    ' *****************************************************************************
    ' Run Report
    ' *****************************************************************************

    Try
      FormControls = MainForm.MainReportHandler.DisableFormControls(Me)
      Form_ProgressBar.Value = Form_ProgressBar.Minimum
      MainForm.SetToolStripText(Label_Status, "Processing Report")

      ReportTimer.Interval = 25
      ReportTimer.Tag = Me.Form_ProgressBar
      ReportTimer.Start()

      If MainForm.UseReportWorkerThreads Then

        If (ReportWorker Is Nothing) Then
          ReportWorker = New BackgroundWorkerThreadSafe(Nothing)
          AddHandler ReportWorker.DoWork, AddressOf ReportWorker_DoWork
          AddHandler ReportWorker.RunWorkerCompleted, AddressOf ReportWorkerCompleted
        End If

        ReportWorker.RunWorkerAsync()
      Else
        RunReport()
        Call ReportWorkerCompleted(Nothing, Nothing)
      End If


    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
    End Try
  End Sub

  ''' <summary>
  ''' Handles the DoWork event of the ReportWorker control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.ComponentModel.DoWorkEventArgs"/> instance containing the event data.</param>
  Private Sub ReportWorker_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) ' Handles backgroundWorker1.DoWork
    RunReport()
  End Sub

  ''' <summary>
  ''' Reports the worker completed.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="System.ComponentModel.RunWorkerCompletedEventArgs"/> instance containing the event data.</param>
  Private Sub ReportWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) ' Handles backgroundWorker1.RunWorkerCompleted
    Try
      ReportTimer.Stop()
      MainForm.MainReportHandler.EnableFormControls(FormControls)
      MainForm.SetToolStripText(Label_Status, "")

      'Form_ProgressBar = MainForm.ResetProgressBar(Form_StatusStrip, Form_ProgressBar)
      Form_ProgressBar.Value = Form_ProgressBar.Minimum
      Form_ProgressBar.Visible = False
      ReportTimer.Tag = Me.Form_ProgressBar
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
    End Try

  End Sub

  ''' <summary>
  ''' Runs the report.
  ''' </summary>
  Private Sub RunReport()
    ' *****************************************************************************
    ' Run Report
    ' *****************************************************************************

    Dim ReportStartDate As Date
    Dim ReportEndDate As Date
    Dim ThisReportDate As Date

    ReportStartDate = MainForm.GetDatetimeValue(Date_ReportStartDate).Date
    ReportEndDate = MainForm.GetDatetimeValue(Date_ReportEndDate).Date
    ThisReportDate = ReportStartDate


    ' Validate

    Dim SelectedFundListItems() As Object

    SelectedFundListItems = MainForm.GetListBoxSelectedItems(Me.List_Fund)

    If (SelectedFundListItems Is Nothing) OrElse (SelectedFundListItems.Length <= 0) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Fund(s) must be selected.", "", True)
      Exit Sub
    End If

    While ThisReportDate <= ReportEndDate
      RunReport_OnDate(ThisReportDate)
      ThisReportDate = RenaissanceUtilities.DatePeriodFunctions.AddPeriodToDate(DealingPeriod.Daily, ThisReportDate, 1)
    End While

  End Sub

  ''' <summary>
  ''' Runs the report_ on date.
  ''' </summary>
  ''' <param name="pReportDate">The p report date.</param>
  Private Sub RunReport_OnDate(ByVal pReportDate As Date)
    ' *****************************************************************************
    ' Run Report
    ' *****************************************************************************

    Dim ReportDate As Date = pReportDate
    Dim PertracDate As Date ' End of calendar month previous to Report Date
    Dim SelectLimitCategory As Integer
    Dim SelectMinimumUsage As Double
    Dim SelectInstrument As Integer
    Dim DetailedExposuresReport As Integer = 0
    Dim TransactionStatusGroup As String = ""
    Dim SelectedRow As DataRowView
    Dim FundIDArray(-1) As Integer
    Dim FundArrayIndex As Integer = 0
    Dim thisFundID As Integer
    Dim thisFundName As String
    Dim CombineTheExposuresReports As Boolean = False
    Dim AdminDatesFilter As Integer = DEFAULT_ADMINISTRATORDATESFILTER

    Dim PreciseSystemKnowledgedate As Date
    Dim PreciseExposuresKnowledgeDate As Date

    Dim AggregateRisk As Boolean
    Dim ExposureFlags As Long

    ' Validate

    Dim SelectedFundListItems() As Object

    SelectedFundListItems = MainForm.GetListBoxSelectedItems(Me.List_Fund)

    If (SelectedFundListItems Is Nothing) OrElse (SelectedFundListItems.Length <= 0) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Fund(s) must be selected.", "", True)
      Exit Sub
    End If

    TransactionStatusGroup = ""
    If MainForm.GetComboSelectedIndex(Combo_TransactionGroup) > 0 Then
      TransactionStatusGroup = MainForm.GetComboSelectedValue(Combo_TransactionGroup).ToString
      ExposureFlags = ExposureFlags Or RiskExposureFlags.TransactionStatusGroup
    End If

    PertracDate = ReportDate.AddDays(0 - ReportDate.Day)
    CombineTheExposuresReports = MainForm.GetCheckBoxChecked(Check_CompositeReport)

    ' Resolve Selection Criteria.

    SelectLimitCategory = 0
    If MainForm.GetComboSelectedIndex(Combo_LimitCategory) > 0 Then
      Try
        SelectLimitCategory = CInt(MainForm.GetComboSelectedValue(Combo_LimitCategory))
      Catch ex As Exception
      End Try
    End If

    SelectInstrument = 0
    If MainForm.GetComboSelectedIndex(Combo_Instrument) > 0 Then
      Try
        SelectInstrument = CInt(MainForm.GetComboSelectedValue(Combo_Instrument))
      Catch ex As Exception
      End Try
    End If

    SelectMinimumUsage = 0
    Try
      Dim ValueString As String
      ValueString = MainForm.GetControlText(Combo_MinimumUsage).Trim

      If ValueString.EndsWith("%") Then
        ValueString = ValueString.Substring(0, ValueString.Length - 1)
      End If

      If IsNumeric(ValueString) Then
        Try
          SelectMinimumUsage = CDbl(ValueString) / 100.0#
        Catch ex As Exception
        End Try
      End If
    Catch ex As Exception
    End Try

    ' DetailedExposuresReport

    If MainForm.GetCheckBoxChecked(Check_DetailReport) Then
      DetailedExposuresReport = 1
    End If

    ' Aggregate Risk Flags and Exposure Flags

    AggregateRisk = False
    ExposureFlags = 0

    If MainForm.GetCheckBoxChecked(Check_AggregateToParent) Then
      AggregateRisk = True
      ExposureFlags = ExposureFlags Or RiskExposureFlags.AggregatedRisk
    End If

    If MainForm.GetCheckBoxChecked(Check_LookthroughRisk) Then
      ExposureFlags = ExposureFlags Or RiskExposureFlags.FundLookthrough
    End If

    If MainForm.GetCheckBoxChecked(Check_SeparateFXForwards) Then
      ExposureFlags = ExposureFlags Or RiskExposureFlags.FXForwardsSeparate ' So that changing this option will cause the Risk data to be re-calculated.
      AdminDatesFilter = AdminDatesFilter Or AdministratorDatesFilter.FXForwardsSeparate
    End If

    If MainForm.GetCheckBoxChecked(Check_SeparateFXTrades) Then
      ExposureFlags = ExposureFlags Or RiskExposureFlags.FXTradesSeparate ' So that changing this option will cause the Risk data to be re-calculated.
      AdminDatesFilter = AdminDatesFilter Or AdministratorDatesFilter.FXTradesSeparate
    End If

    ' Resolve KnowledgeDates

    PreciseSystemKnowledgedate = MainForm.Main_Knowledgedate

    If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) And (MainForm.Main_Knowledgedate.TimeOfDay.TotalSeconds >= LAST_SECOND) Then
      ExposureFlags = ExposureFlags Or RiskExposureFlags.SystemKnowledgeDateWholeDay
    End If

    PreciseExposuresKnowledgeDate = KNOWLEDGEDATE_NOW
    If MainForm.GetRadioChecked(Radio_WholeDay) = True Then
      PreciseExposuresKnowledgeDate = MainForm.GetDatetimeValue(Date_KnowledgeDate).Date
      PreciseExposuresKnowledgeDate = PreciseExposuresKnowledgeDate.AddSeconds(LAST_SECOND)
      ExposureFlags = ExposureFlags Or RiskExposureFlags.KnowledgeDateWholeDay
    ElseIf Me.Radio_PreciseTime.Checked = True Then
      PreciseExposuresKnowledgeDate = MainForm.GetDatetimeValue(Date_KnowledgeDate)
    End If

    ' *********************************************************
    ' Run Reports for each fund in order.
    ' *********************************************************

    If (SelectedFundListItems.Count > 0) Then
      ReDim FundIDArray(SelectedFundListItems.Count - 1)
    End If

    For Each SelectedRow In SelectedFundListItems
      thisFundID = CType(SelectedRow.Row, RenaissanceDataClass.DSFund.tblFundRow).FundID
      thisFundName = CType(SelectedRow.Row, RenaissanceDataClass.DSFund.tblFundRow).FundName
      FundIDArray(FundArrayIndex) = thisFundID
      FundArrayIndex += 1

      ' *********************************************************
      ' 2) Check Exposure Information Exists and is current
      ' *********************************************************

      MainForm.SetToolStripText(Label_Status, "Calculating Exposures : " & thisFundName)
      If Not (MainForm.InvokeRequired) Then
        Application.DoEvents()
      End If

      ' 2) Check Exposure Information Exists and is current
      If (MainForm.GetCheckBoxChecked(Check_RecreateExposures) = True) Or (TransactionStatusGroup.Length > 0) Then
        ' Always Re-Create information if the Check box is checked

        If Risk_ProcessAllLimits(MainForm, Label_Status, thisFundID, thisFundName, PertracDate, ReportDate, PreciseExposuresKnowledgeDate, ExposureFlags, TransactionStatusGroup, AdminDatesFilter) = False Then
          MainForm.SetToolStripText(Label_Status, "Status : Error processing Risk Limits")
          MainForm.LogError(Me.Name & ", btnRunReport_Click", LOG_LEVELS.Warning, "", "Error processing Risk Limits.", "", True)
          Exit Sub
        End If
      ElseIf CheckExposuresExist(MainForm, Label_Status, thisFundID, ReportDate, PreciseExposuresKnowledgeDate, ExposureFlags) = False Then
        ' Create Exposures if they do not already exist

        If Risk_ProcessAllLimits(MainForm, Label_Status, thisFundID, thisFundName, PertracDate, ReportDate, PreciseExposuresKnowledgeDate, ExposureFlags, TransactionStatusGroup, AdminDatesFilter) = False Then
          MainForm.SetToolStripText(Label_Status, "Status : Error processing Risk Limits")
          MainForm.LogError(Me.Name & ", btnRunReport_Click", LOG_LEVELS.Warning, "", "Error processing Risk Limits.", "", True)
          Exit Sub
        End If
      Else
        ' Fund / Date / KnowledgeDate combination exists, but is it current ?
        ' If Knowledgedate was not 'Now' then of course it is, otherwise check underlying tables for updates

        If CheckExposuresAreCurrent(MainForm, Label_Status, thisFundID, ReportDate, PreciseExposuresKnowledgeDate, ExposureFlags) = False Then
          ' Recreate Exposures if Transactions / FX / Price or Limits have been Added / Changed / Updated.

          If Risk_ProcessAllLimits(MainForm, Label_Status, thisFundID, thisFundName, PertracDate, ReportDate, PreciseExposuresKnowledgeDate, ExposureFlags, TransactionStatusGroup, AdminDatesFilter) = False Then
            MainForm.SetToolStripText(Label_Status, "Status : Error processing Risk Limits")
            MainForm.LogError(Me.Name & ", btnRunReport_Click", LOG_LEVELS.Warning, "", "Error processing Risk Limits.", "", True)
            Exit Sub
          End If
        End If

      End If


      ' *********************************************************
      ' 3) Run Report
      ' *********************************************************

      MainForm.SetToolStripText(Label_Status, "Retrieving Data : " & thisFundName)
      If Not (MainForm.InvokeRequired) Then
        Application.DoEvents()
      End If

      If (CombineTheExposuresReports = False) Then
        Try
          Dim ReportData As DataTable

          ReportData = MainForm.MainReportHandler.GetData_rptRiskExposuresReport(thisFundID, DetailedExposuresReport, ReportDate, SelectLimitCategory, SelectInstrument, SelectMinimumUsage, ExposureFlags, PreciseExposuresKnowledgeDate, PreciseSystemKnowledgedate)

          '----
          ' test sort of the DataTable, commented sort done directmly in the proc rptRiskExposuresReport
          '----
          'Dim ReportData1 As DataTable
          'ReportData1 = MainForm.MainReportHandler.GetData_rptRiskExposuresReport(thisFundID, DetailedExposuresReport, ReportDate, SelectLimitCategory, SelectInstrument, SelectMinimumUsage, ExposureFlags, PreciseExposuresKnowledgeDate, PreciseSystemKnowledgedate)
          'Dim rows As DataRow() = ReportData1.Select("", "ltpLimitType,ltpSortOrder,DataCategory,DataCharacteristic,InstrumentDescription,expLimitGreaterOrLessThan,expLimit,InstrumentDescriptionDetail")
          'ReportData = New DataTable()
          'For Each col As DataColumn In ReportData1.Columns
          '  ReportData.Columns.Add(col.ColumnName, col.DataType)
          'Next col
          'For Each row As DataRow In rows
          '  ReportData.ImportRow(row)
          'Next row

          ''For debug: display the exposures an see if they are sorted
          'If DetailedExposuresReport <> 0 Then
          '  Dim header As String() = {"RN", "DataCategory", "expLimitSectorName", "DataCharacteristic", "InstrumentDescription", "InstrumentDescriptionDetail"}
          '  Dim lines As New ArrayList()

          '  Dim line As String = ""
          '  Dim i As Integer

          '  For i = 0 To header.Length - 1
          '    line = line & header(i) & ","
          '  Next i
          '  lines.Add(line)

          '  For Each row As DataRow In ReportData.Rows
          '    If row.Item("DataCategory").ToString = "Eligibilité" Then

          '      'And (row.Item("DataCharacteristic").ToString = "67" or row.Item("DataCharacteristic").ToString="Instruments Dérivés")_
          '      line = ""
          '      For i = 0 To header.Length - 1
          '        line = line & row.Item(header(i)).ToString & ","
          '      Next i
          '      lines.Add(line)

          '    End If

          '  Next row

          '  Dim sa() As String = DirectCast(lines.ToArray(GetType(String)), String())

          '  Try
          '    System.IO.File.WriteAllLines("C:\aatempExtracts\exposures.csv", sa)
          '  Catch ex As Exception
          '    System.Console.Write("ERROR saving file: " & ex.Message)

          '  End Try

          'End If

          Me.MainForm.SetToolStripText(Label_Status, "Processing Report : " & thisFundName)
          If Not (MainForm.InvokeRequired) Then
            Application.DoEvents()
          End If

          If (Not (ReportData Is Nothing)) AndAlso (ReportData.Rows.Count > 0) Then
            Call MainForm.MainReportHandler.rptRiskExposuresReport(ReportData, thisFundID, DetailedExposuresReport, ReportDate, SelectLimitCategory, SelectInstrument, SelectMinimumUsage, ExposureFlags, PreciseExposuresKnowledgeDate, PreciseSystemKnowledgedate, CombineTheExposuresReports, Nothing) 'Form_ProgressBar)
          Else
            MainForm.LogError(Me.Name & ", btnRunReport_Click", LOG_LEVELS.Warning, "", "There is no Exposure Data for fund : " & thisFundName, "", True)
          End If

        Catch ex As Exception
        End Try
      End If
    Next

    ' Combined Report...

    If CombineTheExposuresReports Then
      Try
        Dim ReportData As DataTable

        ReportData = MainForm.MainReportHandler.GetData_rptRiskExposuresReport(FundIDArray, DetailedExposuresReport, ReportDate, SelectLimitCategory, SelectInstrument, SelectMinimumUsage, ExposureFlags, PreciseExposuresKnowledgeDate, PreciseSystemKnowledgedate)

        Me.MainForm.SetToolStripText(Label_Status, "Processing Exposures Report")
        If Not (MainForm.InvokeRequired) Then
          Application.DoEvents()
        End If

        If (Not (ReportData Is Nothing)) AndAlso (ReportData.Rows.Count > 0) Then
          Call MainForm.MainReportHandler.rptRiskExposuresReport(ReportData, thisFundID, DetailedExposuresReport, ReportDate, SelectLimitCategory, SelectInstrument, SelectMinimumUsage, ExposureFlags, PreciseExposuresKnowledgeDate, PreciseSystemKnowledgedate, CombineTheExposuresReports, Nothing) 'Form_ProgressBar)
        Else
          MainForm.LogError(Me.Name & ", btnRunReport_Click", LOG_LEVELS.Warning, "", "There is no Exposure Data.", "", True)
        End If

      Catch ex As Exception
      End Try

    End If

  End Sub

  ''' <summary>
  ''' Handles the Click event of the btnClose control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
    ' *****************************************************************************
    ' Close Form
    ' *****************************************************************************

    Me.Close()

  End Sub

#End Region


End Class


