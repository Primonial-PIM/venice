' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmPortfolioLargePositionsReport.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals


''' <summary>
''' Class frmPortfolioLargePositionsReport
''' </summary>
Public Class frmPortfolioLargePositionsReport

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmPortfolioLargePositionsReport"/> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The BTN run report
    ''' </summary>
  Friend WithEvents btnRunReport As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN close
    ''' </summary>
  Friend WithEvents btnClose As System.Windows.Forms.Button
    ''' <summary>
    ''' The label2
    ''' </summary>
  Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ portfolio description
    ''' </summary>
  Friend WithEvents Combo_PortfolioDescription As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ filing date
    ''' </summary>
  Friend WithEvents Combo_FilingDate As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label1
    ''' </summary>
  Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The radio_ all portfolios
    ''' </summary>
  Friend WithEvents Radio_AllPortfolios As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The radio_ single portfolio
    ''' </summary>
  Friend WithEvents Radio_SinglePortfolio As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The radio_ selected portfolios
    ''' </summary>
  Friend WithEvents Radio_SelectedPortfolios As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The grid_ portfolios
    ''' </summary>
  Friend WithEvents Grid_Portfolios As C1.Win.C1FlexGrid.C1FlexGrid
    ''' <summary>
    ''' The panel_ portfolio select
    ''' </summary>
  Friend WithEvents Panel_PortfolioSelect As System.Windows.Forms.Panel
    ''' <summary>
    ''' The panel_ position select
    ''' </summary>
  Friend WithEvents Panel_PositionSelect As System.Windows.Forms.Panel
    ''' <summary>
    ''' The radio_ all positions
    ''' </summary>
  Friend WithEvents Radio_AllPositions As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The radio_ top N positions
    ''' </summary>
  Friend WithEvents Radio_TopNPositions As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The radio_ position by USD
    ''' </summary>
  Friend WithEvents Radio_PositionByUSD As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The spin_ top N positions
    ''' </summary>
  Friend WithEvents Spin_TopNPositions As System.Windows.Forms.NumericUpDown
    ''' <summary>
    ''' The spin_ positions by USD value
    ''' </summary>
  Friend WithEvents Spin_PositionsByUSDValue As System.Windows.Forms.NumericUpDown
    ''' <summary>
    ''' The label3
    ''' </summary>
  Friend WithEvents Label3 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label4
    ''' </summary>
  Friend WithEvents Label4 As System.Windows.Forms.Label
    ''' <summary>
    ''' The BTN_ grid all
    ''' </summary>
  Friend WithEvents btn_GridAll As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN_ grid none
    ''' </summary>
  Friend WithEvents btn_GridNone As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN_ grid latest
    ''' </summary>
  Friend WithEvents btn_GridLatest As System.Windows.Forms.Button
    ''' <summary>
    ''' The combo_ report to run
    ''' </summary>
  Friend WithEvents Combo_ReportToRun As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label5
    ''' </summary>
  Friend WithEvents Label5 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ reference fund
    ''' </summary>
  Friend WithEvents Combo_ReferenceFund As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The form_ status strip
    ''' </summary>
  Friend WithEvents Form_StatusStrip As System.Windows.Forms.StatusStrip
    ''' <summary>
    ''' The form_ progress bar
    ''' </summary>
  Friend WithEvents Form_ProgressBar As System.Windows.Forms.ToolStripProgressBar
    ''' <summary>
    ''' The label_ status
    ''' </summary>
  Friend WithEvents Label_Status As System.Windows.Forms.ToolStripStatusLabel
    ''' <summary>
    ''' The label6
    ''' </summary>
  Friend WithEvents Label6 As System.Windows.Forms.Label
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPortfolioLargePositionsReport))
    Me.btnRunReport = New System.Windows.Forms.Button
    Me.btnClose = New System.Windows.Forms.Button
    Me.Label2 = New System.Windows.Forms.Label
    Me.Combo_PortfolioDescription = New System.Windows.Forms.ComboBox
    Me.Combo_FilingDate = New System.Windows.Forms.ComboBox
    Me.Label1 = New System.Windows.Forms.Label
    Me.Radio_AllPortfolios = New System.Windows.Forms.RadioButton
    Me.Radio_SinglePortfolio = New System.Windows.Forms.RadioButton
    Me.Radio_SelectedPortfolios = New System.Windows.Forms.RadioButton
    Me.Grid_Portfolios = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.Panel_PortfolioSelect = New System.Windows.Forms.Panel
    Me.btn_GridLatest = New System.Windows.Forms.Button
    Me.btn_GridNone = New System.Windows.Forms.Button
    Me.btn_GridAll = New System.Windows.Forms.Button
    Me.Panel_PositionSelect = New System.Windows.Forms.Panel
    Me.Spin_PositionsByUSDValue = New System.Windows.Forms.NumericUpDown
    Me.Spin_TopNPositions = New System.Windows.Forms.NumericUpDown
    Me.Radio_PositionByUSD = New System.Windows.Forms.RadioButton
    Me.Radio_TopNPositions = New System.Windows.Forms.RadioButton
    Me.Radio_AllPositions = New System.Windows.Forms.RadioButton
    Me.Label3 = New System.Windows.Forms.Label
    Me.Label4 = New System.Windows.Forms.Label
    Me.Combo_ReportToRun = New System.Windows.Forms.ComboBox
    Me.Label5 = New System.Windows.Forms.Label
    Me.Combo_ReferenceFund = New System.Windows.Forms.ComboBox
    Me.Label6 = New System.Windows.Forms.Label
    Me.Form_StatusStrip = New System.Windows.Forms.StatusStrip
    Me.Form_ProgressBar = New System.Windows.Forms.ToolStripProgressBar
    Me.Label_Status = New System.Windows.Forms.ToolStripStatusLabel
    CType(Me.Grid_Portfolios, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Panel_PortfolioSelect.SuspendLayout()
    Me.Panel_PositionSelect.SuspendLayout()
    CType(Me.Spin_PositionsByUSDValue, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Spin_TopNPositions, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Form_StatusStrip.SuspendLayout()
    Me.SuspendLayout()
    '
    'btnRunReport
    '
    Me.btnRunReport.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnRunReport.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnRunReport.Location = New System.Drawing.Point(44, 526)
    Me.btnRunReport.Name = "btnRunReport"
    Me.btnRunReport.Size = New System.Drawing.Size(148, 28)
    Me.btnRunReport.TabIndex = 4
    Me.btnRunReport.Text = "Run Report"
    '
    'btnClose
    '
    Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnClose.Location = New System.Drawing.Point(260, 526)
    Me.btnClose.Name = "btnClose"
    Me.btnClose.Size = New System.Drawing.Size(75, 28)
    Me.btnClose.TabIndex = 5
    Me.btnClose.Text = "&Close"
    '
    'Label2
    '
    Me.Label2.Location = New System.Drawing.Point(28, 64)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(100, 20)
    Me.Label2.TabIndex = 53
    Me.Label2.Text = "Portfolio Name"
    '
    'Combo_PortfolioDescription
    '
    Me.Combo_PortfolioDescription.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_PortfolioDescription.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_PortfolioDescription.Location = New System.Drawing.Point(132, 60)
    Me.Combo_PortfolioDescription.Name = "Combo_PortfolioDescription"
    Me.Combo_PortfolioDescription.Size = New System.Drawing.Size(252, 21)
    Me.Combo_PortfolioDescription.TabIndex = 2
    '
    'Combo_FilingDate
    '
    Me.Combo_FilingDate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_FilingDate.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_FilingDate.Location = New System.Drawing.Point(132, 92)
    Me.Combo_FilingDate.Name = "Combo_FilingDate"
    Me.Combo_FilingDate.Size = New System.Drawing.Size(252, 21)
    Me.Combo_FilingDate.TabIndex = 3
    '
    'Label1
    '
    Me.Label1.Location = New System.Drawing.Point(28, 96)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(100, 20)
    Me.Label1.TabIndex = 104
    Me.Label1.Text = "Filing Date"
    '
    'Radio_AllPortfolios
    '
    Me.Radio_AllPortfolios.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Radio_AllPortfolios.Location = New System.Drawing.Point(8, 4)
    Me.Radio_AllPortfolios.Name = "Radio_AllPortfolios"
    Me.Radio_AllPortfolios.Size = New System.Drawing.Size(368, 24)
    Me.Radio_AllPortfolios.TabIndex = 0
    Me.Radio_AllPortfolios.Text = "All Portfolios"
    '
    'Radio_SinglePortfolio
    '
    Me.Radio_SinglePortfolio.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Radio_SinglePortfolio.Location = New System.Drawing.Point(8, 32)
    Me.Radio_SinglePortfolio.Name = "Radio_SinglePortfolio"
    Me.Radio_SinglePortfolio.Size = New System.Drawing.Size(368, 24)
    Me.Radio_SinglePortfolio.TabIndex = 1
    Me.Radio_SinglePortfolio.Text = "Single Portfolio"
    '
    'Radio_SelectedPortfolios
    '
    Me.Radio_SelectedPortfolios.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Radio_SelectedPortfolios.Location = New System.Drawing.Point(8, 120)
    Me.Radio_SelectedPortfolios.Name = "Radio_SelectedPortfolios"
    Me.Radio_SelectedPortfolios.Size = New System.Drawing.Size(160, 24)
    Me.Radio_SelectedPortfolios.TabIndex = 4
    Me.Radio_SelectedPortfolios.Text = "Selected Portfolios"
    '
    'Grid_Portfolios
    '
    Me.Grid_Portfolios.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_Portfolios.ColumnInfo = resources.GetString("Grid_Portfolios.ColumnInfo")
    Me.Grid_Portfolios.Location = New System.Drawing.Point(32, 148)
    Me.Grid_Portfolios.Name = "Grid_Portfolios"
    Me.Grid_Portfolios.Rows.DefaultSize = 17
    Me.Grid_Portfolios.Size = New System.Drawing.Size(352, 102)
    Me.Grid_Portfolios.TabIndex = 5
    '
    'Panel_PortfolioSelect
    '
    Me.Panel_PortfolioSelect.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Panel_PortfolioSelect.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Panel_PortfolioSelect.Controls.Add(Me.btn_GridLatest)
    Me.Panel_PortfolioSelect.Controls.Add(Me.btn_GridNone)
    Me.Panel_PortfolioSelect.Controls.Add(Me.btn_GridAll)
    Me.Panel_PortfolioSelect.Controls.Add(Me.Grid_Portfolios)
    Me.Panel_PortfolioSelect.Controls.Add(Me.Label2)
    Me.Panel_PortfolioSelect.Controls.Add(Me.Combo_PortfolioDescription)
    Me.Panel_PortfolioSelect.Controls.Add(Me.Combo_FilingDate)
    Me.Panel_PortfolioSelect.Controls.Add(Me.Label1)
    Me.Panel_PortfolioSelect.Controls.Add(Me.Radio_AllPortfolios)
    Me.Panel_PortfolioSelect.Controls.Add(Me.Radio_SinglePortfolio)
    Me.Panel_PortfolioSelect.Controls.Add(Me.Radio_SelectedPortfolios)
    Me.Panel_PortfolioSelect.Location = New System.Drawing.Point(4, 80)
    Me.Panel_PortfolioSelect.Name = "Panel_PortfolioSelect"
    Me.Panel_PortfolioSelect.Size = New System.Drawing.Size(392, 258)
    Me.Panel_PortfolioSelect.TabIndex = 2
    '
    'btn_GridLatest
    '
    Me.btn_GridLatest.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btn_GridLatest.Location = New System.Drawing.Point(276, 124)
    Me.btn_GridLatest.Name = "btn_GridLatest"
    Me.btn_GridLatest.Size = New System.Drawing.Size(44, 20)
    Me.btn_GridLatest.TabIndex = 8
    Me.btn_GridLatest.Text = "Latest"
    '
    'btn_GridNone
    '
    Me.btn_GridNone.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btn_GridNone.Location = New System.Drawing.Point(224, 124)
    Me.btn_GridNone.Name = "btn_GridNone"
    Me.btn_GridNone.Size = New System.Drawing.Size(44, 20)
    Me.btn_GridNone.TabIndex = 7
    Me.btn_GridNone.Text = "None"
    '
    'btn_GridAll
    '
    Me.btn_GridAll.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btn_GridAll.Location = New System.Drawing.Point(172, 124)
    Me.btn_GridAll.Name = "btn_GridAll"
    Me.btn_GridAll.Size = New System.Drawing.Size(44, 20)
    Me.btn_GridAll.TabIndex = 6
    Me.btn_GridAll.Text = "All"
    '
    'Panel_PositionSelect
    '
    Me.Panel_PositionSelect.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Panel_PositionSelect.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Panel_PositionSelect.Controls.Add(Me.Spin_PositionsByUSDValue)
    Me.Panel_PositionSelect.Controls.Add(Me.Spin_TopNPositions)
    Me.Panel_PositionSelect.Controls.Add(Me.Radio_PositionByUSD)
    Me.Panel_PositionSelect.Controls.Add(Me.Radio_TopNPositions)
    Me.Panel_PositionSelect.Controls.Add(Me.Radio_AllPositions)
    Me.Panel_PositionSelect.Location = New System.Drawing.Point(4, 358)
    Me.Panel_PositionSelect.Name = "Panel_PositionSelect"
    Me.Panel_PositionSelect.Size = New System.Drawing.Size(392, 160)
    Me.Panel_PositionSelect.TabIndex = 3
    '
    'Spin_PositionsByUSDValue
    '
    Me.Spin_PositionsByUSDValue.Increment = New Decimal(New Integer() {100000, 0, 0, 0})
    Me.Spin_PositionsByUSDValue.Location = New System.Drawing.Point(24, 120)
    Me.Spin_PositionsByUSDValue.Maximum = New Decimal(New Integer() {-727379968, 232, 0, 0})
    Me.Spin_PositionsByUSDValue.Name = "Spin_PositionsByUSDValue"
    Me.Spin_PositionsByUSDValue.Size = New System.Drawing.Size(148, 20)
    Me.Spin_PositionsByUSDValue.TabIndex = 4
    Me.Spin_PositionsByUSDValue.ThousandsSeparator = True
    Me.Spin_PositionsByUSDValue.Value = New Decimal(New Integer() {1, 0, 0, 0})
    '
    'Spin_TopNPositions
    '
    Me.Spin_TopNPositions.Location = New System.Drawing.Point(24, 60)
    Me.Spin_TopNPositions.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
    Me.Spin_TopNPositions.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
    Me.Spin_TopNPositions.Name = "Spin_TopNPositions"
    Me.Spin_TopNPositions.Size = New System.Drawing.Size(148, 20)
    Me.Spin_TopNPositions.TabIndex = 2
    Me.Spin_TopNPositions.ThousandsSeparator = True
    Me.Spin_TopNPositions.Value = New Decimal(New Integer() {1, 0, 0, 0})
    '
    'Radio_PositionByUSD
    '
    Me.Radio_PositionByUSD.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Radio_PositionByUSD.Location = New System.Drawing.Point(8, 92)
    Me.Radio_PositionByUSD.Name = "Radio_PositionByUSD"
    Me.Radio_PositionByUSD.Size = New System.Drawing.Size(368, 24)
    Me.Radio_PositionByUSD.TabIndex = 3
    Me.Radio_PositionByUSD.Text = "Positions of 'n' USD or more."
    '
    'Radio_TopNPositions
    '
    Me.Radio_TopNPositions.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Radio_TopNPositions.Location = New System.Drawing.Point(8, 32)
    Me.Radio_TopNPositions.Name = "Radio_TopNPositions"
    Me.Radio_TopNPositions.Size = New System.Drawing.Size(368, 24)
    Me.Radio_TopNPositions.TabIndex = 1
    Me.Radio_TopNPositions.Text = "Top 'n' Positions, by Value"
    '
    'Radio_AllPositions
    '
    Me.Radio_AllPositions.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Radio_AllPositions.Location = New System.Drawing.Point(8, 4)
    Me.Radio_AllPositions.Name = "Radio_AllPositions"
    Me.Radio_AllPositions.Size = New System.Drawing.Size(368, 24)
    Me.Radio_AllPositions.TabIndex = 0
    Me.Radio_AllPositions.Text = "All Positions"
    '
    'Label3
    '
    Me.Label3.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label3.Location = New System.Drawing.Point(4, 64)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(148, 16)
    Me.Label3.TabIndex = 122
    Me.Label3.Text = "Select Portfolios"
    '
    'Label4
    '
    Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label4.Location = New System.Drawing.Point(4, 342)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(148, 16)
    Me.Label4.TabIndex = 123
    Me.Label4.Text = "Select Positions"
    '
    'Combo_ReportToRun
    '
    Me.Combo_ReportToRun.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_ReportToRun.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_ReportToRun.Items.AddRange(New Object() {"Positions by Fund and Fund Type", "Aggregated Positions"})
    Me.Combo_ReportToRun.Location = New System.Drawing.Point(136, 4)
    Me.Combo_ReportToRun.Name = "Combo_ReportToRun"
    Me.Combo_ReportToRun.Size = New System.Drawing.Size(252, 21)
    Me.Combo_ReportToRun.TabIndex = 0
    '
    'Label5
    '
    Me.Label5.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label5.Location = New System.Drawing.Point(8, 8)
    Me.Label5.Name = "Label5"
    Me.Label5.Size = New System.Drawing.Size(120, 20)
    Me.Label5.TabIndex = 125
    Me.Label5.Text = "Report to Run"
    '
    'Combo_ReferenceFund
    '
    Me.Combo_ReferenceFund.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_ReferenceFund.Location = New System.Drawing.Point(137, 32)
    Me.Combo_ReferenceFund.Name = "Combo_ReferenceFund"
    Me.Combo_ReferenceFund.Size = New System.Drawing.Size(252, 21)
    Me.Combo_ReferenceFund.TabIndex = 1
    '
    'Label6
    '
    Me.Label6.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label6.Location = New System.Drawing.Point(8, 36)
    Me.Label6.Name = "Label6"
    Me.Label6.Size = New System.Drawing.Size(120, 20)
    Me.Label6.TabIndex = 127
    Me.Label6.Text = "Reference Fund"
    '
    'Form_StatusStrip
    '
    Me.Form_StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Form_ProgressBar, Me.Label_Status})
    Me.Form_StatusStrip.Location = New System.Drawing.Point(0, 560)
    Me.Form_StatusStrip.Name = "Form_StatusStrip"
    Me.Form_StatusStrip.Size = New System.Drawing.Size(402, 22)
    Me.Form_StatusStrip.TabIndex = 130
    Me.Form_StatusStrip.Text = " "
    '
    'Form_ProgressBar
    '
    Me.Form_ProgressBar.Maximum = 20
    Me.Form_ProgressBar.Name = "Form_ProgressBar"
    Me.Form_ProgressBar.Size = New System.Drawing.Size(150, 17)
    Me.Form_ProgressBar.Step = 1
    Me.Form_ProgressBar.Visible = False
    '
    'Label_Status
    '
    Me.Label_Status.Name = "Label_Status"
    Me.Label_Status.Size = New System.Drawing.Size(10, 17)
    Me.Label_Status.Text = " "
    '
    'frmPortfolioLargePositionsReport
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(402, 582)
    Me.Controls.Add(Me.Form_StatusStrip)
    Me.Controls.Add(Me.Combo_ReferenceFund)
    Me.Controls.Add(Me.Label6)
    Me.Controls.Add(Me.Combo_ReportToRun)
    Me.Controls.Add(Me.Label5)
    Me.Controls.Add(Me.Label4)
    Me.Controls.Add(Me.Label3)
    Me.Controls.Add(Me.Panel_PositionSelect)
    Me.Controls.Add(Me.Panel_PortfolioSelect)
    Me.Controls.Add(Me.btnClose)
    Me.Controls.Add(Me.btnRunReport)
    Me.Name = "frmPortfolioLargePositionsReport"
    Me.Text = "Portfolio Large Positions Report"
    CType(Me.Grid_Portfolios, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Panel_PortfolioSelect.ResumeLayout(False)
    Me.Panel_PositionSelect.ResumeLayout(False)
    CType(Me.Spin_PositionsByUSDValue, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Spin_TopNPositions, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Form_StatusStrip.ResumeLayout(False)
    Me.Form_StatusStrip.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "


  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
  Private WithEvents _MainForm As VeniceMain

  ' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
  Private FormTooltip As New ToolTip()

    ''' <summary>
    ''' The report worker
    ''' </summary>
	Private ReportWorker As BackgroundWorkerThreadSafe = Nothing
    ''' <summary>
    ''' The form controls
    ''' </summary>
  Private FormControls As ArrayList = Nothing
    ''' <summary>
    ''' The report timer
    ''' </summary>
  Private WithEvents ReportTimer As New Windows.Forms.Timer()

  ' Form Constants, specific to the table being updated.

    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
  Private ALWAYS_CLOSE_THIS_FORM As Boolean = True

  ' The standard ChangeID for this form. e.g. tblPerson
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

  ' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
  Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
  Private THIS_FORM_FormID As VeniceFormID

  ' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
  Private FormIsValid As Boolean
    ''' <summary>
    ''' The form changed
    ''' </summary>
  Private FormChanged As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
  Private _FormOpenFailed As Boolean

  ' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
  Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
  Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
  Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
  Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
  Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
    ' Public property to return handle to the 'Main' Venice form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
  Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
    ' Public property maintaining a value indicating if the cursor is over the 'Cancel'
    ' Button on this form.
    ' This property is specifically designed for use by the field formating Event functions
    ' In order that they do not impose format restrictions if the user is about to click the 
    ' 'Cancel' button.
    '
    Get
      Return False
    End Get
    Set(ByVal Value As Boolean)
    End Set
  End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
  Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
    Get
      Return False
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
    Get
      Return True
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmPortfolioLargePositionsReport"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  Public Sub New(ByVal pMainForm As VeniceMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()

    _MainForm = pMainForm
    AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate
    AddHandler ReportTimer.Tick, AddressOf MainForm.IncrementStatusBarTimerEvent

    _FormOpenFailed = False

    ' ******************************************************
    ' Form Specific Settings :
    ' ******************************************************

    ' Form Permissioning :-

    THIS_FORM_PermissionArea = Me.Name
    THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

    ' 'This' form ID

    THIS_FORM_FormID = VeniceFormID.frmPortfolioLargePositionsReport

    ' Format Event Handlers for form controls

    AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

    AddHandler Combo_PortfolioDescription.KeyUp, AddressOf MainForm.ComboSelectAsYouType

    ' Set up the ToolTip
    MainForm.SetFormToolTip(Me, FormTooltip)

    ' ******************************************************
    ' End Form Specific.
    ' ******************************************************

    THIS_FORM_ChangeID = RenaissanceChangeID.KnowledgeDate

  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
  Public Sub ResetForm() Implements StandardVeniceForm.ResetForm

    Call Form_Load(Me, New System.EventArgs)
  End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
  Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
    ALWAYS_CLOSE_THIS_FORM = True
    Me.Close()
  End Sub

    ''' <summary>
    ''' Handles the FormClosing event of the frmPortfolioLargePositionsReport control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.FormClosingEventArgs"/> instance containing the event data.</param>
  Private Sub frmPortfolioLargePositionsReport_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
    If (ReportWorker IsNot Nothing) Then
      If ReportWorker.IsBusy Then
        e.Cancel = True
        Exit Sub
      End If

      Try
        RemoveHandler ReportTimer.Tick, AddressOf MainForm.IncrementStatusBarTimerEvent
        RemoveHandler ReportWorker.DoWork, AddressOf ReportWorker_DoWork
        RemoveHandler ReportWorker.RunWorkerCompleted, AddressOf ReportWorkerCompleted
        ReportWorker.Dispose()
        ReportWorker = Nothing
      Catch ex As Exception
      End Try
    End If

  End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
    _FormOpenFailed = False

    If Not (MainForm Is Nothing) Then
      FormIsValid = True
    Else
      MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If


    ' Check User permissions
    Call CheckPermissions()
    If (HasReadPermission = False) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    ' Build Combos

    Try


      Call SetPortfolioCombo()
      Call SetPortfolioGrid()
      Call SetFundsCombo()

      Combo_ReportToRun.SelectedIndex = 0

      Me.Radio_AllPortfolios.Checked = True
      Me.Radio_AllPositions.Checked = True

      Me.Spin_TopNPositions.Value = 5
      Me.Spin_PositionsByUSDValue.Value = 5000000

      MainForm.SetComboSelectionLengths(Me)

    Catch ex As Exception
    End Try

  End Sub

    ''' <summary>
    ''' Handles the Closing event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
  Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    Dim HideForm As Boolean

    ' Hide or Close this form ?
    ' All depends on how many of this form type are Open or in Cache...


    If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
      HideForm = False
    Else
      HideForm = True
      If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.ReportForm_CacheCount Then
        HideForm = False
      End If
    End If

    If HideForm = True Then
      MainForm.HideInFormsCollection(Me)
      Me.Hide() ' NPP Fix

      e.Cancel = True
    Else
      Try
        MainForm.RemoveFromFormsCollection(Me)
        RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

        RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

        RemoveHandler Combo_PortfolioDescription.KeyUp, AddressOf MainForm.ComboSelectAsYouType

      Catch ex As Exception
      End Try
    End If

  End Sub


#End Region

  ' Routine to handle changes / updates to tables by this and other windows.
  ' If this, or any other, form posts a change to a table, then it will invoke an update event 
  ' detailing what tables have been altered.
  ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
  ' the 'VeniceAutoUpdate' event of the main Venice form.
  ' Each form may them react as appropriate to changes in any table that might impact it.
  '
    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    Dim KnowledgeDateChanged As Boolean
    Dim SetButtonStatus_Flag As Boolean

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

    KnowledgeDateChanged = False
    SetButtonStatus_Flag = False

    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
      KnowledgeDateChanged = True
    End If

    ' ****************************************************************
    ' Check for changes relevant to this form
    ' ****************************************************************

    ' Changes to the KnowledgeDate :-
    If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
      SetButtonStatus_Flag = True
    End If

    ' Changes to the tblPortfolioIndex table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblPortfolioIndex) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Call SetPortfolioCombo()
      Call SetFilingDateCombo()
      Call SetPortfolioGrid()
    End If

    ' Changes to the tblFund table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFund) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Call SetFundsCombo()
    End If

    ' Changes to the tblUserPermissions table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

      ' Check ongoing permissions.

      Call CheckPermissions()

      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

        FormIsValid = False
        Me.Close()
        Exit Sub
      End If

      SetButtonStatus_Flag = True

    End If

    ' ****************************************************************
    '
    ' ****************************************************************


  End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

  ' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
  Private Sub CheckPermissions()
    ' *****************************************************************************
    '
    ' *****************************************************************************

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

  End Sub


#End Region

#Region " Set Form Combos (Form Specific Code) "

    ''' <summary>
    ''' Sets the portfolio combo.
    ''' </summary>
  Private Sub SetPortfolioCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_PortfolioDescription, _
    RenaissanceStandardDatasets.tblPortfolioIndex, _
    "PortfolioDescription", _
    "PortfolioTicker", _
    "", True, True, False)  ' 

  End Sub

    ''' <summary>
    ''' Sets the funds combo.
    ''' </summary>
  Private Sub SetFundsCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_ReferenceFund, _
    RenaissanceStandardDatasets.tblFund, _
    "FundName", _
    "FundID", _
    "", False, True, True, 0)  ' 

  End Sub

    ''' <summary>
    ''' Sets the filing date combo.
    ''' </summary>
  Private Sub SetFilingDateCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_FilingDate, _
    RenaissanceStandardDatasets.tblPortfolioIndex, _
    "PortfolioFilingDate", _
    "PortfolioID", _
    "PortfolioTicker='" & Me.Combo_PortfolioDescription.SelectedValue.ToString & "'", False, False, False)         ' 

    If (Combo_FilingDate.Items.Count > 0) AndAlso (Combo_FilingDate.SelectedIndex < 0) Then
      Combo_FilingDate.SelectedIndex = 0
    End If
  End Sub

    ''' <summary>
    ''' Sets the portfolio grid.
    ''' </summary>
  Private Sub SetPortfolioGrid()
		Dim DSPortfolioIndex As RenaissanceDataClass.DSPortfolioIndex
		Dim rowPortfolioIndex As RenaissanceDataClass.DSPortfolioIndex.tblPortfolioIndexRow
		Dim SelectedRows As RenaissanceDataClass.DSPortfolioIndex.tblPortfolioIndexRow()

		Try
			Me.Grid_Portfolios.Rows.Count = 1

			DSPortfolioIndex = MainForm.Load_Table(RenaissanceStandardDatasets.tblPortfolioIndex, False)
			If (DSPortfolioIndex Is Nothing) Then Exit Sub

			SelectedRows = DSPortfolioIndex.tblPortfolioIndex.Select("true", "PortfolioFilingDate Desc, PortfolioDescription")
			For Each rowPortfolioIndex In SelectedRows
				Grid_Portfolios.Rows.Count += 1

				Grid_Portfolios.Item(Grid_Portfolios.Rows.Count - 1, "Select") = False
				Grid_Portfolios.Item(Grid_Portfolios.Rows.Count - 1, "Ticker") = rowPortfolioIndex.PortfolioTicker
				Grid_Portfolios.Item(Grid_Portfolios.Rows.Count - 1, "Name") = rowPortfolioIndex.PortfolioDescription
				Grid_Portfolios.Item(Grid_Portfolios.Rows.Count - 1, "FilingDate") = rowPortfolioIndex.PortfolioFilingDate
				Grid_Portfolios.Item(Grid_Portfolios.Rows.Count - 1, "PortfolioID") = rowPortfolioIndex.PortfolioID
			Next

		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in SetPortfolioGrid()", ex.StackTrace, True)
		End Try

	End Sub

#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

	' ******************************************************************
	' Convenient Enumerations relating to Portfolo Selection options.
	' ******************************************************************

    ''' <summary>
    ''' Enum enumPortfolioSelection
    ''' </summary>
	Private Enum enumPortfolioSelection As Integer
        ''' <summary>
        ''' All portfolios
        ''' </summary>
		AllPortfolios
        ''' <summary>
        ''' The single portfolio
        ''' </summary>
		SinglePortfolio
        ''' <summary>
        ''' The selected portfolio
        ''' </summary>
		SelectedPortfolio
	End Enum

    ''' <summary>
    ''' Enum enumPortfolioRows
    ''' </summary>
	Private Enum enumPortfolioRows As Integer
        ''' <summary>
        ''' All rows
        ''' </summary>
		AllRows
        ''' <summary>
        ''' The top N
        ''' </summary>
		TopN
        ''' <summary>
        ''' The by USD value
        ''' </summary>
		ByUSDValue
	End Enum

    ''' <summary>
    ''' Handles the Click event of the btnRunReport control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnRunReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRunReport.Click
		' *****************************************************************************
		' Run Report
		' *****************************************************************************

		Try
			FormControls = MainForm.MainReportHandler.DisableFormControls(Me)
			Form_ProgressBar.Value = Form_ProgressBar.Minimum
			MainForm.SetToolStripText(Label_Status, "Processing Report")

			If (ReportWorker Is Nothing) Then
				ReportWorker = New BackgroundWorkerThreadSafe(Nothing)
				AddHandler ReportWorker.DoWork, AddressOf ReportWorker_DoWork
				AddHandler ReportWorker.RunWorkerCompleted, AddressOf ReportWorkerCompleted
			End If

			ReportTimer.Interval = 25
			ReportTimer.Tag = Me.Form_ProgressBar
			ReportTimer.Start()


			If MainForm.UseReportWorkerThreads Then
				ReportWorker.RunWorkerAsync()
			Else
				RunReport()
				Call ReportWorkerCompleted(Nothing, Nothing)
			End If


		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
		End Try
	End Sub

    ''' <summary>
    ''' Handles the DoWork event of the ReportWorker control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.DoWorkEventArgs"/> instance containing the event data.</param>
	Private Sub ReportWorker_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs)	' Handles backgroundWorker1.DoWork
		RunReport()
	End Sub

    ''' <summary>
    ''' Reports the worker completed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.RunWorkerCompletedEventArgs"/> instance containing the event data.</param>
	Private Sub ReportWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs)	' Handles backgroundWorker1.RunWorkerCompleted
		Try
			ReportTimer.Stop()
			MainForm.MainReportHandler.EnableFormControls(FormControls)
			MainForm.SetToolStripText(Label_Status, "")

			'Form_ProgressBar = MainForm.ResetProgressBar(Form_StatusStrip, Form_ProgressBar)
			Form_ProgressBar.Value = Form_ProgressBar.Minimum
			Form_ProgressBar.Visible = False
			ReportTimer.Tag = Me.Form_ProgressBar
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
		End Try

	End Sub

    ''' <summary>
    ''' Runs the report.
    ''' </summary>
	Private Sub RunReport()
		' *****************************************************************************
		' Run Report
		' *****************************************************************************

		Dim PortfolioSelection As enumPortfolioSelection
		Dim SelectedPortfolios As ArrayList
		Dim FundID As Integer

		Dim PortfolioRows As enumPortfolioRows
		Dim PortfolioRowsThreshold As Long

		Dim tblPortfolioData As DataTable
		Dim LastPortfolioID As Integer
		Dim PortfolioCounter As Integer
		Dim NCounter As Integer
		Dim SortedRows As DataRow()
		Dim thisRow As DataRow
		Dim nextRow As DataRow

		Dim rptDataView As DataView

		' ******************************************************************
		' Reference Fund ?
		' ******************************************************************
		FundID = 0
		Try
			If (MainForm.GetComboSelectedIndex(Combo_ReferenceFund) >= 0) AndAlso (IsNumeric(MainForm.GetComboSelectedValue(Combo_ReferenceFund))) Then
				FundID = CInt(MainForm.GetComboSelectedValue(Combo_ReferenceFund))
			End If
		Catch ex As Exception
		End Try

		' ******************************************************************
		' What Portfolios to display ?
		' ******************************************************************

		' All
		PortfolioSelection = enumPortfolioSelection.AllPortfolios
		SelectedPortfolios = Nothing

		Try
			If MainForm.GetRadioChecked(Radio_SinglePortfolio) Then
				' Single
				If (MainForm.GetComboSelectedIndex(Combo_FilingDate) >= 0) And (IsNumeric(MainForm.GetComboSelectedValue(Combo_FilingDate))) Then
					PortfolioSelection = enumPortfolioSelection.SinglePortfolio
					SelectedPortfolios = New ArrayList
					SelectedPortfolios.Add(CInt(MainForm.GetComboSelectedValue(Combo_FilingDate)))
				End If
			ElseIf MainForm.GetRadioChecked(Radio_SelectedPortfolios) Then
				' Selected
				PortfolioSelection = enumPortfolioSelection.SelectedPortfolio
				SelectedPortfolios = New ArrayList

				For PortfolioCounter = 1 To (MainForm.GetGridRowCount(Grid_Portfolios) - 1)
					If CType(MainForm.GetGridItemByName(Grid_Portfolios, PortfolioCounter, "Select"), Boolean) = True Then
						SelectedPortfolios.Add(MainForm.GetGridItemByName(Grid_Portfolios, PortfolioCounter, "PortfolioID"))
					End If
				Next

				If (SelectedPortfolios.Count = 1) Then
					PortfolioSelection = enumPortfolioSelection.SinglePortfolio
				End If
			End If
		Catch ex As Exception
		End Try

		' ******************************************************************
		' What Rows to Display
		' ******************************************************************

		' All
		PortfolioRows = enumPortfolioRows.AllRows
		PortfolioRowsThreshold = 0

		Try
			If MainForm.GetRadioChecked(Radio_TopNPositions) Then
				' Top N
				PortfolioRows = enumPortfolioRows.TopN
				PortfolioRowsThreshold = CLng(MainForm.GetNumericUpDownValue(Spin_TopNPositions))
			ElseIf Me.Radio_PositionByUSD.Checked Then
				' by USD
				PortfolioRows = enumPortfolioRows.ByUSDValue
				PortfolioRowsThreshold = CLng(MainForm.GetNumericUpDownValue(Spin_PositionsByUSDValue))
			End If
		Catch ex As Exception
		End Try

		' ******************************************************************
		'
		' OK, Fetch the Data
		'
		' ******************************************************************

		MainForm.SetToolStripText(Me.Label_Status, "Fetching Report Data")
		If Not (MainForm.InvokeRequired) Then
			Application.DoEvents()
		End If

		tblPortfolioData = MainForm.MainReportHandler.GetData_rptViewPortfolio(SelectedPortfolios, FundID, DEFAULT_STATUSGROUPFILTER, DEFAULT_ADMINISTRATORDATESFILTER)

		If (tblPortfolioData Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Problem getting Portfolio Data. Report aborted.", "", True)
			Exit Sub
		End If

		' Now, If a single portfolio was specified, then the DataTable contains only that data, otherwise 
		' it Contains data for all portfolios.

		' ******************************************************************
		' Filter the data
		' ******************************************************************

		' Add Selected Column and 'Top' Count column.

		Try
			tblPortfolioData.Columns.Add(New DataColumn("rptSelectedPortfolio", GetType(Integer)))
			tblPortfolioData.Columns.Add(New DataColumn("rptNCount", GetType(Integer)))
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Adding columns to ReportTable", ex.StackTrace, True)
			Exit Sub
		End Try


		' Resolve useful column ordinals and initialise the new columns.

		Dim PortfolioIDOrdinal As Integer
		Dim NCountOrdinal As Integer
		Dim SelectedOrdinal As Integer
		Dim USDValueOrdinal As Integer
		Dim HoldingOrdinal As Integer
		Dim WeightOrdinal As Integer
		Dim TickerOrdinal As Integer
		Dim CounterOrdinal As Integer
		Dim FundWeightOrdinal As Integer

		Try
			PortfolioIDOrdinal = tblPortfolioData.Columns.IndexOf("PortfolioID")
			NCountOrdinal = tblPortfolioData.Columns.IndexOf("rptNCount")
			SelectedOrdinal = tblPortfolioData.Columns.IndexOf("rptSelectedPortfolio")
			USDValueOrdinal = tblPortfolioData.Columns.IndexOf("USDValue")
			HoldingOrdinal = tblPortfolioData.Columns.IndexOf("Holding")
			WeightOrdinal = tblPortfolioData.Columns.IndexOf("Weight")
			TickerOrdinal = tblPortfolioData.Columns.IndexOf("Ticker")
			CounterOrdinal = tblPortfolioData.Columns.IndexOf("Counter")
			FundWeightOrdinal = tblPortfolioData.Columns.IndexOf("FundWeight")

			' Initialise new Columns
			For Each thisRow In tblPortfolioData.Rows
				thisRow(SelectedOrdinal) = 0
				thisRow(NCountOrdinal) = 0
			Next
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error initialising Count and Select Columns. Roport Aborted.", ex.StackTrace, True)
			Exit Sub
		End Try



		' ******************************************************************
		' MarkSelectedPortfolios
		'
		' Mark rows in the returned dataset to indicate those rows belonging to the chosen
		' Portfolios. -The returned dataset will likely include non-selected portfolio data.-
		' 
		' ******************************************************************


		If (PortfolioSelection = enumPortfolioSelection.AllPortfolios) Then
			For Each thisRow In tblPortfolioData.Rows
				thisRow(SelectedOrdinal) = (-1)
			Next
		Else
			Dim IDCounter As Integer
			Dim thisPortfolioID As Integer

			If (Not (SelectedPortfolios Is Nothing)) Then
				For IDCounter = 0 To (SelectedPortfolios.Count - 1)
					Try
						thisPortfolioID = CInt(SelectedPortfolios(IDCounter))

						SortedRows = tblPortfolioData.Select("PortfolioID=" & thisPortfolioID.ToString)
						If (Not (SortedRows Is Nothing)) AndAlso (SortedRows.Length > 0) Then
							For PortfolioCounter = 0 To (SortedRows.Length - 1)
								thisRow = SortedRows(PortfolioCounter)
								thisRow(SelectedOrdinal) = (-1)
							Next
						End If

					Catch Inner_ex As Exception
					End Try
				Next
			End If

		End If


		' ******************************************************************
		' Aggregate Positions ?
		' Aggregate ? -If the Aggregation report is being run
		'
		' If the Aggregated position report is being run, then in order to be able
		' to show the aggregated positions by decending USD Value, the aggregation must
		' be done here rather than in the report.
		' 
		' The method of aggregation is simply to take the selected data, ordered by
		' Instrument (Ticker), and then roll up the Holding, Value etc by instrument.
		' The last instrument line is marked as selected, all other lines are marked 
		' as not-selected and thus are omitted from the final report DataView.
		' ******************************************************************

		Select Case MainForm.GetComboSelectedIndex(Combo_ReportToRun)
			Case 0 ' Positions by Fund, etc...

			Case 1 ' Aggregated report.
				Dim SumOfHolding As Double
				Dim SumOfWeight As Double
				Dim SumOfUSDValue As Double
				Dim SumOfReferenceFundWeight As Double
				Dim PositionCount As Integer

				SumOfHolding = 0
				SumOfWeight = 0
				SumOfUSDValue = 0
				SumOfReferenceFundWeight = 0
				PositionCount = 0

				' Gather Selected Data, sorted by Ticker.

				SortedRows = tblPortfolioData.Select("rptSelectedPortfolio<>0", "Ticker")

				' Cycle through the sorted data, rolling up the Holding, Weight and USDValue fields by
				' Instrument (Ticker).

				For PortfolioCounter = 0 To (SortedRows.Length - 1)
					thisRow = SortedRows(PortfolioCounter)
					If (PortfolioCounter < (SortedRows.Length - 1)) Then
						nextRow = SortedRows(PortfolioCounter + 1)
					Else
						nextRow = Nothing
					End If

					SumOfHolding += CDbl(thisRow(HoldingOrdinal))
					SumOfWeight += CDbl(thisRow(WeightOrdinal))
					SumOfUSDValue += CDbl(thisRow(USDValueOrdinal))
					SumOfReferenceFundWeight += CDbl(thisRow(FundWeightOrdinal))

					PositionCount += 1
					thisRow(SelectedOrdinal) = 0
					thisRow(PortfolioIDOrdinal) = 0

					If (nextRow Is Nothing) OrElse (thisRow(TickerOrdinal).ToString <> nextRow(TickerOrdinal).ToString) Then
						thisRow(HoldingOrdinal) = SumOfHolding
						thisRow(WeightOrdinal) = SumOfWeight
						thisRow(USDValueOrdinal) = SumOfUSDValue
						thisRow(FundWeightOrdinal) = SumOfReferenceFundWeight
						thisRow(CounterOrdinal) = PositionCount
						thisRow(SelectedOrdinal) = (-1)

						SumOfHolding = 0
						SumOfWeight = 0
						SumOfUSDValue = 0
						SumOfReferenceFundWeight = 0
						PositionCount = 0
					End If

				Next

		End Select


		' ******************************************************************
		' Calculate Position ranking.
		'
		' Note that I have chosen to apply the position filtering after the 
		' Aggregation process.
		' I think it is more sensible to do it this way.
		' ******************************************************************

		SortedRows = tblPortfolioData.Select("rptSelectedPortfolio<>0", "PortfolioID, USDValue Desc")
		NCounter = 1
		LastPortfolioID = (-1)

		For PortfolioCounter = 0 To (SortedRows.Length - 1)
			thisRow = SortedRows(PortfolioCounter)

			' TOP n Counter
			If CInt(thisRow(PortfolioIDOrdinal)) <> LastPortfolioID Then
				NCounter = 1
			End If
			thisRow(NCountOrdinal) = NCounter
			NCounter += 1
			LastPortfolioID = CInt(thisRow(PortfolioIDOrdinal))
		Next

		' ******************************************************************
		' Produce Select String for the Report DataView.
		' ******************************************************************

		Dim DVSelect As String
		DVSelect = "(rptSelectedPortfolio<>0)"

		If PortfolioRows = enumPortfolioRows.TopN Then
			If (DVSelect.Length > 0) Then
				DVSelect = DVSelect & " AND "
			End If

			DVSelect = DVSelect & "(rptNCount<=" & PortfolioRowsThreshold.ToString & ")"

		ElseIf PortfolioRows = enumPortfolioRows.ByUSDValue Then
			If (DVSelect.Length > 0) Then
				DVSelect = DVSelect & " AND "
			End If

			DVSelect = DVSelect & "(USDValue>=" & PortfolioRowsThreshold.ToString & ")"

		End If

		If (DVSelect.Length <= 0) Then
			DVSelect = "True"
		End If

		' ******************************************************************
		' Create Report DataView.
		' ******************************************************************

		rptDataView = New DataView(tblPortfolioData, DVSelect, "USDValue Desc", DataViewRowState.CurrentRows)


		' ******************************************************************
		' Display the report
		' ******************************************************************

		MainForm.SetToolStripText(Label_Status, "Rendering Report")
		If Not (MainForm.InvokeRequired) Then
			Application.DoEvents()
		End If

		Select Case MainForm.GetComboSelectedIndex(Combo_ReportToRun)
			Case 0
				Call MainForm.MainReportHandler.DisplayReport("rptPortfolioLargePositions", rptDataView, 0, Now.Date, MainForm.Main_Knowledgedate, Nothing)	' Form_ProgressBar)

			Case 1
				Call MainForm.MainReportHandler.DisplayReport("rptPortfolioAggregatedLargePositions", rptDataView, 0, Now.Date, MainForm.Main_Knowledgedate, Nothing)	' Form_ProgressBar)

		End Select


	End Sub


    ''' <summary>
    ''' Handles the Click event of the btnClose control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
		' *****************************************************************************
		' Close Form
		' *****************************************************************************

		Me.Close()

	End Sub

#End Region

#Region " Form Control Events"

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_PortfolioDescription control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_PortfolioDescription_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_PortfolioDescription.SelectedIndexChanged
		' ************************************************************************************
		'
		'
		'
		' ************************************************************************************

		If Combo_PortfolioDescription.SelectedIndex < 0 Then
			If Combo_FilingDate.Items.Count > 0 Then
				Combo_FilingDate.Items.Clear()
			End If

			Exit Sub
		End If

		Call Me.SetFilingDateCombo()


	End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the Radio_Portfolio control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Radio_Portfolio_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_AllPortfolios.CheckedChanged, Radio_SinglePortfolio.CheckedChanged, Radio_SelectedPortfolios.CheckedChanged
		' ************************************************************************************
		'
		'
		'
		' ************************************************************************************

		If Me.Radio_SinglePortfolio.Checked Then
			Me.Combo_PortfolioDescription.Enabled = True
			Me.Combo_FilingDate.Enabled = True
			Me.Grid_Portfolios.Visible = False
			Me.btn_GridAll.Visible = False
			Me.btn_GridNone.Visible = False
			Me.btn_GridLatest.Visible = False

		ElseIf Me.Radio_SelectedPortfolios.Checked Then
			Me.Combo_PortfolioDescription.Enabled = False
			Me.Combo_FilingDate.Enabled = False
			Me.Grid_Portfolios.Visible = True
			Me.btn_GridAll.Visible = True
			Me.btn_GridNone.Visible = True
			Me.btn_GridLatest.Visible = True
		Else
			If (Me.Radio_AllPortfolios.Checked = False) Then
				Me.Radio_AllPortfolios.Checked = True
			Else
				Me.Combo_PortfolioDescription.Enabled = False
				Me.Combo_FilingDate.Enabled = False
				Me.Grid_Portfolios.Visible = False
				Me.btn_GridAll.Visible = False
				Me.btn_GridNone.Visible = False
				Me.btn_GridLatest.Visible = False
			End If
		End If

	End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the Radio_AllPositions control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Radio_AllPositions_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_AllPositions.CheckedChanged, Radio_TopNPositions.CheckedChanged, Radio_PositionByUSD.CheckedChanged
		' ************************************************************************************
		'
		'
		'
		' ************************************************************************************

		If Me.Radio_TopNPositions.Checked Then
			Me.Spin_TopNPositions.Enabled = True
			Me.Spin_PositionsByUSDValue.Enabled = False
		ElseIf Me.Radio_PositionByUSD.Checked Then
			Me.Spin_TopNPositions.Enabled = False
			Me.Spin_PositionsByUSDValue.Enabled = True
		Else
			If Me.Radio_AllPositions.Checked = False Then
				Me.Radio_AllPositions.Checked = True
			Else
				Me.Spin_TopNPositions.Enabled = False
				Me.Spin_PositionsByUSDValue.Enabled = False
			End If
		End If

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btn_GridAll control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btn_GridAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_GridAll.Click
		' ************************************************************************************
		' Function to Select All Portfolios in the Portfolio Grid
		'
		'
		' ************************************************************************************

		Dim PortfolioCounter As Integer

		Try
			For PortfolioCounter = 1 To (Me.Grid_Portfolios.Rows.Count - 1)
				Grid_Portfolios.Item(PortfolioCounter, "Select") = True
			Next
		Catch ex As Exception
		End Try
	End Sub

    ''' <summary>
    ''' Handles the Click event of the btn_GridNone control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btn_GridNone_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_GridNone.Click
		' ************************************************************************************
		' Function to Un-Select All Portfolios in the Portfolio Grid
		'
		'
		' ************************************************************************************

		Dim PortfolioCounter As Integer

		Try
			For PortfolioCounter = 1 To (Me.Grid_Portfolios.Rows.Count - 1)
				Grid_Portfolios.Item(PortfolioCounter, "Select") = False
			Next
		Catch ex As Exception
		End Try
	End Sub

    ''' <summary>
    ''' Handles the Click event of the btn_GridLatest control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btn_GridLatest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_GridLatest.Click
		' ************************************************************************************
		' Function to Select the Lastest Filing Date version of each portfolio in the 
		' Portfolio Grid.
		'
		' ************************************************************************************
		Dim DSPortfolioIndex As RenaissanceDataClass.DSPortfolioIndex
		Dim rowPortfolioIndex As RenaissanceDataClass.DSPortfolioIndex.tblPortfolioIndexRow
		Dim SelectedRows As RenaissanceDataClass.DSPortfolioIndex.tblPortfolioIndexRow()

		Dim lastTicker As String
		Dim PortfolioCounter As Integer
		Dim GridCounter As Integer

		Try
			lastTicker = ""
			DSPortfolioIndex = MainForm.Load_Table(RenaissanceStandardDatasets.tblPortfolioIndex, False)
			If (DSPortfolioIndex Is Nothing) Then Exit Sub

			SelectedRows = DSPortfolioIndex.tblPortfolioIndex.Select("true", "PortfolioTicker, PortfolioFilingDate Desc")

			For PortfolioCounter = 0 To (SelectedRows.Length - 1)
				rowPortfolioIndex = SelectedRows(PortfolioCounter)

				If (PortfolioCounter = 0) OrElse (rowPortfolioIndex.PortfolioTicker <> lastTicker) Then
					For GridCounter = 1 To (Me.Grid_Portfolios.Rows.Count - 1)
						If CInt(Grid_Portfolios.Item(GridCounter, "PortfolioID")) = rowPortfolioIndex.PortfolioID Then
							Grid_Portfolios.Item(GridCounter, "Select") = True
							Exit For
						End If

					Next

					lastTicker = rowPortfolioIndex.PortfolioTicker
				End If
			Next

		Catch ex As Exception

		End Try


	End Sub

#End Region

End Class
