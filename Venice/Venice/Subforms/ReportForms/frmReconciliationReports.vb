﻿' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 01-23-2013
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmReconciliationReports.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************

Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals


''' <summary>
''' Class frmReconciliationReports
''' </summary>
Public Class frmReconciliationReports

	Inherits System.Windows.Forms.Form
	Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmReconciliationReports"/> class from being created.
    ''' </summary>
	Private Sub New()
		MyBase.New()

		'This call is required by the Windows Form Designer.
		InitializeComponent()

		'Add any initialization after the InitializeComponent() call

	End Sub

	'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
    ''' <summary>
    ''' The BTN run report
    ''' </summary>
	Friend WithEvents btnRunReport As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN close
    ''' </summary>
	Friend WithEvents btnClose As System.Windows.Forms.Button
    ''' <summary>
    ''' The date_ valuation date
    ''' </summary>
	Friend WithEvents Date_ValuationDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The combo_ report name
    ''' </summary>
	Friend WithEvents Combo_ReportName As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label4
    ''' </summary>
	Friend WithEvents Label4 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ start date
    ''' </summary>
	Friend WithEvents Label_StartDate As System.Windows.Forms.Label
    ''' <summary>
    ''' The form_ status strip
    ''' </summary>
	Friend WithEvents Form_StatusStrip As System.Windows.Forms.StatusStrip
    ''' <summary>
    ''' The form_ progress bar
    ''' </summary>
	Friend WithEvents Form_ProgressBar As System.Windows.Forms.ToolStripProgressBar
    ''' <summary>
    ''' The combo_ transaction group
    ''' </summary>
	Friend WithEvents Combo_TransactionGroup As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label8
    ''' </summary>
	Friend WithEvents Label8 As System.Windows.Forms.Label
    ''' <summary>
    ''' The list_ fund
    ''' </summary>
	Friend WithEvents List_Fund As System.Windows.Forms.ListBox
    ''' <summary>
    ''' The label3
    ''' </summary>
	Friend WithEvents Label3 As System.Windows.Forms.Label
    ''' <summary>
    ''' The group box1
    ''' </summary>
	Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The check_ S r_ ignore todays trades
    ''' </summary>
	Friend WithEvents Check_SR_IgnoreTodaysTrades As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ S r_ ignore status
    ''' </summary>
	Friend WithEvents Check_SR_IgnoreStatus As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The label_ status
    ''' </summary>
	Friend WithEvents Label_Status As System.Windows.Forms.ToolStripStatusLabel
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Me.btnRunReport = New System.Windows.Forms.Button
		Me.btnClose = New System.Windows.Forms.Button
		Me.Date_ValuationDate = New System.Windows.Forms.DateTimePicker
		Me.Label_StartDate = New System.Windows.Forms.Label
		Me.Combo_ReportName = New System.Windows.Forms.ComboBox
		Me.Label4 = New System.Windows.Forms.Label
		Me.Form_StatusStrip = New System.Windows.Forms.StatusStrip
		Me.Form_ProgressBar = New System.Windows.Forms.ToolStripProgressBar
		Me.Label_Status = New System.Windows.Forms.ToolStripStatusLabel
		Me.Combo_TransactionGroup = New System.Windows.Forms.ComboBox
		Me.Label8 = New System.Windows.Forms.Label
		Me.List_Fund = New System.Windows.Forms.ListBox
		Me.Label3 = New System.Windows.Forms.Label
		Me.GroupBox1 = New System.Windows.Forms.GroupBox
		Me.Check_SR_IgnoreTodaysTrades = New System.Windows.Forms.CheckBox
		Me.Check_SR_IgnoreStatus = New System.Windows.Forms.CheckBox
		Me.Form_StatusStrip.SuspendLayout()
		Me.GroupBox1.SuspendLayout()
		Me.SuspendLayout()
		'
		'btnRunReport
		'
		Me.btnRunReport.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnRunReport.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnRunReport.Location = New System.Drawing.Point(53, 293)
		Me.btnRunReport.Name = "btnRunReport"
		Me.btnRunReport.Size = New System.Drawing.Size(148, 28)
		Me.btnRunReport.TabIndex = 5
		Me.btnRunReport.Text = "Run Report"
		'
		'btnClose
		'
		Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnClose.Location = New System.Drawing.Point(269, 293)
		Me.btnClose.Name = "btnClose"
		Me.btnClose.Size = New System.Drawing.Size(75, 28)
		Me.btnClose.TabIndex = 6
		Me.btnClose.Text = "&Close"
		'
		'Date_ValuationDate
		'
		Me.Date_ValuationDate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Date_ValuationDate.Location = New System.Drawing.Point(140, 178)
		Me.Date_ValuationDate.Name = "Date_ValuationDate"
		Me.Date_ValuationDate.Size = New System.Drawing.Size(247, 20)
		Me.Date_ValuationDate.TabIndex = 2
		'
		'Label_StartDate
		'
		Me.Label_StartDate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.Label_StartDate.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label_StartDate.Location = New System.Drawing.Point(8, 182)
		Me.Label_StartDate.Name = "Label_StartDate"
		Me.Label_StartDate.Size = New System.Drawing.Size(110, 16)
		Me.Label_StartDate.TabIndex = 102
		Me.Label_StartDate.Text = "Valuation Date"
		'
		'Combo_ReportName
		'
		Me.Combo_ReportName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_ReportName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.Combo_ReportName.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_ReportName.Items.AddRange(New Object() {"Inventories Reconciliation Report"})
		Me.Combo_ReportName.Location = New System.Drawing.Point(140, 12)
		Me.Combo_ReportName.Name = "Combo_ReportName"
		Me.Combo_ReportName.Size = New System.Drawing.Size(247, 21)
		Me.Combo_ReportName.TabIndex = 0
		'
		'Label4
		'
		Me.Label4.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label4.Location = New System.Drawing.Point(8, 16)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(130, 20)
		Me.Label4.TabIndex = 108
		Me.Label4.Text = "Report Name"
		'
		'Form_StatusStrip
		'
		Me.Form_StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Form_ProgressBar, Me.Label_Status})
		Me.Form_StatusStrip.Location = New System.Drawing.Point(0, 336)
		Me.Form_StatusStrip.Name = "Form_StatusStrip"
		Me.Form_StatusStrip.Size = New System.Drawing.Size(397, 22)
		Me.Form_StatusStrip.TabIndex = 126
		Me.Form_StatusStrip.Text = " "
		'
		'Form_ProgressBar
		'
		Me.Form_ProgressBar.Maximum = 20
		Me.Form_ProgressBar.Name = "Form_ProgressBar"
		Me.Form_ProgressBar.Size = New System.Drawing.Size(150, 17)
		Me.Form_ProgressBar.Step = 1
		Me.Form_ProgressBar.Visible = False
		'
		'Label_Status
		'
		Me.Label_Status.Name = "Label_Status"
		Me.Label_Status.Size = New System.Drawing.Size(10, 17)
		Me.Label_Status.Text = " "
		'
		'Combo_TransactionGroup
		'
		Me.Combo_TransactionGroup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_TransactionGroup.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_TransactionGroup.Location = New System.Drawing.Point(140, 204)
		Me.Combo_TransactionGroup.Name = "Combo_TransactionGroup"
		Me.Combo_TransactionGroup.Size = New System.Drawing.Size(247, 21)
		Me.Combo_TransactionGroup.TabIndex = 3
		'
		'Label8
		'
		Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.Label8.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label8.Location = New System.Drawing.Point(8, 207)
		Me.Label8.Name = "Label8"
		Me.Label8.Size = New System.Drawing.Size(126, 16)
		Me.Label8.TabIndex = 137
		Me.Label8.Text = "Transaction Group"
		'
		'List_Fund
		'
		Me.List_Fund.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
								Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.List_Fund.FormattingEnabled = True
		Me.List_Fund.Location = New System.Drawing.Point(140, 48)
		Me.List_Fund.Name = "List_Fund"
		Me.List_Fund.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
		Me.List_Fund.Size = New System.Drawing.Size(247, 121)
		Me.List_Fund.TabIndex = 1
		'
		'Label3
		'
		Me.Label3.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label3.Location = New System.Drawing.Point(8, 48)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(120, 20)
		Me.Label3.TabIndex = 140
		Me.Label3.Text = "Fund Name"
		'
		'GroupBox1
		'
		Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.GroupBox1.Controls.Add(Me.Check_SR_IgnoreTodaysTrades)
		Me.GroupBox1.Controls.Add(Me.Check_SR_IgnoreStatus)
		Me.GroupBox1.Location = New System.Drawing.Point(6, 235)
		Me.GroupBox1.Name = "GroupBox1"
		Me.GroupBox1.Size = New System.Drawing.Size(387, 48)
		Me.GroupBox1.TabIndex = 4
		Me.GroupBox1.TabStop = False
		Me.GroupBox1.Text = "Subscriptions and Redemptions"
		'
		'Check_SR_IgnoreTodaysTrades
		'
		Me.Check_SR_IgnoreTodaysTrades.AutoSize = True
		Me.Check_SR_IgnoreTodaysTrades.Location = New System.Drawing.Point(183, 22)
		Me.Check_SR_IgnoreTodaysTrades.Name = "Check_SR_IgnoreTodaysTrades"
		Me.Check_SR_IgnoreTodaysTrades.Size = New System.Drawing.Size(130, 17)
		Me.Check_SR_IgnoreTodaysTrades.TabIndex = 1
		Me.Check_SR_IgnoreTodaysTrades.Text = "Ignore 'Todays' trades"
		Me.Check_SR_IgnoreTodaysTrades.UseVisualStyleBackColor = True
		'
		'Check_SR_IgnoreStatus
		'
		Me.Check_SR_IgnoreStatus.AutoSize = True
		Me.Check_SR_IgnoreStatus.Location = New System.Drawing.Point(25, 22)
		Me.Check_SR_IgnoreStatus.Name = "Check_SR_IgnoreStatus"
		Me.Check_SR_IgnoreStatus.Size = New System.Drawing.Size(120, 17)
		Me.Check_SR_IgnoreStatus.TabIndex = 0
		Me.Check_SR_IgnoreStatus.Text = "Ignore Trade Status"
		Me.Check_SR_IgnoreStatus.UseVisualStyleBackColor = True
		'
		'frmReconciliationReports
		'
		Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
		Me.ClientSize = New System.Drawing.Size(397, 358)
		Me.Controls.Add(Me.GroupBox1)
		Me.Controls.Add(Me.List_Fund)
		Me.Controls.Add(Me.Label3)
		Me.Controls.Add(Me.Combo_TransactionGroup)
		Me.Controls.Add(Me.Label8)
		Me.Controls.Add(Me.Form_StatusStrip)
		Me.Controls.Add(Me.Combo_ReportName)
		Me.Controls.Add(Me.Label4)
		Me.Controls.Add(Me.Date_ValuationDate)
		Me.Controls.Add(Me.Label_StartDate)
		Me.Controls.Add(Me.btnClose)
		Me.Controls.Add(Me.btnRunReport)
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Name = "frmReconciliationReports"
		Me.Text = "Reconciliation Reports"
		Me.Form_StatusStrip.ResumeLayout(False)
		Me.Form_StatusStrip.PerformLayout()
		Me.GroupBox1.ResumeLayout(False)
		Me.GroupBox1.PerformLayout()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

#End Region

#Region " Form Locals and Constants "


	' Form 'Parent', the Main Venice form.
	' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
	Private WithEvents _MainForm As VeniceMain

	' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
	Private FormTooltip As New ToolTip()

    ''' <summary>
    ''' The report worker
    ''' </summary>
	Private ReportWorker As BackgroundWorkerThreadSafe = Nothing
    ''' <summary>
    ''' The form controls
    ''' </summary>
	Private FormControls As ArrayList = Nothing
    ''' <summary>
    ''' The report timer
    ''' </summary>
	Private WithEvents ReportTimer As New Windows.Forms.Timer()

	' Form Constants, specific to the table being updated.

    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
	Private ALWAYS_CLOSE_THIS_FORM As Boolean = True

	' The standard ChangeID for this form. e.g. tblPerson
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
	Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

	' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
	Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
	Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

	' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
	Private THIS_FORM_FormID As VeniceFormID

	' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
	Private FormIsValid As Boolean
    ''' <summary>
    ''' The form changed
    ''' </summary>
	Private FormChanged As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
	Private _FormOpenFailed As Boolean

	' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
	Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
	Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
	Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
	Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
	Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
	Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return False
		End Get
		Set(ByVal Value As Boolean)
		End Set
	End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
	Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
		Get
			Return False
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
		Get
			Return True
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmReconciliationReports"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
	Public Sub New(ByVal pMainForm As VeniceMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()

		_MainForm = pMainForm
		AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate
		AddHandler ReportTimer.Tick, AddressOf MainForm.IncrementStatusBarTimerEvent

		_FormOpenFailed = False

		' ******************************************************
		' Form Specific Settings :
		' ******************************************************

		' Form Permissioning :-

		THIS_FORM_PermissionArea = Me.Name
		THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

		' 'This' form ID

		THIS_FORM_FormID = VeniceFormID.frmReconciliationReports

		' Format Event Handlers for form controls

		AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

		AddHandler Combo_ReportName.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_TransactionGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

		' Initialise List_GroupList
		Dim FundsDS As RenaissanceDataClass.DSFund = MainForm.Load_Table(RenaissanceStandardDatasets.tblFund, False)

    Me.List_Fund.DataSource = New DataView(FundsDS.tblFund, "FundClosed=0", "FundName", DataViewRowState.CurrentRows)
		List_Fund.DisplayMember = "FundName"
		List_Fund.ValueMember = "FundID"

		' ******************************************************
		' End Form Specific.
		' ******************************************************

		THIS_FORM_ChangeID = RenaissanceChangeID.KnowledgeDate

	End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

	' Form Initialisation code.
	'
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
	Public Sub ResetForm() Implements StandardVeniceForm.ResetForm

		Call Form_Load(Me, New System.EventArgs)
	End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
	Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub

    ''' <summary>
    ''' Handles the FormClosing event of the frmReconciliationReports control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.FormClosingEventArgs"/> instance containing the event data.</param>
	Private Sub frmReconciliationReports_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
		If (ReportWorker IsNot Nothing) Then
			If ReportWorker.IsBusy Then
				e.Cancel = True
				Exit Sub
			End If

			Try
				RemoveHandler ReportTimer.Tick, AddressOf MainForm.IncrementStatusBarTimerEvent
				RemoveHandler ReportWorker.DoWork, AddressOf ReportWorker_DoWork
				RemoveHandler ReportWorker.RunWorkerCompleted, AddressOf ReportWorkerCompleted
				ReportWorker.Dispose()
				ReportWorker = Nothing
			Catch ex As Exception
			End Try
		End If

	End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
		_FormOpenFailed = False

		If Not (MainForm Is Nothing) Then
			FormIsValid = True
		Else
			MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If


		' Check User permissions
		Call CheckPermissions()
		If (HasReadPermission = False) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		' Build Combos

		Try

			Call SetTransactionGroupCombo()

			MainForm.SetComboSelectionLengths(Me)

			Me.Combo_ReportName.SelectedIndex = 0

			' Initial Valuation Date 

			Me.Date_ValuationDate.Value = RenaissanceUtilities.DatePeriodFunctions.AddPeriodToDate(DealingPeriod.Daily, Now.Date, -1)

			Check_SR_IgnoreStatus.Checked = False
			Check_SR_IgnoreTodaysTrades.Checked = True

			If (Combo_TransactionGroup.Items.Count > 0) Then
				Me.Combo_TransactionGroup.SelectedValue = DEFAULT_STATUSGROUPFILTER_EOD
			End If

		Catch ex As Exception
		End Try

	End Sub

    ''' <summary>
    ''' Handles the Closing event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
	Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		Dim HideForm As Boolean

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...


		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else
			HideForm = True
			If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.ReportForm_CacheCount Then
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			e.Cancel = True
		Else
			Try
				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

				RemoveHandler Combo_ReportName.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_TransactionGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType

			Catch ex As Exception
			End Try
		End If

	End Sub


#End Region

	' Routine to handle changes / updates to tables by this and other windows.
	' If this, or any other, form posts a change to a table, then it will invoke an update event 
	' detailing what tables have been altered.
	' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
	' the 'VeniceAutoUpdate' event of the main Venice form.
	' Each form may them react as appropriate to changes in any table that might impact it.
	'
    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		Dim KnowledgeDateChanged As Boolean
		Dim SetButtonStatus_Flag As Boolean

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		KnowledgeDateChanged = False
		SetButtonStatus_Flag = False

		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
			KnowledgeDateChanged = True
		End If

		' ****************************************************************
		' Check for changes relevant to this form
		' ****************************************************************

		' Changes to the KnowledgeDate :-
		If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
			SetButtonStatus_Flag = True
		End If

		' Changes to the tblFund table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFund) = True) Or KnowledgeDateChanged Then

			Dim FundsDS As RenaissanceDataClass.DSFund = MainForm.Load_Table(RenaissanceStandardDatasets.tblFund, False)

      Me.List_Fund.DataSource = New DataView(FundsDS.tblFund, "FundClosed=0", "FundName", DataViewRowState.CurrentRows)
			List_Fund.DisplayMember = "FundName"
			List_Fund.ValueMember = "FundID"

		End If

		' Changes to the tblSophisStatusGroups table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblSophisStatusGroups) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			Call SetTransactionGroupCombo()
		End If

		' Changes to the tblUserPermissions table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

			' Check ongoing permissions.

			Call CheckPermissions()
			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

				FormIsValid = False
				Me.Close()
				Exit Sub
			End If

			SetButtonStatus_Flag = True

		End If

		' ****************************************************************
		'
		' ****************************************************************


	End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

	' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
	Private Sub CheckPermissions()
		' *****************************************************************************
		'
		' *****************************************************************************

		Dim Permissions As Integer

		Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

		HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
		HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
		HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
		HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

	End Sub


#End Region

#Region " Set Form Combos (Form Specific Code) "

    ''' <summary>
    ''' Sets the transaction group combo.
    ''' </summary>
	Private Sub SetTransactionGroupCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_TransactionGroup, _
		RenaissanceStandardDatasets.tblSophisStatusGroups, _
		"GROUP_NAME", _
		"GROUP_NAME", _
		"", True, True, True)		' 

	End Sub

#End Region

#Region " SetButton / Control Events (Form Specific Code) "




#End Region


#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

    ''' <summary>
    ''' Handles the Click event of the btnRunReport control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnRunReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRunReport.Click
		' *****************************************************************************
		' Run Report
		' *****************************************************************************

		Try
			FormControls = MainForm.MainReportHandler.DisableFormControls(Me)
			Form_ProgressBar.Value = Form_ProgressBar.Minimum
			MainForm.SetToolStripText(Label_Status, "Processing Report")

			If (ReportWorker Is Nothing) Then
				ReportWorker = New BackgroundWorkerThreadSafe(Nothing)
				AddHandler ReportWorker.DoWork, AddressOf ReportWorker_DoWork
				AddHandler ReportWorker.RunWorkerCompleted, AddressOf ReportWorkerCompleted
			End If

			ReportTimer.Interval = 25
			ReportTimer.Tag = Me.Form_ProgressBar
			ReportTimer.Start()

			If MainForm.UseReportWorkerThreads Then
				ReportWorker.RunWorkerAsync()
			Else
				RunReport()
				Call ReportWorkerCompleted(Nothing, Nothing)
			End If

		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
		End Try
	End Sub

    ''' <summary>
    ''' Handles the DoWork event of the ReportWorker control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.DoWorkEventArgs"/> instance containing the event data.</param>
	Private Sub ReportWorker_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs)	' Handles backgroundWorker1.DoWork
		RunReport()
	End Sub

    ''' <summary>
    ''' Reports the worker completed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.RunWorkerCompletedEventArgs"/> instance containing the event data.</param>
	Private Sub ReportWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs)	' Handles backgroundWorker1.RunWorkerCompleted
		Try
			ReportTimer.Stop()
			MainForm.MainReportHandler.EnableFormControls(FormControls)
			MainForm.SetToolStripText(Label_Status, "")

			'Form_ProgressBar = MainForm.ResetProgressBar(Form_StatusStrip, Form_ProgressBar)
			Form_ProgressBar.Value = Form_ProgressBar.Minimum
			Form_ProgressBar.Visible = False
			ReportTimer.Tag = Me.Form_ProgressBar
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
		End Try

	End Sub

    ''' <summary>
    ''' Runs the report.
    ''' </summary>
	Private Sub RunReport()
		' *****************************************************************************
		' Run Report
		' *****************************************************************************

		Dim ValuationDate As Date
		Dim StatusGroupFilter As String
		Dim AdministratorDatesFilter As RenaissanceGlobals.AdministratorDatesFilter = RenaissanceGlobals.AdministratorDatesFilter.None
		Dim SelectedFundListItems() As Object
		Dim SelectedRow As DataRowView
		Dim thisFundID As Integer
		Dim thisFundName As String
		' Validate

		' Fund ID

		Try

			SelectedFundListItems = MainForm.GetListBoxSelectedItems(Me.List_Fund)

			If (SelectedFundListItems Is Nothing) OrElse (SelectedFundListItems.Length <= 0) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Fund(s) must be selected.", "", True)
				Exit Sub
			End If

			ValuationDate = MainForm.GetDatetimeValue(Me.Date_ValuationDate).Date

			StatusGroupFilter = Nz(MainForm.GetComboSelectedValue(Combo_TransactionGroup), "")

			' AdministratorDatesFilter (bitmap)
			'
			'--	1		- Ignore the transaction status for Subscriptions and Redemptions
			'--	2		- Ignore the Subscriptions and Redemptions on (or after of course) the given Value Date

			If (MainForm.GetCheckBoxChecked(Me.Check_SR_IgnoreStatus)) Then
        AdministratorDatesFilter += RenaissanceGlobals.AdministratorDatesFilter.SubscriptionRedemption_IgnoreTransactionStatus
			End If
			If (MainForm.GetCheckBoxChecked(Me.Check_SR_IgnoreTodaysTrades)) Then
				AdministratorDatesFilter += RenaissanceGlobals.AdministratorDatesFilter.SubscriptionRedemption_IgnoreTodaysTrades
			End If

		Catch ex As Exception

			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error preparing for Reconciliation report.", ex.StackTrace, True)
			Exit Sub
		End Try

		' Run Report.

		Try

			' 
			For Each SelectedRow In SelectedFundListItems

				thisFundID = CType(SelectedRow.Row, RenaissanceDataClass.DSFund.tblFundRow).FundID
				thisFundName = CType(SelectedRow.Row, RenaissanceDataClass.DSFund.tblFundRow).FundName

				Me.MainForm.SetToolStripText(Label_Status, "Processing Report : " & thisFundName)
				If Not (MainForm.InvokeRequired) Then
					Application.DoEvents()
				End If

				Call MainForm.MainReportHandler.rptReconciliationReports(MainForm.GetComboSelectedIndex(Combo_ReportName), thisFundID, thisFundName, ValuationDate, StatusGroupFilter, AdministratorDatesFilter, Nothing)	 ' Form_ProgressBar)

			Next


		Catch ex As Exception
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error processing Cash report.", ex.StackTrace, True)

		End Try

	End Sub


    ''' <summary>
    ''' Handles the Click event of the btnClose control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
		' *****************************************************************************
		' Close Form
		' *****************************************************************************

		Me.Close()

	End Sub

#End Region

#Region " Form Control Events"


#End Region


End Class
