' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmAttributionReports.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals

Imports RenaissanceUtilities.DatePeriodFunctions

''' <summary>
''' Class frmAttributionsReports
''' </summary>
Public Class frmAttributionsReports

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmAttributionsReports"/> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The BTN run report
    ''' </summary>
  Friend WithEvents btnRunReport As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN close
    ''' </summary>
  Friend WithEvents btnClose As System.Windows.Forms.Button
    ''' <summary>
    ''' The label2
    ''' </summary>
  Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ attribution year
    ''' </summary>
  Friend WithEvents Combo_AttributionYear As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label1
    ''' </summary>
  Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The radio_ milestone only
    ''' </summary>
  Friend WithEvents Radio_MilestoneOnly As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The radio_ prefer milestone
    ''' </summary>
  Friend WithEvents Radio_PreferMilestone As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The radio_ non milestone
    ''' </summary>
  Friend WithEvents Radio_NonMilestone As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The radio_ prefer non milestone
    ''' </summary>
  Friend WithEvents Radio_PreferNonMilestone As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The check_ lookthrough attributions
    ''' </summary>
  Friend WithEvents Check_LookthroughAttributions As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ aggregate to parent
    ''' </summary>
  Friend WithEvents Check_AggregateToParent As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ recalculate this month
    ''' </summary>
  Friend WithEvents Check_RecalculateThisMonth As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ recalculate last month
    ''' </summary>
  Friend WithEvents Check_RecalculateLastMonth As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ save unit prices
    ''' </summary>
  Friend WithEvents Check_SaveUnitPrices As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The combo_ fund
    ''' </summary>
  Friend WithEvents Combo_Fund As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ report name
    ''' </summary>
  Friend WithEvents Combo_ReportName As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The form_ status strip
    ''' </summary>
  Friend WithEvents Form_StatusStrip As System.Windows.Forms.StatusStrip
    ''' <summary>
    ''' The form_ progress bar
    ''' </summary>
  Friend WithEvents Form_ProgressBar As System.Windows.Forms.ToolStripProgressBar
    ''' <summary>
    ''' The label_ status
    ''' </summary>
  Friend WithEvents Label_Status As System.Windows.Forms.ToolStripStatusLabel
    ''' <summary>
    ''' The group_ selection
    ''' </summary>
  Friend WithEvents Group_Selection As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The group_ re calculation
    ''' </summary>
  Friend WithEvents Group_ReCalculation As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The group_ report grouping
    ''' </summary>
  Friend WithEvents Group_ReportGrouping As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The combo_ report grouping option
    ''' </summary>
  Friend WithEvents Combo_ReportGroupingOption As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label3
    ''' </summary>
  Friend WithEvents Label3 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ selected report group
    ''' </summary>
  Friend WithEvents Combo_SelectedReportGroup As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label5
    ''' </summary>
	Friend WithEvents Label5 As System.Windows.Forms.Label
    ''' <summary>
    ''' The date_ date to
    ''' </summary>
	Friend WithEvents Date_DateTo As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The label6
    ''' </summary>
	Friend WithEvents Label6 As System.Windows.Forms.Label
    ''' <summary>
    ''' The check_ dont show others
    ''' </summary>
	Friend WithEvents Check_DontShowOthers As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The combo_ attribution month
    ''' </summary>
	Friend WithEvents Combo_AttributionMonth As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label7
    ''' </summary>
  Friend WithEvents Label7 As System.Windows.Forms.Label
    ''' <summary>
    ''' The check_ show budget
    ''' </summary>
  Friend WithEvents Check_ShowBudget As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The label4
    ''' </summary>
	Friend WithEvents Label4 As System.Windows.Forms.Label
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.btnRunReport = New System.Windows.Forms.Button
    Me.btnClose = New System.Windows.Forms.Button
    Me.Label2 = New System.Windows.Forms.Label
    Me.Combo_AttributionYear = New System.Windows.Forms.ComboBox
    Me.Label1 = New System.Windows.Forms.Label
    Me.Radio_MilestoneOnly = New System.Windows.Forms.RadioButton
    Me.Radio_PreferMilestone = New System.Windows.Forms.RadioButton
    Me.Radio_NonMilestone = New System.Windows.Forms.RadioButton
    Me.Radio_PreferNonMilestone = New System.Windows.Forms.RadioButton
    Me.Check_LookthroughAttributions = New System.Windows.Forms.CheckBox
    Me.Check_AggregateToParent = New System.Windows.Forms.CheckBox
    Me.Check_RecalculateThisMonth = New System.Windows.Forms.CheckBox
    Me.Check_RecalculateLastMonth = New System.Windows.Forms.CheckBox
    Me.Check_SaveUnitPrices = New System.Windows.Forms.CheckBox
    Me.Combo_Fund = New System.Windows.Forms.ComboBox
    Me.Combo_ReportName = New System.Windows.Forms.ComboBox
    Me.Label4 = New System.Windows.Forms.Label
    Me.Form_StatusStrip = New System.Windows.Forms.StatusStrip
    Me.Form_ProgressBar = New System.Windows.Forms.ToolStripProgressBar
    Me.Label_Status = New System.Windows.Forms.ToolStripStatusLabel
    Me.Group_Selection = New System.Windows.Forms.GroupBox
    Me.Group_ReCalculation = New System.Windows.Forms.GroupBox
    Me.Group_ReportGrouping = New System.Windows.Forms.GroupBox
    Me.Check_DontShowOthers = New System.Windows.Forms.CheckBox
    Me.Combo_SelectedReportGroup = New System.Windows.Forms.ComboBox
    Me.Label5 = New System.Windows.Forms.Label
    Me.Combo_ReportGroupingOption = New System.Windows.Forms.ComboBox
    Me.Label3 = New System.Windows.Forms.Label
    Me.Date_DateTo = New System.Windows.Forms.DateTimePicker
    Me.Label6 = New System.Windows.Forms.Label
    Me.Combo_AttributionMonth = New System.Windows.Forms.ComboBox
    Me.Label7 = New System.Windows.Forms.Label
    Me.Check_ShowBudget = New System.Windows.Forms.CheckBox
    Me.Form_StatusStrip.SuspendLayout()
    Me.Group_Selection.SuspendLayout()
    Me.Group_ReCalculation.SuspendLayout()
    Me.Group_ReportGrouping.SuspendLayout()
    Me.SuspendLayout()
    '
    'btnRunReport
    '
    Me.btnRunReport.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnRunReport.Location = New System.Drawing.Point(49, 499)
    Me.btnRunReport.Name = "btnRunReport"
    Me.btnRunReport.Size = New System.Drawing.Size(148, 28)
    Me.btnRunReport.TabIndex = 8
    Me.btnRunReport.Text = "Run Report"
    '
    'btnClose
    '
    Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnClose.Location = New System.Drawing.Point(265, 499)
    Me.btnClose.Name = "btnClose"
    Me.btnClose.Size = New System.Drawing.Size(75, 28)
    Me.btnClose.TabIndex = 9
    Me.btnClose.Text = "&Close"
    '
    'Label2
    '
    Me.Label2.Location = New System.Drawing.Point(9, 42)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(120, 17)
    Me.Label2.TabIndex = 53
    Me.Label2.Text = "Fund Name"
    '
    'Combo_AttributionYear
    '
    Me.Combo_AttributionYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_AttributionYear.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_AttributionYear.Location = New System.Drawing.Point(133, 66)
    Me.Combo_AttributionYear.Name = "Combo_AttributionYear"
    Me.Combo_AttributionYear.Size = New System.Drawing.Size(252, 21)
    Me.Combo_AttributionYear.TabIndex = 2
    '
    'Label1
    '
    Me.Label1.Location = New System.Drawing.Point(9, 70)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(120, 17)
    Me.Label1.TabIndex = 55
    Me.Label1.Text = "Attribution Year"
    '
    'Radio_MilestoneOnly
    '
    Me.Radio_MilestoneOnly.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Radio_MilestoneOnly.Location = New System.Drawing.Point(6, 17)
    Me.Radio_MilestoneOnly.Name = "Radio_MilestoneOnly"
    Me.Radio_MilestoneOnly.Size = New System.Drawing.Size(168, 20)
    Me.Radio_MilestoneOnly.TabIndex = 0
    Me.Radio_MilestoneOnly.Text = "Only Milestone Data"
    '
    'Radio_PreferMilestone
    '
    Me.Radio_PreferMilestone.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Radio_PreferMilestone.Location = New System.Drawing.Point(6, 38)
    Me.Radio_PreferMilestone.Name = "Radio_PreferMilestone"
    Me.Radio_PreferMilestone.Size = New System.Drawing.Size(168, 24)
    Me.Radio_PreferMilestone.TabIndex = 1
    Me.Radio_PreferMilestone.Text = "Prefer Milestone Data"
    '
    'Radio_NonMilestone
    '
    Me.Radio_NonMilestone.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Radio_NonMilestone.Location = New System.Drawing.Point(198, 17)
    Me.Radio_NonMilestone.Name = "Radio_NonMilestone"
    Me.Radio_NonMilestone.Size = New System.Drawing.Size(168, 20)
    Me.Radio_NonMilestone.TabIndex = 2
    Me.Radio_NonMilestone.Text = "Only Non-Milestone Data"
    '
    'Radio_PreferNonMilestone
    '
    Me.Radio_PreferNonMilestone.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Radio_PreferNonMilestone.Location = New System.Drawing.Point(198, 38)
    Me.Radio_PreferNonMilestone.Name = "Radio_PreferNonMilestone"
    Me.Radio_PreferNonMilestone.Size = New System.Drawing.Size(168, 24)
    Me.Radio_PreferNonMilestone.TabIndex = 3
    Me.Radio_PreferNonMilestone.Text = "Prefer Non-Milestone Data"
    '
    'Check_LookthroughAttributions
    '
    Me.Check_LookthroughAttributions.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_LookthroughAttributions.Location = New System.Drawing.Point(6, 62)
    Me.Check_LookthroughAttributions.Name = "Check_LookthroughAttributions"
    Me.Check_LookthroughAttributions.Size = New System.Drawing.Size(285, 20)
    Me.Check_LookthroughAttributions.TabIndex = 4
    Me.Check_LookthroughAttributions.Text = "Report on Lookthrough Attributions"
    '
    'Check_AggregateToParent
    '
    Me.Check_AggregateToParent.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_AggregateToParent.Location = New System.Drawing.Point(6, 84)
    Me.Check_AggregateToParent.Name = "Check_AggregateToParent"
    Me.Check_AggregateToParent.Size = New System.Drawing.Size(285, 20)
    Me.Check_AggregateToParent.TabIndex = 5
    Me.Check_AggregateToParent.Text = "Aggregate positions to Parent Instrument"
    '
    'Check_RecalculateThisMonth
    '
    Me.Check_RecalculateThisMonth.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_RecalculateThisMonth.Location = New System.Drawing.Point(6, 16)
    Me.Check_RecalculateThisMonth.Name = "Check_RecalculateThisMonth"
    Me.Check_RecalculateThisMonth.Size = New System.Drawing.Size(352, 20)
    Me.Check_RecalculateThisMonth.TabIndex = 0
    Me.Check_RecalculateThisMonth.Text = "Recalculate This Months Attributions"
    '
    'Check_RecalculateLastMonth
    '
    Me.Check_RecalculateLastMonth.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_RecalculateLastMonth.Location = New System.Drawing.Point(6, 40)
    Me.Check_RecalculateLastMonth.Name = "Check_RecalculateLastMonth"
    Me.Check_RecalculateLastMonth.Size = New System.Drawing.Size(352, 20)
    Me.Check_RecalculateLastMonth.TabIndex = 1
    Me.Check_RecalculateLastMonth.Text = "Recalculate Last Months Attributions"
    '
    'Check_SaveUnitPrices
    '
    Me.Check_SaveUnitPrices.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_SaveUnitPrices.Location = New System.Drawing.Point(6, 64)
    Me.Check_SaveUnitPrices.Name = "Check_SaveUnitPrices"
    Me.Check_SaveUnitPrices.Size = New System.Drawing.Size(352, 20)
    Me.Check_SaveUnitPrices.TabIndex = 2
    Me.Check_SaveUnitPrices.Text = "Save Unit prices before Attribution calculation"
    '
    'Combo_Fund
    '
    Me.Combo_Fund.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Fund.Location = New System.Drawing.Point(133, 38)
    Me.Combo_Fund.Name = "Combo_Fund"
    Me.Combo_Fund.Size = New System.Drawing.Size(252, 21)
    Me.Combo_Fund.TabIndex = 1
    '
    'Combo_ReportName
    '
    Me.Combo_ReportName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_ReportName.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_ReportName.Items.AddRange(New Object() {"Attribution Report", "Attribution Sector Bips", "Attribution Sector Returns", "Attribution Sector Weights", "Attribution (Money)", "Attribution Best / Worst", "P&L Detail Report", "Fund Performance Report (Event Board)"})
    Me.Combo_ReportName.Location = New System.Drawing.Point(133, 10)
    Me.Combo_ReportName.Name = "Combo_ReportName"
    Me.Combo_ReportName.Size = New System.Drawing.Size(252, 21)
    Me.Combo_ReportName.TabIndex = 0
    '
    'Label4
    '
    Me.Label4.Location = New System.Drawing.Point(9, 14)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(120, 17)
    Me.Label4.TabIndex = 68
    Me.Label4.Text = "Report Name"
    '
    'Form_StatusStrip
    '
    Me.Form_StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Form_ProgressBar, Me.Label_Status})
    Me.Form_StatusStrip.Location = New System.Drawing.Point(0, 541)
    Me.Form_StatusStrip.Name = "Form_StatusStrip"
    Me.Form_StatusStrip.Size = New System.Drawing.Size(396, 22)
    Me.Form_StatusStrip.TabIndex = 69
    Me.Form_StatusStrip.Text = " "
    '
    'Form_ProgressBar
    '
    Me.Form_ProgressBar.Maximum = 20
    Me.Form_ProgressBar.Name = "Form_ProgressBar"
    Me.Form_ProgressBar.Size = New System.Drawing.Size(150, 17)
    Me.Form_ProgressBar.Step = 1
    Me.Form_ProgressBar.Visible = False
    '
    'Label_Status
    '
    Me.Label_Status.Name = "Label_Status"
    Me.Label_Status.Size = New System.Drawing.Size(10, 17)
    Me.Label_Status.Text = " "
    '
    'Group_Selection
    '
    Me.Group_Selection.Controls.Add(Me.Check_ShowBudget)
    Me.Group_Selection.Controls.Add(Me.Radio_PreferNonMilestone)
    Me.Group_Selection.Controls.Add(Me.Radio_MilestoneOnly)
    Me.Group_Selection.Controls.Add(Me.Radio_PreferMilestone)
    Me.Group_Selection.Controls.Add(Me.Radio_NonMilestone)
    Me.Group_Selection.Controls.Add(Me.Check_AggregateToParent)
    Me.Group_Selection.Controls.Add(Me.Check_LookthroughAttributions)
    Me.Group_Selection.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Group_Selection.Location = New System.Drawing.Point(12, 154)
    Me.Group_Selection.Name = "Group_Selection"
    Me.Group_Selection.Size = New System.Drawing.Size(373, 133)
    Me.Group_Selection.TabIndex = 5
    Me.Group_Selection.TabStop = False
    Me.Group_Selection.Text = "Attribution Selection"
    '
    'Group_ReCalculation
    '
    Me.Group_ReCalculation.Controls.Add(Me.Check_SaveUnitPrices)
    Me.Group_ReCalculation.Controls.Add(Me.Check_RecalculateThisMonth)
    Me.Group_ReCalculation.Controls.Add(Me.Check_RecalculateLastMonth)
    Me.Group_ReCalculation.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Group_ReCalculation.Location = New System.Drawing.Point(12, 293)
    Me.Group_ReCalculation.Name = "Group_ReCalculation"
    Me.Group_ReCalculation.Size = New System.Drawing.Size(373, 90)
    Me.Group_ReCalculation.TabIndex = 6
    Me.Group_ReCalculation.TabStop = False
    Me.Group_ReCalculation.Text = "Attribution ReCalculation"
    '
    'Group_ReportGrouping
    '
    Me.Group_ReportGrouping.Controls.Add(Me.Check_DontShowOthers)
    Me.Group_ReportGrouping.Controls.Add(Me.Combo_SelectedReportGroup)
    Me.Group_ReportGrouping.Controls.Add(Me.Label5)
    Me.Group_ReportGrouping.Controls.Add(Me.Combo_ReportGroupingOption)
    Me.Group_ReportGrouping.Controls.Add(Me.Label3)
    Me.Group_ReportGrouping.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Group_ReportGrouping.Location = New System.Drawing.Point(12, 389)
    Me.Group_ReportGrouping.Name = "Group_ReportGrouping"
    Me.Group_ReportGrouping.Size = New System.Drawing.Size(373, 104)
    Me.Group_ReportGrouping.TabIndex = 7
    Me.Group_ReportGrouping.TabStop = False
    Me.Group_ReportGrouping.Text = "Report Grouping"
    '
    'Check_DontShowOthers
    '
    Me.Check_DontShowOthers.Enabled = False
    Me.Check_DontShowOthers.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_DontShowOthers.Location = New System.Drawing.Point(121, 73)
    Me.Check_DontShowOthers.Name = "Check_DontShowOthers"
    Me.Check_DontShowOthers.Size = New System.Drawing.Size(245, 20)
    Me.Check_DontShowOthers.TabIndex = 2
    Me.Check_DontShowOthers.Text = "Don't show 'Others'"
    '
    'Combo_SelectedReportGroup
    '
    Me.Combo_SelectedReportGroup.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectedReportGroup.Location = New System.Drawing.Point(121, 46)
    Me.Combo_SelectedReportGroup.Name = "Combo_SelectedReportGroup"
    Me.Combo_SelectedReportGroup.Size = New System.Drawing.Size(245, 21)
    Me.Combo_SelectedReportGroup.TabIndex = 1
    '
    'Label5
    '
    Me.Label5.Location = New System.Drawing.Point(5, 50)
    Me.Label5.Name = "Label5"
    Me.Label5.Size = New System.Drawing.Size(100, 17)
    Me.Label5.TabIndex = 76
    Me.Label5.Text = "Selected Group"
    '
    'Combo_ReportGroupingOption
    '
    Me.Combo_ReportGroupingOption.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_ReportGroupingOption.Items.AddRange(New Object() {"No additional Grouping", "Group by Fund Type Attribution Grouping", "Group by Selected fund Type Group", "Group by Instrument Type", "Group by Selected Instrument Type"})
    Me.Combo_ReportGroupingOption.Location = New System.Drawing.Point(121, 19)
    Me.Combo_ReportGroupingOption.Name = "Combo_ReportGroupingOption"
    Me.Combo_ReportGroupingOption.Size = New System.Drawing.Size(245, 21)
    Me.Combo_ReportGroupingOption.TabIndex = 0
    '
    'Label3
    '
    Me.Label3.Location = New System.Drawing.Point(5, 23)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(100, 17)
    Me.Label3.TabIndex = 74
    Me.Label3.Text = "Report Grouping"
    '
    'Date_DateTo
    '
    Me.Date_DateTo.Location = New System.Drawing.Point(133, 122)
    Me.Date_DateTo.Name = "Date_DateTo"
    Me.Date_DateTo.Size = New System.Drawing.Size(252, 20)
    Me.Date_DateTo.TabIndex = 4
    '
    'Label6
    '
    Me.Label6.Location = New System.Drawing.Point(9, 126)
    Me.Label6.Name = "Label6"
    Me.Label6.Size = New System.Drawing.Size(120, 17)
    Me.Label6.TabIndex = 132
    Me.Label6.Text = "Latest Attribution Date"
    '
    'Combo_AttributionMonth
    '
    Me.Combo_AttributionMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_AttributionMonth.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_AttributionMonth.Location = New System.Drawing.Point(132, 94)
    Me.Combo_AttributionMonth.Name = "Combo_AttributionMonth"
    Me.Combo_AttributionMonth.Size = New System.Drawing.Size(252, 21)
    Me.Combo_AttributionMonth.TabIndex = 3
    '
    'Label7
    '
    Me.Label7.Location = New System.Drawing.Point(8, 98)
    Me.Label7.Name = "Label7"
    Me.Label7.Size = New System.Drawing.Size(120, 17)
    Me.Label7.TabIndex = 134
    Me.Label7.Text = "Attribution Month"
    '
    'Check_ShowBudget
    '
    Me.Check_ShowBudget.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_ShowBudget.Location = New System.Drawing.Point(6, 106)
    Me.Check_ShowBudget.Name = "Check_ShowBudget"
    Me.Check_ShowBudget.Size = New System.Drawing.Size(285, 20)
    Me.Check_ShowBudget.TabIndex = 6
    Me.Check_ShowBudget.Text = "Display 'Budget' Column"
    '
    'frmAttributionsReports
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(396, 563)
    Me.Controls.Add(Me.Combo_AttributionMonth)
    Me.Controls.Add(Me.Label7)
    Me.Controls.Add(Me.Label6)
    Me.Controls.Add(Me.Date_DateTo)
    Me.Controls.Add(Me.Group_ReportGrouping)
    Me.Controls.Add(Me.Group_ReCalculation)
    Me.Controls.Add(Me.Group_Selection)
    Me.Controls.Add(Me.Form_StatusStrip)
    Me.Controls.Add(Me.Combo_ReportName)
    Me.Controls.Add(Me.Label4)
    Me.Controls.Add(Me.Combo_Fund)
    Me.Controls.Add(Me.Combo_AttributionYear)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.btnClose)
    Me.Controls.Add(Me.btnRunReport)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frmAttributionsReports"
    Me.Text = "Attribution Reports"
    Me.Form_StatusStrip.ResumeLayout(False)
    Me.Form_StatusStrip.PerformLayout()
    Me.Group_Selection.ResumeLayout(False)
    Me.Group_ReCalculation.ResumeLayout(False)
    Me.Group_ReportGrouping.ResumeLayout(False)
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "


	' Form 'Parent', the Main Venice form.
	' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
	Private WithEvents _MainForm As VeniceMain

	' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
	Private FormTooltip As New ToolTip()

    ''' <summary>
    ''' The report worker
    ''' </summary>
	Private ReportWorker As BackgroundWorkerThreadSafe = Nothing
    ''' <summary>
    ''' The form controls
    ''' </summary>
	Private FormControls As ArrayList = Nothing
    ''' <summary>
    ''' The report timer
    ''' </summary>
	Private WithEvents ReportTimer As New Windows.Forms.Timer()

	' Form Constants, specific to the table being updated.

    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
	Private ALWAYS_CLOSE_THIS_FORM As Boolean = True

	' The standard ChangeID for this form. e.g. tblPerson
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
	Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

	' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
	Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
	Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

	' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
	Private THIS_FORM_FormID As VeniceFormID

	' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
	Private FormIsValid As Boolean
    ''' <summary>
    ''' The form changed
    ''' </summary>
	Private FormChanged As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
	Private _FormOpenFailed As Boolean


	' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
	Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
	Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
	Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
	Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
	Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
	Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return False
		End Get
		Set(ByVal Value As Boolean)
		End Set
	End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
	Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
		Get
			Return False
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
		Get
			Return True
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

    ''' <summary>
    ''' Gets or sets the selected report.
    ''' </summary>
    ''' <value>The selected report.</value>
	Public Property SelectedReport() As Integer
		Get
			Try
				Return Combo_ReportName.SelectedIndex
			Catch ex As Exception
			End Try
		End Get
		Set(ByVal value As Integer)
			Try
				If (Combo_ReportName.Items.Count > value) AndAlso (Combo_ReportName.Items.Count > 0) Then
					Combo_ReportName.SelectedIndex = value
				End If
			Catch ex As Exception
			End Try
		End Set
	End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmAttributionsReports"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
	Public Sub New(ByVal pMainForm As VeniceMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()

		_MainForm = pMainForm
		AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate
		AddHandler ReportTimer.Tick, AddressOf MainForm.IncrementStatusBarTimerEvent

		_FormOpenFailed = False

		' ******************************************************
		' Form Specific Settings :
		' ******************************************************

		' Form Permissioning :-

		THIS_FORM_PermissionArea = Me.Name
		THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

		' 'This' form ID

		THIS_FORM_FormID = VeniceFormID.frmAttributionsReports

		' Format Event Handlers for form controls

		AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

		AddHandler Combo_Fund.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

		' ******************************************************
		' End Form Specific.
		' ******************************************************

		THIS_FORM_ChangeID = RenaissanceChangeID.KnowledgeDate

	End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

	' Form Initialisation code.
	'
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
	Public Sub ResetForm() Implements StandardVeniceForm.ResetForm

		Call Form_Load(Me, New System.EventArgs)
	End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
	Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub

    ''' <summary>
    ''' Handles the FormClosing event of the frmAttributionsReports control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.FormClosingEventArgs"/> instance containing the event data.</param>
	Private Sub frmAttributionsReports_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
		If (ReportWorker IsNot Nothing) Then
			If ReportWorker.IsBusy Then
				e.Cancel = True
				Exit Sub
			End If

			Try
				RemoveHandler ReportTimer.Tick, AddressOf MainForm.IncrementStatusBarTimerEvent
				RemoveHandler ReportWorker.DoWork, AddressOf ReportWorker_DoWork
				RemoveHandler ReportWorker.RunWorkerCompleted, AddressOf ReportWorkerCompleted
				ReportWorker.Dispose()
				ReportWorker = Nothing
			Catch ex As Exception
			End Try
		End If

	End Sub


    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		_FormOpenFailed = False

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)

		If Not (MainForm Is Nothing) Then
			FormIsValid = True
		Else
			MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
			_FormOpenFailed = True
			Exit Sub
		End If


		' Check User permissions
		Call CheckPermissions()
		If (HasReadPermission = False) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

			FormIsValid = False
			_FormOpenFailed = True
		End If

		' Build Combos

		Me.Combo_ReportName.SelectedIndex = 0

		Call Me.SetFundCombo()
		Call Me.SetAttributionYearCombo(Now.Year)

		Me.Date_DateTo.Value = New Date(Now.Year + 1, 1, 1).AddDays(-1)
		Check_DontShowOthers.Checked = False

		Me.Radio_PreferMilestone.Checked = True
		Me.Check_SaveUnitPrices.Enabled = False
    Me.Check_ShowBudget.Checked = True

		MainForm.SetComboSelectionLengths(Me)

	End Sub

    ''' <summary>
    ''' Handles the Closing event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
	Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		Dim HideForm As Boolean

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...


		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else
			HideForm = True
			If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.ReportForm_CacheCount Then
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			e.Cancel = True
		Else
			Try
				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

				RemoveHandler Combo_Fund.KeyUp, AddressOf MainForm.ComboSelectAsYouType

			Catch ex As Exception
			End Try
		End If

	End Sub


#End Region

    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		' Routine to handle changes / updates to tables by this and other windows.
		' If this, or any other, form posts a change to a table, then it will invoke an update event 
		' detailing what tables have been altered.
		' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
		' the 'VeniceAutoUpdate' event of the main Venice form.
		' Each form may them react as appropriate to changes in any table that might impact it.
		'
		Dim KnowledgeDateChanged As Boolean
		Dim SetButtonStatus_Flag As Boolean

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		KnowledgeDateChanged = False
		SetButtonStatus_Flag = False

		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
			KnowledgeDateChanged = True
		End If

		' ****************************************************************
		' Check for changes relevant to this form
		' ****************************************************************

		' Changes to the KnowledgeDate :-
		If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
			SetButtonStatus_Flag = True
		End If

		' Changes to the tblFund table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFund) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			Call SetFundCombo()
		End If

		' Changes to the tblAttribution table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblAttribution) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			Call SetAttributionYearCombo(Now.Year)
		End If

		' Changes to the tblUserPermissions table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

			' Check ongoing permissions.

			Call CheckPermissions()
			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

				FormIsValid = False
				Me.Close()
				Exit Sub
			End If

			SetButtonStatus_Flag = True

		End If

		' ****************************************************************
		'
		' ****************************************************************


	End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

	' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
	Private Sub CheckPermissions()
		' *****************************************************************************
		'
		' *****************************************************************************

		Dim Permissions As Integer

		Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

		HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
		HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
		HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
		HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

	End Sub


#End Region

#Region " Set Form Combos (Form Specific Code) "

    ''' <summary>
    ''' Sets the fund combo.
    ''' </summary>
	Private Sub SetFundCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_Fund, _
    RenaissanceStandardDatasets.tblFund, _
    "FundName", _
    "FundID", _
    "FundClosed=0", False, True, True)     ' 

	End Sub

    ''' <summary>
    ''' Sets the attribution year combo.
    ''' </summary>
    ''' <param name="pInitialvalue">The p initialvalue.</param>
	Sub SetAttributionYearCombo(Optional ByVal pInitialvalue As Integer = 0)
		' ********************************************************************************
		' Set the Attribution Year Combo to reflect those years for which Attribution
		' records exist for the selected fund.
		'
		' ********************************************************************************

		Dim thisRow As DataRow

		Dim InitialValue As Integer
		Dim Counter As Integer

		InitialValue = pInitialvalue
		Try
			If Me.Combo_AttributionYear.SelectedIndex >= 0 Then
				InitialValue = CInt(Me.Combo_AttributionYear.SelectedValue)
			End If
			Me.Combo_AttributionYear.Items.Clear()
		Catch ex As Exception
			InitialValue = 0
		End Try

		Try
			If Me.Combo_Fund.SelectedIndex <= 0 Then ' Note Leading Blank.
				Exit Sub
			End If
			If Not IsNumeric(Combo_Fund.SelectedValue) Then
				Exit Sub
			End If
		Catch ex As Exception
			Exit Sub
		End Try

		Dim thisCommand As SqlCommand = Nothing
		Dim tmpTable As New DataTable

		Try
			Dim Query As String
			Query = "SELECT DATEPART(yyyy, Attribdate) FROM fn_tblAttribution_SelectKD(Null) WHERE (AttribFund = " & Combo_Fund.SelectedValue.ToString & ") GROUP BY DATEPART(yyyy, Attribdate) ORDER BY DATEPART(yyyy, Attribdate) DESC"
			thisCommand = New SqlCommand(Query, MainForm.GetVeniceConnection())

			If (thisCommand IsNot Nothing) AndAlso (thisCommand.Connection IsNot Nothing) Then
				SyncLock thisCommand.Connection
          MainForm.LoadTable_Custom(tmpTable, thisCommand)
          'tmpTable.Load(thisCommand.ExecuteReader)
        End SyncLock
      End If
    Catch ex As Exception
    Finally
      Try
        If (thisCommand IsNot Nothing) AndAlso (thisCommand.Connection IsNot Nothing) Then
          thisCommand.Connection.Close()
          thisCommand.Connection = Nothing
        End If
      Catch ex As Exception
      End Try
    End Try
    thisCommand = Nothing

    Try
      If tmpTable.Rows.Count > 0 Then
        For Each thisRow In tmpTable.Rows
          Me.Combo_AttributionYear.Items.Add(thisRow.Item(0).ToString)
        Next

        tmpTable.Rows.Clear()
      End If
    Catch ex As Exception
    End Try

    ' Initial value
    Try
      If (InitialValue > 0) Then
        For Counter = 0 To (Me.Combo_AttributionYear.Items.Count - 1)
          If CInt(Me.Combo_AttributionYear.Items(Counter)) = InitialValue Then
            Me.Combo_AttributionYear.SelectedIndex = Counter
            Exit Sub
          End If
        Next
      End If
      If (Me.Combo_AttributionYear.Items.Count > 0) AndAlso (Me.Combo_AttributionYear.SelectedIndex < 0) Then
        Me.Combo_AttributionYear.SelectedIndex = 0
      End If
    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Sets the attribution month combo.
  ''' </summary>
  Sub SetAttributionMonthCombo()
    ' ********************************************************************************
    ' Set the Attribution Month Combo to reflect those Dates for which Attribution
    ' records exist for the selected fund.
    '
    ' ********************************************************************************

    Dim thisRow As DataRow

    Try
      If Me.Combo_Fund.SelectedIndex <= 0 Then ' Note Leading Blank.
        Exit Sub
      End If
      If Not IsNumeric(Combo_Fund.SelectedValue) Then
        Exit Sub
      End If
    Catch ex As Exception
      Exit Sub
    End Try

    Dim thisCommand As SqlCommand = Nothing
    Dim tmpTable As New DataTable

    Try
      Dim Query As String
      Query = "SELECT Attribdate FROM fn_tblAttribution_SelectKD(Null) WHERE (AttribFund = " & Combo_Fund.SelectedValue.ToString & ") GROUP BY Attribdate ORDER BY Attribdate DESC"
      thisCommand = New SqlCommand(Query, MainForm.GetVeniceConnection())

      If (thisCommand IsNot Nothing) AndAlso (thisCommand.Connection IsNot Nothing) Then
        SyncLock thisCommand.Connection
          MainForm.LoadTable_Custom(tmpTable, thisCommand)
          'tmpTable.Load(thisCommand.ExecuteReader)
        End SyncLock
      End If
    Catch ex As Exception
    Finally
      Try
        If (thisCommand IsNot Nothing) AndAlso (thisCommand.Connection IsNot Nothing) Then
          thisCommand.Connection.Close()
          thisCommand.Connection = Nothing
        End If
      Catch ex As Exception
      End Try
    End Try
    thisCommand = Nothing

    Try
      If tmpTable.Rows.Count > 0 Then
        For Each thisRow In tmpTable.Rows
          Me.Combo_AttributionMonth.Items.Add(CDate(thisRow.Item(0)).ToString(QUERY_SHORTDATEFORMAT))
        Next

        tmpTable.Rows.Clear()
      End If
    Catch ex As Exception
    End Try

  End Sub

#End Region

#Region " SetButton / Control Events (Form Specific Code) "

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_ReportName control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_ReportName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_ReportName.SelectedIndexChanged
		' *****************************************************************
		'
		'
		' *****************************************************************

		Try

			If Me.Combo_ReportName.SelectedIndex >= 0 Then
				Me.Combo_AttributionMonth.Enabled = False
				Me.Combo_AttributionYear.Enabled = True
				Me.Date_DateTo.Enabled = True

				Select Case Me.Combo_ReportName.Text
					Case "Attribution Report"
						Me.Group_ReportGrouping.Enabled = True
						Me.Check_AggregateToParent.Enabled = True

					Case "Attribution Sector Bips"
						Me.Group_ReportGrouping.Enabled = True
						Me.Check_AggregateToParent.Enabled = False
						Me.Check_AggregateToParent.Checked = False

					Case "Attribution Sector Returns"
						Me.Group_ReportGrouping.Enabled = True
						'If Me.Combo_ReportGroupingOption.Items.Count > 0 Then
						'  Combo_ReportGroupingOption.SelectedIndex = 0
						'End If
						'If Me.Combo_SelectedReportGroup.Items.Count > 0 Then
						'  Combo_SelectedReportGroup.SelectedIndex = 0
						'End If

						Me.Check_AggregateToParent.Enabled = False
						Me.Check_AggregateToParent.Checked = False

					Case "Attribution Sector Weights"
						Me.Group_ReportGrouping.Enabled = True

						Me.Check_AggregateToParent.Enabled = False
						Me.Check_AggregateToParent.Checked = False

					Case "Attribution (Money)"
						Me.Group_ReportGrouping.Enabled = True
						Me.Check_AggregateToParent.Enabled = True

					Case "Attribution Best / Worst"
						Me.Group_ReportGrouping.Enabled = False
						If Me.Combo_ReportGroupingOption.Items.Count > 0 Then
							Combo_ReportGroupingOption.SelectedIndex = 0
						End If
						If Me.Combo_SelectedReportGroup.Items.Count > 0 Then
							Combo_SelectedReportGroup.SelectedIndex = 0
						End If

						Me.Check_AggregateToParent.Enabled = True

					Case "P&L Detail Report"
						Me.Combo_AttributionMonth.Enabled = True
						Me.Combo_AttributionYear.Enabled = False
						Me.Group_ReportGrouping.Enabled = False
						If Me.Combo_ReportGroupingOption.Items.Count > 0 Then
							Combo_ReportGroupingOption.SelectedIndex = 0
						End If
						If Me.Combo_SelectedReportGroup.Items.Count > 0 Then
							Combo_SelectedReportGroup.SelectedIndex = 0
						End If
						Me.Check_AggregateToParent.Enabled = True
						Me.Date_DateTo.Enabled = False

				End Select
			End If

		Catch ex As Exception
		End Try

	End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_ReportGroupingOption control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_ReportGroupingOption_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_ReportGroupingOption.SelectedIndexChanged
		' *****************************************************************
		'
		'
		' *****************************************************************

		Try
			Try
				Combo_SelectedReportGroup.DataSource = Nothing
				Me.Combo_SelectedReportGroup.Items.Clear()
			Catch ex As Exception
			End Try

			Select Case Me.Combo_ReportGroupingOption.SelectedIndex

				Case 0
					'No additional Grouping

					Combo_SelectedReportGroup.Enabled = False
					Check_DontShowOthers.Enabled = False
					Check_DontShowOthers.Checked = False

				Case 1
					'Group by Fund Type Attribution Grouping

					Combo_SelectedReportGroup.Enabled = False
					Check_DontShowOthers.Enabled = False
					Check_DontShowOthers.Checked = False

				Case 2
					'Group by Selected fund Type Group

					Combo_SelectedReportGroup.Enabled = True
					Check_DontShowOthers.Enabled = True
					Check_DontShowOthers.Checked = False

					Call MainForm.SetTblGenericCombo( _
						Me.Combo_SelectedReportGroup, _
						RenaissanceStandardDatasets.tblFundType, _
						"FundTypeAttributionGrouping", _
						"FundTypeAttributionGrouping", _
						"", True, True, False)		 ' 

				Case 3
					'Group by Instrument Type

					Combo_SelectedReportGroup.Enabled = False
					Check_DontShowOthers.Enabled = False
					Check_DontShowOthers.Checked = False

				Case 4
					'Group by Selected Instrument Type

					Combo_SelectedReportGroup.Enabled = True
					Check_DontShowOthers.Enabled = True
					Check_DontShowOthers.Checked = False

					Call MainForm.SetTblGenericCombo( _
						Me.Combo_SelectedReportGroup, _
						RenaissanceStandardDatasets.tblInstrumentType, _
						"InstrumentTypeDescription", _
						"InstrumentTypeID", _
						"")			' 

				Case Else
					Combo_SelectedReportGroup.Enabled = False
					Check_DontShowOthers.Enabled = False
					Check_DontShowOthers.Checked = False

			End Select
		Catch ex As Exception
		End Try

	End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_Fund control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_Fund_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_Fund.SelectedIndexChanged
		' *****************************************************************
		'
		'
		' *****************************************************************

		Try

			Call SetAttributionYearCombo(Now.Year)
			Call SetAttributionMonthCombo()

			If (Combo_AttributionYear.SelectedIndex < 0) AndAlso (Combo_AttributionYear.Items.Count > 0) Then
				Combo_AttributionYear.SelectedIndex = 0
			End If

		Catch ex As Exception
		End Try
	End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the Check_RecalculateThisMonth control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Check_RecalculateThisMonth_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_RecalculateThisMonth.CheckedChanged, Check_RecalculateLastMonth.CheckedChanged
		' *****************************************************************
		'
		'
		' *****************************************************************

		If (Me.Check_RecalculateThisMonth.Checked = False) And (Me.Check_RecalculateLastMonth.Checked = False) Then
			Me.Check_SaveUnitPrices.Checked = False
			Me.Check_SaveUnitPrices.Enabled = False
		Else
			Me.Check_SaveUnitPrices.Enabled = True
		End If
	End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_AttributionYear control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_AttributionYear_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_AttributionYear.SelectedIndexChanged
		' *****************************************************************
		'
		'
		' *****************************************************************

		If (Combo_AttributionYear.SelectedIndex >= 0) Then
			Me.Date_DateTo.Value = New Date(CInt(Me.Combo_AttributionYear.Items(Combo_AttributionYear.SelectedIndex)) + 1, 1, 1).AddDays(-1)
		End If
	End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_AttributionMonth control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_AttributionMonth_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Combo_AttributionMonth.SelectedIndexChanged
		' *****************************************************************
		'
		'
		' *****************************************************************

		If (Combo_AttributionMonth.SelectedIndex >= 0) Then
			If IsDate(Combo_AttributionMonth.SelectedItem) Then
				Combo_AttributionYear.SelectedItem = CDate(Combo_AttributionMonth.SelectedItem).Year.ToString
			End If
		End If

	End Sub

#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

    ''' <summary>
    ''' Handles the Click event of the btnRunReport control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnRunReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRunReport.Click
		' *****************************************************************************
		' Run Report
		' *****************************************************************************

		Try
			FormControls = MainForm.MainReportHandler.DisableFormControls(Me)
			Form_ProgressBar.Value = Form_ProgressBar.Minimum
			MainForm.SetToolStripText(Label_Status, "Processing Report")

			If (ReportWorker Is Nothing) Then
				ReportWorker = New BackgroundWorkerThreadSafe(Nothing)
				AddHandler ReportWorker.DoWork, AddressOf ReportWorker_DoWork
				AddHandler ReportWorker.RunWorkerCompleted, AddressOf ReportWorkerCompleted
			End If

			ReportTimer.Interval = 25
			ReportTimer.Tag = Me.Form_ProgressBar
			ReportTimer.Start()

			If MainForm.UseReportWorkerThreads Then
				ReportWorker.RunWorkerAsync()
			Else
				RunReport()
				Call ReportWorkerCompleted(Nothing, Nothing)
			End If


		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
		End Try
	End Sub

    ''' <summary>
    ''' Handles the DoWork event of the ReportWorker control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.DoWorkEventArgs"/> instance containing the event data.</param>
	Private Sub ReportWorker_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs)	' Handles backgroundWorker1.DoWork
		RunReport()
	End Sub

    ''' <summary>
    ''' Reports the worker completed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.RunWorkerCompletedEventArgs"/> instance containing the event data.</param>
	Private Sub ReportWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs)	' Handles backgroundWorker1.RunWorkerCompleted
		Try
			ReportTimer.Stop()
			MainForm.MainReportHandler.EnableFormControls(FormControls)
			MainForm.SetToolStripText(Label_Status, "")

			'Form_ProgressBar = MainForm.ResetProgressBar(Form_StatusStrip, Form_ProgressBar)
			Form_ProgressBar.Value = Form_ProgressBar.Minimum
			Form_ProgressBar.Visible = False
			ReportTimer.Tag = Me.Form_ProgressBar
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
		End Try

	End Sub

    ''' <summary>
    ''' Runs the report.
    ''' </summary>
	Private Sub RunReport()
		' *****************************************************************************
		' Run Report
		' *****************************************************************************

		Dim FundID As Integer
		Dim LegalEntity As String
		Dim AttributionYear As Integer
		Dim AttributionMonth As Date
		Dim MaxAttributionDate As Date
		Dim RecordPreference As Attribution_RecordPreference
		Dim AttributionDate As Date
		Dim ReportName As String
		Dim DontShowOthers As Boolean = False
    Dim ShowBudget As Boolean = True

		Dim AttribPeriod As RenaissanceGlobals.DealingPeriod = DealingPeriod.Monthly

		' Validate

		If (MainForm.GetComboSelectedValue(Combo_Fund) Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Fund must be selected.", "", True)
			Exit Sub
		End If

		If (CInt(MainForm.GetComboSelectedIndex(Combo_Fund)) <= 0) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Fund must be selected.", "", True)
			Exit Sub
		End If
		FundID = CInt(MainForm.GetComboSelectedValue(Combo_Fund))

		LegalEntity = ""

		If IsNumeric(MainForm.GetControlText(Combo_AttributionYear)) = False Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Attribution Year must be selected.", "", True)
			Exit Sub
		End If
		AttributionYear = CInt(MainForm.GetControlText(Combo_AttributionYear))

		AttributionMonth = Renaissance_BaseDate
		If IsDate(MainForm.GetControlText(Combo_AttributionMonth)) Then
			AttributionMonth = CDate(MainForm.GetControlText(Combo_AttributionMonth))
		End If

		' Get Max Attribution Date

		MaxAttributionDate = Renaissance_BaseDate
		If IsDate(MainForm.GetDatetimeValue(Date_DateTo)) Then
			MaxAttributionDate = MainForm.GetDatetimeValue(Date_DateTo)
		End If


    ' Budget Column. 

    ShowBudget = MainForm.GetCheckBoxChecked(Check_ShowBudget)

		' Don't Show Others ?

		DontShowOthers = MainForm.GetCheckBoxChecked(Check_DontShowOthers)

		Try

			RecordPreference = Attribution_RecordPreference.PreferMilestone
			If MainForm.GetRadioChecked(Me.Radio_MilestoneOnly) Then
				RecordPreference = Attribution_RecordPreference.OnlyMilestone
			ElseIf MainForm.GetRadioChecked(Me.Radio_NonMilestone) Then
				RecordPreference = Attribution_RecordPreference.OnlyNonMilestone
			ElseIf MainForm.GetRadioChecked(Me.Radio_PreferNonMilestone) Then
				RecordPreference = Attribution_RecordPreference.PreferNonMilestone
			End If

			If MainForm.GetCheckBoxChecked(Me.Check_RecalculateLastMonth) Then
				' Set Attribution Date to be the last day of last month 

				'AttributionDate = Now.Date
				'AttributionDate = AttributionDate.AddMonths(1)
				'AttributionDate = AttributionDate.AddDays(0 - AttributionDate.Day)

				AttributionDate = FitDateToPeriod(AttribPeriod, Now.Date)
				AttributionDate = AddPeriodToDate(AttribPeriod, AttributionDate, -1)

				' Save Unit Prices
				If MainForm.GetCheckBoxChecked(Me.Check_SaveUnitPrices) = True Then
					MainForm.SetToolStripText(Label_Status, "Saving last Month's Unit Prices.")
					If Not (MainForm.InvokeRequired) Then
            'Application.DoEvents()
					End If

					Call Attribution_SaveFundPrices_Asynch(MainForm, FundID, AttributionDate, DEFAULT_STATUSGROUPFILTER, DEFAULT_ADMINISTRATORDATESFILTER, Nothing)	' Form_ProgressBar)
					Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblPrice), True)
				End If

				MainForm.SetToolStripText(Label_Status, "Saving last Month's Attributions.")
				If Not (MainForm.InvokeRequired) Then
          'Application.DoEvents()
				End If

				' Run Attributions
				' Calculate Attributions

				Call Attribution_SaveAtributions_Asynch(MainForm, _
				FundID, _
				AttributionDate, _
				AttribPeriod, _
				False, _
				Me.Check_LookthroughAttributions.Checked, _
				True, _
				True, _
				MainForm.Main_Knowledgedate, MainForm.Main_Knowledgedate, _
				MainForm.Main_Knowledgedate, Nothing)	' Form_ProgressBar)

				Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblAttribution), True)

			End If

			If MainForm.GetCheckBoxChecked(Me.Check_RecalculateThisMonth) Then
				' Set Attribution Date to be the last day of this month 

				'AttributionDate = Now.Date
				'AttributionDate = AttributionDate.AddMonths(1)
				'AttributionDate = AttributionDate.AddDays(0 - AttributionDate.Day)

				AttributionDate = FitDateToPeriod(AttribPeriod, Now.Date)

				' Save Unit Prices
				If MainForm.GetCheckBoxChecked(Me.Check_SaveUnitPrices) = True Then
					MainForm.SetToolStripText(Label_Status, "Saving this Month's Unit Prices.")
					If Not (MainForm.InvokeRequired) Then
            'Application.DoEvents()
					End If

					Call Attribution_SaveFundPrices_Asynch(MainForm, FundID, AttributionDate, DEFAULT_STATUSGROUPFILTER, DEFAULT_ADMINISTRATORDATESFILTER, Nothing)	' Form_ProgressBar)
					Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblPrice), True)
				End If

				MainForm.SetToolStripText(Label_Status, "Saving this Month's Unit Attributions.")
				If Not (MainForm.InvokeRequired) Then
          'Application.DoEvents()
				End If

				' Run Attributions
				' Calculate Attributions

				Call Attribution_SaveAtributions_Asynch(MainForm, _
				FundID, _
				AttributionDate, _
				AttribPeriod, _
				False, _
				Me.Check_LookthroughAttributions.Checked, _
				True, _
				True, _
				MainForm.Main_Knowledgedate, MainForm.Main_Knowledgedate, _
				MainForm.Main_Knowledgedate, Nothing)	' Form_ProgressBar)

				Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblAttribution), True)

			End If

		Catch ex As Exception
		End Try

		' Display Report

		' Attribution(Report)
		' Attribution Sector Bips
		' Attribution Sector Returns
		' Attribution Sector Weights
		' Attribution(Money)
		' Attribution(Best / Worst)

		Select Case MainForm.GetComboSelectedIndex(Me.Combo_ReportName)
			Case 0
				ReportName = "rptAttribution"			' "[spu_ViewAttributions]"

			Case 1
				ReportName = "rptSectorBips"		 ' "[spu_ViewAttributions]"

			Case 2
				ReportName = "rptSectorReturns"			' "[spu_ViewAttributionsBySectorReturns]"

			Case 3
				ReportName = "rptSectorWeights"			' "[spu_ViewAttributionsBySectorReturns]"

			Case 4
				ReportName = "rptAttributionMoney"		 ' "[spu_ViewAttributions]"

			Case 5
				ReportName = "rptAttributionBestWorst"		 ' "[spu_ViewAttributions]"

			Case 6
				ReportName = "rptPnLDetailReport"		 ' "[spu_ViewAttributions]"

			Case 7
				ReportName = "rptFundPerformanceReport"		 ' 

			Case Else
				ReportName = ""
		End Select

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "Run Attribution Report : " & ReportName & ", " & RecordPreference.ToString, "", "", False)

		Try
			If (ReportName.Length > 0) Then
				MainForm.SetToolStripText(Label_Status, "Loading Data and Rendering Report.")
        'If Not (MainForm.InvokeRequired) Then
        '	Application.DoEvents()
        'End If

				Dim ReportGroupingOption As Integer = MainForm.GetComboSelectedIndex(Me.Combo_ReportGroupingOption)
				Dim ReportGroupingChoice As String = ""

				If (MainForm.GetComboSelectedValue(Combo_SelectedReportGroup) IsNot Nothing) Then
					ReportGroupingChoice = MainForm.GetComboSelectedValue(Combo_SelectedReportGroup).ToString
				End If

				Select Case MainForm.GetComboSelectedIndex(Me.Combo_ReportName)

					Case 7 ' "rptFundPerformanceReport"

            Call MainForm.MainReportHandler.rptAttributionFundPerformance(ReportName, FundID, RenaissanceGlobals.DealingPeriod.Monthly, LegalEntity, AttributionYear, AttributionMonth, MaxAttributionDate, RecordPreference, MainForm.GetCheckBoxChecked(Check_LookthroughAttributions), True, MainForm.GetCheckBoxChecked(Check_AggregateToParent), ReportGroupingOption, ReportGroupingChoice, DontShowOthers, Nothing)  '  Form_ProgressBar)

					Case Else

            Call MainForm.MainReportHandler.rptAttribution(ReportName, FundID, RenaissanceGlobals.DealingPeriod.Monthly, LegalEntity, AttributionYear, AttributionMonth, MaxAttributionDate, RecordPreference, MainForm.GetCheckBoxChecked(Check_LookthroughAttributions), True, MainForm.GetCheckBoxChecked(Check_AggregateToParent), ReportGroupingOption, ReportGroupingChoice, DontShowOthers, ShowBudget, Nothing) '  Form_ProgressBar)

				End Select


			Else
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, "", "Attribution report not found.", "", True)
			End If

		Catch ex As Exception
		End Try

	End Sub


    ''' <summary>
    ''' Handles the Click event of the btnClose control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
		' *****************************************************************************
		' Close Form
		' *****************************************************************************

		Me.Close()

	End Sub

#End Region




End Class
