' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmPFPCReconcilliationReport.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Option Compare Text

Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals


''' <summary>
''' Class frmPFPCReconciliationReport
''' </summary>
Public Class frmPFPCReconciliationReport

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmPFPCReconciliationReport"/> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The BTN run report
    ''' </summary>
  Friend WithEvents btnRunReport As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN close
    ''' </summary>
  Friend WithEvents btnClose As System.Windows.Forms.Button
    ''' <summary>
    ''' The label2
    ''' </summary>
  Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ fund
    ''' </summary>
  Friend WithEvents Combo_Fund As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The date_ value date
    ''' </summary>
  Friend WithEvents Date_ValueDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The label_ trade date
    ''' </summary>
  Friend WithEvents Label_TradeDate As System.Windows.Forms.Label
    ''' <summary>
    ''' The form_ status strip
    ''' </summary>
  Friend WithEvents Form_StatusStrip As System.Windows.Forms.StatusStrip
    ''' <summary>
    ''' The form_ progress bar
    ''' </summary>
  Friend WithEvents Form_ProgressBar As System.Windows.Forms.ToolStripProgressBar
    ''' <summary>
    ''' The label_ status
    ''' </summary>
  Friend WithEvents Label_Status As System.Windows.Forms.ToolStripStatusLabel
    ''' <summary>
    ''' The check_ show detail
    ''' </summary>
  Friend WithEvents Check_ShowDetail As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.btnRunReport = New System.Windows.Forms.Button
    Me.btnClose = New System.Windows.Forms.Button
    Me.Label2 = New System.Windows.Forms.Label
    Me.Combo_Fund = New System.Windows.Forms.ComboBox
    Me.Date_ValueDate = New System.Windows.Forms.DateTimePicker
    Me.Label_TradeDate = New System.Windows.Forms.Label
    Me.Check_ShowDetail = New System.Windows.Forms.CheckBox
    Me.Form_StatusStrip = New System.Windows.Forms.StatusStrip
    Me.Form_ProgressBar = New System.Windows.Forms.ToolStripProgressBar
    Me.Label_Status = New System.Windows.Forms.ToolStripStatusLabel
    Me.Form_StatusStrip.SuspendLayout()
    Me.SuspendLayout()
    '
    'btnRunReport
    '
    Me.btnRunReport.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnRunReport.Location = New System.Drawing.Point(44, 112)
    Me.btnRunReport.Name = "btnRunReport"
    Me.btnRunReport.Size = New System.Drawing.Size(148, 28)
    Me.btnRunReport.TabIndex = 3
    Me.btnRunReport.Text = "Run Report"
    '
    'btnClose
    '
    Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnClose.Location = New System.Drawing.Point(260, 112)
    Me.btnClose.Name = "btnClose"
    Me.btnClose.Size = New System.Drawing.Size(75, 28)
    Me.btnClose.TabIndex = 4
    Me.btnClose.Text = "&Close"
    '
    'Label2
    '
    Me.Label2.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label2.Location = New System.Drawing.Point(16, 16)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(120, 20)
    Me.Label2.TabIndex = 53
    Me.Label2.Text = "Fund Name"
    '
    'Combo_Fund
    '
    Me.Combo_Fund.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Fund.Location = New System.Drawing.Point(140, 12)
    Me.Combo_Fund.Name = "Combo_Fund"
    Me.Combo_Fund.Size = New System.Drawing.Size(252, 21)
    Me.Combo_Fund.TabIndex = 0
    '
    'Date_ValueDate
    '
    Me.Date_ValueDate.Location = New System.Drawing.Point(140, 44)
    Me.Date_ValueDate.Name = "Date_ValueDate"
    Me.Date_ValueDate.Size = New System.Drawing.Size(160, 20)
    Me.Date_ValueDate.TabIndex = 1
    '
    'Label_TradeDate
    '
    Me.Label_TradeDate.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label_TradeDate.Location = New System.Drawing.Point(16, 48)
    Me.Label_TradeDate.Name = "Label_TradeDate"
    Me.Label_TradeDate.Size = New System.Drawing.Size(100, 16)
    Me.Label_TradeDate.TabIndex = 102
    Me.Label_TradeDate.Text = "Valuation Date"
    '
    'Check_ShowDetail
    '
    Me.Check_ShowDetail.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Check_ShowDetail.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_ShowDetail.Location = New System.Drawing.Point(16, 76)
    Me.Check_ShowDetail.Name = "Check_ShowDetail"
    Me.Check_ShowDetail.Size = New System.Drawing.Size(140, 16)
    Me.Check_ShowDetail.TabIndex = 2
    Me.Check_ShowDetail.Text = "Show Detail"
    '
    'Form_StatusStrip
    '
    Me.Form_StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Form_ProgressBar, Me.Label_Status})
    Me.Form_StatusStrip.Location = New System.Drawing.Point(0, 154)
    Me.Form_StatusStrip.Name = "Form_StatusStrip"
    Me.Form_StatusStrip.Size = New System.Drawing.Size(402, 22)
    Me.Form_StatusStrip.TabIndex = 103
    Me.Form_StatusStrip.Text = " "
    '
    'Form_ProgressBar
    '
    Me.Form_ProgressBar.Maximum = 20
    Me.Form_ProgressBar.Name = "Form_ProgressBar"
    Me.Form_ProgressBar.Size = New System.Drawing.Size(150, 17)
    Me.Form_ProgressBar.Step = 1
    Me.Form_ProgressBar.Visible = False
    '
    'Label_Status
    '
    Me.Label_Status.Name = "Label_Status"
    Me.Label_Status.Size = New System.Drawing.Size(10, 17)
    Me.Label_Status.Text = " "
    '
    'frmPFPCReconciliationReport
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(402, 176)
    Me.Controls.Add(Me.Form_StatusStrip)
    Me.Controls.Add(Me.Check_ShowDetail)
    Me.Controls.Add(Me.Date_ValueDate)
    Me.Controls.Add(Me.Label_TradeDate)
    Me.Controls.Add(Me.Combo_Fund)
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.btnClose)
    Me.Controls.Add(Me.btnRunReport)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frmPFPCReconciliationReport"
    Me.Text = "PFPC Reconcilliation Report"
    Me.Form_StatusStrip.ResumeLayout(False)
    Me.Form_StatusStrip.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "


  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
  Private WithEvents _MainForm As VeniceMain

  ' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
  Private FormTooltip As New ToolTip()

    ''' <summary>
    ''' The report worker
    ''' </summary>
	Private ReportWorker As BackgroundWorkerThreadSafe = Nothing
    ''' <summary>
    ''' The form controls
    ''' </summary>
  Private FormControls As ArrayList = Nothing
    ''' <summary>
    ''' The report timer
    ''' </summary>
  Private WithEvents ReportTimer As New Windows.Forms.Timer()

  ' Form Constants, specific to the table being updated.

    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
  Private ALWAYS_CLOSE_THIS_FORM As Boolean = True

  ' The standard ChangeID for this form. e.g. tblPerson
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

  ' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
  Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
  Private THIS_FORM_FormID As VeniceFormID

  ' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
  Private FormIsValid As Boolean
    ''' <summary>
    ''' The form changed
    ''' </summary>
  Private FormChanged As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
  Private _FormOpenFailed As Boolean

  ' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
  Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
  Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
  Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
  Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
  Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
    ' Public property to return handle to the 'Main' Venice form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
  Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
    ' Public property maintaining a value indicating if the cursor is over the 'Cancel'
    ' Button on this form.
    ' This property is specifically designed for use by the field formating Event functions
    ' In order that they do not impose format restrictions if the user is about to click the 
    ' 'Cancel' button.
    '
    Get
      Return False
    End Get
    Set(ByVal Value As Boolean)
    End Set
  End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
  Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
    Get
      Return False
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
    Get
      Return True
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmPFPCReconciliationReport"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  Public Sub New(ByVal pMainForm As VeniceMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()

    _MainForm = pMainForm
    AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate
    AddHandler ReportTimer.Tick, AddressOf MainForm.IncrementStatusBarTimerEvent

    _FormOpenFailed = False

    ' ******************************************************
    ' Form Specific Settings :
    ' ******************************************************

    ' Form Permissioning :-

    THIS_FORM_PermissionArea = Me.Name
    THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

    ' 'This' form ID

    THIS_FORM_FormID = VeniceFormID.frmPFPCReconciliationReport

    ' Format Event Handlers for form controls

    AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

    AddHandler Combo_Fund.KeyUp, AddressOf MainForm.ComboSelectAsYouType

    ' Set up the ToolTip
    MainForm.SetFormToolTip(Me, FormTooltip)

    ' ******************************************************
    ' End Form Specific.
    ' ******************************************************

    THIS_FORM_ChangeID = RenaissanceChangeID.KnowledgeDate

  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
  Public Sub ResetForm() Implements StandardVeniceForm.ResetForm

    Call Form_Load(Me, New System.EventArgs)
  End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
  Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
    ALWAYS_CLOSE_THIS_FORM = True
    Me.Close()
  End Sub

    ''' <summary>
    ''' Handles the FormClosing event of the frmPFPCReconciliationReport control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.FormClosingEventArgs"/> instance containing the event data.</param>
  Private Sub frmPFPCReconciliationReport_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
    If (ReportWorker IsNot Nothing) Then
      If ReportWorker.IsBusy Then
        e.Cancel = True
        Exit Sub
      End If

      Try
        RemoveHandler ReportTimer.Tick, AddressOf MainForm.IncrementStatusBarTimerEvent
        RemoveHandler ReportWorker.DoWork, AddressOf ReportWorker_DoWork
        RemoveHandler ReportWorker.RunWorkerCompleted, AddressOf ReportWorkerCompleted
        ReportWorker.Dispose()
        ReportWorker = Nothing
      Catch ex As Exception
      End Try
    End If

  End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
    _FormOpenFailed = False

    If Not (MainForm Is Nothing) Then
      FormIsValid = True
    Else
      MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If


    ' Check User permissions
    Call CheckPermissions()
    If (HasReadPermission = False) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    ' Build Combos

    Try

			Call SetFundCombo()

			' Set Form Defaults

			Me.Date_ValueDate.Value = (New Date(Now.Year, Now.Month, 1)).AddDays(-1)
			Me.Check_ShowDetail.Checked = True

      MainForm.SetComboSelectionLengths(Me)

			' Specific Assumption !
			' Assumed that Balanced Master is Fund 1, however should not really cause
			' a problem if it is not.

			If (Me.Combo_Fund.Items.Count > 0) Then
				Try
					Combo_Fund.SelectedValue = 1
				Catch ex As Exception
				End Try
			End If

    Catch ex As Exception
    End Try

  End Sub

    ''' <summary>
    ''' Handles the Closing event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
  Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    Dim HideForm As Boolean

    ' Hide or Close this form ?
    ' All depends on how many of this form type are Open or in Cache...


    If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
      HideForm = False
    Else
      HideForm = True
      If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.ReportForm_CacheCount Then
        HideForm = False
      End If
    End If

    If HideForm = True Then
      MainForm.HideInFormsCollection(Me)
      Me.Hide() ' NPP Fix

      e.Cancel = True
    Else
      Try
        MainForm.RemoveFromFormsCollection(Me)
        RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

        RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

        RemoveHandler Combo_Fund.KeyUp, AddressOf MainForm.ComboSelectAsYouType

      Catch ex As Exception
      End Try
    End If

  End Sub


#End Region

  ' Routine to handle changes / updates to tables by this and other windows.
  ' If this, or any other, form posts a change to a table, then it will invoke an update event 
  ' detailing what tables have been altered.
  ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
  ' the 'VeniceAutoUpdate' event of the main Venice form.
  ' Each form may them react as appropriate to changes in any table that might impact it.
  '
    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    Dim KnowledgeDateChanged As Boolean
    Dim SetButtonStatus_Flag As Boolean

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

    KnowledgeDateChanged = False
    SetButtonStatus_Flag = False

    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
      KnowledgeDateChanged = True
    End If

    ' ****************************************************************
    ' Check for changes relevant to this form
    ' ****************************************************************

    ' Changes to the KnowledgeDate :-
    If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
      SetButtonStatus_Flag = True
    End If

    ' Changes to the tblFund table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFund) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Call SetFundCombo()
    End If


    ' Changes to the tblUserPermissions table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

      ' Check ongoing permissions.

      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

        FormIsValid = False
        Me.Close()
        Exit Sub
      End If

      SetButtonStatus_Flag = True

    End If

    ' ****************************************************************
    '
    ' ****************************************************************


  End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

  ' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
  Private Sub CheckPermissions()
    ' *****************************************************************************
    '
    ' *****************************************************************************

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

  End Sub


#End Region

#Region " Set Form Combos (Form Specific Code) "

    ''' <summary>
    ''' Sets the fund combo.
    ''' </summary>
  Private Sub SetFundCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_Fund, _
    RenaissanceStandardDatasets.tblFund, _
    "FundName", _
    "FundID", _
    "FundClosed=0", False, True, True)  ' 

  End Sub



#End Region

#Region " Form Control Event functions"

    ''' <summary>
    ''' Handles the ValueChanged event of the Date_ValueDate control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Date_ValueDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Date_ValueDate.ValueChanged
    Dim CurrentDate As Date
    Dim MonthEndDate As Date

    CurrentDate = Me.Date_ValueDate.Value

    MonthEndDate = CurrentDate.AddMonths(1)
    MonthEndDate = MonthEndDate.AddDays(-MonthEndDate.Day)

    If CurrentDate.CompareTo(MonthEndDate) = 0 Then
      Exit Sub
    End If
    Me.Date_ValueDate.Value = MonthEndDate
  End Sub

#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

    ''' <summary>
    ''' Handles the Click event of the btnRunReport control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnRunReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRunReport.Click
    ' *****************************************************************************
    ' Run Report
    ' *****************************************************************************

    Try
      FormControls = MainForm.MainReportHandler.DisableFormControls(Me)
      Form_ProgressBar.Value = Form_ProgressBar.Minimum
      MainForm.SetToolStripText(Label_Status, "Processing Report")

      If (ReportWorker Is Nothing) Then
				ReportWorker = New BackgroundWorkerThreadSafe(Nothing)
        AddHandler ReportWorker.DoWork, AddressOf ReportWorker_DoWork
        AddHandler ReportWorker.RunWorkerCompleted, AddressOf ReportWorkerCompleted
      End If

      ReportTimer.Interval = 25
      ReportTimer.Tag = Me.Form_ProgressBar
      ReportTimer.Start()


      If MainForm.UseReportWorkerThreads Then
        ReportWorker.RunWorkerAsync()
      Else
        RunReport()
        Call ReportWorkerCompleted(Nothing, Nothing)
      End If


    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
    End Try
  End Sub

    ''' <summary>
    ''' Handles the DoWork event of the ReportWorker control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.DoWorkEventArgs"/> instance containing the event data.</param>
  Private Sub ReportWorker_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) ' Handles backgroundWorker1.DoWork
    RunReport()
  End Sub

    ''' <summary>
    ''' Reports the worker completed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.RunWorkerCompletedEventArgs"/> instance containing the event data.</param>
  Private Sub ReportWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) ' Handles backgroundWorker1.RunWorkerCompleted
    Try
      ReportTimer.Stop()
      MainForm.MainReportHandler.EnableFormControls(FormControls)
      MainForm.SetToolStripText(Label_Status, "")

      'Form_ProgressBar = MainForm.ResetProgressBar(Form_StatusStrip, Form_ProgressBar)
      Form_ProgressBar.Value = Form_ProgressBar.Minimum
      Form_ProgressBar.Visible = False
      ReportTimer.Tag = Me.Form_ProgressBar
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
    End Try

  End Sub

    ''' <summary>
    ''' Runs the report.
    ''' </summary>
  Private Sub RunReport()
    ' *****************************************************************************
    ' Run Report
    ' *****************************************************************************

    Dim FundID As Integer
    Dim ValueDate As Date


    ' Validate

    If (MainForm.GetComboSelectedValue(Combo_Fund) Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Fund must be selected.", "", True)
      Exit Sub
    End If

    If (CInt(MainForm.GetComboSelectedIndex(Combo_Fund)) <= 0) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Fund must be selected.", "", True)
      Exit Sub
    End If
    FundID = CInt(MainForm.GetComboSelectedValue(Combo_Fund))

    ValueDate = MainForm.GetDatetimeValue(Date_ValueDate).Date

    MainForm.SetToolStripText(Label_Status, "")
    If Not (MainForm.InvokeRequired) Then
      Application.DoEvents()
    End If

    Dim ReportData As DataView

    Try

      MainForm.SetToolStripText(Label_Status, "Reconciling PFPC Data")
      If Not (MainForm.InvokeRequired) Then
        Application.DoEvents()
      End If

			If ReconcilePortfolioValuation(Me.Name, FundID, ValueDate, DEFAULT_STATUSGROUPFILTER, DEFAULT_ADMINISTRATORDATESFILTER) = True Then

				MainForm.SetToolStripText(Label_Status, "Loading Report Data")
				If Not (MainForm.InvokeRequired) Then
					Application.DoEvents()
				End If

				ReportData = MainForm.MainReportHandler.GetData_PFPCReconciliation(FundID, ValueDate, MainForm.GetCheckBoxChecked(Check_ShowDetail))

				If (Not (ReportData Is Nothing)) Then
					Dim ReportFieldValues(2) As classReportFieldValue

					ReportFieldValues(0) = New classReportFieldValue(ReportHandler.REPORT_ValueDate_FieldName, ValueDate)
					ReportFieldValues(1) = New classReportFieldValue(ReportHandler.REPORT_KnowledgeDate_FieldName, MainForm.Main_Knowledgedate)
					ReportFieldValues(2) = Nothing

					MainForm.SetToolStripText(Label_Status, "Rendering Report")
					If Not (MainForm.InvokeRequired) Then
						Application.DoEvents()
					End If

					If MainForm.GetCheckBoxChecked(Check_ShowDetail) Then
						' Only populate this field if the Detail report is being run
						ReportFieldValues(2) = New classReportFieldValue("PFPCIssuedShares_" & FundID.ToString & "_" & ValueDate.ToString(QUERY_SHORTDATEFORMAT), "Text_PFPCUnitCount", "", "Text")

						' Display report
						Call MainForm.MainReportHandler.DisplayReport("rptPFPCPortfolioReconcilliationDetail", ReportData, FundID, ReportFieldValues, Nothing) ' Form_ProgressBar)
					Else
						Call MainForm.MainReportHandler.DisplayReport("rptPFPCPortfolioReconcilliation", ReportData, FundID, ReportFieldValues, Nothing) ' Form_ProgressBar)
					End If
				End If

			End If

    Catch ex As Exception
    End Try

    Me.MainForm.SetToolStripText(Label_Status, "")

  End Sub


    ''' <summary>
    ''' Handles the Click event of the btnClose control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
    ' *****************************************************************************
    ' Close Form
    ' *****************************************************************************

    Me.Close()

  End Sub

#End Region

#Region " PFPC Reconciliation Code"

    ''' <summary>
    ''' Reconciles the portfolio valuation.
    ''' </summary>
    ''' <param name="Permission_Area">The permission_ area.</param>
    ''' <param name="p_FundID">The p_ fund ID.</param>
    ''' <param name="p_ReportDate">The p_ report date.</param>
    ''' <param name="pStatusGroupFilter">The p status group filter.</param>
    ''' <param name="pAdministratorDatesFilter">The p administrator dates filter.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	Private Function ReconcilePortfolioValuation(ByVal Permission_Area As String, ByVal p_FundID As Long, ByVal p_ReportDate As Date, ByVal pStatusGroupFilter As String, ByVal pAdministratorDatesFilter As Integer) As Boolean
		' ********************************************************************************************
		' Purpose:  To populate the tblPFPCPortfolioReconcilliation table with the reconcilliation data for
		'
		' Accepts:  Permission_Area : Used for error Reporting.
		'           p_FundID        : Fund ID to reconcile for.
		'           p_ReportDate    : Value Date to report for.
		'
		' Returns:  None.
		'
		' ********************************************************************************************

		Dim RVal As Boolean
		RVal = False

		Dim DSFCP_Portfolio As New DataSet
		Dim tblFCP_Portfolio As System.Data.DataTable
		Dim FCP_Valuation_Rows As DataRow()
		Dim FCP_ValuationRow As DataRow

		Dim DS_PFPC_Valuation As RenaissanceDataClass.DSPFPCPortfolioValuation
		Dim tblPFPC_Valuation As RenaissanceDataClass.DSPFPCPortfolioValuation.tblPFPCPortfolioValuationDataTable
		Dim PFPC_Valuation_Rows As RenaissanceDataClass.DSPFPCPortfolioValuation.tblPFPCPortfolioValuationRow()
		Dim PFPC_ValuationRow As RenaissanceDataClass.DSPFPCPortfolioValuation.tblPFPCPortfolioValuationRow

		Dim ThisStandardDataset As StandardDataset
		ThisStandardDataset = RenaissanceStandardDatasets.tblPFPCPortfolioReconcilliation

		Dim Local_Value As Double
		Dim FCP_PFPCID As String
		Dim PFPC_PFPCID As String

		Dim Temp_FCPInstrumentID As Long
		' Dim Counter As Integer

		Dim DS_PortfolioRec As RenaissanceDataClass.DSPFPCPortfolioReconcilliation
		Dim Tbl_PortfolioRec As RenaissanceDataClass.DSPFPCPortfolioReconcilliation.tblPFPCPortfolioReconcilliationDataTable
		Dim PortRecAdaptor As SqlDataAdapter
		Dim newPortRecRow As RenaissanceDataClass.DSPFPCPortfolioReconcilliation.tblPFPCPortfolioReconcilliationRow

		'Try
		'	Dim SelectedPortRecRows As RenaissanceDataClass.DSPFPCPortfolioReconcilliation.tblPFPCPortfolioReconcilliationRow()

		'	PortRecAdaptor = MainForm.MainDataHandler.Get_Adaptor(ThisStandardDataset.Adaptorname, VENICE_CONNECTION, ThisStandardDataset.TableName)
		'	If (PortRecAdaptor Is Nothing) Then
		'		MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Failed to get tblPFPCPortfolioreconciliation Adaptor.", "", True)
		'		Exit Function
		'	End If

		'	DS_PortfolioRec = MainForm.Load_Table(ThisStandardDataset, True)
		'	If (DS_PortfolioRec Is Nothing) Then
		'		MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Failed to get tblPFPCPortfolioreconciliation table.", "", True)
		'		Exit Function
		'	End If
		'	Tbl_PortfolioRec = DS_PortfolioRec.tblPFPCPortfolioReconcilliation
		'	SelectedPortRecRows = Tbl_PortfolioRec.Select("(FundID=" & p_FundID & ") AND (ValuationDate='" & p_ReportDate.ToString(QUERY_SHORTDATEFORMAT) & "')")

		'	If (Not (SelectedPortRecRows Is Nothing)) AndAlso (SelectedPortRecRows.Length > 0) Then
		'		SyncLock Tbl_PortfolioRec
		'			For Counter = 0 To (SelectedPortRecRows.Length - 1)
		'				SelectedPortRecRows(Counter).Delete()
		'			Next

		'			PortRecAdaptor.Update(SelectedPortRecRows)
		'		End SyncLock
		'	End If

		'Catch ex As Exception
		'	Call MainForm.LogError(Me.Name & ", ReconcilePortfolioValuation()", LOG_LEVELS.Error, ex.Message, "Error clearing tblPFPCPortfolioValuation records.", ex.StackTrace, True)
		'	GoTo ReconcilePortfolioValuation_Exit

		'End Try

		' ****************************************************************
		'
		' Clear existing data for this Fund and Value Date combination
		' Delete existing Records
		'
		' ****************************************************************

		Dim DeleteCommand As New SqlCommand
		Try
			DeleteCommand.CommandType = CommandType.Text
			DeleteCommand.CommandText = "DELETE FROM tblPFPCPortfolioReconcilliation WHERE (FundID=@FundID) AND (ValuationDate=@ValuationDate)"
			DeleteCommand.Parameters.Add("@FundID", SqlDbType.Int).Value = p_FundID
			DeleteCommand.Parameters.Add("@ValuationDate", SqlDbType.DateTime).Value = p_ReportDate
			DeleteCommand.Connection = MainForm.GetVeniceConnection()

			SyncLock DeleteCommand.Connection
				DeleteCommand.ExecuteNonQuery()
			End SyncLock

		Catch ex As Exception
			MainForm.LogError(Permission_Area, LOG_LEVELS.Error, ex.Message, "Error Deleting existing Records", ex.StackTrace, True)
		Finally

			If (DeleteCommand.Connection IsNot Nothing) Then
				Try
					DeleteCommand.Connection.Close()
				Catch ex As Exception
				Finally
					DeleteCommand.Connection = Nothing
				End Try
			End If

		End Try

		' ****************************************************************
		'
		' Get Adaptor and Table references for tblPFPCPortfolioreconciliation
		'
		' ****************************************************************

		Try

			PortRecAdaptor = MainForm.MainDataHandler.Get_Adaptor(ThisStandardDataset.Adaptorname, VENICE_CONNECTION, ThisStandardDataset.TableName)
			If (PortRecAdaptor Is Nothing) Then
				MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Failed to get tblPFPCPortfolioreconciliation Adaptor.", "", True)
				Exit Function
			End If

			DS_PortfolioRec = MainForm.Load_Table(ThisStandardDataset, True)
			If (DS_PortfolioRec Is Nothing) Then
				MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Failed to get tblPFPCPortfolioreconciliation table.", "", True)
				Exit Function
			End If
			Tbl_PortfolioRec = DS_PortfolioRec.tblPFPCPortfolioReconcilliation

		Catch ex As Exception
			Call MainForm.LogError(Me.Name & ", ReconcilePortfolioValuation()", LOG_LEVELS.Error, ex.Message, "Error clearing tblPFPCPortfolioValuation records.", ex.StackTrace, True)
			GoTo ReconcilePortfolioValuation_Exit
		End Try

		' ****************************************************************
		' Establish F&C Portfolio Recordset
		' ****************************************************************

		Dim FCP_Portfolio_Adaptor As New SqlDataAdapter
		Try

			MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.GetVeniceConnection(), FCP_Portfolio_Adaptor, "spu_Valuation")
			' tblFCP_Portfolio = New System.Data.DataTable("FCP_Valuation")

			FCP_Portfolio_Adaptor.SelectCommand.Parameters("@FundID").Value = p_FundID
			FCP_Portfolio_Adaptor.SelectCommand.Parameters("@LegalEntity").Value = ""
			FCP_Portfolio_Adaptor.SelectCommand.Parameters("@ValueDate").Value = p_ReportDate
			FCP_Portfolio_Adaptor.SelectCommand.Parameters("@UnitPriceVariant").Value = 0
			FCP_Portfolio_Adaptor.SelectCommand.Parameters("@StatusGroupFilter").Value = pStatusGroupFilter
			FCP_Portfolio_Adaptor.SelectCommand.Parameters("@AdministratorDatesFilter").Value = pAdministratorDatesFilter
			FCP_Portfolio_Adaptor.SelectCommand.Parameters("@DisregardWatermarkPoints").Value = 0
			FCP_Portfolio_Adaptor.SelectCommand.Parameters("@AggregateToInstrumentParentID").Value = 0
			FCP_Portfolio_Adaptor.SelectCommand.Parameters("@ShowDetailedTransactions").Value = 0
			FCP_Portfolio_Adaptor.SelectCommand.Parameters("@DetailDate").Value = p_ReportDate
			FCP_Portfolio_Adaptor.SelectCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate '  Altered NPP, 26 Aug 2008 for AO. from KNOWLEDGEDATE_NOW.

			SyncLock FCP_Portfolio_Adaptor.SelectCommand.Connection
				FCP_Portfolio_Adaptor.Fill(DSFCP_Portfolio)
			End SyncLock
			tblFCP_Portfolio = DSFCP_Portfolio.Tables(0)

			FCP_Valuation_Rows = tblFCP_Portfolio.Select("True", "InstrumentPFPCID, InstrumentDescription")
			FCP_Portfolio_Adaptor = Nothing

		Catch ex As Exception
			Call MainForm.LogError(Me.Name & ", ReconcilePortfolioValuation()", LOG_LEVELS.Error, ex.Message, "Error getting FCP Portfolio (Valuation).", ex.StackTrace, True)
			GoTo ReconcilePortfolioValuation_Exit

		Finally
			Try
				If (FCP_Portfolio_Adaptor IsNot Nothing) AndAlso (FCP_Portfolio_Adaptor.SelectCommand IsNot Nothing) AndAlso (FCP_Portfolio_Adaptor.SelectCommand.Connection IsNot Nothing) Then
					FCP_Portfolio_Adaptor.SelectCommand.Connection.Close()
					FCP_Portfolio_Adaptor.SelectCommand.Connection = Nothing
				End If
			Catch ex As Exception
			End Try
		End Try


		' ****************************************************************
		' Establish PFPC Portfolio Recordset
		' ****************************************************************

		Try
			DS_PFPC_Valuation = MainForm.Load_Table(RenaissanceStandardDatasets.tblPFPCPortfolioValuation, True) ' Force refresh, just in case.

			If (DS_PFPC_Valuation Is Nothing) Then
				Call MainForm.LogError(Me.Name & ", ReconcilePortfolioValuation()", LOG_LEVELS.Error, "", "Error getting PFPC Portfolio Recordset.", "", True)
				GoTo ReconcilePortfolioValuation_Exit
			End If

			tblPFPC_Valuation = DS_PFPC_Valuation.tblPFPCPortfolioValuation
			PFPC_Valuation_Rows = tblPFPC_Valuation.Select("(FundID=" & p_FundID & ") AND (ValuationDate='" & p_ReportDate.ToString(QUERY_SHORTDATEFORMAT) & "')", "InstrumentID, InstrumentDescription")

		Catch ex As Exception
			Call MainForm.LogError(Me.Name & ", ReconcilePortfolioValuation()", LOG_LEVELS.Error, ex.Message, "Error establishing PFPC Portfolio Recordset and Adaptor.", ex.StackTrace, True)
			GoTo ReconcilePortfolioValuation_Exit
		End Try

		' *************************************************************************
		' Compare Recordsets
		' *************************************************************************

		Dim FCP_ValuationCounter As Integer
		Dim PFPC_ValuationCounter As Integer

		FCP_ValuationCounter = 0
		PFPC_ValuationCounter = 0

		Try

			While (FCP_ValuationCounter < FCP_Valuation_Rows.Length) Or (PFPC_ValuationCounter < PFPC_Valuation_Rows.Length)
				FCP_PFPCID = ""
				PFPC_PFPCID = ""

				Try

					If (FCP_ValuationCounter < FCP_Valuation_Rows.Length) Then
						FCP_ValuationRow = FCP_Valuation_Rows(FCP_ValuationCounter)
						FCP_PFPCID = Nz(FCP_ValuationRow("InstrumentPFPCID"), "").ToString.ToUpper
					Else
						FCP_ValuationRow = Nothing
					End If

					If (PFPC_ValuationCounter < PFPC_Valuation_Rows.Length) Then
						PFPC_ValuationRow = PFPC_Valuation_Rows(PFPC_ValuationCounter)
						PFPC_PFPCID = Nz(PFPC_ValuationRow.InstrumentID, "").ToString.ToUpper
					Else
						PFPC_ValuationRow = Nothing
					End If

					If IsNumeric(FCP_PFPCID) Then
						If FCP_PFPCID.StartsWith(".") Then
							If (FCP_PFPCID.Length < 7) Then
								FCP_PFPCID = "0000000" & FCP_PFPCID
								FCP_PFPCID = FCP_PFPCID.Substring(FCP_PFPCID.Length - 7)
							End If
						End If
					End If

					If IsNumeric(PFPC_PFPCID) Then
						If PFPC_PFPCID.StartsWith(".") Then
							If (PFPC_PFPCID.Length < 7) Then
								PFPC_PFPCID = "0000000" & PFPC_PFPCID
								PFPC_PFPCID = PFPC_PFPCID.Substring(PFPC_PFPCID.Length - 7)
							End If
						End If
					End If

				Catch ex As Exception
					Call MainForm.LogError(Me.Name & ", ReconcilePortfolioValuation()", LOG_LEVELS.Error, ex.Message, "Error getting Portfolio rows and Instrument IDs.", ex.StackTrace, True)
					GoTo ReconcilePortfolioValuation_Exit
				End Try


				If (FCP_ValuationCounter >= FCP_Valuation_Rows.Length) And (PFPC_ValuationCounter < PFPC_Valuation_Rows.Length) Then ' (ValuationRS.EOF = True) And (PFPCRS.EOF = False) Then
					' OK , No more Valuation RS, Save PFPC record
					Temp_FCPInstrumentID = 0

					' Kludge for Brian.
					If PFPC_ValuationRow.InstrumentID.ToUpper.StartsWith("USD") Then
						Temp_FCPInstrumentID = 1
					ElseIf PFPC_ValuationRow.InstrumentID.ToUpper.StartsWith("GBP") Then
						Temp_FCPInstrumentID = 2
					ElseIf PFPC_ValuationRow.InstrumentID.ToUpper.StartsWith("EUR") Then
						Temp_FCPInstrumentID = 3
					End If

					Try

						newPortRecRow = Tbl_PortfolioRec.NewtblPFPCPortfolioReconcilliationRow
						newPortRecRow.ValuationDate = p_ReportDate
						newPortRecRow.FundID = p_FundID
						newPortRecRow.FCPInstrumentID = Temp_FCPInstrumentID
						newPortRecRow.PFPCInstrumentID = PFPC_ValuationRow.InstrumentID
						newPortRecRow.PFPCInstrumentDescription = PFPC_ValuationRow.InstrumentDescription
						newPortRecRow.FCP_Holding = 0
						newPortRecRow.FCP_HoldingCompleteness = 0
						newPortRecRow.PFPC_Holding = PFPC_ValuationRow.Amount
						newPortRecRow.Holding_Difference = PFPC_ValuationRow.Amount
						newPortRecRow.FCP_Price = 0
						newPortRecRow.FCP_PriceFinal = 0
						newPortRecRow.FCP_PriceAdministrator = 0
						newPortRecRow.FCP_PriceDate = #1/1/1900#
						newPortRecRow.PFPC_Price = PFPC_ValuationRow.LocalCcyPrice
						newPortRecRow.Price_Difference = PFPC_ValuationRow.LocalCcyPrice
						newPortRecRow.FCP_FXRate = 0
						newPortRecRow.PFPC_FXRate = PFPC_ValuationRow.FxRate
						newPortRecRow.FXRate_Difference = PFPC_ValuationRow.FxRate
						newPortRecRow.FCP_LocalCcyValue = 0
						newPortRecRow.PFPC_LocalCcyValue = PFPC_ValuationRow.LocalCcyValue
						newPortRecRow.LocalCcyValue_Difference = -(PFPC_ValuationRow.LocalCcyValue)
						newPortRecRow.FCP_BookCcyValue = 0
						newPortRecRow.PFPC_BookCcyValue = PFPC_ValuationRow.BookCcyValue
						newPortRecRow.BookCcyValue_Difference = -(PFPC_ValuationRow.BookCcyValue)

						Tbl_PortfolioRec.Rows.Add(newPortRecRow)

					Catch ex As Exception
						Call MainForm.LogError(Me.Name & ", ReconcilePortfolioValuation()", LOG_LEVELS.Error, ex.Message, "Error saving reconciliation Row.", ex.StackTrace, True)
						GoTo ReconcilePortfolioValuation_Exit
					End Try

					FCP_ValuationCounter += 1
					PFPC_ValuationCounter += 1

				ElseIf (FCP_ValuationCounter < FCP_Valuation_Rows.Length) And (PFPC_ValuationCounter >= PFPC_Valuation_Rows.Length) Then
					' OK , No more PFPC record, Save Valuation RS.

					Local_Value = CDbl(Nz(CDbl(FCP_ValuationRow("SignedUnits")), 0)) * CDbl(Nz(CDbl(FCP_ValuationRow("PriceLevel")), 0))
					'Local_Value = Nz(ValuationRS!SignedUnits, 0) * Nz(ValuationRS!PriceLevel, 0)

					Try
						newPortRecRow = Tbl_PortfolioRec.NewtblPFPCPortfolioReconcilliationRow
						newPortRecRow.ValuationDate = p_ReportDate
						newPortRecRow.FundID = p_FundID
						newPortRecRow.FCPInstrumentID = CInt(FCP_ValuationRow("Instrument"))
						newPortRecRow.PFPCInstrumentID = ""
						newPortRecRow.PFPCInstrumentDescription = ""
						newPortRecRow.FCP_Holding = CDbl(FCP_ValuationRow("SignedUnits"))
						newPortRecRow.FCP_HoldingCompleteness = CDbl(FCP_ValuationRow("TransactionCompleteness"))
						newPortRecRow.PFPC_Holding = 0
						newPortRecRow.Holding_Difference = CDbl(FCP_ValuationRow("SignedUnits")) '
						newPortRecRow.FCP_Price = CDbl(FCP_ValuationRow("PriceLevel"))
						newPortRecRow.FCP_PriceFinal = CInt(FCP_ValuationRow("PriceFinal"))
						newPortRecRow.FCP_PriceAdministrator = CInt(FCP_ValuationRow("PriceIsAdministrator"))
						newPortRecRow.FCP_PriceDate = CDate(FCP_ValuationRow("PriceDate"))
						newPortRecRow.PFPC_Price = 0
						newPortRecRow.Price_Difference = CDbl(FCP_ValuationRow("PriceLevel"))
						newPortRecRow.FCP_FXRate = CDbl(FCP_ValuationRow("InstrumentFXRate"))
						newPortRecRow.PFPC_FXRate = 0
						newPortRecRow.FXRate_Difference = CDbl(FCP_ValuationRow("InstrumentFXRate")) '
						newPortRecRow.FCP_LocalCcyValue = Local_Value
						newPortRecRow.PFPC_LocalCcyValue = 0
						newPortRecRow.LocalCcyValue_Difference = Local_Value ' 
						newPortRecRow.FCP_BookCcyValue = CDbl(FCP_ValuationRow("Value"))
						newPortRecRow.PFPC_BookCcyValue = 0
						newPortRecRow.BookCcyValue_Difference = CDbl(FCP_ValuationRow("Value"))

						Tbl_PortfolioRec.Rows.Add(newPortRecRow)
					Catch ex As Exception
						Call MainForm.LogError(Me.Name & ", ReconcilePortfolioValuation()", LOG_LEVELS.Error, ex.Message, "Error saving reconciliation Row.", ex.StackTrace, True)
						GoTo ReconcilePortfolioValuation_Exit
					End Try

					FCP_ValuationCounter += 1
					PFPC_ValuationCounter += 1

				Else
					' OK, Both Records exist, Compare if OK, Else advance accordingly.

					If (FCP_ValuationRow.IsNull("InstrumentPFPCID") Or PFPC_ValuationRow.IsNull("InstrumentID")) Then
						' One or Both ID fields are <Null>
						' Output and Advance the Null Record.

						If FCP_ValuationRow.IsNull("InstrumentPFPCID") Then
							Local_Value = CDbl(Nz(CDbl(FCP_ValuationRow("SignedUnits")), 0)) * CDbl(Nz(CDbl(FCP_ValuationRow("PriceLevel")), 0))
							' Local_Value = Nz(ValuationRS!SignedUnits, 0) * Nz(ValuationRS!PriceLevel, 0)

							Try
								newPortRecRow = Tbl_PortfolioRec.NewtblPFPCPortfolioReconcilliationRow
								newPortRecRow.ValuationDate = p_ReportDate
								newPortRecRow.FundID = p_FundID
								newPortRecRow.FCPInstrumentID = CInt(FCP_ValuationRow("Instrument"))
								newPortRecRow.PFPCInstrumentID = ""
								newPortRecRow.PFPCInstrumentDescription = ""
								newPortRecRow.FCP_Holding = CDbl(FCP_ValuationRow("SignedUnits"))
								newPortRecRow.FCP_HoldingCompleteness = CDbl(FCP_ValuationRow("TransactionCompleteness"))
								newPortRecRow.PFPC_Holding = 0
								newPortRecRow.Holding_Difference = CDbl(FCP_ValuationRow("SignedUnits")) '
								newPortRecRow.FCP_Price = CDbl(FCP_ValuationRow("PriceLevel"))
								newPortRecRow.FCP_PriceFinal = CInt(FCP_ValuationRow("PriceFinal"))
								newPortRecRow.FCP_PriceAdministrator = CInt(FCP_ValuationRow("PriceIsAdministrator"))
								newPortRecRow.FCP_PriceDate = CDate(FCP_ValuationRow("PriceDate"))
								newPortRecRow.PFPC_Price = 0
								newPortRecRow.Price_Difference = CDbl(FCP_ValuationRow("PriceLevel"))
								newPortRecRow.FCP_FXRate = CDbl(FCP_ValuationRow("InstrumentFXRate"))
								newPortRecRow.PFPC_FXRate = 0
								newPortRecRow.FXRate_Difference = CDbl(FCP_ValuationRow("InstrumentFXRate")) '
								newPortRecRow.FCP_LocalCcyValue = Local_Value
								newPortRecRow.PFPC_LocalCcyValue = 0
								newPortRecRow.LocalCcyValue_Difference = Local_Value ' 
								newPortRecRow.FCP_BookCcyValue = CDbl(FCP_ValuationRow("Value"))
								newPortRecRow.PFPC_BookCcyValue = 0
								newPortRecRow.BookCcyValue_Difference = CDbl(FCP_ValuationRow("Value"))

								Tbl_PortfolioRec.Rows.Add(newPortRecRow)

							Catch ex As Exception
								Call MainForm.LogError(Me.Name & ", ReconcilePortfolioValuation()", LOG_LEVELS.Error, ex.Message, "Error saving reconciliation Row.", ex.StackTrace, True)
								GoTo ReconcilePortfolioValuation_Exit
							End Try

							FCP_ValuationCounter += 1
						End If

						If PFPC_ValuationRow.IsNull("InstrumentID") Then

							Try
								newPortRecRow = Tbl_PortfolioRec.NewtblPFPCPortfolioReconcilliationRow
								newPortRecRow.ValuationDate = p_ReportDate
								newPortRecRow.FundID = p_FundID
								newPortRecRow.FCPInstrumentID = 0
								newPortRecRow.PFPCInstrumentID = PFPC_ValuationRow.InstrumentID
								newPortRecRow.PFPCInstrumentDescription = PFPC_ValuationRow.InstrumentDescription
								newPortRecRow.FCP_Holding = 0
								newPortRecRow.FCP_HoldingCompleteness = 0
								newPortRecRow.PFPC_Holding = PFPC_ValuationRow.Amount
								newPortRecRow.Holding_Difference = PFPC_ValuationRow.Amount
								newPortRecRow.FCP_Price = 0
								newPortRecRow.FCP_PriceFinal = 0
								newPortRecRow.FCP_PriceAdministrator = 0
								newPortRecRow.FCP_PriceDate = #1/1/1900#
								newPortRecRow.PFPC_Price = PFPC_ValuationRow.LocalCcyPrice
								newPortRecRow.Price_Difference = PFPC_ValuationRow.LocalCcyPrice
								newPortRecRow.FCP_FXRate = 0
								newPortRecRow.PFPC_FXRate = PFPC_ValuationRow.FxRate
								newPortRecRow.FXRate_Difference = PFPC_ValuationRow.FxRate
								newPortRecRow.FCP_LocalCcyValue = 0
								newPortRecRow.PFPC_LocalCcyValue = PFPC_ValuationRow.LocalCcyValue
								newPortRecRow.LocalCcyValue_Difference = -(PFPC_ValuationRow.LocalCcyValue)
								newPortRecRow.FCP_BookCcyValue = 0
								newPortRecRow.PFPC_BookCcyValue = PFPC_ValuationRow.BookCcyValue
								newPortRecRow.BookCcyValue_Difference = -(PFPC_ValuationRow.BookCcyValue)

								Tbl_PortfolioRec.Rows.Add(newPortRecRow)

							Catch ex As Exception
								Call MainForm.LogError(Me.Name & ", ReconcilePortfolioValuation()", LOG_LEVELS.Error, ex.Message, "Error saving reconciliation Row.", ex.StackTrace, True)
								GoTo ReconcilePortfolioValuation_Exit
							End Try

							PFPC_ValuationCounter += 1
						End If

						' OK, Both IDs are not NULL...
					ElseIf (FCP_PFPCID = PFPC_PFPCID) Then

						' IDs present and the same
						Local_Value = CDbl(Nz(CDbl(FCP_ValuationRow("SignedUnits")), 0)) * CDbl(Nz(CDbl(FCP_ValuationRow("PriceLevel")), 0))

						Try
							newPortRecRow = Tbl_PortfolioRec.NewtblPFPCPortfolioReconcilliationRow
							newPortRecRow.ValuationDate = p_ReportDate
							newPortRecRow.FundID = p_FundID
							newPortRecRow.FCPInstrumentID = CInt(FCP_ValuationRow("Instrument"))
							newPortRecRow.PFPCInstrumentID = PFPC_ValuationRow.InstrumentID
							newPortRecRow.PFPCInstrumentDescription = PFPC_ValuationRow.InstrumentDescription
							newPortRecRow.FCP_Holding = CDbl(FCP_ValuationRow("SignedUnits"))
							newPortRecRow.FCP_HoldingCompleteness = CDbl(FCP_ValuationRow("TransactionCompleteness"))
							newPortRecRow.PFPC_Holding = PFPC_ValuationRow.Amount
							newPortRecRow.Holding_Difference = CDbl(FCP_ValuationRow("SignedUnits")) - PFPC_ValuationRow.Amount	'
							newPortRecRow.FCP_Price = CDbl(FCP_ValuationRow("PriceLevel"))
							newPortRecRow.FCP_PriceFinal = CInt(FCP_ValuationRow("PriceFinal"))
							newPortRecRow.FCP_PriceAdministrator = CInt(FCP_ValuationRow("PriceIsAdministrator"))
							newPortRecRow.FCP_PriceDate = CDate(FCP_ValuationRow("PriceDate"))
							newPortRecRow.PFPC_Price = PFPC_ValuationRow.LocalCcyPrice
							newPortRecRow.Price_Difference = CDbl(FCP_ValuationRow("PriceLevel")) - PFPC_ValuationRow.LocalCcyPrice
							newPortRecRow.FCP_FXRate = CDbl(FCP_ValuationRow("InstrumentFXRate"))
							newPortRecRow.PFPC_FXRate = PFPC_ValuationRow.FxRate
							newPortRecRow.FXRate_Difference = CDbl(FCP_ValuationRow("InstrumentFXRate")) - PFPC_ValuationRow.FxRate	'
							newPortRecRow.FCP_LocalCcyValue = Local_Value
							newPortRecRow.PFPC_LocalCcyValue = PFPC_ValuationRow.LocalCcyValue
							newPortRecRow.LocalCcyValue_Difference = Local_Value - PFPC_ValuationRow.LocalCcyValue ' 
							newPortRecRow.FCP_BookCcyValue = CDbl(FCP_ValuationRow("Value"))
							newPortRecRow.PFPC_BookCcyValue = PFPC_ValuationRow.BookCcyValue
							newPortRecRow.BookCcyValue_Difference = CDbl(FCP_ValuationRow("Value")) - PFPC_ValuationRow.BookCcyValue

							Tbl_PortfolioRec.Rows.Add(newPortRecRow)
						Catch ex As Exception
							Call MainForm.LogError(Me.Name & ", ReconcilePortfolioValuation()", LOG_LEVELS.Error, ex.Message, "Error saving reconciliation Row.", ex.StackTrace, True)
							GoTo ReconcilePortfolioValuation_Exit
						End Try

						FCP_ValuationCounter += 1
						PFPC_ValuationCounter += 1

					ElseIf (FCP_PFPCID < PFPC_PFPCID) Then
						' IDs Present, ValuationRS is less

						Local_Value = CDbl(Nz(CDbl(FCP_ValuationRow("SignedUnits")), 0)) * CDbl(Nz(CDbl(FCP_ValuationRow("PriceLevel")), 0))
						' Local_Value = Nz(ValuationRS!SignedUnits, 0) * Nz(ValuationRS!PriceLevel, 0)

						Try
							newPortRecRow = Tbl_PortfolioRec.NewtblPFPCPortfolioReconcilliationRow
							newPortRecRow.ValuationDate = p_ReportDate
							newPortRecRow.FundID = p_FundID
							newPortRecRow.FCPInstrumentID = CInt(FCP_ValuationRow("Instrument"))
							newPortRecRow.PFPCInstrumentID = ""
							newPortRecRow.PFPCInstrumentDescription = ""
							newPortRecRow.FCP_Holding = CDbl(FCP_ValuationRow("SignedUnits"))
							newPortRecRow.FCP_HoldingCompleteness = CDbl(FCP_ValuationRow("TransactionCompleteness"))
							newPortRecRow.PFPC_Holding = 0
							newPortRecRow.Holding_Difference = CDbl(FCP_ValuationRow("SignedUnits")) '
							newPortRecRow.FCP_Price = CDbl(FCP_ValuationRow("PriceLevel"))
							newPortRecRow.FCP_PriceFinal = CInt(FCP_ValuationRow("PriceFinal"))
							newPortRecRow.FCP_PriceAdministrator = CInt(FCP_ValuationRow("PriceIsAdministrator"))
							newPortRecRow.FCP_PriceDate = CDate(FCP_ValuationRow("PriceDate"))
							newPortRecRow.PFPC_Price = 0
							newPortRecRow.Price_Difference = CDbl(FCP_ValuationRow("PriceLevel"))
							newPortRecRow.FCP_FXRate = CDbl(FCP_ValuationRow("InstrumentFXRate"))
							newPortRecRow.PFPC_FXRate = 0
							newPortRecRow.FXRate_Difference = CDbl(FCP_ValuationRow("InstrumentFXRate")) '
							newPortRecRow.FCP_LocalCcyValue = Local_Value
							newPortRecRow.PFPC_LocalCcyValue = 0
							newPortRecRow.LocalCcyValue_Difference = Local_Value ' 
							newPortRecRow.FCP_BookCcyValue = CDbl(FCP_ValuationRow("Value"))
							newPortRecRow.PFPC_BookCcyValue = 0
							newPortRecRow.BookCcyValue_Difference = CDbl(FCP_ValuationRow("Value"))

							Tbl_PortfolioRec.Rows.Add(newPortRecRow)

						Catch ex As Exception
							Call MainForm.LogError(Me.Name & ", ReconcilePortfolioValuation()", LOG_LEVELS.Error, ex.Message, "Error saving reconciliation Row.", ex.StackTrace, True)
							GoTo ReconcilePortfolioValuation_Exit
						End Try

						FCP_ValuationCounter += 1

					ElseIf (FCP_PFPCID > PFPC_PFPCID) Then
						' IDs Present, PFPCRS is less
						Temp_FCPInstrumentID = 0

						' Kludge for Brian.
						If PFPC_ValuationRow.InstrumentID.ToUpper.StartsWith("USD") Then
							Temp_FCPInstrumentID = 1
						ElseIf PFPC_ValuationRow.InstrumentID.ToUpper.StartsWith("GBP") Then
							Temp_FCPInstrumentID = 2
						ElseIf PFPC_ValuationRow.InstrumentID.ToUpper.StartsWith("EUR") Then
							Temp_FCPInstrumentID = 3
						End If

						Try
							newPortRecRow = Tbl_PortfolioRec.NewtblPFPCPortfolioReconcilliationRow
							newPortRecRow.ValuationDate = p_ReportDate
							newPortRecRow.FundID = p_FundID
							newPortRecRow.FCPInstrumentID = Temp_FCPInstrumentID
							newPortRecRow.PFPCInstrumentID = PFPC_ValuationRow.InstrumentID
							newPortRecRow.PFPCInstrumentDescription = PFPC_ValuationRow.InstrumentDescription
							newPortRecRow.FCP_Holding = 0
							newPortRecRow.FCP_HoldingCompleteness = 0
							newPortRecRow.PFPC_Holding = PFPC_ValuationRow.Amount
							newPortRecRow.Holding_Difference = PFPC_ValuationRow.Amount
							newPortRecRow.FCP_Price = 0
							newPortRecRow.FCP_PriceFinal = 0
							newPortRecRow.FCP_PriceAdministrator = 0
							newPortRecRow.FCP_PriceDate = #1/1/1900#
							newPortRecRow.PFPC_Price = PFPC_ValuationRow.LocalCcyPrice
							newPortRecRow.Price_Difference = PFPC_ValuationRow.LocalCcyPrice
							newPortRecRow.FCP_FXRate = 0
							newPortRecRow.PFPC_FXRate = PFPC_ValuationRow.FxRate
							newPortRecRow.FXRate_Difference = PFPC_ValuationRow.FxRate
							newPortRecRow.FCP_LocalCcyValue = 0
							newPortRecRow.PFPC_LocalCcyValue = PFPC_ValuationRow.LocalCcyValue
							newPortRecRow.LocalCcyValue_Difference = -(PFPC_ValuationRow.LocalCcyValue)
							newPortRecRow.FCP_BookCcyValue = 0
							newPortRecRow.PFPC_BookCcyValue = PFPC_ValuationRow.BookCcyValue
							newPortRecRow.BookCcyValue_Difference = -(PFPC_ValuationRow.BookCcyValue)

							Tbl_PortfolioRec.Rows.Add(newPortRecRow)

						Catch ex As Exception
							Call MainForm.LogError(Me.Name & ", ReconcilePortfolioValuation()", LOG_LEVELS.Error, ex.Message, "Error saving reconciliation Row.", ex.StackTrace, True)
							GoTo ReconcilePortfolioValuation_Exit
						End Try

						PFPC_ValuationCounter += 1
					End If

				End If

				'FCP_ValuationCounter += 1
				'PFPC_ValuationCounter += 1
			End While

		Catch ex As Exception
			Call MainForm.LogError(Me.Name & ", ReconcilePortfolioValuation()", LOG_LEVELS.Error, ex.Message, "Error calculating Portfolio reconciliation records", ex.StackTrace, True)
			GoTo ReconcilePortfolioValuation_Exit
		End Try

		Try
			PortRecAdaptor.Update(Tbl_PortfolioRec)
		Catch ex As Exception
			Call MainForm.LogError(Me.Name & ", ReconcilePortfolioValuation()", LOG_LEVELS.Error, ex.Message, "Error saving Portfolio reconciliation records", ex.StackTrace, True)
			GoTo ReconcilePortfolioValuation_Exit
		End Try

		RVal = True

ReconcilePortfolioValuation_exit:

		' Tidy Up.

		Try
			tblFCP_Portfolio = Nothing
			FCP_Valuation_Rows = Nothing
			FCP_ValuationRow = Nothing

			DS_PFPC_Valuation = Nothing
			tblPFPC_Valuation = Nothing
			PFPC_Valuation_Rows = Nothing
			PFPC_ValuationRow = Nothing

			DS_PortfolioRec = Nothing
			Tbl_PortfolioRec = Nothing
			PortRecAdaptor = Nothing
		Catch ex As Exception
		End Try

		Return RVal
	End Function

    ''' <summary>
    ''' Saves to portfolio reconcilliation.
    ''' </summary>
    ''' <param name="pTbl_PortRec">The p TBL_ port rec.</param>
    ''' <param name="pValuationDate">The p valuation date.</param>
    ''' <param name="pFundID">The p fund ID.</param>
    ''' <param name="pFundName">Name of the p fund.</param>
    ''' <param name="pFCP_InstrumentID">The p FC p_ instrument ID.</param>
    ''' <param name="pPFPCInstrumentID">The p PFPC instrument ID.</param>
    ''' <param name="pPFPCInstrumentDescription">The p PFPC instrument description.</param>
    ''' <param name="pFCP_Holding">The p FC p_ holding.</param>
    ''' <param name="pFCP_HoldingCompleteness">The p FC p_ holding completeness.</param>
    ''' <param name="pPFPC_Holding">The p PFP c_ holding.</param>
    ''' <param name="pHolding_Difference">The p holding_ difference.</param>
    ''' <param name="pFCP_Price">The p FC p_ price.</param>
    ''' <param name="pFCP_PriceFinal">The p FC p_ price final.</param>
    ''' <param name="pFCP_PriceAdministrator">The p FC p_ price administrator.</param>
    ''' <param name="pFCP_PriceDate">The p FC p_ price date.</param>
    ''' <param name="pPFPC_Price">The p PFP c_ price.</param>
    ''' <param name="pPrice_Difference">The p price_ difference.</param>
    ''' <param name="pFCP_FXRate">The p FC p_ FX rate.</param>
    ''' <param name="pPFPC_FXRate">The p PFP c_ FX rate.</param>
    ''' <param name="pFXRate_Difference">The p FX rate_ difference.</param>
    ''' <param name="pFCP_LocalCcyValue">The p FC p_ local ccy value.</param>
    ''' <param name="pPFPC_LocalCcyValue">The p PFP c_ local ccy value.</param>
    ''' <param name="pLocalCcyValue_Difference">The p local ccy value_ difference.</param>
    ''' <param name="pFCP_BookCcyValue">The p FC p_ book ccy value.</param>
    ''' <param name="pPFPC_BookCcyValue">The p PFP c_ book ccy value.</param>
    ''' <param name="pBookCcyValue_Difference">The p book ccy value_ difference.</param>
    ''' <returns>System.Int64.</returns>
	Private Function SaveToPortfolioReconcilliation( _
		ByRef pTbl_PortRec As RenaissanceDataClass.DSPFPCPortfolioReconcilliation.tblPFPCPortfolioReconcilliationDataTable, _
		ByVal pValuationDate As Date, ByVal pFundID As Long, ByVal pFundName As String, _
		ByVal pFCP_InstrumentID As Long, ByVal pPFPCInstrumentID As String, _
		ByVal pPFPCInstrumentDescription As String, _
		ByVal pFCP_Holding As Double, ByVal pFCP_HoldingCompleteness As Double, ByVal pPFPC_Holding As Double, ByVal pHolding_Difference As Double, _
		ByVal pFCP_Price As Double, ByVal pFCP_PriceFinal As Long, ByVal pFCP_PriceAdministrator As Long, ByVal pFCP_PriceDate As Date, ByVal pPFPC_Price As Double, ByVal pPrice_Difference As Double, _
		ByVal pFCP_FXRate As Double, ByVal pPFPC_FXRate As Double, ByVal pFXRate_Difference As Double, _
		ByVal pFCP_LocalCcyValue As Double, ByVal pPFPC_LocalCcyValue As Double, ByVal pLocalCcyValue_Difference As Double, _
		ByVal pFCP_BookCcyValue As Double, ByVal pPFPC_BookCcyValue As Double, ByVal pBookCcyValue_Difference As Double) As Long

		' ********************************************************************************************
		' Purpose:  To add a record to the tblPFPCPortfolioReconcilliation table.
		'
		'
		' ********************************************************************************************

		Dim newPortRecRow As RenaissanceDataClass.DSPFPCPortfolioReconcilliation.tblPFPCPortfolioReconcilliationRow

		newPortRecRow = pTbl_PortRec.NewtblPFPCPortfolioReconcilliationRow
		newPortRecRow.ValuationDate = pValuationDate
		newPortRecRow.FundID = pFundID
		newPortRecRow.FCPInstrumentID = pFCP_InstrumentID
		newPortRecRow.PFPCInstrumentID = pPFPCInstrumentID
		newPortRecRow.PFPCInstrumentDescription = pPFPCInstrumentDescription
		newPortRecRow.FCP_Holding = pFCP_Holding
		newPortRecRow.FCP_HoldingCompleteness = pFCP_HoldingCompleteness
		newPortRecRow.PFPC_Holding = pPFPC_Holding
		newPortRecRow.Holding_Difference = pHolding_Difference
		newPortRecRow.FCP_Price = pFCP_Price
		newPortRecRow.FCP_PriceFinal = pFCP_PriceFinal
		newPortRecRow.FCP_PriceAdministrator = pFCP_PriceAdministrator
		newPortRecRow.FCP_PriceDate = pFCP_PriceDate
		newPortRecRow.PFPC_Price = pPFPC_Price
		newPortRecRow.Price_Difference = pPrice_Difference
		newPortRecRow.FCP_FXRate = pFCP_FXRate
		newPortRecRow.PFPC_FXRate = pPFPC_FXRate
		newPortRecRow.FXRate_Difference = pFXRate_Difference
		newPortRecRow.FCP_LocalCcyValue = pFCP_LocalCcyValue
		newPortRecRow.PFPC_LocalCcyValue = pPFPC_LocalCcyValue
		newPortRecRow.LocalCcyValue_Difference = pLocalCcyValue_Difference
		newPortRecRow.FCP_BookCcyValue = pFCP_BookCcyValue
		newPortRecRow.PFPC_BookCcyValue = pPFPC_BookCcyValue
		newPortRecRow.BookCcyValue_Difference = pBookCcyValue_Difference

		pTbl_PortRec.Rows.Add(newPortRecRow)

	End Function

#End Region

End Class
