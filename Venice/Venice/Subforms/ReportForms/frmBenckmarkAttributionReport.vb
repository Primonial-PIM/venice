' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmBenckmarkAttributionReport.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals

Imports RenaissanceUtilities.DatePeriodFunctions

''' <summary>
''' Class frmBenckmarkAttributionReport
''' </summary>
Public Class frmBenckmarkAttributionReport

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmBenckmarkAttributionReport"/> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The BTN run report
    ''' </summary>
  Friend WithEvents btnRunReport As System.Windows.Forms.Button
    ''' <summary>
    ''' The date_ start date
    ''' </summary>
  Friend WithEvents Date_StartDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The label4
    ''' </summary>
  Friend WithEvents Label4 As System.Windows.Forms.Label
    ''' <summary>
    ''' The date_ end date
    ''' </summary>
  Friend WithEvents Date_EndDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The label5
    ''' </summary>
  Friend WithEvents Label5 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ benchmark price series
    ''' </summary>
	Friend WithEvents Combo_BenchmarkPriceSeries As FCP_TelerikControls.FCP_RadComboBox
    ''' <summary>
    ''' The label1
    ''' </summary>
  Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ index portfolio
    ''' </summary>
  Friend WithEvents Combo_IndexPortfolio As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label2
    ''' </summary>
  Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ fund name
    ''' </summary>
  Friend WithEvents Combo_FundName As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label6
    ''' </summary>
  Friend WithEvents Label6 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ FX forward costs
    ''' </summary>
  Friend WithEvents Combo_FXForwardCosts As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label7
    ''' </summary>
  Friend WithEvents Label7 As System.Windows.Forms.Label
    ''' <summary>
    ''' The panel1
    ''' </summary>
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The radio_ pertrac series
    ''' </summary>
  Friend WithEvents Radio_PertracSeries As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The radio_ venice series
    ''' </summary>
  Friend WithEvents Radio_VeniceSeries As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The check_ lookthrough attributions
    ''' </summary>
  Friend WithEvents Check_LookthroughAttributions As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The combo_ attribution period
    ''' </summary>
	Friend WithEvents Combo_AttributionPeriod As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label3
    ''' </summary>
	Friend WithEvents Label3 As System.Windows.Forms.Label
    ''' <summary>
    ''' The form_ status strip
    ''' </summary>
	Friend WithEvents Form_StatusStrip As System.Windows.Forms.StatusStrip
    ''' <summary>
    ''' The form_ progress bar
    ''' </summary>
	Friend WithEvents Form_ProgressBar As System.Windows.Forms.ToolStripProgressBar
    ''' <summary>
    ''' The label_ status
    ''' </summary>
	Friend WithEvents Label_Status As System.Windows.Forms.ToolStripStatusLabel
    ''' <summary>
    ''' The BTN close
    ''' </summary>
	Friend WithEvents btnClose As System.Windows.Forms.Button
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Me.btnRunReport = New System.Windows.Forms.Button
		Me.btnClose = New System.Windows.Forms.Button
		Me.Date_StartDate = New System.Windows.Forms.DateTimePicker
		Me.Label4 = New System.Windows.Forms.Label
		Me.Date_EndDate = New System.Windows.Forms.DateTimePicker
		Me.Label5 = New System.Windows.Forms.Label
		Me.Combo_BenchmarkPriceSeries = New FCP_TelerikControls.FCP_RadComboBox
		Me.Label1 = New System.Windows.Forms.Label
		Me.Combo_IndexPortfolio = New System.Windows.Forms.ComboBox
		Me.Label2 = New System.Windows.Forms.Label
		Me.Combo_FundName = New System.Windows.Forms.ComboBox
		Me.Label6 = New System.Windows.Forms.Label
		Me.Combo_FXForwardCosts = New System.Windows.Forms.ComboBox
		Me.Label7 = New System.Windows.Forms.Label
		Me.Panel1 = New System.Windows.Forms.Panel
		Me.Radio_PertracSeries = New System.Windows.Forms.RadioButton
		Me.Radio_VeniceSeries = New System.Windows.Forms.RadioButton
		Me.Check_LookthroughAttributions = New System.Windows.Forms.CheckBox
		Me.Combo_AttributionPeriod = New System.Windows.Forms.ComboBox
		Me.Label3 = New System.Windows.Forms.Label
		Me.Form_StatusStrip = New System.Windows.Forms.StatusStrip
		Me.Form_ProgressBar = New System.Windows.Forms.ToolStripProgressBar
		Me.Label_Status = New System.Windows.Forms.ToolStripStatusLabel
		Me.Panel1.SuspendLayout()
		Me.Form_StatusStrip.SuspendLayout()
		Me.SuspendLayout()
		'
		'btnRunReport
		'
		Me.btnRunReport.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnRunReport.Location = New System.Drawing.Point(58, 265)
		Me.btnRunReport.Name = "btnRunReport"
		Me.btnRunReport.Size = New System.Drawing.Size(148, 28)
		Me.btnRunReport.TabIndex = 10
		Me.btnRunReport.Text = "Run Report"
		'
		'btnClose
		'
		Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnClose.Location = New System.Drawing.Point(274, 265)
		Me.btnClose.Name = "btnClose"
		Me.btnClose.Size = New System.Drawing.Size(75, 28)
		Me.btnClose.TabIndex = 11
		Me.btnClose.Text = "&Close"
		'
		'Date_StartDate
		'
		Me.Date_StartDate.Location = New System.Drawing.Point(142, 194)
		Me.Date_StartDate.Name = "Date_StartDate"
		Me.Date_StartDate.Size = New System.Drawing.Size(160, 20)
		Me.Date_StartDate.TabIndex = 7
		'
		'Label4
		'
		Me.Label4.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label4.Location = New System.Drawing.Point(3, 198)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(100, 16)
		Me.Label4.TabIndex = 110
		Me.Label4.Text = "Start Date"
		'
		'Date_EndDate
		'
		Me.Date_EndDate.Location = New System.Drawing.Point(142, 220)
		Me.Date_EndDate.Name = "Date_EndDate"
		Me.Date_EndDate.Size = New System.Drawing.Size(160, 20)
		Me.Date_EndDate.TabIndex = 8
		'
		'Label5
		'
		Me.Label5.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label5.Location = New System.Drawing.Point(3, 224)
		Me.Label5.Name = "Label5"
		Me.Label5.Size = New System.Drawing.Size(100, 16)
		Me.Label5.TabIndex = 112
		Me.Label5.Text = "End Date"
		'
		'Combo_BenchmarkPriceSeries
		'
		Me.Combo_BenchmarkPriceSeries.Location = New System.Drawing.Point(142, 35)
		Me.Combo_BenchmarkPriceSeries.Name = "Combo_BenchmarkPriceSeries"
		'
		'
		'
		Me.Combo_BenchmarkPriceSeries.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
		Me.Combo_BenchmarkPriceSeries.SelectNoMatch = False
		Me.Combo_BenchmarkPriceSeries.Size = New System.Drawing.Size(252, 21)
		Me.Combo_BenchmarkPriceSeries.TabIndex = 1
		Me.Combo_BenchmarkPriceSeries.ThemeName = "ControlDefault"
		'
		'Label1
		'
		Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label1.Location = New System.Drawing.Point(3, 39)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(138, 20)
		Me.Label1.TabIndex = 115
		Me.Label1.Text = "Benchmark Price Series"
		'
		'Combo_IndexPortfolio
		'
		Me.Combo_IndexPortfolio.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_IndexPortfolio.Location = New System.Drawing.Point(142, 8)
		Me.Combo_IndexPortfolio.Name = "Combo_IndexPortfolio"
		Me.Combo_IndexPortfolio.Size = New System.Drawing.Size(252, 21)
		Me.Combo_IndexPortfolio.TabIndex = 0
		'
		'Label2
		'
		Me.Label2.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label2.Location = New System.Drawing.Point(3, 12)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(120, 20)
		Me.Label2.TabIndex = 117
		Me.Label2.Text = "Index Portfolio"
		'
		'Combo_FundName
		'
		Me.Combo_FundName.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_FundName.Location = New System.Drawing.Point(142, 89)
		Me.Combo_FundName.Name = "Combo_FundName"
		Me.Combo_FundName.Size = New System.Drawing.Size(252, 21)
		Me.Combo_FundName.TabIndex = 3
		'
		'Label6
		'
		Me.Label6.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label6.Location = New System.Drawing.Point(3, 93)
		Me.Label6.Name = "Label6"
		Me.Label6.Size = New System.Drawing.Size(120, 20)
		Me.Label6.TabIndex = 119
		Me.Label6.Text = "Fund Name"
		'
		'Combo_FXForwardCosts
		'
		Me.Combo_FXForwardCosts.Enabled = False
		Me.Combo_FXForwardCosts.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_FXForwardCosts.Location = New System.Drawing.Point(142, 140)
		Me.Combo_FXForwardCosts.Name = "Combo_FXForwardCosts"
		Me.Combo_FXForwardCosts.Size = New System.Drawing.Size(252, 21)
		Me.Combo_FXForwardCosts.TabIndex = 5
		'
		'Label7
		'
		Me.Label7.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label7.Location = New System.Drawing.Point(3, 144)
		Me.Label7.Name = "Label7"
		Me.Label7.Size = New System.Drawing.Size(148, 20)
		Me.Label7.TabIndex = 121
		Me.Label7.Text = "Currency Forward Costs (%)"
		'
		'Panel1
		'
		Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Panel1.Controls.Add(Me.Radio_PertracSeries)
		Me.Panel1.Controls.Add(Me.Radio_VeniceSeries)
		Me.Panel1.Location = New System.Drawing.Point(142, 56)
		Me.Panel1.Name = "Panel1"
		Me.Panel1.Size = New System.Drawing.Size(251, 24)
		Me.Panel1.TabIndex = 2
		'
		'Radio_PertracSeries
		'
		Me.Radio_PertracSeries.AutoSize = True
		Me.Radio_PertracSeries.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Radio_PertracSeries.Location = New System.Drawing.Point(142, 3)
		Me.Radio_PertracSeries.Name = "Radio_PertracSeries"
		Me.Radio_PertracSeries.Size = New System.Drawing.Size(97, 18)
		Me.Radio_PertracSeries.TabIndex = 1
		Me.Radio_PertracSeries.Text = "Pertrac Series"
		Me.Radio_PertracSeries.UseVisualStyleBackColor = True
		'
		'Radio_VeniceSeries
		'
		Me.Radio_VeniceSeries.AutoSize = True
		Me.Radio_VeniceSeries.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Radio_VeniceSeries.Location = New System.Drawing.Point(12, 3)
		Me.Radio_VeniceSeries.Name = "Radio_VeniceSeries"
		Me.Radio_VeniceSeries.Size = New System.Drawing.Size(116, 18)
		Me.Radio_VeniceSeries.TabIndex = 0
		Me.Radio_VeniceSeries.Text = "Venice Instrument"
		Me.Radio_VeniceSeries.UseVisualStyleBackColor = True
		'
		'Check_LookthroughAttributions
		'
		Me.Check_LookthroughAttributions.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Check_LookthroughAttributions.Location = New System.Drawing.Point(142, 116)
		Me.Check_LookthroughAttributions.Name = "Check_LookthroughAttributions"
		Me.Check_LookthroughAttributions.Size = New System.Drawing.Size(252, 20)
		Me.Check_LookthroughAttributions.TabIndex = 4
		Me.Check_LookthroughAttributions.Text = "Report on Lookthrough Attributions"
		'
		'Combo_AttributionPeriod
		'
		Me.Combo_AttributionPeriod.Enabled = False
		Me.Combo_AttributionPeriod.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_AttributionPeriod.Location = New System.Drawing.Point(142, 167)
		Me.Combo_AttributionPeriod.Name = "Combo_AttributionPeriod"
		Me.Combo_AttributionPeriod.Size = New System.Drawing.Size(252, 21)
		Me.Combo_AttributionPeriod.TabIndex = 6
		'
		'Label3
		'
		Me.Label3.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label3.Location = New System.Drawing.Point(3, 171)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(120, 20)
		Me.Label3.TabIndex = 124
		Me.Label3.Text = "Attribution Period"
		'
		'Form_StatusStrip
		'
		Me.Form_StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Form_ProgressBar, Me.Label_Status})
		Me.Form_StatusStrip.Location = New System.Drawing.Point(0, 306)
		Me.Form_StatusStrip.Name = "Form_StatusStrip"
		Me.Form_StatusStrip.Size = New System.Drawing.Size(401, 22)
		Me.Form_StatusStrip.TabIndex = 125
		Me.Form_StatusStrip.Text = " "
		'
		'Form_ProgressBar
		'
		Me.Form_ProgressBar.Maximum = 20
		Me.Form_ProgressBar.Name = "Form_ProgressBar"
		Me.Form_ProgressBar.Size = New System.Drawing.Size(150, 17)
		Me.Form_ProgressBar.Step = 1
		Me.Form_ProgressBar.Visible = False
		'
		'Label_Status
		'
		Me.Label_Status.Name = "Label_Status"
		Me.Label_Status.Size = New System.Drawing.Size(10, 17)
		Me.Label_Status.Text = " "
		'
		'frmBenckmarkAttributionReport
		'
		Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
		Me.ClientSize = New System.Drawing.Size(401, 328)
		Me.Controls.Add(Me.Form_StatusStrip)
		Me.Controls.Add(Me.Combo_AttributionPeriod)
		Me.Controls.Add(Me.Label3)
		Me.Controls.Add(Me.Check_LookthroughAttributions)
		Me.Controls.Add(Me.Panel1)
		Me.Controls.Add(Me.Combo_FXForwardCosts)
		Me.Controls.Add(Me.Combo_FundName)
		Me.Controls.Add(Me.Label6)
		Me.Controls.Add(Me.Combo_IndexPortfolio)
		Me.Controls.Add(Me.Label2)
		Me.Controls.Add(Me.Combo_BenchmarkPriceSeries)
		Me.Controls.Add(Me.Label1)
		Me.Controls.Add(Me.Date_EndDate)
		Me.Controls.Add(Me.Label5)
		Me.Controls.Add(Me.Date_StartDate)
		Me.Controls.Add(Me.Label4)
		Me.Controls.Add(Me.btnClose)
		Me.Controls.Add(Me.btnRunReport)
		Me.Controls.Add(Me.Label7)
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Name = "frmBenckmarkAttributionReport"
		Me.Text = "Benchmark Attribution report"
		Me.Panel1.ResumeLayout(False)
		Me.Panel1.PerformLayout()
		Me.Form_StatusStrip.ResumeLayout(False)
		Me.Form_StatusStrip.PerformLayout()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

#End Region

#Region " Form Locals and Constants "


	' Form 'Parent', the Main Venice form.
	' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
	Private WithEvents _MainForm As VeniceMain

	' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
	Private FormTooltip As New ToolTip()

    ''' <summary>
    ''' The report worker
    ''' </summary>
	Private ReportWorker As BackgroundWorkerThreadSafe = Nothing
    ''' <summary>
    ''' The form controls
    ''' </summary>
	Private FormControls As ArrayList = Nothing
    ''' <summary>
    ''' The report timer
    ''' </summary>
	Private WithEvents ReportTimer As New Windows.Forms.Timer()

	' Form Constants, specific to the table being updated.

    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
	Private ALWAYS_CLOSE_THIS_FORM As Boolean = True

	' The standard ChangeID for this form. e.g. tblPerson
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
	Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

	' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
	Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
	Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

	' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
	Private THIS_FORM_FormID As VeniceFormID

	' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
	Private FormIsValid As Boolean
    ''' <summary>
    ''' The form changed
    ''' </summary>
	Private FormChanged As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
	Private _FormOpenFailed As Boolean

	' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
	Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
	Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
	Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
	Private HasDeletePermission As Boolean

#End Region

    ''' <summary>
    ''' The global benchmark attribution results
    ''' </summary>
	Dim GlobalBenchmarkAttributionResults As DataTable

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
	Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
	Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return False
		End Get
		Set(ByVal Value As Boolean)
		End Set
	End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
	Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
		Get
			Return False
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
		Get
			Return True
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmBenckmarkAttributionReport"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
	Public Sub New(ByVal pMainForm As VeniceMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()

		_MainForm = pMainForm
		AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate
		AddHandler ReportTimer.Tick, AddressOf MainForm.IncrementStatusBarTimerEvent

		_FormOpenFailed = False

		' ******************************************************
		' Form Specific Settings :
		' ******************************************************

		' Form Permissioning :-

		THIS_FORM_PermissionArea = Me.Name
		THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

		' 'This' form ID

		THIS_FORM_FormID = VeniceFormID.frmBenckmarkAttributionReport

		' Format Event Handlers for form controls

		AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

		'AddHandler Combo_BenchmarkPriceSeries.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_BenchmarkPriceSeries.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		'AddHandler Combo_BenchmarkPriceSeries.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		'AddHandler Combo_BenchmarkPriceSeries.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

		AddHandler Combo_IndexPortfolio.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_IndexPortfolio.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_IndexPortfolio.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_IndexPortfolio.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

		AddHandler Combo_FundName.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_FundName.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_FundName.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_FundName.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

		AddHandler Combo_FXForwardCosts.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_FXForwardCosts.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_FXForwardCosts.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_FXForwardCosts.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

		' ******************************************************
		' End Form Specific.
		' ******************************************************

		THIS_FORM_ChangeID = RenaissanceChangeID.KnowledgeDate

	End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

	' Form Initialisation code.
	'
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
	Public Sub ResetForm() Implements StandardVeniceForm.ResetForm

		Call Form_Load(Me, New System.EventArgs)
	End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
	Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub

    ''' <summary>
    ''' Handles the FormClosing event of the frmBenckmarkAttributionReport control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.FormClosingEventArgs"/> instance containing the event data.</param>
	Private Sub frmBenckmarkAttributionReport_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
		If (ReportWorker IsNot Nothing) Then
			If ReportWorker.IsBusy Then
				e.Cancel = True
				Exit Sub
			End If

			Try
				RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

				RemoveHandler Combo_BenchmarkPriceSeries.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_BenchmarkPriceSeries.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_BenchmarkPriceSeries.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_BenchmarkPriceSeries.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

				RemoveHandler Combo_IndexPortfolio.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_IndexPortfolio.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_IndexPortfolio.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_IndexPortfolio.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

				RemoveHandler Combo_FundName.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_FundName.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_FundName.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_FundName.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

				RemoveHandler Combo_FXForwardCosts.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_FXForwardCosts.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_FXForwardCosts.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_FXForwardCosts.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

			Catch ex As Exception
			End Try

			Try
				RemoveHandler ReportTimer.Tick, AddressOf MainForm.IncrementStatusBarTimerEvent
				RemoveHandler ReportWorker.DoWork, AddressOf ReportWorker_DoWork
				RemoveHandler ReportWorker.RunWorkerCompleted, AddressOf ReportWorkerCompleted
				ReportWorker.Dispose()
				ReportWorker = Nothing
			Catch ex As Exception
			End Try
		End If

	End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		' *******************************************************************
		' Initialise form.
		'
		' *******************************************************************

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
		_FormOpenFailed = False

		If Not (MainForm Is Nothing) Then
			FormIsValid = True
		Else
			MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If


		' Check User permissions
		Call CheckPermissions()
		If (HasReadPermission = False) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		' Build Combos

		Try

			If (Me.Radio_VeniceSeries.Checked = True) Then
				Call Me.SetBenchmarkPriceSeriesIndex()
			Else
				Me.Radio_PertracSeries.Checked = True
			End If
			Call Me.SetIndexPortfolioCombo()
			Call Me.SetFundNameCombo()
			Call Me.SetCurrencyForwardCombo()
			Call Me.SetAttributionPeriodCombo()

			MainForm.SetComboSelectionLengths(Me)

			Me.Date_StartDate.Value = CDate("1 Jan 2005")
			Me.Date_EndDate.Value = Now.Date.AddDays(0 - Now.Date.Day)

		Catch ex As Exception
		End Try

		Me.Combo_AttributionPeriod.SelectedValue = RenaissanceGlobals.DealingPeriod.Monthly

		Me.Combo_IndexPortfolio.Focus()

	End Sub

    ''' <summary>
    ''' Handles the Closing event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
	Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		' *******************************************************************
		' Close (or hide) this form. Remove handlers and tidy up as necessary.
		'
		' *******************************************************************

		Dim HideForm As Boolean

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...


		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else
			HideForm = True
			If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.ReportForm_CacheCount Then
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			e.Cancel = True
		Else
			Try
				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

				RemoveHandler Combo_BenchmarkPriceSeries.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_BenchmarkPriceSeries.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_BenchmarkPriceSeries.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_BenchmarkPriceSeries.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

				RemoveHandler Combo_IndexPortfolio.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_IndexPortfolio.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_IndexPortfolio.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_IndexPortfolio.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

				RemoveHandler Combo_FundName.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_FundName.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_FundName.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_FundName.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

				RemoveHandler Combo_FXForwardCosts.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_FXForwardCosts.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_FXForwardCosts.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_FXForwardCosts.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

			Catch ex As Exception
			End Try
		End If

	End Sub


#End Region

    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		' *******************************************************************
		' Routine to handle changes / updates to tables by this and other windows.
		' If this, or any other, form posts a change to a table, then it will invoke an update event 
		' detailing what tables have been altered.
		' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
		' the 'VeniceAutoUpdate' event of the main Venice form.
		' Each form may them react as appropriate to changes in any table that might impact it.
		'
		' *******************************************************************

		Dim KnowledgeDateChanged As Boolean
		Dim SetButtonStatus_Flag As Boolean

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		KnowledgeDateChanged = False
		SetButtonStatus_Flag = False

		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
			KnowledgeDateChanged = True
		End If

		' ****************************************************************
		' Check for changes relevant to this form
		' ****************************************************************

		' Changes to the KnowledgeDate :-
		If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
			SetButtonStatus_Flag = True
		End If


		' Changes to the tblFund table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFund) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			Call SetFundNameCombo()
		End If

		' Changes to the tblInstrument table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrument) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			If Me.Radio_VeniceSeries.Checked = True Then
				Call Me.SetBenchmarkPriceSeriesIndex()
			End If
			Call Me.SetCurrencyForwardCombo()

		End If

		' Changes to the tblPortfolioIndex table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblPortfolioIndex) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			Call Me.SetIndexPortfolioCombo()
		End If

		' Changes to the tblUserPermissions table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

			' Check ongoing permissions.

			Call CheckPermissions()
			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

				FormIsValid = False
				Me.Close()
				Exit Sub
			End If

			SetButtonStatus_Flag = True

		End If

		' ****************************************************************
		'
		' ****************************************************************


	End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

	' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
	Private Sub CheckPermissions()
		' *****************************************************************************
		'
		' *****************************************************************************

		Dim Permissions As Integer

		Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

		HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
		HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
		HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
		HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

	End Sub


#End Region

#Region " Set Form Combos (Form Specific Code) "

    ''' <summary>
    ''' Sets the index of the benchmark price series.
    ''' </summary>
	Private Sub SetBenchmarkPriceSeriesIndex()

		If (MainForm Is Nothing) Then Exit Sub


		If Me.Radio_PertracSeries.Checked = True Then

			Try
				Combo_BenchmarkPriceSeries.SelectedValue = Nothing
				Combo_BenchmarkPriceSeries.DataSource = Nothing
				Combo_BenchmarkPriceSeries.ValueMember = ""
				Combo_BenchmarkPriceSeries.DisplayMember = ""
			Catch ex As Exception
			End Try

			Combo_BenchmarkPriceSeries.MasternameCollection = MainForm.MasternameDictionary
			Combo_BenchmarkPriceSeries.SelectNoMatch = False

		Else
			Combo_BenchmarkPriceSeries.SelectedValue = Nothing
			Combo_BenchmarkPriceSeries.MasternameCollection = Nothing

			Call MainForm.SetTblGenericCombo( _
			Me.Combo_BenchmarkPriceSeries, _
			RenaissanceStandardDatasets.tblInstrument, _
			"InstrumentDescription", _
			"InstrumentID", _
			"", False, True, True, 0)	 ' 
		End If

	End Sub


    ''' <summary>
    ''' Sets the index portfolio combo.
    ''' </summary>
	Private Sub SetIndexPortfolioCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_IndexPortfolio, _
		RenaissanceStandardDatasets.tblPortfolioIndex, _
		"PortfolioDescription", _
		"PortfolioTicker", _
		"PortfolioType=" & CInt(RenaissanceGlobals.PortfolioType.IndexWeighting).ToString, True, True, False)		' 

	End Sub


    ''' <summary>
    ''' Sets the fund name combo.
    ''' </summary>
	Private Sub SetFundNameCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_FundName, _
		RenaissanceStandardDatasets.tblFund, _
		"FundName", _
		"FundID", _
		"", False, True)	' 

	End Sub


    ''' <summary>
    ''' Sets the currency forward combo.
    ''' </summary>
	Private Sub SetCurrencyForwardCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_FXForwardCosts, _
		RenaissanceStandardDatasets.tblInstrument, _
		"InstrumentDescription", _
		"InstrumentID", _
		"", False, True, True, 0)		' 

	End Sub

    ''' <summary>
    ''' Sets the attribution period combo.
    ''' </summary>
	Private Sub SetAttributionPeriodCombo()
		Call MainForm.SetTblGenericCombo( _
		Me.Combo_AttributionPeriod, GetType(RenaissanceGlobals.DealingPeriod))
	End Sub


#End Region

#Region " SetButton / Control Events (Form Specific Code) "

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_Fund control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_Fund_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

	End Sub


#End Region

#Region " Form Control Event Code"

    ''' <summary>
    ''' Handles the CheckedChanged event of the Radio_VeniceSeries control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Radio_VeniceSeries_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_VeniceSeries.CheckedChanged
		Try
			If (Radio_VeniceSeries.Checked) Then
				Call Me.SetBenchmarkPriceSeriesIndex()
			End If
		Catch ex As Exception
		End Try
	End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the Radio_PertracSeries control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Radio_PertracSeries_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_PertracSeries.CheckedChanged
		Try
			If (Radio_PertracSeries.Checked) Then
				Call Me.SetBenchmarkPriceSeriesIndex()
			End If
		Catch ex As Exception
		End Try
	End Sub

    ''' <summary>
    ''' Handles the ValueChanged event of the Date_StartDate control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Date_StartDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Date_StartDate.ValueChanged
		Dim TempDate As Date

		TempDate = FitDateToPeriod(DealingPeriod.Monthly, Date_StartDate.Value, False)
		If (TempDate.CompareTo(Date_StartDate.Value) <> 0) Then
			Date_StartDate.Value = TempDate
		End If

	End Sub

    ''' <summary>
    ''' Handles the ValueChanged event of the Date_EndDate control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Date_EndDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Date_EndDate.ValueChanged
		Dim TempDate As Date

		TempDate = FitDateToPeriod(DealingPeriod.Monthly, Date_EndDate.Value)
		If (TempDate.CompareTo(Date_EndDate.Value) <> 0) Then
			Date_EndDate.Value = TempDate
		End If

	End Sub
#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

    ''' <summary>
    ''' Handles the Click event of the btnRunReport control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnRunReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRunReport.Click
		' *****************************************************************************
		' Run Report
		'
		' The report functionallity is held within the 'RunReport()' procedure. This is
		' called either by a worker thread or by the interface thread depending on the
		' current user's configuration.
		'
		' *****************************************************************************

		Try
			FormControls = MainForm.MainReportHandler.DisableFormControls(Me)
			Form_ProgressBar.Value = Form_ProgressBar.Minimum
			MainForm.SetToolStripText(Label_Status, "Processing Report")

			If (ReportWorker Is Nothing) Then
				ReportWorker = New BackgroundWorkerThreadSafe(Nothing)
				AddHandler ReportWorker.DoWork, AddressOf ReportWorker_DoWork
				AddHandler ReportWorker.RunWorkerCompleted, AddressOf ReportWorkerCompleted
			End If

			ReportTimer.Interval = 25
			ReportTimer.Tag = Me.Form_ProgressBar
			ReportTimer.Start()


			If MainForm.UseReportWorkerThreads Then
				ReportWorker.RunWorkerAsync()
			Else
				RunReport()
				Call ReportWorkerCompleted(Nothing, Nothing)
			End If


		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
		End Try
	End Sub

    ''' <summary>
    ''' Handles the DoWork event of the ReportWorker control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.DoWorkEventArgs"/> instance containing the event data.</param>
	Private Sub ReportWorker_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs)	' Handles backgroundWorker1.DoWork
		' *******************************************************************
		' Worker thread, Run report.
		' *******************************************************************

		RunReport()
	End Sub

    ''' <summary>
    ''' Reports the worker completed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.RunWorkerCompletedEventArgs"/> instance containing the event data.</param>
	Private Sub ReportWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs)	' Handles backgroundWorker1.RunWorkerCompleted
		' *******************************************************************
		' Reset report form having run the report.
		' *******************************************************************

		Try
			ReportTimer.Stop()
			MainForm.MainReportHandler.EnableFormControls(FormControls)
			MainForm.SetToolStripText(Label_Status, "")

			'Form_ProgressBar = MainForm.ResetProgressBar(Form_StatusStrip, Form_ProgressBar)
			Form_ProgressBar.Value = Form_ProgressBar.Minimum
			Form_ProgressBar.Visible = False
			ReportTimer.Tag = Me.Form_ProgressBar
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
		End Try

	End Sub


    ''' <summary>
    ''' Runs the report.
    ''' </summary>
	Private Sub RunReport()
		' *****************************************************************************
		' Run Benchmark Attribution Report.
		' *****************************************************************************

		Dim BenchmarkSeriesID As Integer = 0
		Dim IndexPortfolioTicker As String = ""
		Dim FundIndex As Integer = 0
		Dim PricingStartDate As Date
		Dim StartDate As Date
		Dim EndDate As Date
		Dim LookthroughAttributionsFlag As Integer = 0
		Dim LatestBenchmarkPortfolio As Date = Renaissance_BaseDate

		Dim PricePeriod As RenaissanceGlobals.DealingPeriod = DealingPeriod.Monthly

		' ***************************************************************************************************************
		' Validate
		' ***************************************************************************************************************

		Try
			BenchmarkSeriesID = 0

			If (Not (MainForm.GetComboSelectedValue(Combo_BenchmarkPriceSeries) Is Nothing)) AndAlso (IsNumeric(MainForm.GetComboSelectedValue(Combo_BenchmarkPriceSeries))) Then
				BenchmarkSeriesID = CInt(MainForm.GetComboSelectedValue(Combo_BenchmarkPriceSeries))
			End If

			If (MainForm.GetComboSelectedValue(Combo_IndexPortfolio) Is Nothing) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Index Portfolio must be selected.", "", True)
				Exit Sub
			End If
			IndexPortfolioTicker = MainForm.GetComboSelectedValue(Combo_IndexPortfolio)

			MainForm.GetComboSelectedValue(Combo_FundName)
			If (MainForm.GetComboSelectedValue(Combo_FundName) Is Nothing) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Fund Name must be selected.", "", True)
				Exit Sub
			End If
			FundIndex = MainForm.GetComboSelectedValue(Combo_FundName)


			If MainForm.GetCheckBoxChecked(Check_LookthroughAttributions) = True Then
				LookthroughAttributionsFlag = (-1)
			Else
				LookthroughAttributionsFlag = 0
			End If

			If MainForm.GetComboSelectedIndex(Combo_AttributionPeriod) >= 0 Then
				PricePeriod = CType(MainForm.GetComboSelectedValue(Combo_AttributionPeriod), RenaissanceGlobals.DealingPeriod)
			End If

			StartDate = FitDateToPeriod(PricePeriod, MainForm.GetDatetimeValue(Date_StartDate).Date, False)	'StartDate.AddDays(1 - StartDate.Day) ' Set to first Day of the month
			PricingStartDate = AddPeriodToDate(PricePeriod, StartDate, -1) 'StartDate.AddMonths(-1)

			EndDate = FitDateToPeriod(PricePeriod, MainForm.GetDatetimeValue(Date_EndDate).Date, True)

			MainForm.SetToolStripText(Label_Status, "")

			If Not (MainForm.InvokeRequired) Then
				Application.DoEvents()
			End If
		Catch ex As Exception

			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Validating Benchmark Index Report form.", ex.StackTrace, True)
			Exit Sub
		End Try

		' ***************************************************************************************************************
		' OK, Gather Data
		' ***************************************************************************************************************

		' 1) Get Benchmark Index Prices

		Dim BenchmarkPriceSeries As New clsInstrumentDetails(PortfolioItemType.Default, 0, MainForm.GetControlText(Combo_BenchmarkPriceSeries), BenchmarkSeriesID) ' clsInstrumentPrice()
		Dim FundPriceSeries As New clsInstrumentDetails(PortfolioItemType.VeniceInstrument, 0, MainForm.GetControlText(Combo_FundName), 0) ' clsInstrumentPrice()
		Dim CurrencyCostPriceSeries As clsInstrumentDetails = Nothing
		Dim CurrencyModifierInUse As Boolean = False

		Try
			' A) Currency Forward Costs

			If (MainForm.GetComboSelectedIndex(Combo_FXForwardCosts) > 0) AndAlso (IsNumeric(MainForm.GetComboSelectedValue(Combo_FXForwardCosts)) = True) Then
				CurrencyCostPriceSeries = New clsInstrumentDetails(PortfolioItemType.VeniceInstrument, 0, MainForm.GetControlText(Combo_FXForwardCosts), 0)
				CurrencyCostPriceSeries.PricingData = GetInstrumentPriceArray(MainForm, PricePeriod, PricingStartDate, EndDate, CInt(MainForm.GetComboSelectedValue(Combo_FXForwardCosts)), MainForm.Main_Knowledgedate)
				CurrencyModifierInUse = True

				Dim LastDate As Date = GetLastInstrumentPriceDate(MainForm, CInt(MainForm.GetComboSelectedValue(Combo_FXForwardCosts)))
				If (LastDate < EndDate) Then
					If MessageBox.Show("The Venice return series for the Currency Costs Instrument ends on " & LastDate.ToString(DISPLAYMEMBER_DATEFORMAT) & vbCrLf & "Do you want to continue (OK) or Cancel ?", "Continue or Cancel ?", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) <> Windows.Forms.DialogResult.OK Then
						Exit Sub
					End If
				End If
			Else
				CurrencyCostPriceSeries = Nothing
			End If

			' B) Benchmark series

			If (BenchmarkSeriesID > 0) Then
				If MainForm.GetRadioChecked(Radio_PertracSeries) Then
					Dim LastDate As Date = GetLastPertracPriceDate(MainForm, BenchmarkSeriesID)

					If (LastDate < EndDate) Then
						If MessageBox.Show("The Pertrac price series for the Benchmark Index ends on " & LastDate.ToString(DISPLAYMEMBER_DATEFORMAT) & vbCrLf & "Do you want to continue (OK) or Cancel ?", "Continue or Cancel ?", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) <> Windows.Forms.DialogResult.OK Then
							Exit Sub
						End If
					End If

					BenchmarkPriceSeries.PricingData = GetPertracPriceArray(MainForm, PricePeriod, PricingStartDate, EndDate, FundIndex, BenchmarkSeriesID, MainForm.Main_Knowledgedate)
				Else
					BenchmarkPriceSeries.PricingData = GetInstrumentPriceArray(MainForm, PricePeriod, PricingStartDate, EndDate, BenchmarkSeriesID, MainForm.Main_Knowledgedate)

					Dim LastDate As Date = GetLastInstrumentPriceDate(MainForm, BenchmarkSeriesID)
					If (LastDate < EndDate) Then
						If MessageBox.Show("The Venice price series for the Benchmark Index ends on " & LastDate.ToString(DISPLAYMEMBER_DATEFORMAT) & vbCrLf & "Do you want to continue (OK) or Cancel ?", "Continue or Cancel ?", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) <> Windows.Forms.DialogResult.OK Then
							Exit Sub
						End If
					End If
				End If
			Else
				BenchmarkPriceSeries.PricingData = Nothing
			End If

			If (CurrencyModifierInUse = True) Then
				BenchmarkPriceSeries.PricingData = AddPriceArrayReturns(BenchmarkPriceSeries.PricingData, CurrencyCostPriceSeries.PricingData, PricePeriod)
			End If

			' C) Get Fund Prices
			' On reflection, we do not want the fund prices because they are not necessarily gross.
			' I think it will be better to synthesise a fund return series from the attributions.

			' Initialise an empty series :-
			Dim TempIndex As Integer
			ReDim FundPriceSeries.PricingData(BenchmarkPriceSeries.PricingData.Length - 1)
			For TempIndex = 0 To (FundPriceSeries.PricingData.Length - 1)
				FundPriceSeries.PricingData(TempIndex) = New clsInstrumentPrice(FitDateToPeriod(PricePeriod, AddPeriodToDate(PricePeriod, PricingStartDate, TempIndex), True), 0, 0)
			Next

		Catch ex As Exception

			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error getting Benchmark Index and Fund price series.", ex.StackTrace, True)
			Exit Sub
		End Try

		' ***************************************************************************************************************
		' Now, what I need to do is work through all applicable versions of the Index portfolio to get a list of unique Index constituents.
		' Assuming that the constituents do not necessarity remain constant.
		' ***************************************************************************************************************

		Dim PortfolioDataIDs As String = ""

		Dim PortfolioIndexDS As RenaissanceDataClass.DSPortfolioIndex
		Dim PortfolioIndexTable As RenaissanceDataClass.DSPortfolioIndex.tblPortfolioIndexDataTable
		Dim SelectedPortfolioIndexRows As RenaissanceDataClass.DSPortfolioIndex.tblPortfolioIndexRow()
		Dim thisPortfolioIndexRow As RenaissanceDataClass.DSPortfolioIndex.tblPortfolioIndexRow

		Dim PortfolioDataDS As RenaissanceDataClass.DSPortfolioData
		Dim PortfolioDataTable As RenaissanceDataClass.DSPortfolioData.tblPortfolioDataDataTable
		Dim SelectedPortfolioDataRows As RenaissanceDataClass.DSPortfolioData.tblPortfolioDataRow()
		Dim thisPortfolioDataRow As RenaissanceDataClass.DSPortfolioData.tblPortfolioDataRow

		Dim InstrumentDS As RenaissanceDataClass.DSInstrument
		Dim InstrumentTable As RenaissanceDataClass.DSInstrument.tblInstrumentDataTable
		Dim SelectedInstrumentRows As RenaissanceDataClass.DSInstrument.tblInstrumentRow()
		Dim thisInstrumentRow As RenaissanceDataClass.DSInstrument.tblInstrumentRow = Nothing

		Try

			' ***************************************************************************************************************
			' Get PortfolioDataIDs for Portfolios which match the Given Ticker.
			' This should represent all portfolios with a Filing Date on-or-before the given Filing Date.
			' ***************************************************************************************************************

			PortfolioIndexDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblPortfolioIndex, False)
			If (PortfolioIndexDS Is Nothing) Then
				MainForm.LogError(Me.Name, LOG_LEVELS.Error, "", "Failed to load tblPortfolioIndex.", "", True)
				Exit Sub
			End If
			PortfolioIndexTable = PortfolioIndexDS.tblPortfolioIndex
			SelectedPortfolioIndexRows = PortfolioIndexTable.Select("(PortfolioTicker='" & IndexPortfolioTicker & "') AND (PortfolioType=" & CInt(PortfolioType.IndexWeighting).ToString & ") AND (PortfolioFilingDate<='" & EndDate.ToString(QUERY_SHORTDATEFORMAT) & "')")
			If (SelectedPortfolioIndexRows Is Nothing) OrElse (SelectedPortfolioIndexRows.Length <= 0) Then
				MainForm.LogError(Me.Name, LOG_LEVELS.Error, "", "No Portfolios were returned for Ticker :'" & IndexPortfolioTicker & "'.", "", True)
				Exit Sub
			End If

			' Build PortfolioDataId string.

			For Each thisPortfolioIndexRow In SelectedPortfolioIndexRows
				If thisPortfolioIndexRow.PortfolioDataID > 0 Then
					If (PortfolioDataIDs.Length > 0) Then
						PortfolioDataIDs &= ","
					End If

					PortfolioDataIDs &= thisPortfolioIndexRow.PortfolioDataID.ToString
				End If
			Next


			' ***************************************************************************************************************
			' OK, Now get all the Portfolio Data Items that relate to the selected Portfolios.
			' This is so that we can build a list of Unique InstrumentIDs (BenchMark Index Sectors) for which
			' we need to retrieve pricing data.
			' ***************************************************************************************************************

			MainForm.SetToolStripText(Label_Status, "Loading Basic tables.")
			If Not (MainForm.InvokeRequired) Then
				Application.DoEvents()
			End If

			PortfolioDataDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblPortfolioData, False)
			If (PortfolioDataDS Is Nothing) Then
				MainForm.LogError(Me.Name, LOG_LEVELS.Error, "", "Failed to load tblPortfolioData.", "", True)
				MainForm.SetToolStripText(Label_Status, "")
				Exit Sub
			End If

			PortfolioDataTable = PortfolioDataDS.tblPortfolioData
			SelectedPortfolioDataRows = PortfolioDataTable.Select("PortfolioDataID IN (" & PortfolioDataIDs & ")", "PortfolioItemType, MarketInstrumentID")
			If (SelectedPortfolioDataRows Is Nothing) OrElse (SelectedPortfolioDataRows.Length <= 0) Then
				MainForm.LogError(Me.Name, LOG_LEVELS.Error, "", "No Portfolio Data Items were returned where PortfolioDataID IN(" & PortfolioDataIDs & ").", "", True)
				MainForm.SetToolStripText(Label_Status, "")
				Exit Sub
			End If

			' Get Instrument Table, It will be used later

			InstrumentDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblInstrument, False)
			If (InstrumentDS Is Nothing) Then
				MainForm.LogError(Me.Name, LOG_LEVELS.Error, "", "Failed to load tblInstrument.", "", True)
				MainForm.SetToolStripText(Label_Status, "")
				Exit Sub
			End If
			InstrumentTable = InstrumentDS.tblInstrument

		Catch ex As Exception

			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error resolving Portfolio Indices and related Portfolio Data items.", ex.StackTrace, True)
			MainForm.SetToolStripText(Label_Status, "")
			Exit Sub
		End Try

		' Count Unique Instruments
		Dim BenchmarkSectorPriceSeries As clsInstrumentDetails() ' clsInstrumentPrice()

		Try
			Dim BenchmarkSectorCount As Integer = 0
			Dim BenchmarkCounter As Integer = 0
			Dim LastInstrumentID As Integer = 0

			' thisPortfolioDataRow
			For BenchmarkCounter = 0 To (SelectedPortfolioDataRows.Length - 1)
				thisPortfolioDataRow = SelectedPortfolioDataRows(BenchmarkCounter)

				' PortfolioItemType
				If (thisPortfolioDataRow.PortfolioItemType = PortfolioItemType.VeniceInstrument) Then
					If LastInstrumentID <> thisPortfolioDataRow.MarketInstrumentID Then
						BenchmarkSectorCount += 1
						LastInstrumentID = thisPortfolioDataRow.MarketInstrumentID
					End If
				End If
			Next

			' ***************************************************************************************************************
			' Now Dimension the Price array and cycle through the PortfolioDataItems collecting the appropriate Pricing Data
			' ***************************************************************************************************************

			ReDim BenchmarkSectorPriceSeries(BenchmarkSectorCount - 1)

			BenchmarkSectorCount = 0
			For BenchmarkCounter = 0 To (SelectedPortfolioDataRows.Length - 1)
				thisPortfolioDataRow = SelectedPortfolioDataRows(BenchmarkCounter)

				If thisPortfolioDataRow.PortfolioItemType = PortfolioItemType.VeniceInstrument Then
					If LastInstrumentID <> thisPortfolioDataRow.MarketInstrumentID Then

						BenchmarkSectorPriceSeries(BenchmarkSectorCount) = New clsInstrumentDetails(PortfolioItemType.VeniceInstrument, thisPortfolioDataRow.MarketInstrumentID)

						' Get Instrument Details. Use Pertrac ID to get data, otherwise use Venice Pricing Data
						SelectedInstrumentRows = InstrumentTable.Select("InstrumentID=" & thisPortfolioDataRow.MarketInstrumentID.ToString)
						If (Not (SelectedInstrumentRows Is Nothing)) AndAlso (SelectedInstrumentRows.Length > 0) Then
							BenchmarkSectorPriceSeries(BenchmarkSectorCount).InstrumentDescription = SelectedInstrumentRows(0).InstrumentDescription

							If (SelectedInstrumentRows(0).IsInstrumentPertracCodeNull = False) Then
								BenchmarkSectorPriceSeries(BenchmarkSectorCount).PertracID = SelectedInstrumentRows(0).InstrumentPertracCode
							End If
						End If

						MainForm.SetToolStripText(Label_Status, "Loading Data for " & BenchmarkSectorPriceSeries(BenchmarkSectorCount).InstrumentDescription)
						If Not (MainForm.InvokeRequired) Then
							Application.DoEvents()
						End If

						If (BenchmarkSectorPriceSeries(BenchmarkSectorCount).PertracID > 0) Then
							Dim LastDate As Date = GetLastPertracPriceDate(MainForm, BenchmarkSectorPriceSeries(BenchmarkSectorCount).PertracID)

							If (LastDate < EndDate) Then
								If MessageBox.Show("The Pertrac price series for " & BenchmarkSectorPriceSeries(BenchmarkSectorCount).InstrumentDescription & " ends on " & LastDate.ToString(DISPLAYMEMBER_DATEFORMAT) & vbCrLf & "Do you want to continue (OK) or Cancel ?", "Continue or Cancel ?", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) <> Windows.Forms.DialogResult.OK Then
									Exit Sub
								End If
							End If

							BenchmarkSectorPriceSeries(BenchmarkSectorCount).PricingData = GetPertracPriceArray(MainForm, PricePeriod, PricingStartDate, EndDate, 0, BenchmarkSectorPriceSeries(BenchmarkSectorCount).PertracID, MainForm.Main_Knowledgedate)
						Else
							Dim LastDate As Date = GetLastInstrumentPriceDate(MainForm, BenchmarkSectorPriceSeries(BenchmarkSectorCount).InstrumentID)
							If (LastDate < EndDate) Then
								If MessageBox.Show("The Venice price series for " & BenchmarkSectorPriceSeries(BenchmarkSectorCount).InstrumentDescription & " ends on " & LastDate.ToString(DISPLAYMEMBER_DATEFORMAT) & vbCrLf & "Do you want to continue (OK) or Cancel ?", "Continue or Cancel ?", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) <> Windows.Forms.DialogResult.OK Then
									Exit Sub
								End If
							End If

							BenchmarkSectorPriceSeries(BenchmarkSectorCount).PricingData = GetInstrumentPriceArray(MainForm, PricePeriod, PricingStartDate, EndDate, BenchmarkSectorPriceSeries(BenchmarkSectorCount).InstrumentID, MainForm.Main_Knowledgedate)
						End If

						If (CurrencyModifierInUse = True) Then
							BenchmarkSectorPriceSeries(BenchmarkSectorCount).PricingData = AddPriceArrayReturns(BenchmarkSectorPriceSeries(BenchmarkSectorCount).PricingData, CurrencyCostPriceSeries.PricingData, PricePeriod)
						End If

						BenchmarkSectorCount += 1
						LastInstrumentID = thisPortfolioDataRow.MarketInstrumentID
					End If
				End If
			Next

		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error retrieving Benchmark Portfolio Indices pricing Data.", ex.StackTrace, True)
			MainForm.SetToolStripText(Label_Status, "")
			Exit Sub
		End Try


		' ***************************************************************************************************************
		' Declare and initialise Results table
		' ***************************************************************************************************************

		Dim BenchmarkAttributionResults As New DataTable("tblBenchmarkResults")
		Dim SelectedBenchmarkResultsRows As DataRow() = Nothing
		Dim thisBenchmarkResultsRow As DataRow = Nothing

		Dim YearCounter As Integer

		Try
			BenchmarkAttributionResults.Columns.Add("StrategyType", GetType(String))
			BenchmarkAttributionResults.Columns.Add("IndexInstrumentID", GetType(Integer))
			BenchmarkAttributionResults.Columns.Add("IndexInstrumentDescription", GetType(String))
			BenchmarkAttributionResults.Columns.Add("FundTypeID", GetType(Integer))
			BenchmarkAttributionResults.Columns.Add("FundTypeDescription", GetType(String))

			' Add a Column for Each years % Return
			YearCounter = StartDate.Year
			Do
				BenchmarkAttributionResults.Columns.Add("Fund" & YearCounter.ToString & "_Return", GetType(Double))
				BenchmarkAttributionResults.Columns.Add("Index" & YearCounter.ToString & "_Return", GetType(Double))
				YearCounter += 1
			Loop Until (YearCounter > EndDate.Year)

			BenchmarkAttributionResults.Columns.Add("FundITD_Return", GetType(Double))
			BenchmarkAttributionResults.Columns.Add("IndexITD_Return", GetType(Double))
			BenchmarkAttributionResults.Columns.Add("FundSumOfWeight", GetType(Double))
			BenchmarkAttributionResults.Columns.Add("IndexSumOfWeight", GetType(Double))
			BenchmarkAttributionResults.Columns.Add("FundCountOfWeight", GetType(Integer))
			BenchmarkAttributionResults.Columns.Add("IndexCountOfWeight", GetType(Integer))
			BenchmarkAttributionResults.Columns.Add("FundAverageWeight", GetType(Double))
			BenchmarkAttributionResults.Columns.Add("IndexAverageWeight", GetType(Double))
			BenchmarkAttributionResults.Columns.Add("FundReturn", GetType(Double))
			BenchmarkAttributionResults.Columns.Add("IndexReturn", GetType(Double))
			BenchmarkAttributionResults.Columns.Add("FundCountOfReturn", GetType(Integer))
			BenchmarkAttributionResults.Columns.Add("IndexCountOfReturn", GetType(Integer))
			BenchmarkAttributionResults.Columns.Add("FundIsPresent", GetType(Boolean))	' Flag to indicate if a fund Attribution is active on this Results Row.
			BenchmarkAttributionResults.Columns.Add("IndexIsPresent", GetType(Boolean))	' Flag to indicate if a Index Attribution is active on this Results Row.

			' In order to accurately calculate the Period-By-Period attributions data it is necessary to know if there are active
			' Fund or Index attributions on a given line. There are often situations where multiple FundTypes are attributed
			' to a single Index sector. In these situations, the Index Weight should only be attributed to a single line.
			' Conversely, there may be situations where an Index sector has no FundType mapping and Fund Weightings should always be considered to be zero.

			BenchmarkAttributionResults.Columns.Add("RelativeWeight", GetType(String))
			BenchmarkAttributionResults.Columns.Add("SectorSelectionAttribution", GetType(Double))
			BenchmarkAttributionResults.Columns.Add("StockPickingAttribution", GetType(Double))
			BenchmarkAttributionResults.Columns.Add("NetAttribution", GetType(Double))

		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Initialising Benchmark Attribution Results table.", ex.StackTrace, True)
			Exit Sub
		End Try

		' ***************************************************************************************************************
		' Set up adaptor / table for benchmark attribution data
		' ***************************************************************************************************************

		Dim adpBenchmarkAttribution As New SqlDataAdapter()
		Dim tblBenchmarkAttribution As New DataTable("BenchmarkAttributions")
		Dim thisBenchmarkAttributionRow As DataRow = Nothing

		' Initialise Adaptor for retrieving Un-Mapped Fund Attributions 

		Dim UnMappedAttributions As New ArrayList
		Dim adpUnMappedAttributions As New SqlDataAdapter
		Dim tblUnMappedAttributions As New DataTable

		Try
			adpUnMappedAttributions.SelectCommand = New SqlCommand("spu_ViewUnMappedAttributions", MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION))
			adpUnMappedAttributions.SelectCommand.CommandType = CommandType.StoredProcedure
			adpUnMappedAttributions.SelectCommand.Parameters.Add(New SqlParameter("@FundID", SqlDbType.Int, 4)).Value = FundIndex
			adpUnMappedAttributions.SelectCommand.Parameters.Add(New SqlParameter("@AttribPeriod", SqlDbType.Int, 4)).Value = PricePeriod
			adpUnMappedAttributions.SelectCommand.Parameters.Add(New SqlParameter("@IndexTicker", SqlDbType.VarChar, 50)).Value = IndexPortfolioTicker
			adpUnMappedAttributions.SelectCommand.Parameters.Add(New SqlParameter("@ValueDate", SqlDbType.DateTime, 8)).Value = StartDate
			adpUnMappedAttributions.SelectCommand.Parameters.Add(New SqlParameter("@LookthroughAttributions", SqlDbType.Int, 4)).Value = LookthroughAttributionsFlag
			adpUnMappedAttributions.SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", SqlDbType.DateTime, 8)).Value = MainForm.Main_Knowledgedate
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Initialising Unmapped Attribution Query adaptor.", ex.StackTrace, True)
			Exit Sub
		End Try

		' OK, Start Getting and Checking Attributions on a Period-By-Period basis

		Dim thisdate As Date
		Dim PeriodCount As Integer = 0
		Dim CurrencyReturnAdjustment As Double = 0
		Dim CurrencyAdjustmentIndex As Integer = 0

		Dim FundSectorPriceSeries(-1) As clsInstrumentDetails
		Dim FundSectorPriceSeriesIndex As Integer

		Dim MissingAttributions As String = ""

		Try
			Dim thisPricingIndex As Integer

			MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION), adpBenchmarkAttribution, "rptBenchmarkAttribution")
			thisdate = StartDate
			PeriodCount = 0

			While thisdate.CompareTo(EndDate) <= 0 ' (thisdate.Year < EndDate.Year) Or (thisdate.Month <= EndDate.Month)
				' Set 'thisDate' to last day of period

				thisdate = FitDateToPeriod(PricePeriod, thisdate, True)
				thisPricingIndex = GetPriceIndex(PricePeriod, PricingStartDate, thisdate)

				MainForm.SetToolStripText(Label_Status, "Processing Attributions : " & thisdate.ToString(DISPLAYMEMBER_DATEFORMAT))
				If Not (MainForm.InvokeRequired) Then
					Application.DoEvents()
				End If

				' Currency Adjustment
				If (CurrencyModifierInUse = True) AndAlso (Not (CurrencyCostPriceSeries Is Nothing)) AndAlso (Not (CurrencyCostPriceSeries.PricingData Is Nothing)) Then
					CurrencyAdjustmentIndex = thisPricingIndex

					If (CurrencyAdjustmentIndex < CurrencyCostPriceSeries.PricingData.Length) Then
						CurrencyReturnAdjustment = CurrencyCostPriceSeries.PricingData(CurrencyAdjustmentIndex).PriceReturn
					Else
						CurrencyReturnAdjustment = 0
					End If
				End If

				' Get Attributions Data for this Date / Period
				Try
					adpBenchmarkAttribution.SelectCommand.Parameters("@FundID").Value = FundIndex
					adpBenchmarkAttribution.SelectCommand.Parameters("@AttribPeriod").Value = PricePeriod
					adpBenchmarkAttribution.SelectCommand.Parameters("@IndexTicker").Value = IndexPortfolioTicker
					adpBenchmarkAttribution.SelectCommand.Parameters("@ValueDate").Value = thisdate.Date
					adpBenchmarkAttribution.SelectCommand.Parameters("@LookthroughAttributions").Value = LookthroughAttributionsFlag
					adpBenchmarkAttribution.SelectCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate

					tblBenchmarkAttribution.Clear()
					SyncLock adpBenchmarkAttribution.SelectCommand.Connection
						adpBenchmarkAttribution.Fill(tblBenchmarkAttribution)
					End SyncLock
				Catch Inner_Ex As Exception
					tblBenchmarkAttribution.Clear()
				End Try

				If (tblBenchmarkAttribution.Rows.Count > 0) Then
					' Two Pass operation, First get the totals for Weights in order to Scale the Attribution rows back to 100%
					Dim TotalFundWeight As Double = 0
					Dim TotalFundBips As Double = 0

					For Each thisBenchmarkAttributionRow In tblBenchmarkAttribution.Rows
						TotalFundWeight += CDbl(thisBenchmarkAttributionRow("SumOfAttribWeight"))
						TotalFundBips += CDbl(thisBenchmarkAttributionRow("SectorBips"))
					Next

					' Set Gross return from which to generate fund price series.
					FundPriceSeries.PricingData(thisPricingIndex).PriceReturn = (TotalFundBips / 10000.0) + CurrencyReturnAdjustment

					' Second Pass, populate the results Row(s) as appropriate.

					For Each thisBenchmarkAttributionRow In tblBenchmarkAttribution.Rows
						' Is there an existing results Row to match this Attribution Row ?

						' IsNewBenchmarkResultsRow = False
						SelectedBenchmarkResultsRows = BenchmarkAttributionResults.Select("(IndexInstrumentID=" & CInt(thisBenchmarkAttributionRow("IndexInstrumentID")).ToString & ") AND (FundTypeID=" & CInt(thisBenchmarkAttributionRow("MappedFundTypeID")).ToString & ")")
						If (SelectedBenchmarkResultsRows Is Nothing) OrElse (SelectedBenchmarkResultsRows.Length <= 0) Then
							' Add new Results Row
							thisBenchmarkResultsRow = BenchmarkAttributionResults.NewRow
							' IsNewBenchmarkResultsRow = True

							thisBenchmarkResultsRow("StrategyType") = thisBenchmarkAttributionRow("InstrumentClass").ToString
							thisBenchmarkResultsRow("IndexInstrumentID") = CInt(thisBenchmarkAttributionRow("IndexInstrumentID"))
							thisBenchmarkResultsRow("IndexInstrumentDescription") = thisBenchmarkAttributionRow("InstrumentDescription").ToString
							thisBenchmarkResultsRow("FundTypeID") = CInt(thisBenchmarkAttributionRow("MappedFundTypeID"))
							thisBenchmarkResultsRow("FundTypeDescription") = thisBenchmarkAttributionRow("FundTypeDescription").ToString

							YearCounter = StartDate.Year
							Do
								thisBenchmarkResultsRow("Fund" & YearCounter.ToString & "_Return") = 0
								thisBenchmarkResultsRow("Index" & YearCounter.ToString & "_Return") = 0
								YearCounter += 1
							Loop Until (YearCounter > EndDate.Year)

							thisBenchmarkResultsRow("FundITD_Return") = 0
							thisBenchmarkResultsRow("IndexITD_Return") = 0
							thisBenchmarkResultsRow("FundSumOfWeight") = 0
							thisBenchmarkResultsRow("IndexSumOfWeight") = 0
							thisBenchmarkResultsRow("FundCountOfWeight") = 0
							thisBenchmarkResultsRow("IndexCountOfWeight") = 0
							thisBenchmarkResultsRow("FundAverageWeight") = 0
							thisBenchmarkResultsRow("IndexAverageWeight") = 0
							thisBenchmarkResultsRow("FundReturn") = 0
							thisBenchmarkResultsRow("IndexReturn") = 0
							thisBenchmarkResultsRow("FundCountOfReturn") = 0
							thisBenchmarkResultsRow("IndexCountOfReturn") = 0
							thisBenchmarkResultsRow("RelativeWeight") = ""
							thisBenchmarkResultsRow("SectorSelectionAttribution") = 0
							thisBenchmarkResultsRow("StockPickingAttribution") = 0
							thisBenchmarkResultsRow("NetAttribution") = 0
							thisBenchmarkResultsRow("FundIsPresent") = False
							thisBenchmarkResultsRow("IndexIsPresent") = False

							BenchmarkAttributionResults.Rows.Add(thisBenchmarkResultsRow)

							ReDim Preserve FundSectorPriceSeries(FundSectorPriceSeries.Length)
							FundSectorPriceSeries(FundSectorPriceSeries.Length - 1) = New clsInstrumentDetails(PortfolioItemType.Default, CInt(thisBenchmarkAttributionRow("MappedFundTypeID")), thisBenchmarkAttributionRow("FundTypeDescription").ToString, 0)
							FundSectorPriceSeries(FundSectorPriceSeries.Length - 1).InitialisePricingArray(PricePeriod, PricingStartDate, EndDate)
							FundSectorPriceSeriesIndex = (FundSectorPriceSeries.Length - 1)
						Else
							thisBenchmarkResultsRow = SelectedBenchmarkResultsRows(0)
							FundSectorPriceSeriesIndex = 0
							While (FundSectorPriceSeriesIndex < FundSectorPriceSeries.Length) AndAlso (FundSectorPriceSeries(FundSectorPriceSeriesIndex).InstrumentID <> CInt(thisBenchmarkAttributionRow("MappedFundTypeID")))
								FundSectorPriceSeriesIndex += 1
							End While
							If (FundSectorPriceSeriesIndex >= FundSectorPriceSeries.Length) Then
								FundSectorPriceSeriesIndex = (-1)
							End If
						End If

						' Record Period-By-Period Fund Sector Returns and Weights
						' Weight is adjusted so that weights total 100%
						' Returns are adjusted so that sector Bips will remain constant given the new weights.

						If (FundSectorPriceSeriesIndex >= 0) Then
							FundSectorPriceSeries(FundSectorPriceSeriesIndex).PricingData(thisPricingIndex).WeightValue = (CDbl(thisBenchmarkAttributionRow("SumOfAttribWeight")) / TotalFundWeight) ' Scale to 100%
							FundSectorPriceSeries(FundSectorPriceSeriesIndex).PricingData(thisPricingIndex).Bips = CDbl(thisBenchmarkAttributionRow("SectorBips"))
							FundSectorPriceSeries(FundSectorPriceSeriesIndex).PricingData(thisPricingIndex).ReturnValue = (CDbl(thisBenchmarkAttributionRow("SectorBips")) / FundSectorPriceSeries(FundSectorPriceSeriesIndex).PricingData(thisPricingIndex).WeightValue) / 10000.0	' Return adjusted to account for Adjusted Weight
						End If

						' Update 'thisBenchmarkResultsRow' with the current Attribution Data

						thisBenchmarkResultsRow("FundSumOfWeight") = CDbl(thisBenchmarkResultsRow("FundSumOfWeight")) + FundSectorPriceSeries(FundSectorPriceSeriesIndex).PricingData(thisPricingIndex).WeightValue
						thisBenchmarkResultsRow("FundCountOfWeight") = CDbl(thisBenchmarkResultsRow("FundCountOfWeight")) + 1.0

						' thisBenchmarkResultsRow("FundAverageWeight") = CDbl(thisBenchmarkResultsRow("FundSumOfWeight")) / CDbl(thisBenchmarkResultsRow("FundCountOfWeight"))

						thisBenchmarkResultsRow("FundReturn") = ((1.0 + CDbl(thisBenchmarkResultsRow("FundReturn"))) * (1.0 + FundSectorPriceSeries(FundSectorPriceSeriesIndex).PricingData(thisPricingIndex).ReturnValue)) - 1.0
						thisBenchmarkResultsRow("FundCountOfReturn") = CDbl(thisBenchmarkResultsRow("FundCountOfReturn")) + 1.0

						thisBenchmarkResultsRow("Fund" & thisdate.Year.ToString & "_Return") = ((1.0 + CDbl(thisBenchmarkResultsRow("Fund" & thisdate.Year.ToString & "_Return"))) * (1.0 + FundSectorPriceSeries(FundSectorPriceSeriesIndex).PricingData(thisPricingIndex).ReturnValue)) - 1.0
						thisBenchmarkResultsRow("FundIsPresent") = True

					Next

				Else
					' There appears to be no Attribution records for this Date / Period

					If MissingAttributions.Length > 0 Then
						MissingAttributions &= ", "
					Else
						MissingAttributions = "There appear to be no mapped " & PricePeriod.ToString & " Attributions for "
					End If
					MissingAttributions &= thisdate.ToString(DISPLAYMEMBER_DATEFORMAT)

				End If ' tblBenchmarkAttribution.Rows.Count > 0


				' **********************************************************
				' Check for missing Mappings 
				' **********************************************************

				Try
					SyncLock adpUnMappedAttributions.SelectCommand.Connection
						adpUnMappedAttributions.SelectCommand.Parameters("@ValueDate").Value = thisdate
						tblUnMappedAttributions.Clear()
						adpUnMappedAttributions.Fill(tblUnMappedAttributions)
					End SyncLock

					If (Not (tblUnMappedAttributions Is Nothing)) AndAlso (tblUnMappedAttributions.Rows.Count > 0) Then
						Dim UnMappedCounter As Integer
						Dim ArrayListCounter As Integer
						Dim FoundFlag As Boolean
						Dim UnMappedFundTypeDescription As String

						For UnMappedCounter = 0 To (tblUnMappedAttributions.Rows.Count - 1)
							' Ignore Un-Mapped CashFund types.

							If CInt(tblUnMappedAttributions.Rows(UnMappedCounter)(0)) <> CInt(FundType.CashFund) Then
								FoundFlag = False
								UnMappedFundTypeDescription = CStr(tblUnMappedAttributions.Rows(UnMappedCounter)(1))

								If (Not (UnMappedAttributions Is Nothing)) AndAlso (UnMappedAttributions.Count > 0) Then
									For ArrayListCounter = 0 To (UnMappedAttributions.Count - 1)
										If CStr(UnMappedAttributions(ArrayListCounter)) = UnMappedFundTypeDescription Then
											FoundFlag = True
											Exit For
										End If
									Next
								End If

								If (FoundFlag = False) Then
									UnMappedAttributions.Add(UnMappedFundTypeDescription)
								End If
							End If
						Next
					End If

				Catch Inner_Ex As Exception
					MainForm.LogError(Me.Name, LOG_LEVELS.Error, Inner_Ex.Message, "Error checking for missing mappings.", Inner_Ex.StackTrace, True)
				End Try

				PeriodCount += 1

				thisdate = AddPeriodToDate(PricePeriod, thisdate)
				' thisdate = thisdate.AddMonths(1)
			End While
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error processing Attributions for " & thisdate.ToString(DISPLAYMEMBER_DATEFORMAT), ex.StackTrace, True)
			Exit Sub
		End Try

		' Create Fund price series from returns
		Try
			Dim TempIndex As Integer

			FundPriceSeries.PricingData(0).PriceValue = 100
			For TempIndex = 1 To (FundPriceSeries.PricingData.Length - 1)
				FundPriceSeries.PricingData(TempIndex).PriceValue = FundPriceSeries.PricingData(TempIndex - 1).PriceValue * (1.0 + FundPriceSeries.PricingData(TempIndex).PriceReturn)
			Next
		Catch ex As Exception
		End Try

		' Missing Attributions ?
		Try
			If (MissingAttributions.Length > 0) Then
				If MessageBox.Show(MissingAttributions & vbCrLf & "Press OK to Continue or Cancel to quit.", "", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Cancel Then
					MainForm.SetToolStripText(Label_Status, "")
					Exit Sub
				End If
			End If
		Catch ex As Exception
		End Try

		' Check for UnMapped Fund Types ?

		Try
			If (Not (UnMappedAttributions Is Nothing)) AndAlso (UnMappedAttributions.Count > 0) Then
				Dim MessageString As String
				Dim UnMappedCounter As Integer

				MessageString = ""

				For UnMappedCounter = 0 To (tblUnMappedAttributions.Rows.Count - 1)
					MessageString = MessageString & CStr(UnMappedAttributions(UnMappedCounter)) & vbCrLf
				Next

				If (MessageString.Length > 0) Then
					If MessageBox.Show("There appear to be Attribution Fund Types that are not mapped to a Benchmark Index Sector : " & vbCrLf & MessageString & vbCrLf & "Do you want to continue with the report ?", "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) <> Windows.Forms.DialogResult.OK Then
						MainForm.SetToolStripText(Label_Status, "Aborted.")
						Exit Sub
					End If
				End If
			End If
		Catch ex As Exception
		End Try


		' In-Fill rows for any Portfolio Index Lines that do not have mappings and thus will not have been caught earlier.
		Try
			MainForm.SetToolStripText(Label_Status, "Setting In-Fill Results.")
			If Not (MainForm.InvokeRequired) Then
				Application.DoEvents()
			End If

			Dim PortfolioCounter As Integer = 0
			SelectedPortfolioIndexRows = PortfolioIndexTable.Select("PortfolioDataID IN (" & PortfolioDataIDs & ")", "PortfolioFilingDate Desc")

			If (Not (SelectedPortfolioIndexRows Is Nothing)) AndAlso (SelectedPortfolioIndexRows.Length > 0) Then
				' Save Latest applicable Portfolio Filing Date for displaying on the report
				LatestBenchmarkPortfolio = SelectedPortfolioIndexRows(0).PortfolioFilingDate

				' In Fill.
				Do
					thisPortfolioIndexRow = SelectedPortfolioIndexRows(PortfolioCounter)

					SelectedPortfolioDataRows = PortfolioDataTable.Select("PortfolioDataID=" & thisPortfolioIndexRow.PortfolioDataID, "PortfolioItemType, MarketInstrumentID")

					If (Not (SelectedPortfolioDataRows Is Nothing)) AndAlso (SelectedPortfolioDataRows.Length > 0) Then
						For Each thisPortfolioDataRow In SelectedPortfolioDataRows
							If thisPortfolioDataRow.PortfolioItemType = PortfolioItemType.VeniceInstrument Then
								' Is there an existing line for this Portfolio Item ?

								SelectedBenchmarkResultsRows = BenchmarkAttributionResults.Select("(IndexInstrumentID=" & thisPortfolioDataRow.MarketInstrumentID.ToString & ")")
								If (SelectedBenchmarkResultsRows Is Nothing) OrElse (SelectedBenchmarkResultsRows.Length <= 0) Then
									Try
										' Add new Row
										thisBenchmarkResultsRow = BenchmarkAttributionResults.NewRow

										SelectedInstrumentRows = InstrumentTable.Select("InstrumentID=" & thisPortfolioDataRow.MarketInstrumentID.ToString)
										If (Not (SelectedInstrumentRows Is Nothing)) AndAlso (SelectedInstrumentRows.Length > 0) Then
											If (SelectedInstrumentRows(0).InstrumentClass.Trim.Length > 0) Then
												thisBenchmarkResultsRow("StrategyType") = SelectedInstrumentRows(0).InstrumentClass
											Else
												thisBenchmarkResultsRow("StrategyType") = "<No Instrument Class>"
											End If
											thisBenchmarkResultsRow("IndexInstrumentDescription") = SelectedInstrumentRows(0).InstrumentDescription
										Else
											thisBenchmarkResultsRow("StrategyType") = "<No Instrument Found>"
											thisBenchmarkResultsRow("IndexInstrumentDescription") = "<No Instrument Found>"
										End If

										thisBenchmarkResultsRow("IndexInstrumentID") = thisPortfolioDataRow.MarketInstrumentID
										thisBenchmarkResultsRow("FundTypeID") = 0
										thisBenchmarkResultsRow("FundTypeDescription") = ""

										YearCounter = StartDate.Year
										Do
											thisBenchmarkResultsRow("Fund" & YearCounter.ToString & "_Return") = 0
											thisBenchmarkResultsRow("Index" & YearCounter.ToString & "_Return") = 0
											YearCounter += 1
										Loop Until (YearCounter > EndDate.Year)

										thisBenchmarkResultsRow("FundITD_Return") = 0
										thisBenchmarkResultsRow("IndexITD_Return") = 0
										thisBenchmarkResultsRow("FundSumOfWeight") = 0
										thisBenchmarkResultsRow("IndexSumOfWeight") = 0
										thisBenchmarkResultsRow("FundCountOfWeight") = 0
										thisBenchmarkResultsRow("IndexCountOfWeight") = 0
										thisBenchmarkResultsRow("FundAverageWeight") = 0
										thisBenchmarkResultsRow("IndexAverageWeight") = 0
										thisBenchmarkResultsRow("FundReturn") = 0
										thisBenchmarkResultsRow("IndexReturn") = 0
										thisBenchmarkResultsRow("FundCountOfReturn") = 0
										thisBenchmarkResultsRow("IndexCountOfReturn") = 0
										thisBenchmarkResultsRow("RelativeWeight") = ""
										thisBenchmarkResultsRow("SectorSelectionAttribution") = 0
										thisBenchmarkResultsRow("StockPickingAttribution") = 0
										thisBenchmarkResultsRow("NetAttribution") = 0
										thisBenchmarkResultsRow("FundIsPresent") = False
										thisBenchmarkResultsRow("IndexIsPresent") = False

										BenchmarkAttributionResults.Rows.Add(thisBenchmarkResultsRow)
									Catch Inner_Ex As Exception
									End Try

								End If

							End If
						Next
					End If

					PortfolioCounter += 1
					' Loop until all portfolios have been checked, or the Just-Checked portfolio Filing date is <= Start Date.
				Loop Until (PortfolioCounter >= SelectedPortfolioIndexRows.Length) Or (StartDate.CompareTo(thisPortfolioIndexRow.PortfolioFilingDate) >= 0)
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error setting Index Sub-Fund InFill Results Rows.", ex.StackTrace, True)
			Exit Sub
		End Try

		' ***********************************************************************************
		' Ta Da !!!!!!
		' ***********************************************************************************
		'
		' OK, we now have a Data Table which has at least one line for each Index Component (Portfolio data Line)
		' including one line for each mapped Portfolio Component / Fund Type combination.
		' To the extent that there were Attributions, the Data Table has been populated with 
		' FundWeight, FundReturn field and Fund Year Return fields, Together with StrategyType, IndexInstrumentID, IndexInstrumentDescription FundTypeID and FundTypeDescription
		'
		' Now we need to fill in the other fields......
		'     Index Weight
		'     Index Return, Index Year Return
		' then
		'     Relative Weight
		'     Sector, Stock Pick and Net Attributions 
		'
		' It would also be usefull to check that all FundTypes that have attribution data asscoiated with them
		' are mapped to something.
		' ***********************************************************************************

		Try
			MainForm.SetToolStripText(Label_Status, "Calculating Fields")
			If Not (MainForm.InvokeRequired) Then
				Application.DoEvents()
			End If
		Catch ex As Exception
		End Try

		' Calculate Average Fund Weights (Needed to determine which Results line to put the Index Weight on.)

		Try
			Dim SumOfAverageWeights As Double = 0

			For Each thisBenchmarkResultsRow In BenchmarkAttributionResults.Rows
				If CDbl(thisBenchmarkResultsRow("FundCountOfWeight")) > 0 Then
					thisBenchmarkResultsRow("FundAverageWeight") = CDbl(thisBenchmarkResultsRow("FundSumOfWeight")) / CDbl(thisBenchmarkResultsRow("FundCountOfWeight"))
					SumOfAverageWeights += CDbl(thisBenchmarkResultsRow("FundAverageWeight"))
				Else
					thisBenchmarkResultsRow("FundAverageWeight") = 0
				End If
			Next

			' Normalise Average weights to 100%
			If (SumOfAverageWeights <> 0) Then
				For Each thisBenchmarkResultsRow In BenchmarkAttributionResults.Rows
					thisBenchmarkResultsRow("FundAverageWeight") = thisBenchmarkResultsRow("FundAverageWeight") / SumOfAverageWeights
				Next
			End If
		Catch ex As Exception
		End Try

		' Index Weights....
		' To do this we will need a period-by-period Benchmark Index Portfolio so that the weights can be averaged
		' as with the Fund Weights.

		Try
			' PortfolioDataIDs should contain the Data IDs for all versions of the Benchmark Index with a Filing Date on-Or-Before the 'EndDate'

			Dim PortfolioCounter As Integer = 0

			SelectedPortfolioIndexRows = PortfolioIndexTable.Select("PortfolioDataID IN (" & PortfolioDataIDs & ")", "PortfolioFilingDate Desc")

			If (Not (SelectedPortfolioIndexRows Is Nothing)) AndAlso (SelectedPortfolioIndexRows.Length > 0) Then

				thisdate = StartDate
				While thisdate.CompareTo(EndDate) <= 0 ' (thisdate.Year < EndDate.Year) Or (thisdate.Month <= EndDate.Month)

					' Set 'thisDate' to last day of month

					thisdate = FitDateToPeriod(PricePeriod, thisdate, True)

					' Select the appropriate version of the Benchmark Portfolio
					' Loop until no more Portfolios are found, or the current Month's date is On-Or-Before the Portfolio Filing Date

					PortfolioCounter = 0
					thisPortfolioIndexRow = Nothing
					Do
						thisPortfolioIndexRow = SelectedPortfolioIndexRows(PortfolioCounter)
						PortfolioCounter += 1
					Loop Until (thisdate.CompareTo(thisPortfolioIndexRow.PortfolioFilingDate) >= 0) Or (PortfolioCounter >= SelectedPortfolioIndexRows.Length)

					SelectedPortfolioDataRows = PortfolioDataTable.Select("PortfolioDataID=" & thisPortfolioIndexRow.PortfolioDataID, "PortfolioItemType, MarketInstrumentID")


					' If this is the start date or if there is a index re-weighting (Filing date) in the current period then use the selected portfolio,
					' otherwise roll forward the return modified weights from the previous period.

					Dim TotalIndexWeight As Double = 0
					Dim SumOfIndexBips As Double = 0
					Dim ActualIndexBips As Double = 0
					Dim ActualIndexReturn As Double = 0
					Dim thisBenchmarkSectorDetails As clsInstrumentDetails
					Dim ThisPriceIndex As Integer
					Dim LastPriceIndex As Integer

					ThisPriceIndex = GetPriceIndex(PricePeriod, PricingStartDate, thisdate)

					ActualIndexReturn = BenchmarkPriceSeries.PricingData(ThisPriceIndex).PriceReturn
					ActualIndexBips = ActualIndexReturn * 10000.0

					If (thisdate = FitDateToPeriod(PricePeriod, StartDate, True)) Or _
						 (thisdate = FitDateToPeriod(PricePeriod, thisPortfolioIndexRow.PortfolioFilingDate, True)) Then

						' Use weights from chosen portfolio 

						If (Not (SelectedPortfolioDataRows Is Nothing)) AndAlso (SelectedPortfolioDataRows.Length > 0) Then

							'Now, for this month's Benchmark composition, calculate the total weight and then calculate the Weight averages.

							' Calculate SumOfIndexWeights
							For Each thisPortfolioDataRow In SelectedPortfolioDataRows
								If thisPortfolioDataRow.PortfolioItemType = PortfolioItemType.VeniceInstrument Then
									TotalIndexWeight += CDbl(thisPortfolioDataRow.Weight)
								End If
							Next

							' Add SumOfWeights to the Results Set 
							For Each thisPortfolioDataRow In SelectedPortfolioDataRows
								If thisPortfolioDataRow.PortfolioItemType = PortfolioItemType.VeniceInstrument Then

									' Bear in mind that there are probably multiple results row for each Index component (Sector)

									SelectedBenchmarkResultsRows = BenchmarkAttributionResults.Select("(IndexInstrumentID=" & thisPortfolioDataRow.MarketInstrumentID.ToString & ")", "FundAverageWeight Desc")

									If (Not (SelectedBenchmarkResultsRows Is Nothing)) AndAlso (SelectedBenchmarkResultsRows.Length > 0) Then
										' There will be multiple result rows for a Benchmark Index component if there are multiple F&C Sectors (Fund Types)
										' mapped to it (and there are attributions for those sectors).
										' We must not duplicate the Index Weightings, so we will only add Index weightings to the line with the
										' highest F&C Fund weighting. This is a bit arbitrary, but it does match the original FB Spreadsheet.
										thisBenchmarkResultsRow = SelectedBenchmarkResultsRows(0)

										thisBenchmarkResultsRow("IndexSumOfWeight") = CDbl(thisBenchmarkResultsRow("IndexSumOfWeight")) + (thisPortfolioDataRow.Weight / TotalIndexWeight) ' Scale to 100%
										thisBenchmarkResultsRow("IndexCountOfWeight") = CDbl(thisBenchmarkResultsRow("IndexCountOfWeight")) + 1.0
										thisBenchmarkResultsRow("IndexIsPresent") = True
									End If

								End If
							Next

							'  Populate 'Weight' value of BenchmarkSectorPriceSeries
							' 

							For Each thisPortfolioDataRow In SelectedPortfolioDataRows
								If thisPortfolioDataRow.PortfolioItemType = PortfolioItemType.VeniceInstrument Then
									For Each thisBenchmarkSectorDetails In BenchmarkSectorPriceSeries
										If (thisBenchmarkSectorDetails.InstrumentID = thisPortfolioDataRow.MarketInstrumentID) Then
											thisBenchmarkSectorDetails.PricingData(ThisPriceIndex).WeightValue = thisPortfolioDataRow.Weight
											Exit For
										End If
									Next
								End If
							Next
						End If

					Else

						' Roll forward weights from previous period.

						LastPriceIndex = ThisPriceIndex - 1

						If (ThisPriceIndex >= 0) Then
							' roll forward historic weights adjusted for actual returns.
							For Each thisBenchmarkSectorDetails In BenchmarkSectorPriceSeries
								thisBenchmarkSectorDetails.PricingData(ThisPriceIndex).WeightValue = (thisBenchmarkSectorDetails.PricingData(LastPriceIndex).WeightValue * (1.0 + thisBenchmarkSectorDetails.PricingData(ThisPriceIndex).PriceReturn)) / (1.0 + ActualIndexReturn)
								TotalIndexWeight += thisBenchmarkSectorDetails.PricingData(ThisPriceIndex).WeightValue
							Next

						End If
					End If

					' Normalise weights to 100% and Adjust Returns to match adjusted weights
					SumOfIndexBips = 0
					For Each thisBenchmarkSectorDetails In BenchmarkSectorPriceSeries
						thisBenchmarkSectorDetails.PricingData(ThisPriceIndex).WeightValue /= TotalIndexWeight
						thisBenchmarkSectorDetails.PricingData(ThisPriceIndex).Bips = (thisBenchmarkSectorDetails.PricingData(ThisPriceIndex).PriceReturn * thisBenchmarkSectorDetails.PricingData(ThisPriceIndex).WeightValue) * 10000.0
						SumOfIndexBips += thisBenchmarkSectorDetails.PricingData(ThisPriceIndex).Bips
					Next

					' Normalise Bips to match actual index return.
					If (SumOfIndexBips <> 0) Then
						For Each thisBenchmarkSectorDetails In BenchmarkSectorPriceSeries
							thisBenchmarkSectorDetails.PricingData(ThisPriceIndex).Bips = thisBenchmarkSectorDetails.PricingData(ThisPriceIndex).Bips * (ActualIndexBips / SumOfIndexBips)
						Next
					End If

					' Adjust returns to match adjusted Bips and Adjusted weights
					For Each thisBenchmarkSectorDetails In BenchmarkSectorPriceSeries
						thisBenchmarkSectorDetails.PricingData(ThisPriceIndex).PriceReturn = (thisBenchmarkSectorDetails.PricingData(ThisPriceIndex).Bips / thisBenchmarkSectorDetails.PricingData(ThisPriceIndex).WeightValue) / 10000.0
					Next

					thisdate = AddPeriodToDate(PricePeriod, thisdate)
				End While
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error setting Sub-Index Weights.", ex.StackTrace, True)
		End Try

		' Now calculate Average Index Weights
		Try
			For Each thisBenchmarkResultsRow In BenchmarkAttributionResults.Rows
				If CDbl(thisBenchmarkResultsRow("IndexCountOfWeight")) > 0 Then
					thisBenchmarkResultsRow("IndexAverageWeight") = CDbl(thisBenchmarkResultsRow("IndexSumOfWeight")) / CDbl(thisBenchmarkResultsRow("IndexCountOfWeight"))
				Else
					thisBenchmarkResultsRow("IndexAverageWeight") = 0
				End If
			Next
		Catch ex As Exception
		End Try

		' Now calculate the Index returns

		Try
			Dim BenchmarkCounter As Integer
			Dim thisPrice As clsInstrumentPrice
			Dim PriceCounter As Integer

			If (Not (BenchmarkSectorPriceSeries Is Nothing)) AndAlso (Not (BenchmarkAttributionResults Is Nothing)) Then
				For Each thisBenchmarkResultsRow In BenchmarkAttributionResults.Rows
					' Isolate Price Series from those previously recovered.

					For BenchmarkCounter = 0 To (BenchmarkSectorPriceSeries.Length - 1)

						If (Not (BenchmarkSectorPriceSeries(BenchmarkCounter) Is Nothing)) AndAlso (BenchmarkSectorPriceSeries(BenchmarkCounter).InstrumentID = CInt(thisBenchmarkResultsRow("IndexInstrumentID"))) Then

							If (Not (BenchmarkSectorPriceSeries(BenchmarkCounter).PricingData Is Nothing)) Then
								' Apply each Price return to the Totalreturn and to the Year return fields.
								CurrencyReturnAdjustment = 0
								thisBenchmarkResultsRow("IndexReturn") = 0
								thisBenchmarkResultsRow("IndexCountOfReturn") = 0

								For PriceCounter = 1 To (BenchmarkSectorPriceSeries(BenchmarkCounter).PricingData.Length - 1)	' Start at Index 1 as index 0 is before StartDate.

									' Set Return.
									thisPrice = BenchmarkSectorPriceSeries(BenchmarkCounter).PricingData(PriceCounter)

									thisBenchmarkResultsRow("IndexReturn") = ((1.0 + CDbl(thisBenchmarkResultsRow("IndexReturn"))) * (1.0 + CurrencyReturnAdjustment + thisPrice.PriceReturn)) - 1.0
									thisBenchmarkResultsRow("IndexCountOfReturn") = CDbl(thisBenchmarkResultsRow("IndexCountOfReturn")) + 1.0
									thisBenchmarkResultsRow("Index" & thisPrice.PriceDate.Year.ToString & "_Return") = ((1.0 + CDbl(thisBenchmarkResultsRow("Index" & thisPrice.PriceDate.Year.ToString & "_Return"))) * (1.0 + CurrencyReturnAdjustment + thisPrice.PriceReturn)) - 1.0
								Next
							End If

							Exit For
						End If
					Next BenchmarkCounter
				Next ' thisBenchmarkResultsRow
			End If ' Things NOT nothing

		Catch Inner_Ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, Inner_Ex.Message, "Error setting Sub-Index returns.", Inner_Ex.StackTrace, True)
			Exit Sub
		End Try

		Try
			' V1 - New (More Accurate) method.

			' WOW, now we're cooking .......

			'     Relative Weight
			'     Sector, Stock Pick and Net Attributions 

			' We need the Index Return for the next step, there are two alternatives :
			' 1) Calculate the weighted return from the Index sectors (As per the spreadsheet)
			' 2) Use the return from the Index price series. 
			' Possibly we should do both as an error check ?

			' For Each period....

			thisdate = FitDateToPeriod(PricePeriod, StartDate, True)
			Dim PricingIndex As Integer
			Dim StartIndex As Integer
			StartIndex = GetPriceIndex(PricePeriod, PricingStartDate, thisdate)

			While thisdate.CompareTo(EndDate) <= 0 ' (thisdate.Year < EndDate.Year) Or (thisdate.Month <= EndDate.Month)
				' Note special StartDate, InOrder to keep in synch with Benchmark pricing which has to start before 'StartDate' so that Index returns can be calculated.
				PricingIndex = GetPriceIndex(PricePeriod, PricingStartDate, thisdate)

				' Calculate This-Period Index and Fund Return 

				Dim BenchmarkWeightedReturn As Double = 0
				Dim BenchmarkPriceReturn As Double = 0
				Dim BenchmarkReturnToUse As Double = 0

				Dim FundWeightedReturn As Double = 0 ' Just as an error check...
				Dim FundPriceReturn As Double = 0
				Dim FundPriceReturn_FR1 As Double = 0

				Dim ThisInstrument As clsInstrumentDetails

				For Each ThisInstrument In BenchmarkSectorPriceSeries
					If (Not (ThisInstrument Is Nothing)) Then
						BenchmarkWeightedReturn += (ThisInstrument.PricingData(PricingIndex).WeightValue * ThisInstrument.PricingData(PricingIndex).ReturnValue)
					End If
				Next

				For Each ThisInstrument In FundSectorPriceSeries
					If (Not (ThisInstrument Is Nothing)) Then
						FundWeightedReturn += (ThisInstrument.PricingData(PricingIndex).WeightValue * ThisInstrument.PricingData(PricingIndex).ReturnValue)
					End If
				Next

				BenchmarkReturnToUse = BenchmarkWeightedReturn
				If (Not (BenchmarkPriceSeries.PricingData Is Nothing)) AndAlso (BenchmarkPriceSeries.PricingData.Length > PricingIndex) Then
					BenchmarkPriceReturn = BenchmarkPriceSeries.PricingData(PricingIndex).PriceReturn
					BenchmarkReturnToUse = BenchmarkPriceReturn	' Give preference to Price Return rather than calculated return 
				End If

				FundPriceReturn = FundPriceSeries.PricingData(PricingIndex).PriceReturn
				FundPriceReturn_FR1 = (FundPriceSeries.PricingData(PricingIndex - 1).PriceValue / FundPriceSeries.PricingData(StartIndex - 1).PriceValue) - 1.0

				Dim ThisResultsBenchmarkSectorInstrument As clsInstrumentDetails
				Dim ThisResultsFundSectorInstrument As clsInstrumentDetails
				Dim InstrumentCounter As Integer
				Dim ThisFundWeight As Double
				Dim ThisIndexWeight As Double
				Dim ThisIndexReturn As Double
				Dim ThisFundReturn As Double
				Dim ThisPeriodSectorNetAttribution As Double
				Dim ThisPeriodStockNetAttribution As Double

				For Each thisBenchmarkResultsRow In BenchmarkAttributionResults.Rows
					ThisResultsBenchmarkSectorInstrument = Nothing
					ThisResultsFundSectorInstrument = Nothing
					ThisFundWeight = 0
					ThisIndexWeight = 0
					ThisIndexReturn = 0
					ThisFundReturn = 0


					For InstrumentCounter = 0 To (BenchmarkSectorPriceSeries.Length - 1)
						If (BenchmarkSectorPriceSeries(InstrumentCounter).InstrumentID = CInt(thisBenchmarkResultsRow("IndexInstrumentID"))) Then
							ThisResultsBenchmarkSectorInstrument = BenchmarkSectorPriceSeries(InstrumentCounter)

							If CBool(thisBenchmarkResultsRow("IndexIsPresent")) = True Then
								ThisIndexWeight = ThisResultsBenchmarkSectorInstrument.PricingData(PricingIndex).WeightValue
							End If

							ThisIndexReturn = ThisResultsBenchmarkSectorInstrument.PricingData(PricingIndex).ReturnValue
						End If
					Next

					For InstrumentCounter = 0 To (FundSectorPriceSeries.Length - 1)
						If (FundSectorPriceSeries(InstrumentCounter).InstrumentID = CInt(thisBenchmarkResultsRow("FundTypeID"))) Then
							ThisResultsFundSectorInstrument = FundSectorPriceSeries(InstrumentCounter)

							If CBool(thisBenchmarkResultsRow("FundIsPresent")) = True Then
								ThisFundWeight = ThisResultsFundSectorInstrument.PricingData(PricingIndex).WeightValue
							End If

							ThisFundReturn = ThisResultsFundSectorInstrument.PricingData(PricingIndex).ReturnValue

						End If
					Next

					' Stock Picking Attribution = 10000* [Fund Weight] *( [Fund Sector Return] - [Index Sector Return])
					' Sector Picking Attribution = 10000*([Fund Weight]-[Index Weight])*([Sector % Return])
					' NT2 = (NT1 * (1 + IR2)) + (N2 * (1 + FR1))

					'  thisBenchmarkResultsRow("SectorSelectionAttribution") = CDbl(thisBenchmarkResultsRow("SectorSelectionAttribution")) + (10000.0# * (ThisFundWeight - ThisIndexWeight) * (ThisIndexReturn - BenchmarkReturnToUse))
					'  thisBenchmarkResultsRow("StockPickingAttribution") = CDbl(thisBenchmarkResultsRow("StockPickingAttribution")) + (10000.0# * ThisFundWeight * (ThisFundReturn - ThisIndexReturn))

					ThisPeriodSectorNetAttribution = 10000.0 * (ThisFundWeight - ThisIndexWeight) * (ThisIndexReturn)
					ThisPeriodStockNetAttribution = 10000.0 * ThisFundWeight * (ThisFundReturn - ThisIndexReturn)

					thisBenchmarkResultsRow("SectorSelectionAttribution") = (CDbl(thisBenchmarkResultsRow("SectorSelectionAttribution")) * (1.0 + BenchmarkPriceReturn)) + (ThisPeriodSectorNetAttribution * (1.0 + FundPriceReturn_FR1))
					thisBenchmarkResultsRow("StockPickingAttribution") = (CDbl(thisBenchmarkResultsRow("StockPickingAttribution")) * (1.0 + BenchmarkPriceReturn)) + (ThisPeriodStockNetAttribution * (1.0 + FundPriceReturn_FR1))
				Next

				thisdate = AddPeriodToDate(PricePeriod, thisdate)
			End While

			' Set Overall / Aggregated fields :-

			For Each thisBenchmarkResultsRow In BenchmarkAttributionResults.Rows
				' Relative Weight
				If (CDbl(thisBenchmarkResultsRow("FundAverageWeight")) > CDbl(thisBenchmarkResultsRow("IndexAverageWeight"))) Then ' thisBenchmarkResultsRow
					thisBenchmarkResultsRow("RelativeWeight") = "OverWeight"
				Else
					thisBenchmarkResultsRow("RelativeWeight") = "UnderWeight"
				End If

				thisBenchmarkResultsRow("NetAttribution") = CDbl(thisBenchmarkResultsRow("SectorSelectionAttribution")) + CDbl(thisBenchmarkResultsRow("StockPickingAttribution"))
			Next

		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error setting Attribution fields.", ex.StackTrace, True)
			Exit Sub
		End Try


		MainForm.SetToolStripText(Label_Status, "Displaying Report")

		GlobalBenchmarkAttributionResults = BenchmarkAttributionResults

		Dim ReportFields(8) As classReportFieldValue
		Dim ReportDescription1 As String = ""
		Dim ReportDescription2 As String = ""

		Try
			Dim StartIndex As Integer
			Dim EndIndex As Integer
			StartIndex = GetPriceIndex(PricePeriod, PricingStartDate, StartDate)
			EndIndex = GetPriceIndex(PricePeriod, PricingStartDate, EndDate)

			ReportFields(0) = New classReportFieldValue("Text_GrandTotalFundReturn", CDbl((FundPriceSeries.PricingData(EndIndex).PriceValue / FundPriceSeries.PricingData(StartIndex - 1).PriceValue) - 1.0).ToString("#,##0.00%"))
			ReportFields(1) = New classReportFieldValue("Text_GrandTotalIndexReturn", CDbl((BenchmarkPriceSeries.PricingData(EndIndex).PriceValue / BenchmarkPriceSeries.PricingData(StartIndex - 1).PriceValue) - 1.0).ToString("#,##0.00%"))

		Catch ex As Exception
		End Try

		If MainForm.Main_Knowledgedate.CompareTo(KNOWLEDGEDATE_NOW) <= 0 Then
			ReportFields(2) = New classReportFieldValue("Text_KnowledgeDate", "Live")
		ElseIf MainForm.Main_Knowledgedate.TimeOfDay.TotalSeconds >= LAST_SECOND Then
			ReportFields(2) = New classReportFieldValue("Text_KnowledgeDate", MainForm.Main_Knowledgedate.ToString(REPORT_DATEFORMAT))
		Else
			ReportFields(2) = New classReportFieldValue("Text_KnowledgeDate", MainForm.Main_Knowledgedate.ToString(REPORT_LONGDATEFORMAT))
		End If

		ReportDescription1 = MainForm.GetControlText(Combo_FundName)

		ReportDescription1 &= " - " & StartDate.ToString(REPORT_MonthAndYear_DATEFORMAT) & " to " & EndDate.ToString(REPORT_MonthAndYear_DATEFORMAT)
		If (CurrencyModifierInUse = True) Then
			ReportDescription1 &= ", Currency Hedged"
		End If

		ReportFields(3) = New classReportFieldValue("Label_ReportDescription1", ReportDescription1)
		ReportFields(4) = New classReportFieldValue("Label_ReportDescription2", ReportDescription2)
		ReportFields(5) = New classReportFieldValue("Text_Footer1", "Data as of " & Now.Date.ToString(REPORT_DATEFORMAT))
		If (MainForm.GetComboSelectedIndex(Combo_BenchmarkPriceSeries) > 0) AndAlso (MainForm.GetControlText(Combo_BenchmarkPriceSeries).Length > 0) Then
			ReportFields(6) = New classReportFieldValue("Text_Footer2", "Index : " & MainForm.GetControlText(Combo_BenchmarkPriceSeries))
		Else
			ReportFields(6) = New classReportFieldValue("Text_Footer2", "Index : " & MainForm.GetControlText(Combo_IndexPortfolio))
		End If
		ReportFields(7) = New classReportFieldValue("Text_Footer5", "Index weightings up to " & LatestBenchmarkPortfolio.ToString(REPORT_MonthAndYear_DATEFORMAT) & " used.")

		ReportFields(8) = New classReportFieldValue("Text_Footer6", "")

		Call MainForm.MainReportHandler.DisplayReport("rptBenchmarkAttribution", BenchmarkAttributionResults, FundIndex, ReportFields, Nothing)

		MainForm.SetToolStripText(Label_Status, "")

	End Sub


    ''' <summary>
    ''' Handles the Click event of the btnClose control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
		' *****************************************************************************
		' Close Form
		' *****************************************************************************

		Me.Close()

	End Sub

#End Region


End Class
