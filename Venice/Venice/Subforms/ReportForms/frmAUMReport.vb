' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmAUMReport.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals


''' <summary>
''' Class frmAUMReport
''' </summary>
Public Class frmAUMReport

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmAUMReport"/> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The BTN run report
    ''' </summary>
  Friend WithEvents btnRunReport As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN close
    ''' </summary>
  Friend WithEvents btnClose As System.Windows.Forms.Button
    ''' <summary>
    ''' The date_ value date
    ''' </summary>
  Friend WithEvents Date_ValueDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The label_ trade date
    ''' </summary>
  Friend WithEvents Label_TradeDate As System.Windows.Forms.Label
    ''' <summary>
    ''' The label1
    ''' </summary>
  Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ leverage
    ''' </summary>
  Friend WithEvents Combo_Leverage As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The form_ status strip
    ''' </summary>
  Friend WithEvents Form_StatusStrip As System.Windows.Forms.StatusStrip
    ''' <summary>
    ''' The form_ progress bar
    ''' </summary>
  Friend WithEvents Form_ProgressBar As System.Windows.Forms.ToolStripProgressBar
    ''' <summary>
    ''' The label_ status
    ''' </summary>
  Friend WithEvents Label_Status As System.Windows.Forms.ToolStripStatusLabel
    ''' <summary>
    ''' The check_ show detail
    ''' </summary>
	Friend WithEvents Check_ShowDetail As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ accounting report
    ''' </summary>
	Friend WithEvents Check_AccountingReport As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The radio_ show net flows report
    ''' </summary>
	Friend WithEvents Radio_ShowNetFlowsReport As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The radio_ show AUM report
    ''' </summary>
	Friend WithEvents Radio_ShowAUMReport As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The group_ AUM report
    ''' </summary>
	Friend WithEvents Group_AUMReport As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The check_ include internal counterparties
    ''' </summary>
	Friend WithEvents Check_IncludeInternalCounterparties As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ omit exempt AUM instruments
    ''' </summary>
	Friend WithEvents Check_OmitExemptAUMInstruments As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The group_ AUM flows
    ''' </summary>
	Friend WithEvents Group_AUMFlows As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The check_ leverage breakdown
    ''' </summary>
	Friend WithEvents Check_LeverageBreakdown As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ display disclaimer
    ''' </summary>
	Friend WithEvents Check_DisplayDisclaimer As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ display disclaimer1
    ''' </summary>
  Friend WithEvents Check_DisplayDisclaimer1 As System.Windows.Forms.CheckBox
  Friend WithEvents Panel_Unused As System.Windows.Forms.Panel
    ''' <summary>
    ''' The check_ use posted MS prices
    ''' </summary>
	Friend WithEvents Check_UsePostedMSPrices As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.btnRunReport = New System.Windows.Forms.Button
    Me.btnClose = New System.Windows.Forms.Button
    Me.Date_ValueDate = New System.Windows.Forms.DateTimePicker
    Me.Label_TradeDate = New System.Windows.Forms.Label
    Me.Label1 = New System.Windows.Forms.Label
    Me.Combo_Leverage = New System.Windows.Forms.ComboBox
    Me.Check_UsePostedMSPrices = New System.Windows.Forms.CheckBox
    Me.Form_StatusStrip = New System.Windows.Forms.StatusStrip
    Me.Form_ProgressBar = New System.Windows.Forms.ToolStripProgressBar
    Me.Label_Status = New System.Windows.Forms.ToolStripStatusLabel
    Me.Check_ShowDetail = New System.Windows.Forms.CheckBox
    Me.Check_AccountingReport = New System.Windows.Forms.CheckBox
    Me.Radio_ShowNetFlowsReport = New System.Windows.Forms.RadioButton
    Me.Radio_ShowAUMReport = New System.Windows.Forms.RadioButton
    Me.Group_AUMReport = New System.Windows.Forms.GroupBox
    Me.Check_DisplayDisclaimer = New System.Windows.Forms.CheckBox
    Me.Check_OmitExemptAUMInstruments = New System.Windows.Forms.CheckBox
    Me.Check_IncludeInternalCounterparties = New System.Windows.Forms.CheckBox
    Me.Group_AUMFlows = New System.Windows.Forms.GroupBox
    Me.Check_DisplayDisclaimer1 = New System.Windows.Forms.CheckBox
    Me.Check_LeverageBreakdown = New System.Windows.Forms.CheckBox
    Me.Panel_Unused = New System.Windows.Forms.Panel
    Me.Form_StatusStrip.SuspendLayout()
    Me.Group_AUMReport.SuspendLayout()
    Me.Group_AUMFlows.SuspendLayout()
    Me.Panel_Unused.SuspendLayout()
    Me.SuspendLayout()
    '
    'btnRunReport
    '
    Me.btnRunReport.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnRunReport.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnRunReport.Location = New System.Drawing.Point(51, 234)
    Me.btnRunReport.Name = "btnRunReport"
    Me.btnRunReport.Size = New System.Drawing.Size(148, 28)
    Me.btnRunReport.TabIndex = 6
    Me.btnRunReport.Text = "Run Report"
    '
    'btnClose
    '
    Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnClose.Location = New System.Drawing.Point(267, 234)
    Me.btnClose.Name = "btnClose"
    Me.btnClose.Size = New System.Drawing.Size(75, 28)
    Me.btnClose.TabIndex = 7
    Me.btnClose.Text = "&Close"
    '
    'Date_ValueDate
    '
    Me.Date_ValueDate.Location = New System.Drawing.Point(140, 8)
    Me.Date_ValueDate.Name = "Date_ValueDate"
    Me.Date_ValueDate.Size = New System.Drawing.Size(160, 20)
    Me.Date_ValueDate.TabIndex = 0
    '
    'Label_TradeDate
    '
    Me.Label_TradeDate.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label_TradeDate.Location = New System.Drawing.Point(16, 12)
    Me.Label_TradeDate.Name = "Label_TradeDate"
    Me.Label_TradeDate.Size = New System.Drawing.Size(100, 16)
    Me.Label_TradeDate.TabIndex = 102
    Me.Label_TradeDate.Text = "Valuation Date"
    '
    'Label1
    '
    Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label1.Location = New System.Drawing.Point(16, 40)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(116, 16)
    Me.Label1.TabIndex = 104
    Me.Label1.Text = "Leverage"
    '
    'Combo_Leverage
    '
    Me.Combo_Leverage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_Leverage.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Leverage.Items.AddRange(New Object() {"Include NO Leverage value", "Include Debt, NOT Equity value", "Include WHOLE Leverage value"})
    Me.Combo_Leverage.Location = New System.Drawing.Point(140, 36)
    Me.Combo_Leverage.Name = "Combo_Leverage"
    Me.Combo_Leverage.Size = New System.Drawing.Size(160, 21)
    Me.Combo_Leverage.TabIndex = 1
    '
    'Check_UsePostedMSPrices
    '
    Me.Check_UsePostedMSPrices.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_UsePostedMSPrices.Location = New System.Drawing.Point(6, 41)
    Me.Check_UsePostedMSPrices.Name = "Check_UsePostedMSPrices"
    Me.Check_UsePostedMSPrices.Size = New System.Drawing.Size(336, 16)
    Me.Check_UsePostedMSPrices.TabIndex = 2
    Me.Check_UsePostedMSPrices.Text = "Use posted Milestone prices to value Fund Units."
    '
    'Form_StatusStrip
    '
    Me.Form_StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Form_ProgressBar, Me.Label_Status})
    Me.Form_StatusStrip.Location = New System.Drawing.Point(0, 279)
    Me.Form_StatusStrip.Name = "Form_StatusStrip"
    Me.Form_StatusStrip.Size = New System.Drawing.Size(392, 22)
    Me.Form_StatusStrip.TabIndex = 105
    Me.Form_StatusStrip.Text = " "
    '
    'Form_ProgressBar
    '
    Me.Form_ProgressBar.Maximum = 20
    Me.Form_ProgressBar.Name = "Form_ProgressBar"
    Me.Form_ProgressBar.Size = New System.Drawing.Size(150, 17)
    Me.Form_ProgressBar.Step = 1
    Me.Form_ProgressBar.Visible = False
    '
    'Label_Status
    '
    Me.Label_Status.Name = "Label_Status"
    Me.Label_Status.Size = New System.Drawing.Size(10, 17)
    Me.Label_Status.Text = " "
    '
    'Check_ShowDetail
    '
    Me.Check_ShowDetail.Checked = True
    Me.Check_ShowDetail.CheckState = System.Windows.Forms.CheckState.Checked
    Me.Check_ShowDetail.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_ShowDetail.Location = New System.Drawing.Point(3, 16)
    Me.Check_ShowDetail.Name = "Check_ShowDetail"
    Me.Check_ShowDetail.Size = New System.Drawing.Size(157, 16)
    Me.Check_ShowDetail.TabIndex = 1
    Me.Check_ShowDetail.Text = "Show Counterparty Detail"
    Me.Check_ShowDetail.Visible = False
    '
    'Check_AccountingReport
    '
    Me.Check_AccountingReport.Checked = True
    Me.Check_AccountingReport.CheckState = System.Windows.Forms.CheckState.Checked
    Me.Check_AccountingReport.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_AccountingReport.Location = New System.Drawing.Point(3, 3)
    Me.Check_AccountingReport.Name = "Check_AccountingReport"
    Me.Check_AccountingReport.Size = New System.Drawing.Size(166, 16)
    Me.Check_AccountingReport.TabIndex = 0
    Me.Check_AccountingReport.Text = "Show Accounting Report"
    Me.Check_AccountingReport.Visible = False
    '
    'Radio_ShowNetFlowsReport
    '
    Me.Radio_ShowNetFlowsReport.AutoSize = True
    Me.Radio_ShowNetFlowsReport.Location = New System.Drawing.Point(12, 194)
    Me.Radio_ShowNetFlowsReport.Name = "Radio_ShowNetFlowsReport"
    Me.Radio_ShowNetFlowsReport.Size = New System.Drawing.Size(164, 17)
    Me.Radio_ShowNetFlowsReport.TabIndex = 5
    Me.Radio_ShowNetFlowsReport.TabStop = True
    Me.Radio_ShowNetFlowsReport.Text = "Show AUM NET Flows report"
    Me.Radio_ShowNetFlowsReport.UseVisualStyleBackColor = True
    '
    'Radio_ShowAUMReport
    '
    Me.Radio_ShowAUMReport.AutoSize = True
    Me.Radio_ShowAUMReport.Location = New System.Drawing.Point(12, 68)
    Me.Radio_ShowAUMReport.Name = "Radio_ShowAUMReport"
    Me.Radio_ShowAUMReport.Size = New System.Drawing.Size(114, 17)
    Me.Radio_ShowAUMReport.TabIndex = 2
    Me.Radio_ShowAUMReport.TabStop = True
    Me.Radio_ShowAUMReport.Text = "Show AUM Report"
    Me.Radio_ShowAUMReport.UseVisualStyleBackColor = True
    '
    'Group_AUMReport
    '
    Me.Group_AUMReport.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Group_AUMReport.Controls.Add(Me.Check_OmitExemptAUMInstruments)
    Me.Group_AUMReport.Controls.Add(Me.Check_UsePostedMSPrices)
    Me.Group_AUMReport.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Group_AUMReport.Location = New System.Drawing.Point(29, 91)
    Me.Group_AUMReport.Name = "Group_AUMReport"
    Me.Group_AUMReport.Size = New System.Drawing.Size(351, 83)
    Me.Group_AUMReport.TabIndex = 4
    Me.Group_AUMReport.TabStop = False
    '
    'Check_DisplayDisclaimer
    '
    Me.Check_DisplayDisclaimer.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_DisplayDisclaimer.Location = New System.Drawing.Point(3, 33)
    Me.Check_DisplayDisclaimer.Name = "Check_DisplayDisclaimer"
    Me.Check_DisplayDisclaimer.Size = New System.Drawing.Size(115, 16)
    Me.Check_DisplayDisclaimer.TabIndex = 5
    Me.Check_DisplayDisclaimer.Text = "Show Subscriptions / Redemptions Disclaimer"
    Me.Check_DisplayDisclaimer.Visible = False
    '
    'Check_OmitExemptAUMInstruments
    '
    Me.Check_OmitExemptAUMInstruments.Enabled = False
    Me.Check_OmitExemptAUMInstruments.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_OmitExemptAUMInstruments.Location = New System.Drawing.Point(6, 19)
    Me.Check_OmitExemptAUMInstruments.Name = "Check_OmitExemptAUMInstruments"
    Me.Check_OmitExemptAUMInstruments.Size = New System.Drawing.Size(336, 16)
    Me.Check_OmitExemptAUMInstruments.TabIndex = 4
    Me.Check_OmitExemptAUMInstruments.Text = "Don't Include Instruments marked for exclusion from the AUM"
    '
    'Check_IncludeInternalCounterparties
    '
    Me.Check_IncludeInternalCounterparties.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_IncludeInternalCounterparties.Location = New System.Drawing.Point(3, 55)
    Me.Check_IncludeInternalCounterparties.Name = "Check_IncludeInternalCounterparties"
    Me.Check_IncludeInternalCounterparties.Size = New System.Drawing.Size(129, 16)
    Me.Check_IncludeInternalCounterparties.TabIndex = 3
    Me.Check_IncludeInternalCounterparties.Text = "Include Internal Counterparties (Funds)"
    Me.Check_IncludeInternalCounterparties.Visible = False
    '
    'Group_AUMFlows
    '
    Me.Group_AUMFlows.Controls.Add(Me.Check_DisplayDisclaimer1)
    Me.Group_AUMFlows.Controls.Add(Me.Check_LeverageBreakdown)
    Me.Group_AUMFlows.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Group_AUMFlows.Location = New System.Drawing.Point(3, 77)
    Me.Group_AUMFlows.Name = "Group_AUMFlows"
    Me.Group_AUMFlows.Size = New System.Drawing.Size(166, 43)
    Me.Group_AUMFlows.TabIndex = 106
    Me.Group_AUMFlows.TabStop = False
    Me.Group_AUMFlows.Visible = False
    '
    'Check_DisplayDisclaimer1
    '
    Me.Check_DisplayDisclaimer1.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_DisplayDisclaimer1.Location = New System.Drawing.Point(6, 33)
    Me.Check_DisplayDisclaimer1.Name = "Check_DisplayDisclaimer1"
    Me.Check_DisplayDisclaimer1.Size = New System.Drawing.Size(75, 16)
    Me.Check_DisplayDisclaimer1.TabIndex = 6
    Me.Check_DisplayDisclaimer1.Text = "Show Subscriptions / Redemptions Disclaimer"
    '
    'Check_LeverageBreakdown
    '
    Me.Check_LeverageBreakdown.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_LeverageBreakdown.Location = New System.Drawing.Point(6, 11)
    Me.Check_LeverageBreakdown.Name = "Check_LeverageBreakdown"
    Me.Check_LeverageBreakdown.Size = New System.Drawing.Size(75, 16)
    Me.Check_LeverageBreakdown.TabIndex = 0
    Me.Check_LeverageBreakdown.Text = "Show Leverage breakdown"
    '
    'Panel_Unused
    '
    Me.Panel_Unused.Controls.Add(Me.Check_DisplayDisclaimer)
    Me.Panel_Unused.Controls.Add(Me.Check_IncludeInternalCounterparties)
    Me.Panel_Unused.Controls.Add(Me.Group_AUMFlows)
    Me.Panel_Unused.Controls.Add(Me.Check_AccountingReport)
    Me.Panel_Unused.Controls.Add(Me.Check_ShowDetail)
    Me.Panel_Unused.Location = New System.Drawing.Point(321, 8)
    Me.Panel_Unused.Name = "Panel_Unused"
    Me.Panel_Unused.Size = New System.Drawing.Size(36, 34)
    Me.Panel_Unused.TabIndex = 107
    Me.Panel_Unused.Visible = False
    '
    'frmAUMReport
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(392, 301)
    Me.Controls.Add(Me.Panel_Unused)
    Me.Controls.Add(Me.Group_AUMReport)
    Me.Controls.Add(Me.Radio_ShowAUMReport)
    Me.Controls.Add(Me.Radio_ShowNetFlowsReport)
    Me.Controls.Add(Me.Form_StatusStrip)
    Me.Controls.Add(Me.Combo_Leverage)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.Date_ValueDate)
    Me.Controls.Add(Me.Label_TradeDate)
    Me.Controls.Add(Me.btnClose)
    Me.Controls.Add(Me.btnRunReport)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frmAUMReport"
    Me.Text = "Assets Under Management Report"
    Me.Form_StatusStrip.ResumeLayout(False)
    Me.Form_StatusStrip.PerformLayout()
    Me.Group_AUMReport.ResumeLayout(False)
    Me.Group_AUMFlows.ResumeLayout(False)
    Me.Panel_Unused.ResumeLayout(False)
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "


	' Form 'Parent', the Main Venice form.
	' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
	Private WithEvents _MainForm As VeniceMain

	' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
	Private FormTooltip As New ToolTip()

    ''' <summary>
    ''' The report worker
    ''' </summary>
	Private ReportWorker As BackgroundWorkerThreadSafe = Nothing
    ''' <summary>
    ''' The form controls
    ''' </summary>
	Private FormControls As ArrayList = Nothing
    ''' <summary>
    ''' The report timer
    ''' </summary>
	Private WithEvents ReportTimer As New Windows.Forms.Timer()

	' Form Constants, specific to the table being updated.
    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
	Private ALWAYS_CLOSE_THIS_FORM As Boolean = True

	' The standard ChangeID for this form. e.g. tblPerson
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
	Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

	' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
	Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
	Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

	' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
	Private THIS_FORM_FormID As VeniceFormID

	' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
	Private FormIsValid As Boolean
    ''' <summary>
    ''' The form changed
    ''' </summary>
	Private FormChanged As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
	Private _FormOpenFailed As Boolean

	' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
	Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
	Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
	Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
	Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
	Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
	Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return False
		End Get
		Set(ByVal Value As Boolean)
		End Set
	End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
	Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
		Get
			Return False
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
		Get
			Return True
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmAUMReport"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
	Public Sub New(ByVal pMainForm As VeniceMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()

		_MainForm = pMainForm
		AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate
		AddHandler ReportTimer.Tick, AddressOf MainForm.IncrementStatusBarTimerEvent

		_FormOpenFailed = False

		' ******************************************************
		' Form Specific Settings :
		' ******************************************************

		' Form Permissioning :-

		THIS_FORM_PermissionArea = Me.Name
		THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

		' 'This' form ID

		THIS_FORM_FormID = VeniceFormID.frmAUMReport

		' Format Event Handlers for form controls

		AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

		' ******************************************************
		' End Form Specific.
		' ******************************************************

		THIS_FORM_ChangeID = RenaissanceChangeID.KnowledgeDate

	End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

	' Form Initialisation code.
	'
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
	Public Sub ResetForm() Implements StandardVeniceForm.ResetForm

		Call Form_Load(Me, New System.EventArgs)
	End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
	Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub

    ''' <summary>
    ''' Handles the FormClosing event of the frmAUMReport control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.FormClosingEventArgs"/> instance containing the event data.</param>
	Private Sub frmAUMReport_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
		If (ReportWorker IsNot Nothing) Then
			If ReportWorker.IsBusy Then
				e.Cancel = True
				Exit Sub
			End If

			If (ReportTimer.Enabled) Then
				Try
					ReportTimer.Stop()
				Catch ex As Exception
				End Try
			End If

			Try
				RemoveHandler ReportTimer.Tick, AddressOf MainForm.IncrementStatusBarTimerEvent
				RemoveHandler ReportWorker.DoWork, AddressOf ReportWorker_DoWork
				RemoveHandler ReportWorker.RunWorkerCompleted, AddressOf ReportWorkerCompleted
				ReportWorker.Dispose()
				ReportWorker = Nothing
			Catch ex As Exception
			End Try

		End If

	End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
		_FormOpenFailed = False

		If Not (MainForm Is Nothing) Then
			FormIsValid = True
		Else
			MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
			_FormOpenFailed = True
			Exit Sub
		End If


		' Check User permissions
		Call CheckPermissions()
		If (HasReadPermission = False) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

			FormIsValid = False
			_FormOpenFailed = True
		End If

		' Build Combos

		Try

			'	Check_OmitExemptAUMInstruments.Checked = True

			Me.Combo_Leverage.SelectedIndex = 1
			MainForm.SetComboSelectionLengths(Me)

		Catch ex As Exception
		End Try

		Me.Radio_ShowAUMReport.Checked = True

	End Sub

    ''' <summary>
    ''' Handles the Closing event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
	Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		Dim HideForm As Boolean

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...


		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else
			HideForm = True
			If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.ReportForm_CacheCount Then
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			e.Cancel = True
		Else
			Try
				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

			Catch ex As Exception
			End Try
		End If

	End Sub


#End Region

    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		' Routine to handle changes / updates to tables by this and other windows.
		' If this, or any other, form posts a change to a table, then it will invoke an update event 
		' detailing what tables have been altered.
		' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
		' the 'VeniceAutoUpdate' event of the main Venice form.
		' Each form may them react as appropriate to changes in any table that might impact it.
		'

		Dim KnowledgeDateChanged As Boolean
		Dim SetButtonStatus_Flag As Boolean

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		KnowledgeDateChanged = False
		SetButtonStatus_Flag = False

		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
			KnowledgeDateChanged = True
		End If

		' ****************************************************************
		' Check for changes relevant to this form
		' ****************************************************************

		' Changes to the KnowledgeDate :-
		If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
			SetButtonStatus_Flag = True
		End If

		' Changes to the tblUserPermissions table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

			' Check ongoing permissions.

			Call CheckPermissions()
			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

				FormIsValid = False
				Me.Close()
				Exit Sub
			End If

			SetButtonStatus_Flag = True

		End If

		' ****************************************************************
		'
		' ****************************************************************


	End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

	' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
	Private Sub CheckPermissions()
		' *****************************************************************************
		'
		' *****************************************************************************

		Dim Permissions As Integer

		Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

		HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
		HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
		HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
		HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

	End Sub


#End Region

#Region " Set Form Combos (Form Specific Code) "



#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "


    ''' <summary>
    ''' Handles the Click event of the btnRunReport control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnRunReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRunReport.Click
		' *****************************************************************************
		' Run Report
		' *****************************************************************************

		'Dim ValueDate As Date
		Dim PriceVariant As Integer = 0

		Try
			Form_ProgressBar.Value = Form_ProgressBar.Minimum
			FormControls = MainForm.MainReportHandler.DisableFormControls(Me)

			If (Check_OmitExemptAUMInstruments.Checked) Then
				PriceVariant = 10	' Default Valuation type, plus Exempt AUM Exempt Instruments.
			End If

			'ValueDate = Me.Date_ValueDate.Value.Date

			MainForm.SetToolStripText(Label_Status, "Processing Report")

			If (ReportWorker Is Nothing) Then
				ReportWorker = New BackgroundWorkerThreadSafe(Nothing)
				AddHandler ReportWorker.DoWork, AddressOf ReportWorker_DoWork
				AddHandler ReportWorker.RunWorkerCompleted, AddressOf ReportWorkerCompleted
			End If

			ReportTimer.Interval = 25
			ReportTimer.Tag = Me.Form_ProgressBar
			ReportTimer.Start()

			If MainForm.UseReportWorkerThreads Then
				ReportWorker.RunWorkerAsync()
			Else
        MainForm.MainReportHandler.rptAssetsUnderManagement(MainForm.GetDatetimeValue(Date_ValueDate).Date, DEFAULT_STATUSGROUPFILTER, DEFAULT_ADMINISTRATORDATESFILTER, MainForm.GetComboSelectedIndex(Combo_Leverage), PriceVariant, MainForm.GetRadioChecked(Me.Radio_ShowNetFlowsReport), MainForm.GetCheckBoxChecked(Check_LeverageBreakdown), MainForm.GetCheckBoxChecked(Check_AccountingReport), MainForm.GetCheckBoxChecked(Check_UsePostedMSPrices), MainForm.GetCheckBoxChecked(Check_ShowDetail), MainForm.GetCheckBoxChecked(Check_IncludeInternalCounterparties), MainForm.GetCheckBoxChecked(Check_DisplayDisclaimer), Label_Status, Nothing)    ' Form_ProgressBar
				Call ReportWorkerCompleted(Nothing, Nothing)
			End If


		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)

		End Try

	End Sub

    ''' <summary>
    ''' Handles the DoWork event of the ReportWorker control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.DoWorkEventArgs"/> instance containing the event data.</param>
	Private Sub ReportWorker_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs)	' Handles backgroundWorker1.DoWork
		' *****************************************************************************
		'
		'
		' *****************************************************************************
		Dim PriceVariant As Integer = 0

		If (MainForm.GetCheckBoxChecked(Check_OmitExemptAUMInstruments)) Then
			PriceVariant = 10	' Default Valuation type, plus Exempt AUM Exampt Instruments.
		End If

		MainForm.MainReportHandler.rptAssetsUnderManagement(MainForm.GetDatetimeValue(Date_ValueDate).Date, DEFAULT_STATUSGROUPFILTER, DEFAULT_ADMINISTRATORDATESFILTER, MainForm.GetComboSelectedIndex(Combo_Leverage), PriceVariant, MainForm.GetRadioChecked(Me.Radio_ShowNetFlowsReport), MainForm.GetCheckBoxChecked(Check_LeverageBreakdown), MainForm.GetCheckBoxChecked(Check_AccountingReport), MainForm.GetCheckBoxChecked(Check_UsePostedMSPrices), MainForm.GetCheckBoxChecked(Check_ShowDetail), MainForm.GetCheckBoxChecked(Check_IncludeInternalCounterparties), MainForm.GetCheckBoxChecked(Check_DisplayDisclaimer), Label_Status, Nothing)		' Form_ProgressBar
	End Sub


    ''' <summary>
    ''' Reports the worker completed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.RunWorkerCompletedEventArgs"/> instance containing the event data.</param>
	Private Sub ReportWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs)	' Handles backgroundWorker1.RunWorkerCompleted
		' *****************************************************************************
		'
		'
		' *****************************************************************************

		Try
			ReportTimer.Stop()
			MainForm.MainReportHandler.EnableFormControls(FormControls)
			MainForm.SetToolStripText(Label_Status, "")

			'Form_ProgressBar = MainForm.ResetProgressBar(Form_StatusStrip, Form_ProgressBar)
			Form_ProgressBar.Value = Form_ProgressBar.Minimum
			Form_ProgressBar.Visible = False
			Form_ProgressBar = MainForm.ResetProgressBar(Form_StatusStrip, Form_ProgressBar)
			ReportTimer.Tag = Me.Form_ProgressBar
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
		End Try

	End Sub


    ''' <summary>
    ''' Handles the Click event of the btnClose control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
		' *****************************************************************************
		' Close Form
		' *****************************************************************************

		Me.Close()

	End Sub

#End Region

#Region " Form Control Event Code"

    ''' <summary>
    ''' Handles the CheckedChanged event of the Radio_ShowAUMReport control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Radio_ShowAUMReport_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_ShowAUMReport.CheckedChanged
		' ****************************************************************
		'
		' ****************************************************************

		If (Me.Created) AndAlso (Not Me.IsDisposed) Then
			If (Radio_ShowAUMReport.Checked) Then
				Me.Group_AUMReport.Enabled = True
				Me.Group_AUMFlows.Enabled = False
			End If
		End If

	End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the Radio_ShowNetFlowsReport control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Radio_ShowNetFlowsReport_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_ShowNetFlowsReport.CheckedChanged
		' ****************************************************************
		'
		' ****************************************************************

		If (Me.Created) AndAlso (Not Me.IsDisposed) Then
			If (Radio_ShowNetFlowsReport.Checked) Then
				Me.Group_AUMReport.Enabled = False
				Me.Group_AUMFlows.Enabled = True
			End If
		End If

	End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the Check_AccountingReport control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Check_AccountingReport_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_AccountingReport.CheckedChanged
		' ****************************************************************
		'
		' ****************************************************************

		If Me.Check_AccountingReport.Checked Then
			Check_ShowDetail.Checked = True
			Check_ShowDetail.Enabled = False
		Else
			Check_ShowDetail.Enabled = True
		End If
	End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the Check_DisplayDisclaimer control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Check_DisplayDisclaimer_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_DisplayDisclaimer.CheckedChanged
		' ****************************************************************
		'
		' ****************************************************************

		If (Check_DisplayDisclaimer.Checked <> Check_DisplayDisclaimer1.Checked) Then
			Check_DisplayDisclaimer1.Checked = Check_DisplayDisclaimer.Checked
		End If

	End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the Check_DisplayDisclaimer1 control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Check_DisplayDisclaimer1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_DisplayDisclaimer1.CheckedChanged
		' ****************************************************************
		'
		' ****************************************************************

		If (Check_DisplayDisclaimer.Checked <> Check_DisplayDisclaimer1.Checked) Then
			Check_DisplayDisclaimer.Checked = Check_DisplayDisclaimer1.Checked
		End If

	End Sub

#End Region

    ''' <summary>
    ''' Handles the CheckedChanged event of the Check_OmitExemptAUMInstruments control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Check_OmitExemptAUMInstruments_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_OmitExemptAUMInstruments.CheckedChanged
		If (Me.Created) AndAlso (Me.IsDisposed = False) Then
			'If (Check_OmitExemptAUMInstruments.Checked) Then
			'	Check_UsePostedMSPrices.Checked = False
			'	Check_UsePostedMSPrices.Enabled = False
			'Else
			'	Check_UsePostedMSPrices.Enabled = True
			'End If
		End If

	End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the Check_UsePostedMSPrices control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Check_UsePostedMSPrices_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_UsePostedMSPrices.CheckedChanged

		If (Me.Created) AndAlso (Me.IsDisposed = False) Then
			'If (Check_UsePostedMSPrices.Checked) Then
			'	Check_OmitExemptAUMInstruments.Enabled = False
			'	Check_OmitExemptAUMInstruments.Checked = False
			'Else
			'	Check_OmitExemptAUMInstruments.Enabled = True
			'End If
		End If
	End Sub


End Class
