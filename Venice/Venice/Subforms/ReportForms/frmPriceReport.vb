' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmPriceReport.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals


''' <summary>
''' Class frmPriceReport
''' </summary>
Public Class frmPriceReport

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmPriceReport"/> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The BTN run report
    ''' </summary>
  Friend WithEvents btnRunReport As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN close
    ''' </summary>
  Friend WithEvents btnClose As System.Windows.Forms.Button
    ''' <summary>
    ''' The combo_ report to run
    ''' </summary>
  Friend WithEvents Combo_ReportToRun As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label5
    ''' </summary>
  Friend WithEvents Label5 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ selected instrument
    ''' </summary>
  Friend WithEvents Combo_SelectedInstrument As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The date_ date to
    ''' </summary>
  Friend WithEvents Date_DateTo As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The label1
    ''' </summary>
  Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The date_ date from
    ''' </summary>
  Friend WithEvents Date_DateFrom As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The label_ trade date
    ''' </summary>
  Friend WithEvents Label_TradeDate As System.Windows.Forms.Label
    ''' <summary>
    ''' The form_ status strip
    ''' </summary>
  Friend WithEvents Form_StatusStrip As System.Windows.Forms.StatusStrip
    ''' <summary>
    ''' The form_ progress bar
    ''' </summary>
  Friend WithEvents Form_ProgressBar As System.Windows.Forms.ToolStripProgressBar
    ''' <summary>
    ''' The label_ status
    ''' </summary>
  Friend WithEvents Label_Status As System.Windows.Forms.ToolStripStatusLabel
    ''' <summary>
    ''' The label_ selected instrument
    ''' </summary>
  Friend WithEvents Label_SelectedInstrument As System.Windows.Forms.Label
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.btnRunReport = New System.Windows.Forms.Button
    Me.btnClose = New System.Windows.Forms.Button
    Me.Label_SelectedInstrument = New System.Windows.Forms.Label
    Me.Combo_SelectedInstrument = New System.Windows.Forms.ComboBox
    Me.Combo_ReportToRun = New System.Windows.Forms.ComboBox
    Me.Label5 = New System.Windows.Forms.Label
    Me.Date_DateTo = New System.Windows.Forms.DateTimePicker
    Me.Label1 = New System.Windows.Forms.Label
    Me.Date_DateFrom = New System.Windows.Forms.DateTimePicker
    Me.Label_TradeDate = New System.Windows.Forms.Label
    Me.Form_StatusStrip = New System.Windows.Forms.StatusStrip
    Me.Form_ProgressBar = New System.Windows.Forms.ToolStripProgressBar
    Me.Label_Status = New System.Windows.Forms.ToolStripStatusLabel
    Me.Form_StatusStrip.SuspendLayout()
    Me.SuspendLayout()
    '
    'btnRunReport
    '
    Me.btnRunReport.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnRunReport.Location = New System.Drawing.Point(44, 140)
    Me.btnRunReport.Name = "btnRunReport"
    Me.btnRunReport.Size = New System.Drawing.Size(148, 28)
    Me.btnRunReport.TabIndex = 4
    Me.btnRunReport.Text = "Run Report"
    '
    'btnClose
    '
    Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnClose.Location = New System.Drawing.Point(260, 140)
    Me.btnClose.Name = "btnClose"
    Me.btnClose.Size = New System.Drawing.Size(75, 28)
    Me.btnClose.TabIndex = 5
    Me.btnClose.Text = "&Close"
    '
    'Label_SelectedInstrument
    '
    Me.Label_SelectedInstrument.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label_SelectedInstrument.Location = New System.Drawing.Point(12, 44)
    Me.Label_SelectedInstrument.Name = "Label_SelectedInstrument"
    Me.Label_SelectedInstrument.Size = New System.Drawing.Size(120, 20)
    Me.Label_SelectedInstrument.TabIndex = 53
    Me.Label_SelectedInstrument.Text = "..."
    '
    'Combo_SelectedInstrument
    '
    Me.Combo_SelectedInstrument.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectedInstrument.Location = New System.Drawing.Point(140, 40)
    Me.Combo_SelectedInstrument.Name = "Combo_SelectedInstrument"
    Me.Combo_SelectedInstrument.Size = New System.Drawing.Size(252, 21)
    Me.Combo_SelectedInstrument.TabIndex = 1
    '
    'Combo_ReportToRun
    '
    Me.Combo_ReportToRun.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_ReportToRun.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_ReportToRun.Items.AddRange(New Object() {"Price Report", "FX Report"})
    Me.Combo_ReportToRun.Location = New System.Drawing.Point(139, 8)
    Me.Combo_ReportToRun.Name = "Combo_ReportToRun"
    Me.Combo_ReportToRun.Size = New System.Drawing.Size(252, 21)
    Me.Combo_ReportToRun.TabIndex = 0
    '
    'Label5
    '
    Me.Label5.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label5.Location = New System.Drawing.Point(11, 12)
    Me.Label5.Name = "Label5"
    Me.Label5.Size = New System.Drawing.Size(120, 20)
    Me.Label5.TabIndex = 127
    Me.Label5.Text = "Report to Run"
    '
    'Date_DateTo
    '
    Me.Date_DateTo.Location = New System.Drawing.Point(140, 100)
    Me.Date_DateTo.Name = "Date_DateTo"
    Me.Date_DateTo.Size = New System.Drawing.Size(160, 20)
    Me.Date_DateTo.TabIndex = 3
    '
    'Label1
    '
    Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label1.Location = New System.Drawing.Point(12, 104)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(116, 16)
    Me.Label1.TabIndex = 131
    Me.Label1.Text = "Date To"
    '
    'Date_DateFrom
    '
    Me.Date_DateFrom.Location = New System.Drawing.Point(140, 72)
    Me.Date_DateFrom.Name = "Date_DateFrom"
    Me.Date_DateFrom.Size = New System.Drawing.Size(160, 20)
    Me.Date_DateFrom.TabIndex = 2
    '
    'Label_TradeDate
    '
    Me.Label_TradeDate.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label_TradeDate.Location = New System.Drawing.Point(12, 76)
    Me.Label_TradeDate.Name = "Label_TradeDate"
    Me.Label_TradeDate.Size = New System.Drawing.Size(116, 16)
    Me.Label_TradeDate.TabIndex = 130
    Me.Label_TradeDate.Text = "Date From"
    '
    'Form_StatusStrip
    '
    Me.Form_StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Form_ProgressBar, Me.Label_Status})
    Me.Form_StatusStrip.Location = New System.Drawing.Point(0, 180)
    Me.Form_StatusStrip.Name = "Form_StatusStrip"
    Me.Form_StatusStrip.Size = New System.Drawing.Size(402, 22)
    Me.Form_StatusStrip.TabIndex = 132
    Me.Form_StatusStrip.Text = " "
    '
    'Form_ProgressBar
    '
    Me.Form_ProgressBar.Maximum = 20
    Me.Form_ProgressBar.Name = "Form_ProgressBar"
    Me.Form_ProgressBar.Size = New System.Drawing.Size(150, 17)
    Me.Form_ProgressBar.Step = 1
    Me.Form_ProgressBar.Visible = False
    '
    'Label_Status
    '
    Me.Label_Status.Name = "Label_Status"
    Me.Label_Status.Size = New System.Drawing.Size(10, 17)
    Me.Label_Status.Text = " "
    '
    'frmPriceReport
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(402, 202)
    Me.Controls.Add(Me.Form_StatusStrip)
    Me.Controls.Add(Me.Date_DateTo)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.Date_DateFrom)
    Me.Controls.Add(Me.Label_TradeDate)
    Me.Controls.Add(Me.Combo_ReportToRun)
    Me.Controls.Add(Me.Label5)
    Me.Controls.Add(Me.Combo_SelectedInstrument)
    Me.Controls.Add(Me.Label_SelectedInstrument)
    Me.Controls.Add(Me.btnClose)
    Me.Controls.Add(Me.btnRunReport)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frmPriceReport"
    Me.Text = "Price and FX Reports"
    Me.Form_StatusStrip.ResumeLayout(False)
    Me.Form_StatusStrip.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "


  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
  Private WithEvents _MainForm As VeniceMain

  ' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
  Private FormTooltip As New ToolTip()

    ''' <summary>
    ''' The report worker
    ''' </summary>
	Private ReportWorker As BackgroundWorkerThreadSafe = Nothing
    ''' <summary>
    ''' The form controls
    ''' </summary>
  Private FormControls As ArrayList = Nothing
    ''' <summary>
    ''' The report timer
    ''' </summary>
  Private WithEvents ReportTimer As New Windows.Forms.Timer()

  ' Form Constants, specific to the table being updated.

    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
  Private ALWAYS_CLOSE_THIS_FORM As Boolean = True

  ' The standard ChangeID for this form. e.g. tblPerson
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

  ' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
  Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
  Private THIS_FORM_FormID As VeniceFormID

  ' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
  Private FormIsValid As Boolean
    ''' <summary>
    ''' The form changed
    ''' </summary>
  Private FormChanged As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
  Private _FormOpenFailed As Boolean

  ' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
  Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
  Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
  Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
  Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
  Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
    ' Public property to return handle to the 'Main' Venice form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
  Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
    ' Public property maintaining a value indicating if the cursor is over the 'Cancel'
    ' Button on this form.
    ' This property is specifically designed for use by the field formating Event functions
    ' In order that they do not impose format restrictions if the user is about to click the 
    ' 'Cancel' button.
    '
    Get
      Return False
    End Get
    Set(ByVal Value As Boolean)
    End Set
  End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
  Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
    Get
      Return False
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
    Get
      Return True
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

    ''' <summary>
    ''' Gets or sets the report to run.
    ''' </summary>
    ''' <value>The report to run.</value>
  Public Property ReportToRun() As Integer
    Get
      Return Combo_ReportToRun.SelectedIndex
    End Get
    Set(ByVal Value As Integer)
      If (Value >= 0) AndAlso (Value < Combo_ReportToRun.Items.Count) Then
        Combo_ReportToRun.SelectedIndex = Value
      Else
        If (Combo_ReportToRun.Items.Count > 0) Then
          Combo_ReportToRun.SelectedIndex = 0
        End If
      End If
    End Set
  End Property
#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmPriceReport"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  Public Sub New(ByVal pMainForm As VeniceMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()

    _MainForm = pMainForm
    AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate
    AddHandler ReportTimer.Tick, AddressOf MainForm.IncrementStatusBarTimerEvent

    _FormOpenFailed = False

    ' ******************************************************
    ' Form Specific Settings :
    ' ******************************************************

    ' Form Permissioning :-

    THIS_FORM_PermissionArea = Me.Name
    THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

    ' 'This' form ID

    THIS_FORM_FormID = VeniceFormID.frmPriceReport

    ' Format Event Handlers for form controls

    AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

    AddHandler Combo_ReportToRun.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_ReportToRun.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_ReportToRun.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_ReportToRun.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

    AddHandler Combo_SelectedInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_SelectedInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_SelectedInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_SelectedInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

		' Set up the ToolTip
    MainForm.SetFormToolTip(Me, FormTooltip)

    ' ******************************************************
    ' End Form Specific.
    ' ******************************************************

    THIS_FORM_ChangeID = RenaissanceChangeID.KnowledgeDate

  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
  Public Sub ResetForm() Implements StandardVeniceForm.ResetForm

    Call Form_Load(Me, New System.EventArgs)
  End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
  Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
    ALWAYS_CLOSE_THIS_FORM = True
    Me.Close()
  End Sub

    ''' <summary>
    ''' Handles the FormClosing event of the frmPriceReport control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.FormClosingEventArgs"/> instance containing the event data.</param>
  Private Sub frmPriceReport_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
    If (ReportWorker IsNot Nothing) Then
      If ReportWorker.IsBusy Then
        e.Cancel = True
        Exit Sub
      End If

      Try
        'MainForm.ClearObjectEventSubscribers(ReportTimer)
        'MainForm.ClearObjectEventSubscribers(ReportWorker)

        RemoveHandler ReportTimer.Tick, AddressOf MainForm.IncrementStatusBarTimerEvent
        RemoveHandler ReportWorker.DoWork, AddressOf ReportWorker_DoWork
        RemoveHandler ReportWorker.RunWorkerCompleted, AddressOf ReportWorkerCompleted

        ReportWorker.Dispose()
        ReportWorker = Nothing
      Catch ex As Exception
      End Try
    End If

    Dim HideForm As Boolean

    ' Hide or Close this form ?
    ' All depends on how many of this form type are Open or in Cache...


    If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
      HideForm = False
    Else
      HideForm = True
      If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.ReportForm_CacheCount Then
        HideForm = False
      End If
    End If

    If HideForm = True Then
      MainForm.HideInFormsCollection(Me)
      Me.Hide() ' NPP Fix

      e.Cancel = True
    Else
      Try
        MainForm.RemoveFromFormsCollection(Me)

        'MainForm.ClearControlEventSubscribers(Me)

        RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

        RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

        RemoveHandler Combo_ReportToRun.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_ReportToRun.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_ReportToRun.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_ReportToRun.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

        RemoveHandler Combo_SelectedInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_SelectedInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_SelectedInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_SelectedInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

      Catch ex As Exception
      End Try
    End If


  End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
    _FormOpenFailed = False

    If Not (MainForm Is Nothing) Then
      FormIsValid = True
    Else
      MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If


    ' Check User permissions
    Call CheckPermissions()
    If (HasReadPermission = False) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    ' Build Combos

    Try

      If (Combo_ReportToRun.Items.Count > 0) Then
        Combo_ReportToRun.SelectedIndex = 0
      End If

      MainForm.SetComboSelectionLengths(Me)

    Catch ex As Exception
    End Try

  End Sub


#End Region

  ' Routine to handle changes / updates to tables by this and other windows.
  ' If this, or any other, form posts a change to a table, then it will invoke an update event 
  ' detailing what tables have been altered.
  ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
  ' the 'VeniceAutoUpdate' event of the main Venice form.
  ' Each form may them react as appropriate to changes in any table that might impact it.
  '
    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    Dim KnowledgeDateChanged As Boolean
    Dim SetButtonStatus_Flag As Boolean

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

    KnowledgeDateChanged = False
    SetButtonStatus_Flag = False

    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
      KnowledgeDateChanged = True
    End If

    ' ****************************************************************
    ' Check for changes relevant to this form
    ' ****************************************************************

    ' Changes to the KnowledgeDate :-
    If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
      SetButtonStatus_Flag = True
    End If

    ' Changes to the tblInstrument table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrument) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Select Case Me.Combo_ReportToRun.SelectedIndex
        Case 1

        Case Else
          Label_SelectedInstrument.Text = "Instrument"
          Call SetInstrumentCombo()

      End Select
    End If

    ' Changes to the tblCurrency table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblCurrency) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Select Case Me.Combo_ReportToRun.SelectedIndex
        Case 1
          Label_SelectedInstrument.Text = "Currency"
          Call SetCurrencyCombo()

        Case Else

      End Select
    End If

    ' Changes to the tblUserPermissions table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

      ' Check ongoing permissions.

      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

        FormIsValid = False
        Me.Close()
        Exit Sub
      End If

      SetButtonStatus_Flag = True

    End If

    ' ****************************************************************
    '
    ' ****************************************************************


  End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

  ' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
  Private Sub CheckPermissions()
    ' *****************************************************************************
    '
    ' *****************************************************************************

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

  End Sub


#End Region

#Region " Set Form Combos (Form Specific Code) "

    ''' <summary>
    ''' Sets the instrument combo.
    ''' </summary>
  Private Sub SetInstrumentCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_SelectedInstrument, _
    RenaissanceStandardDatasets.tblInstrument, _
    "InstrumentDescription", _
    "InstrumentID", _
    "", False, True, True, 0)  ' 

  End Sub

    ''' <summary>
    ''' Sets the currency combo.
    ''' </summary>
  Private Sub SetCurrencyCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_SelectedInstrument, _
    RenaissanceStandardDatasets.tblCurrency, _
    "CurrencyDescription", _
    "CurrencyID", _
    "", False, True, True, 0)  ' 

  End Sub


#End Region


#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "


    ''' <summary>
    ''' Handles the Click event of the btnRunReport control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnRunReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRunReport.Click
    ' *****************************************************************************
    ' Run Report
    ' *****************************************************************************

    Try
      FormControls = MainForm.MainReportHandler.DisableFormControls(Me)
      Form_ProgressBar.Value = Form_ProgressBar.Minimum
      MainForm.SetToolStripText(Label_Status, "Processing Report")

      If (ReportWorker Is Nothing) Then
				ReportWorker = New BackgroundWorkerThreadSafe(Nothing)
        AddHandler ReportWorker.DoWork, AddressOf ReportWorker_DoWork
        AddHandler ReportWorker.RunWorkerCompleted, AddressOf ReportWorkerCompleted
      End If

      ReportTimer.Interval = 25
      ReportTimer.Tag = Me.Form_ProgressBar
      ReportTimer.Start()


      If MainForm.UseReportWorkerThreads Then
        ReportWorker.RunWorkerAsync()
      Else
        RunReport()
        Call ReportWorkerCompleted(Nothing, Nothing)
      End If


    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
    End Try
  End Sub

    ''' <summary>
    ''' Handles the DoWork event of the ReportWorker control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.DoWorkEventArgs"/> instance containing the event data.</param>
  Private Sub ReportWorker_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) ' Handles backgroundWorker1.DoWork
    RunReport()
  End Sub

    ''' <summary>
    ''' Reports the worker completed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.RunWorkerCompletedEventArgs"/> instance containing the event data.</param>
  Private Sub ReportWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) ' Handles backgroundWorker1.RunWorkerCompleted
    Try
      ReportTimer.Stop()
      MainForm.MainReportHandler.EnableFormControls(FormControls)
      MainForm.SetToolStripText(Label_Status, "")

      'Form_ProgressBar = MainForm.ResetProgressBar(Form_StatusStrip, Form_ProgressBar)
      Form_ProgressBar.Value = Form_ProgressBar.Minimum
      Form_ProgressBar.Visible = False
      ReportTimer.Tag = Me.Form_ProgressBar
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
    End Try

  End Sub

    ''' <summary>
    ''' Runs the report.
    ''' </summary>
  Private Sub RunReport()
    ' *****************************************************************************
    ' Run Report
    ' *****************************************************************************

    Dim InstrumentID As Integer
    Dim DateFrom As Date
    Dim DateTo As Date


    ' Validate

    If (MainForm.GetComboSelectedValue(Combo_SelectedInstrument) Is Nothing) OrElse (MainForm.GetComboSelectedIndex(Combo_SelectedInstrument) < 0) Then
      InstrumentID = 0
    Else
      InstrumentID = MainForm.GetComboSelectedValue(Combo_SelectedInstrument)
    End If

    DateFrom = MainForm.GetDatetimeValue(Date_DateFrom).Date
    DateTo = MainForm.GetDatetimeValue(Date_DateTo).Date

    Select Case MainForm.GetComboSelectedIndex(Combo_ReportToRun)
      Case 1
        Try
          Call MainForm.MainReportHandler.rptFXRates(InstrumentID, DateFrom, DateTo, Nothing) ' Form_ProgressBar)
        Catch ex As Exception
        End Try

      Case Else
        Try
          Call MainForm.MainReportHandler.rptPrices(InstrumentID, DateFrom, DateTo, Nothing) ' Form_ProgressBar)
        Catch ex As Exception
        End Try

    End Select

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnClose control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
    ' *****************************************************************************
    ' Close Form
    ' *****************************************************************************

    Me.Close()

  End Sub

#End Region


    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_ReportToRun control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_ReportToRun_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_ReportToRun.SelectedIndexChanged

    Try
      Select Case Me.Combo_ReportToRun.SelectedIndex
        Case 1
          Label_SelectedInstrument.Text = "Currency"
          Call SetCurrencyCombo()

        Case Else
          Label_SelectedInstrument.Text = "Instrument"
          Call SetInstrumentCombo()

      End Select
    Catch ex As Exception
    End Try

    Try
      If (Combo_SelectedInstrument.Items.Count > 0) AndAlso (Combo_SelectedInstrument.SelectedIndex < 0) Then
        Combo_SelectedInstrument.SelectedIndex = 0
      End If
    Catch ex As Exception
    End Try

  End Sub
End Class
