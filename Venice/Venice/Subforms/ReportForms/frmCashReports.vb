' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmCashReports.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals


''' <summary>
''' Class frmCashReports
''' </summary>
Public Class frmCashReports

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmCashReports"/> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The BTN run report
    ''' </summary>
  Friend WithEvents btnRunReport As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN close
    ''' </summary>
  Friend WithEvents btnClose As System.Windows.Forms.Button
    ''' <summary>
    ''' The label2
    ''' </summary>
  Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ fund
    ''' </summary>
  Friend WithEvents Combo_Fund As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The date_ detail start
    ''' </summary>
  Friend WithEvents Date_DetailStart As System.Windows.Forms.DateTimePicker
  ''' <summary>
  ''' The combo_ legal entity
  ''' </summary>
  Friend WithEvents Combo_LegalEntity As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The combo_ report name
  ''' </summary>
  Friend WithEvents Combo_ReportName As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The label4
  ''' </summary>
  Friend WithEvents Label4 As System.Windows.Forms.Label
  ''' <summary>
  ''' The date_ price and FX
  ''' </summary>
  Friend WithEvents Date_PriceAndFX As System.Windows.Forms.DateTimePicker
  ''' <summary>
  ''' The date_ detail end
  ''' </summary>
  Friend WithEvents Date_DetailEnd As System.Windows.Forms.DateTimePicker
  ''' <summary>
  ''' The label_ start date
  ''' </summary>
  Friend WithEvents Label_StartDate As System.Windows.Forms.Label
  ''' <summary>
  ''' The label_ end date
  ''' </summary>
  Friend WithEvents Label_EndDate As System.Windows.Forms.Label
  ''' <summary>
  ''' The form_ status strip
  ''' </summary>
  Friend WithEvents Form_StatusStrip As System.Windows.Forms.StatusStrip
  ''' <summary>
  ''' The form_ progress bar
  ''' </summary>
  Friend WithEvents Form_ProgressBar As System.Windows.Forms.ToolStripProgressBar
  ''' <summary>
  ''' The label_ status
  ''' </summary>
  Friend WithEvents Label_Status As System.Windows.Forms.ToolStripStatusLabel
  ''' <summary>
  ''' The label_ price and FX
  ''' </summary>
  Friend WithEvents Label_PriceAndFX As System.Windows.Forms.Label
  ''' <summary>
  ''' Initializes the component.
  ''' </summary>
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.btnRunReport = New System.Windows.Forms.Button
    Me.btnClose = New System.Windows.Forms.Button
    Me.Label2 = New System.Windows.Forms.Label
    Me.Combo_Fund = New System.Windows.Forms.ComboBox
    Me.Date_DetailStart = New System.Windows.Forms.DateTimePicker
    Me.Label_StartDate = New System.Windows.Forms.Label
    Me.Combo_LegalEntity = New System.Windows.Forms.ComboBox
    Me.Combo_ReportName = New System.Windows.Forms.ComboBox
    Me.Label4 = New System.Windows.Forms.Label
    Me.Date_PriceAndFX = New System.Windows.Forms.DateTimePicker
    Me.Label_PriceAndFX = New System.Windows.Forms.Label
    Me.Date_DetailEnd = New System.Windows.Forms.DateTimePicker
    Me.Label_EndDate = New System.Windows.Forms.Label
    Me.Form_StatusStrip = New System.Windows.Forms.StatusStrip
    Me.Form_ProgressBar = New System.Windows.Forms.ToolStripProgressBar
    Me.Label_Status = New System.Windows.Forms.ToolStripStatusLabel
    Me.Form_StatusStrip.SuspendLayout()
    Me.SuspendLayout()
    '
    'btnRunReport
    '
    Me.btnRunReport.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnRunReport.Location = New System.Drawing.Point(44, 172)
    Me.btnRunReport.Name = "btnRunReport"
    Me.btnRunReport.Size = New System.Drawing.Size(148, 28)
    Me.btnRunReport.TabIndex = 6
    Me.btnRunReport.Text = "Run Report"
    '
    'btnClose
    '
    Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnClose.Location = New System.Drawing.Point(260, 172)
    Me.btnClose.Name = "btnClose"
    Me.btnClose.Size = New System.Drawing.Size(75, 28)
    Me.btnClose.TabIndex = 7
    Me.btnClose.Text = "&Close"
    '
    'Label2
    '
    Me.Label2.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label2.Location = New System.Drawing.Point(16, 48)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(120, 20)
    Me.Label2.TabIndex = 53
    Me.Label2.Text = "Fund Name"
    '
    'Combo_Fund
    '
    Me.Combo_Fund.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Fund.Location = New System.Drawing.Point(140, 44)
    Me.Combo_Fund.Name = "Combo_Fund"
    Me.Combo_Fund.Size = New System.Drawing.Size(252, 21)
    Me.Combo_Fund.TabIndex = 1
    '
    'Date_DetailStart
    '
    Me.Date_DetailStart.Location = New System.Drawing.Point(140, 80)
    Me.Date_DetailStart.Name = "Date_DetailStart"
    Me.Date_DetailStart.Size = New System.Drawing.Size(160, 20)
    Me.Date_DetailStart.TabIndex = 3
    '
    'Label_StartDate
    '
    Me.Label_StartDate.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label_StartDate.Location = New System.Drawing.Point(16, 84)
    Me.Label_StartDate.Name = "Label_StartDate"
    Me.Label_StartDate.Size = New System.Drawing.Size(100, 16)
    Me.Label_StartDate.TabIndex = 102
    Me.Label_StartDate.Text = "Detail Start Date"
    '
    'Combo_LegalEntity
    '
    Me.Combo_LegalEntity.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_LegalEntity.Location = New System.Drawing.Point(361, 79)
    Me.Combo_LegalEntity.Name = "Combo_LegalEntity"
    Me.Combo_LegalEntity.Size = New System.Drawing.Size(29, 21)
    Me.Combo_LegalEntity.TabIndex = 2
    Me.Combo_LegalEntity.Visible = False
    '
    'Combo_ReportName
    '
    Me.Combo_ReportName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_ReportName.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_ReportName.Items.AddRange(New Object() {"Available Cash (High Detail)", "Available Cash (Medium Detail)", "Available Cash (Low Detail)", "Valued Cash (High Detail)", "Valued Cash (Medium Detail)", "Valued Cash (Low Detail)", "Cash Movement", "Cash Move Detail (Settlement Date)", "Cash Move Detail (Value Date)"})
    Me.Combo_ReportName.Location = New System.Drawing.Point(140, 12)
    Me.Combo_ReportName.Name = "Combo_ReportName"
    Me.Combo_ReportName.Size = New System.Drawing.Size(252, 21)
    Me.Combo_ReportName.TabIndex = 0
    '
    'Label4
    '
    Me.Label4.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label4.Location = New System.Drawing.Point(16, 16)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(120, 20)
    Me.Label4.TabIndex = 108
    Me.Label4.Text = "Report Name"
    '
    'Date_PriceAndFX
    '
    Me.Date_PriceAndFX.Location = New System.Drawing.Point(140, 136)
    Me.Date_PriceAndFX.Name = "Date_PriceAndFX"
    Me.Date_PriceAndFX.Size = New System.Drawing.Size(160, 20)
    Me.Date_PriceAndFX.TabIndex = 5
    '
    'Label_PriceAndFX
    '
    Me.Label_PriceAndFX.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label_PriceAndFX.Location = New System.Drawing.Point(16, 140)
    Me.Label_PriceAndFX.Name = "Label_PriceAndFX"
    Me.Label_PriceAndFX.Size = New System.Drawing.Size(120, 16)
    Me.Label_PriceAndFX.TabIndex = 110
    Me.Label_PriceAndFX.Text = "Price and FX Date"
    '
    'Date_DetailEnd
    '
    Me.Date_DetailEnd.Location = New System.Drawing.Point(140, 108)
    Me.Date_DetailEnd.Name = "Date_DetailEnd"
    Me.Date_DetailEnd.Size = New System.Drawing.Size(160, 20)
    Me.Date_DetailEnd.TabIndex = 4
    '
    'Label_EndDate
    '
    Me.Label_EndDate.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label_EndDate.Location = New System.Drawing.Point(16, 112)
    Me.Label_EndDate.Name = "Label_EndDate"
    Me.Label_EndDate.Size = New System.Drawing.Size(100, 16)
    Me.Label_EndDate.TabIndex = 112
    Me.Label_EndDate.Text = "Detail End Date"
    '
    'Form_StatusStrip
    '
    Me.Form_StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Form_ProgressBar, Me.Label_Status})
    Me.Form_StatusStrip.Location = New System.Drawing.Point(0, 217)
    Me.Form_StatusStrip.Name = "Form_StatusStrip"
    Me.Form_StatusStrip.Size = New System.Drawing.Size(402, 22)
    Me.Form_StatusStrip.TabIndex = 126
    Me.Form_StatusStrip.Text = " "
    '
    'Form_ProgressBar
    '
    Me.Form_ProgressBar.Maximum = 20
    Me.Form_ProgressBar.Name = "Form_ProgressBar"
    Me.Form_ProgressBar.Size = New System.Drawing.Size(150, 17)
    Me.Form_ProgressBar.Step = 1
    Me.Form_ProgressBar.Visible = False
    '
    'Label_Status
    '
    Me.Label_Status.Name = "Label_Status"
    Me.Label_Status.Size = New System.Drawing.Size(10, 17)
    Me.Label_Status.Text = " "
    '
    'frmCashReports
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(402, 239)
    Me.Controls.Add(Me.Form_StatusStrip)
    Me.Controls.Add(Me.Date_DetailEnd)
    Me.Controls.Add(Me.Label_EndDate)
    Me.Controls.Add(Me.Date_PriceAndFX)
    Me.Controls.Add(Me.Label_PriceAndFX)
    Me.Controls.Add(Me.Combo_ReportName)
    Me.Controls.Add(Me.Label4)
    Me.Controls.Add(Me.Combo_LegalEntity)
    Me.Controls.Add(Me.Date_DetailStart)
    Me.Controls.Add(Me.Label_StartDate)
    Me.Controls.Add(Me.Combo_Fund)
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.btnClose)
    Me.Controls.Add(Me.btnRunReport)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frmCashReports"
    Me.Text = "Cash Reports"
    Me.Form_StatusStrip.ResumeLayout(False)
    Me.Form_StatusStrip.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "


  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
  ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
  ''' </summary>
  Private WithEvents _MainForm As VeniceMain

  ' Form ToolTip
  ''' <summary>
  ''' Control to provide tooltip functionality.
  ''' </summary>
  Private FormTooltip As New ToolTip()

  ''' <summary>
  ''' The report worker
  ''' </summary>
  Private ReportWorker As BackgroundWorkerThreadSafe = Nothing
  ''' <summary>
  ''' The form controls
  ''' </summary>
  Private FormControls As ArrayList = Nothing
  ''' <summary>
  ''' The report timer
  ''' </summary>
  Private WithEvents ReportTimer As New Windows.Forms.Timer()

  ' Form Constants, specific to the table being updated.

  ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
  ''' </summary>
  Private ALWAYS_CLOSE_THIS_FORM As Boolean = True

  ' The standard ChangeID for this form. e.g. tblPerson
  ''' <summary>
  ''' The standard ChangeID for this form.
  ''' </summary>
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

  ' Form specific Permissioning variables
  ''' <summary>
  ''' The THI s_ FOR m_ permission area
  ''' </summary>
  Private THIS_FORM_PermissionArea As String
  ''' <summary>
  ''' The THI s_ FOR m_ permission type
  ''' </summary>
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
  ''' <summary>
  ''' The THI s_ FOR m_ form ID
  ''' </summary>
  Private THIS_FORM_FormID As VeniceFormID

  ' Form Status Flags

  ''' <summary>
  ''' The form is valid
  ''' </summary>
  Private FormIsValid As Boolean
  ''' <summary>
  ''' The form changed
  ''' </summary>
  Private FormChanged As Boolean
  ''' <summary>
  ''' The _ form open failed
  ''' </summary>
  Private _FormOpenFailed As Boolean

  ' User Permission Flags

  ''' <summary>
  ''' The has read permission
  ''' </summary>
  Private HasReadPermission As Boolean
  ''' <summary>
  ''' The has update permission
  ''' </summary>
  Private HasUpdatePermission As Boolean
  ''' <summary>
  ''' The has insert permission
  ''' </summary>
  Private HasInsertPermission As Boolean
  ''' <summary>
  ''' The has delete permission
  ''' </summary>
  Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

  ''' <summary>
  ''' Gets the main form.
  ''' </summary>
  ''' <value>The main form.</value>
  Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
    ' Public property to return handle to the 'Main' Venice form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

  ''' <summary>
  ''' Gets or sets a value indicating whether this instance is over cancel button.
  ''' </summary>
  ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
  Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
    ' Public property maintaining a value indicating if the cursor is over the 'Cancel'
    ' Button on this form.
    ' This property is specifically designed for use by the field formating Event functions
    ' In order that they do not impose format restrictions if the user is about to click the 
    ' 'Cancel' button.
    '
    Get
      Return False
    End Get
    Set(ByVal Value As Boolean)
    End Set
  End Property

  ''' <summary>
  ''' Gets a value indicating whether this instance is in paint.
  ''' </summary>
  ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
  Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
    Get
      Return False
    End Get
  End Property

  ''' <summary>
  ''' Gets a value indicating whether [in use].
  ''' </summary>
  ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
    Get
      Return True
    End Get
  End Property

  ''' <summary>
  ''' Gets a value indicating whether [form open failed].
  ''' </summary>
  ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

#End Region

  ''' <summary>
  ''' Initializes a new instance of the <see cref="frmCashReports"/> class.
  ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  Public Sub New(ByVal pMainForm As VeniceMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()

    _MainForm = pMainForm
    AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate
    AddHandler ReportTimer.Tick, AddressOf MainForm.IncrementStatusBarTimerEvent

    _FormOpenFailed = False

    ' ******************************************************
    ' Form Specific Settings :
    ' ******************************************************

    ' Form Permissioning :-

    THIS_FORM_PermissionArea = Me.Name
    THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

    ' 'This' form ID

    THIS_FORM_FormID = VeniceFormID.frmCashReports

    ' Format Event Handlers for form controls

    AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

    AddHandler Combo_Fund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_LegalEntity.KeyUp, AddressOf MainForm.ComboSelectAsYouType

    ' Set up the ToolTip
    MainForm.SetFormToolTip(Me, FormTooltip)

    ' ******************************************************
    ' End Form Specific.
    ' ******************************************************

    THIS_FORM_ChangeID = RenaissanceChangeID.KnowledgeDate

  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
  ''' <summary>
  ''' Resets the form.
  ''' </summary>
  Public Sub ResetForm() Implements StandardVeniceForm.ResetForm

    Call Form_Load(Me, New System.EventArgs)
  End Sub

  ''' <summary>
  ''' Closes the form.
  ''' </summary>
  Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
    ALWAYS_CLOSE_THIS_FORM = True
    Me.Close()
  End Sub

  ''' <summary>
  ''' Handles the FormClosing event of the frmCashReports control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.Windows.Forms.FormClosingEventArgs"/> instance containing the event data.</param>
  Private Sub frmCashReports_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
    If (ReportWorker IsNot Nothing) Then
      If ReportWorker.IsBusy Then
        e.Cancel = True
        Exit Sub
      End If

      Try
        RemoveHandler ReportTimer.Tick, AddressOf MainForm.IncrementStatusBarTimerEvent
        RemoveHandler ReportWorker.DoWork, AddressOf ReportWorker_DoWork
        RemoveHandler ReportWorker.RunWorkerCompleted, AddressOf ReportWorkerCompleted
        ReportWorker.Dispose()
        ReportWorker = Nothing
      Catch ex As Exception
      End Try
    End If

  End Sub

  ''' <summary>
  ''' Handles the Load event of the Form control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
    _FormOpenFailed = False

    If Not (MainForm Is Nothing) Then
      FormIsValid = True
    Else
      MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If


    ' Check User permissions
    Call CheckPermissions()
    If (HasReadPermission = False) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    ' Build Combos

    Try


      Call SetFundCombo()
      Call SetLegalEntityCombo()

      MainForm.SetComboSelectionLengths(Me)

      Me.Combo_ReportName.SelectedIndex = 0

      ' Initial Start Date 
      Dim TempDate As Date
      TempDate = Now.Date
      TempDate = TempDate.AddDays(0 - TempDate.Day)
      TempDate = TempDate.AddDays(0 - TempDate.Day)
      Me.Date_DetailStart.Value = TempDate

      Me.Combo_Fund.SelectedIndex = 0
      Me.Combo_LegalEntity.SelectedIndex = 0

    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Handles the Closing event of the Form control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
  Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    Dim HideForm As Boolean

    ' Hide or Close this form ?
    ' All depends on how many of this form type are Open or in Cache...


    If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
      HideForm = False
    Else
      HideForm = True
      If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.ReportForm_CacheCount Then
        HideForm = False
      End If
    End If

    If HideForm = True Then
      MainForm.HideInFormsCollection(Me)
      Me.Hide() ' NPP Fix

      e.Cancel = True
    Else
      Try
        MainForm.RemoveFromFormsCollection(Me)
        RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

        RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

        RemoveHandler Combo_Fund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_LegalEntity.KeyUp, AddressOf MainForm.ComboSelectAsYouType

      Catch ex As Exception
      End Try
    End If

  End Sub


#End Region

  ' Routine to handle changes / updates to tables by this and other windows.
  ' If this, or any other, form posts a change to a table, then it will invoke an update event 
  ' detailing what tables have been altered.
  ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
  ' the 'VeniceAutoUpdate' event of the main Venice form.
  ' Each form may them react as appropriate to changes in any table that might impact it.
  '
  ''' <summary>
  ''' Autoes the update.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    Dim KnowledgeDateChanged As Boolean
    Dim SetButtonStatus_Flag As Boolean

    If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

    KnowledgeDateChanged = False
    SetButtonStatus_Flag = False

    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
      KnowledgeDateChanged = True
    End If

    ' ****************************************************************
    ' Check for changes relevant to this form
    ' ****************************************************************

    ' Changes to the KnowledgeDate :-
    If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
      SetButtonStatus_Flag = True
    End If

    ' Changes to the tblFund table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFund) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Call SetFundCombo()
      Call SetLegalEntityCombo()
    End If


    ' Changes to the tblUserPermissions table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

      ' Check ongoing permissions.

      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

        FormIsValid = False
        Me.Close()
        Exit Sub
      End If

      SetButtonStatus_Flag = True

    End If

    ' ****************************************************************
    '
    ' ****************************************************************


  End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

  ' Check User permissions
  ''' <summary>
  ''' Checks the permissions.
  ''' </summary>
  Private Sub CheckPermissions()
    ' *****************************************************************************
    '
    ' *****************************************************************************

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

  End Sub


#End Region

#Region " Set Form Combos (Form Specific Code) "

  ''' <summary>
  ''' Sets the fund combo.
  ''' </summary>
  Private Sub SetFundCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_Fund, _
    RenaissanceStandardDatasets.tblFund, _
    "FundName", _
    "FundID", _
    "FundClosed=0", False, True, True)  ' 

  End Sub

  ''' <summary>
  ''' Sets the legal entity combo.
  ''' </summary>
  Private Sub SetLegalEntityCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_LegalEntity, _
    RenaissanceStandardDatasets.tblFund, _
    "FundLegalEntity", _
    "FundLegalEntity", _
    "", True, True, True)   ' 

  End Sub


#End Region

#Region " SetButton / Control Events (Form Specific Code) "

  ''' <summary>
  ''' Handles the SelectedIndexChanged event of the Combo_Fund control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_Fund_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_Fund.SelectedIndexChanged

  End Sub


#End Region


#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

  ''' <summary>
  ''' Handles the Click event of the btnRunReport control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnRunReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRunReport.Click
    ' *****************************************************************************
    ' Run Report
    ' *****************************************************************************

    Try
      FormControls = MainForm.MainReportHandler.DisableFormControls(Me)
      Form_ProgressBar.Value = Form_ProgressBar.Minimum
      MainForm.SetToolStripText(Label_Status, "Processing Report")

      If (ReportWorker Is Nothing) Then
        ReportWorker = New BackgroundWorkerThreadSafe(Nothing)
        AddHandler ReportWorker.DoWork, AddressOf ReportWorker_DoWork
        AddHandler ReportWorker.RunWorkerCompleted, AddressOf ReportWorkerCompleted
      End If

      ReportTimer.Interval = 25
      ReportTimer.Tag = Me.Form_ProgressBar
      ReportTimer.Start()

      If MainForm.UseReportWorkerThreads Then
        ReportWorker.RunWorkerAsync()
      Else
        RunReport()
        Call ReportWorkerCompleted(Nothing, Nothing)
      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
    End Try
  End Sub

  ''' <summary>
  ''' Handles the DoWork event of the ReportWorker control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.ComponentModel.DoWorkEventArgs"/> instance containing the event data.</param>
  Private Sub ReportWorker_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) ' Handles backgroundWorker1.DoWork
    RunReport()
  End Sub

  ''' <summary>
  ''' Reports the worker completed.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="System.ComponentModel.RunWorkerCompletedEventArgs"/> instance containing the event data.</param>
  Private Sub ReportWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) ' Handles backgroundWorker1.RunWorkerCompleted
    Try
      ReportTimer.Stop()
      MainForm.MainReportHandler.EnableFormControls(FormControls)
      MainForm.SetToolStripText(Label_Status, "")

      'Form_ProgressBar = MainForm.ResetProgressBar(Form_StatusStrip, Form_ProgressBar)
      Form_ProgressBar.Value = Form_ProgressBar.Minimum
      Form_ProgressBar.Visible = False
      ReportTimer.Tag = Me.Form_ProgressBar
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
    End Try

  End Sub

  ''' <summary>
  ''' Runs the report.
  ''' </summary>
  Private Sub RunReport()
    ' *****************************************************************************
    ' Run Report
    ' *****************************************************************************

    Dim FundID As Integer
    Dim LegalEntity As String
    Dim DetailStartDate As Date
    Dim DetailEndDate As Date
    Dim PriceAndFXDate As Date


    ' Validate

    ' Fund ID

    Try
      If (MainForm.GetComboSelectedIndex(Combo_Fund) <= 0) Then
        FundID = 0
      Else
        FundID = CInt(MainForm.GetComboSelectedValue(Combo_Fund))
      End If

      ' Legal Entity

      LegalEntity = ""
      'If MainForm.GetComboSelectedIndex(Combo_LegalEntity) > 0 Then
      '  LegalEntity = MainForm.GetComboSelectedValue(Combo_LegalEntity)
      'End If

      DetailStartDate = MainForm.GetDatetimeValue(Me.Date_DetailStart).Date
      DetailEndDate = MainForm.GetDatetimeValue(Me.Date_DetailEnd).Date
      PriceAndFXDate = MainForm.GetDatetimeValue(Me.Date_PriceAndFX).Date

    Catch ex As Exception

      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error preparing for Cash report.", ex.StackTrace, True)
      Exit Sub
    End Try

    ' Reun Report.

    Try

      Me.MainForm.SetToolStripText(Label_Status, "Processing Report")
      If Not (MainForm.InvokeRequired) Then
        Application.DoEvents()
      End If

      ' rptCashReports
      Call MainForm.MainReportHandler.rptCashReports(MainForm.GetComboSelectedIndex(Combo_ReportName), FundID, MainForm.GetControlText(Me.Combo_Fund), LegalEntity, DetailStartDate, DetailEndDate, PriceAndFXDate, DEFAULT_STATUSGROUPFILTER, DEFAULT_ADMINISTRATORDATESFILTER, Nothing) ' Form_ProgressBar)

    Catch ex As Exception
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error processing Cash report.", ex.StackTrace, True)

    End Try

  End Sub


  ''' <summary>
  ''' Handles the Click event of the btnClose control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
    ' *****************************************************************************
    ' Close Form
    ' *****************************************************************************

    Me.Close()

  End Sub

#End Region

#Region " Form Control Events"

  ''' <summary>
  ''' Handles the SelectedIndexChanged event of the Combo_ReportName control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_ReportName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_ReportName.SelectedIndexChanged
    Select Case Me.Combo_ReportName.SelectedIndex
      Case 6
        Me.Label_StartDate.Text = "Start Date"
        Me.Label_EndDate.Text = "End Date"
        Me.Label_PriceAndFX.Visible = False
        Me.Date_PriceAndFX.Visible = False

      Case Else
        Me.Label_StartDate.Text = "Detail Start Date"
        Me.Label_EndDate.Text = "Detail End Date"
        Me.Label_PriceAndFX.Visible = True
        Me.Date_PriceAndFX.Visible = True

    End Select
  End Sub

#End Region
End Class
