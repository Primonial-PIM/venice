' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmFundFeeReports.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals

Imports RenaissanceDataClass
Imports RenaissanceUtilities.DatePeriodFunctions

''' <summary>
''' Class frmFundFeeReports
''' </summary>
Public Class frmFundFeeReports

	Inherits System.Windows.Forms.Form
	Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmFundFeeReports"/> class from being created.
    ''' </summary>
	Private Sub New()
		MyBase.New()

		'This call is required by the Windows Form Designer.
		InitializeComponent()

		'Add any initialization after the InitializeComponent() call

	End Sub

	'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
    ''' <summary>
    ''' The BTN run report
    ''' </summary>
	Friend WithEvents btnRunReport As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN close
    ''' </summary>
	Friend WithEvents btnClose As System.Windows.Forms.Button
    ''' <summary>
    ''' The combo_ fund
    ''' </summary>
	Friend WithEvents Combo_Fund As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ counterparty
    ''' </summary>
	Friend WithEvents Combo_Counterparty As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label_ counterparty
    ''' </summary>
	Friend WithEvents Label_Counterparty As System.Windows.Forms.Label
    ''' <summary>
    ''' The date_ single fee date
    ''' </summary>
	Friend WithEvents Date_SingleFeeDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The combo_ report name
    ''' </summary>
	Friend WithEvents Combo_ReportName As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label4
    ''' </summary>
	Friend WithEvents Label4 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ investor group
    ''' </summary>
	Friend WithEvents Combo_InvestorGroup As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The form_ status strip
    ''' </summary>
	Friend WithEvents Form_StatusStrip As System.Windows.Forms.StatusStrip
    ''' <summary>
    ''' The form_ progress bar
    ''' </summary>
	Friend WithEvents Form_ProgressBar As System.Windows.Forms.ToolStripProgressBar
    ''' <summary>
    ''' The label_ status
    ''' </summary>
	Friend WithEvents Label_Status As System.Windows.Forms.ToolStripStatusLabel
    ''' <summary>
    ''' The date_ fee end date
    ''' </summary>
	Friend WithEvents Date_FeeEndDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The radio_ single fund
    ''' </summary>
	Friend WithEvents Radio_SingleFund As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The radio_ selected funds
    ''' </summary>
	Friend WithEvents Radio_SelectedFunds As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The radio_ single date
    ''' </summary>
	Friend WithEvents Radio_SingleDate As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The radio_ fee date thru
    ''' </summary>
	Friend WithEvents Radio_FeeDateThru As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The group box1
    ''' </summary>
	Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The group box2
    ''' </summary>
	Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The list_ fund I ds
    ''' </summary>
	Friend WithEvents List_FundIDs As System.Windows.Forms.ListBox
    ''' <summary>
    ''' The group box3
    ''' </summary>
	Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The radio_ gross fees
    ''' </summary>
	Friend WithEvents Radio_GrossFees As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The radio_ net fees
    ''' </summary>
	Friend WithEvents Radio_NetFees As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The group box4
    ''' </summary>
	Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The radio_ in fund currency
    ''' </summary>
	Friend WithEvents Radio_InFundCurrency As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The radio_ in USD
    ''' </summary>
  Friend WithEvents Radio_InUSD As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The radio_ in GBP
    ''' </summary>
	Friend WithEvents Radio_InGBP As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The check_ calculate missing fees
    ''' </summary>
	Friend WithEvents Check_CalculateMissingFees As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ recalculate all fees
    ''' </summary>
	Friend WithEvents Check_RecalculateAllFees As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The label1
    ''' </summary>
	Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Me.btnRunReport = New System.Windows.Forms.Button
		Me.btnClose = New System.Windows.Forms.Button
		Me.Combo_Fund = New System.Windows.Forms.ComboBox
		Me.Combo_Counterparty = New System.Windows.Forms.ComboBox
		Me.Label_Counterparty = New System.Windows.Forms.Label
		Me.Date_SingleFeeDate = New System.Windows.Forms.DateTimePicker
		Me.Combo_ReportName = New System.Windows.Forms.ComboBox
		Me.Label4 = New System.Windows.Forms.Label
		Me.Combo_InvestorGroup = New System.Windows.Forms.ComboBox
		Me.Label1 = New System.Windows.Forms.Label
		Me.Form_StatusStrip = New System.Windows.Forms.StatusStrip
		Me.Form_ProgressBar = New System.Windows.Forms.ToolStripProgressBar
		Me.Label_Status = New System.Windows.Forms.ToolStripStatusLabel
		Me.Date_FeeEndDate = New System.Windows.Forms.DateTimePicker
		Me.Radio_SingleFund = New System.Windows.Forms.RadioButton
		Me.Radio_SelectedFunds = New System.Windows.Forms.RadioButton
		Me.Radio_SingleDate = New System.Windows.Forms.RadioButton
		Me.Radio_FeeDateThru = New System.Windows.Forms.RadioButton
		Me.GroupBox1 = New System.Windows.Forms.GroupBox
		Me.List_FundIDs = New System.Windows.Forms.ListBox
		Me.GroupBox2 = New System.Windows.Forms.GroupBox
		Me.GroupBox3 = New System.Windows.Forms.GroupBox
		Me.Radio_GrossFees = New System.Windows.Forms.RadioButton
		Me.Radio_NetFees = New System.Windows.Forms.RadioButton
		Me.GroupBox4 = New System.Windows.Forms.GroupBox
		Me.Radio_InGBP = New System.Windows.Forms.RadioButton
		Me.Radio_InFundCurrency = New System.Windows.Forms.RadioButton
		Me.Radio_InUSD = New System.Windows.Forms.RadioButton
		Me.Check_CalculateMissingFees = New System.Windows.Forms.CheckBox
		Me.Check_RecalculateAllFees = New System.Windows.Forms.CheckBox
		Me.Form_StatusStrip.SuspendLayout()
		Me.GroupBox1.SuspendLayout()
		Me.GroupBox2.SuspendLayout()
		Me.GroupBox3.SuspendLayout()
		Me.GroupBox4.SuspendLayout()
		Me.SuspendLayout()
		'
		'btnRunReport
		'
		Me.btnRunReport.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnRunReport.Location = New System.Drawing.Point(58, 401)
		Me.btnRunReport.Name = "btnRunReport"
		Me.btnRunReport.Size = New System.Drawing.Size(148, 28)
		Me.btnRunReport.TabIndex = 9
		Me.btnRunReport.Text = "Run Report"
		'
		'btnClose
		'
		Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnClose.Location = New System.Drawing.Point(274, 401)
		Me.btnClose.Name = "btnClose"
		Me.btnClose.Size = New System.Drawing.Size(75, 28)
		Me.btnClose.TabIndex = 10
		Me.btnClose.Text = "&Close"
		'
		'Combo_Fund
		'
		Me.Combo_Fund.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_Fund.Location = New System.Drawing.Point(131, 11)
		Me.Combo_Fund.Name = "Combo_Fund"
		Me.Combo_Fund.Size = New System.Drawing.Size(252, 21)
		Me.Combo_Fund.TabIndex = 1
		'
		'Combo_Counterparty
		'
		Me.Combo_Counterparty.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_Counterparty.Location = New System.Drawing.Point(140, 230)
		Me.Combo_Counterparty.Name = "Combo_Counterparty"
		Me.Combo_Counterparty.Size = New System.Drawing.Size(252, 21)
		Me.Combo_Counterparty.TabIndex = 3
		'
		'Label_Counterparty
		'
		Me.Label_Counterparty.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label_Counterparty.Location = New System.Drawing.Point(16, 234)
		Me.Label_Counterparty.Name = "Label_Counterparty"
		Me.Label_Counterparty.Size = New System.Drawing.Size(104, 16)
		Me.Label_Counterparty.TabIndex = 88
		Me.Label_Counterparty.Text = "Counterparty"
		'
		'Date_SingleFeeDate
		'
		Me.Date_SingleFeeDate.Location = New System.Drawing.Point(131, 12)
		Me.Date_SingleFeeDate.Name = "Date_SingleFeeDate"
		Me.Date_SingleFeeDate.Size = New System.Drawing.Size(160, 20)
		Me.Date_SingleFeeDate.TabIndex = 1
		'
		'Combo_ReportName
		'
		Me.Combo_ReportName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.Combo_ReportName.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_ReportName.Items.AddRange(New Object() {"Fund Fees Status Report", "Fund Fees Report", "Fund Fee Counterparty Detail Report"})
		Me.Combo_ReportName.Location = New System.Drawing.Point(140, 8)
		Me.Combo_ReportName.Name = "Combo_ReportName"
		Me.Combo_ReportName.Size = New System.Drawing.Size(252, 21)
		Me.Combo_ReportName.TabIndex = 0
		'
		'Label4
		'
		Me.Label4.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label4.Location = New System.Drawing.Point(16, 12)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(120, 20)
		Me.Label4.TabIndex = 103
		Me.Label4.Text = "Report Name"
		'
		'Combo_InvestorGroup
		'
		Me.Combo_InvestorGroup.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_InvestorGroup.Location = New System.Drawing.Point(140, 257)
		Me.Combo_InvestorGroup.Name = "Combo_InvestorGroup"
		Me.Combo_InvestorGroup.Size = New System.Drawing.Size(252, 21)
		Me.Combo_InvestorGroup.TabIndex = 4
		'
		'Label1
		'
		Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label1.Location = New System.Drawing.Point(16, 261)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(104, 16)
		Me.Label1.TabIndex = 106
		Me.Label1.Text = "Investor Group"
		'
		'Form_StatusStrip
		'
		Me.Form_StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Form_ProgressBar, Me.Label_Status})
		Me.Form_StatusStrip.Location = New System.Drawing.Point(0, 438)
		Me.Form_StatusStrip.Name = "Form_StatusStrip"
		Me.Form_StatusStrip.Size = New System.Drawing.Size(415, 22)
		Me.Form_StatusStrip.TabIndex = 11
		Me.Form_StatusStrip.Text = " "
		'
		'Form_ProgressBar
		'
		Me.Form_ProgressBar.Maximum = 20
		Me.Form_ProgressBar.Name = "Form_ProgressBar"
		Me.Form_ProgressBar.Size = New System.Drawing.Size(150, 17)
		Me.Form_ProgressBar.Step = 1
		Me.Form_ProgressBar.Visible = False
		'
		'Label_Status
		'
		Me.Label_Status.Name = "Label_Status"
		Me.Label_Status.Size = New System.Drawing.Size(10, 17)
		Me.Label_Status.Text = " "
		'
		'Date_FeeEndDate
		'
		Me.Date_FeeEndDate.Location = New System.Drawing.Point(131, 38)
		Me.Date_FeeEndDate.Name = "Date_FeeEndDate"
		Me.Date_FeeEndDate.Size = New System.Drawing.Size(160, 20)
		Me.Date_FeeEndDate.TabIndex = 3
		'
		'Radio_SingleFund
		'
		Me.Radio_SingleFund.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Radio_SingleFund.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Radio_SingleFund.Location = New System.Drawing.Point(7, 12)
		Me.Radio_SingleFund.Name = "Radio_SingleFund"
		Me.Radio_SingleFund.Size = New System.Drawing.Size(118, 20)
		Me.Radio_SingleFund.TabIndex = 0
		Me.Radio_SingleFund.Text = "Single (or all) Fund(s)."
		Me.Radio_SingleFund.UseVisualStyleBackColor = True
		'
		'Radio_SelectedFunds
		'
		Me.Radio_SelectedFunds.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Radio_SelectedFunds.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Radio_SelectedFunds.Location = New System.Drawing.Point(7, 38)
		Me.Radio_SelectedFunds.Name = "Radio_SelectedFunds"
		Me.Radio_SelectedFunds.Size = New System.Drawing.Size(118, 20)
		Me.Radio_SelectedFunds.TabIndex = 2
		Me.Radio_SelectedFunds.Text = "Selected Funds"
		Me.Radio_SelectedFunds.UseVisualStyleBackColor = True
		'
		'Radio_SingleDate
		'
		Me.Radio_SingleDate.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Radio_SingleDate.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Radio_SingleDate.Location = New System.Drawing.Point(7, 13)
		Me.Radio_SingleDate.Name = "Radio_SingleDate"
		Me.Radio_SingleDate.Size = New System.Drawing.Size(118, 20)
		Me.Radio_SingleDate.TabIndex = 0
		Me.Radio_SingleDate.Text = "Single Month"
		Me.Radio_SingleDate.UseVisualStyleBackColor = True
		'
		'Radio_FeeDateThru
		'
		Me.Radio_FeeDateThru.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Radio_FeeDateThru.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Radio_FeeDateThru.Location = New System.Drawing.Point(7, 39)
		Me.Radio_FeeDateThru.Name = "Radio_FeeDateThru"
		Me.Radio_FeeDateThru.Size = New System.Drawing.Size(118, 20)
		Me.Radio_FeeDateThru.TabIndex = 2
		Me.Radio_FeeDateThru.Text = "All Months thru..."
		Me.Radio_FeeDateThru.UseVisualStyleBackColor = True
		'
		'GroupBox1
		'
		Me.GroupBox1.Controls.Add(Me.List_FundIDs)
		Me.GroupBox1.Controls.Add(Me.Combo_Fund)
		Me.GroupBox1.Controls.Add(Me.Radio_SingleFund)
		Me.GroupBox1.Controls.Add(Me.Radio_SelectedFunds)
		Me.GroupBox1.Location = New System.Drawing.Point(9, 32)
		Me.GroupBox1.Name = "GroupBox1"
		Me.GroupBox1.Size = New System.Drawing.Size(388, 124)
		Me.GroupBox1.TabIndex = 1
		Me.GroupBox1.TabStop = False
		'
		'List_FundIDs
		'
		Me.List_FundIDs.FormattingEnabled = True
		Me.List_FundIDs.Location = New System.Drawing.Point(131, 38)
		Me.List_FundIDs.Name = "List_FundIDs"
		Me.List_FundIDs.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
		Me.List_FundIDs.Size = New System.Drawing.Size(251, 82)
		Me.List_FundIDs.TabIndex = 3
		'
		'GroupBox2
		'
		Me.GroupBox2.Controls.Add(Me.Date_SingleFeeDate)
		Me.GroupBox2.Controls.Add(Me.Date_FeeEndDate)
		Me.GroupBox2.Controls.Add(Me.Radio_FeeDateThru)
		Me.GroupBox2.Controls.Add(Me.Radio_SingleDate)
		Me.GroupBox2.Location = New System.Drawing.Point(9, 157)
		Me.GroupBox2.Name = "GroupBox2"
		Me.GroupBox2.Size = New System.Drawing.Size(388, 66)
		Me.GroupBox2.TabIndex = 2
		Me.GroupBox2.TabStop = False
		'
		'GroupBox3
		'
		Me.GroupBox3.Controls.Add(Me.Radio_GrossFees)
		Me.GroupBox3.Controls.Add(Me.Radio_NetFees)
		Me.GroupBox3.Location = New System.Drawing.Point(9, 321)
		Me.GroupBox3.Name = "GroupBox3"
		Me.GroupBox3.Size = New System.Drawing.Size(388, 35)
		Me.GroupBox3.TabIndex = 7
		Me.GroupBox3.TabStop = False
		'
		'Radio_GrossFees
		'
		Me.Radio_GrossFees.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Radio_GrossFees.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Radio_GrossFees.Location = New System.Drawing.Point(7, 10)
		Me.Radio_GrossFees.Name = "Radio_GrossFees"
		Me.Radio_GrossFees.Size = New System.Drawing.Size(118, 20)
		Me.Radio_GrossFees.TabIndex = 0
		Me.Radio_GrossFees.Text = "Gross Fees"
		Me.Radio_GrossFees.UseVisualStyleBackColor = True
		'
		'Radio_NetFees
		'
		Me.Radio_NetFees.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Radio_NetFees.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Radio_NetFees.Location = New System.Drawing.Point(144, 10)
		Me.Radio_NetFees.Name = "Radio_NetFees"
		Me.Radio_NetFees.Size = New System.Drawing.Size(118, 20)
		Me.Radio_NetFees.TabIndex = 1
		Me.Radio_NetFees.Text = "Net Fees"
		Me.Radio_NetFees.UseVisualStyleBackColor = True
		'
		'GroupBox4
		'
		Me.GroupBox4.Controls.Add(Me.Radio_InGBP)
		Me.GroupBox4.Controls.Add(Me.Radio_InFundCurrency)
		Me.GroupBox4.Controls.Add(Me.Radio_InUSD)
		Me.GroupBox4.Location = New System.Drawing.Point(9, 359)
		Me.GroupBox4.Name = "GroupBox4"
		Me.GroupBox4.Size = New System.Drawing.Size(388, 35)
		Me.GroupBox4.TabIndex = 8
		Me.GroupBox4.TabStop = False
		'
		'Radio_InGBP
		'
		Me.Radio_InGBP.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Radio_InGBP.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Radio_InGBP.Location = New System.Drawing.Point(265, 10)
		Me.Radio_InGBP.Name = "Radio_InGBP"
		Me.Radio_InGBP.Size = New System.Drawing.Size(100, 20)
		Me.Radio_InGBP.TabIndex = 2
		Me.Radio_InGBP.Text = "In GBP"
		Me.Radio_InGBP.UseVisualStyleBackColor = True
		'
		'Radio_InFundCurrency
		'
		Me.Radio_InFundCurrency.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Radio_InFundCurrency.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Radio_InFundCurrency.Location = New System.Drawing.Point(7, 10)
		Me.Radio_InFundCurrency.Name = "Radio_InFundCurrency"
		Me.Radio_InFundCurrency.Size = New System.Drawing.Size(100, 20)
		Me.Radio_InFundCurrency.TabIndex = 0
		Me.Radio_InFundCurrency.Text = "Local Currency"
		Me.Radio_InFundCurrency.UseVisualStyleBackColor = True
		'
		'Radio_InUSD
		'
		Me.Radio_InUSD.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Radio_InUSD.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Radio_InUSD.Location = New System.Drawing.Point(132, 10)
		Me.Radio_InUSD.Name = "Radio_InUSD"
		Me.Radio_InUSD.Size = New System.Drawing.Size(100, 20)
		Me.Radio_InUSD.TabIndex = 1
		Me.Radio_InUSD.Text = "In USD"
		Me.Radio_InUSD.UseVisualStyleBackColor = True
		'
		'Check_CalculateMissingFees
		'
		Me.Check_CalculateMissingFees.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Check_CalculateMissingFees.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Check_CalculateMissingFees.Location = New System.Drawing.Point(16, 282)
		Me.Check_CalculateMissingFees.Name = "Check_CalculateMissingFees"
		Me.Check_CalculateMissingFees.Size = New System.Drawing.Size(190, 17)
		Me.Check_CalculateMissingFees.TabIndex = 5
		Me.Check_CalculateMissingFees.Text = "Calculate Missing Fees"
		Me.Check_CalculateMissingFees.UseVisualStyleBackColor = True
		'
		'Check_RecalculateAllFees
		'
		Me.Check_RecalculateAllFees.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Check_RecalculateAllFees.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Check_RecalculateAllFees.Location = New System.Drawing.Point(16, 303)
		Me.Check_RecalculateAllFees.Name = "Check_RecalculateAllFees"
		Me.Check_RecalculateAllFees.Size = New System.Drawing.Size(190, 17)
		Me.Check_RecalculateAllFees.TabIndex = 6
		Me.Check_RecalculateAllFees.Text = "ReCalculate all Non M/S Fees"
		Me.Check_RecalculateAllFees.UseVisualStyleBackColor = True
		'
		'frmFundFeeReports
		'
		Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
		Me.ClientSize = New System.Drawing.Size(415, 460)
		Me.Controls.Add(Me.Check_RecalculateAllFees)
		Me.Controls.Add(Me.Check_CalculateMissingFees)
		Me.Controls.Add(Me.GroupBox4)
		Me.Controls.Add(Me.GroupBox3)
		Me.Controls.Add(Me.GroupBox2)
		Me.Controls.Add(Me.GroupBox1)
		Me.Controls.Add(Me.Form_StatusStrip)
		Me.Controls.Add(Me.Combo_InvestorGroup)
		Me.Controls.Add(Me.Label1)
		Me.Controls.Add(Me.Combo_ReportName)
		Me.Controls.Add(Me.Label4)
		Me.Controls.Add(Me.Combo_Counterparty)
		Me.Controls.Add(Me.Label_Counterparty)
		Me.Controls.Add(Me.btnClose)
		Me.Controls.Add(Me.btnRunReport)
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Name = "frmFundFeeReports"
		Me.Text = "Fund Fee Reports"
		Me.Form_StatusStrip.ResumeLayout(False)
		Me.Form_StatusStrip.PerformLayout()
		Me.GroupBox1.ResumeLayout(False)
		Me.GroupBox2.ResumeLayout(False)
		Me.GroupBox3.ResumeLayout(False)
		Me.GroupBox4.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

#End Region

#Region " Form Locals and Constants "


	' Form 'Parent', the Main Venice form.
	' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
	Private WithEvents _MainForm As VeniceMain

	' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
	Private FormTooltip As New ToolTip()

    ''' <summary>
    ''' The report worker
    ''' </summary>
	Private ReportWorker As BackgroundWorkerThreadSafe = Nothing
    ''' <summary>
    ''' The form controls
    ''' </summary>
	Private FormControls As ArrayList = Nothing
    ''' <summary>
    ''' The report timer
    ''' </summary>
	Private WithEvents ReportTimer As New Windows.Forms.Timer()

	' Form Constants, specific to the table being updated.

    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
	Private ALWAYS_CLOSE_THIS_FORM As Boolean = True

	' The standard ChangeID for this form. e.g. tblPerson
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
	Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

	' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
	Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
	Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

	' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
	Private THIS_FORM_FormID As VeniceFormID

	' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
	Private FormIsValid As Boolean
    ''' <summary>
    ''' The form changed
    ''' </summary>
	Private FormChanged As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
	Private _FormOpenFailed As Boolean

	' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
	Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
	Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
	Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
	Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
	Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
	Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return False
		End Get
		Set(ByVal Value As Boolean)
		End Set
	End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
	Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
		Get
			Return False
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
		Get
			Return True
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmFundFeeReports"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
	Public Sub New(ByVal pMainForm As VeniceMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()

		_MainForm = pMainForm
		AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate
		AddHandler ReportTimer.Tick, AddressOf MainForm.IncrementStatusBarTimerEvent

		_FormOpenFailed = False

		' ******************************************************
		' Form Specific Settings :
		' ******************************************************

		' Form Permissioning :-

		THIS_FORM_PermissionArea = Me.Name
		THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

		' 'This' form ID

		THIS_FORM_FormID = VeniceFormID.frmFundFeeReports

		' Format Event Handlers for form controls

		AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

		AddHandler Combo_Fund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_Counterparty.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_InvestorGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

		' ******************************************************
		' End Form Specific.
		' ******************************************************

		THIS_FORM_ChangeID = RenaissanceChangeID.KnowledgeDate

	End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

	' Form Initialisation code.
	'
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
	Public Sub ResetForm() Implements StandardVeniceForm.ResetForm

		Call Form_Load(Me, New System.EventArgs)
	End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
	Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub

    ''' <summary>
    ''' Handles the FormClosing event of the frmFundFeeReports control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.FormClosingEventArgs"/> instance containing the event data.</param>
	Private Sub frmFundFeeReports_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
		If (ReportWorker IsNot Nothing) Then
			If ReportWorker.IsBusy Then
				e.Cancel = True
				Exit Sub
			End If

			Try
				RemoveHandler ReportTimer.Tick, AddressOf MainForm.IncrementStatusBarTimerEvent
				RemoveHandler ReportWorker.DoWork, AddressOf ReportWorker_DoWork
				RemoveHandler ReportWorker.RunWorkerCompleted, AddressOf ReportWorkerCompleted
				ReportWorker.Dispose()
				ReportWorker = Nothing
			Catch ex As Exception
			End Try
		End If

	End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
		_FormOpenFailed = False

		If Not (MainForm Is Nothing) Then
			FormIsValid = True
		Else
			MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If


		' Check User permissions
		Call CheckPermissions()
		If (HasReadPermission = False) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		' Build Combos

		Try

			Me.Combo_ReportName.SelectedIndex = 0

			Call SetFundCombo()
			Call SetCounterpartyCombo()
			Call SetInvestorGroupCombo()

			MainForm.SetComboSelectionLengths(Me)

			Me.Radio_SingleFund.Checked = True
			Me.Radio_FeeDateThru.Checked = True
			Me.Radio_NetFees.Checked = True
			Me.Radio_InUSD.Checked = True
			Me.Check_CalculateMissingFees.Checked = True
			Me.Check_RecalculateAllFees.Checked = False

		Catch ex As Exception
		End Try

	End Sub

    ''' <summary>
    ''' Handles the Closing event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
	Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		Dim HideForm As Boolean

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...


		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else
			HideForm = True
			If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.ReportForm_CacheCount Then
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			e.Cancel = True
		Else
			Try
				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

				RemoveHandler Combo_Fund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_Counterparty.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_InvestorGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType

			Catch ex As Exception
			End Try
		End If

	End Sub


#End Region

	' Routine to handle changes / updates to tables by this and other windows.
	' If this, or any other, form posts a change to a table, then it will invoke an update event 
	' detailing what tables have been altered.
	' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
	' the 'VeniceAutoUpdate' event of the main Venice form.
	' Each form may them react as appropriate to changes in any table that might impact it.
	'
    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		Dim KnowledgeDateChanged As Boolean
		Dim SetButtonStatus_Flag As Boolean

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		KnowledgeDateChanged = False
		SetButtonStatus_Flag = False

		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
			KnowledgeDateChanged = True
		End If

		' ****************************************************************
		' Check for changes relevant to this form
		' ****************************************************************

		' Changes to the KnowledgeDate :-
		If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
			SetButtonStatus_Flag = True
		End If

		' Changes to the tblFund table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFund) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			Call SetFundCombo()
		End If

		' Changes to the tblCounterparty table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblCounterparty) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			Call SetCounterpartyCombo()
		End If

		' Changes to the tblInvestorGroup table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInvestorGroup) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			Call SetInvestorGroupCombo()
		End If

		' Changes to the tblUserPermissions table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

			' Check ongoing permissions.

			Call CheckPermissions()
			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

				FormIsValid = False
				Me.Close()
				Exit Sub
			End If

			SetButtonStatus_Flag = True

		End If

		' ****************************************************************
		'
		' ****************************************************************


	End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

	' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
	Private Sub CheckPermissions()
		' *****************************************************************************
		'
		' *****************************************************************************

		Dim Permissions As Integer

		Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

		HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
		HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
		HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
		HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

	End Sub


#End Region

#Region " Set Form Combos (Form Specific Code) "

    ''' <summary>
    ''' Sets the fund combo.
    ''' </summary>
	Private Sub SetFundCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_Fund, _
    RenaissanceStandardDatasets.tblFund, _
    "FundName", _
    "FundID", _
    "FundClosed=0", False, True, True, 0, "All Funds")     ' 

		Try
			Dim TmpCombo As New ComboBox
      Call MainForm.SetTblGenericCombo( _
      TmpCombo, _
      RenaissanceStandardDatasets.tblFund, _
      "FundName", _
      "FundID", _
      "FundClosed=0", False, True, False)    ' 

			List_FundIDs.DataSource = Nothing
			List_FundIDs.DisplayMember = "DM"
			List_FundIDs.ValueMember = "VM"
			List_FundIDs.DataSource = TmpCombo.DataSource

			TmpCombo.DataSource = Nothing
			TmpCombo = Nothing

		Catch ex As Exception
		End Try

	End Sub

    ''' <summary>
    ''' Sets the counterparty combo.
    ''' </summary>
	Private Sub SetCounterpartyCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_Counterparty, _
		RenaissanceStandardDatasets.tblCounterparty, _
		"CounterpartyName", _
		"CounterpartyID", _
		"", False, True, True)	 ' 

	End Sub

    ''' <summary>
    ''' Sets the investor group combo.
    ''' </summary>
	Private Sub SetInvestorGroupCombo()
		Call MainForm.SetTblGenericCombo( _
		Me.Combo_InvestorGroup, _
		RenaissanceStandardDatasets.tblInvestorGroup, _
		"IGName", _
		"IGID", _
		"", False, True, True)	 ' 
	End Sub

#End Region

#Region " SetButton / Control Events (Form Specific Code) "

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_Fund control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_Fund_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_Fund.SelectedIndexChanged

	End Sub


#End Region


#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "


    ''' <summary>
    ''' Handles the Click event of the btnRunReport control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnRunReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRunReport.Click
		' *****************************************************************************
		' Run Report
		' *****************************************************************************

		Try
			FormControls = MainForm.MainReportHandler.DisableFormControls(Me)
			Form_ProgressBar.Value = Form_ProgressBar.Minimum
			MainForm.SetToolStripText(Label_Status, "Processing Report")

			If (ReportWorker Is Nothing) Then
				ReportWorker = New BackgroundWorkerThreadSafe(Nothing)
				AddHandler ReportWorker.DoWork, AddressOf ReportWorker_DoWork
				AddHandler ReportWorker.RunWorkerCompleted, AddressOf ReportWorkerCompleted
			End If

			ReportTimer.Interval = 25
			ReportTimer.Tag = Me.Form_ProgressBar
			ReportTimer.Start()


			If MainForm.UseReportWorkerThreads Then
				ReportWorker.RunWorkerAsync()
			Else
				RunReport()
				Call ReportWorkerCompleted(Nothing, Nothing)
			End If


		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
		End Try
	End Sub

    ''' <summary>
    ''' Handles the DoWork event of the ReportWorker control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.DoWorkEventArgs"/> instance containing the event data.</param>
	Private Sub ReportWorker_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs)	' Handles backgroundWorker1.DoWork
		RunReport()
	End Sub

    ''' <summary>
    ''' Reports the worker completed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.RunWorkerCompletedEventArgs"/> instance containing the event data.</param>
	Private Sub ReportWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs)	' Handles backgroundWorker1.RunWorkerCompleted
		Try
			ReportTimer.Stop()
			MainForm.MainReportHandler.EnableFormControls(FormControls)
			MainForm.SetToolStripText(Label_Status, "")

			'Form_ProgressBar = MainForm.ResetProgressBar(Form_StatusStrip, Form_ProgressBar)
			Form_ProgressBar.Value = Form_ProgressBar.Minimum
			Form_ProgressBar.Visible = False
			ReportTimer.Tag = Me.Form_ProgressBar
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
		End Try

	End Sub

    ''' <summary>
    ''' Runs the report.
    ''' </summary>
	Private Sub RunReport()
		' *****************************************************************************
		' Run Report
		' *****************************************************************************

		Dim FundIDs(-1) As Integer
		Dim CounterpartyID As Integer
		Dim InvestorGroupID As Integer
		Dim ReportOnSingleDate As Boolean = False
		Dim ValueDate As Date
		Dim NetFees As Boolean = False
		Dim InUSD As Boolean = False
    Dim InGBP As Boolean = False

		' Validate

		If MainForm.GetRadioChecked(Me.Radio_SingleFund) Then
			If (MainForm.GetComboSelectedValue(Combo_Fund) Is Nothing) OrElse (MainForm.GetComboSelectedIndex(Combo_Fund) < 0) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Fund must be selected.", "", True)
				Exit Sub
			End If

			If (MainForm.GetComboSelectedIndex(Combo_Fund) = 0) Then ' All Funds.
				Dim AllListItems As Object()
				Dim FundCounter As Integer
				Dim ThisRowView As DataRowView

				AllListItems = MainForm.GetListBoxItems(List_FundIDs)

				If (AllListItems Is Nothing) OrElse (AllListItems.Length <= 0) Then
					Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "There appear to be no Funds to to report on.", "", True)
					Exit Sub
				End If

				ReDim FundIDs(AllListItems.Length - 1)

				Try
					For FundCounter = 0 To (AllListItems.Length - 1)
						ThisRowView = AllListItems(FundCounter)

						FundIDs(FundCounter) = CInt(ThisRowView.Row("VM"))
					Next
				Catch ex As Exception
				End Try
			Else
				ReDim FundIDs(0)
				FundIDs(0) = CInt(MainForm.GetComboSelectedValue(Combo_Fund))
			End If

		Else
			Dim SelectedItems As Object()
			Dim FundCounter As Integer
			Dim ThisRowView As DataRowView

			SelectedItems = MainForm.GetListBoxSelectedItems(List_FundIDs)

			If (SelectedItems Is Nothing) OrElse (SelectedItems.Length <= 0) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Fund(s) must be selected.", "", True)
				Exit Sub
			End If

			ReDim FundIDs(SelectedItems.Length - 1)

			Try
				For FundCounter = 0 To (SelectedItems.Length - 1)
					ThisRowView = SelectedItems(FundCounter)

					FundIDs(FundCounter) = CInt(ThisRowView.Row("VM"))
				Next
			Catch ex As Exception
			End Try

		End If


		InvestorGroupID = 0
		If MainForm.GetComboSelectedIndex(Combo_InvestorGroup) > 0 Then
			InvestorGroupID = CInt(MainForm.GetComboSelectedValue(Combo_InvestorGroup))
		End If

		CounterpartyID = 0
		If MainForm.GetComboSelectedIndex(Combo_Counterparty) > 0 Then
			CounterpartyID = CInt(MainForm.GetComboSelectedValue(Combo_Counterparty))
		End If

		If MainForm.GetRadioChecked(Me.Radio_SingleDate) Then
			ReportOnSingleDate = True
			ValueDate = MainForm.GetDatetimeValue(Date_SingleFeeDate).Date
			ValueDate = FitDateToPeriod(DealingPeriod.Monthly, ValueDate, True)
		Else
			ReportOnSingleDate = False
			ValueDate = MainForm.GetDatetimeValue(Date_FeeEndDate).Date
			ValueDate = FitDateToPeriod(DealingPeriod.Monthly, ValueDate, True)
		End If

		NetFees = MainForm.GetRadioChecked(Radio_NetFees)
    InUSD = MainForm.GetRadioChecked(Radio_InUSD)
    InGBP = MainForm.GetRadioChecked(Radio_InGBP)
    If (InUSD And InGBP) Then ' Give Priority to GBP
      InUSD = False
    End If

		' Calculate Fees as appropriate

		Dim CalculateMissingFees As Boolean = MainForm.GetCheckBoxChecked(Me.Check_CalculateMissingFees)
		Dim CalculateAllFees As Boolean = MainForm.GetCheckBoxChecked(Me.Check_RecalculateAllFees)
		Dim UnitHolderFeeCalcObject As UnitHolderFeeClass = Nothing

		Try
			If (CalculateMissingFees Or CalculateAllFees) Then

				Me.MainForm.SetToolStripText(Label_Status, "Calculating Fund Fees")
				If Not (MainForm.InvokeRequired) Then
					Application.DoEvents()
				End If

				Dim Counter As Integer
				Dim FundID As Integer
				Dim RecalcThisPeriod As Boolean
				Dim MilestonesDS As DSFundMilestones
				Dim SelectedMilestones() As DSFundMilestones.tblFundMilestonesRow
				Dim thisMilestones As DSFundMilestones.tblFundMilestonesRow
				Dim LastMSDate As Date
				Dim ThisDate As Date
				Dim FundRow As DSFund.tblFundRow
				Dim FundPricingPeriod As Integer = RenaissanceGlobals.DealingPeriod.Daily
				Dim FundUsesEqualisation As Integer = 0

				Dim thisCommand As SqlCommand = Nothing

				UnitHolderFeeCalcObject = New UnitHolderFeeClass(MainForm, DEFAULT_STATUSGROUPFILTER, DEFAULT_ADMINISTRATORDATESFILTER)
				MilestonesDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblFundMilestones, False)

				Try
					thisCommand = New SqlCommand()

					thisCommand.CommandType = CommandType.Text
					thisCommand.Connection = MainForm.GetVeniceConnection
					thisCommand.CommandText = "SELECT COUNT(RN) FROM fn_tblUnitHolderFees_SelectKD(Null) WHERE (FeeFund = @FeeFund) AND (FeeDate = @FeeDate) AND (IsMilestone = @IsMilestone)"
					thisCommand.Parameters.Add("@FeeFund", SqlDbType.Int)
					thisCommand.Parameters.Add("@FeeDate", SqlDbType.DateTime)
					thisCommand.Parameters.Add("@IsMilestone", SqlDbType.Bit)

					For Counter = 0 To (FundIDs.Length - 1)
						FundID = FundIDs(Counter)
						thisCommand.Parameters("@FeeFund").Value = FundID
						FundRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, FundID), DSFund.tblFundRow)

						If (FundRow IsNot Nothing) Then
							FundPricingPeriod = CType(FundRow.FundPricingPeriod, RenaissanceGlobals.DealingPeriod)
							FundUsesEqualisation = CInt(FundRow.FundUsesEqualisation)
						Else
							FundPricingPeriod = DealingPeriod.Daily
							FundUsesEqualisation = 0
						End If

						' Get Last Milestone (If present), assume Fund Starts two periods ago if no Milestone.
						SelectedMilestones = MilestonesDS.tblFundMilestones.Select("MilestoneFundID=" & FundID.ToString, "MilestoneDate")
						If (SelectedMilestones IsNot Nothing) AndAlso (SelectedMilestones.Length > 0) Then
							thisMilestones = SelectedMilestones(SelectedMilestones.Length - 1)
							LastMSDate = thisMilestones.MilestoneDate
						Else
							LastMSDate = AddPeriodToDate(FundPricingPeriod, FitDateToPeriod(FundPricingPeriod, Now.Date, True), -2)
						End If

						If (ReportOnSingleDate) Then
							ThisDate = ValueDate
						Else
							ThisDate = AddPeriodToDate(FundPricingPeriod, LastMSDate, 1)
							ThisDate = FitDateToPeriod(FundPricingPeriod, ThisDate, True)
						End If

						' Loop through Dates from Last MS to Report Date calculating Fees if necessary.
						While ThisDate <= ValueDate
							RecalcThisPeriod = CalculateAllFees
							thisCommand.Parameters("@FeeDate").Value = ThisDate

							' If M/S Fees exist for this Date, Don't recalc regardless..
							thisCommand.Parameters("@IsMilestone").Value = 1
              If CInt(MainForm.ExecuteScalar(thisCommand)) > 0 Then
                RecalcThisPeriod = False

              ElseIf (Not CalculateAllFees) Then
                ' Do Fees already exist for this date ?
                thisCommand.Parameters("@IsMilestone").Value = 0

                If CInt(MainForm.ExecuteScalar(thisCommand)) <= 0 Then
                  RecalcThisPeriod = True
                End If

              End If

							' Recalculate

							If (RecalcThisPeriod) Then

								Me.MainForm.SetToolStripText(Label_Status, "Calculating Fund Fees : " & FundID.ToString & ", " & ThisDate.ToString(DISPLAYMEMBER_DATEFORMAT))
								If Not (MainForm.InvokeRequired) Then
									Application.DoEvents()
								End If

								UnitHolderFeeCalcObject.SaveSinglePeriodFundFees(FundID, LastMSDate, ThisDate, False)

							End If

							ThisDate = AddPeriodToDate(FundPricingPeriod, ThisDate, 1)

						End While

					Next

				Catch ex As Exception
				Finally
					If (thisCommand IsNot Nothing) Then
						If thisCommand.Connection IsNot Nothing Then
							thisCommand.Connection.Close()
						End If

						thisCommand.Connection = Nothing
						thisCommand.Parameters.Clear()
						thisCommand = Nothing
					End If
				End Try

			End If
		Catch ex As Exception
		Finally
			If (UnitHolderFeeCalcObject IsNot Nothing) Then
				UnitHolderFeeCalcObject.Dispose()
				UnitHolderFeeCalcObject = Nothing
			End If
		End Try

		' Get Report Data

		Me.MainForm.SetToolStripText(Label_Status, "")
		If Not (MainForm.InvokeRequired) Then
			Application.DoEvents()
		End If

		Dim ReportDataTable As DataTable
		Dim ReportDataView As DataView

		Try

			Me.MainForm.SetToolStripText(Label_Status, "Processing Report")
			If Not (MainForm.InvokeRequired) Then
				Application.DoEvents()
			End If

			' Get Report Data

			' rptUnitHolderStatement
			If (FundIDs.Length = 1) Then
				If (ReportOnSingleDate) Then
					ReportDataTable = MainForm.MainReportHandler.GetFundFeesData(FundIDs(0), CounterpartyID, InvestorGroupID, ValueDate)
				Else
					ReportDataTable = MainForm.MainReportHandler.GetFundFeesData(FundIDs(0), CounterpartyID, InvestorGroupID)
				End If
			Else
				If (ReportOnSingleDate) Then
					ReportDataTable = MainForm.MainReportHandler.GetFundFeesData(0, CounterpartyID, InvestorGroupID, ValueDate)
				Else
					ReportDataTable = MainForm.MainReportHandler.GetFundFeesData(0, CounterpartyID, InvestorGroupID)
				End If
			End If

			ReportDataView = New DataView(ReportDataTable)

			Try
				Dim SelectString As String = ""

				If (Not ReportOnSingleDate) Then
					SelectString = "(FeeDate <= '" & ValueDate.ToString(QUERY_SHORTDATEFORMAT) & "')"
				End If

				If (FundIDs.Length > 1) Then
					Dim IN_String As String
					Dim FundCounter As Integer

					IN_String = FundIDs(0).ToString
					For FundCounter = 1 To (FundIDs.Length - 1)
						IN_String &= "," & FundIDs(FundCounter).ToString
					Next

					If (SelectString.Length > 0) Then
						SelectString &= " AND "
					End If

					SelectString &= "(FeeFund IN (" & IN_String & "))"
				End If

				ReportDataView.RowFilter = SelectString

			Catch ex As Exception
				ReportDataView.RowFilter = "true"
			End Try

			' Display appropriate Report

			If (Not (ReportDataView Is Nothing)) Then
				Select Case MainForm.GetComboSelectedIndex(Combo_ReportName)
					Case 0

						Call MainForm.MainReportHandler.rptFundFees("rptFundFeeStatusReport", ReportDataView, 0, ValueDate, NetFees, InUSD, InGBP, MainForm.Main_Knowledgedate, Nothing)	'Form_ProgressBar)

					Case 1

						Call MainForm.MainReportHandler.rptFundFees("rptFundFeeReport", ReportDataView, 0, ValueDate, NetFees, InUSD, InGBP, MainForm.Main_Knowledgedate, Nothing)	'Form_ProgressBar)

					Case 2 ' rptFundFeeStatusDetailReport

						Call MainForm.MainReportHandler.rptFundFees("rptFundFeeStatusDetailReport", ReportDataView, 0, ValueDate, NetFees, InUSD, InGBP, MainForm.Main_Knowledgedate, Nothing)	'Form_ProgressBar)

					Case 3

					Case 4

					Case Else

				End Select

			Else

			End If


		Catch ex As Exception
		End Try

	End Sub


    ''' <summary>
    ''' Handles the Click event of the btnClose control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
		' *****************************************************************************
		' Close Form
		' *****************************************************************************

		Me.Close()

	End Sub

#End Region


    ''' <summary>
    ''' Handles the CheckedChanged event of the Radio_SingleFund control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Radio_SingleFund_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_SingleFund.CheckedChanged
		' ****************************************************************
		'
		' ****************************************************************

		If (Radio_SingleFund.Checked) Then
			Radio_SelectedFunds.Checked = False

			List_FundIDs.Enabled = False
			Combo_Fund.Enabled = True

		End If

	End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the Radio_SelectedFunds control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Radio_SelectedFunds_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_SelectedFunds.CheckedChanged
		' ****************************************************************
		'
		' ****************************************************************

		If (Radio_SelectedFunds.Checked) Then
			Radio_SingleFund.Checked = False

			List_FundIDs.Enabled = True
			Combo_Fund.Enabled = False

		End If

	End Sub


    ''' <summary>
    ''' Handles the CheckedChanged event of the Radio_SingleDate control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Radio_SingleDate_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_SingleDate.CheckedChanged
		' ****************************************************************
		'
		' ****************************************************************

		If (Radio_SingleDate.Checked) Then
			Radio_FeeDateThru.Checked = False

			Me.Date_SingleFeeDate.Enabled = True
			Me.Date_FeeEndDate.Enabled = False
		End If

	End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the Radio_FeeDateThru control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Radio_FeeDateThru_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_FeeDateThru.CheckedChanged
		' ****************************************************************
		'
		' ****************************************************************

		If (Radio_FeeDateThru.Checked) Then
			Radio_SingleDate.Checked = False

			Me.Date_SingleFeeDate.Enabled = False
			Me.Date_FeeEndDate.Enabled = True
		End If

	End Sub

End Class
