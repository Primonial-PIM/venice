' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmRiskExposureChangeReport.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals


''' <summary>
''' Class frmRiskExposureChangeReport
''' </summary>
Public Class frmRiskExposureChangeReport

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmRiskExposureChangeReport"/> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The BTN run report
    ''' </summary>
  Friend WithEvents btnRunReport As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN close
    ''' </summary>
  Friend WithEvents btnClose As System.Windows.Forms.Button
    ''' <summary>
    ''' The label2
    ''' </summary>
  Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ fund
    ''' </summary>
  Friend WithEvents Combo_Fund As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The date_ from_ report date
    ''' </summary>
  Friend WithEvents Date_From_ReportDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The label_ trade date
    ''' </summary>
  Friend WithEvents Label_TradeDate As System.Windows.Forms.Label
    ''' <summary>
    ''' The label3
    ''' </summary>
  Friend WithEvents Label3 As System.Windows.Forms.Label
    ''' <summary>
    ''' The panel5
    ''' </summary>
  Friend WithEvents Panel5 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The radio_ from_ live
    ''' </summary>
  Friend WithEvents Radio_From_Live As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The radio_ from_ precise time
    ''' </summary>
  Friend WithEvents Radio_From_PreciseTime As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The radio_ from_ whole day
    ''' </summary>
  Friend WithEvents Radio_From_WholeDay As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The date_ from_ knowledge date
    ''' </summary>
  Friend WithEvents Date_From_KnowledgeDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The combo_ from_ bookmark
    ''' </summary>
  Friend WithEvents Combo_From_Bookmark As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label4
    ''' </summary>
  Friend WithEvents Label4 As System.Windows.Forms.Label
    ''' <summary>
    ''' The panel4
    ''' </summary>
  Friend WithEvents Panel4 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The panel6
    ''' </summary>
  Friend WithEvents Panel6 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The label1
    ''' </summary>
  Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label5
    ''' </summary>
  Friend WithEvents Label5 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label6
    ''' </summary>
  Friend WithEvents Label6 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ minimum usage
    ''' </summary>
  Friend WithEvents Combo_MinimumUsage As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label7
    ''' </summary>
  Friend WithEvents Label7 As System.Windows.Forms.Label
    ''' <summary>
    ''' The panel1
    ''' </summary>
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The panel2
    ''' </summary>
  Friend WithEvents Panel2 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The combo_ limit category
    ''' </summary>
  Friend WithEvents Combo_LimitCategory As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ instrument
    ''' </summary>
  Friend WithEvents Combo_Instrument As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The check_ recreate exposures
    ''' </summary>
  Friend WithEvents Check_RecreateExposures As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ aggregate to parent
    ''' </summary>
  Friend WithEvents Check_AggregateToParent As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The combo_ from_ existing dates
    ''' </summary>
  Friend WithEvents Combo_From_ExistingDates As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ to_ bookmark
    ''' </summary>
  Friend WithEvents Combo_To_Bookmark As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label8
    ''' </summary>
  Friend WithEvents Label8 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label9
    ''' </summary>
  Friend WithEvents Label9 As System.Windows.Forms.Label
    ''' <summary>
    ''' The panel3
    ''' </summary>
  Friend WithEvents Panel3 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The radio_ to_ live
    ''' </summary>
  Friend WithEvents Radio_To_Live As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The radio_ to_ precise time
    ''' </summary>
  Friend WithEvents Radio_To_PreciseTime As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The radio_ to_ whole day
    ''' </summary>
  Friend WithEvents Radio_To_WholeDay As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The date_ to_ knowledge date
    ''' </summary>
  Friend WithEvents Date_To_KnowledgeDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The date_ to_ report date
    ''' </summary>
  Friend WithEvents Date_To_ReportDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The label10
    ''' </summary>
  Friend WithEvents Label10 As System.Windows.Forms.Label
    ''' <summary>
    ''' The form_ status strip
    ''' </summary>
  Friend WithEvents Form_StatusStrip As System.Windows.Forms.StatusStrip
    ''' <summary>
    ''' The form_ progress bar
    ''' </summary>
  Friend WithEvents Form_ProgressBar As System.Windows.Forms.ToolStripProgressBar
    ''' <summary>
    ''' The label_ status
    ''' </summary>
	Friend WithEvents Label_Status As System.Windows.Forms.ToolStripStatusLabel
    ''' <summary>
    ''' The check_ lookthrough risk
    ''' </summary>
	Friend WithEvents Check_LookthroughRisk As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The combo_ transaction group start
    ''' </summary>
	Friend WithEvents Combo_TransactionGroupStart As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label11
    ''' </summary>
	Friend WithEvents Label11 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ transaction group end
    ''' </summary>
	Friend WithEvents Combo_TransactionGroupEnd As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label12
    ''' </summary>
	Friend WithEvents Label12 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ to_ existing dates
    ''' </summary>
  Friend WithEvents Combo_To_ExistingDates As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Me.btnRunReport = New System.Windows.Forms.Button
		Me.btnClose = New System.Windows.Forms.Button
		Me.Label2 = New System.Windows.Forms.Label
		Me.Combo_Fund = New System.Windows.Forms.ComboBox
		Me.Date_From_ReportDate = New System.Windows.Forms.DateTimePicker
		Me.Label_TradeDate = New System.Windows.Forms.Label
		Me.Label3 = New System.Windows.Forms.Label
		Me.Panel5 = New System.Windows.Forms.Panel
		Me.Radio_From_Live = New System.Windows.Forms.RadioButton
		Me.Radio_From_PreciseTime = New System.Windows.Forms.RadioButton
		Me.Radio_From_WholeDay = New System.Windows.Forms.RadioButton
		Me.Date_From_KnowledgeDate = New System.Windows.Forms.DateTimePicker
		Me.Combo_From_Bookmark = New System.Windows.Forms.ComboBox
		Me.Label4 = New System.Windows.Forms.Label
		Me.Panel4 = New System.Windows.Forms.Panel
		Me.Panel6 = New System.Windows.Forms.Panel
		Me.Label1 = New System.Windows.Forms.Label
		Me.Combo_LimitCategory = New System.Windows.Forms.ComboBox
		Me.Label5 = New System.Windows.Forms.Label
		Me.Combo_Instrument = New System.Windows.Forms.ComboBox
		Me.Label6 = New System.Windows.Forms.Label
		Me.Combo_MinimumUsage = New System.Windows.Forms.ComboBox
		Me.Label7 = New System.Windows.Forms.Label
		Me.Panel1 = New System.Windows.Forms.Panel
		Me.Panel2 = New System.Windows.Forms.Panel
		Me.Check_RecreateExposures = New System.Windows.Forms.CheckBox
		Me.Check_AggregateToParent = New System.Windows.Forms.CheckBox
		Me.Combo_From_ExistingDates = New System.Windows.Forms.ComboBox
		Me.Combo_To_Bookmark = New System.Windows.Forms.ComboBox
		Me.Label8 = New System.Windows.Forms.Label
		Me.Label9 = New System.Windows.Forms.Label
		Me.Panel3 = New System.Windows.Forms.Panel
		Me.Radio_To_Live = New System.Windows.Forms.RadioButton
		Me.Radio_To_PreciseTime = New System.Windows.Forms.RadioButton
		Me.Radio_To_WholeDay = New System.Windows.Forms.RadioButton
		Me.Date_To_KnowledgeDate = New System.Windows.Forms.DateTimePicker
		Me.Date_To_ReportDate = New System.Windows.Forms.DateTimePicker
		Me.Label10 = New System.Windows.Forms.Label
		Me.Combo_To_ExistingDates = New System.Windows.Forms.ComboBox
		Me.Form_StatusStrip = New System.Windows.Forms.StatusStrip
		Me.Form_ProgressBar = New System.Windows.Forms.ToolStripProgressBar
		Me.Label_Status = New System.Windows.Forms.ToolStripStatusLabel
		Me.Check_LookthroughRisk = New System.Windows.Forms.CheckBox
		Me.Combo_TransactionGroupStart = New System.Windows.Forms.ComboBox
		Me.Label11 = New System.Windows.Forms.Label
		Me.Combo_TransactionGroupEnd = New System.Windows.Forms.ComboBox
		Me.Label12 = New System.Windows.Forms.Label
		Me.Panel5.SuspendLayout()
		Me.Panel4.SuspendLayout()
		Me.Panel1.SuspendLayout()
		Me.Panel3.SuspendLayout()
		Me.Form_StatusStrip.SuspendLayout()
		Me.SuspendLayout()
		'
		'btnRunReport
		'
		Me.btnRunReport.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnRunReport.Location = New System.Drawing.Point(44, 439)
		Me.btnRunReport.Name = "btnRunReport"
		Me.btnRunReport.Size = New System.Drawing.Size(148, 28)
		Me.btnRunReport.TabIndex = 19
		Me.btnRunReport.Text = "Run Report"
		'
		'btnClose
		'
		Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnClose.Location = New System.Drawing.Point(260, 439)
		Me.btnClose.Name = "btnClose"
		Me.btnClose.Size = New System.Drawing.Size(75, 28)
		Me.btnClose.TabIndex = 20
		Me.btnClose.Text = "&Close"
		'
		'Label2
		'
		Me.Label2.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label2.Location = New System.Drawing.Point(12, 16)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(120, 20)
		Me.Label2.TabIndex = 53
		Me.Label2.Text = "Fund Name"
		'
		'Combo_Fund
		'
		Me.Combo_Fund.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_Fund.Location = New System.Drawing.Point(136, 12)
		Me.Combo_Fund.Name = "Combo_Fund"
		Me.Combo_Fund.Size = New System.Drawing.Size(256, 21)
		Me.Combo_Fund.TabIndex = 0
		'
		'Date_From_ReportDate
		'
		Me.Date_From_ReportDate.Location = New System.Drawing.Point(136, 44)
		Me.Date_From_ReportDate.Name = "Date_From_ReportDate"
		Me.Date_From_ReportDate.Size = New System.Drawing.Size(236, 20)
		Me.Date_From_ReportDate.TabIndex = 1
		'
		'Label_TradeDate
		'
		Me.Label_TradeDate.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label_TradeDate.Location = New System.Drawing.Point(12, 48)
		Me.Label_TradeDate.Name = "Label_TradeDate"
		Me.Label_TradeDate.Size = New System.Drawing.Size(100, 16)
		Me.Label_TradeDate.TabIndex = 102
		Me.Label_TradeDate.Text = "Start Date"
		'
		'Label3
		'
		Me.Label3.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label3.Location = New System.Drawing.Point(12, 105)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(116, 16)
		Me.Label3.TabIndex = 115
		Me.Label3.Text = "Start KnowledgeDate"
		'
		'Panel5
		'
		Me.Panel5.Controls.Add(Me.Radio_From_Live)
		Me.Panel5.Controls.Add(Me.Radio_From_PreciseTime)
		Me.Panel5.Controls.Add(Me.Radio_From_WholeDay)
		Me.Panel5.Location = New System.Drawing.Point(12, 125)
		Me.Panel5.Name = "Panel5"
		Me.Panel5.Size = New System.Drawing.Size(376, 24)
		Me.Panel5.TabIndex = 5
		'
		'Radio_From_Live
		'
		Me.Radio_From_Live.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Radio_From_Live.Checked = True
		Me.Radio_From_Live.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Radio_From_Live.Location = New System.Drawing.Point(12, 4)
		Me.Radio_From_Live.Name = "Radio_From_Live"
		Me.Radio_From_Live.Size = New System.Drawing.Size(104, 16)
		Me.Radio_From_Live.TabIndex = 0
		Me.Radio_From_Live.TabStop = True
		Me.Radio_From_Live.Text = "'Live' KD"
		'
		'Radio_From_PreciseTime
		'
		Me.Radio_From_PreciseTime.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Radio_From_PreciseTime.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Radio_From_PreciseTime.Location = New System.Drawing.Point(252, 4)
		Me.Radio_From_PreciseTime.Name = "Radio_From_PreciseTime"
		Me.Radio_From_PreciseTime.Size = New System.Drawing.Size(104, 16)
		Me.Radio_From_PreciseTime.TabIndex = 2
		Me.Radio_From_PreciseTime.Text = "Precise Time"
		'
		'Radio_From_WholeDay
		'
		Me.Radio_From_WholeDay.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Radio_From_WholeDay.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Radio_From_WholeDay.Location = New System.Drawing.Point(128, 4)
		Me.Radio_From_WholeDay.Name = "Radio_From_WholeDay"
		Me.Radio_From_WholeDay.Size = New System.Drawing.Size(104, 16)
		Me.Radio_From_WholeDay.TabIndex = 1
		Me.Radio_From_WholeDay.Text = "WholeDay"
		'
		'Date_From_KnowledgeDate
		'
		Me.Date_From_KnowledgeDate.CustomFormat = "dd MMM yyyy"
		Me.Date_From_KnowledgeDate.Enabled = False
		Me.Date_From_KnowledgeDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
		Me.Date_From_KnowledgeDate.Location = New System.Drawing.Point(136, 101)
		Me.Date_From_KnowledgeDate.Name = "Date_From_KnowledgeDate"
		Me.Date_From_KnowledgeDate.Size = New System.Drawing.Size(256, 20)
		Me.Date_From_KnowledgeDate.TabIndex = 4
		'
		'Combo_From_Bookmark
		'
		Me.Combo_From_Bookmark.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_From_Bookmark.Location = New System.Drawing.Point(136, 161)
		Me.Combo_From_Bookmark.Name = "Combo_From_Bookmark"
		Me.Combo_From_Bookmark.Size = New System.Drawing.Size(252, 21)
		Me.Combo_From_Bookmark.TabIndex = 6
		'
		'Label4
		'
		Me.Label4.Location = New System.Drawing.Point(12, 165)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(100, 20)
		Me.Label4.TabIndex = 116
		Me.Label4.Text = "Bookmark"
		'
		'Panel4
		'
		Me.Panel4.Controls.Add(Me.Panel6)
		Me.Panel4.Location = New System.Drawing.Point(12, 201)
		Me.Panel4.Name = "Panel4"
		Me.Panel4.Size = New System.Drawing.Size(376, 3)
		Me.Panel4.TabIndex = 118
		'
		'Panel6
		'
		Me.Panel6.Location = New System.Drawing.Point(0, 116)
		Me.Panel6.Name = "Panel6"
		Me.Panel6.Size = New System.Drawing.Size(428, 2)
		Me.Panel6.TabIndex = 48
		'
		'Label1
		'
		Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label1.Location = New System.Drawing.Point(12, 209)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(116, 16)
		Me.Label1.TabIndex = 119
		Me.Label1.Text = "Reporting Criteria"
		'
		'Combo_LimitCategory
		'
		Me.Combo_LimitCategory.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_LimitCategory.Location = New System.Drawing.Point(135, 233)
		Me.Combo_LimitCategory.Name = "Combo_LimitCategory"
		Me.Combo_LimitCategory.Size = New System.Drawing.Size(256, 21)
		Me.Combo_LimitCategory.TabIndex = 13
		'
		'Label5
		'
		Me.Label5.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label5.Location = New System.Drawing.Point(11, 237)
		Me.Label5.Name = "Label5"
		Me.Label5.Size = New System.Drawing.Size(120, 20)
		Me.Label5.TabIndex = 121
		Me.Label5.Text = "Limit Category"
		'
		'Combo_Instrument
		'
		Me.Combo_Instrument.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_Instrument.Location = New System.Drawing.Point(135, 265)
		Me.Combo_Instrument.Name = "Combo_Instrument"
		Me.Combo_Instrument.Size = New System.Drawing.Size(256, 21)
		Me.Combo_Instrument.TabIndex = 14
		'
		'Label6
		'
		Me.Label6.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label6.Location = New System.Drawing.Point(11, 269)
		Me.Label6.Name = "Label6"
		Me.Label6.Size = New System.Drawing.Size(120, 20)
		Me.Label6.TabIndex = 123
		Me.Label6.Text = "Limit Instrument"
		'
		'Combo_MinimumUsage
		'
		Me.Combo_MinimumUsage.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_MinimumUsage.Items.AddRange(New Object() {"0%", "5%", "10%", "20%", "25%", "50%", "75%", "100%"})
		Me.Combo_MinimumUsage.Location = New System.Drawing.Point(136, 297)
		Me.Combo_MinimumUsage.Name = "Combo_MinimumUsage"
		Me.Combo_MinimumUsage.Size = New System.Drawing.Size(256, 21)
		Me.Combo_MinimumUsage.TabIndex = 15
		'
		'Label7
		'
		Me.Label7.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label7.Location = New System.Drawing.Point(12, 301)
		Me.Label7.Name = "Label7"
		Me.Label7.Size = New System.Drawing.Size(120, 20)
		Me.Label7.TabIndex = 125
		Me.Label7.Text = "Minimum Change"
		'
		'Panel1
		'
		Me.Panel1.Controls.Add(Me.Panel2)
		Me.Panel1.Location = New System.Drawing.Point(13, 329)
		Me.Panel1.Name = "Panel1"
		Me.Panel1.Size = New System.Drawing.Size(376, 3)
		Me.Panel1.TabIndex = 126
		'
		'Panel2
		'
		Me.Panel2.Location = New System.Drawing.Point(0, 116)
		Me.Panel2.Name = "Panel2"
		Me.Panel2.Size = New System.Drawing.Size(428, 2)
		Me.Panel2.TabIndex = 48
		'
		'Check_RecreateExposures
		'
		Me.Check_RecreateExposures.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Check_RecreateExposures.Location = New System.Drawing.Point(12, 345)
		Me.Check_RecreateExposures.Name = "Check_RecreateExposures"
		Me.Check_RecreateExposures.Size = New System.Drawing.Size(352, 20)
		Me.Check_RecreateExposures.TabIndex = 16
		Me.Check_RecreateExposures.Text = "Recreate Exposures regardless of existing data."
		'
		'Check_AggregateToParent
		'
		Me.Check_AggregateToParent.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Check_AggregateToParent.Location = New System.Drawing.Point(12, 373)
		Me.Check_AggregateToParent.Name = "Check_AggregateToParent"
		Me.Check_AggregateToParent.Size = New System.Drawing.Size(352, 20)
		Me.Check_AggregateToParent.TabIndex = 17
		Me.Check_AggregateToParent.Text = "Aggregate risk to Parent Instrument"
		'
		'Combo_From_ExistingDates
		'
		Me.Combo_From_ExistingDates.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_From_ExistingDates.Location = New System.Drawing.Point(136, 44)
		Me.Combo_From_ExistingDates.Name = "Combo_From_ExistingDates"
		Me.Combo_From_ExistingDates.Size = New System.Drawing.Size(256, 21)
		Me.Combo_From_ExistingDates.TabIndex = 2
		'
		'Combo_To_Bookmark
		'
		Me.Combo_To_Bookmark.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_To_Bookmark.Location = New System.Drawing.Point(540, 161)
		Me.Combo_To_Bookmark.Name = "Combo_To_Bookmark"
		Me.Combo_To_Bookmark.Size = New System.Drawing.Size(252, 21)
		Me.Combo_To_Bookmark.TabIndex = 12
		'
		'Label8
		'
		Me.Label8.Location = New System.Drawing.Point(416, 165)
		Me.Label8.Name = "Label8"
		Me.Label8.Size = New System.Drawing.Size(100, 20)
		Me.Label8.TabIndex = 133
		Me.Label8.Text = "Bookmark"
		'
		'Label9
		'
		Me.Label9.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label9.Location = New System.Drawing.Point(416, 105)
		Me.Label9.Name = "Label9"
		Me.Label9.Size = New System.Drawing.Size(116, 16)
		Me.Label9.TabIndex = 132
		Me.Label9.Text = "KnowledgeDate"
		'
		'Panel3
		'
		Me.Panel3.Controls.Add(Me.Radio_To_Live)
		Me.Panel3.Controls.Add(Me.Radio_To_PreciseTime)
		Me.Panel3.Controls.Add(Me.Radio_To_WholeDay)
		Me.Panel3.Location = New System.Drawing.Point(416, 125)
		Me.Panel3.Name = "Panel3"
		Me.Panel3.Size = New System.Drawing.Size(376, 24)
		Me.Panel3.TabIndex = 11
		'
		'Radio_To_Live
		'
		Me.Radio_To_Live.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Radio_To_Live.Checked = True
		Me.Radio_To_Live.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Radio_To_Live.Location = New System.Drawing.Point(12, 4)
		Me.Radio_To_Live.Name = "Radio_To_Live"
		Me.Radio_To_Live.Size = New System.Drawing.Size(104, 16)
		Me.Radio_To_Live.TabIndex = 0
		Me.Radio_To_Live.TabStop = True
		Me.Radio_To_Live.Text = "'Live' KD"
		'
		'Radio_To_PreciseTime
		'
		Me.Radio_To_PreciseTime.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Radio_To_PreciseTime.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Radio_To_PreciseTime.Location = New System.Drawing.Point(252, 4)
		Me.Radio_To_PreciseTime.Name = "Radio_To_PreciseTime"
		Me.Radio_To_PreciseTime.Size = New System.Drawing.Size(104, 16)
		Me.Radio_To_PreciseTime.TabIndex = 2
		Me.Radio_To_PreciseTime.Text = "Precise Time"
		'
		'Radio_To_WholeDay
		'
		Me.Radio_To_WholeDay.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Radio_To_WholeDay.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Radio_To_WholeDay.Location = New System.Drawing.Point(128, 4)
		Me.Radio_To_WholeDay.Name = "Radio_To_WholeDay"
		Me.Radio_To_WholeDay.Size = New System.Drawing.Size(104, 16)
		Me.Radio_To_WholeDay.TabIndex = 1
		Me.Radio_To_WholeDay.Text = "WholeDay"
		'
		'Date_To_KnowledgeDate
		'
		Me.Date_To_KnowledgeDate.CustomFormat = "dd MMM yyyy"
		Me.Date_To_KnowledgeDate.Enabled = False
		Me.Date_To_KnowledgeDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
		Me.Date_To_KnowledgeDate.Location = New System.Drawing.Point(540, 101)
		Me.Date_To_KnowledgeDate.Name = "Date_To_KnowledgeDate"
		Me.Date_To_KnowledgeDate.Size = New System.Drawing.Size(256, 20)
		Me.Date_To_KnowledgeDate.TabIndex = 10
		'
		'Date_To_ReportDate
		'
		Me.Date_To_ReportDate.Location = New System.Drawing.Point(540, 44)
		Me.Date_To_ReportDate.Name = "Date_To_ReportDate"
		Me.Date_To_ReportDate.Size = New System.Drawing.Size(236, 20)
		Me.Date_To_ReportDate.TabIndex = 7
		'
		'Label10
		'
		Me.Label10.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label10.Location = New System.Drawing.Point(416, 48)
		Me.Label10.Name = "Label10"
		Me.Label10.Size = New System.Drawing.Size(100, 16)
		Me.Label10.TabIndex = 136
		Me.Label10.Text = "End Date"
		'
		'Combo_To_ExistingDates
		'
		Me.Combo_To_ExistingDates.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_To_ExistingDates.Location = New System.Drawing.Point(540, 44)
		Me.Combo_To_ExistingDates.Name = "Combo_To_ExistingDates"
		Me.Combo_To_ExistingDates.Size = New System.Drawing.Size(256, 21)
		Me.Combo_To_ExistingDates.TabIndex = 8
		'
		'Form_StatusStrip
		'
		Me.Form_StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Form_ProgressBar, Me.Label_Status})
		Me.Form_StatusStrip.Location = New System.Drawing.Point(0, 480)
		Me.Form_StatusStrip.Name = "Form_StatusStrip"
		Me.Form_StatusStrip.Size = New System.Drawing.Size(814, 22)
		Me.Form_StatusStrip.TabIndex = 21
		Me.Form_StatusStrip.Text = " "
		'
		'Form_ProgressBar
		'
		Me.Form_ProgressBar.Maximum = 20
		Me.Form_ProgressBar.Name = "Form_ProgressBar"
		Me.Form_ProgressBar.Size = New System.Drawing.Size(150, 17)
		Me.Form_ProgressBar.Step = 1
		Me.Form_ProgressBar.Visible = False
		'
		'Label_Status
		'
		Me.Label_Status.Name = "Label_Status"
		Me.Label_Status.Size = New System.Drawing.Size(10, 17)
		Me.Label_Status.Text = " "
		'
		'Check_LookthroughRisk
		'
		Me.Check_LookthroughRisk.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Check_LookthroughRisk.Location = New System.Drawing.Point(12, 399)
		Me.Check_LookthroughRisk.Name = "Check_LookthroughRisk"
		Me.Check_LookthroughRisk.Size = New System.Drawing.Size(352, 20)
		Me.Check_LookthroughRisk.TabIndex = 18
		Me.Check_LookthroughRisk.Text = "Lookthrough Fund Risk"
		'
		'Combo_TransactionGroupStart
		'
		Me.Combo_TransactionGroupStart.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_TransactionGroupStart.Location = New System.Drawing.Point(136, 73)
		Me.Combo_TransactionGroupStart.Name = "Combo_TransactionGroupStart"
		Me.Combo_TransactionGroupStart.Size = New System.Drawing.Size(256, 21)
		Me.Combo_TransactionGroupStart.TabIndex = 3
		'
		'Label11
		'
		Me.Label11.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label11.Location = New System.Drawing.Point(12, 77)
		Me.Label11.Name = "Label11"
		Me.Label11.Size = New System.Drawing.Size(116, 16)
		Me.Label11.TabIndex = 139
		Me.Label11.Text = "Transaction Group"
		'
		'Combo_TransactionGroupEnd
		'
		Me.Combo_TransactionGroupEnd.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_TransactionGroupEnd.Location = New System.Drawing.Point(540, 72)
		Me.Combo_TransactionGroupEnd.Name = "Combo_TransactionGroupEnd"
		Me.Combo_TransactionGroupEnd.Size = New System.Drawing.Size(256, 21)
		Me.Combo_TransactionGroupEnd.TabIndex = 9
		'
		'Label12
		'
		Me.Label12.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label12.Location = New System.Drawing.Point(416, 76)
		Me.Label12.Name = "Label12"
		Me.Label12.Size = New System.Drawing.Size(116, 16)
		Me.Label12.TabIndex = 141
		Me.Label12.Text = "Transaction Group"
		'
		'frmRiskExposureChangeReport
		'
		Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
		Me.ClientSize = New System.Drawing.Size(814, 502)
		Me.Controls.Add(Me.Combo_TransactionGroupEnd)
		Me.Controls.Add(Me.Label12)
		Me.Controls.Add(Me.Combo_TransactionGroupStart)
		Me.Controls.Add(Me.Label11)
		Me.Controls.Add(Me.Check_LookthroughRisk)
		Me.Controls.Add(Me.Form_StatusStrip)
		Me.Controls.Add(Me.Date_To_ReportDate)
		Me.Controls.Add(Me.Label10)
		Me.Controls.Add(Me.Combo_To_ExistingDates)
		Me.Controls.Add(Me.Combo_To_Bookmark)
		Me.Controls.Add(Me.Label8)
		Me.Controls.Add(Me.Label9)
		Me.Controls.Add(Me.Panel3)
		Me.Controls.Add(Me.Date_To_KnowledgeDate)
		Me.Controls.Add(Me.Check_AggregateToParent)
		Me.Controls.Add(Me.Check_RecreateExposures)
		Me.Controls.Add(Me.Panel1)
		Me.Controls.Add(Me.Combo_MinimumUsage)
		Me.Controls.Add(Me.Label7)
		Me.Controls.Add(Me.Combo_Instrument)
		Me.Controls.Add(Me.Label6)
		Me.Controls.Add(Me.Combo_LimitCategory)
		Me.Controls.Add(Me.Label5)
		Me.Controls.Add(Me.Label1)
		Me.Controls.Add(Me.Panel4)
		Me.Controls.Add(Me.Combo_From_Bookmark)
		Me.Controls.Add(Me.Label4)
		Me.Controls.Add(Me.Label3)
		Me.Controls.Add(Me.Panel5)
		Me.Controls.Add(Me.Date_From_KnowledgeDate)
		Me.Controls.Add(Me.Date_From_ReportDate)
		Me.Controls.Add(Me.Label_TradeDate)
		Me.Controls.Add(Me.Combo_Fund)
		Me.Controls.Add(Me.Label2)
		Me.Controls.Add(Me.btnClose)
		Me.Controls.Add(Me.btnRunReport)
		Me.Controls.Add(Me.Combo_From_ExistingDates)
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Name = "frmRiskExposureChangeReport"
		Me.Text = "Risk Exposure Report"
		Me.Panel5.ResumeLayout(False)
		Me.Panel4.ResumeLayout(False)
		Me.Panel1.ResumeLayout(False)
		Me.Panel3.ResumeLayout(False)
		Me.Form_StatusStrip.ResumeLayout(False)
		Me.Form_StatusStrip.PerformLayout()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

#End Region

#Region " Form Locals and Constants "


  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
  Private WithEvents _MainForm As VeniceMain

  ' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
  Private FormTooltip As New ToolTip()

    ''' <summary>
    ''' The report worker
    ''' </summary>
	Private ReportWorker As BackgroundWorkerThreadSafe = Nothing
    ''' <summary>
    ''' The form controls
    ''' </summary>
  Private FormControls As ArrayList = Nothing
    ''' <summary>
    ''' The report timer
    ''' </summary>
  Private WithEvents ReportTimer As New Windows.Forms.Timer()

  ' Form Constants, specific to the table being updated.

    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
  Private ALWAYS_CLOSE_THIS_FORM As Boolean = True

  ' The standard ChangeID for this form. e.g. tblPerson
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

  ' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
  Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
  Private THIS_FORM_FormID As VeniceFormID

  ' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
  Private FormIsValid As Boolean
    ''' <summary>
    ''' The form changed
    ''' </summary>
  Private FormChanged As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
  Private _FormOpenFailed As Boolean

  ' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
  Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
  Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
  Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
  Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
  Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
    ' Public property to return handle to the 'Main' Venice form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
  Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
    ' Public property maintaining a value indicating if the cursor is over the 'Cancel'
    ' Button on this form.
    ' This property is specifically designed for use by the field formating Event functions
    ' In order that they do not impose format restrictions if the user is about to click the 
    ' 'Cancel' button.
    '
    Get
      Return False
    End Get
    Set(ByVal Value As Boolean)
    End Set
  End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
  Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
    Get
      Return False
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
    Get
      Return True
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmRiskExposureChangeReport"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  Public Sub New(ByVal pMainForm As VeniceMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()

    _MainForm = pMainForm
    AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate
    AddHandler ReportTimer.Tick, AddressOf MainForm.IncrementStatusBarTimerEvent

    _FormOpenFailed = False

    ' ******************************************************
    ' Form Specific Settings :
    ' ******************************************************

    ' Form Permissioning :-

    THIS_FORM_PermissionArea = Me.Name
    THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

    ' 'This' form ID

    THIS_FORM_FormID = VeniceFormID.frmRiskExposureChangeReport

    ' Format Event Handlers for form controls

    AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

    AddHandler Combo_Fund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_Instrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_LimitCategory.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_TransactionGroupStart.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_TransactionGroupEnd.KeyUp, AddressOf MainForm.ComboSelectAsYouType

    ' Set up the ToolTip
    MainForm.SetFormToolTip(Me, FormTooltip)

    ' ******************************************************
    ' End Form Specific.
    ' ******************************************************

    THIS_FORM_ChangeID = RenaissanceChangeID.KnowledgeDate

  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
  Public Sub ResetForm() Implements StandardVeniceForm.ResetForm

    Call Form_Load(Me, New System.EventArgs)
  End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
  Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
    ALWAYS_CLOSE_THIS_FORM = True
    Me.Close()
  End Sub

    ''' <summary>
    ''' Handles the FormClosing event of the frmRiskExposureChangeReport control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.FormClosingEventArgs"/> instance containing the event data.</param>
  Private Sub frmRiskExposureChangeReport_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
    If (ReportWorker IsNot Nothing) Then
      If ReportWorker.IsBusy Then
        e.Cancel = True
        Exit Sub
      End If

      Try
        RemoveHandler ReportTimer.Tick, AddressOf MainForm.IncrementStatusBarTimerEvent
        RemoveHandler ReportWorker.DoWork, AddressOf ReportWorker_DoWork
        RemoveHandler ReportWorker.RunWorkerCompleted, AddressOf ReportWorkerCompleted
        ReportWorker.Dispose()
        ReportWorker = Nothing
      Catch ex As Exception
      End Try
    End If

  End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
    _FormOpenFailed = False

    If Not (MainForm Is Nothing) Then
      FormIsValid = True
    Else
      MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If


    ' Check User permissions
    Call CheckPermissions()
    If (HasReadPermission = False) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    ' Build Combos

    Try

      Call SetFundCombo()
      Call SetBookmarkCombo()
      Call SetLimitCategoryCombo()
      Call SetInstrumentCombo()
			Call SetTransactionGroupCombo()

      MainForm.SetComboSelectionLengths(Me)

      Me.Radio_From_Live.Checked = True
      Me.Radio_To_Live.Checked = True
      Me.Combo_MinimumUsage.SelectedIndex = 0
			Me.Check_RecreateExposures.Checked = True

    Catch ex As Exception
    End Try

  End Sub

    ''' <summary>
    ''' Handles the Closing event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
  Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    Dim HideForm As Boolean

    ' Hide or Close this form ?
    ' All depends on how many of this form type are Open or in Cache...


    If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
      HideForm = False
    Else
      HideForm = True
      If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.ReportForm_CacheCount Then
        HideForm = False
      End If
    End If

    If HideForm = True Then
      MainForm.HideInFormsCollection(Me)
      Me.Hide() ' NPP Fix

      e.Cancel = True
    Else
      Try
        MainForm.RemoveFromFormsCollection(Me)
        RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

        RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

        RemoveHandler Combo_Fund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_Instrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_LimitCategory.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_TransactionGroupStart.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_TransactionGroupEnd.KeyUp, AddressOf MainForm.ComboSelectAsYouType

      Catch ex As Exception
      End Try
    End If

  End Sub


#End Region

  ' Routine to handle changes / updates to tables by this and other windows.
  ' If this, or any other, form posts a change to a table, then it will invoke an update event 
  ' detailing what tables have been altered.
  ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
  ' the 'VeniceAutoUpdate' event of the main Venice form.
  ' Each form may them react as appropriate to changes in any table that might impact it.
  '
    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    Dim KnowledgeDateChanged As Boolean
    Dim SetButtonStatus_Flag As Boolean

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

    KnowledgeDateChanged = False
    SetButtonStatus_Flag = False

    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
      KnowledgeDateChanged = True
    End If

    ' ****************************************************************
    ' Check for changes relevant to this form
    ' ****************************************************************

    ' Changes to the KnowledgeDate :-
    If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
      SetButtonStatus_Flag = True
    End If

    ' Changes to the tblFund table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFund) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Call SetFundCombo()
		End If

		' Changes to the tblSophisStatusGroups table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblSophisStatusGroups) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			Call SetTransactionGroupCombo()
		End If

    ' Changes to the tblBookmark table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblBookmark) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Call SetBookmarkCombo()
    End If

    ' Changes to the tblLimitType table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblLimitType) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Call SetLimitCategoryCombo()
    End If

    ' Changes to the tblInstrument table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrument) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Call SetInstrumentCombo()
    End If

    ' Changes to the tblExposure table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblExposure) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Call Set_ExistingDateCombo()
    End If

    ' Changes to the tblUserPermissions table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

      ' Check ongoing permissions.

      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

        FormIsValid = False
        Me.Close()
        Exit Sub
      End If

      SetButtonStatus_Flag = True

    End If

    ' ****************************************************************
    '
    ' ****************************************************************


  End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

  ' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
  Private Sub CheckPermissions()
    ' *****************************************************************************
    '
    ' *****************************************************************************

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

  End Sub


#End Region

#Region " Set Form Combos (Form Specific Code) "

    ''' <summary>
    ''' Sets the fund combo.
    ''' </summary>
  Private Sub SetFundCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_Fund, _
    RenaissanceStandardDatasets.tblFund, _
    "FundName", _
    "FundID", _
    "FundClosed=0", False, True, True)  ' 

  End Sub

    ''' <summary>
    ''' Sets the transaction group combo.
    ''' </summary>
	Private Sub SetTransactionGroupCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_TransactionGroupStart, _
		RenaissanceStandardDatasets.tblSophisStatusGroups, _
		"GROUP_NAME", _
		"GROUP_NAME", _
		"", True, True, True)		' 

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_TransactionGroupEnd, _
		RenaissanceStandardDatasets.tblSophisStatusGroups, _
		"GROUP_NAME", _
		"GROUP_NAME", _
		"", True, True, True)		' 

	End Sub

    ''' <summary>
    ''' Sets the bookmark combo.
    ''' </summary>
  Private Sub SetBookmarkCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_From_Bookmark, _
    RenaissanceStandardDatasets.tblBookmark, _
    "BookMarkDescription", _
    "BookmarkDate", _
    "", False, True, True)    ' 

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_To_Bookmark, _
    RenaissanceStandardDatasets.tblBookmark, _
    "BookMarkDescription", _
    "BookmarkDate", _
    "", False, True, True)    ' 

  End Sub

    ''' <summary>
    ''' Sets the limit category combo.
    ''' </summary>
  Private Sub SetLimitCategoryCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_LimitCategory, _
    RenaissanceStandardDatasets.tblLimitType, _
    "ltpLimitType", _
    "ltpID", _
    "", False, True, True)    ' 

  End Sub

    ''' <summary>
    ''' Sets the instrument combo.
    ''' </summary>
  Private Sub SetInstrumentCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_Instrument, _
    RenaissanceStandardDatasets.tblInstrument, _
    "InstrumentDescription", _
    "InstrumentID", _
    "", False, True, True)    ' 

  End Sub

    ''' <summary>
    ''' Set_s the existing date combo.
    ''' </summary>
  Private Sub Set_ExistingDateCombo()


		If (Combo_Fund.SelectedIndex > 0) And (IsNumeric(Combo_Fund.SelectedValue)) Then

			Dim ThisCommand As New SqlCommand
			Dim ThisConnection As SqlConnection = Nothing
			Dim tblExistingExposureDates As New DataTable

			Try
				' First, Select Trade Blotter entries reflecting trades against previous versions of the given transaction.
				' 

				ThisConnection = MainForm.GetVeniceConnection
				ThisCommand.CommandType = CommandType.Text
				ThisCommand.CommandText = "SELECT DISTINCT expDate FROM dbo.fn_tblExposure_SelectKD(null) WHERE expFund=" & CInt(Combo_Fund.SelectedValue).ToString & "ORDER BY expDate DESC"
				ThisCommand.Connection = ThisConnection

        MainForm.LoadTable_Custom(tblExistingExposureDates, ThisCommand)
        'tblExistingExposureDates.Load(ThisCommand.ExecuteReader)

				Call MainForm.SetTblGenericCombo( _
				 Me.Combo_From_ExistingDates, _
				 tblExistingExposureDates.Select, _
				 "expDate", _
				 "expDate", _
				 "", False, False, True)			 ' 

				Call MainForm.SetTblGenericCombo( _
				 Me.Combo_To_ExistingDates, _
				 tblExistingExposureDates.Select, _
				 "expDate", _
				 "expDate", _
				 "", False, False, True)			 ' 

			Catch ex As Exception
			Finally
				Try
					If (ThisConnection IsNot Nothing) Then
						ThisConnection.Close()
					End If
				Catch ex As Exception
				End Try

				ThisCommand = Nothing
			End Try

		Else
			Combo_From_ExistingDates.Items.Clear()
			Combo_To_ExistingDates.Items.Clear()

		End If

  End Sub
#End Region

#Region " Control Events"

    ''' <summary>
    ''' Handles the CheckedChanged event of the FromRadios control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub FromRadios_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_From_Live.CheckedChanged, Radio_From_WholeDay.CheckedChanged, Radio_From_PreciseTime.CheckedChanged
    ' *******************************************************************************
    '
    '
    ' *******************************************************************************

    If Me.Radio_From_Live.Checked = True Then
      Me.Date_From_KnowledgeDate.Enabled = False
      Me.Date_From_KnowledgeDate.Value = Now.Date
    Else
      Me.Date_From_KnowledgeDate.Enabled = True

      If Me.Radio_From_WholeDay.Checked = True Then
        Me.Date_From_KnowledgeDate.CustomFormat = DISPLAYMEMBER_DATEFORMAT
      Else
        Me.Date_From_KnowledgeDate.CustomFormat = DISPLAYMEMBER_LONGDATEFORMAT
      End If
    End If

  End Sub


    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_From_Bookmark control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_From_Bookmark_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_From_Bookmark.SelectedIndexChanged
    ' *****************************************************************************
    ' If a Bookmark Date is selected, update the Displayed Knowledgedate.
    ' *****************************************************************************

    If Combo_From_Bookmark.SelectedIndex > 0 Then
      If IsDate(Combo_From_Bookmark.SelectedValue) Then
        If CDate(Combo_From_Bookmark.SelectedValue).TimeOfDay.TotalSeconds >= LAST_SECOND Then
          Me.Radio_From_PreciseTime.Checked = False
          Me.Radio_From_WholeDay.Checked = True
          Me.Date_From_KnowledgeDate.Value = CDate(Combo_From_Bookmark.SelectedValue)
        Else
          Me.Radio_From_PreciseTime.Checked = True
          Me.Date_From_KnowledgeDate.Value = CDate(Combo_From_Bookmark.SelectedValue)
        End If
      End If
      Combo_From_Bookmark.SelectedIndex = 0
    End If

  End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the ToRadios control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub ToRadios_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_To_Live.CheckedChanged, Radio_To_WholeDay.CheckedChanged, Radio_To_PreciseTime.CheckedChanged
    ' *******************************************************************************
    '
    '
    ' *******************************************************************************

    If Me.Radio_To_Live.Checked = True Then
      Me.Date_To_KnowledgeDate.Enabled = False
      Me.Date_To_KnowledgeDate.Value = Now.Date
    Else
      Me.Date_To_KnowledgeDate.Enabled = True

      If Me.Radio_To_WholeDay.Checked = True Then
        Me.Date_To_KnowledgeDate.CustomFormat = DISPLAYMEMBER_DATEFORMAT
      Else
        Me.Date_To_KnowledgeDate.CustomFormat = DISPLAYMEMBER_LONGDATEFORMAT
      End If
    End If

  End Sub


    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_To_Bookmark control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_To_Bookmark_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_To_Bookmark.SelectedIndexChanged
    ' *****************************************************************************
    ' If a Bookmark Date is selected, update the Displayed Knowledgedate.
    ' *****************************************************************************

    If Combo_To_Bookmark.SelectedIndex > 0 Then
      If IsDate(Combo_To_Bookmark.SelectedValue) Then
        If CDate(Combo_To_Bookmark.SelectedValue).TimeOfDay.TotalSeconds >= LAST_SECOND Then
          Me.Radio_To_PreciseTime.Checked = False
          Me.Radio_To_WholeDay.Checked = True
          Me.Date_To_KnowledgeDate.Value = CDate(Combo_To_Bookmark.SelectedValue)
        Else
          Me.Radio_To_PreciseTime.Checked = True
          Me.Date_To_KnowledgeDate.Value = CDate(Combo_To_Bookmark.SelectedValue)
        End If
      End If
      Combo_To_Bookmark.SelectedIndex = 0
    End If

  End Sub

    ''' <summary>
    ''' Handles the LostFocus event of the Combo_MinimumUsage control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_MinimumUsage_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Combo_MinimumUsage.LostFocus

    Dim ValueText As String

    Try
      ValueText = Combo_MinimumUsage.Text.Trim

      If ValueText.EndsWith("%") AndAlso (ValueText.Length > 1) Then
        ValueText = ValueText.Substring(0, ValueText.Length - 1)
      End If
      If IsNumeric(ValueText) Then
        If CDbl(ValueText) <= 1 Then
          ' Form a percentage
          Combo_MinimumUsage.Text = (CDbl(ValueText) * 100).ToString & "%"
        Else
          ' Is a Percentage
          Combo_MinimumUsage.Text = ValueText & "%"
        End If
      End If
    Catch ex As Exception
    End Try

  End Sub


    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_From_ExistingDates control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_From_ExistingDates_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_From_ExistingDates.SelectedIndexChanged
    Try
      If (Combo_From_ExistingDates.Focused) Then
        If (Combo_From_ExistingDates.SelectedIndex > 0) AndAlso (IsDate(Combo_From_ExistingDates.SelectedValue)) Then
          Me.Date_From_ReportDate.Value = CDate(Combo_From_ExistingDates.SelectedValue)
        End If
      End If
    Catch ex As Exception
    End Try
  End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_To_ExistingDates control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_To_ExistingDates_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_To_ExistingDates.SelectedIndexChanged
    Try
      If (Combo_To_ExistingDates.Focused) Then
        If (Combo_To_ExistingDates.SelectedIndex > 0) AndAlso (IsDate(Combo_To_ExistingDates.SelectedValue)) Then
          Me.Date_To_ReportDate.Value = CDate(Combo_To_ExistingDates.SelectedValue)
        End If
      End If
    Catch ex As Exception
    End Try
  End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_Fund control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_Fund_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_Fund.SelectedIndexChanged
    If Combo_Fund.Focused Then
      If (Combo_Fund.SelectedIndex > 0) And IsNumeric(Combo_Fund.SelectedValue) Then
        Call Set_ExistingDateCombo()

      End If
    End If
  End Sub

#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "


    ''' <summary>
    ''' Handles the Click event of the btnRunReport control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnRunReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRunReport.Click
    ' *****************************************************************************
    ' Run Report
    ' *****************************************************************************

    Try
      FormControls = MainForm.MainReportHandler.DisableFormControls(Me)
      Form_ProgressBar.Value = Form_ProgressBar.Minimum
      MainForm.SetToolStripText(Label_Status, "Processing Report")

      If (ReportWorker Is Nothing) Then
				ReportWorker = New BackgroundWorkerThreadSafe(Nothing)
        AddHandler ReportWorker.DoWork, AddressOf ReportWorker_DoWork
        AddHandler ReportWorker.RunWorkerCompleted, AddressOf ReportWorkerCompleted
      End If

      ReportTimer.Interval = 25
      ReportTimer.Tag = Me.Form_ProgressBar
      ReportTimer.Start()


      If MainForm.UseReportWorkerThreads Then
        ReportWorker.RunWorkerAsync()
      Else
        RunReport()
        Call ReportWorkerCompleted(Nothing, Nothing)
      End If


    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
    End Try
  End Sub

    ''' <summary>
    ''' Handles the DoWork event of the ReportWorker control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.DoWorkEventArgs"/> instance containing the event data.</param>
  Private Sub ReportWorker_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) ' Handles backgroundWorker1.DoWork
    RunReport()
  End Sub

    ''' <summary>
    ''' Reports the worker completed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.RunWorkerCompletedEventArgs"/> instance containing the event data.</param>
  Private Sub ReportWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) ' Handles backgroundWorker1.RunWorkerCompleted
    Try
      ReportTimer.Stop()
      MainForm.MainReportHandler.EnableFormControls(FormControls)
      MainForm.SetToolStripText(Label_Status, "")

      'Form_ProgressBar = MainForm.ResetProgressBar(Form_StatusStrip, Form_ProgressBar)
      Form_ProgressBar.Value = Form_ProgressBar.Minimum
      Form_ProgressBar.Visible = False
      ReportTimer.Tag = Me.Form_ProgressBar
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
    End Try

  End Sub

    ''' <summary>
    ''' Runs the report.
    ''' </summary>
  Private Sub RunReport()
    ' *****************************************************************************
    ' Run Report
    ' *****************************************************************************

		Dim FundID As Integer
		Dim FundName As String = ""
    Dim ReportDate_From As Date
    Dim PertracDate_From As Date ' End of calendar month previous to Report Date
    Dim ReportDate_To As Date
    Dim PertracDate_To As Date ' End of calendar month previous to Report Date
    Dim SelectLimitCategory As Integer
    Dim SelectMinimumUsage As Double
    Dim SelectInstrument As Integer
		Dim TransactionStatusGroupStart As String
		Dim TransactionStatusGroupEnd As String

    Dim PreciseSystemKnowledgedate As Date
    Dim PreciseExposures_FromKnowledgeDate As Date
    Dim PreciseExposures_ToKnowledgeDate As Date

    Dim AggregateRisk As Boolean
		Dim ExposureFlagsStart As Long = 0L
		Dim ExposureFlagsEnd As Long = 0L

    ' Validate

    If (MainForm.GetComboSelectedValue(Combo_Fund) Is Nothing) OrElse (MainForm.GetComboSelectedIndex(Combo_Fund) <= 0) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Fund must be selected.", "", True)
      Exit Sub
		End If

		FundID = CInt(MainForm.GetComboSelectedValue(Combo_Fund))
    FundName = CStr(MainForm.GetComboSelectedText(Combo_Fund))

		TransactionStatusGroupStart = ""
		If MainForm.GetComboSelectedIndex(Combo_TransactionGroupStart) > 0 Then
			TransactionStatusGroupStart = MainForm.GetComboSelectedValue(Combo_TransactionGroupStart).ToString
			ExposureFlagsStart = ExposureFlagsStart Or RiskExposureFlags.TransactionStatusGroup
		End If

		TransactionStatusGroupEnd = ""
		If MainForm.GetComboSelectedIndex(Combo_TransactionGroupEnd) > 0 Then
			TransactionStatusGroupEnd = MainForm.GetComboSelectedValue(Combo_TransactionGroupEnd).ToString
			ExposureFlagsEnd = ExposureFlagsEnd Or RiskExposureFlags.TransactionStatusGroup
		End If

    ReportDate_From = MainForm.GetDatetimeValue(Date_From_ReportDate).Date
    PertracDate_From = ReportDate_From.AddDays(0 - ReportDate_From.Day)

    ReportDate_To = MainForm.GetDatetimeValue(Date_To_ReportDate).Date
    PertracDate_To = ReportDate_To.AddDays(0 - ReportDate_To.Day)

    ' Resolve Selection Criteria.

    SelectLimitCategory = 0
    If MainForm.GetComboSelectedIndex(Combo_LimitCategory) > 0 Then
      Try
        SelectLimitCategory = CInt(MainForm.GetComboSelectedValue(Combo_LimitCategory))
      Catch ex As Exception
      End Try
    End If

    SelectInstrument = 0
    If MainForm.GetComboSelectedIndex(Combo_Instrument) > 0 Then
      Try
        SelectInstrument = CInt(MainForm.GetComboSelectedValue(Combo_Instrument))
      Catch ex As Exception
      End Try
    End If

    SelectMinimumUsage = 0
    Try
      Dim ValueString As String
      ValueString = MainForm.GetControlText(Combo_MinimumUsage).Trim

      If ValueString.EndsWith("%") Then
        ValueString = ValueString.Substring(0, ValueString.Length - 1)
      End If

      If IsNumeric(ValueString) Then
        Try
          SelectMinimumUsage = CDbl(ValueString) / 100
        Catch ex As Exception
        End Try
      End If
    Catch ex As Exception
    End Try

    ' Aggregate Risk Flags and Exposure Flags

    AggregateRisk = False

    If MainForm.GetCheckBoxChecked(Check_AggregateToParent) Then
      AggregateRisk = True
			ExposureFlagsStart = ExposureFlagsStart Or RiskExposureFlags.AggregatedRisk
			ExposureFlagsEnd = ExposureFlagsEnd Or RiskExposureFlags.AggregatedRisk
		End If

		If MainForm.GetCheckBoxChecked(Check_LookthroughRisk) Then
			ExposureFlagsStart = ExposureFlagsStart Or RiskExposureFlags.FundLookthrough
			ExposureFlagsEnd = ExposureFlagsEnd Or RiskExposureFlags.FundLookthrough
		End If

    ' System KnowledgeDates()

    PreciseSystemKnowledgedate = MainForm.Main_Knowledgedate

    If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) And (MainForm.Main_Knowledgedate.TimeOfDay.TotalSeconds >= LAST_SECOND) Then
			ExposureFlagsStart = ExposureFlagsStart Or RiskExposureFlags.SystemKnowledgeDateWholeDay
			ExposureFlagsEnd = ExposureFlagsEnd Or RiskExposureFlags.SystemKnowledgeDateWholeDay
		End If

    ' Resolve From KnowledgeDates

    PreciseExposures_FromKnowledgeDate = KNOWLEDGEDATE_NOW
    If MainForm.GetRadioChecked(Radio_From_WholeDay) = True Then
      PreciseExposures_FromKnowledgeDate = MainForm.GetDatetimeValue(Date_From_KnowledgeDate).Date
      PreciseExposures_FromKnowledgeDate = PreciseExposures_FromKnowledgeDate.AddSeconds(LAST_SECOND)
			ExposureFlagsStart = ExposureFlagsStart Or RiskExposureFlags.KnowledgeDateWholeDay
    ElseIf Me.Radio_From_PreciseTime.Checked = True Then
      PreciseExposures_FromKnowledgeDate = MainForm.GetDatetimeValue(Date_From_KnowledgeDate)
    End If

    ' Resolve To KnowledgeDates
    PreciseExposures_ToKnowledgeDate = KNOWLEDGEDATE_NOW
    If MainForm.GetRadioChecked(Radio_To_WholeDay) = True Then
      PreciseExposures_ToKnowledgeDate = MainForm.GetDatetimeValue(Date_To_KnowledgeDate).Date
      PreciseExposures_ToKnowledgeDate = PreciseExposures_ToKnowledgeDate.AddSeconds(LAST_SECOND)
			ExposureFlagsEnd = ExposureFlagsEnd Or RiskExposureFlags.KnowledgeDateWholeDay
    ElseIf Me.Radio_To_PreciseTime.Checked = True Then
      PreciseExposures_ToKnowledgeDate = MainForm.GetDatetimeValue(Date_To_KnowledgeDate)
    End If

    ' *********************************************************
    ' 2) Check Exposure Information Exists and is current
    ' *********************************************************

    MainForm.SetToolStripText(Label_Status, "Calculating Exposures")
    If Not (MainForm.InvokeRequired) Then
      Application.DoEvents()
    End If

    ' 2) Check Exposure Information Exists and is current
		If (MainForm.GetCheckBoxChecked(Check_RecreateExposures) = True) OrElse (Trim(TransactionStatusGroupStart).Length > 0) OrElse (Trim(TransactionStatusGroupEnd).Length > 0) Then
			' Always Re-Create information if the Check box is checked

			If Risk_ProcessAllLimits(MainForm, Label_Status, FundID, FundName, PertracDate_From, ReportDate_From, PreciseExposures_FromKnowledgeDate, ExposureFlagsStart, DEFAULT_STATUSGROUPFILTER, DEFAULT_ADMINISTRATORDATESFILTER) = False Then
				MainForm.SetToolStripText(Label_Status, "Status : Error processing Risk Limits")
				MainForm.LogError(Me.Name & ", btnRunReport_Click", LOG_LEVELS.Warning, "", "Error processing Risk Limits.", "", True)
				Exit Sub
			End If

			If Risk_ProcessAllLimits(MainForm, Label_Status, FundID, FundName, PertracDate_To, ReportDate_To, PreciseExposures_ToKnowledgeDate, ExposureFlagsEnd, DEFAULT_STATUSGROUPFILTER, DEFAULT_ADMINISTRATORDATESFILTER) = False Then
				MainForm.SetToolStripText(Label_Status, "Status : Error processing Risk Limits")
				MainForm.LogError(Me.Name & ", btnRunReport_Click", LOG_LEVELS.Warning, "", "Error processing Risk Limits.", "", True)
				Exit Sub
			End If
		Else
			If CheckExposuresExist(MainForm, Label_Status, FundID, ReportDate_From, PreciseExposures_FromKnowledgeDate, ExposureFlagsStart) = False Then
				' Re-Create Exposures if they do not already exist

				If Risk_ProcessAllLimits(MainForm, Label_Status, FundID, FundName, PertracDate_From, ReportDate_From, PreciseExposures_FromKnowledgeDate, ExposureFlagsStart, DEFAULT_STATUSGROUPFILTER, DEFAULT_ADMINISTRATORDATESFILTER) = False Then
					MainForm.SetToolStripText(Label_Status, "Status : Error processing Risk Limits")
					MainForm.LogError(Me.Name & ", btnRunReport_Click", LOG_LEVELS.Warning, "", "Error processing Risk Limits.", "", True)
					Exit Sub
				End If
			Else
				' Fund / Date / KnowledgeDate combination exists, but is it current ?
				' If Knowledgedate was not 'Now' then of course it is, otherwise check underlying tables for updates
				'   If Knowledgedate < SYSTEM_KNOWLEDGEDATE_CUTOFF Then

				If CheckExposuresAreCurrent(MainForm, Label_Status, FundID, ReportDate_From, PreciseExposures_FromKnowledgeDate, ExposureFlagsStart) = False Then
					' Recreate Exposures if Transactions / FX / Price or Limits have been Added / Changed / Updated.

					If Risk_ProcessAllLimits(MainForm, Label_Status, FundID, FundName, PertracDate_From, ReportDate_From, PreciseExposures_FromKnowledgeDate, ExposureFlagsStart, DEFAULT_STATUSGROUPFILTER, DEFAULT_ADMINISTRATORDATESFILTER) = False Then
						MainForm.SetToolStripText(Label_Status, "Status : Error processing Risk Limits")
						MainForm.LogError(Me.Name & ", btnRunReport_Click", LOG_LEVELS.Warning, "", "Error processing Risk Limits.", "", True)
						Exit Sub
					End If
				End If
			End If

			If CheckExposuresExist(MainForm, Label_Status, FundID, ReportDate_To, PreciseExposures_ToKnowledgeDate, ExposureFlagsEnd) = False Then
				' Re-Create Exposures if they do not already exist

				If Risk_ProcessAllLimits(MainForm, Label_Status, FundID, FundName, PertracDate_To, ReportDate_To, PreciseExposures_ToKnowledgeDate, ExposureFlagsEnd, DEFAULT_STATUSGROUPFILTER, DEFAULT_ADMINISTRATORDATESFILTER) = False Then
					MainForm.SetToolStripText(Label_Status, "Status : Error processing Risk Limits")
					MainForm.LogError(Me.Name & ", btnRunReport_Click", LOG_LEVELS.Warning, "", "Error processing Risk Limits.", "", True)
					Exit Sub
				End If
			Else
				' Fund / Date / KnowledgeDate combination exists, but is it current ?
				' If Knowledgedate was not 'Now' then of course it is, otherwise check underlying tables for updates
				'   If Knowledgedate < SYSTEM_KNOWLEDGEDATE_CUTOFF Then

				If CheckExposuresAreCurrent(MainForm, Label_Status, FundID, ReportDate_To, PreciseExposures_ToKnowledgeDate, ExposureFlagsEnd) = False Then
					' Recreate Exposures if Transactions / FX / Price or Limits have been Added / Changed / Updated.

					If Risk_ProcessAllLimits(MainForm, Label_Status, FundID, FundName, PertracDate_To, ReportDate_To, PreciseExposures_ToKnowledgeDate, ExposureFlagsEnd, DEFAULT_STATUSGROUPFILTER, DEFAULT_ADMINISTRATORDATESFILTER) = False Then
						MainForm.SetToolStripText(Label_Status, "Status : Error processing Risk Limits")
						MainForm.LogError(Me.Name & ", btnRunReport_Click", LOG_LEVELS.Warning, "", "Error processing Risk Limits.", "", True)
						Exit Sub
					End If
				End If
			End If

		End If


    ' *********************************************************
    ' 3) Run Report
    ' *********************************************************

    MainForm.SetToolStripText(Label_Status, "Retrieving Data")
    If Not (MainForm.InvokeRequired) Then
      Application.DoEvents()
    End If

    Try
      Dim ReportData As DataTable

			ReportData = MainForm.MainReportHandler.GetData_rptRiskExposuresChangeReport(FundID, ReportDate_From, ReportDate_To, SelectLimitCategory, SelectInstrument, SelectMinimumUsage, ExposureFlagsStart, ExposureFlagsEnd, PreciseExposures_FromKnowledgeDate, PreciseExposures_ToKnowledgeDate, PreciseSystemKnowledgedate)

      Me.MainForm.SetToolStripText(Label_Status, "Processing Report")
      If Not (MainForm.InvokeRequired) Then
        Application.DoEvents()
      End If

      If (Not (ReportData Is Nothing)) AndAlso (ReportData.Rows.Count > 0) Then
				Call MainForm.MainReportHandler.rptRiskExposuresChangeReport(ReportData, FundID, ReportDate_From, ReportDate_To, SelectLimitCategory, SelectInstrument, SelectMinimumUsage, PreciseExposures_FromKnowledgeDate, PreciseExposures_ToKnowledgeDate, PreciseSystemKnowledgedate, Nothing) 'Form_ProgressBar)
      Else
        MainForm.LogError(Me.Name & ", btnRunReport_Click", LOG_LEVELS.Warning, "", "There is no Expense Data for this report selection.", "", True)
      End If

    Catch ex As Exception
    End Try

  End Sub


    ''' <summary>
    ''' Handles the Click event of the btnClose control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
    ' *****************************************************************************
    ' Close Form
    ' *****************************************************************************

    Me.Close()

  End Sub

#End Region

End Class


