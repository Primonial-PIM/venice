' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 11-06-2012
' ***********************************************************************
' <copyright file="VeniceSplash.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.IO

''' <summary>
''' Class VeniceSplash
''' </summary>
Public NotInheritable Class VeniceSplash

    ''' <summary>
    ''' The end time
    ''' </summary>
  Public EndTime As Date

    ''' <summary>
    ''' Initializes a new instance of the <see cref="VeniceSplash"/> class.
    ''' </summary>
  Public Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.

    Me.ControlBox = False

	End Sub

    ''' <summary>
    ''' Handles the FormClosing event of the Splash control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.FormClosingEventArgs"/> instance containing the event data.</param>
	Private Sub Splash_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
		Try
			ClosingTimer.Stop()
		Catch ex As Exception
		End Try
	End Sub


    ''' <summary>
    ''' Starts the timer.
    ''' </summary>
    ''' <param name="pEndTime">The p end time.</param>
  Public Sub StartTimer(ByVal pEndTime As Date)
    EndTime = pEndTime

    Me.ClosingTimer.Start()

  End Sub

    ''' <summary>
    ''' Handles the Tick event of the ClosingTimer control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub ClosingTimer_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClosingTimer.Tick
    If (Now > EndTime) Then
      ClosingTimer.Stop()
			Me.Close()
    End If
  End Sub

End Class
