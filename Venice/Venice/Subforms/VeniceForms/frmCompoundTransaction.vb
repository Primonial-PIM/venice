' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmCompoundTransaction.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports System.Collections
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals

''' <summary>
''' Class frmCompoundTransaction
''' </summary>
Public Class frmCompoundTransaction

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmCompoundTransaction"/> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The LBL transaction
    ''' </summary>
  Friend WithEvents lblTransaction As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit audit ID
    ''' </summary>
  Friend WithEvents editAuditID As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The edit transaction comment
    ''' </summary>
  Friend WithEvents editTransactionComment As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The BTN nav first
    ''' </summary>
  Friend WithEvents btnNavFirst As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN nav prev
    ''' </summary>
  Friend WithEvents btnNavPrev As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN nav next
    ''' </summary>
  Friend WithEvents btnNavNext As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN last
    ''' </summary>
  Friend WithEvents btnLast As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN add
    ''' </summary>
  Friend WithEvents btnAdd As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN delete
    ''' </summary>
  Friend WithEvents btnDelete As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN cancel
    ''' </summary>
  Friend WithEvents btnCancel As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN save
    ''' </summary>
  Friend WithEvents btnSave As System.Windows.Forms.Button
    ''' <summary>
    ''' The combo_ select transaction
    ''' </summary>
  Friend WithEvents Combo_SelectTransaction As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The panel1
    ''' </summary>
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The label1
    ''' </summary>
  Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The group box1
    ''' </summary>
  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The BTN close
    ''' </summary>
  Friend WithEvents btnClose As System.Windows.Forms.Button
    ''' <summary>
    ''' The combo_ transaction fund
    ''' </summary>
  Friend WithEvents Combo_TransactionFund As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label_ cpty is fund
    ''' </summary>
  Friend WithEvents label_CptyIsFund As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ transaction instrument
    ''' </summary>
  Friend WithEvents Combo_TransactionInstrument As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label2
    ''' </summary>
  Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label3
    ''' </summary>
  Friend WithEvents Label3 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ transaction type
    ''' </summary>
  Friend WithEvents Combo_TransactionType As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ transaction counterparty
    ''' </summary>
  Friend WithEvents Combo_TransactionCounterparty As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ transaction group
    ''' </summary>
  Friend WithEvents Combo_TransactionGroup As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label5
    ''' </summary>
  Friend WithEvents Label5 As System.Windows.Forms.Label
    ''' <summary>
    ''' The group box2
    ''' </summary>
  Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The group box3
    ''' </summary>
  Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The combo_ data entry manager
    ''' </summary>
	Friend WithEvents Combo_DataEntryManager As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ transaction manager
    ''' </summary>
	Friend WithEvents Combo_TransactionManager As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ investment manager
    ''' </summary>
	Friend WithEvents Combo_InvestmentManager As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The date_ decision date
    ''' </summary>
	Friend WithEvents Date_DecisionDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The date_ value date
    ''' </summary>
	Friend WithEvents Date_ValueDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The date_ confirmation date
    ''' </summary>
	Friend WithEvents Date_ConfirmationDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The date_ settlement date
    ''' </summary>
	Friend WithEvents Date_SettlementDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The group box4
    ''' </summary>
	Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The combo_ value or amount
    ''' </summary>
	Friend WithEvents Combo_ValueOrAmount As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label12
    ''' </summary>
	Friend WithEvents Label12 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ amount
    ''' </summary>
	Friend WithEvents edit_Amount As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The edit_ price
    ''' </summary>
	Friend WithEvents edit_Price As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The edit_settlement
    ''' </summary>
	Friend WithEvents edit_settlement As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The edit_ costs
    ''' </summary>
	Friend WithEvents edit_Costs As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The label17
    ''' </summary>
	Friend WithEvents Label17 As System.Windows.Forms.Label
    ''' <summary>
    ''' The check_ initial data entry
    ''' </summary>
	Friend WithEvents Check_InitialDataEntry As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ bank confirmation
    ''' </summary>
	Friend WithEvents Check_BankConfirmation As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ instruction given
    ''' </summary>
	Friend WithEvents Check_InstructionGiven As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ final data entry
    ''' </summary>
	Friend WithEvents Check_FinalDataEntry As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ settlement confirmed
    ''' </summary>
	Friend WithEvents Check_SettlementConfirmed As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ price confirmation
    ''' </summary>
	Friend WithEvents Check_PriceConfirmation As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The group box5
    ''' </summary>
	Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The button_ get price
    ''' </summary>
	Friend WithEvents Button_GetPrice As System.Windows.Forms.Button
    ''' <summary>
    ''' The check_ final amount
    ''' </summary>
	Friend WithEvents Check_FinalAmount As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ final price
    ''' </summary>
	Friend WithEvents Check_FinalPrice As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ final settlement
    ''' </summary>
	Friend WithEvents Check_FinalSettlement As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ final costs
    ''' </summary>
	Friend WithEvents Check_FinalCosts As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The label_ compound transaction
    ''' </summary>
	Friend WithEvents Label_CompoundTransaction As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ amount
    ''' </summary>
	Friend WithEvents Label_Amount As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ price
    ''' </summary>
	Friend WithEvents Label_Price As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ settlement
    ''' </summary>
	Friend WithEvents Label_Settlement As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ costs
    ''' </summary>
	Friend WithEvents Label_Costs As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ decision date
    ''' </summary>
	Friend WithEvents Label_DecisionDate As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ trade date
    ''' </summary>
	Friend WithEvents Label_TradeDate As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ confirmation date
    ''' </summary>
	Friend WithEvents Label_ConfirmationDate As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ settlement date
    ''' </summary>
	Friend WithEvents Label_SettlementDate As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ counterparty
    ''' </summary>
	Friend WithEvents Label_Counterparty As System.Windows.Forms.Label
    ''' <summary>
    ''' The button_ view transactions
    ''' </summary>
	Friend WithEvents Button_ViewTransactions As System.Windows.Forms.Button
    ''' <summary>
    ''' The root menu
    ''' </summary>
	Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
    ''' <summary>
    ''' The tab_ transaction details
    ''' </summary>
	Friend WithEvents Tab_TransactionDetails As System.Windows.Forms.TabControl
    ''' <summary>
    ''' The tab_ basic details
    ''' </summary>
	Friend WithEvents Tab_BasicDetails As System.Windows.Forms.TabPage
    ''' <summary>
    ''' The group box7
    ''' </summary>
	Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The check_ dont update price
    ''' </summary>
	Friend WithEvents Check_DontUpdatePrice As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The group box6
    ''' </summary>
	Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The group box8
    ''' </summary>
	Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The tab_ transfer details
    ''' </summary>
	Friend WithEvents Tab_TransferDetails As System.Windows.Forms.TabPage
    ''' <summary>
    ''' The label15
    ''' </summary>
	Friend WithEvents Label15 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ specific initial equalisation
    ''' </summary>
	Friend WithEvents Edit_SpecificInitialEqualisation As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The check_ specific initial equalisation
    ''' </summary>
	Friend WithEvents Check_SpecificInitialEqualisation As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The label16
    ''' </summary>
	Friend WithEvents Label16 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ effective watermark
    ''' </summary>
	Friend WithEvents Edit_EffectiveWatermark As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The label18
    ''' </summary>
	Friend WithEvents Label18 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ effective price
    ''' </summary>
	Friend WithEvents Edit_EffectivePrice As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The check_ is transfer
    ''' </summary>
	Friend WithEvents Check_IsTransfer As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The date time_ effective value date
    ''' </summary>
	Friend WithEvents DateTime_EffectiveValueDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The label19
    ''' </summary>
	Friend WithEvents Label19 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ cost percentage
    ''' </summary>
	Friend WithEvents edit_CostPercentage As RenaissanceControls.PercentageTextBox
    ''' <summary>
    ''' The check_ costs as percent
    ''' </summary>
	Friend WithEvents Check_CostsAsPercent As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The combo_ transfer from counterparty
    ''' </summary>
	Friend WithEvents Combo_TransferFromCounterparty As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label4
    ''' </summary>
	Friend WithEvents Label4 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label9
    ''' </summary>
	Friend WithEvents Label9 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label13
    ''' </summary>
	Friend WithEvents Label13 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label14
    ''' </summary>
	Friend WithEvents Label14 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label10
    ''' </summary>
	Friend WithEvents Label10 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ trade status
    ''' </summary>
	Friend WithEvents Combo_TradeStatus As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label6
    ''' </summary>
	Friend WithEvents Label6 As System.Windows.Forms.Label
    ''' <summary>
    ''' The BTN new using
    ''' </summary>
	Friend WithEvents btnNewUsing As System.Windows.Forms.Button
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Me.lblTransaction = New System.Windows.Forms.Label
		Me.editAuditID = New System.Windows.Forms.TextBox
		Me.editTransactionComment = New System.Windows.Forms.TextBox
		Me.btnNavFirst = New System.Windows.Forms.Button
		Me.btnNavPrev = New System.Windows.Forms.Button
		Me.btnNavNext = New System.Windows.Forms.Button
		Me.btnLast = New System.Windows.Forms.Button
		Me.btnAdd = New System.Windows.Forms.Button
		Me.btnDelete = New System.Windows.Forms.Button
		Me.btnCancel = New System.Windows.Forms.Button
		Me.btnSave = New System.Windows.Forms.Button
		Me.Combo_SelectTransaction = New System.Windows.Forms.ComboBox
		Me.Panel1 = New System.Windows.Forms.Panel
		Me.btnNewUsing = New System.Windows.Forms.Button
		Me.Button_ViewTransactions = New System.Windows.Forms.Button
		Me.Label1 = New System.Windows.Forms.Label
		Me.GroupBox1 = New System.Windows.Forms.GroupBox
		Me.btnClose = New System.Windows.Forms.Button
		Me.Combo_TransactionFund = New System.Windows.Forms.ComboBox
		Me.label_CptyIsFund = New System.Windows.Forms.Label
		Me.Combo_TransactionInstrument = New System.Windows.Forms.ComboBox
		Me.Label2 = New System.Windows.Forms.Label
		Me.Combo_TransactionType = New System.Windows.Forms.ComboBox
		Me.Label3 = New System.Windows.Forms.Label
		Me.Combo_TransactionCounterparty = New System.Windows.Forms.ComboBox
		Me.Label_Counterparty = New System.Windows.Forms.Label
		Me.Combo_TransactionGroup = New System.Windows.Forms.ComboBox
		Me.Label5 = New System.Windows.Forms.Label
		Me.GroupBox2 = New System.Windows.Forms.GroupBox
		Me.GroupBox3 = New System.Windows.Forms.GroupBox
		Me.Combo_DataEntryManager = New System.Windows.Forms.ComboBox
		Me.Combo_TransactionManager = New System.Windows.Forms.ComboBox
		Me.Combo_InvestmentManager = New System.Windows.Forms.ComboBox
		Me.Date_DecisionDate = New System.Windows.Forms.DateTimePicker
		Me.Label_DecisionDate = New System.Windows.Forms.Label
		Me.Date_ValueDate = New System.Windows.Forms.DateTimePicker
		Me.Label_TradeDate = New System.Windows.Forms.Label
		Me.Date_ConfirmationDate = New System.Windows.Forms.DateTimePicker
		Me.Label_ConfirmationDate = New System.Windows.Forms.Label
		Me.Date_SettlementDate = New System.Windows.Forms.DateTimePicker
		Me.Label_SettlementDate = New System.Windows.Forms.Label
		Me.GroupBox4 = New System.Windows.Forms.GroupBox
		Me.Combo_ValueOrAmount = New System.Windows.Forms.ComboBox
		Me.Label12 = New System.Windows.Forms.Label
		Me.edit_Amount = New RenaissanceControls.NumericTextBox
		Me.Label_Amount = New System.Windows.Forms.Label
		Me.edit_Price = New RenaissanceControls.NumericTextBox
		Me.Label_Price = New System.Windows.Forms.Label
		Me.edit_settlement = New RenaissanceControls.NumericTextBox
		Me.Label_Settlement = New System.Windows.Forms.Label
		Me.edit_Costs = New RenaissanceControls.NumericTextBox
		Me.Label_Costs = New System.Windows.Forms.Label
		Me.Check_FinalAmount = New System.Windows.Forms.CheckBox
		Me.Check_FinalPrice = New System.Windows.Forms.CheckBox
		Me.Check_FinalSettlement = New System.Windows.Forms.CheckBox
		Me.Check_FinalCosts = New System.Windows.Forms.CheckBox
		Me.Label17 = New System.Windows.Forms.Label
		Me.Check_InitialDataEntry = New System.Windows.Forms.CheckBox
		Me.Check_BankConfirmation = New System.Windows.Forms.CheckBox
		Me.Check_InstructionGiven = New System.Windows.Forms.CheckBox
		Me.Check_FinalDataEntry = New System.Windows.Forms.CheckBox
		Me.Check_SettlementConfirmed = New System.Windows.Forms.CheckBox
		Me.Check_PriceConfirmation = New System.Windows.Forms.CheckBox
		Me.GroupBox5 = New System.Windows.Forms.GroupBox
		Me.Button_GetPrice = New System.Windows.Forms.Button
		Me.Label_CompoundTransaction = New System.Windows.Forms.Label
		Me.RootMenu = New System.Windows.Forms.MenuStrip
		Me.Tab_TransactionDetails = New System.Windows.Forms.TabControl
		Me.Tab_BasicDetails = New System.Windows.Forms.TabPage
		Me.Check_CostsAsPercent = New System.Windows.Forms.CheckBox
		Me.edit_CostPercentage = New RenaissanceControls.PercentageTextBox
		Me.GroupBox7 = New System.Windows.Forms.GroupBox
		Me.Check_DontUpdatePrice = New System.Windows.Forms.CheckBox
		Me.GroupBox6 = New System.Windows.Forms.GroupBox
		Me.GroupBox8 = New System.Windows.Forms.GroupBox
		Me.Tab_TransferDetails = New System.Windows.Forms.TabPage
		Me.Label9 = New System.Windows.Forms.Label
		Me.Label13 = New System.Windows.Forms.Label
		Me.Label14 = New System.Windows.Forms.Label
		Me.Label10 = New System.Windows.Forms.Label
		Me.Combo_TransferFromCounterparty = New System.Windows.Forms.ComboBox
		Me.Label4 = New System.Windows.Forms.Label
		Me.Label15 = New System.Windows.Forms.Label
		Me.Edit_SpecificInitialEqualisation = New RenaissanceControls.NumericTextBox
		Me.Check_SpecificInitialEqualisation = New System.Windows.Forms.CheckBox
		Me.Label16 = New System.Windows.Forms.Label
		Me.Edit_EffectiveWatermark = New RenaissanceControls.NumericTextBox
		Me.Label18 = New System.Windows.Forms.Label
		Me.Edit_EffectivePrice = New RenaissanceControls.NumericTextBox
		Me.Check_IsTransfer = New System.Windows.Forms.CheckBox
		Me.DateTime_EffectiveValueDate = New System.Windows.Forms.DateTimePicker
		Me.Label19 = New System.Windows.Forms.Label
		Me.Combo_TradeStatus = New System.Windows.Forms.ComboBox
		Me.Label6 = New System.Windows.Forms.Label
		Me.Panel1.SuspendLayout()
		Me.Tab_TransactionDetails.SuspendLayout()
		Me.Tab_BasicDetails.SuspendLayout()
		Me.GroupBox6.SuspendLayout()
		Me.Tab_TransferDetails.SuspendLayout()
		Me.SuspendLayout()
		'
		'lblTransaction
		'
		Me.lblTransaction.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.lblTransaction.Location = New System.Drawing.Point(8, 501)
		Me.lblTransaction.Name = "lblTransaction"
		Me.lblTransaction.Size = New System.Drawing.Size(112, 20)
		Me.lblTransaction.TabIndex = 39
		Me.lblTransaction.Text = "Comment"
		'
		'editAuditID
		'
		Me.editAuditID.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.editAuditID.Enabled = False
		Me.editAuditID.Location = New System.Drawing.Point(520, 32)
		Me.editAuditID.Name = "editAuditID"
		Me.editAuditID.Size = New System.Drawing.Size(64, 20)
		Me.editAuditID.TabIndex = 1
		'
		'editTransactionComment
		'
		Me.editTransactionComment.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.editTransactionComment.Location = New System.Drawing.Point(120, 501)
		Me.editTransactionComment.Multiline = True
		Me.editTransactionComment.Name = "editTransactionComment"
		Me.editTransactionComment.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
		Me.editTransactionComment.Size = New System.Drawing.Size(464, 56)
		Me.editTransactionComment.TabIndex = 29
		'
		'btnNavFirst
		'
		Me.btnNavFirst.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnNavFirst.Location = New System.Drawing.Point(104, 8)
		Me.btnNavFirst.Name = "btnNavFirst"
		Me.btnNavFirst.Size = New System.Drawing.Size(40, 28)
		Me.btnNavFirst.TabIndex = 1
		Me.btnNavFirst.Text = "<<"
		'
		'btnNavPrev
		'
		Me.btnNavPrev.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnNavPrev.Location = New System.Drawing.Point(145, 8)
		Me.btnNavPrev.Name = "btnNavPrev"
		Me.btnNavPrev.Size = New System.Drawing.Size(35, 28)
		Me.btnNavPrev.TabIndex = 2
		Me.btnNavPrev.Text = "<"
		'
		'btnNavNext
		'
		Me.btnNavNext.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnNavNext.Location = New System.Drawing.Point(184, 8)
		Me.btnNavNext.Name = "btnNavNext"
		Me.btnNavNext.Size = New System.Drawing.Size(35, 28)
		Me.btnNavNext.TabIndex = 3
		Me.btnNavNext.Text = ">"
		'
		'btnLast
		'
		Me.btnLast.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnLast.Location = New System.Drawing.Point(220, 8)
		Me.btnLast.Name = "btnLast"
		Me.btnLast.Size = New System.Drawing.Size(40, 28)
		Me.btnLast.TabIndex = 4
		Me.btnLast.Text = ">>"
		'
		'btnAdd
		'
		Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnAdd.Location = New System.Drawing.Point(272, 8)
		Me.btnAdd.Name = "btnAdd"
		Me.btnAdd.Size = New System.Drawing.Size(75, 28)
		Me.btnAdd.TabIndex = 5
		Me.btnAdd.Text = "&New"
		'
		'btnDelete
		'
		Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnDelete.Location = New System.Drawing.Point(224, 645)
		Me.btnDelete.Name = "btnDelete"
		Me.btnDelete.Size = New System.Drawing.Size(75, 28)
		Me.btnDelete.TabIndex = 32
		Me.btnDelete.Text = "&Delete"
		'
		'btnCancel
		'
		Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
		Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnCancel.Location = New System.Drawing.Point(308, 645)
		Me.btnCancel.Name = "btnCancel"
		Me.btnCancel.Size = New System.Drawing.Size(75, 28)
		Me.btnCancel.TabIndex = 33
		Me.btnCancel.Text = "&Cancel"
		'
		'btnSave
		'
		Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnSave.Location = New System.Drawing.Point(136, 645)
		Me.btnSave.Name = "btnSave"
		Me.btnSave.Size = New System.Drawing.Size(75, 28)
		Me.btnSave.TabIndex = 31
		Me.btnSave.Text = "&Save"
		'
		'Combo_SelectTransaction
		'
		Me.Combo_SelectTransaction.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_SelectTransaction.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_SelectTransaction.Location = New System.Drawing.Point(120, 32)
		Me.Combo_SelectTransaction.Name = "Combo_SelectTransaction"
		Me.Combo_SelectTransaction.Size = New System.Drawing.Size(376, 21)
		Me.Combo_SelectTransaction.TabIndex = 0
		'
		'Panel1
		'
		Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Panel1.Controls.Add(Me.btnNewUsing)
		Me.Panel1.Controls.Add(Me.btnNavFirst)
		Me.Panel1.Controls.Add(Me.btnNavPrev)
		Me.Panel1.Controls.Add(Me.btnNavNext)
		Me.Panel1.Controls.Add(Me.btnLast)
		Me.Panel1.Controls.Add(Me.btnAdd)
		Me.Panel1.Controls.Add(Me.Button_ViewTransactions)
		Me.Panel1.Location = New System.Drawing.Point(80, 589)
		Me.Panel1.Name = "Panel1"
		Me.Panel1.Size = New System.Drawing.Size(444, 48)
		Me.Panel1.TabIndex = 30
		'
		'btnNewUsing
		'
		Me.btnNewUsing.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnNewUsing.Location = New System.Drawing.Point(356, 8)
		Me.btnNewUsing.Name = "btnNewUsing"
		Me.btnNewUsing.Size = New System.Drawing.Size(75, 28)
		Me.btnNewUsing.TabIndex = 6
		Me.btnNewUsing.Text = "New &Using"
		'
		'Button_ViewTransactions
		'
		Me.Button_ViewTransactions.Enabled = False
		Me.Button_ViewTransactions.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Button_ViewTransactions.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Button_ViewTransactions.Location = New System.Drawing.Point(8, 8)
		Me.Button_ViewTransactions.Name = "Button_ViewTransactions"
		Me.Button_ViewTransactions.Size = New System.Drawing.Size(90, 28)
		Me.Button_ViewTransactions.TabIndex = 0
		Me.Button_ViewTransactions.Text = "View Transaction(s)"
		'
		'Label1
		'
		Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label1.Location = New System.Drawing.Point(8, 32)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(100, 23)
		Me.Label1.TabIndex = 17
		Me.Label1.Text = "Select"
		'
		'GroupBox1
		'
		Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.GroupBox1.Location = New System.Drawing.Point(8, 60)
		Me.GroupBox1.Name = "GroupBox1"
		Me.GroupBox1.Size = New System.Drawing.Size(576, 4)
		Me.GroupBox1.TabIndex = 77
		Me.GroupBox1.TabStop = False
		Me.GroupBox1.Text = "GroupBox1"
		'
		'btnClose
		'
		Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnClose.Location = New System.Drawing.Point(392, 645)
		Me.btnClose.Name = "btnClose"
		Me.btnClose.Size = New System.Drawing.Size(75, 28)
		Me.btnClose.TabIndex = 34
		Me.btnClose.Text = "&Close"
		'
		'Combo_TransactionFund
		'
		Me.Combo_TransactionFund.Enabled = False
		Me.Combo_TransactionFund.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_TransactionFund.Location = New System.Drawing.Point(120, 70)
		Me.Combo_TransactionFund.Name = "Combo_TransactionFund"
		Me.Combo_TransactionFund.Size = New System.Drawing.Size(466, 21)
		Me.Combo_TransactionFund.TabIndex = 2
		'
		'label_CptyIsFund
		'
		Me.label_CptyIsFund.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.label_CptyIsFund.Location = New System.Drawing.Point(8, 74)
		Me.label_CptyIsFund.Name = "label_CptyIsFund"
		Me.label_CptyIsFund.Size = New System.Drawing.Size(80, 16)
		Me.label_CptyIsFund.TabIndex = 80
		Me.label_CptyIsFund.Text = "Fund"
		'
		'Combo_TransactionInstrument
		'
		Me.Combo_TransactionInstrument.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_TransactionInstrument.Enabled = False
		Me.Combo_TransactionInstrument.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_TransactionInstrument.Location = New System.Drawing.Point(120, 96)
		Me.Combo_TransactionInstrument.Name = "Combo_TransactionInstrument"
		Me.Combo_TransactionInstrument.Size = New System.Drawing.Size(466, 21)
		Me.Combo_TransactionInstrument.TabIndex = 3
		'
		'Label2
		'
		Me.Label2.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label2.Location = New System.Drawing.Point(8, 100)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(104, 16)
		Me.Label2.TabIndex = 82
		Me.Label2.Text = "Instrument"
		'
		'Combo_TransactionType
		'
		Me.Combo_TransactionType.Enabled = False
		Me.Combo_TransactionType.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_TransactionType.Location = New System.Drawing.Point(120, 122)
		Me.Combo_TransactionType.Name = "Combo_TransactionType"
		Me.Combo_TransactionType.Size = New System.Drawing.Size(174, 21)
		Me.Combo_TransactionType.TabIndex = 4
		'
		'Label3
		'
		Me.Label3.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label3.Location = New System.Drawing.Point(8, 126)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(102, 16)
		Me.Label3.TabIndex = 84
		Me.Label3.Text = "Transaction Type"
		'
		'Combo_TransactionCounterparty
		'
		Me.Combo_TransactionCounterparty.Enabled = False
		Me.Combo_TransactionCounterparty.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_TransactionCounterparty.Location = New System.Drawing.Point(120, 148)
		Me.Combo_TransactionCounterparty.Name = "Combo_TransactionCounterparty"
		Me.Combo_TransactionCounterparty.Size = New System.Drawing.Size(174, 21)
		Me.Combo_TransactionCounterparty.TabIndex = 5
		'
		'Label_Counterparty
		'
		Me.Label_Counterparty.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label_Counterparty.Location = New System.Drawing.Point(8, 152)
		Me.Label_Counterparty.Name = "Label_Counterparty"
		Me.Label_Counterparty.Size = New System.Drawing.Size(104, 16)
		Me.Label_Counterparty.TabIndex = 86
		Me.Label_Counterparty.Text = "Counterparty"
		'
		'Combo_TransactionGroup
		'
		Me.Combo_TransactionGroup.Enabled = False
		Me.Combo_TransactionGroup.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_TransactionGroup.Location = New System.Drawing.Point(120, 174)
		Me.Combo_TransactionGroup.Name = "Combo_TransactionGroup"
		Me.Combo_TransactionGroup.Size = New System.Drawing.Size(174, 21)
		Me.Combo_TransactionGroup.TabIndex = 6
		'
		'Label5
		'
		Me.Label5.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label5.Location = New System.Drawing.Point(8, 178)
		Me.Label5.Name = "Label5"
		Me.Label5.Size = New System.Drawing.Size(104, 16)
		Me.Label5.TabIndex = 88
		Me.Label5.Text = "Transaction Group"
		'
		'GroupBox2
		'
		Me.GroupBox2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.GroupBox2.Location = New System.Drawing.Point(8, 203)
		Me.GroupBox2.Name = "GroupBox2"
		Me.GroupBox2.Size = New System.Drawing.Size(576, 4)
		Me.GroupBox2.TabIndex = 89
		Me.GroupBox2.TabStop = False
		Me.GroupBox2.Text = "GroupBox2"
		'
		'GroupBox3
		'
		Me.GroupBox3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.GroupBox3.Location = New System.Drawing.Point(296, 117)
		Me.GroupBox3.Name = "GroupBox3"
		Me.GroupBox3.Size = New System.Drawing.Size(2, 78)
		Me.GroupBox3.TabIndex = 90
		Me.GroupBox3.TabStop = False
		Me.GroupBox3.Text = "GroupBox3"
		'
		'Combo_DataEntryManager
		'
		Me.Combo_DataEntryManager.Enabled = False
		Me.Combo_DataEntryManager.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_DataEntryManager.Location = New System.Drawing.Point(361, 151)
		Me.Combo_DataEntryManager.Name = "Combo_DataEntryManager"
		Me.Combo_DataEntryManager.Size = New System.Drawing.Size(22, 21)
		Me.Combo_DataEntryManager.TabIndex = 202
		Me.Combo_DataEntryManager.Visible = False
		'
		'Combo_TransactionManager
		'
		Me.Combo_TransactionManager.Enabled = False
		Me.Combo_TransactionManager.Location = New System.Drawing.Point(333, 150)
		Me.Combo_TransactionManager.Name = "Combo_TransactionManager"
		Me.Combo_TransactionManager.Size = New System.Drawing.Size(22, 21)
		Me.Combo_TransactionManager.TabIndex = 201
		Me.Combo_TransactionManager.Visible = False
		'
		'Combo_InvestmentManager
		'
		Me.Combo_InvestmentManager.Enabled = False
		Me.Combo_InvestmentManager.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_InvestmentManager.Location = New System.Drawing.Point(305, 149)
		Me.Combo_InvestmentManager.Name = "Combo_InvestmentManager"
		Me.Combo_InvestmentManager.Size = New System.Drawing.Size(22, 21)
		Me.Combo_InvestmentManager.TabIndex = 200
		Me.Combo_InvestmentManager.Visible = False
		'
		'Date_DecisionDate
		'
		Me.Date_DecisionDate.Location = New System.Drawing.Point(120, 211)
		Me.Date_DecisionDate.Name = "Date_DecisionDate"
		Me.Date_DecisionDate.Size = New System.Drawing.Size(174, 20)
		Me.Date_DecisionDate.TabIndex = 10
		'
		'Label_DecisionDate
		'
		Me.Label_DecisionDate.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label_DecisionDate.Location = New System.Drawing.Point(8, 215)
		Me.Label_DecisionDate.Name = "Label_DecisionDate"
		Me.Label_DecisionDate.Size = New System.Drawing.Size(100, 16)
		Me.Label_DecisionDate.TabIndex = 98
		Me.Label_DecisionDate.Text = "Decision Date"
		'
		'Date_ValueDate
		'
		Me.Date_ValueDate.Location = New System.Drawing.Point(120, 235)
		Me.Date_ValueDate.Name = "Date_ValueDate"
		Me.Date_ValueDate.Size = New System.Drawing.Size(174, 20)
		Me.Date_ValueDate.TabIndex = 11
		'
		'Label_TradeDate
		'
		Me.Label_TradeDate.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label_TradeDate.Location = New System.Drawing.Point(8, 239)
		Me.Label_TradeDate.Name = "Label_TradeDate"
		Me.Label_TradeDate.Size = New System.Drawing.Size(100, 16)
		Me.Label_TradeDate.TabIndex = 100
		Me.Label_TradeDate.Text = "Trade Date"
		'
		'Date_ConfirmationDate
		'
		Me.Date_ConfirmationDate.Location = New System.Drawing.Point(412, 235)
		Me.Date_ConfirmationDate.Name = "Date_ConfirmationDate"
		Me.Date_ConfirmationDate.Size = New System.Drawing.Size(174, 20)
		Me.Date_ConfirmationDate.TabIndex = 13
		'
		'Label_ConfirmationDate
		'
		Me.Label_ConfirmationDate.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label_ConfirmationDate.Location = New System.Drawing.Point(300, 239)
		Me.Label_ConfirmationDate.Name = "Label_ConfirmationDate"
		Me.Label_ConfirmationDate.Size = New System.Drawing.Size(108, 16)
		Me.Label_ConfirmationDate.TabIndex = 104
		Me.Label_ConfirmationDate.Text = "Confirmation Date"
		'
		'Date_SettlementDate
		'
		Me.Date_SettlementDate.Location = New System.Drawing.Point(412, 211)
		Me.Date_SettlementDate.Name = "Date_SettlementDate"
		Me.Date_SettlementDate.Size = New System.Drawing.Size(174, 20)
		Me.Date_SettlementDate.TabIndex = 12
		'
		'Label_SettlementDate
		'
		Me.Label_SettlementDate.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label_SettlementDate.Location = New System.Drawing.Point(300, 215)
		Me.Label_SettlementDate.Name = "Label_SettlementDate"
		Me.Label_SettlementDate.Size = New System.Drawing.Size(108, 16)
		Me.Label_SettlementDate.TabIndex = 102
		Me.Label_SettlementDate.Text = "Settlement Date"
		'
		'GroupBox4
		'
		Me.GroupBox4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.GroupBox4.Location = New System.Drawing.Point(8, 263)
		Me.GroupBox4.Name = "GroupBox4"
		Me.GroupBox4.Size = New System.Drawing.Size(576, 4)
		Me.GroupBox4.TabIndex = 105
		Me.GroupBox4.TabStop = False
		Me.GroupBox4.Text = "GroupBox4"
		'
		'Combo_ValueOrAmount
		'
		Me.Combo_ValueOrAmount.Enabled = False
		Me.Combo_ValueOrAmount.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_ValueOrAmount.Items.AddRange(New Object() {"Value", "Amount"})
		Me.Combo_ValueOrAmount.Location = New System.Drawing.Point(118, 4)
		Me.Combo_ValueOrAmount.Name = "Combo_ValueOrAmount"
		Me.Combo_ValueOrAmount.Size = New System.Drawing.Size(174, 21)
		Me.Combo_ValueOrAmount.TabIndex = 0
		'
		'Label12
		'
		Me.Label12.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label12.Location = New System.Drawing.Point(6, 8)
		Me.Label12.Name = "Label12"
		Me.Label12.Size = New System.Drawing.Size(104, 16)
		Me.Label12.TabIndex = 20
		Me.Label12.Text = "Value or Amount"
		'
		'edit_Amount
		'
		Me.edit_Amount.Location = New System.Drawing.Point(118, 52)
		Me.edit_Amount.Name = "edit_Amount"
		Me.edit_Amount.RenaissanceTag = Nothing
		Me.edit_Amount.Size = New System.Drawing.Size(140, 20)
		Me.edit_Amount.TabIndex = 1
		Me.edit_Amount.Text = "0.00"
		Me.edit_Amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		Me.edit_Amount.TextFormat = "#,##0.00"
		Me.edit_Amount.Value = 0
		'
		'Label_Amount
		'
		Me.Label_Amount.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label_Amount.Location = New System.Drawing.Point(6, 56)
		Me.Label_Amount.Name = "Label_Amount"
		Me.Label_Amount.Size = New System.Drawing.Size(108, 16)
		Me.Label_Amount.TabIndex = 21
		Me.Label_Amount.Text = "Amount"
		'
		'edit_Price
		'
		Me.edit_Price.Location = New System.Drawing.Point(118, 76)
		Me.edit_Price.Name = "edit_Price"
		Me.edit_Price.RenaissanceTag = Nothing
		Me.edit_Price.Size = New System.Drawing.Size(140, 20)
		Me.edit_Price.TabIndex = 2
		Me.edit_Price.Text = "0.00"
		Me.edit_Price.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		Me.edit_Price.TextFormat = "#,##0.00"
		Me.edit_Price.Value = 0
		'
		'Label_Price
		'
		Me.Label_Price.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label_Price.Location = New System.Drawing.Point(6, 80)
		Me.Label_Price.Name = "Label_Price"
		Me.Label_Price.Size = New System.Drawing.Size(68, 16)
		Me.Label_Price.TabIndex = 22
		Me.Label_Price.Text = "Price"
		'
		'edit_settlement
		'
		Me.edit_settlement.Location = New System.Drawing.Point(118, 124)
		Me.edit_settlement.Name = "edit_settlement"
		Me.edit_settlement.RenaissanceTag = Nothing
		Me.edit_settlement.Size = New System.Drawing.Size(140, 20)
		Me.edit_settlement.TabIndex = 4
		Me.edit_settlement.Text = "0.00"
		Me.edit_settlement.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		Me.edit_settlement.TextFormat = "#,##0.00"
		Me.edit_settlement.Value = 0
		'
		'Label_Settlement
		'
		Me.Label_Settlement.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label_Settlement.Location = New System.Drawing.Point(6, 128)
		Me.Label_Settlement.Name = "Label_Settlement"
		Me.Label_Settlement.Size = New System.Drawing.Size(108, 16)
		Me.Label_Settlement.TabIndex = 25
		Me.Label_Settlement.Text = "Settlement"
		'
		'edit_Costs
		'
		Me.edit_Costs.Location = New System.Drawing.Point(118, 100)
		Me.edit_Costs.Name = "edit_Costs"
		Me.edit_Costs.RenaissanceTag = Nothing
		Me.edit_Costs.Size = New System.Drawing.Size(140, 20)
		Me.edit_Costs.TabIndex = 3
		Me.edit_Costs.Text = "0.00"
		Me.edit_Costs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		Me.edit_Costs.TextFormat = "#,##0.00"
		Me.edit_Costs.Value = 0
		'
		'Label_Costs
		'
		Me.Label_Costs.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label_Costs.Location = New System.Drawing.Point(6, 104)
		Me.Label_Costs.Name = "Label_Costs"
		Me.Label_Costs.Size = New System.Drawing.Size(68, 16)
		Me.Label_Costs.TabIndex = 24
		Me.Label_Costs.Text = "Costs"
		'
		'Check_FinalAmount
		'
		Me.Check_FinalAmount.BackColor = System.Drawing.SystemColors.Control
		Me.Check_FinalAmount.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Check_FinalAmount.Location = New System.Drawing.Point(262, 52)
		Me.Check_FinalAmount.Name = "Check_FinalAmount"
		Me.Check_FinalAmount.Size = New System.Drawing.Size(16, 16)
		Me.Check_FinalAmount.TabIndex = 5
		Me.Check_FinalAmount.UseVisualStyleBackColor = False
		'
		'Check_FinalPrice
		'
		Me.Check_FinalPrice.BackColor = System.Drawing.SystemColors.Control
		Me.Check_FinalPrice.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Check_FinalPrice.Location = New System.Drawing.Point(262, 76)
		Me.Check_FinalPrice.Name = "Check_FinalPrice"
		Me.Check_FinalPrice.Size = New System.Drawing.Size(16, 16)
		Me.Check_FinalPrice.TabIndex = 6
		Me.Check_FinalPrice.UseVisualStyleBackColor = False
		'
		'Check_FinalSettlement
		'
		Me.Check_FinalSettlement.BackColor = System.Drawing.SystemColors.Control
		Me.Check_FinalSettlement.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Check_FinalSettlement.Location = New System.Drawing.Point(262, 124)
		Me.Check_FinalSettlement.Name = "Check_FinalSettlement"
		Me.Check_FinalSettlement.Size = New System.Drawing.Size(16, 16)
		Me.Check_FinalSettlement.TabIndex = 8
		Me.Check_FinalSettlement.UseVisualStyleBackColor = False
		'
		'Check_FinalCosts
		'
		Me.Check_FinalCosts.BackColor = System.Drawing.SystemColors.Control
		Me.Check_FinalCosts.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Check_FinalCosts.Location = New System.Drawing.Point(262, 100)
		Me.Check_FinalCosts.Name = "Check_FinalCosts"
		Me.Check_FinalCosts.Size = New System.Drawing.Size(16, 16)
		Me.Check_FinalCosts.TabIndex = 7
		Me.Check_FinalCosts.UseVisualStyleBackColor = False
		'
		'Label17
		'
		Me.Label17.Location = New System.Drawing.Point(254, 32)
		Me.Label17.Name = "Label17"
		Me.Label17.Size = New System.Drawing.Size(32, 15)
		Me.Label17.TabIndex = 3
		Me.Label17.Text = "Final"
		'
		'Check_InitialDataEntry
		'
		Me.Check_InitialDataEntry.BackColor = System.Drawing.SystemColors.Control
		Me.Check_InitialDataEntry.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Check_InitialDataEntry.Location = New System.Drawing.Point(296, 100)
		Me.Check_InitialDataEntry.Name = "Check_InitialDataEntry"
		Me.Check_InitialDataEntry.Size = New System.Drawing.Size(129, 16)
		Me.Check_InitialDataEntry.TabIndex = 11
		Me.Check_InitialDataEntry.Text = "Initial Data Entry"
		Me.Check_InitialDataEntry.UseVisualStyleBackColor = False
		'
		'Check_BankConfirmation
		'
		Me.Check_BankConfirmation.BackColor = System.Drawing.SystemColors.Control
		Me.Check_BankConfirmation.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Check_BankConfirmation.Location = New System.Drawing.Point(296, 76)
		Me.Check_BankConfirmation.Name = "Check_BankConfirmation"
		Me.Check_BankConfirmation.Size = New System.Drawing.Size(129, 16)
		Me.Check_BankConfirmation.TabIndex = 10
		Me.Check_BankConfirmation.Text = "Bank Confirmation"
		Me.Check_BankConfirmation.UseVisualStyleBackColor = False
		'
		'Check_InstructionGiven
		'
		Me.Check_InstructionGiven.BackColor = System.Drawing.SystemColors.Control
		Me.Check_InstructionGiven.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Check_InstructionGiven.Location = New System.Drawing.Point(296, 52)
		Me.Check_InstructionGiven.Name = "Check_InstructionGiven"
		Me.Check_InstructionGiven.Size = New System.Drawing.Size(129, 16)
		Me.Check_InstructionGiven.TabIndex = 9
		Me.Check_InstructionGiven.Text = "Instruction Given"
		Me.Check_InstructionGiven.UseVisualStyleBackColor = False
		'
		'Check_FinalDataEntry
		'
		Me.Check_FinalDataEntry.BackColor = System.Drawing.SystemColors.Control
		Me.Check_FinalDataEntry.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Check_FinalDataEntry.Location = New System.Drawing.Point(432, 100)
		Me.Check_FinalDataEntry.Name = "Check_FinalDataEntry"
		Me.Check_FinalDataEntry.Size = New System.Drawing.Size(133, 16)
		Me.Check_FinalDataEntry.TabIndex = 14
		Me.Check_FinalDataEntry.Text = "Final Data Entry"
		Me.Check_FinalDataEntry.UseVisualStyleBackColor = False
		'
		'Check_SettlementConfirmed
		'
		Me.Check_SettlementConfirmed.BackColor = System.Drawing.SystemColors.Control
		Me.Check_SettlementConfirmed.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Check_SettlementConfirmed.Location = New System.Drawing.Point(432, 76)
		Me.Check_SettlementConfirmed.Name = "Check_SettlementConfirmed"
		Me.Check_SettlementConfirmed.Size = New System.Drawing.Size(133, 16)
		Me.Check_SettlementConfirmed.TabIndex = 13
		Me.Check_SettlementConfirmed.Text = "Settlement Confirmed"
		Me.Check_SettlementConfirmed.UseVisualStyleBackColor = False
		'
		'Check_PriceConfirmation
		'
		Me.Check_PriceConfirmation.BackColor = System.Drawing.SystemColors.Control
		Me.Check_PriceConfirmation.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Check_PriceConfirmation.Location = New System.Drawing.Point(432, 52)
		Me.Check_PriceConfirmation.Name = "Check_PriceConfirmation"
		Me.Check_PriceConfirmation.Size = New System.Drawing.Size(133, 16)
		Me.Check_PriceConfirmation.TabIndex = 12
		Me.Check_PriceConfirmation.Text = "Price Confirmation"
		Me.Check_PriceConfirmation.UseVisualStyleBackColor = False
		'
		'GroupBox5
		'
		Me.GroupBox5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.GroupBox5.Location = New System.Drawing.Point(8, 489)
		Me.GroupBox5.Name = "GroupBox5"
		Me.GroupBox5.Size = New System.Drawing.Size(576, 4)
		Me.GroupBox5.TabIndex = 135
		Me.GroupBox5.TabStop = False
		Me.GroupBox5.Text = "GroupBox5"
		'
		'Button_GetPrice
		'
		Me.Button_GetPrice.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Button_GetPrice.Location = New System.Drawing.Point(78, 76)
		Me.Button_GetPrice.Name = "Button_GetPrice"
		Me.Button_GetPrice.Size = New System.Drawing.Size(36, 20)
		Me.Button_GetPrice.TabIndex = 23
		Me.Button_GetPrice.TabStop = False
		Me.Button_GetPrice.Text = "Get"
		'
		'Label_CompoundTransaction
		'
		Me.Label_CompoundTransaction.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label_CompoundTransaction.Location = New System.Drawing.Point(120, 561)
		Me.Label_CompoundTransaction.Name = "Label_CompoundTransaction"
		Me.Label_CompoundTransaction.Size = New System.Drawing.Size(464, 20)
		Me.Label_CompoundTransaction.TabIndex = 137
		Me.Label_CompoundTransaction.Text = "Compound Transaction."
		'
		'RootMenu
		'
		Me.RootMenu.Location = New System.Drawing.Point(0, 0)
		Me.RootMenu.Name = "RootMenu"
		Me.RootMenu.Size = New System.Drawing.Size(592, 24)
		Me.RootMenu.TabIndex = 138
		Me.RootMenu.Text = " "
		'
		'Tab_TransactionDetails
		'
		Me.Tab_TransactionDetails.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Tab_TransactionDetails.Controls.Add(Me.Tab_BasicDetails)
		Me.Tab_TransactionDetails.Controls.Add(Me.Tab_TransferDetails)
		Me.Tab_TransactionDetails.Location = New System.Drawing.Point(5, 273)
		Me.Tab_TransactionDetails.Name = "Tab_TransactionDetails"
		Me.Tab_TransactionDetails.SelectedIndex = 0
		Me.Tab_TransactionDetails.Size = New System.Drawing.Size(581, 214)
		Me.Tab_TransactionDetails.TabIndex = 14
		'
		'Tab_BasicDetails
		'
		Me.Tab_BasicDetails.BackColor = System.Drawing.SystemColors.Control
		Me.Tab_BasicDetails.Controls.Add(Me.Check_CostsAsPercent)
		Me.Tab_BasicDetails.Controls.Add(Me.edit_CostPercentage)
		Me.Tab_BasicDetails.Controls.Add(Me.GroupBox7)
		Me.Tab_BasicDetails.Controls.Add(Me.Check_DontUpdatePrice)
		Me.Tab_BasicDetails.Controls.Add(Me.GroupBox6)
		Me.Tab_BasicDetails.Controls.Add(Me.Button_GetPrice)
		Me.Tab_BasicDetails.Controls.Add(Me.edit_Price)
		Me.Tab_BasicDetails.Controls.Add(Me.Label12)
		Me.Tab_BasicDetails.Controls.Add(Me.Check_FinalDataEntry)
		Me.Tab_BasicDetails.Controls.Add(Me.Combo_ValueOrAmount)
		Me.Tab_BasicDetails.Controls.Add(Me.Check_SettlementConfirmed)
		Me.Tab_BasicDetails.Controls.Add(Me.Label_Amount)
		Me.Tab_BasicDetails.Controls.Add(Me.Check_PriceConfirmation)
		Me.Tab_BasicDetails.Controls.Add(Me.edit_Amount)
		Me.Tab_BasicDetails.Controls.Add(Me.Check_InitialDataEntry)
		Me.Tab_BasicDetails.Controls.Add(Me.Label_Price)
		Me.Tab_BasicDetails.Controls.Add(Me.Check_BankConfirmation)
		Me.Tab_BasicDetails.Controls.Add(Me.Label_Costs)
		Me.Tab_BasicDetails.Controls.Add(Me.Check_InstructionGiven)
		Me.Tab_BasicDetails.Controls.Add(Me.edit_Costs)
		Me.Tab_BasicDetails.Controls.Add(Me.Label17)
		Me.Tab_BasicDetails.Controls.Add(Me.Label_Settlement)
		Me.Tab_BasicDetails.Controls.Add(Me.Check_FinalSettlement)
		Me.Tab_BasicDetails.Controls.Add(Me.edit_settlement)
		Me.Tab_BasicDetails.Controls.Add(Me.Check_FinalCosts)
		Me.Tab_BasicDetails.Controls.Add(Me.Check_FinalAmount)
		Me.Tab_BasicDetails.Controls.Add(Me.Check_FinalPrice)
		Me.Tab_BasicDetails.Location = New System.Drawing.Point(4, 22)
		Me.Tab_BasicDetails.Name = "Tab_BasicDetails"
		Me.Tab_BasicDetails.Padding = New System.Windows.Forms.Padding(3)
		Me.Tab_BasicDetails.Size = New System.Drawing.Size(573, 188)
		Me.Tab_BasicDetails.TabIndex = 0
		Me.Tab_BasicDetails.Text = "Value and Status"
		'
		'Check_CostsAsPercent
		'
		Me.Check_CostsAsPercent.BackColor = System.Drawing.SystemColors.Control
		Me.Check_CostsAsPercent.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Check_CostsAsPercent.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Check_CostsAsPercent.Location = New System.Drawing.Point(86, 100)
		Me.Check_CostsAsPercent.Name = "Check_CostsAsPercent"
		Me.Check_CostsAsPercent.Size = New System.Drawing.Size(28, 19)
		Me.Check_CostsAsPercent.TabIndex = 29
		Me.Check_CostsAsPercent.Text = "%"
		Me.Check_CostsAsPercent.UseVisualStyleBackColor = False
		'
		'edit_CostPercentage
		'
		Me.edit_CostPercentage.Location = New System.Drawing.Point(118, 100)
		Me.edit_CostPercentage.Name = "edit_CostPercentage"
		Me.edit_CostPercentage.RenaissanceTag = Nothing
		Me.edit_CostPercentage.Size = New System.Drawing.Size(140, 20)
		Me.edit_CostPercentage.TabIndex = 28
		Me.edit_CostPercentage.Text = "0.00%"
		Me.edit_CostPercentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		Me.edit_CostPercentage.TextFormat = "#,##0.00%"
		Me.edit_CostPercentage.Value = 0
		Me.edit_CostPercentage.Visible = False
		'
		'GroupBox7
		'
		Me.GroupBox7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.GroupBox7.Location = New System.Drawing.Point(10, 176)
		Me.GroupBox7.Name = "GroupBox7"
		Me.GroupBox7.Size = New System.Drawing.Size(555, 4)
		Me.GroupBox7.TabIndex = 27
		Me.GroupBox7.TabStop = False
		Me.GroupBox7.Text = "GroupBox7"
		'
		'Check_DontUpdatePrice
		'
		Me.Check_DontUpdatePrice.BackColor = System.Drawing.SystemColors.Control
		Me.Check_DontUpdatePrice.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Check_DontUpdatePrice.Location = New System.Drawing.Point(13, 155)
		Me.Check_DontUpdatePrice.Name = "Check_DontUpdatePrice"
		Me.Check_DontUpdatePrice.Size = New System.Drawing.Size(572, 19)
		Me.Check_DontUpdatePrice.TabIndex = 15
		Me.Check_DontUpdatePrice.Text = "Exempt this Transaction from Price Auto-update process."
		Me.Check_DontUpdatePrice.UseVisualStyleBackColor = False
		'
		'GroupBox6
		'
		Me.GroupBox6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.GroupBox6.Controls.Add(Me.GroupBox8)
		Me.GroupBox6.Location = New System.Drawing.Point(10, 148)
		Me.GroupBox6.Name = "GroupBox6"
		Me.GroupBox6.Size = New System.Drawing.Size(555, 4)
		Me.GroupBox6.TabIndex = 26
		Me.GroupBox6.TabStop = False
		Me.GroupBox6.Text = "GroupBox6"
		'
		'GroupBox8
		'
		Me.GroupBox8.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.GroupBox8.Location = New System.Drawing.Point(0, 26)
		Me.GroupBox8.Name = "GroupBox8"
		Me.GroupBox8.Size = New System.Drawing.Size(555, 4)
		Me.GroupBox8.TabIndex = 0
		Me.GroupBox8.TabStop = False
		Me.GroupBox8.Text = "GroupBox8"
		'
		'Tab_TransferDetails
		'
		Me.Tab_TransferDetails.BackColor = System.Drawing.SystemColors.Control
		Me.Tab_TransferDetails.Controls.Add(Me.Label9)
		Me.Tab_TransferDetails.Controls.Add(Me.Label13)
		Me.Tab_TransferDetails.Controls.Add(Me.Label14)
		Me.Tab_TransferDetails.Controls.Add(Me.Label10)
		Me.Tab_TransferDetails.Controls.Add(Me.Combo_TransferFromCounterparty)
		Me.Tab_TransferDetails.Controls.Add(Me.Label4)
		Me.Tab_TransferDetails.Controls.Add(Me.Label15)
		Me.Tab_TransferDetails.Controls.Add(Me.Edit_SpecificInitialEqualisation)
		Me.Tab_TransferDetails.Controls.Add(Me.Check_SpecificInitialEqualisation)
		Me.Tab_TransferDetails.Controls.Add(Me.Label16)
		Me.Tab_TransferDetails.Controls.Add(Me.Edit_EffectiveWatermark)
		Me.Tab_TransferDetails.Controls.Add(Me.Label18)
		Me.Tab_TransferDetails.Controls.Add(Me.Edit_EffectivePrice)
		Me.Tab_TransferDetails.Controls.Add(Me.Check_IsTransfer)
		Me.Tab_TransferDetails.Controls.Add(Me.DateTime_EffectiveValueDate)
		Me.Tab_TransferDetails.Controls.Add(Me.Label19)
		Me.Tab_TransferDetails.Location = New System.Drawing.Point(4, 22)
		Me.Tab_TransferDetails.Name = "Tab_TransferDetails"
		Me.Tab_TransferDetails.Padding = New System.Windows.Forms.Padding(3)
		Me.Tab_TransferDetails.Size = New System.Drawing.Size(573, 188)
		Me.Tab_TransferDetails.TabIndex = 1
		Me.Tab_TransferDetails.Text = "Transfer Details"
		'
		'Label9
		'
		Me.Label9.Location = New System.Drawing.Point(359, 131)
		Me.Label9.Name = "Label9"
		Me.Label9.Size = New System.Drawing.Size(208, 16)
		Me.Label9.TabIndex = 92
		Me.Label9.Text = "Use with care."
		'
		'Label13
		'
		Me.Label13.Location = New System.Drawing.Point(359, 83)
		Me.Label13.Name = "Label13"
		Me.Label13.Size = New System.Drawing.Size(208, 39)
		Me.Label13.TabIndex = 91
		Me.Label13.Text = " Zero for normal Watermarks or Specify if different."
		'
		'Label14
		'
		Me.Label14.Location = New System.Drawing.Point(359, 57)
		Me.Label14.Name = "Label14"
		Me.Label14.Size = New System.Drawing.Size(208, 16)
		Me.Label14.TabIndex = 90
		Me.Label14.Text = "(Original Subscription Price)"
		'
		'Label10
		'
		Me.Label10.Location = New System.Drawing.Point(359, 32)
		Me.Label10.Name = "Label10"
		Me.Label10.Size = New System.Drawing.Size(208, 16)
		Me.Label10.TabIndex = 89
		Me.Label10.Text = "(Original Subscription Value Date)"
		'
		'Combo_TransferFromCounterparty
		'
		Me.Combo_TransferFromCounterparty.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_TransferFromCounterparty.Enabled = False
		Me.Combo_TransferFromCounterparty.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_TransferFromCounterparty.Location = New System.Drawing.Point(179, 156)
		Me.Combo_TransferFromCounterparty.Name = "Combo_TransferFromCounterparty"
		Me.Combo_TransferFromCounterparty.Size = New System.Drawing.Size(388, 21)
		Me.Combo_TransferFromCounterparty.TabIndex = 87
		'
		'Label4
		'
		Me.Label4.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label4.Location = New System.Drawing.Point(34, 159)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(142, 16)
		Me.Label4.TabIndex = 88
		Me.Label4.Text = "Transfer from Counterparty"
		'
		'Label15
		'
		Me.Label15.Location = New System.Drawing.Point(34, 131)
		Me.Label15.Name = "Label15"
		Me.Label15.Size = New System.Drawing.Size(136, 16)
		Me.Label15.TabIndex = 9
		Me.Label15.Text = "Initial Equalisation"
		'
		'Edit_SpecificInitialEqualisation
		'
		Me.Edit_SpecificInitialEqualisation.Location = New System.Drawing.Point(179, 128)
		Me.Edit_SpecificInitialEqualisation.Name = "Edit_SpecificInitialEqualisation"
		Me.Edit_SpecificInitialEqualisation.RenaissanceTag = Nothing
		Me.Edit_SpecificInitialEqualisation.Size = New System.Drawing.Size(174, 20)
		Me.Edit_SpecificInitialEqualisation.TabIndex = 5
		Me.Edit_SpecificInitialEqualisation.Text = "0.00"
		Me.Edit_SpecificInitialEqualisation.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		Me.Edit_SpecificInitialEqualisation.TextFormat = "#,##0.00"
		Me.Edit_SpecificInitialEqualisation.Value = 0
		'
		'Check_SpecificInitialEqualisation
		'
		Me.Check_SpecificInitialEqualisation.BackColor = System.Drawing.SystemColors.Control
		Me.Check_SpecificInitialEqualisation.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Check_SpecificInitialEqualisation.Location = New System.Drawing.Point(37, 106)
		Me.Check_SpecificInitialEqualisation.Name = "Check_SpecificInitialEqualisation"
		Me.Check_SpecificInitialEqualisation.Size = New System.Drawing.Size(273, 16)
		Me.Check_SpecificInitialEqualisation.TabIndex = 4
		Me.Check_SpecificInitialEqualisation.Text = "Specify an Initial Equalisation value."
		Me.Check_SpecificInitialEqualisation.UseVisualStyleBackColor = False
		'
		'Label16
		'
		Me.Label16.Location = New System.Drawing.Point(34, 83)
		Me.Label16.Name = "Label16"
		Me.Label16.Size = New System.Drawing.Size(139, 16)
		Me.Label16.TabIndex = 8
		Me.Label16.Text = "Equalisation Watermark"
		'
		'Edit_EffectiveWatermark
		'
		Me.Edit_EffectiveWatermark.Location = New System.Drawing.Point(179, 80)
		Me.Edit_EffectiveWatermark.Name = "Edit_EffectiveWatermark"
		Me.Edit_EffectiveWatermark.RenaissanceTag = Nothing
		Me.Edit_EffectiveWatermark.Size = New System.Drawing.Size(174, 20)
		Me.Edit_EffectiveWatermark.TabIndex = 3
		Me.Edit_EffectiveWatermark.Text = "0.00"
		Me.Edit_EffectiveWatermark.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		Me.Edit_EffectiveWatermark.TextFormat = "#,##0.00"
		Me.Edit_EffectiveWatermark.Value = 0
		'
		'Label18
		'
		Me.Label18.Location = New System.Drawing.Point(34, 57)
		Me.Label18.Name = "Label18"
		Me.Label18.Size = New System.Drawing.Size(139, 16)
		Me.Label18.TabIndex = 7
		Me.Label18.Text = "Equalisation Price"
		'
		'Edit_EffectivePrice
		'
		Me.Edit_EffectivePrice.Location = New System.Drawing.Point(179, 54)
		Me.Edit_EffectivePrice.Name = "Edit_EffectivePrice"
		Me.Edit_EffectivePrice.RenaissanceTag = Nothing
		Me.Edit_EffectivePrice.Size = New System.Drawing.Size(174, 20)
		Me.Edit_EffectivePrice.TabIndex = 2
		Me.Edit_EffectivePrice.Text = "0.00"
		Me.Edit_EffectivePrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		Me.Edit_EffectivePrice.TextFormat = "#,##0.00"
		Me.Edit_EffectivePrice.Value = 0
		'
		'Check_IsTransfer
		'
		Me.Check_IsTransfer.BackColor = System.Drawing.SystemColors.Control
		Me.Check_IsTransfer.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Check_IsTransfer.Location = New System.Drawing.Point(7, 6)
		Me.Check_IsTransfer.Name = "Check_IsTransfer"
		Me.Check_IsTransfer.Size = New System.Drawing.Size(273, 16)
		Me.Check_IsTransfer.TabIndex = 0
		Me.Check_IsTransfer.Text = "Transaction is a Transfer"
		Me.Check_IsTransfer.UseVisualStyleBackColor = False
		'
		'DateTime_EffectiveValueDate
		'
		Me.DateTime_EffectiveValueDate.Location = New System.Drawing.Point(179, 28)
		Me.DateTime_EffectiveValueDate.Name = "DateTime_EffectiveValueDate"
		Me.DateTime_EffectiveValueDate.Size = New System.Drawing.Size(174, 20)
		Me.DateTime_EffectiveValueDate.TabIndex = 1
		'
		'Label19
		'
		Me.Label19.Location = New System.Drawing.Point(34, 32)
		Me.Label19.Name = "Label19"
		Me.Label19.Size = New System.Drawing.Size(139, 16)
		Me.Label19.TabIndex = 6
		Me.Label19.Text = "Equalisation Value Date"
		'
		'Combo_TradeStatus
		'
		Me.Combo_TradeStatus.Enabled = False
		Me.Combo_TradeStatus.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_TradeStatus.Location = New System.Drawing.Point(412, 121)
		Me.Combo_TradeStatus.Name = "Combo_TradeStatus"
		Me.Combo_TradeStatus.Size = New System.Drawing.Size(174, 21)
		Me.Combo_TradeStatus.TabIndex = 7
		'
		'Label6
		'
		Me.Label6.Location = New System.Drawing.Point(300, 125)
		Me.Label6.Name = "Label6"
		Me.Label6.Size = New System.Drawing.Size(102, 16)
		Me.Label6.TabIndex = 145
		Me.Label6.Text = "Trade Status"
		'
		'frmCompoundTransaction
		'
		Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
		Me.ClientSize = New System.Drawing.Size(592, 683)
		Me.Controls.Add(Me.Combo_TradeStatus)
		Me.Controls.Add(Me.Label6)
		Me.Controls.Add(Me.Tab_TransactionDetails)
		Me.Controls.Add(Me.RootMenu)
		Me.Controls.Add(Me.Label_CompoundTransaction)
		Me.Controls.Add(Me.GroupBox5)
		Me.Controls.Add(Me.GroupBox4)
		Me.Controls.Add(Me.Date_ConfirmationDate)
		Me.Controls.Add(Me.Label_ConfirmationDate)
		Me.Controls.Add(Me.Date_SettlementDate)
		Me.Controls.Add(Me.Label_SettlementDate)
		Me.Controls.Add(Me.Date_ValueDate)
		Me.Controls.Add(Me.Label_TradeDate)
		Me.Controls.Add(Me.Date_DecisionDate)
		Me.Controls.Add(Me.Label_DecisionDate)
		Me.Controls.Add(Me.Combo_DataEntryManager)
		Me.Controls.Add(Me.Combo_TransactionManager)
		Me.Controls.Add(Me.Combo_InvestmentManager)
		Me.Controls.Add(Me.GroupBox2)
		Me.Controls.Add(Me.Combo_TransactionGroup)
		Me.Controls.Add(Me.Label5)
		Me.Controls.Add(Me.Combo_TransactionCounterparty)
		Me.Controls.Add(Me.Label_Counterparty)
		Me.Controls.Add(Me.Combo_TransactionType)
		Me.Controls.Add(Me.Label3)
		Me.Controls.Add(Me.Combo_TransactionInstrument)
		Me.Controls.Add(Me.Label2)
		Me.Controls.Add(Me.Combo_TransactionFund)
		Me.Controls.Add(Me.label_CptyIsFund)
		Me.Controls.Add(Me.btnClose)
		Me.Controls.Add(Me.GroupBox1)
		Me.Controls.Add(Me.Label1)
		Me.Controls.Add(Me.Panel1)
		Me.Controls.Add(Me.Combo_SelectTransaction)
		Me.Controls.Add(Me.btnSave)
		Me.Controls.Add(Me.editAuditID)
		Me.Controls.Add(Me.editTransactionComment)
		Me.Controls.Add(Me.lblTransaction)
		Me.Controls.Add(Me.btnDelete)
		Me.Controls.Add(Me.btnCancel)
		Me.Controls.Add(Me.GroupBox3)
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Name = "frmCompoundTransaction"
		Me.Text = "Add/Edit Compound Transaction"
		Me.Panel1.ResumeLayout(False)
		Me.Tab_TransactionDetails.ResumeLayout(False)
		Me.Tab_BasicDetails.ResumeLayout(False)
		Me.Tab_BasicDetails.PerformLayout()
		Me.GroupBox6.ResumeLayout(False)
		Me.Tab_TransferDetails.ResumeLayout(False)
		Me.Tab_TransferDetails.PerformLayout()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

#End Region

#Region " Form Locals and Constants "


	' Form 'Parent', the Main Venice form.
	' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
	Private WithEvents _MainForm As VeniceMain

	' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
	Private FormTooltip As New ToolTip()

	' Form Menu


	' Form Constants, specific to the table being updated.

    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
	Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

	' Form Locals, initialised on 'New' defining what standard data items to use
    ''' <summary>
  ''' Variable holding the form's data table name. taken from the "ThisStandardDataset" object.
    ''' </summary>
	Private THIS_TABLENAME As String
    ''' <summary>
  ''' Variable holding the form's data adaptor name. taken from the "ThisStandardDataset" object.
    ''' </summary>
	Private THIS_ADAPTORNAME As String
    ''' <summary>
  ''' Variable holding the form's dataset name. taken from the "ThisStandardDataset" object.
    ''' </summary>
	Private THIS_DATASETNAME As String


	' The standard ChangeID for this form. e.g. tblCompoundTransaction
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
	Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

    ''' <summary>
  ''' Principal control used for selecting items on this form. 
    ''' </summary>
	Private THIS_FORM_SelectingCombo As ComboBox
    ''' <summary>
  ''' Control to select after proessing the "New" button.
    ''' </summary>
	Private THIS_FORM_NewMoveToControl As Control

	' Form Specific Order fields
    ''' <summary>
  ''' Field Name to show in the Select combo.
    ''' </summary>
	Private THIS_FORM_SelectBy As String
    ''' <summary>
  ''' Field Name to order the Select combo by.
    ''' </summary>
	Private THIS_FORM_OrderBy As String

    ''' <summary>
  ''' The field name that holds the unique identifier for each entry. e.g. FundID for the tblFund table.
    ''' </summary>
	Private THIS_FORM_ValueMember As String

	' Form Specific Select Criteria
    ''' <summary>
    ''' The THI s_ FOR m_ select criteria
    ''' </summary>
	Private THIS_FORM_SelectCriteria As String = "True"

	' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
	Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
	Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

	' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
	Private THIS_FORM_FormID As VeniceFormID

	' Data Structures

    ''' <summary>
    ''' My dataset
    ''' </summary>
	Private myDataset As RenaissanceDataClass.DSCompoundTransaction		 ' Form Specific !!!!
    ''' <summary>
    ''' My table
    ''' </summary>
	Private myTable As RenaissanceDataClass.DSCompoundTransaction.tblCompoundTransactionDataTable
    ''' <summary>
    ''' My connection
    ''' </summary>
	Private myConnection As SqlConnection
    ''' <summary>
    ''' My adaptor
    ''' </summary>
	Private myAdaptor As SqlDataAdapter

    ''' <summary>
    ''' The this standard dataset
    ''' </summary>
	Private ThisStandardDataset As RenaissanceGlobals.StandardDataset


	' Active Element.

    ''' <summary>
    ''' The sorted rows
    ''' </summary>
	Private SortedRows() As DataRow
    ''' <summary>
    ''' The select by sorted rows
    ''' </summary>
	Private SelectBySortedRows() As DataRow
    ''' <summary>
    ''' The this data row
    ''' </summary>
	Private thisDataRow As RenaissanceDataClass.DSCompoundTransaction.tblCompoundTransactionRow		' Form Specific !!!!
    ''' <summary>
    ''' The this audit ID
    ''' </summary>
	Private thisAuditID As Integer
    ''' <summary>
    ''' The this position
    ''' </summary>
	Private thisPosition As Integer
    ''' <summary>
    ''' The __ is over cancel button
    ''' </summary>
	Private __IsOverCancelButton As Boolean
    ''' <summary>
    ''' The _ in use
    ''' </summary>
	Private _InUse As Boolean

	' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
	Private FormIsValid As Boolean
    ''' <summary>
    ''' The form changed
    ''' </summary>
	Private FormChanged As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
	Private _FormOpenFailed As Boolean
    ''' <summary>
    ''' The in paint
    ''' </summary>
	Private InPaint As Boolean
    ''' <summary>
    ''' The add new record
    ''' </summary>
	Private AddNewRecord As Boolean

	' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
	Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
	Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
	Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
	Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
	Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
	Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return __IsOverCancelButton
		End Get
		Set(ByVal Value As Boolean)
			__IsOverCancelButton = Value
		End Set
	End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
	Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
		Get
			Return InPaint
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
		Get
			Return _InUse
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

    ''' <summary>
    ''' Gets or sets the form select criteria.
    ''' </summary>
    ''' <value>The form select criteria.</value>
	Public Property FormSelectCriteria() As String
		Get
			Return THIS_FORM_SelectCriteria
		End Get
		Set(ByVal Value As String)
			If (THIS_FORM_SelectCriteria <> Value) Then
				THIS_FORM_SelectCriteria = Value
				If (Value.Equals("True")) Then
					Me.Text = "Add/Edit Compound Transaction"
				Else
					Me.Text = "Add/Edit Compound Transaction (Selected View)"
				End If
				Call Form_Load(Me, New System.EventArgs)
			End If
		End Set
	End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmCompoundTransaction"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
	Public Sub New(ByVal pMainForm As VeniceMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()

		_MainForm = pMainForm
		AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

		_FormOpenFailed = False

		' ******************************************************
		' Form Specific Settings :
		' ******************************************************

		THIS_FORM_SelectingCombo = Me.Combo_SelectTransaction
		THIS_FORM_NewMoveToControl = Me.editTransactionComment

		' Default Select and Order fields.

		THIS_FORM_SelectBy = "TransactionTicket"
		THIS_FORM_OrderBy = "TransactionTicket"

		THIS_FORM_ValueMember = "TransactionCompoundID"

		' Form Permissioning :-

		THIS_FORM_PermissionArea = "frmTransaction"
		THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

		' 'This' form ID

		THIS_FORM_FormID = VeniceFormID.frmCompoundTransaction

		' This form's dataset type.

		ThisStandardDataset = RenaissanceStandardDatasets.tblCompoundTransaction ' This Defines the Form Data !!! 

		' Format Event Handlers for form controls

		AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

		' Form Control Changed events
		AddHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
		AddHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		AddHandler Combo_TransactionFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_TransactionInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_TransactionType.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_TradeStatus.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_TransactionCounterparty.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_TransactionGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
		AddHandler Combo_InvestmentManager.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_TransactionManager.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_DataEntryManager.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		AddHandler Combo_TransactionFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_TransactionInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_TransactionType.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_TradeStatus.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_TransactionCounterparty.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_InvestmentManager.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_TransactionManager.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_DataEntryManager.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

		AddHandler Combo_TransactionFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_TransactionInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_TransactionType.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_TradeStatus.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_TransactionCounterparty.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_InvestmentManager.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_TransactionManager.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_DataEntryManager.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

		AddHandler Combo_TransactionFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_TransactionInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_TransactionType.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_TradeStatus.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_TransactionCounterparty.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_InvestmentManager.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_TransactionManager.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_DataEntryManager.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

		AddHandler Combo_TransactionFund.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_TransactionInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_TransactionType.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_TradeStatus.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_TransactionCounterparty.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_TransactionGroup.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_TransactionGroup.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_InvestmentManager.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_TransactionManager.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_DataEntryManager.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Date_DecisionDate.ValueChanged, AddressOf Me.FormControlChanged
		AddHandler Date_ValueDate.ValueChanged, AddressOf Me.FormControlChanged
		AddHandler Date_SettlementDate.ValueChanged, AddressOf Me.FormControlChanged
		AddHandler Date_ConfirmationDate.ValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_ValueOrAmount.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler edit_Amount.TextChanged, AddressOf Me.FormControlChanged
		AddHandler edit_Price.TextChanged, AddressOf Me.FormControlChanged
		AddHandler edit_Costs.TextChanged, AddressOf Me.FormControlChanged
		AddHandler edit_settlement.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Check_FinalAmount.CheckStateChanged, AddressOf Me.FormControlChanged
		AddHandler Check_FinalPrice.CheckStateChanged, AddressOf Me.FormControlChanged
		AddHandler Check_FinalCosts.CheckStateChanged, AddressOf Me.FormControlChanged
		AddHandler Check_FinalSettlement.CheckStateChanged, AddressOf Me.FormControlChanged
		AddHandler Check_InstructionGiven.CheckStateChanged, AddressOf Me.FormControlChanged
		AddHandler Check_BankConfirmation.CheckStateChanged, AddressOf Me.FormControlChanged
		AddHandler Check_InitialDataEntry.CheckStateChanged, AddressOf Me.FormControlChanged
		AddHandler Check_PriceConfirmation.CheckStateChanged, AddressOf Me.FormControlChanged
		AddHandler Check_SettlementConfirmed.CheckStateChanged, AddressOf Me.FormControlChanged
		AddHandler Check_FinalDataEntry.CheckStateChanged, AddressOf Me.FormControlChanged
		AddHandler editTransactionComment.TextChanged, AddressOf Me.FormControlChanged

		AddHandler Check_IsTransfer.CheckStateChanged, AddressOf Me.FormControlChanged
		AddHandler Check_CostsAsPercent.CheckStateChanged, AddressOf Me.FormControlChanged
		AddHandler Check_SpecificInitialEqualisation.CheckStateChanged, AddressOf Me.FormControlChanged
		AddHandler Check_DontUpdatePrice.CheckStateChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_EffectivePrice.ValueChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_SpecificInitialEqualisation.ValueChanged, AddressOf Me.FormControlChanged
		AddHandler Edit_EffectiveWatermark.ValueChanged, AddressOf Me.FormControlChanged
		AddHandler edit_CostPercentage.ValueChanged, AddressOf Me.FormControlChanged
		AddHandler DateTime_EffectiveValueDate.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_TransferFromCounterparty.SelectedValueChanged, AddressOf Me.FormControlChanged

		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

		' ******************************************************
		' End Form Specific.
		' ******************************************************

		' Data object names standard to this Form type.

		THIS_TABLENAME = ThisStandardDataset.TableName
		THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
		THIS_DATASETNAME = ThisStandardDataset.DatasetName

		THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

		' Establish / Retrieve data objects for this form.

		myConnection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)
		myAdaptor = MainForm.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, VENICE_CONNECTION, THIS_TABLENAME)
		myDataset = MainForm.Load_Table(ThisStandardDataset, False)
		myTable = myDataset.Tables(0)

		MainForm.BuildStandardFormMenu(Me.RootMenu, myDataset.Tables(0), AddressOf SelectMenuEvent, AddressOf OrderMenuEvent, AddressOf AuditReportMenuEvent)

	End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

	' Form Initialisation code.
	'
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
	Public Sub ResetForm() Implements StandardVeniceForm.ResetForm
		THIS_FORM_SelectCriteria = "True"
		THIS_FORM_SelectBy = "TransactionTicket"
		THIS_FORM_OrderBy = "TransactionTicket"

		Call Form_Load(Me, New System.EventArgs)
	End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
	Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
		_FormOpenFailed = False
		_InUse = True

		' Initialise Data structures. Connection, Adaptor and Dataset.

		If Not (MainForm Is Nothing) Then
			FormIsValid = True
		Else
			MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myConnection Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myAdaptor Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myDataset Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		' Initialse form

		InPaint = True
		IsOverCancelButton = False

		' Check User permissions
		Call CheckPermissions()
		If (HasReadPermission = False) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		' Build Sorted data list from which this form operates
		Call SetSortedRows()

		Call SetFundCombo()
		Call SetInstrumentCombo()
		Call SetTransactionTypeCombo()
		Call SetTradeStatusCombo()
		Call SetCounterpartyCombo()
		Call SetTransactionGroupCombo()
		Call SetPersonCombos()

		' Display initial record.

		thisPosition = 0
		If THIS_FORM_SelectingCombo.Items.Count > 0 Then
			Me.THIS_FORM_SelectingCombo.SelectedIndex = 0
			thisDataRow = SortedRows(thisPosition)
			Call GetFormData(thisDataRow)
		Else
			Me.THIS_FORM_SelectingCombo.SelectedIndex = (-1)
			Call GetFormData(Nothing)
		End If

		InPaint = False


	End Sub

    ''' <summary>
    ''' Handles the Closing event of the frmCompoundTransaction control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
	Private Sub frmCompoundTransaction_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		Dim HideForm As Boolean

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...

		_InUse = False

		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else
			If (FormChanged = True) Then
				Call SetFormData()
			End If

			HideForm = True
			If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			e.Cancel = True
		Else
			Try
				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

				' Form Control Changed events
				RemoveHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
				RemoveHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				RemoveHandler Combo_TransactionFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_TransactionInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_TransactionType.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_TradeStatus.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_TransactionCounterparty.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_TransactionGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
				RemoveHandler Combo_InvestmentManager.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_TransactionManager.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_DataEntryManager.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				RemoveHandler Combo_TransactionFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_TransactionInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_TransactionType.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_TradeStatus.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_TransactionCounterparty.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_InvestmentManager.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_TransactionManager.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_DataEntryManager.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

				RemoveHandler Combo_TransactionFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_TransactionInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_TransactionType.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_TradeStatus.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_TransactionCounterparty.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_InvestmentManager.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_TransactionManager.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_DataEntryManager.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

				RemoveHandler Combo_TransactionFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_TransactionInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_TransactionType.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_TradeStatus.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_TransactionCounterparty.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_InvestmentManager.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_TransactionManager.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_DataEntryManager.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

				RemoveHandler Combo_TransactionFund.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_TransactionInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_TransactionType.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_TradeStatus.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_TransactionCounterparty.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_TransactionGroup.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_TransactionGroup.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_InvestmentManager.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_TransactionManager.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_DataEntryManager.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Date_DecisionDate.ValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Date_ValueDate.ValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Date_SettlementDate.ValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Date_ConfirmationDate.ValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_ValueOrAmount.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_Amount.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_Price.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_Costs.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_settlement.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_FinalAmount.CheckStateChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_FinalPrice.CheckStateChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_FinalCosts.CheckStateChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_FinalSettlement.CheckStateChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_InstructionGiven.CheckStateChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_BankConfirmation.CheckStateChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_InitialDataEntry.CheckStateChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_PriceConfirmation.CheckStateChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_SettlementConfirmed.CheckStateChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_FinalDataEntry.CheckStateChanged, AddressOf Me.FormControlChanged
				RemoveHandler editTransactionComment.TextChanged, AddressOf Me.FormControlChanged

				RemoveHandler Check_IsTransfer.CheckStateChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_CostsAsPercent.CheckStateChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_SpecificInitialEqualisation.CheckStateChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_DontUpdatePrice.CheckStateChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_EffectivePrice.ValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_SpecificInitialEqualisation.ValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Edit_EffectiveWatermark.ValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_CostPercentage.ValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler DateTime_EffectiveValueDate.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_TransferFromCounterparty.SelectedValueChanged, AddressOf Me.FormControlChanged

			Catch ex As Exception
			End Try
		End If

	End Sub


#End Region

    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		' *******************************************************************************************************
		' Routine to handle changes / updates to tables by this and other windows.
		' If this, or any other, form posts a change to a table, then it will invoke an update event 
		' detailing what tables have been altered.
		' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
		' the 'VeniceAutoUpdate' event of the main Venice form.
		' Each form may them react as appropriate to changes in any table that might impact it.
		'
		' *******************************************************************************************************

		Dim OrgInPaint As Boolean
		Dim KnowledgeDateChanged As Boolean
		Dim SetButtonStatus_Flag As Boolean
		Dim RefreshForm As Boolean

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		OrgInPaint = InPaint
		InPaint = True
		KnowledgeDateChanged = False
		SetButtonStatus_Flag = False

		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
			KnowledgeDateChanged = True
			RefreshForm = True
		End If

		' ****************************************************************
		' Check for changes relevant to this form
		' ****************************************************************

		' Changes to the tblFund table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFund) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			Call SetFundCombo()

		End If

		' Changes to the tblInstrument table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrument) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			Call SetInstrumentCombo()

		End If

		' Changes to the tblTransactionType table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblCompoundTransactionTemplate) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			Call SetTransactionTypeCombo()

		End If

		' Changes to the tblTradeStatus table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblTradeStatus) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			Call SetTradeStatusCombo()

		End If

		' Changes to the tblCounterparty table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblCounterparty) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			Call SetCounterpartyCombo()

		End If

		' Changes to the tblPerson table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblPerson) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			Call SetPersonCombos()

		End If

		' Changes to the tblTransaction table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblTransaction) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			Call SetTransactionGroupCombo()

		End If

		' Changes to the KnowledgeDate :-
		If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
			SetButtonStatus_Flag = True
			RefreshForm = True
		End If

		' Changes to the tblUserPermissions table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

			' Check ongoing permissions.

			Call CheckPermissions()
			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

				FormIsValid = False
				Me.Close()
				Exit Sub
			End If

			SetButtonStatus_Flag = True

		End If


		' ****************************************************************
		' Changes to the Main FORM table :-
		' ****************************************************************

		If (e.TableChanged(THIS_FORM_ChangeID) = True) Or KnowledgeDateChanged Then

			' Re-Set Controls etc.
			Call SetSortedRows()

			RefreshForm = True

			' Move again to the correct item
			thisPosition = Get_Position(thisAuditID)

			' Validate current position.
			If thisPosition >= 0 And thisPosition < Me.SortedRows.GetLength(0) Then
				thisDataRow = Me.SortedRows(thisPosition)
			Else
				If (Me.SortedRows.GetLength(0) > 0) And (AddNewRecord = False) Then
					thisPosition = 0
					thisDataRow = Me.SortedRows(thisPosition)
					FormChanged = False
				Else
					thisDataRow = Nothing
				End If
			End If

			' Set SelectingCombo Index.
			If (Me.THIS_FORM_SelectingCombo.Items.Count <= 0) Then
				Try
					Me.THIS_FORM_SelectingCombo.SelectedIndex = -1
				Catch ex As Exception
				End Try
			ElseIf (thisPosition < 0) Then
				Try
					MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
				Catch ex As Exception
				End Try
			Else
				Try
					Me.THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
				Catch ex As Exception
				End Try
			End If

		End If

		' ****************************************************************
		' Repaint if not currently in Edit Mode
		'
		' ****************************************************************

		InPaint = OrgInPaint


		' Retrieve form data if appropriate, i.e. this data item is not already being edited.
		If (RefreshForm = True) And (FormChanged = False) And (Me.AddNewRecord = False) Then
			GetFormData(thisDataRow) ' Includes a call to 'SetButtonStatus()'
		Else
			If SetButtonStatus_Flag Then
        Call SetButtonStatus()
			End If
		End If

	End Sub

    ''' <summary>
    ''' Handles the Changed event of the Combo_TransactionType control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_TransactionType_Changed(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_TransactionType.SelectedIndexChanged
		' ******************************************************************************************
		' If the Transaction type changes, ensure the form labels are appropriate.
		' 
		' This principally applies to the transaction status check boxes.
		' ******************************************************************************************

		Try

			If (InPaint = False) Then
				Call Me.SetFormLabels()

				If (Combo_TransactionType.Enabled) AndAlso (Me.Combo_TransactionType.SelectedIndex >= 0) AndAlso (IsNumeric(Combo_TransactionType.SelectedValue)) Then
					Try
						Dim TransactionFlags As RenaissanceGlobals.CompoundTransactionFlags

						TransactionFlags = CType(LookupTableValue(MainForm, RenaissanceGlobals.RenaissanceStandardDatasets.tblCompoundTransactionTemplate, CInt(Me.Combo_TransactionType.SelectedValue), "TemplateFlags"), RenaissanceGlobals.TransactionTypes)

						' Transfer conditional formatting

						If (TransactionFlags And RenaissanceGlobals.CompoundTransactionFlags.UsesIsTransferFlag) Then

							Check_IsTransfer.Enabled = True

							If (Check_IsTransfer.Checked) Then

								DateTime_EffectiveValueDate.Enabled = True
								Edit_EffectivePrice.Enabled = True
								Edit_EffectiveWatermark.Enabled = True
								Check_SpecificInitialEqualisation.Enabled = True
								Combo_TransferFromCounterparty.Enabled = True
								If (Check_SpecificInitialEqualisation.Checked) Then
									Edit_SpecificInitialEqualisation.Enabled = True
								Else
									Edit_SpecificInitialEqualisation.Enabled = False
								End If

							Else

								DateTime_EffectiveValueDate.Enabled = False
								Edit_EffectivePrice.Enabled = False
								Edit_EffectiveWatermark.Enabled = False
								Check_SpecificInitialEqualisation.Enabled = False
								Edit_SpecificInitialEqualisation.Enabled = False
								Combo_TransferFromCounterparty.Enabled = False

							End If

						Else

							Check_IsTransfer.Enabled = False
							DateTime_EffectiveValueDate.Enabled = False
							Edit_EffectivePrice.Enabled = False
							Edit_EffectiveWatermark.Enabled = False
							Check_SpecificInitialEqualisation.Enabled = False
							Edit_SpecificInitialEqualisation.Enabled = False
							Combo_TransferFromCounterparty.Enabled = False

						End If

					Catch ex As Exception
					End Try
				End If
			End If

		Catch ex As Exception
		End Try
	End Sub

    ''' <summary>
    ''' Updates the units and settlement.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub UpdateUnitsAndSettlement(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_TransactionType.SelectedIndexChanged, edit_Amount.TextChanged, edit_Price.TextChanged, edit_Costs.TextChanged, edit_settlement.TextChanged
		' ******************************************************************************************
		' If the Selected Transaction Type, Units, Price, Costs or Settlement fields are changed,
		' then re-calculate the appropriate Units or Settlement fields.
		'
		' The 'InPaint' controls are used to prevent recursive updates caused by the update events
		' triggered on the Units and Settlement edit controls.
		' ******************************************************************************************

		Try
			If (InPaint = False) AndAlso (CType(sender, Control).Enabled = True) Then
				Try
					InPaint = True

					Dim Units As Double = 0
					Dim Price As Double = 0
					Dim Costs As Double = 0
          Dim BrokerCosts As Double = 0
          Dim Settlement As Double = 0
					Dim CashMovement As Double = 1
					Dim TransactionInstrument As Integer

					If (Me.edit_Amount.Focused = False) AndAlso (IsNumeric(Me.edit_Amount.Value)) Then
						Units = CDbl(Me.edit_Amount.Value)
					ElseIf IsNumeric(Me.edit_Amount.Text) Then
						Units = CDbl(Me.edit_Amount.Text)
					End If

					If (Me.edit_Price.Focused = False) AndAlso (IsNumeric(Me.edit_Price.Value)) Then
						Price = CDbl(Me.edit_Price.Value)
					ElseIf IsNumeric(Me.edit_Price.Text) Then
						Price = CDbl(Me.edit_Price.Text)
					End If

					If (Me.edit_Costs.Focused = False) AndAlso (IsNumeric(Me.edit_Costs.Value)) Then
						Costs = CDbl(Me.edit_Costs.Value)
					ElseIf IsNumeric(Me.edit_Costs.Text) Then
						Costs = CDbl(Me.edit_Costs.Text)
					End If

					If (Me.edit_settlement.Focused = False) AndAlso (IsNumeric(Me.edit_settlement.Value)) Then
						Settlement = CDbl(Me.edit_settlement.Value)
					ElseIf IsNumeric(Me.edit_settlement.Text) Then
						Settlement = CDbl(Me.edit_settlement.Text)
					End If

					If (Me.Combo_TransactionType.SelectedIndex >= 0) Then
						CashMovement = GetCashMove(Me.Name, Me.MainForm, Me.Combo_TransactionType.SelectedValue)
					End If

					If (Me.Combo_TransactionInstrument.SelectedIndex >= 0) AndAlso (IsNumeric(Combo_TransactionInstrument.SelectedValue)) Then
						TransactionInstrument = CInt(Combo_TransactionInstrument.SelectedValue)
					Else
						TransactionInstrument = 0
					End If

					If (Me.Combo_ValueOrAmount.Text.ToUpper.StartsWith("V")) Then
            Me.edit_Amount.Value = GetTransactionUnitsOrSettlement("Value", Units, Price, GetInstrumentContractSize(Me.Name, MainForm, TransactionInstrument), BrokerCosts, Costs, Settlement, CashMovement)
					Else
            Me.edit_settlement.Value = GetTransactionUnitsOrSettlement("Amount", Units, Price, GetInstrumentContractSize(Me.Name, MainForm, TransactionInstrument), BrokerCosts, Costs, Settlement, CashMovement)
					End If

				Catch ex As Exception
				Finally
					InPaint = False
				End Try

			End If
		Catch ex As Exception

		End Try

	End Sub

    ''' <summary>
    ''' Handles the Click event of the Button_GetPrice control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Button_GetPrice_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_GetPrice.Click
		' ****************************************************************
		' Function to retrieve an Instrument Price appropriate to the Given 
		' Instrument and Value Date.
		'
		' ****************************************************************
		Dim myConnection As SqlConnection
		Dim QueryCommand As New SqlCommand

		Dim InstrumentID As Integer
		Dim PriceDate As Date
		Dim InstrumentPrice As Object

		Try
			InstrumentID = 0
			PriceDate = Now().Date

			myConnection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)

			If (Me.Combo_TransactionInstrument.SelectedIndex > 0) Then
				InstrumentID = Me.Combo_TransactionInstrument.SelectedValue
			End If

			If IsDate(Me.Date_ValueDate.Value) Then
				PriceDate = Me.Date_ValueDate.Value
			End If

			QueryCommand.Connection = myConnection
			QueryCommand.CommandText = "SELECT dbo.fn_SingleInstrumentBestPrice(@InstrumentID, @ValueDate, 0, Null)"
			QueryCommand.Parameters.Clear()
			QueryCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@InstrumentID", System.Data.SqlDbType.Int, 4))
			QueryCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValueDate", System.Data.SqlDbType.DateTime, 8))
			QueryCommand.Parameters("@InstrumentID").Value = InstrumentID
			QueryCommand.Parameters("@ValueDate").Value = PriceDate

			SyncLock myConnection
				InstrumentPrice = QueryCommand.ExecuteScalar
			End SyncLock

			If IsNumeric(InstrumentPrice) Then
				Me.edit_Price.Value = CDbl(InstrumentPrice)
			End If
		Catch ex As Exception
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error getting an Instrument Price.", ex.StackTrace, True)
		End Try

	End Sub

    ''' <summary>
    ''' Handles the Click event of the Button_ViewTransactions control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Button_ViewTransactions_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_ViewTransactions.Click
		' ****************************************************************
		' Identify and display the Transaction Record associated
		' with the currently displayed Compound Transaction.
		' ****************************************************************

		Dim TransactionTable As RenaissanceDataClass.DSTransaction.tblTransactionDataTable
		Dim TransactionRows() As RenaissanceDataClass.DSTransaction.tblTransactionRow
		Dim TargetAuditID As Integer
		Dim thisFormHandle As RenaissanceGlobals.RenaissanceFormHandle

		' Firstly Resolve the AuditID of the CompoundTransaction To move to.
		' 
		Try
			TransactionTable = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblTransaction, False), RenaissanceDataClass.DSTransaction).tblTransaction
			TransactionRows = TransactionTable.Select("TransactionCompoundRN=" & thisDataRow.RN.ToString)

			If (TransactionRows.Length <= 0) Then Exit Sub
			TargetAuditID = TransactionRows(0)("AuditID")

			thisFormHandle = MainForm.New_VeniceForm(VeniceFormID.frmTransaction)
			CType(thisFormHandle.Form, frmTransaction).FormSelectCriteria = "TransactionCompoundRN=" & thisDataRow.RN.ToString
			CType(thisFormHandle.Form, frmTransaction).MoveToAuditID(TargetAuditID)

		Catch ex As Exception
			Call MainForm.LogError(Me.Name & ", ViewTransactions_Click", LOG_LEVELS.Error, ex.Message, "Error Showing Transaction Form", ex.StackTrace, True)
		End Try

	End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the Check_CostsAsPercent control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Check_CostsAsPercent_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_CostsAsPercent.CheckedChanged
		' ****************************************************************
		'
		' ****************************************************************

		If Me.Check_CostsAsPercent.Checked Then
			Me.edit_Costs.Visible = False
			Me.edit_CostPercentage.Visible = True
		Else
			Me.edit_Costs.Visible = True
			Me.edit_CostPercentage.Visible = False
		End If
	End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the Check_IsTransfer control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Check_IsTransfer_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_IsTransfer.CheckedChanged
		' **********************************************************************
		'
		'
		' **********************************************************************

		Try
			If (Me.Created) AndAlso (Not Me.IsDisposed) AndAlso (Not Me.InPaint) Then
				If (Check_IsTransfer.Enabled) Then
					If (Check_IsTransfer.Checked) Then

						DateTime_EffectiveValueDate.Enabled = True
						Edit_EffectivePrice.Enabled = True
						Edit_EffectiveWatermark.Enabled = True
						Check_SpecificInitialEqualisation.Enabled = True
						Combo_TransferFromCounterparty.Enabled = True
						If (Check_SpecificInitialEqualisation.Checked) Then
							Edit_SpecificInitialEqualisation.Enabled = True
						Else
							Edit_SpecificInitialEqualisation.Enabled = False
						End If

					Else
						DateTime_EffectiveValueDate.Enabled = False
						Edit_EffectivePrice.Enabled = False
						Edit_EffectiveWatermark.Enabled = False
						Check_SpecificInitialEqualisation.Enabled = False
						Edit_SpecificInitialEqualisation.Enabled = False
						Combo_TransferFromCounterparty.Enabled = False
					End If
				End If
			End If

		Catch ex As Exception
		End Try

	End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the Check_SpecificInitialEqualisation control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Check_SpecificInitialEqualisation_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_SpecificInitialEqualisation.CheckedChanged

		' **********************************************************************
		'
		'
		' **********************************************************************

		Try
			If (Me.Created) AndAlso (Not Me.IsDisposed) AndAlso (Not Me.InPaint) Then
				If (Check_SpecificInitialEqualisation.Enabled) Then
					If (Check_SpecificInitialEqualisation.Checked) Then
						Edit_SpecificInitialEqualisation.Enabled = True
					Else
						Edit_SpecificInitialEqualisation.Enabled = False
					End If
				End If
			End If

		Catch ex As Exception
		End Try

	End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

	' Build Sorted list from the Source dataset and update the Select Combo
    ''' <summary>
    ''' Sets the sorted rows.
    ''' </summary>
	Private Sub SetSortedRows()

		Dim OrgInPaint As Boolean

		Dim thisrow As DataRow
		Dim thisDrowDownWidth As Integer
		Dim SizingBitmap As Bitmap
		Dim SizingGraphics As Graphics

		' Form Specific Selection Combo :-
		If (THIS_FORM_SelectingCombo Is Nothing) Then Exit Sub

		' Code is pretty Generic form here on...

		' Set paint local so that changes to the selection combo do not trigger form updates.

		OrgInPaint = InPaint
		InPaint = True

		' Get selected Row sets, or exit if no data is present.
		If myDataset Is Nothing Then
			ReDim SortedRows(0)
			ReDim SelectBySortedRows(0)
		Else
			Try
				SortedRows = myTable.Select(THIS_FORM_SelectCriteria, THIS_FORM_OrderBy)
				SelectBySortedRows = myTable.Select(THIS_FORM_SelectCriteria, THIS_FORM_SelectBy)
			Catch ex As Exception
				Me.MainForm.LogError(Me.Name & ",SetSortedRows()", LOG_LEVELS.Error, ex.Message, "Error Selecting Data Rows.", ex.StackTrace, True)

				ReDim SortedRows(0)
				ReDim SelectBySortedRows(0)
			End Try
		End If

		' Set Combo data source
		THIS_FORM_SelectingCombo.DataSource = SortedRows
		THIS_FORM_SelectingCombo.DisplayMember = THIS_FORM_SelectBy
		THIS_FORM_SelectingCombo.ValueMember = THIS_FORM_ValueMember

		' Ensure that the Selection Combo's DrowDownWidth is wide enough to take the required strings

		thisDrowDownWidth = THIS_FORM_SelectingCombo.Width
		For Each thisrow In SortedRows

			' Compute the string dimensions in the given font
			SizingBitmap = New Bitmap(1, 1, System.Drawing.Imaging.PixelFormat.Format32bppArgb)
			SizingGraphics = Graphics.FromImage(SizingBitmap)
			Dim stringSize As SizeF = SizingGraphics.MeasureString(thisrow(THIS_FORM_SelectBy).ToString, THIS_FORM_SelectingCombo.Font)
			If (stringSize.Width) > thisDrowDownWidth Then
				thisDrowDownWidth = CInt(stringSize.Width)
			End If
		Next

		THIS_FORM_SelectingCombo.DropDownWidth = thisDrowDownWidth

		InPaint = OrgInPaint
	End Sub


	' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
	Private Sub CheckPermissions()

		Dim Permissions As Integer

		Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

		HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
		HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
		HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
		HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

	End Sub

	' Flag changes to form controls, Event Associations made in 'New' routine.
    ''' <summary>
  ''' Basic event handler, called when a form value is changed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

		If InPaint = False Then
			If (Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord) Then
				FormChanged = True
				Me.btnSave.Enabled = True
				Me.btnCancel.Enabled = True
			End If
		End If

	End Sub

    ''' <summary>
    ''' Selects the menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Public Sub SelectMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Select Menu
		' Event association is made during the dynamic menu creation

		THIS_FORM_SelectBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

		Call SetSortedRows()

	End Sub

    ''' <summary>
    ''' Orders the menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Public Sub OrderMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Order Menu
		' Event association is made during the dynamic menu creation

		THIS_FORM_OrderBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

		Call SetSortedRows()

	End Sub


    ''' <summary>
    ''' Audits the report menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Public Sub AuditReportMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Audit Report Menu
		' Event association is made during the dynamic menu creation

		Try
			Select Case CInt(CType(sender, System.Windows.Forms.ToolStripMenuItem).Tag)
				Case 0 ' This Record
					If (Me.AddNewRecord = False) AndAlso (thisAuditID >= 0) Then
						Run_AuditReport_ThisRecord(MainForm, ThisStandardDataset, thisAuditID, MainForm.VeniceProgressBar)
					End If

				Case 1 ' All Records
					Run_AuditReport_ALLRecords(MainForm, ThisStandardDataset, MainForm.VeniceProgressBar)

			End Select
		Catch ex As Exception
			Call MainForm.LogError(Me.Name & ", AuditReportMenuEvent()", 0, ex.Message, "Error calling Audit report.", ex.StackTrace, True)
		End Try

	End Sub




#End Region

#Region " Set Form Combos (Form Specific Code) "

    ''' <summary>
    ''' Sets the fund combo.
    ''' </summary>
	Private Sub SetFundCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_TransactionFund, _
    RenaissanceStandardDatasets.tblFund, _
    "FundName", _
    "FundID", _
    "FundClosed=0")   ' 

	End Sub

    ''' <summary>
    ''' Sets the instrument combo.
    ''' </summary>
	Private Sub SetInstrumentCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_TransactionInstrument, _
		RenaissanceStandardDatasets.tblInstrument, _
		"InstrumentDescription", _
		"InstrumentID", _
		"")		' 

	End Sub

    ''' <summary>
    ''' Sets the transaction type combo.
    ''' </summary>
	Private Sub SetTransactionTypeCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_TransactionType, _
		RenaissanceStandardDatasets.tblCompoundTransactionTemplate, _
		"TemplateDescription", _
		"TemplateID", _
		"", True)		 ' 

	End Sub

    ''' <summary>
    ''' Sets the trade status combo.
    ''' </summary>
	Private Sub SetTradeStatusCombo()
		' *******************************************************************************
		'
		' *******************************************************************************

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_TradeStatus, _
		RenaissanceStandardDatasets.tblTradeStatus, _
		"tradeStatusName", _
		"tradeStatusID", _
		"")		' 

	End Sub

    ''' <summary>
    ''' Sets the counterparty combo.
    ''' </summary>
	Private Sub SetCounterpartyCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_TransactionCounterparty, _
		RenaissanceStandardDatasets.tblCounterparty, _
		"CounterpartyName", _
		"CounterpartyID", _
		"", True)		 ' 

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_TransferFromCounterparty, _
		RenaissanceStandardDatasets.tblCounterparty, _
		"CounterpartyName", _
		"CounterpartyID", _
		"", True, True, True, 0, "")	 ' 

	End Sub

    ''' <summary>
    ''' Sets the transaction group combo.
    ''' </summary>
	Private Sub SetTransactionGroupCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_TransactionGroup, _
		RenaissanceStandardDatasets.tblTransaction, _
		"TransactionGroup", _
		"TransactionGroup", _
		"", _
		True)		' 

	End Sub

    ''' <summary>
    ''' Sets the person combos.
    ''' </summary>
	Private Sub SetPersonCombos()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_InvestmentManager, _
		RenaissanceStandardDatasets.tblPerson, _
		"Person", _
		"Person", _
		"")		' 

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_TransactionManager, _
		RenaissanceStandardDatasets.tblPerson, _
		"Person", _
		"Person", _
		"")		' 

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_DataEntryManager, _
		RenaissanceStandardDatasets.tblPerson, _
		"Person", _
		"Person", _
		"")		' 

	End Sub

#End Region

#Region " Get & Set Form Data / SetButton / ValidateForm / btnCancel Events (Form Specific Code) "

    ''' <summary>
    ''' Gets the form data.
    ''' </summary>
    ''' <param name="ThisRow">The this row.</param>
	Private Sub GetFormData(ByRef ThisRow As RenaissanceDataClass.DSCompoundTransaction.tblCompoundTransactionRow)
		' *******************************************************************************
		' Routine to populate form Controls given a data row.
		' Form is cleared for invalid Datarows or Invalid Form.
		' *******************************************************************************

		Dim OrgInpaint As Boolean

		' Set 'Paint' flag to prevent event actions caused by changing the value of form fields.
		OrgInpaint = InPaint
		InPaint = True

		Tab_TransactionDetails.SelectedIndex = 0

		If (ThisRow Is Nothing) Or (FormIsValid = False) Or (Me.InUse = False) Then
			' Bad / New Datarow - Clear Form.

			thisAuditID = (-1)

			Me.editAuditID.Text = ""

			MainForm.ClearComboSelection(Combo_TransactionFund)
			MainForm.ClearComboSelection(Combo_TransactionInstrument)
			MainForm.ClearComboSelection(Combo_TransactionType)

			If (Combo_TradeStatus.Items.Count > 0) Then
				Combo_TradeStatus.SelectedValue = 0
			End If

			If (Me.Combo_TransactionCounterparty.Items.Count > 0) Then
				MainForm.ClearComboSelection(Combo_TransactionCounterparty)
				Me.Combo_TransactionCounterparty.SelectedValue = RenaissanceGlobals.Counterparty.MARKET
			End If

			If (Combo_TransferFromCounterparty.Items.Count > 0) Then
				Combo_TransferFromCounterparty.SelectedIndex = 0
			End If

			MainForm.ClearComboSelection(Combo_TransactionGroup)
			MainForm.ClearComboSelection(Combo_InvestmentManager)
			MainForm.ClearComboSelection(Combo_TransactionManager)
			MainForm.ClearComboSelection(Combo_DataEntryManager)

			Me.Date_DecisionDate.Text = ""
			Me.Date_ValueDate.Text = ""
			Me.Date_SettlementDate.Text = ""
			Me.Date_ConfirmationDate.Text = ""
			Me.Combo_ValueOrAmount.SelectedIndex = 0
			Me.edit_Amount.Value = 0
			Me.edit_Price.Value = 0
			Me.edit_Costs.Value = 0
			Me.edit_CostPercentage.Value = 0
			Me.edit_settlement.Value = 0
			Me.Check_CostsAsPercent.CheckState = CheckState.Unchecked
			Me.Check_DontUpdatePrice.CheckState = CheckState.Unchecked
			Me.Check_FinalAmount.CheckState = CheckState.Unchecked
			Me.Check_FinalPrice.CheckState = CheckState.Unchecked
			Me.Check_FinalCosts.CheckState = CheckState.Unchecked
			Me.Check_FinalSettlement.CheckState = CheckState.Unchecked
			Me.Check_InstructionGiven.CheckState = CheckState.Unchecked
			Me.Check_BankConfirmation.CheckState = CheckState.Unchecked
			Me.Check_InitialDataEntry.CheckState = CheckState.Unchecked
			Me.Check_PriceConfirmation.CheckState = CheckState.Unchecked
			Me.Check_SettlementConfirmed.CheckState = CheckState.Unchecked
			Me.Check_FinalDataEntry.CheckState = CheckState.Unchecked

			Me.Check_IsTransfer.Checked = False
			Me.Edit_EffectivePrice.Value = 0
			Me.DateTime_EffectiveValueDate.Text = ""
			Me.Check_SpecificInitialEqualisation.Checked = False
			Me.Edit_SpecificInitialEqualisation.Value = 0
			Me.Edit_EffectiveWatermark.Value = 0

			Me.editTransactionComment.Text = ""

			Me.Button_ViewTransactions.Enabled = True

			If AddNewRecord = True Then
				Me.btnCancel.Enabled = True
				Me.THIS_FORM_SelectingCombo.Enabled = False
			Else
				Me.btnCancel.Enabled = False
				Me.THIS_FORM_SelectingCombo.Enabled = True
			End If

		Else

			' Populate Form with given data.
			Try
				thisAuditID = thisDataRow.AuditID

				Me.editAuditID.Text = thisDataRow.AuditID.ToString

				Me.Combo_TransactionFund.SelectedValue = thisDataRow.TransactionFund
				Me.Combo_TransactionInstrument.SelectedValue = thisDataRow.TransactionInstrument
				Me.Combo_TransactionType.SelectedValue = thisDataRow.TransactionTemplateID
				Me.Combo_TradeStatus.SelectedValue = thisDataRow.TransactionTradeStatusID
				Me.Combo_TransactionCounterparty.SelectedValue = thisDataRow.TransactionCounterparty
				Me.Combo_TransactionGroup.Text = thisDataRow.TransactionGroup
				Me.Combo_InvestmentManager.Text = thisDataRow.TransactionInvestment
				Me.Combo_TransactionManager.Text = thisDataRow.TransactionExecution
				Me.Combo_DataEntryManager.Text = thisDataRow.TransactionDataEntry
				Me.Date_DecisionDate.Value = thisDataRow.TransactionDecisionDate
				Me.Date_ValueDate.Value = thisDataRow.TransactionValueDate
				Me.Date_SettlementDate.Value = thisDataRow.TransactionSettlementDate
				Me.Date_ConfirmationDate.Value = thisDataRow.TransactionConfirmationDate

				Me.Check_IsTransfer.Checked = thisDataRow.TransactionIsTransfer
				If (thisDataRow.TransactionIsTransfer) Then
					Me.Edit_EffectivePrice.Value = thisDataRow.TransactionEffectivePrice
					Me.DateTime_EffectiveValueDate.Value = thisDataRow.TransactionEffectiveValueDate
					Me.Edit_EffectiveWatermark.Value = thisDataRow.TransactionEffectiveWatermark
					Me.Check_SpecificInitialEqualisation.Checked = thisDataRow.TransactionSpecificInitialEqualisationFlag

					If (thisDataRow.TransactionSpecificInitialEqualisationFlag) Then
						Me.Edit_SpecificInitialEqualisation.Value = thisDataRow.TransactionSpecificInitialEqualisationValue
					Else
						Me.Edit_SpecificInitialEqualisation.Value = 0
					End If

					If (Combo_TransferFromCounterparty.Items.Count > 0) Then
						If (thisDataRow.Parameter1_Type = SqlDbType.Int) AndAlso (thisDataRow.Parameter1_Name = "TransferFromCounterparty") AndAlso IsNumeric(thisDataRow.Parameter1_Value) Then
							Combo_TransferFromCounterparty.SelectedValue = CInt(thisDataRow.Parameter1_Value)
						Else
							Combo_TransferFromCounterparty.SelectedIndex = 0
						End If
					End If

				Else
					Me.Edit_EffectivePrice.Value = thisDataRow.TransactionPrice
					Me.DateTime_EffectiveValueDate.Value = thisDataRow.TransactionValueDate
					Me.Check_SpecificInitialEqualisation.Checked = False
					Me.Edit_SpecificInitialEqualisation.Value = 0
					Me.Edit_EffectiveWatermark.Value = 0

					If (Combo_TransferFromCounterparty.Items.Count > 0) Then
						Combo_TransferFromCounterparty.SelectedIndex = 0
					End If

				End If

				If thisDataRow.TransactionValueorAmount.ToUpper = "VALUE" Then
					Me.Combo_ValueOrAmount.SelectedIndex = 0
				Else
					Me.Combo_ValueOrAmount.SelectedIndex = 1
				End If

				'Me.Combo_ValueOrAmount.Text = thisDataRow.TransactionValueorAmount
				Me.edit_Amount.Value = thisDataRow.TransactionUnits
				Me.edit_Price.Value = thisDataRow.TransactionPrice
				Me.edit_Costs.Value = thisDataRow.TransactionCosts
				Me.edit_CostPercentage.Value = thisDataRow.TransactionCostPercent

				Me.Check_CostsAsPercent.Checked = thisDataRow.TransactionCostIsPercent
				Me.Check_DontUpdatePrice.Checked = thisDataRow.TransactionExemptFromUpdate

				Try
          Me.edit_settlement.Value = Math.Round(GetTransactionUnitsOrSettlement("Amount", thisDataRow.TransactionUnits, thisDataRow.TransactionPrice, GetInstrumentContractSize(Me.Name, MainForm, thisDataRow.TransactionInstrument), thisDataRow.TransactionCosts, thisDataRow.TransactionBrokerFees, 0, GetCashMove(Me.Name, Me.MainForm, thisDataRow.TransactionType)), 6)
				Catch ex As Exception
					Me.edit_settlement.Value = 0
				End Try

				If thisDataRow.TransactionFinalAmount = 0 Then
					Me.Check_FinalAmount.CheckState = CheckState.Unchecked
				Else
					Me.Check_FinalAmount.CheckState = CheckState.Checked
				End If

				If thisDataRow.TransactionFinalPrice = 0 Then
					Me.Check_FinalPrice.CheckState = CheckState.Unchecked
				Else
					Me.Check_FinalPrice.CheckState = CheckState.Checked
				End If

				If thisDataRow.TransactionFinalCosts = 0 Then
					Me.Check_FinalCosts.CheckState = CheckState.Unchecked
				Else
					Me.Check_FinalCosts.CheckState = CheckState.Checked
				End If

				If thisDataRow.TransactionFinalSettlement = 0 Then
					Me.Check_FinalSettlement.CheckState = CheckState.Unchecked
				Else
					Me.Check_FinalSettlement.CheckState = CheckState.Checked
				End If

				If thisDataRow.TransactionInstructionFlag = 0 Then
					Me.Check_InstructionGiven.CheckState = CheckState.Unchecked
				Else
					Me.Check_InstructionGiven.CheckState = CheckState.Checked
				End If

				If thisDataRow.TransactionBankConfirmation = 0 Then
					Me.Check_BankConfirmation.CheckState = CheckState.Unchecked
				Else
					Me.Check_BankConfirmation.CheckState = CheckState.Checked
				End If

				If thisDataRow.TransactionInitialDataEntry = 0 Then
					Me.Check_InitialDataEntry.CheckState = CheckState.Unchecked
				Else
					Me.Check_InitialDataEntry.CheckState = CheckState.Checked
				End If

				If thisDataRow.TransactionAmountConfirmed = 0 Then
					Me.Check_PriceConfirmation.CheckState = CheckState.Unchecked
				Else
					Me.Check_PriceConfirmation.CheckState = CheckState.Checked
				End If

				If thisDataRow.TransactionSettlementConfirmed = 0 Then
					Me.Check_SettlementConfirmed.CheckState = CheckState.Unchecked
				Else
					Me.Check_SettlementConfirmed.CheckState = CheckState.Checked
				End If

				If thisDataRow.TransactionFinalDataEntry = 0 Then
					Me.Check_FinalDataEntry.CheckState = CheckState.Unchecked
				Else
					Me.Check_FinalDataEntry.CheckState = CheckState.Checked
				End If

				Me.editTransactionComment.Text = thisDataRow.TransactionComment

				AddNewRecord = False
				' MainForm.SetComboSelectionLengths(Me)

				Me.btnCancel.Enabled = False
				Me.THIS_FORM_SelectingCombo.Enabled = True

			Catch ex As Exception

				Call MainForm.LogError(Me.Name & ", GetFormData", 0, ex.Message, "Error Showing Data", ex.StackTrace, True)
				Call GetFormData(Nothing)

			End Try

		End If

		Call SetFormLabels()


    Me.btnSave.Enabled = False

    ' As it says on the can.... (Perform before DoEvents() incase ThisRow becomes invalid during DoEvents() i.e. Background updating)
    Call SetButtonStatus()

    ' Allow Field events to trigger before 'InPaint' Is re-set. 
    ' (Should) Prevent Validation errors during Form Draw.
    Application.DoEvents()

    FormChanged = False

    ' Restore 'Paint' flag.
    InPaint = OrgInpaint

	End Sub

    ''' <summary>
    ''' Sets the form labels.
    ''' </summary>
	Private Sub SetFormLabels()
		' *******************************************************************************
		' Set the Transaction form labels appropriate to the current transaction type.
		'
		' *******************************************************************************

		Dim FormLabelsDS As RenaissanceDataClass.DSDataDictionary_TransactionFormLabels.tblDataDictionary_TransactionFormLabelsDataTable
		Dim FormLabelRows() As RenaissanceDataClass.DSDataDictionary_TransactionFormLabels.tblDataDictionary_TransactionFormLabelsRow
		Dim FormLabelRow As RenaissanceDataClass.DSDataDictionary_TransactionFormLabels.tblDataDictionary_TransactionFormLabelsRow
		Dim FormLabelTransactionType As Integer

		Try
			FormLabelTransactionType = 0
			If (Me.Combo_TransactionType.SelectedIndex >= 0) Then
				Try
					FormLabelTransactionType = CType(Me.Combo_TransactionType.SelectedValue, Integer)
				Catch ex As Exception
				End Try
			End If

			FormLabelsDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblDataDictionary_TransactionFormLabels, False).Tables(0)
			FormLabelRows = FormLabelsDS.Select("((TransactionType=0) OR (TransactionType=" & FormLabelTransactionType.ToString & ")) AND (CompoundFlag>0)", "TransactionType DESC")
			If (FormLabelRows.Length) > 0 Then
				FormLabelRow = FormLabelRows(0)
			Else
				FormLabelRow = Nothing
			End If
		Catch ex As Exception
			FormLabelRow = Nothing
		End Try

		If (FormLabelRow Is Nothing) Then
			Me.Label_Counterparty.Text = "Counterparty"

			Me.Label_Price.Text = "Price"
			Me.Label_DecisionDate.Text = "Decision Date"
			Me.Label_TradeDate.Text = "Trade Date"
			Me.Label_SettlementDate.Text = "Settlement Date"
			Me.Label_ConfirmationDate.Text = "Conirmation Date"

			Me.Check_InstructionGiven.Text = "Instruction Given"
			Me.Check_BankConfirmation.Text = "Bank Confirmation"
			Me.Check_InitialDataEntry.Text = "Initial Data Entry"
			Me.Check_PriceConfirmation.Text = "Price Confirmation"
			Me.Check_SettlementConfirmed.Text = "Settlement Confirmed"
			Me.Check_FinalDataEntry.Text = "Final Data Entry"
		Else
			Me.Label_Counterparty.Text = FormLabelRow.CounterpartyText
			Me.Label_Price.Text = FormLabelRow.TransactionPrice

			Me.Label_DecisionDate.Text = FormLabelRow.DecisionDateText
			Me.Label_TradeDate.Text = FormLabelRow.TradeDateText
			Me.Label_SettlementDate.Text = FormLabelRow.SettlementDateText
			Me.Label_ConfirmationDate.Text = FormLabelRow.ConfirmationDateText

			Me.Check_InstructionGiven.Text = FormLabelRow.FieldText1
			Me.Check_BankConfirmation.Text = FormLabelRow.FieldText2
			Me.Check_InitialDataEntry.Text = FormLabelRow.FieldText3
			Me.Check_PriceConfirmation.Text = FormLabelRow.FieldText4
			Me.Check_SettlementConfirmed.Text = FormLabelRow.FieldText5
			Me.Check_FinalDataEntry.Text = FormLabelRow.FieldText6

		End If

	End Sub

    ''' <summary>
    ''' Sets the form data.
    ''' </summary>
    ''' <param name="pConfirm">if set to <c>true</c> [p confirm].</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	Private Function SetFormData(Optional ByVal pConfirm As Boolean = True) As Boolean
		' *************************************************************
		' Routine to Save changes to the current (possibly New) transaction.
		'
		' *************************************************************
		Dim ErrMessage As String
		Dim ErrFlag As Boolean
		Dim ErrStack As String

		Dim PreviousCTRN As Integer
		Dim CT_TemplateFlags As Integer

		Dim CT_Handler As New CompoundTransactionHandler

		ErrMessage = ""
		ErrStack = ""
		PreviousCTRN = 0
		CT_TemplateFlags = 0

		' *************************************************************
		' Appropriate Save permission :-
		' *************************************************************

		If (Me.HasUpdatePermission = False) And ((Me.HasInsertPermission = False) Or (Me.AddNewRecord = False)) Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "You do not have permission to save this record.", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Return False
			Exit Function
		End If

		' *************************************************************
		' If Save button is disabled then should not be able to save, exit silently.
		' *************************************************************
		If Me.btnSave.Enabled = False Then
			Call btnCancel_Click(Me, New System.EventArgs)
			Return False
			Exit Function
		End If

		' *************************************************************
		' KnowledgeDate OK :-
		' *************************************************************

		If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Changes to Venice are not allowed when the Knowledgedate is not set to `NOW`", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Return False
			Exit Function
		End If

		' Confirm Save, if required.

		If (pConfirm = True) Then
			If MessageBox.Show("Save Changes ?", "Save Changes ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
				Return True
				Exit Function
			End If
		End If

		' *************************************************************
		' Procedure to Save the current form information.
		' *************************************************************

		Dim StatusString As String
		Dim LogString As String
		Dim UpdateRows(0) As DataRow
		Dim Position As Integer

		If (FormChanged = False) Or (FormIsValid = False) Then
			Return False
			Exit Function
		End If

		' Validation
		StatusString = ""
		If ValidateForm(StatusString) = False Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Validation Error." & vbCrLf & StatusString, "", True)
			Return False
			Exit Function
		End If

		' Check current position in the table.
		Position = Get_Position(thisAuditID)

		' Allow for new or missing ID.
		If Position < 0 Then
			If (AddNewRecord = True) OrElse (SortedRows.Length <= 0) Then
				thisDataRow = myTable.NewRow
				LogString = "Add: "
				AddNewRecord = True
			Else
				Return False
				Exit Function
			End If
		Else
			' Row found OK.
			LogString = "Edit : AuditID = " & thisAuditID.ToString

			thisDataRow = Me.SortedRows(Position)

			If Not (thisDataRow Is Nothing) Then
				PreviousCTRN = thisDataRow.RN
			End If
		End If

		' Check if Instrument has changed on an existing transaction
		If (AddNewRecord = False) Then

			If (thisDataRow.TransactionInstrument <> CInt(Combo_TransactionInstrument.SelectedValue)) Then
				' Confirm
				Dim InitialName As String
				Dim NewName As String

				InitialName = LookupTableValue(MainForm, RenaissanceGlobals.RenaissanceStandardDatasets.tblInstrument, thisDataRow.TransactionInstrument, "InstrumentDescription")
				NewName = LookupTableValue(MainForm, RenaissanceGlobals.RenaissanceStandardDatasets.tblInstrument, CInt(Combo_TransactionInstrument.SelectedValue), "InstrumentDescription")

				If MessageBox.Show("You are changing the Instrument on an existing transaction from" & vbCrLf & InitialName & " to " & vbCrLf & NewName & vbCrLf & "Do you wish to continue ?", "Transaction Query", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) <> Windows.Forms.DialogResult.OK Then
					Return False
					Exit Function
				End If

			End If

			If (thisDataRow.TransactionTemplateID <> CInt(Combo_TransactionType.SelectedValue)) Then
				' Confirm
				Dim InitialName As String
				Dim NewName As String

				InitialName = LookupTableValue(MainForm, RenaissanceGlobals.RenaissanceStandardDatasets.tblCompoundTransactionTemplate, thisDataRow.TransactionTemplateID, "TemplateDescription")
				NewName = LookupTableValue(MainForm, RenaissanceGlobals.RenaissanceStandardDatasets.tblCompoundTransactionTemplate, CInt(Combo_TransactionType.SelectedValue), "TemplateDescription")

				If MessageBox.Show("You are changing the Transaction Type on an existing transaction from" & vbCrLf & InitialName & " to " & vbCrLf & NewName & vbCrLf & "Do you wish to continue ?", "Transaction Query", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) <> Windows.Forms.DialogResult.OK Then
					Return False
					Exit Function
				End If

			End If
		End If


		' Get Template Flags
		Dim CompoundTemplateTable As RenaissanceDataClass.DSCompoundTransactionTemplate.tblCompoundTransactionTemplateDataTable
		Dim CompoundTemplateRows() As RenaissanceDataClass.DSCompoundTransactionTemplate.tblCompoundTransactionTemplateRow
		CompoundTemplateTable = CType(Me.MainForm.Load_Table(RenaissanceStandardDatasets.tblCompoundTransactionTemplate, False), RenaissanceDataClass.DSCompoundTransactionTemplate).tblCompoundTransactionTemplate
		CompoundTemplateRows = CompoundTemplateTable.Select("TemplateID=" & CInt(Me.Combo_TransactionType.SelectedValue).ToString, "TemplateFlags DESC")
		If (Not (CompoundTemplateRows Is Nothing)) AndAlso (CompoundTemplateRows.Length > 0) Then
			CT_TemplateFlags = CompoundTemplateRows(0).TemplateFlags
		End If

		' Set 'Paint' flag.
		InPaint = True

		' *************************************************************
		' Lock the Data Table, to prevent update conflicts.
		' *************************************************************
		SyncLock myTable

			' Initiate Edit,
			thisDataRow.BeginEdit()

			' Set Data Values

			thisDataRow.AdminStatus = AdminStatus.General_CompoundTransaction
			thisDataRow.TransactionParentID = 0
			thisDataRow.TransactionTemplateID = Me.Combo_TransactionType.SelectedValue
			thisDataRow.TransactionFund = Me.Combo_TransactionFund.SelectedValue
			thisDataRow.TransactionInstrument = Me.Combo_TransactionInstrument.SelectedValue
			thisDataRow.TransactionType = 0
			thisDataRow.TransactionTradeStatusID = Me.Combo_TradeStatus.SelectedValue
			thisDataRow.TransactionCounterparty = Me.Combo_TransactionCounterparty.SelectedValue
			thisDataRow.TransactionGroup = Me.Combo_TransactionGroup.Text
			thisDataRow.TransactionInvestment = Me.Combo_InvestmentManager.Text
			thisDataRow.TransactionExecution = Me.Combo_TransactionManager.Text
			thisDataRow.TransactionDataEntry = Combo_DataEntryManager.Text
			thisDataRow.TransactionDecisionDate = Me.Date_DecisionDate.Value.Date
			thisDataRow.TransactionValueDate = Me.Date_ValueDate.Value.Date
			thisDataRow.TransactionFIFOValueDate = Me.Date_ValueDate.Value.Date
			thisDataRow.TransactionSettlementDate = Me.Date_SettlementDate.Value.Date
			thisDataRow.TransactionConfirmationDate = Me.Date_ConfirmationDate.Value.Date
			thisDataRow.TransactionValueorAmount = Me.Combo_ValueOrAmount.Text
			thisDataRow.TransactionUnits = Me.edit_Amount.Value
			thisDataRow.TransactionPrice = Me.edit_Price.Value
			thisDataRow.TransactionCosts = Me.edit_Costs.Value
			thisDataRow.TransactionCostPercent = Me.edit_CostPercentage.Value

			' Transfer Details.

			If (Check_IsTransfer.Enabled) AndAlso (Check_IsTransfer.Checked) Then
				thisDataRow.TransactionIsTransfer = True

				thisDataRow.TransactionEffectivePrice = Edit_EffectivePrice.Value
				thisDataRow.TransactionEffectiveValueDate = DateTime_EffectiveValueDate.Value

				If (Check_SpecificInitialEqualisation.Checked) Then
					thisDataRow.TransactionSpecificInitialEqualisationFlag = True
					thisDataRow.TransactionSpecificInitialEqualisationValue = Edit_SpecificInitialEqualisation.Value
				Else
					thisDataRow.TransactionSpecificInitialEqualisationFlag = False
					thisDataRow.TransactionSpecificInitialEqualisationValue = 0
				End If
				thisDataRow.TransactionEffectiveWatermark = Edit_EffectiveWatermark.Value

			Else
				thisDataRow.TransactionIsTransfer = False
				thisDataRow.TransactionEffectivePrice = thisDataRow.TransactionPrice
				thisDataRow.TransactionEffectiveValueDate = thisDataRow.TransactionValueDate
				thisDataRow.TransactionSpecificInitialEqualisationFlag = False
				thisDataRow.TransactionSpecificInitialEqualisationValue = 0
				thisDataRow.TransactionEffectiveWatermark = 0
			End If

			If Me.Check_CostsAsPercent.CheckState = CheckState.Unchecked Then
				thisDataRow.TransactionCostIsPercent = False
			Else
				thisDataRow.TransactionCostIsPercent = True
			End If

			If Me.Check_DontUpdatePrice.CheckState = CheckState.Unchecked Then
				thisDataRow.TransactionExemptFromUpdate = False
			Else
				thisDataRow.TransactionExemptFromUpdate = True
			End If

			If Me.Check_FinalAmount.CheckState = CheckState.Unchecked Then
				thisDataRow.TransactionFinalAmount = 0
			Else
				thisDataRow.TransactionFinalAmount = (-1)
			End If

			If Me.Check_FinalPrice.CheckState = CheckState.Unchecked Then
				thisDataRow.TransactionFinalPrice = 0
			Else
				thisDataRow.TransactionFinalPrice = (-1)
			End If

			If Me.Check_FinalCosts.CheckState = CheckState.Unchecked Then
				thisDataRow.TransactionFinalCosts = 0
			Else
				thisDataRow.TransactionFinalCosts = (-1)
			End If

			If Me.Check_FinalSettlement.CheckState = CheckState.Unchecked Then
				thisDataRow.TransactionFinalSettlement = 0
			Else
				thisDataRow.TransactionFinalSettlement = (-1)
			End If

			If Me.Check_InstructionGiven.CheckState = CheckState.Unchecked Then
				thisDataRow.TransactionInstructionFlag = 0
			Else
				thisDataRow.TransactionInstructionFlag = (-1)
			End If

			If Me.Check_BankConfirmation.CheckState = CheckState.Unchecked Then
				thisDataRow.TransactionBankConfirmation = 0
			Else
				thisDataRow.TransactionBankConfirmation = (-1)
			End If

			If Me.Check_InitialDataEntry.CheckState = CheckState.Unchecked Then
				thisDataRow.TransactionInitialDataEntry = 0
			Else
				thisDataRow.TransactionInitialDataEntry = (-1)
			End If

			If Me.Check_PriceConfirmation.CheckState = CheckState.Unchecked Then
				thisDataRow.TransactionAmountConfirmed = 0
			Else
				thisDataRow.TransactionAmountConfirmed = (-1)
			End If

			If Me.Check_SettlementConfirmed.CheckState = CheckState.Unchecked Then
				thisDataRow.TransactionSettlementConfirmed = 0
			Else
				thisDataRow.TransactionSettlementConfirmed = (-1)
			End If

			If Me.Check_FinalDataEntry.CheckState = CheckState.Unchecked Then
				thisDataRow.TransactionFinalDataEntry = 0
			Else
				thisDataRow.TransactionFinalDataEntry = (-1)
			End If

			thisDataRow.TransactionComment = Me.editTransactionComment.Text

			thisDataRow.ParameterCount = 0
			thisDataRow.Parameter1_Name = ""
			thisDataRow.Parameter1_Type = 0
			thisDataRow.Parameter1_Value = 0
			thisDataRow.Parameter2_Name = ""
			thisDataRow.Parameter2_Type = 0
			thisDataRow.Parameter2_Value = 0
			thisDataRow.Parameter3_Name = ""
			thisDataRow.Parameter3_Type = 0
			thisDataRow.Parameter3_Value = 0
			thisDataRow.Parameter4_Name = ""
			thisDataRow.Parameter4_Type = 0
			thisDataRow.Parameter4_Value = 0
			thisDataRow.Parameter5_Name = ""
			thisDataRow.Parameter5_Type = 0
			thisDataRow.Parameter5_Value = 0

			If (CT_TemplateFlags And CInt(RenaissanceGlobals.CompoundTransactionFlags.UsesIsTransferFlag)) Then
				If (Combo_TransferFromCounterparty.SelectedIndex > 0) AndAlso (IsNumeric(Combo_TransferFromCounterparty.SelectedValue)) Then
					thisDataRow.ParameterCount = thisDataRow.ParameterCount + 1
					thisDataRow.Parameter1_Name = "TransferFromCounterparty"
					thisDataRow.Parameter1_Type = SqlDbType.Int
					thisDataRow.Parameter1_Value = Combo_TransferFromCounterparty.SelectedValue.ToString
				End If
			End If


			thisDataRow.CompoundTransactionGroup = 0
			thisDataRow.CompoundTransactionComment = Me.editTransactionComment.Text
			thisDataRow.CompoundTransactionFlags = CT_TemplateFlags
			thisDataRow.CompoundTransactionPosted = False

			thisDataRow.EndEdit()
			InPaint = False

			' Add and Update DataRow. 

			ErrFlag = False

			If AddNewRecord = True Then
				Try
					myTable.Rows.Add(thisDataRow)
				Catch ex As Exception
					ErrFlag = True
					ErrMessage = ex.Message
					ErrStack = ex.StackTrace
				End Try
			End If

			UpdateRows(0) = thisDataRow

			' Post Additions / Updates to the underlying table :-
			Dim temp As Integer
			Try
				If (ErrFlag = False) Then
					myAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
					myAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

					temp = MainForm.AdaptorUpdate(Me.Name, myAdaptor, UpdateRows)

					Call CT_Handler.ProcessCompoundTransaction(Me.MainForm, Me.Name, thisDataRow, PreviousCTRN)

				End If

			Catch ex As Exception

				ErrMessage = ex.Message
				ErrFlag = True
				ErrStack = ex.StackTrace

			End Try

			thisAuditID = thisDataRow.AuditID

			' De-Restrict this Form Selection if a New Transaction Has been added.
			THIS_FORM_SelectCriteria = "True"

		End SyncLock

		' *************************************************************
		' Error report. Not done in the Try-Catch as that would serve to persist the SyncLock.
		' *************************************************************

		If (ErrFlag = True) Then
			Call MainForm.LogError(Me.Name & ", SetFormData", 0, ErrMessage, "Error Saving Data", ErrStack, True)
		End If

		' Finish off

		AddNewRecord = False
		FormChanged = False

		Me.THIS_FORM_SelectingCombo.Enabled = True


		' Propogate changes
		' Refrest Tables

		Call MainForm.Load_Table_Background(RenaissanceStandardDatasets.tblTransaction, "", True, True)
		'Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblTransaction))

		myDataset = MainForm.Load_Table(RenaissanceStandardDatasets.tblCompoundTransaction, True)
		myTable = myDataset.Tables(0)
		' Load_Table contains call to Main_RaiseEvent()
		'Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID), True)

	End Function

    ''' <summary>
    ''' Sets the button status.
    ''' </summary>
  Private Sub SetButtonStatus()
    ' ***********************************************************************
    ' Sets the status of the form controlls appropriate to the current users 
    ' permissions and the 'Changed' or 'New' status of the form.
    ' ***********************************************************************

    ' For the moment, consider Compound Transactions as Un-Editable using this form.
    ' Treat them as Read-Only.

    Dim IsThisACompoundTransaction As Boolean
    IsThisACompoundTransaction = False

    ' No Read permission :-

    If Me.HasReadPermission = False Then
      MainForm.LogError(Me.Name & ", SetButtonStatus()", 0, "", "You do not have Read permission for this Form", "", True)
      Me.Close()
      Exit Sub
    End If

    If (AddNewRecord = True) Then
      Me.Button_ViewTransactions.Enabled = False
    Else
      Me.Button_ViewTransactions.Enabled = True
    End If

    ' Has Insert Permission.
    If Me.HasInsertPermission Then
      Me.btnAdd.Enabled = True
      Me.btnNewUsing.Enabled = True
    Else
      Me.btnAdd.Enabled = False
      Me.btnNewUsing.Enabled = False
    End If

    ' Has Delete permission. 
    If (Me.HasDeletePermission) And (Me.AddNewRecord = False) And (thisPosition >= 0) Then
      Me.btnDelete.Enabled = True
    Else
      Me.btnDelete.Enabled = False
    End If

    If (Me.MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And _
     ((Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord)) And _
     (IsThisACompoundTransaction = False) Then

      Me.Combo_TransactionFund.Enabled = True
      Me.Combo_TransactionInstrument.Enabled = True
      Me.Combo_TransactionType.Enabled = True
      Me.Combo_TradeStatus.Enabled = True
      Me.Combo_TransactionCounterparty.Enabled = True
      Me.Combo_TransactionGroup.Enabled = True
      Me.Combo_InvestmentManager.Enabled = True
      Me.Combo_TransactionManager.Enabled = True
      Me.Combo_DataEntryManager.Enabled = True
      Me.Date_DecisionDate.Enabled = True
      Me.Date_ValueDate.Enabled = True
      Me.Date_SettlementDate.Enabled = True
      Me.Date_ConfirmationDate.Enabled = True
      Me.Combo_ValueOrAmount.Enabled = True

      If (Me.Combo_ValueOrAmount.Text.ToUpper.StartsWith("V")) Then
        Me.edit_Amount.Enabled = False
        Me.edit_Price.Enabled = True
        Me.edit_Costs.Enabled = True
        Me.edit_CostPercentage.Enabled = True
        Me.edit_settlement.Enabled = True
      Else
        Me.edit_Amount.Enabled = True
        Me.edit_Price.Enabled = True
        Me.edit_Costs.Enabled = True
        Me.edit_CostPercentage.Enabled = False
        Me.edit_settlement.Enabled = False
      End If

      Me.Check_CostsAsPercent.Enabled = True
      Me.Check_DontUpdatePrice.Enabled = True
      Me.Check_FinalAmount.Enabled = True
      Me.Check_FinalPrice.Enabled = True
      Me.Check_FinalCosts.Enabled = True
      Me.Check_FinalSettlement.Enabled = True
      Me.Check_InstructionGiven.Enabled = True
      Me.Check_BankConfirmation.Enabled = True
      Me.Check_InitialDataEntry.Enabled = True
      Me.Check_PriceConfirmation.Enabled = True
      Me.Check_SettlementConfirmed.Enabled = True
      Me.Check_FinalDataEntry.Enabled = True

      Dim TransactionFlags As RenaissanceGlobals.CompoundTransactionFlags

      Try
        TransactionFlags = CType(LookupTableValue(MainForm, RenaissanceGlobals.RenaissanceStandardDatasets.tblCompoundTransactionTemplate, CInt(Me.Combo_TransactionType.SelectedValue), "TemplateFlags"), RenaissanceGlobals.TransactionTypes)
      Catch ex As Exception
        TransactionFlags = CompoundTransactionFlags.UsesIsTransferFlag
      End Try

      If (TransactionFlags And RenaissanceGlobals.CompoundTransactionFlags.UsesIsTransferFlag) Then
        Me.Check_IsTransfer.Enabled = True

        If (Check_IsTransfer.Checked) Then

          DateTime_EffectiveValueDate.Enabled = True
          Edit_EffectivePrice.Enabled = True
          Edit_EffectiveWatermark.Enabled = True
          Check_SpecificInitialEqualisation.Enabled = True
          Combo_TransferFromCounterparty.Enabled = True

          If (Check_SpecificInitialEqualisation.Checked) Then
            Edit_SpecificInitialEqualisation.Enabled = True
          Else
            Edit_SpecificInitialEqualisation.Enabled = False
          End If

        Else

          DateTime_EffectiveValueDate.Enabled = False
          Edit_EffectivePrice.Enabled = False
          Edit_EffectiveWatermark.Enabled = False
          Check_SpecificInitialEqualisation.Enabled = False
          Edit_SpecificInitialEqualisation.Enabled = False
          Combo_TransferFromCounterparty.Enabled = False

        End If

      Else

        Check_IsTransfer.Enabled = False
        DateTime_EffectiveValueDate.Enabled = False
        Edit_EffectivePrice.Enabled = False
        Edit_EffectiveWatermark.Enabled = False
        Check_SpecificInitialEqualisation.Enabled = False
        Edit_SpecificInitialEqualisation.Enabled = False
        Combo_TransferFromCounterparty.Enabled = False

      End If

      Me.editTransactionComment.Enabled = True
    Else

      Me.Combo_TransactionFund.Enabled = False
      Me.Combo_TransactionInstrument.Enabled = False
      Me.Combo_TransactionType.Enabled = False
      Me.Combo_TradeStatus.Enabled = False
      Me.Combo_TransactionCounterparty.Enabled = False
      Me.Combo_TransactionGroup.Enabled = False
      Me.Combo_InvestmentManager.Enabled = False
      Me.Combo_TransactionManager.Enabled = False
      Me.Combo_DataEntryManager.Enabled = False
      Me.Date_DecisionDate.Enabled = False
      Me.Date_ValueDate.Enabled = False
      Me.Date_SettlementDate.Enabled = False
      Me.Date_ConfirmationDate.Enabled = False
      Me.Combo_ValueOrAmount.Enabled = False

      Me.edit_Amount.Enabled = False
      Me.edit_Price.Enabled = False
      Me.edit_Costs.Enabled = False
      Me.edit_CostPercentage.Enabled = False
      Me.edit_settlement.Enabled = False

      Me.Check_CostsAsPercent.Enabled = False
      Me.Check_DontUpdatePrice.Enabled = False
      Me.Check_FinalAmount.Enabled = False
      Me.Check_FinalPrice.Enabled = False
      Me.Check_FinalCosts.Enabled = False
      Me.Check_FinalSettlement.Enabled = False
      Me.Check_InstructionGiven.Enabled = False
      Me.Check_BankConfirmation.Enabled = False
      Me.Check_InitialDataEntry.Enabled = False
      Me.Check_PriceConfirmation.Enabled = False
      Me.Check_SettlementConfirmed.Enabled = False
      Me.Check_FinalDataEntry.Enabled = False
      Me.editTransactionComment.Enabled = False
      Me.Check_IsTransfer.Enabled = False
      Me.DateTime_EffectiveValueDate.Enabled = False
      Me.Edit_EffectivePrice.Enabled = False
      Me.Edit_EffectiveWatermark.Enabled = False
      Me.Check_SpecificInitialEqualisation.Enabled = False
      Me.Edit_SpecificInitialEqualisation.Enabled = False
      Me.Combo_TransferFromCounterparty.Enabled = False


    End If

  End Sub

    ''' <summary>
    ''' Validates the form.
    ''' </summary>
    ''' <param name="pReturnString">The p return string.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	Private Function ValidateForm(ByRef pReturnString As String) As Boolean
		' ***********************************************************************
		' Form Validation code.
		' 
		' This code should be the final arbiter of what is allowed. no assumptions regarding 
		' prior validation should be made.
		' 
		' This Code is called by the SetFormData routine before position changes.
		'
		' ***********************************************************************
		Dim RVal As Boolean

		RVal = True
		pReturnString = ""

		' Validate 

		If Me.Combo_TransactionFund.SelectedIndex < 0 Then
			pReturnString = "Transaction Fund must not be left blank."
			RVal = False
		End If

		If (RVal) AndAlso (Me.Combo_TransactionInstrument.SelectedIndex < 0) Then
			pReturnString = "Transaction Instrument must not be left blank."
			RVal = False
		End If

		If (RVal) AndAlso (Me.Combo_TransactionType.SelectedIndex < 0) Then
			pReturnString = "Transaction Type must not be left blank."
			RVal = False
		End If

		If (RVal) AndAlso (Me.Combo_TradeStatus.SelectedIndex < 0) Then
			pReturnString = "Trade Status must not be left blank."
			RVal = False
		End If

		If (RVal) AndAlso (Me.Combo_TransactionCounterparty.SelectedIndex < 0) Then
			pReturnString = "Counterparty must not be left blank."
			RVal = False
		End If

		'If (RVal) AndAlso (Me.Combo_InvestmentManager.SelectedIndex < 0) Then
		'  pReturnString = "Investment Manager must not be left blank."
		'  RVal = False
		'End If

		'If (RVal) AndAlso (Me.Combo_DataEntryManager.SelectedIndex < 0) Then
		'  pReturnString = "Data Entry Manager must not be left blank."
		'  RVal = False
		'End If

		If (RVal) AndAlso (Me.Combo_ValueOrAmount.SelectedIndex < 0) Then
			pReturnString = "`Value or Amount` must not be left blank."
			RVal = False
		End If

		If (RVal) AndAlso (IsDate(Me.Date_DecisionDate.Text) = False) Then
			pReturnString = "Decision Date must not be left blank."
			RVal = False
		End If

		If (RVal) AndAlso (IsDate(Me.Date_ValueDate.Text) = False) Then
			pReturnString = "Trade Date must not be left blank."
			RVal = False
		End If

		If (RVal) AndAlso (IsDate(Me.Date_SettlementDate.Text) = False) Then
			pReturnString = "Settlement Date must not be left blank."
			RVal = False
		End If

		If (RVal) AndAlso (IsDate(Me.Date_ConfirmationDate.Text) = False) Then
			pReturnString = "Confirmation Date must not be left blank."
			RVal = False
		End If

		Dim CT_TemplateFlags As Integer
		Dim CompoundTemplateTable As RenaissanceDataClass.DSCompoundTransactionTemplate.tblCompoundTransactionTemplateDataTable
		Dim CompoundTemplateRows() As RenaissanceDataClass.DSCompoundTransactionTemplate.tblCompoundTransactionTemplateRow

		Try
			CompoundTemplateTable = CType(Me.MainForm.Load_Table(RenaissanceStandardDatasets.tblCompoundTransactionTemplate, False), RenaissanceDataClass.DSCompoundTransactionTemplate).tblCompoundTransactionTemplate
			CompoundTemplateRows = CompoundTemplateTable.Select("TemplateID=" & CInt(Me.Combo_TransactionType.SelectedValue).ToString, "TemplateFlags DESC")
			If (Not (CompoundTemplateRows Is Nothing)) AndAlso (CompoundTemplateRows.Length > 0) Then
				CT_TemplateFlags = CompoundTemplateRows(0).TemplateFlags
			End If

			If (CT_TemplateFlags And CInt(RenaissanceGlobals.CompoundTransactionFlags.UsesIsTransferFlag)) Then
				If (Combo_TransferFromCounterparty.SelectedIndex <= 0) OrElse (Not IsNumeric(Combo_TransferFromCounterparty.SelectedValue)) Then
					pReturnString = "'Transfer From Counterparty' must be specified for 'Holding Transfer' type Compound transactions.."
					RVal = False
				End If
			End If
		Catch ex As Exception
		Finally
			CompoundTemplateRows = Nothing
			CompoundTemplateTable = Nothing
		End Try

		Return RVal

	End Function

    ''' <summary>
    ''' Handles the MouseEnter event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnCancel_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseEnter
		' ***********************************************************************
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		' ***********************************************************************

		Me.IsOverCancelButton = True
	End Sub

    ''' <summary>
    ''' Handles the MouseLeave event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnCancel_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseLeave
		' ***********************************************************************
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		' ***********************************************************************

		Me.IsOverCancelButton = False
	End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_ValueOrAmount control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_ValueOrAmount_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_ValueOrAmount.SelectedIndexChanged
		' ***********************************************************************
		'
		'
		' ***********************************************************************

		If Me.InPaint = False Then
			If (Me.Combo_ValueOrAmount.Text.ToUpper.StartsWith("V")) Then
				Me.edit_Amount.Enabled = False
				Me.edit_Price.Enabled = True
				Me.edit_Costs.Enabled = True
				Me.edit_settlement.Enabled = True
			Else
				Me.edit_Amount.Enabled = True
				Me.edit_Price.Enabled = True
				Me.edit_Costs.Enabled = True
				Me.edit_settlement.Enabled = False
			End If
		End If

	End Sub

#End Region

#Region " Navigation Code / GetPosition() (Generic Code) "

    ''' <summary>
    ''' Moves to audit ID.
    ''' </summary>
    ''' <param name="pAuditID">The p audit ID.</param>
	Public Sub MoveToAuditID(ByVal pAuditID As Integer)
		' ****************************************************************
		' Subroutine to move the current form focus to a given AuditID
		' ****************************************************************

		Dim findDataRow As DataRow
		Dim Counter As Integer

		Try
			If (SortedRows Is Nothing) Then Exit Sub
			If (SortedRows.Length <= 0) Then Exit Sub

			For Counter = 0 To (SortedRows.Length - 1)
				findDataRow = SortedRows(Counter)
				If findDataRow("AuditID").Equals(pAuditID) Then
					THIS_FORM_SelectingCombo.SelectedIndex = Counter
				End If
			Next
		Catch ex As Exception
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Moving to given AuditID.", ex.StackTrace, True)
		End Try

	End Sub

    ''' <summary>
    ''' Handles the SelectComboChanged event of the Combo control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_SelectComboChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
		' Selection Combo. SelectedItem changed.
		'

		' Don't react to changes made in paint routines etc.
		If InPaint = True Then Exit Sub

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		' Find the correct data row, then show it...
		thisPosition = THIS_FORM_SelectingCombo.SelectedIndex

		If (thisPosition >= 0) Then
			thisDataRow = Me.SortedRows(thisPosition)
		Else
			thisDataRow = Nothing
		End If

		Call GetFormData(thisDataRow)

	End Sub


    ''' <summary>
    ''' Handles the Click event of the btnNavPrev control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnNavPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavPrev.Click
		' 'Previous' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		If thisPosition > 0 Then thisPosition -= 1
		If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
			thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
		End If

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

		Exit Sub

		If myTable.Rows.Count > thisPosition Then
			thisDataRow = myTable.Rows(thisPosition)
			Call GetFormData(thisDataRow)
		Else
			Call GetFormData(Nothing)
		End If

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnNavNext control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnNavNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavNext.Click
		' 'Next' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		If thisPosition < (myTable.Rows.Count - 1) Then thisPosition += 1
		If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
			thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
		End If

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

		Exit Sub

		If myTable.Rows.Count > thisPosition Then
			thisDataRow = myTable.Rows(thisPosition)
			Call GetFormData(thisDataRow)
		Else
			Call GetFormData(Nothing)
		End If

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnNavFirst control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnNavFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavFirst.Click
		' 'First' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		thisPosition = 0
		If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
			thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
		End If

		If (THIS_FORM_SelectingCombo.SelectedIndex = thisPosition) Then
			Combo_SelectComboChanged(THIS_FORM_SelectingCombo, New EventArgs)
		Else
			THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
		End If

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnLast control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLast.Click
		' 'Last' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		thisPosition = THIS_FORM_SelectingCombo.Items.Count - 1

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

	End Sub

    ''' <summary>
    ''' Get_s the position.
    ''' </summary>
    ''' <param name="pAuditID">The p audit ID.</param>
    ''' <returns>System.Int32.</returns>
	Private Function Get_Position(ByVal pAuditID As Integer) As Integer
		' Returns the position in the current 'SortedRows' array of the entry corresponding to the given 
		' AudidID.

		Dim MinIndex As Integer
		Dim MaxIndex As Integer
		Dim CurrentMin As Integer
		Dim CurrentMax As Integer
		Dim searchDataRow As DataRow

		Try
			' SortedRows exists ?

			If SortedRows Is Nothing Then
				Return (-1)
				Exit Function
			End If

			' Return (-1) if there are no rows in the 'SortedRows'

			If (SortedRows.GetLength(0) <= 0) Then
				Return (-1)
				Exit Function
			End If


			' Use a modified Search moving outwards from the last returned value to locate the required value.
			' Reflecting the fact that for the most part One looks for closely located records.

			MinIndex = 0
			MaxIndex = SortedRows.GetLength(0) - 1


			' Check First and Last records (Incase 'First' or 'Last' was pressed).

			searchDataRow = SortedRows(MinIndex)

			If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(searchDataRow("AuditID")) = pAuditID) Then
				Return MinIndex
				Exit Function
			End If

			searchDataRow = SortedRows(MaxIndex)
			If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(searchDataRow("AuditID")) = pAuditID) Then
				Return MaxIndex
				Exit Function
			End If

			' now search outwards from the last returned value.

			If (Me.thisPosition >= MinIndex) And (Me.thisPosition <= MaxIndex) Then
				CurrentMin = thisPosition
				CurrentMax = CurrentMin + 1
			Else
				CurrentMin = CInt((MinIndex + MaxIndex) / 2)
				CurrentMax = CurrentMin + 1
			End If

			While (CurrentMin >= MinIndex) Or (CurrentMax <= MaxIndex)
				If (CurrentMin >= MinIndex) Then
					searchDataRow = SortedRows(CurrentMin)

					If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(searchDataRow("AuditID")) = pAuditID) Then
						Return CurrentMin
						Exit Function
					End If

					CurrentMin -= 1
				End If

				If (CurrentMax <= MaxIndex) Then
					searchDataRow = SortedRows(CurrentMax)

					If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(searchDataRow("AuditID")) = pAuditID) Then
						Return CurrentMax
						Exit Function
					End If

					CurrentMax += 1
				End If

			End While

			Return (-1)
			Exit Function

		Catch ex As Exception
			MainForm.LogError(Me.Name & ", Get_Position()", LOG_LEVELS.Warning, ex.Message, "", ex.StackTrace, True)
			Return (-1)
			Exit Function
		End Try

	End Function

#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

    ''' <summary>
    ''' Handles the Click event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
		' Cancel Changes, redisplay form.

		FormChanged = False
		AddNewRecord = False

		Me.THIS_FORM_SelectingCombo.Enabled = True
		Me.Button_ViewTransactions.Enabled = True

		If (thisPosition >= 0) And (thisPosition < Me.SortedRows.GetLength(0)) Then
			thisDataRow = Me.SortedRows(thisPosition)
			Call GetFormData(thisDataRow)
		Else
			Call btnNavFirst_Click(Me, New System.EventArgs)
		End If

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnSave control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
		' Save Changes, if any, without prompting.

		If (FormChanged = True) Then
			Call SetFormData(False)
		End If

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnDelete control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
		Dim Position As Integer
		Dim NextAuditID As Integer

		If (AddNewRecord = True) Then
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Sub
		End If

		' No Appropriate Save permission :-

		If (Me.HasDeletePermission = False) Then
			MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "You do not have permission to Delete this record.", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Sub
		End If

		' *************************************************************
		' KnowledgeDate OK :-
		' *************************************************************

		If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
			MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "Changes to Venice are not allowed when the Knowledgedate is not set to `NOW`", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Sub
		End If

		' *************************************************************
		' Confirm :-
		' *************************************************************
		If MsgBox("Are you Sure ?", MsgBoxStyle.OkCancel, "Confirm Delete") = MsgBoxResult.Cancel Then
			Exit Sub
		End If

		' Check Data position.

		Position = Get_Position(thisAuditID)

		If Position < 0 Then Exit Sub

		' Check Referential Integrity 
		If CheckReferentialIntegrity(_MainForm, Me.SortedRows(Position)) = False Then
			MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "Deleting this record would voilate referential integrity.", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Sub
		End If

		' Resolve row to show after deleting this one.

		NextAuditID = (-1)
		If (Position + 1) < Me.SortedRows.GetLength(0) Then
			NextAuditID = CInt(Me.SortedRows(Position + 1)("AuditID"))
		ElseIf (Position - 1) >= 0 Then
			NextAuditID = CInt(Me.SortedRows(Position - 1)("AuditID"))
		Else
			NextAuditID = (-1)
		End If

		' Delete this row

		Try
			Me.SortedRows(Position).Delete()

			MainForm.AdaptorUpdate(Me.Name, myAdaptor, myTable)

		Catch ex As Exception

			Call MainForm.LogError(Me.Name & ", Delete()", LOG_LEVELS.Error, ex.Message, "Error Deleting Record.", ex.StackTrace, True)
		End Try

		' Tidy Up.

		FormChanged = False

		thisAuditID = NextAuditID
		Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID), True)

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnAdd control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
		' Prepare form to Add a new record.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		thisPosition = -1
		InPaint = True
		MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
		InPaint = False

		GetFormData(Nothing)
		AddNewRecord = True

		Me.btnCancel.Enabled = True
		Me.THIS_FORM_SelectingCombo.Enabled = False

    Call SetButtonStatus()

		THIS_FORM_NewMoveToControl.Focus()

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnNewUsing control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnNewUsing_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewUsing.Click
		' Prepare form to Add a new record using the existing details

		If (FormChanged = True) Then
			Call SetFormData()
		End If


		InPaint = True
		MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
		Me.editAuditID.Text = "0"
		InPaint = False

		AddNewRecord = True
		thisPosition = -1
		thisAuditID = (-1)
		FormChanged = True
		Me.btnCancel.Enabled = True
		Me.btnSave.Enabled = True
		Me.THIS_FORM_SelectingCombo.Enabled = False

    Call SetButtonStatus()

		THIS_FORM_NewMoveToControl.Focus()
	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnClose control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
		' Close Form

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		Me.Close()

	End Sub

#End Region

#Region " Bug hunting "

  ' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
  ' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
  ' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

    ''' <summary>
    ''' Called when [row updating].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

    ''' <summary>
    ''' Called when [row updated].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

    ''' <summary>
    ''' Called when [row fill error].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
    Dim a As Integer

    a = 1
  End Sub

#End Region



End Class
