' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmCharacteristics.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals

Imports RenaissanceDataClass
Imports C1.Win.C1FlexGrid

''' <summary>
''' Class frmCharacteristics
''' </summary>
Public Class frmCharacteristics

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

  ''' <summary>
  ''' Prevents a default instance of the <see cref="frmCharacteristics"/> class from being created.
  ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  ''' <summary>
  ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
  ''' </summary>
  ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  ''' <summary>
  ''' The components
  ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  ''' <summary>
  ''' The combo_ category
  ''' </summary>
  Friend WithEvents Combo_Category As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The label_ category
  ''' </summary>
  Friend WithEvents label_Category As System.Windows.Forms.Label
  ''' <summary>
  ''' The combo_ instrument
  ''' </summary>
  Friend WithEvents Combo_Instrument As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The label2
  ''' </summary>
  Friend WithEvents Label2 As System.Windows.Forms.Label
  ''' <summary>
  ''' The label3
  ''' </summary>
  Friend WithEvents Label3 As System.Windows.Forms.Label
  ''' <summary>
  ''' The date_ updated date
  ''' </summary>
  Friend WithEvents Date_UpdatedDate As System.Windows.Forms.DateTimePicker
  ''' <summary>
  ''' The combo_ updated date operator
  ''' </summary>
  Friend WithEvents Combo_UpdatedDateOperator As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The check_ entered characteristics
  ''' </summary>
  Friend WithEvents Check_EnteredCharacteristics As System.Windows.Forms.CheckBox
  ''' <summary>
  ''' The grid_ characteristics
  ''' </summary>
  Friend WithEvents Grid_Characteristics As C1.Win.C1FlexGrid.C1FlexGrid
  ''' <summary>
  ''' The root menu
  ''' </summary>
  Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
  ''' <summary>
  ''' The form_ status strip
  ''' </summary>
  Friend WithEvents Form_StatusStrip As System.Windows.Forms.StatusStrip
  ''' <summary>
  ''' The form_ progress bar
  ''' </summary>
  Friend WithEvents Form_ProgressBar As System.Windows.Forms.ToolStripProgressBar
  ''' <summary>
  ''' The combo_ characteristic
  ''' </summary>
  Friend WithEvents Combo_Characteristic As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The label_ characteristic
  ''' </summary>
  Friend WithEvents Label_Characteristic As System.Windows.Forms.Label
  ''' <summary>
  ''' The context menu_ grid characteristics
  ''' </summary>
  Friend WithEvents ContextMenu_GridCharacteristics As System.Windows.Forms.ContextMenuStrip
  ''' <summary>
  ''' The context_ add new category
  ''' </summary>
  Friend WithEvents Context_AddNewCategory As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The context_ add new characteristic
  ''' </summary>
  Friend WithEvents Context_AddNewCharacteristic As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The context_ separator save
  ''' </summary>
  Friend WithEvents Context_SeparatorSave As System.Windows.Forms.ToolStripSeparator
  ''' <summary>
  ''' The context_ save changes
  ''' </summary>
  Friend WithEvents Context_SaveChanges As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The context_ cancel changes
  ''' </summary>
  Friend WithEvents Context_CancelChanges As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The BTN save
  ''' </summary>
  Friend WithEvents btnSave As System.Windows.Forms.Button
  ''' <summary>
  ''' The BTN cancel
  ''' </summary>
  Friend WithEvents btnCancel As System.Windows.Forms.Button
  ''' <summary>
  ''' The context_ rename separator
  ''' </summary>
  Friend WithEvents Context_RenameSeparator As System.Windows.Forms.ToolStripSeparator
  ''' <summary>
  ''' The context_ rename label
  ''' </summary>
  Friend WithEvents Context_RenameLabel As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The context_ rename text box
  ''' </summary>
  Friend WithEvents Context_RenameTextBox As System.Windows.Forms.ToolStripTextBox
  ''' <summary>
  ''' The context_ separator delete
  ''' </summary>
  Friend WithEvents Context_SeparatorDelete As System.Windows.Forms.ToolStripSeparator
  ''' <summary>
  ''' The context_ delete
  ''' </summary>
  Friend WithEvents Context_Delete As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The context_ characteristic value separator
  ''' </summary>
  Friend WithEvents Context_CharacteristicValueSeparator As System.Windows.Forms.ToolStripSeparator
  ''' <summary>
  ''' The context_ set characteristic default value
  ''' </summary>
  Friend WithEvents Context_SetCharacteristicDefaultValue As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The context_ set characteristic fixed value
  ''' </summary>
  Friend WithEvents Context_SetCharacteristicFixedValue As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The context_ expand instruments
  ''' </summary>
  Friend WithEvents Context_ExpandInstruments As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The context_ colapse instruments
  ''' </summary>
  Friend WithEvents Context_ColapseInstruments As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The context_ expand categories
  ''' </summary>
  Friend WithEvents Context_ExpandCategories As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The context_ colapse categories
  ''' </summary>
  Friend WithEvents Context_ColapseCategories As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The tool strip separator1
  ''' </summary>
  Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
  ''' <summary>
  ''' The status label_ characteristics
  ''' </summary>
  Friend WithEvents StatusLabel_Characteristics As System.Windows.Forms.ToolStripStatusLabel
  ''' <summary>
  ''' The combo_ select by fund
  ''' </summary>
  Friend WithEvents Combo_SelectByFund As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The label1
  ''' </summary>
  Friend WithEvents Label1 As System.Windows.Forms.Label
  ''' <summary>
  ''' The check_ count limit
  ''' </summary>
  Friend WithEvents Check_CountLimit As System.Windows.Forms.CheckBox
  ''' <summary>
  ''' The numeric_ count limit
  ''' </summary>
  Friend WithEvents Numeric_CountLimit As System.Windows.Forms.NumericUpDown
  ''' <summary>
  ''' The context_ separator copy paste
  ''' </summary>
  Friend WithEvents Context_SeparatorCopyPaste As System.Windows.Forms.ToolStripSeparator
  ''' <summary>
  ''' The context_ copy characteristic
  ''' </summary>
  Friend WithEvents Context_CopyCharacteristic As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The context_ paste characteristic
  ''' </summary>
  Friend WithEvents Context_PasteCharacteristic As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The context_ expand this instrument
  ''' </summary>
  Friend WithEvents Context_ExpandThisInstrument As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The tool strip_ expand this instrument
  ''' </summary>
  Friend WithEvents ToolStrip_ExpandThisInstrument As System.Windows.Forms.ToolStripSeparator
  ''' <summary>
  ''' The label_ status
  ''' </summary>
  Friend WithEvents Label_Status As System.Windows.Forms.ToolStripStatusLabel
  ''' <summary>
  ''' Initializes the component.
  ''' </summary>
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCharacteristics))
    Me.Combo_Category = New System.Windows.Forms.ComboBox
    Me.label_Category = New System.Windows.Forms.Label
    Me.Combo_Instrument = New System.Windows.Forms.ComboBox
    Me.Label2 = New System.Windows.Forms.Label
    Me.Label3 = New System.Windows.Forms.Label
    Me.Date_UpdatedDate = New System.Windows.Forms.DateTimePicker
    Me.Combo_UpdatedDateOperator = New System.Windows.Forms.ComboBox
    Me.Check_EnteredCharacteristics = New System.Windows.Forms.CheckBox
    Me.Grid_Characteristics = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.ContextMenu_GridCharacteristics = New System.Windows.Forms.ContextMenuStrip(Me.components)
    Me.Context_ExpandThisInstrument = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStrip_ExpandThisInstrument = New System.Windows.Forms.ToolStripSeparator
    Me.Context_ExpandInstruments = New System.Windows.Forms.ToolStripMenuItem
    Me.Context_ColapseInstruments = New System.Windows.Forms.ToolStripMenuItem
    Me.Context_ExpandCategories = New System.Windows.Forms.ToolStripMenuItem
    Me.Context_ColapseCategories = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
    Me.Context_AddNewCategory = New System.Windows.Forms.ToolStripMenuItem
    Me.Context_AddNewCharacteristic = New System.Windows.Forms.ToolStripMenuItem
    Me.Context_RenameSeparator = New System.Windows.Forms.ToolStripSeparator
    Me.Context_RenameLabel = New System.Windows.Forms.ToolStripMenuItem
    Me.Context_RenameTextBox = New System.Windows.Forms.ToolStripTextBox
    Me.Context_CharacteristicValueSeparator = New System.Windows.Forms.ToolStripSeparator
    Me.Context_SetCharacteristicDefaultValue = New System.Windows.Forms.ToolStripMenuItem
    Me.Context_SetCharacteristicFixedValue = New System.Windows.Forms.ToolStripMenuItem
    Me.Context_SeparatorDelete = New System.Windows.Forms.ToolStripSeparator
    Me.Context_Delete = New System.Windows.Forms.ToolStripMenuItem
    Me.Context_SeparatorCopyPaste = New System.Windows.Forms.ToolStripSeparator
    Me.Context_CopyCharacteristic = New System.Windows.Forms.ToolStripMenuItem
    Me.Context_PasteCharacteristic = New System.Windows.Forms.ToolStripMenuItem
    Me.Context_SeparatorSave = New System.Windows.Forms.ToolStripSeparator
    Me.Context_SaveChanges = New System.Windows.Forms.ToolStripMenuItem
    Me.Context_CancelChanges = New System.Windows.Forms.ToolStripMenuItem
    Me.RootMenu = New System.Windows.Forms.MenuStrip
    Me.Form_StatusStrip = New System.Windows.Forms.StatusStrip
    Me.Form_ProgressBar = New System.Windows.Forms.ToolStripProgressBar
    Me.Label_Status = New System.Windows.Forms.ToolStripStatusLabel
    Me.StatusLabel_Characteristics = New System.Windows.Forms.ToolStripStatusLabel
    Me.Combo_Characteristic = New System.Windows.Forms.ComboBox
    Me.Label_Characteristic = New System.Windows.Forms.Label
    Me.btnSave = New System.Windows.Forms.Button
    Me.btnCancel = New System.Windows.Forms.Button
    Me.Combo_SelectByFund = New System.Windows.Forms.ComboBox
    Me.Label1 = New System.Windows.Forms.Label
    Me.Check_CountLimit = New System.Windows.Forms.CheckBox
    Me.Numeric_CountLimit = New System.Windows.Forms.NumericUpDown
    CType(Me.Grid_Characteristics, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.ContextMenu_GridCharacteristics.SuspendLayout()
    Me.Form_StatusStrip.SuspendLayout()
    CType(Me.Numeric_CountLimit, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'Combo_Category
    '
    Me.Combo_Category.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Category.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Category.Location = New System.Drawing.Point(128, 87)
    Me.Combo_Category.Name = "Combo_Category"
    Me.Combo_Category.Size = New System.Drawing.Size(808, 21)
    Me.Combo_Category.TabIndex = 2
    '
    'label_Category
    '
    Me.label_Category.Location = New System.Drawing.Point(16, 91)
    Me.label_Category.Name = "label_Category"
    Me.label_Category.Size = New System.Drawing.Size(104, 16)
    Me.label_Category.TabIndex = 82
    Me.label_Category.Text = "Category"
    '
    'Combo_Instrument
    '
    Me.Combo_Instrument.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Instrument.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Instrument.Location = New System.Drawing.Point(128, 59)
    Me.Combo_Instrument.Name = "Combo_Instrument"
    Me.Combo_Instrument.Size = New System.Drawing.Size(808, 21)
    Me.Combo_Instrument.TabIndex = 1
    '
    'Label2
    '
    Me.Label2.Location = New System.Drawing.Point(16, 63)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(104, 16)
    Me.Label2.TabIndex = 84
    Me.Label2.Text = "Instrument"
    '
    'Label3
    '
    Me.Label3.Location = New System.Drawing.Point(16, 147)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(104, 16)
    Me.Label3.TabIndex = 88
    Me.Label3.Text = "Date Updated"
    '
    'Date_UpdatedDate
    '
    Me.Date_UpdatedDate.Location = New System.Drawing.Point(228, 141)
    Me.Date_UpdatedDate.Name = "Date_UpdatedDate"
    Me.Date_UpdatedDate.Size = New System.Drawing.Size(174, 20)
    Me.Date_UpdatedDate.TabIndex = 5
    '
    'Combo_UpdatedDateOperator
    '
    Me.Combo_UpdatedDateOperator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_UpdatedDateOperator.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_UpdatedDateOperator.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", ""})
    Me.Combo_UpdatedDateOperator.Location = New System.Drawing.Point(128, 143)
    Me.Combo_UpdatedDateOperator.Name = "Combo_UpdatedDateOperator"
    Me.Combo_UpdatedDateOperator.Size = New System.Drawing.Size(96, 21)
    Me.Combo_UpdatedDateOperator.TabIndex = 4
    '
    'Check_EnteredCharacteristics
    '
    Me.Check_EnteredCharacteristics.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_EnteredCharacteristics.Location = New System.Drawing.Point(128, 178)
    Me.Check_EnteredCharacteristics.Name = "Check_EnteredCharacteristics"
    Me.Check_EnteredCharacteristics.Size = New System.Drawing.Size(214, 16)
    Me.Check_EnteredCharacteristics.TabIndex = 6
    Me.Check_EnteredCharacteristics.Text = "Select Only Entered Characteristics"
    '
    'Grid_Characteristics
    '
    Me.Grid_Characteristics.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None
    Me.Grid_Characteristics.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None
    Me.Grid_Characteristics.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_Characteristics.AutoGenerateColumns = False
    Me.Grid_Characteristics.ColumnInfo = resources.GetString("Grid_Characteristics.ColumnInfo")
    Me.Grid_Characteristics.ContextMenuStrip = Me.ContextMenu_GridCharacteristics
    Me.Grid_Characteristics.Cursor = System.Windows.Forms.Cursors.Default
    Me.Grid_Characteristics.Location = New System.Drawing.Point(4, 202)
    Me.Grid_Characteristics.Name = "Grid_Characteristics"
    Me.Grid_Characteristics.Rows.DefaultSize = 17
    Me.Grid_Characteristics.Size = New System.Drawing.Size(936, 501)
    Me.Grid_Characteristics.TabIndex = 9
    Me.Grid_Characteristics.Tree.Column = 0
    '
    'ContextMenu_GridCharacteristics
    '
    Me.ContextMenu_GridCharacteristics.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Context_ExpandThisInstrument, Me.ToolStrip_ExpandThisInstrument, Me.Context_ExpandInstruments, Me.Context_ColapseInstruments, Me.Context_ExpandCategories, Me.Context_ColapseCategories, Me.ToolStripSeparator1, Me.Context_AddNewCategory, Me.Context_AddNewCharacteristic, Me.Context_RenameSeparator, Me.Context_RenameLabel, Me.Context_RenameTextBox, Me.Context_CharacteristicValueSeparator, Me.Context_SetCharacteristicDefaultValue, Me.Context_SetCharacteristicFixedValue, Me.Context_SeparatorDelete, Me.Context_Delete, Me.Context_SeparatorCopyPaste, Me.Context_CopyCharacteristic, Me.Context_PasteCharacteristic, Me.Context_SeparatorSave, Me.Context_SaveChanges, Me.Context_CancelChanges})
    Me.ContextMenu_GridCharacteristics.Name = "ContextMenu_GridCharacteristics"
    Me.ContextMenu_GridCharacteristics.Size = New System.Drawing.Size(261, 401)
    '
    'Context_ExpandThisInstrument
    '
    Me.Context_ExpandThisInstrument.Name = "Context_ExpandThisInstrument"
    Me.Context_ExpandThisInstrument.Size = New System.Drawing.Size(260, 22)
    Me.Context_ExpandThisInstrument.Text = "Expand This Instrument"
    '
    'ToolStrip_ExpandThisInstrument
    '
    Me.ToolStrip_ExpandThisInstrument.Name = "ToolStrip_ExpandThisInstrument"
    Me.ToolStrip_ExpandThisInstrument.Size = New System.Drawing.Size(257, 6)
    '
    'Context_ExpandInstruments
    '
    Me.Context_ExpandInstruments.Name = "Context_ExpandInstruments"
    Me.Context_ExpandInstruments.Size = New System.Drawing.Size(260, 22)
    Me.Context_ExpandInstruments.Text = "Expand all Instruments"
    '
    'Context_ColapseInstruments
    '
    Me.Context_ColapseInstruments.Name = "Context_ColapseInstruments"
    Me.Context_ColapseInstruments.Size = New System.Drawing.Size(260, 22)
    Me.Context_ColapseInstruments.Text = "Colapse all Instruments"
    '
    'Context_ExpandCategories
    '
    Me.Context_ExpandCategories.Name = "Context_ExpandCategories"
    Me.Context_ExpandCategories.Size = New System.Drawing.Size(260, 22)
    Me.Context_ExpandCategories.Text = "Expand all Categories"
    '
    'Context_ColapseCategories
    '
    Me.Context_ColapseCategories.Name = "Context_ColapseCategories"
    Me.Context_ColapseCategories.Size = New System.Drawing.Size(260, 22)
    Me.Context_ColapseCategories.Text = "Colapse all Categories"
    '
    'ToolStripSeparator1
    '
    Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
    Me.ToolStripSeparator1.Size = New System.Drawing.Size(257, 6)
    '
    'Context_AddNewCategory
    '
    Me.Context_AddNewCategory.Name = "Context_AddNewCategory"
    Me.Context_AddNewCategory.Size = New System.Drawing.Size(260, 22)
    Me.Context_AddNewCategory.Text = "Add New Category"
    '
    'Context_AddNewCharacteristic
    '
    Me.Context_AddNewCharacteristic.Name = "Context_AddNewCharacteristic"
    Me.Context_AddNewCharacteristic.Size = New System.Drawing.Size(260, 22)
    Me.Context_AddNewCharacteristic.Text = "Add New Characteristic"
    Me.Context_AddNewCharacteristic.Visible = False
    '
    'Context_RenameSeparator
    '
    Me.Context_RenameSeparator.Name = "Context_RenameSeparator"
    Me.Context_RenameSeparator.Size = New System.Drawing.Size(257, 6)
    Me.Context_RenameSeparator.Visible = False
    '
    'Context_RenameLabel
    '
    Me.Context_RenameLabel.Enabled = False
    Me.Context_RenameLabel.Name = "Context_RenameLabel"
    Me.Context_RenameLabel.Size = New System.Drawing.Size(260, 22)
    Me.Context_RenameLabel.Text = "Rename ..."
    Me.Context_RenameLabel.Visible = False
    '
    'Context_RenameTextBox
    '
    Me.Context_RenameTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Context_RenameTextBox.Name = "Context_RenameTextBox"
    Me.Context_RenameTextBox.Size = New System.Drawing.Size(200, 23)
    Me.Context_RenameTextBox.Visible = False
    '
    'Context_CharacteristicValueSeparator
    '
    Me.Context_CharacteristicValueSeparator.Name = "Context_CharacteristicValueSeparator"
    Me.Context_CharacteristicValueSeparator.Size = New System.Drawing.Size(257, 6)
    Me.Context_CharacteristicValueSeparator.Visible = False
    '
    'Context_SetCharacteristicDefaultValue
    '
    Me.Context_SetCharacteristicDefaultValue.Name = "Context_SetCharacteristicDefaultValue"
    Me.Context_SetCharacteristicDefaultValue.Size = New System.Drawing.Size(260, 22)
    Me.Context_SetCharacteristicDefaultValue.Text = "Set Characteristic Default Value"
    Me.Context_SetCharacteristicDefaultValue.Visible = False
    '
    'Context_SetCharacteristicFixedValue
    '
    Me.Context_SetCharacteristicFixedValue.Name = "Context_SetCharacteristicFixedValue"
    Me.Context_SetCharacteristicFixedValue.Size = New System.Drawing.Size(260, 22)
    Me.Context_SetCharacteristicFixedValue.Text = "Set Characteristic Fixed Value"
    Me.Context_SetCharacteristicFixedValue.Visible = False
    '
    'Context_SeparatorDelete
    '
    Me.Context_SeparatorDelete.Name = "Context_SeparatorDelete"
    Me.Context_SeparatorDelete.Size = New System.Drawing.Size(257, 6)
    Me.Context_SeparatorDelete.Visible = False
    '
    'Context_Delete
    '
    Me.Context_Delete.Name = "Context_Delete"
    Me.Context_Delete.Size = New System.Drawing.Size(260, 22)
    Me.Context_Delete.Text = "Delete ..."
    Me.Context_Delete.Visible = False
    '
    'Context_SeparatorCopyPaste
    '
    Me.Context_SeparatorCopyPaste.Name = "Context_SeparatorCopyPaste"
    Me.Context_SeparatorCopyPaste.Size = New System.Drawing.Size(257, 6)
    '
    'Context_CopyCharacteristic
    '
    Me.Context_CopyCharacteristic.Name = "Context_CopyCharacteristic"
    Me.Context_CopyCharacteristic.Size = New System.Drawing.Size(260, 22)
    Me.Context_CopyCharacteristic.Text = "Copy Characteristic values"
    Me.Context_CopyCharacteristic.Visible = False
    '
    'Context_PasteCharacteristic
    '
    Me.Context_PasteCharacteristic.Name = "Context_PasteCharacteristic"
    Me.Context_PasteCharacteristic.Size = New System.Drawing.Size(260, 22)
    Me.Context_PasteCharacteristic.Text = "Paste Characteristic values"
    Me.Context_PasteCharacteristic.Visible = False
    '
    'Context_SeparatorSave
    '
    Me.Context_SeparatorSave.Name = "Context_SeparatorSave"
    Me.Context_SeparatorSave.Size = New System.Drawing.Size(257, 6)
    Me.Context_SeparatorSave.Visible = False
    '
    'Context_SaveChanges
    '
    Me.Context_SaveChanges.Name = "Context_SaveChanges"
    Me.Context_SaveChanges.Size = New System.Drawing.Size(260, 22)
    Me.Context_SaveChanges.Text = "Save Changes"
    Me.Context_SaveChanges.Visible = False
    '
    'Context_CancelChanges
    '
    Me.Context_CancelChanges.Name = "Context_CancelChanges"
    Me.Context_CancelChanges.Size = New System.Drawing.Size(260, 22)
    Me.Context_CancelChanges.Text = "Cancel Changes"
    Me.Context_CancelChanges.Visible = False
    '
    'RootMenu
    '
    Me.RootMenu.Location = New System.Drawing.Point(0, 0)
    Me.RootMenu.Name = "RootMenu"
    Me.RootMenu.Size = New System.Drawing.Size(944, 24)
    Me.RootMenu.TabIndex = 10
    Me.RootMenu.Text = " "
    '
    'Form_StatusStrip
    '
    Me.Form_StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Form_ProgressBar, Me.Label_Status, Me.StatusLabel_Characteristics})
    Me.Form_StatusStrip.Location = New System.Drawing.Point(0, 704)
    Me.Form_StatusStrip.Name = "Form_StatusStrip"
    Me.Form_StatusStrip.Size = New System.Drawing.Size(944, 22)
    Me.Form_StatusStrip.TabIndex = 108
    Me.Form_StatusStrip.Text = " "
    '
    'Form_ProgressBar
    '
    Me.Form_ProgressBar.Maximum = 20
    Me.Form_ProgressBar.Name = "Form_ProgressBar"
    Me.Form_ProgressBar.Size = New System.Drawing.Size(150, 17)
    Me.Form_ProgressBar.Step = 1
    Me.Form_ProgressBar.Visible = False
    '
    'Label_Status
    '
    Me.Label_Status.Name = "Label_Status"
    Me.Label_Status.Size = New System.Drawing.Size(10, 17)
    Me.Label_Status.Text = " "
    '
    'StatusLabel_Characteristics
    '
    Me.StatusLabel_Characteristics.Name = "StatusLabel_Characteristics"
    Me.StatusLabel_Characteristics.Size = New System.Drawing.Size(10, 17)
    Me.StatusLabel_Characteristics.Text = " "
    '
    'Combo_Characteristic
    '
    Me.Combo_Characteristic.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Characteristic.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Characteristic.Location = New System.Drawing.Point(128, 115)
    Me.Combo_Characteristic.Name = "Combo_Characteristic"
    Me.Combo_Characteristic.Size = New System.Drawing.Size(808, 21)
    Me.Combo_Characteristic.TabIndex = 3
    '
    'Label_Characteristic
    '
    Me.Label_Characteristic.Location = New System.Drawing.Point(16, 119)
    Me.Label_Characteristic.Name = "Label_Characteristic"
    Me.Label_Characteristic.Size = New System.Drawing.Size(104, 16)
    Me.Label_Characteristic.TabIndex = 110
    Me.Label_Characteristic.Text = "Characteristic"
    '
    'btnSave
    '
    Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnSave.Location = New System.Drawing.Point(665, 147)
    Me.btnSave.Name = "btnSave"
    Me.btnSave.Size = New System.Drawing.Size(117, 28)
    Me.btnSave.TabIndex = 7
    Me.btnSave.Text = "&Save"
    Me.btnSave.Visible = False
    '
    'btnCancel
    '
    Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnCancel.Location = New System.Drawing.Point(819, 147)
    Me.btnCancel.Name = "btnCancel"
    Me.btnCancel.Size = New System.Drawing.Size(117, 28)
    Me.btnCancel.TabIndex = 8
    Me.btnCancel.Text = "&Cancel"
    Me.btnCancel.Visible = False
    '
    'Combo_SelectByFund
    '
    Me.Combo_SelectByFund.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_SelectByFund.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectByFund.Location = New System.Drawing.Point(128, 32)
    Me.Combo_SelectByFund.Name = "Combo_SelectByFund"
    Me.Combo_SelectByFund.Size = New System.Drawing.Size(808, 21)
    Me.Combo_SelectByFund.TabIndex = 0
    '
    'Label1
    '
    Me.Label1.Location = New System.Drawing.Point(16, 36)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(104, 16)
    Me.Label1.TabIndex = 116
    Me.Label1.Text = "Fund"
    '
    'Check_CountLimit
    '
    Me.Check_CountLimit.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_CountLimit.Location = New System.Drawing.Point(361, 178)
    Me.Check_CountLimit.Name = "Check_CountLimit"
    Me.Check_CountLimit.Size = New System.Drawing.Size(143, 16)
    Me.Check_CountLimit.TabIndex = 117
    Me.Check_CountLimit.Text = "Select where count <="
    '
    'Numeric_CountLimit
    '
    Me.Numeric_CountLimit.Enabled = False
    Me.Numeric_CountLimit.Location = New System.Drawing.Point(502, 177)
    Me.Numeric_CountLimit.Name = "Numeric_CountLimit"
    Me.Numeric_CountLimit.Size = New System.Drawing.Size(47, 20)
    Me.Numeric_CountLimit.TabIndex = 118
    '
    'frmCharacteristics
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(944, 726)
    Me.Controls.Add(Me.Numeric_CountLimit)
    Me.Controls.Add(Me.Check_CountLimit)
    Me.Controls.Add(Me.Combo_SelectByFund)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.btnSave)
    Me.Controls.Add(Me.btnCancel)
    Me.Controls.Add(Me.Combo_Characteristic)
    Me.Controls.Add(Me.Label_Characteristic)
    Me.Controls.Add(Me.Form_StatusStrip)
    Me.Controls.Add(Me.RootMenu)
    Me.Controls.Add(Me.Grid_Characteristics)
    Me.Controls.Add(Me.Check_EnteredCharacteristics)
    Me.Controls.Add(Me.Combo_UpdatedDateOperator)
    Me.Controls.Add(Me.Date_UpdatedDate)
    Me.Controls.Add(Me.Label3)
    Me.Controls.Add(Me.Combo_Instrument)
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.Combo_Category)
    Me.Controls.Add(Me.label_Category)
    Me.Name = "frmCharacteristics"
    Me.Text = "Add / Edit Characteristics"
    CType(Me.Grid_Characteristics, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ContextMenu_GridCharacteristics.ResumeLayout(False)
    Me.ContextMenu_GridCharacteristics.PerformLayout()
    Me.Form_StatusStrip.ResumeLayout(False)
    Me.Form_StatusStrip.PerformLayout()
    CType(Me.Numeric_CountLimit, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "

  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
  ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
  ''' </summary>
  Private WithEvents _MainForm As VeniceMain

  ' Form ToolTip
  ''' <summary>
  ''' Control to provide tooltip functionality.
  ''' </summary>
  Private FormTooltip As New ToolTip()

  ' Form Menu


  ' Form Constants, specific to the table being updated.

  ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
  ''' </summary>
  Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

  ' Form Locals, initialised on 'New' defining what standard data items to use
  ''' <summary>
  ''' Variable holding the form's data table name. taken from the "ThisStandardDataset" object.
  ''' </summary>
  Private THIS_TABLENAME As String
  ''' <summary>
  ''' Variable holding the form's data adaptor name. taken from the "ThisStandardDataset" object.
  ''' </summary>
  Private THIS_ADAPTORNAME As String
  ''' <summary>
  ''' Variable holding the form's dataset name. taken from the "ThisStandardDataset" object.
  ''' </summary>
  Private THIS_DATASETNAME As String


  ' The standard ChangeID for this form. e.g. tblFund
  ''' <summary>
  ''' The standard ChangeID for this form.
  ''' </summary>
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

  ' Form Specific Order fields
  ''' <summary>
  ''' Field Name to order the Select combo by.
  ''' </summary>
  Private THIS_FORM_OrderBy As String

  ' Form specific Permissioning variables
  ''' <summary>
  ''' The THI s_ FOR m_ permission area
  ''' </summary>
  Private THIS_FORM_PermissionArea As String
  ''' <summary>
  ''' The THI s_ FOR m_ permission type
  ''' </summary>
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
  ''' <summary>
  ''' The THI s_ FOR m_ form ID
  ''' </summary>
  Private THIS_FORM_FormID As VeniceFormID

  ' Data Structures

  ''' <summary>
  ''' My dataset
  ''' </summary>
  Private myDataset As DataSet
  ''' <summary>
  ''' My table
  ''' </summary>
  Private myTable As DataTable
  ''' <summary>
  ''' My connection
  ''' </summary>
  Private myConnection As SqlConnection
  ''' <summary>
  ''' My adaptor
  ''' </summary>
  Private myAdaptor As SqlDataAdapter
  ''' <summary>
  ''' My data view
  ''' </summary>
  Private myDataView As DataView

  ''' <summary>
  ''' The this standard dataset
  ''' </summary>
  Private ThisStandardDataset As RenaissanceGlobals.StandardDataset

  ''' <summary>
  ''' The updated category entries
  ''' </summary>
  Private UpdatedCategoryEntries As New ArrayList
  ''' <summary>
  ''' The expanded nodes
  ''' </summary>
  Private ExpandedNodes As New Dictionary(Of ULong, Boolean)
  ''' <summary>
  ''' The last grid scroll
  ''' </summary>
  Private LastGridScroll As System.Drawing.Point = New System.Drawing.Point(0, 0)
  ''' <summary>
  ''' The last entered grid row
  ''' </summary>
  Private LastEnteredGridRow As Integer = 0
  ''' <summary>
  ''' The last entered grid col
  ''' </summary>
  Private LastEnteredGridCol As Integer = 0
  ''' <summary>
  ''' The grid rename focus
  ''' </summary>
  Private GridRenameFocus As Boolean = False
  ''' <summary>
  ''' The grid rename item
  ''' </summary>
  Private GridRenameItem As String = ""
  ''' <summary>
  ''' The grid delete item
  ''' </summary>
  Private GridDeleteItem As String = ""

  ''' <summary>
  ''' The fund holdings
  ''' </summary>
  Private FundHoldings As New Dictionary(Of Integer, SortedList)

  ' List of Instruments and Categories. built in the paint function.

  ''' <summary>
  ''' The category list
  ''' </summary>
  Private CategoryList As New ArrayList

  ' Active Element.

  ''' <summary>
  ''' The __ is over cancel button
  ''' </summary>
  Private __IsOverCancelButton As Boolean
  ''' <summary>
  ''' The _ in use
  ''' </summary>
  Private _InUse As Boolean

  ' Form Status Flags

  ''' <summary>
  ''' The form is valid
  ''' </summary>
  Private FormIsValid As Boolean
  ''' <summary>
  ''' The in paint
  ''' </summary>
  Private InPaint As Boolean
  ''' <summary>
  ''' The _ form open failed
  ''' </summary>
  Private _FormOpenFailed As Boolean

  ' User Permission Flags

  ''' <summary>
  ''' The has read permission
  ''' </summary>
  Private HasReadPermission As Boolean
  ''' <summary>
  ''' The has update permission
  ''' </summary>
  Private HasUpdatePermission As Boolean
  ''' <summary>
  ''' The has insert permission
  ''' </summary>
  Private HasInsertPermission As Boolean
  ''' <summary>
  ''' The has delete permission
  ''' </summary>
  Private HasDeletePermission As Boolean

  ''' <summary>
  ''' Class GridColumns
  ''' </summary>
  Private Class GridColumns
    ''' <summary>
    ''' The first col
    ''' </summary>
    Public Shared FirstCol As Integer = 0
    ''' <summary>
    ''' The ISIN col
    ''' </summary>
    Public Shared ISIN As Integer = 0
    ''' <summary>
    ''' The data weighting column
    ''' </summary>
    Public Shared DataWeighting As Integer = 1
    ''' <summary>
    ''' The last updated column
    ''' </summary>
    Public Shared LastUpdated As Integer = 2
    ''' <summary>
    ''' The instrument ID column
    ''' </summary>
    Public Shared InstrumentID As Integer = 3
    ''' <summary>
    ''' The category ID column
    ''' </summary>
    Public Shared CategoryID As Integer = 4
    ''' <summary>
    ''' The characteristic ID column
    ''' </summary>
    Public Shared CharacteristicID As Integer = 5
    ''' <summary>
    ''' The characteristic type ID column
    ''' </summary>
    Public Shared CharacteristicTypeID As Integer = 6
    ''' <summary>
    ''' Internal counter column
    ''' </summary>
    Public Shared Counter As Integer = 7
    ''' <summary>
    ''' The risk data ID column
    ''' </summary>
    Public Shared RiskDataID As Integer = 8
    ''' <summary>
    ''' Boolean value indicating expanded state.
    ''' </summary>
    Public Shared Expanded As Integer = 9
  End Class

  ''' <summary>
  ''' Init_s the grid columns class.
  ''' </summary>
  Private Sub Init_GridColumnsClass()
    GridColumns.FirstCol = Me.Grid_Characteristics.Cols("FirstCol").SafeIndex
    GridColumns.ISIN = Me.Grid_Characteristics.Cols("ISIN").SafeIndex
    GridColumns.DataWeighting = Me.Grid_Characteristics.Cols("DataWeighting").SafeIndex
    GridColumns.LastUpdated = Me.Grid_Characteristics.Cols("LastUpdated").SafeIndex
    GridColumns.InstrumentID = Me.Grid_Characteristics.Cols("InstrumentID").SafeIndex
    GridColumns.CategoryID = Me.Grid_Characteristics.Cols("CategoryID").SafeIndex
    GridColumns.CharacteristicID = Me.Grid_Characteristics.Cols("CharacteristicID").SafeIndex
    GridColumns.CharacteristicTypeID = Me.Grid_Characteristics.Cols("CharacteristicType").SafeIndex
    GridColumns.Counter = Me.Grid_Characteristics.Cols("Counter").SafeIndex
    GridColumns.RiskDataID = Me.Grid_Characteristics.Cols("RiskDataID").SafeIndex
    GridColumns.Expanded = Me.Grid_Characteristics.Cols("Expanded").SafeIndex
  End Sub

  ''' <summary>
  ''' Class CharacteristicSummaryClass
  ''' </summary>
  Private Class CharacteristicSummaryClass
    ''' <summary>
    ''' The risk data ID
    ''' </summary>
    Public RiskDataID As Integer
    ''' <summary>
    ''' The data category ID
    ''' </summary>
    Public DataCategoryID As Integer
    ''' <summary>
    ''' The data characteristic ID
    ''' </summary>
    Public DataCharacteristicID As Integer
    ''' <summary>
    ''' The data characteristic type ID
    ''' </summary>
    Public DataCharacteristicTypeID As Integer
    ''' <summary>
    ''' The instrument ID
    ''' </summary>
    Public InstrumentID As Integer
    ''' <summary>
    ''' The instrument description
    ''' </summary>
    Public InstrumentDescription As String
    ''' <summary>
    ''' The instrument ISIN code
    ''' </summary>
    Public InstrumentISIN As String
    ''' <summary>
    ''' The data category
    ''' </summary>
    Public DataCategory As String
    ''' <summary>
    ''' The data characteristic
    ''' </summary>
    Public DataCharacteristic As String
    ''' <summary>
    ''' The data view counter
    ''' </summary>
    Public DataViewCounter As Integer
    ''' <summary>
    ''' The characteristics count
    ''' </summary>
    Public CharacteristicsCount As Integer
    ''' <summary>
    ''' The data weighting
    ''' </summary>
    Public DataWeighting As Double
    ''' <summary>
    ''' The date entered
    ''' </summary>
    Public DateEntered As Date

    ''' <summary>
    ''' Initializes a new instance of the <see cref="CharacteristicSummaryClass"/> class.
    ''' </summary>
    Public Sub New()
      RiskDataID = 0
      DataCategoryID = 0
      DataCharacteristicID = 0
      DataCharacteristicTypeID = RenaissanceGlobals.RiskCharacteristicType.Normal
      InstrumentID = 0
      DataCategory = ""
      DataCharacteristic = ""
      InstrumentDescription = ""
      DataViewCounter = 0
      CharacteristicsCount = 0
      DataWeighting = 0.0
      DateEntered = Renaissance_BaseDate
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="CharacteristicSummaryClass"/> class.
    ''' </summary>
    ''' <param name="pRiskDataID">The p risk data ID.</param>
    ''' <param name="pInstrumentID">The p instrument ID.</param>
    ''' <param name="pDataCategoryID">The p data category ID.</param>
    ''' <param name="pDataCharacteristicID">The p data characteristic ID.</param>
    ''' <param name="pDataCharacteristicTypeID">The p data characteristic type ID.</param>
    ''' <param name="pDataWeighting">The p data weighting.</param>
    Public Sub New(ByVal pRiskDataID As Integer, ByVal pInstrumentID As Integer, ByVal pDataCategoryID As Integer, ByVal pDataCharacteristicID As Integer, ByVal pDataCharacteristicTypeID As Integer, ByVal pDataWeighting As Double)
      ' Used to save updated characteristic values before saving.
      Me.New()

      RiskDataID = pRiskDataID
      DataCategoryID = pDataCategoryID
      DataCharacteristicID = pDataCharacteristicID
      InstrumentID = pInstrumentID
      DataWeighting = pDataWeighting
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="CharacteristicSummaryClass"/> class.
    ''' </summary>
    ''' <param name="pRiskDataID">The p risk data ID.</param>
    ''' <param name="pInstrumentID">The p instrument ID.</param>
    ''' <param name="pInstrumentName">Name of the p instrument.</param>
    ''' <param name="pDataCategoryID">The p data category ID.</param>
    ''' <param name="pCategoryName">Name of the p category.</param>
    ''' <param name="pDataCharacteristicID">The p data characteristic ID.</param>
    ''' <param name="pCharacteristicName">Name of the p characteristic.</param>
    ''' <param name="pDataCharacteristicTypeID">The p data characteristic type ID.</param>
    ''' <param name="pDataWeighting">The p data weighting.</param>
    ''' <param name="pDateEntered">The p date entered.</param>
    Public Sub New(ByVal pRiskDataID As Integer, ByVal pInstrumentID As Integer, ByVal pInstrumentName As String, ByVal pDataCategoryID As Integer, ByVal pCategoryName As String, ByVal pDataCharacteristicID As Integer, ByVal pCharacteristicName As String, ByVal pDataCharacteristicTypeID As Integer, ByVal pDataWeighting As Double, ByVal pDateEntered As Date)
      ' Used in ExpandRow()
      Me.New()

      RiskDataID = pRiskDataID
      DataCategoryID = pDataCategoryID
      DataCategory = pCategoryName
      DataCharacteristicID = pDataCharacteristicID
      DataCharacteristic = pCharacteristicName
      DataCharacteristicTypeID = pDataCharacteristicTypeID
      InstrumentID = pInstrumentID
      InstrumentDescription = pInstrumentName
      DataWeighting = pDataWeighting
      DateEntered = pDateEntered
    End Sub
  End Class

#End Region

#Region " Form 'Properties' "

  ''' <summary>
  ''' Gets the main form.
  ''' </summary>
  ''' <value>The main form.</value>
  Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
    ' Public property to return handle to the 'Main' Venice form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

  ''' <summary>
  ''' Gets or sets a value indicating whether this instance is over cancel button.
  ''' </summary>
  ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
  Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
    ' Public property maintaining a value indicating if the cursor is over the 'Cancel'
    ' Button on this form.
    ' This property is specifically designed for use by the field formating Event functions
    ' In order that they do not impose format restrictions if the user is about to click the 
    ' 'Cancel' button.
    '
    Get
      Return __IsOverCancelButton
    End Get
    Set(ByVal Value As Boolean)
      __IsOverCancelButton = Value
    End Set
  End Property

  ''' <summary>
  ''' Gets a value indicating whether this instance is in paint.
  ''' </summary>
  ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
  Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
    Get
      Return InPaint
    End Get
  End Property

  ''' <summary>
  ''' Gets a value indicating whether [in use].
  ''' </summary>
  ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
    Get
      Return _InUse
    End Get
  End Property

  ''' <summary>
  ''' Gets a value indicating whether [form open failed].
  ''' </summary>
  ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

#End Region

  ''' <summary>
  ''' Initializes a new instance of the <see cref="frmCharacteristics"/> class.
  ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  Public Sub New(ByVal pMainForm As VeniceMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()


    _MainForm = pMainForm
    AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

    _FormOpenFailed = False
    _InUse = True

    ' Default Select and Order fields.

    THIS_FORM_OrderBy = "InstrumentDescription"

    ' Form Permissioning :-

    THIS_FORM_PermissionArea = "frmCharacteristics"
    THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

    ' 'This' form ID

    THIS_FORM_FormID = VeniceFormID.frmCharacteristics

    ' This form's dataset type.

    ThisStandardDataset = RenaissanceStandardDatasets.tblSelectCharacteristics ' This Defines the Form Data !!! 


    ' Data object names standard to this Form type.

    THIS_TABLENAME = ThisStandardDataset.TableName
    THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
    THIS_DATASETNAME = ThisStandardDataset.DatasetName

    THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

    ' Establish / Retrieve data objects for this form.

    myConnection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)
    myAdaptor = MainForm.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, VENICE_CONNECTION, THIS_TABLENAME)
    myDataset = MainForm.Load_Table(ThisStandardDataset, False)
    myTable = myDataset.Tables(0)

    ' Report
    SetTransactionReportMenu(RootMenu)

    ' Grid :- Grid_Characteristics

    Grid_Characteristics.Rows.Count = 1
    Grid_Characteristics.Cols.Fixed = 0
    '    Grid_Characteristics.ExtendLastCol = True

    'styles
    Dim cs As CellStyle = Grid_Characteristics.Styles.Normal
    cs.Border.Direction = BorderDirEnum.Vertical
    cs.WordWrap = False

    cs = Grid_Characteristics.Styles.Add("Data")
    cs.BackColor = Color.FromArgb(233, 233, 245)
    cs.ForeColor = Color.Black

    cs = Grid_Characteristics.Styles.Add("DataNegative")
    cs.BackColor = Color.FromArgb(245, 233, 233)
    cs.ForeColor = Color.Red

    cs = Grid_Characteristics.Styles.Add("Data_Default")
    cs.BackColor = Color.FromArgb(210, 210, 245)
    cs.ForeColor = Color.Black

    cs = Grid_Characteristics.Styles.Add("DataNegative")
    cs.BackColor = Color.FromArgb(245, 210, 210)
    cs.ForeColor = Color.Red

    cs = Grid_Characteristics.Styles.Add("SourceNode")
    cs.BackColor = Color.Yellow
    cs.Font = New Font(Grid_Characteristics.Font, FontStyle.Bold)

    cs = Grid_Characteristics.Styles.Add("UpdatedWeightPositive")
    cs.ForeColor = Color.Black
    cs.Font = New Font(Grid_Characteristics.Font, FontStyle.Bold)

    cs = Grid_Characteristics.Styles.Add("UpdatedWeightNegative")
    cs.ForeColor = Color.Red
    cs.Font = New Font(Grid_Characteristics.Font, FontStyle.Bold)

    'outline tree
    Grid_Characteristics.Tree.Column = 0
    Grid_Characteristics.Tree.Style = TreeStyleFlags.Simple
    Grid_Characteristics.AllowMerging = AllowMergingEnum.Nodes

    'other
    Grid_Characteristics.AllowResizing = AllowResizingEnum.Columns
    Grid_Characteristics.SelectionMode = SelectionModeEnum.Cell

    Call Init_GridColumnsClass()

    ' Establish initial DataView and Initialise Characteristics Grid.

    Try
      myDataView = New DataView(myTable, "True", "", DataViewRowState.CurrentRows)
      myDataView.Sort = "InstrumentID, DataCategoryId, DataCharacteristicId"
    Catch ex As Exception

    End Try

    ' Form Control Changed events
    AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

    AddHandler Combo_Category.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Characteristic.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Instrument.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_SelectByFund.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_UpdatedDateOperator.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Date_UpdatedDate.ValueChanged, AddressOf Me.FormControlChanged

    AddHandler Check_EnteredCharacteristics.CheckedChanged, AddressOf Me.FormControlChanged
    AddHandler Check_CountLimit.CheckedChanged, AddressOf Me.FormControlChanged
    AddHandler Numeric_CountLimit.ValueChanged, AddressOf Me.FormControlChanged

    AddHandler Combo_Category.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_Characteristic.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_Instrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_SelectByFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_UpdatedDateOperator.KeyUp, AddressOf MainForm.ComboSelectAsYouType

    AddHandler Combo_Category.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_Characteristic.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_Instrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_SelectByFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_UpdatedDateOperator.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

    AddHandler Combo_Category.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_Characteristic.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_Instrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_SelectByFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_UpdatedDateOperator.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

    AddHandler Combo_Category.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_Characteristic.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_Instrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_SelectByFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_UpdatedDateOperator.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

    ' Set up the ToolTip
    MainForm.SetFormToolTip(Me, FormTooltip)

  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
  ''' <summary>
  ''' Resets the form.
  ''' </summary>
  Public Sub ResetForm() Implements StandardVeniceForm.ResetForm
    THIS_FORM_OrderBy = "TransactionFund, TransactionInstrument"

    Call Form_Load(Me, New System.EventArgs)
  End Sub

  ''' <summary>
  ''' Closes the form.
  ''' </summary>
  Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
    ALWAYS_CLOSE_THIS_FORM = True
    Me.Close()
  End Sub

  ''' <summary>
  ''' Handles the Load event of the Form control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    ' *************************************************************
    '
    '
    ' *************************************************************

    Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
    _FormOpenFailed = False
    _InUse = True

    ' Initialise Data structures. Connection, Adaptor and Dataset.

    If Not (MainForm Is Nothing) Then
      FormIsValid = True
    Else
      MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (myConnection Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (myAdaptor Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (myDataset Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If


    ' Check User permissions
    Try
      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

        FormIsValid = False
        _FormOpenFailed = True

        Exit Sub
      End If
    Catch ex As Exception
      FormIsValid = False
      _FormOpenFailed = True

      Exit Sub
    End Try

    ' Initialse form

    InPaint = True
    IsOverCancelButton = False

    ' Initialise main select controls
    ' Build Sorted data list from which this form operates

    Try
      UpdatedCategoryEntries.Clear()
      ExpandedNodes.Clear()
      LastGridScroll = New System.Drawing.Point(0, 0)

      Call SetInstrumentCombo()
      Call SetFundCombo()
      Call SetCategoryCombo()
      Call SetCharacteristicCombo()

      Me.Combo_Category.SelectedIndex = -1
      Me.Combo_Instrument.SelectedIndex = -1
      Me.Combo_SelectByFund.SelectedIndex = -1
      Me.Combo_Characteristic.SelectedIndex = -1
      Me.Combo_UpdatedDateOperator.SelectedIndex = 0
      Me.Date_UpdatedDate.Value = Renaissance_BaseDate

      Me.Check_EnteredCharacteristics.Checked = False
      Me.Check_CountLimit.Checked = False
      Me.Numeric_CountLimit.Value = 0

      Dim NewStyle As C1.Win.C1FlexGrid.CellStyle
      NewStyle = Grid_Characteristics.Styles.Add("Weight_IntegerFormatLeft", Grid_Characteristics.GetCellStyle(0, GridColumns.DataWeighting))
      NewStyle.Format = "#,##0"
      NewStyle.Font = New Font(NewStyle.Font, FontStyle.Regular)
      NewStyle.ForeColor = Color.Navy
      NewStyle.TextAlign = TextAlignEnum.LeftCenter

      NewStyle = Grid_Characteristics.Styles.Add("Weight_IntegerFormatMiddle", Grid_Characteristics.GetCellStyle(0, GridColumns.DataWeighting))
      NewStyle.Format = "#,##0"
      NewStyle.ForeColor = Color.CornflowerBlue
      NewStyle.TextAlign = TextAlignEnum.LeftCenter

      NewStyle = Grid_Characteristics.Styles.Add("Weight_CompoundCharacteristic", Grid_Characteristics.GetCellStyle(0, GridColumns.DataWeighting))
      NewStyle.ForeColor = Color.Navy

      Call SetSortedRows()

      Call MainForm.SetComboSelectionLengths(Me)
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error initialising Set Characteristics Form.", ex.StackTrace, True)
    End Try

    InPaint = False

  End Sub

  ''' <summary>
  ''' Handles the Closing event of the frm control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
  Private Sub frm_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    Dim HideForm As Boolean

    ' Hide or Close this form ?
    ' All depends on how many of this form type are Open or in Cache...

    _InUse = False

    If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
      HideForm = False
    Else
      HideForm = True
      If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
        HideForm = False
      End If
    End If

    If HideForm = True Then
      MainForm.HideInFormsCollection(Me)
      Me.Hide() ' NPP Fix

      e.Cancel = True
    Else
      Try
        MainForm.RemoveFromFormsCollection(Me)
        RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

        RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

        RemoveHandler Combo_Category.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_Instrument.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_SelectByFund.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_UpdatedDateOperator.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Date_UpdatedDate.ValueChanged, AddressOf Me.FormControlChanged

        RemoveHandler Check_EnteredCharacteristics.CheckedChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_CountLimit.CheckedChanged, AddressOf Me.FormControlChanged
        RemoveHandler Numeric_CountLimit.ValueChanged, AddressOf Me.FormControlChanged

        RemoveHandler Combo_Category.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_Instrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_SelectByFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_UpdatedDateOperator.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        RemoveHandler Combo_Category.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_Instrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_SelectByFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_UpdatedDateOperator.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

        RemoveHandler Combo_Category.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_Instrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_SelectByFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_UpdatedDateOperator.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

        RemoveHandler Combo_Category.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_Instrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_SelectByFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_UpdatedDateOperator.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

      Catch ex As Exception
      End Try
    End If

  End Sub

  ''' <summary>
  ''' Handles the CheckedChanged event of the Check_EnteredCharacteristics control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Check_EnteredCharacteristics_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_EnteredCharacteristics.CheckedChanged
    ' *************************************************************
    '
    '
    ' *************************************************************

    Try
      Call SetSortedRows(True)
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Check_EnteredCharacteristics_CheckedChanged()", ex.StackTrace, True)
    End Try

  End Sub

  ''' <summary>
  ''' Handles the CheckedChanged event of the Check_CountLimit control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Check_CountLimit_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_CountLimit.CheckedChanged
    ' *************************************************************
    '
    '
    ' *************************************************************

    Try
      Numeric_CountLimit.Enabled = Check_CountLimit.Checked
      Call SetSortedRows(True)
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Check_CountLimit_CheckedChanged()", ex.StackTrace, True)
    End Try

  End Sub

  ''' <summary>
  ''' Handles the ValueChanged event of the Numeric_CountLimit control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Numeric_CountLimit_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Numeric_CountLimit.ValueChanged
    ' *************************************************************
    '
    '
    ' *************************************************************

    Try
      If (Numeric_CountLimit.Enabled) Then
        Call SetSortedRows(True)
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Numeric_CountLimit_ValueChanged()", ex.StackTrace, True)
    End Try

  End Sub

#End Region



  ' Routine to handle changes / updates to tables by this and other windows.
  ' If this, or any other, form posts a change to a table, then it will invoke an update event 
  ' detailing what tables have been altered.
  ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
  ' the 'VeniceAutoUpdate' event of the main Venice form.
  ' Each form may them react as appropriate to changes in any table that might impact it.
  '
  ''' <summary>
  ''' Autoes the update.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    Dim OrgInPaint As Boolean
    Dim KnowledgeDateChanged As Boolean
    Dim RefreshGrid As Boolean

    If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

    OrgInPaint = InPaint

    Try
      InPaint = True
      KnowledgeDateChanged = False
      RefreshGrid = False

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
        KnowledgeDateChanged = True
      End If
    Catch ex As Exception
    End Try

    ' ****************************************************************
    ' Check for changes relevant to this form
    ' ****************************************************************

    ' Changes to the tblRiskDataCharacteristic table :-

    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblRiskDataCharacteristic) = True) Or KnowledgeDateChanged Then
        Call SetCharacteristicCombo()
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblRiskDataCharacteristic", ex.StackTrace, True)
    End Try

    ' Changes to the tblRiskDataCategory table :-

    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblRiskDataCategory) = True) Or KnowledgeDateChanged Then
        Call SetCategoryCombo()
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblRiskDataCategory", ex.StackTrace, True)
    End Try

    ' Changes to the tblInstrument table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrument) = True) Or KnowledgeDateChanged Then
        Call SetInstrumentCombo()
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblInstrument", ex.StackTrace, True)
    End Try

    ' Changes to the tblFund table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFund) = True) Or KnowledgeDateChanged Then
        Call SetFundCombo()
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblInstrument", ex.StackTrace, True)
    End Try

    ' Changes to the tblTransaction table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblTransaction) = True) Or KnowledgeDateChanged Then
        FundHoldings.Clear()
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblTransaction", ex.StackTrace, True)
    End Try

    ' Changes to the KnowledgeDate :-

    Try
      If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
        RefreshGrid = True
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: KnowledgeDate", ex.StackTrace, True)
    End Try

    ' Changes to the tblUserPermissions table :-

    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

        ' Check ongoing permissions.

        Call CheckPermissions()
        If (HasReadPermission = False) Then
          Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

          FormIsValid = False
          InPaint = OrgInPaint
          Me.Close()
          Exit Sub
        End If

        RefreshGrid = True

      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: tblUserPermissions", ex.StackTrace, True)
    End Try


    ' ****************************************************************
    ' Changes to the Main FORM table :-
    ' 
    ' ****************************************************************

    Try
      If (e.TableChanged(THIS_FORM_ChangeID) = True) Or _
       (RefreshGrid = True) Or _
       KnowledgeDateChanged Then

        ' Re-Set Controls etc.
        Call SetSortedRows(True)

      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: Main Form table", ex.StackTrace, True)
    End Try

    InPaint = OrgInPaint

  End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

  ''' <summary>
  ''' Gets the characteristics select string.
  ''' </summary>
  ''' <returns>System.String.</returns>
  Private Function GetCharacteristicsSelectString() As String
    ' *******************************************************************************
    ' Build a Select String appropriate to the form status.
    ' *******************************************************************************

    Dim SelectString As String
    Dim FieldSelectString As String

    SelectString = ""
    FieldSelectString = ""
    MainForm.SetToolStripText(Label_Status, "")

    If (myDataset Is Nothing) Then
      Return "True"
      Exit Function
    End If

    Try

      ' Select on Category...
      If (Me.Combo_Category.SelectedIndex > 0) Then
        SelectString = "(DataCategoryId=" & Combo_Category.SelectedValue.ToString & ")"
      End If

      ' Select on Combo_Characteristic

      If (Me.Combo_Characteristic.SelectedIndex > 0) Then
        If (SelectString.Length > 0) Then
          SelectString &= " AND "
        End If
        SelectString &= "(DataCharacteristicId=" & Combo_Characteristic.SelectedValue.ToString & ")"
      End If

      ' Select on Instrument

      If (Me.Combo_Instrument.SelectedIndex > 0) Then
        If (SelectString.Length > 0) Then
          SelectString &= " AND "
        End If
        SelectString &= "(InstrumentID=" & Combo_Instrument.SelectedValue.ToString & ")"
      End If

      ' Select on Fund

      If (Me.Combo_SelectByFund.SelectedIndex > 0) Then
        ' Add a nonsense condition to the select string, just so it changes.

        If (SelectString.Length > 0) Then
          SelectString &= " AND "
        End If
        SelectString &= "(" & Combo_SelectByFund.SelectedValue.ToString & ">=0)"

        ' Populate Fund Instruments list if necessary.

        If Not FundHoldings.ContainsKey(CInt(Me.Combo_SelectByFund.SelectedValue)) Then
          Try

            Dim QueryCommand As New SqlCommand
            Dim ResultsTable As New DataTable
            Dim thisRow As DataRow

            Try
              QueryCommand.CommandType = CommandType.Text
              QueryCommand.CommandText = "SELECT Instrument from [dbo].[fn_ActiveFundInstruments](@FundID, null, @KnowledgeDate)"
              QueryCommand.Parameters.Add("@FundID", SqlDbType.Int).Value = CInt(Me.Combo_SelectByFund.SelectedValue)
              QueryCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime).Value = MainForm.Main_Knowledgedate
              QueryCommand.Connection = MainForm.GetVeniceConnection

              SyncLock QueryCommand.Connection
                MainForm.LoadTable_Custom(ResultsTable, QueryCommand)
                'ResultsTable.Load(QueryCommand.ExecuteReader)
              End SyncLock

              If ResultsTable.Rows.Count > 0 Then
                Dim tempArray As SortedList = New SortedList(ResultsTable.Rows.Count)

                For Each thisRow In ResultsTable.Rows
                  tempArray.Add(CInt(thisRow(0)), True)
                Next

                FundHoldings(CInt(Me.Combo_SelectByFund.SelectedValue)) = tempArray
              Else
                ' Empty list.
                FundHoldings(CInt(Me.Combo_SelectByFund.SelectedValue)) = New SortedList
              End If

            Catch ex As Exception
              MainForm.LogError("GetCharacteristicsSelectString()", LOG_LEVELS.Error, ex.Message, "Error getting Fund positions.", ex.StackTrace)
            Finally
              If (QueryCommand.Connection IsNot Nothing) Then
                QueryCommand.Connection.Close()
              End If
            End Try

          Catch ex As Exception

          End Try

        End If


      End If

      ' Select on Entered Date

      If (Me.Combo_UpdatedDateOperator.SelectedIndex > 0) Then
        If (SelectString.Length > 0) Then
          SelectString &= " AND "
        End If

        SelectString &= "(DateEntered " & Combo_UpdatedDateOperator.SelectedItem.ToString & " #" & Me.Date_UpdatedDate.Value.ToString(QUERY_SHORTDATEFORMAT) & "#)"
      End If

      ' Check for the 'Incomplete Transactions' option.

      If Me.Check_EnteredCharacteristics.Checked = True Then
        If SelectString.Length <= 0 Then
          SelectString = ENTERED_CHARACTERISTICS_QUERY
        Else
          SelectString = "(" & SelectString & ") AND (" & ENTERED_CHARACTERISTICS_QUERY & ")"
        End If
      End If

      If SelectString.Length <= 0 Then
        SelectString = "true"
      End If

    Catch ex As Exception
      SelectString = "true"
    End Try

    Return SelectString

  End Function

  ''' <summary>
  ''' Sets the sorted rows.
  ''' </summary>
  ''' <param name="ForceRefresh">if set to <c>true</c> [force refresh].</param>
  Private Sub SetSortedRows(Optional ByVal ForceRefresh As Boolean = False)
    ' *******************************************************************************
    ' Build a Select String appropriate to the form status and apply it to the DataView object.
    ' *******************************************************************************

    Dim SelectString As String

    SelectString = GetCharacteristicsSelectString()

    If (myDataView Is Nothing) Then
      Exit Sub
    End If

    Try
      If (Not (myDataView.RowFilter = SelectString)) Or (ForceRefresh) Then
        If ForceRefresh Then
          myDataView.RowFilter = "False"
        End If

        myDataView.RowFilter = SelectString
        PaintCharacteristicsGrid()

      End If
    Catch ex As Exception
      SelectString = "true"
      myDataView.RowFilter = SelectString
    End Try

    Me.Label_Status.Text = "(" & myDataView.Count.ToString & " Records) " & SelectString

  End Sub


  ' Check User permissions
  ''' <summary>
  ''' Checks the permissions.
  ''' </summary>
  Private Sub CheckPermissions()
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

  End Sub

  ''' <summary>
  ''' Basic event handler, called when a form value is changed.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' *******************************************************************************
    ' In the event of one of the Form Controls changing, refresh the data grid.
    ' *******************************************************************************

    If InPaint = False Then
      LastGridScroll = New System.Drawing.Point(0, 0)

      Call SetSortedRows()

    End If
  End Sub

  ''' <summary>
  ''' Handles the KeyUp event of the Combo_FieldSelect control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
  Private Sub Combo_FieldSelect_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
    ' *******************************************************************************
    ' In the event of one of the Form Controls changing, refresh the data grid.
    ' *******************************************************************************

    If InPaint = False Then

      Call SetSortedRows()

    End If
  End Sub


  ''' <summary>
  ''' Updates the selected value combo.
  ''' </summary>
  ''' <param name="pFieldName">Name of the p field.</param>
  ''' <param name="pValueCombo">The p value combo.</param>
  Private Sub UpdateSelectedValueCombo(ByVal pFieldName As String, ByRef pValueCombo As ComboBox)
    ' *******************************************************************************
    ' Build a Value Combo Box appropriate to the chosen DataTable Select Field.
    '
    ' By default the combo will contain all existing values for the chosen field, however
    ' If a relationship is defined for thi table and field, then a Combo will be built
    ' which reflects this relationship.
    ' e.g. the tblReferentialIntegrity table defined a relationship between the
    ' TransactionCounterparty field and the tblCounterparty.Counterparty field, thus if
    ' the chosen Select field ti 'Counterparty.' then the Value combo will be built using
    ' the CounterpartyID & CounterpartyName details from the tblCounterparty table.
    '
    ' *******************************************************************************

    Dim tblRefIntegrity As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityDataTable
    Dim IntegrityRows() As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityRow
    Dim TransactionDataset As DataSet
    Dim TransactionTable As DataTable
    Dim SortOrder As Boolean

    Dim Org_InPaint As Boolean = InPaint

    Try
      InPaint = True

      SortOrder = True

      ' Clear the Value Combo.

      Try
        pValueCombo.DataSource = Nothing
        pValueCombo.DisplayMember = ""
        pValueCombo.ValueMember = ""
        pValueCombo.Items.Clear()
      Catch ex As Exception
      End Try

      ' By default, make the Combo a Standard Dropdown, so that non-list items can be entered.

      pValueCombo.DropDownStyle = ComboBoxStyle.DropDown

      ' Exit if no FiledName is given (having cleared the combo).

      If (pFieldName.Length <= 0) Then Exit Sub

      ' Adjust the Field name if appropriate.

      If (pFieldName.EndsWith(".")) Then
        pFieldName = "Transaction" & pFieldName.Substring(0, pFieldName.Length - 1)
      End If

      ' Get a handle to the Standard Characteristics Table.
      ' This is so that the field types can be determined and used.

      Try
        TransactionDataset = MainForm.Load_Table(ThisStandardDataset, False)
        TransactionTable = myDataset.Tables(0)
      Catch ex As Exception
        Exit Sub
      End Try

      ' If the selected field is a Date then sort the selection combo in reverse order.

      If (TransactionTable.Columns(pFieldName).DataType Is GetType(System.DateTime)) Then
        SortOrder = False
      End If

      ' Get a handle to the Referential Integrity table and the row matching this table and field.
      ' this table defines a relationship between Transaction Table fields and other tables.
      ' This information can be used to build more meaningfull Value Combos.

      Try
        tblRefIntegrity = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblReferentialIntegrity, False), RenaissanceDataClass.DSReferentialIntegrity).tblReferentialIntegrity
        IntegrityRows = tblRefIntegrity.Select("(FeedsTable LIKE 'tblSelectCharacteristics') AND (FeedsField = '" & pFieldName & "')", "RN")
        If (IntegrityRows.Length <= 0) Then
          GoTo StandardExit
        End If

      Catch ex As Exception
        Exit Sub
      End Try

      If (IntegrityRows(0).IsDescriptionFieldNull) Then
        ' No Linked Description Field

        GoTo StandardExit
      End If

      ' OK, a referential record exists.
      ' Determine the Table and Field Names to use to build the Value Combo.

      Dim TableName As String
      Dim TableField As String
      Dim DescriptionField As String
      Dim stdDS As StandardDataset
      Dim thisChangeID As RenaissanceChangeID

      Try

        TableName = IntegrityRows(0).TableName
        TableField = IntegrityRows(0).TableField
        DescriptionField = IntegrityRows(0).DescriptionField

        thisChangeID = CType(System.Enum.Parse(GetType(RenaissanceChangeID), TableName), RenaissanceChangeID)

        stdDS = RenaissanceStandardDatasets.GetStandardDataset(thisChangeID)

        ' Build the appropriate Values Combo.

        Call MainForm.SetTblGenericCombo( _
        pValueCombo, _
        stdDS, _
        DescriptionField, _
        TableField, _
        "", False, SortOrder, True)    ' 

        ' For Referential Integrity generated combo's, make the Combo a 
        ' DropDownList

        pValueCombo.DropDownStyle = ComboBoxStyle.DropDownList

      Catch ex As Exception
        GoTo StandardExit

      End Try

      Exit Sub

StandardExit:

      ' Build the standard Combo, just use the values from the given field.

      Call MainForm.SetTblGenericCombo( _
      pValueCombo, _
      RenaissanceStandardDatasets.tblTransaction, _
      pFieldName, _
      pFieldName, _
      "", True, SortOrder)   ' 

    Catch ex As Exception
    Finally
      InPaint = Org_InPaint
    End Try

  End Sub

  ''' <summary>
  ''' Gets the field change ID.
  ''' </summary>
  ''' <param name="pFieldName">Name of the p field.</param>
  ''' <returns>RenaissanceChangeID.</returns>
  Private Function GetFieldChangeID(ByVal pFieldName As String) As RenaissanceChangeID
    Dim tblRefIntegrity As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityDataTable
    Dim IntegrityRows() As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityRow

    If (pFieldName.EndsWith(".")) Then
      pFieldName = "Transaction" & pFieldName.Substring(0, pFieldName.Length - 1)
    End If

    Try
      tblRefIntegrity = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblReferentialIntegrity, False), RenaissanceDataClass.DSReferentialIntegrity).tblReferentialIntegrity
      IntegrityRows = tblRefIntegrity.Select("(FeedsTable LIKE '%tblTransaction') AND (FeedsField = '" & pFieldName & "')", "RN")
      If (IntegrityRows.Length <= 0) Then
        Return RenaissanceChangeID.None
        Exit Function
      End If

    Catch ex As Exception
      Return RenaissanceChangeID.None
      Exit Function
    End Try


    If (IntegrityRows(0).IsDescriptionFieldNull) Then
      Return RenaissanceChangeID.None
      Exit Function
    End If

    ' OK, a referential record exists.
    ' Determine the Table and Field Names to use to build the Value Combo.

    Dim TableName As String
    Dim TableField As String
    Dim DescriptionField As String
    Dim thisChangeID As RenaissanceChangeID

    Try

      TableName = IntegrityRows(0).TableName
      TableField = IntegrityRows(0).TableField
      DescriptionField = IntegrityRows(0).DescriptionField

      thisChangeID = CType(System.Enum.Parse(GetType(RenaissanceChangeID), TableName), RenaissanceChangeID)

      Return thisChangeID
      Exit Function
    Catch ex As Exception
    End Try

    Return RenaissanceChangeID.None
    Exit Function

  End Function

  ''' <summary>
  ''' Handles the BeforeEdit event of the Grid_Characteristics control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="C1.Win.C1FlexGrid.RowColEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Characteristics_BeforeEdit(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles Grid_Characteristics.BeforeEdit
    ' ***************************************************************************************
    '
    '
    ' ***************************************************************************************

    Try
      Dim thisGridRow As C1.Win.C1FlexGrid.Row
      thisGridRow = Grid_Characteristics.Rows(e.Row)

      ' Don't Edit node lines
      If (thisGridRow.IsNode) Then
        e.Cancel = True
        Exit Sub
      End If

      If (CInt(thisGridRow(GridColumns.CharacteristicTypeID)) <> RenaissanceGlobals.RiskCharacteristicType.Normal) Then
        e.Cancel = True
        Exit Sub
      End If

    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Handles the AfterEdit event of the Grid_Characteristics control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="C1.Win.C1FlexGrid.RowColEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Characteristics_AfterEdit(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles Grid_Characteristics.AfterEdit
    ' ***************************************************************************************
    '
    '
    ' ***************************************************************************************
    Me.btnSave.Visible = True
    Me.btnCancel.Visible = True

    Me.Context_SaveChanges.Visible = True
    Me.Context_CancelChanges.Visible = True
    Me.Context_SeparatorSave.Visible = True

    Try
      Dim Counter As Integer
      Dim thisCategory As CharacteristicSummaryClass = Nothing

      Dim thisGridRow As C1.Win.C1FlexGrid.Row

      thisGridRow = Grid_Characteristics.Rows(e.Row)

      Dim thisRiskDataID As Integer = CInt(thisGridRow(GridColumns.RiskDataID))
      Dim thisInstrumentID As Integer = CInt(thisGridRow(GridColumns.InstrumentID))
      Dim thisCatagoryID As Integer = CInt(thisGridRow(GridColumns.CategoryID))
      Dim thisCharacteristicID As Integer = CInt(thisGridRow(GridColumns.CharacteristicID))
      Dim thisCharacteristicTypeID As Integer = CInt(thisGridRow(GridColumns.CharacteristicTypeID))
      Dim thisDataWeighting As Double = CDbl(thisGridRow(GridColumns.DataWeighting))

      If (thisDataWeighting >= 0) Then
        Grid_Characteristics.SetCellStyle(e.Row, GridColumns.DataWeighting, Grid_Characteristics.Styles("UpdatedWeightPositive"))
      Else
        Grid_Characteristics.SetCellStyle(e.Row, GridColumns.DataWeighting, Grid_Characteristics.Styles("UpdatedWeightNegative"))
      End If

      ' Has this Characteristic been changed before ?

      If UpdatedCategoryEntries.Count > 0 Then

        For Counter = 0 To (UpdatedCategoryEntries.Count - 1)
          thisCategory = UpdatedCategoryEntries(Counter)  '  CharacteristicSummaryClass

          If (thisCategory.InstrumentID = thisInstrumentID) And (thisCategory.DataCategoryID = thisCatagoryID) And (thisCategory.DataCharacteristicID = thisCharacteristicID) Then
            thisCategory.DataWeighting = thisDataWeighting
            UpdatedCategoryEntries(Counter) = thisCategory

            Exit Sub
          End If

        Next Counter

      End If

      ' Add new Characteristic

      UpdatedCategoryEntries.Add(New CharacteristicSummaryClass(thisRiskDataID, thisInstrumentID, thisCatagoryID, thisCharacteristicID, thisCharacteristicTypeID, thisDataWeighting))

    Catch ex As Exception
    End Try

  End Sub


  ''' <summary>
  ''' Handles the DoubleClick event of the Grid_Characteristics control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Characteristics_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Grid_Characteristics.DoubleClick

    ' ***************************************************************************************
    ' Expand if DoubleClick on a Node
    '
    ' What to do if double click on a characteristic ?
    ' ***************************************************************************************

    Try
      If (Grid_Characteristics.RowSel > 0) AndAlso (Grid_Characteristics.Rows(Grid_Characteristics.RowSel).IsNode) Then
        Grid_Characteristics.Rows(Grid_Characteristics.RowSel).Node.Expanded = (Not Grid_Characteristics.Rows(Grid_Characteristics.RowSel).Node.Expanded)
        Exit Sub
      End If
    Catch ex As Exception
    End Try

    'Try
    '  Dim ParentID As Integer
    '  Dim thisFormHandle As RenaissanceGlobals.RenaissanceFormHandle
    '
    '  ParentID = Me.Grid_Characteristics.Rows(Grid_Characteristics.RowSel)("ParentID")

    '  If (ParentID > 0) Then
    '    Grid_Characteristics.Cursor = Cursors.WaitCursor

    '    thisFormHandle = MainForm.New_VeniceForm(VeniceFormID.frmTransaction)
    '    CType(thisFormHandle.Form, frmTransaction).FormSelectCriteria = GetTransactionSelectString(True)
    '    CType(thisFormHandle.Form, frmTransaction).MoveToAuditID(ParentID)
    '  End If

    'Catch ex As Exception
    'Finally
    '  Grid_Characteristics.Cursor = Cursors.Default
    'End Try

  End Sub

#End Region

#Region " Set Form Combos (Form Specific Code) "

  ''' <summary>
  ''' Sets the instrument combo.
  ''' </summary>
  Private Sub SetInstrumentCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_Instrument, _
    RenaissanceStandardDatasets.tblInstrument, _
    "InstrumentDescription", _
    "InstrumentID", _
    "", False, True, True)    ' 

  End Sub

  ''' <summary>
  ''' Sets the fund combo.
  ''' </summary>
  Private Sub SetFundCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_SelectByFund, _
    RenaissanceStandardDatasets.tblFund, _
    "FundName", _
    "FundID", _
    "FundClosed=0", False, True, True)    ' 

  End Sub

  ''' <summary>
  ''' Sets the category combo.
  ''' </summary>
  Private Sub SetCategoryCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_Category, _
    RenaissanceStandardDatasets.tblRiskDataCategory, _
    "DataCategory", _
    "DataCategoryId", _
    "", False, True, True)    ' 

  End Sub


  ''' <summary>
  ''' Sets the characteristic combo.
  ''' </summary>
  Private Sub SetCharacteristicCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_Characteristic, _
    RenaissanceStandardDatasets.tblRiskDataCharacteristic, _
    "DataCharacteristic", _
    "DataCharacteristicId", _
    "", False, True, True)    ' 

  End Sub

#End Region

#Region " Transaction report Menu"

  ''' <summary>
  ''' Sets the transaction report menu.
  ''' </summary>
  ''' <param name="RootMenu">The root menu.</param>
  ''' <returns>MenuStrip.</returns>
  Private Function SetTransactionReportMenu(ByRef RootMenu As MenuStrip) As MenuStrip

    Dim ReportMenu As New ToolStripMenuItem("&Reports")
    'Dim newMenuItem As ToolStripMenuItem

    'newMenuItem = ReportMenu.DropDownItems.Add("Transaction &Ticket", Nothing, AddressOf Me.rptTransactionTicket)

    ReportMenu.DropDownItems.Add(New ToolStripSeparator)

    'newMenuItem = ReportMenu.DropDownItems.Add("Transaction By &Value Date", Nothing, AddressOf Me.rptTransactionByDate)
    'newMenuItem = ReportMenu.DropDownItems.Add("Transaction By &Entry Date", Nothing, AddressOf Me.rptTransactionByEntryDate)
    'newMenuItem = ReportMenu.DropDownItems.Add("Transaction By &Group", Nothing, AddressOf Me.rptTransactionByGroup)
    'newMenuItem = ReportMenu.DropDownItems.Add("Transaction By &Instrument", Nothing, AddressOf Me.rptTransactionByStock)
    'newMenuItem = ReportMenu.DropDownItems.Add("&Incomplete Transaction Report", Nothing, AddressOf Me.rptIncompleteTransactions)
    'newMenuItem = ReportMenu.DropDownItems.Add("Transaction &Price Differences", Nothing, AddressOf Me.rptTransactionPriceDifference)
    'newMenuItem = ReportMenu.DropDownItems.Add("&Instrument Position Report", Nothing, AddressOf Me.rptInstrumentPositionsReport)

    'ReportMenu.DropDownItems.Add(New ToolStripSeparator)

    'newMenuItem = ReportMenu.DropDownItems.Add("Trade &Blotter", Nothing, AddressOf Me.rptTradeBlotter)
    'newMenuItem = ReportMenu.DropDownItems.Add("&Final Trade Blotter", Nothing, AddressOf Me.rptFinalTradeBlotter)
    'newMenuItem = ReportMenu.DropDownItems.Add("Trade &Revision Blotter", Nothing, AddressOf Me.rptTradeRevisionBlotter)
    'newMenuItem = ReportMenu.DropDownItems.Add("Final Trade &Revision Blotter", Nothing, AddressOf Me.rptFinalTradeRevisionBlotter)

    ReportMenu.DropDownItems.Add(New ToolStripSeparator)


    RootMenu.Items.Add(ReportMenu)
    Return RootMenu

  End Function


#End Region

#Region " Bug hunting "

  ' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
  ' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
  ' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

  ''' <summary>
  ''' Called when [row updating].
  ''' </summary>
  ''' <param name="Sender">The sender.</param>
  ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  ''' <summary>
  ''' Called when [row updated].
  ''' </summary>
  ''' <param name="Sender">The sender.</param>
  ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  ''' <summary>
  ''' Called when [row fill error].
  ''' </summary>
  ''' <param name="Sender">The sender.</param>
  ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
    Dim a As Integer

    a = 1
  End Sub

#End Region

  ''' <summary>
  ''' Class CategoryListComparerClass
  ''' </summary>
  Private Class CategoryListComparerClass
    Implements IComparer

    ''' <summary>
    ''' Compares two objects and returns a value indicating whether one is less than, equal to, or greater than the other.
    ''' </summary>
    ''' <param name="x">The first object to compare.</param>
    ''' <param name="y">The second object to compare.</param>
    ''' <returns>Value Condition Less than zero <paramref name="x" /> is less than <paramref name="y" />. Zero <paramref name="x" /> equals <paramref name="y" />. Greater than zero <paramref name="x" /> is greater than <paramref name="y" />.</returns>
    Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare
      ' ************************************************************************
      ' Implement class to compare two CategoryList items.
      ' Nothing is deemed to be less than something.
      '
      ' ************************************************************************

      Try

        If (TypeOf x Is CharacteristicSummaryClass) AndAlso (TypeOf y Is CharacteristicSummaryClass) Then
          Dim xCategory As CharacteristicSummaryClass = x
          Dim yCategory As CharacteristicSummaryClass = y

          If (x Is Nothing) AndAlso (y Is Nothing) Then
            Return 0

          ElseIf (x Is Nothing) Then
            Return (-1)

          ElseIf (y Is Nothing) Then
            Return (1)

          Else
            ' Compare
            Dim RVal As Integer = 0

            RVal = CType(xCategory.InstrumentDescription, IComparable).CompareTo(yCategory.InstrumentDescription)

            If RVal <> 0 Then
              Return RVal
            End If

            RVal = CType(xCategory.InstrumentID, IComparable).CompareTo(yCategory.InstrumentID)

            If RVal <> 0 Then
              Return RVal
            End If

            RVal = CType(xCategory.DataCategory, IComparable).CompareTo(yCategory.DataCategory)

            If RVal <> 0 Then
              Return RVal
            End If

            RVal = CType(xCategory.DataCategoryID, IComparable).CompareTo(yCategory.DataCategoryID)

            If RVal <> 0 Then
              Return RVal
            End If

            RVal = CType(xCategory.DataCharacteristic, IComparable).CompareTo(yCategory.DataCharacteristic)

            If RVal <> 0 Then
              Return RVal
            End If

            RVal = CType(xCategory.DataCharacteristicID, IComparable).CompareTo(yCategory.DataCharacteristicID)

            Return RVal

          End If

        Else

          Return 0

        End If

      Catch ex As Exception
      End Try

      Return 0

    End Function

  End Class

  ''' <summary>
  ''' Paints the characteristics grid.
  ''' </summary>
  Private Sub PaintCharacteristicsGrid()
    ' **********************************************************************************
    ' Routine to Paint (In Outline) the Characteristics Grid.
    '
    ' This Routine populates the grid with the appropriate heirarchy of Funds and Instruments.
    ' To improve performance, the Transaction lines are not added, they are in-filled as 
    ' Instrument lines are expanded.
    '
    ' **********************************************************************************

    Dim Counter As Integer
    Dim thisRowView As DataRowView
    Dim thisRow As RenaissanceDataClass.DSSelectCharacteristics.tblSelectCharacteristicsRow
    Dim NewGridRow As C1.Win.C1FlexGrid.Row = Nothing
    Dim InstrumentGridRow As C1.Win.C1FlexGrid.Row = Nothing
    Dim thisGridRow As C1.Win.C1FlexGrid.Row
    Dim InstrumentRow As DSInstrument.tblInstrumentRow

    Dim LastInstrument As Integer
    Dim LastCategory As Integer
    Dim thisInstrumentID As ULong
    Dim thisCategoryID As ULong
    Dim thisKey As ULong
    Dim thisValue As Boolean
    Dim KeyVal As ULong
    Dim DoFundSelection As Boolean = False
    Dim ThisFundInstrumentList As SortedList
    Dim ActiveCountLimit As Integer

    If (UpdatedCategoryEntries.Count > 0) Then
      Me.btnSave.Visible = True
      Me.btnCancel.Visible = True

      Me.Context_SaveChanges.Visible = True
      Me.Context_CancelChanges.Visible = True
      Me.Context_SeparatorSave.Visible = True
    Else
      Me.btnSave.Visible = False
      Me.btnCancel.Visible = False

      Me.Context_SaveChanges.Visible = False
      Me.Context_CancelChanges.Visible = False
      Me.Context_SeparatorSave.Visible = False
    End If

    If (Me.Combo_SelectByFund.SelectedIndex > 0) AndAlso FundHoldings.ContainsKey(CInt(Me.Combo_SelectByFund.SelectedValue)) Then
      DoFundSelection = True
      ThisFundInstrumentList = FundHoldings(CInt(Me.Combo_SelectByFund.SelectedValue))
    Else
      ThisFundInstrumentList = New SortedList
    End If

    ActiveCountLimit = CInt(Numeric_CountLimit.Value)

    Try
      Grid_Characteristics.Redraw = False

      Try
        ' Clear Grid, to start
        Grid_Characteristics.Rows.Count = 1 ' (The Header Row)

        ' Add Header Line.
        NewGridRow = Grid_Characteristics.Rows.Add()
        NewGridRow(GridColumns.FirstCol) = "Primonial"
        NewGridRow.IsNode = True
        NewGridRow.Node.Level = 0

      Catch ex As Exception
      End Try

      InPaint = True

      ' Initialise tracking variables.

      LastCategory = (-1)
      LastInstrument = (-1)

      '

      CategoryList.Clear()
      Dim thisCategory As CharacteristicSummaryClass = Nothing

      Try
        Dim InstrumentsDS As RenaissanceDataClass.DSInstrument = MainForm.Load_Table(RenaissanceStandardDatasets.tblInstrument)
        Dim RiskDataCategoryDS As RenaissanceDataClass.DSRiskDataCategory = MainForm.Load_Table(RenaissanceStandardDatasets.tblRiskDataCategory)
        Dim RiskDataCategoryRow As RenaissanceDataClass.DSRiskDataCategory.tblRiskDataCategoryRow
        Dim SelectedRows() As DataRow
        InstrumentRow = Nothing

        ' Temporarily hash the RiskDataCategory table, it is a very small table.
        ' The data could be taken from the DB, but to improve network performance we will try to add this field locally.
        ' 
        Dim RiskDataCategoryDictionary As New Dictionary(Of Integer, String)
        For Each RiskDataCategoryRow In RiskDataCategoryDS.tblRiskDataCategory.Rows
          RiskDataCategoryDictionary.Add(RiskDataCategoryRow.DataCategoryId, RiskDataCategoryRow.DataCategory)
        Next

        ' Loop through the selected data (DataView) adding Instruments to the ArrayList.

        For Counter = 0 To (myDataView.Count - 1)

          ' Resolve Transaction Row.
          thisRowView = myDataView.Item(Counter)
          thisRow = thisRowView.Row

          If (Not DoFundSelection) OrElse (ThisFundInstrumentList.ContainsKey(thisRow.InstrumentID)) Then

            ' Add a New Category Row if this transaction represents the start of a new Category or Instrument

            If (thisRow.InstrumentID <> LastInstrument) OrElse (thisRow.DataCategoryId <> LastCategory) Then

              thisCategory = New CharacteristicSummaryClass

              If (InstrumentRow Is Nothing) OrElse (InstrumentRow.InstrumentID <> thisRow.InstrumentID) Then
                SelectedRows = InstrumentsDS.tblInstrument.Select("AuditID=" & thisRow.InstrumentID.ToString)
                InstrumentRow = SelectedRows(0) ' CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, CInt(thisRow.InstrumentID)), DSInstrument.tblInstrumentRow)
              End If

              thisCategory.InstrumentID = thisRow.InstrumentID
              thisCategory.InstrumentDescription = InstrumentRow.InstrumentDescription
              thisCategory.InstrumentISIN = InstrumentRow.InstrumentISIN
              thisCategory.DataCategoryID = thisRow.DataCategoryId

              RiskDataCategoryDictionary.TryGetValue(thisRow.DataCategoryId, thisCategory.DataCategory)

              thisCategory.DataCharacteristicID = thisRow.DataCharacteristicId
              thisCategory.DataCharacteristic = ""
              thisCategory.DataViewCounter = Counter
              thisCategory.CharacteristicsCount = 0
              CategoryList.Add(thisCategory)
            End If

            If (thisRow.RiskDataID) Or (thisRow.DataWeighting) Then
              thisCategory.CharacteristicsCount += 1
            End If

            LastInstrument = thisRow.InstrumentID
            LastCategory = thisRow.DataCategoryId

          End If

        Next

        ' Sort

        CategoryList.Sort(New CategoryListComparerClass)

        ' Now add the Categories to the Grid.

        LastCategory = (-1)
        LastInstrument = (-1)

        For Counter = 0 To (CategoryList.Count - 1)
          thisCategory = CategoryList(Counter)  '  CharacteristicSummaryClass

          ' Add a New Row if this Characteristic represents the start of a new Instrument.

          If (LastInstrument <> thisCategory.InstrumentID) Then

            NewGridRow = Grid_Characteristics.Rows.Add()
            NewGridRow(GridColumns.FirstCol) = thisCategory.InstrumentDescription
            NewGridRow(GridColumns.ISIN) = thisCategory.InstrumentISIN
            NewGridRow(GridColumns.InstrumentID) = thisCategory.InstrumentID
            NewGridRow(GridColumns.CategoryID) = 0
            NewGridRow(GridColumns.Counter) = Counter ' thisCategory.Counter
            NewGridRow(GridColumns.Expanded) = False
            NewGridRow(GridColumns.DataWeighting) = 0
            NewGridRow.IsNode = True
            NewGridRow.Node.Level = 1
            Grid_Characteristics.SetCellStyle(NewGridRow.Index, GridColumns.DataWeighting, Grid_Characteristics.Styles("Weight_IntegerFormatLeft"))
            InstrumentGridRow = NewGridRow

            LastCategory = -1

            If (thisCategory.DataCategoryID > 0) Then
              NewGridRow = Grid_Characteristics.Rows.Add()
              NewGridRow(GridColumns.FirstCol) = ""
              NewGridRow.IsNode = False
            Else
              ' Ensure that it will not be expanded later
              KeyVal = (CULng(thisCategory.InstrumentID) << 32)
              ExpandedNodes(KeyVal) = False

            End If

          End If

          If (InstrumentGridRow IsNot Nothing) Then
            InstrumentGridRow(GridColumns.DataWeighting) = CInt(InstrumentGridRow(GridColumns.DataWeighting)) + thisCategory.CharacteristicsCount

            If (Check_CountLimit.Checked) AndAlso (Numeric_CountLimit.Value < CInt(InstrumentGridRow(GridColumns.DataWeighting))) Then
              InstrumentGridRow.Visible = False
            End If
          End If

          LastCategory = thisCategory.DataCategoryID
          LastInstrument = thisCategory.InstrumentID

        Next

        ' Now Colapse and expand rows as necessary.

        For Counter = (Grid_Characteristics.Rows.Count - 1) To 1 Step -1
          Try
            thisGridRow = Grid_Characteristics.Rows(Counter)

            If (thisGridRow.IsNode) AndAlso (thisGridRow.Node.Level >= 1) Then
              Grid_Characteristics.Rows(Counter).Node.Expanded = False
            End If
          Catch Inner_Ex As Exception
          End Try
        Next

        For Counter = (Grid_Characteristics.Rows.Count - 1) To 1 Step -1
          Try
            thisGridRow = Grid_Characteristics.Rows(Counter)

            If (thisGridRow.IsNode) AndAlso (thisGridRow.Node.Level = 1) AndAlso (thisGridRow.Visible) Then
              thisInstrumentID = CULng(thisGridRow(GridColumns.InstrumentID))
              thisCategoryID = CULng(thisGridRow(GridColumns.CategoryID))
              thisKey = ((thisInstrumentID << 32) + thisCategoryID)

              If (ExpandedNodes.TryGetValue(thisKey, thisValue)) Then
                If (thisValue) Then
                  ExpandRow(Counter)

                  thisGridRow.Node.Expanded = True
                Else
                  thisGridRow.Node.Expanded = False
                End If
              Else
                thisGridRow.Node.Expanded = False
              End If

            End If
          Catch Inner_Ex As Exception
          End Try
        Next

        For Counter = (Grid_Characteristics.Rows.Count - 1) To 1 Step -1
          Try
            thisGridRow = Grid_Characteristics.Rows(Counter)

            If (thisGridRow.IsNode) AndAlso (thisGridRow.Node.Level = 2) Then
              thisInstrumentID = CULng(thisGridRow(GridColumns.InstrumentID))
              thisCategoryID = CULng(thisGridRow(GridColumns.CategoryID))
              thisKey = ((thisInstrumentID << 32) + thisCategoryID)

              If (ExpandedNodes.TryGetValue(thisKey, thisValue)) Then
                If (thisValue) Then
                  ExpandRow(Counter)

                  thisGridRow.Node.Expanded = True
                Else
                  thisGridRow.Node.Expanded = False
                End If
              Else
                thisGridRow.Node.Expanded = False
              End If

            End If
          Catch Inner_Ex As Exception
          End Try
        Next

      Catch ex As Exception


      End Try

    Catch ex As Exception
    Finally
      Grid_Characteristics.Redraw = True
      Grid_Characteristics.Refresh()
      InPaint = False
      Grid_Characteristics.ScrollPosition = LastGridScroll
    End Try

  End Sub

  ''' <summary>
  ''' The m_ expanding node
  ''' </summary>
  Private m_ExpandingNode As Boolean = False

  ''' <summary>
  ''' Handles the BeforeCollapse event of the Grid_Characteristics control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="C1.Win.C1FlexGrid.RowColEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Characteristics_BeforeCollapse(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles Grid_Characteristics.BeforeCollapse
    ' **********************************************************************************
    ' Handle the expanding of Instrument sections.
    '
    '
    ' **********************************************************************************
    Dim KeyVal As ULong
    Dim thisInstrumentID As ULong
    Dim thisCategoryID As ULong
    Dim thisRow As C1.Win.C1FlexGrid.Row

    ' if we're working, ignore this
    If m_ExpandingNode Then
      Return
    End If

    Try

      ' don't allow Collapsing/expanding groups of rows (with shift-click)
      If e.Row < 0 Then
        e.Cancel = True
        Return
      End If

      ' if we're already collapsed, populate node
      ' before expanding
      thisRow = Grid_Characteristics.Rows(e.Row)
      thisInstrumentID = CULng(thisRow(GridColumns.InstrumentID))
      thisCategoryID = CULng(thisRow(GridColumns.CategoryID))
      KeyVal = ((thisInstrumentID << 32) + thisCategoryID)

      If (thisRow.IsNode) AndAlso (thisRow.Node.Collapsed) Then
        If InPaint = False Then
          ExpandedNodes(KeyVal) = True
        End If

        Try
          m_ExpandingNode = True
          ExpandRow(e.Row)
        Catch ex As Exception
        Finally
          m_ExpandingNode = False
        End Try
      Else
        If InPaint = False Then
          If (thisRow.IsNode) Then
            If (ExpandedNodes.ContainsKey(KeyVal)) Then
              ExpandedNodes(KeyVal) = False
            End If
          End If

        End If
      End If

    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Handles the AfterDragColumn event of the Grid_Characteristics control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="C1.Win.C1FlexGrid.DragRowColEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Characteristics_AfterDragColumn(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.DragRowColEventArgs) Handles Grid_Characteristics.AfterDragColumn
    ' **********************************************************************************
    ' Handle the Moving of Grid columns
    '
    ' Simply re-set the values in the GridColumns class
    '
    ' **********************************************************************************
    Call Init_GridColumnsClass()
  End Sub

  ''' <summary>
  ''' Expands the row.
  ''' </summary>
  ''' <param name="RowNumber">The row number.</param>
  Private Sub ExpandRow(ByVal RowNumber As Integer)
    ' **********************************************************************************
    ' Process the expanding of Category sections.
    '
    ' Inserts the appropriate Characterisric lines under a Category group.
    ' Use the 'Counter' column to determine whether in-fill has already occurred.
    ' 'Counter' value < 0 indicates in-fill has occurred.
    '
    ' The 'Counter' column will contain the Index reference in the DataView at which Characterisrics
    ' relating to the selected Category & Instrument are located
    ' The List and Counter values are updated each time the Dataview is updated in order to keep
    ' these value in-sync.
    '
    ' **********************************************************************************
    Dim DataViewCounter As Integer
    Dim thisRowView As DataRowView
    Dim thisRow As RenaissanceDataClass.DSSelectCharacteristics.tblSelectCharacteristicsRow
    Dim thisCategory As CharacteristicSummaryClass

    Dim thisGridRow As C1.Win.C1FlexGrid.Row
    Dim NewGridRow As C1.Win.C1FlexGrid.Row = Nothing
    Dim CategoryGridRow As C1.Win.C1FlexGrid.Row = Nothing
    Dim CategoryID As Integer
    Dim InstrumentID As Integer
    Dim CategoryListCounter As Integer
    Dim thisCategoryListItem As CharacteristicSummaryClass
    Dim thisCharcteristicListItem As CharacteristicSummaryClass
    Dim StartCounter As Integer
    Dim Expanded As Boolean
    Dim ListCounter As Integer

    Dim thisCategoryID As Integer

    Dim FirstLine As Boolean = True
    Dim UpdateRow As Integer = RowNumber + 1
    Dim KeyVal As ULong

    Dim OnlyEnteredCharacteristics As Boolean = Check_EnteredCharacteristics.Checked

    ' CategoryList

    Try
      ' Get Instrument and CategoryIDs
      thisGridRow = Grid_Characteristics.Rows(RowNumber)

      CategoryID = CInt(thisGridRow(GridColumns.CategoryID))
      InstrumentID = CInt(thisGridRow(GridColumns.InstrumentID))
      Expanded = CBool(thisGridRow(GridColumns.Expanded))

      ' Are the IDs OK, has In-Fill already occured ?

      If (Expanded = False) And (thisGridRow.IsNode) Then

        If (thisGridRow.Node.Level = 1) Then ' Expand Instrument -> Show Categories

          CategoryListCounter = CInt(thisGridRow(GridColumns.Counter))
          thisCategoryListItem = CategoryList(CategoryListCounter)
          StartCounter = thisCategoryListItem.DataViewCounter

          If (StartCounter >= 0) AndAlso (InstrumentID > 0) Then

            ' Mark Instrument Line as having been filled : Set Expanded to True
            thisGridRow(GridColumns.Expanded) = True

            '' Validate StartCounter
            '      If (StartCounter > (myDataView.Count - 1)) Then
            '        Exit Sub
            '      End If

            ' Add Lines.
            For ListCounter = CategoryListCounter To (CategoryList.Count - 1)

              ' Get Category Row.
              thisCategoryListItem = CategoryList(ListCounter)

              If (thisCategoryListItem.InstrumentID = InstrumentID) And (CInt(thisCategoryListItem.DataCategoryID) > 0) Then

                If (FirstLine) Or (thisCategoryListItem.DataCategoryID <> thisCategoryID) Then
                  ' New Category

                  If (FirstLine = False) Then
                    ' Add Line
                    UpdateRow += 1

                    Grid_Characteristics.Rows.Insert(UpdateRow)
                  End If

                  NewGridRow = Grid_Characteristics.Rows(UpdateRow)

                  ' Fill in other Category details

                  NewGridRow.IsNode = True
                  NewGridRow.Node.Level = 2
                  NewGridRow.Node.Expanded = False

                  NewGridRow(GridColumns.FirstCol) = CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblRiskDataCategory, CInt(thisCategoryListItem.DataCategoryID), "DataCategory")) ' thisRow.DataCategory
                  NewGridRow(GridColumns.ISIN) = ""
                  NewGridRow(GridColumns.InstrumentID) = thisCategoryListItem.InstrumentID
                  NewGridRow(GridColumns.CategoryID) = thisCategoryListItem.DataCategoryID
                  NewGridRow(GridColumns.CharacteristicID) = 0
                  NewGridRow(GridColumns.CharacteristicTypeID) = RenaissanceGlobals.RiskCharacteristicType.Normal
                  NewGridRow(GridColumns.DataWeighting) = 0
                  NewGridRow(GridColumns.RiskDataID) = 0
                  NewGridRow(GridColumns.Counter) = ListCounter
                  NewGridRow(GridColumns.Expanded) = False
                  Grid_Characteristics.SetCellStyle(NewGridRow.Index, GridColumns.DataWeighting, Grid_Characteristics.Styles("Weight_IntegerFormatMiddle"))
                  CategoryGridRow = NewGridRow

                  If (thisCategoryListItem.DataCharacteristicID > 0) Then
                    ' Add Dummy Row - Facilitates in-fill later.
                    UpdateRow += 1
                    NewGridRow = Grid_Characteristics.Rows.Insert(UpdateRow)
                    NewGridRow(GridColumns.FirstCol) = ""
                    NewGridRow.IsNode = False
                  Else
                    ' Ensure that it will not be expanded later
                    KeyVal = ((CULng(thisCategoryListItem.InstrumentID) << 32) + CULng(thisCategoryListItem.DataCategoryID))
                    ExpandedNodes(KeyVal) = False
                  End If

                  Grid_Characteristics.Rows(UpdateRow - 1).Node.Expanded = False

                End If

                If (CategoryGridRow IsNot Nothing) Then
                  CategoryGridRow(GridColumns.DataWeighting) = CInt(CategoryGridRow(GridColumns.DataWeighting)) + thisCategoryListItem.CharacteristicsCount
                End If

                FirstLine = False
                thisCategoryID = thisCategoryListItem.DataCategoryID

              Else
                ' Exit if we move on to a different Instrument
                Exit Sub
              End If

            Next

          End If

        ElseIf (thisGridRow.Node.Level = 2) Then ' Expand Category -> Show Characteristics

          CategoryListCounter = CInt(thisGridRow(GridColumns.Counter))
          thisCategoryListItem = CategoryList(CategoryListCounter)
          StartCounter = thisCategoryListItem.DataViewCounter

          If (StartCounter >= 0) AndAlso (InstrumentID > 0) AndAlso (CategoryID > 0) Then

            ' Mark Risk Category Line as having been filled : Set Expanded to True
            thisGridRow(GridColumns.Expanded) = True

            ' Validate StartCounter
            If (StartCounter > (myDataView.Count - 1)) Then
              Exit Sub
            End If

            ' Add Lines.
            Dim CharacteristicList As New ArrayList

            For DataViewCounter = StartCounter To (myDataView.Count - 1)
              thisRowView = myDataView.Item(DataViewCounter)
              thisRow = thisRowView.Row

              If (OnlyEnteredCharacteristics = False) Or (thisRow.RiskDataID > 0) Or (thisRow.DataWeighting <> 0.0) Then

                thisCategoryID = thisRow.DataCategoryId

                ' If this Transaction is appropriate to the selected group....

                If (thisRow.InstrumentID = InstrumentID) AndAlso (thisCategoryID = CategoryID) And (CInt(thisRow.DataCharacteristicId) > 0) Then
                  CharacteristicList.Add(New CharacteristicSummaryClass(thisRow.RiskDataID, thisRow.InstrumentID, "", thisRow.DataCategoryId, "", thisRow.DataCharacteristicId, CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblRiskDataCharacteristic, CInt(thisRow.DataCharacteristicId), "DataCharacteristic")), thisRow.DataCharacteristicTypeID, thisRow.DataWeighting, thisRow.DateEntered))
                End If
              End If

            Next

            CharacteristicList.Sort(New CategoryListComparerClass)

            For ListCounter = 0 To (CharacteristicList.Count - 1)
              thisCharcteristicListItem = CharacteristicList(ListCounter)

              thisCategoryID = thisCharcteristicListItem.DataCategoryID

              ' If this Transaction is appropriate to the selected group....

              If (thisCharcteristicListItem.InstrumentID = InstrumentID) AndAlso (thisCategoryID = CategoryID) And (CInt(thisCharcteristicListItem.DataCharacteristicID) > 0) Then

                ' If this is the first Characteristic, then overwrite the 'Dummy' grid line, else insert
                ' a new grid line.

                If (FirstLine = False) Then
                  ' Add Line
                  UpdateRow += 1

                  Grid_Characteristics.Rows.Insert(UpdateRow)
                  Grid_Characteristics.Rows(UpdateRow).IsNode = False
                End If

                ' Set Values / Styles
                thisGridRow = Grid_Characteristics.Rows(UpdateRow)

                thisGridRow(GridColumns.FirstCol) = thisCharcteristicListItem.DataCharacteristic
                thisGridRow(GridColumns.ISIN) = ""
                thisGridRow(GridColumns.InstrumentID) = thisCharcteristicListItem.InstrumentID
                thisGridRow(GridColumns.CategoryID) = thisCharcteristicListItem.DataCategoryID
                thisGridRow(GridColumns.CharacteristicID) = thisCharcteristicListItem.DataCharacteristicID
                thisGridRow(GridColumns.CharacteristicTypeID) = thisCharcteristicListItem.DataCharacteristicTypeID
                thisGridRow(GridColumns.RiskDataID) = thisCharcteristicListItem.RiskDataID

                ' Only set DataWeighting if it has a value, otherwise leave blank.
                'thisGridRow(GridColumns.DataWeighting) = thisCharcteristicListItem.DataWeighting

                If (thisCharcteristicListItem.RiskDataID) Then
                  thisGridRow(GridColumns.DataWeighting) = thisCharcteristicListItem.DataWeighting

                  If (thisCharcteristicListItem.DataWeighting >= 0) Then
                    Grid_Characteristics.SetCellStyle(UpdateRow, GridColumns.DataWeighting, Grid_Characteristics.Styles("Data"))
                  Else
                    Grid_Characteristics.SetCellStyle(UpdateRow, GridColumns.DataWeighting, Grid_Characteristics.Styles("DataNegative"))
                  End If

                ElseIf (thisCharcteristicListItem.DataWeighting) Then
                  thisGridRow(GridColumns.DataWeighting) = thisCharcteristicListItem.DataWeighting

                  If (thisCharcteristicListItem.DataWeighting >= 0) Then
                    Grid_Characteristics.SetCellStyle(UpdateRow, GridColumns.DataWeighting, Grid_Characteristics.Styles("Data_Default"))
                  Else
                    Grid_Characteristics.SetCellStyle(UpdateRow, GridColumns.DataWeighting, Grid_Characteristics.Styles("DataNegative_Default"))
                  End If

                End If

                If (thisCharcteristicListItem.DateEntered <> Renaissance_BaseDate) Then
                  thisGridRow(GridColumns.LastUpdated) = thisCharcteristicListItem.DateEntered
                End If

                ' Row Edited ?

                If UpdatedCategoryEntries.Count > 0 Then
                  Dim UpdateCounter As Integer

                  For UpdateCounter = 0 To (UpdatedCategoryEntries.Count - 1)
                    thisCategory = UpdatedCategoryEntries(UpdateCounter)  '  CharacteristicSummaryClass

                    If (thisCategory.InstrumentID = thisCharcteristicListItem.InstrumentID) And (thisCategory.DataCategoryID = thisCharcteristicListItem.DataCategoryID) And (thisCategory.DataCharacteristicID = thisCharcteristicListItem.DataCharacteristicID) Then
                      thisGridRow(GridColumns.DataWeighting) = thisCategory.DataWeighting

                      If (thisCategory.DataWeighting >= 0) Then
                        Grid_Characteristics.SetCellStyle(UpdateRow, GridColumns.DataWeighting, Grid_Characteristics.Styles("UpdatedWeightPositive"))
                      Else
                        Grid_Characteristics.SetCellStyle(UpdateRow, GridColumns.DataWeighting, Grid_Characteristics.Styles("UpdatedWeightNegative"))
                      End If

                      Exit For
                    End If

                  Next UpdateCounter
                End If

                FirstLine = False
              Else
                ' If the current transaction does not match, and we have added some lines already, then
                ' assume that we have come to the end of the section and exit.
                ' This logic works on the assumption that the DataView is ordered correctly by InstrumentID and CategoryID. 

                If (FirstLine = False) Then
                  ' If we have worked through the required Instrument / Category, then exit.
                  Exit Sub
                End If

              End If

            Next ListCounter
          End If

        End If ' (thisGridRow.Node.Level = 2) Then

      End If ' If (Expanded = False) And (thisGridRow.IsNode) Then

    Catch ex As Exception

    End Try

  End Sub

  ''' <summary>
  ''' Expands the instrument.
  ''' </summary>
  ''' <param name="pGridRow">The p grid row.</param>
  Private Sub ExpandInstrument(ByVal pGridRow As Integer)
    ' **********************************************************************************
    ' 
    '
    ' **********************************************************************************

    Try
      If (pGridRow > 0) And (pGridRow < Grid_Characteristics.Rows.Count) Then
        Dim thisGridRow As C1.Win.C1FlexGrid.Row = Grid_Characteristics.Rows(pGridRow)
        Dim tempGridRow As C1.Win.C1FlexGrid.Row
        Dim ThisInstrument As Integer = thisGridRow(GridColumns.InstrumentID)
        Dim rowCounter As Integer
        Dim InstrumentEndRow As Integer
        Dim ChangesMade As Boolean = False

        ' Exit if 'Primonial' node

        If (thisGridRow.IsNode = True) Then
          If (thisGridRow.Node.Level <= 0) Then
            Exit Sub
          ElseIf (thisGridRow.Node.Level = 1) Then
            thisGridRow.Node.Expanded = True
          End If
        End If

        ' Find Next Instrument Row

        If (pGridRow < (Grid_Characteristics.Rows.Count - 1)) Then
          For rowCounter = (pGridRow + 1) To (Grid_Characteristics.Rows.Count - 1)
            tempGridRow = Grid_Characteristics.Rows(rowCounter)

            If (tempGridRow.IsNode) AndAlso (tempGridRow.Node.Level <= 1) Then
              Exit For
            End If
          Next rowCounter
        End If

        ' Validate rowCounter

        If (rowCounter < Grid_Characteristics.Rows.Count) Then
          rowCounter -= 1
        ElseIf (rowCounter >= Grid_Characteristics.Rows.Count) Then
          rowCounter = Grid_Characteristics.Rows.Count - 1
        End If

        InstrumentEndRow = rowCounter

        ' Work upwards, expanding the Categories

        For rowCounter = InstrumentEndRow To 0 Step -1
          tempGridRow = Grid_Characteristics.Rows(rowCounter)

          If (tempGridRow.IsNode) Then
            If (tempGridRow.Node.Level = 2) AndAlso (tempGridRow.Node.Expanded = False) Then
              tempGridRow.Node.Expanded = True
            ElseIf (tempGridRow.Node.Level <= 1) Then
              Exit For
            End If
          Else
            ' Row Edited ?

            If UpdatedCategoryEntries.Count > 0 Then
              Dim UpdateCounter As Integer
              Dim thisCategory As CharacteristicSummaryClass

              For UpdateCounter = 0 To (UpdatedCategoryEntries.Count - 1)
                thisCategory = UpdatedCategoryEntries(UpdateCounter)  '  CharacteristicSummaryClass

                If (thisCategory.InstrumentID = CInt(tempGridRow(GridColumns.InstrumentID))) AndAlso (thisCategory.DataCategoryID = CInt(tempGridRow(GridColumns.CategoryID))) AndAlso (thisCategory.DataCharacteristicID = CInt(tempGridRow(GridColumns.CharacteristicID))) Then
                  tempGridRow(GridColumns.DataWeighting) = thisCategory.DataWeighting

                  If (thisCategory.DataWeighting >= 0) Then
                    Grid_Characteristics.SetCellStyle(rowCounter, GridColumns.DataWeighting, Grid_Characteristics.Styles("UpdatedWeightPositive"))
                  Else
                    Grid_Characteristics.SetCellStyle(rowCounter, GridColumns.DataWeighting, Grid_Characteristics.Styles("UpdatedWeightNegative"))
                  End If

                  Exit For
                End If

              Next UpdateCounter
            End If

          End If

        Next rowCounter

      End If

    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Handles the Click event of the btnSave control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
    ' **********************************************************************************
    '
    '
    ' **********************************************************************************

    Try
      ' Hmmm, How are we goint to do this?
      ' Plan A is to take the Characteristic entries in 'UpdatedCategoryEntries', build a new tblRiskData and then update it.
      ' So here we go...


      Dim UpdateCounter As Integer
      Dim thisCategory As CharacteristicSummaryClass

      Dim newTblRiskData As New RenaissanceDataClass.DSRiskData.tblRiskDataDataTable
      Dim newTblRiskDataRow As RenaissanceDataClass.DSRiskData.tblRiskDataRow

      For UpdateCounter = 0 To (UpdatedCategoryEntries.Count - 1)
        thisCategory = UpdatedCategoryEntries(UpdateCounter)  '  CharacteristicSummaryClass

        ' Only Save 'Normal' Risk Characteristic Data.
        If (thisCategory.DataCharacteristicTypeID = RenaissanceGlobals.RiskCharacteristicType.Normal) Then

          newTblRiskDataRow = newTblRiskData.NewtblRiskDataRow

          newTblRiskDataRow.RiskDataID = thisCategory.RiskDataID
          newTblRiskDataRow.InstrumentID = thisCategory.InstrumentID
          newTblRiskDataRow.DataCharacteristicId = thisCategory.DataCharacteristicID

          newTblRiskDataRow.DataWeighting = thisCategory.DataWeighting

          newTblRiskData.AddtblRiskDataRow(newTblRiskDataRow)

        End If

      Next UpdateCounter

      UpdatedCategoryEntries.Clear()

      Try
        If (newTblRiskData.Rows.Count > 0) Then

          MainForm.AdaptorUpdate("frmCharacteristics, SaveCharacteristicChanges()", RenaissanceStandardDatasets.tblRiskData, newTblRiskData.Select())

        End If
      Catch ex As Exception
        MainForm.LogError("frmCharacteristics, SaveCharacteristicChanges()", LOG_LEVELS.Error, ex.Message, "Error saving Characteristic Changes.", ex.StackTrace, True)
      End Try

      Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblRiskData), True)

    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Handles the Click event of the btnCancel control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    ' **********************************************************************************
    '
    '
    ' **********************************************************************************

    Try
      UpdatedCategoryEntries.Clear()

      PaintCharacteristicsGrid()

    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Handles the Click event of the Context_AddNewCategory control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Context_AddNewCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Context_AddNewCategory.Click
    ' **********************************************************************************
    ' Add new Category.
    '
    ' **********************************************************************************

    Try
      Dim NewCategoryName As String

      ' Get New Category name

      NewCategoryName = InputBox("New Category Name", "New Category", "")

      ' If Name has length, i.e. 'OK' was pressed...

      If (NewCategoryName.Length > 0) Then

        ' Create Category table and add new row.

        Dim TblRiskCategory As New RenaissanceDataClass.DSRiskDataCategory.tblRiskDataCategoryDataTable
        Dim newTblRiskCategoryRow As RenaissanceDataClass.DSRiskDataCategory.tblRiskDataCategoryRow
        Dim UpdateString As String = ""

        newTblRiskCategoryRow = TblRiskCategory.NewtblRiskDataCategoryRow

        newTblRiskCategoryRow.DataCategoryId = 0
        newTblRiskCategoryRow.DataCategory = NewCategoryName

        TblRiskCategory.AddtblRiskDataCategoryRow(newTblRiskCategoryRow)

        ' Update table.

        Try

          MainForm.AdaptorUpdate("frmCharacteristics, SaveNewCategory()", RenaissanceStandardDatasets.tblRiskDataCategory, TblRiskCategory.Select())
          If (TblRiskCategory.Rows.Count > 0) Then
            UpdateString = TblRiskCategory.Rows(0)("AuditID")
          End If
        Catch ex As Exception
          MainForm.LogError("frmCharacteristics, SaveNewCategory()", LOG_LEVELS.Error, ex.Message, "Error saving New Category.", ex.StackTrace, True)
        End Try

        ' Reload main Category table and cause update event.

        'Call MainForm.Load_Table(RenaissanceStandardDatasets.tblRiskDataCategory, True, True)
        MainForm.Load_Table_Background(RenaissanceStandardDatasets.tblRiskDataCategory, UpdateString, True, True)

      End If

    Catch ex As Exception

    End Try
  End Sub

  ''' <summary>
  ''' Handles the Click event of the Context_AddNewCharacteristic control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Context_AddNewCharacteristic_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Context_AddNewCharacteristic.Click
    ' **********************************************************************************
    ' Add New Risk Data Characteristic from the Grid Context menu.
    '
    ' **********************************************************************************
    Try
      If (LastEnteredGridRow > 0) And (LastEnteredGridRow < Grid_Characteristics.Rows.Count) Then

        Dim thisRow As C1.Win.C1FlexGrid.Row = Grid_Characteristics.Rows(LastEnteredGridRow)

        Dim NewCharacteristicName As String

        NewCharacteristicName = InputBox("Enter the new Characteristic Name" & vbCrLf & "for category `" & CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblRiskDataCategory, CInt(thisRow(GridColumns.CategoryID)), "DataCategory")) & "`", "New Characteristic", "")

        If (NewCharacteristicName.Length > 0) Then

          Dim TblRiskCharacteristic As New RenaissanceDataClass.DSRiskDataCharacteristic.tblRiskDataCharacteristicDataTable
          Dim newTblRiskCharacteristicRow As RenaissanceDataClass.DSRiskDataCharacteristic.tblRiskDataCharacteristicRow
          Dim UpdateString As String = ""

          newTblRiskCharacteristicRow = TblRiskCharacteristic.NewtblRiskDataCharacteristicRow

          newTblRiskCharacteristicRow.DataCharacteristicId = 0
          newTblRiskCharacteristicRow.DataCategoryId = thisRow(GridColumns.CategoryID)
          newTblRiskCharacteristicRow.DataCharacteristic = NewCharacteristicName
          newTblRiskCharacteristicRow.DataCharacteristicTypeID = RenaissanceGlobals.RiskCharacteristicType.Normal

          TblRiskCharacteristic.AddtblRiskDataCharacteristicRow(newTblRiskCharacteristicRow)

          Try

            MainForm.AdaptorUpdate("frmCharacteristics, SaveNewCharacteristic()", RenaissanceStandardDatasets.tblRiskDataCharacteristic, TblRiskCharacteristic.Select())
            If (TblRiskCharacteristic.Rows.Count > 0) Then
              UpdateString = TblRiskCharacteristic.Rows(0)("AuditID")
            End If

          Catch ex As Exception
            MainForm.LogError("frmCharacteristics, SaveNewCharacteristic()", LOG_LEVELS.Error, ex.Message, "Error saving New Characteristic.", ex.StackTrace, True)
          End Try

          ' Reload Characteristic table and cause update event.
          Call MainForm.Load_Table_Background(RenaissanceStandardDatasets.tblRiskDataCharacteristic, UpdateString, True, True)

        End If

      End If

    Catch ex As Exception

    End Try
  End Sub

  ''' <summary>
  ''' Handles the Click event of the Context_SetCharacteristicDefaultValue control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Context_SetCharacteristicDefaultValue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Context_SetCharacteristicDefaultValue.Click
    ' **********************************************************************************
    '
    '
    ' **********************************************************************************

    Try
      If (LastEnteredGridRow > 0) And (LastEnteredGridRow < Grid_Characteristics.Rows.Count) Then
        Dim thisRow As C1.Win.C1FlexGrid.Row = Grid_Characteristics.Rows(LastEnteredGridRow)
        Dim thisCharacteristicRow As DSRiskDataCharacteristic.tblRiskDataCharacteristicRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblRiskDataCharacteristic, CInt(thisRow(GridColumns.CharacteristicID)))

        Dim NewCharacteristicDefault As String
        NewCharacteristicDefault = InputBox("Set Default Value :" & vbCrLf & "for Characteristic `" & CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblRiskDataCharacteristic, CInt(thisRow(GridColumns.CharacteristicID)), "DataCharacteristic")) & "`." & vbCrLf & "Type 'null' to clear an existing value.", "New Characteristic Default", "")

        If (NewCharacteristicDefault.Length > 0) Then

          If (NewCharacteristicDefault.ToLower = "null") Or (NewCharacteristicDefault.ToLower = "'null'") Then

            thisCharacteristicRow.SetDefaultValueNull()

          ElseIf ConvertIsNumeric(NewCharacteristicDefault) Then

            thisCharacteristicRow.DefaultValue = CDbl(ConvertValue(NewCharacteristicDefault, GetType(Double)))

          Else

            Call MessageBox.Show("Sorry...", "Sorry, did not recognise that value as a number.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub

          End If

          MainForm.AdaptorUpdate("frmCharacteristics", RenaissanceStandardDatasets.tblRiskDataCharacteristic, New System.Data.DataRow() {thisCharacteristicRow})

          Call MainForm.Load_Table_Background(RenaissanceStandardDatasets.tblRiskDataCharacteristic, thisCharacteristicRow.AuditID.ToString(), True, True)

        End If


      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error saving default characteristic value.", ex.StackTrace, True)
    End Try
  End Sub

  ''' <summary>
  ''' Handles the Click event of the Context_SetCharacteristicFixedValue control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Context_SetCharacteristicFixedValue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Context_SetCharacteristicFixedValue.Click
    ' **********************************************************************************
    '
    '
    ' **********************************************************************************

    Try
      If (LastEnteredGridRow > 0) And (LastEnteredGridRow < Grid_Characteristics.Rows.Count) Then
        Dim thisRow As C1.Win.C1FlexGrid.Row = Grid_Characteristics.Rows(LastEnteredGridRow)
        Dim thisCharacteristicRow As DSRiskDataCharacteristic.tblRiskDataCharacteristicRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblRiskDataCharacteristic, CInt(thisRow(GridColumns.CharacteristicID)))

        Dim NewCharacteristicDefault As String
        NewCharacteristicDefault = InputBox("Set Fixed Value :" & vbCrLf & "for Characteristic `" & CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblRiskDataCharacteristic, CInt(thisRow(GridColumns.CharacteristicID)), "DataCharacteristic")) & "`." & vbCrLf & "Type 'null' to clear an existing value.", "New Characteristic Default", "")

        If (NewCharacteristicDefault.Length > 0) Then

          If (NewCharacteristicDefault.ToLower = "null") Or (NewCharacteristicDefault.ToLower = "'null'") Then

            thisCharacteristicRow.SetFixedValueNull()

          ElseIf ConvertIsNumeric(NewCharacteristicDefault) Then

            thisCharacteristicRow.FixedValue = CDbl(ConvertValue(NewCharacteristicDefault, GetType(Double)))

          Else

            Call MessageBox.Show("Sorry...", "Sorry, did not recognise that value as a number.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub

          End If

          MainForm.AdaptorUpdate("frmCharacteristics", RenaissanceStandardDatasets.tblRiskDataCharacteristic, New System.Data.DataRow() {thisCharacteristicRow})

          Call MainForm.Load_Table_Background(RenaissanceStandardDatasets.tblRiskDataCharacteristic, thisCharacteristicRow.AuditID.ToString(), True, True)

        End If


      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error saving fixed characteristic value.", ex.StackTrace, True)
    End Try

  End Sub

  ''' <summary>
  ''' Handles the Click event of the Context_ExpandThisInstrument control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Context_ExpandThisInstrument_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Context_ExpandThisInstrument.Click
    ' **********************************************************************************
    ' 
    '
    ' **********************************************************************************

    Try
      If (LastEnteredGridRow > 0) And (LastEnteredGridRow < Grid_Characteristics.Rows.Count) Then

        Call ExpandInstrument(LastEnteredGridRow)
      End If

    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Handles the Click event of the Context_ExpandInstruments control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Context_ExpandInstruments_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Context_ExpandInstruments.Click
    ' **********************************************************************************
    ' 
    '
    ' **********************************************************************************

    Try
      Dim Counter As Integer
      Dim thisGridRow As C1.Win.C1FlexGrid.Row
      Grid_Characteristics.Visible = False

      Counter = 1
      While Counter < Grid_Characteristics.Rows.Count

        Try
          thisGridRow = Grid_Characteristics.Rows(Counter)

          If (thisGridRow.IsNode) AndAlso (thisGridRow.Node.Level = 1) And (Not thisGridRow.Node.Expanded) Then
            thisGridRow.Node.Expanded = True
          End If
        Catch Inner_Ex As Exception
        End Try

        If (Counter Mod 100) = 0 Then
          StatusLabel_Characteristics.Text = "Expanding Instruments, " & CInt(Grid_Characteristics.Rows.Count - Counter).ToString & " to go..."
          Application.DoEvents()
        End If

        Counter += 1
      End While

    Catch ex As Exception
    Finally
      StatusLabel_Characteristics.Text = ""
      Grid_Characteristics.Visible = True
    End Try
  End Sub

  ''' <summary>
  ''' Handles the Click event of the Context_ColapseInstruments control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Context_ColapseInstruments_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Context_ColapseInstruments.Click
    ' **********************************************************************************
    ' 
    '
    ' **********************************************************************************

    Try
      Dim Counter As Integer
      Dim thisGridRow As C1.Win.C1FlexGrid.Row

      For Counter = (Grid_Characteristics.Rows.Count - 1) To 1 Step -1
        StatusLabel_Characteristics.Text = "Collapsing, " & Counter.ToString & " to go..."

        Try
          thisGridRow = Grid_Characteristics.Rows(Counter)

          If (thisGridRow.IsNode) AndAlso (thisGridRow.Node.Level >= 1) And (thisGridRow.Node.Expanded) Then
            thisGridRow.Node.Expanded = False
          End If
        Catch Inner_Ex As Exception
        End Try

        If (Counter Mod 100) = 0 Then
          Application.DoEvents()
        End If
      Next
    Catch ex As Exception
    Finally
      StatusLabel_Characteristics.Text = ""
    End Try
  End Sub

  ''' <summary>
  ''' Handles the Click event of the Context_ExpandCategories control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Context_ExpandCategories_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Context_ExpandCategories.Click
    ' **********************************************************************************
    ' 
    '
    ' **********************************************************************************

    Try
      If (Grid_Characteristics IsNot Nothing) Then

        If MessageBox.Show("Confirm", "Are you sure? This may take a while...", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.OK Then
          Context_ExpandInstruments_Click(sender, e)

          Dim Counter As Integer
          Dim thisGridRow As C1.Win.C1FlexGrid.Row

          For Counter = (Grid_Characteristics.Rows.Count - 1) To 1 Step -1
            StatusLabel_Characteristics.Text = "Expanding Categories, " & Counter.ToString & " to go..."

            Try
              thisGridRow = Grid_Characteristics.Rows(Counter)

              If (thisGridRow.IsNode) AndAlso (thisGridRow.Node.Level >= 2) And (Not thisGridRow.Node.Expanded) Then
                thisGridRow.Node.Expanded = True
              End If
            Catch Inner_Ex As Exception
            End Try

            If (Counter Mod 100) = 0 Then
              Application.DoEvents()
            End If
          Next
        End If
      End If


    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Handles the Click event of the Context_ColapseCategories control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Context_ColapseCategories_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Context_ColapseCategories.Click
    ' **********************************************************************************
    ' 
    '
    ' **********************************************************************************

    Try
      Dim Counter As Integer
      Dim thisGridRow As C1.Win.C1FlexGrid.Row

      For Counter = (Grid_Characteristics.Rows.Count - 1) To 1 Step -1
        StatusLabel_Characteristics.Text = "Collapsing, " & Counter.ToString & " to go..."

        Try
          thisGridRow = Grid_Characteristics.Rows(Counter)

          If (thisGridRow.IsNode) AndAlso (thisGridRow.Node.Level >= 2) And (thisGridRow.Node.Expanded) Then
            thisGridRow.Node.Expanded = False
          End If
        Catch Inner_Ex As Exception
        End Try

        If (Counter Mod 100) = 0 Then
          Application.DoEvents()
        End If
      Next
    Catch ex As Exception
    Finally
      StatusLabel_Characteristics.Text = ""
    End Try

  End Sub

  ''' <summary>
  ''' Handles the Click event of the Context_CopyCharacteristic control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Context_CopyCharacteristic_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Context_CopyCharacteristic.Click
    ' **********************************************************************************
    '
    ' **********************************************************************************

    Try
      If (LastEnteredGridRow > 0) And (LastEnteredGridRow < Grid_Characteristics.Rows.Count) Then
        Dim thisGridRow As C1.Win.C1FlexGrid.Row = Grid_Characteristics.Rows(LastEnteredGridRow)
        Dim tempGridRow As C1.Win.C1FlexGrid.Row
        Dim CharacteristicsClipboard As New Dictionary(Of Integer, CharacteristicSummaryClass)
        Dim RowCounter As Integer
        Dim ThisInstrument As Integer = 0
        Dim ThisCategory As Integer = 0
        Dim ThisCharacteristic As Integer = 0
        Dim ThisCharacteristicType As Integer = 0
        Dim DataViewCounter As Integer
        Dim thisDataRowView As DataRowView
        Dim thisDataRow As RenaissanceDataClass.DSSelectCharacteristics.tblSelectCharacteristicsRow
        Dim thisCharacteristicSummary As CharacteristicSummaryClass

        If thisGridRow.IsNode Then
          ThisInstrument = thisGridRow(GridColumns.InstrumentID)
          ThisCategory = thisGridRow(GridColumns.CategoryID)

          For DataViewCounter = 0 To (myDataView.Count - 1)
            thisDataRowView = myDataView.Item(DataViewCounter)
            thisDataRow = thisDataRowView.Row

            If (thisDataRow.InstrumentID = ThisInstrument) AndAlso (thisDataRow.RiskDataID <> 0) AndAlso (CType(thisDataRow.DataCharacteristicTypeID, RenaissanceGlobals.RiskCharacteristicType) = RenaissanceGlobals.RiskCharacteristicType.Normal) Then
              If (thisGridRow.Node.Level = 1) OrElse (thisDataRow.DataCategoryId = ThisCategory) Then
                thisCharacteristicSummary = New CharacteristicSummaryClass(0, 0, thisDataRow.DataCategoryId, thisDataRow.DataCharacteristicId, thisDataRow.DataCharacteristicTypeID, thisDataRow.DataWeighting)

                CharacteristicsClipboard.Add(thisDataRow.DataCharacteristicId, thisCharacteristicSummary)
              End If
            End If
          Next

          For RowCounter = (LastEnteredGridRow + 1) To (Grid_Characteristics.Rows.Count - 1)
            tempGridRow = Grid_Characteristics.Rows(RowCounter)

            If (tempGridRow.IsNode = False) Then
              If tempGridRow(GridColumns.RiskDataID) <> 0 Then
                ThisCharacteristic = CInt(tempGridRow(GridColumns.CharacteristicID))
                ThisCategory = CInt(tempGridRow(GridColumns.CategoryID))
                ThisCharacteristicType = CInt(tempGridRow(GridColumns.CharacteristicTypeID))

                ' Save value if it is not a Compound Characteristic
                If (CType(ThisCharacteristicType, RenaissanceGlobals.RiskCharacteristicType) = RenaissanceGlobals.RiskCharacteristicType.Normal) Then
                  thisCharacteristicSummary = New CharacteristicSummaryClass(0, 0, ThisCategory, ThisCharacteristic, ThisCharacteristicType, CDbl(tempGridRow(GridColumns.DataWeighting)))

                  If (CharacteristicsClipboard.ContainsKey(ThisCharacteristic)) Then
                    CharacteristicsClipboard(ThisCharacteristic) = thisCharacteristicSummary
                  Else
                    CharacteristicsClipboard.Add(ThisCharacteristic, thisCharacteristicSummary)
                  End If
                End If

              End If
            End If

            If (tempGridRow.Node.Level <= thisGridRow.Node.Level) Then
              Exit For
            End If
          Next

        Else
          If (thisGridRow(GridColumns.RiskDataID) <> 0) Then
            ThisCharacteristic = CInt(thisGridRow(GridColumns.CharacteristicID))
            ThisCategory = CInt(thisGridRow(GridColumns.CategoryID))
            ThisCharacteristicType = CInt(thisGridRow(GridColumns.CharacteristicTypeID))

            If (CType(ThisCharacteristicType, RenaissanceGlobals.RiskCharacteristicType) = RenaissanceGlobals.RiskCharacteristicType.Normal) Then
              thisCharacteristicSummary = New CharacteristicSummaryClass(0, 0, ThisCategory, ThisCharacteristic, ThisCharacteristicType, CDbl(thisGridRow(GridColumns.DataWeighting)))

              CharacteristicsClipboard.Add(ThisCharacteristic, thisCharacteristicSummary)
            End If
          End If
        End If

        If MainForm.TempStorageDictionary.ContainsKey(THIS_FORM_FormID.ToString & "_copy") Then
          MainForm.TempStorageDictionary(THIS_FORM_FormID.ToString & "_copy") = CharacteristicsClipboard
        Else
          MainForm.TempStorageDictionary.Add(THIS_FORM_FormID.ToString & "_copy", CharacteristicsClipboard)
        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Handles the Click event of the Context_PasteCharacteristic control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Context_PasteCharacteristic_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Context_PasteCharacteristic.Click
    ' **********************************************************************************
    '
    ' **********************************************************************************

    Try
      Dim ThisInstrument As Integer = 0
      Dim ThisCategory As Integer = 0
      Dim ThisCharacteristic As Integer = 0
      Dim CharacteristicsClipboard As Dictionary(Of Integer, CharacteristicSummaryClass)
      Dim CharacteristicsArray() As CharacteristicSummaryClass
      Dim thisCharacteristicSummary As CharacteristicSummaryClass
      Dim RowCounter As Integer
      Dim ExpandInstrumentFlag As Boolean = False

      If MainForm.TempStorageDictionary.ContainsKey(THIS_FORM_FormID.ToString & "_copy") Then
        CharacteristicsClipboard = CType(MainForm.TempStorageDictionary(THIS_FORM_FormID.ToString & "_copy"), Dictionary(Of Integer, CharacteristicSummaryClass))
        CharacteristicsArray = CharacteristicsClipboard.Values.ToArray

        If (LastEnteredGridRow > 0) And (LastEnteredGridRow < Grid_Characteristics.Rows.Count) Then
          Dim thisGridRow As C1.Win.C1FlexGrid.Row = Grid_Characteristics.Rows(LastEnteredGridRow)

          If thisGridRow.IsNode Then
            ThisInstrument = thisGridRow(GridColumns.InstrumentID)
            ThisCategory = thisGridRow(GridColumns.CategoryID)

            For RowCounter = 0 To (CharacteristicsArray.Length - 1)
              thisCharacteristicSummary = CharacteristicsArray(RowCounter)

              If (thisGridRow.Node.Level = 1) OrElse (thisCharacteristicSummary.DataCategoryID = ThisCategory) Then
                Add_UpdatedCategoryEntry(ThisInstrument, thisCharacteristicSummary)
                ExpandInstrumentFlag = True
              End If
            Next

          Else ' Paste to a single Characteristic

            ThisInstrument = CInt(thisGridRow(GridColumns.InstrumentID))
            ThisCategory = thisGridRow(GridColumns.CategoryID)
            ThisCharacteristic = CInt(thisGridRow(GridColumns.CharacteristicID))

            For RowCounter = 0 To (CharacteristicsArray.Length - 1)
              thisCharacteristicSummary = CharacteristicsArray(RowCounter)

              If (thisCharacteristicSummary.DataCharacteristicID = ThisCharacteristic) Then

                thisGridRow(GridColumns.DataWeighting) = thisCharacteristicSummary.DataWeighting

                ' Add This Characteristic to the Updates list.

                Add_UpdatedCategoryEntry(ThisInstrument, thisCharacteristicSummary)

                Exit For
              End If
            Next

          End If

        End If

        If (ExpandInstrumentFlag) Then
          ExpandInstrument(LastEnteredGridRow)
        End If
      End If

      If (UpdatedCategoryEntries.Count > 0) Then
        Me.btnSave.Visible = True
        Me.btnCancel.Visible = True

        Me.Context_SaveChanges.Visible = True
        Me.Context_CancelChanges.Visible = True
        Me.Context_SeparatorSave.Visible = True
      End If
    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Add_s the updated category entry.
  ''' </summary>
  ''' <param name="thisInstrumentID">The this instrument ID.</param>
  ''' <param name="pFromCategory">The p from category.</param>
  Private Sub Add_UpdatedCategoryEntry(ByVal thisInstrumentID As Integer, ByVal pFromCategory As CharacteristicSummaryClass)

    Try
      Dim Counter As Integer
      Dim thisCategory As CharacteristicSummaryClass

      ' Has this Characteristic been changed before ?

      If UpdatedCategoryEntries.Count > 0 Then

        For Counter = 0 To (UpdatedCategoryEntries.Count - 1)
          thisCategory = UpdatedCategoryEntries(Counter)  '  CharacteristicSummaryClass

          If (thisCategory.InstrumentID = thisInstrumentID) And (thisCategory.DataCategoryID = pFromCategory.DataCategoryID) And (thisCategory.DataCharacteristicID = pFromCategory.DataCharacteristicID) Then
            thisCategory.DataWeighting = pFromCategory.DataWeighting
            UpdatedCategoryEntries(Counter) = thisCategory

            Exit Sub
          End If

        Next Counter

      End If

      ' Add new Characteristic

      UpdatedCategoryEntries.Add(New CharacteristicSummaryClass(0, thisInstrumentID, pFromCategory.DataCategoryID, pFromCategory.DataCharacteristicID, pFromCategory.DataCharacteristicTypeID, pFromCategory.DataWeighting))

    Catch ex As Exception

    End Try
  End Sub

  ''' <summary>
  ''' Handles the Click event of the Context_SaveChanges control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Context_SaveChanges_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Context_SaveChanges.Click
    ' **********************************************************************************
    ' Save SGrid changes
    '
    ' **********************************************************************************

    btnSave_Click(sender, e)
  End Sub

  ''' <summary>
  ''' Handles the Click event of the Context_CancelChanges control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Context_CancelChanges_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Context_CancelChanges.Click
    ' **********************************************************************************
    ' Cancel grid changes
    '
    ' **********************************************************************************

    btnCancel_Click(sender, e)
  End Sub

  ''' <summary>
  ''' Handles the MouseEnterCell event of the Grid_Characteristics control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="C1.Win.C1FlexGrid.RowColEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Characteristics_MouseEnterCell(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles Grid_Characteristics.MouseEnterCell
    ' **********************************************************************************
    ' Record cell just entered and re-arrange the context menu according to the type
    ' of Grid Row.
    ' **********************************************************************************

    Try
      LastEnteredGridRow = e.Row
      LastEnteredGridCol = e.Col

      Dim enableAddCharacteristic As Boolean = False
      Dim enableSetCharacteristicValues As Boolean = False
      Dim enableRename As Boolean = False
      Dim enableDelete As Boolean = False
      Dim enableCopyPaste As Boolean = True

      If (e.Row <= 0) Then
        ' Header row, Primonial Row
        Grid_Characteristics.ContextMenuStrip = Nothing
      Else
        Dim thisRow As C1.Win.C1FlexGrid.Row = Grid_Characteristics.Rows(LastEnteredGridRow)

        If (Grid_Characteristics.ContextMenuStrip Is Nothing) Then
          Grid_Characteristics.ContextMenuStrip = Me.ContextMenu_GridCharacteristics
        End If

        If (thisRow.IsNode) Then
          If (thisRow.Node.Level <= 0) Then
            ' Node Level = 0 : Primonial
            enableCopyPaste = False

          ElseIf (thisRow.Node.Level <= 1) Then
            ' Node Level = 1 : Instrument

          Else
            ' Node Level = 2 : Category

            enableAddCharacteristic = True
            enableRename = True
            enableDelete = True

            Context_RenameLabel.Text = "Rename Category" '  `" & CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblRiskDataCategory, CInt(thisRow(GridColumns.CategoryID)), "DataCategory")) & "`"
            Context_RenameTextBox.Text = CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblRiskDataCategory, CInt(thisRow(GridColumns.CategoryID)), "DataCategory"))
            Context_Delete.Text = "Delete `" & CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblRiskDataCategory, CInt(thisRow(GridColumns.CategoryID)), "DataCategory")) & "`"
            GridRenameItem = "CATEGORY"
            GridDeleteItem = "CATEGORY"

          End If
        Else
          ' Not a Node : Characteristic

          enableAddCharacteristic = True
          enableSetCharacteristicValues = True
          enableRename = True
          enableDelete = True

          Context_RenameLabel.Text = "Rename Characteristic" '  `" & CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblRiskDataCharacteristic, CInt(thisRow(GridColumns.CharacteristicID)), "DataCharacteristic")) & "`"
          Context_RenameTextBox.Text = CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblRiskDataCharacteristic, CInt(thisRow(GridColumns.CharacteristicID)), "DataCharacteristic"))
          Context_Delete.Text = "Delete `" & CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblRiskDataCharacteristic, CInt(thisRow(GridColumns.CharacteristicID)), "DataCharacteristic")) & "`"
          GridRenameItem = "CHARACTERISTIC"
          GridDeleteItem = "CHARACTERISTIC"

        End If
      End If

      ' Show / Hide menu options.

      Context_AddNewCharacteristic.Visible = enableAddCharacteristic
      Context_RenameSeparator.Visible = enableRename
      Context_RenameLabel.Visible = enableRename
      Context_RenameTextBox.Visible = enableRename
      Context_SeparatorDelete.Visible = enableDelete
      Context_Delete.Visible = enableDelete

      Context_CharacteristicValueSeparator.Visible = enableSetCharacteristicValues
      Context_SetCharacteristicDefaultValue.Visible = enableSetCharacteristicValues
      Context_SetCharacteristicFixedValue.Visible = enableSetCharacteristicValues

      Context_SeparatorCopyPaste.Visible = enableCopyPaste
      Context_CopyCharacteristic.Visible = enableCopyPaste
      Context_PasteCharacteristic.Visible = enableCopyPaste

      GridRenameFocus = False

    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Handles the AfterScroll event of the Grid_Characteristics control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="C1.Win.C1FlexGrid.RangeEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Characteristics_AfterScroll(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RangeEventArgs) Handles Grid_Characteristics.AfterScroll
    Try
      If InPaint = False Then
        LastGridScroll = Grid_Characteristics.ScrollPosition
      End If
    Catch ex As Exception

    End Try
  End Sub

  ''' <summary>
  ''' Handles the GotFocus event of the Context_RenameTextBox control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Context_RenameTextBox_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Context_RenameTextBox.GotFocus

  End Sub

  ''' <summary>
  ''' Handles the KeyDown event of the Context_RenameTextBox control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
  Private Sub Context_RenameTextBox_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Context_RenameTextBox.KeyDown
    Try
      If GridRenameFocus Then
        If e.KeyCode = Keys.Enter Then
          Grid_Rename_Item(sender)
        End If
      End If
    Catch ex As Exception

    End Try
  End Sub


  ''' <summary>
  ''' Handles the LostFocus event of the Context_RenameTextBox control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Context_RenameTextBox_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Context_RenameTextBox.LostFocus
    Try
      If GridRenameFocus Then
        Grid_Rename_Item(sender)
      End If
    Catch ex As Exception

    End Try
  End Sub

  ''' <summary>
  ''' Handles the TextChanged event of the Context_RenameTextBox control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Context_RenameTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Context_RenameTextBox.TextChanged
    Try
      GridRenameFocus = True
    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Handles the Opened event of the ContextMenu_GridCharacteristics control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub ContextMenu_GridCharacteristics_Opened(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContextMenu_GridCharacteristics.Opened

    Try
      GridRenameFocus = False
    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Grid_s the rename_ item.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function Grid_Rename_Item(ByVal sender As Object) As Boolean
    ' **********************************************************************************
    '
    '
    ' **********************************************************************************

    Try
      If GridRenameFocus And (LastEnteredGridRow > 0) And (LastEnteredGridRow < Grid_Characteristics.Rows.Count) Then
        GridRenameFocus = False

        Dim thisRow As C1.Win.C1FlexGrid.Row = Grid_Characteristics.Rows(LastEnteredGridRow)

        If GridRenameItem = "CATEGORY" Then

          If MessageBox.Show("Rename Category" & vbCrLf & "`" & CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblRiskDataCategory, CInt(thisRow(GridColumns.CategoryID)), "DataCategory")) & "`" & vbCrLf & "to" & vbCrLf & "`" & Context_RenameTextBox.Text & "`", "Rename this Category ?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button3) = Windows.Forms.DialogResult.Yes Then

            Dim thisCategoryRow As DSRiskDataCategory.tblRiskDataCategoryRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblRiskDataCategory, CInt(thisRow(GridColumns.CategoryID)))

            If (thisCategoryRow IsNot Nothing) Then
              thisCategoryRow.DataCategory = Context_RenameTextBox.Text

              MainForm.AdaptorUpdate("frmCharacteristics", RenaissanceStandardDatasets.tblRiskDataCategory, New System.Data.DataRow() {thisCategoryRow})

              Call MainForm.Load_Table_Background(RenaissanceStandardDatasets.tblRiskDataCategory, thisCategoryRow.AuditID.ToString(), True, True)

            End If

          End If

        ElseIf GridRenameItem = "CHARACTERISTIC" Then

          If MessageBox.Show("Rename Characteristic" & vbCrLf & "`" & CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblRiskDataCharacteristic, CInt(thisRow(GridColumns.CharacteristicID)), "DataCharacteristic")) & "`" & vbCrLf & "to" & vbCrLf & "`" & Context_RenameTextBox.Text & "`", "Rename this Characteristic ?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button3) = Windows.Forms.DialogResult.Yes Then

            Dim thisCharacteristicRow As DSRiskDataCharacteristic.tblRiskDataCharacteristicRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblRiskDataCharacteristic, CInt(thisRow(GridColumns.CharacteristicID)))

            If (thisCharacteristicRow IsNot Nothing) Then
              thisCharacteristicRow.DataCharacteristic = Context_RenameTextBox.Text

              MainForm.AdaptorUpdate("frmCharacteristics", RenaissanceStandardDatasets.tblRiskDataCharacteristic, New System.Data.DataRow() {thisCharacteristicRow})

              Call MainForm.Load_Table_Background(RenaissanceStandardDatasets.tblRiskDataCharacteristic, thisCharacteristicRow.AuditID.ToString(), True, True)

            End If

          End If

        End If

      End If

    Catch ex As Exception
    Finally
      ContextMenu_GridCharacteristics.Hide()
    End Try

    Return True
  End Function

  ''' <summary>
  ''' Handles the Click event of the Context_Delete control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Context_Delete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Context_Delete.Click
    ' **********************************************************************************
    '
    '
    ' **********************************************************************************

    Try
      Dim UpdateString As String = ""

      If (LastEnteredGridRow > 0) And (LastEnteredGridRow < Grid_Characteristics.Rows.Count) Then
        GridRenameFocus = False

        Dim thisRow As C1.Win.C1FlexGrid.Row = Grid_Characteristics.Rows(LastEnteredGridRow)

        If GridRenameItem = "CATEGORY" Then

          If MessageBox.Show("Delete Category" & vbCrLf & "`" & CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblRiskDataCategory, CInt(thisRow(GridColumns.CategoryID)), "DataCategory")) & "` ?", "Delete Category ?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button3) = Windows.Forms.DialogResult.Yes Then

            Dim thisCategoryRow As DSRiskDataCategory.tblRiskDataCategoryRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblRiskDataCategory, thisRow(GridColumns.CategoryID))

            If (thisCategoryRow IsNot Nothing) Then
              UpdateString = thisCategoryRow.AuditID.ToString()
              thisCategoryRow.Delete()

              MainForm.AdaptorUpdate("frmCharacteristics", RenaissanceStandardDatasets.tblRiskDataCategory, New System.Data.DataRow() {thisCategoryRow})

              Call MainForm.Load_Table_Background(RenaissanceStandardDatasets.tblRiskDataCategory, UpdateString, True, True)

            End If

          End If

        ElseIf GridRenameItem = "CHARACTERISTIC" Then

          If MessageBox.Show("Delete Characteristic" & vbCrLf & "`" & CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblRiskDataCharacteristic, CInt(thisRow(GridColumns.CharacteristicID)), "DataCharacteristic")) & "` ?", "Delete Characteristic ?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button3) = Windows.Forms.DialogResult.Yes Then

            Dim thisCharacteristicRow As DSRiskDataCharacteristic.tblRiskDataCharacteristicRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblRiskDataCharacteristic, thisRow(GridColumns.CharacteristicID))

            If (thisCharacteristicRow IsNot Nothing) Then
              UpdateString = thisCharacteristicRow.AuditID.ToString()
              thisCharacteristicRow.Delete()

              MainForm.AdaptorUpdate("frmCharacteristics", RenaissanceStandardDatasets.tblRiskDataCharacteristic, New System.Data.DataRow() {thisCharacteristicRow})

              Call MainForm.Load_Table_Background(RenaissanceStandardDatasets.tblRiskDataCharacteristic, UpdateString, True, True)

            End If

          End If

        End If

      End If

    Catch ex As Exception
    Finally
      ContextMenu_GridCharacteristics.Hide()
    End Try

  End Sub




End Class
