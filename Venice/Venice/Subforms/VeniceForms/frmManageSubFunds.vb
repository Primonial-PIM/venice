﻿' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 08-15-2013
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmManageSubFunds.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals
Imports RenaissancePertracDataClass

Imports RenaissanceDataClass
Imports C1.Win.C1FlexGrid

''' <summary>
''' Class frmManageSubFunds
''' </summary>
Public Class frmManageSubFunds

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmManageSubFunds"/> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The combo_ transaction fund
    ''' </summary>
  Friend WithEvents Combo_TransactionFund As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label_ cpty is fund
    ''' </summary>
  Friend WithEvents label_CptyIsFund As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ sub fund
    ''' </summary>
  Friend WithEvents Combo_SubFund As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label2
    ''' </summary>
  Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The grid_ positions
    ''' </summary>
  Friend WithEvents Grid_Positions As C1.Win.C1FlexGrid.C1FlexGrid
    ''' <summary>
    ''' The root menu
    ''' </summary>
  Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
    ''' <summary>
    ''' The form_ status strip
    ''' </summary>
  Friend WithEvents Form_StatusStrip As System.Windows.Forms.StatusStrip
    ''' <summary>
    ''' The form_ progress bar
    ''' </summary>
  Friend WithEvents Form_ProgressBar As System.Windows.Forms.ToolStripProgressBar
    ''' <summary>
    ''' The tab control_ transactions
    ''' </summary>
  Friend WithEvents TabControl_Transactions As System.Windows.Forms.TabControl
    ''' <summary>
    ''' The tab_ portfolio
    ''' </summary>
  Friend WithEvents Tab_Portfolio As System.Windows.Forms.TabPage
    ''' <summary>
    ''' The tab_ transactions
    ''' </summary>
  Friend WithEvents Tab_Transactions As System.Windows.Forms.TabPage
    ''' <summary>
    ''' The edit_ threshold
    ''' </summary>
  Friend WithEvents edit_Threshold As RenaissanceControls.PercentageTextBox
    ''' <summary>
    ''' The label1
    ''' </summary>
  Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The grid_ pending transactions
    ''' </summary>
  Friend WithEvents Grid_PendingTransactions As C1.Win.C1FlexGrid.C1FlexGrid
    ''' <summary>
    ''' The label4
    ''' </summary>
  Friend WithEvents Label4 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label3
    ''' </summary>
  Friend WithEvents Label3 As System.Windows.Forms.Label
    ''' <summary>
    ''' The grid_ pending transfers
    ''' </summary>
  Friend WithEvents Grid_PendingTransfers As C1.Win.C1FlexGrid.C1FlexGrid
    ''' <summary>
    ''' The button_ trades_ selectnone
    ''' </summary>
  Friend WithEvents Button_Trades_Selectnone As System.Windows.Forms.Button
    ''' <summary>
    ''' The button_ trades_ select all
    ''' </summary>
  Friend WithEvents Button_Trades_SelectAll As System.Windows.Forms.Button
    ''' <summary>
    ''' The button_ trades_ submit
    ''' </summary>
  Friend WithEvents Button_Trades_Submit As System.Windows.Forms.Button
    ''' <summary>
    ''' The button_ transfer_ submit
    ''' </summary>
  Friend WithEvents Button_Transfer_Submit As System.Windows.Forms.Button
    ''' <summary>
    ''' The button_ transfer_ select none
    ''' </summary>
  Friend WithEvents Button_Transfer_SelectNone As System.Windows.Forms.Button
    ''' <summary>
    ''' The button_ transfer_ select all
    ''' </summary>
  Friend WithEvents Button_Transfer_SelectAll As System.Windows.Forms.Button
    ''' <summary>
    ''' The panel1
    ''' </summary>
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The label5
    ''' </summary>
  Friend WithEvents Label5 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label9
    ''' </summary>
  Friend WithEvents Label9 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ transfer quantity
    ''' </summary>
  Friend WithEvents edit_TransferQuantity As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The combo_ to sub fund
    ''' </summary>
  Friend WithEvents Combo_ToSubFund As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label8
    ''' </summary>
  Friend WithEvents Label8 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ from sub fund
    ''' </summary>
  Friend WithEvents Combo_FromSubFund As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label7
    ''' </summary>
  Friend WithEvents Label7 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ instrument
    ''' </summary>
  Friend WithEvents Combo_Instrument As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label6
    ''' </summary>
  Friend WithEvents Label6 As System.Windows.Forms.Label
    ''' <summary>
    ''' The button_ transfer
    ''' </summary>
  Friend WithEvents Button_Transfer As System.Windows.Forms.Button
    ''' <summary>
    ''' The check_ transfer at zero value
    ''' </summary>
  Friend WithEvents Check_TransferAtZeroValue As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The context menu_ positions
    ''' </summary>
  Friend WithEvents ContextMenu_Positions As System.Windows.Forms.ContextMenuStrip
    ''' <summary>
    ''' The context menu_ transactions
    ''' </summary>
  Friend WithEvents ContextMenu_Transactions As System.Windows.Forms.ContextMenuStrip
    ''' <summary>
    ''' The context menu_ transfers
    ''' </summary>
  Friend WithEvents ContextMenu_Transfers As System.Windows.Forms.ContextMenuStrip
    ''' <summary>
    ''' The button_ transfer_ cancel
    ''' </summary>
  Friend WithEvents Button_Transfer_Cancel As System.Windows.Forms.Button
    ''' <summary>
    ''' The group box1
    ''' </summary>
  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The radio_ transfer_ selected
    ''' </summary>
  Friend WithEvents Radio_Transfer_Selected As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The radio_ transfer_ all
    ''' </summary>
  Friend WithEvents Radio_Transfer_All As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The group box2
    ''' </summary>
  Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The radio_ transactions_ selected
    ''' </summary>
  Friend WithEvents Radio_Transactions_Selected As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The radio_ transactions_ all
    ''' </summary>
  Friend WithEvents Radio_Transactions_All As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The menu_ enter this transfer
    ''' </summary>
  Friend WithEvents Menu_EnterThisTransfer As System.Windows.Forms.ToolStripMenuItem
    ''' <summary>
    ''' The menu_ enter all transfers
    ''' </summary>
  Friend WithEvents Menu_EnterAllTransfers As System.Windows.Forms.ToolStripMenuItem
    ''' <summary>
    ''' The tool strip separator1
    ''' </summary>
  Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    ''' <summary>
    ''' The menu_ remove this transfer
    ''' </summary>
  Friend WithEvents Menu_RemoveThisTransfer As System.Windows.Forms.ToolStripMenuItem
    ''' <summary>
    ''' The menu_ remove ALL transfers
    ''' </summary>
  Friend WithEvents Menu_RemoveALLTransfers As System.Windows.Forms.ToolStripMenuItem
    ''' <summary>
    ''' The menu_ submit transaction
    ''' </summary>
  Friend WithEvents Menu_SubmitTransaction As System.Windows.Forms.ToolStripMenuItem
    ''' <summary>
    ''' The menu_ submit ALL transactions
    ''' </summary>
  Friend WithEvents Menu_SubmitALLTransactions As System.Windows.Forms.ToolStripMenuItem
    ''' <summary>
    ''' The menu_ view transactions
    ''' </summary>
  Friend WithEvents Menu_ViewTransactions As System.Windows.Forms.ToolStripMenuItem
    ''' <summary>
    ''' The label_ drag new instrument
    ''' </summary>
  Friend WithEvents Label_DragNewInstrument As System.Windows.Forms.Label
    ''' <summary>
    ''' The check_ by ISIN
    ''' </summary>
  Friend WithEvents Check_ByISIN As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The label10
    ''' </summary>
  Friend WithEvents Label10 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ warning
    ''' </summary>
  Friend WithEvents edit_Warning As RenaissanceControls.PercentageTextBox
    ''' <summary>
    ''' The menu_ transactions separator
    ''' </summary>
  Friend WithEvents Menu_TransactionsSeparator As System.Windows.Forms.ToolStripSeparator
    ''' <summary>
    ''' The menu_ cancel manual change
    ''' </summary>
  Friend WithEvents Menu_CancelManualChange As System.Windows.Forms.ToolStripMenuItem
    ''' <summary>
    ''' The tab control_ sub funds
    ''' </summary>
  Friend WithEvents TabControl_SubFunds As System.Windows.Forms.TabControl
    ''' <summary>
    ''' The tab_ transfer
    ''' </summary>
  Friend WithEvents Tab_Transfer As System.Windows.Forms.TabPage
    ''' <summary>
    ''' The tab_ valuation
    ''' </summary>
  Friend WithEvents Tab_Valuation As System.Windows.Forms.TabPage
    ''' <summary>
    ''' The grid_ valuations
    ''' </summary>
  Friend WithEvents Grid_Valuations As C1.Win.C1FlexGrid.C1FlexGrid
  Friend WithEvents Check_ValuationsIncludeUnsettledFX As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The label_ status
    ''' </summary>
  Friend WithEvents Label_Status As System.Windows.Forms.ToolStripStatusLabel
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmManageSubFunds))
    Me.Combo_TransactionFund = New System.Windows.Forms.ComboBox
    Me.label_CptyIsFund = New System.Windows.Forms.Label
    Me.Combo_SubFund = New System.Windows.Forms.ComboBox
    Me.Label2 = New System.Windows.Forms.Label
    Me.Grid_Positions = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.RootMenu = New System.Windows.Forms.MenuStrip
    Me.Form_StatusStrip = New System.Windows.Forms.StatusStrip
    Me.Form_ProgressBar = New System.Windows.Forms.ToolStripProgressBar
    Me.Label_Status = New System.Windows.Forms.ToolStripStatusLabel
    Me.TabControl_Transactions = New System.Windows.Forms.TabControl
    Me.Tab_Portfolio = New System.Windows.Forms.TabPage
    Me.TabControl_SubFunds = New System.Windows.Forms.TabControl
    Me.Tab_Transfer = New System.Windows.Forms.TabPage
    Me.Panel1 = New System.Windows.Forms.Panel
    Me.Check_ByISIN = New System.Windows.Forms.CheckBox
    Me.Label_DragNewInstrument = New System.Windows.Forms.Label
    Me.Check_TransferAtZeroValue = New System.Windows.Forms.CheckBox
    Me.Button_Transfer = New System.Windows.Forms.Button
    Me.Label9 = New System.Windows.Forms.Label
    Me.edit_TransferQuantity = New RenaissanceControls.NumericTextBox
    Me.Combo_ToSubFund = New System.Windows.Forms.ComboBox
    Me.Label8 = New System.Windows.Forms.Label
    Me.Combo_FromSubFund = New System.Windows.Forms.ComboBox
    Me.Label7 = New System.Windows.Forms.Label
    Me.Combo_Instrument = New System.Windows.Forms.ComboBox
    Me.Label6 = New System.Windows.Forms.Label
    Me.Label5 = New System.Windows.Forms.Label
    Me.Tab_Valuation = New System.Windows.Forms.TabPage
    Me.Grid_Valuations = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.Label10 = New System.Windows.Forms.Label
    Me.edit_Warning = New RenaissanceControls.PercentageTextBox
    Me.Label1 = New System.Windows.Forms.Label
    Me.edit_Threshold = New RenaissanceControls.PercentageTextBox
    Me.Tab_Transactions = New System.Windows.Forms.TabPage
    Me.GroupBox2 = New System.Windows.Forms.GroupBox
    Me.Radio_Transactions_Selected = New System.Windows.Forms.RadioButton
    Me.Radio_Transactions_All = New System.Windows.Forms.RadioButton
    Me.Button_Transfer_Cancel = New System.Windows.Forms.Button
    Me.GroupBox1 = New System.Windows.Forms.GroupBox
    Me.Radio_Transfer_Selected = New System.Windows.Forms.RadioButton
    Me.Radio_Transfer_All = New System.Windows.Forms.RadioButton
    Me.Button_Transfer_Submit = New System.Windows.Forms.Button
    Me.Button_Transfer_SelectNone = New System.Windows.Forms.Button
    Me.Button_Transfer_SelectAll = New System.Windows.Forms.Button
    Me.Button_Trades_Submit = New System.Windows.Forms.Button
    Me.Button_Trades_Selectnone = New System.Windows.Forms.Button
    Me.Button_Trades_SelectAll = New System.Windows.Forms.Button
    Me.Label4 = New System.Windows.Forms.Label
    Me.Label3 = New System.Windows.Forms.Label
    Me.Grid_PendingTransfers = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.ContextMenu_Transfers = New System.Windows.Forms.ContextMenuStrip(Me.components)
    Me.Menu_EnterThisTransfer = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_EnterAllTransfers = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
    Me.Menu_RemoveThisTransfer = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_RemoveALLTransfers = New System.Windows.Forms.ToolStripMenuItem
    Me.Grid_PendingTransactions = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.ContextMenu_Positions = New System.Windows.Forms.ContextMenuStrip(Me.components)
    Me.Menu_ViewTransactions = New System.Windows.Forms.ToolStripMenuItem
    Me.ContextMenu_Transactions = New System.Windows.Forms.ContextMenuStrip(Me.components)
    Me.Menu_SubmitTransaction = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_SubmitALLTransactions = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_TransactionsSeparator = New System.Windows.Forms.ToolStripSeparator
    Me.Menu_CancelManualChange = New System.Windows.Forms.ToolStripMenuItem
    Me.Check_ValuationsIncludeUnsettledFX = New System.Windows.Forms.CheckBox
    CType(Me.Grid_Positions, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Form_StatusStrip.SuspendLayout()
    Me.TabControl_Transactions.SuspendLayout()
    Me.Tab_Portfolio.SuspendLayout()
    Me.TabControl_SubFunds.SuspendLayout()
    Me.Tab_Transfer.SuspendLayout()
    Me.Panel1.SuspendLayout()
    Me.Tab_Valuation.SuspendLayout()
    CType(Me.Grid_Valuations, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_Transactions.SuspendLayout()
    Me.GroupBox2.SuspendLayout()
    Me.GroupBox1.SuspendLayout()
    CType(Me.Grid_PendingTransfers, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.ContextMenu_Transfers.SuspendLayout()
    CType(Me.Grid_PendingTransactions, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.ContextMenu_Positions.SuspendLayout()
    Me.ContextMenu_Transactions.SuspendLayout()
    Me.SuspendLayout()
    '
    'Combo_TransactionFund
    '
    Me.Combo_TransactionFund.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_TransactionFund.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_TransactionFund.Location = New System.Drawing.Point(128, 32)
    Me.Combo_TransactionFund.Name = "Combo_TransactionFund"
    Me.Combo_TransactionFund.Size = New System.Drawing.Size(1212, 21)
    Me.Combo_TransactionFund.TabIndex = 0
    '
    'label_CptyIsFund
    '
    Me.label_CptyIsFund.Location = New System.Drawing.Point(16, 36)
    Me.label_CptyIsFund.Name = "label_CptyIsFund"
    Me.label_CptyIsFund.Size = New System.Drawing.Size(104, 16)
    Me.label_CptyIsFund.TabIndex = 82
    Me.label_CptyIsFund.Text = "Fund"
    '
    'Combo_SubFund
    '
    Me.Combo_SubFund.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_SubFund.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SubFund.Location = New System.Drawing.Point(128, 60)
    Me.Combo_SubFund.Name = "Combo_SubFund"
    Me.Combo_SubFund.Size = New System.Drawing.Size(1212, 21)
    Me.Combo_SubFund.TabIndex = 1
    '
    'Label2
    '
    Me.Label2.Location = New System.Drawing.Point(16, 64)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(104, 16)
    Me.Label2.TabIndex = 84
    Me.Label2.Text = "SubFund"
    '
    'Grid_Positions
    '
    Me.Grid_Positions.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_Positions.AutoGenerateColumns = False
    Me.Grid_Positions.ColumnInfo = resources.GetString("Grid_Positions.ColumnInfo")
    Me.Grid_Positions.Cursor = System.Windows.Forms.Cursors.Default
    Me.Grid_Positions.DropMode = C1.Win.C1FlexGrid.DropModeEnum.Manual
    Me.Grid_Positions.Location = New System.Drawing.Point(3, 37)
    Me.Grid_Positions.Name = "Grid_Positions"
    Me.Grid_Positions.Rows.DefaultSize = 17
    Me.Grid_Positions.Size = New System.Drawing.Size(1327, 579)
    Me.Grid_Positions.TabIndex = 104
    Me.Grid_Positions.Tree.Column = 0
    Me.Grid_Positions.Tree.Style = CType(((C1.Win.C1FlexGrid.TreeStyleFlags.Lines Or C1.Win.C1FlexGrid.TreeStyleFlags.Symbols) _
                Or C1.Win.C1FlexGrid.TreeStyleFlags.ButtonBar), C1.Win.C1FlexGrid.TreeStyleFlags)
    '
    'RootMenu
    '
    Me.RootMenu.Location = New System.Drawing.Point(0, 0)
    Me.RootMenu.Name = "RootMenu"
    Me.RootMenu.Size = New System.Drawing.Size(1348, 24)
    Me.RootMenu.TabIndex = 107
    Me.RootMenu.Text = " "
    '
    'Form_StatusStrip
    '
    Me.Form_StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Form_ProgressBar, Me.Label_Status})
    Me.Form_StatusStrip.Location = New System.Drawing.Point(0, 896)
    Me.Form_StatusStrip.Name = "Form_StatusStrip"
    Me.Form_StatusStrip.Size = New System.Drawing.Size(1348, 22)
    Me.Form_StatusStrip.TabIndex = 108
    Me.Form_StatusStrip.Text = " "
    '
    'Form_ProgressBar
    '
    Me.Form_ProgressBar.Maximum = 20
    Me.Form_ProgressBar.Name = "Form_ProgressBar"
    Me.Form_ProgressBar.Size = New System.Drawing.Size(150, 17)
    Me.Form_ProgressBar.Step = 1
    Me.Form_ProgressBar.Visible = False
    '
    'Label_Status
    '
    Me.Label_Status.Name = "Label_Status"
    Me.Label_Status.Size = New System.Drawing.Size(10, 17)
    Me.Label_Status.Text = " "
    '
    'TabControl_Transactions
    '
    Me.TabControl_Transactions.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.TabControl_Transactions.Controls.Add(Me.Tab_Portfolio)
    Me.TabControl_Transactions.Controls.Add(Me.Tab_Transactions)
    Me.TabControl_Transactions.Location = New System.Drawing.Point(3, 102)
    Me.TabControl_Transactions.Name = "TabControl_Transactions"
    Me.TabControl_Transactions.SelectedIndex = 0
    Me.TabControl_Transactions.Size = New System.Drawing.Size(1342, 791)
    Me.TabControl_Transactions.TabIndex = 109
    '
    'Tab_Portfolio
    '
    Me.Tab_Portfolio.Controls.Add(Me.TabControl_SubFunds)
    Me.Tab_Portfolio.Controls.Add(Me.Label10)
    Me.Tab_Portfolio.Controls.Add(Me.edit_Warning)
    Me.Tab_Portfolio.Controls.Add(Me.Label1)
    Me.Tab_Portfolio.Controls.Add(Me.edit_Threshold)
    Me.Tab_Portfolio.Controls.Add(Me.Grid_Positions)
    Me.Tab_Portfolio.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Portfolio.Name = "Tab_Portfolio"
    Me.Tab_Portfolio.Padding = New System.Windows.Forms.Padding(3)
    Me.Tab_Portfolio.Size = New System.Drawing.Size(1334, 765)
    Me.Tab_Portfolio.TabIndex = 0
    Me.Tab_Portfolio.Text = "Portfolio"
    Me.Tab_Portfolio.UseVisualStyleBackColor = True
    '
    'TabControl_SubFunds
    '
    Me.TabControl_SubFunds.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.TabControl_SubFunds.Controls.Add(Me.Tab_Transfer)
    Me.TabControl_SubFunds.Controls.Add(Me.Tab_Valuation)
    Me.TabControl_SubFunds.Location = New System.Drawing.Point(3, 622)
    Me.TabControl_SubFunds.Name = "TabControl_SubFunds"
    Me.TabControl_SubFunds.SelectedIndex = 0
    Me.TabControl_SubFunds.Size = New System.Drawing.Size(1328, 140)
    Me.TabControl_SubFunds.TabIndex = 110
    '
    'Tab_Transfer
    '
    Me.Tab_Transfer.Controls.Add(Me.Panel1)
    Me.Tab_Transfer.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Transfer.Name = "Tab_Transfer"
    Me.Tab_Transfer.Padding = New System.Windows.Forms.Padding(3)
    Me.Tab_Transfer.Size = New System.Drawing.Size(1320, 114)
    Me.Tab_Transfer.TabIndex = 0
    Me.Tab_Transfer.Text = "Internal Transfer"
    Me.Tab_Transfer.UseVisualStyleBackColor = True
    '
    'Panel1
    '
    Me.Panel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Panel1.Controls.Add(Me.Check_ByISIN)
    Me.Panel1.Controls.Add(Me.Label_DragNewInstrument)
    Me.Panel1.Controls.Add(Me.Check_TransferAtZeroValue)
    Me.Panel1.Controls.Add(Me.Button_Transfer)
    Me.Panel1.Controls.Add(Me.Label9)
    Me.Panel1.Controls.Add(Me.edit_TransferQuantity)
    Me.Panel1.Controls.Add(Me.Combo_ToSubFund)
    Me.Panel1.Controls.Add(Me.Label8)
    Me.Panel1.Controls.Add(Me.Combo_FromSubFund)
    Me.Panel1.Controls.Add(Me.Label7)
    Me.Panel1.Controls.Add(Me.Combo_Instrument)
    Me.Panel1.Controls.Add(Me.Label6)
    Me.Panel1.Controls.Add(Me.Label5)
    Me.Panel1.Location = New System.Drawing.Point(2, 2)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(1317, 111)
    Me.Panel1.TabIndex = 107
    '
    'Check_ByISIN
    '
    Me.Check_ByISIN.BackColor = System.Drawing.SystemColors.Control
    Me.Check_ByISIN.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_ByISIN.Location = New System.Drawing.Point(597, 23)
    Me.Check_ByISIN.Name = "Check_ByISIN"
    Me.Check_ByISIN.Size = New System.Drawing.Size(46, 19)
    Me.Check_ByISIN.TabIndex = 154
    Me.Check_ByISIN.Text = "ISIN"
    Me.Check_ByISIN.UseVisualStyleBackColor = False
    '
    'Label_DragNewInstrument
    '
    Me.Label_DragNewInstrument.BackColor = System.Drawing.Color.LightYellow
    Me.Label_DragNewInstrument.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Label_DragNewInstrument.Location = New System.Drawing.Point(20, 21)
    Me.Label_DragNewInstrument.Name = "Label_DragNewInstrument"
    Me.Label_DragNewInstrument.Size = New System.Drawing.Size(104, 21)
    Me.Label_DragNewInstrument.TabIndex = 111
    Me.Label_DragNewInstrument.Text = "Instrument"
    Me.Label_DragNewInstrument.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'Check_TransferAtZeroValue
    '
    Me.Check_TransferAtZeroValue.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Check_TransferAtZeroValue.Location = New System.Drawing.Point(602, 75)
    Me.Check_TransferAtZeroValue.Name = "Check_TransferAtZeroValue"
    Me.Check_TransferAtZeroValue.Size = New System.Drawing.Size(188, 16)
    Me.Check_TransferAtZeroValue.TabIndex = 110
    Me.Check_TransferAtZeroValue.Text = "Transfer at Zero Value"
    Me.Check_TransferAtZeroValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Check_TransferAtZeroValue.UseVisualStyleBackColor = True
    '
    'Button_Transfer
    '
    Me.Button_Transfer.Location = New System.Drawing.Point(808, 46)
    Me.Button_Transfer.Name = "Button_Transfer"
    Me.Button_Transfer.Size = New System.Drawing.Size(123, 22)
    Me.Button_Transfer.TabIndex = 109
    Me.Button_Transfer.Text = "Transfer"
    Me.Button_Transfer.UseVisualStyleBackColor = True
    '
    'Label9
    '
    Me.Label9.Location = New System.Drawing.Point(599, 52)
    Me.Label9.Name = "Label9"
    Me.Label9.Size = New System.Drawing.Size(90, 16)
    Me.Label9.TabIndex = 108
    Me.Label9.Text = "Quantity or %"
    Me.Label9.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'edit_TransferQuantity
    '
    Me.edit_TransferQuantity.Location = New System.Drawing.Point(695, 49)
    Me.edit_TransferQuantity.Name = "edit_TransferQuantity"
    Me.edit_TransferQuantity.RenaissanceTag = Nothing
    Me.edit_TransferQuantity.SelectTextOnFocus = False
    Me.edit_TransferQuantity.Size = New System.Drawing.Size(95, 20)
    Me.edit_TransferQuantity.TabIndex = 107
    Me.edit_TransferQuantity.Text = "0.00"
    Me.edit_TransferQuantity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.edit_TransferQuantity.TextFormat = "#,##0.00"
    Me.edit_TransferQuantity.Value = 0
    '
    'Combo_ToSubFund
    '
    Me.Combo_ToSubFund.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_ToSubFund.Location = New System.Drawing.Point(132, 75)
    Me.Combo_ToSubFund.Name = "Combo_ToSubFund"
    Me.Combo_ToSubFund.Size = New System.Drawing.Size(459, 21)
    Me.Combo_ToSubFund.TabIndex = 92
    '
    'Label8
    '
    Me.Label8.Location = New System.Drawing.Point(20, 79)
    Me.Label8.Name = "Label8"
    Me.Label8.Size = New System.Drawing.Size(104, 16)
    Me.Label8.TabIndex = 93
    Me.Label8.Text = "To :"
    '
    'Combo_FromSubFund
    '
    Me.Combo_FromSubFund.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_FromSubFund.Location = New System.Drawing.Point(132, 48)
    Me.Combo_FromSubFund.Name = "Combo_FromSubFund"
    Me.Combo_FromSubFund.Size = New System.Drawing.Size(459, 21)
    Me.Combo_FromSubFund.TabIndex = 90
    '
    'Label7
    '
    Me.Label7.Location = New System.Drawing.Point(20, 52)
    Me.Label7.Name = "Label7"
    Me.Label7.Size = New System.Drawing.Size(104, 16)
    Me.Label7.TabIndex = 91
    Me.Label7.Text = "From :"
    '
    'Combo_Instrument
    '
    Me.Combo_Instrument.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Instrument.Location = New System.Drawing.Point(132, 21)
    Me.Combo_Instrument.Name = "Combo_Instrument"
    Me.Combo_Instrument.Size = New System.Drawing.Size(459, 21)
    Me.Combo_Instrument.TabIndex = 88
    '
    'Label6
    '
    Me.Label6.Location = New System.Drawing.Point(883, 21)
    Me.Label6.Name = "Label6"
    Me.Label6.Size = New System.Drawing.Size(104, 16)
    Me.Label6.TabIndex = 89
    Me.Label6.Text = "Instrument"
    '
    'Label5
    '
    Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label5.Location = New System.Drawing.Point(4, 3)
    Me.Label5.Name = "Label5"
    Me.Label5.Size = New System.Drawing.Size(104, 16)
    Me.Label5.TabIndex = 87
    Me.Label5.Text = "Internal Transfer :"
    '
    'Tab_Valuation
    '
    Me.Tab_Valuation.Controls.Add(Me.Check_ValuationsIncludeUnsettledFX)
    Me.Tab_Valuation.Controls.Add(Me.Grid_Valuations)
    Me.Tab_Valuation.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Valuation.Name = "Tab_Valuation"
    Me.Tab_Valuation.Padding = New System.Windows.Forms.Padding(3)
    Me.Tab_Valuation.Size = New System.Drawing.Size(1320, 114)
    Me.Tab_Valuation.TabIndex = 1
    Me.Tab_Valuation.Text = "Investment Value"
    Me.Tab_Valuation.UseVisualStyleBackColor = True
    '
    'Grid_Valuations
    '
    Me.Grid_Valuations.ColumnInfo = resources.GetString("Grid_Valuations.ColumnInfo")
    Me.Grid_Valuations.Location = New System.Drawing.Point(3, 3)
    Me.Grid_Valuations.Name = "Grid_Valuations"
    Me.Grid_Valuations.Rows.Count = 5
    Me.Grid_Valuations.Rows.DefaultSize = 17
    Me.Grid_Valuations.Size = New System.Drawing.Size(609, 108)
    Me.Grid_Valuations.TabIndex = 0
    '
    'Label10
    '
    Me.Label10.Location = New System.Drawing.Point(689, 9)
    Me.Label10.Name = "Label10"
    Me.Label10.Size = New System.Drawing.Size(140, 16)
    Me.Label10.TabIndex = 109
    Me.Label10.Text = "Warning Threshold"
    Me.Label10.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'edit_Warning
    '
    Me.edit_Warning.Location = New System.Drawing.Point(835, 6)
    Me.edit_Warning.Name = "edit_Warning"
    Me.edit_Warning.RenaissanceTag = Nothing
    Me.edit_Warning.SelectTextOnFocus = False
    Me.edit_Warning.Size = New System.Drawing.Size(81, 20)
    Me.edit_Warning.TabIndex = 108
    Me.edit_Warning.Text = "0.00%"
    Me.edit_Warning.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.edit_Warning.TextFormat = "#,##0.00%"
    Me.edit_Warning.Value = 0
    '
    'Label1
    '
    Me.Label1.Location = New System.Drawing.Point(368, 9)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(228, 16)
    Me.Label1.TabIndex = 106
    Me.Label1.Text = "Rebalance Threshold (%Fund)"
    Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'edit_Threshold
    '
    Me.edit_Threshold.Location = New System.Drawing.Point(602, 6)
    Me.edit_Threshold.Name = "edit_Threshold"
    Me.edit_Threshold.RenaissanceTag = Nothing
    Me.edit_Threshold.SelectTextOnFocus = False
    Me.edit_Threshold.Size = New System.Drawing.Size(81, 20)
    Me.edit_Threshold.TabIndex = 105
    Me.edit_Threshold.Text = "0.00%"
    Me.edit_Threshold.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.edit_Threshold.TextFormat = "#,##0.00%"
    Me.edit_Threshold.Value = 0
    '
    'Tab_Transactions
    '
    Me.Tab_Transactions.Controls.Add(Me.GroupBox2)
    Me.Tab_Transactions.Controls.Add(Me.Button_Transfer_Cancel)
    Me.Tab_Transactions.Controls.Add(Me.GroupBox1)
    Me.Tab_Transactions.Controls.Add(Me.Button_Transfer_Submit)
    Me.Tab_Transactions.Controls.Add(Me.Button_Transfer_SelectNone)
    Me.Tab_Transactions.Controls.Add(Me.Button_Transfer_SelectAll)
    Me.Tab_Transactions.Controls.Add(Me.Button_Trades_Submit)
    Me.Tab_Transactions.Controls.Add(Me.Button_Trades_Selectnone)
    Me.Tab_Transactions.Controls.Add(Me.Button_Trades_SelectAll)
    Me.Tab_Transactions.Controls.Add(Me.Label4)
    Me.Tab_Transactions.Controls.Add(Me.Label3)
    Me.Tab_Transactions.Controls.Add(Me.Grid_PendingTransfers)
    Me.Tab_Transactions.Controls.Add(Me.Grid_PendingTransactions)
    Me.Tab_Transactions.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Transactions.Name = "Tab_Transactions"
    Me.Tab_Transactions.Padding = New System.Windows.Forms.Padding(3)
    Me.Tab_Transactions.Size = New System.Drawing.Size(1334, 765)
    Me.Tab_Transactions.TabIndex = 1
    Me.Tab_Transactions.Text = "Transactions"
    Me.Tab_Transactions.UseVisualStyleBackColor = True
    '
    'GroupBox2
    '
    Me.GroupBox2.Controls.Add(Me.Radio_Transactions_Selected)
    Me.GroupBox2.Controls.Add(Me.Radio_Transactions_All)
    Me.GroupBox2.Location = New System.Drawing.Point(638, 0)
    Me.GroupBox2.Name = "GroupBox2"
    Me.GroupBox2.Size = New System.Drawing.Size(165, 34)
    Me.GroupBox2.TabIndex = 95
    Me.GroupBox2.TabStop = False
    '
    'Radio_Transactions_Selected
    '
    Me.Radio_Transactions_Selected.AutoSize = True
    Me.Radio_Transactions_Selected.Location = New System.Drawing.Point(77, 11)
    Me.Radio_Transactions_Selected.Name = "Radio_Transactions_Selected"
    Me.Radio_Transactions_Selected.Size = New System.Drawing.Size(67, 17)
    Me.Radio_Transactions_Selected.TabIndex = 1
    Me.Radio_Transactions_Selected.TabStop = True
    Me.Radio_Transactions_Selected.Text = "Selected"
    Me.Radio_Transactions_Selected.UseVisualStyleBackColor = True
    '
    'Radio_Transactions_All
    '
    Me.Radio_Transactions_All.AutoSize = True
    Me.Radio_Transactions_All.Location = New System.Drawing.Point(17, 11)
    Me.Radio_Transactions_All.Name = "Radio_Transactions_All"
    Me.Radio_Transactions_All.Size = New System.Drawing.Size(36, 17)
    Me.Radio_Transactions_All.TabIndex = 0
    Me.Radio_Transactions_All.TabStop = True
    Me.Radio_Transactions_All.Text = "All"
    Me.Radio_Transactions_All.UseVisualStyleBackColor = True
    '
    'Button_Transfer_Cancel
    '
    Me.Button_Transfer_Cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Button_Transfer_Cancel.Location = New System.Drawing.Point(813, 462)
    Me.Button_Transfer_Cancel.Name = "Button_Transfer_Cancel"
    Me.Button_Transfer_Cancel.Size = New System.Drawing.Size(123, 22)
    Me.Button_Transfer_Cancel.TabIndex = 94
    Me.Button_Transfer_Cancel.Text = "Cancel Transfers"
    Me.Button_Transfer_Cancel.UseVisualStyleBackColor = True
    '
    'GroupBox1
    '
    Me.GroupBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.GroupBox1.Controls.Add(Me.Radio_Transfer_Selected)
    Me.GroupBox1.Controls.Add(Me.Radio_Transfer_All)
    Me.GroupBox1.Location = New System.Drawing.Point(638, 453)
    Me.GroupBox1.Name = "GroupBox1"
    Me.GroupBox1.Size = New System.Drawing.Size(165, 34)
    Me.GroupBox1.TabIndex = 93
    Me.GroupBox1.TabStop = False
    '
    'Radio_Transfer_Selected
    '
    Me.Radio_Transfer_Selected.AutoSize = True
    Me.Radio_Transfer_Selected.Location = New System.Drawing.Point(77, 11)
    Me.Radio_Transfer_Selected.Name = "Radio_Transfer_Selected"
    Me.Radio_Transfer_Selected.Size = New System.Drawing.Size(67, 17)
    Me.Radio_Transfer_Selected.TabIndex = 1
    Me.Radio_Transfer_Selected.TabStop = True
    Me.Radio_Transfer_Selected.Text = "Selected"
    Me.Radio_Transfer_Selected.UseVisualStyleBackColor = True
    '
    'Radio_Transfer_All
    '
    Me.Radio_Transfer_All.AutoSize = True
    Me.Radio_Transfer_All.Location = New System.Drawing.Point(17, 11)
    Me.Radio_Transfer_All.Name = "Radio_Transfer_All"
    Me.Radio_Transfer_All.Size = New System.Drawing.Size(36, 17)
    Me.Radio_Transfer_All.TabIndex = 0
    Me.Radio_Transfer_All.TabStop = True
    Me.Radio_Transfer_All.Text = "All"
    Me.Radio_Transfer_All.UseVisualStyleBackColor = True
    '
    'Button_Transfer_Submit
    '
    Me.Button_Transfer_Submit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Button_Transfer_Submit.Location = New System.Drawing.Point(503, 462)
    Me.Button_Transfer_Submit.Name = "Button_Transfer_Submit"
    Me.Button_Transfer_Submit.Size = New System.Drawing.Size(123, 22)
    Me.Button_Transfer_Submit.TabIndex = 92
    Me.Button_Transfer_Submit.Text = "Enter Transfers"
    Me.Button_Transfer_Submit.UseVisualStyleBackColor = True
    '
    'Button_Transfer_SelectNone
    '
    Me.Button_Transfer_SelectNone.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Button_Transfer_SelectNone.Location = New System.Drawing.Point(250, 462)
    Me.Button_Transfer_SelectNone.Name = "Button_Transfer_SelectNone"
    Me.Button_Transfer_SelectNone.Size = New System.Drawing.Size(123, 22)
    Me.Button_Transfer_SelectNone.TabIndex = 91
    Me.Button_Transfer_SelectNone.Text = "Select None"
    Me.Button_Transfer_SelectNone.UseVisualStyleBackColor = True
    '
    'Button_Transfer_SelectAll
    '
    Me.Button_Transfer_SelectAll.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Button_Transfer_SelectAll.Location = New System.Drawing.Point(121, 462)
    Me.Button_Transfer_SelectAll.Name = "Button_Transfer_SelectAll"
    Me.Button_Transfer_SelectAll.Size = New System.Drawing.Size(123, 22)
    Me.Button_Transfer_SelectAll.TabIndex = 90
    Me.Button_Transfer_SelectAll.Text = "Select All"
    Me.Button_Transfer_SelectAll.UseVisualStyleBackColor = True
    '
    'Button_Trades_Submit
    '
    Me.Button_Trades_Submit.Location = New System.Drawing.Point(503, 8)
    Me.Button_Trades_Submit.Name = "Button_Trades_Submit"
    Me.Button_Trades_Submit.Size = New System.Drawing.Size(123, 22)
    Me.Button_Trades_Submit.TabIndex = 89
    Me.Button_Trades_Submit.Text = "Submit Trades"
    Me.Button_Trades_Submit.UseVisualStyleBackColor = True
    '
    'Button_Trades_Selectnone
    '
    Me.Button_Trades_Selectnone.Location = New System.Drawing.Point(250, 8)
    Me.Button_Trades_Selectnone.Name = "Button_Trades_Selectnone"
    Me.Button_Trades_Selectnone.Size = New System.Drawing.Size(123, 22)
    Me.Button_Trades_Selectnone.TabIndex = 88
    Me.Button_Trades_Selectnone.Text = "Select None"
    Me.Button_Trades_Selectnone.UseVisualStyleBackColor = True
    '
    'Button_Trades_SelectAll
    '
    Me.Button_Trades_SelectAll.Location = New System.Drawing.Point(121, 8)
    Me.Button_Trades_SelectAll.Name = "Button_Trades_SelectAll"
    Me.Button_Trades_SelectAll.Size = New System.Drawing.Size(123, 22)
    Me.Button_Trades_SelectAll.TabIndex = 87
    Me.Button_Trades_SelectAll.Text = "Select All"
    Me.Button_Trades_SelectAll.UseVisualStyleBackColor = True
    '
    'Label4
    '
    Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label4.Location = New System.Drawing.Point(6, 471)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(104, 16)
    Me.Label4.TabIndex = 86
    Me.Label4.Text = "Internal Transfers"
    '
    'Label3
    '
    Me.Label3.Location = New System.Drawing.Point(6, 34)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(104, 16)
    Me.Label3.TabIndex = 85
    Me.Label3.Text = "Market Orders"
    '
    'Grid_PendingTransfers
    '
    Me.Grid_PendingTransfers.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_PendingTransfers.AutoClipboard = True
    Me.Grid_PendingTransfers.CausesValidation = False
    Me.Grid_PendingTransfers.ColumnInfo = resources.GetString("Grid_PendingTransfers.ColumnInfo")
    Me.Grid_PendingTransfers.ContextMenuStrip = Me.ContextMenu_Transfers
    Me.Grid_PendingTransfers.EditOptions = C1.Win.C1FlexGrid.EditFlags.None
    Me.Grid_PendingTransfers.KeyActionTab = C1.Win.C1FlexGrid.KeyActionEnum.MoveAcross
    Me.Grid_PendingTransfers.Location = New System.Drawing.Point(6, 490)
    Me.Grid_PendingTransfers.Name = "Grid_PendingTransfers"
    Me.Grid_PendingTransfers.Rows.Count = 13
    Me.Grid_PendingTransfers.Rows.DefaultSize = 17
    Me.Grid_PendingTransfers.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.ListBox
    Me.Grid_PendingTransfers.Size = New System.Drawing.Size(1322, 269)
    Me.Grid_PendingTransfers.TabIndex = 20
    '
    'ContextMenu_Transfers
    '
    Me.ContextMenu_Transfers.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_EnterThisTransfer, Me.Menu_EnterAllTransfers, Me.ToolStripSeparator1, Me.Menu_RemoveThisTransfer, Me.Menu_RemoveALLTransfers})
    Me.ContextMenu_Transfers.Name = "ContextMenu_Transfers"
    Me.ContextMenu_Transfers.Size = New System.Drawing.Size(192, 98)
    '
    'Menu_EnterThisTransfer
    '
    Me.Menu_EnterThisTransfer.Name = "Menu_EnterThisTransfer"
    Me.Menu_EnterThisTransfer.Size = New System.Drawing.Size(191, 22)
    Me.Menu_EnterThisTransfer.Text = "Enter THIS Transfer"
    '
    'Menu_EnterAllTransfers
    '
    Me.Menu_EnterAllTransfers.Name = "Menu_EnterAllTransfers"
    Me.Menu_EnterAllTransfers.Size = New System.Drawing.Size(191, 22)
    Me.Menu_EnterAllTransfers.Text = "Enter ALL Transfers"
    '
    'ToolStripSeparator1
    '
    Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
    Me.ToolStripSeparator1.Size = New System.Drawing.Size(188, 6)
    '
    'Menu_RemoveThisTransfer
    '
    Me.Menu_RemoveThisTransfer.Name = "Menu_RemoveThisTransfer"
    Me.Menu_RemoveThisTransfer.Size = New System.Drawing.Size(191, 22)
    Me.Menu_RemoveThisTransfer.Text = "Remove THIS Transfer"
    '
    'Menu_RemoveALLTransfers
    '
    Me.Menu_RemoveALLTransfers.Name = "Menu_RemoveALLTransfers"
    Me.Menu_RemoveALLTransfers.Size = New System.Drawing.Size(191, 22)
    Me.Menu_RemoveALLTransfers.Text = "Remove ALL Transfers"
    '
    'Grid_PendingTransactions
    '
    Me.Grid_PendingTransactions.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_PendingTransactions.CausesValidation = False
    Me.Grid_PendingTransactions.ColumnInfo = resources.GetString("Grid_PendingTransactions.ColumnInfo")
    Me.Grid_PendingTransactions.EditOptions = C1.Win.C1FlexGrid.EditFlags.None
    Me.Grid_PendingTransactions.KeyActionTab = C1.Win.C1FlexGrid.KeyActionEnum.MoveAcross
    Me.Grid_PendingTransactions.Location = New System.Drawing.Point(6, 53)
    Me.Grid_PendingTransactions.Name = "Grid_PendingTransactions"
    Me.Grid_PendingTransactions.Rows.Count = 13
    Me.Grid_PendingTransactions.Rows.DefaultSize = 17
    Me.Grid_PendingTransactions.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.ListBox
    Me.Grid_PendingTransactions.Size = New System.Drawing.Size(1322, 393)
    Me.Grid_PendingTransactions.TabIndex = 19
    '
    'ContextMenu_Positions
    '
    Me.ContextMenu_Positions.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_ViewTransactions})
    Me.ContextMenu_Positions.Name = "ContextMenu_Positions"
    Me.ContextMenu_Positions.Size = New System.Drawing.Size(170, 26)
    '
    'Menu_ViewTransactions
    '
    Me.Menu_ViewTransactions.Name = "Menu_ViewTransactions"
    Me.Menu_ViewTransactions.Size = New System.Drawing.Size(169, 22)
    Me.Menu_ViewTransactions.Text = "View Transactions"
    '
    'ContextMenu_Transactions
    '
    Me.ContextMenu_Transactions.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_SubmitTransaction, Me.Menu_SubmitALLTransactions, Me.Menu_TransactionsSeparator, Me.Menu_CancelManualChange})
    Me.ContextMenu_Transactions.Name = "ContextMenu_Transactions"
    Me.ContextMenu_Transactions.Size = New System.Drawing.Size(262, 76)
    '
    'Menu_SubmitTransaction
    '
    Me.Menu_SubmitTransaction.Name = "Menu_SubmitTransaction"
    Me.Menu_SubmitTransaction.Size = New System.Drawing.Size(261, 22)
    Me.Menu_SubmitTransaction.Text = "Submit THIS Transaction"
    '
    'Menu_SubmitALLTransactions
    '
    Me.Menu_SubmitALLTransactions.Name = "Menu_SubmitALLTransactions"
    Me.Menu_SubmitALLTransactions.Size = New System.Drawing.Size(261, 22)
    Me.Menu_SubmitALLTransactions.Text = "Submit ALL Transactions"
    '
    'Menu_TransactionsSeparator
    '
    Me.Menu_TransactionsSeparator.Name = "Menu_TransactionsSeparator"
    Me.Menu_TransactionsSeparator.Size = New System.Drawing.Size(258, 6)
    '
    'Menu_CancelManualChange
    '
    Me.Menu_CancelManualChange.Name = "Menu_CancelManualChange"
    Me.Menu_CancelManualChange.Size = New System.Drawing.Size(261, 22)
    Me.Menu_CancelManualChange.Text = "Cancel Manual change to this trade"
    Me.Menu_CancelManualChange.Visible = False
    '
    'Check_ValuationsIncludeUnsettledFX
    '
    Me.Check_ValuationsIncludeUnsettledFX.AutoSize = True
    Me.Check_ValuationsIncludeUnsettledFX.Location = New System.Drawing.Point(618, 6)
    Me.Check_ValuationsIncludeUnsettledFX.Name = "Check_ValuationsIncludeUnsettledFX"
    Me.Check_ValuationsIncludeUnsettledFX.Size = New System.Drawing.Size(286, 17)
    Me.Check_ValuationsIncludeUnsettledFX.TabIndex = 1
    Me.Check_ValuationsIncludeUnsettledFX.Text = "Include Unsettled FX Trades in Open Forward FX totals"
    Me.Check_ValuationsIncludeUnsettledFX.UseVisualStyleBackColor = True
    '
    'frmManageSubFunds
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(1348, 918)
    Me.Controls.Add(Me.TabControl_Transactions)
    Me.Controls.Add(Me.Form_StatusStrip)
    Me.Controls.Add(Me.RootMenu)
    Me.Controls.Add(Me.Combo_SubFund)
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.Combo_TransactionFund)
    Me.Controls.Add(Me.label_CptyIsFund)
    Me.Name = "frmManageSubFunds"
    Me.Text = "Manage Sub Funds"
    CType(Me.Grid_Positions, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Form_StatusStrip.ResumeLayout(False)
    Me.Form_StatusStrip.PerformLayout()
    Me.TabControl_Transactions.ResumeLayout(False)
    Me.Tab_Portfolio.ResumeLayout(False)
    Me.Tab_Portfolio.PerformLayout()
    Me.TabControl_SubFunds.ResumeLayout(False)
    Me.Tab_Transfer.ResumeLayout(False)
    Me.Panel1.ResumeLayout(False)
    Me.Panel1.PerformLayout()
    Me.Tab_Valuation.ResumeLayout(False)
    Me.Tab_Valuation.PerformLayout()
    CType(Me.Grid_Valuations, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_Transactions.ResumeLayout(False)
    Me.GroupBox2.ResumeLayout(False)
    Me.GroupBox2.PerformLayout()
    Me.GroupBox1.ResumeLayout(False)
    Me.GroupBox1.PerformLayout()
    CType(Me.Grid_PendingTransfers, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ContextMenu_Transfers.ResumeLayout(False)
    CType(Me.Grid_PendingTransactions, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ContextMenu_Positions.ResumeLayout(False)
    Me.ContextMenu_Transactions.ResumeLayout(False)
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "

  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
  Private WithEvents _MainForm As VeniceMain

  ' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
  Private FormTooltip As New ToolTip()

  ' Form Menu


  ' Form Constants, specific to the table being updated.

    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
  Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

  ' Form Locals, initialised on 'New' defining what standard data items to use
    ''' <summary>
  ''' Variable holding the form's data table name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_TABLENAME As String
    ''' <summary>
  ''' Variable holding the form's data adaptor name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_ADAPTORNAME As String
    ''' <summary>
  ''' Variable holding the form's dataset name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_DATASETNAME As String


  ' The standard ChangeID for this form. e.g. tblFund
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

  ' Form Specific Order fields
    ''' <summary>
  ''' Field Name to order the Select combo by.
    ''' </summary>
  Private THIS_FORM_OrderBy As String

  ' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
  Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
  Private THIS_FORM_FormID As VeniceFormID

  ' Data Structures

    ''' <summary>
    ''' My connection
    ''' </summary>
  Private myConnection As SqlConnection

    ''' <summary>
    ''' The transaction dataset
    ''' </summary>
  Private TransactionDataset As DataSet
    ''' <summary>
    ''' The transaction table
    ''' </summary>
  Private TransactionTable As DataTable
    ''' <summary>
    ''' The transaction data view
    ''' </summary>
  Private TransactionDataView As DataView
    ''' <summary>
    ''' The contra transaction dataset
    ''' </summary>
  Private ContraTransactionDataset As RenaissanceDataClass.DSTransactionContra
    ''' <summary>
    ''' The contra transaction table
    ''' </summary>
  Private ContraTransactionTable As RenaissanceDataClass.DSTransactionContra.tblTransactionContraDataTable
    ''' <summary>
    ''' The contra transaction data view
    ''' </summary>
  Private ContraTransactionDataView As DataView

    ''' <summary>
    ''' The sub fund heirarchy table
    ''' </summary>
  Private SubFundHeirarchyTable As DataTable = Nothing
    ''' <summary>
    ''' The sub fund dictionary
    ''' </summary>
  Private subFundDictionary As New Dictionary(Of Integer, DataRow)
    ''' <summary>
    ''' The FX map
    ''' </summary>
  Private FXMap As New Dictionary(Of Integer, Double)
    ''' <summary>
    ''' The grid sub fund map
    ''' </summary>
  Private gridSubFundMap As New Dictionary(Of Integer, C1.Win.C1FlexGrid.Row)
    ''' <summary>
    ''' The grid_ positions_ updates
    ''' </summary>
  Private Grid_Positions_Updates As New Dictionary(Of Integer, Dictionary(Of UInteger, PositionChangesClass))
    ''' <summary>
    ''' The grid_ transaction_ updates
    ''' </summary>
  Private Grid_Transaction_Updates As New Dictionary(Of Integer, Dictionary(Of UInteger, TransactionChangesClass))
    ''' <summary>
    ''' The grid_ positions_ additional instruments
    ''' </summary>
  Private Grid_Positions_AdditionalInstruments As New Dictionary(Of Integer, Dictionary(Of Integer, Boolean))
    ''' <summary>
    ''' The grid_ transfer_ trades
    ''' </summary>
  Private Grid_Transfer_Trades As New ArrayList()

    ''' <summary>
    ''' The _ trade status dictionary
    ''' </summary>
  Friend _TradeStatusDictionary As New LookupCollection(Of Integer, String)     ' Dictionary(Of Integer, String)
    ''' <summary>
    ''' The _last selected fund
    ''' </summary>
  Private _lastSelectedFund As Integer = 0

    ''' <summary>
    ''' The last entered transfers grid row
    ''' </summary>
  Private LastEnteredTransfersGridRow As Integer = 0
    ''' <summary>
    ''' The last entered transfers grid col
    ''' </summary>
  Private LastEnteredTransfersGridCol As Integer = 0
    ''' <summary>
    ''' The last entered transactions grid row
    ''' </summary>
  Private LastEnteredTransactionsGridRow As Integer = 0
    ''' <summary>
    ''' The last entered transactions grid col
    ''' </summary>
  Private LastEnteredTransactionsGridCol As Integer = 0
    ''' <summary>
    ''' The last entered positions grid row
    ''' </summary>
  Private LastEnteredPositionsGridRow As Integer = 0
    ''' <summary>
    ''' The last entered positions grid col
    ''' </summary>
  Private LastEnteredPositionsGridCol As Integer = 0

    ''' <summary>
    ''' The fund valuations
    ''' </summary>
  Private FundValuations As New Dictionary(Of Integer, Double)

    ''' <summary>
    ''' The this standard dataset
    ''' </summary>
  Private ThisStandardDataset As RenaissanceGlobals.StandardDataset

    ''' <summary>
    ''' The instruments DS
    ''' </summary>
  Private InstrumentsDS As RenaissanceDataClass.DSInstrument
    ''' <summary>
    ''' The best prices table
    ''' </summary>
  Private BestPricesTable As DataTable
    ''' <summary>
    ''' The sorted prices
    ''' </summary>
  Private SortedPrices As DataRow()

  ' Active Element.

    ''' <summary>
    ''' The __ is over cancel button
    ''' </summary>
  Private __IsOverCancelButton As Boolean
    ''' <summary>
    ''' The _ in use
    ''' </summary>
  Private _InUse As Boolean

  ' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
  Private FormIsValid As Boolean
    ''' <summary>
    ''' The in paint
    ''' </summary>
  Private InPaint As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
  Private _FormOpenFailed As Boolean

  ' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
  Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
  Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
  Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
  Private HasDeletePermission As Boolean

    ''' <summary>
    ''' Class GridColumns
    ''' </summary>
  Private Class GridColumns
        ''' <summary>
        ''' The first col
        ''' </summary>
    Public Shared FirstCol As Integer = 0
        ''' <summary>
        ''' The ISIN
        ''' </summary>
    Public Shared ISIN As Integer = 1
        ''' <summary>
        ''' The local value
        ''' </summary>
    Public Shared LocalValue As Integer = 2
        ''' <summary>
        ''' The local currency
        ''' </summary>
    Public Shared LocalCurrency As Integer = 3
        ''' <summary>
        ''' The fund value
        ''' </summary>
    Public Shared FundValue As Integer = 4
        ''' <summary>
        ''' The actual total fund weight
        ''' </summary>
    Public Shared ActualTotalFundWeight As Integer = 5
        ''' <summary>
        ''' The actual sub fund weight
        ''' </summary>
    Public Shared ActualSubFundWeight As Integer = 6
        ''' <summary>
        ''' The target sub fund weight
        ''' </summary>
    Public Shared TargetSubFundWeight As Integer = 7
        ''' <summary>
        ''' The force
        ''' </summary>
    Public Shared Force As Integer = 8
        ''' <summary>
        ''' The ignore
        ''' </summary>
    Public Shared Ignore As Integer = 9
        ''' <summary>
        ''' The trade
        ''' </summary>
    Public Shared Trade As Integer = 10
        ''' <summary>
        ''' The type
        ''' </summary>
    Public Shared Type As Integer = 11
        ''' <summary>
        ''' The units
        ''' </summary>
    Public Shared Units As Integer = 12
        ''' <summary>
        ''' The price
        ''' </summary>
    Public Shared Price As Integer = 13
        ''' <summary>
        ''' The FX to fund
        ''' </summary>
    Public Shared PriceDate As Integer = 14
    ''' <summary>
    ''' The FX to fund
    ''' </summary>
    Public Shared FXToFund As Integer = 15
        ''' <summary>
        ''' The parent ID
        ''' </summary>
    Public Shared ParentID As Integer = 16
        ''' <summary>
        ''' The fund ID
        ''' </summary>
    Public Shared FundID As Integer = 17
        ''' <summary>
        ''' The sub fund ID
        ''' </summary>
    Public Shared SubFundID As Integer = 18
        ''' <summary>
        ''' The instrument ID
        ''' </summary>
    Public Shared InstrumentID As Integer = 19
        ''' <summary>
        ''' The counter
        ''' </summary>
    Public Shared Counter As Integer = 20
        ''' <summary>
        ''' The modified
        ''' </summary>
    Public Shared Modified As Integer = 21

        ''' <summary>
        ''' Init_s the grid columns class.
        ''' </summary>
        ''' <param name="Grid_Positions">The grid_ positions.</param>
    Public Shared Sub Init_GridColumnsClass(ByVal Grid_Positions As C1.Win.C1FlexGrid.C1FlexGrid)
      Try
        GridColumns.FirstCol = Grid_Positions.Cols("FirstCol").SafeIndex
        GridColumns.ISIN = Grid_Positions.Cols("col_ISIN").SafeIndex
        GridColumns.LocalValue = Grid_Positions.Cols("LocalValue").SafeIndex
        GridColumns.FundValue = Grid_Positions.Cols("FundValue").SafeIndex
        GridColumns.LocalCurrency = Grid_Positions.Cols("LocalCurrency").SafeIndex
        GridColumns.Units = Grid_Positions.Cols("Units").SafeIndex
        GridColumns.Type = Grid_Positions.Cols("col_Type").SafeIndex
        GridColumns.Price = Grid_Positions.Cols("Price").SafeIndex
        GridColumns.PriceDate = Grid_Positions.Cols("PriceDate").SafeIndex
        GridColumns.Force = Grid_Positions.Cols("col_Force").SafeIndex
        GridColumns.Ignore = Grid_Positions.Cols("col_Ignore").SafeIndex
        GridColumns.Trade = Grid_Positions.Cols("col_Trade").SafeIndex
        GridColumns.TargetSubFundWeight = Grid_Positions.Cols("TargetSubFundWeight").SafeIndex
        GridColumns.ActualSubFundWeight = Grid_Positions.Cols("ActualSubFundWeight").SafeIndex
        GridColumns.ActualTotalFundWeight = Grid_Positions.Cols("ActualTotalFundWeight").SafeIndex
        GridColumns.FXToFund = Grid_Positions.Cols("FXToFund").SafeIndex
        GridColumns.ParentID = Grid_Positions.Cols("ParentID").SafeIndex
        GridColumns.FundID = Grid_Positions.Cols("FundID").SafeIndex
        GridColumns.SubFundID = Grid_Positions.Cols("SubFundID").SafeIndex
        GridColumns.InstrumentID = Grid_Positions.Cols("InstrumentID").SafeIndex
        GridColumns.Counter = Grid_Positions.Cols("Counter").SafeIndex
        GridColumns.Modified = Grid_Positions.Cols("Modified").SafeIndex
      Catch ex As Exception
      End Try

    End Sub
  End Class

    ''' <summary>
    ''' Class PositionChangesClass
    ''' </summary>
  Private Class PositionChangesClass

        ''' <summary>
        ''' The _ target weight
        ''' </summary>
    Public _TargetWeight As Double
        ''' <summary>
        ''' The _ target weight_ changed
        ''' </summary>
    Public _TargetWeight_Changed As Boolean
        ''' <summary>
        ''' The _ force
        ''' </summary>
    Public _Force As Boolean
        ''' <summary>
        ''' The _ force_ changed
        ''' </summary>
    Public _Force_Changed As Boolean
        ''' <summary>
        ''' The _ ignore
        ''' </summary>
    Public _Ignore As Boolean
        ''' <summary>
        ''' The _ ignore_ changed
        ''' </summary>
    Public _Ignore_Changed As Boolean

        ''' <summary>
        ''' Initializes a new instance of the <see cref="PositionChangesClass"/> class.
        ''' </summary>
    Public Sub New()
      _TargetWeight = 0.0#
      _TargetWeight_Changed = False
      _Force = False
      _Force_Changed = False
      _Ignore = False
      _Ignore_Changed = False
    End Sub

        ''' <summary>
        ''' Gets or sets the target weight.
        ''' </summary>
        ''' <value>The target weight.</value>
    Public Property TargetWeight() As Double
      Get
        Return _TargetWeight
      End Get
      Set(ByVal value As Double)
        _TargetWeight = value
        _TargetWeight_Changed = True
      End Set
    End Property

        ''' <summary>
        ''' Gets or sets a value indicating whether [target weight_ changed].
        ''' </summary>
        ''' <value><c>true</c> if [target weight_ changed]; otherwise, <c>false</c>.</value>
    Public Property TargetWeight_Changed() As Boolean
      Get
        Return _TargetWeight_Changed
      End Get
      Set(ByVal value As Boolean)
        _TargetWeight_Changed = value
        _TargetWeight = 0.0#
      End Set
    End Property

        ''' <summary>
        ''' Gets or sets a value indicating whether this <see cref="PositionChangesClass"/> is force.
        ''' </summary>
        ''' <value><c>true</c> if force; otherwise, <c>false</c>.</value>
    Public Property Force() As Boolean
      Get
        Return _Force
      End Get
      Set(ByVal value As Boolean)
        _Force = value
        _Force_Changed = True
      End Set
    End Property

        ''' <summary>
        ''' Gets or sets a value indicating whether [force_ changed].
        ''' </summary>
        ''' <value><c>true</c> if [force_ changed]; otherwise, <c>false</c>.</value>
    Public Property Force_Changed() As Boolean
      Get
        Return _Force_Changed
      End Get
      Set(ByVal value As Boolean)
        _Force_Changed = value
        _Force = 0.0#
      End Set
    End Property

        ''' <summary>
        ''' Gets or sets a value indicating whether this <see cref="PositionChangesClass"/> is ignore.
        ''' </summary>
        ''' <value><c>true</c> if ignore; otherwise, <c>false</c>.</value>
    Public Property Ignore() As Boolean
      Get
        Return _Ignore
      End Get
      Set(ByVal value As Boolean)
        _Ignore = value
        _Ignore_Changed = True
      End Set
    End Property

        ''' <summary>
        ''' Gets or sets a value indicating whether [ignore_ changed].
        ''' </summary>
        ''' <value><c>true</c> if [ignore_ changed]; otherwise, <c>false</c>.</value>
    Public Property Ignore_Changed() As Boolean
      Get
        Return _Ignore_Changed
      End Get
      Set(ByVal value As Boolean)
        _Ignore_Changed = value
        _Ignore = 0.0#
      End Set
    End Property

  End Class

    ''' <summary>
    ''' Class TransactionChangesClass
    ''' </summary>
  Private Class TransactionChangesClass

        ''' <summary>
        ''' The _ transaction amount
        ''' </summary>
    Public _TransactionAmount As Double
        ''' <summary>
        ''' The _ transaction amount_ changed
        ''' </summary>
    Public _TransactionAmount_Changed As Boolean
        ''' <summary>
        ''' The _ transaction price
        ''' </summary>
    Public _TransactionPrice As Double
        ''' <summary>
        ''' The _ transaction price_ changed
        ''' </summary>
    Public _TransactionPrice_Changed As Boolean
        ''' <summary>
        ''' The _ transaction settlement
        ''' </summary>
    Public _TransactionSettlement As Double
        ''' <summary>
        ''' The _ transaction settlement_ changed
        ''' </summary>
    Public _TransactionSettlement_Changed As Boolean
        ''' <summary>
        ''' The _ FX to fund currency
        ''' </summary>
    Public _FXToFundCurrency As Boolean
        ''' <summary>
        ''' The _ FX to fund currency_ changed
        ''' </summary>
    Public _FXToFundCurrency_Changed As Boolean
        ''' <summary>
        ''' The _ trade status ID
        ''' </summary>
    Public _TradeStatusID As Integer
        ''' <summary>
        ''' The _ trade status I d_ changed
        ''' </summary>
    Public _TradeStatusID_Changed As Boolean

        ''' <summary>
        ''' Initializes a new instance of the <see cref="TransactionChangesClass"/> class.
        ''' </summary>
    Public Sub New()
      _TransactionAmount = 0.0#
      _TransactionAmount_Changed = False
      _TransactionPrice = 0.0#
      _TransactionPrice_Changed = False
      _FXToFundCurrency = False
      _FXToFundCurrency_Changed = False
      _TradeStatusID = TradeStatus.PendingMO
      _TradeStatusID_Changed = False
    End Sub

        ''' <summary>
        ''' Gets or sets the transaction amount.
        ''' </summary>
        ''' <value>The transaction amount.</value>
    Public Property TransactionAmount() As Double
      Get
        Return _TransactionAmount
      End Get
      Set(ByVal value As Double)
        _TransactionAmount = value
        _TransactionAmount_Changed = True
        _TransactionSettlement = 0.0#
        _TransactionSettlement_Changed = False
      End Set
    End Property

        ''' <summary>
        ''' Gets or sets a value indicating whether [transaction amount_ changed].
        ''' </summary>
        ''' <value><c>true</c> if [transaction amount_ changed]; otherwise, <c>false</c>.</value>
    Public Property TransactionAmount_Changed() As Boolean
      Get
        Return _TransactionAmount_Changed
      End Get
      Set(ByVal value As Boolean)
        _TransactionAmount_Changed = value
        _TransactionAmount = 0.0#

        If value Then
          _TransactionSettlement_Changed = False
          _TransactionSettlement = 0.0#
        End If
      End Set
    End Property

        ''' <summary>
        ''' Gets or sets the transaction settlement.
        ''' </summary>
        ''' <value>The transaction settlement.</value>
    Public Property TransactionSettlement() As Double
      Get
        Return _TransactionSettlement
      End Get
      Set(ByVal value As Double)
        _TransactionSettlement = value
        _TransactionSettlement_Changed = True
        _TransactionAmount = 0.0#
        _TransactionAmount_Changed = False
      End Set
    End Property

        ''' <summary>
        ''' Gets or sets a value indicating whether [transaction settlement_ changed].
        ''' </summary>
        ''' <value><c>true</c> if [transaction settlement_ changed]; otherwise, <c>false</c>.</value>
    Public Property TransactionSettlement_Changed() As Boolean
      Get
        Return _TransactionSettlement_Changed
      End Get
      Set(ByVal value As Boolean)
        _TransactionSettlement_Changed = value
        _TransactionSettlement = 0.0#

        If value Then
          _TransactionAmount_Changed = False
          _TransactionAmount = 0.0#
        End If
      End Set
    End Property

        ''' <summary>
        ''' Gets or sets the transaction price.
        ''' </summary>
        ''' <value>The transaction price.</value>
    Public Property TransactionPrice() As Double
      Get
        Return _TransactionPrice
      End Get
      Set(ByVal value As Double)
        _TransactionPrice = value
        _TransactionPrice_Changed = True
      End Set
    End Property

        ''' <summary>
        ''' Gets or sets a value indicating whether [transaction price_ changed].
        ''' </summary>
        ''' <value><c>true</c> if [transaction price_ changed]; otherwise, <c>false</c>.</value>
    Public Property TransactionPrice_Changed() As Boolean
      Get
        Return _TransactionPrice_Changed
      End Get
      Set(ByVal value As Boolean)
        _TransactionPrice_Changed = value
        _TransactionPrice = 0.0#
      End Set
    End Property

        ''' <summary>
        ''' Gets or sets a value indicating whether [FX to fund currency].
        ''' </summary>
        ''' <value><c>true</c> if [FX to fund currency]; otherwise, <c>false</c>.</value>
    Public Property FXToFundCurrency() As Boolean
      Get
        Return _FXToFundCurrency
      End Get
      Set(ByVal value As Boolean)
        _FXToFundCurrency = value
        _FXToFundCurrency_Changed = True
      End Set
    End Property

        ''' <summary>
        ''' Gets or sets a value indicating whether [FX to fund currency_ changed].
        ''' </summary>
        ''' <value><c>true</c> if [FX to fund currency_ changed]; otherwise, <c>false</c>.</value>
    Public Property FXToFundCurrency_Changed() As Boolean
      Get
        Return _FXToFundCurrency_Changed
      End Get
      Set(ByVal value As Boolean)
        _FXToFundCurrency_Changed = value
        _FXToFundCurrency = 0.0#
      End Set
    End Property

        ''' <summary>
        ''' Gets or sets the trade status ID.
        ''' </summary>
        ''' <value>The trade status ID.</value>
    Public Property TradeStatusID() As Integer
      Get
        Return _TradeStatusID
      End Get
      Set(ByVal value As Integer)
        _TradeStatusID = value
        _TradeStatusID_Changed = True
      End Set
    End Property

        ''' <summary>
        ''' Gets or sets a value indicating whether [trade status I d_ changed].
        ''' </summary>
        ''' <value><c>true</c> if [trade status I d_ changed]; otherwise, <c>false</c>.</value>
    Public Property TradeStatusID_Changed() As Boolean
      Get
        Return _TradeStatusID_Changed
      End Get
      Set(ByVal value As Boolean)
        _TradeStatusID_Changed = value
        _FXToFundCurrency = TradeStatus.PendingMO
      End Set
    End Property

  End Class

    ''' <summary>
    ''' Class PendingTransfer
    ''' </summary>
  Private Class PendingTransfer
        ''' <summary>
        ''' The _ counter
        ''' </summary>
    Private Shared _Counter As Integer = 1

        ''' <summary>
        ''' The _ transfer ID
        ''' </summary>
    Private _TransferID As Integer

        ''' <summary>
        ''' From fund ID
        ''' </summary>
    Public FromFundID As Integer
        ''' <summary>
        ''' To fund ID
        ''' </summary>
    Public ToFundID As Integer
        ''' <summary>
        ''' From sub fund ID
        ''' </summary>
    Public FromSubFundID As Integer
        ''' <summary>
        ''' To sub fund ID
        ''' </summary>
    Public ToSubFundID As Integer
        ''' <summary>
        ''' The instrument ID
        ''' </summary>
    Public InstrumentID As Integer
        ''' <summary>
        ''' The quantity
        ''' </summary>
    Public Quantity As Double
        ''' <summary>
        ''' The valuation price
        ''' </summary>
    Public ValuationPrice As Double
        ''' <summary>
        ''' The trade price
        ''' </summary>
    Public TradePrice As Double
        ''' <summary>
        ''' The settlement
        ''' </summary>
    Public Settlement As Double
        ''' <summary>
        ''' The currency
        ''' </summary>
    Public Currency As String
        ''' <summary>
        ''' The is net
        ''' </summary>
    Public IsNet As Boolean

        ''' <summary>
        ''' Initializes a new instance of the <see cref="PendingTransfer"/> class.
        ''' </summary>
    Public Sub New()
      Me._TransferID = PendingTransfer._Counter
      PendingTransfer._Counter += 1

      FromFundID = 0
      ToFundID = 0
      FromSubFundID = 0
      ToSubFundID = 0
      InstrumentID = 0
      Quantity = 0.0#
      ValuationPrice = 0.0#
      TradePrice = 0.0#
      Settlement = 0.0#
      Currency = ""
      IsNet = False

    End Sub

        ''' <summary>
        ''' Gets the transfer ID.
        ''' </summary>
        ''' <value>The transfer ID.</value>
    Public ReadOnly Property TransferID() As Integer
      Get
        Return _TransferID
      End Get
    End Property

  End Class

  ''' <summary>
  ''' Class PositionSummaryClass
  ''' </summary>
  Private Class PositionSummaryClass
    ''' <summary>
    ''' The fund ID
    ''' </summary>
    Public FundID As Integer
    ''' <summary>
    ''' The fund name
    ''' </summary>
    Public FundName As String
    ''' <summary>
    ''' The sub fund ID
    ''' </summary>
    Public SubFundID As Integer
    ''' <summary>
    ''' The sub fund name
    ''' </summary>
    Public SubFundName As String
    ''' <summary>
    ''' The sub fund level
    ''' </summary>
    Public SubFundLevel As Integer
    ''' <summary>
    ''' The instrument ID
    ''' </summary>
    Public InstrumentID As UInteger
    ''' <summary>
    ''' The instrument description
    ''' </summary>
    Public InstrumentDescription As String
    ''' <summary>
    ''' The instrument ISIN
    ''' </summary>
    Public InstrumentISIN As String
    ''' <summary>
    ''' The instrument type
    ''' </summary>
    Public InstrumentType As Integer
    ''' <summary>
    ''' The instrument currency ID
    ''' </summary>
    Public InstrumentCurrencyID As Integer
    ''' <summary>
    ''' The instrument price
    ''' </summary>
    Public InstrumentPrice As Double
    ''' <summary>
    ''' The instrument Price Date
    ''' </summary>
    Public InstrumentPriceDate As Date
    ''' <summary>
    ''' The FX to fund
    ''' </summary>
    Public FXToFund As Double
    ''' <summary>
    ''' The parent ID
    ''' </summary>
    Public ParentID As Integer
    ''' <summary>
    ''' The counter
    ''' </summary>
    Public Counter As Integer
    ''' <summary>
    ''' The instrument currency
    ''' </summary>
    Public InstrumentCurrency As String
    ''' <summary>
    ''' The instrument position
    ''' </summary>
    Public InstrumentPosition As Double
    ''' <summary>
    ''' The instrument local value
    ''' </summary>
    Public InstrumentLocalValue As Double
    ''' <summary>
    ''' The instrument fund value
    ''' </summary>
    Public InstrumentFundValue As Double
    ''' <summary>
    ''' The instrument sub fund target weight
    ''' </summary>
    Public InstrumentSubFundTargetWeight As Double
    ''' <summary>
    ''' The instrument sub fund actual weight
    ''' </summary>
    Public InstrumentSubFundActualWeight As Double
    ''' <summary>
    ''' The instrument fund actual weight
    ''' </summary>
    Public InstrumentFundActualWeight As Double
    ''' <summary>
    ''' The tree order
    ''' </summary>
    Public TreeOrder As Integer
    ''' <summary>
    ''' The basket component
    ''' </summary>
    Public BasketComponent As Boolean
    ''' <summary>
    ''' The is transfer
    ''' </summary>
    Public IsTransfer As Boolean
    ''' <summary>
    ''' The is deleted
    ''' </summary>
    Public IsDeleted As Boolean

    ''' <summary>
    ''' Initializes a new instance of the <see cref="PositionSummaryClass"/> class.
    ''' </summary>
    Public Sub New()
      FundID = 0
      FundName = ""
      SubFundID = 0
      SubFundName = ""
      SubFundLevel = 0
      InstrumentID = 0
      InstrumentDescription = ""
      InstrumentISIN = ""
      InstrumentType = 0
      InstrumentCurrencyID = 0
      InstrumentPrice = 0.0#
      InstrumentPriceDate = Renaissance_BaseDate
      FXToFund = 1.0#
      ParentID = 0
      Counter = 0
      InstrumentCurrency = ""
      InstrumentPosition = 0.0#
      InstrumentLocalValue = 0.0#
      InstrumentFundValue = 0.0#
      InstrumentSubFundTargetWeight = 0.0#
      InstrumentSubFundActualWeight = 0.0#
      InstrumentFundActualWeight = 0.0#
      BasketComponent = False
      IsTransfer = False
      IsDeleted = False

      TreeOrder = 0
    End Sub

  End Class

    ''' <summary>
    ''' Class PositionSummaryComparer
    ''' </summary>
  Private Class PositionSummaryComparer
    ' **********************************************************************************
    ' Custom Comparer calss for sorting PositionSummaryClass objects by TreeOrder, InstrumentDescription and InstrumentID
    '
    ' **********************************************************************************

    Implements IComparer

        ''' <summary>
        ''' Compares two objects and returns a value indicating whether one is less than, equal to, or greater than the other.
        ''' </summary>
        ''' <param name="x">The first object to compare.</param>
        ''' <param name="y">The second object to compare.</param>
        ''' <returns>Value Condition Less than zero <paramref name="x" /> is less than <paramref name="y" />. Zero <paramref name="x" /> equals <paramref name="y" />. Greater than zero <paramref name="x" /> is greater than <paramref name="y" />.</returns>
    Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare
      Dim X_Instrument As PositionSummaryClass
      Dim Y_Instrument As PositionSummaryClass

      Dim RVal As Integer = 0

      Try
        If (TypeOf x Is PositionSummaryClass) Then
          X_Instrument = CType(x, PositionSummaryClass)
        Else
          Return 0
        End If

        If (TypeOf y Is PositionSummaryClass) Then
          Y_Instrument = CType(y, PositionSummaryClass)
        Else
          Return 0
        End If

        RVal = X_Instrument.TreeOrder.CompareTo(Y_Instrument.TreeOrder)

        If RVal = 0 Then
          RVal = X_Instrument.InstrumentDescription.CompareTo(Y_Instrument.InstrumentDescription)

          If RVal = 0 Then
            RVal = X_Instrument.InstrumentID.CompareTo(Y_Instrument.InstrumentID)

            If RVal = 0 Then
              RVal = X_Instrument.IsTransfer.CompareTo(Y_Instrument.IsTransfer)
            End If
          End If
        End If

      Catch ex As Exception

        Return 0
      End Try

      Return RVal

    End Function
  End Class


#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
  Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
    ' Public property to return handle to the 'Main' Venice form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
  Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
    ' Public property maintaining a value indicating if the cursor is over the 'Cancel'
    ' Button on this form.
    ' This property is specifically designed for use by the field formating Event functions
    ' In order that they do not impose format restrictions if the user is about to click the 
    ' 'Cancel' button.
    '
    Get
      Return __IsOverCancelButton
    End Get
    Set(ByVal Value As Boolean)
      __IsOverCancelButton = Value
    End Set
  End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
  Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
    Get
      Return InPaint
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
    Get
      Return _InUse
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmManageSubFunds"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  Public Sub New(ByVal pMainForm As VeniceMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()

    _MainForm = pMainForm
    AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

    _FormOpenFailed = False
    _InUse = True

    ' Default Select and Order fields.

    THIS_FORM_OrderBy = "TransactionFund, TransactionInstrument"

    ' Form Permissioning :-

    THIS_FORM_PermissionArea = "frmManageSubFunds"
    THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

    ' 'This' form ID

    THIS_FORM_FormID = VeniceFormID.frmManageSubFunds

    ' This form's dataset type.

    ThisStandardDataset = RenaissanceStandardDatasets.tblTransaction ' This Defines the Form Data !!! 


    ' Data object names standard to this Form type.

    THIS_TABLENAME = ThisStandardDataset.TableName
    THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
    THIS_DATASETNAME = ThisStandardDataset.DatasetName

    THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

    ' Establish / Retrieve data objects for this form.

    myConnection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)
    TransactionDataset = MainForm.Load_Table(ThisStandardDataset, False)
    TransactionTable = TransactionDataset.Tables(0)

    ContraTransactionDataset = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblTransactionContra, False), RenaissanceDataClass.DSTransactionContra)
    ContraTransactionTable = ContraTransactionDataset.tblTransactionContra

    Try
      ' Grid :- Grid_Positions

      Grid_Positions.Rows.Count = 1
      Grid_Positions.Cols.Fixed = 0
      '    Grid_Positions.ExtendLastCol = True

      'styles
      Dim cs As CellStyle = Grid_Positions.Styles.Normal
      cs.Border.Direction = BorderDirEnum.Vertical
      cs.WordWrap = False

      cs = Grid_Positions.Styles.Add("Data")
      cs.BackColor = SystemColors.Info
      cs.ForeColor = Color.Black

      cs = Grid_Positions.Styles.Add("DataNegative")
      cs.BackColor = SystemColors.Info
      cs.ForeColor = Color.Red

      cs = Grid_Positions.Styles.Add("SourceNode")
      cs.BackColor = Color.Yellow
      cs.Font = New Font(Grid_Positions.Font, FontStyle.Bold)

      cs = Grid_Positions.Styles.Add("NumberPositive")
      cs.ForeColor = Color.Black

      cs = Grid_Positions.Styles.Add("NumberNegative")
      cs.ForeColor = Color.Red

      cs = Grid_Positions.Styles.Add("NumberPositiveHighlight")
      cs.ForeColor = Color.Black
      cs.BackColor = Color.LightYellow

      cs = Grid_Positions.Styles.Add("NumberNegativeHighlight")
      cs.ForeColor = Color.Red
      cs.BackColor = Color.LightYellow

      cs = Grid_Positions.Styles.Add("FundNumberPositive")
      cs.ForeColor = Color.Black
      cs.Font = New Font(Grid_Positions.Font, FontStyle.Bold)

      cs = Grid_Positions.Styles.Add("FundNumberNegative")
      cs.ForeColor = Color.Red
      cs.Font = New Font(Grid_Positions.Font, FontStyle.Bold)

      cs = Grid_Positions.Styles.Add("FundNumberPositiveHighlight")
      cs.ForeColor = Color.Black
      cs.BackColor = Color.LightYellow
      cs.Font = New Font(Grid_Positions.Font, FontStyle.Bold)

      cs = Grid_Positions.Styles.Add("FundNumberNegativeHighlight")
      cs.ForeColor = Color.Red
      cs.BackColor = Color.LightYellow
      cs.Font = New Font(Grid_Positions.Font, FontStyle.Bold)

      cs = Grid_Positions.Styles.Add("NumberPositiveWarning")
      cs.ForeColor = Color.Black
      cs.BackColor = Color.MistyRose
      cs.Border.Color = Color.Red
      cs.Border.Style = BorderStyleEnum.Flat
      cs.Border.Direction = BorderDirEnum.Both
      cs.Border.Width = 2
      cs.Font = New Font(Grid_Positions.Font, FontStyle.Bold)

      cs = Grid_Positions.Styles.Add("NumberNegativeWarning")
      cs.ForeColor = Color.Red
      cs.BackColor = Color.MistyRose
      cs.Border.Color = Color.Red
      cs.Border.Style = BorderStyleEnum.Flat
      cs.Border.Style = BorderStyleEnum.Flat
      cs.Border.Direction = BorderDirEnum.Both
      cs.Border.Width = 2
      cs.Font = New Font(Grid_Positions.Font, FontStyle.Bold)

      cs = Grid_Positions.Styles.Add("Hidden")
      cs.Display = DisplayEnum.None

      cs = Grid_Positions.Styles.Add("Visible")

      'outline tree
      Grid_Positions.Tree.Column = 0
      Grid_Positions.AllowMerging = AllowMergingEnum.Nodes

      'other
      Grid_Positions.AllowResizing = AllowResizingEnum.Columns
      Grid_Positions.SelectionMode = SelectionModeEnum.Cell

      Call GridColumns.Init_GridColumnsClass(Grid_Positions)

      Me.Grid_Positions.Cols(GridColumns.FXToFund).Visible = False
      Me.Grid_Positions.Cols(GridColumns.ParentID).Visible = False
      Me.Grid_Positions.Cols(GridColumns.FundID).Visible = False
      Me.Grid_Positions.Cols(GridColumns.SubFundID).Visible = False
      Me.Grid_Positions.Cols(GridColumns.InstrumentID).Visible = False
      Me.Grid_Positions.Cols(GridColumns.Counter).Visible = False
      Me.Grid_Positions.Cols(GridColumns.Modified).Visible = False

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error initialising Sub Funds Form.", ex.StackTrace, True)
    End Try

    ' Initialise Grid_PendingTransactions

    Try
      ' Grid :- Grid_PendingTransactions

      Grid_PendingTransactions.Rows.Count = 1
      Grid_PendingTransactions.Cols.Fixed = 0

      'styles
      Dim cs As CellStyle = Grid_PendingTransactions.Styles.Normal
      'cs.Border.Direction = BorderDirEnum.Vertical
      cs.WordWrap = False

      cs = Grid_PendingTransactions.Styles.Add("NumberPositive")
      cs.ForeColor = Color.Black

      cs = Grid_PendingTransactions.Styles.Add("NumberNegative")
      cs.ForeColor = Color.Red

      cs = Grid_PendingTransactions.Styles.Add("NumberPositiveHighlight")
      cs.ForeColor = Color.Black
      cs.BackColor = Color.LightYellow

      cs = Grid_PendingTransactions.Styles.Add("NumberNegativeHighlight")
      cs.ForeColor = Color.Red
      cs.BackColor = Color.LightYellow

      Grid_PendingTransactions.Cols("TradeStatusID").DataMap = _TradeStatusDictionary

    Catch ex As Exception

    End Try

    Try
      ' Grid :- Grid_Valuations

      Grid_Valuations.Rows.Count = 1
      Grid_Valuations.Cols.Fixed = 0

      'styles
      Dim cs As CellStyle = Grid_Valuations.Styles.Normal
      'cs.Border.Direction = BorderDirEnum.Vertical
      cs.WordWrap = False

      cs = Grid_Valuations.Styles.Add("NumberPositive")
      cs.ForeColor = Color.Black

      cs = Grid_Valuations.Styles.Add("NumberNegative")
      cs.ForeColor = Color.Red

    Catch ex As Exception

    End Try

    ' Establish initial DataView and Initialise Transactions Grig.

    Try
      TransactionDataView = New DataView(TransactionTable, "True", "", DataViewRowState.CurrentRows)
      ContraTransactionDataView = New DataView(ContraTransactionTable, "True", "", DataViewRowState.CurrentRows)
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error initialising Sub Funds Form.", ex.StackTrace, True)
    End Try

    ' Form Control Changed events

    Try

      AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

      AddHandler Combo_TransactionFund.SelectedValueChanged, AddressOf Me.FormControlChanged
      AddHandler Combo_SubFund.SelectedValueChanged, AddressOf Me.FormControlChanged

      AddHandler Combo_TransactionFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
      AddHandler Combo_SubFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
      'AddHandler Combo_Instrument.SelectedValueChanged, AddressOf Me.FormControlChanged
      'AddHandler Combo_FromSubFund.SelectedValueChanged, AddressOf Me.FormControlChanged
      'AddHandler Combo_ToSubFund.SelectedValueChanged, AddressOf Me.FormControlChanged

      AddHandler Combo_TransactionFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
      AddHandler Combo_SubFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
      AddHandler Combo_Instrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
      AddHandler Combo_FromSubFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
      AddHandler Combo_ToSubFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

      AddHandler Combo_TransactionFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
      AddHandler Combo_SubFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
      AddHandler Combo_Instrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
      AddHandler Combo_FromSubFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
      AddHandler Combo_ToSubFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

      AddHandler Combo_TransactionFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
      AddHandler Combo_SubFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
      AddHandler Combo_Instrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
      AddHandler Combo_FromSubFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
      AddHandler Combo_ToSubFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error initialising Sub Funds Form.", ex.StackTrace, True)
    End Try

    ' Set up the ToolTip
    MainForm.SetFormToolTip(Me, FormTooltip)

    InstrumentsDS = Nothing
    BestPricesTable = Nothing

    Try
      Grid_Positions.Cols("TargetSubFundWeight").Editor = New CustomC1PercentageTextBox
    Catch ex As Exception
    End Try

    Try
      Grid_PendingTransactions.Cols("Amount").Editor = New CustomC1NumericTextBox
      Grid_PendingTransactions.Cols("Price").Editor = New CustomC1NumericTextBox
      Grid_PendingTransactions.Cols("Settlement").Editor = New CustomC1NumericTextBox
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error initialising Sub Funds Form.", ex.StackTrace, True)
    End Try


  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
  Public Sub ResetForm() Implements StandardVeniceForm.ResetForm
    THIS_FORM_OrderBy = "TransactionFund, TransactionInstrument"

    Call Form_Load(Me, New System.EventArgs)
  End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
  Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
    ALWAYS_CLOSE_THIS_FORM = True
    Me.Close()
  End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
    _FormOpenFailed = False
    _InUse = True

    ' Initialise Data structures. Connection, Adaptor and Dataset.

    If Not (MainForm Is Nothing) Then
      FormIsValid = True
    Else
      MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (myConnection Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (TransactionDataset Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If


    ' Check User permissions
    Try
      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form.", "", True)

        FormIsValid = False
        _FormOpenFailed = True

        Exit Sub
      End If
    Catch ex As Exception
      FormIsValid = False
      _FormOpenFailed = True

      Exit Sub
    End Try


    Try
      ' Initialse form

      InPaint = True
      IsOverCancelButton = False

      ' Initialise main select controls
      ' Build Sorted data list from which this form operates

      Call getSubFundHeirarchy()
      Call getFXRates(MainForm.Main_Knowledgedate.Date) ' Should give FXs for current date unless Knowledge date is set.
      Call SetFundCombo()
      Call SetInstrumentCombo()

      Me.Combo_TransactionFund.SelectedIndex = -1

      Call SetSubFundCombos()
      Call SetTradeStatusCombos()

      Me.Combo_SubFund.SelectedIndex = -1

      Call MainForm.SetComboSelectionLengths(Me)

      Me.Check_TransferAtZeroValue.Checked = False

      edit_Threshold.Value = 0.01#
      edit_Warning.Value = 0.2#
      TabControl_Transactions.SelectedIndex = 0
      Radio_Transfer_Selected.Checked = False
      Radio_Transfer_All.Checked = True
      Radio_Transactions_All.Checked = False
      Radio_Transactions_Selected.Checked = True

      TabControl_SubFunds.SelectedTab = Tab_Transfer

      Call SetSortedRows()

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error initialising Sub Funds Form.", ex.StackTrace, True)
    Finally
      InPaint = False
    End Try


  End Sub

    ''' <summary>
    ''' Handles the Closing event of the frm control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
  Private Sub frm_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    Dim HideForm As Boolean

    ' Hide or Close this form ?
    ' All depends on how many of this form type are Open or in Cache...

    _InUse = False

    If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
      HideForm = False
    Else
      HideForm = True
      If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
        HideForm = False
      End If
    End If

    If HideForm = True Then
      MainForm.HideInFormsCollection(Me)
      Me.Hide() ' NPP Fix

      e.Cancel = True
    Else
      Try
        MainForm.RemoveFromFormsCollection(Me)
        RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

        RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

        RemoveHandler Combo_TransactionFund.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_SubFund.SelectedValueChanged, AddressOf Me.FormControlChanged

        RemoveHandler Combo_TransactionFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_SubFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        RemoveHandler Combo_TransactionFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_SubFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

        RemoveHandler Combo_TransactionFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_SubFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

        RemoveHandler Combo_TransactionFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_SubFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

      Catch ex As Exception
      End Try
    End If

  End Sub


#End Region


  ' Routine to handle changes / updates to tables by this and other windows.
  ' If this, or any other, form posts a change to a table, then it will invoke an update event 
  ' detailing what tables have been altered.
  ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
  ' the 'VeniceAutoUpdate' event of the main Venice form.
  ' Each form may them react as appropriate to changes in any table that might impact it.
  '
    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    Dim OrgInPaint As Boolean
    Dim KnowledgeDateChanged As Boolean
    Dim RefreshGrid As Boolean

    If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

    OrgInPaint = InPaint

    Try
      InPaint = True
      KnowledgeDateChanged = False
      RefreshGrid = False

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
        KnowledgeDateChanged = True
      End If
    Catch ex As Exception
    End Try

    ' ****************************************************************
    ' Check for changes relevant to this form
    ' ****************************************************************

    ' Changes to the tblPrice table :-
    ' Clear 'Best Prices' cache.

    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblPrice) = True) Or KnowledgeDateChanged Then
        Call GetInstrumentPrice(-1) ' Flush Static Variables

        If (BestPricesTable IsNot Nothing) Then
          BestPricesTable.Clear()
        End If
        BestPricesTable = Nothing

        RefreshGrid = True
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblPrice", ex.StackTrace, True)
    End Try

    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFX) = True) Or KnowledgeDateChanged Then
        Call getFXRates(MainForm.Main_Knowledgedate.Date) ' Should give FXs for current date unless Knowledge date is set.
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblFX", ex.StackTrace, True)
    End Try

    ' Changes to the tblFund table :-

    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFund) = True) Or KnowledgeDateChanged Then
        Call SetFundCombo()
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblFund", ex.StackTrace, True)
    End Try

    ' Changes to the tblInstrument table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrument) = True) Or KnowledgeDateChanged Then
        InstrumentsDS = Nothing
        Call SetInstrumentCombo()

        RefreshGrid = True
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblInstrument", ex.StackTrace, True)
    End Try

    ' Changes to the tblSubFund table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblSubFund) = True) Or KnowledgeDateChanged Then
        getSubFundHeirarchy()
        Call SetSubFundCombos()

        ' Clear the grid for SubFund changes. The Draw routine does not handle Sub Funds disappearing or moving.
        gridsubFundMap.Clear() ' Important, if setting grid rows to 1 !!!
        Grid_Positions.Rows.Count = 1

        RefreshGrid = True
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblSubFund", ex.StackTrace, True)
    End Try

    ' Changes to the tblTradeStatus table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblTradeStatus) = True) Or KnowledgeDateChanged Then

        Call SetTradeStatusCombos()

        RefreshGrid = True
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblSubFund", ex.StackTrace, True)
    End Try

    ' Changes to the tblTransaction table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblTransaction) = True) Or KnowledgeDateChanged Then

        ' RefreshGrid = True ' Wait for the tblTransactionContra Update. Code is written such that the tblTransaction update should arrive before the tblTransactionContra update.
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblTransaction", ex.StackTrace, True)
    End Try

    ' Changes to the tblTransactionContra table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblTransactionContra) = True) Or KnowledgeDateChanged Then

        RefreshGrid = True
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblTransaction", ex.StackTrace, True)
    End Try

    ' Changes to the tblGroupList table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGroupList) = True) Or KnowledgeDateChanged Then
        RefreshGrid = True
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblGroupList", ex.StackTrace, True)
    End Try

    ' Changes to the tblGroupItems table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGroupItems) = True) Or KnowledgeDateChanged Then
        RefreshGrid = True
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblGroupItems", ex.StackTrace, True)
    End Try

    ' Changes to the tblGroupItemData table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGroupItemData) = True) Or KnowledgeDateChanged Then
        RefreshGrid = True
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblGroupItemData", ex.StackTrace, True)
    End Try


    ' Changes to the KnowledgeDate :-
    Try
      If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
        RefreshGrid = True
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: KnowledgeDate", ex.StackTrace, True)
    End Try


    ' Changes to the tblUserPermissions table :-

    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

        ' Check ongoing permissions.

        Call CheckPermissions()
        If (HasReadPermission = False) Then
          Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

          FormIsValid = False
          InPaint = OrgInPaint
          Me.Close()
          Exit Sub
        End If

        RefreshGrid = True

      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: tblUserPermissions", ex.StackTrace, True)
    End Try


    ' ****************************************************************
    ' Changes to the Main FORM table :-
    ' (or the tblTransactions table)
    ' ****************************************************************

    Try
      If (RefreshGrid = True) Or _
        KnowledgeDateChanged Then

        ' Re-Set Controls etc.
        Call SetSortedRows(True)

      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: Main Form table", ex.StackTrace, True)
    End Try

    InPaint = OrgInPaint

  End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

    ''' <summary>
    ''' Gets the FX to fund.
    ''' </summary>
    ''' <param name="InstrumentCurrencyID">The instrument currency ID.</param>
    ''' <param name="FundCurrencyID">The fund currency ID.</param>
    ''' <returns>System.Double.</returns>
  Private Function GetFXToFund(ByVal InstrumentCurrencyID As Integer, ByVal FundCurrencyID As Integer) As Double
    ' *******************************************************************************
    '
    ' *******************************************************************************
    Dim RVal As Double = 1.0#

    Try
      Dim InstrumentFX As Double = 1.0#
      Dim FundFX As Double = 1.0#

      If (InstrumentCurrencyID = FundCurrencyID) Then
        Return 1.0#
      End If

      If (FXMap IsNot Nothing) Then

        If (FXMap.ContainsKey(InstrumentCurrencyID)) Then
          InstrumentFX = FXMap(InstrumentCurrencyID)
        End If

        If (FXMap.ContainsKey(FundCurrencyID)) Then
          FundFX = FXMap(FundCurrencyID)
        End If

        RVal = InstrumentFX / FundFX

      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in GetFXToFund()", ex.StackTrace, True)
    End Try

    Return RVal

  End Function

    ''' <summary>
    ''' Gets the FX rates.
    ''' </summary>
    ''' <param name="ValueDate">The value date.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function getFXRates(ByVal ValueDate As Date) As Boolean
    ' *******************************************************************************
    '
    ' *******************************************************************************
    Dim FXTable As New DataTable
    Dim FXRow As DataRow
    Dim DBConnection As SqlConnection = Nothing
    Dim ThisCurrency As Integer

    Try
      If (ValueDate <= RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW) Then
        ValueDate = Now.Date
      End If

      If (FXMap Is Nothing) Then
        FXMap = New Dictionary(Of Integer, Double)
      End If

      FXMap.Clear()

      Dim thisSelectCommand As New SqlCommand

      Try
        DBConnection = MainForm.GetVeniceConnection()

        thisSelectCommand.Connection = MainForm.GetVeniceConnection()
        thisSelectCommand.CommandType = CommandType.StoredProcedure
        thisSelectCommand.CommandText = "adp_tblBestFX_SelectCommand"
        thisSelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT
        thisSelectCommand.Parameters.Add(New SqlParameter("@ValueDate", SqlDbType.DateTime)).Value = ValueDate
        thisSelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", SqlDbType.DateTime)).Value = MainForm.Main_Knowledgedate

        MainForm.LoadTable_Custom(FXTable, thisSelectCommand)

      Catch ex As Exception
        MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error loading Sub-Fund Tree in getFXRates().", ex.StackTrace, True)

      Finally
        If (DBConnection IsNot Nothing) AndAlso (DBConnection.State And ConnectionState.Open) Then
          DBConnection.Close()
        End If
        thisSelectCommand.Connection = Nothing
      End Try

      If (FXTable IsNot Nothing) AndAlso (FXTable.Rows.Count > 0) Then
        For Each FXRow In FXTable.Rows
          If (IsNumeric(FXRow("FXCurrencyCode"))) Then
            ThisCurrency = CInt(FXRow("FXCurrencyCode"))

            If (FXMap.ContainsKey(ThisCurrency) = False) Then
              FXMap.Add(ThisCurrency, CDbl(FXRow("FXRate")))
            End If
          End If
        Next
      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in getFXRates()", ex.StackTrace, True)
    End Try

    Return True

  End Function

    ''' <summary>
    ''' Gets the sub fund heirarchy.
    ''' </summary>
    ''' <returns>Dictionary{System.Int32DataRow}.</returns>
  Private Function getSubFundHeirarchy() As Dictionary(Of Integer, DataRow)
    ' *******************************************************************************
    '
    ' *******************************************************************************
    ' Dim SubFundDS As DSSubFund
    Dim SubFundRow As DataRow
    Dim DBConnection As SqlConnection = Nothing

    Try
      ' Initialise

      subFundDictionary.Clear()
      If (SubFundHeirarchyTable Is Nothing) Then
        SubFundHeirarchyTable = New DataTable
      Else
        SubFundHeirarchyTable.Clear()
      End If

      '' Get Sub Fund table
      'SubFundDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblSubFund)

      'If (SubFundDS Is Nothing) Then
      '  Return subFundDictionary
      'End If

      'SyncLock SubFundDS
      '  MainForm.CopyTable(SubFundDS.tblSubFund, SubFundTable)
      'End SyncLock

      ' Get Sub Fund Tree
      Dim thisSelectCommand As New SqlCommand

      Try
        DBConnection = MainForm.GetVeniceConnection()

        thisSelectCommand.Connection = DBConnection
        thisSelectCommand.CommandType = CommandType.StoredProcedure
        thisSelectCommand.CommandText = "adp_tblSubFund_Heirarchy"
        thisSelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT
        thisSelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", SqlDbType.DateTime)).Value = MainForm.Main_Knowledgedate

        MainForm.LoadTable_Custom(SubFundHeirarchyTable, thisSelectCommand)
        'SubFundHeirarchyTable.Load(thisSelectCommand.ExecuteReader)

      Catch ex As Exception
        MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error loading Sub-Fund Tree in getSubFundHeirarchy().", ex.StackTrace, True)

      Finally
        If (DBConnection IsNot Nothing) AndAlso (DBConnection.State And ConnectionState.Open) Then
          DBConnection.Close()
        End If
        thisSelectCommand.Connection = Nothing
      End Try

      If (SubFundHeirarchyTable.Rows.Count > 0) Then
        For Each SubFundRow In SubFundHeirarchyTable.Rows
          subFundDictionary.Add(CInt(SubFundRow("EquivalentFundID")), SubFundRow)
        Next
      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in getSubFundHeirarchy()", ex.StackTrace, True)
    End Try

    Return subFundDictionary

  End Function

    ''' <summary>
    ''' Gets the transaction select string.
    ''' </summary>
    ''' <param name="OnlyUsertblTransactionFields">if set to <c>true</c> [only usertbl transaction fields].</param>
    ''' <returns>System.String.</returns>
  Private Function GetTransactionSelectString(Optional ByVal OnlyUsertblTransactionFields As Boolean = False) As String
    ' *******************************************************************************
    ' Build a Select String appropriate to the form status.
    ' *******************************************************************************

    Dim SelectString As String

    SelectString = ""
    MainForm.SetToolStripText(Label_Status, "")

    If (TransactionDataset Is Nothing) Then
      Return "True"
      Exit Function
    End If

    Try

      ' Select on Fund...

      If IsFieldTransactionField("TransactionFund") Then
        SelectString = "(TransactionFund=" & Nz(Combo_TransactionFund.SelectedValue, 0).ToString & ")"
      End If

      '' Select on Sub Fund

      'If (Me.Combo_SubFund.SelectedIndex > 0) Then
      '  If IsFieldTransactionField("TransactionSubFund") = True Then
      '    If (SelectString.Length > 0) Then
      '      SelectString &= " AND "
      '    End If
      '    SelectString &= "(TransactionSubFund=" & Combo_SubFund.SelectedValue.ToString & ")"
      '  End If
      'End If

      If SelectString.Length <= 0 Then
        SelectString = "true"
      End If

    Catch ex As Exception
      SelectString = "true"
    End Try

    Return SelectString

  End Function

    ''' <summary>
    ''' Determines whether [is field transaction field] [the specified field name].
    ''' </summary>
    ''' <param name="FieldName">Name of the field.</param>
    ''' <returns><c>true</c> if [is field transaction field] [the specified field name]; otherwise, <c>false</c>.</returns>
  Private Function IsFieldTransactionField(ByVal FieldName As String) As Boolean
    ' *******************************************************************************
    ' Simple function to return a boolean value indicating whether or not a column name is
    ' part of the tblTransactions table.
    ' *******************************************************************************

    Try
      Dim TransactionTable As New RenaissanceDataClass.DSTransaction.tblTransactionDataTable

      If (TransactionTable Is Nothing) Then
        Return False
      Else
        Return TransactionTable.Columns.Contains(FieldName)
      End If
    Catch ex As Exception
      Return (False)
    End Try

    Return (False)

  End Function

    ''' <summary>
    ''' Sets the sorted rows.
    ''' </summary>
    ''' <param name="ForceRefresh">if set to <c>true</c> [force refresh].</param>
  Private Sub SetSortedRows(Optional ByVal ForceRefresh As Boolean = False)
    ' *******************************************************************************
    ' Build a Select String appropriate to the form status and apply it to the DataView object.
    ' *******************************************************************************

    Dim SelectString As String

    SelectString = GetTransactionSelectString()

    If (TransactionDataView Is Nothing) Then
      Exit Sub
    End If

    Try
      ' If (Not (TransactionDataView.RowFilter = SelectString)) Or (ForceRefresh = True) Then
      If ForceRefresh = True Then
        TransactionDataView.RowFilter = "False"
        ContraTransactionDataView.RowFilter = "False"
      End If

      TransactionDataView.RowFilter = SelectString
      ContraTransactionDataView.RowFilter = SelectString
      PaintPositionsGrid()

      ' End If
    Catch ex As Exception
      Try
        TransactionDataView.RowFilter = "true"
        ContraTransactionDataView.RowFilter = "true"
      Catch ex_inner As Exception
      End Try
    End Try

    Me.Label_Status.Text = "(" & TransactionDataView.Count.ToString & " Records) " & SelectString

  End Sub


  ' Check User permissions
  ''' <summary>
  ''' Checks the permissions.
  ''' </summary>
  Private Sub CheckPermissions()
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Dim Permissions As Integer

    Try
      Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

      HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
      HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
      HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
      HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in CheckPermissions()", ex.StackTrace, True)
      HasReadPermission = False
      HasUpdatePermission = False
      HasInsertPermission = False
      HasDeletePermission = False
    End Try

  End Sub

  ''' <summary>
  ''' Basic event handler, called when a form value is changed.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' *******************************************************************************
    ' In the event of one of the Form Controls changing, refresh the data grid.
    ' *******************************************************************************

    If InPaint = False Then
      Try
        InPaint = True

        If (TypeOf sender Is ComboBox) Then
          MainForm.GenericCombo_SelectedIndexChanged(sender, Nothing)
        End If

        If sender Is Combo_TransactionFund Then
          ' Clear Updates history.

          Dim thisSelectedFund As Integer = 0

          If (IsNumeric(Combo_TransactionFund.SelectedValue)) Then
            thisSelectedFund = CInt(Combo_TransactionFund.SelectedValue)
          End If

          If (Grid_Positions_Updates.Count > 0) OrElse (Grid_Transaction_Updates.Count > 0) OrElse (Grid_Transfer_Trades.Count > 0) Then
            ' Prompt to save ?

            If MessageBox.Show("There are unsaved Transfers or Transactions." & vbCrLf & "Do you wish to change fund and lose these changes ?", "Lose Changes?", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Cancel Then
              Combo_TransactionFund.SelectedValue = _lastSelectedFund
              Exit Sub
            End If

          End If

          If (thisSelectedFund <> _lastSelectedFund) Then
            Grid_Positions_Updates.Clear()
            Grid_Transaction_Updates.Clear()
            Grid_Transfer_Trades.Clear()
            Grid_Positions_AdditionalInstruments.Clear()
          End If

          _lastSelectedFund = thisSelectedFund

          Call SetSubFundCombos()
        End If

      Catch ex As Exception
        MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in FormControlChanged()", ex.StackTrace, True)
      Finally
        InPaint = False
      End Try

      Call SetSortedRows()

      Grid_Positions.Focus()

    End If
  End Sub

  ''' <summary>
  ''' Handles the KeyUp event of the Combo_FieldSelect control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
  Private Sub Combo_FieldSelect_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
    ' *******************************************************************************
    ' In the event of one of the Form Controls changing, refresh the data grid.
    ' *******************************************************************************

    If InPaint = False Then

      Call SetSortedRows()

    End If
  End Sub


  ''' <summary>
  ''' Gets the field change ID.
  ''' </summary>
  ''' <param name="pFieldName">Name of the p field.</param>
  ''' <returns>RenaissanceChangeID.</returns>
  Private Function GetFieldChangeID(ByVal pFieldName As String) As RenaissanceChangeID
    Dim tblRefIntegrity As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityDataTable
    Dim IntegrityRows() As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityRow

    If (pFieldName.EndsWith(".")) Then
      pFieldName = "Transaction" & pFieldName.Substring(0, pFieldName.Length - 1)
    End If

    Try
      tblRefIntegrity = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblReferentialIntegrity, False), RenaissanceDataClass.DSReferentialIntegrity).tblReferentialIntegrity
      IntegrityRows = tblRefIntegrity.Select("(FeedsTable LIKE '%tblSelectTransaction') AND (FeedsField = '" & pFieldName & "')", "RN")
      If (IntegrityRows.Length <= 0) Then
        Return RenaissanceChangeID.None
        Exit Function
      End If

    Catch ex As Exception
      Return RenaissanceChangeID.None
      Exit Function
    End Try


    If (IntegrityRows(0).IsDescriptionFieldNull) Then
      Return RenaissanceChangeID.None
      Exit Function
    End If

    ' OK, a referential record exists.
    ' Determine the Table and Field Names to use to build the Value Combo.

    Dim TableName As String
    Dim TableField As String
    Dim DescriptionField As String
    Dim thisChangeID As RenaissanceChangeID

    Try

      TableName = IntegrityRows(0).TableName
      TableField = IntegrityRows(0).TableField
      DescriptionField = IntegrityRows(0).DescriptionField

      thisChangeID = CType(System.Enum.Parse(GetType(RenaissanceChangeID), TableName), RenaissanceChangeID)

      Return thisChangeID
      Exit Function
    Catch ex As Exception
    End Try

    Return RenaissanceChangeID.None
    Exit Function

  End Function

#End Region

#Region " Set Form Combos (Form Specific Code) "

  ''' <summary>
  ''' Sets the fund combo.
  ''' </summary>
  Private Sub SetFundCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_TransactionFund, _
    RenaissanceStandardDatasets.tblFund, _
    "FundName", _
    "FundID", _
    "FundClosed=0", False, True, True)   ' 

  End Sub

  ''' <summary>
  ''' Sets the trade status combos.
  ''' </summary>
  Private Sub SetTradeStatusCombos()

    Try
      Dim TradeStatusDS As DSTradeStatus
      Dim TradeStatusRow As DSTradeStatus.tblTradeStatusRow

      TradeStatusDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblTradeStatus)

      _TradeStatusDictionary.Clear()

      For Each TradeStatusRow In TradeStatusDS.tblTradeStatus
        _TradeStatusDictionary.Add(TradeStatusRow.TradeStatusID, TradeStatusRow.TradeStatusName)
      Next

    Catch ex As Exception
    Finally
      _TradeStatusDictionary.Sort()
    End Try



  End Sub

  ''' <summary>
  ''' Sets the sub fund combos.
  ''' </summary>
  Private Sub SetSubFundCombos()

    Dim OrgInPaint As Boolean = InPaint
    Try
      Dim FundID As Integer = 0

      InPaint = True

      If (IsNumeric(Combo_TransactionFund.SelectedValue)) Then
        FundID = CInt(Combo_TransactionFund.SelectedValue)
      End If

      Call MainForm.SetTblGenericCombo( _
      Me.Combo_SubFund, _
      SubFundHeirarchyTable.Select("TopFundID=" & FundID.ToString(), "SubFundName"), _
      "SubFundName", _
      "SubFundID", _
      "", False, True, True)   ' 

      Call MainForm.SetTblGenericCombo( _
      Me.Combo_FromSubFund, _
      SubFundHeirarchyTable.Select("TopFundID=" & FundID.ToString(), "SubFundName"), _
      "SubFundName", _
      "SubFundID", _
      "", False, True, True)   ' 

      Call MainForm.SetTblGenericCombo( _
      Me.Combo_ToSubFund, _
      SubFundHeirarchyTable.Select("TopFundID=" & FundID.ToString(), "SubFundName"), _
      "SubFundName", _
      "SubFundID", _
      "", False, True, True)   ' 

    Catch ex As Exception
    Finally
      InPaint = OrgInPaint
    End Try

  End Sub

  ''' <summary>
  ''' Sets the instrument combo.
  ''' </summary>
  Private Sub SetInstrumentCombo()

    Try
      Dim FundID As Integer = 0
      If (Combo_TransactionFund.SelectedIndex >= 0) AndAlso (CInt(Nz(Combo_TransactionFund.SelectedValue, 0)) > 0) Then

        FundID = CInt(Combo_TransactionFund.SelectedValue)

      End If

      If (Check_ByISIN.Checked) Then

        Call MainForm.SetTblGenericCombo( _
        Me.Combo_Instrument, _
        RenaissanceStandardDatasets.tblInstrument, _
        "InstrumentISIN|InstrumentDescription", _
        "InstrumentID", _
        "InstrumentISIN<>''", True, True, True, 0)   ' 

      Else

        Call MainForm.SetTblGenericCombo( _
        Me.Combo_Instrument, _
        RenaissanceStandardDatasets.tblInstrument, _
        "InstrumentDescription|InstrumentISIN", _
        "InstrumentID", _
        "", True, True, True, 0)   ' 

      End If

    Catch ex As Exception
    End Try

  End Sub



#End Region

#Region " Paint Form, Calculate Transaction, totals etc."

  ''' <summary>
  ''' Paints the positions grid.
  ''' This Routine populates the grid with the appropriate heirarchy of Funds, Instruments and positions.
  ''' </summary>
  Private Sub PaintPositionsGrid()
    ' **********************************************************************************
    ' Routine to Paint the Positions Grid.
    '
    ' This Routine populates the grid with the appropriate heirarchy of Funds and Instruments.
    '
    ' **********************************************************************************

    Dim Counter As Integer

    Dim thisDataView As DataView
    Dim thisRowView As DataRowView
    Dim thisRow As DataRow = Nothing
    Dim DataViewCount As Integer
    Dim thisInstrumentRow As RenaissanceDataClass.DSInstrument.tblInstrumentRow = Nothing
    Dim thisFundRow As RenaissanceDataClass.DSFund.tblFundRow = Nothing
    Dim LastFund As Integer
    Dim LastSubFund As Integer
    Dim LastInstrumentID As UInteger
    Dim thisFundID As Integer
    Dim thisSubFundID As Integer
    Dim thisInstrumentID As UInteger
    Dim SelectedFundID As Integer
    Dim SelectedSubFundID As Integer
    Dim InstrumentPosition As Double
    Dim InstrumentValue As Double
    Dim SubFundHeirarchySelection() As DataRow
    Dim ThisSubFundChanges As Dictionary(Of UInteger, PositionChangesClass)
    Dim thisAdditionalInstruments As Dictionary(Of Integer, Boolean)
    Dim ValuationGridTotals As New Dictionary(Of Integer, Double)
    Dim FXForwardsTotals As New Dictionary(Of Integer, Double)
    Dim ValuationsGridIncludeUnsettledFX As Boolean = False
    Dim ForwardFXTransactionTypes() As TransactionTypes
    Dim SpotFXTransactionTypes() As TransactionTypes = {TransactionTypes.BuyFX, TransactionTypes.SellFX}

    Try
      Grid_Positions.Redraw = False
      FundValuations.Clear()
      Grid_Valuations.Rows.Count = 1

      If (IsNumeric(Combo_TransactionFund.SelectedValue)) Then
        SelectedFundID = CInt(Combo_TransactionFund.SelectedValue)
      Else
        SelectedFundID = 0
      End If

      If (IsNumeric(Combo_SubFund.SelectedValue)) Then
        SelectedSubFundID = CInt(Combo_SubFund.SelectedValue)
      Else
        SelectedSubFundID = 0
      End If

      InPaint = True

      ' Initialise tracking variables.
      LastFund = (-1)
      LastSubFund = (-1)
      LastInstrumentID = 0
      ' LastInstrumentLine = (-1)
      InstrumentPosition = 0
      InstrumentValue = 0

      If (Check_ValuationsIncludeUnsettledFX.Checked = False) Then
        ForwardFXTransactionTypes = New TransactionTypes() {TransactionTypes.BuyFXForward, TransactionTypes.SellFXForward}
      Else
        ForwardFXTransactionTypes = New TransactionTypes() {TransactionTypes.BuyFXForward, TransactionTypes.SellFXForward, TransactionTypes.BuyFX, TransactionTypes.SellFX}
      End If

      ' To Do .......
      ' 1) Create Temporary Copy of Selected Data
      ' 2) Add in prospective Positions / Weightings.
      ' 3) Sort.
      ' 4) Display

      ' OK.....

      Dim SubFundHeirarchyRow As DataRow = Nothing

      TransactionDataView.Sort = "TransactionFund, TransactionsubFund, TransactionInstrument"
      ContraTransactionDataView.Sort = TransactionDataView.Sort

      ' Get Groups Data.

      Dim DSGroups As RenaissanceDataClass.DSGroupsAggregated
      Dim tblGroups As RenaissanceDataClass.DSGroupsAggregated.tblGroupsAggregatedDataTable = Nothing
      Dim SelectedGroupRows() As RenaissanceDataClass.DSGroupsAggregated.tblGroupsAggregatedRow
      Dim thisGroupRow As RenaissanceDataClass.DSGroupsAggregated.tblGroupsAggregatedRow


      DSGroups = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblGroupsAggregated), RenaissanceDataClass.DSGroupsAggregated)
      If (DSGroups IsNot Nothing) Then
        tblGroups = DSGroups.tblGroupsAggregated
      End If

      ' 

      Dim InstrumentList As New ArrayList
      Dim thisInstrument As PositionSummaryClass = Nothing
      Dim lastInstrument As PositionSummaryClass = Nothing
      Dim NewGridRow As C1.Win.C1FlexGrid.Row = Nothing
      Dim thisTreeOrder As Integer
      Dim thisSubFundLevel As Integer
      Dim thisInstrumentPrice As Double
      Dim thisInstrumentPriceDate As Date
      Dim thisLocalValue As Double
      Dim FundCurrencyID As Integer
      Dim ThisSubFundInstrumentList As New Dictionary(Of UInteger, PositionSummaryClass)
      Dim ActiveSubFundList As New ArrayList
      Dim ThisUniversalID As UInteger
      Dim SkipThisLine As Boolean = False
      Dim thisIsFxForward As Boolean

      Try

        ' Add in 'AdditionalInstruments' Instruments first. Just seems easier to do it this way....

        For Each thisSubFundID In Grid_Positions_AdditionalInstruments.Keys
          SubFundHeirarchyRow = Nothing

          ' Get SubFund table row, it should be cached at this point.

          If (Not subFundDictionary.TryGetValue(thisSubFundID, SubFundHeirarchyRow)) Then
            If (Not subFundDictionary.TryGetValue(thisSubFundID Or SUBFUND_EFFECTIVEFUNDMODIFIER, SubFundHeirarchyRow)) Then
              SubFundHeirarchyRow = Nothing
            End If
          End If

          ' If the Sub Fund exists....

          If (SubFundHeirarchyRow IsNot Nothing) Then

            ' Add 'Additional' instruments to the InstrumentList collection.

            thisAdditionalInstruments = Grid_Positions_AdditionalInstruments(thisSubFundID)

            For Each thisInstrumentID In thisAdditionalInstruments.Keys

              thisInstrument = New PositionSummaryClass

              thisInstrument.FundID = CInt(SubFundHeirarchyRow("TopFundID"))
              thisFundRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, thisInstrument.FundID)
              thisInstrumentRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, thisInstrumentID)

              thisInstrument.FundName = thisFundRow.FundName
              thisInstrument.SubFundID = thisSubFundID
              If (SubFundHeirarchyRow IsNot Nothing) Then
                thisInstrument.SubFundName = CStr(SubFundHeirarchyRow("SubFundName"))
              Else
                thisInstrument.SubFundName = "."
              End If
              thisInstrument.SubFundLevel = CInt(SubFundHeirarchyRow("Level"))
              thisInstrument.InstrumentID = thisInstrumentID
              thisInstrument.InstrumentISIN = thisInstrumentRow.InstrumentISIN
              thisInstrument.InstrumentDescription = thisInstrumentRow.InstrumentDescription
              thisInstrument.InstrumentCurrency = thisInstrumentRow.InstrumentCurrency
              thisInstrument.InstrumentCurrencyID = thisInstrumentRow.InstrumentCurrencyID
              thisInstrument.InstrumentType = thisInstrumentRow.InstrumentType
              thisInstrument.ParentID = thisInstrumentID
              Call GetInstrumentPrice(thisInstrumentID, thisInstrument.InstrumentPrice, thisInstrument.InstrumentPriceDate)
              thisInstrument.FXToFund = GetFXToFund(thisInstrumentRow.InstrumentCurrencyID, thisFundRow.FundBaseCurrency)
              thisInstrument.Counter = -10
              thisInstrument.TreeOrder = CInt(SubFundHeirarchyRow("TreeOrder"))
              thisInstrument.InstrumentLocalValue = 0.0#
              thisInstrument.InstrumentFundValue = 0.0#
              thisInstrument.InstrumentSubFundTargetWeight = 0.0#
              thisInstrument.BasketComponent = True

              InstrumentList.Add(thisInstrument)
            Next

          End If
        Next


        ' For Performance reasons, I have chosen to work with the tblTransaction and tblTransactionContra tables, rather than a combined Valuation table.
        ' This is because the separate tables can be updated quickly whist a Valuation query takes a couple of seconds.
        ' It is necessary to work through the Contra Transactions to get the actual Cash.

        Dim Ordinal_TransactionParentID As Integer
        Dim Ordinal_Fund As Integer
        Dim Ordinal_SubFund As Integer
        Dim Ordinal_Instrument As Integer
        Dim Ordinal_TransactionType_Contra As Integer
        Dim Ordinal_SignedUnits As Integer
        Dim Ordinal_ValueDate As Integer
        Dim Ordinal_SettlementDate As Integer
        Dim Ordinal_TransactionIsForwardPayment As Integer

        Dim thisTransactionParentID As Integer
        Dim thisTransactionType_Contra As Integer
        Dim thisValueDate As Date
        Dim thisSettlementDate As Date
        Dim thisSignedUnits As Double

        For DataViewCount = 0 To 1

          If (DataViewCount = 0) Then
            thisDataView = TransactionDataView
          Else
            thisDataView = ContraTransactionDataView
          End If

          Ordinal_Fund = thisDataView.Table.Columns("TransactionFund").Ordinal
          Ordinal_SubFund = thisDataView.Table.Columns("TransactionSubFund").Ordinal
          Ordinal_Instrument = thisDataView.Table.Columns("TransactionInstrument").Ordinal
          Ordinal_TransactionType_Contra = thisDataView.Table.Columns("TransactionType_Contra").Ordinal
          Ordinal_SignedUnits = thisDataView.Table.Columns("TransactionSignedUnits").Ordinal
          Ordinal_ValueDate = thisDataView.Table.Columns("TransactionValueDate").Ordinal
          Ordinal_SettlementDate = thisDataView.Table.Columns("TransactionSettlementDate").Ordinal
          Ordinal_TransactionParentID = thisDataView.Table.Columns("TransactionParentID").Ordinal
          Ordinal_TransactionIsForwardPayment = thisDataView.Table.Columns("TransactionIsForwardPayment").Ordinal

          ' Loop through the selected data (DataView) adding Instruments to the ArrayList.

          For Counter = 0 To (thisDataView.Count) ' Loop to full value of thisDataView.Count so that the 'basket-Infill' code runs for the last sub fund, but exit loop early.

            If (Counter < thisDataView.Count) Then
              ' Resolve Transaction Row.
              thisRowView = thisDataView.Item(Counter)
              thisRow = thisRowView.Row

              thisFundID = CInt(thisRow(Ordinal_Fund))
              thisSubFundID = CInt(thisRow(Ordinal_SubFund))
              If (thisSubFundID = 0) Then thisSubFundID = thisFundID

              thisTransactionType_Contra = CInt(thisRow(Ordinal_TransactionType_Contra))

            End If

            '

            If (thisSubFundID <> LastSubFund) OrElse (Counter >= thisDataView.Count) Then

              ' Keep list of Sub Funds processed so far

              If (ActiveSubFundList.Contains(thisSubFundID) = False) Then
                ActiveSubFundList.Add(thisSubFundID)
              End If

              ' Add any Instruments from the relevant basket that have not already been added

              If (DataViewCount = 0) AndAlso (LastSubFund > 0) AndAlso (SubFundHeirarchyRow IsNot Nothing) AndAlso (CInt(SubFundHeirarchyRow("ReferenceBasket")) > 0) AndAlso (tblGroups IsNot Nothing) Then

                SelectedGroupRows = tblGroups.Select("GroupListID=" & SubFundHeirarchyRow("ReferenceBasket").ToString)

                If (SelectedGroupRows IsNot Nothing) AndAlso (SelectedGroupRows.Length > 0) Then

                  For Each thisGroupRow In SelectedGroupRows
                    ThisUniversalID = CUInt(thisGroupRow.GroupPertracCode)

                    ' Venice Instrument.

                    If RenaissancePertracDataClass.PertracDataClass.GetFlagsFromID(ThisUniversalID) = RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.VeniceInstrument Then
                      thisInstrumentID = CUInt(RenaissancePertracDataClass.PertracDataClass.GetInstrumentFromID(ThisUniversalID))

                      If (thisInstrumentRow Is Nothing) OrElse (thisInstrumentRow.InstrumentID <> thisInstrumentID) Then
                        thisInstrumentRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, thisInstrumentID)
                      End If

                      ' Note this code could run when displaying multiple Funds (Not Yet supported) in which case we are possibly filling in for the previous fund.
                      If (thisFundRow Is Nothing) OrElse (LastFund <> thisFundRow.FundID) Then
                        thisFundRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, thisFundID)
                        FundCurrencyID = thisFundRow.FundBaseCurrency
                      End If

                      If (ThisSubFundInstrumentList.ContainsKey(thisInstrumentID) = False) Then

                        thisInstrument = New PositionSummaryClass

                        thisInstrument.FundID = LastFund

                        thisInstrument.FundName = thisFundRow.FundName
                        thisInstrument.SubFundID = LastSubFund
                        If (SubFundHeirarchyRow IsNot Nothing) Then
                          thisInstrument.SubFundName = CStr(SubFundHeirarchyRow("SubFundName"))
                        Else
                          thisInstrument.SubFundName = "."
                        End If
                        thisInstrument.SubFundLevel = thisSubFundLevel
                        thisInstrument.InstrumentID = thisInstrumentID
                        thisInstrument.InstrumentISIN = thisInstrumentRow.InstrumentISIN
                        thisInstrument.InstrumentDescription = thisInstrumentRow.InstrumentDescription
                        thisInstrument.InstrumentCurrency = thisInstrumentRow.InstrumentCurrency
                        thisInstrument.InstrumentCurrencyID = thisInstrumentRow.InstrumentCurrencyID
                        thisInstrument.InstrumentType = thisInstrumentRow.InstrumentType
                        thisInstrument.ParentID = thisInstrumentID
                        Call GetInstrumentPrice(thisInstrumentID, thisInstrument.InstrumentPrice, thisInstrument.InstrumentPriceDate)
                        thisInstrument.FXToFund = GetFXToFund(thisInstrumentRow.InstrumentCurrencyID, FundCurrencyID)
                        thisInstrument.Counter = -Counter
                        thisInstrument.TreeOrder = thisTreeOrder
                        thisInstrument.InstrumentLocalValue = 0.0#
                        thisInstrument.InstrumentFundValue = 0.0#
                        thisInstrument.InstrumentSubFundTargetWeight = thisGroupRow.GroupPercent
                        thisInstrument.BasketComponent = True

                        InstrumentList.Add(thisInstrument)

                      Else
                        thisInstrument = ThisSubFundInstrumentList(thisInstrumentID)
                        thisInstrument.BasketComponent = True
                      End If
                    Else
                      ' Not Venice Instrument.

                      thisInstrument = New PositionSummaryClass

                      thisInstrument.FundID = LastFund
                      thisInstrument.FundName = thisFundRow.FundName
                      thisInstrument.SubFundID = thisSubFundID
                      If (SubFundHeirarchyRow IsNot Nothing) Then
                        thisInstrument.SubFundName = CStr(SubFundHeirarchyRow("SubFundName"))
                      Else
                        thisInstrument.SubFundName = "."
                      End If
                      thisInstrument.SubFundLevel = thisSubFundLevel
                      thisInstrument.InstrumentID = 0
                      thisInstrument.InstrumentISIN = ""
                      thisInstrument.InstrumentDescription = thisGroupRow.PertracName
                      thisInstrument.InstrumentCurrency = ""
                      thisInstrument.InstrumentCurrencyID = 0
                      thisInstrument.ParentID = thisInstrument.InstrumentID
                      thisInstrument.InstrumentPrice = 0.0#
                      thisInstrument.InstrumentPriceDate = Renaissance_BaseDate
                      thisInstrument.FXToFund = 1.0#
                      thisInstrument.Counter = -Counter
                      thisInstrument.TreeOrder = thisTreeOrder
                      thisInstrument.InstrumentType = 0
                      thisInstrument.InstrumentLocalValue = 0.0#
                      thisInstrument.InstrumentFundValue = 0.0#
                      thisInstrument.InstrumentSubFundTargetWeight = thisGroupRow.GroupPercent
                      thisInstrument.BasketComponent = True

                      InstrumentList.Add(thisInstrument)

                    End If

                  Next ' For Each thisGroupRow In SelectedGroupRows

                End If

              End If ' If (LastSubFund > 0) etc ...

              If (Counter >= thisDataView.Count) Then
                Exit For
              End If

              ' OK, New SubFund etc...

              ThisSubFundInstrumentList.Clear()

              If (subFundDictionary.TryGetValue(thisSubFundID, SubFundHeirarchyRow)) OrElse (subFundDictionary.TryGetValue(CInt(thisSubFundID Or SUBFUND_EFFECTIVEFUNDMODIFIER), SubFundHeirarchyRow)) Then
                SubFundHeirarchyRow = subFundDictionary(thisSubFundID)
                thisTreeOrder = CInt(SubFundHeirarchyRow("TreeOrder"))
                thisSubFundLevel = CInt(SubFundHeirarchyRow("Level"))
              Else
                SubFundHeirarchyRow = Nothing
                thisTreeOrder = -1
                thisSubFundLevel = 1
              End If

            End If

            ' Get Fund Info

            If (thisFundRow Is Nothing) OrElse (thisFundID <> thisFundRow.FundID) Then
              thisFundRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, thisFundID)
              FundCurrencyID = thisFundRow.FundBaseCurrency
            End If

            ' Instrument Info

            thisInstrumentID = CUInt(thisRow(Ordinal_Instrument)) ' Get this here, as it may be set differently above.

            If (thisInstrumentRow Is Nothing) OrElse (thisInstrumentRow.InstrumentID <> thisInstrumentID) Then
              thisInstrumentRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, thisInstrumentID)
            End If

            ' Get Instrument Price early so that I can use it in the 'SkipThisLine' decision

            Call GetInstrumentPrice(thisInstrumentID, thisInstrumentPrice, thisInstrumentPriceDate)

            ' Process Instrument (if not Zero-Value Expense (Expenses with a value tend to be provisions))

            SkipThisLine = False

            If (thisInstrumentRow.InstrumentType = InstrumentTypes.Expense) AndAlso (thisInstrumentPrice = 0.0#) Then
              SkipThisLine = True
            End If

            If (thisInstrumentID = thisFundRow.FundUnit) Then
              SkipThisLine = True
            End If

            If (Not SkipThisLine) Then

              ' Add a New Instrument Row if this transaction represents the start of a new Fund, Sub Fund or Instrument

              If (thisFundID <> LastFund) OrElse (thisSubFundID <> LastSubFund) OrElse (thisInstrumentID <> LastInstrumentID) Then

                ' OK, Add Instrument details...

                thisInstrument = New PositionSummaryClass

                thisInstrument.FundID = thisFundID
                thisInstrument.FundName = thisFundRow.FundName
                thisInstrument.SubFundID = thisSubFundID
                If (SubFundHeirarchyRow IsNot Nothing) Then
                  thisInstrument.SubFundName = CStr(SubFundHeirarchyRow("SubFundName"))
                Else
                  thisInstrument.SubFundName = "."
                End If
                thisInstrument.SubFundLevel = thisSubFundLevel
                thisInstrument.InstrumentID = thisInstrumentID
                thisInstrument.InstrumentISIN = thisInstrumentRow.InstrumentISIN
                thisInstrument.InstrumentDescription = thisInstrumentRow.InstrumentDescription
                thisInstrument.InstrumentCurrency = thisInstrumentRow.InstrumentCurrency
                thisInstrument.InstrumentCurrencyID = thisInstrumentRow.InstrumentCurrencyID
                thisInstrument.InstrumentType = thisInstrumentRow.InstrumentType
                thisInstrument.ParentID = thisInstrumentRow.InstrumentParent
                thisInstrument.InstrumentPrice = thisInstrumentPrice
                thisInstrument.InstrumentPriceDate = thisInstrumentPriceDate
                thisInstrument.FXToFund = GetFXToFund(thisInstrumentRow.InstrumentCurrencyID, FundCurrencyID)
                thisInstrument.Counter = Counter
                thisInstrument.Counter = Counter
                thisInstrument.TreeOrder = thisTreeOrder
                thisInstrument.InstrumentLocalValue = 0.0#
                thisInstrument.InstrumentFundValue = 0.0#
                thisInstrument.InstrumentSubFundTargetWeight = 0.0#

                ' Keep record of instruments contained in this sub fund. (so we can add missing basket entries later)

                If (ThisSubFundInstrumentList.ContainsKey(thisInstrumentID) = False) Then
                  ThisSubFundInstrumentList.Add(thisInstrumentID, thisInstrument)
                End If

                ' Set this Instrument weight etc.

                If (tblGroups IsNot Nothing) AndAlso (SubFundHeirarchyRow IsNot Nothing) AndAlso (CInt(SubFundHeirarchyRow("ReferenceBasket")) > 0) Then

                  ThisUniversalID = RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(thisInstrument.InstrumentID, RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.VeniceInstrument)
                  SelectedGroupRows = tblGroups.Select("(GroupListID=" & SubFundHeirarchyRow("ReferenceBasket").ToString & ") AND (GroupPertracCode=" & ThisUniversalID.ToString & ")")

                  If (SelectedGroupRows.Length > 0) Then
                    thisInstrument.InstrumentSubFundTargetWeight = SelectedGroupRows(0).GroupPercent
                  End If

                End If

                InstrumentList.Add(thisInstrument)

              End If

              ' Aggregate Position

              thisLocalValue = (CDbl(Nz(thisRow(Ordinal_SignedUnits), 0.0#)) * Nz(thisInstrumentRow.InstrumentContractSize, 1.0#) * Nz(thisInstrumentRow.InstrumentMultiplier, 1) * thisInstrument.InstrumentPrice)

              thisInstrument.InstrumentPosition += (CDbl(Nz(thisRow(Ordinal_SignedUnits), 0.0#)) * CDbl(Nz(thisInstrumentRow.InstrumentParentEquivalentRatio, 1.0#)))
              thisInstrument.InstrumentLocalValue += thisLocalValue
              thisInstrument.InstrumentFundValue = thisInstrument.InstrumentLocalValue * thisInstrument.FXToFund

              ' Accumulate Fund Values

              If (Not FundValuations.ContainsKey(thisFundID)) Then
                FundValuations.Add(thisFundID, (thisLocalValue * thisInstrument.FXToFund))
              Else
                FundValuations(thisFundID) = FundValuations(thisFundID) + (thisLocalValue * thisInstrument.FXToFund)
              End If

            End If ' If (Not SkipThisLine) Then

            ' FX Forward. (Only interested if it has not expired)

            thisIsFxForward = False

            'If ((thisTransactionType_Contra = TransactionTypes.BuyFXForward) OrElse (thisTransactionType_Contra = TransactionTypes.SellFXForward)) _
            '    OrElse ((ValuationsGridIncludeUnsettledFX) AndAlso ((thisTransactionType_Contra = TransactionTypes.BuyFX) OrElse (thisTransactionType_Contra = TransactionTypes.SellFX))) Then

            ' Include in Forwards total if 1) It is a Forward FX, 2) It is a cash trade with the TransactionIsForwardPayment flag or 3) The option is set to include all unsettled Spot FXs.
            If (ForwardFXTransactionTypes.Contains(thisTransactionType_Contra)) OrElse (SpotFXTransactionTypes.Contains(thisTransactionType_Contra) AndAlso (CBool(Nz(thisRow(Ordinal_TransactionIsForwardPayment), False)))) Then
              thisValueDate = CDate(thisRow(Ordinal_ValueDate)).Date
              thisSettlementDate = CDate(thisRow(Ordinal_SettlementDate)).Date
              thisTransactionParentID = CInt(thisRow(Ordinal_TransactionParentID))
              thisSignedUnits = CDbl(thisRow(Ordinal_SignedUnits))

              If (thisSettlementDate > Now.Date) Then
                thisIsFxForward = True

                If (FXForwardsTotals.ContainsKey(thisInstrument.InstrumentCurrencyID)) Then
                  FXForwardsTotals(thisInstrument.InstrumentCurrencyID) += thisSignedUnits
                Else
                  FXForwardsTotals.Add(thisInstrument.InstrumentCurrencyID, thisSignedUnits)
                End If

              End If
            End If

            LastFund = thisFundID
            LastSubFund = thisSubFundID
            LastInstrumentID = thisInstrumentID

          Next ' For Counter = 0 To (myDataView.Count) 

        Next ' DataViewCount

        ' ********************************************************************************
        ' Add In Pending Transfers
        '
        ' ********************************************************************************
        Dim thisPendingTransfer As PendingTransfer
        Dim FirstLegInstrument As PositionSummaryClass
        Dim SecondLegInstrument As PositionSummaryClass = Nothing
        Dim ThirdLegInstrument As PositionSummaryClass
        Dim FourthLegInstrument As PositionSummaryClass

        For Counter = 0 To (Grid_Transfer_Trades.Count - 1)
          thisPendingTransfer = Grid_Transfer_Trades(Counter)

          ' Leg 1 

          FirstLegInstrument = New PositionSummaryClass

          FirstLegInstrument.FundID = thisPendingTransfer.FromFundID
          thisFundRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, FirstLegInstrument.FundID)
          FirstLegInstrument.FundName = thisFundRow.FundName
          FirstLegInstrument.SubFundID = thisPendingTransfer.FromSubFundID
          thisSubFundID = FirstLegInstrument.SubFundID

          If (subFundDictionary.TryGetValue(thisSubFundID, SubFundHeirarchyRow)) OrElse (subFundDictionary.TryGetValue(CInt(thisSubFundID Or SUBFUND_EFFECTIVEFUNDMODIFIER), SubFundHeirarchyRow)) Then
            SubFundHeirarchyRow = subFundDictionary(thisSubFundID)
            thisTreeOrder = CInt(SubFundHeirarchyRow("TreeOrder"))
            thisSubFundLevel = CInt(SubFundHeirarchyRow("Level"))
          Else
            SubFundHeirarchyRow = Nothing
            thisTreeOrder = -1
            thisSubFundLevel = 1
          End If


          If (SubFundHeirarchyRow IsNot Nothing) Then
            FirstLegInstrument.SubFundName = CStr(SubFundHeirarchyRow("SubFundName"))
          Else
            FirstLegInstrument.SubFundName = "."
          End If

          FirstLegInstrument.SubFundLevel = thisSubFundLevel
          FirstLegInstrument.InstrumentID = thisPendingTransfer.InstrumentID
          thisInstrumentRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, FirstLegInstrument.InstrumentID)
          FirstLegInstrument.InstrumentISIN = thisInstrumentRow.InstrumentISIN
          FirstLegInstrument.InstrumentDescription = thisInstrumentRow.InstrumentDescription
          FirstLegInstrument.InstrumentCurrency = thisInstrumentRow.InstrumentCurrency
          FirstLegInstrument.InstrumentCurrencyID = thisInstrumentRow.InstrumentCurrencyID
          FirstLegInstrument.InstrumentType = thisInstrumentRow.InstrumentType
          FirstLegInstrument.ParentID = thisInstrumentRow.InstrumentParent
          FirstLegInstrument.InstrumentPrice = thisPendingTransfer.TradePrice
          FirstLegInstrument.FXToFund = GetFXToFund(thisInstrumentRow.InstrumentCurrencyID, thisFundRow.FundBaseCurrency)
          FirstLegInstrument.Counter = -1
          FirstLegInstrument.TreeOrder = thisTreeOrder
          FirstLegInstrument.InstrumentPosition = -thisPendingTransfer.Quantity
          FirstLegInstrument.InstrumentLocalValue = -thisPendingTransfer.Quantity * thisPendingTransfer.ValuationPrice * (thisInstrumentRow.InstrumentContractSize * thisInstrumentRow.InstrumentMultiplier)
          FirstLegInstrument.InstrumentFundValue = FirstLegInstrument.InstrumentLocalValue * FirstLegInstrument.FXToFund
          FirstLegInstrument.InstrumentSubFundTargetWeight = 0.0#
          FirstLegInstrument.IsTransfer = True

          ' Set this Instrument weight etc.

          If (tblGroups IsNot Nothing) AndAlso (SubFundHeirarchyRow IsNot Nothing) AndAlso (CInt(SubFundHeirarchyRow("ReferenceBasket")) > 0) Then

            ThisUniversalID = RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(FirstLegInstrument.InstrumentID, RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.VeniceInstrument)
            SelectedGroupRows = tblGroups.Select("(GroupListID=" & SubFundHeirarchyRow("ReferenceBasket").ToString & ") AND (GroupPertracCode=" & ThisUniversalID.ToString & ")")

            If (SelectedGroupRows.Length > 0) Then
              FirstLegInstrument.InstrumentSubFundTargetWeight = SelectedGroupRows(0).GroupPercent
            End If

          End If

          InstrumentList.Add(FirstLegInstrument)

          ' Leg 1 Cash

          If (thisPendingTransfer.TradePrice <> 0.0#) Then

            SecondLegInstrument = New PositionSummaryClass

            SecondLegInstrument.FundID = FirstLegInstrument.FundID
            SecondLegInstrument.FundName = FirstLegInstrument.FundName
            SecondLegInstrument.SubFundID = FirstLegInstrument.SubFundID
            SecondLegInstrument.SubFundName = FirstLegInstrument.SubFundName
            SecondLegInstrument.SubFundLevel = FirstLegInstrument.SubFundLevel
            SecondLegInstrument.InstrumentID = CInt(Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblCurrency, thisInstrumentRow.InstrumentCurrencyID, "CurrencyCashInstrumentID"), 0))
            thisInstrumentRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, SecondLegInstrument.InstrumentID)
            SecondLegInstrument.InstrumentISIN = thisInstrumentRow.InstrumentISIN
            SecondLegInstrument.InstrumentDescription = thisInstrumentRow.InstrumentDescription
            SecondLegInstrument.InstrumentCurrency = thisInstrumentRow.InstrumentCurrency
            SecondLegInstrument.InstrumentCurrencyID = thisInstrumentRow.InstrumentCurrencyID
            SecondLegInstrument.InstrumentType = InstrumentTypes.Cash
            SecondLegInstrument.ParentID = SecondLegInstrument.InstrumentID
            SecondLegInstrument.InstrumentPrice = 1.0#
            SecondLegInstrument.FXToFund = GetFXToFund(thisInstrumentRow.InstrumentCurrencyID, thisFundRow.FundBaseCurrency)
            SecondLegInstrument.Counter = -1
            SecondLegInstrument.TreeOrder = FirstLegInstrument.TreeOrder
            SecondLegInstrument.InstrumentPosition = -FirstLegInstrument.InstrumentLocalValue
            SecondLegInstrument.InstrumentLocalValue = -FirstLegInstrument.InstrumentLocalValue
            SecondLegInstrument.InstrumentFundValue = -FirstLegInstrument.InstrumentFundValue
            SecondLegInstrument.InstrumentSubFundTargetWeight = 0.0#
            SecondLegInstrument.IsTransfer = True

            ' Set this Instrument weight etc.

            If (tblGroups IsNot Nothing) AndAlso (SubFundHeirarchyRow IsNot Nothing) AndAlso (CInt(SubFundHeirarchyRow("ReferenceBasket")) > 0) Then

              ThisUniversalID = RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(SecondLegInstrument.InstrumentID, RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.VeniceInstrument)
              SelectedGroupRows = tblGroups.Select("(GroupListID=" & SubFundHeirarchyRow("ReferenceBasket").ToString & ") AND (GroupPertracCode=" & ThisUniversalID.ToString & ")")

              If (SelectedGroupRows.Length > 0) Then
                SecondLegInstrument.InstrumentSubFundTargetWeight = SelectedGroupRows(0).GroupPercent
              End If

            End If

            InstrumentList.Add(SecondLegInstrument)

          End If

          ' Leg 2 

          ThirdLegInstrument = New PositionSummaryClass

          ThirdLegInstrument.FundID = thisPendingTransfer.ToFundID
          thisFundRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, ThirdLegInstrument.FundID)

          ThirdLegInstrument.FundName = FirstLegInstrument.FundName
          ThirdLegInstrument.SubFundID = thisPendingTransfer.ToSubFundID
          thisSubFundID = ThirdLegInstrument.SubFundID

          If (subFundDictionary.TryGetValue(thisSubFundID, SubFundHeirarchyRow)) OrElse (subFundDictionary.TryGetValue(CInt(thisSubFundID Or SUBFUND_EFFECTIVEFUNDMODIFIER), SubFundHeirarchyRow)) Then
            SubFundHeirarchyRow = subFundDictionary(thisSubFundID)
            thisTreeOrder = CInt(SubFundHeirarchyRow("TreeOrder"))
            thisSubFundLevel = CInt(SubFundHeirarchyRow("Level"))
          Else
            SubFundHeirarchyRow = Nothing
            thisTreeOrder = -1
            thisSubFundLevel = 1
          End If


          If (SubFundHeirarchyRow IsNot Nothing) Then
            ThirdLegInstrument.SubFundName = CStr(SubFundHeirarchyRow("SubFundName"))
          Else
            ThirdLegInstrument.SubFundName = "."
          End If

          ThirdLegInstrument.SubFundLevel = thisSubFundLevel
          ThirdLegInstrument.InstrumentID = FirstLegInstrument.InstrumentID
          ThirdLegInstrument.InstrumentISIN = FirstLegInstrument.InstrumentISIN
          ThirdLegInstrument.InstrumentDescription = FirstLegInstrument.InstrumentDescription
          ThirdLegInstrument.InstrumentCurrency = FirstLegInstrument.InstrumentCurrency
          ThirdLegInstrument.InstrumentCurrencyID = FirstLegInstrument.InstrumentCurrencyID
          ThirdLegInstrument.InstrumentType = FirstLegInstrument.InstrumentType
          ThirdLegInstrument.ParentID = FirstLegInstrument.InstrumentID
          ThirdLegInstrument.InstrumentPrice = FirstLegInstrument.InstrumentPrice
          ThirdLegInstrument.FXToFund = FirstLegInstrument.FXToFund
          ThirdLegInstrument.Counter = -1
          ThirdLegInstrument.TreeOrder = thisTreeOrder
          ThirdLegInstrument.InstrumentPosition = -FirstLegInstrument.InstrumentPosition
          ThirdLegInstrument.InstrumentLocalValue = -FirstLegInstrument.InstrumentLocalValue
          ThirdLegInstrument.InstrumentFundValue = -FirstLegInstrument.InstrumentFundValue
          ThirdLegInstrument.InstrumentSubFundTargetWeight = 0.0#
          ThirdLegInstrument.IsTransfer = True

          ' Set this Instrument weight etc.

          If (tblGroups IsNot Nothing) AndAlso (SubFundHeirarchyRow IsNot Nothing) AndAlso (CInt(SubFundHeirarchyRow("ReferenceBasket")) > 0) Then

            ThisUniversalID = RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(ThirdLegInstrument.InstrumentID, RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.VeniceInstrument)
            SelectedGroupRows = tblGroups.Select("(GroupListID=" & SubFundHeirarchyRow("ReferenceBasket").ToString & ") AND (GroupPertracCode=" & ThisUniversalID.ToString & ")")

            If (SelectedGroupRows.Length > 0) Then
              ThirdLegInstrument.InstrumentSubFundTargetWeight = SelectedGroupRows(0).GroupPercent
            End If

          End If

          InstrumentList.Add(ThirdLegInstrument)

          ' Leg 2 Cash

          If (thisPendingTransfer.TradePrice <> 0.0#) Then

            FourthLegInstrument = New PositionSummaryClass

            FourthLegInstrument.FundID = ThirdLegInstrument.FundID
            FourthLegInstrument.FundName = ThirdLegInstrument.FundName
            FourthLegInstrument.SubFundID = ThirdLegInstrument.SubFundID
            FourthLegInstrument.SubFundName = ThirdLegInstrument.SubFundName
            FourthLegInstrument.SubFundLevel = ThirdLegInstrument.SubFundLevel
            FourthLegInstrument.InstrumentID = SecondLegInstrument.InstrumentID
            FourthLegInstrument.InstrumentISIN = SecondLegInstrument.InstrumentISIN
            FourthLegInstrument.InstrumentDescription = SecondLegInstrument.InstrumentDescription
            FourthLegInstrument.InstrumentCurrency = SecondLegInstrument.InstrumentCurrency
            FourthLegInstrument.InstrumentCurrencyID = SecondLegInstrument.InstrumentCurrencyID
            FourthLegInstrument.InstrumentType = SecondLegInstrument.InstrumentType
            FourthLegInstrument.ParentID = SecondLegInstrument.ParentID
            FourthLegInstrument.InstrumentPrice = 1.0#
            FourthLegInstrument.FXToFund = SecondLegInstrument.FXToFund
            FourthLegInstrument.Counter = -1
            FourthLegInstrument.TreeOrder = ThirdLegInstrument.TreeOrder
            FourthLegInstrument.InstrumentPosition = -SecondLegInstrument.InstrumentPosition
            FourthLegInstrument.InstrumentLocalValue = -ThirdLegInstrument.InstrumentLocalValue
            FourthLegInstrument.InstrumentFundValue = -ThirdLegInstrument.InstrumentFundValue
            FourthLegInstrument.InstrumentSubFundTargetWeight = 0.0#
            FourthLegInstrument.IsTransfer = True

            ' Set this Instrument weight etc.

            If (tblGroups IsNot Nothing) AndAlso (SubFundHeirarchyRow IsNot Nothing) AndAlso (CInt(SubFundHeirarchyRow("ReferenceBasket")) > 0) Then

              ThisUniversalID = RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(FourthLegInstrument.InstrumentID, RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.VeniceInstrument)
              SelectedGroupRows = tblGroups.Select("(GroupListID=" & SubFundHeirarchyRow("ReferenceBasket").ToString & ") AND (GroupPertracCode=" & ThisUniversalID.ToString & ")")

              If (SelectedGroupRows.Length > 0) Then
                FourthLegInstrument.InstrumentSubFundTargetWeight = SelectedGroupRows(0).GroupPercent
              End If

            End If

            InstrumentList.Add(FourthLegInstrument)

          End If

        Next

        ' ********************************************************************************
        ' Add in Any Sub Funds (That have refrence groups) that were not included above...
        ' ActiveSubFundList contains a list of Sub Funds added so far...
        ' ********************************************************************************

        If (SelectedFundID > 0) Then
          SubFundHeirarchySelection = SubFundHeirarchyTable.Select("TopFundID=" & SelectedFundID.ToString, "TreeOrder")

          ' If the Fund has changed, clear grid and re-start.

          If (gridsubFundMap.ContainsKey(SelectedFundID) = False) Then
            gridsubFundMap.Clear()

            ' Clear Grid, to start
            Grid_Positions.Rows.Count = 1

          End If

        Else
          ' Not Currenly allow 'All Funds' selection.
          SubFundHeirarchySelection = SubFundHeirarchyTable.Select("false", "TreeOrder")
          'SubFundHeirarchySelection = SubFundHeirarchyTable.Select("true", "TreeOrder")
        End If

        For Each SubFundHeirarchyRow In SubFundHeirarchySelection

          thisSubFundID = CInt(SubFundHeirarchyRow("SubFundID"))

          If (ActiveSubFundList.Contains(thisSubFundID) = False) AndAlso (CInt(SubFundHeirarchyRow("ReferenceBasket")) > 0) Then
            ' Sub Fund not already in the InstrumentList. Add basket entries.

            SelectedGroupRows = tblGroups.Select("GroupListID=" & SubFundHeirarchyRow("ReferenceBasket").ToString)

            If (SelectedGroupRows IsNot Nothing) AndAlso (SelectedGroupRows.Length > 0) Then

              For Each thisGroupRow In SelectedGroupRows
                ThisUniversalID = CUInt(thisGroupRow.GroupPertracCode)

                ' Venice Instrument.

                If RenaissancePertracDataClass.PertracDataClass.GetFlagsFromID(ThisUniversalID) = RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.VeniceInstrument Then
                  thisInstrumentID = CUInt(RenaissancePertracDataClass.PertracDataClass.GetInstrumentFromID(ThisUniversalID))

                  If (thisInstrumentRow Is Nothing) OrElse (thisInstrumentRow.InstrumentID <> thisInstrumentID) Then
                    thisInstrumentRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, thisInstrumentID)
                  End If

                  If (ThisSubFundInstrumentList.ContainsKey(thisInstrumentID)) Then
                    thisInstrument = ThisSubFundInstrumentList(thisInstrumentID)
                    thisInstrument.BasketComponent = True

                  Else

                    thisInstrument = New PositionSummaryClass

                    thisInstrument.FundID = thisFundID
                    thisInstrument.FundName = thisFundRow.FundName
                    thisInstrument.SubFundID = thisSubFundID
                    If (SubFundHeirarchyRow IsNot Nothing) Then
                      thisInstrument.SubFundName = CStr(SubFundHeirarchyRow("SubFundName"))
                    Else
                      thisInstrument.SubFundName = "."
                    End If
                    thisInstrument.SubFundLevel = thisSubFundLevel
                    thisInstrument.InstrumentID = thisInstrumentID
                    thisInstrument.InstrumentISIN = thisInstrumentRow.InstrumentISIN
                    thisInstrument.InstrumentDescription = thisInstrumentRow.InstrumentDescription
                    thisInstrument.InstrumentCurrency = thisInstrumentRow.InstrumentCurrency
                    thisInstrument.InstrumentCurrencyID = thisInstrumentRow.InstrumentCurrencyID
                    thisInstrument.InstrumentType = thisInstrumentRow.InstrumentType
                    thisInstrument.ParentID = thisInstrumentID
                    Call GetInstrumentPrice(thisInstrumentID, thisInstrument.InstrumentPrice, thisInstrument.InstrumentPriceDate)
                    thisInstrument.FXToFund = GetFXToFund(thisInstrumentRow.InstrumentCurrencyID, FundCurrencyID)
                    thisInstrument.Counter = -Counter
                    thisInstrument.TreeOrder = thisTreeOrder
                    thisInstrument.InstrumentLocalValue = 0.0#
                    thisInstrument.InstrumentFundValue = 0.0#
                    thisInstrument.InstrumentSubFundTargetWeight = thisGroupRow.GroupPercent
                    thisInstrument.BasketComponent = True

                    InstrumentList.Add(thisInstrument)

                  End If
                Else
                  ' Not Venice Instrument.

                  thisInstrument = New PositionSummaryClass

                  thisInstrument.FundID = thisFundID
                  thisInstrument.FundName = thisFundRow.FundName
                  thisInstrument.SubFundID = thisSubFundID
                  If (SubFundHeirarchyRow IsNot Nothing) Then
                    thisInstrument.SubFundName = CStr(SubFundHeirarchyRow("SubFundName"))
                  Else
                    thisInstrument.SubFundName = "."
                  End If
                  thisInstrument.SubFundLevel = thisSubFundLevel
                  thisInstrument.InstrumentID = 0
                  thisInstrument.InstrumentISIN = ""
                  thisInstrument.InstrumentDescription = thisGroupRow.PertracName
                  thisInstrument.InstrumentCurrency = ""
                  thisInstrument.InstrumentCurrencyID = ""
                  thisInstrument.ParentID = thisInstrument.InstrumentID
                  thisInstrument.InstrumentPrice = 0.0#
                  thisInstrument.InstrumentPriceDate = Renaissance_BaseDate
                  thisInstrument.FXToFund = 1.0#
                  thisInstrument.Counter = -Counter
                  thisInstrument.TreeOrder = thisTreeOrder
                  thisInstrument.InstrumentType = 0
                  thisInstrument.InstrumentLocalValue = 0.0#
                  thisInstrument.InstrumentFundValue = 0.0#
                  thisInstrument.InstrumentSubFundTargetWeight = thisGroupRow.GroupPercent
                  thisInstrument.BasketComponent = True

                  InstrumentList.Add(thisInstrument)

                End If

              Next ' For Each thisGroupRow In SelectedGroupRows

            End If

          End If

        Next

        ' Sort Instrument List.

        InstrumentList.Sort(New PositionSummaryComparer())

        ' Roll Up duplicate InstrumentList entries (Cash lines)

        For Counter = (InstrumentList.Count - 2) To 0 Step -1
          thisInstrument = InstrumentList(Counter)
          lastInstrument = InstrumentList(Counter + 1)

          If (thisInstrument.FundID = lastInstrument.FundID) AndAlso (thisInstrument.SubFundID = lastInstrument.SubFundID) AndAlso (thisInstrument.InstrumentID = lastInstrument.InstrumentID) Then
            thisInstrument.InstrumentLocalValue += lastInstrument.InstrumentLocalValue
            thisInstrument.InstrumentFundValue += lastInstrument.InstrumentFundValue
            thisInstrument.InstrumentPosition += lastInstrument.InstrumentPosition
            thisInstrument.IsTransfer = thisInstrument.IsTransfer Or lastInstrument.IsTransfer

            If (thisInstrument.InstrumentSubFundTargetWeight = 0.0#) AndAlso (lastInstrument.InstrumentSubFundTargetWeight <> 0.0#) Then
              thisInstrument.InstrumentSubFundTargetWeight = lastInstrument.InstrumentSubFundTargetWeight
            End If

            lastInstrument.InstrumentPosition = 0.0#
            lastInstrument.InstrumentFundValue = 0.0#
            lastInstrument.InstrumentLocalValue = 0.0#
            lastInstrument.IsDeleted = True
          End If

          ' Since we're here, add up asset values for the ValuationGrid
          Dim OmitInstrumentTypes() As Integer = {InstrumentTypes.Cash, InstrumentTypes.Interest, InstrumentTypes.Margin, InstrumentTypes.Notional, InstrumentTypes.UnrealisedPnL, InstrumentTypes.Forward}

          If (thisInstrument.InstrumentLocalValue <> 0.0#) Then
            If (Not OmitInstrumentTypes.Contains(thisInstrument.InstrumentType)) Then
              If (ValuationGridTotals.ContainsKey(thisInstrument.InstrumentCurrencyID)) Then
                ValuationGridTotals(thisInstrument.InstrumentCurrencyID) += thisInstrument.InstrumentLocalValue
              Else
                ValuationGridTotals.Add(thisInstrument.InstrumentCurrencyID, thisInstrument.InstrumentLocalValue)
              End If
            End If
          End If
        Next

        ' ********************************************************************************
        ' Clear out Zero positions
        ' ********************************************************************************
        ' Causes all kinds of problems. Just test to ignore them later on.

        'For Counter = (InstrumentList.Count - 1) To 0 Step -1
        '  thisInstrument = InstrumentList(Counter)

        '  If (thisInstrument.BasketComponent = False) AndAlso (Math.Abs(thisInstrument.InstrumentPosition) < 0.00001#) AndAlso (Math.Abs(thisInstrument.InstrumentLocalValue) < 1.0#) Then
        '    ' InstrumentList.RemoveAt(Counter)
        '    ' InstrumentList.Remove(thisInstrument)
        '  End If

        'Next

        ' OK, by now there should be an entry in InstrumentList for each actual position and each additional instrument not held, but in the reference baskets.

        ' Build Outline grid using SubFundHeirarchyTable and SubFundHeirarchySelection, set above...

        Dim lastGridRow As C1.Win.C1FlexGrid.Row
        Dim SubFundNode As C1.Win.C1FlexGrid.Row = Nothing
        Dim ThisGridRow As C1.Win.C1FlexGrid.Row = Nothing
        Dim ChildRange As C1.Win.C1FlexGrid.CellRange = Nothing
        Dim ThisLevel As Integer
        Dim ChildCounter As Integer
        Dim RowFound As Boolean = False
        Dim NodeInsertIndex As Integer

        ' Add Top level node if necessary

        If (Grid_Positions.Rows.Count <= 1) Then
          ' Add Header Line.
          Grid_Positions.Rows.Add()
          Grid_Positions(Grid_Positions.Rows.Count - 1, GridColumns.FirstCol) = "Primonial"
          Grid_Positions.Rows(Grid_Positions.Rows.Count - 1).IsNode = True
          Grid_Positions.Rows(Grid_Positions.Rows.Count - 1).Node.Level = 0
          Grid_Positions.Rows(Grid_Positions.Rows.Count - 1).AllowEditing = False

          If (Not gridsubFundMap.ContainsKey(0)) Then gridsubFundMap.Add(0, Grid_Positions.Rows(Grid_Positions.Rows.Count - 1))

        End If

        ' Mark Existing Rows, so that they may be removed if no longer needed.

        For Counter = 2 To (Grid_Positions.Rows.Count - 1)
          ThisGridRow = Grid_Positions.Rows(Counter)

          ThisGridRow(GridColumns.Counter) = -9999
        Next

        ' Add Fund / Sub Fund Nodes.

        lastGridRow = gridsubFundMap(0)

        For Each SubFundHeirarchyRow In SubFundHeirarchySelection
          thisSubFundID = CInt(SubFundHeirarchyRow("SubFundID"))
          ThisLevel = CInt(SubFundHeirarchyRow("Level"))

          If (gridsubFundMap.ContainsKey(thisSubFundID)) Then
            lastGridRow = gridsubFundMap(thisSubFundID)
          Else
            'Check for existing range....
            ChildRange = lastGridRow.Node.GetCellRange
            ChildRange.Normalize()

            If (ChildRange.TopRow = ChildRange.BottomRow) Then ' Empty, Insert
              ' Empty, does not matter

              NodeInsertIndex = lastGridRow.Index + 1

            ElseIf (ThisLevel <= lastGridRow.Node.Level) Then
              ' Inserting Sibling or higher

              NodeInsertIndex = ChildRange.BottomRow + 1
            ElseIf (lastGridRow.Node.Children > 0) Then
              ' Inserting a Child node
              ' If this node has children, then we want to insert this new node before the first node child, after an non-node children.
              NodeInsertIndex = lastGridRow.Node.GetNode(NodeTypeEnum.FirstChild).Row.SafeIndex
            Else
              NodeInsertIndex = ChildRange.BottomRow + 1
            End If

            'Grid_Positions.Rows.InsertRange(lastGridRow.Index + 1, 1)
            'lastGridRow = Grid_Positions.Rows(lastGridRow.Index + 1)
            'lastGridRow.IsNode = True
            lastGridRow = Grid_Positions.Rows.InsertNode(NodeInsertIndex, ThisLevel).Row
            lastGridRow.AllowEditing = False

            gridsubFundMap.Add(thisSubFundID, lastGridRow)
          End If

          lastGridRow.Node.Level = ThisLevel
          lastGridRow(GridColumns.FirstCol) = SubFundHeirarchyRow("SubFundName")
          lastGridRow(GridColumns.FundID) = CInt(SubFundHeirarchyRow("TopFundID"))
          lastGridRow(GridColumns.SubFundID) = thisSubFundID
          lastGridRow(GridColumns.InstrumentID) = (0UI)
          lastGridRow(GridColumns.Counter) = (-1)
          lastGridRow.Visible = True
        Next

        '

        LastSubFund = (-1)

        ' Loop through the instruments, Add to grid.

        Dim InsertInstrumentIndex As Integer
        Dim ModifiedTargetWeight As Boolean
        Dim ThisTargetWeight As Double
        Dim ThisRowChange As PositionChangesClass

        For Counter = 0 To (InstrumentList.Count - 1)
          thisInstrument = InstrumentList(Counter)  '  TransactionSummaryClass

          ' Ignore Zero positions ?

          If (thisInstrument.IsDeleted = False) AndAlso (Not ((thisInstrument.BasketComponent = False) AndAlso (thisInstrument.IsTransfer = False) AndAlso (Math.Abs(thisInstrument.InstrumentPosition) < 0.00001#) AndAlso (Math.Abs(thisInstrument.InstrumentLocalValue) < 1.0#))) Then

            ' Get Parent node for this Sub Fund

            If (thisInstrument.SubFundID <> LastSubFund) OrElse (SubFundNode Is Nothing) Then
              If (gridsubFundMap.ContainsKey(thisInstrument.SubFundID)) Then
                SubFundNode = gridsubFundMap(thisInstrument.SubFundID)
              Else
                SubFundNode = gridsubFundMap(thisInstrument.FundID)
              End If

              ' Get Children
              ChildRange = SubFundNode.Node.GetCellRange
              ChildRange.Normalize()

              If (SubFundNode.Node.Children > 0) Then
                InsertInstrumentIndex = SubFundNode.Node.GetNode(NodeTypeEnum.FirstChild).Row.SafeIndex
              Else
                InsertInstrumentIndex = ChildRange.BottomRow + 1
              End If

            End If

            RowFound = False
            For ChildCounter = ChildRange.TopRow To ChildRange.BottomRow
              ThisGridRow = Grid_Positions.Rows(ChildCounter)

              If (ThisGridRow.IsNode = False) Then
                thisSubFundID = CInt(ThisGridRow(GridColumns.SubFundID))
                thisInstrumentID = CUInt(ThisGridRow(GridColumns.InstrumentID))

                If (thisSubFundID = thisInstrument.SubFundID) AndAlso (thisInstrumentID = thisInstrument.InstrumentID) Then
                  RowFound = True
                  NewGridRow = ThisGridRow
                  Exit For
                End If
              End If
            Next

            If (RowFound = False) Then
              ' Add New Row if necessary.

              Grid_Positions.Rows.InsertRange(InsertInstrumentIndex, 1)
              NewGridRow = Grid_Positions.Rows(InsertInstrumentIndex)
              NewGridRow(GridColumns.FundID) = thisInstrument.FundID
              NewGridRow(GridColumns.SubFundID) = thisInstrument.SubFundID
              NewGridRow(GridColumns.InstrumentID) = thisInstrument.InstrumentID
              InsertInstrumentIndex += 1
            End If

            ' Update Row Details

            NewGridRow(GridColumns.FirstCol) = thisInstrument.InstrumentDescription
            NewGridRow(GridColumns.ISIN) = thisInstrument.InstrumentISIN
            NewGridRow(GridColumns.Units) = thisInstrument.InstrumentPosition
            NewGridRow(GridColumns.LocalValue) = thisInstrument.InstrumentLocalValue
            NewGridRow(GridColumns.LocalCurrency) = thisInstrument.InstrumentCurrency
            NewGridRow(GridColumns.FundValue) = thisInstrument.InstrumentFundValue
            If (thisInstrument.BasketComponent) Then
              NewGridRow(GridColumns.TargetSubFundWeight) = thisInstrument.InstrumentSubFundTargetWeight
            Else
              NewGridRow(GridColumns.TargetSubFundWeight) = Nothing
            End If

            NewGridRow(GridColumns.ActualSubFundWeight) = thisInstrument.InstrumentSubFundActualWeight
            NewGridRow(GridColumns.ActualTotalFundWeight) = thisInstrument.InstrumentFundActualWeight
            NewGridRow(GridColumns.Price) = thisInstrument.InstrumentPrice

            If (thisInstrument.InstrumentType <> 0) AndAlso (thisInstrument.InstrumentType <> InstrumentTypes.Cash) Then
              NewGridRow(GridColumns.PriceDate) = thisInstrument.InstrumentPriceDate
            End If

            NewGridRow(GridColumns.FXToFund) = thisInstrument.FXToFund
            NewGridRow(GridColumns.Force) = Nothing
            NewGridRow(GridColumns.Ignore) = Nothing
            NewGridRow(GridColumns.Trade) = Nothing
            NewGridRow(GridColumns.Type) = Nothing
            NewGridRow(GridColumns.Counter) = (Counter)

            If (thisInstrument.BasketComponent) Then
              NewGridRow(GridColumns.Force) = False
              Grid_Positions.SetCellStyle(NewGridRow.SafeIndex, GridColumns.Force, Grid_Positions.Styles("Visible"))
              Grid_Positions.SetCellStyle(NewGridRow.SafeIndex, GridColumns.Ignore, Grid_Positions.Styles("Visible"))
            Else
              Grid_Positions.SetCellStyle(NewGridRow.SafeIndex, GridColumns.Force, Grid_Positions.Styles("Hidden"))
              Grid_Positions.SetCellStyle(NewGridRow.SafeIndex, GridColumns.Ignore, Grid_Positions.Styles("Hidden"))
            End If

            ' Modified Row ?

            NewGridRow(GridColumns.Modified) = False

            ModifiedTargetWeight = False
            If (NewGridRow(GridColumns.TargetSubFundWeight) Is Nothing) Then
              ThisTargetWeight = 0.0#
            Else
              ThisTargetWeight = CDbl(NewGridRow(GridColumns.TargetSubFundWeight))
            End If

            If (Grid_Positions_Updates.ContainsKey(thisInstrument.SubFundID)) Then
              ThisSubFundChanges = Grid_Positions_Updates(thisInstrument.SubFundID)

              If (ThisSubFundChanges.ContainsKey(thisInstrument.InstrumentID)) Then

                ThisRowChange = ThisSubFundChanges(thisInstrument.InstrumentID)

                If (ThisRowChange.TargetWeight_Changed) Then
                  NewGridRow(GridColumns.TargetSubFundWeight) = ThisRowChange.TargetWeight
                  ModifiedTargetWeight = True
                  NewGridRow(GridColumns.Modified) = True
                End If

                If (ThisRowChange.Force_Changed) Then
                  NewGridRow(GridColumns.Force) = ThisRowChange.Force
                  NewGridRow(GridColumns.Modified) = True
                End If

                If (ThisRowChange.Ignore_Changed) Then
                  NewGridRow(GridColumns.Ignore) = ThisRowChange.Ignore
                  NewGridRow(GridColumns.Modified) = True
                End If

              End If
            Else
              ThisSubFundChanges = Nothing
            End If

            ' Formatting

            If thisInstrument.InstrumentPosition < 0 Then
              Grid_Positions.SetCellStyle(NewGridRow.SafeIndex, GridColumns.Units, Grid_Positions.Styles("NumberNegative"))
            Else
              Grid_Positions.SetCellStyle(NewGridRow.SafeIndex, GridColumns.Units, Grid_Positions.Styles("NumberPositive"))
            End If

            If thisInstrument.InstrumentLocalValue < 0 Then
              If thisInstrument.IsTransfer Then
                Grid_Positions.SetCellStyle(NewGridRow.SafeIndex, GridColumns.LocalValue, Grid_Positions.Styles("NumberNegativeHighlight"))
              Else
                Grid_Positions.SetCellStyle(NewGridRow.SafeIndex, GridColumns.LocalValue, Grid_Positions.Styles("NumberNegative"))
              End If
              Grid_Positions.SetCellStyle(NewGridRow.SafeIndex, GridColumns.FundValue, Grid_Positions.Styles("NumberNegative"))
            Else
              If thisInstrument.IsTransfer Then
                Grid_Positions.SetCellStyle(NewGridRow.SafeIndex, GridColumns.LocalValue, Grid_Positions.Styles("NumberPositiveHighlight"))
              Else
                Grid_Positions.SetCellStyle(NewGridRow.SafeIndex, GridColumns.LocalValue, Grid_Positions.Styles("NumberPositive"))
              End If

              Grid_Positions.SetCellStyle(NewGridRow.SafeIndex, GridColumns.FundValue, Grid_Positions.Styles("NumberPositive"))
            End If

            If (ModifiedTargetWeight) Then
              If (ThisTargetWeight < 0.0#) Then
                Grid_Positions.SetCellStyle(NewGridRow.SafeIndex, GridColumns.TargetSubFundWeight, Grid_Positions.Styles("NumberNegativeHighlight"))
              Else
                Grid_Positions.SetCellStyle(NewGridRow.SafeIndex, GridColumns.TargetSubFundWeight, Grid_Positions.Styles("NumberPositiveHighlight"))
              End If
            Else
              If (ThisTargetWeight < 0.0#) Then
                Grid_Positions.SetCellStyle(NewGridRow.SafeIndex, GridColumns.TargetSubFundWeight, Grid_Positions.Styles("NumberNegative"))
              Else
                Grid_Positions.SetCellStyle(NewGridRow.SafeIndex, GridColumns.TargetSubFundWeight, Grid_Positions.Styles("NumberPositive"))
              End If
            End If

            ' 

            LastSubFund = thisInstrument.SubFundID
          End If

        Next

        ' Remove obsolete lines

        For Counter = (Grid_Positions.Rows.Count - 1) To 2 Step -1
          ThisGridRow = Grid_Positions.Rows(Counter)

          If (ThisGridRow(GridColumns.Counter) = -9999) Then
            Grid_Positions.RemoveItem(Counter)
          End If
        Next


        ' Calculate Valuation

        Call SetSubFundValuations(Grid_Positions, 1, FundValuations)
        Call SetSubFundWeightTotals(Grid_Positions, GridColumns.TargetSubFundWeight)

        ' Transactions

        Call SetPendingTransactions()


        '  Show only selected Sub Fund

        Dim SelectedChildRange As C1.Win.C1FlexGrid.CellRange = Nothing

        If (SelectedSubFundID > 0) AndAlso gridsubFundMap.ContainsKey(SelectedSubFundID) Then
          SubFundNode = gridsubFundMap(SelectedSubFundID)

          SelectedChildRange = SubFundNode.Node.GetCellRange
          SelectedChildRange.Normalize()

          For ChildCounter = 2 To (Grid_Positions.Rows.Count - 1)
            If (ChildCounter < SelectedChildRange.TopRow) OrElse (ChildCounter > SelectedChildRange.BottomRow) Then
              Grid_Positions.Rows(ChildCounter).Visible = False
            Else
              Grid_Positions.Rows(ChildCounter).Visible = True
            End If
          Next

          While (SubFundNode.Node.Level >= 1)
            SubFundNode = SubFundNode.Node.GetNode(NodeTypeEnum.Parent).Row
            SubFundNode.Visible = True
          End While
        Else
          '' Show everything.
          'For ChildCounter = 0 To (Grid_Positions.Rows.Count - 1)
          '    Grid_Positions.Rows(ChildCounter).Visible = True
          'Next
        End If

        ' Paint Valuations grid.

        Try
          Dim CurrencyDS As RenaissanceDataClass.DSCurrency = MainForm.Load_Table(RenaissanceStandardDatasets.tblCurrency)
          Dim SelectedCurrencies() As RenaissanceDataClass.DSCurrency.tblCurrencyRow
          Dim thisCurrency As RenaissanceDataClass.DSCurrency.tblCurrencyRow

          If (CurrencyDS IsNot Nothing) Then
            SelectedCurrencies = CurrencyDS.tblCurrency.Select("true", "CurrencyCode")

            If (SelectedCurrencies IsNot Nothing) AndAlso (SelectedCurrencies.Length > 0) Then
              For Counter = 0 To (SelectedCurrencies.Length - 1)
                thisCurrency = SelectedCurrencies(Counter)

                If (ValuationGridTotals.ContainsKey(thisCurrency.CurrencyID)) Then

                  NewGridRow = Grid_Valuations.Rows.Add()
                  NewGridRow("Col_Currency") = thisCurrency.CurrencyCode
                  NewGridRow("Col_Value") = ValuationGridTotals(thisCurrency.CurrencyID)
                  NewGridRow("Col_EURO") = ValuationGridTotals(thisCurrency.CurrencyID) * GetFXToFund(thisCurrency.CurrencyID, REFERENCE_REPORT_CURRENCY)
                  If FXForwardsTotals.ContainsKey(thisCurrency.CurrencyID) Then
                    NewGridRow("Col_ForwardOutstanding") = FXForwardsTotals(thisCurrency.CurrencyID)
                  Else
                    NewGridRow("Col_ForwardOutstanding") = 0.0#
                  End If

                  Try
                    If (ValuationGridTotals(thisCurrency.CurrencyID) < 0.0#) Then
                      Grid_Valuations.SetCellStyle(NewGridRow.SafeIndex, Grid_Valuations.Cols("Col_Value").Index, "NumberNegative")
                      Grid_Valuations.SetCellStyle(NewGridRow.SafeIndex, Grid_Valuations.Cols("Col_EURO").Index, "NumberNegative")
                    Else
                      Grid_Valuations.SetCellStyle(NewGridRow.SafeIndex, Grid_Valuations.Cols("Col_Value").Index, "NumberPositive")
                      Grid_Valuations.SetCellStyle(NewGridRow.SafeIndex, Grid_Valuations.Cols("Col_EURO").Index, "NumberPositive")
                    End If

                    If (CDbl(NewGridRow("Col_ForwardOutstanding")) < 0.0#) Then
                      Grid_Valuations.SetCellStyle(NewGridRow.SafeIndex, Grid_Valuations.Cols("Col_ForwardOutstanding").Index, "NumberNegative")
                    Else
                      Grid_Valuations.SetCellStyle(NewGridRow.SafeIndex, Grid_Valuations.Cols("Col_ForwardOutstanding").Index, "NumberPositive")
                    End If

                  Catch ex As Exception
                  End Try

                End If

                If (thisCurrency.CurrencyID = REFERENCE_REPORT_CURRENCY) Then
                  Grid_Valuations.Cols("Col_EURO").Caption = "Asset Value (" & thisCurrency.CurrencyCode & ")"
                End If

              Next
            End If
          End If

        Catch ex As Exception
          MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error Drawing Valuations Grid.", ex.Message, ex.StackTrace, True)
        End Try


      Catch ex As Exception
        MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error Drawing Positions Grid.", ex.Message, ex.StackTrace, True)
      Finally
        InstrumentList.Clear()
      End Try

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error Drawing Positions Grid.", ex.Message, ex.StackTrace, True)
    Finally
      Grid_Positions.Redraw = True
      Grid_Positions.Refresh()
      '
      InPaint = False
    End Try

  End Sub

  ''' <summary>
  ''' Sets the pending transactions.
  ''' </summary>
  Private Sub SetPendingTransactions()
    ' **********************************************************************************
    '
    '
    ' **********************************************************************************
    Dim OrgInPaint As Boolean = InPaint

    ' Stops this process from running during the initialise phase.
    If (Not FormIsValid) Then
      Exit Sub
    End If

    '
    Try
      Dim SubFundNode As C1.Win.C1FlexGrid.Row = Nothing

      Dim Counter As Integer
      Dim ThisGridRow As C1.Win.C1FlexGrid.Row
      Dim NewTransactionRow As C1.Win.C1FlexGrid.Row
      Dim thisFundID As Integer
      Dim thisInstrumentID As UInteger
      Dim SelectedSubFundID As Integer
      Dim thisSubFundID As Integer
      Dim ThisPrice As Double
      Dim ThisFX As Double
      Dim ThisTargetWeight As Double
      Dim thisActualWeight As Double
      Dim ThisPositionChanges As Dictionary(Of UInteger, PositionChangesClass)
      Dim ThisPositionRowChange As PositionChangesClass = Nothing
      Dim SubFundHeirarchyRow As DataRow = Nothing
      Dim InstrumentRow As DSInstrument.tblInstrumentRow
      Dim FundRow As DSFund.tblFundRow
      Dim FundCurrency As String

      Dim SuggestedWorkflowRule As RenaissanceDataClass.DSWorkflowRules.tblWorkflowRulesRow = Nothing
      Dim ExpectedWorkflow As RenaissanceDataClass.DSWorkflows.tblWorkflowsRow = Nothing
      Dim ExpectedTradeStatus As RenaissanceGlobals.TradeStatus
      Dim ThisTransactionChanges As Dictionary(Of UInteger, TransactionChangesClass)
      Dim ThisTransactionRowChange As TransactionChangesClass = Nothing

      Dim FundValuation As Double = 0.0#
      Dim SubFundValuation As Double = 0.0#
      Dim thisTrade_FundPercent As Double = 0.0#
      Dim thisAmountPrecision As Integer = 4
      Dim TradeModified As Boolean

      InPaint = True

      Grid_PendingTransactions.Rows.Count = 1
      Grid_PendingTransfers.Rows.Count = 1

      ' Selected Sub Fund ?

      If (IsNumeric(Combo_SubFund.SelectedValue)) Then
        SelectedSubFundID = CInt(Combo_SubFund.SelectedValue)
      Else
        SelectedSubFundID = 0
      End If

      ' Pending Transaction

      For Counter = (Grid_Positions.Rows.Count - 1) To 2 Step -1

        ThisGridRow = Grid_Positions.Rows(Counter)

        ThisGridRow(GridColumns.Trade) = Nothing
        ThisGridRow(GridColumns.Type) = ""

        thisFundID = CInt(ThisGridRow(GridColumns.FundID))
        thisSubFundID = CInt(ThisGridRow(GridColumns.SubFundID))
        thisInstrumentID = CUInt(ThisGridRow(GridColumns.InstrumentID))
        ThisPrice = CDbl(ThisGridRow(GridColumns.Price))
        ThisFX = CDbl(ThisGridRow(GridColumns.FXToFund))
        TradeModified = False

        FundValuation = FundValuations(thisFundID)

        FundRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, thisFundID), DSFund.tblFundRow)
        InstrumentRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, thisInstrumentID), DSInstrument.tblInstrumentRow)
        FundCurrency = CStr(Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblCurrency, FundRow.FundBaseCurrency, "CurrencyCode"), "???"))

        If (InstrumentRow IsNot Nothing) Then
          thisAmountPrecision = InstrumentRow.InstrumentOrderPrecision
        Else
          thisAmountPrecision = 4
        End If

        ' Ignore if Sub Fund is selected and this is not it..

        If (SelectedSubFundID > 0) AndAlso (SelectedSubFundID <> thisSubFundID) Then
          Continue For
        End If

        If (Not subFundDictionary.TryGetValue(thisSubFundID, SubFundHeirarchyRow)) Then
          If (Not subFundDictionary.TryGetValue(thisSubFundID Or SUBFUND_EFFECTIVEFUNDMODIFIER, SubFundHeirarchyRow)) Then
            SubFundHeirarchyRow = Nothing
          End If
        End If

        If (ThisGridRow.IsNode) OrElse (((SubFundHeirarchyRow Is Nothing) OrElse (CInt(SubFundHeirarchyRow("ReferenceBasket")) = 0)) AndAlso ((ThisGridRow(GridColumns.TargetSubFundWeight) Is Nothing) AndAlso (Nz(ThisGridRow(GridColumns.Force), False) = False))) Then
          Continue For
        End If

        ' Get Sub Fund Node (Has Sub Fund Value !)

        If (gridSubFundMap.ContainsKey(thisSubFundID)) Then
          SubFundNode = gridSubFundMap(thisSubFundID)
        Else
          SubFundNode = gridSubFundMap(thisFundID)
        End If
        SubFundValuation = CDbl(SubFundNode(GridColumns.FundValue))

        ' 

        ThisTargetWeight = CDbl(Nz(ThisGridRow(GridColumns.TargetSubFundWeight), 0.0#))
        thisActualWeight = CDbl(Nz(ThisGridRow(GridColumns.ActualSubFundWeight), 0.0#))

        If (SubFundValuation = 0.0#) Then
          thisTrade_FundPercent = 0.0#
        Else
          thisTrade_FundPercent = (ThisTargetWeight - thisActualWeight) * (SubFundValuation / FundValuation)
        End If

        ' Get User-Entered Position changes, if they exist.

        ThisPositionRowChange = Nothing
        If (Grid_Positions_Updates.ContainsKey(thisSubFundID)) Then
          ThisPositionChanges = Grid_Positions_Updates(thisSubFundID)

          If (ThisPositionChanges.ContainsKey(thisInstrumentID)) Then

            ThisPositionRowChange = ThisPositionChanges(thisInstrumentID)
          End If
        End If

        ' Calculate Trade.
        ' Ignore Cash and notional instruments

        ThisGridRow(GridColumns.Trade) = Nothing
        ThisGridRow(GridColumns.Type) = ""

        If (InstrumentRow Is Nothing) OrElse ((InstrumentRow.InstrumentType <> InstrumentTypes.Cash) AndAlso (InstrumentRow.InstrumentType <> InstrumentTypes.Notional)) Then

          ' Modified Transaction ?

          ThisTransactionRowChange = Nothing
          If (Grid_Transaction_Updates.ContainsKey(thisSubFundID)) Then
            ThisTransactionChanges = Grid_Transaction_Updates(thisSubFundID)

            If (ThisTransactionChanges.ContainsKey(thisInstrumentID)) Then
              ThisTransactionRowChange = ThisTransactionChanges(thisInstrumentID)
            End If
          End If

          If (ThisGridRow(GridColumns.Modified)) Then
            ' Modified Weight

            If (ThisPositionRowChange IsNot Nothing) AndAlso (ThisPositionRowChange.TargetWeight_Changed) AndAlso ((ThisPositionRowChange.Ignore_Changed = False) OrElse (ThisPositionRowChange.Ignore = False)) Then

              If (ThisTransactionRowChange Is Nothing) OrElse (InstrumentRow Is Nothing) OrElse ((ThisTransactionRowChange.TransactionAmount_Changed = False) AndAlso (ThisTransactionRowChange.TransactionSettlement_Changed = False)) Then
                If (Math.Abs(ThisTargetWeight - thisActualWeight) >= 0.00001) Then
                  ThisGridRow(GridColumns.Trade) = thisTrade_FundPercent
                  ThisGridRow(GridColumns.Type) = "New Target Weight"
                End If
              Else
                If (ThisTransactionRowChange.TransactionAmount_Changed) Then
                  thisTrade_FundPercent = (ThisTransactionRowChange.TransactionAmount * ThisPrice * ThisFX * InstrumentRow.InstrumentMultiplier * InstrumentRow.InstrumentContractSize) / FundValuation
                ElseIf (ThisTransactionRowChange.TransactionSettlement_Changed) Then
                  ' When will the Settlement value be in Fund Currency and when in Instrument currency?
                  ' 1) If Not changed 'AutoFX', but Fund Defaults to 'AutoFX' then Settlement is in Fund Currency
                  ' 2) If 'AutoFX' changed and True, then user entered Settlement value will be in Fund Currency.
                  ' 3) Otherwise User changed settlement value is in Instrument currency.

                  If (ThisTransactionRowChange.FXToFundCurrency_Changed = False) AndAlso (FundRow.FundDefaultTradeFXToFundCurrency) Then
                    thisTrade_FundPercent = (ThisTransactionRowChange.TransactionSettlement) / FundValuation
                  ElseIf (ThisTransactionRowChange.FXToFundCurrency_Changed) AndAlso (ThisTransactionRowChange.FXToFundCurrency) Then
                    thisTrade_FundPercent = (ThisTransactionRowChange.TransactionSettlement) / FundValuation
                  Else
                    thisTrade_FundPercent = (ThisTransactionRowChange.TransactionSettlement * ThisFX) / FundValuation
                  End If
                End If

                ThisGridRow(GridColumns.Trade) = thisTrade_FundPercent
                ThisGridRow(GridColumns.Type) = "Modified New Target Weight"
                TradeModified = True
              End If

            Else
              If (ThisPositionRowChange IsNot Nothing) AndAlso ((ThisPositionRowChange.Ignore_Changed = False) OrElse (ThisPositionRowChange.Ignore = False)) Then
                If (ThisPositionRowChange.Force) Then

                  If (ThisTransactionRowChange Is Nothing) OrElse (InstrumentRow Is Nothing) OrElse ((ThisTransactionRowChange.TransactionAmount_Changed = False) AndAlso (ThisTransactionRowChange.TransactionSettlement_Changed = False)) Then
                    ThisGridRow(GridColumns.Trade) = thisTrade_FundPercent
                    ThisGridRow(GridColumns.Type) = "Forced Rebalance"
                  Else
                    If (ThisTransactionRowChange.TransactionAmount_Changed) Then
                      thisTrade_FundPercent = (ThisTransactionRowChange.TransactionAmount * ThisPrice * ThisFX * InstrumentRow.InstrumentMultiplier * InstrumentRow.InstrumentContractSize) / FundValuation
                    ElseIf (ThisTransactionRowChange.TransactionSettlement_Changed) Then
                      If (ThisTransactionRowChange.FXToFundCurrency) Then
                        thisTrade_FundPercent = (ThisTransactionRowChange.TransactionSettlement) / FundValuation
                      Else
                        thisTrade_FundPercent = (ThisTransactionRowChange.TransactionSettlement * ThisFX) / FundValuation
                      End If
                    End If

                    ThisGridRow(GridColumns.Trade) = thisTrade_FundPercent
                    ThisGridRow(GridColumns.Type) = "Modified Forced Rebalance"
                    TradeModified = True
                  End If


                ElseIf (Math.Abs(ThisTargetWeight - thisActualWeight) >= edit_Threshold.Value) OrElse (CBool(Nz(ThisGridRow(GridColumns.Force), False)) AndAlso (Math.Abs(ThisTargetWeight - thisActualWeight) > 0.0#)) Then

                  If (ThisTransactionRowChange Is Nothing) OrElse (InstrumentRow Is Nothing) OrElse ((ThisTransactionRowChange.TransactionAmount_Changed = False) AndAlso (ThisTransactionRowChange.TransactionSettlement_Changed = False)) Then
                    ThisGridRow(GridColumns.Trade) = thisTrade_FundPercent
                    ThisGridRow(GridColumns.Type) = "Rebalance"
                  Else
                    If (ThisTransactionRowChange.TransactionAmount_Changed) Then
                      thisTrade_FundPercent = (ThisTransactionRowChange.TransactionAmount * ThisPrice * ThisFX * InstrumentRow.InstrumentMultiplier * InstrumentRow.InstrumentContractSize) / FundValuation
                    ElseIf (ThisTransactionRowChange.TransactionSettlement_Changed) Then
                      If (ThisTransactionRowChange.FXToFundCurrency) Then
                        thisTrade_FundPercent = (ThisTransactionRowChange.TransactionSettlement) / FundValuation
                      Else
                        thisTrade_FundPercent = (ThisTransactionRowChange.TransactionSettlement * ThisFX) / FundValuation
                      End If
                    End If

                    ThisGridRow(GridColumns.Trade) = thisTrade_FundPercent
                    ThisGridRow(GridColumns.Type) = "Modified Rebalance"
                    TradeModified = True
                  End If

                End If
              End If


            End If

          ElseIf (Math.Abs(ThisTargetWeight - thisActualWeight) >= edit_Threshold.Value) OrElse (CBool(Nz(ThisGridRow(GridColumns.Force), False)) AndAlso (Math.Abs(ThisTargetWeight - thisActualWeight) > 0.0#)) Then

            If (ThisTransactionRowChange Is Nothing) OrElse (InstrumentRow Is Nothing) OrElse ((ThisTransactionRowChange.TransactionAmount_Changed = False) AndAlso (ThisTransactionRowChange.TransactionSettlement_Changed = False)) Then
              ThisGridRow(GridColumns.Trade) = thisTrade_FundPercent
              ThisGridRow(GridColumns.Type) = "Rebalance"
            Else
              If (ThisTransactionRowChange.TransactionAmount_Changed) Then
                thisTrade_FundPercent = (ThisTransactionRowChange.TransactionAmount * ThisPrice * ThisFX * InstrumentRow.InstrumentMultiplier * InstrumentRow.InstrumentContractSize) / FundValuation
              ElseIf (ThisTransactionRowChange.TransactionSettlement_Changed) Then

                ' When will the Settlement value be in Fund Currency and when in Instrument currency?
                ' 1) If Not changed 'AutoFX', but Fund Defaults to 'AutoFX' then Settlement is in Fund Currency
                ' 2) If 'AutoFX' changed and True, then user entered Settlement value will be in Fund Currency.
                ' 3) Otherwise User changed settlement value is in Instrument currency.
                If (ThisTransactionRowChange.FXToFundCurrency_Changed = False) AndAlso (FundRow.FundDefaultTradeFXToFundCurrency) Then
                  thisTrade_FundPercent = (ThisTransactionRowChange.TransactionSettlement) / FundValuation
                ElseIf (ThisTransactionRowChange.FXToFundCurrency_Changed) AndAlso (ThisTransactionRowChange.FXToFundCurrency) Then
                  thisTrade_FundPercent = (ThisTransactionRowChange.TransactionSettlement) / FundValuation
                Else
                  thisTrade_FundPercent = (ThisTransactionRowChange.TransactionSettlement * ThisFX) / FundValuation
                End If

              End If

              ThisGridRow(GridColumns.Trade) = thisTrade_FundPercent
              ThisGridRow(GridColumns.Type) = "Modified Rebalance"
              TradeModified = True
            End If

          Else ' Not Rebalance or Modified :-
            ' 
            If (ThisTransactionRowChange IsNot Nothing) AndAlso ((ThisTransactionRowChange.TransactionAmount_Changed) OrElse (ThisTransactionRowChange.TransactionSettlement_Changed)) Then
              If (ThisTransactionRowChange.TransactionAmount_Changed) Then
                thisTrade_FundPercent = (ThisTransactionRowChange.TransactionAmount * ThisPrice * ThisFX * InstrumentRow.InstrumentMultiplier * InstrumentRow.InstrumentContractSize) / FundValuation
              ElseIf (ThisTransactionRowChange.TransactionSettlement_Changed) Then
                If (ThisTransactionRowChange.FXToFundCurrency) Then
                  thisTrade_FundPercent = (ThisTransactionRowChange.TransactionSettlement) / FundValuation
                Else
                  thisTrade_FundPercent = (ThisTransactionRowChange.TransactionSettlement * ThisFX) / FundValuation
                End If
              End If

              ThisGridRow(GridColumns.Trade) = thisTrade_FundPercent
              ThisGridRow(GridColumns.Type) = "Manual Trade"
              TradeModified = True
            End If

          End If

        End If

        If (ThisGridRow(GridColumns.Trade) IsNot Nothing) Then
          If (CDbl(ThisGridRow(GridColumns.Trade)) < 0.0#) Then
            If (TradeModified) Then
              Grid_Positions.SetCellStyle(ThisGridRow.SafeIndex, GridColumns.Trade, Grid_Positions.Styles("NumberNegativeHighlight"))
            Else
              Grid_Positions.SetCellStyle(ThisGridRow.SafeIndex, GridColumns.Trade, Grid_Positions.Styles("NumberNegative"))
            End If
          Else
            If (TradeModified) Then
              Grid_Positions.SetCellStyle(ThisGridRow.SafeIndex, GridColumns.Trade, Grid_Positions.Styles("NumberPositiveHighlight"))
            Else
              Grid_Positions.SetCellStyle(ThisGridRow.SafeIndex, GridColumns.Trade, Grid_Positions.Styles("NumberPositive"))
            End If

          End If
        End If

        ' Grid_PendingTransactions

        If (ThisGridRow(GridColumns.Trade) IsNot Nothing) AndAlso (thisInstrumentID > 0) Then

          ' NOTE At present InstrumentID are only Venice InstrumentIDs. It may be necessary in the future to use 'Universal' style IDs.

          ExpectedTradeStatus = RenaissanceGlobals.TradeStatus.PendingMO

          SuggestedWorkflowRule = GetExpectedWorkflowRule(MainForm, thisFundID, InstrumentRow.InstrumentType, 0, InstrumentRow.InstrumentCurrencyID, InstrumentRow.InstrumentID, 0, True, False)
          If (SuggestedWorkflowRule IsNot Nothing) Then
            ExpectedWorkflow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblWorkflows, SuggestedWorkflowRule.ResultingWorkflow), DSWorkflows.tblWorkflowsRow)
          Else
            ExpectedWorkflow = Nothing
          End If

          NewTransactionRow = Grid_PendingTransactions.Rows.Add()

          NewTransactionRow("FundID") = thisFundID
          NewTransactionRow("SubFundID") = thisSubFundID
          NewTransactionRow("InstrumentID") = thisInstrumentID

          If (SubFundHeirarchyRow IsNot Nothing) Then
            NewTransactionRow("SubFundName") = CStr(SubFundHeirarchyRow("SubFundName"))
          Else
            NewTransactionRow("SubFundName") = ""
            NewTransactionRow("SubFundID") = thisFundID
          End If

          NewTransactionRow("InstrumentDescription") = InstrumentRow.InstrumentDescription
          NewTransactionRow("ISIN") = InstrumentRow.InstrumentISIN
          NewTransactionRow("Amount") = Math.Round(CDbl(ThisGridRow(GridColumns.Trade)) * FundValuation / (ThisPrice * ThisFX * InstrumentRow.InstrumentMultiplier * InstrumentRow.InstrumentContractSize), thisAmountPrecision)
          If (Double.IsInfinity(NewTransactionRow("Amount"))) Then
            NewTransactionRow("Amount") = 0.0#
          End If

          NewTransactionRow("Price") = ThisGridRow(GridColumns.Price)

          If (ThisTransactionRowChange IsNot Nothing) Then
            If (ThisTransactionRowChange.FXToFundCurrency_Changed) Then
              NewTransactionRow("AutoFX") = ThisTransactionRowChange.FXToFundCurrency
            ElseIf (FundRow.FundDefaultTradeFXToFundCurrency) AndAlso (InstrumentRow.InstrumentCurrencyID <> FundRow.FundBaseCurrency) Then
              NewTransactionRow("AutoFX") = True
            Else
              NewTransactionRow("AutoFX") = False
            End If

            If (ThisTransactionRowChange.FXToFundCurrency) AndAlso ((ThisTransactionRowChange.TransactionSettlement_Changed) OrElse (InstrumentRow.InstrumentOrderOnlyByValue)) Then
              NewTransactionRow("Currency") = FundCurrency
            Else
              NewTransactionRow("Currency") = InstrumentRow.InstrumentCurrency
            End If
          Else
            NewTransactionRow("Currency") = InstrumentRow.InstrumentCurrency
            If (FundRow.FundDefaultTradeFXToFundCurrency) AndAlso (InstrumentRow.InstrumentCurrencyID <> FundRow.FundBaseCurrency) Then
              NewTransactionRow("AutoFX") = True

              ' If 'FX'ing and is by value, then Value must be in Fund Currency
              If (InstrumentRow.InstrumentOrderOnlyByValue) Then
                NewTransactionRow("Currency") = FundCurrency
              End If
            Else
              NewTransactionRow("AutoFX") = False
            End If
          End If

          If (InstrumentRow.InstrumentOrderOnlyByValue) Then
            NewTransactionRow("ValueOrAmount") = "Value"
          Else
            NewTransactionRow("ValueOrAmount") = "Quantity"
          End If

          If (ExpectedWorkflow Is Nothing) Then
            NewTransactionRow("Workflow") = "<None>"
          Else
            NewTransactionRow("Workflow") = ExpectedWorkflow.WorkflowDescription
            ExpectedTradeStatus = ExpectedWorkflow.ResultingTradeStatus
          End If
          NewTransactionRow("Status") = Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblTradeStatus, ExpectedTradeStatus, "TradeStatusName"), ExpectedTradeStatus.ToString)
          NewTransactionRow("TradeStatusID") = ExpectedTradeStatus

          ' Formatting

          If (CDbl(ThisGridRow(GridColumns.Trade)) < 0.0#) Then
            Grid_PendingTransactions.SetCellStyle(NewTransactionRow.SafeIndex, Grid_PendingTransactions.Cols("Amount").Index, Grid_PendingTransactions.Styles("NumberNegative"))
          Else
            Grid_PendingTransactions.SetCellStyle(NewTransactionRow.SafeIndex, Grid_PendingTransactions.Cols("Amount").Index, Grid_PendingTransactions.Styles("NumberPositive"))
          End If

          If (CDbl(ThisGridRow(GridColumns.Price)) < 0.0#) Then
            Grid_PendingTransactions.SetCellStyle(NewTransactionRow.SafeIndex, Grid_PendingTransactions.Cols("Price").Index, Grid_PendingTransactions.Styles("NumberNegative"))
          Else
            Grid_PendingTransactions.SetCellStyle(NewTransactionRow.SafeIndex, Grid_PendingTransactions.Cols("Price").Index, Grid_PendingTransactions.Styles("NumberPositive"))
          End If

          ' Default Settlement Value

          If (NewTransactionRow("ValueOrAmount") = "Value") AndAlso (CBool(NewTransactionRow("AutoFX"))) Then
            NewTransactionRow("Settlement") = Math.Round(CDbl(NewTransactionRow("Amount")) * NewTransactionRow("Price") * InstrumentRow.InstrumentContractSize * InstrumentRow.InstrumentMultiplier) * ThisFX
          Else
            NewTransactionRow("Settlement") = Math.Round(CDbl(NewTransactionRow("Amount")) * NewTransactionRow("Price") * InstrumentRow.InstrumentContractSize * InstrumentRow.InstrumentMultiplier)
          End If

          If (CDbl(NewTransactionRow("Settlement")) < 0.0#) Then
            Grid_PendingTransactions.SetCellStyle(NewTransactionRow.SafeIndex, Grid_PendingTransactions.Cols("Settlement").Index, Grid_PendingTransactions.Styles("NumberNegative"))
          Else
            Grid_PendingTransactions.SetCellStyle(NewTransactionRow.SafeIndex, Grid_PendingTransactions.Cols("Settlement").Index, Grid_PendingTransactions.Styles("NumberPositive"))
          End If

          ' Apply user-Transaction changes (Price or Amount)

          If (Grid_Transaction_Updates.ContainsKey(thisSubFundID)) Then
            ThisTransactionChanges = Grid_Transaction_Updates(thisSubFundID)

            If (ThisTransactionChanges.ContainsKey(thisInstrumentID)) Then
              ThisTransactionRowChange = ThisTransactionChanges(thisInstrumentID)

              ' Custom Price

              If (ThisTransactionRowChange.TransactionPrice_Changed) Then
                NewTransactionRow("Price") = ThisTransactionRowChange.TransactionPrice

                If (CDbl(NewTransactionRow("Price")) < 0.0#) Then
                  Grid_PendingTransactions.SetCellStyle(NewTransactionRow.SafeIndex, Grid_PendingTransactions.Cols("Price").Index, Grid_PendingTransactions.Styles("NumberNegativeHighlight"))
                Else
                  Grid_PendingTransactions.SetCellStyle(NewTransactionRow.SafeIndex, Grid_PendingTransactions.Cols("Price").Index, Grid_PendingTransactions.Styles("NumberPositiveHighlight"))
                End If

              End If

              If (ThisTransactionRowChange.TransactionAmount_Changed) Then
                NewTransactionRow("Amount") = ThisTransactionRowChange.TransactionAmount

                If (CDbl(NewTransactionRow("Amount")) < 0.0#) Then
                  Grid_PendingTransactions.SetCellStyle(NewTransactionRow.SafeIndex, Grid_PendingTransactions.Cols("Amount").Index, Grid_PendingTransactions.Styles("NumberNegativeHighlight"))
                Else
                  Grid_PendingTransactions.SetCellStyle(NewTransactionRow.SafeIndex, Grid_PendingTransactions.Cols("Amount").Index, Grid_PendingTransactions.Styles("NumberPositiveHighlight"))
                End If

                NewTransactionRow("Settlement") = Math.Round(CDbl(NewTransactionRow("Amount")) * NewTransactionRow("Price") * InstrumentRow.InstrumentContractSize * InstrumentRow.InstrumentMultiplier)

                If (CDbl(NewTransactionRow("Settlement")) < 0.0#) Then
                  Grid_PendingTransactions.SetCellStyle(NewTransactionRow.SafeIndex, Grid_PendingTransactions.Cols("Settlement").Index, Grid_PendingTransactions.Styles("NumberNegative"))
                Else
                  Grid_PendingTransactions.SetCellStyle(NewTransactionRow.SafeIndex, Grid_PendingTransactions.Cols("Settlement").Index, Grid_PendingTransactions.Styles("NumberPositive"))
                End If

              ElseIf (ThisTransactionRowChange.TransactionSettlement_Changed) Then

                NewTransactionRow("Settlement") = ThisTransactionRowChange.TransactionSettlement
                NewTransactionRow("ValueOrAmount") = "Value"

                If (CDbl(NewTransactionRow("Settlement")) < 0.0#) Then
                  Grid_PendingTransactions.SetCellStyle(NewTransactionRow.SafeIndex, Grid_PendingTransactions.Cols("Settlement").Index, Grid_PendingTransactions.Styles("NumberNegativeHighlight"))
                Else
                  Grid_PendingTransactions.SetCellStyle(NewTransactionRow.SafeIndex, Grid_PendingTransactions.Cols("Settlement").Index, Grid_PendingTransactions.Styles("NumberPositiveHighlight"))
                End If

                NewTransactionRow("Amount") = Math.Round(CDbl(NewTransactionRow("Settlement")) / (NewTransactionRow("Price") * InstrumentRow.InstrumentContractSize * InstrumentRow.InstrumentMultiplier), thisAmountPrecision)

                If (CDbl(NewTransactionRow("Amount")) < 0.0#) Then
                  Grid_PendingTransactions.SetCellStyle(NewTransactionRow.SafeIndex, Grid_PendingTransactions.Cols("Amount").Index, Grid_PendingTransactions.Styles("NumberNegative"))
                Else
                  Grid_PendingTransactions.SetCellStyle(NewTransactionRow.SafeIndex, Grid_PendingTransactions.Cols("Amount").Index, Grid_PendingTransactions.Styles("NumberPositive"))
                End If

              End If

              If (ThisTransactionRowChange.TradeStatusID_Changed) Then
                NewTransactionRow("TradeStatusID") = ThisTransactionRowChange.TradeStatusID
              End If

            End If
          End If

        End If

      Next

      ' Pending Transfers

      Dim ThisTransfer As PendingTransfer
      Dim NewTransferRow As C1.Win.C1FlexGrid.Row
      Dim SubFundRow As DSSubFund.tblSubFundRow

      For Counter = 0 To (Grid_Transfer_Trades.Count - 1)
        ThisTransfer = Grid_Transfer_Trades(Counter)

        NewTransferRow = Grid_PendingTransfers.Rows.Add()

        NewTransferRow("TransferID") = ThisTransfer.TransferID
        NewTransferRow("IsTransfer") = True
        NewTransferRow("IsNet") = False

        SubFundRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblSubFund, ThisTransfer.FromSubFundID), DSSubFund.tblSubFundRow)
        If (SubFundRow IsNot Nothing) Then
          NewTransferRow("FromSubFundName") = SubFundRow.SubFundName
        Else
          NewTransferRow("FromSubFundName") = "MainFund"
        End If

        SubFundRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblSubFund, ThisTransfer.ToSubFundID), DSSubFund.tblSubFundRow)
        If (SubFundRow IsNot Nothing) Then
          NewTransferRow("ToSubFundName") = SubFundRow.SubFundName
        Else
          NewTransferRow("ToSubFundName") = "MainFund"
        End If

        InstrumentRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, ThisTransfer.InstrumentID), DSInstrument.tblInstrumentRow)
        NewTransferRow("InstrumentDescription") = InstrumentRow.InstrumentDescription
        NewTransferRow("ISIN") = InstrumentRow.InstrumentISIN
        NewTransferRow("Currency") = InstrumentRow.InstrumentCurrency

        NewTransferRow("Amount") = ThisTransfer.Quantity
        NewTransferRow("Price") = ThisTransfer.TradePrice
        NewTransferRow("Settlement") = ThisTransfer.Settlement

      Next

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error in SetPendingTransactions().", ex.Message, ex.StackTrace, True)
    Finally
      InPaint = OrgInPaint
    End Try
  End Sub

  ''' <summary>
  ''' Sets the sub fund valuations.
  ''' </summary>
  ''' <param name="myGrid">My grid.</param>
  ''' <param name="GridIndex">Index of the grid.</param>
  ''' <param name="FundValuations">The fund valuations.</param>
  Private Sub SetSubFundValuations(ByVal myGrid As C1.Win.C1FlexGrid.C1FlexGrid, ByVal GridIndex As Integer, ByVal FundValuations As Dictionary(Of Integer, Double))
    ' **********************************************************************************
    '
    '
    ' **********************************************************************************

    Try
      Dim ThisGridRow As C1.Win.C1FlexGrid.Row = Nothing
      Dim ChildRange As C1.Win.C1FlexGrid.CellRange = Nothing
      Dim ChildCounter As Integer
      Dim SectionValuation As Double = 0.0#

      Dim IsRoot As Boolean = False
      Dim IsRenaissanceFund As Boolean = False
      Dim ThisLevel As Integer

      Dim thisInstrumentID As UInteger
      Dim thisInstrumentRow As RenaissanceDataClass.DSInstrument.tblInstrumentRow = Nothing

      If (GridIndex >= myGrid.Rows.Count) OrElse (myGrid.Rows(GridIndex).IsNode = False) Then
        Exit Sub
      End If

      '

      If (myGrid.Rows(GridIndex).Node.Level = 0) Then
        IsRoot = True
      End If
      ThisLevel = myGrid.Rows(GridIndex).Node.Level

      If (FundValuations.ContainsKey(CInt(myGrid.Rows(GridIndex)(GridColumns.SubFundID)))) Then ' 
        IsRenaissanceFund = True
      End If

      '

      ChildRange = myGrid.Rows(GridIndex).Node.GetCellRange
      ChildRange.Normalize()

      ' Get SubFund Valuation

      For ChildCounter = (ChildRange.TopRow + 1) To ChildRange.BottomRow
        ThisGridRow = Grid_Positions.Rows(ChildCounter)

        If (ThisGridRow.IsNode) Then
          If (ThisGridRow.Node.Level = (ThisLevel + 1)) Then
            ' Recurse

            SetSubFundValuations(myGrid, ThisGridRow.SafeIndex, FundValuations)
          End If
        Else
          SectionValuation += CDbl(ThisGridRow(GridColumns.FundValue))
        End If

      Next

      myGrid.Rows(GridIndex)(GridColumns.FundValue) = SectionValuation

      ' Set Weights

      If (Not IsRoot) Then
        Dim FoundNode As Boolean = False

        For ChildCounter = (ChildRange.TopRow) To ChildRange.BottomRow
          ThisGridRow = Grid_Positions.Rows(ChildCounter)

          If (ChildCounter > ChildRange.TopRow) AndAlso (ThisGridRow.IsNode) Then
            FoundNode = True
          End If

          If (IsRenaissanceFund) Then
            ' Set Actual Fund Weights
            thisInstrumentID = CInt(ThisGridRow(GridColumns.InstrumentID))
            thisInstrumentRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, thisInstrumentID)


            If (SectionValuation = 0.0#) Then
              ThisGridRow(GridColumns.ActualTotalFundWeight) = 0.0#
            Else
              ThisGridRow(GridColumns.ActualTotalFundWeight) = ThisGridRow(GridColumns.FundValue) / SectionValuation
            End If

            If (ThisGridRow.IsNode) Then
              If (ThisGridRow(GridColumns.ActualTotalFundWeight) < 0.0#) Then
                Grid_Positions.SetCellStyle(ThisGridRow.SafeIndex, GridColumns.ActualTotalFundWeight, Grid_Positions.Styles("FundNumberNegative"))
              Else
                Grid_Positions.SetCellStyle(ThisGridRow.SafeIndex, GridColumns.ActualTotalFundWeight, Grid_Positions.Styles("FundNumberPositive"))
              End If
            Else

              If (Math.Abs(ThisGridRow(GridColumns.ActualTotalFundWeight)) >= edit_Warning.Value) AndAlso ((thisInstrumentRow Is Nothing) OrElse (thisInstrumentRow.InstrumentType <> InstrumentTypes.Cash)) Then
                If (ThisGridRow(GridColumns.ActualTotalFundWeight) < 0.0#) Then
                  Grid_Positions.SetCellStyle(ThisGridRow.SafeIndex, GridColumns.ActualTotalFundWeight, Grid_Positions.Styles("NumberNegativeWarning"))
                  ' ThisGridRow.Style = Grid_Positions.Styles("NumberNegativeWarning")
                Else
                  Grid_Positions.SetCellStyle(ThisGridRow.SafeIndex, GridColumns.ActualTotalFundWeight, Grid_Positions.Styles("NumberPositiveWarning"))
                End If

              ElseIf (ThisGridRow(GridColumns.ActualTotalFundWeight) < 0.0#) Then
                Grid_Positions.SetCellStyle(ThisGridRow.SafeIndex, GridColumns.ActualTotalFundWeight, Grid_Positions.Styles("NumberNegative"))
              Else
                Grid_Positions.SetCellStyle(ThisGridRow.SafeIndex, GridColumns.ActualTotalFundWeight, Grid_Positions.Styles("NumberPositive"))
              End If
            End If
          End If

          If (Not FoundNode) Then ' OrElse (ThisGridRow.IsNode) Then
            If (SectionValuation = 0.0#) Then
              ThisGridRow(GridColumns.ActualSubFundWeight) = 0.0#
            Else
              ThisGridRow(GridColumns.ActualSubFundWeight) = ThisGridRow(GridColumns.FundValue) / SectionValuation
            End If

            If (ThisGridRow(GridColumns.ActualSubFundWeight) < 0.0#) Then
              Grid_Positions.SetCellStyle(ThisGridRow.SafeIndex, GridColumns.ActualSubFundWeight, Grid_Positions.Styles("NumberNegative"))
            Else
              Grid_Positions.SetCellStyle(ThisGridRow.SafeIndex, GridColumns.ActualSubFundWeight, Grid_Positions.Styles("NumberPositive"))
            End If

          End If


        Next
      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error setting Sub Fund valuations.", ex.StackTrace, True)
    End Try
  End Sub

  ''' <summary>
  ''' Sets the sub fund weight totals.
  ''' </summary>
  ''' <param name="myGrid">My grid.</param>
  ''' <param name="WeightColID">The weight col ID.</param>
  Private Sub SetSubFundWeightTotals(ByVal myGrid As C1.Win.C1FlexGrid.C1FlexGrid, ByVal WeightColID As Integer)
    ' **********************************************************************************
    '
    '
    ' **********************************************************************************

    Dim ThisGridRow As C1.Win.C1FlexGrid.Row = Nothing
    Dim LastGridNode As C1.Win.C1FlexGrid.Row = Nothing
    Dim GridCounter As Integer

    Try

      For GridCounter = 1 To (myGrid.Rows.Count - 1)
        ThisGridRow = myGrid.Rows(GridCounter)

        If (ThisGridRow.IsNode) Then
          LastGridNode = ThisGridRow
          LastGridNode(WeightColID) = Nothing
        Else
          If (LastGridNode IsNot Nothing) AndAlso (ThisGridRow(WeightColID) IsNot Nothing) AndAlso (CDbl(ThisGridRow(WeightColID)) <> 0.0#) Then
            LastGridNode(WeightColID) = CDbl(LastGridNode(WeightColID)) + CDbl(ThisGridRow(WeightColID))

            If CDbl(LastGridNode(WeightColID)) < 0.0# Then
              myGrid.SetCellStyle(LastGridNode.SafeIndex, WeightColID, myGrid.Styles("FundNumberNegativeHighlight"))
            ElseIf CDbl(LastGridNode(WeightColID)) = 1.0# Then
              myGrid.SetCellStyle(LastGridNode.SafeIndex, WeightColID, myGrid.Styles("FundNumberPositive"))
            Else
              myGrid.SetCellStyle(LastGridNode.SafeIndex, WeightColID, myGrid.Styles("FundNumberPositiveHighlight"))
            End If
          End If
        End If

      Next

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error setting Sub Fund totals.", ex.StackTrace, True)
    End Try
  End Sub

  ''' <summary>
  ''' Handles the AfterDragColumn event of the Grid_Positions control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="C1.Win.C1FlexGrid.DragRowColEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Positions_AfterDragColumn(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.DragRowColEventArgs) Handles Grid_Positions.AfterDragColumn
    ' **********************************************************************************
    ' Handle the Moving of Grid columns
    '
    ' Simply re-set the values in the GridColumns class
    '
    ' **********************************************************************************
    Call GridColumns.Init_GridColumnsClass(Grid_Positions)
  End Sub

#End Region

#Region " Button And Grid events."

  ''' <summary>
  ''' Handles the CellChanged event of the Grid_Positions control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="C1.Win.C1FlexGrid.RowColEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Positions_CellChanged(ByVal sender As System.Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles Grid_Positions.CellChanged
    ' ******************************************************************************************************
    '
    '
    ' ******************************************************************************************************

    If (InPaint) Then
      Exit Sub
    End If

    Try
      Dim ThisSubFund As Integer
      Dim ThisInstrument As UInteger

      Dim ThisSubFundChanges As Dictionary(Of UInteger, PositionChangesClass)
      Dim ThisGridRow As C1.Win.C1FlexGrid.Row
      Dim ThisRowChange As PositionChangesClass

      If (e.Col = GridColumns.TargetSubFundWeight) OrElse (e.Col = GridColumns.Force) OrElse (e.Col = GridColumns.Ignore) Then


        ' Grid_Positions_Updates

        ThisGridRow = Grid_Positions.Rows(e.Row)
        ThisSubFund = CInt(ThisGridRow(GridColumns.SubFundID))
        ThisInstrument = CUInt(ThisGridRow(GridColumns.InstrumentID))

        If (Grid_Positions_Updates.ContainsKey(ThisSubFund)) Then
          ThisSubFundChanges = Grid_Positions_Updates(ThisSubFund)
        Else
          ThisSubFundChanges = New Dictionary(Of UInteger, PositionChangesClass)
          Grid_Positions_Updates.Add(ThisSubFund, ThisSubFundChanges)
        End If

        If (ThisSubFundChanges.ContainsKey(ThisInstrument)) Then
          ThisRowChange = ThisSubFundChanges(ThisInstrument)
        Else
          ThisRowChange = New PositionChangesClass
          ThisSubFundChanges(ThisInstrument) = ThisRowChange
        End If

        Select Case e.Col

          Case GridColumns.TargetSubFundWeight

            If (CDbl(Grid_Positions(e.Row, e.Col)) < 0.0#) Then
              Grid_Positions.SetCellStyle(e.Row, e.Col, Grid_Positions.Styles("NumberNegativeHighlight"))
            Else
              Grid_Positions.SetCellStyle(e.Row, e.Col, Grid_Positions.Styles("NumberPositiveHighlight"))
            End If

            ThisRowChange.TargetWeight = CDbl(ThisGridRow(GridColumns.TargetSubFundWeight))

            ' Prevent recursion !
            If (ThisGridRow.IsNode = False) Then
              Call SetSubFundWeightTotals(Grid_Positions, GridColumns.TargetSubFundWeight)
            End If

          Case GridColumns.Force

            ThisRowChange.Force = CBool(Nz(ThisGridRow(GridColumns.Force), False))

            If (ThisRowChange.Force) Then
              InPaint = True
              ThisGridRow(GridColumns.Ignore) = False
              ThisRowChange.Ignore = False
            End If

          Case GridColumns.Ignore

            ThisRowChange.Ignore = CBool(Nz(ThisGridRow(GridColumns.Ignore), False))

            If (ThisRowChange.Ignore) Then
              InPaint = True
              ThisGridRow(GridColumns.Force) = False
              ThisRowChange.Force = False
            End If

        End Select

        ' Refresh Pending Transactions.

        Grid_Positions(e.Row, GridColumns.Modified) = True

        InPaint = False

        SetPendingTransactions()

      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Grid_Positions_CellChanged().", ex.StackTrace, True)
    Finally
      InPaint = False
    End Try
  End Sub

  ''' <summary>
  ''' Handles the ValueChanged event of the edit_Threshold control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub edit_Threshold_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles edit_Threshold.ValueChanged
    ' ******************************************************************************************************
    ' ******************************************************************************************************

    Try
      If (InPaint = False) Then
        SetPendingTransactions()
      End If
    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Handles the ValueChanged event of the edit_Warning control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub edit_Warning_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles edit_Warning.ValueChanged
    ' ******************************************************************************************************
    ' ******************************************************************************************************

    Try
      If (InPaint = False) Then
        Call SetSubFundValuations(Grid_Positions, 1, FundValuations)
      End If
    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Handles the BeforeMouseDown event of the Grid_PendingTransactions control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="C1.Win.C1FlexGrid.BeforeMouseDownEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_PendingTransactions_BeforeMouseDown(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.BeforeMouseDownEventArgs) Handles Grid_PendingTransactions.BeforeMouseDown
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      If (LastEnteredTransactionsGridRow <= 0) OrElse (LastEnteredTransactionsGridRow >= Grid_PendingTransactions.Rows.Count) Then
        Menu_CancelManualChange.Visible = False
        Menu_TransactionsSeparator.Visible = False
        Exit Sub
      End If

      Dim thisRow As C1.Win.C1FlexGrid.Row = Grid_PendingTransactions.Rows(LastEnteredTransactionsGridRow)
      Dim FundID As Integer = CInt(thisRow("FundID"))
      Dim SubFundID As Integer = CInt(thisRow("SubFundID"))
      Dim InstrumentID As Integer = CInt(thisRow("InstrumentID"))
      Dim ThisTransactionRowChange As TransactionChangesClass = Nothing
      Dim ThisTransactionChanges As Dictionary(Of UInteger, TransactionChangesClass)

      ThisTransactionRowChange = Nothing
      If (Grid_Transaction_Updates.ContainsKey(SubFundID)) Then
        ThisTransactionChanges = Grid_Transaction_Updates(SubFundID)

        If (ThisTransactionChanges.ContainsKey(InstrumentID)) Then
          Menu_CancelManualChange.Visible = True
          Menu_TransactionsSeparator.Visible = True
          Exit Sub
        End If
      End If

      Menu_CancelManualChange.Visible = False
      Menu_TransactionsSeparator.Visible = False
    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Handles the Click event of the Menu_CancelManualChange control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Menu_CancelManualChange_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_CancelManualChange.Click
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      If (LastEnteredTransactionsGridRow <= 0) OrElse (LastEnteredTransactionsGridRow >= Grid_PendingTransactions.Rows.Count) Then
        Exit Sub
      End If

      Dim thisRow As C1.Win.C1FlexGrid.Row = Grid_PendingTransactions.Rows(LastEnteredTransactionsGridRow)
      Dim FundID As Integer = CInt(thisRow("FundID"))
      Dim SubFundID As Integer = CInt(thisRow("SubFundID"))
      Dim InstrumentID As Integer = CInt(thisRow("InstrumentID"))
      Dim ThisTransactionRowChange As TransactionChangesClass = Nothing
      Dim ThisTransactionChanges As Dictionary(Of UInteger, TransactionChangesClass)

      ThisTransactionRowChange = Nothing
      If (Grid_Transaction_Updates.ContainsKey(SubFundID)) Then
        ThisTransactionChanges = Grid_Transaction_Updates(SubFundID)

        If (ThisTransactionChanges.ContainsKey(InstrumentID)) Then
          ThisTransactionChanges.Remove(InstrumentID)
          Call SetPendingTransactions()
        End If
      End If

    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Handles the KeyDown event of the Grid_PendingTransactions control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_PendingTransactions_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Grid_PendingTransactions.KeyDown
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        Try
          If e.Control And (e.KeyCode = Keys.C) Then
            Call MainForm.CopyGridSelection(CType(sender, C1.Win.C1FlexGrid.C1FlexGrid), True, False, True)
          End If
        Catch ex As Exception
        End Try

      End If
    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Handles the MouseEnterCell event of the Grid_PendingTransactions control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="C1.Win.C1FlexGrid.RowColEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_PendingTransactions_MouseEnterCell(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles Grid_PendingTransactions.MouseEnterCell
    ' *******************************************************************************
    '
    ' *******************************************************************************
    Try
      LastEnteredTransactionsGridRow = e.Row
      LastEnteredTransactionsGridCol = e.Col

      If (e.Row <= 0) Then
        Grid_PendingTransactions.ContextMenuStrip = Nothing
      Else
        Grid_PendingTransactions.ContextMenuStrip = ContextMenu_Transactions
      End If

    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Handles the CellChanged event of the Grid_PendingTransactions control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="C1.Win.C1FlexGrid.RowColEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_PendingTransactions_CellChanged(ByVal sender As System.Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles Grid_PendingTransactions.CellChanged
    ' ******************************************************************************************************
    '
    '
    ' ******************************************************************************************************

    Try
      Dim ThisSubFund As Integer
      Dim ThisInstrument As UInteger
      Dim AmountIndex As Integer = Grid_PendingTransactions.Cols("Amount").SafeIndex
      Dim PriceIndex As Integer = Grid_PendingTransactions.Cols("Price").SafeIndex
      Dim SettlementIndex As Integer = Grid_PendingTransactions.Cols("Settlement").SafeIndex
      Dim AutoFXIndex As Integer = Grid_PendingTransactions.Cols("AutoFX").SafeIndex
      Dim TradeStatusIDIndex As Integer = Grid_PendingTransactions.Cols("TradeStatusID").SafeIndex
      Dim InstrumentRow As DSInstrument.tblInstrumentRow
      Dim FundRow As DSFund.tblFundRow
      Dim thisFundID As Integer

      Dim ThisTransactionChanges As Dictionary(Of UInteger, TransactionChangesClass)
      Dim ThisGridRow As C1.Win.C1FlexGrid.Row
      Dim ThisRowChange As TransactionChangesClass
      Dim FundCurrency As String

      If (InPaint) Then
        Exit Sub
      End If

      If (e.Col = AmountIndex) OrElse (e.Col = PriceIndex) OrElse (e.Col = SettlementIndex) OrElse (e.Col = TradeStatusIDIndex) OrElse (e.Col = AutoFXIndex) Then


        ' Grid_PendingTransactions

        ThisGridRow = Grid_PendingTransactions.Rows(e.Row)
        thisFundID = CInt(ThisGridRow("FundID"))
        ThisSubFund = CInt(ThisGridRow("SubFundID"))
        ThisInstrument = CUInt(ThisGridRow("InstrumentID"))

        If (Grid_Transaction_Updates.ContainsKey(ThisSubFund)) Then
          ThisTransactionChanges = Grid_Transaction_Updates(ThisSubFund)
        Else
          ThisTransactionChanges = New Dictionary(Of UInteger, TransactionChangesClass)
          Grid_Transaction_Updates.Add(ThisSubFund, ThisTransactionChanges)
        End If

        If (ThisTransactionChanges.ContainsKey(ThisInstrument)) Then
          ThisRowChange = ThisTransactionChanges(ThisInstrument)
        Else
          ThisRowChange = New TransactionChangesClass
          ThisTransactionChanges(ThisInstrument) = ThisRowChange
        End If

        FundRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, thisFundID), DSFund.tblFundRow)
        InstrumentRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, CInt(PertracDataClass.GetInstrumentFromID(ThisInstrument))), DSInstrument.tblInstrumentRow)
        FundCurrency = CStr(Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblCurrency, FundRow.FundBaseCurrency, "CurrencyCode"), "???"))

        Select Case e.Col

          Case AmountIndex

            ThisRowChange.TransactionAmount = CDbl(Nz(ThisGridRow(AmountIndex), 0.0#))

            If (CDbl(Grid_PendingTransactions(e.Row, e.Col)) < 0.0#) Then
              Grid_PendingTransactions.SetCellStyle(e.Row, e.Col, Grid_PendingTransactions.Styles("NumberNegativeHighlight"))
            Else
              Grid_PendingTransactions.SetCellStyle(e.Row, e.Col, Grid_PendingTransactions.Styles("NumberPositiveHighlight"))
            End If

            Try
              InPaint = True

              Dim SettlementAmount As Double

              If (InstrumentRow IsNot Nothing) Then
                SettlementAmount = ThisRowChange.TransactionAmount * CDbl(Nz(ThisGridRow(PriceIndex), 0.0#)) * InstrumentRow.InstrumentContractSize * InstrumentRow.InstrumentMultiplier
              Else
                SettlementAmount = 0.0#
              End If

              If Math.Abs(CDbl(Nz(ThisGridRow(SettlementIndex), 0.0#)) - SettlementAmount) > 0.01 Then
                ThisGridRow(SettlementIndex) = SettlementAmount

                If (SettlementAmount < 0.0#) Then
                  Grid_PendingTransactions.SetCellStyle(e.Row, SettlementIndex, Grid_PendingTransactions.Styles("NumberNegative"))
                Else
                  Grid_PendingTransactions.SetCellStyle(e.Row, SettlementIndex, Grid_PendingTransactions.Styles("NumberPositive"))
                End If

              End If

            Catch ex As Exception
            Finally
              InPaint = False
            End Try

          Case AutoFXIndex
            ThisRowChange.FXToFundCurrency = CBool(Nz(ThisGridRow(AutoFXIndex), False))

            If (ThisRowChange.TransactionSettlement_Changed) Then
              ThisGridRow("Currency") = FundCurrency
            Else
              ThisGridRow("Currency") = InstrumentRow.InstrumentCurrency
            End If

          Case PriceIndex

            ThisRowChange.TransactionPrice = CDbl(Nz(ThisGridRow(PriceIndex), 0.0#))

            If (CDbl(Grid_PendingTransactions(e.Row, e.Col)) < 0.0#) Then
              Grid_PendingTransactions.SetCellStyle(e.Row, e.Col, Grid_PendingTransactions.Styles("NumberNegativeHighlight"))
            Else
              Grid_PendingTransactions.SetCellStyle(e.Row, e.Col, Grid_PendingTransactions.Styles("NumberPositiveHighlight"))
            End If

          Case SettlementIndex

            ThisRowChange.TransactionSettlement = CDbl(Nz(ThisGridRow(SettlementIndex), 0.0#))

            If (CDbl(Grid_PendingTransactions(e.Row, e.Col)) < 0.0#) Then
              Grid_PendingTransactions.SetCellStyle(e.Row, e.Col, Grid_PendingTransactions.Styles("NumberNegativeHighlight"))
            Else
              Grid_PendingTransactions.SetCellStyle(e.Row, e.Col, Grid_PendingTransactions.Styles("NumberPositiveHighlight"))
            End If

            Try
              InPaint = True

              Dim TransactionAmount As Double

              If (InstrumentRow IsNot Nothing) Then
                TransactionAmount = ThisRowChange.TransactionSettlement / (CDbl(Nz(ThisGridRow(PriceIndex), 0.0#)) * InstrumentRow.InstrumentContractSize * InstrumentRow.InstrumentMultiplier)
              Else
                TransactionAmount = 0.0#
              End If

              If Math.Abs(CDbl(Nz(ThisGridRow(AmountIndex), 0.0#)) - TransactionAmount) > 0.000001 Then
                ThisGridRow(AmountIndex) = TransactionAmount

                If (TransactionAmount < 0.0#) Then
                  Grid_PendingTransactions.SetCellStyle(e.Row, AmountIndex, Grid_PendingTransactions.Styles("NumberNegative"))
                Else
                  Grid_PendingTransactions.SetCellStyle(e.Row, AmountIndex, Grid_PendingTransactions.Styles("NumberPositive"))
                End If

              End If

            Catch ex As Exception
            Finally
              InPaint = False
            End Try

          Case TradeStatusIDIndex

            ThisRowChange.TradeStatusID = (Nz(ThisGridRow(TradeStatusIDIndex), TradeStatus.PendingMO))


        End Select

        ' Updates Settlement amounts and formatting

        SetPendingTransactions()

      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error in Grid_PendingTransactions_CellChanged().", ex.Message, ex.StackTrace, True)
    End Try
  End Sub

  ''' <summary>
  ''' Handles the PercentageValue event of the edit_TransferQuantity control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub edit_TransferQuantity_PercentageValue(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles edit_TransferQuantity.PercentageValue
    ' ******************************************************************************************************
    ' Translate Transfer percentage to reqular quantity
    '
    ' ******************************************************************************************************

    Try

      Dim InstrumentID As Integer
      Dim InstrumentRow As DSInstrument.tblInstrumentRow
      Dim FundRow As DSFund.tblFundRow
      Dim PercentageValue As Double = edit_TransferQuantity.Value
      Dim FundID As Integer
      Dim FundValuation As Double
      Dim FundFX As Double
      Dim InstrumentPrice As Double

      ' Fund ID :

      If (IsNumeric(Combo_TransactionFund.SelectedValue)) Then
        FundID = CInt(Combo_TransactionFund.SelectedValue)
      Else
        edit_TransferQuantity.Value = 0.0#
        Exit Sub
      End If

      ' Instrument ID :

      If (IsNumeric(Combo_Instrument.SelectedValue)) Then
        InstrumentID = CInt(Combo_Instrument.SelectedValue)
      Else
        edit_TransferQuantity.Value = 0.0#
        Exit Sub
      End If

      ' Fund and Instrument Details :

      FundRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, FundID), DSFund.tblFundRow)
      InstrumentRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, InstrumentID), DSInstrument.tblInstrumentRow)

      If (InstrumentRow Is Nothing) Then
        edit_TransferQuantity.Value = 0.0#
        Exit Sub
      End If

      ' Fund Valuation :

      FundValuation = FundValuations(FundID)

      ' Instrument Price :

      If (InstrumentRow.InstrumentType = InstrumentTypes.Cash) Then
        InstrumentPrice = 1.0#
      Else
        InstrumentPrice = GetInstrumentPrice(InstrumentID)
      End If
      If (InstrumentPrice = 0.0#) Then
        edit_TransferQuantity.Value = 0.0#
        Exit Sub
      End If

      ' FX : 

      FundFX = GetFXToFund(InstrumentRow.InstrumentCurrencyID, FundRow.FundBaseCurrency)

      ' OK, at last....

      edit_TransferQuantity.Value = (PercentageValue * FundValuation) / (Math.Abs(InstrumentPrice) * FundFX)

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error in edit_TransferQuantity_PercentageValue().", ex.Message, ex.StackTrace, True)
    End Try

  End Sub

  ''' <summary>
  ''' Handles the BeforeMouseDown event of the Grid_Positions control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="C1.Win.C1FlexGrid.BeforeMouseDownEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Positions_BeforeMouseDown(ByVal sender As System.Object, ByVal e As C1.Win.C1FlexGrid.BeforeMouseDownEventArgs) Handles Grid_Positions.BeforeMouseDown
    ' *******************************************************************************
    '
    ' *******************************************************************************


    Try
      If (e.Button = Windows.Forms.MouseButtons.Left) Then
        Dim hti As HitTestInfo = Grid_Positions.HitTest(e.X, e.Y)

        If (hti.Row < 0) OrElse (hti.Row >= Grid_Positions.Rows.Count) Then
          Exit Sub
        End If

        ' Allow dragging on any non-Editable Column on Any Non-Node Row.
        If (Grid_Positions.Rows(hti.Row).IsNode = False) AndAlso (Grid_Positions.Cols(hti.Column).AllowEditing = False) Then
          'select the row 
          Dim index As Integer = hti.Row
          Grid_Positions.Select(index, 0, index, Grid_Positions.Cols.Count - 1, False)

          'do drag drop
          Dim dd As DragDropEffects = Grid_Positions.DoDragDrop(index, DragDropEffects.Move)

          'if it worked :
          If dd = DragDropEffects.Move Then
            ' Anything To do here ?
            dd = dd
          End If
        End If

      End If

    Catch ex As Exception

    End Try

  End Sub

  ''' <summary>
  ''' Handles the DragOver event of the Grid_Positions control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.Windows.Forms.DragEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Positions_DragOver(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles Grid_Positions.DragOver
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Try

      'check that we have the type of data we want
      If e.Data.GetDataPresent(GetType(Integer)) Then
        Dim pt As Point = Grid_Positions.PointToClient(New Point(e.X, e.Y))
        Dim hti As HitTestInfo = Grid_Positions.HitTest(pt.X, pt.Y)

        If (Grid_Positions.Rows(hti.Row).IsNode = False) OrElse (Grid_Positions.Rows(hti.Row).Node.Level > 0) Then
          e.Effect = DragDropEffects.Move
        End If
      ElseIf e.Data.GetDataPresent(GetType(String)) Then
        Dim pt As Point = Grid_Positions.PointToClient(New Point(e.X, e.Y))
        Dim hti As HitTestInfo = Grid_Positions.HitTest(pt.X, pt.Y)

        If (hti.Row > 0) Then
          If (Grid_Positions.Rows(hti.Row).Node.Level > 0) Then
            e.Effect = DragDropEffects.Move
          End If
        End If



      End If

    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Handles the DragDrop event of the Grid_Positions control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.Windows.Forms.DragEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Positions_DragDrop(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles Grid_Positions.DragDrop
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Try

      'find the drop position
      Dim pt As Point = Grid_Positions.PointToClient(New Point(e.X, e.Y))
      Dim hti As HitTestInfo = Grid_Positions.HitTest(pt.X, pt.Y)
      Dim TargetIndex As Integer = hti.Row
      Dim SourceIndex As Integer = CType(e.Data.GetData(GetType(Integer)), Integer)
      Dim DragString As String = CType(e.Data.GetData(GetType(String)), String)
      Dim TargetRow As C1.Win.C1FlexGrid.Row
      Dim SourceRow As C1.Win.C1FlexGrid.Row
      Dim DragStringArray() As String
      Dim TargetSubFund As Integer
      Dim SubFundNode As C1.Win.C1FlexGrid.Row

      If DragString = "DragInstrument" Then
        ' Temp Instrument 

        If (IsNumeric(Combo_Instrument.SelectedValue)) Then
          TargetRow = Grid_Positions.Rows(TargetIndex)

          If (TargetRow.Index <= 0) Then
            Exit Sub
          End If

          TargetSubFund = CInt(TargetRow(GridColumns.SubFundID))
          Dim thisAdditionalInstruments As Dictionary(Of Integer, Boolean)
          Dim thisInstrumentID As Integer = CInt(Combo_Instrument.SelectedValue)

          If (Grid_Positions_AdditionalInstruments.ContainsKey(TargetSubFund)) Then
            thisAdditionalInstruments = Grid_Positions_AdditionalInstruments(TargetSubFund)
          Else
            thisAdditionalInstruments = New Dictionary(Of Integer, Boolean)
            Grid_Positions_AdditionalInstruments.Add(TargetSubFund, thisAdditionalInstruments)
          End If

          If (Not thisAdditionalInstruments.ContainsKey(thisInstrumentID)) Then
            thisAdditionalInstruments.Add(thisInstrumentID, True)

            SubFundNode = gridSubFundMap(TargetSubFund)

            If (SubFundNode IsNot Nothing) Then
              SubFundNode.Node.Expanded = True
            End If

            PaintPositionsGrid()
          End If
        End If
      ElseIf (DragString IsNot Nothing) AndAlso (DragString.Length > 0) Then
        ' Basket ?

        DragStringArray = DragString.Split(ControlChars.CrLf.ToCharArray(), StringSplitOptions.RemoveEmptyEntries)

        If (DragStringArray.Length > 0) Then

          Try
            TargetRow = Grid_Positions.Rows(TargetIndex)
            TargetSubFund = 0

            If (TargetRow.Index > 0) Then
              TargetSubFund = CInt(TargetRow(GridColumns.SubFundID))

              SubFundNode = gridSubFundMap(TargetSubFund)

              If (SubFundNode IsNot Nothing) Then
                SubFundNode.Node.Expanded = True
              End If
            End If

            ProcessDraggedBasket(DragStringArray, TargetSubFund)

            e.Effect = DragDropEffects.Copy
          Catch ex As Exception
          End Try

        End If

      ElseIf SourceIndex > 0 Then
        ' Transfer...

        TargetRow = Grid_Positions.Rows(TargetIndex)
        SourceRow = Grid_Positions.Rows(SourceIndex)

        If (SourceRow.Index <= 0) OrElse (TargetRow.Index <= 0) OrElse (SourceIndex = TargetIndex) Then
          Exit Sub
        End If

        TargetSubFund = CInt(TargetRow(GridColumns.SubFundID))
        Dim SourceSubFund As Integer = CInt(SourceRow(GridColumns.SubFundID))
        Dim ThisUniversalID As UInteger = CUInt(SourceRow(GridColumns.InstrumentID))
        Dim InstrumentID As Integer

        'SubFundNode = gridSubFundMap(TargetSubFund)
        'If (SubFundNode IsNot Nothing) Then
        '  SubFundNode.Node.Expanded = True
        'End If

        'If RenaissancePertracDataClass.PertracDataClass.GetFlagsFromID(ThisUniversalID) = RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.VeniceInstrument Then
        'End If

        InstrumentID = CInt(RenaissancePertracDataClass.PertracDataClass.GetInstrumentFromID(ThisUniversalID))

        Combo_Instrument.SelectedValue = InstrumentID
        Combo_FromSubFund.SelectedValue = SourceSubFund
        Combo_ToSubFund.SelectedValue = TargetSubFund

        If (CType(Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, InstrumentID, "InstrumentType"), 0), InstrumentTypes) = InstrumentTypes.Cash) Then
          edit_TransferQuantity.Value = CDbl(SourceRow(GridColumns.Units))
        Else
          edit_TransferQuantity.Value = CDbl(SourceRow(GridColumns.Units))
        End If

      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error in Grid_Positions_DragDrop().", ex.Message, ex.StackTrace, True)
    End Try

  End Sub

  ''' <summary>
  ''' Processes the dragged basket.
  ''' </summary>
  ''' <param name="DragStringArray">The drag string array.</param>
  ''' <param name="SubFundDraggedTo">The sub fund dragged to.</param>
  Private Sub ProcessDraggedBasket(ByVal DragStringArray() As String, ByVal SubFundDraggedTo As Integer)
    ' *******************************************************************************
    '
    ' *******************************************************************************
    Try
      Dim LoadedFundID As Integer = 0
      Dim FundID As Integer = 0
      Dim SubFundID As Integer
      Dim SubFundName As String = ""
      Dim Counter As Integer
      Dim TempArray() As String
      Dim BasketStartIndex As Integer = 0
      Dim SubFundHeirarchySelection() As DataRow
      Dim InstrumentIDIndex As Integer = (-1)
      Dim ISINIndex As Integer = (-1)
      Dim CurrencyIndex As Integer = (-1)
      Dim WeightIndex As Integer = (-1)

      Dim DragStringParsed(DragStringArray.Length) As Array
      Dim InstrumentIDs(-1) As Integer
      Dim InstrumentWeights(-1) As Double
      Dim thisInstrumentDS As RenaissanceDataClass.DSInstrument = Nothing
      Dim thisInstrumentTable As RenaissanceDataClass.DSInstrument.tblInstrumentDataTable = Nothing
      Dim SelectedInstrumentRows() As RenaissanceDataClass.DSInstrument.tblInstrumentRow = Nothing
      Dim thisInstrumentRow As RenaissanceDataClass.DSInstrument.tblInstrumentRow = Nothing

      ' Fund ID :

      If (IsNumeric(Combo_TransactionFund.SelectedValue)) Then
        LoadedFundID = CInt(Combo_TransactionFund.SelectedValue)
      Else
        Exit Sub
      End If

      ' Get Header details and Basket Column ordinals.

      For Counter = 0 To (DragStringArray.Length - 1)
        DragStringParsed(Counter) = DragStringArray(Counter).Split(New Char() {Chr(9)}, 2, StringSplitOptions.None)
      Next

      For Counter = 0 To (DragStringArray.Length - 1)
        TempArray = DragStringParsed(Counter)

        Select Case (Trim(TempArray(0)).ToUpper)

          Case "FUND"
            If (IsNumeric(Trim(TempArray(1)))) Then FundID = CInt(Trim(TempArray(1)))

          Case "SUB FUND ID", "SUBFUND ID", "SUBFUNDID"
            If (IsNumeric(Trim(TempArray(1)))) Then SubFundID = CInt(Trim(TempArray(1)))

          Case "SUB FUND NAME", "SUBFUND NAME", "SUBFUNDNAME"
            SubFundName = CStr(TempArray(1)).Trim()

          Case "INSTRUMENT ID", "INSTRUMENTID", "ISIN", "CURRENCY", "WEIGHT"
            BasketStartIndex = Counter
            Exit For

        End Select
      Next

      ' Validation.

      If (BasketStartIndex = 0) OrElse (DragStringArray.Length <= (BasketStartIndex + 1)) Then
        MessageBox.Show("No Basket entries provided.", "Warning", MessageBoxButtons.OK)
        Exit Sub
      End If

      If FundID = 0 Then
        MessageBox.Show("Did not identify FundID, or zero given.", "Warning", MessageBoxButtons.OK)
        Exit Sub
      End If

      If FundID <> LoadedFundID Then
        MessageBox.Show("Fund ID did not match the currently shown fund.", "Warning", MessageBoxButtons.OK)
        Exit Sub
      End If

      If (SubFundID = 0) AndAlso (SubFundName.Length = 0) AndAlso (SubFundDraggedTo <= 0) Then
        MessageBox.Show("No Sub Fund ID or Name provided.", "Warning", MessageBoxButtons.OK)
        Exit Sub
      End If

      If (SubFundID = 0) Then
        SubFundHeirarchySelection = SubFundHeirarchyTable.Select("(TopFundID=" & LoadedFundID.ToString & ") AND (SubFundName='" & SubFundName.Trim() & "')", "TreeOrder")

        If (SubFundHeirarchySelection.Length > 0) Then
          SubFundID = CInt(SubFundHeirarchySelection(0)("SubFundID"))
        End If

        If (SubFundID = 0) Then
          SubFundID = SubFundDraggedTo
        End If

        If (SubFundID = 0) Then
          MessageBox.Show("No Sub Fund matches the name '" & SubFundName & "'.", "Warning", MessageBoxButtons.OK)
          Exit Sub
        End If
      Else
        SubFundHeirarchySelection = SubFundHeirarchyTable.Select("SubFundID=" & SubFundID.ToString, "TreeOrder")
      End If


      ' Parse Basket.

      TempArray = DragStringArray(BasketStartIndex).Split(New Char() {Chr(9)}, StringSplitOptions.None)
      If (TempArray IsNot Nothing) AndAlso (TempArray.Length > 0) Then
        For Counter = 0 To (TempArray.Length - 1)
          Select Case Trim(TempArray(Counter)).ToUpper

            Case "INSTRUMENTID"
              InstrumentIDIndex = Counter

            Case "ISIN"
              ISINIndex = Counter

            Case "CURRENCY"
              CurrencyIndex = Counter

            Case "WEIGHT"
              WeightIndex = Counter

          End Select
        Next
      End If

      If (WeightIndex < 0) Then
        MessageBox.Show("Basket Weight data not identified. Column 'WEIGHT' not found.", "Warning", MessageBoxButtons.OK)
        Exit Sub
      End If

      If (InstrumentIDIndex < 0) AndAlso ((ISINIndex < 0) Or (CurrencyIndex < 0)) Then
        MessageBox.Show("Basket instrument identifiers not found. InstrumentID or (ISIN or Currency).", "Warning", MessageBoxButtons.OK)
      End If

      ' Redim...

      ReDim InstrumentIDs((DragStringArray.Length - BasketStartIndex) - 1)
      ReDim InstrumentWeights((DragStringArray.Length - BasketStartIndex) - 1)

      ' Get Fund Details

      For Counter = (BasketStartIndex + 1) To (DragStringArray.Length - 1)
        TempArray = DragStringArray(Counter).Split(New Char() {Chr(9)}, StringSplitOptions.None)

        If (InstrumentIDIndex >= 0) AndAlso (IsNumeric(TempArray(InstrumentIDIndex))) AndAlso (CInt(TempArray(InstrumentIDIndex)) > 0) Then
          InstrumentIDs(Counter - BasketStartIndex) = CInt(TempArray(InstrumentIDIndex))
          InstrumentWeights(Counter - BasketStartIndex) = ConvertValue(TempArray(WeightIndex), GetType(Double)) ' CDbl(TempArray(WeightIndex))
        ElseIf (ISINIndex >= 0) Then

          If (thisInstrumentTable Is Nothing) Then
            thisInstrumentDS = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblInstrument), RenaissanceDataClass.DSInstrument)

            If (thisInstrumentDS IsNot Nothing) Then
              thisInstrumentTable = thisInstrumentDS.tblInstrument
            Else
              MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "Failed to load tblInstrument.", "", "", True)
              Exit Sub
            End If
          End If

          If (CurrencyIndex < 0) OrElse (TempArray(CurrencyIndex).Length = 0) Then
            SelectedInstrumentRows = thisInstrumentTable.Select("InstrumentISIN='" & TempArray(ISINIndex) & "'", "InstrumentCurrency")
          Else
            SelectedInstrumentRows = thisInstrumentTable.Select("(InstrumentISIN='" & TempArray(ISINIndex) & "') AND (InstrumentCurrency='" & TempArray(CurrencyIndex).ToUpper() & "')", "InstrumentCurrency")
          End If

          If (SelectedInstrumentRows IsNot Nothing) AndAlso (SelectedInstrumentRows.Length > 0) Then
            InstrumentIDs(Counter - BasketStartIndex) = SelectedInstrumentRows(0).InstrumentID
            InstrumentWeights(Counter - BasketStartIndex) = ConvertValue(TempArray(WeightIndex), GetType(Double))
          Else
            MessageBox.Show("ISIN : " & TempArray(ISINIndex) & " Not Found.")
          End If
        End If

      Next

      ' Save Sub Fund ?

      ' Associated Basket ?

      If (SubFundHeirarchySelection IsNot Nothing) AndAlso (SubFundHeirarchySelection.Length > 0) AndAlso (CInt(SubFundHeirarchySelection(0)("ReferenceBasket")) > 0) Then

        Select Case MessageBox.Show("Do you want to save these details to the Genoa Basket?", "Save Basket", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)

          Case Windows.Forms.DialogResult.Cancel

            Exit Sub

          Case Windows.Forms.DialogResult.Yes

            ' Save Basket, Will also send update (which will re-Paint grid).

            Call SaveGenoaBasket(CInt(SubFundHeirarchySelection(0)("ReferenceBasket")), InstrumentIDs, InstrumentWeights)

            Exit Sub

        End Select

      Else

        Select Case MessageBox.Show("Do you want to import these basket details?", "Import Basket", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)

          Case Windows.Forms.DialogResult.Cancel

            Exit Sub

          Case Else

            Select Case MessageBox.Show("Do you want to save these details to a new Genoa Basket?" & vbCrLf & "(This is necessary to get the Force and Ignore check boxes.)", "Save Basket", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)

              Case Windows.Forms.DialogResult.Cancel

                Exit Sub

              Case Windows.Forms.DialogResult.Yes

                ' Create new Basket, Update SubFund with new reference, save basket contents.
                ' Use the Baskets table, if it is loaded, else use a temporary one.

                Dim BasketID As Integer

                Dim BasketDS As DSGroupList = MainForm.Load_Table(RenaissanceStandardDatasets.tblGroupList)
                Dim BasketRow As DSGroupList.tblGroupListRow
                Dim FundRow As DSFund.tblFundRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, FundID)
                Dim SubFundRow As DSSubFund.tblSubFundRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblSubFund, SubFundID)

                If (BasketDS IsNot Nothing) AndAlso (FundRow IsNot Nothing) AndAlso (SubFundRow IsNot Nothing) Then

                  Try

                    BasketRow = BasketDS.tblGroupList.NewtblGroupListRow

                    BasketRow.DefaultConstraintGroup = 0
                    BasketRow.DefaultCovarianceMatrix = 0
                    BasketRow.GroupDateFrom = Renaissance_BaseDate
                    BasketRow.GroupDateTo = Renaissance_EndDate_Data
                    BasketRow.GroupGroup = ""
                    BasketRow.GroupListName = FundRow.FundName & ", " & SubFundRow.SubFundName

                    BasketDS.tblGroupList.Rows.Add(BasketRow)

                    MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblGroupList, BasketDS.tblGroupList)

                    SubFundRow.ReferenceBasket = BasketRow.GroupListID

                    MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblSubFund, New DataRow() {SubFundRow})

                    Dim ThisMessage As New RenaissanceGlobals.RenaissanceUpdateEventArgs

                    ThisMessage.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGroupList) = True
                    ThisMessage.UpdateDetail(RenaissanceGlobals.RenaissanceChangeID.tblGroupList) = BasketRow.GroupListID.ToString
                    ThisMessage.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblSubFund) = True
                    ThisMessage.UpdateDetail(RenaissanceGlobals.RenaissanceChangeID.tblSubFund) = SubFundRow.SubFundID.ToString

                    MainForm.Main_RaiseEvent(ThisMessage)

                    ' Save Basket, Will also send update (which will re-Paint grid).

                    BasketID = BasketRow.GroupListID
                    Call SaveGenoaBasket(BasketID, InstrumentIDs, InstrumentWeights)

                    Exit Sub

                  Catch ex As Exception
                    MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error saving new basket in ProcessDraggedBasket().", ex.Message, ex.StackTrace, True)
                  End Try

                Else
                  MessageBox.Show("Sorry, Failed to load Basket table, get Fund details or get the subFund details.")
                End If

              Case Else


            End Select

        End Select

      End If

      ' just update temporary details.

      Dim SubFundNode As C1.Win.C1FlexGrid.Row = Nothing
      Dim ThisGridRow As C1.Win.C1FlexGrid.Row = Nothing
      Dim ThisInstrument As UInteger
      Dim ChildCounter As Integer
      Dim ChildRange As C1.Win.C1FlexGrid.CellRange = Nothing
      Dim ThisSubFundChanges As Dictionary(Of UInteger, PositionChangesClass)
      Dim thisAdditionalInstruments As Dictionary(Of Integer, Boolean)
      Dim ThisRowChange As PositionChangesClass
      Dim ExistingInstruments As New ArrayList

      If (gridsubFundMap.ContainsKey(SubFundID)) Then
        SubFundNode = gridsubFundMap(SubFundID)
      End If

      If (Grid_Positions_Updates.ContainsKey(SubFundID)) Then
        ThisSubFundChanges = Grid_Positions_Updates(SubFundID)
      Else
        ThisSubFundChanges = New Dictionary(Of UInteger, PositionChangesClass)
        Grid_Positions_Updates.Add(SubFundID, ThisSubFundChanges)
      End If

      ' 1) Zero Existing Entries

      If (SubFundNode IsNot Nothing) Then

        ' Get Children
        ChildRange = SubFundNode.Node.GetCellRange
        ChildRange.Normalize()

        For ChildCounter = ChildRange.TopRow To ChildRange.BottomRow
          ThisGridRow = Grid_Positions.Rows(ChildCounter)

          If (ThisGridRow.IsNode = False) AndAlso (CInt(ThisGridRow(GridColumns.SubFundID)) = SubFundID) Then
            ThisInstrument = CUInt(ThisGridRow(GridColumns.InstrumentID))

            If (CDbl(ThisGridRow(GridColumns.TargetSubFundWeight)) <> 0.0#) Then
              If (ThisSubFundChanges.ContainsKey(ThisInstrument)) Then
                ThisRowChange = ThisSubFundChanges(ThisInstrument)
              Else
                ThisRowChange = New PositionChangesClass
                ThisSubFundChanges(ThisInstrument) = ThisRowChange
              End If

              ThisRowChange.TargetWeight = 0.0#
            End If

            If (Not ExistingInstruments.Contains(CInt(ThisGridRow(GridColumns.InstrumentID)))) Then
              ExistingInstruments.Add(CInt(ThisGridRow(GridColumns.InstrumentID)))
            End If
          End If
        Next

      End If

      ' 2) Set Temp Instruments
      ' 3) Set Temp Weights

      For Counter = 0 To (InstrumentIDs.Length - 1)
        If (InstrumentIDs(Counter) > 0) Then
          ThisInstrument = CUInt(InstrumentIDs(Counter))

          ' Additional Instrument

          If (Not ExistingInstruments.Contains(InstrumentIDs(Counter))) Then
            If (Grid_Positions_AdditionalInstruments.ContainsKey(SubFundID)) Then
              thisAdditionalInstruments = Grid_Positions_AdditionalInstruments(SubFundID)
            Else
              thisAdditionalInstruments = New Dictionary(Of Integer, Boolean)
              Grid_Positions_AdditionalInstruments.Add(SubFundID, thisAdditionalInstruments)
            End If

            If (Not thisAdditionalInstruments.ContainsKey(CInt(ThisInstrument))) Then
              thisAdditionalInstruments.Add(CInt(ThisInstrument), True)
            End If
          End If

          ' Target Weight

          If (ThisSubFundChanges.ContainsKey(ThisInstrument)) Then
            ThisRowChange = ThisSubFundChanges(ThisInstrument)
          Else
            ThisRowChange = New PositionChangesClass
            ThisSubFundChanges(ThisInstrument) = ThisRowChange
          End If

          ThisRowChange.TargetWeight = InstrumentWeights(Counter)

        End If
      Next

      PaintPositionsGrid()

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error in ProcessDraggedBasket().", ex.Message, ex.StackTrace, True)
    End Try
  End Sub

  ''' <summary>
  ''' Saves the genoa basket.
  ''' </summary>
  ''' <param name="BasketID">The basket ID.</param>
  ''' <param name="InstrumentIDs">The instrument I ds.</param>
  ''' <param name="InstrumentWeights">The instrument weights.</param>
  ''' <returns>System.String.</returns>
  Private Function SaveGenoaBasket(ByVal BasketID As Integer, ByVal InstrumentIDs() As Integer, ByVal InstrumentWeights() As Double) As String
    ' *******************************************************************************
    ' Given a GroupID (BasketID), update the group members and member weights using the given
    ' Instrument and Weight Arrays.
    ' 
    ' At this time, InstrumentIDs() are assumed to be Venice Instruments.
    ' Existing Non-Venice Instrument members are ignored.
    ' *******************************************************************************

    Dim UpdateDetail As String = ""

    Try

      Dim Permissions As Integer = MainForm.CheckPermissions(Me.Name, PermissionFeatureType.TypeForm)

      If ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) = 0) AndAlso ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) = 0) Then
        MessageBox.Show("You do not have Permissions to save Group data.")
        Return UpdateDetail
      End If
    Catch ex As Exception
    End Try

    Try
      Dim GroupItemsDS As RenaissanceDataClass.DSGroupItems
      Dim GroupItemsTable As RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable = Nothing
      Dim SelectedItems() As RenaissanceDataClass.DSGroupItems.tblGroupItemsRow
      Dim thisItem As RenaissanceDataClass.DSGroupItems.tblGroupItemsRow

      Dim GroupDataDS As RenaissanceDataClass.DSGroupItemData
      Dim GroupDataTable As RenaissanceDataClass.DSGroupItemData.tblGroupItemDataDataTable = Nothing
      Dim SelectedData() As RenaissanceDataClass.DSGroupItemData.tblGroupItemDataRow
      Dim thisData As RenaissanceDataClass.DSGroupItemData.tblGroupItemDataRow

      Dim ItemCount As Integer
      Dim thisIndex As Integer
      Dim ChangesMade As Boolean = False

      Dim ProcessesInstruments As New ArrayList

      ' Get Data tables...

      GroupItemsDS = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblGroupItems), RenaissanceDataClass.DSGroupItems)
      If (GroupItemsDS IsNot Nothing) Then GroupItemsTable = GroupItemsDS.tblGroupItems

      GroupDataDS = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblGroupItemData), RenaissanceDataClass.DSGroupItemData)
      If (GroupDataDS IsNot Nothing) Then GroupDataTable = GroupDataDS.tblGroupItemData

      ' Exit if either table is unavailable.

      If (GroupItemsTable Is Nothing) OrElse (GroupDataTable Is Nothing) Then
        Return UpdateDetail
      End If

      ' 


      SelectedItems = GroupItemsTable.Select("GroupID=" & BasketID.ToString("###0"), "GroupItemID")

      ' Get Group Total Weight.

      Dim TotalGroupWeight As Double = 0.0#

      For ItemCount = 0 To (SelectedItems.Length - 1)
        thisItem = SelectedItems(ItemCount)

        SelectedData = GroupDataTable.Select("GroupItemID=" & thisItem.GroupItemID.ToString)

        If (SelectedData IsNot Nothing) AndAlso (SelectedData.Length > 0) Then
          TotalGroupWeight += SelectedData(0).GroupHolding
        End If
      Next

      If (TotalGroupWeight = 0.0#) Then
        TotalGroupWeight = 100.0#
      End If

      ' Update

      SyncLock GroupItemsTable

        ' Delete any rows that are no longer in the Group
        ' Note, at present, only deleting Venice instruments.
        ' Update existing instrument weights.

        If (SelectedItems IsNot Nothing) AndAlso (SelectedItems.Length > 0) Then
          For ItemCount = 0 To (SelectedItems.Length - 1)
            thisItem = SelectedItems(ItemCount)

            Select Case PertracDataClass.GetFlagsFromID(thisItem.GroupPertracCode)

              Case PertracDataClass.PertracInstrumentFlags.VeniceInstrument

                If (InstrumentIDs.Contains(CInt(PertracDataClass.GetInstrumentFromID(thisItem.GroupPertracCode))) = False) Then

                  UpdateDetail &= thisItem.GroupItemID.ToString & ","
                  SelectedData = GroupDataTable.Select("GroupItemID=" & thisItem.GroupItemID.ToString)
                  ChangesMade = True

                  ' Delete Item

                  thisItem.Delete()

                  ' Delete Data

                  If (SelectedData IsNot Nothing) AndAlso (SelectedData.Length > 0) Then
                    SelectedData(0).Delete()
                  End If

                Else
                  ' Update Data

                  thisIndex = Array.IndexOf(InstrumentIDs, CInt(PertracDataClass.GetInstrumentFromID(thisItem.GroupPertracCode)))

                  SelectedData = GroupDataTable.Select("GroupItemID=" & thisItem.GroupItemID.ToString)

                  If (SelectedData IsNot Nothing) AndAlso (SelectedData.Length > 0) Then
                    SelectedData(0).GroupHolding = TotalGroupWeight * InstrumentWeights(thisIndex)
                    SelectedData(0).GroupPercent = InstrumentWeights(thisIndex)

                    UpdateDetail &= SelectedData(0).GroupItemID.ToString & ","

                  End If

                  ' record processed InstrumentID so it is not added later
                  ProcessesInstruments.Add(InstrumentIDs(thisIndex))

                  ChangesMade = True

                End If

            End Select

          Next
        End If

        ' Update So Far

        If (ChangesMade) Then
          MainForm.AdaptorUpdate(Me.Name, RenaissanceGlobals.RenaissanceStandardDatasets.tblGroupItems, GroupItemsTable)
          MainForm.AdaptorUpdate(Me.Name, RenaissanceGlobals.RenaissanceStandardDatasets.tblGroupItemData, GroupDataTable)
        End If


        ' Add New Instruments / Weights

        For thisIndex = 0 To (InstrumentIDs.Length - 1)
          If (InstrumentIDs(thisIndex) > 0) AndAlso (Not ProcessesInstruments.Contains(InstrumentIDs(thisIndex))) Then

            ChangesMade = True

            ' Add new Basket member.

            thisItem = GroupItemsTable.NewtblGroupItemsRow

            thisItem.GroupID = BasketID
            thisItem.GroupItemID = 0
            thisItem.GroupPertracCode = CInt(PertracDataClass.SetFlagsToID(CUInt(InstrumentIDs(thisIndex)), PertracDataClass.PertracInstrumentFlags.VeniceInstrument))
            thisItem.GroupIndexCode = 0
            thisItem.GroupSector = ""

            GroupItemsTable.Rows.Add(thisItem)

            MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblGroupItems, New DataRow() {thisItem})

            UpdateDetail &= thisItem.GroupItemID.ToString("###0") & ","

            ' Add Data (Weight) for the new member. 

            thisData = GroupDataTable.NewtblGroupItemDataRow

            thisData.GroupItemID = thisItem.GroupItemID
            thisData.GroupPertracCode = thisItem.GroupPertracCode
            thisData.GroupLiquidity = 0
            thisData.GroupHolding = TotalGroupWeight * InstrumentWeights(thisIndex)
            thisData.GroupNewHolding = thisData.GroupHolding
            thisData.GroupPercent = InstrumentWeights(thisIndex)
            thisData.GroupExpectedReturn = 0.0#
            thisData.GroupUpperBound = 0.0#
            thisData.GroupLowerBound = 0.0#
            thisData.GroupFloor = 0.0#
            thisData.GroupCap = 0.0#
            thisData.GroupTradeSize = 0.0#
            thisData.GroupBeta = 0.0#
            thisData.GroupAlpha = 0.0#
            thisData.GroupIndexE = 0.0#
            thisData.GroupBetaE = 0.0#
            thisData.GroupAlphaE = 0.0#
            thisData.GroupStdErr = 0.0#
            thisData.GroupScalingFactor = 1.0#

            GroupDataTable.Rows.Add(thisData)

            MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblGroupItemData, New DataRow() {thisData})

          End If
        Next

      End SyncLock


      If (ChangesMade) Then

        ' Send Update.

        Dim ThisMessage As New RenaissanceGlobals.RenaissanceUpdateEventArgs

        ThisMessage.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGroupItems) = True
        ThisMessage.UpdateDetail(RenaissanceGlobals.RenaissanceChangeID.tblGroupItems) = UpdateDetail
        ThisMessage.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGroupItemData) = True
        ThisMessage.UpdateDetail(RenaissanceGlobals.RenaissanceChangeID.tblGroupItemData) = UpdateDetail

        MainForm.Main_RaiseEvent(ThisMessage)

      End If


    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error in SaveGenoaBasket().", ex.Message, ex.StackTrace, True)
    End Try

    Return UpdateDetail

  End Function



  ''' <summary>
  ''' Handles the Click event of the Button_Transfer control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Button_Transfer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Transfer.Click
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Try
      Dim ThisTransfer As New PendingTransfer
      Dim existingTransfer As PendingTransfer

      Dim thisInstrumentID As Integer
      Dim InstrumentRow As DSInstrument.tblInstrumentRow
      Dim FundRow As DSFund.tblFundRow
      Dim PercentageValue As Double = edit_TransferQuantity.Value
      Dim FundID As Integer
      Dim FromSubFundID As Integer
      Dim ToSubFundID As Integer
      Dim InstrumentPrice As Double
      Dim TransactionPrice As Double
      Dim SubFundNode As C1.Win.C1FlexGrid.Row

      ' Check. Should not happen !

      If (SubFundHeirarchyTable Is Nothing) Then
        Call getSubFundHeirarchy()
      End If

      ' Fund ID :

      If (IsNumeric(Combo_TransactionFund.SelectedValue)) Then
        FundID = CInt(Combo_TransactionFund.SelectedValue)
      Else
        Exit Sub
      End If

      ' Sub Fund ID :

      If (IsNumeric(Combo_FromSubFund.SelectedValue)) Then
        FromSubFundID = CInt(Combo_FromSubFund.SelectedValue)
      Else
        Exit Sub
      End If
      ' Fund ID :

      If (IsNumeric(Combo_ToSubFund.SelectedValue)) Then
        ToSubFundID = CInt(Combo_ToSubFund.SelectedValue)
      Else
        Exit Sub
      End If

      ' Instrument ID :

      If (IsNumeric(Combo_Instrument.SelectedValue)) Then
        thisInstrumentID = CInt(Combo_Instrument.SelectedValue)
      Else
        Exit Sub
      End If

      ' Fund and Instrument Details :

      FundRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, FundID), DSFund.tblFundRow)
      InstrumentRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, thisInstrumentID), DSInstrument.tblInstrumentRow)

      If (InstrumentRow Is Nothing) Then
        Exit Sub
      End If

      ' Instrument Price :

      If (InstrumentRow.InstrumentType = InstrumentTypes.Cash) Then
        InstrumentPrice = 1.0#
        TransactionPrice = 0.0#
      Else
        InstrumentPrice = GetInstrumentPrice(thisInstrumentID)
        TransactionPrice = InstrumentPrice
        If (InstrumentPrice = 0.0#) Then
          Exit Sub
        End If

        If (Check_TransferAtZeroValue.Checked) Then
          TransactionPrice = 0.0#
        End If
      End If

      '

      With ThisTransfer
        .FromFundID = FundID
        .ToFundID = FundID
        .FromSubFundID = FromSubFundID
        .ToSubFundID = ToSubFundID
        .InstrumentID = thisInstrumentID
        .Quantity = edit_TransferQuantity.Value
        .TradePrice = TransactionPrice
        .ValuationPrice = InstrumentPrice
        .Settlement = (edit_TransferQuantity.Value * InstrumentPrice) * InstrumentRow.InstrumentContractSize * InstrumentRow.InstrumentMultiplier
        .Currency = InstrumentRow.InstrumentCurrency
        .IsNet = False
      End With

      ' Net with existing transfer ?

      For Each existingTransfer In Grid_Transfer_Trades
        If _
        (existingTransfer.FromFundID = ThisTransfer.FromFundID) AndAlso _
        (existingTransfer.ToFundID = ThisTransfer.ToFundID) AndAlso _
        (existingTransfer.FromSubFundID = ThisTransfer.FromSubFundID) AndAlso _
        (existingTransfer.ToSubFundID = ThisTransfer.ToSubFundID) AndAlso _
        (existingTransfer.InstrumentID = ThisTransfer.InstrumentID) AndAlso _
        (existingTransfer.ValuationPrice = ThisTransfer.ValuationPrice) AndAlso _
        (existingTransfer.TradePrice = ThisTransfer.TradePrice) Then

          existingTransfer.Quantity += ThisTransfer.Quantity
          existingTransfer.Settlement += ThisTransfer.Settlement

          ThisTransfer = Nothing
          Exit For
        End If

        If _
        (existingTransfer.FromFundID = ThisTransfer.ToFundID) AndAlso _
        (existingTransfer.ToFundID = ThisTransfer.FromFundID) AndAlso _
        (existingTransfer.FromSubFundID = ThisTransfer.ToSubFundID) AndAlso _
        (existingTransfer.ToSubFundID = ThisTransfer.FromSubFundID) AndAlso _
        (existingTransfer.InstrumentID = ThisTransfer.InstrumentID) AndAlso _
        (existingTransfer.ValuationPrice = ThisTransfer.ValuationPrice) AndAlso _
        (existingTransfer.TradePrice = ThisTransfer.TradePrice) Then

          existingTransfer.Quantity -= ThisTransfer.Quantity
          existingTransfer.Settlement -= ThisTransfer.Settlement

          ThisTransfer = Nothing
          Exit For
        End If

      Next

      If (ThisTransfer IsNot Nothing) Then
        Grid_Transfer_Trades.Add(ThisTransfer)
      End If

      ' Clear Transfer details

      Try
        edit_TransferQuantity.Value = 0.0#
        Combo_Instrument.SelectedIndex = 0
        Combo_FromSubFund.SelectedIndex = 0
        Combo_ToSubFund.SelectedIndex = 0
      Catch ex As Exception
      End Try

      '

      SubFundNode = gridSubFundMap(ToSubFundID)

      If (SubFundNode IsNot Nothing) Then
        SubFundNode.Node.Expanded = True
      End If

      SubFundNode = gridSubFundMap(FromSubFundID)

      If (SubFundNode IsNot Nothing) Then
        SubFundNode.Node.Expanded = True
      End If

      '

      PaintPositionsGrid()

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error in Button_Transfer_Click().", ex.Message, ex.StackTrace, True)
    End Try

  End Sub

  ''' <summary>
  ''' Handles the Click event of the Button_Transfer_Submit control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Button_Transfer_Submit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Transfer_Submit.Click
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Dim DResult As DialogResult
    Dim Selection As C1.Win.C1FlexGrid.RowCollection
    Dim thisRow As C1.Win.C1FlexGrid.Row
    Dim ThisTransfer As PendingTransfer
    Dim ThisTransferID As Integer
    Dim TransferCounter As Integer

    Try
      If (HasInsertPermission = False) Then
        MessageBox.Show("You do not have permissions to insert Transactions.", "Permission Denied", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Exit Sub
      End If
    Catch ex As Exception
    End Try

    Try
      If Radio_Transfer_All.Checked Then
        If Grid_PendingTransfers.Rows.Count > 1 Then
          DResult = MessageBox.Show("Commit ALL of the pending Internal Transfer trades ?", "Commit ALL Transfers", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2)
          Selection = Grid_PendingTransfers.Rows
        Else
          Exit Sub
        End If
      Else
        Selection = Grid_PendingTransfers.Rows.Selected

        If (Selection.Count > 2) OrElse ((Selection.Count = 1) AndAlso (Selection(0).SafeIndex > 0)) Then
          DResult = MessageBox.Show("Commit the " & Selection.Count.ToString & " selected Internal Transfer trade" & IIf(Selection.Count.ToString > 1, "s", "") & " ?", "Commit ALL Transfers", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2)
        Else
          DResult = MessageBox.Show("There are no Transfer trades selected.", "Commit ALL Transfers", MessageBoxButtons.OK, MessageBoxIcon.Information)
          Exit Sub
        End If
      End If

      If (DResult = Windows.Forms.DialogResult.OK) AndAlso (Selection IsNot Nothing) AndAlso (Selection.Count > 0) Then
        Dim UpdateString As String = ""
        Dim PostedUpdate As Boolean = False

        For Each thisRow In Selection
          If (thisRow.SafeIndex > 0) Then
            ThisTransferID = CInt(thisRow("TransferID"))

            For TransferCounter = 0 To (Grid_Transfer_Trades.Count - 1)
              ThisTransfer = Grid_Transfer_Trades(TransferCounter)

              If (ThisTransfer.TransferID = ThisTransferID) Then

                PostedUpdate = True
                UpdateString &= PostTransferTrade(ThisTransfer)

                ' Just in case.
                ThisTransfer.Quantity = 0.0#

                ' Clear 
                Grid_Transfer_Trades.RemoveAt(TransferCounter)
                Exit For
              End If
            Next
          End If
        Next

        If (PostedUpdate) Then
          MainForm.Main_RaiseEvent(New RenaissanceUpdateEventArgs(RenaissanceChangeID.tblTransaction, UpdateString))
        End If

        ' Not necessary, should be done by AutoUpdate()
        ' PaintTransactionGrid()
      End If

    Catch ex As Exception

    End Try
  End Sub

  ''' <summary>
  ''' Handles the Click event of the Button_Transfer_Cancel control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Button_Transfer_Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Transfer_Cancel.Click
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Dim DResult As DialogResult
    Dim Selection As C1.Win.C1FlexGrid.RowCollection
    Dim thisRow As C1.Win.C1FlexGrid.Row
    Dim ThisTransfer As PendingTransfer
    Dim ThisTransferID As Integer
    Dim TransferCounter As Integer

    Try
      If Radio_Transfer_All.Checked Then
        If Grid_PendingTransfers.Rows.Count > 1 Then
          DResult = MessageBox.Show("Cancel ALL of the pending Internal Transfer trades ?", "Cancel ALL Transfers", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2)
          Selection = Grid_PendingTransfers.Rows
        Else
          Exit Sub
        End If
      Else
        Selection = Grid_PendingTransfers.Rows.Selected

        If (Selection.Count > 2) OrElse ((Selection.Count = 1) AndAlso (Selection(0).SafeIndex > 0)) Then
          DResult = MessageBox.Show("Cancel the " & Selection.Count.ToString & " selected Internal Transfer trade" & IIf(Selection.Count.ToString > 1, "s", "") & " ?", "Cancel ALL Transfers", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2)
        Else
          Exit Sub
        End If
      End If

      If (DResult = Windows.Forms.DialogResult.OK) AndAlso (Selection IsNot Nothing) AndAlso (Selection.Count > 0) Then

        For Each thisRow In Selection
          If (thisRow.SafeIndex > 0) Then
            ThisTransferID = CInt(thisRow("TransferID"))

            For TransferCounter = 0 To (Grid_Transfer_Trades.Count - 1)
              ThisTransfer = Grid_Transfer_Trades(TransferCounter)

              If (ThisTransfer.TransferID = ThisTransferID) Then

                ' Just in case.
                ThisTransfer.Quantity = 0.0#

                Grid_Transfer_Trades.RemoveAt(TransferCounter)
                Exit For
              End If
            Next
          End If
        Next

        PaintPositionsGrid()
      End If

    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Handles the Click event of the Button_Transfer_SelectAll control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Button_Transfer_SelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Transfer_SelectAll.Click
    ' *******************************************************************************
    '
    ' *******************************************************************************
    Try
      Grid_PendingTransfers.Select(1, 0, Grid_PendingTransfers.Rows.Count - 1, Grid_PendingTransfers.Cols.Count - 1)
    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Handles the Click event of the Button_Transfer_SelectNone control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Button_Transfer_SelectNone_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Transfer_SelectNone.Click
    ' *******************************************************************************
    '
    ' *******************************************************************************
    Try
      Grid_PendingTransfers.Select(0, 0)
    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Handles the MouseEnterCell event of the Grid_PendingTransfers control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="C1.Win.C1FlexGrid.RowColEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_PendingTransfers_MouseEnterCell(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles Grid_PendingTransfers.MouseEnterCell
    ' *******************************************************************************
    '
    ' *******************************************************************************
    Try
      LastEnteredTransfersGridRow = e.Row
      LastEnteredTransfersGridCol = e.Col

      If (e.Row <= 0) Then
        Grid_PendingTransfers.ContextMenuStrip = Nothing
      Else
        Grid_PendingTransfers.ContextMenuStrip = ContextMenu_Transfers
      End If

    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Handles the VisibleChanged event of the ContextMenu_Transfers control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub ContextMenu_Transfers_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ContextMenu_Transfers.VisibleChanged
    ' *******************************************************************************
    '
    ' *******************************************************************************
    Try
      If (ContextMenu_Transfers.Visible = True) Then
        Grid_PendingTransfers.Select(LastEnteredTransfersGridRow, 0, LastEnteredTransfersGridRow, Grid_PendingTransfers.Cols.Count - 1)
      End If
    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Handles the Click event of the Menu_EnterThisTransfer control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Menu_EnterThisTransfer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_EnterThisTransfer.Click
    ' *******************************************************************************
    '
    ' *******************************************************************************
    Try
      'LastEnteredTransfersGridRow
      'LastEnteredTransfersGridCol
      Dim RadioVal As Boolean = Radio_Transfer_All.Checked

      Grid_PendingTransfers.Select(LastEnteredTransfersGridRow, 0, LastEnteredTransfersGridRow, Grid_PendingTransfers.Cols.Count - 1)
      Radio_Transfer_Selected.Checked = True
      Call Button_Transfer_Submit_Click(Button_Transfer, Nothing)

      If (RadioVal) Then
        Radio_Transfer_All.Checked = True
      End If

    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Handles the Click event of the Menu_EnterAllTransfers control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Menu_EnterAllTransfers_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_EnterAllTransfers.Click
    ' *******************************************************************************
    '
    ' *******************************************************************************
    Try
      Dim RadioVal As Boolean = Radio_Transfer_Selected.Checked

      'LastEnteredTransfersGridRow
      'LastEnteredTransfersGridCol
      Call Button_Transfer_SelectAll_Click(Button_Transfer_SelectAll, Nothing)
      Radio_Transfer_All.Checked = True
      Call Button_Transfer_Submit_Click(Button_Transfer, Nothing)

      If (RadioVal) Then
        Radio_Transfer_Selected.Checked = True
      End If

    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Handles the Click event of the Menu_RemoveThisTransfer control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Menu_RemoveThisTransfer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_RemoveThisTransfer.Click
    ' *******************************************************************************
    '
    ' *******************************************************************************
    Try
      'LastEnteredTransfersGridRow
      'LastEnteredTransfersGridCol
      Dim RadioVal As Boolean = Radio_Transfer_All.Checked

      Grid_PendingTransfers.Select(LastEnteredTransfersGridRow, 0, LastEnteredTransfersGridRow, Grid_PendingTransfers.Cols.Count - 1)
      Radio_Transfer_Selected.Checked = True
      Call Button_Transfer_Cancel_Click(Button_Transfer, Nothing)

      If (RadioVal) Then
        Radio_Transfer_All.Checked = True
      End If

    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Handles the Click event of the Menu_RemoveALLTransfers control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Menu_RemoveALLTransfers_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_RemoveALLTransfers.Click
    ' *******************************************************************************
    '
    ' *******************************************************************************
    Try
      Dim RadioVal As Boolean = Radio_Transfer_Selected.Checked

      Call Button_Transfer_SelectAll_Click(Button_Transfer_SelectAll, Nothing)
      Radio_Transfer_All.Checked = True
      Call Button_Transfer_Cancel_Click(Button_Transfer, Nothing)

      If (RadioVal) Then
        Radio_Transfer_Selected.Checked = True
      End If
    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Handles the MouseEnterCell event of the Grid_Positions control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="C1.Win.C1FlexGrid.RowColEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Positions_MouseEnterCell(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles Grid_Positions.MouseEnterCell
    ' *******************************************************************************
    '
    ' *******************************************************************************
    Try
      LastEnteredPositionsGridRow = e.Row
      LastEnteredPositionsGridCol = e.Col

      If (e.Row <= 0) Then
        Grid_Positions.ContextMenuStrip = Nothing
      Else
        Grid_Positions.ContextMenuStrip = ContextMenu_Positions
      End If

    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Handles the Click event of the Menu_ViewTransactions control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Menu_ViewTransactions_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_ViewTransactions.Click
    Try

      If (LastEnteredPositionsGridRow > 0) And (LastEnteredPositionsGridRow < Grid_Positions.Rows.Count) Then
        Dim ThisFundID As Integer = 0
        Dim ThisInstrument As Integer = 0
        Dim InstrumentType As InstrumentTypes
        Dim thisFormHandle As RenaissanceGlobals.RenaissanceFormHandle

        ThisFundID = CInt(Grid_Positions.GetData(LastEnteredPositionsGridRow, GridColumns.FundID))

        If IsNumeric(Grid_Positions.GetData(LastEnteredPositionsGridRow, GridColumns.InstrumentID)) Then
          ThisInstrument = CInt(Grid_Positions.GetData(LastEnteredPositionsGridRow, GridColumns.InstrumentID))
        Else
          Exit Sub
        End If

        InstrumentType = CType(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, ThisInstrument, "InstrumentType"), InstrumentTypes)

        Select Case InstrumentType

          Case InstrumentTypes.Future, InstrumentTypes.Option

            thisFormHandle = MainForm.New_VeniceForm(VeniceFormID.frmUpdateTransactions)
            CType(thisFormHandle.Form, frmUpdateTransactions).SetFormSelectCriteria(ThisFundID, ThisInstrument)

          Case Else

            thisFormHandle = MainForm.New_VeniceForm(VeniceFormID.frmSelectTransaction)
            CType(thisFormHandle.Form, frmSelectTransaction).SetFormSelectCriteria(ThisFundID, ThisInstrument, Renaissance_BaseDate)

        End Select

      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error viewing transaction details.", ex.Message, ex.StackTrace, True)
    End Try
  End Sub

  ''' <summary>
  ''' Handles the Click event of the Button_Trades_SelectAll control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Button_Trades_SelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Trades_SelectAll.Click
    ' *******************************************************************************
    '
    ' *******************************************************************************
    Try
      Grid_PendingTransactions.Select(1, 0, Grid_PendingTransactions.Rows.Count - 1, Grid_PendingTransactions.Cols.Count - 1)
    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Handles the Click event of the Button_Trades_Selectnone control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Button_Trades_Selectnone_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Trades_Selectnone.Click
    ' *******************************************************************************
    '
    ' *******************************************************************************
    Try
      Grid_PendingTransactions.Select(0, 0)
    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Handles the Click event of the Button_Trades_Submit control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Button_Trades_Submit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Trades_Submit.Click
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Dim DResult As DialogResult
    Dim Selection As C1.Win.C1FlexGrid.RowCollection
    Dim thisRow As C1.Win.C1FlexGrid.Row

    Try
      If (HasInsertPermission = False) Then
        MessageBox.Show("You do not have permissions to insert Transactions.", "Permission Denied", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Exit Sub
      End If
    Catch ex As Exception
    End Try

    Try

      If Radio_Transactions_All.Checked Then
        If Grid_PendingTransactions.Rows.Count > 1 Then
          DResult = MessageBox.Show("Commit ALL of the pending Transaction trades ?", "Commit ALL Transactions", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2)
          Selection = Grid_PendingTransactions.Rows
        Else
          Exit Sub
        End If
      Else
        Selection = Grid_PendingTransactions.Rows.Selected

        If (Selection.Count > 2) OrElse ((Selection.Count = 1) AndAlso (Selection(0).SafeIndex > 0)) Then
          DResult = MessageBox.Show("Commit the " & Selection.Count.ToString & " selected Transaction trade" & IIf(Selection.Count.ToString > 1, "s", "") & " ?", "Commit Selected Transactions", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2)
        Else
          DResult = MessageBox.Show("There are no Transaction trades selected.", "Commit ALL Transactions", MessageBoxButtons.OK, MessageBoxIcon.Information)
          Exit Sub
        End If
      End If

      If (DResult = Windows.Forms.DialogResult.OK) AndAlso (Selection IsNot Nothing) AndAlso (Selection.Count > 0) Then

        Dim TransactionDS As DSTransaction
        Dim TransactionTable As DSTransaction.tblTransactionDataTable
        Dim newTransaction As DSTransaction.tblTransactionRow
        Dim TransactionTableIsLoaded As Boolean = True
        Dim InstrumentRow As DSInstrument.tblInstrumentRow
        Dim thisFundRow As RenaissanceDataClass.DSFund.tblFundRow = Nothing

        Dim ThisTransactionChanges As Dictionary(Of UInteger, TransactionChangesClass)
        Dim ThisTransactionRowChange As TransactionChangesClass = Nothing
        Dim thisFundID As Integer
        Dim thisSubFundID As Integer
        Dim thisInstrumentID As Integer
        Dim thisTransactionType As Integer
        Dim thisUnits As Double
        Dim thisPrice As Double
        Dim thisValueOrAmount As String
        Dim thisSettlement As Double
        Dim thisAutoFX As Boolean
        Dim FXToFund As Double

        ' Use the Transactions table, if it is loaded, else use a temporary one.

        TransactionDS = MainForm.MainDataHandler.Get_Dataset(RenaissanceStandardDatasets.GetStandardDataset(RenaissanceChangeID.tblTransaction).DatasetName)
        If (TransactionDS Is Nothing) Then
          TransactionDS = New DSTransaction
          TransactionTableIsLoaded = False
        End If
        TransactionTable = TransactionDS.tblTransaction

        Dim UpdateString As String = ""
        Dim PostedUpdate As Boolean = False

        For Each thisRow In Selection
          If (thisRow.SafeIndex > 0) Then

            ' OK, Onwards.....

            thisFundID = CInt(thisRow("FundID"))
            thisSubFundID = CInt(thisRow("SubFundID"))
            thisInstrumentID = CInt(thisRow("InstrumentID"))
            thisUnits = CDbl(thisRow("Amount"))
            thisPrice = CDbl(thisRow("Price"))
            thisValueOrAmount = CStr(thisRow("ValueOrAmount")) ' NewTransactionRow("ValueOrAmount") = "Value"
            thisSettlement = CDbl(thisRow("Settlement"))
            thisAutoFX = CBool(thisRow("AutoFX"))

            thisFundRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, thisFundID)
            InstrumentRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, thisInstrumentID), DSInstrument.tblInstrumentRow)

            FXToFund = GetFXToFund(InstrumentRow.InstrumentCurrencyID, thisFundRow.FundBaseCurrency)

            If (InstrumentRow.InstrumentType = InstrumentTypes.Cash) Then
              If (thisUnits < 0.0#) Then
                thisTransactionType = TransactionTypes.SellFX
              Else
                thisTransactionType = TransactionTypes.BuyFX
              End If
            Else
              If (thisUnits < 0.0#) Then
                thisTransactionType = TransactionTypes.Sell
              Else
                thisTransactionType = TransactionTypes.Buy
              End If
            End If

            newTransaction = TransactionTable.NewtblTransactionRow

            newTransaction.TransactionInstrument = thisInstrumentID
            newTransaction.TransactionVersusInstrument = 0
            newTransaction.TransactionUnits = Math.Abs(thisUnits)
            newTransaction.TransactionPrice = thisPrice

            newTransaction.TransactionFund = thisFundID
            newTransaction.TransactionSubFund = thisSubFundID
            newTransaction.TransactionType = thisTransactionType

            newTransaction.TransactionTradeStatusID = CInt(thisRow("TradeStatusID"))
            newTransaction.TransactionCounterparty = 1 ' Market

            newTransaction.TransactionGroup = ""
            newTransaction.TransactionInvestment = ""
            newTransaction.TransactionExecution = ""
            newTransaction.TransactionDataEntry = ""
            newTransaction.TransactionDecisionDate = Now.Date

            If (InstrumentRow.InstrumentDealingCutOffTime > 0) AndAlso (InstrumentRow.InstrumentDealingCutOffTime < CInt(Now.TimeOfDay.TotalSeconds)) Then
              ' After Cutoff
              newTransaction.TransactionValueDate = RenaissanceUtilities.DatePeriodFunctions.AddPeriodToDate(DealingPeriod.Daily, Now.Date, 1)
            Else
              newTransaction.TransactionValueDate = Now.Date
            End If

            If (newTransaction.TransactionType = TransactionTypes.SellFX) OrElse (newTransaction.TransactionType = TransactionTypes.Sell) Then
              newTransaction.TransactionSettlementDate = RenaissanceUtilities.DatePeriodFunctions.AddPeriodToDate(InstrumentRow.InstrumentDealingPeriod, newTransaction.TransactionValueDate, InstrumentRow.InstrumentDefaultSettlementDays_Sell)
            Else
              newTransaction.TransactionSettlementDate = RenaissanceUtilities.DatePeriodFunctions.AddPeriodToDate(InstrumentRow.InstrumentDealingPeriod, newTransaction.TransactionValueDate, InstrumentRow.InstrumentDefaultSettlementDays_Buy)
            End If

            newTransaction.TransactionConfirmationDate = newTransaction.TransactionSettlementDate

            ' For Value orders, set the amount precisely, it should be rounded in the 'Save Order File' process if necessary,
            ' But for now, we need to preserve the Value precisely.
            newTransaction.TransactionSettlementCurrencyID = 0
            newTransaction.TransactionFXRate = 1.0#

            If (thisValueOrAmount.StartsWith("V")) Then
              newTransaction.TransactionValueorAmount = "Value"

              If (thisAutoFX) Then
                newTransaction.TransactionUnits = Math.Abs(thisSettlement / (thisPrice * InstrumentRow.InstrumentContractSize * InstrumentRow.InstrumentMultiplier * FXToFund))
                newTransaction.TransactionSettlement = thisSettlement * FXToFund
                newTransaction.TransactionSettlementCurrencyID = thisFundRow.FundBaseCurrency
                newTransaction.TransactionFXRate = FXToFund

              Else
                newTransaction.TransactionUnits = Math.Abs(thisSettlement / (thisPrice * InstrumentRow.InstrumentContractSize * InstrumentRow.InstrumentMultiplier))
                newTransaction.TransactionSettlement = thisSettlement
              End If
            Else
              newTransaction.TransactionValueorAmount = "Amount"

              If (thisAutoFX) Then
                newTransaction.TransactionSettlementCurrencyID = thisFundRow.FundBaseCurrency
                newTransaction.TransactionFXRate = FXToFund
              End If
            End If

            newTransaction.TransactionCosts = 0.0#
            newTransaction.TransactionCostPercent = 0.0#

            ' Transfer Details.

            newTransaction.TransactionIsTransfer = False
            newTransaction.TransactionEffectivePrice = newTransaction.TransactionPrice
            newTransaction.TransactionEffectiveValueDate = newTransaction.TransactionValueDate
            newTransaction.TransactionSpecificInitialEqualisationFlag = False
            newTransaction.TransactionSpecificInitialEqualisationValue = 0
            newTransaction.TransactionEffectiveWatermark = 0

            newTransaction.TransactionCostIsPercent = False
            newTransaction.TransactionExemptFromUpdate = True
            newTransaction.TransactionFinalAmount = (-1)
            newTransaction.TransactionFinalPrice = (-1)
            newTransaction.TransactionFinalCosts = (-1)
            newTransaction.TransactionFinalSettlement = (-1)
            newTransaction.TransactionInstructionFlag = (-1)
            newTransaction.TransactionRedeemWholeHoldingFlag = False
            newTransaction.TransactionBankConfirmation = 0
            newTransaction.TransactionInitialDataEntry = 0
            newTransaction.TransactionAmountConfirmed = 0
            newTransaction.TransactionSettlementConfirmed = 0
            newTransaction.TransactionFinalDataEntry = 0
            newTransaction.TransactionRedemptionID = 0
            newTransaction.TransactionComment = "Sub-Fund Transfer."
            newTransaction.TransactionCTFLAGS = 0
            newTransaction.TransactionCompoundRN = 0

            If (Math.Abs(newTransaction.TransactionUnits) >= 0.00001) AndAlso (Double.IsInfinity(Math.Abs(newTransaction.TransactionUnits)) = False) Then
              TransactionTable.AddtblTransactionRow(newTransaction)

              MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblTransaction, New DSTransaction.tblTransactionRow() {newTransaction})

              PostedUpdate = True

              If (newTransaction.AuditID <> 0) Then
                UpdateString &= newTransaction.AuditID.ToString & ","
              End If
            End If

            ' Remove Transaction Modification record for processed trades.

            If (Grid_Transaction_Updates.ContainsKey(thisSubFundID)) Then
              ThisTransactionChanges = Grid_Transaction_Updates(thisSubFundID)

              If (ThisTransactionChanges.ContainsKey(CUInt(thisInstrumentID))) Then
                ThisTransactionChanges.Remove(thisInstrumentID)
              End If
            End If
          End If
        Next

        If (PostedUpdate) Then
          MainForm.Main_RaiseEvent(New RenaissanceUpdateEventArgs(RenaissanceChangeID.tblTransaction, UpdateString))
        End If
      End If


    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error submitting trades.", ex.Message, ex.StackTrace, True)

    End Try



  End Sub


  ''' <summary>
  ''' Handles the Move event of the ContextMenu_Transactions control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub ContextMenu_Transactions_Move(ByVal sender As Object, ByVal e As System.EventArgs) Handles ContextMenu_Transactions.Move
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Try
      Dim thisContextMenu As ContextMenuStrip = CType(sender, ContextMenuStrip)
      Dim ht As HitTestInfo = Grid_PendingTransactions.HitTest(Grid_PendingTransactions.PointToClient(thisContextMenu.PointToScreen(New Point(0, 0))))

      If (ht.Row > 0) Then
        LastEnteredTransactionsGridRow = ht.Row

        If (ContextMenu_Transactions.Visible) Then
          Grid_PendingTransactions.Select(LastEnteredTransactionsGridRow, 0, LastEnteredTransactionsGridRow, Grid_PendingTransactions.Cols.Count - 1)
        End If
      End If

    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Handles the VisibleChanged event of the ContextMenu_Transactions control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub ContextMenu_Transactions_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ContextMenu_Transactions.VisibleChanged
    ' *******************************************************************************
    '
    ' *******************************************************************************
    Try
      If (ContextMenu_Transactions.Visible) Then
        Grid_PendingTransactions.Select(LastEnteredTransactionsGridRow, 0, LastEnteredTransactionsGridRow, Grid_PendingTransactions.Cols.Count - 1)
      End If
    Catch ex As Exception
    End Try
  End Sub


  ''' <summary>
  ''' Handles the Click event of the Menu_SubmitTransaction control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Menu_SubmitTransaction_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_SubmitTransaction.Click
    ' *******************************************************************************
    '
    ' *******************************************************************************
    Try
      Dim RadioVal As Boolean = Radio_Transactions_All.Checked

      Grid_PendingTransactions.Select(LastEnteredTransactionsGridRow, 0, LastEnteredTransactionsGridRow, Grid_PendingTransactions.Cols.Count - 1)
      Radio_Transactions_Selected.Checked = True
      Call Button_Trades_Submit_Click(Button_Trades_Submit, Nothing)

      If (RadioVal) Then
        Radio_Transactions_All.Checked = True
      End If
    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Handles the Click event of the Menu_SubmitALLTransactions control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Menu_SubmitALLTransactions_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_SubmitALLTransactions.Click
    ' *******************************************************************************
    '
    ' *******************************************************************************
    Try
      Dim RadioVal As Boolean = Radio_Transactions_Selected.Checked

      Call Button_Trades_SelectAll_Click(Button_Trades_SelectAll, Nothing)
      Radio_Transactions_All.Checked = True
      Call Button_Trades_Submit_Click(Button_Trades_Submit, Nothing)

      If (RadioVal) Then
        Radio_Transactions_Selected.Checked = True
      End If
    Catch ex As Exception
    End Try
  End Sub


#End Region

#Region " Post Transfers and Transactions."

    ''' <summary>
    ''' Posts the transfer trade.
    ''' </summary>
    ''' <param name="ThisTransfer">The this transfer.</param>
    ''' <returns>System.String.</returns>
  Private Function PostTransferTrade(ByVal ThisTransfer As PendingTransfer) As String
    ' *******************************************************************************
    ' Save Transfer trade.
    '
    ' Note : Does not post Update message.
    '
    ' *******************************************************************************
    Dim UpdateString As String = ""

    Try

      Dim TransactionDS As DSTransaction
      Dim TransactionTable As DSTransaction.tblTransactionDataTable
      Dim newTransaction As DSTransaction.tblTransactionRow
      Dim TransactionTableIsLoaded As Boolean = True
      Dim InstrumentRow As DSInstrument.tblInstrumentRow
      Dim LegCount As Integer

      ' Use the Transactions table, if it is loaded, else use a temporary one.

      TransactionDS = MainForm.MainDataHandler.Get_Dataset(RenaissanceStandardDatasets.GetStandardDataset(RenaissanceChangeID.tblTransaction).DatasetName)
      If (TransactionDS Is Nothing) Then
        TransactionDS = New DSTransaction
        TransactionTableIsLoaded = False
      End If
      TransactionTable = TransactionDS.tblTransaction

      ' OK, Onwards.....

      InstrumentRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, ThisTransfer.InstrumentID), DSInstrument.tblInstrumentRow)

      For LegCount = 1 To 2

        newTransaction = TransactionTable.NewtblTransactionRow

        newTransaction.TransactionInstrument = ThisTransfer.InstrumentID
        newTransaction.TransactionVersusInstrument = 0
        newTransaction.TransactionUnits = Math.Abs(ThisTransfer.Quantity)
        newTransaction.TransactionPrice = ThisTransfer.TradePrice

        If (LegCount = 1) Then
          newTransaction.TransactionFund = ThisTransfer.FromFundID
          newTransaction.TransactionSubFund = ThisTransfer.FromSubFundID

          If (InstrumentRow.InstrumentType = InstrumentTypes.Cash) Then
            If (ThisTransfer.Quantity < 0.0#) Then
              ' Negative Cash Transfer From this Fund (i.e. Credit to this fund) = Buy FX
              newTransaction.TransactionType = TransactionTypes.BuyFX
            Else
              ' Positive Cash Transfer From this Fund = Sell FX
              newTransaction.TransactionType = TransactionTypes.SellFX
            End If
          Else
            If (ThisTransfer.Quantity < 0.0#) Then
              ' Negative Fund Transfer From this Fund (i.e. Credit to this fund) = Buy 
              newTransaction.TransactionType = TransactionTypes.Buy
            Else
              ' Positive fund Transfer From this Fund (i.e. Debit to this fund) = Sell 
              newTransaction.TransactionType = TransactionTypes.Sell
            End If
          End If

        ElseIf (LegCount = 2) Then
          newTransaction.TransactionFund = ThisTransfer.ToFundID
          newTransaction.TransactionSubFund = ThisTransfer.ToSubFundID

          ' Reverse of leg One. Exercise care here....

          If (InstrumentRow.InstrumentType = InstrumentTypes.Cash) Then
            If (ThisTransfer.Quantity < 0.0#) Then
              newTransaction.TransactionType = TransactionTypes.SellFX
            Else
              newTransaction.TransactionType = TransactionTypes.BuyFX
            End If
          Else
            If (ThisTransfer.Quantity < 0.0#) Then
              newTransaction.TransactionType = TransactionTypes.Sell
            Else
              newTransaction.TransactionType = TransactionTypes.Buy
            End If
          End If

        End If

        newTransaction.TransactionTradeStatusID = DEFAULT_EOD_TRADESTATUS
        newTransaction.TransactionCounterparty = 1 ' Market

        newTransaction.TransactionGroup = ""
        newTransaction.TransactionInvestment = ""
        newTransaction.TransactionExecution = ""
        newTransaction.TransactionDataEntry = ""
        newTransaction.TransactionDecisionDate = Now.Date
        newTransaction.TransactionValueDate = Now.Date

        ' For transfers, settlement is instant
        newTransaction.TransactionSettlementDate = newTransaction.TransactionValueDate

        'If (newTransaction.TransactionType = TransactionTypes.SellFX) OrElse (newTransaction.TransactionType = TransactionTypes.Sell) Then
        '  newTransaction.TransactionSettlementDate = RenaissanceUtilities.DatePeriodFunctions.AddPeriodToDate(InstrumentRow.InstrumentDealingPeriod, newTransaction.TransactionValueDate, InstrumentRow.InstrumentDefaultSettlementDays_Sell)
        'Else
        '  newTransaction.TransactionSettlementDate = RenaissanceUtilities.DatePeriodFunctions.AddPeriodToDate(InstrumentRow.InstrumentDealingPeriod, newTransaction.TransactionValueDate, InstrumentRow.InstrumentDefaultSettlementDays_Buy)
        'End If

        newTransaction.TransactionConfirmationDate = newTransaction.TransactionSettlementDate
        newTransaction.TransactionValueorAmount = "Amount"
        newTransaction.TransactionCosts = 0.0#
        newTransaction.TransactionCostPercent = 0.0#
        newTransaction.TransactionSettlement = newTransaction.TransactionUnits

        ' Transfer Details.

        newTransaction.TransactionIsTransfer = True
        newTransaction.TransactionEffectivePrice = newTransaction.TransactionPrice
        newTransaction.TransactionEffectiveValueDate = newTransaction.TransactionValueDate
        newTransaction.TransactionSpecificInitialEqualisationFlag = False
        newTransaction.TransactionSpecificInitialEqualisationValue = 0
        newTransaction.TransactionEffectiveWatermark = 0

        newTransaction.TransactionCostIsPercent = False
        newTransaction.TransactionExemptFromUpdate = True
        newTransaction.TransactionFinalAmount = (-1)
        newTransaction.TransactionFinalPrice = (-1)
        newTransaction.TransactionFinalCosts = (-1)
        newTransaction.TransactionFinalSettlement = (-1)
        newTransaction.TransactionInstructionFlag = (-1)
        newTransaction.TransactionRedeemWholeHoldingFlag = False
        newTransaction.TransactionBankConfirmation = 0
        newTransaction.TransactionInitialDataEntry = 0
        newTransaction.TransactionAmountConfirmed = 0
        newTransaction.TransactionSettlementConfirmed = 0
        newTransaction.TransactionFinalDataEntry = 0
        newTransaction.TransactionRedemptionID = 0
        newTransaction.TransactionComment = "Sub-Fund Transfer."
        newTransaction.TransactionCTFLAGS = 0
        newTransaction.TransactionCompoundRN = 0

        If (Math.Abs(newTransaction.TransactionUnits) >= 0.00001) Then
          TransactionTable.AddtblTransactionRow(newTransaction)

          MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblTransaction, New DSTransaction.tblTransactionRow() {newTransaction})

          UpdateString &= newTransaction.AuditID.ToString & ","
        End If

      Next

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error in PostTransferTrade().", ex.Message, ex.StackTrace, True)
    End Try

    Return UpdateString
  End Function


#End Region

#Region " Bug hunting "

  ' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
  ' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
  ' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

    ''' <summary>
    ''' Called when [row updating].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

    ''' <summary>
    ''' Called when [row updated].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

    ''' <summary>
    ''' Called when [row fill error].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
    Dim a As Integer

    a = 1
  End Sub

#End Region



    ''' <summary>
    ''' Gets the instrument price.
    ''' </summary>
    ''' <param name="InstrumentID">The instrument ID.</param>
    ''' <returns>System.Double.</returns>
  Private Function GetInstrumentPrice(ByVal InstrumentID As UInteger) As Double
    ' ******************************************************************************************************
    '
    ' ******************************************************************************************************

    Try
      Dim PriceLevel As Double
      Dim PriceDate As Date

      Call GetInstrumentPrice(CInt(RenaissancePertracDataClass.PertracDataClass.GetInstrumentFromID(InstrumentID)), PriceLevel, PriceDate)

      Return PriceLevel
    Catch ex As Exception
    End Try

    Return 0.0#
  End Function

  Private Function GetInstrumentPrice(ByVal InstrumentID As Integer) As Double
    ' ******************************************************************************************************
    '
    ' ******************************************************************************************************

    Try
      Dim PriceLevel As Double
      Dim PriceDate As Date

      Call GetInstrumentPrice(CInt(RenaissancePertracDataClass.PertracDataClass.GetInstrumentFromID(InstrumentID)), PriceLevel, PriceDate)

      Return PriceLevel
    Catch ex As Exception
    End Try

    Return 0.0#
  End Function

    ''' <summary>
    ''' Gets the instrument price.
    ''' </summary>
  ''' <param name="InstrumentID">The instrument ID.</param>
  ''' <param name="PriceValue">(Output) Returns the PriceValue.</param>
  ''' <param name="PriceDate">(Output) Returns the PriceDate.</param>
  Private Sub GetInstrumentPrice(ByVal InstrumentID As Integer, ByRef PriceValue As Double, ByRef PriceDate As Date)
    ' ******************************************************************************************************
    '
    ' Return the Latest Price for a given Instrument from the sorted array of
    ' Instrument prices.
    '
    ' ******************************************************************************************************
    Static LastInstrumentID As Integer = 0
    Static LastInstrumentPrice As Double = 0
    Static LastInstrumentPriceDate As Date = Renaissance_BaseDate

    Try
      If (InstrumentID = LastInstrumentID) OrElse (InstrumentID <= 0) Then
        If (InstrumentID <= 0) Then ' Facilitate the explicit clearing of Static variables
          LastInstrumentID = 0
          LastInstrumentPrice = 0
          LastInstrumentPriceDate = Renaissance_BaseDate
        End If

        PriceValue = LastInstrumentPrice
        PriceDate = LastInstrumentPriceDate
        Exit Sub
      End If
    Catch ex As Exception
    End Try

    Try

      ' Check the Prices table is loaded and the sorted Prices array is established

      If (BestPricesTable Is Nothing) Then
        BestPricesTable = New DataTable
        Dim thisCommand As New SqlCommand

        Try

          thisCommand.CommandType = CommandType.Text
          thisCommand.CommandText = "SELECT InstrumentID, PriceDate, PriceLevel FROM dbo.fn_BestPrice(@ValueDate, @OnlyFinalPrices, @KnowledgeDate)"
          thisCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

          thisCommand.Parameters.Add(New SqlParameter("@ValueDate", SqlDbType.DateTime)).Value = RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW
          thisCommand.Parameters.Add(New SqlParameter("@OnlyFinalPrices", SqlDbType.Int)).Value = 0
          thisCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", SqlDbType.DateTime)).Value = MainForm.Main_Knowledgedate

          thisCommand.Connection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)

          If (thisCommand IsNot Nothing) AndAlso (thisCommand.Connection IsNot Nothing) Then

            SyncLock thisCommand.Connection
              MainForm.LoadTable_Custom(BestPricesTable, thisCommand)
            End SyncLock
          End If

          SortedPrices = BestPricesTable.Select("True", "InstrumentID, PriceDate")

        Catch ex As Exception

          MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error Getting Best Prices.", ex.Message, ex.StackTrace, True)

          If (BestPricesTable IsNot Nothing) Then
            BestPricesTable.Clear()
          End If

          BestPricesTable = Nothing
          SortedPrices = Nothing

        Finally

          If (thisCommand IsNot Nothing) Then
            thisCommand.Connection = Nothing
          End If
          thisCommand = Nothing

        End Try

      End If

      If (BestPricesTable Is Nothing) OrElse (SortedPrices.Length <= 0) Then
        LastInstrumentID = 0
        LastInstrumentPrice = 0
        LastInstrumentPriceDate = Renaissance_BaseDate

        PriceValue = LastInstrumentPrice
        PriceDate = LastInstrumentPriceDate
        Exit Sub
      End If

    Catch ex As Exception
      LastInstrumentID = 0
      LastInstrumentPrice = 0
      LastInstrumentPriceDate = Renaissance_BaseDate

      PriceValue = LastInstrumentPrice
      PriceDate = LastInstrumentPriceDate

      Exit Sub
    End Try

    ' Find Latest Price

    ' Code changed to use the fn_BestPrice function , thus there is only One Price per instrument.

    Dim FirstIndex As Integer
    Dim MidIndex As Integer
    Dim LastIndex As Integer
    Dim thisPriceRow As DataRow

    Dim InstrumentIdOrdinal As Integer = BestPricesTable.Columns.IndexOf("InstrumentID")
    Dim PriceLevelOrdinal As Integer = BestPricesTable.Columns.IndexOf("PriceLevel")
    Dim PriceDateOrdinal As Integer = BestPricesTable.Columns.IndexOf("PriceDate")

    Try
      FirstIndex = 0
      LastIndex = SortedPrices.Length - 1

      ' Check the First and last Items

      thisPriceRow = SortedPrices(FirstIndex)
      If CInt(thisPriceRow(InstrumentIdOrdinal)) = InstrumentID Then
        LastInstrumentID = InstrumentID
        LastInstrumentPrice = CDbl(thisPriceRow(PriceLevelOrdinal))
        LastInstrumentPriceDate = CDate(thisPriceRow(PriceDateOrdinal))

        PriceValue = LastInstrumentPrice
        PriceDate = LastInstrumentPriceDate

        Exit Sub
      End If

      thisPriceRow = SortedPrices(LastIndex)
      If CInt(thisPriceRow(InstrumentIdOrdinal)) = InstrumentID Then
        LastInstrumentID = InstrumentID
        LastInstrumentPrice = CDbl(thisPriceRow(PriceLevelOrdinal))
        LastInstrumentPriceDate = CDate(thisPriceRow(PriceDateOrdinal))

        PriceValue = LastInstrumentPrice
        PriceDate = LastInstrumentPriceDate

        Exit Sub
      End If

      ' Chop the array to find the Instrument.

      While (LastIndex > (FirstIndex + 1))

        MidIndex = CInt((LastIndex + FirstIndex) / 2)
        thisPriceRow = SortedPrices(MidIndex)

        If CInt(thisPriceRow(InstrumentIdOrdinal)) = InstrumentID Then
          LastInstrumentID = InstrumentID
          LastInstrumentPrice = CDbl(thisPriceRow(PriceLevelOrdinal))
          LastInstrumentPriceDate = CDate(thisPriceRow(PriceDateOrdinal))

          PriceValue = LastInstrumentPrice
          PriceDate = LastInstrumentPriceDate

          Exit Sub
        ElseIf CInt(thisPriceRow(InstrumentIdOrdinal)) < InstrumentID Then
          FirstIndex = MidIndex
        Else
          LastIndex = MidIndex
        End If

      End While
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error returning Best Price.", ex.Message, ex.StackTrace, True)
    End Try

    LastInstrumentID = 0
    LastInstrumentPrice = 0.0#
    LastInstrumentPriceDate = Renaissance_BaseDate

    PriceValue = LastInstrumentPrice
    PriceDate = LastInstrumentPriceDate

    Exit Sub

  End Sub

    ''' <summary>
    ''' Handles the Paint event of the Menu_SubmitTransaction control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.PaintEventArgs"/> instance containing the event data.</param>
  Private Sub Menu_SubmitTransaction_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Menu_SubmitTransaction.Paint

    'If (Menu_SubmitTransaction.Visible = False) Then
    '  Dim MousePoint As Point = MousePosition()
    '  Dim pt As Point = Grid_Positions.PointToClient(New Point(MousePoint.X, MousePoint.Y))
    '  Dim hti As HitTestInfo = Grid_PendingTransactions.HitTest(pt.X, pt.Y)

    '  If (hti.Row < 0) Then
    '    Grid_PendingTransactions.Select(Grid_PendingTransactions.Rows.Count - 1, 0, Grid_PendingTransactions.Rows.Count - 1, Grid_PendingTransactions.Cols.Count - 1)
    '    LastEnteredTransactionsGridRow = Grid_PendingTransactions.Rows.Count - 1
    '  Else
    '    Grid_PendingTransactions.Select(hti.Row - 1, 0, hti.Row - 1, Grid_PendingTransactions.Cols.Count - 1)
    '    LastEnteredTransactionsGridRow = hti.Row - 1
    '  End If

    '  Debug.Print("LastEnteredTransactionsGridRow = " & LastEnteredTransactionsGridRow.ToString())

    'End If

  End Sub

    ''' <summary>
    ''' Handles the MouseDown event of the Label_DragNewInstrument control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.MouseEventArgs"/> instance containing the event data.</param>
  Private Sub Label_DragNewInstrument_MouseDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Label_DragNewInstrument.MouseDown
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      If Label_DragNewInstrument.DoDragDrop("DragInstrument", DragDropEffects.Copy Or DragDropEffects.Move) = DragDropEffects.Move Then

      End If
    Catch ex As Exception
    End Try
  End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the Check_ByISIN control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Check_ByISIN_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_ByISIN.CheckedChanged
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      Call SetInstrumentCombo()
    Catch ex As Exception
    End Try
  End Sub

    ''' <summary>
    ''' Handles the KeyDown event of the Grid_Positions control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Positions_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Grid_Positions.KeyDown
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        Try
          If e.Control And (e.KeyCode = Keys.C) Then
            Call MainForm.CopyGridSelection(CType(sender, C1.Win.C1FlexGrid.C1FlexGrid), True, False, True)
          ElseIf e.Control And (e.KeyCode = Keys.V) Then
            Dim ClipboardData As String
            Dim DragStringArray() As String

            ClipboardData = Clipboard.GetData(System.Windows.Forms.DataFormats.Text)

            If (ClipboardData IsNot Nothing) AndAlso (ClipboardData.Length > 0) Then
              DragStringArray = ClipboardData.Split(ControlChars.CrLf.ToCharArray(), StringSplitOptions.RemoveEmptyEntries)

              If (DragStringArray.Length > 0) Then

                Try

                  ProcessDraggedBasket(DragStringArray, 0)

                Catch ex As Exception
                End Try

              End If

            End If
          End If
        Catch ex As Exception
        End Try

      End If
    Catch ex As Exception
    End Try
  End Sub

    ''' <summary>
    ''' Handles the KeyDown event of the Grid_Valuations control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Valuations_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Grid_Valuations.KeyDown
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        Try
          If e.Control And (e.KeyCode = Keys.C) Then
            Call MainForm.CopyGridSelection(CType(sender, C1.Win.C1FlexGrid.C1FlexGrid), True, False, True)
          End If
        Catch ex As Exception
        End Try

      End If
    Catch ex As Exception
    End Try
  End Sub



  Private Sub Check_ValuationsIncludeUnsettledFX_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_ValuationsIncludeUnsettledFX.CheckedChanged

    Try
      Call PaintPositionsGrid()
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error in Check_ValuationsIncludeUnsettledFX_CheckedChanged().", ex.Message, ex.StackTrace, True)
    End Try

  End Sub

End Class
