' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmInstrumentParent.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals


''' <summary>
''' Class frmInstrumentParent
''' </summary>
Public Class frmInstrumentParent

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmInstrumentParent"/> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The BTN cancel
    ''' </summary>
  Friend WithEvents btnCancel As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN save
    ''' </summary>
  Friend WithEvents btnSave As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN close
    ''' </summary>
  Friend WithEvents btnClose As System.Windows.Forms.Button
    ''' <summary>
    ''' The tree view_ instruments
    ''' </summary>
  Friend WithEvents TreeView_Instruments As System.Windows.Forms.TreeView
    ''' <summary>
    ''' The button_ move up
    ''' </summary>
  Friend WithEvents Button_MoveUp As System.Windows.Forms.Button
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.btnCancel = New System.Windows.Forms.Button
    Me.btnSave = New System.Windows.Forms.Button
    Me.btnClose = New System.Windows.Forms.Button
    Me.TreeView_Instruments = New System.Windows.Forms.TreeView
    Me.Button_MoveUp = New System.Windows.Forms.Button
    Me.SuspendLayout()
    '
    'btnCancel
    '
    Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnCancel.Location = New System.Drawing.Point(8, 500)
    Me.btnCancel.Name = "btnCancel"
    Me.btnCancel.Size = New System.Drawing.Size(75, 28)
    Me.btnCancel.TabIndex = 1
    Me.btnCancel.Text = "&Cancel"
    '
    'btnSave
    '
    Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnSave.Location = New System.Drawing.Point(320, 500)
    Me.btnSave.Name = "btnSave"
    Me.btnSave.Size = New System.Drawing.Size(75, 28)
    Me.btnSave.TabIndex = 4
    Me.btnSave.Text = "&Save"
    '
    'btnClose
    '
    Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnClose.Location = New System.Drawing.Point(92, 500)
    Me.btnClose.Name = "btnClose"
    Me.btnClose.Size = New System.Drawing.Size(75, 28)
    Me.btnClose.TabIndex = 2
    Me.btnClose.Text = "&Close"
    '
    'TreeView_Instruments
    '
    Me.TreeView_Instruments.AllowDrop = True
    Me.TreeView_Instruments.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.TreeView_Instruments.Location = New System.Drawing.Point(4, 4)
    Me.TreeView_Instruments.Name = "TreeView_Instruments"
    Me.TreeView_Instruments.Size = New System.Drawing.Size(452, 480)
    Me.TreeView_Instruments.Sorted = True
    Me.TreeView_Instruments.TabIndex = 0
    '
    'Button_MoveUp
    '
    Me.Button_MoveUp.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Button_MoveUp.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Button_MoveUp.Location = New System.Drawing.Point(232, 500)
    Me.Button_MoveUp.Name = "Button_MoveUp"
    Me.Button_MoveUp.Size = New System.Drawing.Size(28, 28)
    Me.Button_MoveUp.TabIndex = 3
    Me.Button_MoveUp.Text = "<"
    '
    'frmInstrumentParent
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.CancelButton = Me.btnCancel
    Me.ClientSize = New System.Drawing.Size(460, 534)
    Me.Controls.Add(Me.Button_MoveUp)
    Me.Controls.Add(Me.TreeView_Instruments)
    Me.Controls.Add(Me.btnClose)
    Me.Controls.Add(Me.btnSave)
    Me.Controls.Add(Me.btnCancel)
    Me.Name = "frmInstrumentParent"
    Me.Text = "Set Instrument Parents"
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Form Locals and Constants "


  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
  Private WithEvents _MainForm As VeniceMain

  ' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
  Private FormTooltip As New ToolTip()

  ' Form Menu


  ' Form Constants, specific to the table being updated.

    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
  Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

  ' Form Locals, initialised on 'New' defining what standard data items to use
    ''' <summary>
  ''' Variable holding the form's data table name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_TABLENAME As String
    ''' <summary>
  ''' Variable holding the form's data adaptor name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_ADAPTORNAME As String
    ''' <summary>
  ''' Variable holding the form's dataset name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_DATASETNAME As String


  ' The standard ChangeID for this form. e.g. tblInstrument
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

  ' Form Specific Order fields
    ''' <summary>
  ''' Field Name to show in the Select combo.
    ''' </summary>
  Private THIS_FORM_SelectBy As String
    ''' <summary>
  ''' Field Name to order the Select combo by.
    ''' </summary>
  Private THIS_FORM_OrderBy As String

    ''' <summary>
  ''' The field name that holds the unique identifier for each entry. e.g. FundID for the tblFund table.
    ''' </summary>
  Private THIS_FORM_ValueMember As String

  ' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
  Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
  Private THIS_FORM_FormID As VeniceFormID

  ' Data Structures

    ''' <summary>
    ''' My dataset
    ''' </summary>
	Private myDataset As RenaissanceDataClass.DSInstrument ' Form Specific !!!!
    ''' <summary>
    ''' My table
    ''' </summary>
	Private myTable As RenaissanceDataClass.DSInstrument.tblInstrumentDataTable
    ''' <summary>
    ''' My connection
    ''' </summary>
	Private myConnection As SqlConnection
    ''' <summary>
    ''' My adaptor
    ''' </summary>
	Private myAdaptor As SqlDataAdapter

    ''' <summary>
    ''' The this standard dataset
    ''' </summary>
	Private ThisStandardDataset As RenaissanceGlobals.StandardDataset


	' Active Element.

    ''' <summary>
    ''' The sorted rows
    ''' </summary>
	Private SortedRows() As DataRow
    ''' <summary>
    ''' The select by sorted rows
    ''' </summary>
	Private SelectBySortedRows() As DataRow
    ''' <summary>
    ''' The this data row
    ''' </summary>
	Private thisDataRow As RenaissanceDataClass.DSInstrument.tblInstrumentRow		' Form Specific !!!!
    ''' <summary>
    ''' The this audit ID
    ''' </summary>
  Private thisAuditID As Integer
    ''' <summary>
    ''' The this position
    ''' </summary>
  Private thisPosition As Integer
    ''' <summary>
    ''' The __ is over cancel button
    ''' </summary>
  Private __IsOverCancelButton As Boolean
    ''' <summary>
    ''' The _ in use
    ''' </summary>
  Private _InUse As Boolean

  ' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
  Private FormIsValid As Boolean
    ''' <summary>
    ''' The form changed
    ''' </summary>
  Private FormChanged As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
  Private _FormOpenFailed As Boolean
    ''' <summary>
    ''' The in paint
    ''' </summary>
  Private InPaint As Boolean
    ''' <summary>
    ''' The add new record
    ''' </summary>
  Private AddNewRecord As Boolean

  ' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
  Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
  Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
  Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
  Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
  Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
    ' Public property to return handle to the 'Main' Venice form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
  Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
    ' Public property maintaining a value indicating if the cursor is over the 'Cancel'
    ' Button on this form.
    ' This property is specifically designed for use by the field formating Event functions
    ' In order that they do not impose format restrictions if the user is about to click the 
    ' 'Cancel' button.
    '
    Get
      Return __IsOverCancelButton
    End Get
    Set(ByVal Value As Boolean)
      __IsOverCancelButton = Value
    End Set
  End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
  Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
    Get
      Return InPaint
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
    Get
      Return _InUse
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

#End Region

    ''' <summary>
    ''' Class ID_Struct
    ''' </summary>
  Private Class ID_Struct
        ''' <summary>
        ''' The audit ID
        ''' </summary>
    Public AuditID As Integer
        ''' <summary>
        ''' The instrument ID
        ''' </summary>
    Public InstrumentID As Integer
        ''' <summary>
        ''' The parent ID
        ''' </summary>
    Public ParentID As Integer

        ''' <summary>
        ''' Initializes a new instance of the <see cref="ID_Struct"/> class.
        ''' </summary>
        ''' <param name="pAuditID">The p audit ID.</param>
        ''' <param name="pInstrumentID">The p instrument ID.</param>
        ''' <param name="pParentID">The p parent ID.</param>
    Public Sub New(ByVal pAuditID As Integer, ByVal pInstrumentID As Integer, ByVal pParentID As Integer)
      Me.AuditID = pAuditID
      Me.InstrumentID = pInstrumentID
      Me.ParentID = pParentID
    End Sub
  End Class

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmInstrumentParent"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  Public Sub New(ByVal pMainForm As VeniceMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()



    _MainForm = pMainForm
    AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

    _FormOpenFailed = False
    _InUse = True

    ' ******************************************************
    ' Form Specific Settings :
    ' ******************************************************


    ' Default Select and Order fields.

    THIS_FORM_SelectBy = "InstrumentDescription"
    THIS_FORM_OrderBy = "InstrumentDescription"

    THIS_FORM_ValueMember = "InstrumentID"

    ' Form Permissioning :-

    THIS_FORM_PermissionArea = "frmInstrument"
    THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

    ' 'This' form ID

    THIS_FORM_FormID = VeniceFormID.frmInstrument

    ' This form's dataset type.

    ThisStandardDataset = RenaissanceStandardDatasets.tblInstrument ' This Defines the Form Data !!! 



    ' Form Control Changed events

    ' Set up the ToolTip
    MainForm.SetFormToolTip(Me, FormTooltip)

    ' ******************************************************
    ' End Form Specific.
    ' ******************************************************

    ' Data object names standard to this Form type.

    THIS_TABLENAME = ThisStandardDataset.TableName
    THIS_ADAPTORNAME = ThisStandardDataset.AdaptorName
    THIS_DATASETNAME = ThisStandardDataset.DatasetName

    THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

    ' Establish / Retrieve data objects for this form.

    myConnection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)
    myAdaptor = MainForm.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, VENICE_CONNECTION, THIS_TABLENAME)
    myDataset = MainForm.Load_Table(ThisStandardDataset, False)
    myTable = myDataset.Tables(0)

  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
  Public Sub ResetForm() Implements StandardVeniceForm.ResetForm
    THIS_FORM_SelectBy = "InstrumentDescription"
    THIS_FORM_OrderBy = "InstrumentDescription"

    Call Form_Load(Me, New System.EventArgs)
  End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
  Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
    ALWAYS_CLOSE_THIS_FORM = True
    Me.Close()
  End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
    _FormOpenFailed = False
    _InUse = True

    ' Initialise Data structures. Connection, Adaptor and Dataset.

    If Not (MainForm Is Nothing) Then
      FormIsValid = True
    Else
      MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (myConnection Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (myAdaptor Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (myDataset Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    ' Initialse form

    InPaint = True
    IsOverCancelButton = False

    ' Check User permissions
    Call CheckPermissions()
    If (HasReadPermission = False) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

      FormIsValid = False
      _FormOpenFailed = True

      Exit Sub
    End If

    ' Build Sorted data list from which this form operates
    Call SetSortedRows()


    ' Display initial record.
    Call GetInitialData()

    InPaint = False


  End Sub

    ''' <summary>
    ''' Handles the Closing event of the frmInstrument control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
  Private Sub frmInstrument_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    Dim HideForm As Boolean

    ' Hide or Close this form ?
    ' All depends on how many of this form type are Open or in Cache...

    _InUse = False

    If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
      HideForm = False
    Else
      If (FormChanged = True) Then
        Call SetFormData()
      End If

      HideForm = True
      If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
        HideForm = False
      End If
    End If

    If HideForm = True Then
      MainForm.HideInFormsCollection(Me)
      Me.Hide() ' NPP Fix

      e.Cancel = True
    Else
      Try
        MainForm.RemoveFromFormsCollection(Me)
        RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate
      Catch ex As Exception
      End Try
    End If

  End Sub


#End Region

  ' Routine to handle changes / updates to tables by this and other windows.
  ' If this, or any other, form posts a change to a table, then it will invoke an update event 
  ' detailing what tables have been altered.
  ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
  ' the 'VeniceAutoUpdate' event of the main Venice form.
  ' Each form may them react as appropriate to changes in any table that might impact it.
  '
    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    Dim OrgInPaint As Boolean
    Dim KnowledgeDateChanged As Boolean
    Dim SetButtonStatus_Flag As Boolean

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

    OrgInPaint = InPaint
    InPaint = True
    KnowledgeDateChanged = False
    SetButtonStatus_Flag = False

    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
      KnowledgeDateChanged = True
    End If

    ' ****************************************************************
    ' Check for changes relevant to this form
    ' ****************************************************************


    ' Changes to the KnowledgeDate :-
    If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
      SetButtonStatus_Flag = True
    End If

    ' Changes to the tblUserPermissions table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

      ' Check ongoing permissions.

      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

        FormIsValid = False
        Me.Close()
        Exit Sub
      End If

      SetButtonStatus_Flag = True

    End If


    ' ****************************************************************
    ' Changes to the Main FORM table :-
    ' ****************************************************************

    If (e.TableChanged(THIS_FORM_ChangeID) = True) Or KnowledgeDateChanged Then

      ' Re-Set Controls etc.
      Call SetSortedRows()

      Dim NodeArray() As TreeNode
      Dim thisDataRow As DataRow
      Dim MissingNodes As Integer
      Dim ParentIDsChanged As Boolean
      Dim NodeCounter As Integer

      MissingNodes = 0
      ParentIDsChanged = False

      ' Check some Instruments still remain

      If SortedRows Is Nothing Then
        GetInitialData()
        GoTo finish
      End If

      If SortedRows.Length <= 0 Then
        GetInitialData()
        GoTo finish
      End If

      ' Check each Instrument in the table against it's respective node (if it exists)
      NodeArray = GetNodeArray(Me.TreeView_Instruments)
      For Each thisDataRow In SortedRows
        MissingNodes += 1

        For NodeCounter = 0 To (NodeArray.GetLength(0) - 1)
          If Not (NodeArray(NodeCounter) Is Nothing) Then
            If CInt(thisDataRow("AuditID")) = CType(NodeArray(NodeCounter).Tag, ID_Struct).AuditID Then
              ' OK, this Instrument exists as a node...
              MissingNodes -= 1

              ' Has it changed ?
              If (CInt(thisDataRow("InstrumentParent")) <> CType(NodeArray(NodeCounter).Tag, ID_Struct).ParentID) Then
                ' Parent has changed

                MessageBox.Show("The Instrument Parent heirarchy has been changed by another user." & vbCrLf & "Parent structure will be re-loaded, any unsaved changed will be lost.", "Warning : External Instrument Change", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                GetInitialData()
                GoTo finish
              End If
            End If
          End If
        Next
      Next

      If (MissingNodes > 0) Then
        MessageBox.Show("The new Instruments have been added by another user." & vbCrLf & "Parent structure will be re-loaded, any unsaved changed will be lost.", "Warning : External Instrument Change", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
        GetInitialData()
      End If

    End If

    ' ****************************************************************
    ' Repaint if not currently in Edit Mode
    '
    ' ****************************************************************
finish:

    InPaint = OrgInPaint
    If SetButtonStatus_Flag Then
      Call SetButtonStatus()
    End If

  End Sub

    ''' <summary>
    ''' Gets the node array.
    ''' </summary>
    ''' <param name="pTreeView">The p tree view.</param>
    ''' <returns>TreeNode[][].</returns>
  Private Function GetNodeArray(ByRef pTreeView As TreeView) As TreeNode()
    ' ****************************************************************
    ' Header function to retrieve an array of ALL TreeNodes from the 
    ' Given TreeView.
    ' ****************************************************************
    Dim ReturnArray() As TreeNode
    Dim NodeCount As Integer

    Try
      Dim ThisNode As TreeNode
      NodeCount = 0
      For Each ThisNode In pTreeView.Nodes
        NodeCount += CountNodeArray(ThisNode)
      Next

      ReDim ReturnArray(NodeCount)

      NodeCount = 0
      For Each ThisNode In pTreeView.Nodes
        PopulateNodeArray(ThisNode, ReturnArray, NodeCount)
      Next

    Catch ex As Exception
      Erase ReturnArray
    End Try

    Return ReturnArray

  End Function

    ''' <summary>
    ''' Counts the node array.
    ''' </summary>
    ''' <param name="pTreeNode">The p tree node.</param>
    ''' <returns>System.Int32.</returns>
  Private Function CountNodeArray(ByRef pTreeNode As TreeNode) As Integer
    ' ****************************************************************
    ' Recursive function used to count the number of TreeNodes in a TreeView.
    ' ****************************************************************
    Dim Rval As Integer

    Dim ThisNode As TreeNode
    Rval = 1
    For Each ThisNode In pTreeNode.Nodes
      Rval += CountNodeArray(ThisNode)
    Next

    Return Rval
  End Function

    ''' <summary>
    ''' Populates the node array.
    ''' </summary>
    ''' <param name="pTreeNode">The p tree node.</param>
    ''' <param name="ReturnArray">The return array.</param>
    ''' <param name="RVal">The R val.</param>
  Private Sub PopulateNodeArray(ByRef pTreeNode As TreeNode, ByRef ReturnArray() As TreeNode, ByRef RVal As Integer)
    ' ****************************************************************
    ' Recursive function to retrieve an array of ALL TreeNodes from the 
    ' Given TreeView.
    ' ****************************************************************

    Dim ThisNode As TreeNode
    ReturnArray(RVal) = pTreeNode
    RVal += 1
    For Each ThisNode In pTreeNode.Nodes
      PopulateNodeArray(ThisNode, ReturnArray, RVal)
    Next

  End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

  ' Build Sorted list from the Source dataset and update the Select Combo
    ''' <summary>
    ''' Sets the sorted rows.
    ''' </summary>
  Private Sub SetSortedRows()

    Dim OrgInPaint As Boolean

    ' Code is pretty Generic form here on...

    ' Set paint local so that changes to the selection combo do not trigger form updates.

    OrgInPaint = InPaint
    InPaint = True

    ' Get selected Row sets, or exit if no data is present.
    If myDataset Is Nothing Then
      ReDim SortedRows(0)
      ReDim SelectBySortedRows(0)
    Else
      SortedRows = myTable.Select("True", THIS_FORM_OrderBy)
      SelectBySortedRows = myTable.Select("True", THIS_FORM_SelectBy)
    End If



    InPaint = OrgInPaint
  End Sub


  ' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
  Private Sub CheckPermissions()

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

  End Sub

  ' Flag changes to form controls, Event Associations made in 'New' routine.
    ''' <summary>
  ''' Basic event handler, called when a form value is changed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    If InPaint = False Then
      If (Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord) Then
        FormChanged = True
        Me.btnSave.Enabled = True
        Me.btnCancel.Enabled = True
      End If
    End If

  End Sub

    ''' <summary>
    ''' Selects the menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
  Public Sub SelectMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
    ' Event handler for the Select Menu
    ' Event association is made during the dynamic menu creation

    THIS_FORM_SelectBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

    Call SetSortedRows()

  End Sub

    ''' <summary>
    ''' Orders the menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
  Public Sub OrderMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
    ' Event handler for the Order Menu
    ' Event association is made during the dynamic menu creation

    THIS_FORM_OrderBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

    Call SetSortedRows()

  End Sub


#End Region

#Region " Get & Set Form Data / SetButton / ValidateForm / btnCancel Events (Form Specific Code) "

    ''' <summary>
    ''' Gets the initial data.
    ''' </summary>
  Private Sub GetInitialData()
    Dim GetRows() As DataRow
    Dim thisRow As DataRow

    GetRows = myTable.Select("InstrumentID = InstrumentParent")

    While (Me.TreeView_Instruments.Nodes.Count > 0)
      Me.TreeView_Instruments.Nodes.RemoveAt(0)
    End While
    Me.TreeView_Instruments.Nodes.Clear()

    Try
      Me.TreeView_Instruments.BeginUpdate()

      Dim RootNode As New TreeNode(">")
      RootNode.Tag = New ID_Struct((-1), (-1), (-1))
      Me.TreeView_Instruments.Nodes.Add(RootNode)

      For Each thisRow In GetRows
        Dim NewNode As New TreeNode(thisRow("InstrumentDescription"))
        NewNode.Tag = New ID_Struct(thisRow("AuditID"), thisRow("InstrumentID"), thisRow("InstrumentParent"))
        FillNode(NewNode)
        RootNode.Nodes.Add(NewNode)
      Next
      Me.TreeView_Instruments.EndUpdate()
      RootNode.Expand()

    Catch ex As Exception
    End Try

    Me.btnCancel.Enabled = False
    Me.btnSave.Enabled = False

  End Sub

    ''' <summary>
    ''' Fills the node.
    ''' </summary>
    ''' <param name="InstrumentNode">The instrument node.</param>
  Private Sub FillNode(ByVal InstrumentNode As TreeNode)
    Dim InstrumentID As Integer

    Try
      InstrumentID = CType(InstrumentNode.Tag, ID_Struct).InstrumentID
    Catch ex As Exception
      Exit Sub
    End Try

    Dim GetRows() As DataRow
    Dim thisRow As DataRow

    GetRows = myTable.Select("(InstrumentParent = " & InstrumentID & ") AND (InstrumentID <> " & InstrumentID & ")")

    InstrumentNode.Nodes.Clear()

    Try
      Me.TreeView_Instruments.BeginUpdate()
      For Each thisRow In GetRows
        Dim NewNode As New TreeNode(thisRow("InstrumentDescription"))
        NewNode.Tag = New ID_Struct(thisRow("AuditID"), thisRow("InstrumentID"), thisRow("InstrumentParent"))
        FillNode(NewNode)
        InstrumentNode.Nodes.Add(NewNode)
      Next
      Me.TreeView_Instruments.EndUpdate()

    Catch ex As Exception
      Exit Sub
    End Try


  End Sub

    ''' <summary>
    ''' Handles the BeforeExpand event of the TreeView_Instruments control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.TreeViewCancelEventArgs"/> instance containing the event data.</param>
  Private Sub TreeView_Instruments_BeforeExpand(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewCancelEventArgs) Handles TreeView_Instruments.BeforeExpand
    'FillNode(e.Node)
  End Sub

    ''' <summary>
    ''' Handles the ItemDrag event of the TreeView_Instruments control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.ItemDragEventArgs"/> instance containing the event data.</param>
  Private Sub TreeView_Instruments_ItemDrag(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemDragEventArgs) Handles TreeView_Instruments.ItemDrag
    'Set the drag node and initiate the DragDrop 
    DoDragDrop(e.Item, DragDropEffects.Move)
  End Sub

    ''' <summary>
    ''' Handles the DragEnter event of the TreeView_Instruments control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.DragEventArgs"/> instance containing the event data.</param>
  Private Sub TreeView_Instruments_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles TreeView_Instruments.DragEnter
    'See if there is a TreeNode being dragged
    If e.Data.GetDataPresent("System.Windows.Forms.TreeNode", _
        True) Then
      'TreeNode found allow move effect
      e.Effect = DragDropEffects.Move
    Else
      'No TreeNode found, prevent move
      e.Effect = DragDropEffects.None
    End If
  End Sub

    ''' <summary>
    ''' Handles the DragLeave event of the TreeView_Instruments control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub TreeView_Instruments_DragLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles TreeView_Instruments.DragLeave

  End Sub

    ''' <summary>
    ''' Handles the DragOver event of the TreeView_Instruments control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.DragEventArgs"/> instance containing the event data.</param>
  Private Sub TreeView_Instruments_DragOver(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles TreeView_Instruments.DragOver
    'Check that there is a TreeNode being dragged 
    If e.Data.GetDataPresent("System.Windows.Forms.TreeNode", _
           True) = False Then Exit Sub

    'Get the TreeView raising the event (incase multiple on form)

    Dim selectedTreeview As TreeView = CType(sender, TreeView)

    'As the mouse moves over nodes, provide feedback to the user by highlighting the node that is the 
    'current drop target

    Dim pt As Point = _
        CType(sender, TreeView).PointToClient(New Point(e.X, e.Y))
    Dim targetNode As TreeNode = selectedTreeview.GetNodeAt(pt)

    'See if the targetNode is currently selected, if so no need to validate again

    If Not (selectedTreeview.SelectedNode Is targetNode) Then
      'Select the    node currently under the cursor
      selectedTreeview.SelectedNode = targetNode

      'Check that the selected node is not the dropNode and
      'also that it is not a child of the dropNode and 
      'therefore an invalid target
      Dim dropNode As TreeNode = _
          CType(e.Data.GetData("System.Windows.Forms.TreeNode"), _
          TreeNode)

      Do Until targetNode Is Nothing
        If targetNode Is dropNode Then
          e.Effect = DragDropEffects.None
          Exit Sub
        End If
        targetNode = targetNode.Parent
      Loop

      'Currently selected node is a suitable target
      e.Effect = DragDropEffects.Move
    End If
  End Sub

    ''' <summary>
    ''' Handles the DragDrop event of the TreeView_Instruments control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.DragEventArgs"/> instance containing the event data.</param>
  Private Sub TreeView_Instruments_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles TreeView_Instruments.DragDrop
    'Check that there is a TreeNode being dragged
    If e.Data.GetDataPresent("System.Windows.Forms.TreeNode", _
          True) = False Then Exit Sub

    'Get the TreeView raising the event (incase multiple on form)
    Dim selectedTreeview As TreeView = CType(sender, TreeView)

    'Get the TreeNode being dragged
    Dim dropNode As TreeNode = _
          CType(e.Data.GetData("System.Windows.Forms.TreeNode"), _
          TreeNode)

    'The target node should be selected from the DragOver event
    Dim targetNode As TreeNode = selectedTreeview.SelectedNode

    If (CType(targetNode.Tag, ID_Struct).InstrumentID = CType(dropNode.Tag, ID_Struct).ParentID) Or ((CType(targetNode.Tag, ID_Struct).InstrumentID <= 0) And (CType(dropNode.Tag, ID_Struct).InstrumentID = CType(dropNode.Tag, ID_Struct).ParentID)) Then
      dropNode.ForeColor = Color.Black
    Else
      dropNode.ForeColor = Color.Red
      Call FormControlChanged(Me, New System.EventArgs)
    End If

    'Remove the drop node from its current location
    dropNode.Remove()

    'If there is no targetNode add dropNode to the bottom of
    'the TreeView root nodes, otherwise add it to the end of
    'the dropNode child nodes
    If targetNode Is Nothing Then
      selectedTreeview.Nodes.Add(dropNode)
    Else
      targetNode.Nodes.Add(dropNode)
    End If

    'Ensure the newley created node is visible to
    'the user and select it
    dropNode.EnsureVisible()
    selectedTreeview.SelectedNode = dropNode

  End Sub

    ''' <summary>
    ''' Handles the Click event of the Button_MoveUp control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Button_MoveUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_MoveUp.Click
    Dim dropnode As TreeNode
    Dim targetNode As TreeNode
    Dim selectedTreeview As TreeView = TreeView_Instruments

    ' Exit if no node is selected
    If Me.TreeView_Instruments.SelectedNode Is Nothing Then
      Exit Sub
    End If
    dropnode = Me.TreeView_Instruments.SelectedNode

    ' Exit if this is the root node
    If (dropnode.Parent Is Nothing) Then
      Exit Sub
    End If
    targetNode = dropnode.Parent
    ' Exit if This node's parent is the root node
    If (CType(targetNode.Tag, ID_Struct).InstrumentID <= 0) Then
      Exit Sub
    End If
    If (targetNode.Parent Is Nothing) Then
      Exit Sub
    End If
    targetNode = targetNode.Parent

    If (CType(targetNode.Tag, ID_Struct).InstrumentID = CType(dropnode.Tag, ID_Struct).ParentID) Or ((CType(targetNode.Tag, ID_Struct).InstrumentID <= 0) And (CType(dropnode.Tag, ID_Struct).InstrumentID = CType(dropnode.Tag, ID_Struct).ParentID)) Then
      dropnode.ForeColor = Color.Black
    Else
      dropnode.ForeColor = Color.Red
      Call FormControlChanged(Me, New System.EventArgs)
    End If


    'Remove the drop node from its current location
    dropnode.Remove()

    'If there is no targetNode add dropNode to the bottom of
    'the TreeView root nodes, otherwise add it to the end of
    'the dropNode child nodes
    If targetNode Is Nothing Then
      selectedTreeview.Nodes.Add(dropnode)
    Else
      targetNode.Nodes.Add(dropnode)
    End If

    'Ensure the newley created node is visible to
    'the user and select it
    dropnode.EnsureVisible()
    selectedTreeview.SelectedNode = dropnode


  End Sub

    ''' <summary>
    ''' Sets the form data.
    ''' </summary>
    ''' <param name="pConfirm">if set to <c>true</c> [p confirm].</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function SetFormData(Optional ByVal pConfirm As Boolean = True) As Boolean
    ' *************************************************************
    '
    ' *************************************************************
    Dim ProtectedItem As Boolean = False

    ' *************************************************************
    ' Appropriate Save permission :-
    ' *************************************************************

    If (Me.HasUpdatePermission = False) And ((Me.HasInsertPermission = False) Or (Me.AddNewRecord = False)) Then
      MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "You do not have permission to save this record.", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Function
    End If

    ' *************************************************************
    ' If Save button is disabled then should not be able to save, exit silently.
    ' *************************************************************
    If Me.btnSave.Enabled = False Then
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Function
    End If

    ' *************************************************************
    ' KnowledgeDate OK :-
    ' *************************************************************

    If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
      MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Changes to Venice are not allowed when the Knowledgedate is not set to `NOW`", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Function
    End If

    ' Confirm Save, if required.

    If (pConfirm = True) Then
      If MessageBox.Show("Save Changes ?", "Save Changes ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
        Return True
        Exit Function
      End If
    End If

    ' *************************************************************
    ' Procedure to Save the current form information.
    ' *************************************************************

    Dim UpdateRows(0) As DataRow

    If (FormChanged = False) Or (FormIsValid = False) Then
      Return False
      Exit Function
    End If


    Try
      ' Set 'Paint' flag.
      InPaint = True

      ' *************************************************************
      ' Lock the Data Table, to prevent update conflicts.
      ' *************************************************************
      SyncLock myTable

        ' Initiate Edit,
        Call SaveChangedNodes(Me.TreeView_Instruments.TopNode)

      End SyncLock

    Catch ex As Exception
    Finally
      InPaint = False
    End Try

    ' Finish off

    AddNewRecord = False
    FormChanged = False
    Me.btnCancel.Enabled = False
    Me.btnSave.Enabled = False

    ' Propogate changes

		Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID), True)

  End Function

    ''' <summary>
    ''' Saves the changed nodes.
    ''' </summary>
    ''' <param name="pParentNode">The p parent node.</param>
  Private Sub SaveChangedNodes(ByRef pParentNode As TreeNode)
    Dim ChildNode As TreeNode
    Dim AuditID As Integer
    Dim InstrumentID As Integer
    Dim CurrentParentID As Integer
    Dim NewParentID As Integer
    Dim UpdateRows(0) As DataRow
    Dim ErrMessage As String
    Dim ErrFlag As Boolean
    Dim ErrStack As String

    For Each ChildNode In pParentNode.Nodes
      Call SaveChangedNodes(ChildNode)

      Try
        ' Get Information on this node and it's parent

        AuditID = CType(ChildNode.Tag, ID_Struct).AuditID
        InstrumentID = CType(ChildNode.Tag, ID_Struct).InstrumentID
        CurrentParentID = CType(ChildNode.Tag, ID_Struct).ParentID
        NewParentID = CType(pParentNode.Tag, ID_Struct).InstrumentID

        thisDataRow = Me.SortedRows(Me.Get_Position(AuditID))

        ' Resolve if it has changed, If not then don't save it - (AuditID = (-1))

        If ((NewParentID <= 0) And (InstrumentID = CurrentParentID)) Or _
            (NewParentID = CurrentParentID) Then
          AuditID = (-1)
          thisDataRow = Nothing
        ElseIf (NewParentID <= 0) Then
          NewParentID = InstrumentID
        End If

      Catch ex As Exception
        AuditID = (-1)
        thisDataRow = Nothing
      End Try

      ' If this Instrument has changed, then save changes.
      If (AuditID > 0) And (Not (thisDataRow Is Nothing)) Then

        thisDataRow.BeginEdit()

        thisDataRow.InstrumentParent = NewParentID
        thisDataRow.EndEdit()
        InPaint = False

        ' Add and Update DataRow. 

        UpdateRows(0) = thisDataRow

        ' Post Additions / Updates to the underlying table :-
        Dim temp As Integer
        Try
          ErrFlag = False

          myAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
          myAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

          temp = MainForm.AdaptorUpdate(Me.Name, myAdaptor, UpdateRows)
          CType(ChildNode.Tag, ID_Struct).ParentID = NewParentID
          ChildNode.ForeColor = Color.Black

        Catch ex As Exception

          ErrMessage = ex.Message
          ErrFlag = True
          ErrStack = ex.StackTrace

          ' *************************************************************
          ' Error report. Not done in the Try-Catch as that would serve to persist the SyncLock.
          ' *************************************************************

          If (ErrFlag = True) Then
            Call MainForm.LogError(Me.Name & ", SetFormData", 0, ErrMessage, "Error Saving Data", ErrStack, True)
          End If

        End Try

      Else
        ChildNode.ForeColor = Color.Black
      End If

    Next


  End Sub


    ''' <summary>
    ''' Sets the button status.
    ''' </summary>
  Private Sub SetButtonStatus()
    ' Sets the status of the form controlls appropriate to the current users 
    'permissions and the 'Changed' or 'New' status of the form.

    ' No Read permission :-

    If Me.HasReadPermission = False Then
      MainForm.LogError(Me.Name & ", SetButtonStatus()", 0, "", "You do not have Read permission for this Form", "", True)
      Me.Close()
      Exit Sub
    End If

    If (Me.MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And _
      ((Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord)) Then

      Me.TreeView_Instruments.Enabled = True

    Else

      Me.TreeView_Instruments.Enabled = False

    End If

  End Sub

    ''' <summary>
    ''' Validates the form.
    ''' </summary>
    ''' <param name="pReturnString">The p return string.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function ValidateForm(ByRef pReturnString As String) As Boolean
    ' Form Validation code.
    ' 
    ' This code should be the final arbiter of what is allowed. no assumptions regarding 
    ' prior validation should be made.
    ' 
    ' This Code is called by the SetFormData routine before position changes.
    '
    Dim RVal As Boolean

    RVal = True
    pReturnString = ""

    ' Validate 

    Return RVal

  End Function

    ''' <summary>
    ''' Handles the MouseEnter event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnCancel_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseEnter
    ' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
    Me.IsOverCancelButton = True
  End Sub

    ''' <summary>
    ''' Handles the MouseLeave event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnCancel_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseLeave
    ' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
    Me.IsOverCancelButton = False
  End Sub

#End Region

#Region " Navigation Code / GetPosition() (Generic Code) "


    ''' <summary>
    ''' Get_s the position.
    ''' </summary>
    ''' <param name="pAuditID">The p audit ID.</param>
    ''' <returns>System.Int32.</returns>
  Private Function Get_Position(ByVal pAuditID As Integer) As Integer
    ' Returns the position in the current 'SortedRows' array of the entry corresponding to the given 
    ' AudidID.

    Dim MinIndex As Integer
    Dim MaxIndex As Integer
    Dim CurrentMin As Integer
    Dim CurrentMax As Integer
    Dim searchDataRow As DataRow

    Try
      ' SortedRows exists ?

      If SortedRows Is Nothing Then
        Return (-1)
        Exit Function
      End If

      ' Return (-1) if there are no rows in the 'SortedRows'

      If (SortedRows.GetLength(0) <= 0) Then
        Return (-1)
        Exit Function
      End If


      ' Use a modified Search moving outwards from the last returned value to locate the required value.
      ' Reflecting the fact that for the most part One looks for closely located records.

      MinIndex = 0
      MaxIndex = SortedRows.GetLength(0) - 1


      ' Check First and Last records (Incase 'First' or 'Last' was pressed).

      searchDataRow = SortedRows(MinIndex)

      If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(searchDataRow("AuditID")) = pAuditID) Then
        Return MinIndex
        Exit Function
      End If

      searchDataRow = SortedRows(MaxIndex)
      If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(searchDataRow("AuditID")) = pAuditID) Then
        Return MaxIndex
        Exit Function
      End If

      ' now search outwards from the last returned value.

      If (Me.thisPosition >= MinIndex) And (Me.thisPosition <= MaxIndex) Then
        CurrentMin = thisPosition
        CurrentMax = CurrentMin + 1
      Else
        CurrentMin = CInt((MinIndex + MaxIndex) / 2)
        CurrentMax = CurrentMin + 1
      End If

      While (CurrentMin >= MinIndex) Or (CurrentMax <= MaxIndex)
        If (CurrentMin >= MinIndex) Then
          searchDataRow = SortedRows(CurrentMin)

          If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(searchDataRow("AuditID")) = pAuditID) Then
            Return CurrentMin
            Exit Function
          End If

          CurrentMin -= 1
        End If

        If (CurrentMax <= MaxIndex) Then
          searchDataRow = SortedRows(CurrentMax)

          If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(searchDataRow("AuditID")) = pAuditID) Then
            Return CurrentMax
            Exit Function
          End If

          CurrentMax += 1
        End If

      End While

      Return (-1)
      Exit Function

    Catch ex As Exception
      MainForm.LogError(Me.Name & ", Get_Position()", LOG_LEVELS.Warning, ex.Message, "", ex.StackTrace, True)
      Return (-1)
      Exit Function
    End Try

  End Function

#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

    ''' <summary>
    ''' Handles the Click event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    ' Cancel Changes, redisplay form.

    FormChanged = False
    AddNewRecord = False

    GetInitialData()

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnSave control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
    ' Save Changes, if any, without prompting.

    If (FormChanged = True) Then
      Call SetFormData(False)
    End If

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnClose control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
    ' Close Form

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    Me.Close()

  End Sub

#End Region

#Region " Bug hunting "

  ' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
  ' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
  ' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

    ''' <summary>
    ''' Called when [row updating].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

    ''' <summary>
    ''' Called when [row updated].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

    ''' <summary>
    ''' Called when [row fill error].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
    Dim a As Integer

    a = 1
  End Sub

#End Region



    ''' <summary>
    ''' Handles the AfterSelect event of the TreeView_Instruments control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.TreeViewEventArgs"/> instance containing the event data.</param>
  Private Sub TreeView_Instruments_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView_Instruments.AfterSelect

  End Sub

    ''' <summary>
    ''' Handles the Click event of the Button1 control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

  End Sub
End Class
