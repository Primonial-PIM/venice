﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCashLadder
  Inherits System.Windows.Forms.Form

  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents Combo_TransactionFund As System.Windows.Forms.ComboBox
  Friend WithEvents label_CptyIsFund As System.Windows.Forms.Label
  Friend WithEvents Combo_SubFund As System.Windows.Forms.ComboBox
  Friend WithEvents Label2 As System.Windows.Forms.Label
  Friend WithEvents Grid_CashLadder As C1.Win.C1FlexGrid.C1FlexGrid
  Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
  Friend WithEvents Form_StatusStrip As System.Windows.Forms.StatusStrip
  Friend WithEvents Form_ProgressBar As System.Windows.Forms.ToolStripProgressBar
  Friend WithEvents Label_Status As System.Windows.Forms.ToolStripStatusLabel
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCashLadder))
    Me.Combo_TransactionFund = New System.Windows.Forms.ComboBox
    Me.label_CptyIsFund = New System.Windows.Forms.Label
    Me.Combo_SubFund = New System.Windows.Forms.ComboBox
    Me.Label2 = New System.Windows.Forms.Label
    Me.Grid_CashLadder = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.RootMenu = New System.Windows.Forms.MenuStrip
    Me.Form_StatusStrip = New System.Windows.Forms.StatusStrip
    Me.Form_ProgressBar = New System.Windows.Forms.ToolStripProgressBar
    Me.Label_Status = New System.Windows.Forms.ToolStripStatusLabel
    Me.Date_StartDate = New System.Windows.Forms.DateTimePicker
    Me.Label_TradeDate = New System.Windows.Forms.Label
    Me.Date_EndDate = New System.Windows.Forms.DateTimePicker
    Me.Label1 = New System.Windows.Forms.Label
    Me.Check_UseSubFunds = New System.Windows.Forms.CheckBox
    Me.Combo_DisplayPeriod = New System.Windows.Forms.ComboBox
    Me.Label3 = New System.Windows.Forms.Label
    Me.Combo_CashView = New System.Windows.Forms.ComboBox
    Me.Label4 = New System.Windows.Forms.Label
    Me.Check_ShowRunningTotals = New System.Windows.Forms.CheckBox
    Me.Button_ForceTransactionsUpdate = New System.Windows.Forms.Button
    CType(Me.Grid_CashLadder, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Form_StatusStrip.SuspendLayout()
    Me.SuspendLayout()
    '
    'Combo_TransactionFund
    '
    Me.Combo_TransactionFund.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_TransactionFund.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_TransactionFund.Location = New System.Drawing.Point(128, 32)
    Me.Combo_TransactionFund.Name = "Combo_TransactionFund"
    Me.Combo_TransactionFund.Size = New System.Drawing.Size(1048, 21)
    Me.Combo_TransactionFund.TabIndex = 0
    '
    'label_CptyIsFund
    '
    Me.label_CptyIsFund.Location = New System.Drawing.Point(16, 36)
    Me.label_CptyIsFund.Name = "label_CptyIsFund"
    Me.label_CptyIsFund.Size = New System.Drawing.Size(104, 16)
    Me.label_CptyIsFund.TabIndex = 82
    Me.label_CptyIsFund.Text = "Fund"
    '
    'Combo_SubFund
    '
    Me.Combo_SubFund.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_SubFund.Enabled = False
    Me.Combo_SubFund.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SubFund.Location = New System.Drawing.Point(128, 60)
    Me.Combo_SubFund.Name = "Combo_SubFund"
    Me.Combo_SubFund.Size = New System.Drawing.Size(1048, 21)
    Me.Combo_SubFund.TabIndex = 1
    '
    'Label2
    '
    Me.Label2.Location = New System.Drawing.Point(16, 64)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(104, 16)
    Me.Label2.TabIndex = 84
    Me.Label2.Text = "SubFund"
    '
    'Grid_CashLadder
    '
    Me.Grid_CashLadder.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None
    Me.Grid_CashLadder.AllowEditing = False
    Me.Grid_CashLadder.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_CashLadder.AutoGenerateColumns = False
    Me.Grid_CashLadder.ColumnInfo = "0,0,0,0,0,85,Columns:"
    Me.Grid_CashLadder.Cursor = System.Windows.Forms.Cursors.Default
    Me.Grid_CashLadder.DropMode = C1.Win.C1FlexGrid.DropModeEnum.Manual
    Me.Grid_CashLadder.Location = New System.Drawing.Point(5, 159)
    Me.Grid_CashLadder.Name = "Grid_CashLadder"
    Me.Grid_CashLadder.Rows.Count = 1
    Me.Grid_CashLadder.Rows.DefaultSize = 17
    Me.Grid_CashLadder.Size = New System.Drawing.Size(1174, 730)
    Me.Grid_CashLadder.StyleInfo = resources.GetString("Grid_CashLadder.StyleInfo")
    Me.Grid_CashLadder.TabIndex = 104
    Me.Grid_CashLadder.Tree.Column = 0
    Me.Grid_CashLadder.Tree.Style = CType(((C1.Win.C1FlexGrid.TreeStyleFlags.Lines Or C1.Win.C1FlexGrid.TreeStyleFlags.Symbols) _
                Or C1.Win.C1FlexGrid.TreeStyleFlags.ButtonBar), C1.Win.C1FlexGrid.TreeStyleFlags)
    '
    'RootMenu
    '
    Me.RootMenu.Location = New System.Drawing.Point(0, 0)
    Me.RootMenu.Name = "RootMenu"
    Me.RootMenu.Size = New System.Drawing.Size(1184, 24)
    Me.RootMenu.TabIndex = 107
    Me.RootMenu.Text = " "
    '
    'Form_StatusStrip
    '
    Me.Form_StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Form_ProgressBar, Me.Label_Status})
    Me.Form_StatusStrip.Location = New System.Drawing.Point(0, 896)
    Me.Form_StatusStrip.Name = "Form_StatusStrip"
    Me.Form_StatusStrip.Size = New System.Drawing.Size(1184, 22)
    Me.Form_StatusStrip.TabIndex = 108
    Me.Form_StatusStrip.Text = " "
    '
    'Form_ProgressBar
    '
    Me.Form_ProgressBar.Maximum = 20
    Me.Form_ProgressBar.Name = "Form_ProgressBar"
    Me.Form_ProgressBar.Size = New System.Drawing.Size(150, 17)
    Me.Form_ProgressBar.Step = 1
    Me.Form_ProgressBar.Visible = False
    '
    'Label_Status
    '
    Me.Label_Status.Name = "Label_Status"
    Me.Label_Status.Size = New System.Drawing.Size(10, 17)
    Me.Label_Status.Text = " "
    '
    'Date_StartDate
    '
    Me.Date_StartDate.CustomFormat = "dd MMMM yyyy"
    Me.Date_StartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
    Me.Date_StartDate.Location = New System.Drawing.Point(128, 89)
    Me.Date_StartDate.Name = "Date_StartDate"
    Me.Date_StartDate.Size = New System.Drawing.Size(184, 20)
    Me.Date_StartDate.TabIndex = 109
    '
    'Label_TradeDate
    '
    Me.Label_TradeDate.Location = New System.Drawing.Point(16, 93)
    Me.Label_TradeDate.Name = "Label_TradeDate"
    Me.Label_TradeDate.Size = New System.Drawing.Size(100, 16)
    Me.Label_TradeDate.TabIndex = 110
    Me.Label_TradeDate.Text = "Start Date"
    '
    'Date_EndDate
    '
    Me.Date_EndDate.CustomFormat = "dd MMMM yyyy"
    Me.Date_EndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
    Me.Date_EndDate.Location = New System.Drawing.Point(467, 87)
    Me.Date_EndDate.Name = "Date_EndDate"
    Me.Date_EndDate.Size = New System.Drawing.Size(184, 20)
    Me.Date_EndDate.TabIndex = 111
    '
    'Label1
    '
    Me.Label1.Location = New System.Drawing.Point(355, 91)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(100, 16)
    Me.Label1.TabIndex = 112
    Me.Label1.Text = "End Date"
    '
    'Check_UseSubFunds
    '
    Me.Check_UseSubFunds.AutoSize = True
    Me.Check_UseSubFunds.Location = New System.Drawing.Point(107, 64)
    Me.Check_UseSubFunds.Name = "Check_UseSubFunds"
    Me.Check_UseSubFunds.Size = New System.Drawing.Size(15, 14)
    Me.Check_UseSubFunds.TabIndex = 113
    Me.Check_UseSubFunds.UseVisualStyleBackColor = True
    '
    'Combo_DisplayPeriod
    '
    Me.Combo_DisplayPeriod.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_DisplayPeriod.Location = New System.Drawing.Point(128, 117)
    Me.Combo_DisplayPeriod.Name = "Combo_DisplayPeriod"
    Me.Combo_DisplayPeriod.Size = New System.Drawing.Size(184, 21)
    Me.Combo_DisplayPeriod.TabIndex = 114
    '
    'Label3
    '
    Me.Label3.Location = New System.Drawing.Point(16, 121)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(104, 16)
    Me.Label3.TabIndex = 115
    Me.Label3.Text = "By Period"
    '
    'Combo_CashView
    '
    Me.Combo_CashView.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_CashView.Items.AddRange(New Object() {"Cash by Settlement Date", "Cash by Trade NAV Date"})
    Me.Combo_CashView.Location = New System.Drawing.Point(467, 117)
    Me.Combo_CashView.Name = "Combo_CashView"
    Me.Combo_CashView.Size = New System.Drawing.Size(184, 21)
    Me.Combo_CashView.TabIndex = 116
    '
    'Label4
    '
    Me.Label4.Location = New System.Drawing.Point(355, 121)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(104, 16)
    Me.Label4.TabIndex = 117
    Me.Label4.Text = "Cash View"
    '
    'Check_ShowRunningTotals
    '
    Me.Check_ShowRunningTotals.AutoSize = True
    Me.Check_ShowRunningTotals.Location = New System.Drawing.Point(694, 120)
    Me.Check_ShowRunningTotals.Name = "Check_ShowRunningTotals"
    Me.Check_ShowRunningTotals.Size = New System.Drawing.Size(131, 17)
    Me.Check_ShowRunningTotals.TabIndex = 119
    Me.Check_ShowRunningTotals.Text = "Show Running Totals."
    Me.Check_ShowRunningTotals.UseVisualStyleBackColor = True
    '
    'Button_ForceTransactionsUpdate
    '
    Me.Button_ForceTransactionsUpdate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Button_ForceTransactionsUpdate.Location = New System.Drawing.Point(958, 87)
    Me.Button_ForceTransactionsUpdate.Name = "Button_ForceTransactionsUpdate"
    Me.Button_ForceTransactionsUpdate.Size = New System.Drawing.Size(214, 31)
    Me.Button_ForceTransactionsUpdate.TabIndex = 120
    Me.Button_ForceTransactionsUpdate.Text = "Full Data Update"
    Me.Button_ForceTransactionsUpdate.UseVisualStyleBackColor = True
    '
    'frmCashLadder
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(1184, 918)
    Me.Controls.Add(Me.Button_ForceTransactionsUpdate)
    Me.Controls.Add(Me.Check_ShowRunningTotals)
    Me.Controls.Add(Me.Combo_CashView)
    Me.Controls.Add(Me.Label4)
    Me.Controls.Add(Me.Combo_DisplayPeriod)
    Me.Controls.Add(Me.Label3)
    Me.Controls.Add(Me.Check_UseSubFunds)
    Me.Controls.Add(Me.Date_EndDate)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.Date_StartDate)
    Me.Controls.Add(Me.Label_TradeDate)
    Me.Controls.Add(Me.Grid_CashLadder)
    Me.Controls.Add(Me.Form_StatusStrip)
    Me.Controls.Add(Me.RootMenu)
    Me.Controls.Add(Me.Combo_SubFund)
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.Combo_TransactionFund)
    Me.Controls.Add(Me.label_CptyIsFund)
    Me.MinimumSize = New System.Drawing.Size(900, 0)
    Me.Name = "frmCashLadder"
    Me.Text = "Fund Cash Ladder"
    CType(Me.Grid_CashLadder, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Form_StatusStrip.ResumeLayout(False)
    Me.Form_StatusStrip.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents Date_StartDate As System.Windows.Forms.DateTimePicker
  Friend WithEvents Label_TradeDate As System.Windows.Forms.Label
  Friend WithEvents Date_EndDate As System.Windows.Forms.DateTimePicker
  Friend WithEvents Label1 As System.Windows.Forms.Label
  Friend WithEvents Check_UseSubFunds As System.Windows.Forms.CheckBox
  Friend WithEvents Combo_DisplayPeriod As System.Windows.Forms.ComboBox
  Friend WithEvents Label3 As System.Windows.Forms.Label
  Friend WithEvents Combo_CashView As System.Windows.Forms.ComboBox
  Friend WithEvents Label4 As System.Windows.Forms.Label
  Friend WithEvents Check_ShowRunningTotals As System.Windows.Forms.CheckBox
  Friend WithEvents Button_ForceTransactionsUpdate As System.Windows.Forms.Button
End Class
