' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmLimits.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals

Imports RenaissanceDataClass
Imports C1.Win.C1FlexGrid

''' <summary>
''' Class frmLimits
''' </summary>
Public Class frmLimits

	Inherits System.Windows.Forms.Form
	Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmLimits"/> class from being created.
    ''' </summary>
	Private Sub New()
		MyBase.New()

		'This call is required by the Windows Form Designer.
		InitializeComponent()

		'Add any initialization after the InitializeComponent() call

	End Sub

	'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
    ''' <summary>
    ''' The combo_ limit fund
    ''' </summary>
	Friend WithEvents Combo_LimitFund As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label_ cpty is fund
    ''' </summary>
	Friend WithEvents label_CptyIsFund As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ limit type
    ''' </summary>
	Friend WithEvents Combo_LimitType As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label2
    ''' </summary>
	Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The grid_ limits
    ''' </summary>
	Friend WithEvents Grid_Limits As C1.Win.C1FlexGrid.C1FlexGrid
    ''' <summary>
    ''' The combo_ field select1
    ''' </summary>
	Friend WithEvents Combo_FieldSelect1 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select1_ operator
    ''' </summary>
	Friend WithEvents Combo_Select1_Operator As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select1_ value
    ''' </summary>
	Friend WithEvents Combo_Select1_Value As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select2_ value
    ''' </summary>
	Friend WithEvents Combo_Select2_Value As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ field select2
    ''' </summary>
	Friend WithEvents Combo_FieldSelect2 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select2_ operator
    ''' </summary>
	Friend WithEvents Combo_Select2_Operator As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select3_ value
    ''' </summary>
	Friend WithEvents Combo_Select3_Value As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ field select3
    ''' </summary>
	Friend WithEvents Combo_FieldSelect3 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select3_ operator
    ''' </summary>
	Friend WithEvents Combo_Select3_Operator As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ and or_1
    ''' </summary>
	Friend WithEvents Combo_AndOr_1 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ and or_2
    ''' </summary>
	Friend WithEvents Combo_AndOr_2 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The status bar_ select limit
    ''' </summary>
	Friend WithEvents StatusBar_SelectLimit As System.Windows.Forms.StatusBar
    ''' <summary>
    ''' The BTN close
    ''' </summary>
	Friend WithEvents btnClose As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN save
    ''' </summary>
	Friend WithEvents btnSave As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN cancel
    ''' </summary>
	Friend WithEvents btnCancel As System.Windows.Forms.Button
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLimits))
		Me.Grid_Limits = New C1.Win.C1FlexGrid.C1FlexGrid
		Me.Combo_LimitFund = New System.Windows.Forms.ComboBox
		Me.label_CptyIsFund = New System.Windows.Forms.Label
		Me.Combo_LimitType = New System.Windows.Forms.ComboBox
		Me.Label2 = New System.Windows.Forms.Label
		Me.Combo_Select1_Operator = New System.Windows.Forms.ComboBox
		Me.Combo_FieldSelect1 = New System.Windows.Forms.ComboBox
		Me.Combo_Select1_Value = New System.Windows.Forms.ComboBox
		Me.Combo_Select2_Value = New System.Windows.Forms.ComboBox
		Me.Combo_FieldSelect2 = New System.Windows.Forms.ComboBox
		Me.Combo_Select2_Operator = New System.Windows.Forms.ComboBox
		Me.Combo_Select3_Value = New System.Windows.Forms.ComboBox
		Me.Combo_FieldSelect3 = New System.Windows.Forms.ComboBox
		Me.Combo_Select3_Operator = New System.Windows.Forms.ComboBox
		Me.Combo_AndOr_1 = New System.Windows.Forms.ComboBox
		Me.Combo_AndOr_2 = New System.Windows.Forms.ComboBox
		Me.StatusBar_SelectLimit = New System.Windows.Forms.StatusBar
		Me.btnClose = New System.Windows.Forms.Button
		Me.btnSave = New System.Windows.Forms.Button
		Me.btnCancel = New System.Windows.Forms.Button
		CType(Me.Grid_Limits, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.SuspendLayout()
		'
		'Grid_Limits
		'
		Me.Grid_Limits.AllowAddNew = True
		Me.Grid_Limits.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None
		Me.Grid_Limits.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
								Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Grid_Limits.AutoClipboard = True
		Me.Grid_Limits.AutoGenerateColumns = False
		Me.Grid_Limits.CausesValidation = False
		Me.Grid_Limits.ColumnInfo = resources.GetString("Grid_Limits.ColumnInfo")
		Me.Grid_Limits.EditOptions = C1.Win.C1FlexGrid.EditFlags.None
		Me.Grid_Limits.KeyActionTab = C1.Win.C1FlexGrid.KeyActionEnum.MoveAcross
		Me.Grid_Limits.Location = New System.Drawing.Point(4, 168)
		Me.Grid_Limits.Name = "Grid_Limits"
		Me.Grid_Limits.Rows.DefaultSize = 17
		Me.Grid_Limits.Size = New System.Drawing.Size(1000, 484)
		Me.Grid_Limits.TabIndex = 13

        'Me.Grid_Limits.BeforeEdit = (s1, e1) =>
        '{
        '    var flex = s1 as C1.Win.C1FlexGrid.C1FlexGrid;
        '    if (e1.Row == 1 && e1.Col == 1)
        '    e1.Cancel = true;
        '};


        '
        'Combo_LimitFund
        '
        Me.Combo_LimitFund.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
              Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Combo_LimitFund.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Combo_LimitFund.Location = New System.Drawing.Point(128, 12)
        Me.Combo_LimitFund.Name = "Combo_LimitFund"
        Me.Combo_LimitFund.Size = New System.Drawing.Size(872, 21)
        Me.Combo_LimitFund.TabIndex = 0
        '
        'label_CptyIsFund
        '
        Me.label_CptyIsFund.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.label_CptyIsFund.Location = New System.Drawing.Point(16, 16)
        Me.label_CptyIsFund.Name = "label_CptyIsFund"
        Me.label_CptyIsFund.Size = New System.Drawing.Size(104, 16)
        Me.label_CptyIsFund.TabIndex = 82
        Me.label_CptyIsFund.Text = "Fund"
        '
        'Combo_LimitType
        '
        Me.Combo_LimitType.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
              Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Combo_LimitType.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Combo_LimitType.Location = New System.Drawing.Point(128, 40)
        Me.Combo_LimitType.Name = "Combo_LimitType"
        Me.Combo_LimitType.Size = New System.Drawing.Size(872, 21)
        Me.Combo_LimitType.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label2.Location = New System.Drawing.Point(16, 44)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(104, 16)
        Me.Label2.TabIndex = 84
        Me.Label2.Text = "Limit Type"
        '
        'Combo_Select1_Operator
        '
        Me.Combo_Select1_Operator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Combo_Select1_Operator.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Combo_Select1_Operator.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like"})
        Me.Combo_Select1_Operator.Location = New System.Drawing.Point(272, 72)
        Me.Combo_Select1_Operator.Name = "Combo_Select1_Operator"
        Me.Combo_Select1_Operator.Size = New System.Drawing.Size(96, 21)
        Me.Combo_Select1_Operator.TabIndex = 3
        '
        'Combo_FieldSelect1
        '
        Me.Combo_FieldSelect1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Combo_FieldSelect1.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Combo_FieldSelect1.Location = New System.Drawing.Point(128, 72)
        Me.Combo_FieldSelect1.Name = "Combo_FieldSelect1"
        Me.Combo_FieldSelect1.Size = New System.Drawing.Size(136, 21)
        Me.Combo_FieldSelect1.Sorted = True
        Me.Combo_FieldSelect1.TabIndex = 2
        '
        'Combo_Select1_Value
        '
        Me.Combo_Select1_Value.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
              Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Combo_Select1_Value.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Combo_Select1_Value.Location = New System.Drawing.Point(376, 72)
        Me.Combo_Select1_Value.Name = "Combo_Select1_Value"
        Me.Combo_Select1_Value.Size = New System.Drawing.Size(336, 21)
        Me.Combo_Select1_Value.TabIndex = 4
        '
        'Combo_Select2_Value
        '
        Me.Combo_Select2_Value.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
              Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Combo_Select2_Value.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Combo_Select2_Value.Location = New System.Drawing.Point(376, 104)
        Me.Combo_Select2_Value.Name = "Combo_Select2_Value"
        Me.Combo_Select2_Value.Size = New System.Drawing.Size(336, 21)
        Me.Combo_Select2_Value.TabIndex = 8
        '
        'Combo_FieldSelect2
        '
        Me.Combo_FieldSelect2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Combo_FieldSelect2.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Combo_FieldSelect2.Location = New System.Drawing.Point(128, 104)
        Me.Combo_FieldSelect2.Name = "Combo_FieldSelect2"
        Me.Combo_FieldSelect2.Size = New System.Drawing.Size(136, 21)
        Me.Combo_FieldSelect2.Sorted = True
        Me.Combo_FieldSelect2.TabIndex = 6
        '
        'Combo_Select2_Operator
        '
        Me.Combo_Select2_Operator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Combo_Select2_Operator.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Combo_Select2_Operator.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like"})
        Me.Combo_Select2_Operator.Location = New System.Drawing.Point(272, 104)
        Me.Combo_Select2_Operator.Name = "Combo_Select2_Operator"
        Me.Combo_Select2_Operator.Size = New System.Drawing.Size(96, 21)
        Me.Combo_Select2_Operator.TabIndex = 7
        '
        'Combo_Select3_Value
        '
        Me.Combo_Select3_Value.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
              Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Combo_Select3_Value.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Combo_Select3_Value.Location = New System.Drawing.Point(376, 136)
        Me.Combo_Select3_Value.Name = "Combo_Select3_Value"
        Me.Combo_Select3_Value.Size = New System.Drawing.Size(336, 21)
        Me.Combo_Select3_Value.TabIndex = 12
        '
        'Combo_FieldSelect3
        '
        Me.Combo_FieldSelect3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Combo_FieldSelect3.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Combo_FieldSelect3.Location = New System.Drawing.Point(128, 136)
        Me.Combo_FieldSelect3.Name = "Combo_FieldSelect3"
        Me.Combo_FieldSelect3.Size = New System.Drawing.Size(136, 21)
        Me.Combo_FieldSelect3.Sorted = True
        Me.Combo_FieldSelect3.TabIndex = 10
        '
        'Combo_Select3_Operator
        '
        Me.Combo_Select3_Operator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Combo_Select3_Operator.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Combo_Select3_Operator.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like"})
        Me.Combo_Select3_Operator.Location = New System.Drawing.Point(272, 136)
        Me.Combo_Select3_Operator.Name = "Combo_Select3_Operator"
        Me.Combo_Select3_Operator.Size = New System.Drawing.Size(96, 21)
        Me.Combo_Select3_Operator.TabIndex = 11
        '
        'Combo_AndOr_1
        '
        Me.Combo_AndOr_1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Combo_AndOr_1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Combo_AndOr_1.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Combo_AndOr_1.Items.AddRange(New Object() {"AND", "OR"})
        Me.Combo_AndOr_1.Location = New System.Drawing.Point(724, 72)
        Me.Combo_AndOr_1.Name = "Combo_AndOr_1"
        Me.Combo_AndOr_1.Size = New System.Drawing.Size(96, 21)
        Me.Combo_AndOr_1.TabIndex = 5
        '
        'Combo_AndOr_2
        '
        Me.Combo_AndOr_2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Combo_AndOr_2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Combo_AndOr_2.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Combo_AndOr_2.Items.AddRange(New Object() {"AND", "OR"})
        Me.Combo_AndOr_2.Location = New System.Drawing.Point(724, 104)
        Me.Combo_AndOr_2.Name = "Combo_AndOr_2"
        Me.Combo_AndOr_2.Size = New System.Drawing.Size(96, 21)
        Me.Combo_AndOr_2.TabIndex = 9
        '
        'StatusBar_SelectLimit
        '
        Me.StatusBar_SelectLimit.Location = New System.Drawing.Point(0, 689)
        Me.StatusBar_SelectLimit.Name = "StatusBar_SelectLimit"
        Me.StatusBar_SelectLimit.Size = New System.Drawing.Size(1008, 16)
        Me.StatusBar_SelectLimit.TabIndex = 17
        Me.StatusBar_SelectLimit.Text = "."
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnClose.Location = New System.Drawing.Point(184, 660)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 28)
        Me.btnClose.TabIndex = 16
        Me.btnClose.Text = "&Close"
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSave.Enabled = False
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnSave.Location = New System.Drawing.Point(8, 660)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 28)
        Me.btnSave.TabIndex = 14
        Me.btnSave.Text = "&Save"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Enabled = False
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnCancel.Location = New System.Drawing.Point(96, 660)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 28)
        Me.btnCancel.TabIndex = 15
        Me.btnCancel.Text = "&Cancel"
        '
        'frmLimits
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(1008, 705)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.StatusBar_SelectLimit)
        Me.Controls.Add(Me.Combo_AndOr_2)
        Me.Controls.Add(Me.Combo_AndOr_1)
        Me.Controls.Add(Me.Combo_Select3_Value)
        Me.Controls.Add(Me.Combo_FieldSelect3)
        Me.Controls.Add(Me.Combo_Select3_Operator)
        Me.Controls.Add(Me.Combo_Select2_Value)
        Me.Controls.Add(Me.Combo_FieldSelect2)
        Me.Controls.Add(Me.Combo_Select2_Operator)
        Me.Controls.Add(Me.Combo_Select1_Value)
        Me.Controls.Add(Me.Combo_FieldSelect1)
        Me.Controls.Add(Me.Combo_Select1_Operator)
        Me.Controls.Add(Me.Combo_LimitType)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Combo_LimitFund)
        Me.Controls.Add(Me.label_CptyIsFund)
        Me.Controls.Add(Me.Grid_Limits)
        Me.Name = "frmLimits"
        Me.Text = "Add / Edit Risk Limits"
        CType(Me.Grid_Limits, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region " Form Locals and Constants "

	' Form 'Parent', the Main Venice form.
	' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
	Private WithEvents _MainForm As VeniceMain

	' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
	Private FormTooltip As New ToolTip()

	' Form Menu


	' Form Constants, specific to the table being updated.

    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
	Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

	' Form Locals, initialised on 'New' defining what standard data items to use
    ''' <summary>
  ''' Variable holding the form's data table name. taken from the "ThisStandardDataset" object.
    ''' </summary>
	Private THIS_TABLENAME As String
    ''' <summary>
  ''' Variable holding the form's data adaptor name. taken from the "ThisStandardDataset" object.
    ''' </summary>
	Private THIS_ADAPTORNAME As String
    ''' <summary>
  ''' Variable holding the form's dataset name. taken from the "ThisStandardDataset" object.
    ''' </summary>
	Private THIS_DATASETNAME As String


	' The standard ChangeID for this form. e.g. tblFund
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
	Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

	' Form Specific Order fields
    ''' <summary>
  ''' Field Name to order the Select combo by.
    ''' </summary>
	Private THIS_FORM_OrderBy As String

	' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
	Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
	Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

	' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
	Private THIS_FORM_FormID As VeniceFormID

	' Data Structures

    ''' <summary>
    ''' My dataset
    ''' </summary>
	Private myDataset As DataSet
    ''' <summary>
    ''' My table
    ''' </summary>
	Private myTable As DataTable
    ''' <summary>
    ''' My connection
    ''' </summary>
	Private myConnection As SqlConnection
    ''' <summary>
    ''' My adaptor
    ''' </summary>
	Private myAdaptor As SqlDataAdapter
	' Private myDataView As DataView

    ''' <summary>
    ''' The this standard dataset
    ''' </summary>
	Private ThisStandardDataset As RenaissanceGlobals.StandardDataset

	' Active Element.

    ''' <summary>
    ''' The __ is over cancel button
    ''' </summary>
	Private __IsOverCancelButton As Boolean
    ''' <summary>
    ''' The _ in use
    ''' </summary>
	Private _InUse As Boolean
    ''' <summary>
    ''' The add new record
    ''' </summary>
	Private AddNewRecord As Boolean = False
    ''' <summary>
    ''' The form changed
    ''' </summary>
	Private FormChanged As Boolean = False

	' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
	Private FormIsValid As Boolean
    ''' <summary>
    ''' The in paint
    ''' </summary>
	Private InPaint As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
	Private _FormOpenFailed As Boolean

	' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
	Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
	Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
	Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
	Private HasDeletePermission As Boolean

	' Grid Dictionary objects

	' 'Hashtable' is used in preference to 'LookupCollection' as 
	' The selection is made using a seperate Combo and the Hashtable is quicker in 
	' all other respects. If the Hashtable dictionary was used as the Editor then the 
	' Items are not ordered correctly and a 'LookupCollection' object should be used instead.

    ''' <summary>
    ''' The limits fund dictionary
    ''' </summary>
	Dim LimitsFundDictionary As Hashtable	'  
    ''' <summary>
    ''' The limits limit type dictionary
    ''' </summary>
	Dim LimitsLimitTypeDictionary As Hashtable '  
    ''' <summary>
    ''' The limits limit instrument dictionary
    ''' </summary>
	Dim LimitsLimitInstrumentDictionary As Hashtable '  
    ''' <summary>
    ''' The limits limit currency dictionary
    ''' </summary>
	Dim LimitsLimitCurrencyDictionary As Hashtable '  
    ''' <summary>
    ''' The limits limit sector dictionary
    ''' </summary>
	Dim LimitsLimitSectorDictionary As Hashtable '  
    ''' <summary>
    ''' The limits limit dealing period dictionary
    ''' </summary>
	Dim LimitsLimitDealingPeriodDictionary As Hashtable	'  
    ''' <summary>
    ''' The limits limit gt or less than dictionary
    ''' </summary>
	Dim LimitsLimitGtOrLessThanDictionary As Hashtable '  
    ''' <summary>
    ''' The limits characteristic dictionary
    ''' </summary>
	Dim LimitsCharacteristicDictionary As Hashtable	'  

    ''' <summary>
    ''' The combo_ limits fund dictionary
    ''' </summary>
	Dim WithEvents Combo_LimitsFundDictionary As New CustomC1Combo ' ComboBox
    ''' <summary>
    ''' The combo_ limits limit type dictionary
    ''' </summary>
	Dim WithEvents Combo_LimitsLimitTypeDictionary As New CustomC1Combo	' ComboBox
    ''' <summary>
    ''' The combo_ limits limit instrument dictionary
    ''' </summary>
	Dim WithEvents Combo_LimitsLimitInstrumentDictionary As New CustomC1Combo	' ComboBox
    ''' <summary>
    ''' The combo_ limits limit currency dictionary
    ''' </summary>
	Dim WithEvents Combo_LimitsLimitCurrencyDictionary As New CustomC1Combo	' ComboBox
    ''' <summary>
    ''' The combo_ limits limit sector dictionary
    ''' </summary>
	Dim WithEvents Combo_LimitsLimitSectorDictionary As New CustomC1Combo	' ComboBox
    ''' <summary>
    ''' The combo_ limits limit sector group
    ''' </summary>
	Dim WithEvents Combo_LimitsLimitSectorGroup As New CustomC1Combo ' ComboBox
    ''' <summary>
    ''' The combo_ limits limit dealing period group
    ''' </summary>
	Dim WithEvents Combo_LimitsLimitDealingPeriodGroup As New CustomC1Combo	' ComboBox
    ''' <summary>
    ''' The combo_ limits gt or less than group
    ''' </summary>
	Dim WithEvents Combo_LimitsGtOrLessThanGroup As New CustomC1Combo	' ComboBox
    ''' <summary>
    ''' The combo_ limits characteristic dictionary
    ''' </summary>
	Dim WithEvents Combo_LimitsCharacteristicDictionary As New CustomC1Combo ' ComboBox

    ''' <summary>
    ''' The limits editor
    ''' </summary>
	Dim WithEvents LimitsEditor As New C1.Win.C1Input.C1NumericEdit

	' Grid Column Numbers

    ''' <summary>
    ''' The grid_ fund
    ''' </summary>
	Private Grid_Fund As Integer = 0
    ''' <summary>
    ''' The grid_ type
    ''' </summary>
	Private Grid_Type As Integer = 1
	'Private Grid_Instrument As Integer = 2
	'Private Grid_Sector As Integer = 3
	'Private Grid_SectorGroup As Integer = 4
	'Private Grid_DealingPeriod As Integer = 5
    ''' <summary>
    ''' The grid_ limit item
    ''' </summary>
	Private Grid_LimitItem As Integer = 2
    ''' <summary>
    ''' The grid_ greater or less than
    ''' </summary>
	Private Grid_GreaterOrLessThan As Integer = 3
    ''' <summary>
    ''' The grid_ limit
    ''' </summary>
	Private Grid_Limit As Integer = 4
    ''' <summary>
    ''' The grid_ ID
    ''' </summary>
	Private Grid_ID As Integer = 5
    ''' <summary>
    ''' The grid_ label
    ''' </summary>
	Private Grid_Label As Integer = 6
    ''' <summary>
    ''' The grid_ premium
    ''' </summary>
	Private Grid_Premium As Integer = 7
    ''' <summary>
    ''' The grid_ delete
    ''' </summary>
	Private Grid_Delete As Integer = 8

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
	Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
	Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return __IsOverCancelButton
		End Get
		Set(ByVal Value As Boolean)
			__IsOverCancelButton = Value
		End Set
	End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
	Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
		Get
			Return InPaint
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
		Get
			Return _InUse
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmLimits"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
	Public Sub New(ByVal pMainForm As VeniceMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()


		_MainForm = pMainForm
		AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

		_FormOpenFailed = False
		_InUse = True

		' Default Select and Order fields.

		THIS_FORM_OrderBy = "limFund, limType"

		' Form Permissioning :-

		THIS_FORM_PermissionArea = Me.Name
		THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

		' 'This' form ID

		THIS_FORM_FormID = VeniceFormID.frmLimits

		' This form's dataset type.

		ThisStandardDataset = RenaissanceStandardDatasets.tblLimits	 ' This Defines the Form Data !!! 


		' Data object names standard to this Form type.

		THIS_TABLENAME = ThisStandardDataset.TableName
		THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
		THIS_DATASETNAME = ThisStandardDataset.DatasetName

		THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

		' Establish / Retrieve data objects for this form.

		myConnection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)
		myAdaptor = MainForm.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, VENICE_CONNECTION, THIS_TABLENAME)
		myDataset = MainForm.Load_Table(ThisStandardDataset, False)
		myTable = myDataset.Tables(0)


		' Establish initial DataView and Initialise Limits Grid.

		Dim thisCol As C1.Win.C1FlexGrid.Column

		'myDataView = New DataView(myTable, "True", "", DataViewRowState.CurrentRows)
		'Grid_Limits.DataSource = myDataView

		' Format Limits Grid, hide unwanted columns.
		Try

			Grid_Limits.Cols("limFund").Move(Grid_Fund)
			Grid_Limits.Cols("limType").Move(Grid_Type)
			'Grid_Limits.Cols("limInstrument").Move(Grid_Instrument)
			'Grid_Limits.Cols("limSector").Move(Grid_Sector)
			'Grid_Limits.Cols("limSectorGroup").Move(Grid_SectorGroup)
			'Grid_Limits.Cols("limDealingPeriod").Move(Grid_DealingPeriod)
			Grid_Limits.Cols("limLimitItem").Move(Grid_LimitItem)
			Grid_Limits.Cols("limGreaterOrLessThan").Move(Grid_GreaterOrLessThan)
			Grid_Limits.Cols("limLimit").Move(Grid_Limit)
			Grid_Limits.Cols("limID").Move(Grid_ID)
			Grid_Limits.Cols("colLabel").Move(Grid_Label)
			Grid_Limits.Cols("limPremium").Move(Grid_Premium)
			Grid_Limits.Cols("ColDelete").Move(Grid_Delete)

			For Each thisCol In Grid_Limits.Cols
				Select Case thisCol.Name
					Case "limFund"
						thisCol.Width = 150

					Case "limType"
						thisCol.Width = 150

					Case "limInstrument"
						thisCol.Width = 300

					Case "limLimitItem"
						thisCol.Width = 300

					Case "limSector"
						thisCol.Width = 150

					Case "limSectorGroup"
						thisCol.Width = 150

					Case "limDealingPeriod"
						thisCol.Width = 75

					Case "limGreaterOrLessThan"
						thisCol.Width = 75

					Case "colLabel"
						thisCol.Width = 25

					Case "ColDelete"
						thisCol.Width = 50

					Case "limPremium"
						thisCol.Width = 50

					Case "limLimit"
						thisCol.Width = 75

					Case Else
						thisCol.Visible = False
				End Select

				If (thisCol.Caption.ToUpper.StartsWith("lim")) Then
					thisCol.Caption = thisCol.Caption.Substring(3)
				End If
			Next

		Catch ex As Exception

		End Try

		' Initialise Field select area

		Call BuildFieldSelectCombos()
		Me.Combo_AndOr_1.SelectedIndex = 0
		Me.Combo_AndOr_2.SelectedIndex = 0


		' Form Control Changed events
		AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

		AddHandler Combo_LimitFund.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_LimitType.SelectedValueChanged, AddressOf Me.FormControlChanged

		AddHandler Combo_Select1_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_Select1_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_Select1_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp
		AddHandler Combo_Select2_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_Select2_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_Select2_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp
		AddHandler Combo_Select3_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_Select3_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_Select3_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp

		AddHandler Combo_AndOr_1.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_AndOr_2.SelectedValueChanged, AddressOf Me.FormControlChanged

		AddHandler Combo_LimitFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_LimitType.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_LimitsFundDictionary.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_LimitsLimitTypeDictionary.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_LimitsLimitInstrumentDictionary.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_LimitsLimitCurrencyDictionary.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_LimitsLimitSectorDictionary.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_LimitsLimitSectorGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_LimitsLimitDealingPeriodGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_LimitsGtOrLessThanGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_LimitsCharacteristicDictionary.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		AddHandler Combo_LimitFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_LimitType.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_LimitsFundDictionary.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_LimitsLimitTypeDictionary.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_LimitsLimitInstrumentDictionary.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_LimitsLimitCurrencyDictionary.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_LimitsLimitSectorDictionary.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_LimitsLimitSectorGroup.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_LimitsLimitDealingPeriodGroup.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_LimitsGtOrLessThanGroup.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_LimitsCharacteristicDictionary.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

		AddHandler Combo_LimitFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_LimitType.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_LimitsFundDictionary.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_LimitsLimitTypeDictionary.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_LimitsLimitInstrumentDictionary.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_LimitsLimitCurrencyDictionary.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_LimitsLimitSectorDictionary.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_LimitsLimitSectorGroup.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_LimitsLimitDealingPeriodGroup.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_LimitsGtOrLessThanGroup.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_LimitsCharacteristicDictionary.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

		AddHandler Combo_LimitFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_LimitType.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_LimitsFundDictionary.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_LimitsLimitTypeDictionary.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_LimitsLimitInstrumentDictionary.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_LimitsLimitCurrencyDictionary.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_LimitsLimitSectorDictionary.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_LimitsLimitSectorGroup.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_LimitsLimitDealingPeriodGroup.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_LimitsGtOrLessThanGroup.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_LimitsCharacteristicDictionary.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

		Combo_LimitsFundDictionary.DropDownStyle = ComboBoxStyle.DropDown
		Combo_LimitsLimitTypeDictionary.DropDownStyle = ComboBoxStyle.DropDown
		Combo_LimitsLimitInstrumentDictionary.DropDownStyle = ComboBoxStyle.DropDown
		Combo_LimitsLimitCurrencyDictionary.DropDownStyle = ComboBoxStyle.DropDown
		Combo_LimitsLimitSectorDictionary.DropDownStyle = ComboBoxStyle.DropDown
		Combo_LimitsLimitSectorGroup.DropDownStyle = ComboBoxStyle.DropDown
		Combo_LimitsLimitDealingPeriodGroup.DropDownStyle = ComboBoxStyle.DropDown
		Combo_LimitsGtOrLessThanGroup.DropDownStyle = ComboBoxStyle.DropDown
		Combo_LimitsCharacteristicDictionary.DropDownStyle = ComboBoxStyle.DropDown

		' Configure Grid

		Try

			'Grid_Limits.Item(0, Grid_Fund) = "Fund"
			'Grid_Limits.Item(0, Grid_Type) = "Limit Type"
			''Grid_Limits.Item(0, Grid_Instrument) = "Instrument"
			''Grid_Limits.Item(0, Grid_Sector) = "Sector"
			''Grid_Limits.Item(0, Grid_SectorGroup) = "Sector Group"
			''Grid_Limits.Item(0, Grid_DealingPeriod) = "Dealing Period"
			'Grid_Limits.Item(0, Grid_LimitItem) = "Limit Item / Area"
			'Grid_Limits.Item(0, Grid_GreaterOrLessThan) = "< or >"
			'Grid_Limits.Item(0, Grid_Limit) = "Limit"
			'Grid_Limits.Item(0, Grid_Delete) = "Delete"

			LimitsEditor.DisplayFormat.CustomFormat = "#,##0.0###"
			LimitsEditor.DisplayFormat.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat
			LimitsEditor.DisplayFormat.Inherit = CType((((C1.Win.C1Input.FormatInfoInheritFlags.NullText Or C1.Win.C1Input.FormatInfoInheritFlags.EmptyAsNull) _
						Or C1.Win.C1Input.FormatInfoInheritFlags.TrimStart) _
						Or C1.Win.C1Input.FormatInfoInheritFlags.TrimEnd), C1.Win.C1Input.FormatInfoInheritFlags)
			LimitsEditor.EditFormat.CustomFormat = "##########0.0#######"
			LimitsEditor.EditFormat.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat
			LimitsEditor.EditFormat.Inherit = CType((((C1.Win.C1Input.FormatInfoInheritFlags.NullText Or C1.Win.C1Input.FormatInfoInheritFlags.EmptyAsNull) _
						Or C1.Win.C1Input.FormatInfoInheritFlags.TrimStart) _
						Or C1.Win.C1Input.FormatInfoInheritFlags.TrimEnd), C1.Win.C1Input.FormatInfoInheritFlags)

		Catch ex As Exception

		End Try

		' Grid_Limits.Cols(Grid_Limit).Editor = LimitsEditor

	End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

	' Form Initialisation code.
	'
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
	Public Sub ResetForm() Implements StandardVeniceForm.ResetForm
		THIS_FORM_OrderBy = "limFund, limType"

		Call Form_Load(Me, New System.EventArgs)
	End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
	Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		' *************************************************************
		'
		'
		'
		' *************************************************************

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
		_FormOpenFailed = False
		_InUse = True

		' Initialise Data structures. Connection, Adaptor and Dataset.

		If Not (MainForm Is Nothing) Then
			FormIsValid = True
		Else
			MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myConnection Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myAdaptor Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myDataset Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If


		' Check User permissions
		Try
			Call CheckPermissions()

			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

				FormIsValid = False
				_FormOpenFailed = True

				Exit Sub
			End If

			If (HasDeletePermission = False) Then
				Me.Grid_Limits.Cols(Grid_Delete).AllowEditing = False
			Else
				Me.Grid_Limits.Cols(Grid_Delete).AllowEditing = True
			End If

		Catch ex As Exception
			FormIsValid = False
			_FormOpenFailed = True

			Exit Sub
		End Try

		' Initialse form

		InPaint = True
		IsOverCancelButton = False

		' Initialise main select controls
		' Build Sorted data list from which this form operates

		Try
			Call SetFundCombo()
			Call SelLimitTypeCombo()

			Me.Combo_LimitFund.SelectedIndex = -1
			Me.Combo_LimitType.SelectedIndex = -1

			Me.Combo_FieldSelect1.SelectedIndex = 0
			Me.Combo_FieldSelect2.SelectedIndex = 0
			Me.Combo_FieldSelect3.SelectedIndex = 0
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error initialising Risk Limits Form. (1)", ex.StackTrace, True)
		End Try

		Try
			If Not (Me.HasUpdatePermission) Then
				Grid_Limits.Cols(Grid_Fund).AllowEditing = False
				Grid_Limits.Cols(Grid_Type).AllowEditing = False
				'Grid_Limits.Cols(Grid_Instrument).AllowEditing = False
				'Grid_Limits.Cols(Grid_Sector).AllowEditing = False
				'Grid_Limits.Cols(Grid_SectorGroup).AllowEditing = False
				'Grid_Limits.Cols(Grid_DealingPeriod).AllowEditing = False
				Grid_Limits.Cols(Grid_LimitItem).AllowEditing = False
				Grid_Limits.Cols(Grid_GreaterOrLessThan).AllowEditing = False
				Grid_Limits.Cols(Grid_Limit).AllowEditing = False
				Grid_Limits.Cols(Grid_Premium).AllowEditing = False
			Else
				Grid_Limits.Cols(Grid_Fund).AllowEditing = True
				Grid_Limits.Cols(Grid_Type).AllowEditing = True
				'Grid_Limits.Cols(Grid_Instrument).AllowEditing = True
				'Grid_Limits.Cols(Grid_Sector).AllowEditing = True
				'Grid_Limits.Cols(Grid_SectorGroup).AllowEditing = True
				'Grid_Limits.Cols(Grid_DealingPeriod).AllowEditing = True
				Grid_Limits.Cols(Grid_LimitItem).AllowEditing = True
				Grid_Limits.Cols(Grid_GreaterOrLessThan).AllowEditing = True
				Grid_Limits.Cols(Grid_Limit).AllowEditing = True
				Grid_Limits.Cols(Grid_Premium).AllowEditing = True
			End If

			If Not (Me.HasDeletePermission) Then
				Grid_Limits.Cols(Grid_Delete).AllowEditing = False
			Else
				Grid_Limits.Cols(Grid_Delete).AllowEditing = True
			End If

		Catch ex As Exception
		End Try

		Call SetLimitsFundDictionary()
		Call SetLimitsLimitTypeDictionary()
		Call SetLimitsLimitInstrumentDictionary()
		Call SetLimitsLimitCurrencyDictionary()
		Call SetLimitsLimitSectorDictionary()
		Call SetSectorGroupCombo()
		Call SetLimitsLimitDealingPeriodDictionary()
		Call SetLimitsLimitGtOrLessThanDictionary()
		Call SetLimitsCharacteristicDictionary()

		Dim NewStyle As C1.Win.C1FlexGrid.CellStyle

		NewStyle = Grid_Limits.Styles.Add("Default_LimitFormat", Grid_Limits.GetCellStyle(0, Grid_Limit))

		NewStyle = Grid_Limits.Styles.Add("LimitFund", Grid_Limits.GetCellStyle(0, Grid_Fund))
		NewStyle.DataMap = LimitsFundDictionary
		NewStyle.Editor = Me.Combo_LimitFund

		NewStyle = Grid_Limits.Styles.Add("LimitType", Grid_Limits.GetCellStyle(0, Grid_Type))
		NewStyle.DataMap = LimitsLimitTypeDictionary
		NewStyle.Editor = Me.Combo_LimitsLimitTypeDictionary

		NewStyle = Grid_Limits.Styles.Add("LimitInstrument", Grid_Limits.GetCellStyle(0, Grid_LimitItem))
		NewStyle.DataMap = LimitsLimitInstrumentDictionary
		NewStyle.Editor = Me.Combo_LimitsLimitInstrumentDictionary

		NewStyle = Grid_Limits.Styles.Add("LimitCurrency", Grid_Limits.GetCellStyle(0, Grid_LimitItem))
		NewStyle.DataMap = LimitsLimitCurrencyDictionary
		NewStyle.Editor = Me.Combo_LimitsLimitCurrencyDictionary

		NewStyle = Grid_Limits.Styles.Add("LimitSector", Grid_Limits.GetCellStyle(0, Grid_LimitItem))
		NewStyle.DataMap = LimitsLimitSectorDictionary
		NewStyle.Editor = Me.Combo_LimitsLimitSectorDictionary

		NewStyle = Grid_Limits.Styles.Add("LimitSectorGroup", Grid_Limits.GetCellStyle(0, Grid_LimitItem))
		NewStyle.Editor = Me.Combo_LimitsLimitSectorGroup

		NewStyle = Grid_Limits.Styles.Add("LimitCharacteristic", Grid_Limits.GetCellStyle(0, Grid_LimitItem))
		NewStyle.DataMap = LimitsCharacteristicDictionary
		NewStyle.Editor = Me.Combo_LimitsCharacteristicDictionary

		NewStyle = Grid_Limits.Styles.Add("LimitDealingPeriod", Grid_Limits.GetCellStyle(0, Grid_LimitItem))
		NewStyle.DataMap = LimitsLimitDealingPeriodDictionary
		NewStyle.Editor = Me.Combo_LimitsLimitDealingPeriodGroup

		NewStyle = Grid_Limits.Styles.Add("LimitGtOrLessThan", Grid_Limits.GetCellStyle(0, Grid_GreaterOrLessThan))
		NewStyle.DataMap = LimitsLimitGtOrLessThanDictionary
		NewStyle.Editor = Me.Combo_LimitsGtOrLessThanGroup

		NewStyle = Grid_Limits.Styles.Add("Limit_PercentageFormat", Grid_Limits.GetCellStyle(0, Grid_Limit))
		NewStyle.Format = "#,##0.00%"

		NewStyle = Grid_Limits.Styles.Add("Limit_IntegerFormat", Grid_Limits.GetCellStyle(0, Grid_Limit))
		NewStyle.Format = "#,##0"


		Grid_Limits.Cols(Grid_Fund).Style = Grid_Limits.Styles("LimitFund")
		Grid_Limits.Cols(Grid_Type).Style = Grid_Limits.Styles("LimitType")
		'Grid_Limits.Cols(Grid_Instrument).Style = Grid_Limits.Styles("LimitInstrument")
		'Grid_Limits.Cols(Grid_Sector).Style = Grid_Limits.Styles("LimitSector")
		'Grid_Limits.Cols(Grid_SectorGroup).Style = Grid_Limits.Styles("LimitSectorGroup")
		'Grid_Limits.Cols(Grid_DealingPeriod).Style = Grid_Limits.Styles("LimitDealingPeriod")
		Grid_Limits.Cols(Grid_LimitItem).Style = Grid_Limits.Styles("Normal")
		Grid_Limits.Cols(Grid_GreaterOrLessThan).Style = Grid_Limits.Styles("LimitGtOrLessThan")

		Try
			Call GetSortedRows()

			Call MainForm.SetComboSelectionLengths(Me)
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error initialising Select Limit Form. (2)", ex.StackTrace, True)
		End Try

		InPaint = False

	End Sub

    ''' <summary>
    ''' Handles the Closing event of the frm control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
	Private Sub frm_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		Dim HideForm As Boolean

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...

		_InUse = False

		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else
			HideForm = True
			If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			e.Cancel = True
		Else
			Try
				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

				RemoveHandler Combo_LimitFund.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_LimitType.SelectedValueChanged, AddressOf Me.FormControlChanged

				RemoveHandler Combo_Select1_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Select1_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Select1_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp
				RemoveHandler Combo_Select2_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Select2_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Select2_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp
				RemoveHandler Combo_Select3_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Select3_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Select3_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp

				RemoveHandler Combo_AndOr_1.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_AndOr_2.SelectedValueChanged, AddressOf Me.FormControlChanged

				RemoveHandler Combo_LimitFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_LimitType.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_LimitsFundDictionary.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_LimitsLimitTypeDictionary.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_LimitsLimitInstrumentDictionary.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_LimitsLimitCurrencyDictionary.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_LimitsLimitSectorDictionary.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_LimitsLimitSectorGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_LimitsLimitDealingPeriodGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_LimitsGtOrLessThanGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_LimitsCharacteristicDictionary.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				RemoveHandler Combo_LimitFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_LimitType.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_LimitsFundDictionary.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_LimitsLimitTypeDictionary.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_LimitsLimitInstrumentDictionary.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_LimitsLimitCurrencyDictionary.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_LimitsLimitSectorDictionary.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_LimitsLimitSectorGroup.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_LimitsLimitDealingPeriodGroup.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_LimitsGtOrLessThanGroup.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_LimitsCharacteristicDictionary.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

				RemoveHandler Combo_LimitFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_LimitType.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_LimitsFundDictionary.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_LimitsLimitTypeDictionary.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_LimitsLimitInstrumentDictionary.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_LimitsLimitCurrencyDictionary.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_LimitsLimitSectorDictionary.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_LimitsLimitSectorGroup.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_LimitsLimitDealingPeriodGroup.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_LimitsGtOrLessThanGroup.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_LimitsCharacteristicDictionary.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

				RemoveHandler Combo_LimitFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_LimitType.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_LimitsFundDictionary.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_LimitsLimitTypeDictionary.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_LimitsLimitInstrumentDictionary.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_LimitsLimitCurrencyDictionary.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_LimitsLimitSectorDictionary.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_LimitsLimitSectorGroup.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_LimitsLimitDealingPeriodGroup.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_LimitsGtOrLessThanGroup.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_LimitsCharacteristicDictionary.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

			Catch ex As Exception
			End Try
		End If

	End Sub


#End Region

	' Routine to handle changes / updates to tables by this and other windows.
	' If this, or any other, form posts a change to a table, then it will invoke an update event 
	' detailing what tables have been altered.
	' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
	' the 'VeniceAutoUpdate' event of the main Venice form.
	' Each form may them react as appropriate to changes in any table that might impact it.
	'
    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		Dim OrgInPaint As Boolean
		Dim KnowledgeDateChanged As Boolean
		Dim RefreshGrid As Boolean

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		OrgInPaint = InPaint

		Try
			InPaint = True
			KnowledgeDateChanged = False
			RefreshGrid = False

			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
				KnowledgeDateChanged = True
			End If
		Catch ex As Exception
		End Try

		' ****************************************************************
		' Check for changes relevant to this form
		' ****************************************************************


		' Changes to the tblFund table :-
		Try
			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFund) = True) Or KnowledgeDateChanged Then
				Call SetFundCombo()
				Call SetLimitsFundDictionary()
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblFund", ex.StackTrace, True)
		End Try

		' Changes to the tblInstrument table :-
		Try
			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrument) = True) Or KnowledgeDateChanged Then
				Call SetLimitsLimitInstrumentDictionary()
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblInstrument", ex.StackTrace, True)
		End Try

		' Changes to the tblCurrency table :-
		Try
			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblCurrency) = True) Or KnowledgeDateChanged Then
				Call SetLimitsLimitCurrencyDictionary()
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblCurrency", ex.StackTrace, True)
		End Try

		' Changes to the tblRiskDataCharacteristic table :-
		Try
			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblRiskDataCharacteristic) = True) Or KnowledgeDateChanged Then
				Call SetLimitsCharacteristicDictionary()
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblRiskDataCharacteristic", ex.StackTrace, True)
		End Try

		' Changes to the KnowledgeDate :-
		Try
			If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
				RefreshGrid = True
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: KnowledgeDate", ex.StackTrace, True)
		End Try

		' Check TableUpdate against first Select Field
		Try
			If Me.Combo_FieldSelect1.SelectedIndex >= 0 Then
				Try

					' If the First FieldSelect combo has selected a Limit Field which is related to
					' another table, as defined in tblReferentialIntegrity (e.g. limFund)
					' Then if that related table is updated, the FieldSelect-Values combo must be updated.

					' If ChangedID is ChangeID of related table then ,..

					If (e.TableChanged(GetFieldChangeID(Combo_FieldSelect1.SelectedItem)) = True) Or KnowledgeDateChanged Then
						Dim SelectedValue As Object
						Dim TextValue As String

						' Save current Value Combo Value.

						SelectedValue = Nothing
						TextValue = ""

						If Me.Combo_Select1_Value.SelectedIndex >= 0 Then
							SelectedValue = Me.Combo_Select1_Value.SelectedValue
						ElseIf (SelectedValue Is Nothing) Then
							TextValue = Me.Combo_Select1_Value.Text
						End If

						' Update FieldSelect-Values Combo.

						Call UpdateSelectedValueCombo(Me.Combo_FieldSelect1.SelectedItem, Me.Combo_Select1_Value)

						' Restore Saved Value.

						If (Not (SelectedValue Is Nothing)) Then
							Me.Combo_Select1_Value.SelectedValue = SelectedValue
						ElseIf TextValue.Length > 0 Then
							Me.Combo_Select1_Value.Text = TextValue
						End If
					End If
				Catch ex As Exception
				End Try
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: First SelectField", ex.StackTrace, True)
		End Try

		' Check TableUpdate against second Select Field
		Try
			If Me.Combo_FieldSelect2.SelectedIndex >= 0 Then
				Try
					' GetFieldChangeID
					If (e.TableChanged(GetFieldChangeID(Combo_FieldSelect2.SelectedItem)) = True) Or KnowledgeDateChanged Then
						Dim SelectedValue As Object
						Dim TextValue As String

						SelectedValue = Nothing
						TextValue = ""

						If Me.Combo_Select2_Value.SelectedIndex >= 0 Then
							SelectedValue = Me.Combo_Select2_Value.SelectedValue
						ElseIf (SelectedValue Is Nothing) Then
							TextValue = Me.Combo_Select2_Value.Text
						End If

						Call UpdateSelectedValueCombo(Me.Combo_FieldSelect2.SelectedItem, Me.Combo_Select2_Value)

						If (Not (SelectedValue Is Nothing)) Then
							Me.Combo_Select2_Value.SelectedValue = SelectedValue
						ElseIf TextValue.Length > 0 Then
							Me.Combo_Select2_Value.Text = TextValue
						End If
					End If
				Catch ex As Exception
				End Try
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: Second SelectField", ex.StackTrace, True)
		End Try

		' Check TableUpdate against third Select Field
		Try
			If Me.Combo_FieldSelect3.SelectedIndex >= 0 Then
				Try
					' GetFieldChangeID
					If (e.TableChanged(GetFieldChangeID(Combo_FieldSelect3.SelectedItem)) = True) Or KnowledgeDateChanged Then
						Dim SelectedValue As Object
						Dim TextValue As String

						SelectedValue = Nothing
						TextValue = ""

						If Me.Combo_Select3_Value.SelectedIndex >= 0 Then
							SelectedValue = Me.Combo_Select3_Value.SelectedValue
						ElseIf (SelectedValue Is Nothing) Then
							TextValue = Me.Combo_Select3_Value.Text
						End If

						Call UpdateSelectedValueCombo(Me.Combo_FieldSelect3.SelectedItem, Me.Combo_Select3_Value)

						If (Not (SelectedValue Is Nothing)) Then
							Me.Combo_Select3_Value.SelectedValue = SelectedValue
						ElseIf TextValue.Length > 0 Then
							Me.Combo_Select3_Value.Text = TextValue
						End If
					End If
				Catch ex As Exception
				End Try
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: Third SelectField", ex.StackTrace, True)
		End Try


		' Changes to the tblUserPermissions table :-

		Try
			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

				' Check ongoing permissions.

				Call CheckPermissions()
				If (HasReadPermission = False) Then
					Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

					FormIsValid = False
					InPaint = OrgInPaint
					Me.Close()
					Exit Sub
				End If

				If (HasDeletePermission = False) Then
					Me.Grid_Limits.Cols(Grid_Delete).AllowEditing = False
				Else
					Me.Grid_Limits.Cols(Grid_Delete).AllowEditing = True
				End If

				RefreshGrid = True

			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: tblUserPermissions", ex.StackTrace, True)
		End Try


		' ****************************************************************
		' Changes to the Main FORM table :-
		' (or the tblLimits table)
		' ****************************************************************

		Try
			If (e.TableChanged(THIS_FORM_ChangeID) = True) Or _
				(e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblLimits) = True) Or _
				(RefreshGrid = True) Or _
				KnowledgeDateChanged Then

				' Re-Set Controls etc.
				Call GetSortedRows()

			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: Main Form table", ex.StackTrace, True)
		End Try

		InPaint = OrgInPaint

	End Sub

#Region " Workhorse functions : GetSortedRows / CheckPermission / ControlsChanged / Menu Events "

    ''' <summary>
    ''' Gets the limit select string.
    ''' </summary>
    ''' <returns>System.String.</returns>
	Private Function GetLimitSelectString() As String
		' *******************************************************************************
		' Build a Select String appropriate to the form status.
		' *******************************************************************************

		Dim SelectString As String
		Dim FieldSelectString As String

		SelectString = ""
		FieldSelectString = ""
		Me.StatusBar_SelectLimit.Text = ""

		Try

			' Select on Fund...

			If (Me.Combo_LimitFund.SelectedIndex > 0) Then
				SelectString = "(limFund=" & Combo_LimitFund.SelectedValue.ToString & ")"
			End If

			' Select on LimitType

			If (Me.Combo_LimitType.SelectedIndex > 0) Then
				If (SelectString.Length > 0) Then
					SelectString &= " AND "
				End If
				SelectString &= "(limType=" & Combo_LimitType.SelectedValue.ToString & ")"
			End If

			' Field Select ...

			FieldSelectString = "("
			Dim LimitsDataset As DataSet
			Dim LimitsTable As DataTable
			Dim FieldName As String

			LimitsDataset = MainForm.Load_Table(RenaissanceStandardDatasets.tblLimits, False)
			LimitsTable = myDataset.Tables(0)

			' Select Field One.

			Try
				If (Me.Combo_FieldSelect1.SelectedIndex > 0) AndAlso (Me.Combo_Select1_Operator.SelectedIndex > 0) And (Me.Combo_Select1_Value.Text.Length > 0) Then
					' Get Field Name, Add 'lim' back on if necessary.

					FieldName = Combo_FieldSelect1.SelectedItem
					If (FieldName.EndsWith(".")) Then
						FieldName = "lim" & FieldName.Substring(0, FieldName.Length - 1)
					End If

					' If the Selected Field Name is a String Field ...

					If (LimitsTable.Columns(FieldName).DataType Is GetType(System.String)) Then

						' If there is a value Selected, Use it - else use the Combo Text.

						If (Not (Combo_Select1_Value.SelectedValue Is Nothing)) Then
							FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.Text & " '" & Combo_Select1_Value.SelectedValue.ToString & "')"
						Else
							FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.Text & " '" & Combo_Select1_Value.Text.ToString & "')"
						End If

					ElseIf (LimitsTable.Columns(FieldName).DataType Is GetType(System.DateTime)) Then

						' For Dates, If there is no 'Selected' value use the Combo text if it is a Valid date, else
						' Use the 'Selected' Value if it is a date.

						If (Me.Combo_Select1_Value.SelectedValue Is Nothing) AndAlso (IsDate(Me.Combo_Select1_Value.Text)) Then
							FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select1_Value.Text).ToString(QUERY_SHORTDATEFORMAT) & "#)"
						ElseIf (Not (Me.Combo_Select1_Value.SelectedValue Is Nothing)) And IsDate(Combo_Select1_Value.SelectedValue) Then
							FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select1_Value.SelectedValue).ToString(QUERY_SHORTDATEFORMAT) & "#)"
						Else
							FieldSelectString &= "(true)"
						End If

					Else
						' Now Deemed to be numeric, or at least not in need of quotes / delimeters...
						' If there is no 'Selected' value use the Combo text if it is a Valid number, else
						' Use the 'Selected' Value if it is a valid numeric.

						If (Not (Combo_Select1_Value.SelectedValue Is Nothing)) AndAlso (IsNumeric(Combo_Select1_Value.SelectedValue)) Then
							FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.SelectedItem.ToString & " " & Combo_Select1_Value.SelectedValue.ToString & ")"
						ElseIf IsNumeric(Combo_Select1_Value.Text) Then
							FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.SelectedItem.ToString & " " & Combo_Select1_Value.Text & ")"
						Else
							FieldSelectString &= "(true)"
						End If
					End If
				End If
			Catch ex As Exception
			End Try

			' Select Field Two.

			Try
				If (Me.Combo_FieldSelect2.SelectedIndex > 0) AndAlso (Me.Combo_Select2_Operator.SelectedIndex > 0) And (Me.Combo_Select2_Value.Text.Length > 0) Then
					FieldName = Combo_FieldSelect2.SelectedItem
					If (FieldName.EndsWith(".")) Then
						FieldName = "lim" & FieldName.Substring(0, FieldName.Length - 1)
					End If

					If (FieldSelectString.Length > 1) Then
						FieldSelectString &= " " & Me.Combo_AndOr_1.SelectedItem.ToString & " "
					End If

					If (LimitsTable.Columns(FieldName).DataType Is GetType(System.String)) Then

						If (Not (Combo_Select2_Value.SelectedValue Is Nothing)) Then
							FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.Text & " '" & Combo_Select2_Value.SelectedValue.ToString & "')"
						Else
							FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.Text & " '" & Combo_Select2_Value.Text.ToString & "')"
						End If

					ElseIf (LimitsTable.Columns(FieldName).DataType Is GetType(System.DateTime)) Then

						If (Me.Combo_Select2_Value.SelectedValue Is Nothing) AndAlso (IsDate(Me.Combo_Select2_Value.Text)) Then
							FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select2_Value.Text).ToString(QUERY_SHORTDATEFORMAT) & "#)"
						ElseIf (Not (Me.Combo_Select2_Value.SelectedValue Is Nothing)) And IsDate(Combo_Select2_Value.SelectedValue) Then
							FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select2_Value.SelectedValue).ToString(QUERY_SHORTDATEFORMAT) & "#)"
						Else
							FieldSelectString &= "(true)"
						End If

					Else
						If (Not (Combo_Select2_Value.SelectedValue Is Nothing)) AndAlso (IsNumeric(Combo_Select2_Value.SelectedValue)) Then
							FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.SelectedItem.ToString & " " & Combo_Select2_Value.SelectedValue.ToString & ")"
						ElseIf IsNumeric(Combo_Select2_Value.Text) Then
							FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.SelectedItem.ToString & " " & Combo_Select2_Value.Text & ")"
						Else
							FieldSelectString &= "(true)"
						End If
					End If
				End If
			Catch ex As Exception
			End Try


			' Select Field Three.

			Try
				If (Me.Combo_FieldSelect3.SelectedIndex > 0) AndAlso (Me.Combo_Select3_Operator.SelectedIndex > 0) And (Me.Combo_Select3_Value.Text.Length > 0) Then
					FieldName = Combo_FieldSelect3.SelectedItem
					If (FieldName.EndsWith(".")) Then
						FieldName = "lim" & FieldName.Substring(0, FieldName.Length - 1)
					End If

					If (FieldSelectString.Length > 1) Then
						FieldSelectString &= " " & Me.Combo_AndOr_2.SelectedItem.ToString & " "
					End If

					If (LimitsTable.Columns(FieldName).DataType Is GetType(System.String)) Then

						If (Not (Combo_Select3_Value.SelectedValue Is Nothing)) Then
							FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.Text & " '" & Combo_Select3_Value.SelectedValue.ToString & "')"
						Else
							FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.Text & " '" & Combo_Select3_Value.Text.ToString & "')"
						End If

					ElseIf (LimitsTable.Columns(FieldName).DataType Is GetType(System.DateTime)) Then

						If (Me.Combo_Select3_Value.SelectedValue Is Nothing) AndAlso (IsDate(Me.Combo_Select3_Value.Text)) Then
							FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select3_Value.Text).ToString(QUERY_SHORTDATEFORMAT) & "#)"
						ElseIf (Not (Me.Combo_Select3_Value.SelectedValue Is Nothing)) And IsDate(Combo_Select3_Value.SelectedValue) Then
							FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select3_Value.SelectedValue).ToString(QUERY_SHORTDATEFORMAT) & "#)"
						Else
							FieldSelectString &= "(true)"
						End If

					Else
						If (Not (Combo_Select3_Value.SelectedValue Is Nothing)) AndAlso (IsNumeric(Combo_Select3_Value.SelectedValue)) Then
							FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.SelectedItem.ToString & " " & Combo_Select3_Value.SelectedValue.ToString & ")"
						ElseIf IsNumeric(Combo_Select3_Value.Text) Then
							FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.SelectedItem.ToString & " " & Combo_Select3_Value.Text & ")"
						Else
							FieldSelectString &= "(true)"
						End If
					End If
				End If
			Catch ex As Exception
			End Try

			FieldSelectString &= ")"

			' If the FieldSelect string is used, then tag it on to the main select string.

			If FieldSelectString.Length > 2 Then
				If (SelectString.Length > 0) Then
					SelectString &= " AND "
				End If
				SelectString &= FieldSelectString
			End If

			If SelectString.Length <= 0 Then
				SelectString = "true"
			End If

		Catch ex As Exception
			SelectString = "true"
		End Try

		Return SelectString

	End Function

    ''' <summary>
    ''' Gets the sorted rows.
    ''' </summary>
	Private Sub GetSortedRows()
		' *******************************************************************************
		' Build a Select String appropriate to the form status and apply it to the Grid object.
		' *******************************************************************************

		Dim SelectString As String
		Dim SelectedRows As DSLimits.tblLimitsRow()
		Dim thisRow As DSLimits.tblLimitsRow
		Dim Counter As Integer

		Dim OrgInpaint As Boolean

		If (AddNewRecord = True) Then
			Call SetFormData()
		End If

		' Set 'Paint' flag to prevent event actions caused by changing the value of form fields.

		OrgInpaint = InPaint
		InPaint = True
		AddNewRecord = False
		FormChanged = False

		' As it says on the can....
		Call SetButtonStatus()

		SelectString = GetLimitSelectString()

		' Get Selected Limit rows
		Try
			SelectedRows = myTable.Select(SelectString, "limFund, limType, limSector, limCharacteristic, limInstrument, limGreaterOrLessThan, limLimit")
		Catch ex As Exception
			Me.Grid_Limits.Rows.Count = 1
			InPaint = OrgInpaint
			Exit Sub
		End Try

		' Exit if no reows to draw
		If (SelectedRows Is Nothing) OrElse (SelectedRows.Length <= 0) Then
			Me.Grid_Limits.Rows.Count = 1
			InPaint = OrgInpaint
			Exit Sub
		End If

		' Populate Grid with the selecte rows.
		Try
			' Clear grid Data, Not Formats.
			Grid_Limits.Clear(ClearFlags.Content)

			' Set Grid Length
			Me.Grid_Limits.Rows.Count = SelectedRows.Length + 1
			If (Grid_Limits.Rows.Count <= SelectedRows.Length + 1) Then
				' It seems hard to determine whether an extra 'new' row will be added at the end of the grig.
				' Set the grid initially, assuming one will be added. If it was not added then add it now.

				Me.Grid_Limits.Rows.Count = SelectedRows.Length + 2
			End If

			If Grid_Limits.Rows.Count > 0 Then
				Grid_Limits.Item(0, Grid_Fund) = "Fund"
				Grid_Limits.Item(0, Grid_Type) = "Limit Type"
				Grid_Limits.Item(0, Grid_LimitItem) = "Limit Item / Area"
				Grid_Limits.Item(0, Grid_GreaterOrLessThan) = "< or >"
				Grid_Limits.Item(0, Grid_Limit) = "Limit"
				Grid_Limits.Item(0, Grid_Label) = ""
				Grid_Limits.Item(0, Grid_Premium) = "On Premium"
				Grid_Limits.Item(0, Grid_Delete) = "Delete"
			End If

			' Set 'New' Row ID top Zero.
			Grid_Limits.Item(SelectedRows.Length + 1, Grid_ID) = 0

			' Set Each Row of the Grid with the associated Limit Data :-
			For Counter = 0 To (SelectedRows.Length - 1)
				thisRow = SelectedRows(Counter)

				Grid_Limits.Item(Counter + 1, Grid_Fund) = thisRow.limFund
				Grid_Limits.Item(Counter + 1, Grid_Type) = thisRow.limType

				Grid_Limits.SetCellStyle(Counter + 1, Grid_Limit, Grid_Limits.Styles("Default_LimitFormat"))

				Select Case thisRow.limType
					Case LimitType.InstrumentCapitalUsage
						Grid_Limits.Item(Counter + 1, Grid_LimitItem) = thisRow.limInstrument
						Grid_Limits.SetCellStyle(Counter + 1, Grid_LimitItem, Grid_Limits.Styles("Normal"))	' Grid_Limits.Styles("LimitInstrument"))

					Case LimitType.SectorCapitalUsage
						Grid_Limits.Item(Counter + 1, Grid_LimitItem) = "" ' thisRow.limSector
						Grid_Limits.SetCellStyle(Counter + 1, Grid_LimitItem, Grid_Limits.Styles("Normal"))	' Grid_Limits.Styles("LimitSector"))

					Case LimitType.TotalCapitalUsage
						Grid_Limits.Item(Counter + 1, Grid_LimitItem) = ""
						Grid_Limits.SetCellStyle(Counter + 1, Grid_LimitItem, Grid_Limits.Styles("Normal"))

					Case LimitType.RealisedDrawdown
						Grid_Limits.Item(Counter + 1, Grid_LimitItem) = thisRow.limInstrument
						Grid_Limits.SetCellStyle(Counter + 1, Grid_LimitItem, Grid_Limits.Styles("LimitInstrument"))
						Grid_Limits.SetCellStyle(Counter + 1, Grid_Limit, Grid_Limits.Styles("Limit_PercentageFormat"))

					Case LimitType.InstrumentWeight
						Grid_Limits.Item(Counter + 1, Grid_LimitItem) = thisRow.limInstrument
						Grid_Limits.SetCellStyle(Counter + 1, Grid_LimitItem, Grid_Limits.Styles("Normal"))	' Grid_Limits.Styles("LimitInstrument"))
						Grid_Limits.SetCellStyle(Counter + 1, Grid_Limit, Grid_Limits.Styles("Limit_PercentageFormat"))

					Case LimitType.SectorWeight
						Grid_Limits.Item(Counter + 1, Grid_LimitItem) = thisRow.limSector
						Grid_Limits.SetCellStyle(Counter + 1, Grid_LimitItem, Grid_Limits.Styles("Normal"))	' Grid_Limits.Styles("LimitSector"))
						Grid_Limits.SetCellStyle(Counter + 1, Grid_Limit, Grid_Limits.Styles("Limit_PercentageFormat"))

					Case LimitType.SpecificInstrumentWeight
						Grid_Limits.Item(Counter + 1, Grid_LimitItem) = thisRow.limInstrument
						Grid_Limits.SetCellStyle(Counter + 1, Grid_LimitItem, Grid_Limits.Styles("LimitInstrument"))
						Grid_Limits.SetCellStyle(Counter + 1, Grid_Limit, Grid_Limits.Styles("Limit_PercentageFormat"))

					Case LimitType.SpecificSectorWeight
						Grid_Limits.Item(Counter + 1, Grid_LimitItem) = thisRow.limSector
						Grid_Limits.SetCellStyle(Counter + 1, Grid_LimitItem, Grid_Limits.Styles("LimitSector"))
						Grid_Limits.SetCellStyle(Counter + 1, Grid_Limit, Grid_Limits.Styles("Limit_PercentageFormat"))

					Case LimitType.NumberOfInstruments
						Grid_Limits.Item(Counter + 1, Grid_LimitItem) = ""
						Grid_Limits.SetCellStyle(Counter + 1, Grid_LimitItem, Grid_Limits.Styles("Normal"))
						Grid_Limits.SetCellStyle(Counter + 1, Grid_Limit, Grid_Limits.Styles("Limit_IntegerFormat"))

					Case LimitType.SpecificInstrumentCapitalUsage
						Grid_Limits.Item(Counter + 1, Grid_LimitItem) = thisRow.limInstrument
						Grid_Limits.SetCellStyle(Counter + 1, Grid_LimitItem, Grid_Limits.Styles("LimitInstrument"))

					Case LimitType.SpecificSectorCapitalUsage
						Grid_Limits.Item(Counter + 1, Grid_LimitItem) = thisRow.limSector
						Grid_Limits.SetCellStyle(Counter + 1, Grid_LimitItem, Grid_Limits.Styles("LimitSector"))

					Case LimitType.SectorGroupWeight
						Grid_Limits.Item(Counter + 1, Grid_LimitItem) = ""
						Grid_Limits.SetCellStyle(Counter + 1, Grid_LimitItem, Grid_Limits.Styles("Normal"))
						Grid_Limits.SetCellStyle(Counter + 1, Grid_Limit, Grid_Limits.Styles("Limit_PercentageFormat"))

					Case LimitType.SpecificSectorGroupWeight
						Grid_Limits.Item(Counter + 1, Grid_LimitItem) = thisRow.limSectorGroup
						Grid_Limits.SetCellStyle(Counter + 1, Grid_LimitItem, Grid_Limits.Styles("LimitSectorGroup"))
						Grid_Limits.SetCellStyle(Counter + 1, Grid_Limit, Grid_Limits.Styles("Limit_PercentageFormat"))

					Case LimitType.DealingPeriod
						Grid_Limits.Item(Counter + 1, Grid_LimitItem) = thisRow.limDealingPeriod
						Grid_Limits.SetCellStyle(Counter + 1, Grid_LimitItem, Grid_Limits.Styles("LimitDealingPeriod"))

					Case LimitType.CurrencyExposure
						Grid_Limits.Item(Counter + 1, Grid_LimitItem) = thisRow.limInstrument
						Grid_Limits.SetCellStyle(Counter + 1, Grid_LimitItem, Grid_Limits.Styles("LimitCurrency"))

					Case LimitType.Characteristic
						Grid_Limits.Item(Counter + 1, Grid_LimitItem) = thisRow.limCharacteristic
						Grid_Limits.SetCellStyle(Counter + 1, Grid_LimitItem, Grid_Limits.Styles("LimitCharacteristic"))
						Grid_Limits.SetCellStyle(Counter + 1, Grid_Limit, Grid_Limits.Styles("Limit_PercentageFormat"))

                    Case LimitType.Ratio_5_10_40
                        Grid_Limits.Item(Counter + 1, Grid_LimitItem) = thisRow.limInstrument
                        Grid_Limits.SetCellStyle(Counter + 1, Grid_LimitItem, Grid_Limits.Styles("Normal")) ' Grid_Limits.Styles("LimitInstrument"))
                        Grid_Limits.SetCellStyle(Counter + 1, Grid_Limit, Grid_Limits.Styles("Limit_PercentageFormat"))

                    Case Else
                        Grid_Limits.Item(Counter + 1, Grid_LimitItem) = ""
                        Grid_Limits.SetCellStyle(Counter + 1, Grid_LimitItem, Grid_Limits.Styles("Normal"))

                End Select

				Grid_Limits.Item(Counter + 1, Grid_GreaterOrLessThan) = thisRow.limGreaterOrLessThan
				Grid_Limits.Item(Counter + 1, Grid_Limit) = thisRow.limLimit
				Grid_Limits.Item(Counter + 1, Grid_Premium) = CBool(thisRow.limOnPremium)
				Grid_Limits.Item(Counter + 1, Grid_ID) = thisRow.limID
				Grid_Limits.Item(Counter + 1, Grid_Label) = Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblLimitType, thisRow.limType, "ltpLabel"), "").ToString
                Grid_Limits.Item(Counter + 1, Grid_Delete) = False

            Next

		Catch ex As Exception
			Me.Grid_Limits.Rows.Count = 1
			InPaint = OrgInpaint
			Exit Sub
		End Try

		Me.StatusBar_SelectLimit.Text = "(" & SelectedRows.Length.ToString & " Records) " & SelectString
		InPaint = OrgInpaint

		Call SetButtonStatus()

	End Sub

    ''' <summary>
    ''' Sets the form data.
    ''' </summary>
	Private Sub SetFormData()
		' **********************************************************************************
		'
		'
		' **********************************************************************************

		Dim GridRow As Integer
		Dim RowIsValid As Boolean

		Dim LimitID As Integer
		Dim LimitFund As Integer
		Dim LimitLimitType As Integer
		Dim LimitInstrument As Integer
		Dim LimitCharacteristic As Integer
		Dim LimitSector As Integer
		Dim LimitSectorGroup As String
		Dim LimitDealingPeriod As Integer
		Dim LimitGreaterOrLessThan As Integer
		Dim LimitValue As Double
		Dim LimitOnPremium As Integer
		Dim LimitIsPercent As Integer
		Dim LimitDeleted As Boolean

		Dim SelectedRows As RenaissanceDataClass.DSLimits.tblLimitsRow()
		Dim thisRow As RenaissanceDataClass.DSLimits.tblLimitsRow

		Try
			myAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
			myAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
		Catch ex As Exception
		End Try

		For GridRow = 1 To (Me.Grid_Limits.Rows.Count - 1)
			RowIsValid = True

			LimitID = 0
			LimitFund = 0
			LimitOnPremium = 0
			LimitLimitType = 0
			LimitInstrument = 0
			LimitCharacteristic = 0
			LimitSector = 0
			LimitSectorGroup = ""
			LimitDealingPeriod = 0
			LimitGreaterOrLessThan = RenaissanceGlobals.Relationship.GreaterThan
			LimitValue = 0
			LimitIsPercent = 0
			LimitDeleted = False

			If IsNumeric(Grid_Limits.Item(GridRow, Grid_ID)) Then
				LimitID = CInt(Grid_Limits.Item(GridRow, Grid_ID))
			Else
				LimitID = 0
			End If

			If IsNumeric(Grid_Limits.Item(GridRow, Grid_Fund)) Then
				LimitFund = CInt(Grid_Limits.Item(GridRow, Grid_Fund))
			Else
				RowIsValid = False
			End If

			If IsNumeric(Grid_Limits.Item(GridRow, Grid_Type)) Then
				LimitLimitType = CInt(Grid_Limits.Item(GridRow, Grid_Type))
			Else
				RowIsValid = False
			End If

			Select Case LimitLimitType
				Case LimitType.InstrumentCapitalUsage

				Case LimitType.SectorCapitalUsage

				Case LimitType.TotalCapitalUsage

				Case LimitType.RealisedDrawdown
					If IsNumeric(Grid_Limits.Item(GridRow, Grid_LimitItem)) Then
						LimitInstrument = CInt(Grid_Limits.Item(GridRow, Grid_LimitItem))
					Else
						RowIsValid = False
					End If

				Case LimitType.InstrumentWeight

				Case LimitType.SectorWeight

				Case LimitType.SpecificInstrumentWeight
					If IsNumeric(Grid_Limits.Item(GridRow, Grid_LimitItem)) Then
						LimitInstrument = CInt(Grid_Limits.Item(GridRow, Grid_LimitItem))
					Else
						RowIsValid = False
					End If

				Case LimitType.SpecificSectorWeight
					If IsNumeric(Grid_Limits.Item(GridRow, Grid_LimitItem)) Then
						LimitSector = CInt(Grid_Limits.Item(GridRow, Grid_LimitItem))
					Else
						RowIsValid = False
					End If

				Case LimitType.NumberOfInstruments

				Case LimitType.SpecificInstrumentCapitalUsage
					If IsNumeric(Grid_Limits.Item(GridRow, Grid_LimitItem)) Then
						LimitInstrument = CInt(Grid_Limits.Item(GridRow, Grid_LimitItem))
					Else
						RowIsValid = False
					End If

				Case LimitType.SpecificSectorCapitalUsage
					If IsNumeric(Grid_Limits.Item(GridRow, Grid_LimitItem)) Then
						LimitSector = CInt(Grid_Limits.Item(GridRow, Grid_LimitItem))
					Else
						RowIsValid = False
					End If

				Case LimitType.SectorGroupWeight

				Case LimitType.SpecificSectorGroupWeight
					If Not (Grid_Limits.Item(GridRow, Grid_LimitItem) Is Nothing) Then
						LimitSectorGroup = CStr(Grid_Limits.Item(GridRow, Grid_LimitItem))
					Else
						RowIsValid = False
					End If

				Case LimitType.DealingPeriod
					If IsNumeric(Grid_Limits.Item(GridRow, Grid_LimitItem)) Then
						LimitDealingPeriod = CInt(Grid_Limits.Item(GridRow, Grid_LimitItem))
					Else
						RowIsValid = False
					End If

				Case LimitType.CurrencyExposure
					If IsNumeric(Grid_Limits.Item(GridRow, Grid_LimitItem)) Then
						LimitInstrument = CInt(Grid_Limits.Item(GridRow, Grid_LimitItem))
					Else
						RowIsValid = False
					End If

				Case LimitType.Characteristic
					LimitIsPercent = 1

					If IsNumeric(Grid_Limits.Item(GridRow, Grid_LimitItem)) Then
						LimitCharacteristic = CInt(Grid_Limits.Item(GridRow, Grid_LimitItem))
					Else
						RowIsValid = False
					End If
				Case Else

			End Select

			If IsNumeric(Grid_Limits.Item(GridRow, Grid_GreaterOrLessThan)) Then
				LimitGreaterOrLessThan = CInt(Grid_Limits.Item(GridRow, Grid_GreaterOrLessThan))
			End If

			If IsNumeric(Grid_Limits.Item(GridRow, Grid_Limit)) Then
				LimitValue = CDbl(Grid_Limits.Item(GridRow, Grid_Limit))
			End If

			If CBool(Grid_Limits.Item(GridRow, Grid_Premium)) Then
				LimitOnPremium = 1
			End If

			Try
				LimitDeleted = CBool(Grid_Limits.Item(GridRow, Grid_Delete))
			Catch ex As Exception
			End Try

			If (LimitDeleted = True) Then
				If (LimitID > 0) Then
					' Find and Delete Row

					Try
						SelectedRows = myTable.Select("limID=" & LimitID.ToString)
						If (Not (SelectedRows Is Nothing)) AndAlso (SelectedRows.Length > 0) Then
							For Each thisRow In SelectedRows
								thisRow.Delete()
							Next

							MainForm.AdaptorUpdate(Me.Name, myAdaptor, SelectedRows)

						End If

					Catch ex As Exception
						MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Deleting Limit, ID = " & LimitID.ToString, ex.StackTrace, True)
					End Try
				End If
			ElseIf (LimitID > 0) Then
				' Find And Update Row if necessary

				Try
					SelectedRows = myTable.Select("limID=" & LimitID.ToString)
					If (Not (SelectedRows Is Nothing)) AndAlso (SelectedRows.Length > 0) Then
						For Each thisRow In SelectedRows

							' Set Limit Values f they have changed.

							If thisRow.limFund <> LimitFund Then
								thisRow.limFund = LimitFund
							End If

							If thisRow.limType <> LimitLimitType Then
								thisRow.limType = LimitLimitType
							End If

							If thisRow.limInstrument <> LimitInstrument Then
								thisRow.limInstrument = LimitInstrument
							End If

							If thisRow.limSector <> LimitSector Then
								thisRow.limSector = LimitSector
							End If

							If thisRow.limSectorGroup <> LimitSectorGroup Then
								thisRow.limSectorGroup = LimitSectorGroup
							End If

							If thisRow.limDealingPeriod <> LimitDealingPeriod Then
								thisRow.limDealingPeriod = LimitDealingPeriod
							End If

							If thisRow.limGreaterOrLessThan <> LimitGreaterOrLessThan Then
								thisRow.limGreaterOrLessThan = LimitGreaterOrLessThan
							End If

							If Math.Abs(thisRow.limLimit - LimitValue) > 0.00000001 Then
								thisRow.limLimit = LimitValue
							End If

							If thisRow.limCharacteristic <> LimitCharacteristic Then
								thisRow.limCharacteristic = LimitCharacteristic
							End If

							If thisRow.limOnPremium <> LimitOnPremium Then
								thisRow.limOnPremium = LimitOnPremium
							End If

							' Update if anything has changed

							If thisRow.RowState <> DataRowState.Unchanged Then
								MainForm.AdaptorUpdate(Me.Name, myAdaptor, New DataRow() {thisRow})
							End If
						Next
					End If

				Catch ex As Exception
					MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Updating Limit, ID = " & LimitID.ToString, ex.StackTrace, True)
				End Try

			Else
				' Add New Row

				Try
					If (LimitLimitType > 0) AndAlso (LimitGreaterOrLessThan > 0) Then
						thisRow = myTable.NewRow

						thisRow.limID = 0
						thisRow.limDescription = ""
						thisRow.limFund = LimitFund
						thisRow.limType = LimitLimitType
						thisRow.limInstrument = LimitInstrument
						thisRow.limCharacteristic = LimitCharacteristic
						thisRow.limSector = LimitSector
						thisRow.limSectorGroup = LimitSectorGroup
						thisRow.limDealingPeriod = LimitDealingPeriod
						thisRow.limGreaterOrLessThan = LimitGreaterOrLessThan
						thisRow.limLimit = LimitValue
						thisRow.limOnPremium = LimitOnPremium
						thisRow.limIsPercent = LimitIsPercent

						myTable.Rows.Add(thisRow)
						MainForm.AdaptorUpdate(Me.Name, myAdaptor, New DataRow() {thisRow})

					End If

				Catch ex As Exception
					MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Adding Limit, ID = " & LimitID.ToString, ex.StackTrace, True)
				End Try

			End If

		Next

		' Re-load the Limits table and re-paint the form.

		MainForm.Load_Table(ThisStandardDataset, True, False)
		Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID), True)

		Me.GetSortedRows()

	End Sub

    ''' <summary>
    ''' Sets the button status.
    ''' </summary>
	Private Sub SetButtonStatus()

		If Me.FormChanged Then
			Me.btnCancel.Enabled = True
			Me.btnSave.Enabled = True
		Else
			Me.btnCancel.Enabled = False
			Me.btnSave.Enabled = False
		End If

	End Sub

    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
	Private Sub CheckPermissions()
		' *******************************************************************************
		' Check User permissions
		'
		' *******************************************************************************

		Dim Permissions As Integer

		Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

		HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
		HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
		HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
		HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

	End Sub

    ''' <summary>
  ''' Basic event handler, called when a form value is changed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
		' *******************************************************************************
		' In the event of one of the Form Controls changing, refresh the data grid.
		' *******************************************************************************

		If InPaint = False Then

			Call GetSortedRows()

		End If
	End Sub

    ''' <summary>
    ''' Handles the KeyUp event of the Combo_FieldSelect control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
	Private Sub Combo_FieldSelect_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Combo_Select1_Value.KeyUp
		' *******************************************************************************
		' In the event of one of the Form Controls changing, refresh the data grid.
		' *******************************************************************************

		If InPaint = False Then

			Call GetSortedRows()

		End If
	End Sub

    ''' <summary>
    ''' Builds the field select combos.
    ''' </summary>
	Private Sub BuildFieldSelectCombos()
		' *********************************************************************************
		' Build the FieldSelect combo boxes.
		'
		' Field Names starting with 'lim' are modified to start with '.'
		' This is done in order to make the name more readable.
		'
		' *********************************************************************************

		Dim LimitsDataset As DataSet
		Dim LimitsTable As DataTable

		Dim ColumnName As String
		Dim thisColumn As DataColumn

		LimitsDataset = MainForm.Load_Table(RenaissanceStandardDatasets.tblLimits, False)
		LimitsTable = LimitsDataset.Tables(0)

		Combo_FieldSelect1.Items.Clear()
		Combo_Select1_Value.Items.Clear()
		Combo_FieldSelect1.Items.Add("")

		Combo_FieldSelect2.Items.Clear()
		Combo_Select2_Value.Items.Clear()
		Combo_FieldSelect2.Items.Add("")

		Combo_FieldSelect3.Items.Clear()
		Combo_Select3_Value.Items.Clear()
		Combo_FieldSelect3.Items.Add("")

		For Each thisColumn In myTable.Columns
			Try
				ColumnName = LimitsTable.Columns(thisColumn.ColumnName).ColumnName
				If (ColumnName.StartsWith("lim")) Then
					ColumnName = ColumnName.Substring(3) & "."
				End If

				Combo_FieldSelect1.Items.Add(ColumnName)
				Combo_FieldSelect2.Items.Add(ColumnName)
				Combo_FieldSelect3.Items.Add(ColumnName)

			Catch ex As Exception
			End Try
		Next

	End Sub

    ''' <summary>
    ''' Updates the selected value combo.
    ''' </summary>
    ''' <param name="pFieldName">Name of the p field.</param>
    ''' <param name="pValueCombo">The p value combo.</param>
	Private Sub UpdateSelectedValueCombo(ByVal pFieldName As String, ByRef pValueCombo As ComboBox)
		' *******************************************************************************
		' Build a Value Combo Box appropriate to the chosen DataTable Select Field.
		'
		' By default the combo will contain all existing values for the chosen field, however
		' If a relationship is defined for thi table and field, then a Combo will be built
		' which reflects this relationship.
		' e.g. the tblReferentialIntegrity table defined a relationship between the
		' limFund field and the tblFund.FundID field, thus if
		' the chosen Select field is 'Fund.' then the Value combo will be built using
		' the FundID & FundName details from the tblFund table.
		'
		' *******************************************************************************

		Dim tblRefIntegrity As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityDataTable
		Dim IntegrityRows() As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityRow
		Dim LimitsDataset As DataSet
		Dim LimitsTable As DataTable
		Dim SortOrder As Boolean

		SortOrder = True

		' Clear the Value Combo.

		Try
			pValueCombo.DataSource = Nothing
			pValueCombo.DisplayMember = ""
			pValueCombo.ValueMember = ""
			pValueCombo.Items.Clear()
		Catch ex As Exception
		End Try

		' By default, make the Combo a Standard Dropdown, so that non-list items can be entered.

		pValueCombo.DropDownStyle = ComboBoxStyle.DropDown

		' Exit if no FiledName is given (having cleared the combo).

		If (pFieldName.Length <= 0) Then Exit Sub

		' Adjust the Field name if appropriate.

		If (pFieldName.EndsWith(".")) Then
			pFieldName = "lim" & pFieldName.Substring(0, pFieldName.Length - 1)
		End If

		' Get a handle to the Standard Limits Table.
		' This is so that the field types can be determined and used.

		Try
			LimitsDataset = MainForm.Load_Table(RenaissanceStandardDatasets.tblLimits, False)
			LimitsTable = myDataset.Tables(0)
		Catch ex As Exception
			Exit Sub
		End Try

		' If the selected field is a Date then sort the selection combo in reverse order.

		If (LimitsTable.Columns(pFieldName).DataType Is GetType(System.DateTime)) Then
			SortOrder = False
		End If

		' Get a handle to the Referential Integrity table and the row matching this table and field.
		' this table defines a relationship between Limits Table fields and other tables.
		' This information can be used to build more meaningfull Value Combos.

		Try
			tblRefIntegrity = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblReferentialIntegrity, False), RenaissanceDataClass.DSReferentialIntegrity).tblReferentialIntegrity
			IntegrityRows = tblRefIntegrity.Select("(FeedsTable LIKE '%tblLimits') AND (FeedsField = '" & pFieldName & "')", "RN")
			If (IntegrityRows.Length <= 0) Then
				GoTo StandardExit
			End If

		Catch ex As Exception
			Exit Sub
		End Try


		If (IntegrityRows(0).IsDescriptionFieldNull) Then
			' No Linked Description Field

			GoTo StandardExit
		End If

		' OK, a referential record exists.
		' Determine the Table and Field Names to use to build the Value Combo.

		Dim TableName As String
		Dim TableField As String
		Dim DescriptionField As String
		Dim stdDS As StandardDataset
		Dim thisChangeID As RenaissanceChangeID

		Try

			TableName = IntegrityRows(0).TableName
			TableField = IntegrityRows(0).TableField
			DescriptionField = IntegrityRows(0).DescriptionField

			thisChangeID = CType(System.Enum.Parse(GetType(RenaissanceChangeID), TableName), RenaissanceChangeID)

			stdDS = RenaissanceStandardDatasets.GetStandardDataset(thisChangeID)

			' Build the appropriate Values Combo.

			Call MainForm.SetTblGenericCombo( _
			pValueCombo, _
			stdDS, _
			DescriptionField, _
			TableField, _
			"", False, SortOrder, True)		 ' 

			' For Referential Integrity generated combo's, make the Combo a 
			' DropDownList

			pValueCombo.DropDownStyle = ComboBoxStyle.DropDownList

		Catch ex As Exception
			GoTo StandardExit

		End Try

		Exit Sub

StandardExit:

		' Build the standard Combo, just use the values from the given field.

		Call MainForm.SetTblGenericCombo( _
		pValueCombo, _
		RenaissanceStandardDatasets.tblLimits, _
		pFieldName, _
		pFieldName, _
		"", True, SortOrder)	 ' 

	End Sub

    ''' <summary>
    ''' Gets the field change ID.
    ''' </summary>
    ''' <param name="pFieldName">Name of the p field.</param>
    ''' <returns>RenaissanceChangeID.</returns>
	Private Function GetFieldChangeID(ByVal pFieldName As String) As RenaissanceChangeID
		' *******************************************************************************
		'
		'
		' *******************************************************************************

		Dim tblRefIntegrity As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityDataTable
		Dim IntegrityRows() As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityRow

		If (pFieldName.EndsWith(".")) Then
			pFieldName = "lim" & pFieldName.Substring(0, pFieldName.Length - 1)
		End If

		Try
			tblRefIntegrity = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblReferentialIntegrity, False), RenaissanceDataClass.DSReferentialIntegrity).tblReferentialIntegrity
			IntegrityRows = tblRefIntegrity.Select("(FeedsTable LIKE '%tblLimits') AND (FeedsField = '" & pFieldName & "')", "RN")
			If (IntegrityRows.Length <= 0) Then
				Return RenaissanceChangeID.None
				Exit Function
			End If

		Catch ex As Exception
			Return RenaissanceChangeID.None
			Exit Function
		End Try


		If (IntegrityRows(0).IsDescriptionFieldNull) Then
			Return RenaissanceChangeID.None
			Exit Function
		End If

		' OK, a referential record exists.
		' Determine the Table and Field Names to use to build the Value Combo.

		Dim TableName As String
		Dim TableField As String
		Dim DescriptionField As String
		Dim thisChangeID As RenaissanceChangeID

		Try

			TableName = IntegrityRows(0).TableName
			TableField = IntegrityRows(0).TableField
			DescriptionField = IntegrityRows(0).DescriptionField

			thisChangeID = CType(System.Enum.Parse(GetType(RenaissanceChangeID), TableName), RenaissanceChangeID)

			Return thisChangeID
			Exit Function
		Catch ex As Exception
		End Try

		Return RenaissanceChangeID.None
		Exit Function

	End Function

    ''' <summary>
    ''' Handles the DoubleClick event of the Grid_Limits control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Grid_Limits_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid_Limits.DoubleClick
		' ***************************************************************************************
		' 
		' ***************************************************************************************


	End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_FieldSelect1 control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_FieldSelect1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_FieldSelect1.SelectedIndexChanged
		' ***************************************************************************************
		' Field Select Combo Chosen, Update the associated Values Combo.
		'
		' ***************************************************************************************

		Call UpdateSelectedValueCombo(Me.Combo_FieldSelect1.SelectedItem, Me.Combo_Select1_Value)
		Me.Combo_Select1_Operator.SelectedIndex = 1

	End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_FieldSelect2 control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_FieldSelect2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_FieldSelect2.SelectedIndexChanged
		' ***************************************************************************************
		' Field Select Combo Chosen, Update the associated Values Combo.
		'
		' ***************************************************************************************
		Call UpdateSelectedValueCombo(Me.Combo_FieldSelect2.SelectedItem, Me.Combo_Select2_Value)
		Me.Combo_Select2_Operator.SelectedIndex = 1
	End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_FieldSelect3 control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_FieldSelect3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_FieldSelect3.SelectedIndexChanged
		' ***************************************************************************************
		' Field Select Combo Chosen, Update the associated Values Combo.
		'
		' ***************************************************************************************
		Call UpdateSelectedValueCombo(Me.Combo_FieldSelect3.SelectedItem, Me.Combo_Select3_Value)
		Me.Combo_Select3_Operator.SelectedIndex = 1
	End Sub

#End Region

#Region " Button Event Code"

    ''' <summary>
    ''' Handles the Click event of the btnSave control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

		Try
			If Me.btnSave.Enabled = True Then
				If Me.FormChanged = True Then
					Me.SetFormData()
				End If
			End If
		Catch ex As Exception

		End Try
	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click

		Call GetSortedRows()

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnClose control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click

		' Close Form

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		Me.Close()

	End Sub

#End Region

#Region " Set Form Combos (Form Specific Code) "

    ''' <summary>
    ''' Sets the fund combo.
    ''' </summary>
	Private Sub SetFundCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_LimitFund, _
    RenaissanceStandardDatasets.tblFund, _
    "FundName", _
    "FundID", _
    "FundClosed=0", False, True, True)   ' 

	End Sub

    ''' <summary>
    ''' Sels the limit type combo.
    ''' </summary>
	Private Sub SelLimitTypeCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_LimitType, _
		RenaissanceStandardDatasets.tblLimitType, _
		"ltpLimitType", _
		"ltpID", _
		"", False, True, True)		' 

	End Sub

    ''' <summary>
    ''' Sets the sector group combo.
    ''' </summary>
	Private Sub SetSectorGroupCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_LimitsLimitSectorGroup, _
		RenaissanceStandardDatasets.tblFundType, _
		"FundTypeGroup", _
		"FundTypeGroup", _
		"", True, True, True, "")			' 

	End Sub

    ''' <summary>
    ''' Sets the limits fund dictionary.
    ''' </summary>
	Private Sub SetLimitsFundDictionary()
		' *******************************************************************************
		'
		' *******************************************************************************

		Dim DSLimitFund As RenaissanceDataClass.DSFund
		Dim SelecteFundRows As RenaissanceDataClass.DSFund.tblFundRow()
		Dim thisFundRow As RenaissanceDataClass.DSFund.tblFundRow
		Dim Counter As Integer

		LimitsFundDictionary = New Hashtable '  LookupCollection

		DSLimitFund = MainForm.Load_Table(RenaissanceStandardDatasets.tblFund, False)
		If (Not (DSLimitFund Is Nothing)) AndAlso (DSLimitFund.tblFund.Rows.Count > 0) Then
			SelecteFundRows = DSLimitFund.tblFund.Select("True", "FundCode")

			LimitsFundDictionary.Add(CInt(0), "All Funds")

			For Counter = 0 To (SelecteFundRows.Length - 1)
				thisFundRow = SelecteFundRows(Counter)
				LimitsFundDictionary.Add(CInt(thisFundRow.FundID), thisFundRow.FundCode.ToString)
			Next
		Else
			LimitsLimitTypeDictionary.Add(0, "No Funds have been defined.")
		End If

		' LimitsFundDictionary.Sort()

		Call MainForm.SetTblGenericCombo( _
		Combo_LimitsFundDictionary, _
		RenaissanceStandardDatasets.tblFund, _
		"FundCode", _
		"FundID", _
		"", False, True, True, 0, "All Funds")

	End Sub

    ''' <summary>
    ''' Sets the limits limit type dictionary.
    ''' </summary>
	Private Sub SetLimitsLimitTypeDictionary()
		' *******************************************************************************
		'
		' *******************************************************************************

		Dim thisDataset As RenaissanceDataClass.DSLimitType
		Dim SelecteRows As RenaissanceDataClass.DSLimitType.tblLimitTypeRow()
		Dim thisRow As RenaissanceDataClass.DSLimitType.tblLimitTypeRow
		Dim Counter As Integer

		LimitsLimitTypeDictionary = New Hashtable	 '  LookupCollection

		thisDataset = MainForm.Load_Table(RenaissanceStandardDatasets.tblLimitType, False)
		If (Not (thisDataset Is Nothing)) AndAlso (thisDataset.tblLimitType.Rows.Count > 0) Then
			SelecteRows = thisDataset.tblLimitType.Select("True", "ltpLimitType")

			For Counter = 0 To (SelecteRows.Length - 1)
				thisRow = SelecteRows(Counter)
				LimitsLimitTypeDictionary.Add(CInt(thisRow.ltpID), thisRow.ltpLimitType.ToString)
			Next
		Else
			LimitsLimitTypeDictionary.Add(0, "No Limit Types have been defined.")
		End If

		' LimitsFundDictionary.Sort()

		Call MainForm.SetTblGenericCombo( _
		Combo_LimitsLimitTypeDictionary, _
		RenaissanceStandardDatasets.tblLimitType, _
		"ltpLimitType", _
		"ltpID", _
		"", False, True, False)

	End Sub

    ''' <summary>
    ''' Sets the limits limit instrument dictionary.
    ''' </summary>
	Private Sub SetLimitsLimitInstrumentDictionary()
		' *******************************************************************************
		'
		' *******************************************************************************

		Dim thisDataset As RenaissanceDataClass.DSInstrument
		Dim SelecteRows As RenaissanceDataClass.DSInstrument.tblInstrumentRow()
		Dim thisRow As RenaissanceDataClass.DSInstrument.tblInstrumentRow
		Dim Counter As Integer

		LimitsLimitInstrumentDictionary = New Hashtable	 '  LookupCollection

		thisDataset = MainForm.Load_Table(RenaissanceStandardDatasets.tblInstrument, False)
		If (Not (thisDataset Is Nothing)) AndAlso (thisDataset.tblInstrument.Rows.Count > 0) Then
			SelecteRows = thisDataset.tblInstrument.Select("True", "InstrumentDescription")

			LimitsLimitInstrumentDictionary.Add(CInt(0), "")
			For Counter = 0 To (SelecteRows.Length - 1)
				thisRow = SelecteRows(Counter)
				LimitsLimitInstrumentDictionary.Add(CInt(thisRow.InstrumentID), thisRow.InstrumentDescription.ToString)
			Next
		Else
			LimitsLimitInstrumentDictionary.Add(0, "No Instruments have been defined.")
		End If

		' LimitsFundDictionary.Sort()

		Call MainForm.SetTblGenericCombo( _
		Combo_LimitsLimitInstrumentDictionary, _
		RenaissanceStandardDatasets.tblInstrument, _
		"InstrumentDescription", _
		"InstrumentID", _
		"", False, True, True, 0)

	End Sub

    ''' <summary>
    ''' Sets the limits limit currency dictionary.
    ''' </summary>
	Private Sub SetLimitsLimitCurrencyDictionary()
		' *******************************************************************************
		'
		' *******************************************************************************

		Dim thisDataset As RenaissanceDataClass.DSCurrency
		Dim SelecteRows As RenaissanceDataClass.DSCurrency.tblCurrencyRow()
		Dim thisRow As RenaissanceDataClass.DSCurrency.tblCurrencyRow
		Dim Counter As Integer

		If (LimitsLimitCurrencyDictionary IsNot Nothing) Then
			LimitsLimitCurrencyDictionary.Clear()
		Else
			LimitsLimitCurrencyDictionary = New Hashtable	 '  LookupCollection
		End If

		thisDataset = MainForm.Load_Table(RenaissanceStandardDatasets.tblCurrency, False)
		If (Not (thisDataset Is Nothing)) AndAlso (thisDataset.tblCurrency.Rows.Count > 0) Then
			SelecteRows = thisDataset.tblCurrency.Select("True", "CurrencyDescription")

			LimitsLimitCurrencyDictionary.Add(CInt(0), "")
			For Counter = 0 To (SelecteRows.Length - 1)
				thisRow = SelecteRows(Counter)
				LimitsLimitCurrencyDictionary.Add(CInt(thisRow.CurrencyID), thisRow.CurrencyDescription.ToString)
			Next
		Else
			LimitsLimitCurrencyDictionary.Add(0, "No Currencies have been defined.")
		End If

		Call MainForm.SetTblGenericCombo( _
		Combo_LimitsLimitCurrencyDictionary, _
		RenaissanceStandardDatasets.tblCurrency, _
		"CurrencyDescription", _
		"CurrencyID", _
		"", False, True, True, 0)

	End Sub

    ''' <summary>
    ''' Sets the limits limit sector dictionary.
    ''' </summary>
	Private Sub SetLimitsLimitSectorDictionary()
		' *******************************************************************************
		'
		' *******************************************************************************

		Dim thisDataset As RenaissanceDataClass.DSFundType
		Dim SelecteRows As RenaissanceDataClass.DSFundType.tblFundTypeRow()
		Dim thisRow As RenaissanceDataClass.DSFundType.tblFundTypeRow
		Dim Counter As Integer

		LimitsLimitSectorDictionary = New Hashtable	 '  LookupCollection

		thisDataset = MainForm.Load_Table(RenaissanceStandardDatasets.tblFundType, False)
		If (Not (thisDataset Is Nothing)) AndAlso (thisDataset.tblFundType.Rows.Count > 0) Then
			SelecteRows = thisDataset.tblFundType.Select("True", "FundTypeDescription")

			LimitsLimitSectorDictionary.Add(CInt(0), "")
			For Counter = 0 To (SelecteRows.Length - 1)
				thisRow = SelecteRows(Counter)
				LimitsLimitSectorDictionary.Add(CInt(thisRow.FundTypeID), thisRow.FundTypeDescription.ToString)
			Next
		Else
			LimitsLimitSectorDictionary.Add(0, "No Fund Types (Sectors) have been defined.")
		End If

		' LimitsFundDictionary.Sort()

		Call MainForm.SetTblGenericCombo( _
		Combo_LimitsLimitSectorDictionary, _
		RenaissanceStandardDatasets.tblFundType, _
		"FundTypeDescription", _
		"FundTypeID", _
		"", False, True, True, 0)

	End Sub

    ''' <summary>
    ''' Sets the limits limit dealing period dictionary.
    ''' </summary>
	Private Sub SetLimitsLimitDealingPeriodDictionary()
		' *******************************************************************************
		'
		' *******************************************************************************

		Dim thisDataset As RenaissanceDataClass.DSDealingPeriod
		Dim SelecteRows As RenaissanceDataClass.DSDealingPeriod.tblDealingPeriodRow()
		Dim thisRow As RenaissanceDataClass.DSDealingPeriod.tblDealingPeriodRow
		Dim Counter As Integer

		LimitsLimitDealingPeriodDictionary = New Hashtable '  

		thisDataset = MainForm.Load_Table(RenaissanceStandardDatasets.tblDealingPeriod, False)
		If (Not (thisDataset Is Nothing)) AndAlso (thisDataset.tblDealingPeriod.Rows.Count > 0) Then
			SelecteRows = thisDataset.tblDealingPeriod.Select("True", "DealingPeriodDescription")

			LimitsLimitDealingPeriodDictionary.Add(CInt(0), "")
			For Counter = 0 To (SelecteRows.Length - 1)
				thisRow = SelecteRows(Counter)
				LimitsLimitDealingPeriodDictionary.Add(CInt(thisRow.DealingPeriodID), thisRow.DealingPeriodDescription.ToString)
			Next
		Else
			LimitsLimitDealingPeriodDictionary.Add(0, "No Dealing Periods have been defined.")
		End If

		' LimitsFundDictionary.Sort()

		Call MainForm.SetTblGenericCombo( _
		Combo_LimitsLimitDealingPeriodGroup, _
		RenaissanceStandardDatasets.tblDealingPeriod, _
		"DealingPeriodDescription", _
		"DealingPeriodID", _
		"", False, True, True, 0)

	End Sub

    ''' <summary>
    ''' Sets the limits limit gt or less than dictionary.
    ''' </summary>
	Private Sub SetLimitsLimitGtOrLessThanDictionary()
		' *******************************************************************************
		'
		' *******************************************************************************

		Dim thisDataset As RenaissanceDataClass.DSRelationship
		Dim SelecteRows As RenaissanceDataClass.DSRelationship.tblRelationshipRow()
		Dim thisRow As RenaissanceDataClass.DSRelationship.tblRelationshipRow
		Dim Counter As Integer

		LimitsLimitGtOrLessThanDictionary = New Hashtable	 '  

		thisDataset = MainForm.Load_Table(RenaissanceStandardDatasets.tblRelationship, False)
		If (Not (thisDataset Is Nothing)) AndAlso (thisDataset.tblRelationship.Rows.Count > 0) Then
			SelecteRows = thisDataset.tblRelationship.Select("True", "relRelationship")

			For Counter = 0 To (SelecteRows.Length - 1)
				thisRow = SelecteRows(Counter)
				LimitsLimitGtOrLessThanDictionary.Add(CInt(thisRow.relID), thisRow.relRelationship.ToString)
			Next
		Else
			LimitsLimitGtOrLessThanDictionary.Add(0, "No Releationships have been defined.")
		End If

		' LimitsFundDictionary.Sort()

		Call MainForm.SetTblGenericCombo( _
		Combo_LimitsGtOrLessThanGroup, _
		RenaissanceStandardDatasets.tblRelationship, _
		"relRelationship", _
		"relID", _
		"", False, True, False)

	End Sub

    ''' <summary>
    ''' Sets the limits characteristic dictionary.
    ''' </summary>
	Private Sub SetLimitsCharacteristicDictionary()
		' *******************************************************************************
		'
		' *******************************************************************************

		Dim thisDataset As RenaissanceDataClass.DSRiskDataCharacteristic
		Dim SelecteRows As RenaissanceDataClass.DSRiskDataCharacteristic.tblRiskDataCharacteristicRow()
		Dim thisRow As RenaissanceDataClass.DSRiskDataCharacteristic.tblRiskDataCharacteristicRow
		Dim Counter As Integer

		If (LimitsCharacteristicDictionary IsNot Nothing) Then
			LimitsCharacteristicDictionary.Clear()
		Else
			LimitsCharacteristicDictionary = New Hashtable	 '  LookupCollection
		End If

		thisDataset = MainForm.Load_Table(RenaissanceStandardDatasets.tblRiskDataCharacteristic, False)
		If (Not (thisDataset Is Nothing)) AndAlso (thisDataset.tblRiskDataCharacteristic.Rows.Count > 0) Then
			SelecteRows = thisDataset.tblRiskDataCharacteristic.Select("True", "DataCharacteristic")

			' Add Default Row to the Dictionary.

			LimitsCharacteristicDictionary.Add(CInt(0), "")

			' Add Dictionary Entries for all of the Table rows.

			For Counter = 0 To (SelecteRows.Length - 1)
				thisRow = SelecteRows(Counter)
				LimitsCharacteristicDictionary.Add(CInt(thisRow.DataCharacteristicId), thisRow.DataCharacteristic.ToString)
			Next
		Else
			LimitsCharacteristicDictionary.Add(0, "No Characteristics have been defined.")
		End If

		Call MainForm.SetTblGenericCombo( _
		Combo_LimitsCharacteristicDictionary, _
		RenaissanceStandardDatasets.tblRiskDataCharacteristic, _
		"DataCharacteristic", _
		"DataCharacteristicId", _
		"", False, True, True, 0)

	End Sub



#End Region

#Region " Grid Event Code"

    ''' <summary>
    ''' The starting limit type
    ''' </summary>
	Private StartingLimitType As Integer

    ''' <summary>
    ''' Handles the StartEdit event of the Grid_Limits control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="C1.Win.C1FlexGrid.RowColEventArgs"/> instance containing the event data.</param>
	Private Sub Grid_Limits_StartEdit(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles Grid_Limits.StartEdit
		' *******************************************************************************
		'
		' *******************************************************************************

		Select Case e.Col
			Case Grid_Fund
				Grid_Limits.Editor = Me.Combo_LimitsFundDictionary

			Case Grid_Type
				Grid_Limits.Editor = Me.Combo_LimitsLimitTypeDictionary
				StartingLimitType = Grid_Limits.Item(e.Row, e.Col)

			Case Grid_LimitItem
				Select Case CInt(Grid_Limits.Item(e.Row, Grid_Type))
					Case LimitType.InstrumentCapitalUsage
						Grid_Limits.Editor = Nothing ' Me.Combo_LimitsLimitInstrumentDictionary

					Case LimitType.SectorCapitalUsage
						Grid_Limits.Editor = Nothing ' Me.Combo_LimitsLimitSectorDictionary

					Case LimitType.TotalCapitalUsage
						Grid_Limits.Editor = Nothing

					Case LimitType.RealisedDrawdown
						Grid_Limits.Editor = Me.Combo_LimitsLimitInstrumentDictionary

					Case LimitType.InstrumentWeight
						Grid_Limits.Editor = Nothing ' Me.Combo_LimitsLimitInstrumentDictionary

					Case LimitType.SectorWeight
						Grid_Limits.Editor = Nothing ' Me.Combo_LimitsLimitSectorDictionary

					Case LimitType.SpecificInstrumentWeight
						Grid_Limits.Editor = Me.Combo_LimitsLimitInstrumentDictionary

					Case LimitType.SpecificSectorWeight
						Grid_Limits.Editor = Me.Combo_LimitsLimitSectorDictionary

					Case LimitType.NumberOfInstruments
						Grid_Limits.Editor = Nothing

					Case LimitType.SpecificInstrumentCapitalUsage
						Grid_Limits.Editor = Me.Combo_LimitsLimitInstrumentDictionary

					Case LimitType.SpecificSectorCapitalUsage
						Grid_Limits.Editor = Me.Combo_LimitsLimitSectorDictionary

					Case LimitType.SectorGroupWeight
						Grid_Limits.Editor = Nothing

					Case LimitType.SpecificSectorGroupWeight
						Grid_Limits.Editor = Me.Combo_LimitsLimitSectorGroup

					Case LimitType.DealingPeriod
						Grid_Limits.Editor = Me.Combo_LimitsLimitDealingPeriodGroup

					Case LimitType.CurrencyExposure
						Grid_Limits.Editor = Me.Combo_LimitsLimitCurrencyDictionary

					Case LimitType.Characteristic
                        Grid_Limits.Editor = Me.Combo_LimitsCharacteristicDictionary

                    Case LimitType.Ratio_5_10_40
                        Grid_Limits.Editor = Nothing

                    Case Else
                        Grid_Limits.Editor = Nothing

                End Select

			Case Grid_GreaterOrLessThan
				Grid_Limits.Editor = Me.Combo_LimitsGtOrLessThanGroup
                If (CInt(Grid_Limits.Item(e.Row, Grid_Type)) = LimitType.Ratio_5_10_40) Then
                    Grid_Limits.Editor = Nothing
                End If

				'Case Grid_Characteristic
				'  Grid_Limits.Editor = Me.Combo_LimitsCharacteristic
            Case Grid_Limit
                ' Grid_Limits.Editor = LimitsEditor
                If (CInt(Grid_Limits.Item(e.Row, Grid_Type)) = LimitType.Ratio_5_10_40) Then
                    Grid_Limits.Editor = Nothing
                End If

            Case Grid_ID

            Case Grid_Delete

        End Select

	End Sub

    ''' <summary>
    ''' Handles the AfterEdit event of the Grid_Limits control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="C1.Win.C1FlexGrid.RowColEventArgs"/> instance containing the event data.</param>
	Private Sub Grid_Limits_AfterEdit(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles Grid_Limits.AfterEdit
		If Me.InPaint = False Then

			If e.Col = Grid_Type Then

				If StartingLimitType <> CInt(CType(Grid_Limits.Cols(e.Col).Editor, System.Windows.Forms.ComboBox).SelectedValue) Then
					' ********************************************************************
					' If the Limit Type has changed, Set the appropriate Item Cell style.
					' ********************************************************************

					Grid_Limits.SetCellStyle(e.Row, Grid_Limit, Grid_Limits.Styles("Default_LimitFormat"))

					Select Case CInt(Me.Grid_Limits.Item(e.Row, Grid_Type))
						Case LimitType.InstrumentCapitalUsage
							Grid_Limits.SetCellStyle(e.Row, Grid_LimitItem, Grid_Limits.Styles("Normal"))	'  Grid_Limits.Styles("LimitInstrument"))
							Grid_Limits.Item(e.Row, Grid_LimitItem) = 0

						Case LimitType.SectorCapitalUsage
							Grid_Limits.SetCellStyle(e.Row, Grid_LimitItem, Grid_Limits.Styles("Normal"))	' Grid_Limits.Styles("LimitSector"))
							Grid_Limits.Item(e.Row, Grid_LimitItem) = ""

						Case LimitType.TotalCapitalUsage
							Grid_Limits.SetCellStyle(e.Row, Grid_LimitItem, Grid_Limits.Styles("Normal"))
							Grid_Limits.Item(e.Row, Grid_LimitItem) = ""

						Case LimitType.RealisedDrawdown
							Grid_Limits.SetCellStyle(e.Row, Grid_LimitItem, Grid_Limits.Styles("LimitInstrument"))
							Grid_Limits.Item(e.Row, Grid_LimitItem) = 0

						Case LimitType.InstrumentWeight
							Grid_Limits.SetCellStyle(e.Row, Grid_LimitItem, Grid_Limits.Styles("Normal"))	' Grid_Limits.Styles("LimitInstrument"))
							Grid_Limits.Item(e.Row, Grid_LimitItem) = 0

						Case LimitType.SectorWeight
							Grid_Limits.SetCellStyle(e.Row, Grid_LimitItem, Grid_Limits.Styles("Normal"))	' Grid_Limits.Styles("LimitSector"))
							Grid_Limits.Item(e.Row, Grid_LimitItem) = 0

						Case LimitType.SpecificInstrumentWeight
							Grid_Limits.SetCellStyle(e.Row, Grid_LimitItem, Grid_Limits.Styles("LimitInstrument"))
							Grid_Limits.Item(e.Row, Grid_LimitItem) = 0

						Case LimitType.SpecificSectorWeight
							Grid_Limits.SetCellStyle(e.Row, Grid_LimitItem, Grid_Limits.Styles("LimitSector"))
							Grid_Limits.Item(e.Row, Grid_LimitItem) = 0

						Case LimitType.NumberOfInstruments
							Grid_Limits.SetCellStyle(e.Row, Grid_LimitItem, Grid_Limits.Styles("Normal"))
							Grid_Limits.Item(e.Row, Grid_LimitItem) = ""
							Grid_Limits.SetCellStyle(e.Row, Grid_Limit, Grid_Limits.Styles("Limit_IntegerFormat"))

						Case LimitType.SpecificInstrumentCapitalUsage
							Grid_Limits.SetCellStyle(e.Row, Grid_LimitItem, Grid_Limits.Styles("LimitInstrument"))
							Grid_Limits.Item(e.Row, Grid_LimitItem) = 0

						Case LimitType.SpecificSectorCapitalUsage
							Grid_Limits.SetCellStyle(e.Row, Grid_LimitItem, Grid_Limits.Styles("LimitSector"))
							Grid_Limits.Item(e.Row, Grid_LimitItem) = 0

						Case LimitType.SectorGroupWeight
							Grid_Limits.SetCellStyle(e.Row, Grid_LimitItem, Grid_Limits.Styles("Normal"))
							Grid_Limits.Item(e.Row, Grid_LimitItem) = ""

						Case LimitType.SpecificSectorGroupWeight
							Grid_Limits.SetCellStyle(e.Row, Grid_LimitItem, Grid_Limits.Styles("LimitSectorGroup"))
							Grid_Limits.Item(e.Row, Grid_LimitItem) = 0

						Case LimitType.DealingPeriod
							Grid_Limits.SetCellStyle(e.Row, Grid_LimitItem, Grid_Limits.Styles("LimitDealingPeriod"))
							Grid_Limits.Item(e.Row, Grid_LimitItem) = 0

						Case LimitType.CurrencyExposure
							Grid_Limits.SetCellStyle(e.Row, Grid_LimitItem, Grid_Limits.Styles("LimitCurrency"))
							Grid_Limits.Item(e.Row, Grid_LimitItem) = 0

						Case LimitType.Characteristic
							Grid_Limits.SetCellStyle(e.Row, Grid_LimitItem, Grid_Limits.Styles("LimitCharacteristic"))
							Grid_Limits.SetCellStyle(e.Row, Grid_Limit, Grid_Limits.Styles("Limit_PercentageFormat"))
							Grid_Limits.Item(e.Row, Grid_LimitItem) = 0

                        Case LimitType.InstrumentWeight
                            Grid_Limits.SetCellStyle(e.Row, Grid_LimitItem, Grid_Limits.Styles("Normal"))   ' Grid_Limits.Styles("LimitInstrument"))
                            Grid_Limits.Item(e.Row, Grid_LimitItem) = 0

                        Case LimitType.Ratio_5_10_40
                            Grid_Limits.SetCellStyle(e.Row, Grid_LimitItem, Grid_Limits.Styles("Normal"))   ' Grid_Limits.Styles("LimitInstrument"))
                            Grid_Limits.Item(e.Row, Grid_LimitItem) = 0

                        Case Else
                            Grid_Limits.SetCellStyle(e.Row, Grid_LimitItem, Grid_Limits.Styles("Normal"))
                            Grid_Limits.Item(e.Row, Grid_LimitItem) = ""

                    End Select

                    Grid_Limits.Item(e.Row, Grid_Label) = LookupTableValue(MainForm, RenaissanceStandardDatasets.tblLimitType, Grid_Limits.Item(e.Row, Grid_Type), "ltpLabel").ToString
                    'limt value and operator are forced to 0.4 and LessThan for ratio_5_10_40
                    If (CInt(Me.Grid_Limits.Item(e.Row, Grid_Type)) = LimitType.Ratio_5_10_40) Then
                        Grid_Limits.Item(e.Row, Grid_GreaterOrLessThan) = RenaissanceGlobals.Relationship.LessThan
                        Grid_Limits.Item(e.Row, Grid_Limit) = 0.4#
                        Grid_Limits.SetCellStyle(e.Row, Grid_Limit, Grid_Limits.Styles("Limit_PercentageFormat"))
                    Else
                        Grid_Limits.Item(e.Row, Grid_GreaterOrLessThan) = RenaissanceGlobals.Relationship.LessThan
                        Grid_Limits.Item(e.Row, Grid_Limit) = 0
                    End If

                    Application.DoEvents()


                    ' Grid_Limits.SetCellStyle(e.Row, Grid_LimitItem, Grid_Limits.GetCellStyle(e.Row, Grid_LimitItem).ForeColor = System.Drawing.Color.Red)

                End If

            ElseIf e.Col = Grid_Limit Then

                Try
                    If Not (Grid_Limits.Cols(e.Col).Editor Is Nothing) Then
                        If (TypeOf Grid_Limits.Cols(e.Col).Editor Is C1.Win.C1Input.C1NumericEdit) Then
                            Grid_Limits.Item(e.Row, e.Col) = CType(Grid_Limits.Cols(e.Col).Editor, C1.Win.C1Input.C1NumericEdit).Value

                            Application.DoEvents()

                            Grid_Limits.Item(e.Row, e.Col) = CType(Grid_Limits.Cols(e.Col).Editor, C1.Win.C1Input.C1NumericEdit).Value

                            'Dim x As String
                            'x = CType(Grid_Limits.Cols(e.Col).Editor, C1.Win.C1Input.C1NumericEdit).Text

                        End If
                    End If
                Catch ex As Exception
                End Try

            ElseIf e.Col = Grid_Delete Then
                Try
                    If Me.HasDeletePermission Then
                        Me.FormChanged = True
                        Call SetButtonStatus()
                    End If
                Catch ex As Exception
                End Try
            ElseIf e.Col = Grid_ID Then
                Exit Sub
            End If

            ' Set default Relationship if it is blank : happens for new rows only (hopefully)

            If IsNumeric(Grid_Limits.Item(e.Row, Grid_GreaterOrLessThan)) AndAlso (CInt(Grid_Limits.Item(e.Row, Grid_GreaterOrLessThan)) <= 0) Then
                Grid_Limits.Item(e.Row, Grid_GreaterOrLessThan) = RenaissanceGlobals.Relationship.LessThan
            End If

            ' Set button statuses

            If (Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord) Then
                Me.FormChanged = True
                Call SetButtonStatus()
            End If
        End If
	End Sub


#End Region

#Region " Bug hunting "

	' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
	' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
	' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

    ''' <summary>
    ''' Called when [row updating].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
	Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
		Dim a As Integer

		If Not e.Command Is Nothing Then
			a = 1
		End If

		If Not e.Errors Is Nothing Then
			a = 2
		End If
	End Sub

    ''' <summary>
    ''' Called when [row updated].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
	Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
		Dim a As Integer

		If Not e.Command Is Nothing Then
			a = 1
		End If

		If Not e.Errors Is Nothing Then
			a = 2
		End If
	End Sub

    ''' <summary>
    ''' Called when [row fill error].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
	Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
		Dim a As Integer

		a = 1
	End Sub

#End Region



End Class
