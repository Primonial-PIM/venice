' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 11-05-2012
' ***********************************************************************
' <copyright file="frmUserPermissions.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Option Compare Text

Imports System.Data.SqlClient
Imports System.Math
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals


''' <summary>
''' Class frmUserPermissions
''' </summary>
Public Class frmUserPermissions

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmUserPermissions"/> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The LBL currency code
    ''' </summary>
  Friend WithEvents lblCurrencyCode As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit user name
    ''' </summary>
  Friend WithEvents editUserName As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The BTN add
    ''' </summary>
  Friend WithEvents btnAdd As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN delete
    ''' </summary>
  Friend WithEvents btnDelete As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN cancel
    ''' </summary>
  Friend WithEvents btnCancel As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN save
    ''' </summary>
  Friend WithEvents btnSave As System.Windows.Forms.Button
    ''' <summary>
    ''' The combo_ select user
    ''' </summary>
  Friend WithEvents Combo_SelectUser As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label1
    ''' </summary>
  Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The group box1
    ''' </summary>
  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The BTN close
    ''' </summary>
  Friend WithEvents btnClose As System.Windows.Forms.Button
    ''' <summary>
    ''' The grid_ forms
    ''' </summary>
  Friend WithEvents Grid_Forms As C1.Win.C1FlexGrid.C1FlexGrid
    ''' <summary>
    ''' The grid_ reports
    ''' </summary>
  Friend WithEvents Grid_Reports As C1.Win.C1FlexGrid.C1FlexGrid
    ''' <summary>
    ''' The list box_ groups
    ''' </summary>
  Friend WithEvents ListBox_Groups As System.Windows.Forms.CheckedListBox
    ''' <summary>
    ''' The label2
    ''' </summary>
  Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label3
    ''' </summary>
  Friend WithEvents Label3 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label4
    ''' </summary>
  Friend WithEvents Label4 As System.Windows.Forms.Label
    ''' <summary>
    ''' The radio_ person
    ''' </summary>
  Friend WithEvents Radio_Person As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The radio_ group
    ''' </summary>
  Friend WithEvents Radio_Group As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.lblCurrencyCode = New System.Windows.Forms.Label
    Me.editUserName = New System.Windows.Forms.TextBox
    Me.btnAdd = New System.Windows.Forms.Button
    Me.btnDelete = New System.Windows.Forms.Button
    Me.btnCancel = New System.Windows.Forms.Button
    Me.btnSave = New System.Windows.Forms.Button
    Me.Combo_SelectUser = New System.Windows.Forms.ComboBox
    Me.Label1 = New System.Windows.Forms.Label
    Me.GroupBox1 = New System.Windows.Forms.GroupBox
    Me.btnClose = New System.Windows.Forms.Button
    Me.Grid_Forms = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.Grid_Reports = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.ListBox_Groups = New System.Windows.Forms.CheckedListBox
    Me.Label2 = New System.Windows.Forms.Label
    Me.Label3 = New System.Windows.Forms.Label
    Me.Label4 = New System.Windows.Forms.Label
    Me.Radio_Person = New System.Windows.Forms.RadioButton
    Me.Radio_Group = New System.Windows.Forms.RadioButton
    CType(Me.Grid_Forms, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Grid_Reports, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'lblCurrencyCode
    '
    Me.lblCurrencyCode.Location = New System.Drawing.Point(12, 56)
    Me.lblCurrencyCode.Name = "lblCurrencyCode"
    Me.lblCurrencyCode.Size = New System.Drawing.Size(100, 16)
    Me.lblCurrencyCode.TabIndex = 39
    Me.lblCurrencyCode.Text = "Name"
    '
    'editUserName
    '
    Me.editUserName.Location = New System.Drawing.Point(124, 56)
    Me.editUserName.Name = "editUserName"
    Me.editUserName.Size = New System.Drawing.Size(272, 20)
    Me.editUserName.TabIndex = 2
    '
    'btnAdd
    '
    Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnAdd.Location = New System.Drawing.Point(24, 144)
    Me.btnAdd.Name = "btnAdd"
    Me.btnAdd.Size = New System.Drawing.Size(75, 28)
    Me.btnAdd.TabIndex = 4
    Me.btnAdd.Text = "&New"
    '
    'btnDelete
    '
    Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnDelete.Location = New System.Drawing.Point(112, 184)
    Me.btnDelete.Name = "btnDelete"
    Me.btnDelete.Size = New System.Drawing.Size(75, 28)
    Me.btnDelete.TabIndex = 6
    Me.btnDelete.Text = "&Delete"
    '
    'btnCancel
    '
    Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnCancel.Location = New System.Drawing.Point(196, 184)
    Me.btnCancel.Name = "btnCancel"
    Me.btnCancel.Size = New System.Drawing.Size(75, 28)
    Me.btnCancel.TabIndex = 7
    Me.btnCancel.Text = "&Cancel"
    '
    'btnSave
    '
    Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnSave.Location = New System.Drawing.Point(24, 184)
    Me.btnSave.Name = "btnSave"
    Me.btnSave.Size = New System.Drawing.Size(75, 28)
    Me.btnSave.TabIndex = 5
    Me.btnSave.Text = "&Save"
    '
    'Combo_SelectUser
    '
    Me.Combo_SelectUser.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectUser.Location = New System.Drawing.Point(124, 12)
    Me.Combo_SelectUser.Name = "Combo_SelectUser"
    Me.Combo_SelectUser.Size = New System.Drawing.Size(272, 21)
    Me.Combo_SelectUser.TabIndex = 0
    '
    'Label1
    '
    Me.Label1.Location = New System.Drawing.Point(12, 12)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(100, 23)
    Me.Label1.TabIndex = 17
    Me.Label1.Text = "Select"
    '
    'GroupBox1
    '
    Me.GroupBox1.Location = New System.Drawing.Point(12, 44)
    Me.GroupBox1.Name = "GroupBox1"
    Me.GroupBox1.Size = New System.Drawing.Size(384, 4)
    Me.GroupBox1.TabIndex = 77
    Me.GroupBox1.TabStop = False
    Me.GroupBox1.Text = "GroupBox1"
    '
    'btnClose
    '
    Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnClose.Location = New System.Drawing.Point(280, 184)
    Me.btnClose.Name = "btnClose"
    Me.btnClose.Size = New System.Drawing.Size(75, 28)
    Me.btnClose.TabIndex = 8
    Me.btnClose.Text = "&Close"
    '
    'Grid_Forms
    '
    Me.Grid_Forms.ColumnInfo = "10,1,0,0,0,85,Columns:"
    Me.Grid_Forms.Location = New System.Drawing.Point(8, 252)
    Me.Grid_Forms.Name = "Grid_Forms"
    Me.Grid_Forms.Rows.DefaultSize = 17
    Me.Grid_Forms.Size = New System.Drawing.Size(420, 340)
    Me.Grid_Forms.TabIndex = 78
    '
    'Grid_Reports
    '
    Me.Grid_Reports.ColumnInfo = "10,1,0,0,0,85,Columns:"
    Me.Grid_Reports.Location = New System.Drawing.Point(436, 252)
    Me.Grid_Reports.Name = "Grid_Reports"
    Me.Grid_Reports.Rows.DefaultSize = 17
    Me.Grid_Reports.Size = New System.Drawing.Size(260, 340)
    Me.Grid_Reports.TabIndex = 79
    '
    'ListBox_Groups
    '
    Me.ListBox_Groups.Location = New System.Drawing.Point(436, 24)
    Me.ListBox_Groups.Name = "ListBox_Groups"
    Me.ListBox_Groups.Size = New System.Drawing.Size(260, 199)
    Me.ListBox_Groups.TabIndex = 80
    '
    'Label2
    '
    Me.Label2.Location = New System.Drawing.Point(436, 4)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(224, 16)
    Me.Label2.TabIndex = 81
    Me.Label2.Text = "Group Membership"
    '
    'Label3
    '
    Me.Label3.Location = New System.Drawing.Point(436, 236)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(224, 16)
    Me.Label3.TabIndex = 82
    Me.Label3.Text = "Report Permissions"
    '
    'Label4
    '
    Me.Label4.Location = New System.Drawing.Point(12, 236)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(224, 16)
    Me.Label4.TabIndex = 83
    Me.Label4.Text = "Form Permissions"
    '
    'Radio_Person
    '
    Me.Radio_Person.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Radio_Person.Location = New System.Drawing.Point(124, 88)
    Me.Radio_Person.Name = "Radio_Person"
    Me.Radio_Person.Size = New System.Drawing.Size(76, 20)
    Me.Radio_Person.TabIndex = 84
    Me.Radio_Person.Text = "Person"
    '
    'Radio_Group
    '
    Me.Radio_Group.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Radio_Group.Location = New System.Drawing.Point(212, 88)
    Me.Radio_Group.Name = "Radio_Group"
    Me.Radio_Group.Size = New System.Drawing.Size(60, 20)
    Me.Radio_Group.TabIndex = 85
    Me.Radio_Group.Text = "Group"
    '
    'frmUserPermissions
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.CancelButton = Me.btnCancel
    Me.ClientSize = New System.Drawing.Size(704, 601)
    Me.Controls.Add(Me.Radio_Group)
    Me.Controls.Add(Me.Radio_Person)
    Me.Controls.Add(Me.Label4)
    Me.Controls.Add(Me.Label3)
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.ListBox_Groups)
    Me.Controls.Add(Me.Grid_Reports)
    Me.Controls.Add(Me.Grid_Forms)
    Me.Controls.Add(Me.btnClose)
    Me.Controls.Add(Me.GroupBox1)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.Combo_SelectUser)
    Me.Controls.Add(Me.btnSave)
    Me.Controls.Add(Me.editUserName)
    Me.Controls.Add(Me.lblCurrencyCode)
    Me.Controls.Add(Me.btnDelete)
    Me.Controls.Add(Me.btnCancel)
    Me.Controls.Add(Me.btnAdd)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frmUserPermissions"
    Me.Text = "Add/Edit Permissions"
    CType(Me.Grid_Forms, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Grid_Reports, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "


  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
  Private WithEvents _MainForm As VeniceMain

  ' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
  Private FormTooltip As New ToolTip()

  ' Form Constants, specific to the table being updated.
    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
  Private ALWAYS_CLOSE_THIS_FORM As Boolean = True

  ' Form Locals, initialised on 'New' defining what standard data items to use
    ''' <summary>
  ''' Variable holding the form's data table name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_TABLENAME As String
    ''' <summary>
  ''' Variable holding the form's data adaptor name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_ADAPTORNAME As String
    ''' <summary>
  ''' Variable holding the form's dataset name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_DATASETNAME As String


  ' The standard ChangeID for this form. e.g. tblUserPermissions
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

    ''' <summary>
  ''' Principal control used for selecting items on this form. 
    ''' </summary>
  Private THIS_FORM_SelectingCombo As ComboBox
    ''' <summary>
  ''' Control to select after proessing the "New" button.
    ''' </summary>
  Private THIS_FORM_NewMoveToControl As Control

  ' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
  Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
  Private THIS_FORM_FormID As VeniceFormID

  ' Data Structures

    ''' <summary>
    ''' My dataset
    ''' </summary>
	Private myDataset As RenaissanceDataClass.DSUserPermissions		 ' Form Specific !!!!
    ''' <summary>
    ''' My table
    ''' </summary>
	Private myTable As RenaissanceDataClass.DSUserPermissions.tblUserPermissionsDataTable
    ''' <summary>
    ''' My connection
    ''' </summary>
	Private myConnection As SqlConnection
    ''' <summary>
    ''' My adaptor
    ''' </summary>
	Private myAdaptor As SqlDataAdapter

    ''' <summary>
    ''' The this standard dataset
    ''' </summary>
	Private ThisStandardDataset As RenaissanceGlobals.StandardDataset


    ''' <summary>
    ''' The __ is over cancel button
    ''' </summary>
	Private __IsOverCancelButton As Boolean
    ''' <summary>
    ''' The _ in use
    ''' </summary>
	Private _InUse As Boolean

	' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
	Private FormIsValid As Boolean
    ''' <summary>
    ''' The form changed
    ''' </summary>
	Private FormChanged As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
	Private _FormOpenFailed As Boolean
    ''' <summary>
    ''' The in paint
    ''' </summary>
	Private InPaint As Boolean
    ''' <summary>
    ''' The add new record
    ''' </summary>
	Private AddNewRecord As Boolean

	' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
	Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
	Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
	Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
	Private HasDeletePermission As Boolean

    ''' <summary>
    ''' Struct PermissionSet
    ''' </summary>
	Private Structure PermissionSet
        ''' <summary>
        ''' The feature
        ''' </summary>
		Dim Feature As String
        ''' <summary>
        ''' The perm read
        ''' </summary>
		Dim PermRead As Boolean
        ''' <summary>
        ''' The perm update
        ''' </summary>
		Dim PermUpdate As Boolean
        ''' <summary>
        ''' The perm insert
        ''' </summary>
		Dim PermInsert As Boolean
        ''' <summary>
        ''' The perm delete
        ''' </summary>
		Dim PermDelete As Boolean

        ''' <summary>
        ''' Initializes a new instance of the <see cref="PermissionSet"/> struct.
        ''' </summary>
        ''' <param name="pFeature">The p feature.</param>
        ''' <param name="pRead">if set to <c>true</c> [p read].</param>
        ''' <param name="pUpdate">if set to <c>true</c> [p update].</param>
        ''' <param name="pInsert">if set to <c>true</c> [p insert].</param>
        ''' <param name="pDelete">if set to <c>true</c> [p delete].</param>
		Public Sub New(ByVal pFeature As String, ByVal pRead As Boolean, ByVal pUpdate As Boolean, ByVal pInsert As Boolean, ByVal pDelete As Boolean)
			Me.Feature = pFeature
			Me.PermRead = pRead
			Me.PermUpdate = pUpdate
			Me.PermInsert = pInsert
			Me.PermDelete = pDelete
		End Sub

	End Structure

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
	Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
	Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return __IsOverCancelButton
		End Get
		Set(ByVal Value As Boolean)
			__IsOverCancelButton = Value
		End Set
	End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
	Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
		Get
			Return InPaint
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
		Get
			Return _InUse
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

#End Region


    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmUserPermissions"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
	Public Sub New(ByVal pMainForm As VeniceMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()

		_MainForm = pMainForm
		AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

		_FormOpenFailed = False
		_InUse = True

		' ******************************************************
		' Form Specific Settings :
		' ******************************************************

		THIS_FORM_SelectingCombo = Me.Combo_SelectUser
		THIS_FORM_NewMoveToControl = Me.editUserName

		' Form Permissioning :-

		THIS_FORM_PermissionArea = Me.Name
		THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

		' 'This' form ID

		THIS_FORM_FormID = VeniceFormID.frmUserPermissions

		' This form's dataset type.

		ThisStandardDataset = RenaissanceStandardDatasets.tblUserPermissions ' This Defines the Form Data !!! 

		' Format Event Handlers for form controls

		AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler
		AddHandler editUserName.LostFocus, AddressOf MainForm.Text_NotNull

		' Form Control Changed events
		AddHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
		AddHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		AddHandler editUserName.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Grid_Forms.CellChanged, AddressOf Me.FormControlChanged
		AddHandler Grid_Reports.CellChanged, AddressOf Me.FormControlChanged
		AddHandler Me.ListBox_Groups.ItemCheck, AddressOf Me.FormControlChanged

		' Basic Grid Configuration

		Grid_Forms.Cols.Count = 5
		Grid_Forms.Cols.Fixed = 1
		Grid_Forms.Rows.Count = 1
		Grid_Forms.Rows.Fixed = 1

		Grid_Forms.Item(0, 1) = "Read"
		Grid_Forms.Item(0, 2) = "Update"
		Grid_Forms.Item(0, 3) = "Insert"
		Grid_Forms.Item(0, 4) = "Delete"

		Grid_Forms.Cols(1).DataType = GetType(Boolean)
		Grid_Forms.Cols(2).DataType = GetType(Boolean)
		Grid_Forms.Cols(3).DataType = GetType(Boolean)
		Grid_Forms.Cols(4).DataType = GetType(Boolean)

		Grid_Forms.Cols(0).Width = Grid_Forms.Width / 2
		Grid_Forms.Cols(1).Width = Grid_Forms.Width / 9
		Grid_Forms.Cols(2).Width = Grid_Forms.Width / 9
		Grid_Forms.Cols(3).Width = Grid_Forms.Width / 9
		Grid_Forms.Cols(4).Width = Grid_Forms.Width / 9


		Grid_Reports.Cols.Fixed = 1
		Grid_Reports.Cols.Count = 2
		Grid_Reports.Item(0, 1) = "Run"
		Grid_Reports.Cols(1).DataType = GetType(Boolean)

		Grid_Reports.Cols(0).Width = Grid_Reports.Width * 0.7
		Grid_Reports.Cols(1).Width = Grid_Reports.Width * 0.2

		ListBox_Groups.Items.Clear()

		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

		' ******************************************************
		' End Form Specific.
		' ******************************************************

		' Data object names standard to this Form type.

		THIS_TABLENAME = ThisStandardDataset.TableName
		THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
		THIS_DATASETNAME = ThisStandardDataset.DatasetName

		THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

		' Establish / Retrieve data objects for this form.

		myConnection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)
		myAdaptor = MainForm.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, VENICE_CONNECTION, THIS_TABLENAME)
		myDataset = MainForm.Load_Table(ThisStandardDataset, False)
		myTable = myDataset.Tables(0)

	End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

	' Form Initialisation code.
	'
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
	Public Sub ResetForm() Implements StandardVeniceForm.ResetForm

		Call Form_Load(Me, New System.EventArgs)
	End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
	Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
		_FormOpenFailed = False
		_InUse = True

		' Initialise Data structures. Connection, Adaptor and Dataset.

		If Not (MainForm Is Nothing) Then
			FormIsValid = True
		Else
			MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myConnection Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myAdaptor Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myDataset Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		' Initialse form

		InPaint = True
		IsOverCancelButton = False

		' Check User permissions
		Call CheckPermissions()
		If (HasReadPermission = False) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		Call LoadGroupsList()
		Call LoadFormsGrid()
		Call LoadReportsGrid()

		Call SetUsersCombo()

		' Display initial record.

		If THIS_FORM_SelectingCombo.Items.Count > 0 Then
			Me.THIS_FORM_SelectingCombo.SelectedIndex = 0
			Call GetFormData()
		Else
			Me.THIS_FORM_SelectingCombo.SelectedIndex = (-1)
			Call GetFormData()
		End If

		InPaint = False


	End Sub

    ''' <summary>
    ''' Handles the Closing event of the frmUserPermissions control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
	Private Sub frmUserPermissions_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		Dim HideForm As Boolean

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...

		_InUse = False

		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else
			If (FormChanged = True) Then
				Call SetFormData()
			End If

			HideForm = True
			If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.NoCache_CacheCount Then
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			e.Cancel = True
		Else
			Try
				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler
				RemoveHandler editUserName.LostFocus, AddressOf MainForm.Text_NotNull

				' Form Control Changed events
				RemoveHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
				RemoveHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				RemoveHandler editUserName.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Grid_Forms.CellChanged, AddressOf Me.FormControlChanged
				RemoveHandler Grid_Reports.CellChanged, AddressOf Me.FormControlChanged
				RemoveHandler Me.ListBox_Groups.ItemCheck, AddressOf Me.FormControlChanged

			Catch ex As Exception
			End Try
		End If

	End Sub


#End Region

	' Routine to handle changes / updates to tables by this and other windows.
	' If this, or any other, form posts a change to a table, then it will invoke an update event 
	' detailing what tables have been altered.
	' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
	' the 'VeniceAutoUpdate' event of the main Venice form.
	' Each form may them react as appropriate to changes in any table that might impact it.
	'
    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		Dim OrgInPaint As Boolean
		Dim KnowledgeDateChanged As Boolean
		Dim SetButtonStatus_Flag As Boolean
		Dim RefreshForm As Boolean = False

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		OrgInPaint = InPaint
		InPaint = True
		KnowledgeDateChanged = False
		SetButtonStatus_Flag = False

		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
			KnowledgeDateChanged = True
			RefreshForm = True
		End If

		' ****************************************************************
		' Check for changes relevant to this form
		' ****************************************************************

		' Changes to the tblUserPermissions table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

			' Check ongoing permissions.

			Call CheckPermissions()
			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

				FormIsValid = False
				Me.Close()
				Exit Sub
			End If

			SetButtonStatus_Flag = True

		End If


		' ****************************************************************
		' Changes to the Main FORM table :-
		' ****************************************************************

		If (e.TableChanged(THIS_FORM_ChangeID) = True) Or KnowledgeDateChanged Then
			Dim thisName As String
			Dim Counter As Integer

			RefreshForm = True

			thisName = Me.editUserName.Text

			SetUsersCombo()

			If (thisName.Length > 0) Then
				For Counter = 0 To (Combo_SelectUser.Items.Count - 1)
					Combo_SelectUser.SelectedValue = thisName
				Next
			ElseIf (Combo_SelectUser.Items.Count > 0) Then
				Combo_SelectUser.SelectedIndex = 0
				Call LoadGroupsList()
				GetFormData()
			End If

			If Me.FormChanged = False Then
				Call LoadGroupsList()
				GetFormData()
			End If
		End If

		' ****************************************************************
		' Repaint if not currently in Edit Mode
		'
		' ****************************************************************

		InPaint = OrgInPaint

		' Retrieve form data if appropriate, i.e. this data item is not already being edited.
		If (RefreshForm = True) AndAlso (FormChanged = False) AndAlso (AddNewRecord = False) Then
			GetFormData()	' 
		Else
			If SetButtonStatus_Flag Then
				Call SetButtonStatus()
			End If
		End If

	End Sub

#Region " Workhorse functions : CheckPermission / ControlsChanged "

	' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
	Private Sub CheckPermissions()

		Dim Permissions As Integer

		Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

		HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
		HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
		HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
		HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

	End Sub

	' Flag changes to form controls, Event Associations made in 'New' routine.
    ''' <summary>
  ''' Basic event handler, called when a form value is changed.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    If InPaint = False Then
      If (Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord) Then
        FormChanged = True
        Me.btnSave.Enabled = True
        Me.btnCancel.Enabled = True
      End If
    End If

  End Sub

  ''' <summary>
  ''' Basic event handler, called when a form value is changed.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="C1.Win.C1FlexGrid.RowColEventArgs"/> instance containing the event data.</param>
  Private Sub FormControlChanged(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs)

    If InPaint = False Then
      If (Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord) Then
        FormChanged = True
        Me.btnSave.Enabled = True
        Me.btnCancel.Enabled = True
      End If
    End If
  End Sub

  ''' <summary>
  ''' Basic event handler, called when a form value is changed.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="System.Windows.Forms.ItemCheckEventArgs"/> instance containing the event data.</param>
	Private Sub FormControlChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs)

		If InPaint = False Then
			If (Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord) Then
				FormChanged = True
				Me.btnSave.Enabled = True
				Me.btnCancel.Enabled = True
			End If
		End If

	End Sub


#End Region

#Region " Set Form Lists and Grids (Form Specific Code) "

    ''' <summary>
    ''' Gets the type of the permission user.
    ''' </summary>
    ''' <param name="pUserName">Name of the p user.</param>
    ''' <returns>RenaissanceGlobals.PermissionUserType.</returns>
	Private Function GetPermissionUserType(ByVal pUserName As String) As RenaissanceGlobals.PermissionUserType

		Dim PermissionsCommand As New SqlCommand


		PermissionsCommand.Connection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)

		PermissionsCommand.CommandType = CommandType.StoredProcedure
		PermissionsCommand.CommandText = "[fn_GetPermissionUserType]"
		PermissionsCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

		PermissionsCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UserName", System.Data.SqlDbType.VarChar, 50))
		PermissionsCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))

		PermissionsCommand.Parameters("@UserName").Value = pUserName

		Try
			SyncLock PermissionsCommand.Connection
				PermissionsCommand.ExecuteNonQuery()
			End SyncLock
		Catch ex As Exception
			Return PermissionUserType.TypeUser
		End Try

		Return System.Enum.Parse(GetType(PermissionUserType), PermissionsCommand.Parameters("@RETURN_VALUE").Value.ToString)

	End Function

    ''' <summary>
    ''' Sets the users combo.
    ''' </summary>
	Private Sub SetUsersCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_SelectUser, _
		RenaissanceStandardDatasets.tblUserPermissions, _
		"Username", _
		"Username", _
		"", True, True, False)

	End Sub

    ''' <summary>
    ''' Loads the groups list.
    ''' </summary>
	Private Sub LoadGroupsList()
		Dim thisTable As New DataTable
		Dim thisCommand As New SqlCommand
		Dim thisAdaptor As New SqlDataAdapter
		Dim Counter As Integer

		Me.ListBox_Groups.Items.Clear()

		thisCommand.Connection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)

		thisCommand.CommandType = CommandType.StoredProcedure
		thisCommand.CommandText = "[spu_GetPermissionGroupList]"
		thisCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

		thisCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))

		thisAdaptor.SelectCommand = thisCommand

		Try
			SyncLock thisCommand.Connection
				thisAdaptor.Fill(thisTable)
			End SyncLock
		Catch ex As Exception

		End Try

		If thisTable.Rows.Count > 0 Then
			For Counter = 0 To (thisTable.Rows.Count - 1)
				Me.ListBox_Groups.Items.Add(thisTable.Rows(Counter).Item(0).ToString.Trim, False)
			Next
		End If
	End Sub

    ''' <summary>
    ''' Loads the forms grid.
    ''' </summary>
	Private Sub LoadFormsGrid()
		Dim FormNames As String()
		Dim Counter As Integer

		FormNames = System.Enum.GetNames(GetType(VeniceFormID))

		If Me.Grid_Forms.Rows.Count <> (FormNames.Length - 1) Then
			Grid_Forms.Rows.Count = (FormNames.Length - 1)
		End If

		For Counter = 1 To (FormNames.Length - 2)	' Start at One to miss 'None', End One element early to Miss 'Max' Element
			Grid_Forms.Item(Counter, 0) = FormNames(Counter)
			Grid_Forms.Item(Counter, 1) = False
			Grid_Forms.Item(Counter, 2) = False
			Grid_Forms.Item(Counter, 3) = False
			Grid_Forms.Item(Counter, 4) = False
		Next

	End Sub

    ''' <summary>
    ''' Loads the reports grid.
    ''' </summary>
	Private Sub LoadReportsGrid()
		Dim ReportNames As String()
		Dim Counter As Integer

		ReportNames = System.Enum.GetNames(GetType(VeniceReportID))

		If Me.Grid_Reports.Rows.Count <> (ReportNames.Length - 1) Then
			Grid_Reports.Rows.Count = (ReportNames.Length - 1)
		End If

		For Counter = 1 To (ReportNames.Length - 2)	' Start at One to miss 'None', End One element early to Miss 'Max' Element
			Grid_Reports.Item(Counter, 0) = ReportNames(Counter)
			Grid_Reports.Item(Counter, 1) = False
		Next

	End Sub

#End Region

#Region " Get & Set Form Data / SetButton / ValidateForm / btnCancel Events (Form Specific Code) "

    ''' <summary>
    ''' Gets the form data.
    ''' </summary>
	Private Sub GetFormData()
		' Routine to populate form Controls given a data row.
		' Form is cleared for invalid Datarows or Invalid Form.

		Dim thisUserName As String
		Dim thisUserType As PermissionUserType

		Dim OrgInpaint As Boolean
		Dim Counter As Integer

		If (Me.Created) AndAlso (Not Me.IsDisposed) Then

			' Set 'Paint' flag to prevent event actions caused by changing the value of form fields.
			OrgInpaint = InPaint
			InPaint = True

			If THIS_FORM_SelectingCombo.SelectedIndex >= 0 Then
				thisUserName = THIS_FORM_SelectingCombo.SelectedValue
			Else
				thisUserName = ""
			End If

			Me.editUserName.Text = thisUserName
			Me.editUserName.Enabled = False

			thisUserType = GetPermissionUserType(thisUserName)
			If thisUserType = PermissionUserType.TypeUser Then
				Me.Radio_Group.Checked = False
				Me.Radio_Person.Checked = True
			Else
				Me.Radio_Person.Checked = False
				Me.Radio_Group.Checked = True
			End If
			Me.Radio_Person.Enabled = False
			Me.Radio_Group.Enabled = False

			' Uncheck List / Grids

			For Counter = 0 To (ListBox_Groups.Items.Count - 1)
				ListBox_Groups.SetItemCheckState(Counter, CheckState.Unchecked)
			Next

			For Counter = 1 To (Grid_Forms.Rows.Count - 1)
				Grid_Forms.Item(Counter, 1) = False
				Grid_Forms.Item(Counter, 2) = False
				Grid_Forms.Item(Counter, 3) = False
				Grid_Forms.Item(Counter, 4) = False
			Next

			For Counter = 1 To (Grid_Reports.Rows.Count - 1)
				Grid_Reports.Item(Counter, 1) = False
			Next

			If (thisUserName Is Nothing) Or (FormIsValid = False) Or (Me.InUse = False) Then
				' Bad / New Datarow - Clear Form.

			Else
				' Populate Form with given data.
				' Permission Usertypr Enumeration. To match Legacy Venice.

				'Public Enum PermissionUserType
				'      TypeUser = 1
				'      TypeGroup = 2
				'End Enum

				'      ' Permission FeatureType Enumeration.
				'Public Enum PermissionFeatureType
				'      TypeForm = 1
				'      TypeReport = 2
				'      TypeSystem = 5
				'      TypeGroup = 10
				'      TypeUser = 20
				'End Enum

				'      ' Permission Bitmap Enumeration.
				'Public Enum VenicePermissionBitmap
				'      PermNone = 0
				'      PermRead = 1
				'      PermUpdate = 2
				'      PermInsert = 4
				'      PermDelete = 8
				'End Enum

				Dim SelectedRows As RenaissanceDataClass.DSUserPermissions.tblUserPermissionsRow()
				Dim thisRow As RenaissanceDataClass.DSUserPermissions.tblUserPermissionsRow

				SelectedRows = Me.myTable.Select("(UserName='" & thisUserName & "')", "FeatureType")

				If (Not (SelectedRows Is Nothing)) AndAlso (SelectedRows.Length > 0) Then

					For Each thisRow In SelectedRows
						Select Case thisRow.FeatureType
							Case PermissionFeatureType.TypeForm

								For Counter = 1 To (Me.Grid_Forms.Rows.Count - 1)
									If CStr(Grid_Forms.Item(Counter, 0)) = thisRow.Feature.Trim Then
										If thisRow.Perm_Read <> 0 Then
											Grid_Forms.Item(Counter, 1) = True
										End If

										If thisRow.Perm_Update <> 0 Then
											Grid_Forms.Item(Counter, 2) = True
										End If

										If thisRow.Perm_Insert <> 0 Then
											Grid_Forms.Item(Counter, 3) = True
										End If

										If thisRow.Perm_Delete <> 0 Then
											Grid_Forms.Item(Counter, 4) = True
										End If

										Exit For
									End If
								Next

							Case PermissionFeatureType.TypeReport

								For Counter = 1 To (Me.Grid_Reports.Rows.Count - 1)
									If CStr(Grid_Reports.Item(Counter, 0)) = thisRow.Feature.Trim Then
										If thisRow.Perm_Read <> 0 Then
											Grid_Reports.Item(Counter, 1) = True
										End If

										Exit For
									End If
								Next

							Case PermissionFeatureType.TypeGroup

								For Counter = 0 To (Me.ListBox_Groups.Items.Count - 1)
									If CStr(ListBox_Groups.Items(Counter)) = thisRow.Feature.Trim Then
										If thisRow.Perm_Read <> 0 Then
											ListBox_Groups.SetItemCheckState(Counter, CheckState.Checked)
										End If

										Exit For
									End If
								Next

						End Select

					Next

				End If

			End If

			' Allow Field events to trigger before 'InPaint' Is re-set. 
			' (Should) Prevent Validation errors during Form Draw.
			Application.DoEvents()

			' Restore 'Paint' flag.
			InPaint = OrgInpaint
			FormChanged = False
			Me.btnSave.Enabled = False

			' As it says on the can....
			Call SetButtonStatus()

		End If

	End Sub

    ''' <summary>
    ''' Sets the form data.
    ''' </summary>
    ''' <param name="pConfirm">if set to <c>true</c> [p confirm].</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	Private Function SetFormData(Optional ByVal pConfirm As Boolean = True) As Boolean
		' *************************************************************
		'
		' *************************************************************
		Dim ErrMessage As String
		Dim ErrFlag As Boolean
		Dim ErrStack As String
		Dim ProtectedItem As Boolean = False

		ErrMessage = ""
		ErrStack = ""

		' *************************************************************
		' Appropriate Save permission :-
		' *************************************************************

		If (Me.HasUpdatePermission = False) And ((Me.HasInsertPermission = False) Or (Me.AddNewRecord = False)) Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "You do not have permission to save this record.", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' *************************************************************
		' If Save button is disabled then should not be able to save, exit silently.
		' *************************************************************
		If Me.btnSave.Enabled = False Then
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' Confirm Save, if required.

		If (pConfirm = True) Then
			If MessageBox.Show("Save Changes ?", "Save Changes ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
				Return True
				Exit Function
			End If
		End If

		' *************************************************************
		' Procedure to Save the current form information.
		' *************************************************************

		Dim thisUserName As String
		Dim thisUserType As PermissionUserType

		Dim StatusString As String = ""
		Dim UpdateRows(0) As DataRow

		If (FormChanged = False) Or (FormIsValid = False) Then
			Return False
			Exit Function
		End If

		thisUserName = editUserName.Text

		If Me.Radio_Group.Checked Then
			thisUserType = PermissionUserType.TypeGroup
		Else
			thisUserType = PermissionUserType.TypeUser
		End If

		' Validation
		If (ValidateForm(StatusString) = False) Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Validation Error." & vbCrLf & StatusString, "", True)
			Return False
			Exit Function
		End If

		' Set 'Paint' flag.
		InPaint = True

		' *************************************************************
		' Establish temporary arrays of permissions
		'
		' *************************************************************

		Dim FormPermissions(Me.Grid_Forms.Rows.Count - 2) As PermissionSet
		Dim ReportPermissions(Me.Grid_Reports.Rows.Count - 2) As PermissionSet
		Dim GroupPermissions(Me.ListBox_Groups.Items.Count - 1) As String
		Dim Counter As Integer

		For Counter = 1 To (Me.Grid_Forms.Rows.Count - 1)
			FormPermissions(Counter - 1) = New PermissionSet(Grid_Forms.Item(Counter, 0), Grid_Forms.Item(Counter, 1), Grid_Forms.Item(Counter, 2), Grid_Forms.Item(Counter, 3), Grid_Forms.Item(Counter, 4))
		Next

		For Counter = 1 To (Me.Grid_Reports.Rows.Count - 1)
			ReportPermissions(Counter - 1) = New PermissionSet(Grid_Reports.Item(Counter, 0), Grid_Reports.Item(Counter, 1), False, False, False)
		Next

		For Counter = 0 To (Me.ListBox_Groups.Items.Count - 1)
			If ListBox_Groups.GetItemChecked(Counter) Then
				GroupPermissions(Counter) = CStr(ListBox_Groups.Items(Counter)).Trim
			Else
				GroupPermissions(Counter) = ""
			End If
		Next

		' *************************************************************
		' Lock the Data Table, to prevent update conflicts.
		' *************************************************************
		SyncLock myTable

			Dim SelectedRows As RenaissanceDataClass.DSUserPermissions.tblUserPermissionsRow()
			Dim thisRow As RenaissanceDataClass.DSUserPermissions.tblUserPermissionsRow
			Dim SearchCounter As Integer
			Dim FoundFlag As Boolean

			Try
				SelectedRows = Me.myTable.Select("(UserName='" & thisUserName & "')", "FeatureType")

				If (Not (SelectedRows Is Nothing)) AndAlso (SelectedRows.Length > 0) Then
					For Counter = 0 To (SelectedRows.Length - 1)
						thisRow = SelectedRows(Counter)


						Select Case thisRow.FeatureType

							Case PermissionFeatureType.TypeForm

								FoundFlag = False
								For SearchCounter = 0 To (FormPermissions.Length - 1)
									If FormPermissions(SearchCounter).Feature = thisRow.Feature Then
										FoundFlag = True

										If FormPermissions(SearchCounter).PermRead <> CBool(thisRow.Perm_Read) Then
											thisRow.Perm_Read = Abs(CInt(FormPermissions(SearchCounter).PermRead))
										End If

										If FormPermissions(SearchCounter).PermUpdate <> CBool(thisRow.Perm_Update) Then
											thisRow.Perm_Update = Abs(CInt(FormPermissions(SearchCounter).PermUpdate))
										End If

										If FormPermissions(SearchCounter).PermInsert <> CBool(thisRow.Perm_Insert) Then
											thisRow.Perm_Insert = Abs(CInt(FormPermissions(SearchCounter).PermInsert))
										End If

										If FormPermissions(SearchCounter).PermDelete <> CBool(thisRow.Perm_Delete) Then
											thisRow.Perm_Delete = Abs(CInt(FormPermissions(SearchCounter).PermDelete))
										End If

										' Clear this Form Permission line, so that it is not added later.
										FormPermissions(SearchCounter).Feature = ""
										Exit For
									End If
								Next

								If FoundFlag = False Then
									' DS Row not needed.
									thisRow.Delete()
								End If

							Case PermissionFeatureType.TypeReport
								FoundFlag = False
								For SearchCounter = 0 To (ReportPermissions.Length - 1)
									If ReportPermissions(SearchCounter).Feature = thisRow.Feature Then
										FoundFlag = True

										If ReportPermissions(SearchCounter).PermRead <> CBool(thisRow.Perm_Read) Then
											thisRow.Perm_Read = Abs(CInt(ReportPermissions(SearchCounter).PermRead))
										End If

										If ReportPermissions(SearchCounter).PermUpdate <> CBool(thisRow.Perm_Update) Then
											thisRow.Perm_Update = Abs(CInt(ReportPermissions(SearchCounter).PermUpdate))
										End If

										If ReportPermissions(SearchCounter).PermInsert <> CBool(thisRow.Perm_Insert) Then
											thisRow.Perm_Insert = Abs(CInt(ReportPermissions(SearchCounter).PermInsert))
										End If

										If ReportPermissions(SearchCounter).PermDelete <> CBool(thisRow.Perm_Delete) Then
											thisRow.Perm_Delete = Abs(CInt(ReportPermissions(SearchCounter).PermDelete))
										End If

										' Clear this Report Permission line, so that it is not added later.
										ReportPermissions(SearchCounter).Feature = ""
										Exit For
									End If
								Next

								If FoundFlag = False Then
									' DS Row not needed.
									thisRow.Delete()
								End If

							Case PermissionFeatureType.TypeGroup
								FoundFlag = False
								For SearchCounter = 0 To (GroupPermissions.Length - 1)
									If GroupPermissions(SearchCounter) = thisRow.Feature Then
										FoundFlag = True

										If CBool(thisRow.Perm_Read) = False Then
											thisRow.Perm_Read = True
										End If

										GroupPermissions(SearchCounter) = ""
										Exit For
									End If
								Next

								If FoundFlag = False Then
									' DS Row not needed.
									thisRow.Delete()
								End If

							Case PermissionFeatureType.TypeSystem

							Case Else

						End Select

					Next
				End If

				' Now Add new permission rows to the table

				' Forms
				For Counter = 0 To (FormPermissions.Length - 1)
					If (FormPermissions(Counter).Feature.Length > 0) Then
						thisRow = myTable.NewtblUserPermissionsRow

						thisRow.Username = thisUserName
						thisRow.UserType = thisUserType
						thisRow.Feature = FormPermissions(Counter).Feature
						thisRow.FeatureType = PermissionFeatureType.TypeForm
						thisRow.Perm_Read = Abs(CInt(FormPermissions(Counter).PermRead))
						thisRow.Perm_Update = Abs(CInt(FormPermissions(Counter).PermUpdate))
						thisRow.Perm_Insert = Abs(CInt(FormPermissions(Counter).PermInsert))
						thisRow.Perm_Delete = Abs(CInt(FormPermissions(Counter).PermDelete))

						myTable.Rows.Add(thisRow)
					End If
				Next

				' Reports
				For Counter = 0 To (ReportPermissions.Length - 1)
					If (ReportPermissions(Counter).Feature.Length > 0) Then
						thisRow = myTable.NewtblUserPermissionsRow

						thisRow.Username = thisUserName
						thisRow.UserType = thisUserType
						thisRow.Feature = ReportPermissions(Counter).Feature
						thisRow.FeatureType = PermissionFeatureType.TypeReport
						thisRow.Perm_Read = Abs(CInt(ReportPermissions(Counter).PermRead))
						thisRow.Perm_Update = Abs(CInt(ReportPermissions(Counter).PermUpdate))
						thisRow.Perm_Insert = Abs(CInt(ReportPermissions(Counter).PermInsert))
						thisRow.Perm_Delete = Abs(CInt(ReportPermissions(Counter).PermDelete))

						myTable.Rows.Add(thisRow)
					End If
				Next

				' Groups
				For Counter = 0 To (GroupPermissions.Length - 1)
					If (GroupPermissions(Counter).Length > 0) Then
						thisRow = myTable.NewtblUserPermissionsRow

						thisRow.Username = thisUserName
						thisRow.UserType = thisUserType
						thisRow.Feature = GroupPermissions(Counter)
						thisRow.FeatureType = PermissionFeatureType.TypeGroup
						thisRow.Perm_Read = Abs(CInt(True))
						thisRow.Perm_Update = 0
						thisRow.Perm_Insert = 0
						thisRow.Perm_Delete = 0

						myTable.Rows.Add(thisRow)
					End If
				Next

				myAdaptor.Update(myTable)
			Catch ex As Exception
				ErrFlag = True
				ErrMessage = ex.Message
				ErrStack = ex.StackTrace
			End Try

		End SyncLock

		' *************************************************************
		' Error report. Not done in the Try-Catch as that would serve to persist the SyncLock.
		' *************************************************************

		If (ErrFlag = True) Then
			Call MainForm.LogError(Me.Name & ", SetFormData", 0, ErrMessage, "Error Saving Data", ErrStack, True)
			InPaint = False
			Return False
			Exit Function
		End If

		' Finish off

		AddNewRecord = False
		FormChanged = False
		InPaint = False

		Me.THIS_FORM_SelectingCombo.Enabled = True

		' Propogate changes

		Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID))

	End Function

    ''' <summary>
    ''' Sets the button status.
    ''' </summary>
	Private Sub SetButtonStatus()
		' Sets the status of the form controlls appropriate to the current users 
		'permissions and the 'Changed' or 'New' status of the form.

		' No Read permission :-

		If Me.HasReadPermission = False Then
			MainForm.LogError(Me.Name & ", SetButtonStatus()", 0, "", "You do not have Read permission for this Form", "", True)
			Me.Close()
			Exit Sub
		End If

		' Has Insert Permission.
		If Me.HasInsertPermission Then
			Me.btnAdd.Enabled = True
		Else
			Me.btnAdd.Enabled = False
		End If

		' Has Delete permission. 
		If (Me.HasDeletePermission) And (Me.AddNewRecord = False) Then
			Me.btnDelete.Enabled = True
		Else
			Me.btnDelete.Enabled = False
		End If

		If (Me.MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And (Me.AddNewRecord) Then

			Me.editUserName.Enabled = True
			Me.Radio_Group.Enabled = True
			Me.Radio_Person.Enabled = True

		Else

			Me.editUserName.Enabled = False
			Me.Radio_Group.Enabled = False
			Me.Radio_Person.Enabled = False

		End If

	End Sub

    ''' <summary>
    ''' Validates the form.
    ''' </summary>
    ''' <param name="pReturnString">The p return string.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	Private Function ValidateForm(ByRef pReturnString As String) As Boolean
		' Form Validation code.
		' 
		' This code should be the final arbiter of what is allowed. no assumptions regarding 
		' prior validation should be made.
		' 
		' This Code is called by the SetFormData routine before position changes.
		'
		Dim RVal As Boolean

		RVal = True
		pReturnString = ""

		' Validate 

		If Me.editUserName.Text.Length <= 0 Then
			pReturnString = "Name must not be left blank."
			RVal = False
		End If

		Return RVal

	End Function

    ''' <summary>
    ''' Handles the MouseEnter event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnCancel_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseEnter
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = True
	End Sub

    ''' <summary>
    ''' Handles the MouseLeave event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnCancel_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseLeave
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = False
	End Sub


#End Region

#Region " Navigation Code / GetPosition() (Generic Code) "

    ''' <summary>
    ''' Handles the SelectComboChanged event of the Combo control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_SelectComboChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_SelectUser.SelectedIndexChanged

		' Selection Combo. SelectedItem changed.
		'

		' Don't react to changes made in paint routines etc.
		If InPaint = True Then Exit Sub

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		Call GetFormData()

	End Sub




#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

    ''' <summary>
    ''' Handles the Click event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
		' Cancel Changes, redisplay form.

		If AddNewRecord = True Then
			Me.editUserName.Text = ""
		End If

		FormChanged = False
		AddNewRecord = False

		Me.THIS_FORM_SelectingCombo.Enabled = True

		If THIS_FORM_SelectingCombo.Items.Count > 0 Then
			THIS_FORM_SelectingCombo.SelectedIndex = 0
		End If

		GetFormData()

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnSave control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
		' Save Changes, if any, without prompting.

		If (FormChanged = True) Then
			Call SetFormData(False)
		End If

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnDelete control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
		Dim thisUserName As String
		Dim Counter As Integer

		If (AddNewRecord = True) Then
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Sub
		End If

		' No Appropriate Save permission :-

		If (Me.HasDeletePermission = False) Then
			MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "You do not have permission to Delete this record.", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Sub
		End If


		' *************************************************************
		' Confirm :-
		' *************************************************************
		If MsgBox("Are you Sure ?", MsgBoxStyle.OKCancel, "Confirm Delete") = MsgBoxResult.Cancel Then
			Exit Sub
		End If

		thisUserName = Me.editUserName.Text

		Dim thisRow As RenaissanceDataClass.DSUserPermissions.tblUserPermissionsRow

		Try
			SyncLock myTable
				For Counter = (myTable.Rows.Count - 1) To 0 Step -1
					thisRow = myTable.Rows(Counter)

					If thisRow.Username = thisUserName Then
						myTable.Rows(Counter).Delete()
					End If

				Next

				myAdaptor.Update(myTable)

			End SyncLock
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Deleting Permissions for " & thisUserName, ex.StackTrace, True)
		End Try


		' Tidy Up.

		FormChanged = False
		Me.editUserName.Text = ""

		Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID))

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnAdd control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
		' Prepare form to Add a new record.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		InPaint = True
		MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
		InPaint = False

		GetFormData()
		AddNewRecord = True
		Me.btnCancel.Enabled = True
		Me.THIS_FORM_SelectingCombo.Enabled = False

		Call SetButtonStatus()

		THIS_FORM_NewMoveToControl.Focus()

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnClose control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
		' Close Form

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		Me.Close()

	End Sub

#End Region

#Region " Bug hunting "

  ' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
  ' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
  ' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

    ''' <summary>
    ''' Called when [row updating].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

    ''' <summary>
    ''' Called when [row updated].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

    ''' <summary>
    ''' Called when [row fill error].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
    Dim a As Integer

    a = 1
  End Sub

#End Region


End Class
