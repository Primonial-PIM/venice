﻿' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 08-15-2013
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmSelectInstruments.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals

Imports RenaissanceDataClass
Imports C1.Win.C1FlexGrid

''' <summary>
''' Class frmSelectInstruments
''' </summary>
Public Class frmSelectInstruments

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmSelectInstruments"/> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The combo_ instrument ISIN
    ''' </summary>
  Friend WithEvents Combo_InstrumentISIN As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label_ cpty is fund
    ''' </summary>
  Friend WithEvents label_CptyIsFund As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ instrument type
    ''' </summary>
  Friend WithEvents Combo_InstrumentType As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label2
    ''' </summary>
  Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The grid_ instruments
    ''' </summary>
  Friend WithEvents Grid_Instruments As C1.Win.C1FlexGrid.C1FlexGrid
    ''' <summary>
    ''' The combo_ field select1
    ''' </summary>
  Friend WithEvents Combo_FieldSelect1 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select1_ operator
    ''' </summary>
  Friend WithEvents Combo_Select1_Operator As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select1_ value
    ''' </summary>
  Friend WithEvents Combo_Select1_Value As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select2_ value
    ''' </summary>
  Friend WithEvents Combo_Select2_Value As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ field select2
    ''' </summary>
  Friend WithEvents Combo_FieldSelect2 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select2_ operator
    ''' </summary>
  Friend WithEvents Combo_Select2_Operator As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select3_ value
    ''' </summary>
  Friend WithEvents Combo_Select3_Value As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ field select3
    ''' </summary>
  Friend WithEvents Combo_FieldSelect3 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select3_ operator
    ''' </summary>
  Friend WithEvents Combo_Select3_Operator As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ and or_1
    ''' </summary>
  Friend WithEvents Combo_AndOr_1 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ and or_2
    ''' </summary>
  Friend WithEvents Combo_AndOr_2 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The root menu
    ''' </summary>
  Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
    ''' <summary>
    ''' The form_ status strip
    ''' </summary>
  Friend WithEvents Form_StatusStrip As System.Windows.Forms.StatusStrip
    ''' <summary>
    ''' The form_ progress bar
    ''' </summary>
  Friend WithEvents Form_ProgressBar As System.Windows.Forms.ToolStripProgressBar
    ''' <summary>
    ''' The label_ status
    ''' </summary>
  Friend WithEvents Label_Status As System.Windows.Forms.ToolStripStatusLabel
    ''' <summary>
    ''' The text_ instrument description
    ''' </summary>
  Friend WithEvents Text_InstrumentDescription As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The label1
    ''' </summary>
  Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.Grid_Instruments = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.Combo_InstrumentISIN = New System.Windows.Forms.ComboBox
    Me.label_CptyIsFund = New System.Windows.Forms.Label
    Me.Combo_InstrumentType = New System.Windows.Forms.ComboBox
    Me.Label2 = New System.Windows.Forms.Label
    Me.Combo_Select1_Operator = New System.Windows.Forms.ComboBox
    Me.Combo_FieldSelect1 = New System.Windows.Forms.ComboBox
    Me.Combo_Select1_Value = New System.Windows.Forms.ComboBox
    Me.Combo_Select2_Value = New System.Windows.Forms.ComboBox
    Me.Combo_FieldSelect2 = New System.Windows.Forms.ComboBox
    Me.Combo_Select2_Operator = New System.Windows.Forms.ComboBox
    Me.Combo_Select3_Value = New System.Windows.Forms.ComboBox
    Me.Combo_FieldSelect3 = New System.Windows.Forms.ComboBox
    Me.Combo_Select3_Operator = New System.Windows.Forms.ComboBox
    Me.Combo_AndOr_1 = New System.Windows.Forms.ComboBox
    Me.Combo_AndOr_2 = New System.Windows.Forms.ComboBox
    Me.RootMenu = New System.Windows.Forms.MenuStrip
    Me.Form_StatusStrip = New System.Windows.Forms.StatusStrip
    Me.Form_ProgressBar = New System.Windows.Forms.ToolStripProgressBar
    Me.Label_Status = New System.Windows.Forms.ToolStripStatusLabel
    Me.Label1 = New System.Windows.Forms.Label
    Me.Text_InstrumentDescription = New System.Windows.Forms.TextBox
    CType(Me.Grid_Instruments, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Form_StatusStrip.SuspendLayout()
    Me.SuspendLayout()
    '
    'Grid_Instruments
    '
    Me.Grid_Instruments.AllowEditing = False
    Me.Grid_Instruments.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_Instruments.AutoClipboard = True
    Me.Grid_Instruments.CausesValidation = False
    Me.Grid_Instruments.ColumnInfo = "5,1,0,0,0,85,Columns:"
    Me.Grid_Instruments.EditOptions = C1.Win.C1FlexGrid.EditFlags.None
    Me.Grid_Instruments.KeyActionTab = C1.Win.C1FlexGrid.KeyActionEnum.MoveAcross
    Me.Grid_Instruments.Location = New System.Drawing.Point(4, 223)
    Me.Grid_Instruments.Name = "Grid_Instruments"
    Me.Grid_Instruments.Rows.DefaultSize = 17
    Me.Grid_Instruments.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.ListBox
    Me.Grid_Instruments.Size = New System.Drawing.Size(880, 372)
    Me.Grid_Instruments.TabIndex = 18
    '
    'Combo_InstrumentISIN
    '
    Me.Combo_InstrumentISIN.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_InstrumentISIN.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_InstrumentISIN.Location = New System.Drawing.Point(128, 32)
    Me.Combo_InstrumentISIN.Name = "Combo_InstrumentISIN"
    Me.Combo_InstrumentISIN.Size = New System.Drawing.Size(752, 21)
    Me.Combo_InstrumentISIN.TabIndex = 0
    '
    'label_CptyIsFund
    '
    Me.label_CptyIsFund.Location = New System.Drawing.Point(16, 36)
    Me.label_CptyIsFund.Name = "label_CptyIsFund"
    Me.label_CptyIsFund.Size = New System.Drawing.Size(104, 16)
    Me.label_CptyIsFund.TabIndex = 82
    Me.label_CptyIsFund.Text = "ISIN"
    '
    'Combo_InstrumentType
    '
    Me.Combo_InstrumentType.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_InstrumentType.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_InstrumentType.Location = New System.Drawing.Point(128, 60)
    Me.Combo_InstrumentType.Name = "Combo_InstrumentType"
    Me.Combo_InstrumentType.Size = New System.Drawing.Size(752, 21)
    Me.Combo_InstrumentType.TabIndex = 1
    '
    'Label2
    '
    Me.Label2.Location = New System.Drawing.Point(16, 64)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(104, 16)
    Me.Label2.TabIndex = 84
    Me.Label2.Text = "Instrument Type"
    '
    'Combo_Select1_Operator
    '
    Me.Combo_Select1_Operator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_Select1_Operator.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Select1_Operator.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like"})
    Me.Combo_Select1_Operator.Location = New System.Drawing.Point(272, 127)
    Me.Combo_Select1_Operator.Name = "Combo_Select1_Operator"
    Me.Combo_Select1_Operator.Size = New System.Drawing.Size(96, 21)
    Me.Combo_Select1_Operator.TabIndex = 7
    '
    'Combo_FieldSelect1
    '
    Me.Combo_FieldSelect1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_FieldSelect1.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_FieldSelect1.Location = New System.Drawing.Point(128, 127)
    Me.Combo_FieldSelect1.Name = "Combo_FieldSelect1"
    Me.Combo_FieldSelect1.Size = New System.Drawing.Size(136, 21)
    Me.Combo_FieldSelect1.Sorted = True
    Me.Combo_FieldSelect1.TabIndex = 6
    '
    'Combo_Select1_Value
    '
    Me.Combo_Select1_Value.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Select1_Value.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Select1_Value.Location = New System.Drawing.Point(376, 127)
    Me.Combo_Select1_Value.Name = "Combo_Select1_Value"
    Me.Combo_Select1_Value.Size = New System.Drawing.Size(216, 21)
    Me.Combo_Select1_Value.TabIndex = 8
    '
    'Combo_Select2_Value
    '
    Me.Combo_Select2_Value.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Select2_Value.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Select2_Value.Location = New System.Drawing.Point(376, 159)
    Me.Combo_Select2_Value.Name = "Combo_Select2_Value"
    Me.Combo_Select2_Value.Size = New System.Drawing.Size(216, 21)
    Me.Combo_Select2_Value.TabIndex = 12
    '
    'Combo_FieldSelect2
    '
    Me.Combo_FieldSelect2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_FieldSelect2.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_FieldSelect2.Location = New System.Drawing.Point(128, 159)
    Me.Combo_FieldSelect2.Name = "Combo_FieldSelect2"
    Me.Combo_FieldSelect2.Size = New System.Drawing.Size(136, 21)
    Me.Combo_FieldSelect2.Sorted = True
    Me.Combo_FieldSelect2.TabIndex = 10
    '
    'Combo_Select2_Operator
    '
    Me.Combo_Select2_Operator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_Select2_Operator.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Select2_Operator.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like"})
    Me.Combo_Select2_Operator.Location = New System.Drawing.Point(272, 159)
    Me.Combo_Select2_Operator.Name = "Combo_Select2_Operator"
    Me.Combo_Select2_Operator.Size = New System.Drawing.Size(96, 21)
    Me.Combo_Select2_Operator.TabIndex = 11
    '
    'Combo_Select3_Value
    '
    Me.Combo_Select3_Value.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Select3_Value.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Select3_Value.Location = New System.Drawing.Point(376, 191)
    Me.Combo_Select3_Value.Name = "Combo_Select3_Value"
    Me.Combo_Select3_Value.Size = New System.Drawing.Size(216, 21)
    Me.Combo_Select3_Value.TabIndex = 16
    '
    'Combo_FieldSelect3
    '
    Me.Combo_FieldSelect3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_FieldSelect3.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_FieldSelect3.Location = New System.Drawing.Point(128, 191)
    Me.Combo_FieldSelect3.Name = "Combo_FieldSelect3"
    Me.Combo_FieldSelect3.Size = New System.Drawing.Size(136, 21)
    Me.Combo_FieldSelect3.Sorted = True
    Me.Combo_FieldSelect3.TabIndex = 14
    '
    'Combo_Select3_Operator
    '
    Me.Combo_Select3_Operator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_Select3_Operator.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Select3_Operator.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like"})
    Me.Combo_Select3_Operator.Location = New System.Drawing.Point(272, 191)
    Me.Combo_Select3_Operator.Name = "Combo_Select3_Operator"
    Me.Combo_Select3_Operator.Size = New System.Drawing.Size(96, 21)
    Me.Combo_Select3_Operator.TabIndex = 15
    '
    'Combo_AndOr_1
    '
    Me.Combo_AndOr_1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_AndOr_1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_AndOr_1.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_AndOr_1.Items.AddRange(New Object() {"AND", "OR"})
    Me.Combo_AndOr_1.Location = New System.Drawing.Point(604, 127)
    Me.Combo_AndOr_1.Name = "Combo_AndOr_1"
    Me.Combo_AndOr_1.Size = New System.Drawing.Size(96, 21)
    Me.Combo_AndOr_1.TabIndex = 9
    '
    'Combo_AndOr_2
    '
    Me.Combo_AndOr_2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_AndOr_2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_AndOr_2.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_AndOr_2.Items.AddRange(New Object() {"AND", "OR"})
    Me.Combo_AndOr_2.Location = New System.Drawing.Point(604, 159)
    Me.Combo_AndOr_2.Name = "Combo_AndOr_2"
    Me.Combo_AndOr_2.Size = New System.Drawing.Size(96, 21)
    Me.Combo_AndOr_2.TabIndex = 13
    '
    'RootMenu
    '
    Me.RootMenu.Location = New System.Drawing.Point(0, 0)
    Me.RootMenu.Name = "RootMenu"
    Me.RootMenu.Size = New System.Drawing.Size(888, 24)
    Me.RootMenu.TabIndex = 104
    Me.RootMenu.Text = " "
    '
    'Form_StatusStrip
    '
    Me.Form_StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Form_ProgressBar, Me.Label_Status})
    Me.Form_StatusStrip.Location = New System.Drawing.Point(0, 600)
    Me.Form_StatusStrip.Name = "Form_StatusStrip"
    Me.Form_StatusStrip.Size = New System.Drawing.Size(888, 22)
    Me.Form_StatusStrip.TabIndex = 105
    Me.Form_StatusStrip.Text = " "
    '
    'Form_ProgressBar
    '
    Me.Form_ProgressBar.Maximum = 20
    Me.Form_ProgressBar.Name = "Form_ProgressBar"
    Me.Form_ProgressBar.Size = New System.Drawing.Size(150, 17)
    Me.Form_ProgressBar.Step = 1
    Me.Form_ProgressBar.Visible = False
    '
    'Label_Status
    '
    Me.Label_Status.Name = "Label_Status"
    Me.Label_Status.Size = New System.Drawing.Size(10, 17)
    Me.Label_Status.Text = " "
    '
    'Label1
    '
    Me.Label1.Location = New System.Drawing.Point(16, 95)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(104, 16)
    Me.Label1.TabIndex = 109
    Me.Label1.Text = "Description"
    '
    'Text_InstrumentDescription
    '
    Me.Text_InstrumentDescription.Location = New System.Drawing.Point(128, 92)
    Me.Text_InstrumentDescription.Name = "Text_InstrumentDescription"
    Me.Text_InstrumentDescription.Size = New System.Drawing.Size(752, 20)
    Me.Text_InstrumentDescription.TabIndex = 110
    '
    'frmSelectInstruments
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(888, 622)
    Me.Controls.Add(Me.Text_InstrumentDescription)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.Form_StatusStrip)
    Me.Controls.Add(Me.RootMenu)
    Me.Controls.Add(Me.Combo_AndOr_2)
    Me.Controls.Add(Me.Combo_AndOr_1)
    Me.Controls.Add(Me.Combo_Select3_Value)
    Me.Controls.Add(Me.Combo_FieldSelect3)
    Me.Controls.Add(Me.Combo_Select3_Operator)
    Me.Controls.Add(Me.Combo_Select2_Value)
    Me.Controls.Add(Me.Combo_FieldSelect2)
    Me.Controls.Add(Me.Combo_Select2_Operator)
    Me.Controls.Add(Me.Combo_Select1_Value)
    Me.Controls.Add(Me.Combo_FieldSelect1)
    Me.Controls.Add(Me.Combo_Select1_Operator)
    Me.Controls.Add(Me.Combo_InstrumentType)
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.Combo_InstrumentISIN)
    Me.Controls.Add(Me.label_CptyIsFund)
    Me.Controls.Add(Me.Grid_Instruments)
    Me.Name = "frmSelectInstruments"
    Me.Text = "Select Instruments"
    CType(Me.Grid_Instruments, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Form_StatusStrip.ResumeLayout(False)
    Me.Form_StatusStrip.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "

  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
  Private WithEvents _MainForm As VeniceMain

  ' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
  Private FormTooltip As New ToolTip()

  ' Form Menu


  ' Form Constants, specific to the table being updated.

    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
  Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

  ' Form Locals, initialised on 'New' defining what standard data items to use
    ''' <summary>
  ''' Variable holding the form's data table name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_TABLENAME As String
    ''' <summary>
  ''' Variable holding the form's data adaptor name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_ADAPTORNAME As String
    ''' <summary>
  ''' Variable holding the form's dataset name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_DATASETNAME As String


  ' The standard ChangeID for this form. e.g. tblFund
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

  ' Form Specific Order fields
    ''' <summary>
  ''' Field Name to order the Select combo by.
    ''' </summary>
  Private THIS_FORM_OrderBy As String

  ' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
  Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
  Private THIS_FORM_FormID As VeniceFormID

  ' Data Structures

    ''' <summary>
    ''' My dataset
    ''' </summary>
  Private myDataset As DataSet
    ''' <summary>
    ''' My table
    ''' </summary>
  Private myTable As DataTable
    ''' <summary>
    ''' My connection
    ''' </summary>
  Private myConnection As SqlConnection
    ''' <summary>
    ''' My adaptor
    ''' </summary>
  Private myAdaptor As SqlDataAdapter
    ''' <summary>
    ''' My data view
    ''' </summary>
  Private myDataView As DataView

    ''' <summary>
    ''' The this standard dataset
    ''' </summary>
  Private ThisStandardDataset As RenaissanceGlobals.StandardDataset

  ' Active Element.

    ''' <summary>
    ''' The __ is over cancel button
    ''' </summary>
  Private __IsOverCancelButton As Boolean
    ''' <summary>
    ''' The _ in use
    ''' </summary>
  Private _InUse As Boolean

  ' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
  Private FormIsValid As Boolean
    ''' <summary>
    ''' The in paint
    ''' </summary>
  Private InPaint As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
  Private _FormOpenFailed As Boolean

  ' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
  Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
  Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
  Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
  Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
  Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
    ' Public property to return handle to the 'Main' Venice form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
  Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
    ' Public property maintaining a value indicating if the cursor is over the 'Cancel'
    ' Button on this form.
    ' This property is specifically designed for use by the field formating Event functions
    ' In order that they do not impose format restrictions if the user is about to click the 
    ' 'Cancel' button.
    '
    Get
      Return __IsOverCancelButton
    End Get
    Set(ByVal Value As Boolean)
      __IsOverCancelButton = Value
    End Set
  End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
  Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
    Get
      Return InPaint
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
    Get
      Return _InUse
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmSelectInstruments"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  Public Sub New(ByVal pMainForm As VeniceMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()


    _MainForm = pMainForm
    AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

    _FormOpenFailed = False
    _InUse = True

    ' Default Select and Order fields.

    THIS_FORM_OrderBy = "InstrumentFund, InstrumentInstrument"

    ' Form Permissioning :-

    THIS_FORM_PermissionArea = Me.Name
    THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

    ' 'This' form ID

    THIS_FORM_FormID = VeniceFormID.frmSelectInstruments

    ' This form's dataset type.

    ThisStandardDataset = RenaissanceStandardDatasets.tblInstrument ' This Defines the Form Data !!! 


    ' Data object names standard to this Form type.

    THIS_TABLENAME = ThisStandardDataset.TableName
    THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
    THIS_DATASETNAME = ThisStandardDataset.DatasetName

    THIS_FORM_ChangeID = ThisStandardDataset.ChangeID


    ' Establish / Retrieve data objects for this form.

    myConnection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)
    myAdaptor = MainForm.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, VENICE_CONNECTION, THIS_TABLENAME)
    myDataset = MainForm.Load_Table(ThisStandardDataset, False)
    myTable = myDataset.Tables(0)

    ' Grid Fields
    Dim FieldsMenu As ToolStripMenuItem = SetFieldSelectMenu(RootMenu)

    ' Establish initial DataView and Initialise Instruments Grig.

    Dim thisCol As C1.Win.C1FlexGrid.Column
    Dim Counter As Integer

    Try
      myDataView = New DataView(myTable, "False", "", DataViewRowState.CurrentRows)

      Grid_Instruments.DataSource = myDataView

      ' Format Instruments Grid, hide unwanted columns.

      Grid_Instruments.Cols("InstrumentDescription").Move(1)
      Grid_Instruments.Cols("InstrumentID").Move(2)
      Grid_Instruments.Cols("InstrumentCode").Move(3)
      Grid_Instruments.Cols("InstrumentCurrency").Move(4)
      Grid_Instruments.Cols("InstrumentType").Move(5)
      Grid_Instruments.Cols("InstrumentISIN").Move(6)
      Grid_Instruments.Cols("InstrumentExchangeCode").Move(7)
      Grid_Instruments.Cols("InstrumentMorningstar").Move(8)
      Grid_Instruments.Cols("InstrumentSEDOL").Move(9)
      Grid_Instruments.Cols("InstrumentLastValidTradeDate").Move(10)
      Grid_Instruments.Cols("InstrumentCapturePrice").Move(11)
      Grid_Instruments.Cols("InstrumentExpiryDate").Move(12)
      ' Grid_Instruments.Cols("InstrumentStrike").Move(13)
    Catch ex As Exception
    End Try

    For Each thisCol In Grid_Instruments.Cols
      Try
        Select Case thisCol.Name
          Case "InstrumentID"
            thisCol.Format = "###0"
          Case "InstrumentDescription"
          Case "InstrumentCode"
          Case "InstrumentCurrency"
          Case "InstrumentType"
            thisCol.Format = "#,##0"
          Case "InstrumentISIN"
          Case "InstrumentExchangeCode"
          Case "InstrumentMorningstar"
          Case "InstrumentSEDOL"
          Case "InstrumentLastValidTradeDate"
            thisCol.Format = DISPLAYMEMBER_DATEFORMAT
          Case "InstrumentCapturePrice"
          Case "InstrumentExpiryDate"
          Case "InstrumentStrike"
            thisCol.Format = "#,##0.0000"

          Case Else
            thisCol.Visible = False

            Try
              If thisCol.DataType Is GetType(Date) Then
                thisCol.Format = DISPLAYMEMBER_DATEFORMAT
              ElseIf thisCol.DataType Is GetType(Integer) Then
                thisCol.Format = "#,##0"
              ElseIf thisCol.DataType Is GetType(Double) Then
                thisCol.Format = "#,##0.0000"
              End If
            Catch ex As Exception
            End Try
        End Select

        If (thisCol.Caption.ToUpper.StartsWith("Instrument")) Then
          thisCol.Caption = thisCol.Caption.Substring(10)
        End If

        If (thisCol.Visible) Then
          Try
            CType(FieldsMenu.DropDownItems("Menu_GridField_" & thisCol.Name), ToolStripMenuItem).Checked = True
          Catch ex As Exception
          End Try
        End If
      Catch ex As Exception
      End Try
    Next

    Try
      For Counter = 0 To (Grid_Instruments.Cols.Count - 1)
        If (Grid_Instruments.Cols(Counter).Caption.ToUpper.StartsWith("Instrument")) Then
          Grid_Instruments.Cols(Counter).Caption = Grid_Instruments.Cols(Counter).Caption.Substring(10)
        End If
      Next
    Catch ex As Exception
    End Try

    ' Initialise Field select area

    Try
      Call BuildFieldSelectCombos()
      Me.Combo_AndOr_1.SelectedIndex = 0
      Me.Combo_AndOr_2.SelectedIndex = 0
    Catch ex As Exception
    End Try

    ' Form Control Changed events
    AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

    AddHandler Combo_InstrumentISIN.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_InstrumentType.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Text_InstrumentDescription.TextChanged, AddressOf Me.FormControlChanged

    AddHandler Combo_Select1_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Select1_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Select1_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp
    AddHandler Combo_Select2_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Select2_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Select2_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp
    AddHandler Combo_Select3_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Select3_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Select3_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp

    AddHandler Combo_AndOr_1.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_AndOr_2.SelectedValueChanged, AddressOf Me.FormControlChanged

    AddHandler Combo_InstrumentISIN.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_InstrumentType.KeyUp, AddressOf MainForm.ComboSelectAsYouType

    AddHandler Combo_InstrumentISIN.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_InstrumentType.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

    AddHandler Combo_InstrumentISIN.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_InstrumentType.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

    AddHandler Combo_InstrumentISIN.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_InstrumentType.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

    ' Set up the ToolTip
    MainForm.SetFormToolTip(Me, FormTooltip)

  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
  Public Sub ResetForm() Implements StandardVeniceForm.ResetForm
    THIS_FORM_OrderBy = "InstrumentFund, InstrumentInstrument"

    Call Form_Load(Me, New System.EventArgs)
  End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
  Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
    ALWAYS_CLOSE_THIS_FORM = True
    Me.Close()
  End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
    _FormOpenFailed = False
    _InUse = True

    ' Initialise Data structures. Connection, Adaptor and Dataset.

    If Not (MainForm Is Nothing) Then
      FormIsValid = True
    Else
      MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (myConnection Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (myAdaptor Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (myDataset Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If


    ' Check User permissions
    Try
      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

        FormIsValid = False
        _FormOpenFailed = True

        Exit Sub
      End If
    Catch ex As Exception
      FormIsValid = False
      _FormOpenFailed = True

      Exit Sub
    End Try

    ' Initialse form

    InPaint = True
    IsOverCancelButton = False

    ' Initialise main select controls
    ' Build Sorted data list from which this form operates

    Try
      Call SetISINCombo()
      Call SetInstrumentTypeCombo()

      If (Combo_InstrumentISIN.Items.Count > 0) Then
        Me.Combo_InstrumentISIN.SelectedIndex = 0
      Else
        Me.Combo_InstrumentISIN.SelectedIndex = -1
      End If

      Me.Combo_InstrumentType.SelectedIndex = -1
      Text_InstrumentDescription.Text = ""

      Me.Combo_FieldSelect1.SelectedIndex = 0
      Me.Combo_FieldSelect2.SelectedIndex = 0
      Me.Combo_FieldSelect3.SelectedIndex = 0

      ' Default Grid Sort : By Value Date and ParentID (Effectively Entry Order)
      For Each thisCol As C1.Win.C1FlexGrid.Column In Grid_Instruments.Cols
        thisCol.Sort = SortFlags.None
      Next
      Grid_Instruments.Cols("InstrumentDescription").Sort = SortFlags.Ascending
      Grid_Instruments.Sort(SortFlags.UseColSort, Grid_Instruments.Cols("InstrumentDescription").Index)

      Call SetSortedRows()

      Call MainForm.SetComboSelectionLengths(Me)
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error initialising Select Instrument Form.", ex.StackTrace, True)
    End Try

    InPaint = False

  End Sub

    ''' <summary>
    ''' Handles the Closing event of the frm control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
  Private Sub frm_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    Dim HideForm As Boolean

    ' Hide or Close this form ?
    ' All depends on how many of this form type are Open or in Cache...

    _InUse = False

    If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
      HideForm = False
    Else
      HideForm = True
      If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
        HideForm = False
      End If
    End If

    If HideForm = True Then
      MainForm.HideInFormsCollection(Me)
      Me.Hide() ' NPP Fix

      e.Cancel = True
    Else
      Try
        MainForm.RemoveFromFormsCollection(Me)
        RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

        RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

        RemoveHandler Combo_InstrumentISIN.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_InstrumentType.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Text_InstrumentDescription.TextChanged, AddressOf Me.FormControlChanged

        RemoveHandler Combo_Select1_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_Select1_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_Select1_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp
        RemoveHandler Combo_Select2_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_Select2_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_Select2_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp
        RemoveHandler Combo_Select3_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_Select3_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_Select3_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp

        RemoveHandler Combo_AndOr_1.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_AndOr_2.SelectedValueChanged, AddressOf Me.FormControlChanged

        RemoveHandler Combo_InstrumentISIN.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_InstrumentType.KeyUp, AddressOf MainForm.ComboSelectAsYouType
   
        RemoveHandler Combo_InstrumentISIN.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_InstrumentType.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
 
        RemoveHandler Combo_InstrumentISIN.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_InstrumentType.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

        RemoveHandler Combo_InstrumentISIN.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_InstrumentType.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
   
      Catch ex As Exception
      End Try
    End If

  End Sub


#End Region

  ' Routine to handle changes / updates to tables by this and other windows.
  ' If this, or any other, form posts a change to a table, then it will invoke an update event 
  ' detailing what tables have been altered.
  ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
  ' the 'VeniceAutoUpdate' event of the main Venice form.
  ' Each form may them react as appropriate to changes in any table that might impact it.
  '
    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    Dim OrgInPaint As Boolean
    Dim KnowledgeDateChanged As Boolean
    Dim RefreshGrid As Boolean

    If (Not Me.Created) OrElse (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then
      Exit Sub
    End If

    OrgInPaint = InPaint

    Try
      InPaint = True
      KnowledgeDateChanged = False
      RefreshGrid = False

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
        KnowledgeDateChanged = True
      End If
    Catch ex As Exception
    End Try

    ' ****************************************************************
    ' Check for changes relevant to this form
    ' ****************************************************************

    ' Changes to the tblInstrumentType table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrumentType) = True) Or KnowledgeDateChanged Then
        Call SetInstrumentTypeCombo()
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblInstrumentType", ex.StackTrace, True)
    End Try

    ' Changes to the tblInstrument table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrument) = True) Or KnowledgeDateChanged Then
        Call SetISINCombo()
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblInstrument", ex.StackTrace, True)
    End Try

    ' Changes to the KnowledgeDate :-
    Try
      If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
        RefreshGrid = True
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: KnowledgeDate", ex.StackTrace, True)
    End Try

    ' Check TableUpdate against first Select Field
    Try
      If Me.Combo_FieldSelect1.SelectedIndex >= 0 Then
        Try

          ' If the First FieldSelect combo has selected a Instrument Field which is related to
          ' another table, as defined in tblReferentialIntegrity (e.g. InstrumentCounterparty)
          ' Then if that related table is updated, the FieldSelect-Values combo must be updated.

          ' If ChangedID is ChangeID of related table then ,..

          If (e.TableChanged(GetFieldChangeID(Combo_FieldSelect1.SelectedItem)) = True) Or KnowledgeDateChanged Then
            Dim SelectedValue As Object
            Dim TextValue As String

            ' Save current Value Combo Value.

            SelectedValue = Nothing
            TextValue = ""

            If Me.Combo_Select1_Value.SelectedIndex >= 0 Then
              SelectedValue = Me.Combo_Select1_Value.SelectedValue
            ElseIf (SelectedValue Is Nothing) Then
              TextValue = Me.Combo_Select1_Value.Text
            End If

            ' Update FieldSelect-Values Combo.

            Call UpdateSelectedValueCombo(Me.Combo_FieldSelect1.SelectedItem, Me.Combo_Select1_Value)

            ' Restore Saved Value.

            If (Not (SelectedValue Is Nothing)) Then
              Me.Combo_Select1_Value.SelectedValue = SelectedValue
            ElseIf TextValue.Length > 0 Then
              Me.Combo_Select1_Value.Text = TextValue
            End If
          End If
        Catch ex As Exception
        End Try
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: First SelectField", ex.StackTrace, True)
    End Try

    ' Check TableUpdate against second Select Field
    Try
      If Me.Combo_FieldSelect2.SelectedIndex >= 0 Then
        Try
          ' GetFieldChangeID
          If (e.TableChanged(GetFieldChangeID(Combo_FieldSelect2.SelectedItem)) = True) Or KnowledgeDateChanged Then
            Dim SelectedValue As Object
            Dim TextValue As String

            SelectedValue = Nothing
            TextValue = ""

            If Me.Combo_Select2_Value.SelectedIndex >= 0 Then
              SelectedValue = Me.Combo_Select2_Value.SelectedValue
            ElseIf (SelectedValue Is Nothing) Then
              TextValue = Me.Combo_Select2_Value.Text
            End If

            Call UpdateSelectedValueCombo(Me.Combo_FieldSelect2.SelectedItem, Me.Combo_Select2_Value)

            If (Not (SelectedValue Is Nothing)) Then
              Me.Combo_Select2_Value.SelectedValue = SelectedValue
            ElseIf TextValue.Length > 0 Then
              Me.Combo_Select2_Value.Text = TextValue
            End If
          End If
        Catch ex As Exception
        End Try
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: Second SelectField", ex.StackTrace, True)
    End Try

    ' Check TableUpdate against third Select Field
    Try
      If Me.Combo_FieldSelect3.SelectedIndex >= 0 Then
        Try
          ' GetFieldChangeID
          If (e.TableChanged(GetFieldChangeID(Combo_FieldSelect3.SelectedItem)) = True) Or KnowledgeDateChanged Then
            Dim SelectedValue As Object
            Dim TextValue As String

            SelectedValue = Nothing
            TextValue = ""

            If Me.Combo_Select3_Value.SelectedIndex >= 0 Then
              SelectedValue = Me.Combo_Select3_Value.SelectedValue
            ElseIf (SelectedValue Is Nothing) Then
              TextValue = Me.Combo_Select3_Value.Text
            End If

            Call UpdateSelectedValueCombo(Me.Combo_FieldSelect3.SelectedItem, Me.Combo_Select3_Value)

            If (Not (SelectedValue Is Nothing)) Then
              Me.Combo_Select3_Value.SelectedValue = SelectedValue
            ElseIf TextValue.Length > 0 Then
              Me.Combo_Select3_Value.Text = TextValue
            End If
          End If
        Catch ex As Exception
        End Try
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: Third SelectField", ex.StackTrace, True)
    End Try


    ' Changes to the tblUserPermissions table :-

    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

        ' Check ongoing permissions.

        Call CheckPermissions()
        If (HasReadPermission = False) Then
          Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

          FormIsValid = False
          InPaint = OrgInPaint
          Me.Close()
          Exit Sub
        End If

        RefreshGrid = True

      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: tblUserPermissions", ex.StackTrace, True)
    End Try


    ' ****************************************************************
    ' Changes to the Main FORM table :-
    ' (or the tblInstruments table)
    ' ****************************************************************

    Try
      If (e.TableChanged(THIS_FORM_ChangeID) = True) Or _
         (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrument) = True) Or _
         (RefreshGrid = True) Or _
         KnowledgeDateChanged Then

        ' Re-Set Controls etc.
        Call SetSortedRows(True)

      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: Main Form table", ex.StackTrace, True)
    End Try

    InPaint = OrgInPaint

  End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

    ''' <summary>
    ''' Gets the instrument select string.
    ''' </summary>
    ''' <param name="OnlyUsertblInstrumentFields">if set to <c>true</c> [only usertbl instrument fields].</param>
    ''' <returns>System.String.</returns>
  Private Function GetInstrumentSelectString(Optional ByVal OnlyUsertblInstrumentFields As Boolean = False) As String
    ' *******************************************************************************
    ' Build a Select String appropriate to the form status.
    ' *******************************************************************************

    Dim SelectString As String
    Dim FieldSelectString As String

    SelectString = ""
    FieldSelectString = ""
    Me.MainForm.SetToolStripText(Label_Status, "")

    Try

      ' Select on ISIN...

      If (Me.Combo_InstrumentISIN.SelectedIndex > 0) Then
        If IsFieldInstrumentField("InstrumentISIN") = True Then
          SelectString = "(InstrumentISIN='" & Combo_InstrumentISIN.SelectedValue.ToString & "')"
        End If
      End If

      ' Select on Instrument Type

      If (Me.Combo_InstrumentType.SelectedIndex > 0) Then
        If IsFieldInstrumentField("InstrumentType") = True Then
          If (SelectString.Length > 0) Then
            SelectString &= " AND "
          End If
          SelectString &= "(InstrumentType=" & Combo_InstrumentType.SelectedValue.ToString & ")"
        End If
      End If


      ' Select on Trade Status Group
      ' Combo_TradeStatusOperator
      ' Combo_TradeStatusGroup

      If (Text_InstrumentDescription.Text.Length > 0) Then
        If (SelectString.Length > 0) Then
          SelectString &= " AND "
        End If

        SelectString &= "(InstrumentDescription LIKE '%" & Text_InstrumentDescription.Text & "%')"
      End If

      ' Field Select ...

      FieldSelectString = "("
      Dim InstrumentDataset As New RenaissanceDataClass.DSInstrument
      Dim InstrumentTable As RenaissanceDataClass.DSInstrument.tblInstrumentDataTable = InstrumentDataset.tblInstrument
      Dim FieldName As String

      ' Don't need the data, just the column defs.
      'InstrumentDataset = MainForm.Load_Table(RenaissanceStandardDatasets.tblInstrument, False)
      'InstrumentTable = myDataset.Tables(0)

      ' Select Field One.

      Try
        If (Me.Combo_FieldSelect1.SelectedIndex > 0) AndAlso (Me.Combo_Select1_Operator.SelectedIndex > 0) And (Me.Combo_Select1_Value.Text.Length > 0) Then
          ' Get Field Name, Add 'Instrument' back on if necessary.

          FieldName = Combo_FieldSelect1.SelectedItem
          If (FieldName.EndsWith(".")) Then
            FieldName = "Instrument" & FieldName.Substring(0, FieldName.Length - 1)
          End If

          If (OnlyUsertblInstrumentFields = False) OrElse (IsFieldInstrumentField(FieldName) = True) Then
            ' If the Selected Field Name is a String Field ...

            If InstrumentTable.Columns.Contains(FieldName) Then
              If (InstrumentTable.Columns(FieldName).DataType Is GetType(System.String)) Then

                ' If there is a value Selected, Use it - else use the Combo Text.

                If (Not (Combo_Select1_Value.SelectedValue Is Nothing)) Then
                  FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.Text & " '" & Combo_Select1_Value.SelectedValue.ToString & "')"
                Else
                  FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.Text & " '" & Combo_Select1_Value.Text.ToString & "')"
                End If

              ElseIf (InstrumentTable.Columns(FieldName).DataType Is GetType(System.DateTime)) Then

                ' For Dates, If there is no 'Selected' value use the Combo text if it is a Valid date, else
                ' Use the 'Selected' Value if it is a date.

                If (Me.Combo_Select1_Value.SelectedValue Is Nothing) AndAlso (IsDate(Me.Combo_Select1_Value.Text)) Then
                  FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select1_Value.Text).ToString(QUERY_SHORTDATEFORMAT) & "#)"
                ElseIf (Not (Me.Combo_Select1_Value.SelectedValue Is Nothing)) And IsDate(Combo_Select1_Value.SelectedValue) Then
                  FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select1_Value.SelectedValue).ToString(QUERY_SHORTDATEFORMAT) & "#)"
                Else
                  FieldSelectString &= "(true)"
                End If

              Else
                ' Now Deemed to be numeric, or at least not in need of quotes / delimeters...
                ' If there is no 'Selected' value use the Combo text if it is a Valid number, else
                ' Use the 'Selected' Value if it is a valid numeric.

                If (Not (Combo_Select1_Value.SelectedValue Is Nothing)) AndAlso (IsNumeric(Combo_Select1_Value.SelectedValue)) Then
                  FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.SelectedItem.ToString & " " & Combo_Select1_Value.SelectedValue.ToString & ")"
                ElseIf IsNumeric(Combo_Select1_Value.Text) Then
                  FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.SelectedItem.ToString & " " & Combo_Select1_Value.Text & ")"
                Else
                  FieldSelectString &= "(true)"
                End If
              End If
            End If

          End If
        End If

      Catch ex As Exception
      End Try

      ' Select Field Two.

      Try
        If (Me.Combo_FieldSelect2.SelectedIndex > 0) AndAlso (Me.Combo_Select2_Operator.SelectedIndex > 0) And (Me.Combo_Select2_Value.Text.Length > 0) Then
          FieldName = Combo_FieldSelect2.SelectedItem
          If (FieldName.EndsWith(".")) Then
            FieldName = "Instrument" & FieldName.Substring(0, FieldName.Length - 1)
          End If

          If (OnlyUsertblInstrumentFields = False) OrElse (IsFieldInstrumentField(FieldName) = True) Then

            If (FieldSelectString.Length > 1) Then
              FieldSelectString &= " " & Me.Combo_AndOr_1.SelectedItem.ToString & " "
            End If

            If (InstrumentTable.Columns(FieldName).DataType Is GetType(System.String)) Then

              If (Not (Combo_Select2_Value.SelectedValue Is Nothing)) Then
                FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.Text & " '" & Combo_Select2_Value.SelectedValue.ToString & "')"
              Else
                FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.Text & " '" & Combo_Select2_Value.Text.ToString & "')"
              End If

            ElseIf (InstrumentTable.Columns(FieldName).DataType Is GetType(System.DateTime)) Then

              If (Me.Combo_Select2_Value.SelectedValue Is Nothing) AndAlso (IsDate(Me.Combo_Select2_Value.Text)) Then
                FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select2_Value.Text).ToString(QUERY_SHORTDATEFORMAT) & "#)"
              ElseIf (Not (Me.Combo_Select2_Value.SelectedValue Is Nothing)) And IsDate(Combo_Select2_Value.SelectedValue) Then
                FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select2_Value.SelectedValue).ToString(QUERY_SHORTDATEFORMAT) & "#)"
              Else
                FieldSelectString &= "(true)"
              End If

            Else
              If (Not (Combo_Select2_Value.SelectedValue Is Nothing)) AndAlso (IsNumeric(Combo_Select2_Value.SelectedValue)) Then
                FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.SelectedItem.ToString & " " & Combo_Select2_Value.SelectedValue.ToString & ")"
              ElseIf IsNumeric(Combo_Select2_Value.Text) Then
                FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.SelectedItem.ToString & " " & Combo_Select2_Value.Text & ")"
              Else
                FieldSelectString &= "(true)"
              End If
            End If
          End If
        End If
      Catch ex As Exception
      End Try


      ' Select Field Three.

      Try
        If (Me.Combo_FieldSelect3.SelectedIndex > 0) AndAlso (Me.Combo_Select3_Operator.SelectedIndex > 0) And (Me.Combo_Select3_Value.Text.Length > 0) Then
          FieldName = Combo_FieldSelect3.SelectedItem
          If (FieldName.EndsWith(".")) Then
            FieldName = "Instrument" & FieldName.Substring(0, FieldName.Length - 1)
          End If

          If (OnlyUsertblInstrumentFields = False) OrElse (IsFieldInstrumentField(FieldName) = True) Then

            If (FieldSelectString.Length > 1) Then
              FieldSelectString &= " " & Me.Combo_AndOr_2.SelectedItem.ToString & " "
            End If

            If (InstrumentTable.Columns(FieldName).DataType Is GetType(System.String)) Then

              If (Not (Combo_Select3_Value.SelectedValue Is Nothing)) Then
                FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.Text & " '" & Combo_Select3_Value.SelectedValue.ToString & "')"
              Else
                FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.Text & " '" & Combo_Select3_Value.Text.ToString & "')"
              End If

            ElseIf (InstrumentTable.Columns(FieldName).DataType Is GetType(System.DateTime)) Then

              If (Me.Combo_Select3_Value.SelectedValue Is Nothing) AndAlso (IsDate(Me.Combo_Select3_Value.Text)) Then
                FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select3_Value.Text).ToString(QUERY_SHORTDATEFORMAT) & "#)"
              ElseIf (Not (Me.Combo_Select3_Value.SelectedValue Is Nothing)) And IsDate(Combo_Select3_Value.SelectedValue) Then
                FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select3_Value.SelectedValue).ToString(QUERY_SHORTDATEFORMAT) & "#)"
              Else
                FieldSelectString &= "(true)"
              End If

            Else
              If (Not (Combo_Select3_Value.SelectedValue Is Nothing)) AndAlso (IsNumeric(Combo_Select3_Value.SelectedValue)) Then
                FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.SelectedItem.ToString & " " & Combo_Select3_Value.SelectedValue.ToString & ")"
              ElseIf IsNumeric(Combo_Select3_Value.Text) Then
                FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.SelectedItem.ToString & " " & Combo_Select3_Value.Text & ")"
              Else
                FieldSelectString &= "(true)"
              End If
            End If
          End If
        End If
      Catch ex As Exception
      End Try

      FieldSelectString &= ")"

      ' If the FieldSelect string is used, then tag it on to the main select string.

      If FieldSelectString.Length > 2 Then
        If (SelectString.Length > 0) Then
          SelectString &= " AND "
        End If
        SelectString &= FieldSelectString
      End If

      If SelectString.Length <= 0 Then
        SelectString = "true"
      End If

    Catch ex As Exception
      SelectString = "true"
    End Try

    Return SelectString

  End Function

    ''' <summary>
    ''' Determines whether [is field instrument field] [the specified field name].
    ''' </summary>
    ''' <param name="FieldName">Name of the field.</param>
    ''' <returns><c>true</c> if [is field instrument field] [the specified field name]; otherwise, <c>false</c>.</returns>
  Private Function IsFieldInstrumentField(ByVal FieldName As String) As Boolean
    ' *******************************************************************************
    ' Simple function to return a boolean value indicating whether or not a column name is
    ' part of the tblInstruments table.
    ' *******************************************************************************

    Try
      Dim InstrumentTable As New RenaissanceDataClass.DSInstrument.tblInstrumentDataTable

      If (InstrumentTable Is Nothing) Then
        Return False
      Else
        Return InstrumentTable.Columns.Contains(FieldName)
      End If
    Catch ex As Exception
      Return (False)
    End Try

    Return (False)

  End Function

    ''' <summary>
    ''' Sets the sorted rows.
    ''' </summary>
    ''' <param name="ForceRefresh">if set to <c>true</c> [force refresh].</param>
  Private Sub SetSortedRows(Optional ByVal ForceRefresh As Boolean = False)
    ' *******************************************************************************
    ' Build a Select String appropriate to the form status and apply it to the DataView object.
    ' *******************************************************************************

    Dim SelectString As String

    If (Me.Created) And (Not Me.IsDisposed) Then

      SelectString = GetInstrumentSelectString()

      Try

        If ForceRefresh Then
          myDataView.RowFilter = "False"
        End If
        If (myDataView IsNot Nothing) AndAlso (Not (myDataView.RowFilter = SelectString)) Then
          myDataView.RowFilter = SelectString
        End If

      Catch ex As Exception
        Try
          SelectString = "true"
          myDataView.RowFilter = SelectString
        Catch ex_inner As Exception
        End Try

      End Try

      Grid_Instruments.ScrollPosition = New Drawing.Point(0, 0)

      Me.Label_Status.Text = "(" & myDataView.Count.ToString & " Records) " & SelectString



    End If

  End Sub


  ' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
  Private Sub CheckPermissions()
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

  End Sub

    ''' <summary>
  ''' Basic event handler, called when a form value is changed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' *******************************************************************************
    ' In the event of one of the Form Controls changing, refresh the data grid.
    ' *******************************************************************************

    If InPaint = False Then

      Call SetSortedRows()

    End If
  End Sub


    ''' <summary>
    ''' Handles the KeyUp event of the Combo_FieldSelect control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
  Private Sub Combo_FieldSelect_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Combo_Select1_Value.KeyUp
    ' *******************************************************************************
    ' In the event of one of the Form Controls changing, refresh the data grid.
    ' *******************************************************************************

    If InPaint = False Then

      Call SetSortedRows()

    End If
  End Sub


    ''' <summary>
    ''' Builds the field select combos.
    ''' </summary>
  Private Sub BuildFieldSelectCombos()
    ' *********************************************************************************
    ' Build the FieldSelect combo boxes.
    '
    ' Field Names starting with 'Instrument' are modified to start with '.'
    ' This is done in order to make the name more readable.
    '
    ' *********************************************************************************

    Dim ColumnName As String
    Dim thisColumn As DataColumn

    Combo_FieldSelect1.Items.Clear()
    Combo_Select1_Value.Items.Clear()
    Combo_FieldSelect1.Items.Add("")

    Combo_FieldSelect2.Items.Clear()
    Combo_Select2_Value.Items.Clear()
    Combo_FieldSelect2.Items.Add("")

    Combo_FieldSelect3.Items.Clear()
    Combo_Select3_Value.Items.Clear()
    Combo_FieldSelect3.Items.Add("")

    For Each thisColumn In myTable.Columns
      Try
        ColumnName = thisColumn.ColumnName
        If (ColumnName.StartsWith("Instrument")) Then
          ColumnName = ColumnName.Substring(10) & "."
        End If

        Combo_FieldSelect1.Items.Add(ColumnName)
        Combo_FieldSelect2.Items.Add(ColumnName)
        Combo_FieldSelect3.Items.Add(ColumnName)

      Catch ex As Exception
      End Try
    Next

  End Sub

    ''' <summary>
    ''' Updates the selected value combo.
    ''' </summary>
    ''' <param name="pFieldName">Name of the p field.</param>
    ''' <param name="pValueCombo">The p value combo.</param>
  Private Sub UpdateSelectedValueCombo(ByVal pFieldName As String, ByRef pValueCombo As ComboBox)
    ' *******************************************************************************
    ' Build a Value Combo Box appropriate to the chosen DataTable Select Field.
    '
    ' By default the combo will contain all existing values for the chosen field, however
    ' If a relationship is defined for thi table and field, then a Combo will be built
    ' which reflects this relationship.
    ' e.g. the tblReferentialIntegrity table defined a relationship between the
    ' InstrumentCounterparty field and the tblCounterparty.Counterparty field, thus if
    ' the chosen Select field ti 'Counterparty.' then the Value combo will be built using
    ' the CounterpartyID & CounterpartyName details from the tblCounterparty table.
    '
    ' *******************************************************************************

    Dim tblRefIntegrity As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityDataTable
    Dim IntegrityRows() As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityRow
    Dim InstrumentDataset As DataSet
    Dim InstrumentTable As DataTable
    Dim SortOrder As Boolean

    SortOrder = True

    ' Clear the Value Combo.

    Try
      pValueCombo.DataSource = Nothing
      pValueCombo.DisplayMember = ""
      pValueCombo.ValueMember = ""
      pValueCombo.Items.Clear()
    Catch ex As Exception
    End Try

    ' By default, make the Combo a Standard Dropdown, so that non-list items can be entered.

    pValueCombo.DropDownStyle = ComboBoxStyle.DropDown

    ' Exit if no FieldName is given (having cleared the combo).

    If (pFieldName.Length <= 0) Then Exit Sub

    ' Adjust the Field name if appropriate.

    If (pFieldName.EndsWith(".")) Then
      pFieldName = "Instrument" & pFieldName.Substring(0, pFieldName.Length - 1)
    End If

    ' Get a handle to the Standard Instruments Table.
    ' This is so that the field types can be determined and used.

    Try
      InstrumentDataset = MainForm.Load_Table(RenaissanceStandardDatasets.tblInstrument, False)
      InstrumentTable = myDataset.Tables(0)
    Catch ex As Exception
      Exit Sub
    End Try

    ' If the selected field is a Date then sort the selection combo in reverse order.

    If (InstrumentTable.Columns(pFieldName).DataType Is GetType(System.DateTime)) Then
      SortOrder = False
    End If

    ' Get a handle to the Referential Integrity table and the row matching this table and field.
    ' this table defines a relationship between Instrument Table fields and other tables.
    ' This information can be used to build more meaningfull Value Combos.

    Try
      tblRefIntegrity = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblReferentialIntegrity, False), RenaissanceDataClass.DSReferentialIntegrity).tblReferentialIntegrity
      IntegrityRows = tblRefIntegrity.Select("(FeedsTable LIKE '%tblInstrument') AND (FeedsField = '" & pFieldName & "')", "RN")
      If (IntegrityRows.Length <= 0) Then
        GoTo StandardExit
      End If

    Catch ex As Exception
      Exit Sub
    End Try

    If (IntegrityRows(0).IsDescriptionFieldNull) Then
      ' No Linked Description Field

      GoTo StandardExit
    End If

    ' OK, a referential record exists.
    ' Determine the Table and Field Names to use to build the Value Combo.

    Dim TableName As String
    Dim TableField As String
    Dim DescriptionField As String
    Dim stdDS As StandardDataset
    Dim thisChangeID As RenaissanceChangeID

    Try

      TableName = IntegrityRows(0).TableName
      TableField = IntegrityRows(0).TableField
      DescriptionField = IntegrityRows(0).DescriptionField

      thisChangeID = CType(System.Enum.Parse(GetType(RenaissanceChangeID), TableName), RenaissanceChangeID)

      stdDS = RenaissanceStandardDatasets.GetStandardDataset(thisChangeID)

      ' Build the appropriate Values Combo.

      Call MainForm.SetTblGenericCombo( _
      pValueCombo, _
      stdDS, _
      DescriptionField, _
      TableField, _
      "", False, SortOrder, True)    ' 

      ' For Referential Integrity generated combo's, make the Combo a 
      ' DropDownList

      pValueCombo.DropDownStyle = ComboBoxStyle.DropDownList

    Catch ex As Exception
      GoTo StandardExit

    End Try

    Exit Sub

StandardExit:

    ' Build the standard Combo, just use the values from the given field.

    Try

      Call MainForm.SetTblGenericCombo( _
       pValueCombo, _
       RenaissanceStandardDatasets.tblInstrument, _
       pFieldName, _
       pFieldName, _
       "", True, SortOrder, False)    ' 

      MainForm.ClearComboSelection(pValueCombo)

    Catch ex As Exception
    End Try

  End Sub

    ''' <summary>
    ''' Gets the field change ID.
    ''' </summary>
    ''' <param name="pFieldName">Name of the p field.</param>
    ''' <returns>RenaissanceChangeID.</returns>
  Private Function GetFieldChangeID(ByVal pFieldName As String) As RenaissanceChangeID
    ' ***************************************************************************************
    '
    '
    ' ***************************************************************************************

    Dim tblRefIntegrity As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityDataTable
    Dim IntegrityRows() As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityRow

    If (pFieldName.EndsWith(".")) Then
      pFieldName = "Instrument" & pFieldName.Substring(0, pFieldName.Length - 1)
    End If

    Try
      tblRefIntegrity = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblReferentialIntegrity, False), RenaissanceDataClass.DSReferentialIntegrity).tblReferentialIntegrity
      IntegrityRows = tblRefIntegrity.Select("(FeedsTable LIKE '%tblInstrument') AND (FeedsField = '" & pFieldName & "')", "RN")
      If (IntegrityRows.Length <= 0) Then
        Return RenaissanceChangeID.None
        Exit Function
      End If

    Catch ex As Exception
      Return RenaissanceChangeID.None
      Exit Function
    End Try


    If (IntegrityRows(0).IsDescriptionFieldNull) Then
      Return RenaissanceChangeID.None
      Exit Function
    End If

    ' OK, a referential record exists.
    ' Determine the Table and Field Names to use to build the Value Combo.

    Dim TableName As String
    Dim TableField As String
    Dim DescriptionField As String
    Dim thisChangeID As RenaissanceChangeID

    Try

      TableName = IntegrityRows(0).TableName
      TableField = IntegrityRows(0).TableField
      DescriptionField = IntegrityRows(0).DescriptionField

      thisChangeID = CType(System.Enum.Parse(GetType(RenaissanceChangeID), TableName), RenaissanceChangeID)

      Return thisChangeID
      Exit Function
    Catch ex As Exception
    End Try

    Return RenaissanceChangeID.None
    Exit Function

  End Function


    ''' <summary>
    ''' Handles the DoubleClick event of the Grid_Instruments control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Instruments_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid_Instruments.DoubleClick
    ' ***************************************************************************************
    ' Spawn an Add/Edit Instruments form reflecting the chosen selection of Instruments
    ' and pointing to the Instrument which was double clicked.
    ' ***************************************************************************************

    Try

      Dim InstrumentID As Integer
      Dim thisFormHandle As RenaissanceGlobals.RenaissanceFormHandle

      InstrumentID = Me.Grid_Instruments.Rows(Grid_Instruments.RowSel).DataSource("InstrumentID")

      thisFormHandle = MainForm.New_VeniceForm(VeniceFormID.frmInstrument)

      CType(thisFormHandle.Form, frmInstrument).MoveToAuditID(InstrumentID)

    Catch ex As Exception
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error opening Instruments Form.", ex.StackTrace, True)
    End Try

  End Sub


    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_FieldSelect1 control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_FieldSelect1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_FieldSelect1.SelectedIndexChanged
    ' ***************************************************************************************
    ' Field Select Combo Chosen, Update the associated Values Combo.
    '
    ' ***************************************************************************************

    Try

      Call UpdateSelectedValueCombo(Me.Combo_FieldSelect1.SelectedItem, Me.Combo_Select1_Value)
      Me.Combo_Select1_Operator.SelectedIndex = 1

    Catch ex As Exception
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Combo_FieldSelect1_SelectedIndexChanged()", ex.StackTrace, True)
    End Try

  End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_FieldSelect2 control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_FieldSelect2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_FieldSelect2.SelectedIndexChanged
    ' ***************************************************************************************
    ' Field Select Combo Chosen, Update the associated Values Combo.
    '
    ' ***************************************************************************************
    Try

      Call UpdateSelectedValueCombo(Me.Combo_FieldSelect2.SelectedItem, Me.Combo_Select2_Value)
      Me.Combo_Select2_Operator.SelectedIndex = 1

    Catch ex As Exception
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Combo_FieldSelect1_SelectedIndexChanged()", ex.StackTrace, True)
    End Try

  End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_FieldSelect3 control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_FieldSelect3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_FieldSelect3.SelectedIndexChanged
    ' ***************************************************************************************
    ' Field Select Combo Chosen, Update the associated Values Combo.
    '
    ' ***************************************************************************************

    Try

      Call UpdateSelectedValueCombo(Me.Combo_FieldSelect3.SelectedItem, Me.Combo_Select3_Value)
      Me.Combo_Select3_Operator.SelectedIndex = 1

    Catch ex As Exception
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Combo_FieldSelect1_SelectedIndexChanged()", ex.StackTrace, True)
    End Try

  End Sub

#End Region

#Region " Set Form Combos (Form Specific Code) "

    ''' <summary>
    ''' Sets the ISIN combo.
    ''' </summary>
  Private Sub SetISINCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_InstrumentISIN, _
    RenaissanceStandardDatasets.tblInstrument, _
    "InstrumentISIN", _
    "InstrumentISIN", _
    "true", True, True, True)   ' 

  End Sub

    ''' <summary>
    ''' Sets the instrument type combo.
    ''' </summary>
  Private Sub SetInstrumentTypeCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_InstrumentType, _
    RenaissanceStandardDatasets.tblInstrumentType, _
    "InstrumentTypeDescription", _
    "InstrumentTypeID", _
    "true", True, True, True)   ' 

  End Sub

#End Region

#Region " Instrument report Menu"

    ''' <summary>
    ''' Sets the field select menu.
    ''' </summary>
    ''' <param name="RootMenu">The root menu.</param>
    ''' <returns>ToolStripMenuItem.</returns>
  Private Function SetFieldSelectMenu(ByRef RootMenu As MenuStrip) As ToolStripMenuItem
    ' ***************************************************************************************
    ' Build the FieldSelect Menu.
    '
    ' Add an item for each field in the SelectInstruments table, when selected the item
    ' will show or hide the associated field on the Instruments grid.
    ' ***************************************************************************************

    Dim ColumnNames(-1) As String
    Dim ColumnCount As Integer

    Dim FieldsMenu As New ToolStripMenuItem("Grid &Fields")
    FieldsMenu.Name = "Menu_GridField"

    Dim newMenuItem As ToolStripMenuItem

    ReDim ColumnNames(myTable.Columns.Count - 1)
    For ColumnCount = 0 To (myTable.Columns.Count - 1)
      ColumnNames(ColumnCount) = myTable.Columns(ColumnCount).ColumnName
      If (ColumnNames(ColumnCount).StartsWith("Instrument")) Then
        ColumnNames(ColumnCount) = ColumnNames(ColumnCount).Substring(10) & "."
      End If
    Next
    Array.Sort(ColumnNames)

    For ColumnCount = 0 To (ColumnNames.Length - 1)
      newMenuItem = FieldsMenu.DropDownItems.Add(ColumnNames(ColumnCount), Nothing, AddressOf Me.ShowGridField)

      If (ColumnNames(ColumnCount).EndsWith(".")) Then
        newMenuItem.Name = "Menu_GridField_Instrument" & ColumnNames(ColumnCount).Substring(0, ColumnNames(ColumnCount).Length - 1)
      Else
        newMenuItem.Name = "Menu_GridField_" & ColumnNames(ColumnCount)
      End If
    Next

    RootMenu.Items.Add(FieldsMenu)
    Return FieldsMenu

  End Function

    ''' <summary>
    ''' Shows the grid field.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
  Private Sub ShowGridField(ByVal sender As Object, ByVal e As EventArgs)
    ' ***************************************************************************************
    ' Callback function for the Grid Fields Menu items.
    '
    ' ***************************************************************************************

    Try
      If TypeOf (sender) Is ToolStripMenuItem Then
        Dim thisMenuItem As ToolStripMenuItem
        Dim FieldName As String

        thisMenuItem = CType(sender, ToolStripMenuItem)

        FieldName = thisMenuItem.Name
        If (FieldName.StartsWith("Menu_GridField_")) Then
          FieldName = FieldName.Substring(15)
        End If

        thisMenuItem.Checked = Not thisMenuItem.Checked

        If (thisMenuItem.Checked) Then
          Grid_Instruments.Cols(FieldName).Visible = True
        Else
          Grid_Instruments.Cols(FieldName).Visible = False
        End If
      End If
    Catch ex As Exception

    End Try
  End Sub

#End Region

#Region " Bug hunting "

  ' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
  ' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
  ' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

    ''' <summary>
    ''' Called when [row updating].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

    ''' <summary>
    ''' Called when [row updated].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

    ''' <summary>
    ''' Called when [row fill error].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
    Dim a As Integer

    a = 1
  End Sub

#End Region






End Class
