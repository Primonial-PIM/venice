﻿' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 08-15-2013
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmFXGrid.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals
Imports RenaissanceDataClass
Imports RenaissanceUtilities.DatePeriodFunctions

''' <summary>
''' Class frmFXGrid
''' </summary>
Public Class frmFXGrid

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmFXGrid"/> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The BTN cancel
    ''' </summary>
  Friend WithEvents btnCancel As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN save
    ''' </summary>
  Friend WithEvents btnSave As System.Windows.Forms.Button
    ''' <summary>
    ''' The root menu
    ''' </summary>
  Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
    ''' <summary>
    ''' The label1
    ''' </summary>
  Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The date_ FX date
    ''' </summary>
  Friend WithEvents Date_FXDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The grid_ FX
    ''' </summary>
  Friend WithEvents Grid_FX As C1.Win.C1FlexGrid.C1FlexGrid
    ''' <summary>
    ''' The label7
    ''' </summary>
  Friend WithEvents Label7 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ reference currency
    ''' </summary>
  Friend WithEvents Combo_ReferenceCurrency As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label_ FX notice
    ''' </summary>
  Friend WithEvents Label_FXNotice As System.Windows.Forms.Label
    ''' <summary>
    ''' The BTN close
    ''' </summary>
  Friend WithEvents btnClose As System.Windows.Forms.Button
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFXGrid))
    Me.btnCancel = New System.Windows.Forms.Button
    Me.btnSave = New System.Windows.Forms.Button
    Me.btnClose = New System.Windows.Forms.Button
    Me.RootMenu = New System.Windows.Forms.MenuStrip
    Me.Label1 = New System.Windows.Forms.Label
    Me.Date_FXDate = New System.Windows.Forms.DateTimePicker
    Me.Grid_FX = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.Label7 = New System.Windows.Forms.Label
    Me.Combo_ReferenceCurrency = New System.Windows.Forms.ComboBox
    Me.Label_FXNotice = New System.Windows.Forms.Label
    CType(Me.Grid_FX, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'btnCancel
    '
    Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnCancel.Location = New System.Drawing.Point(98, 383)
    Me.btnCancel.Name = "btnCancel"
    Me.btnCancel.Size = New System.Drawing.Size(75, 28)
    Me.btnCancel.TabIndex = 6
    Me.btnCancel.Text = "&Cancel"
    '
    'btnSave
    '
    Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnSave.Location = New System.Drawing.Point(13, 383)
    Me.btnSave.Name = "btnSave"
    Me.btnSave.Size = New System.Drawing.Size(75, 28)
    Me.btnSave.TabIndex = 5
    Me.btnSave.Text = "&Save"
    '
    'btnClose
    '
    Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnClose.Location = New System.Drawing.Point(182, 383)
    Me.btnClose.Name = "btnClose"
    Me.btnClose.Size = New System.Drawing.Size(75, 28)
    Me.btnClose.TabIndex = 7
    Me.btnClose.Text = "&Close"
    '
    'RootMenu
    '
    Me.RootMenu.AllowMerge = False
    Me.RootMenu.Location = New System.Drawing.Point(0, 0)
    Me.RootMenu.Name = "RootMenu"
    Me.RootMenu.Size = New System.Drawing.Size(527, 24)
    Me.RootMenu.TabIndex = 8
    Me.RootMenu.Text = "MenuStrip1"
    '
    'Label1
    '
    Me.Label1.AutoSize = True
    Me.Label1.Location = New System.Drawing.Point(12, 37)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(30, 13)
    Me.Label1.TabIndex = 9
    Me.Label1.Text = "Date"
    '
    'Date_FXDate
    '
    Me.Date_FXDate.CustomFormat = "dd MMM yyyy"
    Me.Date_FXDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
    Me.Date_FXDate.Location = New System.Drawing.Point(59, 33)
    Me.Date_FXDate.Name = "Date_FXDate"
    Me.Date_FXDate.Size = New System.Drawing.Size(179, 20)
    Me.Date_FXDate.TabIndex = 10
    '
    'Grid_FX
    '
    Me.Grid_FX.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_FX.ColumnInfo = resources.GetString("Grid_FX.ColumnInfo")
    Me.Grid_FX.Location = New System.Drawing.Point(6, 92)
    Me.Grid_FX.Name = "Grid_FX"
    Me.Grid_FX.Rows.Count = 13
    Me.Grid_FX.Rows.DefaultSize = 17
    Me.Grid_FX.Size = New System.Drawing.Size(515, 273)
    Me.Grid_FX.TabIndex = 11
    '
    'Label7
    '
    Me.Label7.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label7.Location = New System.Drawing.Point(248, 34)
    Me.Label7.Name = "Label7"
    Me.Label7.Size = New System.Drawing.Size(95, 20)
    Me.Label7.TabIndex = 93
    Me.Label7.Text = "Reference Currency"
    Me.Label7.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Combo_ReferenceCurrency
    '
    Me.Combo_ReferenceCurrency.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_ReferenceCurrency.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_ReferenceCurrency.Location = New System.Drawing.Point(352, 32)
    Me.Combo_ReferenceCurrency.Name = "Combo_ReferenceCurrency"
    Me.Combo_ReferenceCurrency.Size = New System.Drawing.Size(169, 21)
    Me.Combo_ReferenceCurrency.TabIndex = 92
    '
    'Label_FXNotice
    '
    Me.Label_FXNotice.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label_FXNotice.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label_FXNotice.Location = New System.Drawing.Point(8, 63)
    Me.Label_FXNotice.Name = "Label_FXNotice"
    Me.Label_FXNotice.Size = New System.Drawing.Size(513, 20)
    Me.Label_FXNotice.TabIndex = 94
    Me.Label_FXNotice.Text = "FX Entered as XXX per unit of currency, unless 'Inverse' is set."
    '
    'frmFXGrid
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.CancelButton = Me.btnCancel
    Me.ClientSize = New System.Drawing.Size(527, 423)
    Me.Controls.Add(Me.Label_FXNotice)
    Me.Controls.Add(Me.Label7)
    Me.Controls.Add(Me.Combo_ReferenceCurrency)
    Me.Controls.Add(Me.Grid_FX)
    Me.Controls.Add(Me.Date_FXDate)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.btnClose)
    Me.Controls.Add(Me.btnSave)
    Me.Controls.Add(Me.btnCancel)
    Me.Controls.Add(Me.RootMenu)
    Me.MainMenuStrip = Me.RootMenu
    Me.MinimumSize = New System.Drawing.Size(500, 300)
    Me.Name = "frmFXGrid"
    Me.Text = "Add/Edit Instrument Prices"
    CType(Me.Grid_FX, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "


  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
  Private WithEvents _MainForm As VeniceMain

  ' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
  Private FormTooltip As New ToolTip()

  ' Form Constants, specific to the table being updated.

    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
  Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

  ' Form Locals, initialised on 'New' defining what standard data items to use
    ''' <summary>
  ''' Variable holding the form's data table name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_TABLENAME As String
    ''' <summary>
  ''' Variable holding the form's data adaptor name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_ADAPTORNAME As String
    ''' <summary>
  ''' Variable holding the form's dataset name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_DATASETNAME As String

    ''' <summary>
    ''' The generic update object
    ''' </summary>
  Private GenericUpdateObject As RenaissanceTimerUpdateClass

  ' The standard ChangeID for this form. e.g. tblGroupList
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

  ' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
  Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
  Private THIS_FORM_FormID As VeniceFormID

  ' Data Structures

    ''' <summary>
    ''' My dataset
    ''' </summary>
  Private myDataset As RenaissanceDataClass.DSFX     ' Form Specific !!!!
    ''' <summary>
    ''' My table
    ''' </summary>
  Private myTable As RenaissanceDataClass.DSFX.tblFXDataTable
    ''' <summary>
    ''' My adaptor
    ''' </summary>
  Private myAdaptor As SqlDataAdapter

  'Private PerformanceDataset As RenaissanceDataClass.DSPerformance			' Form Specific !!!!
  'Private PerformanceTable As RenaissanceDataClass.DSPerformance.tblPerformanceDataTable
  'Private PerformanceAdaptor As SqlDataAdapter

    ''' <summary>
    ''' The this standard dataset
    ''' </summary>
  Private ThisStandardDataset As RenaissanceGlobals.StandardDataset

  '

    ''' <summary>
    ''' The edit rate control
    ''' </summary>
  Private EditRateControl As CustomC1NumericTextBox


  ' Active Element.

    ''' <summary>
    ''' The _ is over cancel button
    ''' </summary>
  Private _IsOverCancelButton As Boolean
    ''' <summary>
    ''' The _ in use
    ''' </summary>
  Private _InUse As Boolean

  ' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
  Private FormIsValid As Boolean
    ''' <summary>
    ''' The form changed
    ''' </summary>
  Private FormChanged As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
  Private _FormOpenFailed As Boolean
    ''' <summary>
    ''' The in paint
    ''' </summary>
  Private InPaint As Boolean
    ''' <summary>
    ''' The in get form data_ tick
    ''' </summary>
  Private InGetFormData_Tick As Boolean
    ''' <summary>
    ''' The add new record
    ''' </summary>
  Private AddNewRecord As Boolean

  ' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
  Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
  Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
  Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
  Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
  Public ReadOnly Property MainForm() As VeniceMain Implements StandardVeniceForm.MainForm
    ' Public property to return handle to the 'Main' Venice form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [form changed flag].
    ''' </summary>
    ''' <value><c>true</c> if [form changed flag]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property FormChangedFlag() As Boolean
    Get
      Return FormChanged
    End Get
  End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
  Public Property IsOverCancelButton() As Boolean Implements StandardVeniceForm.IsOverCancelButton
    ' Public property maintaining a value indicating if the cursor is over the 'Cancel'
    ' Button on this form.
    ' This property is specifically designed for use by the field formating Event functions
    ' In order that they do not impose format restrictions if the user is about to click the 
    ' 'Cancel' button.
    '
    Get
      Return _IsOverCancelButton
    End Get
    Set(ByVal Value As Boolean)
      _IsOverCancelButton = Value
    End Set
  End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
  Public ReadOnly Property IsInPaint() As Boolean Implements StandardVeniceForm.IsInPaint
    Get
      Return InPaint
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property InUse() As Boolean Implements StandardVeniceForm.InUse
    Get
      Return _InUse
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property FormOpenFailed() As Boolean Implements StandardVeniceForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

#End Region

#Region " Local Classes"


#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmFXGrid"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  Public Sub New(ByVal pMainForm As VeniceMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()

    _MainForm = pMainForm
    AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

    _FormOpenFailed = False
    _InUse = True

    ' ******************************************************
    ' Form Specific Settings :
    ' ******************************************************

    ' Form Permissioning :-

    THIS_FORM_PermissionArea = Me.Name
    THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

    ' 'This' form ID

    THIS_FORM_FormID = VeniceFormID.frmFXGrid

    ' This form's dataset type.

    ThisStandardDataset = RenaissanceStandardDatasets.tblFX  ' This Defines the Form Data !!! 

    ' Format Event Handlers for form controls

    AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

    ' Form Control Changed events

    ' Set up the ToolTip
    MainForm.SetFormToolTip(Me, FormTooltip)

    ' ******************************************************
    ' End Form Specific.
    ' ******************************************************

    ' Data object names standard to this Form type.

    THIS_TABLENAME = ThisStandardDataset.TableName
    THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
    THIS_DATASETNAME = ThisStandardDataset.DatasetName

    THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

    ' Establish / Retrieve data objects for this form.

    myAdaptor = New SqlDataAdapter
    MainForm.MainAdaptorHandler.Set_AdaptorCommands(Nothing, myAdaptor, "TBLFXONDATE")

    myDataset = New RenaissanceDataClass.DSFX ' MainForm.Load_Table(ThisStandardDataset, False)
    myTable = myDataset.tblFX

    Me.RootMenu.PerformLayout()

  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
  Public Sub ResetForm() Implements StandardVeniceForm.ResetForm

    Call Form_Load(Me, New System.EventArgs)
  End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
  Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    ALWAYS_CLOSE_THIS_FORM = True
    Me.Close()
  End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
    _FormOpenFailed = False
    _InUse = True

    ' Initialise Data structures. Connection, Adaptor and Dataset.

    If Not (MainForm Is Nothing) Then
      FormIsValid = True
    Else
      MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (myAdaptor Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (myDataset Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    ' Initialse form

    InPaint = True
    IsOverCancelButton = False

    ' Check User permissions
    Call CheckPermissions()
    If (HasReadPermission = False) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    ' Initialise Grid

    Grid_FX.DataSource = Nothing
    Grid_FX.AutoGenerateColumns = False
    Grid_FX.Rows.Count = 1

    EditRateControl = New CustomC1NumericTextBox
    EditRateControl.TextFormat = "###0.0000"
    Grid_FX.Cols("FXRate").Editor = EditRateControl

    '

    Call SetCurrencyCombo()

    Combo_ReferenceCurrency.SelectedValue = Globals.REFERENCE_REPORT_CURRENCY
    Date_FXDate.Value = AddPeriodToDate(DealingPeriod.Daily, Now.Date, -1)

    ' Initialise Timer

    GenericUpdateObject = MainForm.AddFormUpdate(Me, AddressOf GetFormData_Tick)

    ' Display initial record.

    Call GetFormData()

    InPaint = False

  End Sub

    ''' <summary>
    ''' Handles the Closing event of the frmEntity control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
  Private Sub frmEntity_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    Dim HideForm As Boolean

    ' Hide or Close this form ?
    ' All depends on how many of this form type are Open or in Cache...

    _InUse = False
    MainForm.RemoveFormUpdate(Me)

    If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
      HideForm = False
    Else
      If (FormChanged = True) Then
        Call SetFormData()
      End If

      HideForm = True
      If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
        HideForm = False
      End If
    End If

    If HideForm = True Then
      MainForm.HideInFormsCollection(Me)
      Me.Hide() ' NPP Fix

      e.Cancel = True
    Else
      Try
        MainForm.RemoveFromFormsCollection(Me)
        RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

        RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

        ' Form Control Changed events

      Catch ex As Exception
      End Try
    End If

  End Sub


#End Region

  ' Routine to handle changes / updates to tables by this and other windows.
  ' If this, or any other, form posts a change to a table, then it will invoke an update event 
  ' detailing what tables have been altered.
  ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
  ' the 'VeniceAutoUpdate' event of the main Venice form.
  ' Each form may them react as appropriate to changes in any table that might impact it.
  '
    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    Dim OrgInPaint As Boolean
    Dim KnowledgeDateChanged As Boolean
    Dim SetButtonStatus_Flag As Boolean
    Dim RefreshForm As Boolean = False

    If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

    OrgInPaint = InPaint
    InPaint = True
    KnowledgeDateChanged = False
    SetButtonStatus_Flag = False

    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
      KnowledgeDateChanged = True
      RefreshForm = True
    End If

    ' ****************************************************************
    ' Check for changes relevant to this form
    ' ****************************************************************

    ' Changes to the KnowledgeDate :-
    If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
      SetButtonStatus_Flag = True
    End If

    '
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblCurrency) = True) Or KnowledgeDateChanged Then
      Call SetCurrencyCombo()
      RefreshForm = True
    End If

    ' Changes to the tblUserPermissions table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

      ' Check ongoing permissions.

      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

        FormIsValid = False
        Me.Close()
        Exit Sub
      End If

      SetButtonStatus_Flag = True

    End If

    ' ****************************************************************
    ' Changes to the Main FORM table :-
    ' ****************************************************************

    If (e.TableChanged(THIS_FORM_ChangeID) = True) Or KnowledgeDateChanged Then

      If (Not FormChanged) Then
        RefreshForm = True
      End If

    End If

    ' ****************************************************************
    ' Repaint if not currently in Edit Mode
    '
    ' ****************************************************************

    InPaint = OrgInPaint

    ' Retrieve form data if appropriate, i.e. this data item is not already being edited.
    If (RefreshForm = True) AndAlso (FormChanged = False) AndAlso (AddNewRecord = False) Then
      GetFormData() ' Includes a call to 'SetButtonStatus()'
    Else
      If SetButtonStatus_Flag Then
        Call SetButtonStatus()
      End If
    End If

  End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

  ' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
  Private Sub CheckPermissions()

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

  End Sub

  ' Flag changes to form controls, Event Associations made in 'New' routine.
    ''' <summary>
  ''' Basic event handler, called when a form value is changed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    If InPaint = False Then
      If (Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord) Then
        FormChanged = True
        Me.btnSave.Enabled = True
        Me.btnCancel.Enabled = True
      End If
    End If

  End Sub

#End Region

#Region " Set Form Combos (Form Specific Code) "

    ''' <summary>
    ''' Sets the currency combo.
    ''' </summary>
  Private Sub SetCurrencyCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_ReferenceCurrency, _
    RenaissanceStandardDatasets.tblCurrency, _
    "CurrencyCode", _
    "CurrencyID", _
    "")   ' 

  End Sub

#End Region

#Region " Get & Set Form Data / SetButton / ValidateForm / btnCancel Events (Form Specific Code) "

    ''' <summary>
    ''' Gets the form data_ tick.
    ''' </summary>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function GetFormData_Tick() As Boolean
    ' *******************************************************************************
    '
    ' Callback process for the Generic Form Update Process.
    '
    ' Function MUST return True / False on Update.
    ' True will clear the update request. False will not.
    '
    ' *******************************************************************************
    Dim RVal As Boolean = False

    If (InGetFormData_Tick) OrElse (Not _InUse) Then
      Return False
      Exit Function
    End If

    Try
      InGetFormData_Tick = True

      ' Find the correct data row, then show it...

      GetFormData()

      RVal = True

    Catch ex As Exception
      RVal = False
    Finally
      InGetFormData_Tick = False
    End Try

    Return RVal

  End Function


    ''' <summary>
    ''' Gets the form data.
    ''' </summary>
  Private Sub GetFormData()
    ' ******************************************************************************
    '
    ' Routine to populate form Controls given a data row.
    ' Form is cleared for invalid Datarows or Invalid Form.
    '
    ' ******************************************************************************

    Dim OrgInpaint As Boolean

    ' Set 'Paint' flag to prevent event actions caused by changing the value of form fields.
    OrgInpaint = InPaint
    InPaint = True


    If (FormIsValid = False) Or (Me.InUse = False) Then
      ' Bad / New Datarow - Clear Form.

      Grid_FX.Rows.Count = 1

      Me.btnCancel.Enabled = False
    Else

      ' Get Instrument Price Data

      LoadSelectedFXData(myTable, Date_FXDate.Value)

      ' Populate Form with given data.
      Try

        Grid_FX.Rows.Count = 1
        Dim NewGridRow As C1.Win.C1FlexGrid.Row
        Dim ThisFXRow As DSFX.tblFXRow
        Dim RowCounter As Integer
        Dim FXCounter As Integer

        Dim CurrencyDS As RenaissanceDataClass.DSCurrency = MainForm.Load_Table(RenaissanceStandardDatasets.tblCurrency)
        Dim SelectedCurrencies() As DSCurrency.tblCurrencyRow = Nothing
        Dim thisCurrency As DSCurrency.tblCurrencyRow = Nothing
        If (CurrencyDS IsNot Nothing) Then
          SelectedCurrencies = CurrencyDS.tblCurrency.Select("true", "CurrencyCode")
        End If
        Dim ReferenceCurrencyFXRate As Double = 1.0#
        Dim ReferenceCurrencyID As Integer = Globals.REFERENCE_REPORT_CURRENCY

        If (IsNumeric(Combo_ReferenceCurrency.SelectedValue)) Then
          ReferenceCurrencyID = CInt(Combo_ReferenceCurrency.SelectedValue)
        End If

        ' Get Reference FX Rate

        For FXCounter = 0 To (myTable.Rows.Count - 1)
          ThisFXRow = myTable.Rows(FXCounter)

          If (ThisFXRow.FXCurrencyCode = ReferenceCurrencyID) Then
            ReferenceCurrencyFXRate = ThisFXRow.FXRate
            Exit For
          End If
        Next


        ' Set Default Currency Rows

        If (SelectedCurrencies IsNot Nothing) AndAlso (SelectedCurrencies.Length > 0) Then
          For RowCounter = 0 To (SelectedCurrencies.Length - 1)
            thisCurrency = SelectedCurrencies(RowCounter)

            NewGridRow = Grid_FX.Rows.Add

            NewGridRow("FXID") = 0
            NewGridRow("CurrencyID") = thisCurrency.CurrencyID
            NewGridRow("Currency") = thisCurrency.CurrencyCode
            NewGridRow("Inverse") = False

            If (thisCurrency.CurrencyID = ReferenceCurrencyID) Then
              NewGridRow("FXRate") = 1.0#
            Else
              NewGridRow("FXRate") = Nothing
            End If

            ' Enter existing FX Rates.

            If (thisCurrency.CurrencyID = RenaissanceGlobals.Currency.USD) Then
              ' Handle USD Case. There is not normally an FX entered for USD as it is always 1.

              If (thisCurrency.CurrencyID <> ReferenceCurrencyID) Then
                NewGridRow("FXRate") = 1.0# / ReferenceCurrencyFXRate
              Else
                NewGridRow("FXRate") = 1.0#
              End If
            Else
              ' Other Currencies.

              For FXCounter = 0 To (myTable.Rows.Count - 1)
                ThisFXRow = myTable.Rows(FXCounter)

                If (ThisFXRow.FXCurrencyCode = thisCurrency.CurrencyID) Then
                  NewGridRow("FXID") = ThisFXRow.FXID

                  If (thisCurrency.CurrencyID <> ReferenceCurrencyID) Then
                    NewGridRow("FXRate") = ThisFXRow.FXRate / ReferenceCurrencyFXRate
                  End If

                  Exit For
                End If
              Next
            End If

            NewGridRow("Modified") = False

          Next
        End If

        ' Format Grid.

        AddNewRecord = False

        Me.btnCancel.Enabled = False

      Catch ex As Exception

        Call MainForm.LogError(Me.Name & ", GetFormData", 0, ex.Message, "Error Showing Data", ex.StackTrace, True)
        Grid_FX.Rows.Count = 1

      End Try

    End If

    ' Allow Field events to trigger before 'InPaint' Is re-set. 
    ' (Should) Prevent Validation errors during Form Draw.
    Application.DoEvents()

    ' Restore 'Paint' flag.
    InPaint = OrgInpaint
    FormChanged = False
    Me.btnSave.Enabled = False

    ' As it says on the can....
    Call SetButtonStatus()

  End Sub

    ''' <summary>
    ''' Sets the form data.
    ''' </summary>
    ''' <param name="pConfirm">if set to <c>true</c> [p confirm].</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function SetFormData(Optional ByVal pConfirm As Boolean = True) As Boolean
    ' *************************************************************
    '
    ' Note that GroupList Entries are also added from the FundSearch Form. Changes
    ' to the GroupList table should be reflected there also.
    '
    ' *************************************************************
    Dim ErrMessage As String
    Dim ErrStack As String

    Try

      ErrMessage = ""
      ErrStack = ""

      ' *************************************************************
      ' Appropriate Save permission :-
      ' *************************************************************

      If (Me.HasUpdatePermission = False) And ((Me.HasInsertPermission = False) Or (Me.AddNewRecord = False)) Then
        MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "You do not have permission to save this record.", "", True)
        Call btnCancel_Click(Me, New System.EventArgs)
        Exit Function
      End If

      ' *************************************************************
      ' If Save button is disabled then should not be able to save, exit silently.
      ' *************************************************************
      If Me.btnSave.Enabled = False Then
        Call btnCancel_Click(Me, New System.EventArgs)
        Exit Function
      End If

      ' *************************************************************
      ' KnowledgeDate OK :-
      ' *************************************************************

      If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
        MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Changes to Venice are not allowed when the Knowledgedate is not set to `NOW`", "", True)
        Call btnCancel_Click(Me, New System.EventArgs)
        Exit Function
      End If

      ' Confirm Save, if required.

      If (pConfirm = True) Then
        If MessageBox.Show("Save Changes ?", "Save Changes ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
          Return True
          Exit Function
        End If
      End If

      ' *************************************************************
      ' Procedure to Save the current form information.
      ' *************************************************************

      Dim StatusString As String

      If (FormChanged = False) Or (FormIsValid = False) Then
        Return False
        Exit Function
      End If

      ' Validation
      StatusString = ""
      If ValidateForm(StatusString) = False Then
        MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Validation Error." & vbCrLf & StatusString, "", True)
        Return False
        Exit Function
      End If

      ' *************************************************************
      ' Collect FXs to Change
      ' *************************************************************

      Dim ReferenceCurrencyID As Integer = Globals.REFERENCE_REPORT_CURRENCY
      Dim RowCounter As Integer
      Dim USDFXRate As Double = 1.0#
      Dim USDRateChanged As Boolean = False
      Dim thisGridRow As C1.Win.C1FlexGrid.Row

      Dim FXDS As RenaissanceDataClass.DSFX
      Dim FXTable As RenaissanceDataClass.DSFX.tblFXDataTable
      Dim FXTableIsLoaded As Boolean = True
      Dim SelectedFXRows() As RenaissanceDataClass.DSFX.tblFXRow

      Dim NewFXRow As DSFX.tblFXRow
      Dim UpdateString As String = ""

      ' Use the FX table, if it is loaded, else use a temporary one.

      FXDS = MainForm.MainDataHandler.Get_Dataset(RenaissanceStandardDatasets.GetStandardDataset(RenaissanceChangeID.tblFX).DatasetName)
      If (FXDS Is Nothing) Then
        FXDS = New DSFX
        FXTableIsLoaded = False
      End If
      FXTable = FXDS.tblFX

      If (IsNumeric(Combo_ReferenceCurrency.SelectedValue)) Then
        ReferenceCurrencyID = CInt(Combo_ReferenceCurrency.SelectedValue)
      End If

      ' Get USD FX Rate and determine if it has changed.

      For RowCounter = 1 To (Grid_FX.Rows.Count - 1)
        thisGridRow = Grid_FX.Rows(RowCounter)

        If (CInt(thisGridRow("CurrencyID")) = RenaissanceGlobals.Currency.USD) Then
          If IsNumeric(thisGridRow("FXRate")) Then

            USDFXRate = CDbl(thisGridRow("FXRate"))

            If (USDFXRate = 0.0#) Then
              USDFXRate = 1.0#
            ElseIf CBool(thisGridRow("Inverse")) Then
              USDFXRate = 1.0# / USDFXRate
            End If
          End If

          If (CBool(thisGridRow("Modified"))) Then
            USDRateChanged = True
          End If

          Exit For
        End If
      Next

      ' Changed FXs

      For RowCounter = 1 To (Grid_FX.Rows.Count - 1)
        thisGridRow = Grid_FX.Rows(RowCounter)

        ' Update FX if the USD rate has changed or if this rate has changed

        If (CInt(thisGridRow("CurrencyID")) <> RenaissanceGlobals.Currency.USD) Then
          If IsNumeric(thisGridRow("FXRate")) Then
            If (USDRateChanged) OrElse (CBool(thisGridRow("Modified"))) Then

              If (CInt(thisGridRow("FXID")) > 0) Then
                SelectedFXRows = FXTable.Select("FXID=" & CInt(thisGridRow("FXID")).ToString())
              Else
                SelectedFXRows = Nothing
              End If

              If (SelectedFXRows Is Nothing) OrElse (SelectedFXRows.Length <= 0) Then
                NewFXRow = FXTable.NewtblFXRow
                NewFXRow.FXID = 0
              Else
                NewFXRow = SelectedFXRows(0)
              End If

              NewFXRow.FXCurrencyCode = CInt(thisGridRow("CurrencyID"))
              NewFXRow.FXDate = Date_FXDate.Value.Date

              ' Rate for Reference currency to itself must always be 1#

              If (CInt(thisGridRow("CurrencyID")) = ReferenceCurrencyID) Then
                NewFXRow.FXRate = 1.0# / USDFXRate
              Else
                If (CBool(thisGridRow("Inverse"))) Then
                  NewFXRow.FXRate = 1.0# / (CDbl(thisGridRow("FXRate")) * USDFXRate)
                Else
                  NewFXRow.FXRate = CDbl(thisGridRow("FXRate")) / USDFXRate
                End If
              End If

              If (NewFXRow.RowState And DataRowState.Detached) Then
                FXTable.Rows.Add(NewFXRow)
              End If

            End If
          End If
        End If
      Next

      ' Update

      Try

        MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblFX, FXTable)

        If (Not FXTableIsLoaded) Then
          For Each NewFXRow In FXTable.Rows
            UpdateString &= NewFXRow.FXID.ToString() & ","
          Next
        End If

      Catch ex As Exception
        MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Saving FX Rates.", ex.StackTrace, True)
      End Try

      ' Finish off

      AddNewRecord = False
      FormChanged = False

      ' Propogate changes

      Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID, UpdateString), True)

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in SetFormData().", ex.StackTrace, True)
    End Try

  End Function

    ''' <summary>
    ''' Sets the button status.
    ''' </summary>
  Private Sub SetButtonStatus()
    ' Sets the status of the form controlls appropriate to the current users 
    'permissions and the 'Changed' or 'New' status of the form.

    ' No Read permission :-

    If Me.HasReadPermission = False Then
      MainForm.LogError(Me.Name & ", SetButtonStatus()", 0, "", "You do not have Read permission for this Form", "", True)
      Me.Close()
      Exit Sub
    End If

    If (Me.MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And _
     ((Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord)) Then

      Me.Grid_FX.Enabled = True

    Else

      Me.Grid_FX.Enabled = False

    End If

  End Sub

    ''' <summary>
    ''' Validates the form.
    ''' </summary>
    ''' <param name="pReturnString">The p return string.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function ValidateForm(ByRef pReturnString As String) As Boolean
    ' Form Validation code.
    ' 
    ' This code should be the final arbiter of what is allowed. no assumptions regarding 
    ' prior validation should be made.
    ' 
    ' This Code is called by the SetFormData routine before position changes.
    '
    Dim RVal As Boolean

    RVal = True
    pReturnString = ""

    ' Validate 

    Return RVal

  End Function

    ''' <summary>
    ''' Handles the MouseEnter event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnCancel_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseEnter
    ' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
    Me.IsOverCancelButton = True
  End Sub

    ''' <summary>
    ''' Handles the MouseLeave event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnCancel_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseLeave
    ' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
    Me.IsOverCancelButton = False
  End Sub

#End Region

#Region " Navigation Code / GetPosition() (Generic Code) "

    ''' <summary>
    ''' Handles the FXDateValueChanged event of the Date control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Date_FXDateValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Date_FXDate.ValueChanged, Combo_ReferenceCurrency.SelectedValueChanged
    ' ***************************************************************
    ' Selection Date. SelectedItem changed.
    '
    ' ***************************************************************

    Try

      Label_FXNotice.Text = "FX Entered as " & Combo_ReferenceCurrency.Text & " per unit of currency, unless 'Inverse' is set."

      ' Don't react to changes made in paint routines etc.

      If InPaint Then Exit Sub

      If (FormChanged = True) Then
        Call SetFormData()
      End If

      If (GenericUpdateObject IsNot Nothing) Then
        Try
          GenericUpdateObject.FormToUpdate = True  '	GetFormDataToUpdate = True
        Catch ex As Exception
        End Try
      Else
        Call GetFormData()
      End If

    Catch ex As Exception
    End Try

  End Sub


#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

    ''' <summary>
    ''' Handles the Click event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    ' Cancel Changes, redisplay form.

    FormChanged = False
    AddNewRecord = False

    Call GetFormData()

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnSave control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
    ' Save Changes, if any, without prompting.

    If (FormChanged = True) Then
      Call SetFormData(False)
    End If

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnClose control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
    ' *************************************************************
    ' Close Form
    ' *************************************************************


    If (FormChanged = True) Then
      Call SetFormData()
    End If

    Me.Close()

  End Sub

#End Region

#Region " Form Control Event Code"

    ''' <summary>
    ''' Handles the CellValueChanged event of the Grid_Prices control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="C1.Win.C1FlexGrid.RowColEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Prices_CellValueChanged(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles Grid_FX.CellChanged
    ' ****************************************************************************************
    '
    '
    ' ****************************************************************************************

    Try
      If (InPaint) Then
        Exit Sub
      End If

      If (Grid_FX.Cols.Contains("Modified")) Then
        If (e.Col = Grid_FX.Cols("Modified").Index) Then
          Exit Sub
        End If
      End If

      Dim ThisGridRow As C1.Win.C1FlexGrid.Row = Nothing

      If (Not InPaint) Then
        Call FormControlChanged(Grid_FX, Nothing)
        Grid_FX.Item(e.Row, Grid_FX.Cols("Modified").Index) = True
      End If

    Catch ex As Exception
    End Try

  End Sub


    ''' <summary>
    ''' Handles the CellParsing event of the Grid_Prices control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.DataGridViewCellParsingEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Prices_CellParsing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellParsingEventArgs)
    ' ****************************************************************************************
    '
    '
    ' ****************************************************************************************

    Try
      e.Value = ConvertValue(e.Value, e.DesiredType)
      e.ParsingApplied = True
    Catch ex As Exception
      e.ParsingApplied = False
    End Try
  End Sub

    ''' <summary>
    ''' Handles the DataError event of the Grid_Prices control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.DataGridViewDataErrorEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Prices_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs)

  End Sub

    ''' <summary>
    ''' The _valid data
    ''' </summary>
  Private _validData As Boolean

#End Region

#Region " Bug hunting "

  ' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
  ' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
  ' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

    ''' <summary>
    ''' Called when [row updating].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

    ''' <summary>
    ''' Called when [row updated].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

    ''' <summary>
    ''' Called when [row fill error].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
    Dim a As Integer

    a = 1
  End Sub

#End Region

    ''' <summary>
    ''' Loads the selected FX data.
    ''' </summary>
    ''' <param name="pFXTable">The p FX table.</param>
    ''' <param name="FXDate">The FX date.</param>
  Private Sub LoadSelectedFXData(ByRef pFXTable As RenaissanceDataClass.DSFX.tblFXDataTable, ByVal FXDate As Date)
    ' ************************************************************************************
    '
    '
    ' ************************************************************************************
    Dim myConnection As SqlConnection = Nothing

    Try
      myConnection = MainForm.GetVeniceConnection

      myAdaptor.SelectCommand.Connection = myConnection
      myAdaptor.SelectCommand.Parameters("@FXDate").Value = Date_FXDate.Value.Date
      myAdaptor.SelectCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

      pFXTable.Clear()

      MainForm.LoadTable_Custom(pFXTable, myAdaptor.SelectCommand)
      'pFXTable.Load(myAdaptor.SelectCommand.ExecuteReader)

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error loading FX Data (LoadSelectedFXData)", ex.Message, ex.StackTrace, True)
      pFXTable.Clear()
    Finally
      Try
        If (myConnection IsNot Nothing) Then
          myConnection.Close()
        End If
        myAdaptor.SelectCommand.Connection = Nothing
      Catch ex As Exception
      End Try
    End Try

  End Sub




End Class
