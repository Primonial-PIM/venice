﻿' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmImportTransactions.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals

Imports RenaissanceDataClass
Imports C1.Win.C1FlexGrid
Imports System.IO

''' <summary>
''' Class frmImportTransactions
''' </summary>
Public Class frmImportTransactions

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

  ''' <summary>
  ''' Prevents a default instance of the <see cref="frmImportTransactions"/> class from being created.
  ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  ''' <summary>
  ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
  ''' </summary>
  ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  ''' <summary>
  ''' The components
  ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  ''' <summary>
  ''' The transaction fund Combo
  ''' </summary>
  Friend WithEvents Combo_TransactionFund As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The `cpty is fund` label
  ''' </summary>
  Friend WithEvents label_CptyIsFund As System.Windows.Forms.Label
  ''' <summary>
  ''' The transactions grid
  ''' </summary>
  Friend WithEvents Grid_Transactions As C1.Win.C1FlexGrid.C1FlexGrid
  ''' <summary>
  ''' The root menu
  ''' </summary>
  Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
  ''' <summary>
  ''' The form_ status strip
  ''' </summary>
  Friend WithEvents Form_StatusStrip As System.Windows.Forms.StatusStrip
  ''' <summary>
  ''' The form_ progress bar
  ''' </summary>
  Friend WithEvents Form_ProgressBar As System.Windows.Forms.ToolStripProgressBar
  Friend WithEvents Combo_SubFund As System.Windows.Forms.ComboBox
  Friend WithEvents Label21 As System.Windows.Forms.Label
  Friend WithEvents Combo_Broker As System.Windows.Forms.ComboBox
  Friend WithEvents Label8 As System.Windows.Forms.Label
  Friend WithEvents Combo_TradeStatus As System.Windows.Forms.ComboBox
  Friend WithEvents Label6 As System.Windows.Forms.Label
  Friend WithEvents Combo_TransactionCounterparty As System.Windows.Forms.ComboBox
  Friend WithEvents Label_Counterparty As System.Windows.Forms.Label
  Friend WithEvents Combo_SpecificShareClass As System.Windows.Forms.ComboBox
  Friend WithEvents Label_SpecificShareClass As System.Windows.Forms.Label
  Friend WithEvents Button_UpdatePendingTransactions As System.Windows.Forms.Button
  Friend WithEvents Button_SaveSelectedTransactions As System.Windows.Forms.Button
  Friend WithEvents Button_SaveAllTransactions As System.Windows.Forms.Button
  Friend WithEvents Button_Clear As System.Windows.Forms.Button
  Friend WithEvents TabControl_Import As System.Windows.Forms.TabControl
  Friend WithEvents TabPage_Import As System.Windows.Forms.TabPage
  Friend WithEvents TabPage_Status As System.Windows.Forms.TabPage
  Friend WithEvents TextBox_Status As System.Windows.Forms.TextBox
  ''' <summary>
  ''' The label_ status
  ''' </summary>
  Friend WithEvents Label_Status As System.Windows.Forms.ToolStripStatusLabel
  ''' <summary>
  ''' Initializes the component.
  ''' </summary>
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.Grid_Transactions = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.Combo_TransactionFund = New System.Windows.Forms.ComboBox
    Me.label_CptyIsFund = New System.Windows.Forms.Label
    Me.RootMenu = New System.Windows.Forms.MenuStrip
    Me.Form_StatusStrip = New System.Windows.Forms.StatusStrip
    Me.Form_ProgressBar = New System.Windows.Forms.ToolStripProgressBar
    Me.Label_Status = New System.Windows.Forms.ToolStripStatusLabel
    Me.Combo_SubFund = New System.Windows.Forms.ComboBox
    Me.Label21 = New System.Windows.Forms.Label
    Me.Combo_Broker = New System.Windows.Forms.ComboBox
    Me.Label8 = New System.Windows.Forms.Label
    Me.Combo_TradeStatus = New System.Windows.Forms.ComboBox
    Me.Label6 = New System.Windows.Forms.Label
    Me.Combo_TransactionCounterparty = New System.Windows.Forms.ComboBox
    Me.Label_Counterparty = New System.Windows.Forms.Label
    Me.Combo_SpecificShareClass = New System.Windows.Forms.ComboBox
    Me.Label_SpecificShareClass = New System.Windows.Forms.Label
    Me.Button_UpdatePendingTransactions = New System.Windows.Forms.Button
    Me.Button_SaveSelectedTransactions = New System.Windows.Forms.Button
    Me.Button_SaveAllTransactions = New System.Windows.Forms.Button
    Me.Button_Clear = New System.Windows.Forms.Button
    Me.TabControl_Import = New System.Windows.Forms.TabControl
    Me.TabPage_Import = New System.Windows.Forms.TabPage
    Me.TabPage_Status = New System.Windows.Forms.TabPage
    Me.TextBox_Status = New System.Windows.Forms.TextBox
    CType(Me.Grid_Transactions, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Form_StatusStrip.SuspendLayout()
    Me.TabControl_Import.SuspendLayout()
    Me.TabPage_Import.SuspendLayout()
    Me.TabPage_Status.SuspendLayout()
    Me.SuspendLayout()
    '
    'Grid_Transactions
    '
    Me.Grid_Transactions.AllowEditing = False
    Me.Grid_Transactions.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_Transactions.AutoClipboard = True
    Me.Grid_Transactions.CausesValidation = False
    Me.Grid_Transactions.ColumnInfo = "5,1,0,0,0,85,Columns:"
    Me.Grid_Transactions.DropMode = C1.Win.C1FlexGrid.DropModeEnum.Manual
    Me.Grid_Transactions.EditOptions = C1.Win.C1FlexGrid.EditFlags.None
    Me.Grid_Transactions.KeyActionTab = C1.Win.C1FlexGrid.KeyActionEnum.MoveAcross
    Me.Grid_Transactions.Location = New System.Drawing.Point(3, 3)
    Me.Grid_Transactions.Name = "Grid_Transactions"
    Me.Grid_Transactions.Rows.DefaultSize = 17
    Me.Grid_Transactions.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.ListBox
    Me.Grid_Transactions.Size = New System.Drawing.Size(866, 315)
    Me.Grid_Transactions.TabIndex = 18
    '
    'Combo_TransactionFund
    '
    Me.Combo_TransactionFund.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Combo_TransactionFund.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_TransactionFund.Location = New System.Drawing.Point(114, 443)
    Me.Combo_TransactionFund.Name = "Combo_TransactionFund"
    Me.Combo_TransactionFund.Size = New System.Drawing.Size(769, 21)
    Me.Combo_TransactionFund.TabIndex = 0
    '
    'label_CptyIsFund
    '
    Me.label_CptyIsFund.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.label_CptyIsFund.Location = New System.Drawing.Point(4, 446)
    Me.label_CptyIsFund.Name = "label_CptyIsFund"
    Me.label_CptyIsFund.Size = New System.Drawing.Size(104, 16)
    Me.label_CptyIsFund.TabIndex = 82
    Me.label_CptyIsFund.Text = "Fund"
    '
    'RootMenu
    '
    Me.RootMenu.Location = New System.Drawing.Point(0, 0)
    Me.RootMenu.Name = "RootMenu"
    Me.RootMenu.Size = New System.Drawing.Size(888, 24)
    Me.RootMenu.TabIndex = 104
    Me.RootMenu.Text = " "
    '
    'Form_StatusStrip
    '
    Me.Form_StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Form_ProgressBar, Me.Label_Status})
    Me.Form_StatusStrip.Location = New System.Drawing.Point(0, 671)
    Me.Form_StatusStrip.Name = "Form_StatusStrip"
    Me.Form_StatusStrip.Size = New System.Drawing.Size(888, 22)
    Me.Form_StatusStrip.TabIndex = 105
    Me.Form_StatusStrip.Text = " "
    '
    'Form_ProgressBar
    '
    Me.Form_ProgressBar.Maximum = 20
    Me.Form_ProgressBar.Name = "Form_ProgressBar"
    Me.Form_ProgressBar.Size = New System.Drawing.Size(150, 17)
    Me.Form_ProgressBar.Step = 1
    Me.Form_ProgressBar.Visible = False
    '
    'Label_Status
    '
    Me.Label_Status.Name = "Label_Status"
    Me.Label_Status.Size = New System.Drawing.Size(10, 17)
    Me.Label_Status.Text = " "
    '
    'Combo_SubFund
    '
    Me.Combo_SubFund.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Combo_SubFund.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SubFund.Location = New System.Drawing.Point(114, 470)
    Me.Combo_SubFund.Name = "Combo_SubFund"
    Me.Combo_SubFund.Size = New System.Drawing.Size(483, 21)
    Me.Combo_SubFund.TabIndex = 214
    '
    'Label21
    '
    Me.Label21.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label21.Location = New System.Drawing.Point(2, 474)
    Me.Label21.Name = "Label21"
    Me.Label21.Size = New System.Drawing.Size(106, 16)
    Me.Label21.TabIndex = 215
    Me.Label21.Text = "Strategy (Sub Fund)"
    '
    'Combo_Broker
    '
    Me.Combo_Broker.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Combo_Broker.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Broker.Location = New System.Drawing.Point(114, 542)
    Me.Combo_Broker.Name = "Combo_Broker"
    Me.Combo_Broker.Size = New System.Drawing.Size(191, 21)
    Me.Combo_Broker.TabIndex = 216
    '
    'Label8
    '
    Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label8.Location = New System.Drawing.Point(2, 546)
    Me.Label8.Name = "Label8"
    Me.Label8.Size = New System.Drawing.Size(104, 16)
    Me.Label8.TabIndex = 217
    Me.Label8.Text = "Broker"
    '
    'Combo_TradeStatus
    '
    Me.Combo_TradeStatus.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Combo_TradeStatus.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_TradeStatus.Location = New System.Drawing.Point(114, 569)
    Me.Combo_TradeStatus.Name = "Combo_TradeStatus"
    Me.Combo_TradeStatus.Size = New System.Drawing.Size(191, 21)
    Me.Combo_TradeStatus.TabIndex = 218
    '
    'Label6
    '
    Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label6.Location = New System.Drawing.Point(2, 573)
    Me.Label6.Name = "Label6"
    Me.Label6.Size = New System.Drawing.Size(102, 16)
    Me.Label6.TabIndex = 219
    Me.Label6.Text = "Trade Status"
    '
    'Combo_TransactionCounterparty
    '
    Me.Combo_TransactionCounterparty.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Combo_TransactionCounterparty.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_TransactionCounterparty.Location = New System.Drawing.Point(114, 596)
    Me.Combo_TransactionCounterparty.Name = "Combo_TransactionCounterparty"
    Me.Combo_TransactionCounterparty.Size = New System.Drawing.Size(191, 21)
    Me.Combo_TransactionCounterparty.TabIndex = 220
    '
    'Label_Counterparty
    '
    Me.Label_Counterparty.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label_Counterparty.Location = New System.Drawing.Point(2, 600)
    Me.Label_Counterparty.Name = "Label_Counterparty"
    Me.Label_Counterparty.Size = New System.Drawing.Size(104, 16)
    Me.Label_Counterparty.TabIndex = 221
    Me.Label_Counterparty.Text = "Counterparty"
    '
    'Combo_SpecificShareClass
    '
    Me.Combo_SpecificShareClass.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Combo_SpecificShareClass.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SpecificShareClass.Location = New System.Drawing.Point(114, 497)
    Me.Combo_SpecificShareClass.Name = "Combo_SpecificShareClass"
    Me.Combo_SpecificShareClass.Size = New System.Drawing.Size(483, 21)
    Me.Combo_SpecificShareClass.TabIndex = 222
    '
    'Label_SpecificShareClass
    '
    Me.Label_SpecificShareClass.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label_SpecificShareClass.Location = New System.Drawing.Point(4, 500)
    Me.Label_SpecificShareClass.Name = "Label_SpecificShareClass"
    Me.Label_SpecificShareClass.Size = New System.Drawing.Size(112, 39)
    Me.Label_SpecificShareClass.TabIndex = 223
    Me.Label_SpecificShareClass.Text = "Transaction Specific to Share Class :"
    '
    'Button_UpdatePendingTransactions
    '
    Me.Button_UpdatePendingTransactions.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Button_UpdatePendingTransactions.Location = New System.Drawing.Point(114, 628)
    Me.Button_UpdatePendingTransactions.Name = "Button_UpdatePendingTransactions"
    Me.Button_UpdatePendingTransactions.Size = New System.Drawing.Size(191, 34)
    Me.Button_UpdatePendingTransactions.TabIndex = 224
    Me.Button_UpdatePendingTransactions.Text = "Change Pending Transactions"
    Me.Button_UpdatePendingTransactions.UseVisualStyleBackColor = True
    '
    'Button_SaveSelectedTransactions
    '
    Me.Button_SaveSelectedTransactions.Location = New System.Drawing.Point(3, 38)
    Me.Button_SaveSelectedTransactions.Name = "Button_SaveSelectedTransactions"
    Me.Button_SaveSelectedTransactions.Size = New System.Drawing.Size(191, 34)
    Me.Button_SaveSelectedTransactions.TabIndex = 225
    Me.Button_SaveSelectedTransactions.Text = "Save Selected Transactions"
    Me.Button_SaveSelectedTransactions.UseVisualStyleBackColor = True
    '
    'Button_SaveAllTransactions
    '
    Me.Button_SaveAllTransactions.Location = New System.Drawing.Point(246, 38)
    Me.Button_SaveAllTransactions.Name = "Button_SaveAllTransactions"
    Me.Button_SaveAllTransactions.Size = New System.Drawing.Size(191, 34)
    Me.Button_SaveAllTransactions.TabIndex = 226
    Me.Button_SaveAllTransactions.Text = "Save All Transactions"
    Me.Button_SaveAllTransactions.UseVisualStyleBackColor = True
    '
    'Button_Clear
    '
    Me.Button_Clear.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Button_Clear.Location = New System.Drawing.Point(729, 38)
    Me.Button_Clear.Name = "Button_Clear"
    Me.Button_Clear.Size = New System.Drawing.Size(154, 34)
    Me.Button_Clear.TabIndex = 227
    Me.Button_Clear.Text = "Clear List"
    Me.Button_Clear.UseVisualStyleBackColor = True
    '
    'TabControl_Import
    '
    Me.TabControl_Import.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.TabControl_Import.Controls.Add(Me.TabPage_Import)
    Me.TabControl_Import.Controls.Add(Me.TabPage_Status)
    Me.TabControl_Import.Location = New System.Drawing.Point(3, 81)
    Me.TabControl_Import.Name = "TabControl_Import"
    Me.TabControl_Import.SelectedIndex = 0
    Me.TabControl_Import.Size = New System.Drawing.Size(880, 347)
    Me.TabControl_Import.TabIndex = 228
    '
    'TabPage_Import
    '
    Me.TabPage_Import.Controls.Add(Me.Grid_Transactions)
    Me.TabPage_Import.Location = New System.Drawing.Point(4, 22)
    Me.TabPage_Import.Name = "TabPage_Import"
    Me.TabPage_Import.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage_Import.Size = New System.Drawing.Size(872, 321)
    Me.TabPage_Import.TabIndex = 0
    Me.TabPage_Import.Text = "Import Trades"
    Me.TabPage_Import.UseVisualStyleBackColor = True
    '
    'TabPage_Status
    '
    Me.TabPage_Status.Controls.Add(Me.TextBox_Status)
    Me.TabPage_Status.Location = New System.Drawing.Point(4, 22)
    Me.TabPage_Status.Name = "TabPage_Status"
    Me.TabPage_Status.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage_Status.Size = New System.Drawing.Size(872, 321)
    Me.TabPage_Status.TabIndex = 1
    Me.TabPage_Status.Text = "Status Messages"
    Me.TabPage_Status.UseVisualStyleBackColor = True
    '
    'TextBox_Status
    '
    Me.TextBox_Status.AcceptsReturn = True
    Me.TextBox_Status.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.TextBox_Status.CausesValidation = False
    Me.TextBox_Status.Location = New System.Drawing.Point(6, 6)
    Me.TextBox_Status.Multiline = True
    Me.TextBox_Status.Name = "TextBox_Status"
    Me.TextBox_Status.ReadOnly = True
    Me.TextBox_Status.ScrollBars = System.Windows.Forms.ScrollBars.Both
    Me.TextBox_Status.Size = New System.Drawing.Size(860, 309)
    Me.TextBox_Status.TabIndex = 0
    '
    'frmImportTransactions
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(888, 693)
    Me.Controls.Add(Me.TabControl_Import)
    Me.Controls.Add(Me.Button_Clear)
    Me.Controls.Add(Me.Button_SaveAllTransactions)
    Me.Controls.Add(Me.Button_SaveSelectedTransactions)
    Me.Controls.Add(Me.Button_UpdatePendingTransactions)
    Me.Controls.Add(Me.Combo_SpecificShareClass)
    Me.Controls.Add(Me.Label_SpecificShareClass)
    Me.Controls.Add(Me.Combo_TransactionCounterparty)
    Me.Controls.Add(Me.Label_Counterparty)
    Me.Controls.Add(Me.Combo_TradeStatus)
    Me.Controls.Add(Me.Label6)
    Me.Controls.Add(Me.Combo_Broker)
    Me.Controls.Add(Me.Label8)
    Me.Controls.Add(Me.Combo_SubFund)
    Me.Controls.Add(Me.Label21)
    Me.Controls.Add(Me.Form_StatusStrip)
    Me.Controls.Add(Me.RootMenu)
    Me.Controls.Add(Me.Combo_TransactionFund)
    Me.Controls.Add(Me.label_CptyIsFund)
    Me.Name = "frmImportTransactions"
    Me.Text = "Import Transactions"
    CType(Me.Grid_Transactions, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Form_StatusStrip.ResumeLayout(False)
    Me.Form_StatusStrip.PerformLayout()
    Me.TabControl_Import.ResumeLayout(False)
    Me.TabPage_Import.ResumeLayout(False)
    Me.TabPage_Status.ResumeLayout(False)
    Me.TabPage_Status.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "

  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
  ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
  ''' </summary>
  Private WithEvents _MainForm As VeniceMain

  ' Form ToolTip
  ''' <summary>
  ''' Control to provide tooltip functionality.
  ''' </summary>
  Private FormTooltip As New ToolTip()

  ' Form Menu


  ' Form Constants, specific to the table being updated.

  ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
  ''' </summary>
  Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

  ' Form Locals, initialised on 'New' defining what standard data items to use
  ''' <summary>
  ''' Variable holding the form's data table name. taken from the "ThisStandardDataset" object.
  ''' </summary>
  Private THIS_TABLENAME As String
  ''' <summary>
  ''' Variable holding the form's data adaptor name. taken from the "ThisStandardDataset" object.
  ''' </summary>
  Private THIS_ADAPTORNAME As String
  ''' <summary>
  ''' Variable holding the form's dataset name. taken from the "ThisStandardDataset" object.
  ''' </summary>
  Private THIS_DATASETNAME As String


  ' The standard ChangeID for this form. e.g. tblFund
  ''' <summary>
  ''' The standard ChangeID for this form.
  ''' </summary>
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

  ' Form Specific Order fields
  ''' <summary>
  ''' Field Name to order the Select combo by.
  ''' </summary>
  Private THIS_FORM_OrderBy As String

  ' Form specific Permissioning variables
  ''' <summary>
  ''' The THI s_ FOR m_ permission area
  ''' </summary>
  Private THIS_FORM_PermissionArea As String
  ''' <summary>
  ''' The THI s_ FOR m_ permission type
  ''' </summary>
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
  ''' <summary>
  ''' The THI s_ FOR m_ form ID
  ''' </summary>
  Private THIS_FORM_FormID As VeniceFormID

  ' Data Structures

  ''' <summary>
  ''' My dataset
  ''' </summary>
  Private myDataset As DSSelectTransaction
  ''' <summary>
  ''' My table
  ''' </summary>
  Private myTable As DSSelectTransaction.tblSelectTransactionDataTable
  ''' <summary>
  ''' My data view
  ''' </summary>
  Private myDataView As DataView

  ''' <summary>
  ''' The this standard dataset
  ''' </summary>
  Private ThisStandardDataset As RenaissanceGlobals.StandardDataset

  ' Active Element.

  ''' <summary>
  ''' The __ is over cancel button
  ''' </summary>
  Private __IsOverCancelButton As Boolean
  ''' <summary>
  ''' The _ in use
  ''' </summary>
  Private _InUse As Boolean

  ' Form Status Flags

  ''' <summary>
  ''' The form is valid
  ''' </summary>
  Private FormIsValid As Boolean
  ''' <summary>
  ''' The in paint
  ''' </summary>
  Private InPaint As Boolean
  ''' <summary>
  ''' The _ form open failed
  ''' </summary>
  Private _FormOpenFailed As Boolean

  ' User Permission Flags

  ''' <summary>
  ''' The has read permission
  ''' </summary>
  Private HasReadPermission As Boolean
  ''' <summary>
  ''' The has update permission
  ''' </summary>
  Private HasUpdatePermission As Boolean
  ''' <summary>
  ''' The has insert permission
  ''' </summary>
  Private HasInsertPermission As Boolean
  ''' <summary>
  ''' The has delete permission
  ''' </summary>
  Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

  ''' <summary>
  ''' Gets the main form.
  ''' </summary>
  ''' <value>The main form.</value>
  Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
    ' Public property to return handle to the 'Main' Venice form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

  ''' <summary>
  ''' Gets or sets a value indicating whether this instance is over cancel button.
  ''' </summary>
  ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
  Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
    ' Public property maintaining a value indicating if the cursor is over the 'Cancel'
    ' Button on this form.
    ' This property is specifically designed for use by the field formating Event functions
    ' In order that they do not impose format restrictions if the user is about to click the 
    ' 'Cancel' button.
    '
    Get
      Return __IsOverCancelButton
    End Get
    Set(ByVal Value As Boolean)
      __IsOverCancelButton = Value
    End Set
  End Property

  ''' <summary>
  ''' Gets a value indicating whether this instance is in paint.
  ''' </summary>
  ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
  Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
    Get
      Return InPaint
    End Get
  End Property

  ''' <summary>
  ''' Gets a value indicating whether [in use].
  ''' </summary>
  ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
    Get
      Return _InUse
    End Get
  End Property

  ''' <summary>
  ''' Gets a value indicating whether [form open failed].
  ''' </summary>
  ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

#End Region

  ''' <summary>
  ''' Initializes a new instance of the <see cref="frmImportTransactions"/> class.
  ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  Public Sub New(ByVal pMainForm As VeniceMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()


    _MainForm = pMainForm
    AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

    _FormOpenFailed = False
    _InUse = True

    ' Default Select and Order fields.

    THIS_FORM_OrderBy = "TransactionFund, TransactionInstrument"

    ' Form Permissioning :-

    THIS_FORM_PermissionArea = Me.Name
    THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

    ' 'This' form ID

    THIS_FORM_FormID = VeniceFormID.frmImportTransactions

    ' This form's dataset type.

    ThisStandardDataset = RenaissanceStandardDatasets.tblSelectTransaction ' This Defines the Form Data !!! 


    ' Data object names standard to this Form type.

    THIS_TABLENAME = ThisStandardDataset.TableName
    THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
    THIS_DATASETNAME = ThisStandardDataset.DatasetName

    THIS_FORM_ChangeID = ThisStandardDataset.ChangeID


    ' Establish / Retrieve data objects for this form.

    myDataset = New DSSelectTransaction
    myTable = myDataset.tblSelectTransaction

    If Not myTable.Columns.Contains("SpecificUnitName") Then
      myTable.Columns.Add(New DataColumn("SpecificUnitName", GetType(String)))
    End If
    If Not myTable.Columns.Contains("SubFundName") Then
      myTable.Columns.Add(New DataColumn("SubFundName", GetType(String)))
    End If

    If Not myTable.Columns.Contains("BrokerName") Then
      myTable.Columns.Add(New DataColumn("BrokerName", GetType(String)))
    End If
    ' Report
    SetTransactionReportMenu(RootMenu)

    ' Grid Fields
    Dim FieldsMenu As ToolStripMenuItem = SetFieldSelectMenu(RootMenu)

    ' Establish initial DataView and Initialise Transactions Grig.

    Dim thisCol As C1.Win.C1FlexGrid.Column
    Dim Counter As Integer

    Try
      myDataView = New DataView(myTable, "False", "", DataViewRowState.CurrentRows)
      
      Grid_Transactions.DataSource = myDataView

      ' Format Transactions Grid, hide unwanted columns.

      Grid_Transactions.Cols("TransactionParentID").Move(1)
      Grid_Transactions.Cols("TransactionTicket").Move(2)
      Grid_Transactions.Cols("FundName").Move(3)
      Grid_Transactions.Cols("SubFundName").Move(4)
      Grid_Transactions.Cols("InstrumentDescription").Move(5)
      Grid_Transactions.Cols("TransactionTypeName").Move(6)
      Grid_Transactions.Cols("TransactionUnits").Move(7)
      Grid_Transactions.Cols("TransactionPrice").Move(8)
      Grid_Transactions.Cols("TransactionCosts").Move(9)
      Grid_Transactions.Cols("TransactionSettlement").Move(10)
      Grid_Transactions.Cols("TransactionValueDate").Move(11)
      Grid_Transactions.Cols("TransactionSettlementDate").Move(12)
      Grid_Transactions.Cols("SettlementCurrencyCode").Move(13)
      Grid_Transactions.Cols("TransactionFXRate").Move(14)
      Grid_Transactions.Cols("TradeStatusName").Move(15)
      Grid_Transactions.Cols("BrokerName").Move(16)
      Grid_Transactions.Cols("TransactionBrokerStrategy").Move(17)
      Grid_Transactions.Cols("SpecificUnitName").Move(18)
    Catch ex As Exception
    End Try

    For Each thisCol In Grid_Transactions.Cols
      Try
        Select Case thisCol.Name
          Case "TransactionParentID"
            thisCol.Width = 30
          Case "TransactionTicket"
          Case "FundName"
            thisCol.Width = 150

          Case "InstrumentDescription"
          Case "TransactionTypeName"
          Case "TransactionUnits"
            thisCol.Format = "#,##0.0000"
          Case "TransactionPrice"
            thisCol.Format = "#,##0.0000"
          Case "TransactionSettlement"
            thisCol.Format = "#,##0.00"
            thisCol.Visible = False
          Case "TransactionCosts"
            thisCol.Format = "#,##0.00"
          Case "TransactionFXRate"
            thisCol.Format = "#,##0.0000"
          Case "TransactionValueDate", "TransactionSettlementDate"
            thisCol.Format = DISPLAYMEMBER_DATEFORMAT
          Case "TradeStatusName"
          Case "TransactionBrokerStrategy"
          Case "BrokerName"
          Case "SettlementCurrencyCode"

          Case Else
            thisCol.Visible = False

            Try
              If thisCol.DataType Is GetType(Date) Then
                thisCol.Format = DISPLAYMEMBER_DATEFORMAT
              ElseIf thisCol.DataType Is GetType(Integer) Then
                thisCol.Format = "#,##0"
              ElseIf thisCol.DataType Is GetType(Double) Then
                thisCol.Format = "#,##0.0000"
              End If
            Catch ex As Exception
            End Try
        End Select

        If (thisCol.Caption.ToUpper.StartsWith("TRANSACTION")) Then
          thisCol.Caption = thisCol.Caption.Substring(11)
        End If

        If (thisCol.Visible) Then
          Try
            CType(FieldsMenu.DropDownItems("Menu_GridField_" & thisCol.Name), ToolStripMenuItem).Checked = True
          Catch ex As Exception
          End Try
        End If
      Catch ex As Exception
      End Try
    Next

    Try
      For Counter = 0 To (Grid_Transactions.Cols.Count - 1)
        If (Grid_Transactions.Cols(Counter).Caption.ToUpper.StartsWith("TRANSACTION")) Then
          Grid_Transactions.Cols(Counter).Caption = Grid_Transactions.Cols(Counter).Caption.Substring(11)
        End If
      Next
    Catch ex As Exception
    End Try


    ' Form Control Changed events
    AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

    AddHandler Combo_TransactionFund.SelectedValueChanged, AddressOf Me.FormControlChanged

    AddHandler Combo_TransactionFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType

    AddHandler Combo_TransactionFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

    AddHandler Combo_TransactionFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

    AddHandler Combo_TransactionFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

    ' Set up the ToolTip
    MainForm.SetFormToolTip(Me, FormTooltip)

  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
  ''' <summary>
  ''' Resets the form.
  ''' </summary>
  Public Sub ResetForm() Implements StandardVeniceForm.ResetForm
    THIS_FORM_OrderBy = "TransactionFund, TransactionInstrument"

    Call Form_Load(Me, New System.EventArgs)
  End Sub

  ''' <summary>
  ''' Closes the form.
  ''' </summary>
  Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
    ALWAYS_CLOSE_THIS_FORM = True
    Me.Close()
  End Sub

  ''' <summary>
  ''' Handles the Load event of the Form control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
    _FormOpenFailed = False
    _InUse = True

    ' Initialise Data structures. Connection, Adaptor and Dataset.

    If Not (MainForm Is Nothing) Then
      FormIsValid = True
    Else
      MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (myDataset Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If


    ' Check User permissions
    Try
      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

        FormIsValid = False
        _FormOpenFailed = True

        Exit Sub
      End If
    Catch ex As Exception
      FormIsValid = False
      _FormOpenFailed = True

      Exit Sub
    End Try

    ' Initialse form

    InPaint = True
    IsOverCancelButton = False

    Call SetFundCombo()
    Call SetSubFundCombo()
    Call SetSpecificShareClassCombo()
    Call SetTradeStatusCombo()
    Call SetCounterpartyCombo()
    Call SetBrokerCombo()

    TabControl_Import.SelectedTab = TabPage_Import

    ' Initialise main select controls
    ' Build Sorted data list from which this form operates

    Try
      If (Combo_TransactionFund.Items.Count > 1) Then
        Me.Combo_TransactionFund.SelectedIndex = 1
      Else
        Me.Combo_TransactionFund.SelectedIndex = -1
      End If

      ' Default Grid Sort : By Value Date and ParentID (Effectively Entry Order)
      For Each thisCol As C1.Win.C1FlexGrid.Column In Grid_Transactions.Cols
        thisCol.Sort = SortFlags.None
      Next
      Grid_Transactions.Cols("TransactionValueDate").Sort = SortFlags.Ascending
      Grid_Transactions.Cols("TransactionParentID").Sort = SortFlags.Ascending
      Grid_Transactions.Sort(SortFlags.UseColSort, Grid_Transactions.Cols("TransactionValueDate").Index, Grid_Transactions.Cols("TransactionParentID").Index)

      Call SetSortedRows()

      Call MainForm.SetComboSelectionLengths(Me)
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error initialising Select Transaction Form.", ex.StackTrace, True)
    End Try

    InPaint = False

  End Sub

  ''' <summary>
  ''' Handles the Closing event of the frm control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
  Private Sub frm_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    Dim HideForm As Boolean

    ' Hide or Close this form ?
    ' All depends on how many of this form type are Open or in Cache...

    _InUse = False

    If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
      HideForm = False
    Else
      HideForm = True
      If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
        HideForm = False
      End If
    End If

    If HideForm = True Then
      MainForm.HideInFormsCollection(Me)
      Me.Hide() ' NPP Fix

      e.Cancel = True
    Else
      Try
        MainForm.RemoveFromFormsCollection(Me)
        RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

        RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

        RemoveHandler Combo_TransactionFund.SelectedValueChanged, AddressOf Me.FormControlChanged

        RemoveHandler Combo_TransactionFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        RemoveHandler Combo_TransactionFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

        RemoveHandler Combo_TransactionFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

        RemoveHandler Combo_TransactionFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

      Catch ex As Exception
      End Try
    End If

  End Sub

#End Region

  ' Routine to handle changes / updates to tables by this and other windows.
  ' If this, or any other, form posts a change to a table, then it will invoke an update event 
  ' detailing what tables have been altered.
  ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
  ' the 'VeniceAutoUpdate' event of the main Venice form.
  ' Each form may them react as appropriate to changes in any table that might impact it.
  '
  ''' <summary>
  ''' Autoes the update.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    Dim OrgInPaint As Boolean
    Dim KnowledgeDateChanged As Boolean
    Dim RefreshGrid As Boolean

    If (Not Me.Created) OrElse (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then
      Exit Sub
    End If

    OrgInPaint = InPaint

    Try
      InPaint = True
      KnowledgeDateChanged = False
      RefreshGrid = False

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
        KnowledgeDateChanged = True
      End If
    Catch ex As Exception
    End Try

    ' ****************************************************************
    ' Check for changes relevant to this form
    ' ****************************************************************


    ' Changes to the tblFund table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFund) = True) Or KnowledgeDateChanged Then
      Try
        ' Re-Set combo.
        Call SetFundCombo()
      Catch ex As Exception
        MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblFund", ex.StackTrace, True)
      End Try
    End If

    ' Changes to the tblSubFund table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblSubFund) = True) Or KnowledgeDateChanged Then
      Try
        ' Re-Set combo.
        Call SetSubFundCombo()
      Catch ex As Exception
        MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblSubFund", ex.StackTrace, True)
      End Try
    End If

    ' Changes to the tblInstrument table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrument) = True) Or KnowledgeDateChanged Then
      Try
        ' Re-Set combo.
        Call SetSpecificShareClassCombo()

      Catch ex As Exception
        MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblInstrument", ex.StackTrace, True)
      End Try
    End If

    ' Changes to the tblTradeStatus table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblTradeStatus) = True) Or KnowledgeDateChanged Then
      Try
        ' Re-Set combo.
        Call SetTradeStatusCombo()
      Catch ex As Exception
        MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblTradeStatus", ex.StackTrace, True)
      End Try
    End If

    ' Changes to the tblCounterparty table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblCounterparty) = True) Or KnowledgeDateChanged Then
      Try
        ' Re-Set combo.
        Call SetCounterpartyCombo()
      Catch ex As Exception
        MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblCounterparty", ex.StackTrace, True)
      End Try
    End If

    ' Changes to the tblBroker table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblBroker) = True) Or KnowledgeDateChanged Then
      Try
        ' Re-Set combo.
        Call SetBrokerCombo()
      Catch ex As Exception
        MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblBroker", ex.StackTrace, True)
      End Try
    End If





    ' Changes to the tblUserPermissions table :-

    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

        ' Check ongoing permissions.

        Call CheckPermissions()
        If (HasReadPermission = False) Then
          Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

          FormIsValid = False
          InPaint = OrgInPaint
          Me.Close()
          Exit Sub
        End If

        RefreshGrid = True

      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: tblUserPermissions", ex.StackTrace, True)
    End Try


    ' ****************************************************************
    ' Changes to the Main FORM table :-
    ' (or the tblTransactions table)
    ' ****************************************************************

    Try
      If (e.TableChanged(THIS_FORM_ChangeID) = True) Or _
         (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblTransaction) = True) Or _
         (RefreshGrid = True) Or _
         KnowledgeDateChanged Then

        ' Re-Set Controls etc.
        Call SetSortedRows(True)

      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: Main Form table", ex.StackTrace, True)
    End Try

    InPaint = OrgInPaint

  End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "



  ''' <summary>
  ''' Sets the sorted rows.
  ''' </summary>
  ''' <param name="ForceRefresh">if set to <c>true</c> [force refresh].</param>
  Private Sub SetSortedRows(Optional ByVal ForceRefresh As Boolean = False)
    ' *******************************************************************************
    ' Build a Select String appropriate to the form status and apply it to the DataView object.
    ' *******************************************************************************

    If (Me.Created) And (Not Me.IsDisposed) Then

      Try

        If ForceRefresh Then
          myDataView.RowFilter = "False"
        End If
        If (myDataView IsNot Nothing) AndAlso (Not (myDataView.RowFilter = "true")) Then
          myDataView.RowFilter = "true"
        End If

      Catch ex As Exception
        Try
          myDataView.RowFilter = "true"
        Catch ex_inner As Exception
        End Try

      End Try

      Me.Label_Status.Text = "(" & myDataView.Count.ToString & " Records)"

    End If

  End Sub


  ' Check User permissions
  ''' <summary>
  ''' Checks the permissions.
  ''' </summary>
  Private Sub CheckPermissions()
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

  End Sub

  ''' <summary>
  ''' Basic event handler, called when a form value is changed.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' *******************************************************************************
    ' In the event of one of the Form Controls changing, refresh the data grid.
    ' *******************************************************************************

    If InPaint = False Then

      Call SetSortedRows()

    End If
  End Sub


  ''' <summary>
  ''' Handles the DoubleClick event of the Grid_Transactions control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Transactions_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid_Transactions.DoubleClick
    ' ***************************************************************************************
    ' Spawn an Add/Edit Transactions form reflecting the chosen selection of transactions
    ' and pointing to the transaction which was double clicked.
    ' ***************************************************************************************

    Try

      Dim ParentID As Integer
      Dim thisFormHandle As RenaissanceGlobals.RenaissanceFormHandle

      If (Grid_Transactions.RowSel > 0) AndAlso (Grid_Transactions.RowSel < Grid_Transactions.Rows.Count) Then
        ParentID = Nz(Me.Grid_Transactions.Rows(Grid_Transactions.RowSel).DataSource("TransactionParentID"), 0)

        If (ParentID > 0) Then
          thisFormHandle = MainForm.New_VeniceForm(VeniceFormID.frmTransaction)
          CType(thisFormHandle.Form, frmTransaction).FormSelectCriteria = "TransactionParentID=" & ParentID.ToString
          CType(thisFormHandle.Form, frmTransaction).MoveToAuditID(ParentID)
        End If
      End If

    Catch ex As Exception
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error opening Transactions Form.", ex.StackTrace, True)
    End Try

  End Sub


#End Region

#Region " Set Form Combos (Form Specific Code) "

  ''' <summary>
  ''' Sets the Fund combo.
  ''' </summary>
  Private Sub SetFundCombo()
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_TransactionFund, _
    RenaissanceStandardDatasets.tblFund, _
    "FundName", _
    "FundID", _
    "FundClosed=0", False, True, True, 0, "")   ' 

  End Sub

  ''' <summary>
  ''' Sets the Sub-Fund combo.
  ''' </summary>
  Private Sub SetSubFundCombo()
    ' *******************************************************************************
    '
    ' *******************************************************************************
    Dim FundID As Integer = 0

    Try

      If (IsNumeric(Combo_TransactionFund.SelectedValue)) Then
        FundID = CInt(Combo_TransactionFund.SelectedValue)
      End If

    Catch ex As Exception
    End Try

    Call MainForm.SetTblGenericCombo( _
      Me.Combo_SubFund, _
      RenaissanceStandardDatasets.tblSubFundHeirarchy, _
      "Level|SubFundName", _
      "SubFundID", _
      "TopFundID=" & FundID.ToString, False, False, True, 0, "", "TreeOrder")   ' 

  End Sub

  ''' <summary>
  ''' Sets the Specific Share Class Combo.
  ''' </summary>
  Private Sub SetSpecificShareClassCombo()
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Dim ThisFundID As Integer = CInt(Nz(Combo_TransactionFund.SelectedValue, 0))

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_SpecificShareClass, _
    RenaissanceStandardDatasets.tblInstrument, _
    "InstrumentDescription", _
    "InstrumentID", _
    "InstrumentFundID=" & ThisFundID.ToString, False, True, True, 0, "")   ' 

  End Sub

  ''' <summary>
  ''' Sets the trade status combo.
  ''' </summary>
  Private Sub SetTradeStatusCombo()
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_TradeStatus, _
    RenaissanceStandardDatasets.tblTradeStatus, _
    "tradeStatusName", _
    "tradeStatusID", _
    "", False, True, True, 0, "")   ' 

  End Sub

  ''' <summary>
  ''' Sets the counterparty combo.
  ''' </summary>
  Private Sub SetCounterpartyCombo()
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_TransactionCounterparty, _
    RenaissanceStandardDatasets.tblCounterparty, _
    "CounterpartyName", _
    "CounterpartyID", _
    "", False, True, True, 0, "")   ' 

  End Sub
  ''' <summary>
  ''' Sets the Broker combo.
  ''' </summary>
  Private Sub SetBrokerCombo()
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_Broker, _
    RenaissanceStandardDatasets.tblBroker, _
    "BrokerDescription", _
    "BrokerID", _
    "", False, True, True, 0, "")   ' 

  End Sub

#End Region

#Region " Transaction report Menu"

  ''' <summary>
  ''' Sets the transaction report menu.
  ''' </summary>
  ''' <param name="RootMenu">The root menu.</param>
  ''' <returns>MenuStrip.</returns>
  Private Function SetTransactionReportMenu(ByRef RootMenu As MenuStrip) As MenuStrip
    ' ***************************************************************************************
    ' Setup the Transaction Reports menu with appropriate items and callbacks
    '
    ' ***************************************************************************************

    Dim ReportMenu As New ToolStripMenuItem("Transaction &Reports")
    'Dim newMenuItem As ToolStripMenuItem

    'newMenuItem = ReportMenu.DropDownItems.Add("Transaction &Ticket", Nothing, AddressOf Me.rptTransactionTicket)

    'ReportMenu.DropDownItems.Add(New ToolStripSeparator)

    'newMenuItem = ReportMenu.DropDownItems.Add("Transaction By &Value Date", Nothing, AddressOf Me.rptTransactionByDate)
    'newMenuItem = ReportMenu.DropDownItems.Add("Transaction By &Entry Date", Nothing, AddressOf Me.rptTransactionByEntryDate)
    'newMenuItem = ReportMenu.DropDownItems.Add("Transaction By &Group", Nothing, AddressOf Me.rptTransactionByGroup)
    'newMenuItem = ReportMenu.DropDownItems.Add("Transaction By &Instrument", Nothing, AddressOf Me.rptTransactionByStock)
    'newMenuItem = ReportMenu.DropDownItems.Add("&Incomplete Transaction Report", Nothing, AddressOf Me.rptIncompleteTransactions)
    'newMenuItem = ReportMenu.DropDownItems.Add("Transaction &Price Differences", Nothing, AddressOf Me.rptTransactionPriceDifference)
    'newMenuItem = ReportMenu.DropDownItems.Add("&Instrument Position Report", Nothing, AddressOf Me.rptInstrumentPositionsReport)

    'ReportMenu.DropDownItems.Add(New ToolStripSeparator)

    'newMenuItem = ReportMenu.DropDownItems.Add("Trade &Blotter", Nothing, AddressOf Me.rptTradeBlotter)
    'newMenuItem = ReportMenu.DropDownItems.Add("&Final Trade Blotter", Nothing, AddressOf Me.rptFinalTradeBlotter)
    'newMenuItem = ReportMenu.DropDownItems.Add("Trade &Revision Blotter", Nothing, AddressOf Me.rptTradeRevisionBlotter)
    'newMenuItem = ReportMenu.DropDownItems.Add("Final Trade &Revision Blotter", Nothing, AddressOf Me.rptFinalTradeRevisionBlotter)

    'ReportMenu.DropDownItems.Add(New ToolStripSeparator)

    'newMenuItem = ReportMenu.DropDownItems.Add("F&X Blotter", Nothing, AddressOf Me.rptFXBlotter)
    'newMenuItem = ReportMenu.DropDownItems.Add("Final FX Blotter", Nothing, AddressOf Me.rptFinalFXBlotter)

    RootMenu.Items.Add(ReportMenu)
    Return RootMenu

  End Function

  ''' <summary>
  ''' Sets the field select menu.
  ''' </summary>
  ''' <param name="RootMenu">The root menu.</param>
  ''' <returns>ToolStripMenuItem.</returns>
  Private Function SetFieldSelectMenu(ByRef RootMenu As MenuStrip) As ToolStripMenuItem
    ' ***************************************************************************************
    ' Build the FieldSelect Menu.
    '
    ' Add an item for each field in the SelectTransactions table, when selected the item
    ' will show or hide the associated field on the transactions grid.
    ' ***************************************************************************************

    Dim ColumnNames(-1) As String
    Dim ColumnCount As Integer

    Dim FieldsMenu As New ToolStripMenuItem("Grid &Fields")
    FieldsMenu.Name = "Menu_GridField"

    Dim newMenuItem As ToolStripMenuItem

    ReDim ColumnNames(myTable.Columns.Count - 1)
    For ColumnCount = 0 To (myTable.Columns.Count - 1)
      ColumnNames(ColumnCount) = myTable.Columns(ColumnCount).ColumnName
      If (ColumnNames(ColumnCount).StartsWith("Transaction")) Then
        ColumnNames(ColumnCount) = ColumnNames(ColumnCount).Substring(11) & "."
      End If
    Next
    Array.Sort(ColumnNames)

    For ColumnCount = 0 To (ColumnNames.Length - 1)
      newMenuItem = FieldsMenu.DropDownItems.Add(ColumnNames(ColumnCount), Nothing, AddressOf Me.ShowGridField)

      If (ColumnNames(ColumnCount).EndsWith(".")) Then
        newMenuItem.Name = "Menu_GridField_Transaction" & ColumnNames(ColumnCount).Substring(0, ColumnNames(ColumnCount).Length - 1)
      Else
        newMenuItem.Name = "Menu_GridField_" & ColumnNames(ColumnCount)
      End If
    Next

    RootMenu.Items.Add(FieldsMenu)
    Return FieldsMenu

  End Function

  ''' <summary>
  ''' Shows the grid field.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
  Private Sub ShowGridField(ByVal sender As Object, ByVal e As EventArgs)
    ' ***************************************************************************************
    ' Callback function for the Grid Fields Menu items.
    '
    ' ***************************************************************************************

    Try
      If TypeOf (sender) Is ToolStripMenuItem Then
        Dim thisMenuItem As ToolStripMenuItem
        Dim FieldName As String

        thisMenuItem = CType(sender, ToolStripMenuItem)

        FieldName = thisMenuItem.Name
        If (FieldName.StartsWith("Menu_GridField_")) Then
          FieldName = FieldName.Substring(15)
        End If

        thisMenuItem.Checked = Not thisMenuItem.Checked

        If (thisMenuItem.Checked) Then
          Grid_Transactions.Cols(FieldName).Visible = True
        Else
          Grid_Transactions.Cols(FieldName).Visible = False
        End If
      End If
    Catch ex As Exception

    End Try
  End Sub

#End Region

#Region " Bug hunting "

  ' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
  ' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
  ' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

  ''' <summary>
  ''' Called when [row updating].
  ''' </summary>
  ''' <param name="Sender">The sender.</param>
  ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  ''' <summary>
  ''' Called when [row updated].
  ''' </summary>
  ''' <param name="Sender">The sender.</param>
  ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  ''' <summary>
  ''' Called when [row fill error].
  ''' </summary>
  ''' <param name="Sender">The sender.</param>
  ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
    Dim a As Integer

    a = 1
  End Sub

#End Region






  Private Sub Grid_Transactions_DragOver(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles Grid_Transactions.DragOver
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Try

      'check that we have the type of data we want

      If (e.Data.GetDataPresent("FileGroupDescriptorW")) Then
        ' Outlook Attachments

        e.Effect = DragDropEffects.Copy

      ElseIf (e.Data.GetDataPresent("FileDrop")) Then
        ' File Explorer

        e.Effect = DragDropEffects.Copy

      ElseIf (e.Data.GetDataPresent("XML Spreadsheet")) Then
        ' 

        e.Effect = DragDropEffects.Copy

      ElseIf (e.Data.GetDataPresent("Csv")) Then
        ' 

        e.Effect = DragDropEffects.Copy

      ElseIf (e.Data.GetDataPresent("Text")) Then
        '

        e.Effect = DragDropEffects.Copy

      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Grid_Transactions_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles Grid_Transactions.DragDrop
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Try
      Dim CSVLines() As String = Nothing

      If (e.Data.GetDataPresent("FileContents")) Then ' If (e.Data.GetDataPresent("FileGroupDescriptorW")) Then 
        ' Outlook 
        Dim sr As StreamReader = New StreamReader(CType(e.Data.GetData("FileContents"), MemoryStream))

        CSVLines = sr.ReadToEnd.Split(vbCrLf.ToCharArray, StringSplitOptions.RemoveEmptyEntries)
        sr.Close()

        ParseFileContents(CSVLines, "FileDrop")

      ElseIf (e.Data.GetDataPresent("FileDrop")) Then
        ' File Explorer
        Dim FileNames() As String = CType(e.Data.GetData("FileDrop"), String())

        If (FileNames IsNot Nothing) AndAlso (FileNames.Length > 0) Then
          Dim sr As StreamReader = Nothing

          For Each thisFile As String In FileNames

            Try

              sr = New StreamReader(thisFile, System.Text.Encoding.UTF8)
              CSVLines = sr.ReadToEnd.Split(vbCrLf.ToCharArray, StringSplitOptions.RemoveEmptyEntries)
              ParseFileContents(CSVLines, "FileDrop")

            Catch ex As Exception
            Finally
              Try
                If (sr IsNot Nothing) Then
                  sr.Close()
                End If
              Catch ex As Exception
              End Try
            End Try

          Next

        End If

      ElseIf (e.Data.GetDataPresent("Csv")) Then
        ' 

        Dim sr As StreamReader = New StreamReader(CType(e.Data.GetData("Csv"), MemoryStream))
        CSVLines = sr.ReadToEnd.Split(vbCrLf.ToCharArray, StringSplitOptions.RemoveEmptyEntries)

        ParseFileContents(CSVLines, "CSV")

      ElseIf (e.Data.GetDataPresent("XML Spreadsheet")) Then
        ' 


      ElseIf (e.Data.GetDataPresent("Text")) Then
        '

        CSVLines = CStr(e.Data.GetData("Text")).Split(vbCrLf.ToCharArray, StringSplitOptions.RemoveEmptyEntries)

        ParseFileContents(CSVLines, "Text")

      End If


    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error in Grid_Transactions_DragDrop()", ex.Message, ex.StackTrace, True)
    End Try
  End Sub

  Private Sub ParseFileContents(ByVal CSVLines() As String, ByVal DropType As String)
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Try
      Dim LineCounter As Integer
      Dim thisLine As String
      Dim thisLineArray() As String

      '

      If (CSVLines Is Nothing) OrElse (CSVLines.Length <= 0) Then
        Exit Sub
      End If

      ' Identify File type

      For LineCounter = 0 To (CSVLines.Length - 1)
        thisLine = CSVLines(LineCounter)

        If (thisLine.Length > 0) AndAlso (thisLine.Contains(";")) Then
          thisLineArray = thisLine.Split(New Char() {";"})

          If (thisLineArray.Count = 3) AndAlso (thisLineArray(0) = "BOF") AndAlso (thisLineArray(2).StartsWith("FR_") OrElse thisLineArray(2).EndsWith("_FR")) Then
            ' BP2S Format ?

            ParseBP2SLongSecuritiesFile(CSVLines)
            Exit Sub

          ElseIf (thisLineArray.Count >= 47) Then
            ' BP2S Format ?

            ParseBP2SLongSecuritiesFile(CSVLines)
            Exit Sub
          ElseIf (thisLineArray.Count >= 15) Then
            ' RB Format ?

            ParseBP2SShortFile(CSVLines)
            Exit Sub
          End If

          Exit For

        ElseIf (thisLine.Length > 0) AndAlso (thisLine.Contains(vbTab)) Then
          thisLineArray = thisLine.Split(New Char() {vbTab})

          If ((thisLineArray.Contains("Code valeur") OrElse thisLineArray.Contains("ISIN") OrElse thisLineArray.Contains("InstrumentID") OrElse thisLineArray.Contains("Instrument ID")) AndAlso (thisLineArray.Contains("Quantité") OrElse thisLineArray.Contains("Quantite") OrElse thisLineArray.Contains("Quantity")) AndAlso (thisLineArray.Contains("Cours") OrElse thisLineArray.Contains("Price"))) Then

            ParseExcelTransactionExport(CSVLines, vbTab)

            Exit Sub
          End If
        ElseIf (thisLine.Length > 0) AndAlso (thisLine.Contains(",")) Then
          thisLineArray = thisLine.Split(New Char() {CChar(",")})

          If ((thisLineArray.Contains("Code valeur") OrElse thisLineArray.Contains("ISIN") OrElse thisLineArray.Contains("InstrumentID") OrElse thisLineArray.Contains("Instrument ID")) AndAlso (thisLineArray.Contains("Quantité") OrElse thisLineArray.Contains("Quantite") OrElse thisLineArray.Contains("Quantity")) AndAlso (thisLineArray.Contains("Cours") OrElse thisLineArray.Contains("Price"))) Then

            ParseExcelTransactionExport(CSVLines, CChar(","))

            Exit Sub
          End If
        End If
      Next

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error in ParseFileContents()", ex.Message, ex.StackTrace, True)

    End Try

  End Sub


  Private Sub ParseBP2SLongSecuritiesFile(ByVal CSVLines() As String)
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Try



    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error in ParseBP2SLongSecuritiesFile()", ex.Message, ex.StackTrace, True)
    End Try
  End Sub


  Private Sub ParseExcelTransactionExport(ByVal CSVLines() As String, ByVal SeparatorCharacter As Char)
    ' *******************************************************************************
    '
    ' *******************************************************************************
    Dim _CultureInfo As Globalization.CultureInfo = New Globalization.CultureInfo("")
    Dim StatusText As New System.Text.StringBuilder

    Dim TradeReference_Col As Integer = (-1)
    Dim FundID_Col As Integer = (-1)
    Dim InstrumentID_Col As Integer = (-1)
    Dim PortfolioCode_Col As Integer = (-1) ' Administrator Code
    Dim ISIN_Col As Integer = (-1)
    Dim Currency_Col As Integer = (-1)
    Dim Quantity_Col As Integer = (-1)
    Dim Price_Col As Integer = (-1)
    Dim Fees_Col As Integer = (-1)
    Dim SettlementCurrency_Col As Integer = (-1)
    Dim FXRate_Col As Integer = (-1)
    Dim TradeDate_Col As Integer = (-1)
    Dim SettlementDate_Col As Integer = (-1)
    Dim BrokerName_Col As Integer = (-1)
    Dim BrokerCode_Col As Integer = (-1)
    Dim BrokerStrategy_Col As Integer = (-1)
    Dim TradeStatusName_Col As Integer = (-1)
    Dim TradeStatusId_Col As Integer = (-1)

    Dim FirstTradeLine As Integer
    Dim LineCounter As Integer
    Dim ColumnCounter As Integer
    Dim FieldValue As String
    Dim thisLine As String
    Dim thisLineArray() As String

    'Dim thisFund As DSFund.tblFundRow
    Dim thisInstrument As DSInstrument.tblInstrumentRow
    Dim thisSettlementCurrency As DSCurrency.tblCurrencyRow
    Dim thisCounterparty As DSCounterparty.tblCounterpartyRow
    Dim thisBroker As DSBroker.tblBrokerRow
    Dim SelectedTransactions() As DSSelectTransaction.tblSelectTransactionRow = Nothing
    Dim thisSelectedTransaction As DSSelectTransaction.tblSelectTransactionRow
    Dim thisFund As DSFund.tblFundRow
    Dim thisTradeStatus As DSTradeStatus.tblTradeStatusRow

    Dim FundID As Integer
    Dim InstrumentID As Integer
    Dim PortfolioCode As String
    Dim TradeReference As String
    Dim TradeDate As Date
    Dim SettlementDate As Date
    Dim BuyOrSell As String
    Dim ISIN As String
    Dim Country As String
    Dim Quantity As Double
    Dim QuantityUnit As String
    Dim Price As Double
    Dim Currency As String
    Dim Comission As Double
    Dim Fees As Double
    Dim SettlementCurrency As String
    Dim BrokerName As String
    Dim BrokerCode As String
    Dim BrokerStrategy As String
    Dim FXRate As Double
    Dim TradeOK As Boolean
    Dim TradeStatusName As String
    Dim TradeStatusId As Integer

    Dim showMsgTab As Boolean = False
    Dim nbrTradeKO As Integer = 0
    Dim nbrTradeRead As Integer = 0
    Try

      For LineCounter = 0 To (CSVLines.Length - 1)
        thisLine = CSVLines(LineCounter)

        If (thisLine.Length > 0) AndAlso (thisLine.Contains(SeparatorCharacter)) Then
          thisLineArray = thisLine.Split(New Char() {SeparatorCharacter})

          If ((thisLineArray.Contains("Code valeur") OrElse thisLineArray.Contains("ISIN") OrElse thisLineArray.Contains("InstrumentID") OrElse thisLineArray.Contains("Instrument ID")) AndAlso (thisLineArray.Contains("Quantité") OrElse thisLineArray.Contains("Quantite") OrElse thisLineArray.Contains("Quantity")) AndAlso (thisLineArray.Contains("Cours") OrElse thisLineArray.Contains("Price"))) Then

            FirstTradeLine = LineCounter + 1

            For ColumnCounter = 0 To (thisLineArray.Length - 1)
              Select Case thisLineArray(ColumnCounter)

                Case "TradeReference", "Trade Reference", "Reference", "Référence", "Référence client"
                  TradeReference_Col = ColumnCounter

                Case "FundID", "Fund ID"
                  FundID_Col = ColumnCounter

                Case "InstrumentID", "Instrument ID"
                  InstrumentID_Col = ColumnCounter

                Case "Portfolio", "PortfolioCode", "Portfolio Code"
                  PortfolioCode_Col = ColumnCounter

                Case "Code valeur", "ISIN"
                  ISIN_Col = ColumnCounter

                Case "Currency", "Devise"
                  Currency_Col = ColumnCounter

                Case "Quantité", "Quantity"
                  Quantity_Col = ColumnCounter

                Case "Cours", "Price"
                  Price_Col = ColumnCounter

                Case "Frais", "Fees", "Costs"
                  Fees_Col = ColumnCounter

                Case "Dev. pmt", "SettlementCurrency", "Settlement Currency"
                  SettlementCurrency_Col = ColumnCounter

                Case "FXRate", "FX Rate"
                  FXRate_Col = ColumnCounter

                Case "TradeDate", "Trade Date"
                  TradeDate_Col = ColumnCounter

                Case "SettlementDate", "Settlement Date"
                  SettlementDate_Col = ColumnCounter

                Case "BrokerName", "Broker Name"
                  BrokerName_Col = ColumnCounter

                Case "BrokerCode", "Broker Code"
                  BrokerCode_Col = ColumnCounter

                Case "BrokerStrategy", "Broker Strategy"
                  BrokerStrategy_Col = ColumnCounter

                Case "TradeStatus", "Trade Status"
                  TradeStatusName_Col = ColumnCounter

                Case "TradeStatusID", "Trade Status ID"
                  TradeStatusId_Col = ColumnCounter

                Case "Frais (dev ref)"

                Case "Dev. réf"


              End Select
            Next

            Exit For
          End If
        End If
      Next


      For LineCounter = FirstTradeLine To (CSVLines.Length - 1)
        thisLine = CSVLines(LineCounter)

        If (Trim(thisLine).Length <= 1) Then
          Continue For
        End If
        If (thisLine.Contains(SeparatorCharacter)) Then
          thisLineArray = thisLine.Split(New Char() {SeparatorCharacter})
          nbrTradeRead = nbrTradeRead + 1

          If (thisLineArray.Count >= Price_Col) AndAlso (thisLineArray.Count >= Quantity_Col) AndAlso (thisLineArray.Count >= ISIN_Col) Then

            FundID = 0
            InstrumentID = 0
            PortfolioCode = ""
            TradeReference = ""
            TradeDate = Now.Date
            SettlementDate = Renaissance_BaseDate
            BuyOrSell = ""
            ISIN = ""
            Country = ""
            Quantity = 0.0#
            QuantityUnit = "UNIT"
            Price = 0.0#
            Currency = ""
            Fees = 0.0#
            Comission = 0.0#
            SettlementCurrency = ""
            BrokerCode = ""
            BrokerStrategy = ""
            BrokerName = ""
            FXRate = 1.0#
            thisSettlementCurrency = Nothing
            thisCounterparty = Nothing
            thisBroker = Nothing
            TradeOK = True
            TradeStatusName = ""
            TradeStatusId = 0

            For ColumnCounter = 0 To (thisLineArray.Length - 1)

              Try
                FieldValue = Trim(thisLineArray(ColumnCounter))

                If (FieldValue.Length > 0) Then

                  Select Case ColumnCounter

                    Case TradeReference_Col
                      TradeReference = Trim(FieldValue)

                    Case FundID_Col
                      FundID = Integer.Parse(FieldValue, _CultureInfo)

                    Case InstrumentID_Col
                      InstrumentID = Integer.Parse(FieldValue, _CultureInfo)

                    Case PortfolioCode_Col
                      PortfolioCode = FieldValue

                    Case ISIN_Col
                      ISIN = FieldValue

                    Case Currency_Col
                      Currency = FieldValue

                    Case Quantity_Col

                      Quantity = Double.Parse(FieldValue, _CultureInfo)

                      If (Quantity < 0.0#) Then
                        BuyOrSell = "SELL"
                      Else
                        BuyOrSell = "BUY"
                      End If

                    Case Price_Col

                      Price = Double.Parse(FieldValue, _CultureInfo)

                    Case Fees_Col
                      Fees = Double.Parse(FieldValue, _CultureInfo)

                    Case SettlementCurrency_Col
                      SettlementCurrency = FieldValue

                    Case FXRate_Col
                      FXRate = Double.Parse(FieldValue, _CultureInfo)

                    Case TradeDate_Col
                      TradeDate = Date.Parse(FieldValue, _CultureInfo)

                    Case SettlementDate_Col
                      SettlementDate = Date.Parse(FieldValue, _CultureInfo)

                    Case BrokerName_Col
                      BrokerName = FieldValue

                    Case BrokerCode_Col
                      BrokerCode = FieldValue

                    Case BrokerStrategy_Col
                      BrokerStrategy = FieldValue

                    Case TradeStatusName_Col
                      TradeStatusName = FieldValue

                    Case TradeStatusId_Col
                      TradeStatusId = Integer.Parse(FieldValue, _CultureInfo)

                  End Select

                End If
              Catch ex As Exception
              End Try

            Next ColumnCounter

            ' Validate 

            If (ISIN.Length <= 0) AndAlso (InstrumentID <= 0) Then
              TradeOK = False
              StatusText.Append("Line " & LineCounter.ToString & " : at least one of ISIN or InstrumentID should be filled in" & vbCrLf)
              'Continue For
            End If

            thisFund = Nothing
            If (FundID > 0) Then
              thisFund = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, FundID)
            End If
            If (thisFund Is Nothing) AndAlso (PortfolioCode.Length > 0) Then
              thisFund = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, 0, "", "RN", "FundAdministratorCode='" & PortfolioCode & "'")
              If (thisFund IsNot Nothing) Then
                FundID = thisFund.FundID
              End If
            End If

            thisInstrument = Nothing
            If (TradeOK) Then
              If (InstrumentID > 0) Then
                thisInstrument = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, InstrumentID)
              End If

              If (thisInstrument Is Nothing) Then
                If (ISIN.Length > 0) And (Currency.Length > 0) Then
                  thisInstrument = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, 0, "", "RN", "InstrumentISIN='" & ISIN & "' AND InstrumentCurrency='" & Currency & "'")
                ElseIf (ISIN.Length > 0) Then
                  thisInstrument = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, 0, "", "RN", "InstrumentISIN='" & ISIN & "'")
                End If
                If (thisInstrument Is Nothing) Then
                  TradeOK = False

                  StatusText.Append("Line " & LineCounter.ToString & " : Could not resolve Instrument from ISIN (" & ISIN & ")" & vbCrLf)

                End If
              End If
            End If

            thisTradeStatus = Nothing
            If (TradeStatusId > 0) Then
              thisTradeStatus = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblTradeStatus, TradeStatusId)
            End If
            If (thisTradeStatus Is Nothing) AndAlso (TradeStatusName.Length > 0) Then
              thisTradeStatus = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblTradeStatus, 0, "", "RN", "TradeStatusName='" & TradeStatusName & "'")
            End If

            If (TradeOK) Then
              If (SettlementCurrency.Length > 0) AndAlso (SettlementCurrency <> Currency) Then
                thisSettlementCurrency = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblCurrency, 0, "", "RN", "CurrencyCode='" & SettlementCurrency & "'")
              End If
            End If

            If (TradeOK) Then
              If (BrokerName.Length > 0) Then
                thisBroker = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblBroker, 0, "", "RN", "BrokerDescription='" & BrokerName & "'")
              End If
              If (thisBroker Is Nothing) AndAlso (BrokerCode.Length > 0) Then
                thisBroker = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblBroker, 0, "", "RN", "EMSX_Broker_ID='" & BrokerCode & "'")
              End If
            End If

            'TODO: check du brokerStrategy
            If (TradeOK And Not (thisBroker Is Nothing)) Then
              Dim ValidOrderTypes As String = thisBroker.ValidOrderTypes
              If (BrokerStrategy <> "" AndAlso (Not ValidOrderTypes Is Nothing) AndAlso Not ValidOrderTypes.Contains(BrokerStrategy)) Then
                TradeOK = False
                StatusText.Append("Line " & LineCounter.ToString & " : The BrokerStrategy " + BrokerStrategy + " is not allowed for this broker ( allowed : " + ValidOrderTypes + ")." & vbCrLf)
                StatusText.Append(" >> " & thisLine & vbCrLf)
              End If

            End If

            If (BuyOrSell.Length = 0) Then
              TradeOK = False

              StatusText.Append("Line " & LineCounter.ToString & " : Could not resolve Buy or Sell." & vbCrLf)
              StatusText.Append(" >> " & thisLine & vbCrLf)

            End If

            If (Quantity = 0.0#) Then
              TradeOK = False

              StatusText.Append("Line " & LineCounter.ToString & " : Quantity appears to be zero." & vbCrLf)
              StatusText.Append(" >> " & thisLine & vbCrLf)
            End If

            If (thisInstrument IsNot Nothing) AndAlso (SettlementDate <= Renaissance_BaseDate) Then
              If (Quantity < 0.0#) Then
                SettlementDate = RenaissanceUtilities.DatePeriodFunctions.AddPeriodToDate(DealingPeriod.Daily, TradeDate, thisInstrument.InstrumentDefaultSettlementDays_Sell)
              Else
                SettlementDate = RenaissanceUtilities.DatePeriodFunctions.AddPeriodToDate(DealingPeriod.Daily, TradeDate, thisInstrument.InstrumentDefaultSettlementDays_Buy)
              End If
            End If

            ' Trade already on form, or trade already exists.

            If (TradeOK) Then ' Add / Amend mytable.
              If (TradeReference.Length > 0) Then
                SelectedTransactions = myTable.Select("TransactionTicket='" & TradeReference & "'", "TransactionParentID, TransactionTicket")

                If (SelectedTransactions Is Nothing) OrElse (SelectedTransactions.Length = 0) Then
                  If (LookupTableRow(MainForm, RenaissanceStandardDatasets.tblTransaction, 0, "", "RN", "TransactionTicket='" & TradeReference & "'") IsNot Nothing) Then
                    ' Trade already exists
                    TradeOK = False

                    StatusText.Append("Line " & LineCounter.ToString & " : Trade already exists. TransactionTicket = `" & TradeReference & "`" & vbCrLf)
                    StatusText.Append(" >> " & thisLine & vbCrLf)

                  End If
                End If
              End If
            End If

            ' Add Transaction to table....

            If (TradeOK) Then ' Add / Amend mytable.

              If (SelectedTransactions IsNot Nothing) AndAlso (SelectedTransactions.Length > 0) Then
                ' Found in table

                thisSelectedTransaction = SelectedTransactions(0)

                If (thisSelectedTransaction.TransactionParentID > 0) Then
                  StatusText.Append("Line " & LineCounter.ToString & " : Trade has already been added. TransactionParentID = " & thisSelectedTransaction.TransactionParentID.ToString & vbCrLf)
                  StatusText.Append(" >> " & thisLine & vbCrLf)

                  Continue For
                End If
              Else
                ' Not in Table

                thisSelectedTransaction = myTable.NewtblSelectTransactionRow

              End If

              Quantity = Math.Abs(Quantity)

              thisSelectedTransaction.TransactionTicket = TradeReference
              thisSelectedTransaction.TransactionFund = FundID
              thisSelectedTransaction.TransactionSubFund = FundID
              thisSelectedTransaction.TransactionInstrument = thisInstrument.InstrumentID

              Select Case CType(thisInstrument.InstrumentType, InstrumentTypes)

                Case InstrumentTypes.Cash
                  If BuyOrSell.ToUpper = "SELL" Then
                    thisSelectedTransaction.TransactionType = TransactionTypes.SellFX
                  Else
                    thisSelectedTransaction.TransactionType = TransactionTypes.BuyFX
                  End If

                Case InstrumentTypes.Unit
                  If BuyOrSell.ToUpper = "SELL" Then
                    thisSelectedTransaction.TransactionType = TransactionTypes.Subscribe
                  Else
                    thisSelectedTransaction.TransactionType = TransactionTypes.Redeem
                  End If

                Case InstrumentTypes.Expense
                  If BuyOrSell.ToUpper = "SELL" Then
                    thisSelectedTransaction.TransactionType = TransactionTypes.Expense_Reduce
                  Else
                    thisSelectedTransaction.TransactionType = TransactionTypes.Expense_Increase
                  End If

                Case Else
                  If BuyOrSell.ToUpper = "SELL" Then
                    thisSelectedTransaction.TransactionType = TransactionTypes.Sell
                  Else
                    thisSelectedTransaction.TransactionType = TransactionTypes.Buy
                  End If

              End Select

              If (QuantityUnit = "AMOUNT") Then ' Cash value, i.e. Venice Value.
                thisSelectedTransaction.TransactionValueorAmount = "Value"
                thisSelectedTransaction.TransactionUnits = Quantity / IIf(Price = 0.0#, 1.0#, Price)
              Else ' Units, i.e. Venice Amount.
                thisSelectedTransaction.TransactionValueorAmount = "Amount"
                thisSelectedTransaction.TransactionUnits = Quantity
              End If

              thisSelectedTransaction.TransactionPrice = Price
              thisSelectedTransaction.TransactionCosts = Comission + Fees
              thisSelectedTransaction.TransactionCostIsPercent = 0
              thisSelectedTransaction.TransactionCostPercent = 0.0#

              If (thisSettlementCurrency IsNot Nothing) Then
                thisSelectedTransaction.TransactionSettlementCurrencyID = thisSettlementCurrency.CurrencyID
                thisSelectedTransaction.TransactionFXRate = FXRate
                thisSelectedTransaction.SettlementCurrencyCode = thisSettlementCurrency.CurrencyCode
              Else
                thisSelectedTransaction.TransactionSettlementCurrencyID = thisInstrument.InstrumentCurrencyID
                thisSelectedTransaction.TransactionFXRate = 1.0#
                thisSelectedTransaction.SettlementCurrencyCode = thisInstrument.InstrumentCurrency
              End If

              If (thisCounterparty Is Nothing) Then
                thisSelectedTransaction.TransactionCounterparty = RenaissanceGlobals.Counterparty.MARKET
                thisSelectedTransaction.CounterpartyName = "MARKET"
              Else
                thisSelectedTransaction.TransactionCounterparty = thisCounterparty.CounterpartyID
                thisSelectedTransaction.CounterpartyName = thisCounterparty.CounterpartyName
              End If

              If (thisBroker Is Nothing) Then
                thisSelectedTransaction.TransactionBroker = 0
              Else
                thisSelectedTransaction.TransactionBroker = thisBroker.BrokerID
                thisSelectedTransaction("BrokerName") = thisBroker.BrokerDescription
              End If

              thisSelectedTransaction.TransactionBrokerStrategy = BrokerStrategy

              thisSelectedTransaction.TransactionDecisionDate = Now.Date
              thisSelectedTransaction.TransactionValueDate = TradeDate
              thisSelectedTransaction.TransactionSettlementDate = SettlementDate
              thisSelectedTransaction.TransactionComment = ""
              thisSelectedTransaction.TransactionSpecificToFundUnitID = 0

              If (thisTradeStatus IsNot Nothing) Then
                thisSelectedTransaction.TransactionTradeStatusID = thisTradeStatus.TradeStatusID
                thisSelectedTransaction.TradeStatusName = thisTradeStatus.TradeStatusName
              Else
                thisSelectedTransaction.TransactionTradeStatusID = TradeStatus.ExecutionComplete
                thisSelectedTransaction.TradeStatusName = "ExecutionComplete"
              End If


              If (thisFund Is Nothing) Then
                thisSelectedTransaction.FundName = "Not Set"
              Else
                thisSelectedTransaction.FundName = thisFund.FundName
              End If

              thisSelectedTransaction.InstrumentDescription = thisInstrument.InstrumentDescription
              thisSelectedTransaction.InstrumentCurrency = thisInstrument.InstrumentCurrency
              thisSelectedTransaction.InstrumentContractSize = thisInstrument.InstrumentContractSize
              thisSelectedTransaction.InstrumentMultiplier = thisInstrument.InstrumentMultiplier
              thisSelectedTransaction.InstrumentISIN = thisInstrument.InstrumentISIN
              thisSelectedTransaction.TransactionTypeName = CType(thisSelectedTransaction.TransactionType, TransactionTypes).ToString

              thisSelectedTransaction.TransactionInvestment = ""
              thisSelectedTransaction.TransactionExecution = ""
              thisSelectedTransaction.TransactionDataEntry = ""

              If (thisSelectedTransaction.RowState And DataRowState.Detached) Then
                myTable.Rows.Add(thisSelectedTransaction)
              End If

            End If ' If (TradeOK) 

          Else

            StatusText.Append("Line " & LineCounter.ToString & " : Too few fields (" & thisLineArray.Count.ToString & ")" & vbCrLf)
            If (thisLine.Length > 0) Then
              StatusText.Append(" >> " & thisLine & vbCrLf)
            End If

          End If ' If (thisLineArray.Count >= 15) Then

          If TradeOK = False Then
            nbrTradeKO = nbrTradeKO + 1
          End If

        Else
          StatusText.Append("Line " & LineCounter.ToString & " : no separator character " + CStr(SeparatorCharacter) + "." & vbCrLf)
          StatusText.Append(" >> " & thisLine & vbCrLf)
        End If


      Next LineCounter

      StatusText.Append("<End of File>" & vbCrLf)
      Dim recap As String = " Total of " & CStr(nbrTradeRead) & " line(s) read.  Failed: " & CStr(nbrTradeKO) & " , Succesfull: " & CStr(nbrTradeRead - nbrTradeKO) & vbCrLf
      recap = recap & "--------------------------------------------------------------------------" & vbCrLf & vbCrLf
      StatusText.Insert(0, recap)
      If (nbrTradeKO > 0) Then
        showMsgTab = True
      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error in ParseExcelTransactionExport()", ex.Message, ex.StackTrace, True)
      StatusText.Append("Error : " & ex.Message & vbCrLf)
      showMsgTab = True
    Finally
      TextBox_Status.Text = StatusText.ToString
    End Try

    If (showMsgTab) Then
      TabControl_Import.SelectedTab = Me.TabPage_Status
    End If

  End Sub


  Private Sub ParseBP2SShortFile(ByVal CSVLines() As String)
    ' *******************************************************************************
    '
    ' *******************************************************************************
    Const _DateFormat_short As String = "yyyyMMdd"
    'Const _TimeFormat As String = "HHmmss"
    'Const _DateFormat_long As String = "yyyyMMddHHmmss"

    Dim _CultureInfo As Globalization.CultureInfo = New Globalization.CultureInfo("")
    Dim StatusText As New System.Text.StringBuilder

    Try
      Dim LineCounter As Integer
      Dim ColumnCounter As Integer
      Dim FieldValue As String
      Dim thisLine As String
      Dim thisLineArray() As String

      Dim thisFund As DSFund.tblFundRow
      Dim thisInstrument As DSInstrument.tblInstrumentRow
      Dim thisSettlementCurrency As DSCurrency.tblCurrencyRow
      Dim thisCounterparty As DSCounterparty.tblCounterpartyRow
      Dim thisBroker As DSBroker.tblBrokerRow

      Dim SelectedTransactions() As DSSelectTransaction.tblSelectTransactionRow
      Dim thisSelectedTransaction As DSSelectTransaction.tblSelectTransactionRow

      Dim PortfolioCode As String
      Dim TradeReference As String
      Dim TradeDate As Date
      Dim SettlementDate As Date
      Dim BuyOrSell As String
      Dim ISIN As String
      Dim Country As String
      Dim Quantity As Double
      Dim QuantityUnit As String
      Dim Price As Double
      Dim Currency As String
      Dim Comission As Double
      Dim SettlementCurrency As String
      Dim BrokerCode As String
      Dim BrokerStrategy As String
      Dim FXRate As Double
      Dim TradeOK As Boolean

      For LineCounter = 0 To (CSVLines.Length - 1)
        thisLine = CSVLines(LineCounter)

        If (thisLine.Length > 0) AndAlso (thisLine.Contains(";")) Then
          thisLineArray = thisLine.Split(New Char() {";"})

          If (thisLineArray.Count >= 15) Then

            PortfolioCode = ""
            TradeReference = "'"
            TradeDate = Renaissance_BaseDate
            SettlementDate = Renaissance_BaseDate
            BuyOrSell = ""
            ISIN = ""
            Country = ""
            Quantity = 0.0#
            QuantityUnit = ""
            Price = 0.0#
            Currency = ""
            Comission = 0.0#
            SettlementCurrency = ""
            BrokerCode = ""
            BrokerStrategy = ""
            FXRate = 1.0#
            thisSettlementCurrency = Nothing
            thisCounterparty = Nothing
            thisBroker = Nothing
            TradeOK = True

            For ColumnCounter = 0 To 14

              FieldValue = thisLineArray(ColumnCounter)

              If (FieldValue.Length > 0) Then

                Select Case ColumnCounter

                  Case 0

                    PortfolioCode = FieldValue

                  Case 1

                    TradeReference = FieldValue

                  Case 2

                    If (FieldValue.Length >= 8) Then
                      Try
                        TradeDate = Date.ParseExact(FieldValue, _DateFormat_short, Nothing)
                      Catch ex As Exception
                      End Try
                    End If

                  Case 3

                    If (FieldValue.Length >= 8) Then
                      Try
                        SettlementDate = Date.ParseExact(FieldValue, _DateFormat_short, Nothing)
                      Catch ex As Exception
                      End Try
                    End If

                  Case 4

                    Select Case (FieldValue.Substring(0, 1).ToUpper)

                      Case "A", "B" ' Achat, Buy
                        BuyOrSell = "BUY"

                      Case "V", "S" ' Vendre, Sell
                        BuyOrSell = "SELL"

                    End Select

                  Case 5

                    ISIN = FieldValue

                  Case 6

                    Country = FieldValue

                  Case 7

                    Quantity = Double.Parse(FieldValue, _CultureInfo)

                  Case 8

                    Select Case (FieldValue.Substring(0, 1).ToUpper)

                      Case "F", "A" ' FAMT, AMT
                        QuantityUnit = "AMOUNT"

                      Case Else

                        QuantityUnit = "UNIT"

                    End Select

                  Case 9

                    Price = Double.Parse(FieldValue, _CultureInfo)

                  Case 10

                    Currency = FieldValue

                  Case 11

                    Comission = Double.Parse(FieldValue, _CultureInfo)

                  Case 12

                    SettlementCurrency = FieldValue

                  Case 13

                    BrokerCode = FieldValue

                  Case 14

                    FXRate = Double.Parse(FieldValue, _CultureInfo)

                  Case 15

                    BrokerStrategy = FieldValue

                End Select

              End If

            Next ColumnCounter

            ' Validate 

            thisFund = Nothing
            If (PortfolioCode.Length > 0) Then
              thisFund = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, 0, "", "RN", "FundAdministratorCode='" & PortfolioCode & "'")
            Else
              ' Use Override ?
            End If
            If (thisFund Is Nothing) Then
              TradeOK = False

              StatusText.Append("Line " & LineCounter.ToString & " : Could not resolve Fund from Administrator code (" & PortfolioCode & ")" & vbCrLf)

            End If

            thisInstrument = Nothing
            If (TradeOK) Then
              If (ISIN.Length > 0) Then
                thisInstrument = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, 0, "", "RN", "InstrumentISIN='" & ISIN & "' AND InstrumentCurrency='" & Currency & "'")
              End If
              If (thisInstrument Is Nothing) Then
                TradeOK = False

                StatusText.Append("Line " & LineCounter.ToString & " : Could not resolve Instrument from ISIN and Currency (" & ISIN & ", " & Currency & ")" & vbCrLf)

              End If
            End If

            If (TradeOK) Then
              If (SettlementCurrency.Length > 0) AndAlso (SettlementCurrency <> Currency) Then
                thisSettlementCurrency = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblCurrency, 0, "", "RN", "CurrencyCode='" & SettlementCurrency & "'")
              End If
            End If

            If (BuyOrSell.Length = 0) Then
              TradeOK = False

              StatusText.Append("Line " & LineCounter.ToString & " : Could not resolve Buy or Sell." & vbCrLf)
              StatusText.Append(" >> " & thisLine & vbCrLf)

            End If

            If (Quantity = 0.0#) Then
              TradeOK = False

              StatusText.Append("Line " & LineCounter.ToString & " : Quantity appears to be zero." & vbCrLf)
              StatusText.Append(" >> " & thisLine & vbCrLf)
            End If

            If (TradeDate <= Renaissance_BaseDate) Then
              TradeOK = False

              StatusText.Append("Line " & LineCounter.ToString & " : Trade Date appears invalid." & vbCrLf)
              StatusText.Append(" >> " & thisLine & vbCrLf)

            ElseIf (SettlementDate <= Renaissance_BaseDate) Then
              SettlementDate = TradeDate
            End If


            ' Add Transaction to table....

            If (TradeOK) Then

              SelectedTransactions = myTable.Select("TransactionTicket='" & TradeReference & "'", "TransactionParentID, TransactionTicket")

              If (SelectedTransactions Is Nothing) OrElse (SelectedTransactions.Length = 0) Then
                If (LookupTableRow(MainForm, RenaissanceStandardDatasets.tblTransaction, 0, "", "RN", "TransactionTicket='" & TradeReference & "'") IsNot Nothing) Then
                  ' Trade already exists
                  TradeOK = False

                  StatusText.Append("Line " & LineCounter.ToString & " : Trade already exists. TransactionTicket = `" & TradeReference & "`" & vbCrLf)
                  StatusText.Append(" >> " & thisLine & vbCrLf)

                End If
              End If

              If (TradeOK) Then ' Add / Amend mytable.

                If (SelectedTransactions IsNot Nothing) AndAlso (SelectedTransactions.Length > 0) Then
                  ' Found in table

                  thisSelectedTransaction = SelectedTransactions(0)

                  If (thisSelectedTransaction.TransactionParentID > 0) Then
                    StatusText.Append("Line " & LineCounter.ToString & " : Trade has already been added. TransactionParentID = " & thisSelectedTransaction.TransactionParentID.ToString & vbCrLf)
                    StatusText.Append(" >> " & thisLine & vbCrLf)

                    Continue For
                  End If
                Else
                  ' Not in Table

                  thisSelectedTransaction = myTable.NewtblSelectTransactionRow

                End If


                thisSelectedTransaction.TransactionTicket = TradeReference
                thisSelectedTransaction.TransactionFund = thisFund.FundID
                thisSelectedTransaction.TransactionSubFund = thisFund.FundID
                thisSelectedTransaction.TransactionInstrument = thisInstrument.InstrumentID

                Select Case CType(thisInstrument.InstrumentType, InstrumentTypes)

                  Case InstrumentTypes.Cash
                    If BuyOrSell.ToUpper = "SELL" Then
                      thisSelectedTransaction.TransactionType = TransactionTypes.SellFX
                    Else
                      thisSelectedTransaction.TransactionType = TransactionTypes.BuyFX
                    End If

                  Case InstrumentTypes.Unit
                    If BuyOrSell.ToUpper = "SELL" Then
                      thisSelectedTransaction.TransactionType = TransactionTypes.Subscribe
                    Else
                      thisSelectedTransaction.TransactionType = TransactionTypes.Redeem
                    End If

                  Case InstrumentTypes.Expense
                    If BuyOrSell.ToUpper = "SELL" Then
                      thisSelectedTransaction.TransactionType = TransactionTypes.Expense_Reduce
                    Else
                      thisSelectedTransaction.TransactionType = TransactionTypes.Expense_Increase
                    End If

                  Case Else
                    If BuyOrSell.ToUpper = "SELL" Then
                      thisSelectedTransaction.TransactionType = TransactionTypes.Sell
                    Else
                      thisSelectedTransaction.TransactionType = TransactionTypes.Buy
                    End If

                End Select

                If (QuantityUnit = "AMOUNT") Then ' Cash value, i.e. Venice Value.
                  thisSelectedTransaction.TransactionValueorAmount = "Value"
                  thisSelectedTransaction.TransactionUnits = Quantity / IIf(Price = 0.0#, 1.0#, Price)
                Else ' Units, i.e. Venice Amount.
                  thisSelectedTransaction.TransactionValueorAmount = "Amount"
                  thisSelectedTransaction.TransactionUnits = Quantity
                End If

                thisSelectedTransaction.TransactionPrice = Price
                thisSelectedTransaction.TransactionCosts = Comission
                thisSelectedTransaction.TransactionCostIsPercent = 0
                thisSelectedTransaction.TransactionCostPercent = 0.0#

                If (thisSettlementCurrency IsNot Nothing) Then
                  thisSelectedTransaction.TransactionSettlementCurrencyID = thisSettlementCurrency.CurrencyID
                  thisSelectedTransaction.TransactionFXRate = FXRate
                  thisSelectedTransaction.SettlementCurrencyCode = thisSettlementCurrency.CurrencyCode
                Else
                  thisSelectedTransaction.TransactionSettlementCurrencyID = thisInstrument.InstrumentCurrencyID
                  thisSelectedTransaction.TransactionFXRate = 1.0#
                  thisSelectedTransaction.SettlementCurrencyCode = thisInstrument.InstrumentCurrency
                End If

                If (thisCounterparty Is Nothing) Then
                  thisSelectedTransaction.TransactionCounterparty = RenaissanceGlobals.Counterparty.MARKET
                  thisSelectedTransaction.CounterpartyName = "MARKET"
                Else
                  thisSelectedTransaction.TransactionCounterparty = thisCounterparty.CounterpartyID
                  thisSelectedTransaction.CounterpartyName = thisCounterparty.CounterpartyName
                End If

                If (thisBroker Is Nothing) Then
                  thisSelectedTransaction.TransactionBroker = 0
                Else
                  thisSelectedTransaction.TransactionBroker = thisBroker.BrokerID
                End If

                thisSelectedTransaction.TransactionDecisionDate = Now.Date
                thisSelectedTransaction.TransactionValueDate = TradeDate
                thisSelectedTransaction.TransactionSettlementDate = SettlementDate
                thisSelectedTransaction.TransactionComment = ""
                thisSelectedTransaction.TransactionSpecificToFundUnitID = 0

                thisSelectedTransaction.TransactionTradeStatusID = TradeStatus.ExecutionComplete
                thisSelectedTransaction.TradeStatusName = "ExecutionComplete"

                thisSelectedTransaction.FundName = thisFund.FundName
                thisSelectedTransaction.InstrumentDescription = thisInstrument.InstrumentDescription
                thisSelectedTransaction.InstrumentCurrency = thisInstrument.InstrumentCurrency
                thisSelectedTransaction.InstrumentContractSize = thisInstrument.InstrumentContractSize
                thisSelectedTransaction.InstrumentMultiplier = thisInstrument.InstrumentMultiplier
                thisSelectedTransaction.InstrumentISIN = thisInstrument.InstrumentISIN
                thisSelectedTransaction.TransactionTypeName = CType(thisSelectedTransaction.TransactionType, TransactionTypes).ToString

                thisSelectedTransaction.TransactionInvestment = ""
                thisSelectedTransaction.TransactionExecution = ""
                thisSelectedTransaction.TransactionDataEntry = ""

                If (thisSelectedTransaction.RowState And DataRowState.Detached) Then
                  myTable.Rows.Add(thisSelectedTransaction)
                End If

              End If ' If (TradeOK) again.

            End If ' If (TradeOK) 

          Else

            StatusText.Append("Line " & LineCounter.ToString & " : Too few fields (" & thisLineArray.Count.ToString & ")" & vbCrLf)
            If (thisLine.Length > 0) Then
              StatusText.Append(" >> " & thisLine & vbCrLf)
            End If

          End If ' If (thisLineArray.Count >= 15) Then

        Else

          StatusText.Append("Line " & LineCounter.ToString & " : Blank, or no `;`." & vbCrLf)
          If (thisLine.Length > 0) Then
            StatusText.Append(" >> " & thisLine & vbCrLf)
          End If
        End If

      Next LineCounter

      StatusText.Append("<End of File>" & vbCrLf)

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error in ParseBP2SShortFile()", ex.Message, ex.StackTrace, True)
      StatusText.Append("Error : " & ex.Message & vbCrLf)
    Finally
      TextBox_Status.Text = StatusText.ToString
    End Try
  End Sub

  Private Sub Grid_Transactions_SelChange(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid_Transactions.SelChange
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Try
      Dim ThisRow As C1.Win.C1FlexGrid.Row
      Dim thisRowView As DataRowView
      Dim thisTransaction As DSSelectTransaction.tblSelectTransactionRow = Nothing
      Dim lastTransaction As DSSelectTransaction.tblSelectTransactionRow = Nothing


      If (Combo_TransactionFund.Items.Count > 0) Then Combo_TransactionFund.SelectedIndex = 0
      If (Combo_SubFund.Items.Count > 0) Then Combo_SubFund.SelectedIndex = 0
      If (Combo_SpecificShareClass.Items.Count > 0) Then Combo_SpecificShareClass.SelectedIndex = 0
      If (Combo_TransactionCounterparty.Items.Count > 0) Then Combo_TransactionCounterparty.SelectedIndex = 0
      If (Combo_Broker.Items.Count > 0) Then Combo_Broker.SelectedIndex = 0
      If (Combo_TradeStatus.Items.Count > 0) Then Combo_TradeStatus.SelectedIndex = 0

      If (Grid_Transactions.RowSel <= 0) OrElse (Grid_Transactions.Rows.Selected.Count <= 0) Then
        Combo_TransactionFund.Enabled = False
        Combo_SubFund.Enabled = False
        Combo_SpecificShareClass.Enabled = False
        Combo_TransactionCounterparty.Enabled = False
        Combo_Broker.Enabled = False
        Combo_TradeStatus.Enabled = False
        Button_UpdatePendingTransactions.Visible = False
      Else
        Combo_TransactionFund.Enabled = True
        Combo_SubFund.Enabled = True
        Combo_SpecificShareClass.Enabled = True
        Combo_TransactionCounterparty.Enabled = True
        Combo_Broker.Enabled = True
        Combo_TradeStatus.Enabled = True
        Button_UpdatePendingTransactions.Visible = True

        If (Grid_Transactions.Rows.Selected.Count = 1) Then
          thisRowView = Grid_Transactions.Rows.Selected(0).DataSource
          Try
            thisTransaction = CType(thisRowView.Row, DSSelectTransaction.tblSelectTransactionRow)

            Combo_TransactionFund.SelectedValue = thisTransaction.TransactionFund

            SetSubFundCombo()
            SetSpecificShareClassCombo()

            If (thisTransaction.TransactionSubFund > 0) AndAlso (thisTransaction.TransactionSubFund <> thisTransaction.TransactionFund) Then
              Combo_SubFund.SelectedValue = thisTransaction.TransactionSubFund
            End If

            If (thisTransaction.TransactionSpecificToFundUnitID > 0) Then
              Combo_SpecificShareClass.SelectedValue = thisTransaction.TransactionSpecificToFundUnitID
            End If

            Combo_TransactionCounterparty.SelectedValue = thisTransaction.TransactionCounterparty
            Combo_Broker.SelectedValue = thisTransaction.TransactionBroker
            Combo_TradeStatus.SelectedValue = thisTransaction.TransactionTradeStatusID
          Catch ex As Exception
          End Try
        Else
          Dim SingleFund As Boolean = True
          Dim SingleSubfund As Boolean = True
          Dim SingleSpecificShareClass As Boolean = True
          Dim SingleCounterparty As Boolean = True
          Dim SingleBroker As Boolean = True
          Dim SingleTradeStatus As Boolean = True

          lastTransaction = Nothing

          For Each ThisRow In Grid_Transactions.Rows.Selected
            thisRowView = ThisRow.DataSource

            Try
              thisTransaction = CType(thisRowView.Row, DSSelectTransaction.tblSelectTransactionRow)

              If (lastTransaction IsNot Nothing) Then
                If (thisTransaction.TransactionFund <> lastTransaction.TransactionFund) Then SingleFund = False
                If (thisTransaction.TransactionSubFund <> lastTransaction.TransactionSubFund) Then SingleSubfund = False
                If (thisTransaction.TransactionSpecificToFundUnitID <> lastTransaction.TransactionSpecificToFundUnitID) Then SingleSpecificShareClass = False
                If (thisTransaction.TransactionCounterparty <> lastTransaction.TransactionCounterparty) Then SingleCounterparty = False
                If (thisTransaction.TransactionBroker <> lastTransaction.TransactionBroker) Then SingleBroker = False
                If (thisTransaction.TransactionTradeStatusID <> lastTransaction.TransactionTradeStatusID) Then SingleTradeStatus = False
              End If

            Catch ex As Exception
            End Try

            lastTransaction = thisTransaction
          Next

          If (SingleFund) Then
            Combo_TransactionFund.SelectedValue = thisTransaction.TransactionFund

            SetSubFundCombo()
            SetSpecificShareClassCombo()
          Else
            Combo_SubFund.Enabled = False
            Combo_SpecificShareClass.Enabled = False
          End If

          If (SingleSubfund) Then
            If (thisTransaction.TransactionSubFund > 0) AndAlso (thisTransaction.TransactionSubFund <> thisTransaction.TransactionFund) Then
              Combo_SubFund.SelectedValue = thisTransaction.TransactionSubFund
            End If
          End If

          If (SingleSpecificShareClass) Then
            If (thisTransaction.TransactionSpecificToFundUnitID > 0) Then
              Combo_SpecificShareClass.SelectedValue = thisTransaction.TransactionSpecificToFundUnitID
            End If
          End If

          If (SingleCounterparty) Then
            Combo_TransactionCounterparty.SelectedValue = thisTransaction.TransactionCounterparty
          End If

          If (SingleBroker) Then
            Combo_Broker.SelectedValue = thisTransaction.TransactionBroker
          End If

          If (SingleTradeStatus) Then
            Combo_TradeStatus.SelectedValue = thisTransaction.TransactionTradeStatusID
          End If

        End If

      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error in Grid_Transactions_SelChange()", ex.Message, ex.StackTrace, True)
    End Try
  End Sub


  Private Sub Combo_TransactionFund_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Combo_TransactionFund.SelectedValueChanged
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Try
      If CType(sender, Control).Focused Then
        SetSubFundCombo()
        SetSpecificShareClassCombo()

        If (CInt(Nz(Combo_TransactionFund.SelectedValue, 0))) > 0 Then
          Combo_SubFund.Enabled = True
          Combo_SpecificShareClass.Enabled = True
        Else
          Combo_SubFund.Enabled = False
          Combo_SpecificShareClass.Enabled = False
        End If
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error in Combo_TransactionFund_SelectedValueChanged()", ex.Message, ex.StackTrace, True)
    End Try
  End Sub

  Private Sub Button_UpdatePendingTransactions_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_UpdatePendingTransactions.Click
    ' *******************************************************************************
    '
    ' *******************************************************************************
    Dim ThisRow As C1.Win.C1FlexGrid.Row
    Dim thisRowView As DataRowView
    Dim thisTransaction As DSSelectTransaction.tblSelectTransactionRow = Nothing
    Dim ThisFundRow As DSFund.tblFundRow = Nothing
    Dim ThisSubFundRow As DSSubFund.tblSubFundRow = Nothing
    Dim ThisInstrumentRow As DSInstrument.tblInstrumentRow = Nothing
    Dim ThisBrokerRow As DSBroker.tblBrokerRow = Nothing
    Dim ThisTradeStatusRow As DSTradeStatus.tblTradeStatusRow = Nothing
    Dim ThisCounterpartyRow As DSCounterparty.tblCounterpartyRow = Nothing

    Dim NewFundID As Integer = CInt(Nz(Combo_TransactionFund.SelectedValue, 0))
    Dim NewSubFundID As Integer = CInt(Nz(Combo_SubFund.SelectedValue, 0))
    Dim NewSpecificShareClass As Integer = CInt(Nz(Combo_SpecificShareClass.SelectedValue, 0))
    Dim NewBrokerID As Integer = CInt(Nz(Combo_Broker.SelectedValue, 0))
    Dim NewTradeStatus As Integer = CInt(Nz(Combo_TradeStatus.SelectedValue, 0))
    Dim NewCounterparty As Integer = CInt(Nz(Combo_TransactionCounterparty.SelectedValue, 0))

    Try

      If (Grid_Transactions.RowSel > 0) AndAlso (Grid_Transactions.Rows.Selected.Count > 0) Then

        If (NewFundID > 0) Then
          ThisFundRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, NewFundID)
        End If
        If (NewSubFundID > 0) Then
          ThisSubFundRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblSubFund, NewSubFundID)
        End If
        If (NewTradeStatus > 0) Then
          ThisTradeStatusRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblTradeStatus, NewTradeStatus)
        End If
        If (NewCounterparty > 0) Then
          ThisCounterpartyRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblCounterparty, NewCounterparty)
        End If
        If (NewSpecificShareClass > 0) Then
          ThisInstrumentRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, NewSpecificShareClass)
        End If

        For Each ThisRow In Grid_Transactions.Rows.Selected

          Try

            thisRowView = ThisRow.DataSource
            thisTransaction = CType(thisRowView.Row, DSSelectTransaction.tblSelectTransactionRow)

            If (thisTransaction.TransactionParentID > 0) Then
              Continue For
            End If

            If (NewFundID > 0) Then
              thisTransaction.TransactionFund = NewFundID

              If (ThisFundRow IsNot Nothing) Then
                thisTransaction.FundName = ThisFundRow.FundName
                thisTransaction.FundLegalEntity = ThisFundRow.FundLegalEntity
              End If
            End If

            If (NewSubFundID > 0) Then
              thisTransaction.TransactionSubFund = NewSubFundID

              If (ThisSubFundRow IsNot Nothing) Then
                thisTransaction("SubFundName") = ThisSubFundRow.SubFundName
              End If
            End If

            If (NewSpecificShareClass > 0) Then
              thisTransaction.TransactionSpecificToFundUnitID = NewSpecificShareClass

              If (ThisInstrumentRow IsNot Nothing) Then
                thisTransaction("SpecificUnitName") = ThisInstrumentRow.InstrumentDescription
              End If
            End If

            If (NewBrokerID > 0) Then
              thisTransaction.TransactionBroker = NewBrokerID
            End If

            If (NewTradeStatus > 0) Then
              thisTransaction.TransactionTradeStatusID = NewTradeStatus

              If (ThisTradeStatusRow IsNot Nothing) Then
                thisTransaction.TradeStatusName = ThisTradeStatusRow.TradeStatusName
              End If
            End If

            If (NewCounterparty > 0) Then
              thisTransaction.TransactionCounterparty = NewCounterparty

              If (ThisCounterpartyRow IsNot Nothing) Then
                thisTransaction.CounterpartyName = ThisCounterpartyRow.CounterpartyName
              End If
            End If

          Catch ex As Exception
          End Try

        Next
      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error in Button_UpdatePendingTransactions_Click()", ex.Message, ex.StackTrace, True)
    End Try
  End Sub

  Private Sub Button_Clear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Clear.Click
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Try

      myTable.Clear()

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error in Button_Clear_Click()", ex.Message, ex.StackTrace, True)
    End Try
  End Sub

  Private Sub Grid_Transactions_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Grid_Transactions.KeyUp
    ' ***********************************************************************************
    '
    '
    ' ***********************************************************************************
    Dim CSVLines() As String = Nothing

    Try
     If e.Control And (e.KeyCode = Keys.V) Then
        Dim ClipboardData As String

        ClipboardData = Clipboard.GetData(System.Windows.Forms.DataFormats.Text)

        CSVLines = ClipboardData.Split(vbCrLf.ToCharArray, StringSplitOptions.RemoveEmptyEntries)

        ParseFileContents(CSVLines, "Text")

        e.Handled = True
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error processing Paste operation.", ex.Message, ex.StackTrace, True)
    End Try
  End Sub

  Private Sub frmImportTransactions_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Grid_Transactions_KeyUp(sender, e)

  End Sub

  Private Sub TabControl_Import_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TabControl_Import.KeyUp
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Grid_Transactions_KeyUp(sender, e)

  End Sub

  Private Sub TextBox_Status_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox_Status.KeyUp
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Grid_Transactions_KeyUp(sender, e)

  End Sub

  Private Sub Button_SaveSelectedTransactions_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_SaveSelectedTransactions.Click
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Try

      SaveTransactions(Grid_Transactions.Rows.Selected)

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error in Button_SaveSelectedTransactions_Click()", ex.Message, ex.StackTrace, True)
    End Try
  End Sub

  Private Sub Button_SaveAllTransactions_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_SaveAllTransactions.Click
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Try

      SaveTransactions(Grid_Transactions.Rows)

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error in Button_SaveAllTransactions_Click()", ex.Message, ex.StackTrace, True)
    End Try
  End Sub

  Private Sub SaveTransactions(ByVal TransactionsCollection As C1.Win.C1FlexGrid.RowColCollection)
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Dim ThisRow As C1.Win.C1FlexGrid.Row
    Dim thisRowView As DataRowView
    Dim thisSelectTransaction As DSSelectTransaction.tblSelectTransactionRow = Nothing
    Dim TransactionDS As DSTransaction
    Dim newTransactionRow As DSTransaction.tblTransactionRow
    Dim StatusText As New System.Text.StringBuilder
    Dim UpdateString As String = ""
    Dim TransactionsUpdated As Boolean = False
    Dim Counter As Integer

    Try

      If (TransactionsCollection Is Nothing) OrElse (TransactionsCollection.Count <= 0) Then
        StatusText.Append("No Transactions selected.")
        Exit Sub
      End If

      Dim TransactionsRows(TransactionsCollection.Count - 1) As C1.Win.C1FlexGrid.Row

      Counter = 0
      For Each ThisRow In TransactionsCollection
        If (TransactionsRows.Length > Counter) Then
          TransactionsRows(Counter) = ThisRow
        End If

        Counter += 1
      Next

      '

      TransactionDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblTransaction)

      For Each ThisRow In TransactionsRows

        Try
          thisRowView = ThisRow.DataSource

          If (thisRowView Is Nothing) Then ' Probably the Heading row.
            Continue For
          End If
          thisSelectTransaction = CType(thisRowView.Row, DSSelectTransaction.tblSelectTransactionRow)

          ' Check it does not already exist

          If (thisSelectTransaction.TransactionParentID > 0) Then
            StatusText.Append("Trade already added. TransactionParentID = `" & thisSelectTransaction.TransactionParentID.ToString & "`" & vbCrLf)

            Continue For
          End If

          If (CStr(thisSelectTransaction.TransactionTicket).Length > 0) AndAlso (LookupTableRow(MainForm, RenaissanceStandardDatasets.tblTransaction, 0, "", "RN", "TransactionTicket='" & thisSelectTransaction.TransactionTicket & "'") IsNot Nothing) Then
            ' Trade already exists

            StatusText.Append("Trade already exists. TransactionTicket = `" & thisSelectTransaction.TransactionTicket & "`" & vbCrLf)

            Continue For

          End If

          If (thisSelectTransaction.TransactionFund <= 0) Then
            ' Trade already exists

            StatusText.Append("Fund not set." & vbCrLf)

            Continue For

          End If

          newTransactionRow = TransactionDS.tblTransaction.NewtblTransactionRow

          newTransactionRow.TransactionTicket = thisSelectTransaction.TransactionTicket
          newTransactionRow.TransactionFund = thisSelectTransaction.TransactionFund
          newTransactionRow.TransactionSubFund = IIf(Nz(thisSelectTransaction.TransactionSubFund, 0) = 0, thisSelectTransaction.TransactionFund, thisSelectTransaction.TransactionSubFund)
          newTransactionRow.TransactionInstrument = thisSelectTransaction.TransactionInstrument
          newTransactionRow.TransactionType = thisSelectTransaction.TransactionType
          newTransactionRow.TransactionValueorAmount = thisSelectTransaction.TransactionValueorAmount
          newTransactionRow.TransactionUnits = thisSelectTransaction.TransactionUnits
          newTransactionRow.TransactionPrice = thisSelectTransaction.TransactionPrice
          newTransactionRow.TransactionCosts = thisSelectTransaction.TransactionCosts
          newTransactionRow.TransactionCostIsPercent = thisSelectTransaction.TransactionCostIsPercent
          newTransactionRow.TransactionCostPercent = thisSelectTransaction.TransactionCostPercent
          newTransactionRow.TransactionSettlementCurrencyID = thisSelectTransaction.TransactionSettlementCurrencyID
          newTransactionRow.TransactionFXRate = thisSelectTransaction.TransactionFXRate
          newTransactionRow.TransactionCounterparty = thisSelectTransaction.TransactionCounterparty
          newTransactionRow.TransactionBroker = thisSelectTransaction.TransactionBroker
          newTransactionRow.TransactionInvestment = ""
          newTransactionRow.TransactionExecution = ""
          newTransactionRow.TransactionDataEntry = ""
          newTransactionRow.TransactionEffectiveValueDate = thisSelectTransaction.TransactionValueDate
          newTransactionRow.TransactionDecisionDate = thisSelectTransaction.TransactionDecisionDate
          newTransactionRow.TransactionValueDate = thisSelectTransaction.TransactionValueDate
          newTransactionRow.TransactionFIFOValueDate = thisSelectTransaction.TransactionValueDate
          newTransactionRow.TransactionSettlementDate = thisSelectTransaction.TransactionSettlementDate
          newTransactionRow.TransactionConfirmationDate = thisSelectTransaction.TransactionValueDate
          newTransactionRow.TransactionExemptFromUpdate = False
          newTransactionRow.TransactionComment = thisSelectTransaction.TransactionComment
          newTransactionRow.TransactionSpecificToFundUnitID = thisSelectTransaction.TransactionSpecificToFundUnitID

          ' Lotfi : add statusId
          If thisSelectTransaction.TransactionTradeStatusID > 0 Then
            newTransactionRow.TransactionTradeStatusID = thisSelectTransaction.TransactionTradeStatusID
          End If
          newTransactionRow.TransactionBrokerStrategy = thisSelectTransaction.TransactionBrokerStrategy

          TransactionDS.tblTransaction.Rows.Add(newTransactionRow)

          TransactionsUpdated = True
          MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblTransaction, New DataRow() {newTransactionRow})

          If (UpdateString.Length = 0) Then
            UpdateString = newTransactionRow.TransactionParentID.ToString
          Else
            UpdateString = UpdateString & "," & newTransactionRow.TransactionParentID.ToString
          End If

          thisSelectTransaction.TransactionParentID = newTransactionRow.TransactionParentID
          'ThisRow.Style = XX

        Catch ex As Exception
          StatusText.Append("Error : " & ex.Message & vbCrLf)
        End Try
      Next

      ' Propogate changes

      If (TransactionsUpdated) Then
        Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblTransaction, UpdateString), True)
      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error in SaveTransactions()", ex.Message, ex.StackTrace, True)
      StatusText.Append("Error : " & ex.Message & vbCrLf)
    Finally
      TextBox_Status.Text = StatusText.ToString
    End Try
  End Sub

End Class
