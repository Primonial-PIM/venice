' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 11-06-2012
' ***********************************************************************
' <copyright file="frmAbout.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals

''' <summary>
''' Class frmAbout
''' </summary>
Public Class frmAbout

	Inherits System.Windows.Forms.Form
	Implements StandardVeniceForm


#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmAbout"/> class from being created.
    ''' </summary>
	Private Sub New()
		MyBase.New()

		'This call is required by the Windows Form Designer.
		InitializeComponent()

		'Add any initialization after the InitializeComponent() call

	End Sub

	'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
    ''' <summary>
    ''' The status1
    ''' </summary>
	Friend WithEvents Status1 As System.Windows.Forms.StatusStrip
    ''' <summary>
    ''' The status label1
    ''' </summary>
	Friend WithEvents StatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    ''' <summary>
    ''' The label_ user name
    ''' </summary>
	Friend WithEvents Label_UserName As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ app name
    ''' </summary>
	Friend WithEvents Label_AppName As System.Windows.Forms.Label
    ''' <summary>
    ''' The list box_ permissions
    ''' </summary>
	Friend WithEvents ListBox_Permissions As System.Windows.Forms.ListBox
    ''' <summary>
    ''' The label_ app
    ''' </summary>
	Friend WithEvents Label_App As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ connection
    ''' </summary>
	Friend WithEvents Label_Connection As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ server
    ''' </summary>
	Friend WithEvents Label_Server As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ main connect string
    ''' </summary>
	Friend WithEvents Label_MainConnectString As System.Windows.Forms.Label
    ''' <summary>
    ''' The tool strip progress bar1
    ''' </summary>
	Friend WithEvents ToolStripProgressBar1 As System.Windows.Forms.ToolStripProgressBar
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAbout))
		Me.Status1 = New System.Windows.Forms.StatusStrip
		Me.ToolStripProgressBar1 = New System.Windows.Forms.ToolStripProgressBar
		Me.StatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel
		Me.Label_UserName = New System.Windows.Forms.Label
		Me.Label_AppName = New System.Windows.Forms.Label
		Me.ListBox_Permissions = New System.Windows.Forms.ListBox
		Me.Label_App = New System.Windows.Forms.Label
		Me.Label_Connection = New System.Windows.Forms.Label
		Me.Label_Server = New System.Windows.Forms.Label
		Me.Label_MainConnectString = New System.Windows.Forms.Label
		Me.Status1.SuspendLayout()
		Me.SuspendLayout()
		'
		'Status1
		'
		Me.Status1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripProgressBar1, Me.StatusLabel1})
		Me.Status1.Location = New System.Drawing.Point(0, 369)
		Me.Status1.Name = "Status1"
		Me.Status1.Size = New System.Drawing.Size(482, 22)
		Me.Status1.TabIndex = 69
		Me.Status1.Text = "StatusStrip1"
		'
		'ToolStripProgressBar1
		'
		Me.ToolStripProgressBar1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
		Me.ToolStripProgressBar1.Maximum = 20
		Me.ToolStripProgressBar1.Name = "ToolStripProgressBar1"
		Me.ToolStripProgressBar1.Size = New System.Drawing.Size(150, 16)
		Me.ToolStripProgressBar1.Step = 1
		Me.ToolStripProgressBar1.Visible = False
		'
		'StatusLabel1
		'
		Me.StatusLabel1.Name = "StatusLabel1"
		Me.StatusLabel1.Size = New System.Drawing.Size(16, 17)
		Me.StatusLabel1.Text = "   "
		'
		'Label_UserName
		'
		Me.Label_UserName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Label_UserName.BackColor = System.Drawing.Color.Transparent
		Me.Label_UserName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label_UserName.Location = New System.Drawing.Point(12, 130)
		Me.Label_UserName.Name = "Label_UserName"
		Me.Label_UserName.Size = New System.Drawing.Size(458, 13)
		Me.Label_UserName.TabIndex = 74
		Me.Label_UserName.Text = "UserName"
		'
		'Label_AppName
		'
		Me.Label_AppName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Label_AppName.BackColor = System.Drawing.Color.Transparent
		Me.Label_AppName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label_AppName.Location = New System.Drawing.Point(12, 152)
		Me.Label_AppName.Name = "Label_AppName"
		Me.Label_AppName.Size = New System.Drawing.Size(458, 13)
		Me.Label_AppName.TabIndex = 75
		Me.Label_AppName.Text = "APP_Name"
		'
		'ListBox_Permissions
		'
		Me.ListBox_Permissions.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
								Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.ListBox_Permissions.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.ListBox_Permissions.FormattingEnabled = True
		Me.ListBox_Permissions.ItemHeight = 14
		Me.ListBox_Permissions.Location = New System.Drawing.Point(12, 179)
		Me.ListBox_Permissions.Name = "ListBox_Permissions"
		Me.ListBox_Permissions.Size = New System.Drawing.Size(458, 172)
		Me.ListBox_Permissions.TabIndex = 76
		'
		'Label_App
		'
		Me.Label_App.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Label_App.BackColor = System.Drawing.Color.Transparent
		Me.Label_App.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label_App.Location = New System.Drawing.Point(12, 9)
		Me.Label_App.Name = "Label_App"
		Me.Label_App.Size = New System.Drawing.Size(458, 13)
		Me.Label_App.TabIndex = 77
		Me.Label_App.Text = "App"
		'
		'Label_Connection
		'
		Me.Label_Connection.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Label_Connection.BackColor = System.Drawing.Color.Transparent
		Me.Label_Connection.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label_Connection.Location = New System.Drawing.Point(12, 32)
		Me.Label_Connection.Name = "Label_Connection"
		Me.Label_Connection.Size = New System.Drawing.Size(458, 13)
		Me.Label_Connection.TabIndex = 78
		Me.Label_Connection.Text = "Connection"
		'
		'Label_Server
		'
		Me.Label_Server.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Label_Server.BackColor = System.Drawing.Color.Transparent
		Me.Label_Server.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label_Server.Location = New System.Drawing.Point(12, 80)
		Me.Label_Server.Name = "Label_Server"
		Me.Label_Server.Size = New System.Drawing.Size(458, 13)
		Me.Label_Server.TabIndex = 79
		Me.Label_Server.Text = "Server"
		'
		'Label_MainConnectString
		'
		Me.Label_MainConnectString.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Label_MainConnectString.BackColor = System.Drawing.Color.Transparent
		Me.Label_MainConnectString.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label_MainConnectString.Location = New System.Drawing.Point(12, 55)
		Me.Label_MainConnectString.Name = "Label_MainConnectString"
		Me.Label_MainConnectString.Size = New System.Drawing.Size(458, 13)
		Me.Label_MainConnectString.TabIndex = 80
		Me.Label_MainConnectString.Text = "Connection"
		'
		'frmAbout
		'
		Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
		Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
		Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
		Me.ClientSize = New System.Drawing.Size(482, 391)
		Me.Controls.Add(Me.Label_MainConnectString)
		Me.Controls.Add(Me.Label_Server)
		Me.Controls.Add(Me.Label_Connection)
		Me.Controls.Add(Me.Label_App)
		Me.Controls.Add(Me.ListBox_Permissions)
		Me.Controls.Add(Me.Label_AppName)
		Me.Controls.Add(Me.Label_UserName)
		Me.Controls.Add(Me.Status1)
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.MinimumSize = New System.Drawing.Size(450, 400)
		Me.Name = "frmAbout"
		Me.Text = "About"
		Me.Status1.ResumeLayout(False)
		Me.Status1.PerformLayout()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

#End Region

#Region " Form Locals and Constants "


	' Form 'Parent', the Main Venice form.
	' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
	Private WithEvents _MainForm As VeniceMain

	' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
	Private FormTooltip As New ToolTip()

    ''' <summary>
    ''' The form controls
    ''' </summary>
	Private FormControls As ArrayList = Nothing

	' Form Constants, specific to the table being updated.

    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
	Private ALWAYS_CLOSE_THIS_FORM As Boolean = True

	' The standard ChangeID for this form. e.g. tblPerson
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
	Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

	' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
	Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
	Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

	' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
	Private THIS_FORM_FormID As VeniceFormID

	' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
	Private FormIsValid As Boolean
    ''' <summary>
    ''' The form changed
    ''' </summary>
	Private FormChanged As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
	Private _FormOpenFailed As Boolean

	' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
	Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
	Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
	Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
	Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
	Public ReadOnly Property MainForm() As VeniceMain Implements StandardVeniceForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
	Public Property IsOverCancelButton() As Boolean Implements StandardVeniceForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return False
		End Get
		Set(ByVal Value As Boolean)
		End Set
	End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
	Public ReadOnly Property IsInPaint() As Boolean Implements StandardVeniceForm.IsInPaint
		Get
			Return False
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property InUse() As Boolean Implements StandardVeniceForm.InUse
		Get
			Return True
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property FormOpenFailed() As Boolean Implements StandardVeniceForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmAbout"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
	Public Sub New(ByVal pMainForm As VeniceMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()

		_MainForm = pMainForm
		AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

		_FormOpenFailed = False

		' ******************************************************
		' Form Specific Settings :
		' ******************************************************

		' Form Permissioning :-

		THIS_FORM_PermissionArea = Me.Name
		THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

		' 'This' form ID

		THIS_FORM_FormID = VeniceFormID.frmAbout

		' Format Event Handlers for form controls

		AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

		' ******************************************************
		' End Form Specific.
		' ******************************************************

		THIS_FORM_ChangeID = RenaissanceChangeID.KnowledgeDate

	End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

	' Form Initialisation code.
	'
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
	Public Sub ResetForm() Implements StandardVeniceForm.ResetForm

		Call Form_Load(Me, New System.EventArgs)
	End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
	Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
		_FormOpenFailed = False

		If Not (MainForm Is Nothing) Then
			FormIsValid = True
		Else
			MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		Try

			GetFormData()

		Catch ex As Exception
		End Try

	End Sub

    ''' <summary>
    ''' Handles the Closing event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
	Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		Dim HideForm As Boolean

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...


		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else
			HideForm = True
			If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.ReportForm_CacheCount Then
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			e.Cancel = True
		Else
			Try
				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

			Catch ex As Exception
			End Try
		End If

	End Sub


#End Region

	' Routine to handle changes / updates to tables by this and other windows.
	' If this, or any other, form posts a change to a table, then it will invoke an update event 
	' detailing what tables have been altered.
	' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
	' the 'VeniceAutoUpdate' event of the main Venice form.
	' Each form may them react as appropriate to changes in any table that might impact it.
	'
    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		' ****************************************************************
		'
		' ****************************************************************

	End Sub


    ''' <summary>
    ''' Gets the form data.
    ''' </summary>
	Private Sub GetFormData()
		' ****************************************************************
		'
		' ****************************************************************

		Dim myConnection As SqlConnection
		Dim ThisCommand As New SqlCommand
		Dim ThisTable As New DataTable
		Dim MaxNameSize As Integer = 0

		Try
			myConnection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)

      Label_App.Text = "Venice " & Application.ProductVersion

			If (myConnection Is Nothing) Then
				Label_Connection.Text = "No Database connection"
			Else
				Label_Connection.Text = myConnection.ConnectionString
			End If

			Label_MainConnectString.Text = "ConnectString = '" & MainForm.SQLConnectString

			Label_Server.Text = "MessageServer = '" & MainForm.MessageServerName

		Catch ex As Exception
			myConnection = Nothing
		End Try

		Try
			ListBox_Permissions.Items.Clear()

			ThisCommand.Connection = MainForm.GetVeniceConnection()

			' UserName

			ThisCommand.CommandType = CommandType.Text
			ThisCommand.CommandText = "SELECT User_Name()"
			Label_UserName.Text = "Username() : '" & ThisCommand.ExecuteScalar.ToString & "'"

			ThisCommand.CommandText = "SELECT APP_NAME()"
			Label_AppName.Text = "App_Name() : '" & ThisCommand.ExecuteScalar.ToString & "'"

			ThisCommand.CommandText = "SELECT * FROM fn_tblUserPermissions_SelectKD(NULL) WHERE [Username] = User_name()"
      MainForm.LoadTable_Custom(ThisTable, ThisCommand)
      'ThisTable.Load(ThisCommand.ExecuteReader)

			For Each ThisRow As DataRow In ThisTable.Rows
				MaxNameSize = Math.Max(MaxNameSize, ThisRow("Feature").ToString.Length)
			Next

			For Each ThisRow As DataRow In ThisTable.Rows
				ListBox_Permissions.Items.Add(ThisRow("Feature").ToString.PadRight(MaxNameSize) & ", Type : " & ThisRow("FeatureType").ToString.PadRight(2) & ", Perms (RIUD) : " & ThisRow("Perm_Read").ToString & ThisRow("Perm_Insert").ToString & ThisRow("Perm_Update").ToString & ThisRow("Perm_Delete").ToString)
			Next

		Catch ex As Exception
		Finally
			Try
				If (ThisCommand IsNot Nothing) AndAlso (ThisCommand.Connection IsNot Nothing) Then
					ThisCommand.Connection.Close()
					ThisCommand.Connection = Nothing
				End If

				ThisCommand.Parameters.Clear()
				ThisCommand = Nothing
			Catch ex As Exception
			End Try
		End Try



	End Sub

	



End Class



