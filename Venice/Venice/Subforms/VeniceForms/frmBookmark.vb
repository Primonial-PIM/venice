' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmBookmark.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals


''' <summary>
''' Class frmBookmark
''' </summary>
Public Class frmBookmark

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmBookmark"/> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The LBL bookmark description
    ''' </summary>
  Friend WithEvents lblBookmarkDescription As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit audit ID
    ''' </summary>
  Friend WithEvents editAuditID As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The edit bookmark description
    ''' </summary>
  Friend WithEvents editBookmarkDescription As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The BTN nav first
    ''' </summary>
  Friend WithEvents btnNavFirst As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN nav prev
    ''' </summary>
  Friend WithEvents btnNavPrev As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN nav next
    ''' </summary>
  Friend WithEvents btnNavNext As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN last
    ''' </summary>
  Friend WithEvents btnLast As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN add
    ''' </summary>
  Friend WithEvents btnAdd As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN delete
    ''' </summary>
  Friend WithEvents btnDelete As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN cancel
    ''' </summary>
  Friend WithEvents btnCancel As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN save
    ''' </summary>
  Friend WithEvents btnSave As System.Windows.Forms.Button
    ''' <summary>
    ''' The combo_ select bookmark description
    ''' </summary>
  Friend WithEvents Combo_SelectBookmarkDescription As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The panel1
    ''' </summary>
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The label1
    ''' </summary>
  Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The group box1
    ''' </summary>
  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The BTN close
    ''' </summary>
  Friend WithEvents btnClose As System.Windows.Forms.Button
    ''' <summary>
    ''' The label3
    ''' </summary>
  Friend WithEvents Label3 As System.Windows.Forms.Label
    ''' <summary>
    ''' The radio_ from live
    ''' </summary>
  Friend WithEvents Radio_FromLive As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The radio_ from precise time
    ''' </summary>
  Friend WithEvents Radio_FromPreciseTime As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The radio_ from whole day
    ''' </summary>
  Friend WithEvents Radio_FromWholeDay As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The date_ bookmark date
    ''' </summary>
  Friend WithEvents Date_BookmarkDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The check_ show to all users
    ''' </summary>
  Friend WithEvents Check_ShowToAllUsers As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The panel_ radios
    ''' </summary>
  Friend WithEvents Panel_Radios As System.Windows.Forms.Panel
    ''' <summary>
    ''' The text_ user name
    ''' </summary>
  Friend WithEvents Text_UserName As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The root menu
    ''' </summary>
  Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
    ''' <summary>
    ''' The label2
    ''' </summary>
  Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.lblBookmarkDescription = New System.Windows.Forms.Label
    Me.editAuditID = New System.Windows.Forms.TextBox
    Me.editBookmarkDescription = New System.Windows.Forms.TextBox
    Me.btnNavFirst = New System.Windows.Forms.Button
    Me.btnNavPrev = New System.Windows.Forms.Button
    Me.btnNavNext = New System.Windows.Forms.Button
    Me.btnLast = New System.Windows.Forms.Button
    Me.btnAdd = New System.Windows.Forms.Button
    Me.btnDelete = New System.Windows.Forms.Button
    Me.btnCancel = New System.Windows.Forms.Button
    Me.btnSave = New System.Windows.Forms.Button
    Me.Combo_SelectBookmarkDescription = New System.Windows.Forms.ComboBox
    Me.Panel1 = New System.Windows.Forms.Panel
    Me.Label1 = New System.Windows.Forms.Label
    Me.GroupBox1 = New System.Windows.Forms.GroupBox
    Me.btnClose = New System.Windows.Forms.Button
    Me.Label3 = New System.Windows.Forms.Label
    Me.Panel_Radios = New System.Windows.Forms.Panel
    Me.Radio_FromLive = New System.Windows.Forms.RadioButton
    Me.Radio_FromPreciseTime = New System.Windows.Forms.RadioButton
    Me.Radio_FromWholeDay = New System.Windows.Forms.RadioButton
    Me.Date_BookmarkDate = New System.Windows.Forms.DateTimePicker
    Me.Check_ShowToAllUsers = New System.Windows.Forms.CheckBox
    Me.Text_UserName = New System.Windows.Forms.TextBox
    Me.Label2 = New System.Windows.Forms.Label
    Me.RootMenu = New System.Windows.Forms.MenuStrip
    Me.Panel1.SuspendLayout()
    Me.Panel_Radios.SuspendLayout()
    Me.SuspendLayout()
    '
    'lblBookmarkDescription
    '
    Me.lblBookmarkDescription.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.lblBookmarkDescription.Location = New System.Drawing.Point(8, 103)
    Me.lblBookmarkDescription.Name = "lblBookmarkDescription"
    Me.lblBookmarkDescription.Size = New System.Drawing.Size(100, 23)
    Me.lblBookmarkDescription.TabIndex = 39
    Me.lblBookmarkDescription.Text = "Bookmark Name"
    '
    'editAuditID
    '
    Me.editAuditID.Enabled = False
    Me.editAuditID.Location = New System.Drawing.Point(308, 31)
    Me.editAuditID.Name = "editAuditID"
    Me.editAuditID.Size = New System.Drawing.Size(64, 20)
    Me.editAuditID.TabIndex = 1
    '
    'editBookmarkDescription
    '
    Me.editBookmarkDescription.Location = New System.Drawing.Point(120, 103)
    Me.editBookmarkDescription.Name = "editBookmarkDescription"
    Me.editBookmarkDescription.Size = New System.Drawing.Size(252, 20)
    Me.editBookmarkDescription.TabIndex = 3
    '
    'btnNavFirst
    '
    Me.btnNavFirst.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNavFirst.Location = New System.Drawing.Point(8, 8)
    Me.btnNavFirst.Name = "btnNavFirst"
    Me.btnNavFirst.Size = New System.Drawing.Size(40, 28)
    Me.btnNavFirst.TabIndex = 0
    Me.btnNavFirst.Text = "<<"
    '
    'btnNavPrev
    '
    Me.btnNavPrev.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNavPrev.Location = New System.Drawing.Point(49, 8)
    Me.btnNavPrev.Name = "btnNavPrev"
    Me.btnNavPrev.Size = New System.Drawing.Size(35, 28)
    Me.btnNavPrev.TabIndex = 1
    Me.btnNavPrev.Text = "<"
    '
    'btnNavNext
    '
    Me.btnNavNext.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNavNext.Location = New System.Drawing.Point(88, 8)
    Me.btnNavNext.Name = "btnNavNext"
    Me.btnNavNext.Size = New System.Drawing.Size(35, 28)
    Me.btnNavNext.TabIndex = 2
    Me.btnNavNext.Text = ">"
    '
    'btnLast
    '
    Me.btnLast.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnLast.Location = New System.Drawing.Point(124, 8)
    Me.btnLast.Name = "btnLast"
    Me.btnLast.Size = New System.Drawing.Size(40, 28)
    Me.btnLast.TabIndex = 3
    Me.btnLast.Text = ">>"
    '
    'btnAdd
    '
    Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnAdd.Location = New System.Drawing.Point(176, 8)
    Me.btnAdd.Name = "btnAdd"
    Me.btnAdd.Size = New System.Drawing.Size(75, 28)
    Me.btnAdd.TabIndex = 4
    Me.btnAdd.Text = "&New"
    '
    'btnDelete
    '
    Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnDelete.Location = New System.Drawing.Point(112, 283)
    Me.btnDelete.Name = "btnDelete"
    Me.btnDelete.Size = New System.Drawing.Size(75, 28)
    Me.btnDelete.TabIndex = 9
    Me.btnDelete.Text = "&Delete"
    '
    'btnCancel
    '
    Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnCancel.Location = New System.Drawing.Point(196, 283)
    Me.btnCancel.Name = "btnCancel"
    Me.btnCancel.Size = New System.Drawing.Size(75, 28)
    Me.btnCancel.TabIndex = 10
    Me.btnCancel.Text = "&Cancel"
    '
    'btnSave
    '
    Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnSave.Location = New System.Drawing.Point(24, 283)
    Me.btnSave.Name = "btnSave"
    Me.btnSave.Size = New System.Drawing.Size(75, 28)
    Me.btnSave.TabIndex = 8
    Me.btnSave.Text = "&Save"
    '
    'Combo_SelectBookmarkDescription
    '
    Me.Combo_SelectBookmarkDescription.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectBookmarkDescription.Location = New System.Drawing.Point(120, 31)
    Me.Combo_SelectBookmarkDescription.Name = "Combo_SelectBookmarkDescription"
    Me.Combo_SelectBookmarkDescription.Size = New System.Drawing.Size(164, 21)
    Me.Combo_SelectBookmarkDescription.TabIndex = 0
    '
    'Panel1
    '
    Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Panel1.Controls.Add(Me.btnNavFirst)
    Me.Panel1.Controls.Add(Me.btnNavPrev)
    Me.Panel1.Controls.Add(Me.btnNavNext)
    Me.Panel1.Controls.Add(Me.btnLast)
    Me.Panel1.Controls.Add(Me.btnAdd)
    Me.Panel1.Location = New System.Drawing.Point(60, 227)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(260, 48)
    Me.Panel1.TabIndex = 7
    '
    'Label1
    '
    Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label1.Location = New System.Drawing.Point(8, 31)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(100, 23)
    Me.Label1.TabIndex = 17
    Me.Label1.Text = "Select"
    '
    'GroupBox1
    '
    Me.GroupBox1.Location = New System.Drawing.Point(8, 63)
    Me.GroupBox1.Name = "GroupBox1"
    Me.GroupBox1.Size = New System.Drawing.Size(364, 4)
    Me.GroupBox1.TabIndex = 77
    Me.GroupBox1.TabStop = False
    Me.GroupBox1.Text = "GroupBox1"
    '
    'btnClose
    '
    Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnClose.Location = New System.Drawing.Point(280, 283)
    Me.btnClose.Name = "btnClose"
    Me.btnClose.Size = New System.Drawing.Size(75, 28)
    Me.btnClose.TabIndex = 11
    Me.btnClose.Text = "&Close"
    '
    'Label3
    '
    Me.Label3.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label3.Location = New System.Drawing.Point(8, 135)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(100, 16)
    Me.Label3.TabIndex = 115
    Me.Label3.Text = "Bookmark Date"
    '
    'Panel_Radios
    '
    Me.Panel_Radios.Controls.Add(Me.Radio_FromLive)
    Me.Panel_Radios.Controls.Add(Me.Radio_FromPreciseTime)
    Me.Panel_Radios.Controls.Add(Me.Radio_FromWholeDay)
    Me.Panel_Radios.Location = New System.Drawing.Point(8, 155)
    Me.Panel_Radios.Name = "Panel_Radios"
    Me.Panel_Radios.Size = New System.Drawing.Size(364, 24)
    Me.Panel_Radios.TabIndex = 5
    '
    'Radio_FromLive
    '
    Me.Radio_FromLive.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Radio_FromLive.Checked = True
    Me.Radio_FromLive.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Radio_FromLive.Location = New System.Drawing.Point(12, 4)
    Me.Radio_FromLive.Name = "Radio_FromLive"
    Me.Radio_FromLive.Size = New System.Drawing.Size(104, 16)
    Me.Radio_FromLive.TabIndex = 0
    Me.Radio_FromLive.TabStop = True
    Me.Radio_FromLive.Text = "'Live' BkMark"
    '
    'Radio_FromPreciseTime
    '
    Me.Radio_FromPreciseTime.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Radio_FromPreciseTime.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Radio_FromPreciseTime.Location = New System.Drawing.Point(252, 4)
    Me.Radio_FromPreciseTime.Name = "Radio_FromPreciseTime"
    Me.Radio_FromPreciseTime.Size = New System.Drawing.Size(104, 16)
    Me.Radio_FromPreciseTime.TabIndex = 2
    Me.Radio_FromPreciseTime.Text = "Precise Time"
    '
    'Radio_FromWholeDay
    '
    Me.Radio_FromWholeDay.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Radio_FromWholeDay.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Radio_FromWholeDay.Location = New System.Drawing.Point(128, 4)
    Me.Radio_FromWholeDay.Name = "Radio_FromWholeDay"
    Me.Radio_FromWholeDay.Size = New System.Drawing.Size(104, 16)
    Me.Radio_FromWholeDay.TabIndex = 1
    Me.Radio_FromWholeDay.Text = "WholeDay"
    '
    'Date_BookmarkDate
    '
    Me.Date_BookmarkDate.CustomFormat = "dd MMM yyyy"
    Me.Date_BookmarkDate.Enabled = False
    Me.Date_BookmarkDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
    Me.Date_BookmarkDate.Location = New System.Drawing.Point(120, 131)
    Me.Date_BookmarkDate.Name = "Date_BookmarkDate"
    Me.Date_BookmarkDate.Size = New System.Drawing.Size(252, 20)
    Me.Date_BookmarkDate.TabIndex = 4
    '
    'Check_ShowToAllUsers
    '
    Me.Check_ShowToAllUsers.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Check_ShowToAllUsers.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_ShowToAllUsers.Location = New System.Drawing.Point(8, 191)
    Me.Check_ShowToAllUsers.Name = "Check_ShowToAllUsers"
    Me.Check_ShowToAllUsers.Size = New System.Drawing.Size(126, 20)
    Me.Check_ShowToAllUsers.TabIndex = 6
    Me.Check_ShowToAllUsers.Text = "Show to all users"
    '
    'Text_UserName
    '
    Me.Text_UserName.Enabled = False
    Me.Text_UserName.Location = New System.Drawing.Point(120, 75)
    Me.Text_UserName.Name = "Text_UserName"
    Me.Text_UserName.Size = New System.Drawing.Size(252, 20)
    Me.Text_UserName.TabIndex = 2
    '
    'Label2
    '
    Me.Label2.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label2.Location = New System.Drawing.Point(8, 75)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(100, 23)
    Me.Label2.TabIndex = 118
    Me.Label2.Text = "User Name"
    '
    'RootMenu
    '
    Me.RootMenu.AllowMerge = False
    Me.RootMenu.Location = New System.Drawing.Point(0, 0)
    Me.RootMenu.Name = "RootMenu"
    Me.RootMenu.Size = New System.Drawing.Size(380, 24)
    Me.RootMenu.TabIndex = 119
    Me.RootMenu.Text = " "
    '
    'frmBookmark
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.CancelButton = Me.btnCancel
    Me.ClientSize = New System.Drawing.Size(380, 320)
    Me.Controls.Add(Me.RootMenu)
    Me.Controls.Add(Me.Text_UserName)
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.Check_ShowToAllUsers)
    Me.Controls.Add(Me.Label3)
    Me.Controls.Add(Me.Panel_Radios)
    Me.Controls.Add(Me.Date_BookmarkDate)
    Me.Controls.Add(Me.btnClose)
    Me.Controls.Add(Me.GroupBox1)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.Panel1)
    Me.Controls.Add(Me.Combo_SelectBookmarkDescription)
    Me.Controls.Add(Me.btnSave)
    Me.Controls.Add(Me.editAuditID)
    Me.Controls.Add(Me.editBookmarkDescription)
    Me.Controls.Add(Me.lblBookmarkDescription)
    Me.Controls.Add(Me.btnDelete)
    Me.Controls.Add(Me.btnCancel)
    Me.Name = "frmBookmark"
    Me.Text = "Add/Edit Bookmark"
    Me.Panel1.ResumeLayout(False)
    Me.Panel_Radios.ResumeLayout(False)
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "


  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
  Private WithEvents _MainForm As VeniceMain

  ' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
  Private FormTooltip As New ToolTip()

  ' Form Menu


  ' Form Constants, specific to the table being updated.
    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
  Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

  ' Form Locals, initialised on 'New' defining what standard data items to use
    ''' <summary>
  ''' Variable holding the form's data table name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_TABLENAME As String
    ''' <summary>
  ''' Variable holding the form's data adaptor name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_ADAPTORNAME As String
    ''' <summary>
  ''' Variable holding the form's dataset name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_DATASETNAME As String


  ' The standard ChangeID for this form. e.g. tblBookmark
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

    ''' <summary>
  ''' Principal control used for selecting items on this form. 
    ''' </summary>
  Private THIS_FORM_SelectingCombo As ComboBox
    ''' <summary>
  ''' Control to select after proessing the "New" button.
    ''' </summary>
  Private THIS_FORM_NewMoveToControl As Control

  ' Form Specific Order fields
    ''' <summary>
  ''' Field Name to show in the Select combo.
    ''' </summary>
  Private THIS_FORM_SelectBy As String
    ''' <summary>
  ''' Field Name to order the Select combo by.
    ''' </summary>
  Private THIS_FORM_OrderBy As String

    ''' <summary>
  ''' The field name that holds the unique identifier for each entry. e.g. FundID for the tblFund table.
    ''' </summary>
  Private THIS_FORM_ValueMember As String

  ' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
  Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
  Private THIS_FORM_FormID As VeniceFormID

  ' Data Structures

    ''' <summary>
    ''' My dataset
    ''' </summary>
	Private myDataset As RenaissanceDataClass.DSBookmark		' Form Specific !!!!
    ''' <summary>
    ''' My table
    ''' </summary>
	Private myTable As RenaissanceDataClass.DSBookmark.tblBookmarkDataTable
    ''' <summary>
    ''' My connection
    ''' </summary>
  Private myConnection As SqlConnection
    ''' <summary>
    ''' My adaptor
    ''' </summary>
  Private myAdaptor As SqlDataAdapter

    ''' <summary>
    ''' The this standard dataset
    ''' </summary>
  Private ThisStandardDataset As RenaissanceGlobals.StandardDataset

  ' Active Element.

    ''' <summary>
    ''' The sorted rows
    ''' </summary>
  Private SortedRows() As DataRow
    ''' <summary>
    ''' The select by sorted rows
    ''' </summary>
  Private SelectBySortedRows() As DataRow
    ''' <summary>
    ''' The this data row
    ''' </summary>
	Private thisDataRow As RenaissanceDataClass.DSBookmark.tblBookmarkRow		' Form Specific !!!!
    ''' <summary>
    ''' The this audit ID
    ''' </summary>
	Private thisAuditID As Integer
    ''' <summary>
    ''' The this position
    ''' </summary>
	Private thisPosition As Integer
    ''' <summary>
    ''' The __ is over cancel button
    ''' </summary>
	Private __IsOverCancelButton As Boolean
    ''' <summary>
    ''' The _ in use
    ''' </summary>
	Private _InUse As Boolean

	' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
	Private FormIsValid As Boolean
    ''' <summary>
    ''' The form changed
    ''' </summary>
	Private FormChanged As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
	Private _FormOpenFailed As Boolean
    ''' <summary>
    ''' The in paint
    ''' </summary>
	Private InPaint As Boolean
    ''' <summary>
    ''' The add new record
    ''' </summary>
	Private AddNewRecord As Boolean

	' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
	Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
	Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
	Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
	Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
	Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
	Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return __IsOverCancelButton
		End Get
		Set(ByVal Value As Boolean)
			__IsOverCancelButton = Value
		End Set
	End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
	Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
		Get
			Return InPaint
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
		Get
			Return _InUse
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmBookmark"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
	Public Sub New(ByVal pMainForm As VeniceMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()

		_MainForm = pMainForm
		AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

		_FormOpenFailed = False

		' ******************************************************
		' Form Specific Settings :
		' ******************************************************

		THIS_FORM_SelectingCombo = Me.Combo_SelectBookmarkDescription
		THIS_FORM_NewMoveToControl = Me.editBookmarkDescription

		' Default Select and Order fields.

		THIS_FORM_SelectBy = "BookMarkDescription"
		THIS_FORM_OrderBy = "BookMarkDescription"

		THIS_FORM_ValueMember = "BookmarkID"

		' Form Permissioning :-

		THIS_FORM_PermissionArea = Me.Name
		THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

		' 'This' form ID

		THIS_FORM_FormID = VeniceFormID.frmBookmark

		' This form's dataset type.

		ThisStandardDataset = RenaissanceStandardDatasets.tblBookmark	' This Defines the Form Data !!! 

		' Format Event Handlers for form controls

		AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler
		AddHandler editBookmarkDescription.LostFocus, AddressOf MainForm.Text_NotNull

		' Form Control Changed events
		AddHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
		AddHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		AddHandler editBookmarkDescription.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Date_BookmarkDate.ValueChanged, AddressOf Me.FormControlChanged
		AddHandler Radio_FromLive.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Radio_FromPreciseTime.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Radio_FromWholeDay.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Check_ShowToAllUsers.CheckedChanged, AddressOf Me.FormControlChanged

		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

		' ******************************************************
		' End Form Specific.
		' ******************************************************

		' Data object names standard to this Form type.

		THIS_TABLENAME = ThisStandardDataset.TableName
		THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
		THIS_DATASETNAME = ThisStandardDataset.DatasetName

		THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

		' Establish / Retrieve data objects for this form.

		myConnection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)
		myAdaptor = MainForm.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, VENICE_CONNECTION, THIS_TABLENAME)
		myDataset = MainForm.Load_Table(ThisStandardDataset, False)
		myTable = myDataset.Tables(0)

		MainForm.BuildStandardFormMenu(Me.RootMenu, myDataset.Tables(0), AddressOf SelectMenuEvent, AddressOf OrderMenuEvent, AddressOf AuditReportMenuEvent)

	End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

	' Form Initialisation code.
	'
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
	Public Sub ResetForm() Implements StandardVeniceForm.ResetForm
		THIS_FORM_SelectBy = "BookmarkDescription"
		THIS_FORM_OrderBy = "BookmarkDescription"

		Call Form_Load(Me, New System.EventArgs)
	End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
	Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
		_FormOpenFailed = False
		_InUse = True

		' Initialise Data structures. Connection, Adaptor and Dataset.

		If Not (MainForm Is Nothing) Then
			FormIsValid = True
		Else
			MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myConnection Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myAdaptor Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myDataset Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		' Initialse form

		InPaint = True
		IsOverCancelButton = False

		' Check User permissions
		Call CheckPermissions()
		If (HasReadPermission = False) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		' Build Sorted data list from which this form operates
		Call SetSortedRows()

		' Display initial record.

		thisPosition = 0
		If THIS_FORM_SelectingCombo.Items.Count > 0 Then
			Me.THIS_FORM_SelectingCombo.SelectedIndex = 0
			thisDataRow = SortedRows(thisPosition)
			Call GetFormData(thisDataRow)
		Else
			Me.THIS_FORM_SelectingCombo.SelectedIndex = (-1)
			Call GetFormData(Nothing)
		End If

		InPaint = False


	End Sub

    ''' <summary>
    ''' Handles the Closing event of the frmBookmark control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
	Private Sub frmBookmark_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		Dim HideForm As Boolean

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...

		_InUse = False

		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else
			If (FormChanged = True) Then
				Call SetFormData()
			End If

			HideForm = True
			If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			e.Cancel = True
		Else
			Try
				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler
				RemoveHandler editBookmarkDescription.LostFocus, AddressOf MainForm.Text_NotNull

				' Form Control Changed events
				RemoveHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
				RemoveHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				RemoveHandler editBookmarkDescription.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Date_BookmarkDate.ValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Radio_FromLive.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Radio_FromPreciseTime.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Radio_FromWholeDay.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_ShowToAllUsers.CheckedChanged, AddressOf Me.FormControlChanged

			Catch ex As Exception
			End Try
		End If

	End Sub


#End Region

	' Routine to handle changes / updates to tables by this and other windows.
	' If this, or any other, form posts a change to a table, then it will invoke an update event 
	' detailing what tables have been altered.
	' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
	' the 'VeniceAutoUpdate' event of the main Venice form.
	' Each form may them react as appropriate to changes in any table that might impact it.
	'
    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		Dim OrgInPaint As Boolean
		Dim KnowledgeDateChanged As Boolean
		Dim SetButtonStatus_Flag As Boolean
		Dim RefreshForm As Boolean

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		OrgInPaint = InPaint
		InPaint = True
		KnowledgeDateChanged = False
		SetButtonStatus_Flag = False
		RefreshForm = False

		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
			KnowledgeDateChanged = True
			RefreshForm = True
		End If

		' ****************************************************************
		' Check for changes relevant to this form
		' ****************************************************************

		' Changes to the KnowledgeDate :-
		If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
			SetButtonStatus_Flag = True
		End If

		' Changes to the tblUserPermissions table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

			' Check ongoing permissions.

			Call CheckPermissions()
			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

				FormIsValid = False
				Me.Close()
				Exit Sub
			End If

			SetButtonStatus_Flag = True

		End If


		' ****************************************************************
		' Changes to the Main FORM table :-
		' ****************************************************************

		If (e.TableChanged(THIS_FORM_ChangeID) = True) Or KnowledgeDateChanged Then

			' Re-Set Controls etc.
			Call SetSortedRows()
			RefreshForm = True

			' Move again to the correct item
			thisPosition = Get_Position(thisAuditID)

			' Validate current position.
			If thisPosition >= 0 And thisPosition < Me.SortedRows.GetLength(0) Then
				thisDataRow = Me.SortedRows(thisPosition)
			Else
				If (Me.SortedRows.GetLength(0) > 0) And (AddNewRecord = False) Then
					thisPosition = 0
					thisDataRow = Me.SortedRows(thisPosition)
					FormChanged = False
				Else
					thisDataRow = Nothing
				End If
			End If

			' Set SelectingCombo Index.
			If (Me.THIS_FORM_SelectingCombo.Items.Count <= 0) Then
				Try
					Me.THIS_FORM_SelectingCombo.SelectedIndex = -1
				Catch ex As Exception
				End Try
			ElseIf (thisPosition < 0) Then
				Try
					MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
				Catch ex As Exception
				End Try
			Else
				Try
					Me.THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
				Catch ex As Exception
				End Try
			End If

		End If

		' ****************************************************************
		' Repaint if not currently in Edit Mode
		'
		' ****************************************************************

		InPaint = OrgInPaint

		' Retrieve form data if appropriate, i.e. this data item is not already being edited.
		If (RefreshForm = True) And (FormChanged = False) And (AddNewRecord = False) Then
			GetFormData(thisDataRow) ' Includes a call to 'SetButtonStatus()'
		Else
			If SetButtonStatus_Flag Then
				Call SetButtonStatus()
			End If
		End If

	End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

	' Build Sorted list from the Source dataset and update the Select Combo
    ''' <summary>
    ''' Sets the sorted rows.
    ''' </summary>
	Private Sub SetSortedRows()

		Dim OrgInPaint As Boolean

		Dim thisrow As DataRow
		Dim thisDrowDownWidth As Integer
		Dim SizingBitmap As Bitmap
		Dim SizingGraphics As Graphics

		' Form Specific Selection Combo :-
		If (THIS_FORM_SelectingCombo Is Nothing) Then Exit Sub

		' Code is pretty Generic form here on...

		' Set paint local so that changes to the selection combo do not trigger form updates.

		OrgInPaint = InPaint
		InPaint = True

		' Get selected Row sets, or exit if no data is present.
		If myDataset Is Nothing Then
			ReDim SortedRows(0)
			ReDim SelectBySortedRows(0)
		Else
			SortedRows = myTable.Select("True", THIS_FORM_OrderBy)
			SelectBySortedRows = myTable.Select("True", THIS_FORM_SelectBy)
		End If

		' Set Combo data source
		THIS_FORM_SelectingCombo.DataSource = SortedRows
		THIS_FORM_SelectingCombo.DisplayMember = THIS_FORM_SelectBy
		THIS_FORM_SelectingCombo.ValueMember = THIS_FORM_ValueMember

		' Ensure that the Selection Combo's DrowDownWidth is wide enough to take the required strings

		thisDrowDownWidth = THIS_FORM_SelectingCombo.Width
		For Each thisrow In SortedRows

			' Compute the string dimensions in the given font
			SizingBitmap = New Bitmap(1, 1, System.Drawing.Imaging.PixelFormat.Format32bppArgb)
			SizingGraphics = Graphics.FromImage(SizingBitmap)
			Dim stringSize As SizeF = SizingGraphics.MeasureString(thisrow(THIS_FORM_SelectBy).ToString, THIS_FORM_SelectingCombo.Font)
			If (stringSize.Width) > thisDrowDownWidth Then
				thisDrowDownWidth = CInt(stringSize.Width)
			End If
		Next

		THIS_FORM_SelectingCombo.DropDownWidth = thisDrowDownWidth

		InPaint = OrgInPaint
	End Sub


	' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
	Private Sub CheckPermissions()

		Dim Permissions As Integer

		Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

		HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
		HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
		HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
		HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

	End Sub

	' Flag changes to form controls, Event Associations made in 'New' routine.
    ''' <summary>
  ''' Basic event handler, called when a form value is changed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

		If InPaint = False Then
			If (Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord) Then
				FormChanged = True
				Me.btnSave.Enabled = True
				Me.btnCancel.Enabled = True
			End If
		End If

	End Sub

    ''' <summary>
    ''' Selects the menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Public Sub SelectMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Select Menu
		' Event association is made during the dynamic menu creation

		THIS_FORM_SelectBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

		Call SetSortedRows()

	End Sub

    ''' <summary>
    ''' Orders the menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Public Sub OrderMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Order Menu
		' Event association is made during the dynamic menu creation

		THIS_FORM_OrderBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

		Call SetSortedRows()

	End Sub

    ''' <summary>
    ''' Audits the report menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Public Sub AuditReportMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Audit Report Menu
		' Event association is made during the dynamic menu creation

		Try
			Select Case CInt(CType(sender, System.Windows.Forms.ToolStripMenuItem).Tag)
				Case 0 ' This Record
					If (Me.AddNewRecord = False) AndAlso (thisAuditID >= 0) Then
						Run_AuditReport_ThisRecord(MainForm, ThisStandardDataset, thisAuditID, MainForm.VeniceProgressBar)
					End If

				Case 1 ' All Records
					Run_AuditReport_ALLRecords(MainForm, ThisStandardDataset, MainForm.VeniceProgressBar)

			End Select
		Catch ex As Exception
			Call MainForm.LogError(Me.Name & ", AuditReportMenuEvent()", 0, ex.Message, "Error calling Audit report.", ex.StackTrace, True)
		End Try

	End Sub


#End Region

#Region " Event functions"

    ''' <summary>
    ''' Handles the CheckedChanged event of the FromRadios control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub FromRadios_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_FromLive.CheckedChanged, Radio_FromWholeDay.CheckedChanged, Radio_FromPreciseTime.CheckedChanged
		' *******************************************************************************
		'
		'
		' *******************************************************************************

		If Me.Radio_FromLive.Checked = True Then
			Me.Date_BookmarkDate.Enabled = False
			Me.Date_BookmarkDate.Value = Now
			Me.Radio_FromPreciseTime.Checked = True
		Else
			Me.Date_BookmarkDate.Enabled = True

			If Me.Radio_FromWholeDay.Checked = True Then
				Me.Date_BookmarkDate.CustomFormat = DISPLAYMEMBER_DATEFORMAT
			Else
				Me.Date_BookmarkDate.CustomFormat = DISPLAYMEMBER_LONGDATEFORMAT
			End If
		End If

	End Sub
#End Region

#Region " Set Form Combos (Form Specific Code) "

#End Region

#Region " Get & Set Form Data / SetButton / ValidateForm / btnCancel Events (Form Specific Code) "

    ''' <summary>
    ''' Gets the form data.
    ''' </summary>
    ''' <param name="ThisRow">The this row.</param>
	Private Sub GetFormData(ByRef ThisRow As RenaissanceDataClass.DSBookmark.tblBookmarkRow)
		' Routine to populate form Controls given a data row.
		' Form is cleared for invalid Datarows or Invalid Form.

		Dim OrgInpaint As Boolean

		' Set 'Paint' flag to prevent event actions caused by changing the value of form fields.
		OrgInpaint = InPaint
		InPaint = True


		If (ThisRow Is Nothing) Or (FormIsValid = False) Or (Me.InUse = False) Then
			' Bad / New Datarow - Clear Form.

			thisAuditID = (-1)

			Me.editAuditID.Text = ""

			Me.editBookmarkDescription.Text = ""
			Me.Text_UserName.Text = ""
			Me.Radio_FromLive.Checked = True
			Me.Check_ShowToAllUsers.Checked = True

			If AddNewRecord = True Then
				Me.btnCancel.Enabled = True
				Me.THIS_FORM_SelectingCombo.Enabled = False
			Else
				Me.btnCancel.Enabled = False
				Me.THIS_FORM_SelectingCombo.Enabled = True
			End If

		Else

			' Populate Form with given data.
			Try
				thisAuditID = thisDataRow.AuditID

				Me.editAuditID.Text = thisDataRow.AuditID.ToString

				Me.Text_UserName.Text = thisDataRow.UserEntered
				Me.editBookmarkDescription.Text = thisDataRow.BookMarkDescription
				Me.Date_BookmarkDate.Value = thisDataRow.BookmarkDate

				If thisDataRow.BookmarkWholeDay = 0 Then
					Me.Radio_FromPreciseTime.Checked = True
				Else
					Me.Radio_FromWholeDay.Checked = True
				End If

				Call FromRadios_CheckedChanged(Me, New EventArgs)

				If (thisDataRow.BookmarkVisibleToAll = 0) Then
					Me.Check_ShowToAllUsers.Checked = False
				Else
					Me.Check_ShowToAllUsers.Checked = True
				End If

				AddNewRecord = False
				' MainForm.SetComboSelectionLengths(Me)

				Me.btnCancel.Enabled = False
				Me.THIS_FORM_SelectingCombo.Enabled = True

			Catch ex As Exception

				Call MainForm.LogError(Me.Name & ", GetFormData", 0, ex.Message, "Error Showing Data", ex.StackTrace, True)
				Call GetFormData(Nothing)

			End Try

		End If

		' Allow Field events to trigger before 'InPaint' Is re-set. 
		' (Should) Prevent Validation errors during Form Draw.
		Application.DoEvents()

		' Restore 'Paint' flag.
		InPaint = OrgInpaint
		FormChanged = False
		Me.btnSave.Enabled = False

		' As it says on the can....
		If (Not ThisRow Is Nothing) AndAlso (ThisRow.Updatable = 0) Then
			Call SetButtonStatus(True)
		Else
			Call SetButtonStatus()
		End If

	End Sub

    ''' <summary>
    ''' Sets the form data.
    ''' </summary>
    ''' <param name="pConfirm">if set to <c>true</c> [p confirm].</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	Private Function SetFormData(Optional ByVal pConfirm As Boolean = True) As Boolean
		' *************************************************************
		'
		' *************************************************************
		Dim ErrMessage As String = ""
		Dim ErrFlag As Boolean
		Dim ErrStack As String = ""
		Dim ProtectedItem As Boolean = False

		' *************************************************************
		' Appropriate Save permission :-
		' *************************************************************

		If (Me.HasUpdatePermission = False) And ((Me.HasInsertPermission = False) Or (Me.AddNewRecord = False)) Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "You do not have permission to save this record.", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' *************************************************************
		' If Save button is disabled then should not be able to save, exit silently.
		' *************************************************************
		If Me.btnSave.Enabled = False Then
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' *************************************************************
		' KnowledgeDate OK :-
		' *************************************************************

		If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Changes to Venice are not allowed when the Knowledgedate is not set to `NOW`", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' Confirm Save, if required.

		If (pConfirm = True) Then
			If MessageBox.Show("Save Changes ?", "Save Changes ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
				Return True
				Exit Function
			End If
		End If

		' *************************************************************
		' Procedure to Save the current form information.
		' *************************************************************

		Dim StatusString As String = ""
		Dim LogString As String
		Dim UpdateRows(0) As DataRow
		Dim Position As Integer

		If (FormChanged = False) Or (FormIsValid = False) Then
			Return False
			Exit Function
		End If

		' Validation
		If ValidateForm(StatusString) = False Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Validation Error." & vbCrLf & StatusString, "", True)
			Return False
			Exit Function
		End If

		' Check current position in the table.
		Position = Get_Position(thisAuditID)

		' Allow for new or missing ID.
		If Position < 0 Then
			If (AddNewRecord = True) OrElse (SortedRows.Length <= 0) Then
				thisDataRow = myTable.NewRow
				LogString = "Add: "
				AddNewRecord = True
			Else
				Return False
				Exit Function
			End If
		Else
			' Row found OK.
			LogString = "Edit : AuditID = " & thisAuditID.ToString

			thisDataRow = Me.SortedRows(Position)

			'' Check for Protected Bookmark
			'If Not thisDataRow.IsBookmarkIDNull Then
			'  If (thisDataRow.BookmarkID > 0) AndAlso (System.Enum.IsDefined(GetType(RenaissanceGlobals.Currency), thisDataRow.BookmarkID) = True) Then
			'    ProtectedItem = True
			'  End If
			'End If

		End If

		' Set 'Paint' flag.
		InPaint = True

		' *************************************************************
		' Lock the Data Table, to prevent update conflicts.
		' *************************************************************
		SyncLock myTable

			' Initiate Edit,
			thisDataRow.BeginEdit()

			' Set Data Values
			If (ProtectedItem = True) AndAlso (AddNewRecord = False) AndAlso (thisDataRow.BookMarkDescription <> Me.editBookmarkDescription.Text) Then
				MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "This Bookmark is protected, Bookmark Code may not be changed.", "", True)
			Else
				thisDataRow.BookMarkDescription = Me.editBookmarkDescription.Text
			End If

			If Me.Radio_FromLive.Checked = True Or Me.Radio_FromPreciseTime.Checked = True Then
				thisDataRow.BookmarkDate = Me.Date_BookmarkDate.Value
				thisDataRow.BookmarkWholeDay = False
			Else
				thisDataRow.BookmarkDate = Me.Date_BookmarkDate.Value.Date.AddSeconds(LAST_SECOND)
				thisDataRow.BookmarkWholeDay = True

				' CDate(CDate(Me.Date_KnowledgeDate.Value).ToString(DISPLAYMEMBER_DATEFORMAT) & " 23:59:59")
			End If

			thisDataRow.BookmarkUserID = 0
			If Me.Check_ShowToAllUsers.Checked = True Then
				thisDataRow.BookmarkVisibleToAll = (-1)
			Else
				thisDataRow.BookmarkVisibleToAll = 0
			End If


			thisDataRow.EndEdit()
			InPaint = False

			' Add and Update DataRow. 

			ErrFlag = False

			If AddNewRecord = True Then
				Try
					myTable.Rows.Add(thisDataRow)
				Catch ex As Exception
					ErrFlag = True
					ErrMessage = ex.Message
					ErrStack = ex.StackTrace
				End Try
			End If

			UpdateRows(0) = thisDataRow

			' Post Additions / Updates to the underlying table :-
			Dim temp As Integer
			Try
				If (ErrFlag = False) Then
					myAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
					myAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

					LogString = MainForm.GetStandardLogString(thisDataRow)

					temp = MainForm.AdaptorUpdate(Me.Name, myAdaptor, UpdateRows)

				End If

			Catch ex As Exception

				ErrMessage = ex.Message
				ErrFlag = True
				ErrStack = ex.StackTrace

			End Try

			thisAuditID = thisDataRow.AuditID


		End SyncLock

		' *************************************************************
		' Error report. Not done in the Try-Catch as that would serve to persist the SyncLock.
		' *************************************************************

		If (ErrFlag = True) Then
			Call MainForm.LogError(Me.Name & ", SetFormData", 0, ErrMessage, "Error Saving Data", ErrStack, True)
		End If

		' Finish off

		AddNewRecord = False
		FormChanged = False

		Me.THIS_FORM_SelectingCombo.Enabled = True

		' Propogate changes

    If (thisDataRow IsNot Nothing) AndAlso (thisDataRow.IsAuditIDNull = False) AndAlso (thisDataRow.AuditID > 0) Then
      Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID, thisDataRow.AuditID.ToString), True)
    Else
      Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID), True)
    End If

	End Function

    ''' <summary>
    ''' Sets the button status.
    ''' </summary>
    ''' <param name="ShowReadOnly">if set to <c>true</c> [show read only].</param>
	Private Sub SetButtonStatus(Optional ByVal ShowReadOnly As Boolean = False)
		' Sets the status of the form controlls appropriate to the current users 
		'permissions and the 'Changed' or 'New' status of the form.

		' No Read permission :-

		If Me.HasReadPermission = False Then
			MainForm.LogError(Me.Name & ", SetButtonStatus()", 0, "", "You do not have Read permission for this Form", "", True)
			Me.Close()
			Exit Sub
		End If

		' Has Insert Permission.
		If Me.HasInsertPermission Then
			Me.btnAdd.Enabled = True
		Else
			Me.btnAdd.Enabled = False
		End If

		' Has Delete permission. 
		If (Me.HasDeletePermission) And (Me.AddNewRecord = False) And (thisPosition >= 0) Then
			Me.btnDelete.Enabled = True
		Else
			Me.btnDelete.Enabled = False
		End If

		If (Not ShowReadOnly) And ((AddNewRecord = True) Or (SortedRows.Length > 0)) And (Me.MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And _
			((Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord)) Then

			Me.editBookmarkDescription.Enabled = True
			Me.Date_BookmarkDate.Enabled = True
			Me.Panel_Radios.Enabled = True
			Me.Check_ShowToAllUsers.Enabled = True

		Else

			Me.editBookmarkDescription.Enabled = False
			Me.Date_BookmarkDate.Enabled = False
			Me.Panel_Radios.Enabled = False
			Me.Check_ShowToAllUsers.Enabled = False

		End If

	End Sub

    ''' <summary>
    ''' Validates the form.
    ''' </summary>
    ''' <param name="pReturnString">The p return string.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	Private Function ValidateForm(ByRef pReturnString As String) As Boolean
		' Form Validation code.
		' 
		' This code should be the final arbiter of what is allowed. no assumptions regarding 
		' prior validation should be made.
		' 
		' This Code is called by the SetFormData routine before position changes.
		'
		Dim RVal As Boolean

		RVal = True
		pReturnString = ""

		' Validate 

		If Me.editBookmarkDescription.Text.Length <= 0 Then
			pReturnString = "Bookmark Description must not be left blank."
			RVal = False
		End If

		Return RVal

	End Function

    ''' <summary>
    ''' Handles the MouseEnter event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnCancel_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseEnter
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = True
	End Sub

    ''' <summary>
    ''' Handles the MouseLeave event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnCancel_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseLeave
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = False
	End Sub


#End Region

#Region " Navigation Code / GetPosition() (Generic Code) "

    ''' <summary>
    ''' Handles the SelectComboChanged event of the Combo control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_SelectComboChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' Selection Combo. SelectedItem changed.
    '

    ' Don't react to changes made in paint routines etc.
    If InPaint = True Then Exit Sub

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    ' Find the correct data row, then show it...
    thisPosition = THIS_FORM_SelectingCombo.SelectedIndex

    If (thisPosition >= 0) Then
      thisDataRow = Me.SortedRows(thisPosition)
    Else
      thisDataRow = Nothing
    End If

    Call GetFormData(thisDataRow)

  End Sub


    ''' <summary>
    ''' Handles the Click event of the btnNavPrev control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnNavPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavPrev.Click
    ' 'Previous' Button.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    If thisPosition > 0 Then thisPosition -= 1
    If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
      thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
    End If

    THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

    Exit Sub

    If myTable.Rows.Count > thisPosition Then
      thisDataRow = myTable.Rows(thisPosition)
      Call GetFormData(thisDataRow)
    Else
      Call GetFormData(Nothing)
    End If

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnNavNext control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnNavNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavNext.Click
    ' 'Next' Button.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    If thisPosition < (myTable.Rows.Count - 1) Then thisPosition += 1
    If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
      thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
    End If

    THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

    Exit Sub

    If myTable.Rows.Count > thisPosition Then
      thisDataRow = myTable.Rows(thisPosition)
      Call GetFormData(thisDataRow)
    Else
      Call GetFormData(Nothing)
    End If

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnNavFirst control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnNavFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavFirst.Click
    ' 'First' Button.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    thisPosition = 0
    If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
      thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
    End If

    If (THIS_FORM_SelectingCombo.SelectedIndex = thisPosition) Then
      Combo_SelectComboChanged(THIS_FORM_SelectingCombo, New EventArgs)
    Else
      THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
    End If

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnLast control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLast.Click
    ' 'Last' Button.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    thisPosition = THIS_FORM_SelectingCombo.Items.Count - 1

    THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

  End Sub

    ''' <summary>
    ''' Get_s the position.
    ''' </summary>
    ''' <param name="pAuditID">The p audit ID.</param>
    ''' <returns>System.Int32.</returns>
  Private Function Get_Position(ByVal pAuditID As Integer) As Integer
    ' Returns the position in the current 'SortedRows' array of the entry corresponding to the given 
    ' AudidID.

    Dim MinIndex As Integer
    Dim MaxIndex As Integer
    Dim CurrentMin As Integer
    Dim CurrentMax As Integer
    Dim searchDataRow As DataRow

    Try
      ' SortedRows exists ?

      If SortedRows Is Nothing Then
        Return (-1)
        Exit Function
      End If

      ' Return (-1) if there are no rows in the 'SortedRows'

      If (SortedRows.GetLength(0) <= 0) Then
        Return (-1)
        Exit Function
      End If


      ' Use a modified Search moving outwards from the last returned value to locate the required value.
      ' Reflecting the fact that for the most part One looks for closely located records.

      MinIndex = 0
      MaxIndex = SortedRows.GetLength(0) - 1


      ' Check First and Last records (Incase 'First' or 'Last' was pressed).

      searchDataRow = SortedRows(MinIndex)

      If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(searchDataRow("AuditID")) = pAuditID) Then
        Return MinIndex
        Exit Function
      End If

      searchDataRow = SortedRows(MaxIndex)
      If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(searchDataRow("AuditID")) = pAuditID) Then
        Return MaxIndex
        Exit Function
      End If

      ' now search outwards from the last returned value.

      If (Me.thisPosition >= MinIndex) And (Me.thisPosition <= MaxIndex) Then
        CurrentMin = thisPosition
        CurrentMax = CurrentMin + 1
      Else
        CurrentMin = CInt((MinIndex + MaxIndex) / 2)
        CurrentMax = CurrentMin + 1
      End If

      While (CurrentMin >= MinIndex) Or (CurrentMax <= MaxIndex)
        If (CurrentMin >= MinIndex) Then
          searchDataRow = SortedRows(CurrentMin)

          If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(searchDataRow("AuditID")) = pAuditID) Then
            Return CurrentMin
            Exit Function
          End If

          CurrentMin -= 1
        End If

        If (CurrentMax <= MaxIndex) Then
          searchDataRow = SortedRows(CurrentMax)

          If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(searchDataRow("AuditID")) = pAuditID) Then
            Return CurrentMax
            Exit Function
          End If

          CurrentMax += 1
        End If

      End While

      Return (-1)
      Exit Function

    Catch ex As Exception
      MainForm.LogError(Me.Name & ", Get_Position()", LOG_LEVELS.Warning, ex.Message, "", ex.StackTrace, True)
      Return (-1)
      Exit Function
    End Try

  End Function

#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

    ''' <summary>
    ''' Handles the Click event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    ' Cancel Changes, redisplay form.

    FormChanged = False
    AddNewRecord = False

    Me.THIS_FORM_SelectingCombo.Enabled = True

    If (thisPosition >= 0) And (thisPosition < Me.SortedRows.GetLength(0)) Then
      thisDataRow = Me.SortedRows(thisPosition)
      Call GetFormData(thisDataRow)
    Else
      Call btnNavFirst_Click(Me, New System.EventArgs)
    End If

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnSave control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
    ' Save Changes, if any, without prompting.

    If (FormChanged = True) Then
      Call SetFormData(False)
    End If

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnDelete control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
    Dim Position As Integer
    Dim NextAuditID As Integer

    If (AddNewRecord = True) Then
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Sub
    End If

    ' No Appropriate Save permission :-

    If (Me.HasDeletePermission = False) Then
      MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "You do not have permission to Delete this record.", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Sub
    End If

    ' *************************************************************
    ' KnowledgeDate OK :-
    ' *************************************************************

    If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
      MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "Changes to Venice are not allowed when the Knowledgedate is not set to `NOW`", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Sub
    End If

    ' *************************************************************
    ' Confirm :-
    ' *************************************************************
    If MsgBox("Are you Sure ?", MsgBoxStyle.OKCancel, "Confirm Delete") = MsgBoxResult.Cancel Then
      Exit Sub
    End If

    ' Check Data position.

    Position = Get_Position(thisAuditID)

    If Position < 0 Then Exit Sub

    ' Check Referential Integrity 
    If CheckReferentialIntegrity(_MainForm, Me.SortedRows(Position)) = False Then
      MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "Deleting this record would voilate referential integrity.", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Sub
    End If

    ' Resolve row to show after deleting this one.

    NextAuditID = (-1)
    If (Position + 1) < Me.SortedRows.GetLength(0) Then
      NextAuditID = CInt(Me.SortedRows(Position + 1)("AuditID"))
    ElseIf (Position - 1) >= 0 Then
      NextAuditID = CInt(Me.SortedRows(Position - 1)("AuditID"))
    Else
      NextAuditID = (-1)
    End If

    ' Delete this row

    Try
      Me.SortedRows(Position).Delete()

      MainForm.AdaptorUpdate(Me.Name, myAdaptor, myTable)

    Catch ex As Exception

      Call MainForm.LogError(Me.Name & ", Delete()", LOG_LEVELS.Error, ex.Message, "Error Deleting Record.", ex.StackTrace, True)
    End Try

    ' Tidy Up.

    FormChanged = False

    thisAuditID = NextAuditID
		Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID), True)

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnAdd control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
    ' Prepare form to Add a new record.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    thisPosition = -1
    InPaint = True
    MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
    InPaint = False

    GetFormData(Nothing)
    AddNewRecord = True
    Me.btnCancel.Enabled = True
    Me.THIS_FORM_SelectingCombo.Enabled = False

    Call SetButtonStatus()

    THIS_FORM_NewMoveToControl.Focus()

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnClose control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
    ' Close Form

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    Me.Close()

  End Sub

#End Region

#Region " Bug hunting "

  ' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
  ' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
  ' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

    ''' <summary>
    ''' Called when [row updating].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

    ''' <summary>
    ''' Called when [row updated].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

    ''' <summary>
    ''' Called when [row fill error].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
    Dim a As Integer

    a = 1
  End Sub

#End Region


End Class
