' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmFees.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals


''' <summary>
''' Class frmFees
''' </summary>
Public Class frmFees

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

  ''' <summary>
  ''' Prevents a default instance of the <see cref="frmFees"/> class from being created.
  ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  ''' <summary>
  ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
  ''' </summary>
  ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  ''' <summary>
  ''' The components
  ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  ''' <summary>
  ''' The BTN add
  ''' </summary>
  Friend WithEvents btnAdd As System.Windows.Forms.Button
  ''' <summary>
  ''' The BTN delete
  ''' </summary>
  Friend WithEvents btnDelete As System.Windows.Forms.Button
  ''' <summary>
  ''' The BTN cancel
  ''' </summary>
  Friend WithEvents btnCancel As System.Windows.Forms.Button
  ''' <summary>
  ''' The BTN save
  ''' </summary>
  Friend WithEvents btnSave As System.Windows.Forms.Button
  ''' <summary>
  ''' The root menu
  ''' </summary>
  Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
  Friend WithEvents Combo_SelectFund As System.Windows.Forms.ComboBox
  Friend WithEvents label_CptyIsFund As System.Windows.Forms.Label
  Friend WithEvents Grid_Fees As C1.Win.C1FlexGrid.C1FlexGrid
  Friend WithEvents Combo_FundID As System.Windows.Forms.ComboBox
  Friend WithEvents Label1 As System.Windows.Forms.Label
  Friend WithEvents Combo_SubFund As System.Windows.Forms.ComboBox
  Friend WithEvents Label21 As System.Windows.Forms.Label
  Friend WithEvents Combo_SpecificShareClass As System.Windows.Forms.ComboBox
  Friend WithEvents Label_SpecificShareClass As System.Windows.Forms.Label
  Friend WithEvents edit_Description As System.Windows.Forms.TextBox
  Friend WithEvents lblTransaction As System.Windows.Forms.Label
  Friend WithEvents Combo_FeeInstrument As System.Windows.Forms.ComboBox
  Friend WithEvents Label2 As System.Windows.Forms.Label
  Friend WithEvents Combo_FeePeriodFrequency As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_FeePaymentFrequency As System.Windows.Forms.ComboBox
  Friend WithEvents Label_Amount As System.Windows.Forms.Label
  Friend WithEvents edit_FeeAmount As RenaissanceControls.NumericTextBox
  Friend WithEvents Radio_FeeAmount As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_FeePercentage As System.Windows.Forms.RadioButton
  Friend WithEvents Panel_PercentageValue As System.Windows.Forms.Panel
  Friend WithEvents Radio_PercentageBeforeFees As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_PercentageAfterSubscriptions As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_PercentagePreviousNAV As System.Windows.Forms.RadioButton
  Friend WithEvents Label4 As System.Windows.Forms.Label
  Friend WithEvents Numeric_DaysPerPeriod As RenaissanceControls.NumericTextBox
  Friend WithEvents Label5 As System.Windows.Forms.Label
  Friend WithEvents Numeric_SettlementTPlus As RenaissanceControls.NumericTextBox
  Friend WithEvents Check_UseFundFinancialYear As System.Windows.Forms.CheckBox
  Friend WithEvents Label6 As System.Windows.Forms.Label
  Friend WithEvents Label3 As System.Windows.Forms.Label
  Friend WithEvents Label7 As System.Windows.Forms.Label
  Friend WithEvents Label8 As System.Windows.Forms.Label
  Friend WithEvents Label9 As System.Windows.Forms.Label
  Friend WithEvents Label10 As System.Windows.Forms.Label
  Friend WithEvents Date_FeeStartDate As System.Windows.Forms.DateTimePicker
  Friend WithEvents Label_TradeDate As System.Windows.Forms.Label
  Friend WithEvents Date_FeeEndDate As System.Windows.Forms.DateTimePicker
  Friend WithEvents Label11 As System.Windows.Forms.Label
  Friend WithEvents Label12 As System.Windows.Forms.Label
  Friend WithEvents Label13 As System.Windows.Forms.Label
  Friend WithEvents Label14 As System.Windows.Forms.Label
  Friend WithEvents Combo_FeeSettlementFrequency As System.Windows.Forms.ComboBox
  Friend WithEvents Label15 As System.Windows.Forms.Label
  Friend WithEvents Label16 As System.Windows.Forms.Label
  Friend WithEvents Label17 As System.Windows.Forms.Label
  ''' <summary>
  ''' The BTN close
  ''' </summary>
  Friend WithEvents btnClose As System.Windows.Forms.Button
  ''' <summary>
  ''' Initializes the component.
  ''' </summary>
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.btnAdd = New System.Windows.Forms.Button
    Me.btnDelete = New System.Windows.Forms.Button
    Me.btnCancel = New System.Windows.Forms.Button
    Me.btnSave = New System.Windows.Forms.Button
    Me.btnClose = New System.Windows.Forms.Button
    Me.RootMenu = New System.Windows.Forms.MenuStrip
    Me.Panel1 = New System.Windows.Forms.Panel
    Me.Grid_Fees = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.Combo_SelectFund = New System.Windows.Forms.ComboBox
    Me.label_CptyIsFund = New System.Windows.Forms.Label
    Me.Combo_FundID = New System.Windows.Forms.ComboBox
    Me.Label1 = New System.Windows.Forms.Label
    Me.Combo_SubFund = New System.Windows.Forms.ComboBox
    Me.Label21 = New System.Windows.Forms.Label
    Me.Combo_SpecificShareClass = New System.Windows.Forms.ComboBox
    Me.Label_SpecificShareClass = New System.Windows.Forms.Label
    Me.edit_Description = New System.Windows.Forms.TextBox
    Me.lblTransaction = New System.Windows.Forms.Label
    Me.Combo_FeeInstrument = New System.Windows.Forms.ComboBox
    Me.Label2 = New System.Windows.Forms.Label
    Me.Combo_FeePeriodFrequency = New System.Windows.Forms.ComboBox
    Me.Combo_FeePaymentFrequency = New System.Windows.Forms.ComboBox
    Me.Label_Amount = New System.Windows.Forms.Label
    Me.edit_FeeAmount = New RenaissanceControls.NumericTextBox
    Me.Radio_FeeAmount = New System.Windows.Forms.RadioButton
    Me.Radio_FeePercentage = New System.Windows.Forms.RadioButton
    Me.Panel_PercentageValue = New System.Windows.Forms.Panel
    Me.Radio_PercentageBeforeFees = New System.Windows.Forms.RadioButton
    Me.Radio_PercentageAfterSubscriptions = New System.Windows.Forms.RadioButton
    Me.Radio_PercentagePreviousNAV = New System.Windows.Forms.RadioButton
    Me.Label4 = New System.Windows.Forms.Label
    Me.Numeric_DaysPerPeriod = New RenaissanceControls.NumericTextBox
    Me.Label5 = New System.Windows.Forms.Label
    Me.Numeric_SettlementTPlus = New RenaissanceControls.NumericTextBox
    Me.Check_UseFundFinancialYear = New System.Windows.Forms.CheckBox
    Me.Label6 = New System.Windows.Forms.Label
    Me.Label3 = New System.Windows.Forms.Label
    Me.Label7 = New System.Windows.Forms.Label
    Me.Label8 = New System.Windows.Forms.Label
    Me.Label9 = New System.Windows.Forms.Label
    Me.Label10 = New System.Windows.Forms.Label
    Me.Date_FeeStartDate = New System.Windows.Forms.DateTimePicker
    Me.Label_TradeDate = New System.Windows.Forms.Label
    Me.Date_FeeEndDate = New System.Windows.Forms.DateTimePicker
    Me.Label11 = New System.Windows.Forms.Label
    Me.Label12 = New System.Windows.Forms.Label
    Me.Label13 = New System.Windows.Forms.Label
    Me.Label14 = New System.Windows.Forms.Label
    Me.Combo_FeeSettlementFrequency = New System.Windows.Forms.ComboBox
    Me.Label15 = New System.Windows.Forms.Label
    Me.Label16 = New System.Windows.Forms.Label
    Me.Label17 = New System.Windows.Forms.Label
    Me.Panel1.SuspendLayout()
    CType(Me.Grid_Fees, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Panel_PercentageValue.SuspendLayout()
    Me.SuspendLayout()
    '
    'btnAdd
    '
    Me.btnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnAdd.Location = New System.Drawing.Point(505, 557)
    Me.btnAdd.Name = "btnAdd"
    Me.btnAdd.Size = New System.Drawing.Size(93, 35)
    Me.btnAdd.TabIndex = 18
    Me.btnAdd.Text = "&New"
    '
    'btnDelete
    '
    Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnDelete.Location = New System.Drawing.Point(723, 557)
    Me.btnDelete.Name = "btnDelete"
    Me.btnDelete.Size = New System.Drawing.Size(75, 35)
    Me.btnDelete.TabIndex = 20
    Me.btnDelete.Text = "&Delete"
    '
    'btnCancel
    '
    Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnCancel.Location = New System.Drawing.Point(804, 557)
    Me.btnCancel.Name = "btnCancel"
    Me.btnCancel.Size = New System.Drawing.Size(75, 35)
    Me.btnCancel.TabIndex = 21
    Me.btnCancel.Text = "&Cancel"
    '
    'btnSave
    '
    Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnSave.Location = New System.Drawing.Point(642, 557)
    Me.btnSave.Name = "btnSave"
    Me.btnSave.Size = New System.Drawing.Size(75, 35)
    Me.btnSave.TabIndex = 19
    Me.btnSave.Text = "&Save"
    '
    'btnClose
    '
    Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnClose.Location = New System.Drawing.Point(885, 557)
    Me.btnClose.Name = "btnClose"
    Me.btnClose.Size = New System.Drawing.Size(75, 35)
    Me.btnClose.TabIndex = 22
    Me.btnClose.Text = "&Close"
    '
    'RootMenu
    '
    Me.RootMenu.Location = New System.Drawing.Point(0, 0)
    Me.RootMenu.Name = "RootMenu"
    Me.RootMenu.Size = New System.Drawing.Size(983, 24)
    Me.RootMenu.TabIndex = 16
    Me.RootMenu.Text = " "
    '
    'Panel1
    '
    Me.Panel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Panel1.Controls.Add(Me.Grid_Fees)
    Me.Panel1.Controls.Add(Me.Combo_SelectFund)
    Me.Panel1.Controls.Add(Me.label_CptyIsFund)
    Me.Panel1.Location = New System.Drawing.Point(0, 0)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(983, 292)
    Me.Panel1.TabIndex = 0
    '
    'Grid_Fees
    '
    Me.Grid_Fees.AllowEditing = False
    Me.Grid_Fees.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_Fees.CausesValidation = False
    Me.Grid_Fees.ColumnInfo = "5,1,0,0,0,85,Columns:"
    Me.Grid_Fees.EditOptions = C1.Win.C1FlexGrid.EditFlags.None
    Me.Grid_Fees.KeyActionTab = C1.Win.C1FlexGrid.KeyActionEnum.MoveAcross
    Me.Grid_Fees.Location = New System.Drawing.Point(4, 30)
    Me.Grid_Fees.Name = "Grid_Fees"
    Me.Grid_Fees.Rows.DefaultSize = 17
    Me.Grid_Fees.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Row
    Me.Grid_Fees.Size = New System.Drawing.Size(972, 255)
    Me.Grid_Fees.TabIndex = 1
    '
    'Combo_SelectFund
    '
    Me.Combo_SelectFund.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_SelectFund.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectFund.Location = New System.Drawing.Point(117, 4)
    Me.Combo_SelectFund.Name = "Combo_SelectFund"
    Me.Combo_SelectFund.Size = New System.Drawing.Size(452, 21)
    Me.Combo_SelectFund.TabIndex = 0
    '
    'label_CptyIsFund
    '
    Me.label_CptyIsFund.Location = New System.Drawing.Point(5, 8)
    Me.label_CptyIsFund.Name = "label_CptyIsFund"
    Me.label_CptyIsFund.Size = New System.Drawing.Size(80, 16)
    Me.label_CptyIsFund.TabIndex = 2
    Me.label_CptyIsFund.Text = "Fund"
    '
    'Combo_FundID
    '
    Me.Combo_FundID.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Combo_FundID.Enabled = False
    Me.Combo_FundID.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_FundID.Location = New System.Drawing.Point(124, 328)
    Me.Combo_FundID.Name = "Combo_FundID"
    Me.Combo_FundID.Size = New System.Drawing.Size(382, 21)
    Me.Combo_FundID.TabIndex = 2
    '
    'Label1
    '
    Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label1.Location = New System.Drawing.Point(12, 332)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(92, 16)
    Me.Label1.TabIndex = 84
    Me.Label1.Text = "Fund"
    '
    'Combo_SubFund
    '
    Me.Combo_SubFund.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Combo_SubFund.Enabled = False
    Me.Combo_SubFund.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SubFund.Location = New System.Drawing.Point(124, 355)
    Me.Combo_SubFund.Name = "Combo_SubFund"
    Me.Combo_SubFund.Size = New System.Drawing.Size(382, 21)
    Me.Combo_SubFund.TabIndex = 3
    '
    'Label21
    '
    Me.Label21.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label21.Location = New System.Drawing.Point(12, 359)
    Me.Label21.Name = "Label21"
    Me.Label21.Size = New System.Drawing.Size(106, 16)
    Me.Label21.TabIndex = 215
    Me.Label21.Text = "Strategy (Sub Fund)"
    '
    'Combo_SpecificShareClass
    '
    Me.Combo_SpecificShareClass.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Combo_SpecificShareClass.Enabled = False
    Me.Combo_SpecificShareClass.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SpecificShareClass.Location = New System.Drawing.Point(124, 382)
    Me.Combo_SpecificShareClass.Name = "Combo_SpecificShareClass"
    Me.Combo_SpecificShareClass.Size = New System.Drawing.Size(382, 21)
    Me.Combo_SpecificShareClass.TabIndex = 4
    '
    'Label_SpecificShareClass
    '
    Me.Label_SpecificShareClass.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label_SpecificShareClass.Location = New System.Drawing.Point(12, 385)
    Me.Label_SpecificShareClass.Name = "Label_SpecificShareClass"
    Me.Label_SpecificShareClass.Size = New System.Drawing.Size(81, 16)
    Me.Label_SpecificShareClass.TabIndex = 222
    Me.Label_SpecificShareClass.Text = "Share Class"
    '
    'edit_Description
    '
    Me.edit_Description.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.edit_Description.Location = New System.Drawing.Point(124, 302)
    Me.edit_Description.Name = "edit_Description"
    Me.edit_Description.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.edit_Description.Size = New System.Drawing.Size(382, 20)
    Me.edit_Description.TabIndex = 1
    '
    'lblTransaction
    '
    Me.lblTransaction.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.lblTransaction.Location = New System.Drawing.Point(12, 305)
    Me.lblTransaction.Name = "lblTransaction"
    Me.lblTransaction.Size = New System.Drawing.Size(100, 20)
    Me.lblTransaction.TabIndex = 224
    Me.lblTransaction.Text = "Description"
    '
    'Combo_FeeInstrument
    '
    Me.Combo_FeeInstrument.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Combo_FeeInstrument.Enabled = False
    Me.Combo_FeeInstrument.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_FeeInstrument.Location = New System.Drawing.Point(124, 409)
    Me.Combo_FeeInstrument.Name = "Combo_FeeInstrument"
    Me.Combo_FeeInstrument.Size = New System.Drawing.Size(382, 21)
    Me.Combo_FeeInstrument.TabIndex = 5
    '
    'Label2
    '
    Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label2.Location = New System.Drawing.Point(12, 413)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(104, 16)
    Me.Label2.TabIndex = 226
    Me.Label2.Text = "Fee Instrument"
    '
    'Combo_FeePeriodFrequency
    '
    Me.Combo_FeePeriodFrequency.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Combo_FeePeriodFrequency.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_FeePeriodFrequency.Location = New System.Drawing.Point(124, 436)
    Me.Combo_FeePeriodFrequency.Name = "Combo_FeePeriodFrequency"
    Me.Combo_FeePeriodFrequency.Size = New System.Drawing.Size(131, 21)
    Me.Combo_FeePeriodFrequency.TabIndex = 6
    '
    'Combo_FeePaymentFrequency
    '
    Me.Combo_FeePaymentFrequency.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Combo_FeePaymentFrequency.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_FeePaymentFrequency.Location = New System.Drawing.Point(124, 512)
    Me.Combo_FeePaymentFrequency.Name = "Combo_FeePaymentFrequency"
    Me.Combo_FeePaymentFrequency.Size = New System.Drawing.Size(131, 21)
    Me.Combo_FeePaymentFrequency.TabIndex = 9
    '
    'Label_Amount
    '
    Me.Label_Amount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label_Amount.Location = New System.Drawing.Point(546, 306)
    Me.Label_Amount.Name = "Label_Amount"
    Me.Label_Amount.Size = New System.Drawing.Size(68, 16)
    Me.Label_Amount.TabIndex = 232
    Me.Label_Amount.Text = "Fee Amount"
    '
    'edit_FeeAmount
    '
    Me.edit_FeeAmount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.edit_FeeAmount.Location = New System.Drawing.Point(658, 302)
    Me.edit_FeeAmount.Name = "edit_FeeAmount"
    Me.edit_FeeAmount.RenaissanceTag = Nothing
    Me.edit_FeeAmount.SelectTextOnFocus = True
    Me.edit_FeeAmount.Size = New System.Drawing.Size(140, 20)
    Me.edit_FeeAmount.TabIndex = 12
    Me.edit_FeeAmount.Text = "0.00"
    Me.edit_FeeAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.edit_FeeAmount.TextFormat = "#,##0.00"
    Me.edit_FeeAmount.Value = 0
    '
    'Radio_FeeAmount
    '
    Me.Radio_FeeAmount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Radio_FeeAmount.AutoSize = True
    Me.Radio_FeeAmount.Checked = True
    Me.Radio_FeeAmount.Location = New System.Drawing.Point(662, 332)
    Me.Radio_FeeAmount.Name = "Radio_FeeAmount"
    Me.Radio_FeeAmount.Size = New System.Drawing.Size(79, 17)
    Me.Radio_FeeAmount.TabIndex = 13
    Me.Radio_FeeAmount.TabStop = True
    Me.Radio_FeeAmount.Text = "Cash Value"
    Me.Radio_FeeAmount.UseVisualStyleBackColor = True
    '
    'Radio_FeePercentage
    '
    Me.Radio_FeePercentage.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Radio_FeePercentage.AutoSize = True
    Me.Radio_FeePercentage.Location = New System.Drawing.Point(814, 332)
    Me.Radio_FeePercentage.Name = "Radio_FeePercentage"
    Me.Radio_FeePercentage.Size = New System.Drawing.Size(80, 17)
    Me.Radio_FeePercentage.TabIndex = 14
    Me.Radio_FeePercentage.Text = "Percentage"
    Me.Radio_FeePercentage.UseVisualStyleBackColor = True
    '
    'Panel_PercentageValue
    '
    Me.Panel_PercentageValue.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Panel_PercentageValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Panel_PercentageValue.Controls.Add(Me.Radio_PercentageBeforeFees)
    Me.Panel_PercentageValue.Controls.Add(Me.Radio_PercentageAfterSubscriptions)
    Me.Panel_PercentageValue.Controls.Add(Me.Radio_PercentagePreviousNAV)
    Me.Panel_PercentageValue.Location = New System.Drawing.Point(808, 355)
    Me.Panel_PercentageValue.Name = "Panel_PercentageValue"
    Me.Panel_PercentageValue.Size = New System.Drawing.Size(160, 75)
    Me.Panel_PercentageValue.TabIndex = 15
    Me.Panel_PercentageValue.Visible = False
    '
    'Radio_PercentageBeforeFees
    '
    Me.Radio_PercentageBeforeFees.AutoSize = True
    Me.Radio_PercentageBeforeFees.Location = New System.Drawing.Point(5, 47)
    Me.Radio_PercentageBeforeFees.Name = "Radio_PercentageBeforeFees"
    Me.Radio_PercentageBeforeFees.Size = New System.Drawing.Size(155, 17)
    Me.Radio_PercentageBeforeFees.TabIndex = 2
    Me.Radio_PercentageBeforeFees.TabStop = True
    Me.Radio_PercentageBeforeFees.Text = "of NAV before Mgmt && Perf."
    Me.Radio_PercentageBeforeFees.UseVisualStyleBackColor = True
    Me.Radio_PercentageBeforeFees.Visible = False
    '
    'Radio_PercentageAfterSubscriptions
    '
    Me.Radio_PercentageAfterSubscriptions.AutoSize = True
    Me.Radio_PercentageAfterSubscriptions.Location = New System.Drawing.Point(5, 26)
    Me.Radio_PercentageAfterSubscriptions.Name = "Radio_PercentageAfterSubscriptions"
    Me.Radio_PercentageAfterSubscriptions.Size = New System.Drawing.Size(149, 17)
    Me.Radio_PercentageAfterSubscriptions.TabIndex = 1
    Me.Radio_PercentageAfterSubscriptions.TabStop = True
    Me.Radio_PercentageAfterSubscriptions.Text = "of NAV after Subscriptions"
    Me.Radio_PercentageAfterSubscriptions.UseVisualStyleBackColor = True
    '
    'Radio_PercentagePreviousNAV
    '
    Me.Radio_PercentagePreviousNAV.AutoSize = True
    Me.Radio_PercentagePreviousNAV.Checked = True
    Me.Radio_PercentagePreviousNAV.Location = New System.Drawing.Point(5, 5)
    Me.Radio_PercentagePreviousNAV.Name = "Radio_PercentagePreviousNAV"
    Me.Radio_PercentagePreviousNAV.Size = New System.Drawing.Size(140, 17)
    Me.Radio_PercentagePreviousNAV.TabIndex = 0
    Me.Radio_PercentagePreviousNAV.TabStop = True
    Me.Radio_PercentagePreviousNAV.Text = "of Previous Closing NAV"
    Me.Radio_PercentagePreviousNAV.UseVisualStyleBackColor = True
    '
    'Label4
    '
    Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label4.Location = New System.Drawing.Point(12, 489)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(104, 16)
    Me.Label4.TabIndex = 238
    Me.Label4.Text = "Days per period"
    '
    'Numeric_DaysPerPeriod
    '
    Me.Numeric_DaysPerPeriod.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Numeric_DaysPerPeriod.Location = New System.Drawing.Point(124, 486)
    Me.Numeric_DaysPerPeriod.Name = "Numeric_DaysPerPeriod"
    Me.Numeric_DaysPerPeriod.RenaissanceTag = Nothing
    Me.Numeric_DaysPerPeriod.SelectTextOnFocus = True
    Me.Numeric_DaysPerPeriod.Size = New System.Drawing.Size(131, 20)
    Me.Numeric_DaysPerPeriod.TabIndex = 8
    Me.Numeric_DaysPerPeriod.Text = "0"
    Me.Numeric_DaysPerPeriod.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.Numeric_DaysPerPeriod.TextFormat = "#,##0"
    Me.Numeric_DaysPerPeriod.Value = 0
    '
    'Label5
    '
    Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label5.Location = New System.Drawing.Point(12, 569)
    Me.Label5.Name = "Label5"
    Me.Label5.Size = New System.Drawing.Size(104, 16)
    Me.Label5.TabIndex = 240
    Me.Label5.Text = "Settlement `T+`"
    '
    'Numeric_SettlementTPlus
    '
    Me.Numeric_SettlementTPlus.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Numeric_SettlementTPlus.Location = New System.Drawing.Point(124, 566)
    Me.Numeric_SettlementTPlus.Name = "Numeric_SettlementTPlus"
    Me.Numeric_SettlementTPlus.RenaissanceTag = Nothing
    Me.Numeric_SettlementTPlus.SelectTextOnFocus = True
    Me.Numeric_SettlementTPlus.Size = New System.Drawing.Size(131, 20)
    Me.Numeric_SettlementTPlus.TabIndex = 11
    Me.Numeric_SettlementTPlus.Text = "0"
    Me.Numeric_SettlementTPlus.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.Numeric_SettlementTPlus.TextFormat = "#,##0"
    Me.Numeric_SettlementTPlus.Value = 0
    '
    'Check_UseFundFinancialYear
    '
    Me.Check_UseFundFinancialYear.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Check_UseFundFinancialYear.AutoSize = True
    Me.Check_UseFundFinancialYear.Location = New System.Drawing.Point(124, 463)
    Me.Check_UseFundFinancialYear.Name = "Check_UseFundFinancialYear"
    Me.Check_UseFundFinancialYear.Size = New System.Drawing.Size(137, 17)
    Me.Check_UseFundFinancialYear.TabIndex = 7
    Me.Check_UseFundFinancialYear.Text = "Use fund Financial year"
    Me.Check_UseFundFinancialYear.UseVisualStyleBackColor = True
    '
    'Label6
    '
    Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label6.Location = New System.Drawing.Point(12, 439)
    Me.Label6.Name = "Label6"
    Me.Label6.Size = New System.Drawing.Size(104, 16)
    Me.Label6.TabIndex = 241
    Me.Label6.Text = "Fee Period"
    '
    'Label3
    '
    Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label3.Location = New System.Drawing.Point(12, 515)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(104, 16)
    Me.Label3.TabIndex = 242
    Me.Label3.Text = "Payment frequency"
    '
    'Label7
    '
    Me.Label7.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label7.Location = New System.Drawing.Point(261, 439)
    Me.Label7.Name = "Label7"
    Me.Label7.Size = New System.Drawing.Size(245, 16)
    Me.Label7.TabIndex = 243
    Me.Label7.Text = "Period over which the Fee Amount is charged."
    '
    'Label8
    '
    Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label8.Location = New System.Drawing.Point(261, 489)
    Me.Label8.Name = "Label8"
    Me.Label8.Size = New System.Drawing.Size(245, 16)
    Me.Label8.TabIndex = 244
    Me.Label8.Text = "Divisor for fee amount. (0 = Actual)"
    '
    'Label9
    '
    Me.Label9.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label9.Location = New System.Drawing.Point(261, 515)
    Me.Label9.Name = "Label9"
    Me.Label9.Size = New System.Drawing.Size(245, 16)
    Me.Label9.TabIndex = 245
    Me.Label9.Text = "How often the fee is accrued."
    '
    'Label10
    '
    Me.Label10.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label10.Location = New System.Drawing.Point(261, 569)
    Me.Label10.Name = "Label10"
    Me.Label10.Size = New System.Drawing.Size(245, 16)
    Me.Label10.TabIndex = 246
    Me.Label10.Text = "Settlement delay (Business Days)."
    '
    'Date_FeeStartDate
    '
    Me.Date_FeeStartDate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Date_FeeStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
    Me.Date_FeeStartDate.Location = New System.Drawing.Point(662, 463)
    Me.Date_FeeStartDate.Name = "Date_FeeStartDate"
    Me.Date_FeeStartDate.Size = New System.Drawing.Size(136, 20)
    Me.Date_FeeStartDate.TabIndex = 16
    '
    'Label_TradeDate
    '
    Me.Label_TradeDate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label_TradeDate.Location = New System.Drawing.Point(546, 464)
    Me.Label_TradeDate.Name = "Label_TradeDate"
    Me.Label_TradeDate.Size = New System.Drawing.Size(84, 16)
    Me.Label_TradeDate.TabIndex = 248
    Me.Label_TradeDate.Text = "Fee Start Date"
    '
    'Date_FeeEndDate
    '
    Me.Date_FeeEndDate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Date_FeeEndDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
    Me.Date_FeeEndDate.Location = New System.Drawing.Point(662, 486)
    Me.Date_FeeEndDate.Name = "Date_FeeEndDate"
    Me.Date_FeeEndDate.Size = New System.Drawing.Size(136, 20)
    Me.Date_FeeEndDate.TabIndex = 17
    '
    'Label11
    '
    Me.Label11.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label11.Location = New System.Drawing.Point(546, 487)
    Me.Label11.Name = "Label11"
    Me.Label11.Size = New System.Drawing.Size(84, 16)
    Me.Label11.TabIndex = 250
    Me.Label11.Text = "Fee End Date"
    '
    'Label12
    '
    Me.Label12.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label12.Location = New System.Drawing.Point(804, 463)
    Me.Label12.Name = "Label12"
    Me.Label12.Size = New System.Drawing.Size(166, 43)
    Me.Label12.TabIndex = 251
    Me.Label12.Text = "Dates between which this fee will be charged."
    '
    'Label13
    '
    Me.Label13.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label13.Location = New System.Drawing.Point(261, 542)
    Me.Label13.Name = "Label13"
    Me.Label13.Size = New System.Drawing.Size(245, 16)
    Me.Label13.TabIndex = 254
    Me.Label13.Text = "How often the fee is settled."
    '
    'Label14
    '
    Me.Label14.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label14.Location = New System.Drawing.Point(12, 542)
    Me.Label14.Name = "Label14"
    Me.Label14.Size = New System.Drawing.Size(104, 16)
    Me.Label14.TabIndex = 253
    Me.Label14.Text = "Settlement frequency"
    '
    'Combo_FeeSettlementFrequency
    '
    Me.Combo_FeeSettlementFrequency.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Combo_FeeSettlementFrequency.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_FeeSettlementFrequency.Location = New System.Drawing.Point(124, 539)
    Me.Combo_FeeSettlementFrequency.Name = "Combo_FeeSettlementFrequency"
    Me.Combo_FeeSettlementFrequency.Size = New System.Drawing.Size(131, 21)
    Me.Combo_FeeSettlementFrequency.TabIndex = 10
    '
    'Label15
    '
    Me.Label15.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label15.Location = New System.Drawing.Point(508, 384)
    Me.Label15.Name = "Label15"
    Me.Label15.Size = New System.Drawing.Size(165, 16)
    Me.Label15.TabIndex = 255
    Me.Label15.Text = "Fee for specific share class."
    '
    'Label16
    '
    Me.Label16.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label16.Location = New System.Drawing.Point(508, 358)
    Me.Label16.Name = "Label16"
    Me.Label16.Size = New System.Drawing.Size(124, 16)
    Me.Label16.TabIndex = 256
    Me.Label16.Text = "Strategy to book to."
    '
    'Label17
    '
    Me.Label17.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label17.Location = New System.Drawing.Point(508, 412)
    Me.Label17.Name = "Label17"
    Me.Label17.Size = New System.Drawing.Size(181, 16)
    Me.Label17.TabIndex = 257
    Me.Label17.Text = "Expense instrument to use."
    '
    'frmFees
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.CancelButton = Me.btnCancel
    Me.ClientSize = New System.Drawing.Size(983, 604)
    Me.Controls.Add(Me.Label17)
    Me.Controls.Add(Me.Label16)
    Me.Controls.Add(Me.Label15)
    Me.Controls.Add(Me.Label13)
    Me.Controls.Add(Me.Label14)
    Me.Controls.Add(Me.Combo_FeeSettlementFrequency)
    Me.Controls.Add(Me.Label12)
    Me.Controls.Add(Me.Date_FeeEndDate)
    Me.Controls.Add(Me.Label11)
    Me.Controls.Add(Me.Date_FeeStartDate)
    Me.Controls.Add(Me.Label_TradeDate)
    Me.Controls.Add(Me.Label10)
    Me.Controls.Add(Me.Label9)
    Me.Controls.Add(Me.Label8)
    Me.Controls.Add(Me.Label7)
    Me.Controls.Add(Me.Label3)
    Me.Controls.Add(Me.Label6)
    Me.Controls.Add(Me.Check_UseFundFinancialYear)
    Me.Controls.Add(Me.Label5)
    Me.Controls.Add(Me.Numeric_SettlementTPlus)
    Me.Controls.Add(Me.Label4)
    Me.Controls.Add(Me.Numeric_DaysPerPeriod)
    Me.Controls.Add(Me.Panel_PercentageValue)
    Me.Controls.Add(Me.Radio_FeePercentage)
    Me.Controls.Add(Me.Radio_FeeAmount)
    Me.Controls.Add(Me.Label_Amount)
    Me.Controls.Add(Me.edit_FeeAmount)
    Me.Controls.Add(Me.Combo_FeePaymentFrequency)
    Me.Controls.Add(Me.Combo_FeePeriodFrequency)
    Me.Controls.Add(Me.Combo_FeeInstrument)
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.edit_Description)
    Me.Controls.Add(Me.lblTransaction)
    Me.Controls.Add(Me.Combo_SpecificShareClass)
    Me.Controls.Add(Me.Label_SpecificShareClass)
    Me.Controls.Add(Me.Combo_SubFund)
    Me.Controls.Add(Me.Label21)
    Me.Controls.Add(Me.Combo_FundID)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.Panel1)
    Me.Controls.Add(Me.RootMenu)
    Me.Controls.Add(Me.btnClose)
    Me.Controls.Add(Me.btnSave)
    Me.Controls.Add(Me.btnAdd)
    Me.Controls.Add(Me.btnDelete)
    Me.Controls.Add(Me.btnCancel)
    Me.MinimumSize = New System.Drawing.Size(999, 500)
    Me.Name = "frmFees"
    Me.Text = "Add/Edit Fee"
    Me.Panel1.ResumeLayout(False)
    CType(Me.Grid_Fees, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Panel_PercentageValue.ResumeLayout(False)
    Me.Panel_PercentageValue.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "


  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
  ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
  ''' </summary>
  Private WithEvents _MainForm As VeniceMain

  ' Form ToolTip
  ''' <summary>
  ''' Control to provide tooltip functionality.
  ''' </summary>
  Private FormTooltip As New ToolTip()

  ' Form Menu


  ' Form Constants, specific to the table being updated.

  ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
  ''' </summary>
  Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

  ' Form Locals, initialised on 'New' defining what standard data items to use
  ''' <summary>
  ''' Variable holding the form's data table name. taken from the "ThisStandardDataset" object.
  ''' </summary>
  Private THIS_TABLENAME As String
  ''' <summary>
  ''' Variable holding the form's data adaptor name. taken from the "ThisStandardDataset" object.
  ''' </summary>
  Private THIS_ADAPTORNAME As String
  ''' <summary>
  ''' Variable holding the form's dataset name. taken from the "ThisStandardDataset" object.
  ''' </summary>
  Private THIS_DATASETNAME As String


  ' The standard ChangeID for this form. e.g. tblFee
  ''' <summary>
  ''' The standard ChangeID for this form.
  ''' </summary>
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

  ''' <summary>
  ''' Control to select after proessing the "New" button.
  ''' </summary>
  Private THIS_FORM_NewMoveToControl As Control

  ' Form Specific Order fields
  ''' <summary>
  ''' Field Name to show in the Select combo.
  ''' </summary>
  Private THIS_FORM_SelectBy As String
  ''' <summary>
  ''' Field Name to order the Select combo by.
  ''' </summary>
  Private THIS_FORM_OrderBy As String

  ''' <summary>
  ''' The field name that holds the unique identifier for each entry. e.g. FundID for the tblFund table.
  ''' </summary>
  Private THIS_FORM_ValueMember As String

  ' Form specific Permissioning variables
  ''' <summary>
  ''' The THI s_ FOR m_ permission area
  ''' </summary>
  Private THIS_FORM_PermissionArea As String
  ''' <summary>
  ''' The THI s_ FOR m_ permission type
  ''' </summary>
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
  ''' <summary>
  ''' The THI s_ FOR m_ form ID
  ''' </summary>
  Private THIS_FORM_FormID As VeniceFormID

  ' Data Structures

  ''' <summary>
  ''' My dataset
  ''' </summary>
  Private myDataset As RenaissanceDataClass.DSFeeDefinitions     ' Form Specific !!!!
  ''' <summary>
  ''' My table
  ''' </summary>
  Private myTable As RenaissanceDataClass.DSFeeDefinitions.tblFeeDefinitionsDataTable
  ''' <summary>
  ''' My connection
  ''' </summary>
  Private myConnection As SqlConnection
  ''' <summary>
  ''' My adaptor
  ''' </summary>
  Private myAdaptor As SqlDataAdapter

  ''' <summary>
  ''' The this standard dataset
  ''' </summary>
  Private ThisStandardDataset As RenaissanceGlobals.StandardDataset
  Private FeesDataView As DataView

  Private FeesDisplayTable As DataTable = Nothing

  ' Active Element.

  ''' <summary>
  ''' The sorted rows
  ''' </summary>
  Private SortedRows() As DataRow
  ''' <summary>
  ''' The select by sorted rows
  ''' </summary>
  Private SelectBySortedRows() As DataRow
  ''' <summary>
  ''' The this data row
  ''' </summary>
  Private thisDataRow As RenaissanceDataClass.DSFeeDefinitions.tblFeeDefinitionsRow    ' Form Specific !!!!
  ''' <summary>
  ''' The this audit ID
  ''' </summary>
  Private thisAuditID As Integer
  ''' <summary>
  ''' The this position
  ''' </summary>
  Private thisPosition As Integer
  ''' <summary>
  ''' The _ is over cancel button
  ''' </summary>
  Private _IsOverCancelButton As Boolean
  ''' <summary>
  ''' The _ in use
  ''' </summary>
  Private _InUse As Boolean

  ' Form Status Flags

  ''' <summary>
  ''' The form is valid
  ''' </summary>
  Private FormIsValid As Boolean
  ''' <summary>
  ''' The form changed
  ''' </summary>
  Private FormChanged As Boolean
  ''' <summary>
  ''' The _ form open failed
  ''' </summary>
  Private _FormOpenFailed As Boolean
  ''' <summary>
  ''' The in paint
  ''' </summary>
  Private InPaint As Boolean
  ''' <summary>
  ''' The add new record
  ''' </summary>
  Private AddNewRecord As Boolean

  ' User Permission Flags

  ''' <summary>
  ''' The has read permission
  ''' </summary>
  Private HasReadPermission As Boolean
  ''' <summary>
  ''' The has update permission
  ''' </summary>
  Private HasUpdatePermission As Boolean
  ''' <summary>
  ''' The has insert permission
  ''' </summary>
  Private HasInsertPermission As Boolean
  ''' <summary>
  ''' The has delete permission
  ''' </summary>
  Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

  ''' <summary>
  ''' Gets the main form.
  ''' </summary>
  ''' <value>The main form.</value>
  Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
    ' Public property to return handle to the 'Main' Venice form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

  ''' <summary>
  ''' Gets or sets a value indicating whether this instance is over cancel button.
  ''' </summary>
  ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
  Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
    ' Public property maintaining a value indicating if the cursor is over the 'Cancel'
    ' Button on this form.
    ' This property is specifically designed for use by the field formating Event functions
    ' In order that they do not impose format restrictions if the user is about to click the 
    ' 'Cancel' button.
    '
    Get
      Return _IsOverCancelButton
    End Get
    Set(ByVal Value As Boolean)
      _IsOverCancelButton = Value
    End Set
  End Property

  ''' <summary>
  ''' Gets a value indicating whether this instance is in paint.
  ''' </summary>
  ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
  Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
    Get
      Return InPaint
    End Get
  End Property

  ''' <summary>
  ''' Gets a value indicating whether [in use].
  ''' </summary>
  ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
    Get
      Return _InUse
    End Get
  End Property

  ''' <summary>
  ''' Gets a value indicating whether [form open failed].
  ''' </summary>
  ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

#End Region

  ''' <summary>
  ''' Initializes a new instance of the <see cref="frmFees"/> class.
  ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  Public Sub New(ByVal pMainForm As VeniceMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()

    _MainForm = pMainForm
    AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

    _FormOpenFailed = False
    _InUse = True

    ' ******************************************************
    ' Form Specific Settings :
    ' ******************************************************

    THIS_FORM_NewMoveToControl = edit_Description

    ' Default Select and Order fields.

    THIS_FORM_SelectBy = "Description"
    THIS_FORM_OrderBy = "Description"

    THIS_FORM_ValueMember = "FeeID"

    ' Form Permissioning :-

    THIS_FORM_PermissionArea = Me.Name
    THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

    ' 'This' form ID

    THIS_FORM_FormID = VeniceFormID.frmFees

    ' This form's dataset type.

    ThisStandardDataset = RenaissanceStandardDatasets.tblFeeDefinitions ' This Defines the Form Data !!! 

    ' Format Event Handlers for form controls

    AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

    ' Form Control Changed events

    AddHandler edit_Description.TextChanged, AddressOf FormControlChanged

    AddHandler Combo_FundID.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_FundID.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_FundID.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_FundID.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_FundID.SelectedValueChanged, AddressOf Me.FormControlChanged

    AddHandler Combo_SubFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_SubFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_SubFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_SubFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_SubFund.SelectedValueChanged, AddressOf Me.FormControlChanged

    AddHandler Combo_SpecificShareClass.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_SpecificShareClass.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_SpecificShareClass.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_SpecificShareClass.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_SpecificShareClass.SelectedValueChanged, AddressOf Me.FormControlChanged

    AddHandler Combo_FeeInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_FeeInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_FeeInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_FeeInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_FeeInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged

    AddHandler Combo_FeePeriodFrequency.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_FeePeriodFrequency.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_FeePeriodFrequency.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_FeePeriodFrequency.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_FeePeriodFrequency.SelectedValueChanged, AddressOf Me.FormControlChanged

    AddHandler Combo_FeePaymentFrequency.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_FeePaymentFrequency.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_FeePaymentFrequency.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_FeePaymentFrequency.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_FeePaymentFrequency.SelectedValueChanged, AddressOf Me.FormControlChanged

    AddHandler Combo_FeeSettlementFrequency.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_FeeSettlementFrequency.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_FeeSettlementFrequency.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_FeeSettlementFrequency.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_FeeSettlementFrequency.SelectedValueChanged, AddressOf Me.FormControlChanged

    AddHandler Check_UseFundFinancialYear.CheckedChanged, AddressOf Me.FormControlChanged
    AddHandler Numeric_DaysPerPeriod.ValueChanged, AddressOf Me.FormControlChanged
    AddHandler Numeric_SettlementTPlus.ValueChanged, AddressOf Me.FormControlChanged
    AddHandler edit_FeeAmount.ValueChanged, AddressOf FormControlChanged
    AddHandler Radio_FeeAmount.CheckedChanged, AddressOf FormControlChanged
    AddHandler Radio_FeePercentage.CheckedChanged, AddressOf FormControlChanged
    AddHandler Radio_PercentagePreviousNAV.CheckedChanged, AddressOf FormControlChanged
    AddHandler Radio_PercentageAfterSubscriptions.CheckedChanged, AddressOf FormControlChanged
    AddHandler Radio_PercentageBeforeFees.CheckedChanged, AddressOf FormControlChanged
    AddHandler Date_FeeStartDate.ValueChanged, AddressOf FormControlChanged
    AddHandler Date_FeeEndDate.ValueChanged, AddressOf FormControlChanged

    ' Set up the ToolTip
    MainForm.SetFormToolTip(Me, FormTooltip)

    ' ******************************************************
    ' End Form Specific.
    ' ******************************************************

    ' Data object names standard to this Form type.

    THIS_TABLENAME = ThisStandardDataset.TableName
    THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
    THIS_DATASETNAME = ThisStandardDataset.DatasetName

    THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

    ' Establish / Retrieve data objects for this form.

    myConnection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)
    myAdaptor = MainForm.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, VENICE_CONNECTION, THIS_TABLENAME)
    myDataset = MainForm.Load_Table(ThisStandardDataset, False)
    myTable = myDataset.Tables(0)

    MainForm.BuildStandardFormMenu(Me.RootMenu, myDataset.Tables(0), AddressOf SelectMenuEvent, AddressOf OrderMenuEvent, AddressOf AuditReportMenuEvent)

    ' ******************************************************'
    '
    ' ******************************************************
    Dim thisCol As C1.Win.C1FlexGrid.Column

    Try
      If (FeesDisplayTable Is Nothing) Then
        FeesDisplayTable = New DataTable

        FeesDisplayTable.Columns.Add(New DataColumn("FeeID", GetType(Integer)))
        FeesDisplayTable.Columns.Add(New DataColumn("Fund", GetType(String)))
        FeesDisplayTable.Columns.Add(New DataColumn("FundID", GetType(Integer)))
        FeesDisplayTable.Columns.Add(New DataColumn("SubFund", GetType(String)))
        FeesDisplayTable.Columns.Add(New DataColumn("ShareClass", GetType(String)))
        FeesDisplayTable.Columns.Add(New DataColumn("Description", GetType(String)))
        FeesDisplayTable.Columns.Add(New DataColumn("Instrument", GetType(String)))
        FeesDisplayTable.Columns.Add(New DataColumn("Period", GetType(String)))
        FeesDisplayTable.Columns.Add(New DataColumn("Value", GetType(String)))
        FeesDisplayTable.Columns.Add(New DataColumn("Details", GetType(String)))

      End If

      FeesDataView = New DataView(FeesDisplayTable, "True", "Fund, Description", DataViewRowState.CurrentRows)
      Grid_Fees.AutoGenerateColumns = True
      Grid_Fees.DataSource = FeesDataView
      Grid_Fees.AutoGenerateColumns = False

      ' Format Transactions Grid, hide unwanted columns.

      Grid_Fees.Cols("FeeID").Move(1)
      Grid_Fees.Cols("Fund").Move(2)
      Grid_Fees.Cols("Description").Move(3)
      Grid_Fees.Cols("Instrument").Move(4)
      Grid_Fees.Cols("Period").Move(5)
      Grid_Fees.Cols("Value").Move(6)
      Grid_Fees.Cols("Details").Move(7)
      Grid_Fees.Cols("SubFund").Move(8)
      Grid_Fees.Cols("ShareClass").Move(9)
      Grid_Fees.Cols("FundID").Move(10)
    Catch ex As Exception
    End Try

    For Each thisCol In Grid_Fees.Cols
      Try
        Select Case thisCol.Name
          Case "FeeID"
            thisCol.Visible = False
          Case "FundID"
            thisCol.Visible = False
          Case "Fund"
            thisCol.Width = 250
            thisCol.Caption = "Fund Name"
          Case "SubFund"
            thisCol.Width = 100
            thisCol.Caption = "Sub Fund"
          Case "ShareClass"
            thisCol.Width = 100
            thisCol.Caption = "Share Class"
          Case "Description"
            thisCol.Width = 300
          Case "Instrument"
            thisCol.Width = 200
            thisCol.Caption = "Fee Instrument"
          Case "Period"
            thisCol.Width = 75
            thisCol.Caption = "Fee Period"
          Case "Value"
            thisCol.Format = "#,##0.0000"
            thisCol.Width = 75
          Case "Details"
            thisCol.Width = 450
          Case Else
            thisCol.Visible = False

            Try
              If thisCol.DataType Is GetType(Date) Then
                thisCol.Format = DISPLAYMEMBER_DATEFORMAT
              ElseIf thisCol.DataType Is GetType(Integer) Then
                thisCol.Format = "#,##0"
              ElseIf thisCol.DataType Is GetType(Double) Then
                thisCol.Format = "#,##0.0000"
              End If
            Catch ex As Exception
            End Try
        End Select
      Catch ex As Exception
      End Try
    Next
  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
  ''' <summary>
  ''' Resets the form.
  ''' </summary>
  Public Sub ResetForm() Implements StandardVeniceForm.ResetForm
    ' ********************************************************************************
    '
    '
    ' ********************************************************************************

    THIS_FORM_SelectBy = "Description"
    THIS_FORM_OrderBy = "Description"

    Call Form_Load(Me, New System.EventArgs)
  End Sub

  ''' <summary>
  ''' Closes the form.
  ''' </summary>
  Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
    ' ********************************************************************************
    '
    '
    ' ********************************************************************************

    ALWAYS_CLOSE_THIS_FORM = True
    Me.Close()
  End Sub

  ''' <summary>
  ''' Handles the Load event of the Form control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    ' ********************************************************************************
    '
    '
    ' ********************************************************************************

    Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
    _FormOpenFailed = False
    _InUse = True

    Try

      ' Initialise Data structures. Connection, Adaptor and Dataset.

      If Not (MainForm Is Nothing) Then
        FormIsValid = True
      Else
        MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        FormIsValid = False
        _FormOpenFailed = True
        Exit Sub
      End If

      If (myConnection Is Nothing) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

        FormIsValid = False
        _FormOpenFailed = True
        Exit Sub
      End If

      If (myAdaptor Is Nothing) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

        FormIsValid = False
        _FormOpenFailed = True
        Exit Sub
      End If

      If (myDataset Is Nothing) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

        FormIsValid = False
        _FormOpenFailed = True
        Exit Sub
      End If

    Catch ex As Exception
    End Try

    Try

      ' Initialse form

      InPaint = True
      IsOverCancelButton = False

      ' Check User permissions
      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

        FormIsValid = False
        _FormOpenFailed = True
        Exit Sub
      End If

      ' Build Sorted data list from which this form operates

      Call SetSortedRows()

      Call SetFundCombos()
      Call SetInstrumentCombo()
      Call SetSpecificShareClassCombo()
      Call SetSubFundCombo()
      Call SetDealingPeriodCombos()

      Call SetFeesDisplayTable(0)

      ' Display initial record.
      Call GetFormData(Nothing)

      Grid_Fees.Select(0, 0, False)

    Catch ex As Exception

      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Initialising Form (Form.Load).", ex.StackTrace, True)

    Finally

      InPaint = False

    End Try

  End Sub

  ''' <summary>
  ''' Handles the Closing event of the frmFees control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
  Private Sub frmFees_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    ' ********************************************************************************
    '
    '
    ' ********************************************************************************

    Dim HideForm As Boolean

    ' Hide or Close this form ?
    ' All depends on how many of this form type are Open or in Cache...

    _InUse = False

    If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
      HideForm = False
    Else
      If (FormChanged = True) Then
        Call SetFormData()
      End If

      HideForm = True
      If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
        HideForm = False
      End If
    End If

    If HideForm = True Then
      MainForm.HideInFormsCollection(Me)
      Me.Hide() ' NPP Fix

      e.Cancel = True
    Else
      Try
        MainForm.RemoveFromFormsCollection(Me)
        RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate
        RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

        RemoveHandler edit_Description.TextChanged, AddressOf FormControlChanged

        RemoveHandler Combo_FundID.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_FundID.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_FundID.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_FundID.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_FundID.SelectedValueChanged, AddressOf Me.FormControlChanged

        RemoveHandler Combo_SubFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_SubFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_SubFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_SubFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_SubFund.SelectedValueChanged, AddressOf Me.FormControlChanged

        RemoveHandler Combo_SpecificShareClass.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_SpecificShareClass.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_SpecificShareClass.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_SpecificShareClass.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_SpecificShareClass.SelectedValueChanged, AddressOf Me.FormControlChanged

        RemoveHandler Combo_FeeInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_FeeInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_FeeInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_FeeInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_FeeInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged

        RemoveHandler Combo_FeePeriodFrequency.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_FeePeriodFrequency.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_FeePeriodFrequency.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_FeePeriodFrequency.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_FeePeriodFrequency.SelectedValueChanged, AddressOf Me.FormControlChanged

        RemoveHandler Combo_FeePaymentFrequency.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_FeePaymentFrequency.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_FeePaymentFrequency.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_FeePaymentFrequency.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_FeePaymentFrequency.SelectedValueChanged, AddressOf Me.FormControlChanged

        RemoveHandler Combo_FeeSettlementFrequency.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_FeeSettlementFrequency.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_FeeSettlementFrequency.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_FeeSettlementFrequency.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_FeeSettlementFrequency.SelectedValueChanged, AddressOf Me.FormControlChanged

        RemoveHandler Check_UseFundFinancialYear.CheckedChanged, AddressOf Me.FormControlChanged
        RemoveHandler Numeric_DaysPerPeriod.ValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Numeric_SettlementTPlus.ValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_FeeAmount.ValueChanged, AddressOf FormControlChanged
        RemoveHandler Radio_FeeAmount.CheckedChanged, AddressOf FormControlChanged
        RemoveHandler Radio_FeePercentage.CheckedChanged, AddressOf FormControlChanged
        RemoveHandler Radio_PercentagePreviousNAV.CheckedChanged, AddressOf FormControlChanged
        RemoveHandler Radio_PercentageAfterSubscriptions.CheckedChanged, AddressOf FormControlChanged
        RemoveHandler Radio_PercentageBeforeFees.CheckedChanged, AddressOf FormControlChanged
        RemoveHandler Date_FeeStartDate.ValueChanged, AddressOf FormControlChanged
        RemoveHandler Date_FeeEndDate.ValueChanged, AddressOf FormControlChanged

      Catch ex As Exception
      End Try
    End If

  End Sub


#End Region

  ' Routine to handle changes / updates to tables by this and other windows.
  ' If this, or any other, form posts a change to a table, then it will invoke an update event 
  ' detailing what tables have been altered.
  ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
  ' the 'VeniceAutoUpdate' event of the main Venice form.
  ' Each form may them react as appropriate to changes in any table that might impact it.
  '
  ''' <summary>
  ''' Autoes the update.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    Dim OrgInPaint As Boolean
    Dim KnowledgeDateChanged As Boolean
    Dim SetButtonStatus_Flag As Boolean
    Dim RefreshForm As Boolean = False

    If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

    OrgInPaint = InPaint
    InPaint = True
    KnowledgeDateChanged = False
    SetButtonStatus_Flag = False

    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
      KnowledgeDateChanged = True
      RefreshForm = True
    End If

    ' ****************************************************************
    ' Check for changes relevant to this form
    ' ****************************************************************

    ' Changes to the tblFund table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFund) = True) Or KnowledgeDateChanged Then
      Try
        ' Re-Set combo.
        Call SetFundCombos()
      Catch ex As Exception
        MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblFund", ex.StackTrace, True)
      End Try
    End If

    ' Changes to the tblInstrument table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrument) = True) Or KnowledgeDateChanged Then
      Try
        ' Re-Set combo.
        Call SetInstrumentCombo()
        Call SetSpecificShareClassCombo()

      Catch ex As Exception
        MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblInstrument", ex.StackTrace, True)
      End Try
    End If

    ' Changes to the tblSubFund table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblSubFund) = True) Or KnowledgeDateChanged Then
      Try
        ' Re-Set combo.
        Call SetSubFundCombo()
      Catch ex As Exception
        MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblSubFund", ex.StackTrace, True)
      End Try
    End If

    ' Changes to the KnowledgeDate :-
    If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
      SetButtonStatus_Flag = True
    End If

    ' Changes to the tblUserPermissions table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

      ' Check ongoing permissions.

      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

        FormIsValid = False
        Me.Close()
        Exit Sub
      End If

      SetButtonStatus_Flag = True

    End If


    ' ****************************************************************
    ' Changes to the Main FORM table :-
    ' ****************************************************************

    If (e.TableChanged(THIS_FORM_ChangeID) = True) Or KnowledgeDateChanged Then
      RefreshForm = True

      ' Re-Set Controls etc.
      Call SetSortedRows()

      ' Move again to the correct item
      thisPosition = Get_Position(thisAuditID)

      ' Validate current position.
      If thisPosition >= 0 And thisPosition < Me.SortedRows.GetLength(0) Then
        thisDataRow = Me.SortedRows(thisPosition)
      Else
        If (Me.SortedRows.GetLength(0) > 0) And (AddNewRecord = False) Then
          thisPosition = 0
          thisDataRow = Me.SortedRows(thisPosition)
          FormChanged = False
        Else
          thisDataRow = Nothing
        End If
      End If

      ' Udate FeesDisplayTable.

      Dim UpdateDetail As String = e.UpdateDetail(THIS_FORM_ChangeID).Trim

      If (UpdateDetail.Length > 0) Then

        Dim FeeIDs() As String = UpdateDetail.Split(New Char() {",", "|"}, StringSplitOptions.RemoveEmptyEntries)
        Dim ThisFeeID As String

        If (FeeIDs Is Nothing) OrElse (FeeIDs.Length = 0) Then
          ' Clear whole cash
          SetFeesDisplayTable(0)
        Else
          For Each ThisFeeID In FeeIDs
            If (ThisFeeID = "-") Then
              SetFeesDisplayTable(0)
              Exit For
            End If

            If (IsNumeric(ThisFeeID)) Then
              SetFeesDisplayTable(CInt(ThisFeeID))

              If (CInt(ThisFeeID) = 0) Then
                Exit For
              End If
            End If
          Next
        End If

      Else
        Call SetFeesDisplayTable(0)
      End If

    End If

    ' ****************************************************************
    ' Repaint if not currently in Edit Mode
    '
    ' ****************************************************************

    InPaint = OrgInPaint

    ' Retrieve form data if appropriate, i.e. this data item is not already being edited.

    If (RefreshForm = True) AndAlso (FormChanged = False) AndAlso (AddNewRecord = False) Then
      GetFormData(thisDataRow) ' Includes a call to 'SetButtonStatus()'
    Else
      If SetButtonStatus_Flag Then
        Call SetButtonStatus()
      End If
    End If

  End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

  Private Sub SetFeesDisplayTable(ByVal AuditID As Integer)
    '
    '
    '

    Try
      Dim SelectedFees() As RenaissanceDataClass.DSFeeDefinitions.tblFeeDefinitionsRow
      Dim thisFee As RenaissanceDataClass.DSFeeDefinitions.tblFeeDefinitionsRow
      Dim SelectedDisplayRows() As DataRow
      Dim thisDisplayRow As DataRow
      Dim thisFeeID As Integer
      Dim AllNew As Boolean = False
      Dim DetalsString As String = ""

      If (FeesDisplayTable IsNot Nothing) Then
        If (myTable IsNot Nothing) Then

          If (AuditID <= 0) Then
            FeesDisplayTable.Clear()
            AllNew = True
            SelectedFees = myTable.Select()
          Else
            SelectedFees = myTable.Select("AuditID=" & AuditID.ToString)
          End If

          For Each thisFee In SelectedFees
            thisDisplayRow = Nothing
            thisFeeID = CInt(Nz(thisFee("FeeID"), 0))

            If (thisFeeID > 0) Then

              If (AllNew) Then
                thisDisplayRow = FeesDisplayTable.NewRow
              Else
                SelectedDisplayRows = FeesDisplayTable.Select("FeeID=" & thisFeeID)

                If (SelectedDisplayRows.Length > 0) Then
                  thisDisplayRow = SelectedDisplayRows(0)
                Else
                  thisDisplayRow = FeesDisplayTable.NewRow
                End If
              End If

              thisDisplayRow("FeeID") = thisFeeID
              thisDisplayRow("Fund") = Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblFund, CInt(Nz(thisFee("FundID"), 0)), "FundName"), "")
              thisDisplayRow("FundID") = CInt(Nz(thisFee("FundID"), 0))
              thisDisplayRow("SubFund") = Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblSubFund, CInt(Nz(thisFee("SubFundID"), 0)), "SubFundName"), "")
              thisDisplayRow("ShareClass") = Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, CInt(Nz(thisFee("ShareClassID"), 0)), "InstrumentClass"), "")
              thisDisplayRow("Description") = Nz(thisFee("Description"), "")
              thisDisplayRow("Instrument") = Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, CInt(Nz(thisFee("FeeInstrumentID"), 0)), "InstrumentDescription"), "")
              thisDisplayRow("Period") = CType(Nz(thisFee("FeeCalculationPeriod"), 0), DealingPeriod).ToString
              thisDisplayRow("Value") = CDbl(Nz(thisFee("FeeRate"), 0.0#))

              Try

                Select Case thisFee.FeeRateType
                  Case FeesType.PercentageValue ' Percentage

                    DetalsString = "Percentage of "

                    Select Case thisFee.FeeCalculationMethod

                      Case FeesPercentageType.ValueAfterSubscriptions  ' % After Subscriptions

                        DetalsString &= "value after Subscriptions"

                      Case FeesPercentageType.ValueBeforeFees  ' % Before Fees

                        DetalsString &= "value before fees"

                      Case Else ' % of Last NAV

                        DetalsString &= "last NAV"

                    End Select

                    DetalsString &= (" per " & CType(thisFee.FeeCalculationPeriod, DealingPeriod).ToString)

                  Case Else ' Amount

                    DetalsString = "Fixed Value of " & thisFee.FeeRate.ToString(DISPLAYMEMBER_DOUBLEFORMAT) & " per " & RenaissanceUtilities.DatePeriodFunctions.SinglePeriodName(thisFee.FeeCalculationPeriod)

                End Select

                DetalsString &= ", paid " & CType(thisFee.FeePaymentPeriod, DealingPeriod).ToString

              Catch ex As Exception
              End Try

              thisDisplayRow("Details") = DetalsString

              If (thisDisplayRow IsNot Nothing) AndAlso (thisDisplayRow.RowState And DataRowState.Detached) Then
                FeesDisplayTable.Rows.Add(thisDisplayRow)
              End If

            End If

          Next

        Else
          FeesDisplayTable.Clear()
        End If
      End If

      FeesDisplayTable.AcceptChanges()

    Catch ex As Exception
      Call MainForm.LogError(Me.Name & ", SetFeesDisplayTable()", LOG_LEVELS.Error, ex.Message, "Error in SetFeesDisplayTable.", ex.StackTrace, True)
    End Try


  End Sub

  ' Build Sorted list from the Source dataset and update the Select Combo
  ''' <summary>
  ''' Sets the sorted rows.
  ''' </summary>
  Private Sub SetSortedRows()

    Dim OrgInPaint As Boolean

    ' Code is pretty Generic form here on...

    ' Set paint local so that changes to the selection combo do not trigger form updates.

    OrgInPaint = InPaint
    InPaint = True

    ' Get selected Row sets, or exit if no data is present.
    If myDataset Is Nothing Then
      ReDim SortedRows(0)
      ReDim SelectBySortedRows(0)
    Else
      SortedRows = myTable.Select("RN >= 0", THIS_FORM_OrderBy)
      SelectBySortedRows = myTable.Select("RN >= 0", THIS_FORM_SelectBy)
    End If

    InPaint = OrgInPaint
  End Sub


  ' Check User permissions
  ''' <summary>
  ''' Checks the permissions.
  ''' </summary>
  Private Sub CheckPermissions()

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

  End Sub

  ' Flag changes to form controls, Event Associations made in 'New' routine.
  ''' <summary>
  ''' Basic event handler, called when a form value is changed.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    If InPaint = False Then

      If (Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord) Then
        FormChanged = True
        Me.btnSave.Enabled = True
        Me.btnCancel.Enabled = True

        If (sender Is Combo_FundID) Then
          Call SetSubFundCombo()
          Call SetSpecificShareClassCombo()
        End If

      End If
    End If

  End Sub

  ''' <summary>
  ''' Selects the menu event.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
  Public Sub SelectMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
    ' Event handler for the Select Menu
    ' Event association is made during the dynamic menu creation

    THIS_FORM_SelectBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

    Call SetSortedRows()

  End Sub

  ''' <summary>
  ''' Orders the menu event.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
  Public Sub OrderMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
    ' Event handler for the Order Menu
    ' Event association is made during the dynamic menu creation

    THIS_FORM_OrderBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

    Call SetSortedRows()

  End Sub


  ''' <summary>
  ''' Audits the report menu event.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
  Public Sub AuditReportMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
    ' Event handler for the Audit Report Menu
    ' Event association is made during the dynamic menu creation

    Try
      Select Case CInt(CType(sender, System.Windows.Forms.ToolStripMenuItem).Tag)
        Case 0 ' This Record
          If (Me.AddNewRecord = False) AndAlso (thisAuditID >= 0) Then
            Run_AuditReport_ThisRecord(MainForm, ThisStandardDataset, thisAuditID, MainForm.VeniceProgressBar)
          End If

        Case 1 ' All Records
          Run_AuditReport_ALLRecords(MainForm, ThisStandardDataset, MainForm.VeniceProgressBar)

      End Select
    Catch ex As Exception
      Call MainForm.LogError(Me.Name & ", AuditReportMenuEvent()", 0, ex.Message, "Error calling Audit report.", ex.StackTrace, True)
    End Try

  End Sub




#End Region

#Region " Set Form Combos (Form Specific Code) "

  ''' <summary>
  ''' Sets the Fund combo.
  ''' </summary>
  Private Sub SetFundCombos()
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_SelectFund, _
    RenaissanceStandardDatasets.tblFund, _
    "FundName", _
    "FundID", _
    "FundClosed=0", False, True, True, 0, "")   ' 

    Call MainForm.SetTblGenericCombo( _
      Me.Combo_FundID, _
      RenaissanceStandardDatasets.tblFund, _
      "FundName", _
      "FundID", _
      "FundClosed=0", False)   ' 

  End Sub

  Private Sub SetSubFundCombo()
    ' *******************************************************************************
    '
    ' *******************************************************************************
    Dim FundID As Integer = 0

    Try

      If (IsNumeric(Combo_FundID.SelectedValue)) Then
        FundID = CInt(Combo_FundID.SelectedValue)
      End If

    Catch ex As Exception
    End Try

    Call MainForm.SetTblGenericCombo( _
      Me.Combo_SubFund, _
      RenaissanceStandardDatasets.tblSubFundHeirarchy, _
      "Level|SubFundName", _
      "SubFundID", _
      "TopFundID=" & FundID.ToString, False, False, True, 0, "", "TreeOrder")   ' 

  End Sub

  ''' <summary>
  ''' Sets the Specific Share Class Combo.
  ''' </summary>
  Private Sub SetSpecificShareClassCombo()
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Dim FundID As Integer = 0

    If (IsNumeric(Combo_FundID.SelectedValue)) Then
      FundID = CInt(Combo_FundID.SelectedValue)
    End If

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_SpecificShareClass, _
    RenaissanceStandardDatasets.tblInstrument, _
    "InstrumentDescription", _
    "InstrumentID", _
    "InstrumentFundID=" & FundID.ToString, False, True, True, 0, "")   ' 

  End Sub

  Private Sub SetInstrumentCombo()
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_FeeInstrument, _
    RenaissanceStandardDatasets.tblInstrument, _
    "InstrumentDescription", _
    "InstrumentID", _
    "(InstrumentType=" & CInt(RenaissanceGlobals.InstrumentTypes.Expense).ToString & ") OR (InstrumentType=" & CInt(RenaissanceGlobals.InstrumentTypes.Fee).ToString & ")", False, True, True, 0, "") ' 

  End Sub

  Private Sub SetDealingPeriodCombos()
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Call MainForm.SetTblGenericCombo( _
    Combo_FeePeriodFrequency, _
    RenaissanceStandardDatasets.tblDealingPeriod, _
    "DealingPeriodDescription", _
    "DealingPeriodID", "true")

    Call MainForm.SetTblGenericCombo( _
    Combo_FeePaymentFrequency, _
    RenaissanceStandardDatasets.tblDealingPeriod, _
    "DealingPeriodDescription", _
    "DealingPeriodID", "true")

    Call MainForm.SetTblGenericCombo( _
    Combo_FeeSettlementFrequency, _
    RenaissanceStandardDatasets.tblDealingPeriod, _
    "DealingPeriodDescription", _
    "DealingPeriodID", "true")

  End Sub

#End Region

#Region " Get & Set Form Data / SetButton / ValidateForm / btnCancel Events (Form Specific Code) "

  ''' <summary>
  ''' Gets the form data.
  ''' </summary>
  ''' <param name="ThisRow">The this row.</param>
  Private Sub GetFormData(ByRef ThisRow As RenaissanceDataClass.DSFeeDefinitions.tblFeeDefinitionsRow)
    ' Routine to populate form Controls given a data row.
    ' Form is cleared for invalid Datarows or Invalid Form.

    Dim OrgInpaint As Boolean

    ' Set 'Paint' flag to prevent event actions caused by changing the value of form fields.
    OrgInpaint = InPaint
    InPaint = True


    If (ThisRow Is Nothing) Or (FormIsValid = False) Or (Me.InUse = False) Then
      ' Bad / New Datarow - Clear Form.

      thisAuditID = (-1)

      edit_Description.Text = ""
      MainForm.ClearComboSelection(Combo_FundID)
      MainForm.ClearComboSelection(Combo_SubFund)
      MainForm.ClearComboSelection(Combo_SpecificShareClass)
      MainForm.ClearComboSelection(Combo_FeeInstrument)
      Combo_FeePeriodFrequency.SelectedValue = DealingPeriod.Annually
      Combo_FeePaymentFrequency.SelectedValue = DealingPeriod.Monthly
      Combo_FeeSettlementFrequency.SelectedValue = DealingPeriod.Monthly
      Check_UseFundFinancialYear.Checked = True
      Numeric_DaysPerPeriod.Value = 0
      Numeric_SettlementTPlus.Value = 0
      edit_FeeAmount.Value = 0.0#
      Radio_FeeAmount.Checked = True
      Panel_PercentageValue.Visible = False
      Radio_PercentagePreviousNAV.Checked = True
      Date_FeeStartDate.Value = Renaissance_BaseDate
      Date_FeeEndDate.Value = Renaissance_EndDate_Data

      If AddNewRecord = True Then
        Me.btnCancel.Enabled = True
      Else
        Me.btnCancel.Enabled = False
      End If

    Else

      ' Populate Form with given data.
      Try
        thisAuditID = thisDataRow.AuditID

        edit_Description.Text = thisDataRow.Description
        Combo_FundID.SelectedValue = thisDataRow.FundID

        Call SetSubFundCombo()
        Call SetSpecificShareClassCombo()

        Combo_SubFund.SelectedValue = thisDataRow.SubFundID
        Combo_SpecificShareClass.SelectedValue = thisDataRow.ShareClassID
        Combo_FeeInstrument.SelectedValue = thisDataRow.FeeInstrumentID
        Combo_FeePeriodFrequency.SelectedValue = thisDataRow.FeeCalculationPeriod
        Combo_FeePaymentFrequency.SelectedValue = thisDataRow.FeePaymentPeriod
        Combo_FeeSettlementFrequency.SelectedValue = thisDataRow.FeeSettlementPeriod

        Check_UseFundFinancialYear.Checked = CBool(thisDataRow.FeeUseFundYearEnd)
        Numeric_DaysPerPeriod.Value = thisDataRow.FeeDaysPerPeriod
        Numeric_SettlementTPlus.Value = thisDataRow.FeeSettlementTPlusDays
        Date_FeeStartDate.Value = thisDataRow.FeeStartDate
        Date_FeeEndDate.Value = thisDataRow.FeeEndDate

        edit_FeeAmount.Value = thisDataRow.FeeRate

        Select Case thisDataRow.FeeRateType
          Case FeesType.PercentageValue ' Percentage

            edit_FeeAmount.TextFormat = DISPLAYMEMBER_PERCENTAGEFORMAT
            Panel_PercentageValue.Visible = True

            Radio_FeeAmount.Checked = False
            Radio_FeePercentage.Checked = True

            Select Case thisDataRow.FeeCalculationMethod

              Case FeesPercentageType.ValueAfterSubscriptions  ' % After Subscriptions

                Radio_PercentageAfterSubscriptions.Checked = True

              Case FeesPercentageType.ValueBeforeFees  ' % Before Fees

                Radio_PercentageBeforeFees.Checked = True

              Case Else ' % of Last NAV

                Radio_PercentagePreviousNAV.Checked = True

            End Select

          Case Else ' Amount

            edit_FeeAmount.TextFormat = DISPLAYMEMBER_DOUBLEFORMAT

            Radio_FeePercentage.Checked = False
            Radio_FeeAmount.Checked = True

            Panel_PercentageValue.Visible = False

        End Select

        AddNewRecord = False
        ' MainForm.SetComboSelectionLengths(Me)

        Me.btnCancel.Enabled = False

      Catch ex As Exception

        Call MainForm.LogError(Me.Name & ", GetFormData", 0, ex.Message, "Error Showing Data", ex.StackTrace, True)
        Call GetFormData(Nothing)

      End Try

    End If

    ' Allow Field events to trigger before 'InPaint' Is re-set. 
    ' (Should) Prevent Validation errors during Form Draw.
    Application.DoEvents()

    ' Restore 'Paint' flag.
    InPaint = OrgInpaint
    FormChanged = False
    Me.btnSave.Enabled = False

    ' As it says on the can....
    Call SetButtonStatus()

  End Sub

  ''' <summary>
  ''' Sets the form data.
  ''' </summary>
  ''' <param name="pConfirm">if set to <c>true</c> [p confirm].</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function SetFormData(Optional ByVal pConfirm As Boolean = True) As Boolean
    ' *************************************************************
    '
    ' *************************************************************
    Dim ErrMessage As String
    Dim ErrFlag As Boolean
    Dim ErrStack As String

    ErrMessage = ""
    ErrStack = ""

    ' *************************************************************
    ' Appropriate Save permission :-
    ' *************************************************************

    If (Me.HasUpdatePermission = False) And ((Me.HasInsertPermission = False) Or (Me.AddNewRecord = False)) Then
      MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "You do not have permission to save this record.", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Function
    End If

    ' *************************************************************
    ' If Save button is disabled then should not be able to save, exit silently.
    ' *************************************************************
    If Me.btnSave.Enabled = False Then
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Function
    End If

    ' *************************************************************
    ' KnowledgeDate OK :-
    ' *************************************************************

    If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
      MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Changes to Venice are not allowed when the Knowledgedate is not set to `NOW`", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Function
    End If

    ' Confirm Save, if required.

    If (pConfirm = True) Then
      If MessageBox.Show("Save Changes ?", "Save Changes ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
        Grid_Fees.Select(0, 0, False)
        Return True
        Exit Function
      End If
    End If

    ' *************************************************************
    ' Procedure to Save the current form information.
    ' *************************************************************

    Dim StatusString As String
    Dim LogString As String
    Dim UpdateRows(0) As DataRow
    Dim Position As Integer

    If (FormChanged = False) Or (FormIsValid = False) Then
      Return False
      Exit Function
    End If

    ' Validation
    StatusString = ""
    If ValidateForm(StatusString) = False Then
      MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Validation Error." & vbCrLf & StatusString, "", True)
      Return False
      Exit Function
    End If

    ' Check current position in the table.
    Position = Get_Position(thisAuditID)

    ' Allow for new or missing ID.
    If Position < 0 Then
      If (AddNewRecord = True) OrElse (SortedRows.Length <= 0) Then
        thisDataRow = myTable.NewRow
        LogString = "Add: "
        AddNewRecord = True
      Else
        Return False
        Exit Function
      End If
    Else
      ' Row found OK.
      LogString = "Edit : AuditID = " & thisAuditID.ToString

      thisDataRow = Me.SortedRows(Position)

    End If

    ' Set 'Paint' flag.
    InPaint = True

    ' *************************************************************
    ' Lock the Data Table, to prevent update conflicts.
    ' *************************************************************
    SyncLock myTable

      ' Initiate Edit,
      thisDataRow.BeginEdit()

      thisDataRow.Description = edit_Description.Text
      thisDataRow.FundID = Combo_FundID.SelectedValue
      thisDataRow.SubFundID = Combo_SubFund.SelectedValue
      thisDataRow.ShareClassID = Combo_SpecificShareClass.SelectedValue
      thisDataRow.FeeInstrumentID = Combo_FeeInstrument.SelectedValue
      thisDataRow.FeeCalculationPeriod = Combo_FeePeriodFrequency.SelectedValue
      thisDataRow.FeePaymentPeriod = Combo_FeePaymentFrequency.SelectedValue
      thisDataRow.FeeSettlementPeriod = Combo_FeeSettlementFrequency.SelectedValue

      thisDataRow.FeeUseFundYearEnd = CInt(Check_UseFundFinancialYear.Checked)
      thisDataRow.FeeDaysPerPeriod = Numeric_DaysPerPeriod.Value
      thisDataRow.FeeSettlementTPlusDays = Numeric_SettlementTPlus.Value
      thisDataRow.FeeStartDate = Date_FeeStartDate.Value
      thisDataRow.FeeEndDate = Date_FeeEndDate.Value

      thisDataRow.FeeRate = edit_FeeAmount.Value

      If (Radio_FeePercentage.Checked) Then
        thisDataRow.FeeRateType = FeesType.PercentageValue

        If (Radio_PercentageAfterSubscriptions.Checked) Then
          thisDataRow.FeeCalculationMethod = FeesPercentageType.ValueAfterSubscriptions
        ElseIf (Radio_PercentageBeforeFees.Checked) Then
          thisDataRow.FeeCalculationMethod = FeesPercentageType.ValueBeforeFees
        Else ' Radio_PercentagePreviousNAV.Checked
          thisDataRow.FeeCalculationMethod = FeesPercentageType.PreviousNAV
        End If
      Else ' Radio_FeeAmount.Checked
        thisDataRow.FeeRateType = FeesType.FixedValue
        thisDataRow.FeeCalculationMethod = 0
      End If

      thisDataRow.EndEdit()
      InPaint = False

      ' Add and Update DataRow. 

      ErrFlag = False

      If AddNewRecord = True Then
        Try
          myTable.Rows.Add(thisDataRow)
        Catch ex As Exception
          ErrFlag = True
          ErrMessage = ex.Message
          ErrStack = ex.StackTrace
        End Try
      End If

      UpdateRows(0) = thisDataRow

      ' Post Additions / Updates to the underlying table :-
      Dim temp As Integer
      Try
        If (ErrFlag = False) Then
          myAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
          myAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

          temp = MainForm.AdaptorUpdate(Me.Name, myAdaptor, UpdateRows)
        End If

      Catch ex As Exception

        ErrMessage = ex.Message
        ErrFlag = True
        ErrStack = ex.StackTrace

      End Try

      thisAuditID = thisDataRow.AuditID


    End SyncLock

    ' *************************************************************
    ' Error report. Not done in the Try-Catch as that would serve to persist the SyncLock.
    ' *************************************************************

    If (ErrFlag = True) Then
      Call MainForm.LogError(Me.Name & ", SetFormData", 0, ErrMessage, "Error Saving Data", ErrStack, True)
    End If

    ' Finish off

    AddNewRecord = False
    FormChanged = False

    ' Propogate changes

    If (thisDataRow IsNot Nothing) AndAlso (thisDataRow.IsAuditIDNull = False) AndAlso (thisDataRow.AuditID > 0) Then
      Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID, thisDataRow.AuditID.ToString), True)
    Else
      Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID), True)
    End If

    Return False
  End Function

  ''' <summary>
  ''' Sets the button status.
  ''' </summary>
  Private Sub SetButtonStatus()
    ' Sets the status of the form controlls appropriate to the current users 
    'permissions and the 'Changed' or 'New' status of the form.

    ' No Read permission :-

    If Me.HasReadPermission = False Then
      MainForm.LogError(Me.Name & ", SetButtonStatus()", 0, "", "You do not have Read permission for this Form", "", True)
      Me.Close()
      Exit Sub
    End If

    ' Has Insert Permission.
    If Me.HasInsertPermission Then
      Me.btnAdd.Enabled = True
    Else
      Me.btnAdd.Enabled = False
    End If

    ' Has Delete permission. 
    If (Me.HasDeletePermission) And (Me.AddNewRecord = False) And (thisPosition >= 0) Then
      Me.btnDelete.Enabled = True
    Else
      Me.btnDelete.Enabled = False
    End If

    If (Me.MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And _
     ((Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord)) Then

      edit_Description.Enabled = True
      Combo_FundID.Enabled = True
      Combo_SubFund.Enabled = True
      Combo_SpecificShareClass.Enabled = True
      Combo_FeeInstrument.Enabled = True
      Combo_FeePeriodFrequency.Enabled = True
      Combo_FeePaymentFrequency.Enabled = True
      Combo_FeeSettlementFrequency.Enabled = True
      Check_UseFundFinancialYear.Enabled = True
      Numeric_DaysPerPeriod.Enabled = True
      Numeric_SettlementTPlus.Enabled = True
      edit_FeeAmount.Enabled = True
      Radio_FeeAmount.Enabled = True
      Panel_PercentageValue.Enabled = True
      Radio_PercentagePreviousNAV.Enabled = True
      Date_FeeStartDate.Enabled = True
      Date_FeeEndDate.Enabled = True

    Else

      edit_Description.Enabled = False
      Combo_FundID.Enabled = False
      Combo_SubFund.Enabled = False
      Combo_SpecificShareClass.Enabled = False
      Combo_FeeInstrument.Enabled = False
      Combo_FeePeriodFrequency.Enabled = False
      Combo_FeePaymentFrequency.Enabled = False
      Combo_FeeSettlementFrequency.Enabled = False
      Check_UseFundFinancialYear.Enabled = False
      Numeric_DaysPerPeriod.Enabled = False
      Numeric_SettlementTPlus.Enabled = False
      edit_FeeAmount.Enabled = False
      Radio_FeeAmount.Enabled = False
      Panel_PercentageValue.Enabled = False
      Radio_PercentagePreviousNAV.Enabled = False
      Date_FeeStartDate.Enabled = False
      Date_FeeEndDate.Enabled = False

    End If

  End Sub

  ''' <summary>
  ''' Validates the form.
  ''' </summary>
  ''' <param name="pReturnString">The p return string.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function ValidateForm(ByRef pReturnString As String) As Boolean
    ' Form Validation code.
    ' 
    ' This code should be the final arbiter of what is allowed. no assumptions regarding 
    ' prior validation should be made.
    ' 
    ' This Code is called by the SetFormData routine before position changes.
    '
    Dim RVal As Boolean

    RVal = True
    pReturnString = ""

    ' Validate 

    Try



    Catch ex As Exception
      pReturnString = ex.Message
      RVal = False
    End Try


    Return RVal

  End Function

  ''' <summary>
  ''' Handles the MouseEnter event of the btnCancel control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnCancel_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseEnter
    ' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
    Me.IsOverCancelButton = True
  End Sub

  ''' <summary>
  ''' Handles the MouseLeave event of the btnCancel control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnCancel_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseLeave
    ' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
    Me.IsOverCancelButton = False
  End Sub


#End Region

#Region " Navigation Code / GetPosition() (Generic Code) "

  ''' <summary>
  ''' Moves to audit ID.
  ''' </summary>
  ''' <param name="pAuditID">The p audit ID.</param>
  Public Sub MoveToAuditID(ByVal pAuditID As Integer)
    ' ****************************************************************
    ' Subroutine to move the current form focus to a given AuditID
    ' ****************************************************************

    Dim thisFeeRow As RenaissanceDataClass.DSFeeDefinitions.tblFeeDefinitionsRow
    Dim Position As Integer
    Application.DoEvents()

    Try
      thisFeeRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFeeDefinitions, pAuditID), RenaissanceDataClass.DSFeeDefinitions.tblFeeDefinitionsRow)

      If (thisFeeRow IsNot Nothing) Then

        Combo_SelectFund.SelectedValue = thisFeeRow.FundID
        Application.DoEvents()
        Grid_Fees.Select(0, 0)

        Position = Get_Position(pAuditID)
        If (Position >= 0) AndAlso (Position < Me.SortedRows.Length) Then
          thisDataRow = Me.SortedRows(Position)

          GetFormData(thisDataRow)
        End If

      End If

    Catch ex As Exception
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Moving to given AuditID.", ex.StackTrace, True)
    End Try

  End Sub

  ''' <summary>
  ''' Get_s the position.
  ''' </summary>
  ''' <param name="pAuditID">The p audit ID.</param>
  ''' <returns>System.Int32.</returns>
  Private Function Get_Position(ByVal pAuditID As Integer) As Integer
    ' Returns the position in the current 'SortedRows' array of the entry corresponding to the given 
    ' AudidID.

    Dim MinIndex As Integer
    Dim MaxIndex As Integer
    Dim CurrentMin As Integer
    Dim CurrentMax As Integer
    Dim searchDataRow As DataRow

    Try
      ' SortedRows exists ?

      If SortedRows Is Nothing Then
        Return (-1)
        Exit Function
      End If

      ' Return (-1) if there are no rows in the 'SortedRows'

      If (SortedRows.GetLength(0) <= 0) Then
        Return (-1)
        Exit Function
      End If


      ' Use a modified Search moving outwards from the last returned value to locate the required value.
      ' Reflecting the fact that for the most part One looks for closely located records.

      MinIndex = 0
      MaxIndex = SortedRows.GetLength(0) - 1


      ' Check First and Last records (Incase 'First' or 'Last' was pressed).

      searchDataRow = SortedRows(MinIndex)

      If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
        Return MinIndex
        Exit Function
      End If

      searchDataRow = SortedRows(MaxIndex)
      If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
        Return MaxIndex
        Exit Function
      End If

      ' now search outwards from the last returned value.

      If (Me.thisPosition >= MinIndex) And (Me.thisPosition <= MaxIndex) Then
        CurrentMin = thisPosition
        CurrentMax = CurrentMin + 1
      Else
        CurrentMin = CInt((MinIndex + MaxIndex) / 2)
        CurrentMax = CurrentMin + 1
      End If

      While (CurrentMin >= MinIndex) Or (CurrentMax <= MaxIndex)
        If (CurrentMin >= MinIndex) Then
          searchDataRow = SortedRows(CurrentMin)

          If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
            Return CurrentMin
            Exit Function
          End If

          CurrentMin -= 1
        End If

        If (CurrentMax <= MaxIndex) Then
          searchDataRow = SortedRows(CurrentMax)

          If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
            Return CurrentMax
            Exit Function
          End If

          CurrentMax += 1
        End If

      End While

      Return (-1)
      Exit Function

    Catch ex As Exception
      MainForm.LogError(Me.Name & ", Get_Position()", LOG_LEVELS.Warning, ex.Message, "", ex.StackTrace, True)
      Return (-1)
      Exit Function
    End Try

  End Function

#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

  ''' <summary>
  ''' Handles the Click event of the btnCancel control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    ' Cancel Changes, redisplay form.

    FormChanged = False
    AddNewRecord = False

    If (thisPosition >= 0) And (thisPosition < Me.SortedRows.GetLength(0)) Then
      thisDataRow = Me.SortedRows(thisPosition)
      Call GetFormData(thisDataRow)
    Else
      Call GetFormData(Nothing)
    End If

  End Sub

  ''' <summary>
  ''' Handles the Click event of the btnSave control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
    ' Save Changes, if any, without prompting.

    If (FormChanged = True) Then
      Call SetFormData(False)
    End If

  End Sub

  ''' <summary>
  ''' Handles the Click event of the btnDelete control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
    Dim Position As Integer
    Dim NextAuditID As Integer

    If (AddNewRecord = True) Then
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Sub
    End If

    ' No Appropriate Save permission :-

    If (Me.HasDeletePermission = False) Then
      MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "You do not have permission to Delete this record.", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Sub
    End If

    ' *************************************************************
    ' KnowledgeDate OK :-
    ' *************************************************************

    If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
      MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "Changes to Venice are not allowed when the Knowledgedate is not set to `NOW`", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Sub
    End If

    ' *************************************************************
    ' Confirm :-
    ' *************************************************************
    If MsgBox("Are you Sure ?", MsgBoxStyle.OkCancel, "Confirm Delete") = MsgBoxResult.Cancel Then
      Exit Sub
    End If

    ' Check Data position.

    Position = Get_Position(thisAuditID)

    If Position < 0 Then Exit Sub

    ' Check Referential Integrity 
    If CheckReferentialIntegrity(_MainForm, Me.SortedRows(Position)) = False Then
      MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "Deleting this record would voilate referential integrity.", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Sub
    End If

    ' Resolve row to show after deleting this one.

    NextAuditID = (-1)
    If (Position + 1) < Me.SortedRows.GetLength(0) Then
      NextAuditID = CInt(Me.SortedRows(Position + 1)("AuditID"))
    ElseIf (Position - 1) >= 0 Then
      NextAuditID = CInt(Me.SortedRows(Position - 1)("AuditID"))
    Else
      NextAuditID = (-1)
    End If

    ' Delete this row

    Try
      Me.SortedRows(Position).Delete()

      MainForm.AdaptorUpdate(Me.Name, myAdaptor, myTable)

    Catch ex As Exception

      Call MainForm.LogError(Me.Name & ", Delete()", LOG_LEVELS.Error, ex.Message, "Error Deleting Record.", ex.StackTrace, True)
    End Try

    ' Tidy Up.

    FormChanged = False

    thisAuditID = NextAuditID
    Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID), True)

  End Sub

  ''' <summary>
  ''' Handles the Click event of the btnAdd control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
    ' Prepare form to Add a new record.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    thisPosition = -1

    GetFormData(Nothing)
    Grid_Fees.Select(0, 0, False)

    AddNewRecord = True
    Me.btnCancel.Enabled = True

    Call SetButtonStatus()

    THIS_FORM_NewMoveToControl.Focus()

  End Sub

  ''' <summary>
  ''' Handles the Click event of the btnClose control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
    ' Close Form

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    Me.Close()

  End Sub

#End Region

#Region " Bug hunting "

  ' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
  ' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
  ' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

  ''' <summary>
  ''' Called when [row updating].
  ''' </summary>
  ''' <param name="Sender">The sender.</param>
  ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  ''' <summary>
  ''' Called when [row updated].
  ''' </summary>
  ''' <param name="Sender">The sender.</param>
  ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  ''' <summary>
  ''' Called when [row fill error].
  ''' </summary>
  ''' <param name="Sender">The sender.</param>
  ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
    Dim a As Integer

    a = 1
  End Sub

#End Region


  Private Sub Radio_FeeAmount_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_FeeAmount.CheckedChanged
    ' *************************************************************
    ' 
    ' *************************************************************

    Try
      If (Radio_FeeAmount.Checked) Then
        edit_FeeAmount.TextFormat = DISPLAYMEMBER_DOUBLEFORMAT

        Panel_PercentageValue.Visible = False
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Radio_FeePercentage_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_FeePercentage.CheckedChanged
    ' *************************************************************
    ' 
    ' *************************************************************

    Try
      If (Radio_FeePercentage.Checked) Then
        edit_FeeAmount.TextFormat = DISPLAYMEMBER_PERCENTAGEFORMAT

        Panel_PercentageValue.Visible = True
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Combo_SelectFund_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_SelectFund.SelectedIndexChanged
    ' *************************************************************
    ' 
    ' *************************************************************

    Try

      If (IsNumeric(Combo_SelectFund.SelectedValue)) Then

        If (CInt(Combo_SelectFund.SelectedValue) <= 0) Then

          FeesDataView.RowFilter = "true"

        Else

          FeesDataView.RowFilter = "FundID=" & CInt(Combo_SelectFund.SelectedValue).ToString

        End If
      Else

        FeesDataView.RowFilter = "true"

      End If

    Catch ex As Exception
      Call MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Combo_SelectFund_SelectedIndexChanged()", ex.StackTrace, True)
    End Try
  End Sub

  Private Sub Grid_Fees_SelChange(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Grid_Fees.SelChange

    ' ***************************************************************************************
    ' Spawn an Add/Edit Transactions form reflecting the chosen selection of transactions
    ' and pointing to the transaction which was double clicked.
    ' ***************************************************************************************

    Try

      Dim FeeID As Integer

      If (Grid_Fees.RowSel > 0) AndAlso (Grid_Fees.RowSel < Grid_Fees.Rows.Count) Then
        FeeID = CInt(Nz(Me.Grid_Fees.Rows(Grid_Fees.RowSel).DataSource("FeeID"), 0))

        If (FeeID > 0) Then

          If (FormChanged = True) Then
            If SetFormData(True) Then
              Exit Sub
            End If
          End If

          ' Move again to the correct item
          thisPosition = Get_Position(FeeID)

          ' Validate current position.
          If thisPosition >= 0 And thisPosition < Me.SortedRows.GetLength(0) Then
            thisDataRow = Me.SortedRows(thisPosition)
          Else
            If (Me.SortedRows.GetLength(0) > 0) And (AddNewRecord = False) Then
              thisPosition = 0
              thisDataRow = Me.SortedRows(thisPosition)
              FormChanged = False
            Else
              thisDataRow = Nothing
            End If
          End If

          GetFormData(thisDataRow)
        End If
      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error opening Transactions Form.", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub Combo_FeePeriodFrequency_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_FeePeriodFrequency.SelectedValueChanged

    Try
      If (IsNumeric(Combo_FeePeriodFrequency.SelectedValue)) Then
        If CType(CInt(Combo_FeePeriodFrequency.SelectedValue), DealingPeriod) = DealingPeriod.Annually Then
          Check_UseFundFinancialYear.Visible = True
        Else
          Check_UseFundFinancialYear.Visible = False
        End If
      Else
        Check_UseFundFinancialYear.Visible = False
      End If
    Catch ex As Exception
    End Try

  End Sub


End Class
