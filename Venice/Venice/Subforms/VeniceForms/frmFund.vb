' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmFund.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals


''' <summary>
''' Class frmFund
''' </summary>
Public Class frmFund

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmFund"/> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The LBL fund code
    ''' </summary>
  Friend WithEvents lblFundCode As System.Windows.Forms.Label
    ''' <summary>
    ''' The LBL fund name
    ''' </summary>
  Friend WithEvents lblFundName As System.Windows.Forms.Label
    ''' <summary>
    ''' The LBL fund legal entity
    ''' </summary>
  Friend WithEvents lblFundLegalEntity As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit audit ID
    ''' </summary>
  Friend WithEvents editAuditID As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The edit fund code
    ''' </summary>
  Friend WithEvents editFundCode As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The edit fund name
    ''' </summary>
  Friend WithEvents editFundName As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The edit fund legal entity
    ''' </summary>
  Friend WithEvents editFundLegalEntity As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The LBL fund base currency
    ''' </summary>
  Friend WithEvents lblFundBaseCurrency As System.Windows.Forms.Label
    ''' <summary>
    ''' The LBL fund unit
    ''' </summary>
  Friend WithEvents lblFundUnit As System.Windows.Forms.Label
    ''' <summary>
    ''' The LBL fund parent fund
    ''' </summary>
  Friend WithEvents lblFundParentFund As System.Windows.Forms.Label
    ''' <summary>
    ''' The BTN nav first
    ''' </summary>
  Friend WithEvents btnNavFirst As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN nav prev
    ''' </summary>
  Friend WithEvents btnNavPrev As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN nav next
    ''' </summary>
  Friend WithEvents btnNavNext As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN last
    ''' </summary>
  Friend WithEvents btnLast As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN add
    ''' </summary>
  Friend WithEvents btnAdd As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN delete
    ''' </summary>
  Friend WithEvents btnDelete As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN cancel
    ''' </summary>
  Friend WithEvents btnCancel As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN save
    ''' </summary>
  Friend WithEvents btnSave As System.Windows.Forms.Button
    ''' <summary>
    ''' The combo_ select fund code
    ''' </summary>
  Friend WithEvents Combo_SelectFundCode As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The panel1
    ''' </summary>
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The combo_ parent fund
    ''' </summary>
  Friend WithEvents Combo_ParentFund As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label1
    ''' </summary>
  Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The group box1
    ''' </summary>
  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The combo_ fund base currency
    ''' </summary>
  Friend WithEvents Combo_FundBaseCurrency As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ fund unit
    ''' </summary>
  Friend WithEvents Combo_FundUnit As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The root menu
    ''' </summary>
	Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
  ''' <summary>
  ''' The tab_ fund details
  ''' </summary>
  Friend WithEvents Tab_FundDetails As System.Windows.Forms.TabControl
  ''' <summary>
  ''' The tab_ manager details
  ''' </summary>
  Friend WithEvents Tab_ManagerDetails As System.Windows.Forms.TabPage
  ''' <summary>
  ''' The tab_ fund terms
  ''' </summary>
  Friend WithEvents Tab_FundTerms As System.Windows.Forms.TabPage
  ''' <summary>
  ''' The tab_ legal
  ''' </summary>
  Friend WithEvents Tab_Legal As System.Windows.Forms.TabPage
  ''' <summary>
  ''' The tab_ objective
  ''' </summary>
  Friend WithEvents Tab_Objective As System.Windows.Forms.TabPage
  ''' <summary>
  ''' The label6
  ''' </summary>
  Friend WithEvents Label6 As System.Windows.Forms.Label
  ''' <summary>
  ''' The label5
  ''' </summary>
  Friend WithEvents Label5 As System.Windows.Forms.Label
  ''' <summary>
  ''' The text_ disclaimer
  ''' </summary>
  Friend WithEvents Text_Disclaimer As System.Windows.Forms.TextBox
  ''' <summary>
  ''' The text_ objective
  ''' </summary>
  Friend WithEvents Text_Objective As System.Windows.Forms.TextBox
  ''' <summary>
  ''' The text_ reuters
  ''' </summary>
  Friend WithEvents Text_Reuters As System.Windows.Forms.TextBox
  ''' <summary>
  ''' The label14
  ''' </summary>
  Friend WithEvents Label14 As System.Windows.Forms.Label
  ''' <summary>
  ''' The text_ bloomberg
  ''' </summary>
  Friend WithEvents Text_Bloomberg As System.Windows.Forms.TextBox
  ''' <summary>
  ''' The label15
  ''' </summary>
  Friend WithEvents Label15 As System.Windows.Forms.Label
  ''' <summary>
  ''' The text_ SEDOL
  ''' </summary>
  Friend WithEvents Text_SEDOL As System.Windows.Forms.TextBox
  ''' <summary>
  ''' The label13
  ''' </summary>
  Friend WithEvents Label13 As System.Windows.Forms.Label
  ''' <summary>
  ''' The text_ ISIN
  ''' </summary>
  Friend WithEvents Text_ISIN As System.Windows.Forms.TextBox
  ''' <summary>
  ''' The label12
  ''' </summary>
  Friend WithEvents Label12 As System.Windows.Forms.Label
  ''' <summary>
  ''' The combo_ listed instrument
  ''' </summary>
  Friend WithEvents Combo_ListedInstrument As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The label11
  ''' </summary>
  Friend WithEvents Label11 As System.Windows.Forms.Label
  ''' <summary>
  ''' The combo_ listings
  ''' </summary>
  Friend WithEvents Combo_Listings As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The label10
  ''' </summary>
  Friend WithEvents Label10 As System.Windows.Forms.Label
  ''' <summary>
  ''' The combo_ custodian
  ''' </summary>
  Friend WithEvents Combo_Custodian As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The label9
  ''' </summary>
  Friend WithEvents Label9 As System.Windows.Forms.Label
  ''' <summary>
  ''' The combo_ administrator
  ''' </summary>
  Friend WithEvents Combo_Administrator As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The label8
  ''' </summary>
  Friend WithEvents Label8 As System.Windows.Forms.Label
  ''' <summary>
  ''' The combo_ jurisdiction
  ''' </summary>
  Friend WithEvents Combo_Jurisdiction As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The label7
  ''' </summary>
  Friend WithEvents Label7 As System.Windows.Forms.Label
  ''' <summary>
  ''' The combo_ subscription frequency
  ''' </summary>
  Friend WithEvents Combo_SubscriptionFrequency As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The label18
  ''' </summary>
  Friend WithEvents Label18 As System.Windows.Forms.Label
  ''' <summary>
  ''' The label17
  ''' </summary>
  Friend WithEvents Label17 As System.Windows.Forms.Label
  ''' <summary>
  ''' The label16
  ''' </summary>
  Friend WithEvents Label16 As System.Windows.Forms.Label
  ''' <summary>
  ''' The combo_ redemption notice
  ''' </summary>
  Friend WithEvents Combo_RedemptionNotice As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The label21
  ''' </summary>
  Friend WithEvents Label21 As System.Windows.Forms.Label
  ''' <summary>
  ''' The combo_ redemption frequency
  ''' </summary>
  Friend WithEvents Combo_RedemptionFrequency As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The label20
  ''' </summary>
  Friend WithEvents Label20 As System.Windows.Forms.Label
  ''' <summary>
  ''' The combo_ subscription notice
  ''' </summary>
  Friend WithEvents Combo_SubscriptionNotice As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The label19
  ''' </summary>
  Friend WithEvents Label19 As System.Windows.Forms.Label
  ''' <summary>
  ''' The label25
  ''' </summary>
  Friend WithEvents Label25 As System.Windows.Forms.Label
  ''' <summary>
  ''' The text_ contact address
  ''' </summary>
  Friend WithEvents Text_ContactAddress As System.Windows.Forms.TextBox
  ''' <summary>
  ''' The label24
  ''' </summary>
  Friend WithEvents Label24 As System.Windows.Forms.Label
  ''' <summary>
  ''' The text_ fund managers
  ''' </summary>
  Friend WithEvents Text_FundManagers As System.Windows.Forms.TextBox
  ''' <summary>
  ''' The combo_ management company
  ''' </summary>
  Friend WithEvents Combo_ManagementCompany As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The label23
  ''' </summary>
  Friend WithEvents Label23 As System.Windows.Forms.Label
  ''' <summary>
  ''' The label22
  ''' </summary>
  Friend WithEvents Label22 As System.Windows.Forms.Label
  ''' <summary>
  ''' The date_ investment start date
  ''' </summary>
  Friend WithEvents Date_InvestmentStartDate As System.Windows.Forms.DateTimePicker
  ''' <summary>
  ''' The combo_ contact E mail
  ''' </summary>
  Friend WithEvents Combo_ContactEMail As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The label27
  ''' </summary>
  Friend WithEvents Label27 As System.Windows.Forms.Label
  ''' <summary>
  ''' The combo_ contact phone
  ''' </summary>
  Friend WithEvents Combo_ContactPhone As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The label26
  ''' </summary>
  Friend WithEvents Label26 As System.Windows.Forms.Label
  ''' <summary>
  ''' The combo_ initial subscription fees
  ''' </summary>
  Friend WithEvents Combo_InitialSubscriptionFees As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The combo_ minimum investment
  ''' </summary>
  Friend WithEvents Combo_MinimumInvestment As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The combo_ performande fees
  ''' </summary>
  Friend WithEvents Combo_PerformandeFees As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The combo_ management fees
  ''' </summary>
  Friend WithEvents Combo_ManagementFees As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The label28
  ''' </summary>
  Friend WithEvents Label28 As System.Windows.Forms.Label
  ''' <summary>
  ''' The label29
  ''' </summary>
  Friend WithEvents Label29 As System.Windows.Forms.Label
  ''' <summary>
  ''' The combo_ review group
  ''' </summary>
  Friend WithEvents Combo_ReviewGroup As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The label30
  ''' </summary>
  Friend WithEvents Label30 As System.Windows.Forms.Label
  ''' <summary>
  ''' The edit administrator code
  ''' </summary>
  Friend WithEvents editAdministratorCode As System.Windows.Forms.TextBox
  ''' <summary>
  ''' The label31
  ''' </summary>
  Friend WithEvents Label31 As System.Windows.Forms.Label
  ''' <summary>
  ''' The tab_ fees
  ''' </summary>
  Friend WithEvents Tab_Fees As System.Windows.Forms.TabPage
  ''' <summary>
  ''' The check_ fund uses equalisation
  ''' </summary>
  Friend WithEvents Check_FundUsesEqualisation As System.Windows.Forms.CheckBox
  ''' <summary>
  ''' The label35
  ''' </summary>
  Friend WithEvents Label35 As System.Windows.Forms.Label
  ''' <summary>
  ''' The combo_ perf fees pay period
  ''' </summary>
  Friend WithEvents Combo_PerfFeesPayPeriod As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The label34
  ''' </summary>
  Friend WithEvents Label34 As System.Windows.Forms.Label
  ''' <summary>
  ''' The combo_ MGMT fees pay period
  ''' </summary>
  Friend WithEvents Combo_MgmtFeesPayPeriod As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The combo_ pricing frequency
  ''' </summary>
  Friend WithEvents Combo_PricingFrequency As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The label37
  ''' </summary>
  Friend WithEvents Label37 As System.Windows.Forms.Label
  ''' <summary>
  ''' The combo_ performance fee instrument
  ''' </summary>
  Friend WithEvents Combo_PerformanceFeeInstrument As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The label36
  ''' </summary>
  Friend WithEvents Label36 As System.Windows.Forms.Label
  ''' <summary>
  ''' The combo_ management fee instrument
  ''' </summary>
  Friend WithEvents Combo_ManagementFeeInstrument As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The label33
  ''' </summary>
  Friend WithEvents Label33 As System.Windows.Forms.Label
  ''' <summary>
  ''' The label38
  ''' </summary>
  Friend WithEvents Label38 As System.Windows.Forms.Label
  ''' <summary>
  ''' The date_ fund year end
  ''' </summary>
  Friend WithEvents Date_FundYearEnd As System.Windows.Forms.DateTimePicker
  ''' <summary>
  ''' The combo_ performance fee crystalised instrument
  ''' </summary>
  Friend WithEvents Combo_PerformanceFeeCrystalisedInstrument As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The label39
  ''' </summary>
  Friend WithEvents Label39 As System.Windows.Forms.Label
  ''' <summary>
  ''' The check_ default trade fx to fund currency
  ''' </summary>
  Friend WithEvents Check_DefaultTradeFxToFundCurrency As System.Windows.Forms.CheckBox
  ''' <summary>
  ''' The combo_ fund report group
  ''' </summary>
  Friend WithEvents Combo_FundReportGroup As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The label40
  ''' </summary>
  Friend WithEvents Label40 As System.Windows.Forms.Label
  ''' <summary>
  ''' The label41
  ''' </summary>
  Friend WithEvents Label41 As System.Windows.Forms.Label
  ''' <summary>
  ''' The edit_ subscription periods to value date
  ''' </summary>
  Friend WithEvents Edit_SubscriptionPeriodsToValueDate As RenaissanceControls.PercentageTextBox
  ''' <summary>
  ''' The label42
  ''' </summary>
  Friend WithEvents Label42 As System.Windows.Forms.Label
  ''' <summary>
  ''' The label43
  ''' </summary>
  Friend WithEvents Label43 As System.Windows.Forms.Label
  ''' <summary>
  ''' The edit_ subscription periods to settlement date
  ''' </summary>
  Friend WithEvents Edit_SubscriptionPeriodsToSettlementDate As RenaissanceControls.PercentageTextBox
  ''' <summary>
  ''' The label44
  ''' </summary>
  Friend WithEvents Label44 As System.Windows.Forms.Label
  ''' <summary>
  ''' The label45
  ''' </summary>
  Friend WithEvents Label45 As System.Windows.Forms.Label
  ''' <summary>
  ''' The edit_ redemption periods to settlement date
  ''' </summary>
  Friend WithEvents Edit_RedemptionPeriodsToSettlementDate As RenaissanceControls.PercentageTextBox
  ''' <summary>
  ''' The edit_ redemption periods to value date
  ''' </summary>
  Friend WithEvents Edit_RedemptionPeriodsToValueDate As RenaissanceControls.PercentageTextBox
  ''' <summary>
  ''' The label46
  ''' </summary>
  Friend WithEvents Label46 As System.Windows.Forms.Label
  ''' <summary>
  ''' The label47
  ''' </summary>
  Friend WithEvents Label47 As System.Windows.Forms.Label
  ''' <summary>
  ''' The check_ fund closed
  ''' </summary>
  Friend WithEvents Check_FundClosed As System.Windows.Forms.CheckBox
  Friend WithEvents Label2 As System.Windows.Forms.Label
  Friend WithEvents Check_ForceMultiSharClassReconciliation As System.Windows.Forms.CheckBox
  Friend WithEvents Check_FundManagementFeesBeforeSubscriptions As System.Windows.Forms.CheckBox
  Friend WithEvents Label53 As System.Windows.Forms.Label
  Friend WithEvents edit_MovementComission As RenaissanceControls.PercentageTextBox
  Friend WithEvents Label3 As System.Windows.Forms.Label
  Friend WithEvents edit_FundNAVReconciliationPlusDays As RenaissanceControls.NumericTextBox
  Friend WithEvents Label4 As System.Windows.Forms.Label
  ''' <summary>
  ''' The BTN close
  ''' </summary>
  Friend WithEvents btnClose As System.Windows.Forms.Button
  ''' <summary>
  ''' Initializes the component.
  ''' </summary>
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.lblFundCode = New System.Windows.Forms.Label
    Me.lblFundName = New System.Windows.Forms.Label
    Me.lblFundLegalEntity = New System.Windows.Forms.Label
    Me.editAuditID = New System.Windows.Forms.TextBox
    Me.editFundCode = New System.Windows.Forms.TextBox
    Me.editFundName = New System.Windows.Forms.TextBox
    Me.editFundLegalEntity = New System.Windows.Forms.TextBox
    Me.lblFundBaseCurrency = New System.Windows.Forms.Label
    Me.lblFundUnit = New System.Windows.Forms.Label
    Me.lblFundParentFund = New System.Windows.Forms.Label
    Me.btnNavFirst = New System.Windows.Forms.Button
    Me.btnNavPrev = New System.Windows.Forms.Button
    Me.btnNavNext = New System.Windows.Forms.Button
    Me.btnLast = New System.Windows.Forms.Button
    Me.btnAdd = New System.Windows.Forms.Button
    Me.btnDelete = New System.Windows.Forms.Button
    Me.btnCancel = New System.Windows.Forms.Button
    Me.btnSave = New System.Windows.Forms.Button
    Me.Combo_SelectFundCode = New System.Windows.Forms.ComboBox
    Me.Panel1 = New System.Windows.Forms.Panel
    Me.Combo_ParentFund = New System.Windows.Forms.ComboBox
    Me.Label1 = New System.Windows.Forms.Label
    Me.GroupBox1 = New System.Windows.Forms.GroupBox
    Me.Combo_FundBaseCurrency = New System.Windows.Forms.ComboBox
    Me.Combo_FundUnit = New System.Windows.Forms.ComboBox
    Me.btnClose = New System.Windows.Forms.Button
    Me.RootMenu = New System.Windows.Forms.MenuStrip
    Me.Tab_FundDetails = New System.Windows.Forms.TabControl
    Me.Tab_Fees = New System.Windows.Forms.TabPage
    Me.Label4 = New System.Windows.Forms.Label
    Me.Label3 = New System.Windows.Forms.Label
    Me.edit_FundNAVReconciliationPlusDays = New RenaissanceControls.NumericTextBox
    Me.Label53 = New System.Windows.Forms.Label
    Me.edit_MovementComission = New RenaissanceControls.PercentageTextBox
    Me.Check_FundManagementFeesBeforeSubscriptions = New System.Windows.Forms.CheckBox
    Me.Check_ForceMultiSharClassReconciliation = New System.Windows.Forms.CheckBox
    Me.Label2 = New System.Windows.Forms.Label
    Me.Combo_PerformanceFeeCrystalisedInstrument = New System.Windows.Forms.ComboBox
    Me.Label39 = New System.Windows.Forms.Label
    Me.Combo_PerformanceFeeInstrument = New System.Windows.Forms.ComboBox
    Me.Label36 = New System.Windows.Forms.Label
    Me.Combo_ManagementFeeInstrument = New System.Windows.Forms.ComboBox
    Me.Label33 = New System.Windows.Forms.Label
    Me.Check_FundUsesEqualisation = New System.Windows.Forms.CheckBox
    Me.Label35 = New System.Windows.Forms.Label
    Me.Combo_PerfFeesPayPeriod = New System.Windows.Forms.ComboBox
    Me.Label34 = New System.Windows.Forms.Label
    Me.Combo_MgmtFeesPayPeriod = New System.Windows.Forms.ComboBox
    Me.Tab_ManagerDetails = New System.Windows.Forms.TabPage
    Me.Combo_ContactEMail = New System.Windows.Forms.ComboBox
    Me.Label27 = New System.Windows.Forms.Label
    Me.Combo_ContactPhone = New System.Windows.Forms.ComboBox
    Me.Label26 = New System.Windows.Forms.Label
    Me.Label25 = New System.Windows.Forms.Label
    Me.Text_ContactAddress = New System.Windows.Forms.TextBox
    Me.Label24 = New System.Windows.Forms.Label
    Me.Text_FundManagers = New System.Windows.Forms.TextBox
    Me.Combo_ManagementCompany = New System.Windows.Forms.ComboBox
    Me.Label23 = New System.Windows.Forms.Label
    Me.Tab_FundTerms = New System.Windows.Forms.TabPage
    Me.Combo_PerformandeFees = New System.Windows.Forms.ComboBox
    Me.Combo_ManagementFees = New System.Windows.Forms.ComboBox
    Me.Label28 = New System.Windows.Forms.Label
    Me.Label29 = New System.Windows.Forms.Label
    Me.Combo_InitialSubscriptionFees = New System.Windows.Forms.ComboBox
    Me.Combo_MinimumInvestment = New System.Windows.Forms.ComboBox
    Me.Label22 = New System.Windows.Forms.Label
    Me.Date_InvestmentStartDate = New System.Windows.Forms.DateTimePicker
    Me.Combo_RedemptionNotice = New System.Windows.Forms.ComboBox
    Me.Label21 = New System.Windows.Forms.Label
    Me.Combo_RedemptionFrequency = New System.Windows.Forms.ComboBox
    Me.Label20 = New System.Windows.Forms.Label
    Me.Combo_SubscriptionNotice = New System.Windows.Forms.ComboBox
    Me.Label19 = New System.Windows.Forms.Label
    Me.Combo_SubscriptionFrequency = New System.Windows.Forms.ComboBox
    Me.Label18 = New System.Windows.Forms.Label
    Me.Label17 = New System.Windows.Forms.Label
    Me.Label16 = New System.Windows.Forms.Label
    Me.Tab_Legal = New System.Windows.Forms.TabPage
    Me.Text_Reuters = New System.Windows.Forms.TextBox
    Me.Label14 = New System.Windows.Forms.Label
    Me.Text_Bloomberg = New System.Windows.Forms.TextBox
    Me.Label15 = New System.Windows.Forms.Label
    Me.Text_SEDOL = New System.Windows.Forms.TextBox
    Me.Label13 = New System.Windows.Forms.Label
    Me.Text_ISIN = New System.Windows.Forms.TextBox
    Me.Label12 = New System.Windows.Forms.Label
    Me.Combo_ListedInstrument = New System.Windows.Forms.ComboBox
    Me.Label11 = New System.Windows.Forms.Label
    Me.Combo_Listings = New System.Windows.Forms.ComboBox
    Me.Label10 = New System.Windows.Forms.Label
    Me.Combo_Custodian = New System.Windows.Forms.ComboBox
    Me.Label9 = New System.Windows.Forms.Label
    Me.Combo_Administrator = New System.Windows.Forms.ComboBox
    Me.Label8 = New System.Windows.Forms.Label
    Me.Combo_Jurisdiction = New System.Windows.Forms.ComboBox
    Me.Label7 = New System.Windows.Forms.Label
    Me.Tab_Objective = New System.Windows.Forms.TabPage
    Me.Label6 = New System.Windows.Forms.Label
    Me.Label5 = New System.Windows.Forms.Label
    Me.Text_Disclaimer = New System.Windows.Forms.TextBox
    Me.Text_Objective = New System.Windows.Forms.TextBox
    Me.Combo_ReviewGroup = New System.Windows.Forms.ComboBox
    Me.Label30 = New System.Windows.Forms.Label
    Me.editAdministratorCode = New System.Windows.Forms.TextBox
    Me.Label31 = New System.Windows.Forms.Label
    Me.Combo_PricingFrequency = New System.Windows.Forms.ComboBox
    Me.Label37 = New System.Windows.Forms.Label
    Me.Label38 = New System.Windows.Forms.Label
    Me.Date_FundYearEnd = New System.Windows.Forms.DateTimePicker
    Me.Check_DefaultTradeFxToFundCurrency = New System.Windows.Forms.CheckBox
    Me.Combo_FundReportGroup = New System.Windows.Forms.ComboBox
    Me.Label40 = New System.Windows.Forms.Label
    Me.Label41 = New System.Windows.Forms.Label
    Me.Edit_SubscriptionPeriodsToValueDate = New RenaissanceControls.PercentageTextBox
    Me.Label42 = New System.Windows.Forms.Label
    Me.Label43 = New System.Windows.Forms.Label
    Me.Edit_SubscriptionPeriodsToSettlementDate = New RenaissanceControls.PercentageTextBox
    Me.Label44 = New System.Windows.Forms.Label
    Me.Label45 = New System.Windows.Forms.Label
    Me.Edit_RedemptionPeriodsToSettlementDate = New RenaissanceControls.PercentageTextBox
    Me.Edit_RedemptionPeriodsToValueDate = New RenaissanceControls.PercentageTextBox
    Me.Label46 = New System.Windows.Forms.Label
    Me.Label47 = New System.Windows.Forms.Label
    Me.Check_FundClosed = New System.Windows.Forms.CheckBox
    Me.Panel1.SuspendLayout()
    Me.Tab_FundDetails.SuspendLayout()
    Me.Tab_Fees.SuspendLayout()
    Me.Tab_ManagerDetails.SuspendLayout()
    Me.Tab_FundTerms.SuspendLayout()
    Me.Tab_Legal.SuspendLayout()
    Me.Tab_Objective.SuspendLayout()
    Me.SuspendLayout()
    '
    'lblFundCode
    '
    Me.lblFundCode.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.lblFundCode.Location = New System.Drawing.Point(9, 79)
    Me.lblFundCode.Name = "lblFundCode"
    Me.lblFundCode.Size = New System.Drawing.Size(100, 17)
    Me.lblFundCode.TabIndex = 39
    Me.lblFundCode.Text = "Fund Code"
    '
    'lblFundName
    '
    Me.lblFundName.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.lblFundName.Location = New System.Drawing.Point(9, 105)
    Me.lblFundName.Name = "lblFundName"
    Me.lblFundName.Size = New System.Drawing.Size(100, 17)
    Me.lblFundName.TabIndex = 40
    Me.lblFundName.Text = "Fund Name"
    '
    'lblFundLegalEntity
    '
    Me.lblFundLegalEntity.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.lblFundLegalEntity.Location = New System.Drawing.Point(9, 131)
    Me.lblFundLegalEntity.Name = "lblFundLegalEntity"
    Me.lblFundLegalEntity.Size = New System.Drawing.Size(100, 17)
    Me.lblFundLegalEntity.TabIndex = 41
    Me.lblFundLegalEntity.Text = "Fund Legal Entity"
    '
    'editAuditID
    '
    Me.editAuditID.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.editAuditID.Enabled = False
    Me.editAuditID.Location = New System.Drawing.Point(573, 32)
    Me.editAuditID.Name = "editAuditID"
    Me.editAuditID.Size = New System.Drawing.Size(64, 20)
    Me.editAuditID.TabIndex = 1
    '
    'editFundCode
    '
    Me.editFundCode.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.editFundCode.Location = New System.Drawing.Point(120, 76)
    Me.editFundCode.Name = "editFundCode"
    Me.editFundCode.Size = New System.Drawing.Size(289, 20)
    Me.editFundCode.TabIndex = 2
    '
    'editFundName
    '
    Me.editFundName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.editFundName.Location = New System.Drawing.Point(120, 102)
    Me.editFundName.Name = "editFundName"
    Me.editFundName.Size = New System.Drawing.Size(516, 20)
    Me.editFundName.TabIndex = 4
    '
    'editFundLegalEntity
    '
    Me.editFundLegalEntity.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.editFundLegalEntity.Location = New System.Drawing.Point(120, 128)
    Me.editFundLegalEntity.Name = "editFundLegalEntity"
    Me.editFundLegalEntity.Size = New System.Drawing.Size(516, 20)
    Me.editFundLegalEntity.TabIndex = 5
    '
    'lblFundBaseCurrency
    '
    Me.lblFundBaseCurrency.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.lblFundBaseCurrency.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.lblFundBaseCurrency.Location = New System.Drawing.Point(440, 79)
    Me.lblFundBaseCurrency.Name = "lblFundBaseCurrency"
    Me.lblFundBaseCurrency.Size = New System.Drawing.Size(106, 18)
    Me.lblFundBaseCurrency.TabIndex = 48
    Me.lblFundBaseCurrency.Text = "Fund Base Currency"
    '
    'lblFundUnit
    '
    Me.lblFundUnit.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.lblFundUnit.Location = New System.Drawing.Point(9, 157)
    Me.lblFundUnit.Name = "lblFundUnit"
    Me.lblFundUnit.Size = New System.Drawing.Size(106, 18)
    Me.lblFundUnit.TabIndex = 49
    Me.lblFundUnit.Text = "Fund Unit"
    '
    'lblFundParentFund
    '
    Me.lblFundParentFund.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.lblFundParentFund.Location = New System.Drawing.Point(9, 184)
    Me.lblFundParentFund.Name = "lblFundParentFund"
    Me.lblFundParentFund.Size = New System.Drawing.Size(106, 18)
    Me.lblFundParentFund.TabIndex = 50
    Me.lblFundParentFund.Text = "Fund Parent Fund"
    '
    'btnNavFirst
    '
    Me.btnNavFirst.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNavFirst.Location = New System.Drawing.Point(8, 8)
    Me.btnNavFirst.Name = "btnNavFirst"
    Me.btnNavFirst.Size = New System.Drawing.Size(40, 28)
    Me.btnNavFirst.TabIndex = 0
    Me.btnNavFirst.Text = "<<"
    '
    'btnNavPrev
    '
    Me.btnNavPrev.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNavPrev.Location = New System.Drawing.Point(49, 8)
    Me.btnNavPrev.Name = "btnNavPrev"
    Me.btnNavPrev.Size = New System.Drawing.Size(35, 28)
    Me.btnNavPrev.TabIndex = 1
    Me.btnNavPrev.Text = "<"
    '
    'btnNavNext
    '
    Me.btnNavNext.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNavNext.Location = New System.Drawing.Point(88, 8)
    Me.btnNavNext.Name = "btnNavNext"
    Me.btnNavNext.Size = New System.Drawing.Size(35, 28)
    Me.btnNavNext.TabIndex = 2
    Me.btnNavNext.Text = ">"
    '
    'btnLast
    '
    Me.btnLast.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnLast.Location = New System.Drawing.Point(124, 8)
    Me.btnLast.Name = "btnLast"
    Me.btnLast.Size = New System.Drawing.Size(40, 28)
    Me.btnLast.TabIndex = 3
    Me.btnLast.Text = ">>"
    '
    'btnAdd
    '
    Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnAdd.Location = New System.Drawing.Point(176, 8)
    Me.btnAdd.Name = "btnAdd"
    Me.btnAdd.Size = New System.Drawing.Size(75, 28)
    Me.btnAdd.TabIndex = 4
    Me.btnAdd.Text = "&New"
    '
    'btnDelete
    '
    Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnDelete.Location = New System.Drawing.Point(246, 728)
    Me.btnDelete.Name = "btnDelete"
    Me.btnDelete.Size = New System.Drawing.Size(75, 28)
    Me.btnDelete.TabIndex = 21
    Me.btnDelete.Text = "&Delete"
    '
    'btnCancel
    '
    Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnCancel.Location = New System.Drawing.Point(330, 728)
    Me.btnCancel.Name = "btnCancel"
    Me.btnCancel.Size = New System.Drawing.Size(75, 28)
    Me.btnCancel.TabIndex = 22
    Me.btnCancel.Text = "&Cancel"
    '
    'btnSave
    '
    Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnSave.Location = New System.Drawing.Point(158, 728)
    Me.btnSave.Name = "btnSave"
    Me.btnSave.Size = New System.Drawing.Size(75, 28)
    Me.btnSave.TabIndex = 20
    Me.btnSave.Text = "&Save"
    '
    'Combo_SelectFundCode
    '
    Me.Combo_SelectFundCode.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_SelectFundCode.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectFundCode.Location = New System.Drawing.Point(120, 32)
    Me.Combo_SelectFundCode.Name = "Combo_SelectFundCode"
    Me.Combo_SelectFundCode.Size = New System.Drawing.Size(429, 21)
    Me.Combo_SelectFundCode.TabIndex = 0
    '
    'Panel1
    '
    Me.Panel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Panel1.Controls.Add(Me.btnNavFirst)
    Me.Panel1.Controls.Add(Me.btnNavPrev)
    Me.Panel1.Controls.Add(Me.btnNavNext)
    Me.Panel1.Controls.Add(Me.btnLast)
    Me.Panel1.Controls.Add(Me.btnAdd)
    Me.Panel1.Location = New System.Drawing.Point(194, 674)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(260, 48)
    Me.Panel1.TabIndex = 19
    '
    'Combo_ParentFund
    '
    Me.Combo_ParentFund.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_ParentFund.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_ParentFund.Location = New System.Drawing.Point(120, 181)
    Me.Combo_ParentFund.Name = "Combo_ParentFund"
    Me.Combo_ParentFund.Size = New System.Drawing.Size(516, 21)
    Me.Combo_ParentFund.TabIndex = 7
    '
    'Label1
    '
    Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label1.Location = New System.Drawing.Point(9, 32)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(100, 23)
    Me.Label1.TabIndex = 25
    Me.Label1.Text = "Select"
    '
    'GroupBox1
    '
    Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.GroupBox1.Location = New System.Drawing.Point(8, 64)
    Me.GroupBox1.Name = "GroupBox1"
    Me.GroupBox1.Size = New System.Drawing.Size(627, 4)
    Me.GroupBox1.TabIndex = 77
    Me.GroupBox1.TabStop = False
    Me.GroupBox1.Text = "GroupBox1"
    '
    'Combo_FundBaseCurrency
    '
    Me.Combo_FundBaseCurrency.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_FundBaseCurrency.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_FundBaseCurrency.Location = New System.Drawing.Point(552, 76)
    Me.Combo_FundBaseCurrency.Name = "Combo_FundBaseCurrency"
    Me.Combo_FundBaseCurrency.Size = New System.Drawing.Size(84, 21)
    Me.Combo_FundBaseCurrency.TabIndex = 3
    '
    'Combo_FundUnit
    '
    Me.Combo_FundUnit.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_FundUnit.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_FundUnit.Location = New System.Drawing.Point(120, 154)
    Me.Combo_FundUnit.Name = "Combo_FundUnit"
    Me.Combo_FundUnit.Size = New System.Drawing.Size(516, 21)
    Me.Combo_FundUnit.TabIndex = 6
    '
    'btnClose
    '
    Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnClose.Location = New System.Drawing.Point(414, 728)
    Me.btnClose.Name = "btnClose"
    Me.btnClose.Size = New System.Drawing.Size(75, 28)
    Me.btnClose.TabIndex = 23
    Me.btnClose.Text = "&Close"
    '
    'RootMenu
    '
    Me.RootMenu.Location = New System.Drawing.Point(0, 0)
    Me.RootMenu.Name = "RootMenu"
    Me.RootMenu.Size = New System.Drawing.Size(645, 24)
    Me.RootMenu.TabIndex = 24
    Me.RootMenu.Text = " "
    '
    'Tab_FundDetails
    '
    Me.Tab_FundDetails.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Tab_FundDetails.Controls.Add(Me.Tab_Fees)
    Me.Tab_FundDetails.Controls.Add(Me.Tab_ManagerDetails)
    Me.Tab_FundDetails.Controls.Add(Me.Tab_FundTerms)
    Me.Tab_FundDetails.Controls.Add(Me.Tab_Legal)
    Me.Tab_FundDetails.Controls.Add(Me.Tab_Objective)
    Me.Tab_FundDetails.Location = New System.Drawing.Point(8, 359)
    Me.Tab_FundDetails.Name = "Tab_FundDetails"
    Me.Tab_FundDetails.SelectedIndex = 0
    Me.Tab_FundDetails.Size = New System.Drawing.Size(629, 300)
    Me.Tab_FundDetails.TabIndex = 18
    '
    'Tab_Fees
    '
    Me.Tab_Fees.BackColor = System.Drawing.SystemColors.Control
    Me.Tab_Fees.Controls.Add(Me.Label4)
    Me.Tab_Fees.Controls.Add(Me.Label3)
    Me.Tab_Fees.Controls.Add(Me.edit_FundNAVReconciliationPlusDays)
    Me.Tab_Fees.Controls.Add(Me.Label53)
    Me.Tab_Fees.Controls.Add(Me.edit_MovementComission)
    Me.Tab_Fees.Controls.Add(Me.Check_FundManagementFeesBeforeSubscriptions)
    Me.Tab_Fees.Controls.Add(Me.Check_ForceMultiSharClassReconciliation)
    Me.Tab_Fees.Controls.Add(Me.Label2)
    Me.Tab_Fees.Controls.Add(Me.Combo_PerformanceFeeCrystalisedInstrument)
    Me.Tab_Fees.Controls.Add(Me.Label39)
    Me.Tab_Fees.Controls.Add(Me.Combo_PerformanceFeeInstrument)
    Me.Tab_Fees.Controls.Add(Me.Label36)
    Me.Tab_Fees.Controls.Add(Me.Combo_ManagementFeeInstrument)
    Me.Tab_Fees.Controls.Add(Me.Label33)
    Me.Tab_Fees.Controls.Add(Me.Check_FundUsesEqualisation)
    Me.Tab_Fees.Controls.Add(Me.Label35)
    Me.Tab_Fees.Controls.Add(Me.Combo_PerfFeesPayPeriod)
    Me.Tab_Fees.Controls.Add(Me.Label34)
    Me.Tab_Fees.Controls.Add(Me.Combo_MgmtFeesPayPeriod)
    Me.Tab_Fees.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Fees.Name = "Tab_Fees"
    Me.Tab_Fees.Padding = New System.Windows.Forms.Padding(3)
    Me.Tab_Fees.Size = New System.Drawing.Size(621, 274)
    Me.Tab_Fees.TabIndex = 4
    Me.Tab_Fees.Text = "Fees"
    '
    'Label4
    '
    Me.Label4.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label4.Location = New System.Drawing.Point(315, 193)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(246, 17)
    Me.Label4.TabIndex = 153
    Me.Label4.Text = "Days"
    '
    'Label3
    '
    Me.Label3.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label3.Location = New System.Drawing.Point(112, 193)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(107, 17)
    Me.Label3.TabIndex = 152
    Me.Label3.Text = "Reconciliation dalay"
    '
    'edit_FundNAVReconciliationPlusDays
    '
    Me.edit_FundNAVReconciliationPlusDays.Location = New System.Drawing.Point(225, 190)
    Me.edit_FundNAVReconciliationPlusDays.Name = "edit_FundNAVReconciliationPlusDays"
    Me.edit_FundNAVReconciliationPlusDays.RenaissanceTag = Nothing
    Me.edit_FundNAVReconciliationPlusDays.SelectTextOnFocus = False
    Me.edit_FundNAVReconciliationPlusDays.Size = New System.Drawing.Size(84, 20)
    Me.edit_FundNAVReconciliationPlusDays.TabIndex = 7
    Me.edit_FundNAVReconciliationPlusDays.Text = "1"
    Me.edit_FundNAVReconciliationPlusDays.TextFormat = "#,##0"
    Me.edit_FundNAVReconciliationPlusDays.Value = 1
    '
    'Label53
    '
    Me.Label53.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label53.Location = New System.Drawing.Point(113, 167)
    Me.Label53.Name = "Label53"
    Me.Label53.Size = New System.Drawing.Size(110, 17)
    Me.Label53.TabIndex = 150
    Me.Label53.Text = "Movement Comission"
    '
    'edit_MovementComission
    '
    Me.edit_MovementComission.Location = New System.Drawing.Point(225, 164)
    Me.edit_MovementComission.Name = "edit_MovementComission"
    Me.edit_MovementComission.RenaissanceTag = Nothing
    Me.edit_MovementComission.SelectTextOnFocus = False
    Me.edit_MovementComission.Size = New System.Drawing.Size(84, 20)
    Me.edit_MovementComission.TabIndex = 6
    Me.edit_MovementComission.Text = "0.00%"
    Me.edit_MovementComission.TextFormat = "#,##0.00%"
    Me.edit_MovementComission.Value = 0
    '
    'Check_FundManagementFeesBeforeSubscriptions
    '
    Me.Check_FundManagementFeesBeforeSubscriptions.AutoSize = True
    Me.Check_FundManagementFeesBeforeSubscriptions.Location = New System.Drawing.Point(20, 245)
    Me.Check_FundManagementFeesBeforeSubscriptions.Name = "Check_FundManagementFeesBeforeSubscriptions"
    Me.Check_FundManagementFeesBeforeSubscriptions.Size = New System.Drawing.Size(276, 17)
    Me.Check_FundManagementFeesBeforeSubscriptions.TabIndex = 9
    Me.Check_FundManagementFeesBeforeSubscriptions.Text = "Take Mgmt fees Before Subscriptions / Redemptions"
    Me.Check_FundManagementFeesBeforeSubscriptions.UseVisualStyleBackColor = True
    '
    'Check_ForceMultiSharClassReconciliation
    '
    Me.Check_ForceMultiSharClassReconciliation.AutoSize = True
    Me.Check_ForceMultiSharClassReconciliation.Location = New System.Drawing.Point(20, 222)
    Me.Check_ForceMultiSharClassReconciliation.Name = "Check_ForceMultiSharClassReconciliation"
    Me.Check_ForceMultiSharClassReconciliation.Size = New System.Drawing.Size(211, 17)
    Me.Check_ForceMultiSharClassReconciliation.TabIndex = 8
    Me.Check_ForceMultiSharClassReconciliation.Text = "Force Multi-Class reconciliation process"
    Me.Check_ForceMultiSharClassReconciliation.UseVisualStyleBackColor = True
    '
    'Label2
    '
    Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label2.BackColor = System.Drawing.SystemColors.Control
    Me.Label2.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label2.Location = New System.Drawing.Point(222, 94)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(393, 17)
    Me.Label2.TabIndex = 3
    Me.Label2.Text = "See 'Unit' Instrument(s) for fee rates."
    '
    'Combo_PerformanceFeeCrystalisedInstrument
    '
    Me.Combo_PerformanceFeeCrystalisedInstrument.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_PerformanceFeeCrystalisedInstrument.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_PerformanceFeeCrystalisedInstrument.Location = New System.Drawing.Point(225, 65)
    Me.Combo_PerformanceFeeCrystalisedInstrument.Name = "Combo_PerformanceFeeCrystalisedInstrument"
    Me.Combo_PerformanceFeeCrystalisedInstrument.Size = New System.Drawing.Size(390, 21)
    Me.Combo_PerformanceFeeCrystalisedInstrument.TabIndex = 2
    '
    'Label39
    '
    Me.Label39.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label39.Location = New System.Drawing.Point(6, 68)
    Me.Label39.Name = "Label39"
    Me.Label39.Size = New System.Drawing.Size(213, 18)
    Me.Label39.TabIndex = 101
    Me.Label39.Text = "Crystalised Performance Instrument"
    '
    'Combo_PerformanceFeeInstrument
    '
    Me.Combo_PerformanceFeeInstrument.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_PerformanceFeeInstrument.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_PerformanceFeeInstrument.Location = New System.Drawing.Point(225, 38)
    Me.Combo_PerformanceFeeInstrument.Name = "Combo_PerformanceFeeInstrument"
    Me.Combo_PerformanceFeeInstrument.Size = New System.Drawing.Size(390, 21)
    Me.Combo_PerformanceFeeInstrument.TabIndex = 1
    '
    'Label36
    '
    Me.Label36.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label36.Location = New System.Drawing.Point(6, 41)
    Me.Label36.Name = "Label36"
    Me.Label36.Size = New System.Drawing.Size(213, 18)
    Me.Label36.TabIndex = 99
    Me.Label36.Text = "Performance Fee Instrument"
    '
    'Combo_ManagementFeeInstrument
    '
    Me.Combo_ManagementFeeInstrument.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_ManagementFeeInstrument.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_ManagementFeeInstrument.Location = New System.Drawing.Point(225, 11)
    Me.Combo_ManagementFeeInstrument.Name = "Combo_ManagementFeeInstrument"
    Me.Combo_ManagementFeeInstrument.Size = New System.Drawing.Size(390, 21)
    Me.Combo_ManagementFeeInstrument.TabIndex = 0
    '
    'Label33
    '
    Me.Label33.BackColor = System.Drawing.SystemColors.Control
    Me.Label33.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label33.Location = New System.Drawing.Point(6, 14)
    Me.Label33.Name = "Label33"
    Me.Label33.Size = New System.Drawing.Size(213, 18)
    Me.Label33.TabIndex = 97
    Me.Label33.Text = "Management Fee Instrument"
    '
    'Check_FundUsesEqualisation
    '
    Me.Check_FundUsesEqualisation.Location = New System.Drawing.Point(369, 222)
    Me.Check_FundUsesEqualisation.Name = "Check_FundUsesEqualisation"
    Me.Check_FundUsesEqualisation.Size = New System.Drawing.Size(40, 17)
    Me.Check_FundUsesEqualisation.TabIndex = 10
    Me.Check_FundUsesEqualisation.Text = "Fund Uses Equalisation"
    Me.Check_FundUsesEqualisation.UseVisualStyleBackColor = True
    Me.Check_FundUsesEqualisation.Visible = False
    '
    'Label35
    '
    Me.Label35.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label35.Location = New System.Drawing.Point(113, 141)
    Me.Label35.Name = "Label35"
    Me.Label35.Size = New System.Drawing.Size(106, 17)
    Me.Label35.TabIndex = 95
    Me.Label35.Text = "Perf. fees paid :"
    '
    'Combo_PerfFeesPayPeriod
    '
    Me.Combo_PerfFeesPayPeriod.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_PerfFeesPayPeriod.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_PerfFeesPayPeriod.Location = New System.Drawing.Point(225, 138)
    Me.Combo_PerfFeesPayPeriod.Name = "Combo_PerfFeesPayPeriod"
    Me.Combo_PerfFeesPayPeriod.Size = New System.Drawing.Size(281, 21)
    Me.Combo_PerfFeesPayPeriod.TabIndex = 5
    '
    'Label34
    '
    Me.Label34.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label34.Location = New System.Drawing.Point(113, 115)
    Me.Label34.Name = "Label34"
    Me.Label34.Size = New System.Drawing.Size(106, 17)
    Me.Label34.TabIndex = 91
    Me.Label34.Text = "Mgmt fees paid :"
    '
    'Combo_MgmtFeesPayPeriod
    '
    Me.Combo_MgmtFeesPayPeriod.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_MgmtFeesPayPeriod.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_MgmtFeesPayPeriod.Location = New System.Drawing.Point(225, 112)
    Me.Combo_MgmtFeesPayPeriod.Name = "Combo_MgmtFeesPayPeriod"
    Me.Combo_MgmtFeesPayPeriod.Size = New System.Drawing.Size(281, 21)
    Me.Combo_MgmtFeesPayPeriod.TabIndex = 4
    '
    'Tab_ManagerDetails
    '
    Me.Tab_ManagerDetails.BackColor = System.Drawing.SystemColors.Control
    Me.Tab_ManagerDetails.Controls.Add(Me.Combo_ContactEMail)
    Me.Tab_ManagerDetails.Controls.Add(Me.Label27)
    Me.Tab_ManagerDetails.Controls.Add(Me.Combo_ContactPhone)
    Me.Tab_ManagerDetails.Controls.Add(Me.Label26)
    Me.Tab_ManagerDetails.Controls.Add(Me.Label25)
    Me.Tab_ManagerDetails.Controls.Add(Me.Text_ContactAddress)
    Me.Tab_ManagerDetails.Controls.Add(Me.Label24)
    Me.Tab_ManagerDetails.Controls.Add(Me.Text_FundManagers)
    Me.Tab_ManagerDetails.Controls.Add(Me.Combo_ManagementCompany)
    Me.Tab_ManagerDetails.Controls.Add(Me.Label23)
    Me.Tab_ManagerDetails.Location = New System.Drawing.Point(4, 22)
    Me.Tab_ManagerDetails.Name = "Tab_ManagerDetails"
    Me.Tab_ManagerDetails.Padding = New System.Windows.Forms.Padding(3)
    Me.Tab_ManagerDetails.Size = New System.Drawing.Size(621, 274)
    Me.Tab_ManagerDetails.TabIndex = 0
    Me.Tab_ManagerDetails.Text = "Manager Details"
    '
    'Combo_ContactEMail
    '
    Me.Combo_ContactEMail.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_ContactEMail.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_ContactEMail.Location = New System.Drawing.Point(108, 244)
    Me.Combo_ContactEMail.Name = "Combo_ContactEMail"
    Me.Combo_ContactEMail.Size = New System.Drawing.Size(510, 21)
    Me.Combo_ContactEMail.TabIndex = 4
    '
    'Label27
    '
    Me.Label27.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label27.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label27.Location = New System.Drawing.Point(3, 247)
    Me.Label27.Name = "Label27"
    Me.Label27.Size = New System.Drawing.Size(99, 17)
    Me.Label27.TabIndex = 96
    Me.Label27.Text = "Contact e-mail"
    '
    'Combo_ContactPhone
    '
    Me.Combo_ContactPhone.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_ContactPhone.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_ContactPhone.Location = New System.Drawing.Point(108, 217)
    Me.Combo_ContactPhone.Name = "Combo_ContactPhone"
    Me.Combo_ContactPhone.Size = New System.Drawing.Size(510, 21)
    Me.Combo_ContactPhone.TabIndex = 3
    '
    'Label26
    '
    Me.Label26.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label26.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label26.Location = New System.Drawing.Point(3, 220)
    Me.Label26.Name = "Label26"
    Me.Label26.Size = New System.Drawing.Size(99, 17)
    Me.Label26.TabIndex = 94
    Me.Label26.Text = "Contact Phone"
    '
    'Label25
    '
    Me.Label25.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label25.Location = New System.Drawing.Point(3, 91)
    Me.Label25.Name = "Label25"
    Me.Label25.Size = New System.Drawing.Size(99, 17)
    Me.Label25.TabIndex = 93
    Me.Label25.Text = "Contact Address"
    '
    'Text_ContactAddress
    '
    Me.Text_ContactAddress.AcceptsReturn = True
    Me.Text_ContactAddress.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Text_ContactAddress.Location = New System.Drawing.Point(108, 88)
    Me.Text_ContactAddress.Multiline = True
    Me.Text_ContactAddress.Name = "Text_ContactAddress"
    Me.Text_ContactAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.Text_ContactAddress.Size = New System.Drawing.Size(510, 123)
    Me.Text_ContactAddress.TabIndex = 2
    '
    'Label24
    '
    Me.Label24.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label24.Location = New System.Drawing.Point(3, 36)
    Me.Label24.Name = "Label24"
    Me.Label24.Size = New System.Drawing.Size(99, 17)
    Me.Label24.TabIndex = 91
    Me.Label24.Text = "Fund Manager(s)"
    '
    'Text_FundManagers
    '
    Me.Text_FundManagers.AcceptsReturn = True
    Me.Text_FundManagers.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Text_FundManagers.Location = New System.Drawing.Point(108, 33)
    Me.Text_FundManagers.Multiline = True
    Me.Text_FundManagers.Name = "Text_FundManagers"
    Me.Text_FundManagers.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.Text_FundManagers.Size = New System.Drawing.Size(510, 49)
    Me.Text_FundManagers.TabIndex = 1
    '
    'Combo_ManagementCompany
    '
    Me.Combo_ManagementCompany.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_ManagementCompany.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_ManagementCompany.Location = New System.Drawing.Point(108, 6)
    Me.Combo_ManagementCompany.Name = "Combo_ManagementCompany"
    Me.Combo_ManagementCompany.Size = New System.Drawing.Size(510, 21)
    Me.Combo_ManagementCompany.TabIndex = 0
    '
    'Label23
    '
    Me.Label23.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label23.Location = New System.Drawing.Point(3, 9)
    Me.Label23.Name = "Label23"
    Me.Label23.Size = New System.Drawing.Size(99, 17)
    Me.Label23.TabIndex = 88
    Me.Label23.Text = "Management Co."
    '
    'Tab_FundTerms
    '
    Me.Tab_FundTerms.BackColor = System.Drawing.SystemColors.Control
    Me.Tab_FundTerms.Controls.Add(Me.Combo_PerformandeFees)
    Me.Tab_FundTerms.Controls.Add(Me.Combo_ManagementFees)
    Me.Tab_FundTerms.Controls.Add(Me.Label28)
    Me.Tab_FundTerms.Controls.Add(Me.Label29)
    Me.Tab_FundTerms.Controls.Add(Me.Combo_InitialSubscriptionFees)
    Me.Tab_FundTerms.Controls.Add(Me.Combo_MinimumInvestment)
    Me.Tab_FundTerms.Controls.Add(Me.Label22)
    Me.Tab_FundTerms.Controls.Add(Me.Date_InvestmentStartDate)
    Me.Tab_FundTerms.Controls.Add(Me.Combo_RedemptionNotice)
    Me.Tab_FundTerms.Controls.Add(Me.Label21)
    Me.Tab_FundTerms.Controls.Add(Me.Combo_RedemptionFrequency)
    Me.Tab_FundTerms.Controls.Add(Me.Label20)
    Me.Tab_FundTerms.Controls.Add(Me.Combo_SubscriptionNotice)
    Me.Tab_FundTerms.Controls.Add(Me.Label19)
    Me.Tab_FundTerms.Controls.Add(Me.Combo_SubscriptionFrequency)
    Me.Tab_FundTerms.Controls.Add(Me.Label18)
    Me.Tab_FundTerms.Controls.Add(Me.Label17)
    Me.Tab_FundTerms.Controls.Add(Me.Label16)
    Me.Tab_FundTerms.Location = New System.Drawing.Point(4, 22)
    Me.Tab_FundTerms.Name = "Tab_FundTerms"
    Me.Tab_FundTerms.Padding = New System.Windows.Forms.Padding(3)
    Me.Tab_FundTerms.Size = New System.Drawing.Size(621, 274)
    Me.Tab_FundTerms.TabIndex = 1
    Me.Tab_FundTerms.Text = "Fund Terms"
    '
    'Combo_PerformandeFees
    '
    Me.Combo_PerformandeFees.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_PerformandeFees.Location = New System.Drawing.Point(131, 113)
    Me.Combo_PerformandeFees.Name = "Combo_PerformandeFees"
    Me.Combo_PerformandeFees.Size = New System.Drawing.Size(175, 21)
    Me.Combo_PerformandeFees.TabIndex = 4
    Me.Combo_PerformandeFees.Visible = False
    '
    'Combo_ManagementFees
    '
    Me.Combo_ManagementFees.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_ManagementFees.Location = New System.Drawing.Point(131, 87)
    Me.Combo_ManagementFees.Name = "Combo_ManagementFees"
    Me.Combo_ManagementFees.Size = New System.Drawing.Size(175, 21)
    Me.Combo_ManagementFees.TabIndex = 3
    Me.Combo_ManagementFees.Visible = False
    '
    'Label28
    '
    Me.Label28.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label28.Location = New System.Drawing.Point(6, 116)
    Me.Label28.Name = "Label28"
    Me.Label28.Size = New System.Drawing.Size(122, 14)
    Me.Label28.TabIndex = 108
    Me.Label28.Text = "Performance Fees"
    Me.Label28.Visible = False
    '
    'Label29
    '
    Me.Label29.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label29.Location = New System.Drawing.Point(6, 90)
    Me.Label29.Name = "Label29"
    Me.Label29.Size = New System.Drawing.Size(122, 14)
    Me.Label29.TabIndex = 107
    Me.Label29.Text = "Management Fees"
    Me.Label29.Visible = False
    '
    'Combo_InitialSubscriptionFees
    '
    Me.Combo_InitialSubscriptionFees.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_InitialSubscriptionFees.Location = New System.Drawing.Point(131, 60)
    Me.Combo_InitialSubscriptionFees.Name = "Combo_InitialSubscriptionFees"
    Me.Combo_InitialSubscriptionFees.Size = New System.Drawing.Size(175, 21)
    Me.Combo_InitialSubscriptionFees.TabIndex = 2
    '
    'Combo_MinimumInvestment
    '
    Me.Combo_MinimumInvestment.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_MinimumInvestment.Location = New System.Drawing.Point(131, 33)
    Me.Combo_MinimumInvestment.Name = "Combo_MinimumInvestment"
    Me.Combo_MinimumInvestment.Size = New System.Drawing.Size(175, 21)
    Me.Combo_MinimumInvestment.TabIndex = 1
    '
    'Label22
    '
    Me.Label22.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label22.Location = New System.Drawing.Point(6, 10)
    Me.Label22.Name = "Label22"
    Me.Label22.Size = New System.Drawing.Size(122, 17)
    Me.Label22.TabIndex = 104
    Me.Label22.Text = "Investment Start Date"
    '
    'Date_InvestmentStartDate
    '
    Me.Date_InvestmentStartDate.Location = New System.Drawing.Point(131, 6)
    Me.Date_InvestmentStartDate.Name = "Date_InvestmentStartDate"
    Me.Date_InvestmentStartDate.Size = New System.Drawing.Size(175, 20)
    Me.Date_InvestmentStartDate.TabIndex = 0
    '
    'Combo_RedemptionNotice
    '
    Me.Combo_RedemptionNotice.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_RedemptionNotice.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_RedemptionNotice.Location = New System.Drawing.Point(438, 87)
    Me.Combo_RedemptionNotice.Name = "Combo_RedemptionNotice"
    Me.Combo_RedemptionNotice.Size = New System.Drawing.Size(177, 21)
    Me.Combo_RedemptionNotice.TabIndex = 8
    '
    'Label21
    '
    Me.Label21.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label21.Location = New System.Drawing.Point(313, 90)
    Me.Label21.Name = "Label21"
    Me.Label21.Size = New System.Drawing.Size(120, 17)
    Me.Label21.TabIndex = 100
    Me.Label21.Text = "Redemption Notice"
    '
    'Combo_RedemptionFrequency
    '
    Me.Combo_RedemptionFrequency.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_RedemptionFrequency.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_RedemptionFrequency.Location = New System.Drawing.Point(438, 60)
    Me.Combo_RedemptionFrequency.Name = "Combo_RedemptionFrequency"
    Me.Combo_RedemptionFrequency.Size = New System.Drawing.Size(177, 21)
    Me.Combo_RedemptionFrequency.TabIndex = 7
    '
    'Label20
    '
    Me.Label20.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label20.Location = New System.Drawing.Point(313, 63)
    Me.Label20.Name = "Label20"
    Me.Label20.Size = New System.Drawing.Size(120, 17)
    Me.Label20.TabIndex = 98
    Me.Label20.Text = "Redemption Frequency"
    '
    'Combo_SubscriptionNotice
    '
    Me.Combo_SubscriptionNotice.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_SubscriptionNotice.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SubscriptionNotice.Location = New System.Drawing.Point(438, 33)
    Me.Combo_SubscriptionNotice.Name = "Combo_SubscriptionNotice"
    Me.Combo_SubscriptionNotice.Size = New System.Drawing.Size(177, 21)
    Me.Combo_SubscriptionNotice.TabIndex = 6
    '
    'Label19
    '
    Me.Label19.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label19.Location = New System.Drawing.Point(313, 36)
    Me.Label19.Name = "Label19"
    Me.Label19.Size = New System.Drawing.Size(120, 17)
    Me.Label19.TabIndex = 96
    Me.Label19.Text = "Subscription Notice"
    '
    'Combo_SubscriptionFrequency
    '
    Me.Combo_SubscriptionFrequency.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_SubscriptionFrequency.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SubscriptionFrequency.Location = New System.Drawing.Point(438, 6)
    Me.Combo_SubscriptionFrequency.Name = "Combo_SubscriptionFrequency"
    Me.Combo_SubscriptionFrequency.Size = New System.Drawing.Size(177, 21)
    Me.Combo_SubscriptionFrequency.TabIndex = 5
    '
    'Label18
    '
    Me.Label18.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label18.Location = New System.Drawing.Point(313, 9)
    Me.Label18.Name = "Label18"
    Me.Label18.Size = New System.Drawing.Size(120, 17)
    Me.Label18.TabIndex = 94
    Me.Label18.Text = "Subscription frequency"
    '
    'Label17
    '
    Me.Label17.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label17.Location = New System.Drawing.Point(6, 63)
    Me.Label17.Name = "Label17"
    Me.Label17.Size = New System.Drawing.Size(122, 14)
    Me.Label17.TabIndex = 85
    Me.Label17.Text = "Initial Subscription Fees"
    '
    'Label16
    '
    Me.Label16.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label16.Location = New System.Drawing.Point(6, 36)
    Me.Label16.Name = "Label16"
    Me.Label16.Size = New System.Drawing.Size(122, 14)
    Me.Label16.TabIndex = 83
    Me.Label16.Text = "Minimum Investment"
    '
    'Tab_Legal
    '
    Me.Tab_Legal.BackColor = System.Drawing.SystemColors.Control
    Me.Tab_Legal.Controls.Add(Me.Text_Reuters)
    Me.Tab_Legal.Controls.Add(Me.Label14)
    Me.Tab_Legal.Controls.Add(Me.Text_Bloomberg)
    Me.Tab_Legal.Controls.Add(Me.Label15)
    Me.Tab_Legal.Controls.Add(Me.Text_SEDOL)
    Me.Tab_Legal.Controls.Add(Me.Label13)
    Me.Tab_Legal.Controls.Add(Me.Text_ISIN)
    Me.Tab_Legal.Controls.Add(Me.Label12)
    Me.Tab_Legal.Controls.Add(Me.Combo_ListedInstrument)
    Me.Tab_Legal.Controls.Add(Me.Label11)
    Me.Tab_Legal.Controls.Add(Me.Combo_Listings)
    Me.Tab_Legal.Controls.Add(Me.Label10)
    Me.Tab_Legal.Controls.Add(Me.Combo_Custodian)
    Me.Tab_Legal.Controls.Add(Me.Label9)
    Me.Tab_Legal.Controls.Add(Me.Combo_Administrator)
    Me.Tab_Legal.Controls.Add(Me.Label8)
    Me.Tab_Legal.Controls.Add(Me.Combo_Jurisdiction)
    Me.Tab_Legal.Controls.Add(Me.Label7)
    Me.Tab_Legal.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Legal.Name = "Tab_Legal"
    Me.Tab_Legal.Size = New System.Drawing.Size(621, 274)
    Me.Tab_Legal.TabIndex = 2
    Me.Tab_Legal.Text = "Legal && Exchange"
    '
    'Text_Reuters
    '
    Me.Text_Reuters.Location = New System.Drawing.Point(402, 181)
    Me.Text_Reuters.Name = "Text_Reuters"
    Me.Text_Reuters.Size = New System.Drawing.Size(159, 20)
    Me.Text_Reuters.TabIndex = 8
    '
    'Label14
    '
    Me.Label14.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label14.Location = New System.Drawing.Point(297, 184)
    Me.Label14.Name = "Label14"
    Me.Label14.Size = New System.Drawing.Size(99, 17)
    Me.Label14.TabIndex = 102
    Me.Label14.Text = "Reuters Code"
    '
    'Text_Bloomberg
    '
    Me.Text_Bloomberg.Location = New System.Drawing.Point(402, 155)
    Me.Text_Bloomberg.Name = "Text_Bloomberg"
    Me.Text_Bloomberg.Size = New System.Drawing.Size(159, 20)
    Me.Text_Bloomberg.TabIndex = 7
    '
    'Label15
    '
    Me.Label15.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label15.Location = New System.Drawing.Point(297, 158)
    Me.Label15.Name = "Label15"
    Me.Label15.Size = New System.Drawing.Size(99, 17)
    Me.Label15.TabIndex = 100
    Me.Label15.Text = "Bloomberg Code"
    '
    'Text_SEDOL
    '
    Me.Text_SEDOL.Location = New System.Drawing.Point(108, 181)
    Me.Text_SEDOL.Name = "Text_SEDOL"
    Me.Text_SEDOL.Size = New System.Drawing.Size(159, 20)
    Me.Text_SEDOL.TabIndex = 6
    '
    'Label13
    '
    Me.Label13.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label13.Location = New System.Drawing.Point(3, 184)
    Me.Label13.Name = "Label13"
    Me.Label13.Size = New System.Drawing.Size(99, 17)
    Me.Label13.TabIndex = 98
    Me.Label13.Text = "SEDOL Code"
    '
    'Text_ISIN
    '
    Me.Text_ISIN.Location = New System.Drawing.Point(108, 155)
    Me.Text_ISIN.Name = "Text_ISIN"
    Me.Text_ISIN.Size = New System.Drawing.Size(159, 20)
    Me.Text_ISIN.TabIndex = 5
    '
    'Label12
    '
    Me.Label12.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label12.Location = New System.Drawing.Point(3, 158)
    Me.Label12.Name = "Label12"
    Me.Label12.Size = New System.Drawing.Size(99, 17)
    Me.Label12.TabIndex = 96
    Me.Label12.Text = "ISIN Code"
    '
    'Combo_ListedInstrument
    '
    Me.Combo_ListedInstrument.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_ListedInstrument.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_ListedInstrument.Location = New System.Drawing.Point(108, 112)
    Me.Combo_ListedInstrument.Name = "Combo_ListedInstrument"
    Me.Combo_ListedInstrument.Size = New System.Drawing.Size(510, 21)
    Me.Combo_ListedInstrument.TabIndex = 4
    '
    'Label11
    '
    Me.Label11.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label11.Location = New System.Drawing.Point(3, 115)
    Me.Label11.Name = "Label11"
    Me.Label11.Size = New System.Drawing.Size(99, 35)
    Me.Label11.TabIndex = 94
    Me.Label11.Text = "Listed Instrument (Inv Trust)"
    '
    'Combo_Listings
    '
    Me.Combo_Listings.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Listings.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Listings.Location = New System.Drawing.Point(108, 85)
    Me.Combo_Listings.Name = "Combo_Listings"
    Me.Combo_Listings.Size = New System.Drawing.Size(510, 21)
    Me.Combo_Listings.TabIndex = 3
    '
    'Label10
    '
    Me.Label10.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label10.Location = New System.Drawing.Point(3, 88)
    Me.Label10.Name = "Label10"
    Me.Label10.Size = New System.Drawing.Size(99, 17)
    Me.Label10.TabIndex = 92
    Me.Label10.Text = "Listing(s)"
    '
    'Combo_Custodian
    '
    Me.Combo_Custodian.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Custodian.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Custodian.Location = New System.Drawing.Point(108, 58)
    Me.Combo_Custodian.Name = "Combo_Custodian"
    Me.Combo_Custodian.Size = New System.Drawing.Size(510, 21)
    Me.Combo_Custodian.TabIndex = 2
    '
    'Label9
    '
    Me.Label9.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label9.Location = New System.Drawing.Point(3, 61)
    Me.Label9.Name = "Label9"
    Me.Label9.Size = New System.Drawing.Size(99, 17)
    Me.Label9.TabIndex = 90
    Me.Label9.Text = "Custodian"
    '
    'Combo_Administrator
    '
    Me.Combo_Administrator.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Administrator.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Administrator.Location = New System.Drawing.Point(108, 31)
    Me.Combo_Administrator.Name = "Combo_Administrator"
    Me.Combo_Administrator.Size = New System.Drawing.Size(510, 21)
    Me.Combo_Administrator.TabIndex = 1
    '
    'Label8
    '
    Me.Label8.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label8.Location = New System.Drawing.Point(3, 34)
    Me.Label8.Name = "Label8"
    Me.Label8.Size = New System.Drawing.Size(99, 17)
    Me.Label8.TabIndex = 88
    Me.Label8.Text = "Administrator"
    '
    'Combo_Jurisdiction
    '
    Me.Combo_Jurisdiction.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Jurisdiction.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Jurisdiction.Location = New System.Drawing.Point(108, 4)
    Me.Combo_Jurisdiction.Name = "Combo_Jurisdiction"
    Me.Combo_Jurisdiction.Size = New System.Drawing.Size(510, 21)
    Me.Combo_Jurisdiction.TabIndex = 0
    '
    'Label7
    '
    Me.Label7.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label7.Location = New System.Drawing.Point(3, 7)
    Me.Label7.Name = "Label7"
    Me.Label7.Size = New System.Drawing.Size(99, 17)
    Me.Label7.TabIndex = 86
    Me.Label7.Text = "Jurisdiction"
    '
    'Tab_Objective
    '
    Me.Tab_Objective.BackColor = System.Drawing.SystemColors.Control
    Me.Tab_Objective.Controls.Add(Me.Label6)
    Me.Tab_Objective.Controls.Add(Me.Label5)
    Me.Tab_Objective.Controls.Add(Me.Text_Disclaimer)
    Me.Tab_Objective.Controls.Add(Me.Text_Objective)
    Me.Tab_Objective.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Objective.Name = "Tab_Objective"
    Me.Tab_Objective.Size = New System.Drawing.Size(621, 274)
    Me.Tab_Objective.TabIndex = 3
    Me.Tab_Objective.Text = "Objective && Disclaimer"
    '
    'Label6
    '
    Me.Label6.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label6.Location = New System.Drawing.Point(3, 111)
    Me.Label6.Name = "Label6"
    Me.Label6.Size = New System.Drawing.Size(128, 17)
    Me.Label6.TabIndex = 52
    Me.Label6.Text = "Disclaimer"
    '
    'Label5
    '
    Me.Label5.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label5.Location = New System.Drawing.Point(3, 2)
    Me.Label5.Name = "Label5"
    Me.Label5.Size = New System.Drawing.Size(128, 17)
    Me.Label5.TabIndex = 51
    Me.Label5.Text = "Objective"
    '
    'Text_Disclaimer
    '
    Me.Text_Disclaimer.AcceptsReturn = True
    Me.Text_Disclaimer.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Text_Disclaimer.Location = New System.Drawing.Point(3, 130)
    Me.Text_Disclaimer.Multiline = True
    Me.Text_Disclaimer.Name = "Text_Disclaimer"
    Me.Text_Disclaimer.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.Text_Disclaimer.Size = New System.Drawing.Size(615, 141)
    Me.Text_Disclaimer.TabIndex = 1
    '
    'Text_Objective
    '
    Me.Text_Objective.AcceptsReturn = True
    Me.Text_Objective.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Text_Objective.Location = New System.Drawing.Point(3, 21)
    Me.Text_Objective.Multiline = True
    Me.Text_Objective.Name = "Text_Objective"
    Me.Text_Objective.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.Text_Objective.Size = New System.Drawing.Size(615, 87)
    Me.Text_Objective.TabIndex = 0
    '
    'Combo_ReviewGroup
    '
    Me.Combo_ReviewGroup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_ReviewGroup.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_ReviewGroup.Location = New System.Drawing.Point(443, 235)
    Me.Combo_ReviewGroup.Name = "Combo_ReviewGroup"
    Me.Combo_ReviewGroup.Size = New System.Drawing.Size(192, 21)
    Me.Combo_ReviewGroup.TabIndex = 11
    '
    'Label30
    '
    Me.Label30.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label30.Location = New System.Drawing.Point(331, 238)
    Me.Label30.Name = "Label30"
    Me.Label30.Size = New System.Drawing.Size(106, 18)
    Me.Label30.TabIndex = 87
    Me.Label30.Text = "Fund Review Group"
    '
    'editAdministratorCode
    '
    Me.editAdministratorCode.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.editAdministratorCode.Location = New System.Drawing.Point(120, 235)
    Me.editAdministratorCode.Name = "editAdministratorCode"
    Me.editAdministratorCode.Size = New System.Drawing.Size(198, 20)
    Me.editAdministratorCode.TabIndex = 10
    '
    'Label31
    '
    Me.Label31.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label31.Location = New System.Drawing.Point(9, 238)
    Me.Label31.Name = "Label31"
    Me.Label31.Size = New System.Drawing.Size(100, 17)
    Me.Label31.TabIndex = 89
    Me.Label31.Text = "Administrator Code"
    '
    'Combo_PricingFrequency
    '
    Me.Combo_PricingFrequency.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_PricingFrequency.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_PricingFrequency.Location = New System.Drawing.Point(121, 208)
    Me.Combo_PricingFrequency.Name = "Combo_PricingFrequency"
    Me.Combo_PricingFrequency.Size = New System.Drawing.Size(197, 21)
    Me.Combo_PricingFrequency.TabIndex = 8
    '
    'Label37
    '
    Me.Label37.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label37.Location = New System.Drawing.Point(10, 211)
    Me.Label37.Name = "Label37"
    Me.Label37.Size = New System.Drawing.Size(106, 18)
    Me.Label37.TabIndex = 91
    Me.Label37.Text = "Pricing Frequency"
    '
    'Label38
    '
    Me.Label38.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label38.Location = New System.Drawing.Point(331, 211)
    Me.Label38.Name = "Label38"
    Me.Label38.Size = New System.Drawing.Size(106, 18)
    Me.Label38.TabIndex = 92
    Me.Label38.Text = "Fund Year End"
    '
    'Date_FundYearEnd
    '
    Me.Date_FundYearEnd.CustomFormat = "MMMM"
    Me.Date_FundYearEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom
    Me.Date_FundYearEnd.Location = New System.Drawing.Point(443, 208)
    Me.Date_FundYearEnd.Name = "Date_FundYearEnd"
    Me.Date_FundYearEnd.Size = New System.Drawing.Size(192, 20)
    Me.Date_FundYearEnd.TabIndex = 9
    '
    'Check_DefaultTradeFxToFundCurrency
    '
    Me.Check_DefaultTradeFxToFundCurrency.AutoSize = True
    Me.Check_DefaultTradeFxToFundCurrency.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Check_DefaultTradeFxToFundCurrency.Location = New System.Drawing.Point(5, 265)
    Me.Check_DefaultTradeFxToFundCurrency.Name = "Check_DefaultTradeFxToFundCurrency"
    Me.Check_DefaultTradeFxToFundCurrency.Size = New System.Drawing.Size(206, 17)
    Me.Check_DefaultTradeFxToFundCurrency.TabIndex = 12
    Me.Check_DefaultTradeFxToFundCurrency.Text = "Default FX for trades to Fund currency"
    Me.Check_DefaultTradeFxToFundCurrency.UseVisualStyleBackColor = True
    '
    'Combo_FundReportGroup
    '
    Me.Combo_FundReportGroup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_FundReportGroup.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_FundReportGroup.Location = New System.Drawing.Point(443, 263)
    Me.Combo_FundReportGroup.Name = "Combo_FundReportGroup"
    Me.Combo_FundReportGroup.Size = New System.Drawing.Size(192, 21)
    Me.Combo_FundReportGroup.TabIndex = 13
    '
    'Label40
    '
    Me.Label40.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label40.Location = New System.Drawing.Point(331, 266)
    Me.Label40.Name = "Label40"
    Me.Label40.Size = New System.Drawing.Size(106, 18)
    Me.Label40.TabIndex = 94
    Me.Label40.Text = "Fund Reporting Group"
    '
    'Label41
    '
    Me.Label41.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label41.Location = New System.Drawing.Point(21, 310)
    Me.Label41.Name = "Label41"
    Me.Label41.Size = New System.Drawing.Size(192, 17)
    Me.Label41.TabIndex = 96
    Me.Label41.Text = "Subscription. After Order Date"
    '
    'Edit_SubscriptionPeriodsToValueDate
    '
    Me.Edit_SubscriptionPeriodsToValueDate.Location = New System.Drawing.Point(183, 307)
    Me.Edit_SubscriptionPeriodsToValueDate.Name = "Edit_SubscriptionPeriodsToValueDate"
    Me.Edit_SubscriptionPeriodsToValueDate.RenaissanceTag = Nothing
    Me.Edit_SubscriptionPeriodsToValueDate.SelectTextOnFocus = False
    Me.Edit_SubscriptionPeriodsToValueDate.Size = New System.Drawing.Size(36, 20)
    Me.Edit_SubscriptionPeriodsToValueDate.TabIndex = 14
    Me.Edit_SubscriptionPeriodsToValueDate.Text = "0"
    Me.Edit_SubscriptionPeriodsToValueDate.TextFormat = "#,##0"
    Me.Edit_SubscriptionPeriodsToValueDate.Value = 0
    '
    'Label42
    '
    Me.Label42.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label42.Location = New System.Drawing.Point(9, 290)
    Me.Label42.Name = "Label42"
    Me.Label42.Size = New System.Drawing.Size(626, 17)
    Me.Label42.TabIndex = 97
    Me.Label42.Text = "Automated Subscription and Redemption :"
    '
    'Label43
    '
    Me.Label43.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label43.Location = New System.Drawing.Point(225, 310)
    Me.Label43.Name = "Label43"
    Me.Label43.Size = New System.Drawing.Size(245, 17)
    Me.Label43.TabIndex = 98
    Me.Label43.Text = "days to NAV (Value) Date, then a further"
    '
    'Edit_SubscriptionPeriodsToSettlementDate
    '
    Me.Edit_SubscriptionPeriodsToSettlementDate.Location = New System.Drawing.Point(434, 307)
    Me.Edit_SubscriptionPeriodsToSettlementDate.Name = "Edit_SubscriptionPeriodsToSettlementDate"
    Me.Edit_SubscriptionPeriodsToSettlementDate.RenaissanceTag = Nothing
    Me.Edit_SubscriptionPeriodsToSettlementDate.SelectTextOnFocus = False
    Me.Edit_SubscriptionPeriodsToSettlementDate.Size = New System.Drawing.Size(36, 20)
    Me.Edit_SubscriptionPeriodsToSettlementDate.TabIndex = 15
    Me.Edit_SubscriptionPeriodsToSettlementDate.Text = "0"
    Me.Edit_SubscriptionPeriodsToSettlementDate.TextFormat = "#,##0"
    Me.Edit_SubscriptionPeriodsToSettlementDate.Value = 0
    '
    'Label44
    '
    Me.Label44.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label44.Location = New System.Drawing.Point(476, 310)
    Me.Label44.Name = "Label44"
    Me.Label44.Size = New System.Drawing.Size(115, 17)
    Me.Label44.TabIndex = 100
    Me.Label44.Text = "days to settlement."
    '
    'Label45
    '
    Me.Label45.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label45.Location = New System.Drawing.Point(476, 334)
    Me.Label45.Name = "Label45"
    Me.Label45.Size = New System.Drawing.Size(115, 17)
    Me.Label45.TabIndex = 105
    Me.Label45.Text = "days to settlement."
    '
    'Edit_RedemptionPeriodsToSettlementDate
    '
    Me.Edit_RedemptionPeriodsToSettlementDate.Location = New System.Drawing.Point(434, 331)
    Me.Edit_RedemptionPeriodsToSettlementDate.Name = "Edit_RedemptionPeriodsToSettlementDate"
    Me.Edit_RedemptionPeriodsToSettlementDate.RenaissanceTag = Nothing
    Me.Edit_RedemptionPeriodsToSettlementDate.SelectTextOnFocus = False
    Me.Edit_RedemptionPeriodsToSettlementDate.Size = New System.Drawing.Size(36, 20)
    Me.Edit_RedemptionPeriodsToSettlementDate.TabIndex = 17
    Me.Edit_RedemptionPeriodsToSettlementDate.Text = "0"
    Me.Edit_RedemptionPeriodsToSettlementDate.TextFormat = "#,##0"
    Me.Edit_RedemptionPeriodsToSettlementDate.Value = 0
    '
    'Edit_RedemptionPeriodsToValueDate
    '
    Me.Edit_RedemptionPeriodsToValueDate.Location = New System.Drawing.Point(183, 331)
    Me.Edit_RedemptionPeriodsToValueDate.Name = "Edit_RedemptionPeriodsToValueDate"
    Me.Edit_RedemptionPeriodsToValueDate.RenaissanceTag = Nothing
    Me.Edit_RedemptionPeriodsToValueDate.SelectTextOnFocus = False
    Me.Edit_RedemptionPeriodsToValueDate.Size = New System.Drawing.Size(36, 20)
    Me.Edit_RedemptionPeriodsToValueDate.TabIndex = 16
    Me.Edit_RedemptionPeriodsToValueDate.Text = "0"
    Me.Edit_RedemptionPeriodsToValueDate.TextFormat = "#,##0"
    Me.Edit_RedemptionPeriodsToValueDate.Value = 0
    '
    'Label46
    '
    Me.Label46.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label46.Location = New System.Drawing.Point(225, 334)
    Me.Label46.Name = "Label46"
    Me.Label46.Size = New System.Drawing.Size(245, 17)
    Me.Label46.TabIndex = 103
    Me.Label46.Text = "days to NAV (Value) Date, then a further"
    '
    'Label47
    '
    Me.Label47.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label47.Location = New System.Drawing.Point(21, 334)
    Me.Label47.Name = "Label47"
    Me.Label47.Size = New System.Drawing.Size(192, 17)
    Me.Label47.TabIndex = 102
    Me.Label47.Text = "Redemption. After Order Date"
    '
    'Check_FundClosed
    '
    Me.Check_FundClosed.AutoSize = True
    Me.Check_FundClosed.Location = New System.Drawing.Point(12, 674)
    Me.Check_FundClosed.Name = "Check_FundClosed"
    Me.Check_FundClosed.Size = New System.Drawing.Size(85, 17)
    Me.Check_FundClosed.TabIndex = 106
    Me.Check_FundClosed.Text = "Fund Closed"
    Me.Check_FundClosed.UseVisualStyleBackColor = True
    '
    'frmFund
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.CancelButton = Me.btnCancel
    Me.ClientSize = New System.Drawing.Size(645, 765)
    Me.Controls.Add(Me.Check_FundClosed)
    Me.Controls.Add(Me.Label45)
    Me.Controls.Add(Me.Edit_RedemptionPeriodsToSettlementDate)
    Me.Controls.Add(Me.Edit_RedemptionPeriodsToValueDate)
    Me.Controls.Add(Me.Label46)
    Me.Controls.Add(Me.Label47)
    Me.Controls.Add(Me.Label44)
    Me.Controls.Add(Me.Edit_SubscriptionPeriodsToSettlementDate)
    Me.Controls.Add(Me.Label42)
    Me.Controls.Add(Me.Edit_SubscriptionPeriodsToValueDate)
    Me.Controls.Add(Me.Combo_FundReportGroup)
    Me.Controls.Add(Me.Label40)
    Me.Controls.Add(Me.Check_DefaultTradeFxToFundCurrency)
    Me.Controls.Add(Me.Date_FundYearEnd)
    Me.Controls.Add(Me.Label38)
    Me.Controls.Add(Me.Combo_PricingFrequency)
    Me.Controls.Add(Me.Label37)
    Me.Controls.Add(Me.editAdministratorCode)
    Me.Controls.Add(Me.Label31)
    Me.Controls.Add(Me.Combo_ReviewGroup)
    Me.Controls.Add(Me.Label30)
    Me.Controls.Add(Me.Tab_FundDetails)
    Me.Controls.Add(Me.RootMenu)
    Me.Controls.Add(Me.btnClose)
    Me.Controls.Add(Me.Combo_FundUnit)
    Me.Controls.Add(Me.Combo_FundBaseCurrency)
    Me.Controls.Add(Me.GroupBox1)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.Combo_ParentFund)
    Me.Controls.Add(Me.Panel1)
    Me.Controls.Add(Me.Combo_SelectFundCode)
    Me.Controls.Add(Me.btnSave)
    Me.Controls.Add(Me.editAuditID)
    Me.Controls.Add(Me.editFundCode)
    Me.Controls.Add(Me.editFundName)
    Me.Controls.Add(Me.editFundLegalEntity)
    Me.Controls.Add(Me.lblFundCode)
    Me.Controls.Add(Me.lblFundName)
    Me.Controls.Add(Me.lblFundLegalEntity)
    Me.Controls.Add(Me.lblFundBaseCurrency)
    Me.Controls.Add(Me.lblFundUnit)
    Me.Controls.Add(Me.lblFundParentFund)
    Me.Controls.Add(Me.btnDelete)
    Me.Controls.Add(Me.btnCancel)
    Me.Controls.Add(Me.Label43)
    Me.Controls.Add(Me.Label41)
    Me.MinimumSize = New System.Drawing.Size(650, 650)
    Me.Name = "frmFund"
    Me.Text = "Add/Edit Funds"
    Me.Panel1.ResumeLayout(False)
    Me.Tab_FundDetails.ResumeLayout(False)
    Me.Tab_Fees.ResumeLayout(False)
    Me.Tab_Fees.PerformLayout()
    Me.Tab_ManagerDetails.ResumeLayout(False)
    Me.Tab_ManagerDetails.PerformLayout()
    Me.Tab_FundTerms.ResumeLayout(False)
    Me.Tab_Legal.ResumeLayout(False)
    Me.Tab_Legal.PerformLayout()
    Me.Tab_Objective.ResumeLayout(False)
    Me.Tab_Objective.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "


  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
  ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
  ''' </summary>
  Private WithEvents _MainForm As RenaissanceGlobals.StandardRenaissanceMainForm ' VeniceMain

  ' Form ToolTip
  ''' <summary>
  ''' Control to provide tooltip functionality.
  ''' </summary>
  Private FormTooltip As New ToolTip()

  ' Form Menu


  ' Form Constants, specific to the table being updated.

  ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
  ''' </summary>
  Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

  ' Form Locals, initialised on 'New' defining what standard data items to use
  ''' <summary>
  ''' Variable holding the form's data table name. taken from the "ThisStandardDataset" object.
  ''' </summary>
  Private THIS_TABLENAME As String
  ''' <summary>
  ''' Variable holding the form's data adaptor name. taken from the "ThisStandardDataset" object.
  ''' </summary>
  Private THIS_ADAPTORNAME As String
  ''' <summary>
  ''' Variable holding the form's dataset name. taken from the "ThisStandardDataset" object.
  ''' </summary>
  Private THIS_DATASETNAME As String


  ' The standard ChangeID for this form. e.g. tblFund
  ''' <summary>
  ''' The standard ChangeID for this form.
  ''' </summary>
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

  ''' <summary>
  ''' Principal control used for selecting items on this form. 
  ''' </summary>
  Private THIS_FORM_SelectingCombo As ComboBox
  ''' <summary>
  ''' Control to select after proessing the "New" button.
  ''' </summary>
  Private THIS_FORM_NewMoveToControl As Control

  ' Form Specific Order fields
  ''' <summary>
  ''' Field Name to show in the Select combo.
  ''' </summary>
  Private THIS_FORM_SelectBy As String
  ''' <summary>
  ''' Field Name to order the Select combo by.
  ''' </summary>
  Private THIS_FORM_OrderBy As String

  ''' <summary>
  ''' The field name that holds the unique identifier for each entry. e.g. FundID for the tblFund table.
  ''' </summary>
  Private THIS_FORM_ValueMember As String

  ' Form specific Permissioning variables
  ''' <summary>
  ''' The THI s_ FOR m_ permission area
  ''' </summary>
  Private THIS_FORM_PermissionArea As String
  ''' <summary>
  ''' The THI s_ FOR m_ permission type
  ''' </summary>
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
  ''' <summary>
  ''' The THI s_ FOR m_ form ID
  ''' </summary>
  Private THIS_FORM_FormID As VeniceFormID

  ' Data Structures

  ''' <summary>
  ''' My dataset
  ''' </summary>
  Private myDataset As RenaissanceDataClass.DSFund ' Form Specific !!!!
  ''' <summary>
  ''' My table
  ''' </summary>
  Private myTable As RenaissanceDataClass.DSFund.tblFundDataTable
  ''' <summary>
  ''' My connection
  ''' </summary>
  Private myConnection As SqlConnection
  ''' <summary>
  ''' My adaptor
  ''' </summary>
  Private myAdaptor As SqlDataAdapter

  ''' <summary>
  ''' The this standard dataset
  ''' </summary>
  Private ThisStandardDataset As RenaissanceGlobals.StandardDataset


  ' Active Element.

  ''' <summary>
  ''' The sorted rows
  ''' </summary>
  Private SortedRows() As DataRow
  ''' <summary>
  ''' The select by sorted rows
  ''' </summary>
  Private SelectBySortedRows() As DataRow
  ''' <summary>
  ''' The this data row
  ''' </summary>
  Private thisDataRow As RenaissanceDataClass.DSFund.tblFundRow   ' Form Specific !!!!
  ''' <summary>
  ''' The this audit ID
  ''' </summary>
  Private thisAuditID As Integer
  ''' <summary>
  ''' The this position
  ''' </summary>
  Private thisPosition As Integer
  ''' <summary>
  ''' The __ is over cancel button
  ''' </summary>
  Private __IsOverCancelButton As Boolean
  ''' <summary>
  ''' The _ in use
  ''' </summary>
  Private _InUse As Boolean

  ' Form Status Flags

  ''' <summary>
  ''' The form is valid
  ''' </summary>
  Private FormIsValid As Boolean
  ''' <summary>
  ''' The form changed
  ''' </summary>
  Private FormChanged As Boolean
  ''' <summary>
  ''' The _ form open failed
  ''' </summary>
  Private _FormOpenFailed As Boolean
  ''' <summary>
  ''' The in paint
  ''' </summary>
  Private InPaint As Boolean
  ''' <summary>
  ''' The add new record
  ''' </summary>
  Private AddNewRecord As Boolean

  ' User Permission Flags

  ''' <summary>
  ''' The has read permission
  ''' </summary>
  Private HasReadPermission As Boolean
  ''' <summary>
  ''' The has update permission
  ''' </summary>
  Private HasUpdatePermission As Boolean
  ''' <summary>
  ''' The has insert permission
  ''' </summary>
  Private HasInsertPermission As Boolean
  ''' <summary>
  ''' The has delete permission
  ''' </summary>
  Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

  ''' <summary>
  ''' Gets the main form.
  ''' </summary>
  ''' <value>The main form.</value>
  Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
    ' Public property to return handle to the 'Main' Venice form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

  ''' <summary>
  ''' Gets or sets a value indicating whether this instance is over cancel button.
  ''' </summary>
  ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
  Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
    ' Public property maintaining a value indicating if the cursor is over the 'Cancel'
    ' Button on this form.
    ' This property is specifically designed for use by the field formating Event functions
    ' In order that they do not impose format restrictions if the user is about to click the 
    ' 'Cancel' button.
    '
    Get
      Return __IsOverCancelButton
    End Get
    Set(ByVal Value As Boolean)
      __IsOverCancelButton = Value
    End Set
  End Property

  ''' <summary>
  ''' Gets a value indicating whether this instance is in paint.
  ''' </summary>
  ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
  Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
    Get
      Return InPaint
    End Get
  End Property

  ''' <summary>
  ''' Gets a value indicating whether [in use].
  ''' </summary>
  ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
    Get
      Return _InUse
    End Get
  End Property

  ''' <summary>
  ''' Gets a value indicating whether [form open failed].
  ''' </summary>
  ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

#End Region

  ''' <summary>
  ''' Initializes a new instance of the <see cref="frmFund"/> class.
  ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  Public Sub New(ByVal pMainForm As VeniceMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()

    _MainForm = pMainForm
    AddHandler _MainForm.RenaissanceAutoUpdate, AddressOf Me.AutoUpdate

    _FormOpenFailed = False
    _InUse = True

    ' ******************************************************
    ' Form Specific Settings :
    ' ******************************************************

    THIS_FORM_SelectingCombo = Me.Combo_SelectFundCode
    THIS_FORM_NewMoveToControl = Me.editFundCode

    ' Default Select and Order fields.

    THIS_FORM_SelectBy = "FundCode"
    THIS_FORM_OrderBy = "FundCode"

    THIS_FORM_ValueMember = "FundID"

    ' Form Permissioning :-

    THIS_FORM_PermissionArea = Me.Name
    THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

    ' 'This' form ID

    THIS_FORM_FormID = VeniceFormID.frmFund

    ' This form's dataset type.

    ThisStandardDataset = RenaissanceStandardDatasets.tblFund ' This Defines the Form Data !!! 

    ' Format Event Handlers for form controls

    AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler
    AddHandler editFundCode.LostFocus, AddressOf MainForm.Text_NotNull
    AddHandler editFundName.LostFocus, AddressOf MainForm.Text_NotNull

    ' Form Control Changed events
    AddHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
    AddHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

    AddHandler Combo_FundBaseCurrency.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_FundUnit.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_ParentFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType

    AddHandler Combo_PricingFrequency.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    'AddHandler Combo_MgmtFeesAccruePeriod.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_MgmtFeesPayPeriod.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    'AddHandler Combo_PerfFeesAccruePeriod.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_PerfFeesPayPeriod.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_ManagementFeeInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_PerformanceFeeInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_PerformanceFeeCrystalisedInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType

    AddHandler Combo_FundBaseCurrency.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_FundUnit.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_ParentFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

    AddHandler Combo_PricingFrequency.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    'AddHandler Combo_MgmtFeesAccruePeriod.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_MgmtFeesPayPeriod.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    'AddHandler Combo_PerfFeesAccruePeriod.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_PerfFeesPayPeriod.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_ManagementFeeInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_PerformanceFeeInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_PerformanceFeeCrystalisedInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

    AddHandler Combo_FundBaseCurrency.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_FundUnit.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_ParentFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

    AddHandler Combo_PricingFrequency.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    'AddHandler Combo_MgmtFeesAccruePeriod.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_MgmtFeesPayPeriod.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    'AddHandler Combo_PerfFeesAccruePeriod.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_PerfFeesPayPeriod.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_ManagementFeeInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_PerformanceFeeInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_PerformanceFeeCrystalisedInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

    AddHandler Combo_FundBaseCurrency.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_FundUnit.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_ParentFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

    AddHandler Combo_PricingFrequency.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    'AddHandler Combo_MgmtFeesAccruePeriod.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_MgmtFeesPayPeriod.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    'AddHandler Combo_PerfFeesAccruePeriod.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_PerfFeesPayPeriod.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_ManagementFeeInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_PerformanceFeeInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_PerformanceFeeCrystalisedInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

    AddHandler Combo_ManagementCompany.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
    AddHandler Combo_ContactPhone.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
    AddHandler Combo_ContactEMail.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
    AddHandler Combo_MinimumInvestment.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
    AddHandler Combo_InitialSubscriptionFees.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
    AddHandler Combo_SubscriptionFrequency.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
    AddHandler Combo_SubscriptionNotice.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
    AddHandler Combo_RedemptionFrequency.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
    AddHandler Combo_RedemptionNotice.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
    AddHandler Combo_Jurisdiction.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
    AddHandler Combo_Administrator.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
    AddHandler Combo_Custodian.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
    AddHandler Combo_Listings.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
    AddHandler Combo_ListedInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_ReviewGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_FundReportGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
    AddHandler Combo_ManagementFees.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
    AddHandler Combo_PerformandeFees.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

    AddHandler Combo_ManagementCompany.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_ContactPhone.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_ContactEMail.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_MinimumInvestment.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_InitialSubscriptionFees.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_SubscriptionFrequency.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_SubscriptionNotice.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_RedemptionFrequency.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_RedemptionNotice.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_Jurisdiction.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_Administrator.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_Custodian.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_Listings.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_ListedInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_ReviewGroup.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_FundReportGroup.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_ManagementFees.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_PerformandeFees.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

    AddHandler Combo_ManagementCompany.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_ContactPhone.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_ContactEMail.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_MinimumInvestment.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_InitialSubscriptionFees.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_SubscriptionFrequency.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_SubscriptionNotice.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_RedemptionFrequency.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_RedemptionNotice.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_Jurisdiction.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_Administrator.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_Custodian.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_Listings.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_ListedInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_ReviewGroup.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_FundReportGroup.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_ManagementFees.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_PerformandeFees.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

    AddHandler Combo_ManagementCompany.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_ContactPhone.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_ContactEMail.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_MinimumInvestment.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_InitialSubscriptionFees.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_SubscriptionFrequency.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_SubscriptionNotice.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_RedemptionFrequency.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_RedemptionNotice.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_Jurisdiction.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_Administrator.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_Custodian.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_Listings.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_ListedInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_ReviewGroup.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_FundReportGroup.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_ManagementFees.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_PerformandeFees.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

    AddHandler editFundCode.TextChanged, AddressOf Me.FormControlChanged
    AddHandler editFundName.TextChanged, AddressOf Me.FormControlChanged
    AddHandler editFundLegalEntity.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_FundBaseCurrency.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_FundUnit.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_ParentFund.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler editAdministratorCode.TextChanged, AddressOf Me.FormControlChanged

    AddHandler edit_MovementComission.ValueChanged, AddressOf Me.FormControlChanged
    AddHandler Edit_SubscriptionPeriodsToValueDate.ValueChanged, AddressOf Me.FormControlChanged
    AddHandler Edit_SubscriptionPeriodsToSettlementDate.ValueChanged, AddressOf Me.FormControlChanged
    AddHandler Edit_RedemptionPeriodsToValueDate.ValueChanged, AddressOf Me.FormControlChanged
    AddHandler Edit_RedemptionPeriodsToSettlementDate.ValueChanged, AddressOf Me.FormControlChanged
    AddHandler edit_FundNAVReconciliationPlusDays.ValueChanged, AddressOf Me.FormControlChanged

    AddHandler Check_FundUsesEqualisation.CheckedChanged, AddressOf Me.FormControlChanged
    AddHandler Check_ForceMultiSharClassReconciliation.CheckedChanged, AddressOf Me.FormControlChanged
    AddHandler Check_FundManagementFeesBeforeSubscriptions.CheckedChanged, AddressOf Me.FormControlChanged
    AddHandler Check_FundClosed.CheckedChanged, AddressOf Me.FormControlChanged
    AddHandler Check_DefaultTradeFxToFundCurrency.CheckedChanged, AddressOf Me.FormControlChanged

    AddHandler Combo_PricingFrequency.SelectedValueChanged, AddressOf Me.FormControlChanged
    'AddHandler Combo_MgmtFeesAccruePeriod.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_MgmtFeesPayPeriod.SelectedValueChanged, AddressOf Me.FormControlChanged
    'AddHandler Combo_PerfFeesAccruePeriod.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_PerfFeesPayPeriod.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_ManagementFeeInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_PerformanceFeeInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_PerformanceFeeCrystalisedInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged

    AddHandler Combo_ManagementCompany.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_ContactPhone.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_ContactEMail.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_MinimumInvestment.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_InitialSubscriptionFees.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_SubscriptionFrequency.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_SubscriptionNotice.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_RedemptionFrequency.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_RedemptionNotice.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Jurisdiction.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Administrator.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Custodian.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Listings.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_ListedInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_ReviewGroup.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_FundReportGroup.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_ManagementFees.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_PerformandeFees.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Date_FundYearEnd.ValueChanged, AddressOf Me.FormControlChanged

    ' Combo_MinimumInvestment
    ' Combo_InitialSubscriptionFees

    AddHandler Combo_ManagementCompany.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_ContactPhone.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_ContactEMail.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_MinimumInvestment.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_InitialSubscriptionFees.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_SubscriptionFrequency.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_SubscriptionNotice.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_RedemptionFrequency.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_RedemptionNotice.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Jurisdiction.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Administrator.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Custodian.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Listings.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_ListedInstrument.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_ReviewGroup.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_FundReportGroup.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_ManagementFees.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_PerformandeFees.TextChanged, AddressOf Me.FormControlChanged

    AddHandler Text_FundManagers.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Text_ContactAddress.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Text_ISIN.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Text_SEDOL.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Text_Bloomberg.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Text_Reuters.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Text_Objective.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Text_Disclaimer.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Date_InvestmentStartDate.ValueChanged, AddressOf Me.FormControlChanged

    ' Set up the ToolTip
    MainForm.SetFormToolTip(Me, FormTooltip)

    ' ******************************************************
    ' End Form Specific.
    ' ******************************************************

    ' Data object names standard to this Form type.

    THIS_TABLENAME = ThisStandardDataset.TableName
    THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
    THIS_DATASETNAME = ThisStandardDataset.DatasetName

    THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

    ' Establish / Retrieve data objects for this form.

    myConnection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)
    myAdaptor = MainForm.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, VENICE_CONNECTION, THIS_TABLENAME)
    myDataset = MainForm.Load_Table(ThisStandardDataset, False)
    myTable = myDataset.Tables(0)

    MainForm.BuildStandardFormMenu(Me.RootMenu, myDataset.Tables(0), AddressOf SelectMenuEvent, AddressOf OrderMenuEvent, AddressOf AuditReportMenuEvent)

  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
  ''' <summary>
  ''' Resets the form.
  ''' </summary>
  Public Sub ResetForm() Implements StandardVeniceForm.ResetForm
    ' *************************************************************************************
    '
    '
    ' *************************************************************************************

    THIS_FORM_SelectBy = "FundCode"
    THIS_FORM_OrderBy = "FundCode"

    Call Form_Load(Me, New System.EventArgs)
  End Sub

  ''' <summary>
  ''' Closes the form.
  ''' </summary>
  Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
    ' *************************************************************************************
    '
    '
    ' *************************************************************************************

    ALWAYS_CLOSE_THIS_FORM = True
    Me.Close()
  End Sub

  ''' <summary>
  ''' Handles the Load event of the Form control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    ' *************************************************************************************
    '
    '
    ' *************************************************************************************

    Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
    _FormOpenFailed = False
    _InUse = True

    ' Initialise Data structures. Connection, Adaptor and Dataset.

    Try

      If Not (MainForm Is Nothing) Then
        FormIsValid = True
      Else
        MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        FormIsValid = False
        _FormOpenFailed = True
        Exit Sub
      End If

      If (myConnection Is Nothing) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

        FormIsValid = False
        _FormOpenFailed = True
        Exit Sub
      End If

      If (myAdaptor Is Nothing) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

        FormIsValid = False
        _FormOpenFailed = True
        Exit Sub
      End If

      If (myDataset Is Nothing) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

        FormIsValid = False
        _FormOpenFailed = True
        Exit Sub
      End If

    Catch ex As Exception
    End Try

    Try

      ' Initialse form

      InPaint = True
      IsOverCancelButton = False

      ' Check User permissions
      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

        FormIsValid = False
        _FormOpenFailed = True
        Exit Sub
      End If

      ' Build Sorted data list from which this form operates
      Call SetSortedRows()

      Call SetFundParentCombo()
      Call SetFundDetailCombos()
      Call SetFundBaseCurrencyCombo()
      Call SetFundUnitCombo()
      Call SetFundReviewGroupCombo()
      Call SetTimePeriodCombos()

      ' Display initial record.

      thisPosition = 0
      If THIS_FORM_SelectingCombo.Items.Count > 0 Then
        Me.THIS_FORM_SelectingCombo.SelectedIndex = 0
        thisDataRow = SortedRows(thisPosition)
        Call GetFormData(thisDataRow)
      Else
        Me.THIS_FORM_SelectingCombo.SelectedIndex = (-1)
        Call GetFormData(Nothing)
      End If

      ' Set active Tab.
      Me.Tab_FundDetails.SelectedIndex = 0

    Catch ex As Exception

      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Initialising Form (Form.Load).", ex.StackTrace, True)

    Finally

      InPaint = False

    End Try

  End Sub

  ''' <summary>
  ''' Handles the Closing event of the frm control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
  Private Sub frm_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    ' *************************************************************************************
    '
    '
    ' *************************************************************************************

    Dim HideForm As Boolean

    ' Hide or Close this form ?
    ' All depends on how many of this form type are Open or in Cache...

    _InUse = False

    If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
      HideForm = False
    Else
      If (FormChanged = True) Then
        Call SetFormData()
      End If

      HideForm = True
      If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
        HideForm = False
      End If
    End If

    If HideForm = True Then
      MainForm.HideInFormsCollection(Me)
      Me.Hide()
      ' Me.Visible = False

      e.Cancel = True
    Else
      Try
        MainForm.RemoveFromFormsCollection(Me)
        RemoveHandler _MainForm.RenaissanceAutoUpdate, AddressOf Me.AutoUpdate

        RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler
        RemoveHandler editFundCode.LostFocus, AddressOf MainForm.Text_NotNull
        RemoveHandler editFundName.LostFocus, AddressOf MainForm.Text_NotNull

        ' Form Control Changed events
        RemoveHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
        RemoveHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        RemoveHandler Combo_FundBaseCurrency.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_FundUnit.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_ParentFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        RemoveHandler Combo_PricingFrequency.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        'RemoveHandler Combo_MgmtFeesAccruePeriod.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_MgmtFeesPayPeriod.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        'RemoveHandler Combo_PerfFeesAccruePeriod.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_PerfFeesPayPeriod.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_ManagementFeeInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_PerformanceFeeInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_PerformanceFeeCrystalisedInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        RemoveHandler Combo_FundBaseCurrency.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_FundUnit.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_ParentFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

        RemoveHandler Combo_PricingFrequency.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        'RemoveHandler Combo_MgmtFeesAccruePeriod.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_MgmtFeesPayPeriod.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        'RemoveHandler Combo_PerfFeesAccruePeriod.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_PerfFeesPayPeriod.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_ManagementFeeInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_PerformanceFeeInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_PerformanceFeeCrystalisedInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

        RemoveHandler Combo_FundBaseCurrency.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_FundUnit.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_ParentFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

        RemoveHandler Combo_PricingFrequency.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        'RemoveHandler Combo_MgmtFeesAccruePeriod.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_MgmtFeesPayPeriod.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        'RemoveHandler Combo_PerfFeesAccruePeriod.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_PerfFeesPayPeriod.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_ManagementFeeInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_PerformanceFeeInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_PerformanceFeeCrystalisedInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

        RemoveHandler Combo_FundBaseCurrency.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_FundUnit.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_ParentFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

        RemoveHandler Combo_PricingFrequency.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        'RemoveHandler Combo_MgmtFeesAccruePeriod.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_MgmtFeesPayPeriod.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        'RemoveHandler Combo_PerfFeesAccruePeriod.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_PerfFeesPayPeriod.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_ManagementFeeInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_PerformanceFeeInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_PerformanceFeeCrystalisedInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

        RemoveHandler Combo_ManagementCompany.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
        RemoveHandler Combo_ContactPhone.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
        RemoveHandler Combo_ContactEMail.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
        RemoveHandler Combo_MinimumInvestment.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
        RemoveHandler Combo_InitialSubscriptionFees.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
        RemoveHandler Combo_SubscriptionFrequency.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
        RemoveHandler Combo_SubscriptionNotice.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
        RemoveHandler Combo_RedemptionFrequency.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
        RemoveHandler Combo_RedemptionNotice.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
        RemoveHandler Combo_Jurisdiction.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
        RemoveHandler Combo_Administrator.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
        RemoveHandler Combo_Custodian.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
        RemoveHandler Combo_Listings.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
        RemoveHandler Combo_ListedInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_ReviewGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_FundReportGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
        RemoveHandler Combo_ManagementFees.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
        RemoveHandler Combo_PerformandeFees.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

        RemoveHandler Combo_ManagementCompany.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_ContactPhone.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_ContactEMail.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_MinimumInvestment.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_InitialSubscriptionFees.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_SubscriptionFrequency.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_SubscriptionNotice.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_RedemptionFrequency.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_RedemptionNotice.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_Jurisdiction.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_Administrator.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_Custodian.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_Listings.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_ListedInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_ReviewGroup.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_FundReportGroup.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_ManagementFees.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_PerformandeFees.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

        RemoveHandler Combo_ManagementCompany.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_ContactPhone.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_ContactEMail.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_MinimumInvestment.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_InitialSubscriptionFees.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_SubscriptionFrequency.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_SubscriptionNotice.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_RedemptionFrequency.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_RedemptionNotice.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_Jurisdiction.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_Administrator.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_Custodian.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_Listings.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_ListedInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_ReviewGroup.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_FundReportGroup.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_ManagementFees.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_PerformandeFees.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

        RemoveHandler Combo_ManagementCompany.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_ContactPhone.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_ContactEMail.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_MinimumInvestment.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_InitialSubscriptionFees.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_SubscriptionFrequency.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_SubscriptionNotice.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_RedemptionFrequency.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_RedemptionNotice.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_Jurisdiction.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_Administrator.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_Custodian.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_Listings.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_ListedInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_ReviewGroup.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_FundReportGroup.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_ManagementFees.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_PerformandeFees.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

        RemoveHandler editFundCode.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler editFundName.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler editFundLegalEntity.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_FundBaseCurrency.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_FundUnit.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_ParentFund.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler editAdministratorCode.TextChanged, AddressOf Me.FormControlChanged

        RemoveHandler edit_MovementComission.ValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Edit_SubscriptionPeriodsToValueDate.ValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Edit_SubscriptionPeriodsToSettlementDate.ValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Edit_RedemptionPeriodsToValueDate.ValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Edit_RedemptionPeriodsToSettlementDate.ValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_FundNAVReconciliationPlusDays.ValueChanged, AddressOf Me.FormControlChanged

        RemoveHandler Check_FundUsesEqualisation.CheckedChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_ForceMultiSharClassReconciliation.CheckedChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_FundManagementFeesBeforeSubscriptions.CheckedChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_FundClosed.CheckedChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_DefaultTradeFxToFundCurrency.CheckedChanged, AddressOf Me.FormControlChanged

        RemoveHandler Date_FundYearEnd.ValueChanged, AddressOf Me.FormControlChanged

        RemoveHandler Combo_PricingFrequency.SelectedValueChanged, AddressOf Me.FormControlChanged
        'RemoveHandler Combo_MgmtFeesAccruePeriod.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_MgmtFeesPayPeriod.SelectedValueChanged, AddressOf Me.FormControlChanged
        'RemoveHandler Combo_PerfFeesAccruePeriod.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_PerfFeesPayPeriod.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_ManagementFeeInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_PerformanceFeeInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_PerformanceFeeCrystalisedInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged

        RemoveHandler Combo_ManagementCompany.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_ContactPhone.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_ContactEMail.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_MinimumInvestment.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_InitialSubscriptionFees.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_SubscriptionFrequency.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_SubscriptionNotice.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_RedemptionFrequency.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_RedemptionNotice.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_Jurisdiction.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_Administrator.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_Custodian.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_Listings.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_ListedInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_ReviewGroup.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_FundReportGroup.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_ManagementFees.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_PerformandeFees.SelectedValueChanged, AddressOf Me.FormControlChanged

        ' Combo_MinimumInvestment
        ' Combo_InitialSubscriptionFees

        RemoveHandler Combo_ManagementCompany.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_ContactPhone.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_ContactEMail.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_MinimumInvestment.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_InitialSubscriptionFees.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_SubscriptionFrequency.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_SubscriptionNotice.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_RedemptionFrequency.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_RedemptionNotice.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_Jurisdiction.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_Administrator.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_Custodian.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_Listings.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_ListedInstrument.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_ReviewGroup.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_FundReportGroup.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_ManagementFees.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_PerformandeFees.TextChanged, AddressOf Me.FormControlChanged

        RemoveHandler Text_FundManagers.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Text_ContactAddress.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Text_ISIN.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Text_SEDOL.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Text_Bloomberg.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Text_Reuters.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Text_Objective.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Text_Disclaimer.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Date_InvestmentStartDate.ValueChanged, AddressOf Me.FormControlChanged


        '
      Catch ex As Exception
      End Try
    End If

  End Sub


#End Region

  ' Routine to handle changes / updates to tables by this and other windows.
  ' If this, or any other, form posts a change to a table, then it will invoke an update event 
  ' detailing what tables have been altered.
  ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
  ' the 'VeniceAutoUpdate' event of the main Venice form.
  ' Each form may them react as appropriate to changes in any table that might impact it.
  '
  ''' <summary>
  ''' Autoes the update.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    Dim OrgInPaint As Boolean
    Dim KnowledgeDateChanged As Boolean
    Dim SetButtonStatus_Flag As Boolean
    Dim RefreshForm As Boolean = False

    If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

    OrgInPaint = InPaint
    InPaint = True
    KnowledgeDateChanged = False
    SetButtonStatus_Flag = False

    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
      KnowledgeDateChanged = True
      RefreshForm = True
    End If

    ' ****************************************************************
    ' Check for changes relevant to this form
    ' ****************************************************************

    ' Changes to the tblFund table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFund) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Call SetFundParentCombo()
      Call SetFundDetailCombos()
    End If

    ' Changes to the tblInstrument table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrument) = True) Or KnowledgeDateChanged Then

      ' ReBuild the Instrument derived combos

      Call SetFundBaseCurrencyCombo()
      Call SetFundUnitCombo()

    End If

    ' Changes to the tblGroupList table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGroupList) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Call SetFundReviewGroupCombo()
    End If

    ' Changes to the KnowledgeDate :-
    If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
      SetButtonStatus_Flag = True
    End If

    ' Changes to the tblUserPermissions table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

      ' Check ongoing permissions.

      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

        FormIsValid = False
        Me.Close()
        Exit Sub
      End If

      SetButtonStatus_Flag = True

    End If


    ' ****************************************************************
    ' Changes to the Main FORM table :-
    ' ****************************************************************

    If (e.TableChanged(THIS_FORM_ChangeID) = True) Or KnowledgeDateChanged Then

      RefreshForm = True

      ' Re-Set Controls etc.
      Call SetSortedRows()

      ' Move again to the correct item
      thisPosition = Get_Position(thisAuditID)

      ' Validate current position.
      If thisPosition >= 0 And thisPosition < Me.SortedRows.GetLength(0) Then
        thisDataRow = Me.SortedRows(thisPosition)
      Else
        If (Me.SortedRows.GetLength(0) > 0) And (AddNewRecord = False) Then
          thisPosition = 0
          thisDataRow = Me.SortedRows(thisPosition)
          FormChanged = False
        Else
          thisDataRow = Nothing
        End If
      End If

      ' Set SelectingCombo Index.
      If (Me.THIS_FORM_SelectingCombo.Items.Count <= 0) Then
        Try
          Me.THIS_FORM_SelectingCombo.SelectedIndex = -1
        Catch ex As Exception
        End Try
      ElseIf (thisPosition < 0) Then
        Try
          MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
        Catch ex As Exception
        End Try
      Else
        Try
          Me.THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
        Catch ex As Exception
        End Try
      End If

    End If

    ' ****************************************************************
    ' Repaint if not currently in Edit Mode
    '
    ' ****************************************************************

    InPaint = OrgInPaint

    ' Retrieve form data if appropriate, i.e. this data item is not already being edited.
    If (RefreshForm = True) AndAlso (FormChanged = False) AndAlso (AddNewRecord = False) Then
      GetFormData(thisDataRow) ' Includes a call to 'SetButtonStatus()'
    Else
      If SetButtonStatus_Flag Then
        Call SetButtonStatus()
      End If
    End If

  End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

  ' Build Sorted list from the Source dataset and update the Select Combo
  ''' <summary>
  ''' Sets the sorted rows.
  ''' </summary>
  Private Sub SetSortedRows()

    Dim OrgInPaint As Boolean

    Dim thisrow As DataRow
    Dim thisDrowDownWidth As Integer
    Dim SizingBitmap As Bitmap
    Dim SizingGraphics As Graphics

    ' Form Specific Selection Combo :-
    If (THIS_FORM_SelectingCombo Is Nothing) Then Exit Sub

    ' Code is pretty Generic form here on...

    ' Set paint local so that changes to the selection combo do not trigger form updates.

    OrgInPaint = InPaint
    InPaint = True

    ' Get selected Row sets, or exit if no data is present.
    If myDataset Is Nothing Then
      ReDim SortedRows(0)
      ReDim SelectBySortedRows(0)
    Else
      SortedRows = myTable.Select("RN >= 0", THIS_FORM_OrderBy)
      SelectBySortedRows = myTable.Select("RN >= 0", THIS_FORM_SelectBy)
    End If

    ' Set Combo data source
    THIS_FORM_SelectingCombo.DataSource = SortedRows
    THIS_FORM_SelectingCombo.DisplayMember = THIS_FORM_SelectBy
    THIS_FORM_SelectingCombo.ValueMember = THIS_FORM_ValueMember

    ' Ensure that the Selection Combo's DrowDownWidth is wide enough to take the required strings

    thisDrowDownWidth = THIS_FORM_SelectingCombo.Width
    For Each thisrow In SortedRows

      ' Compute the string dimensions in the given font
      SizingBitmap = New Bitmap(1, 1, System.Drawing.Imaging.PixelFormat.Format32bppArgb)
      SizingGraphics = Graphics.FromImage(SizingBitmap)
      Dim stringSize As SizeF = SizingGraphics.MeasureString(thisrow(THIS_FORM_SelectBy).ToString, THIS_FORM_SelectingCombo.Font)
      If (stringSize.Width) > thisDrowDownWidth Then
        thisDrowDownWidth = CInt(stringSize.Width)
      End If
    Next

    THIS_FORM_SelectingCombo.DropDownWidth = thisDrowDownWidth

    InPaint = OrgInPaint
  End Sub


  ' Check User permissions
  ''' <summary>
  ''' Checks the permissions.
  ''' </summary>
  Private Sub CheckPermissions()

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

  End Sub

  ' Flag changes to form controls, Event Associations made in 'New' routine.
  ''' <summary>
  ''' Basic event handler, called when a form value is changed.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    If InPaint = False Then
      If (Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord) Then
        FormChanged = True
        Me.btnSave.Enabled = True
        Me.btnCancel.Enabled = True
      End If
    End If

  End Sub

  ''' <summary>
  ''' Selects the menu event.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
  Public Sub SelectMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
    ' Event handler for the Select Menu
    ' Event association is made during the dynamic menu creation

    THIS_FORM_SelectBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

    Call SetSortedRows()

  End Sub

  ''' <summary>
  ''' Orders the menu event.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
  Public Sub OrderMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
    ' Event handler for the Order Menu
    ' Event association is made during the dynamic menu creation

    THIS_FORM_OrderBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

    Call SetSortedRows()

  End Sub


  ''' <summary>
  ''' Audits the report menu event.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
  Public Sub AuditReportMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
    ' Event handler for the Audit Report Menu
    ' Event association is made during the dynamic menu creation

    Try
      Select Case CInt(CType(sender, System.Windows.Forms.ToolStripMenuItem).Tag)
        Case 0 ' This Record
          If (Me.AddNewRecord = False) AndAlso (thisAuditID >= 0) Then
            Run_AuditReport_ThisRecord(MainForm, ThisStandardDataset, thisAuditID, MainForm.VeniceProgressBar)
          End If

        Case 1 ' All Records
          Run_AuditReport_ALLRecords(MainForm, ThisStandardDataset, MainForm.VeniceProgressBar)

      End Select
    Catch ex As Exception
      Call MainForm.LogError(Me.Name & ", AuditReportMenuEvent()", 0, ex.Message, "Error calling Audit report.", ex.StackTrace, True)
    End Try

  End Sub


#End Region

#Region " Set Form Combos (Form Specific Code) "

  ''' <summary>
  ''' Sets the fund parent combo.
  ''' </summary>
  Private Sub SetFundParentCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_ParentFund, _
    RenaissanceStandardDatasets.tblFund, _
    "FundCode", _
    "FundID", _
    "", False, True, True)

  End Sub

  ' 
  ''' <summary>
  ''' Sets the fund detail combos.
  ''' </summary>
  Private Sub SetFundDetailCombos()


    Call MainForm.SetTblGenericCombo( _
    Me.Combo_ManagementCompany, _
    RenaissanceStandardDatasets.tblFund, _
    "FundManagementCompany", _
    "FundManagementCompany", _
    "", True, True, False)

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_ContactPhone, _
    RenaissanceStandardDatasets.tblFund, _
    "FundContactPhone", _
    "FundContactPhone", _
    "", True, True, False)

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_ContactEMail, _
    RenaissanceStandardDatasets.tblFund, _
    "FundContactEmail", _
    "FundContactEmail", _
    "", True, True, False)

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_SubscriptionFrequency, _
    RenaissanceStandardDatasets.tblFund, _
    "FundSubscriptionPeriod", _
    "FundSubscriptionPeriod", _
    "", True, True, False)

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_SubscriptionNotice, _
    RenaissanceStandardDatasets.tblFund, _
    "FundSubscriptionNotice", _
    "FundSubscriptionNotice", _
    "", True, True, False)

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_RedemptionFrequency, _
    RenaissanceStandardDatasets.tblFund, _
    "FundRedemptionPeriod", _
    "FundRedemptionPeriod", _
    "", True, True, False)

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_RedemptionNotice, _
    RenaissanceStandardDatasets.tblFund, _
    "FundRedemptionNotice", _
    "FundRedemptionNotice", _
    "", True, True, False)

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_Jurisdiction, _
    RenaissanceStandardDatasets.tblFund, _
    "FundJurisdiction", _
    "FundJurisdiction", _
    "", True, True, False)

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_Administrator, _
    RenaissanceStandardDatasets.tblFund, _
    "FundAdministrator", _
    "FundAdministrator", _
    "", True, True, False)

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_Custodian, _
    RenaissanceStandardDatasets.tblFund, _
    "FundCustodian", _
    "FundCustodian", _
    "", True, True, False)

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_Listings, _
    RenaissanceStandardDatasets.tblFund, _
    "FundListings", _
    "FundListings", _
    "", True, True, False)

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_MinimumInvestment, _
    RenaissanceStandardDatasets.tblFund, _
    "FundMinimumInvestment", _
    "FundMinimumInvestment", _
    "", True, True, False)

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_InitialSubscriptionFees, _
    RenaissanceStandardDatasets.tblFund, _
    "FundInitialSubscriptionFee", _
    "FundInitialSubscriptionFee", _
    "", True, True, False)

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_ManagementFees, _
    RenaissanceStandardDatasets.tblFund, _
    "FundManagementFeeText", _
    "FundManagementFeeText", _
    "", True, True, False)

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_PerformandeFees, _
    RenaissanceStandardDatasets.tblFund, _
    "FundPerformanceFeeText", _
    "FundPerformanceFeeText", _
    "", True, True, False)

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_FundReportGroup, _
    RenaissanceStandardDatasets.tblFund, _
    "FundReportGroup", _
    "FundReportGroup", _
    "", True, True, False)

  End Sub

  ''' <summary>
  ''' Sets the fund base currency combo.
  ''' </summary>
  Private Sub SetFundBaseCurrencyCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_FundBaseCurrency, _
    RenaissanceStandardDatasets.tblInstrument, _
    "InstrumentDescription", _
    "InstrumentID", _
    "InstrumentType = " & RenaissanceGlobals.InstrumentTypes.Cash)   ' 

  End Sub

  ''' <summary>
  ''' Sets the fund unit combo.
  ''' </summary>
  Private Sub SetFundUnitCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_FundUnit, _
    RenaissanceStandardDatasets.tblInstrument, _
    "InstrumentDescription", _
    "InstrumentID", _
    "InstrumentType = " & CInt(RenaissanceGlobals.InstrumentTypes.Unit).ToString) ' 

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_ListedInstrument, _
    RenaissanceStandardDatasets.tblInstrument, _
    "InstrumentDescription", _
    "InstrumentID", _
    "InstrumentType = " & CInt(RenaissanceGlobals.InstrumentTypes.Share).ToString, False, True, True, 0, "")  ' 

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_ManagementFeeInstrument, _
    RenaissanceStandardDatasets.tblInstrument, _
    "InstrumentDescription", _
    "InstrumentID", _
    "InstrumentIsManagementFee <> 0", False, True, True, 0)  ' 

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_PerformanceFeeInstrument, _
    RenaissanceStandardDatasets.tblInstrument, _
    "InstrumentDescription", _
    "InstrumentID", _
    "InstrumentIsIncentiveFee <> 0", False, True, True, 0)   ' 

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_PerformanceFeeCrystalisedInstrument, _
    RenaissanceStandardDatasets.tblInstrument, _
    "InstrumentDescription", _
    "InstrumentID", _
    "InstrumentIsIncentiveFee <> 0", False, True, True, 0)   ' 
  End Sub

  ' Combo_ReviewGroup
  ''' <summary>
  ''' Sets the fund review group combo.
  ''' </summary>
  Private Sub SetFundReviewGroupCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_ReviewGroup, _
    RenaissanceStandardDatasets.tblGroupList, _
    "GroupListName", _
    "GroupListID", _
    "true", False, True, True, 0, "") ' 

  End Sub

  ''' <summary>
  ''' Sets the time period combos.
  ''' </summary>
  Private Sub SetTimePeriodCombos()

    Try

      Call MainForm.SetTblGenericCombo(Me.Combo_PricingFrequency, GetType(RenaissanceGlobals.DealingPeriod))
      'Call MainForm.SetTblGenericCombo(Me.Combo_MgmtFeesAccruePeriod, GetType(RenaissanceGlobals.DealingPeriod))
      Call MainForm.SetTblGenericCombo(Me.Combo_MgmtFeesPayPeriod, GetType(RenaissanceGlobals.DealingPeriod))
      'Call MainForm.SetTblGenericCombo(Me.Combo_PerfFeesAccruePeriod, GetType(RenaissanceGlobals.DealingPeriod))
      Call MainForm.SetTblGenericCombo(Me.Combo_PerfFeesPayPeriod, GetType(RenaissanceGlobals.DealingPeriod))

    Catch ex As Exception
      Call MainForm.LogError(Me.Name & ", SetTimePeriodCombos", LOG_LEVELS.Error, ex.Message, "Error setting Time-Period Combos", ex.StackTrace, True)
    End Try

  End Sub


#End Region

#Region " Get & Set Form Data / SetButton / ValidateForm / btnCancel Events (Form Specific Code) "

  ''' <summary>
  ''' Gets the form data.
  ''' </summary>
  ''' <param name="ThisRow">The this row.</param>
  Private Sub GetFormData(ByRef ThisRow As RenaissanceDataClass.DSFund.tblFundRow)
    ' Routine to populate form Controls given a data row.
    ' Form is cleared for invalid Datarows or Invalid Form.

    Dim OrgInpaint As Boolean

    ' Set 'Paint' flag to prevent event actions caused by changing the value of form fields.
    OrgInpaint = InPaint
    InPaint = True


    If (ThisRow Is Nothing) Or (FormIsValid = False) Or (Me.InUse = False) Then
      ' Bad / New Datarow - Clear Form.

      thisAuditID = (-1)

      Me.editAuditID.Text = ""

      Me.editFundCode.Text = ""
      Me.editFundName.Text = ""
      Me.editFundLegalEntity.Text = ""
      Me.editAdministratorCode.Text = ""

      edit_MovementComission.Value = 0.0#
      Edit_SubscriptionPeriodsToValueDate.Value = 0.0#
      Edit_SubscriptionPeriodsToSettlementDate.Value = 0.0#
      Edit_RedemptionPeriodsToValueDate.Value = 0.0#
      Edit_RedemptionPeriodsToSettlementDate.Value = 0.0#
      edit_FundNAVReconciliationPlusDays.Value = 1.0#

      Check_FundUsesEqualisation.Checked = False
      Check_ForceMultiSharClassReconciliation.Checked = False
      Check_FundManagementFeesBeforeSubscriptions.Checked = False
      Check_FundClosed.Checked = False
      Check_DefaultTradeFxToFundCurrency.Checked = False

      Date_InvestmentStartDate.Value = Now.Date
      Date_FundYearEnd.Value = New Date(Now.Date.Year, 12, 31)

      Text_FundManagers.Text = ""
      Text_ContactAddress.Text = ""
      Text_ISIN.Text = ""
      Text_SEDOL.Text = ""
      Text_Bloomberg.Text = ""
      Text_Reuters.Text = ""
      Text_Objective.Text = ""
      Text_Disclaimer.Text = ""

      MainForm.ClearComboSelection(Combo_FundBaseCurrency)
      MainForm.ClearComboSelection(Combo_FundUnit)
      MainForm.ClearComboSelection(Combo_ParentFund)
      MainForm.ClearComboSelection(Combo_ReviewGroup)
      MainForm.ClearComboSelection(Combo_FundReportGroup)
      MainForm.ClearComboSelection(Combo_ManagementCompany)
      MainForm.ClearComboSelection(Combo_ContactPhone)
      MainForm.ClearComboSelection(Combo_ContactEMail)
      MainForm.ClearComboSelection(Combo_SubscriptionFrequency)
      MainForm.ClearComboSelection(Combo_SubscriptionNotice)
      MainForm.ClearComboSelection(Combo_RedemptionFrequency)
      MainForm.ClearComboSelection(Combo_RedemptionNotice)
      MainForm.ClearComboSelection(Combo_Jurisdiction)
      MainForm.ClearComboSelection(Combo_Administrator)
      MainForm.ClearComboSelection(Combo_Custodian)
      MainForm.ClearComboSelection(Combo_Listings)
      MainForm.ClearComboSelection(Combo_ListedInstrument)
      MainForm.ClearComboSelection(Combo_MinimumInvestment)
      MainForm.ClearComboSelection(Combo_InitialSubscriptionFees)
      MainForm.ClearComboSelection(Combo_ManagementFees)
      MainForm.ClearComboSelection(Combo_PerformandeFees)
      MainForm.ClearComboSelection(Combo_PerformanceFeeInstrument)
      MainForm.ClearComboSelection(Combo_PerformanceFeeCrystalisedInstrument)
      MainForm.ClearComboSelection(Combo_ManagementFeeInstrument)

      If (Combo_PricingFrequency.Items.Count > 0) Then Combo_PricingFrequency.SelectedValue = RenaissanceGlobals.DealingPeriod.Daily
      'If (Combo_MgmtFeesAccruePeriod.Items.Count > 0) Then Combo_MgmtFeesAccruePeriod.SelectedValue = RenaissanceGlobals.DealingPeriod.Daily
      If (Combo_MgmtFeesPayPeriod.Items.Count > 0) Then Combo_MgmtFeesPayPeriod.SelectedValue = RenaissanceGlobals.DealingPeriod.Daily
      'If (Combo_PerfFeesAccruePeriod.Items.Count > 0) Then Combo_PerfFeesAccruePeriod.SelectedValue = RenaissanceGlobals.DealingPeriod.Daily
      If (Combo_PerfFeesPayPeriod.Items.Count > 0) Then Combo_PerfFeesPayPeriod.SelectedValue = RenaissanceGlobals.DealingPeriod.Daily

      If AddNewRecord = True Then
        Me.btnCancel.Enabled = True
      Else
        Me.btnCancel.Enabled = False
      End If
    Else
      ' Populate Form with given data.
      Try
        thisAuditID = thisDataRow.AuditID

        Me.editAuditID.Text = thisDataRow.AuditID.ToString

        Me.editFundCode.Text = thisDataRow.FundCode
        Me.editFundName.Text = thisDataRow.FundName
        Me.editFundLegalEntity.Text = thisDataRow.FundLegalEntity
        Me.editAdministratorCode.Text = thisDataRow.FundAdministratorCode

        Check_FundUsesEqualisation.Checked = CBool(thisDataRow.FundUsesEqualisation)
        Check_ForceMultiSharClassReconciliation.Checked = CBool(thisDataRow.FundForceMultiShareClassReconciliation)
        Check_FundManagementFeesBeforeSubscriptions.Checked = CBool(thisDataRow.FundManagementFeesBeforeSubscriptions)
        Check_FundClosed.Checked = CBool(thisDataRow.FundClosed)
        Check_DefaultTradeFxToFundCurrency.Checked = thisDataRow.FundDefaultTradeFXToFundCurrency

        ' Combo_FundBaseCurrency
        If (thisDataRow.FundBaseCurrency > 0) Then
          Me.Combo_FundBaseCurrency.SelectedValue = thisDataRow.FundBaseCurrency
          Me.Combo_FundBaseCurrency.Text = Me.Combo_FundBaseCurrency.Text
        Else
          MainForm.ClearComboSelection(Combo_FundBaseCurrency)
        End If

        ' Combo_FundUnit
        If (thisDataRow.FundUnit > 0) Then
          Me.Combo_FundUnit.SelectedValue = thisDataRow.FundUnit
          If CInt(Me.Combo_FundUnit.SelectedValue) = thisDataRow.FundUnit Then
            Me.Combo_FundUnit.Text = Me.Combo_FundUnit.Text
          Else
            MainForm.ClearComboSelection(Combo_FundUnit)
          End If
        Else
          MainForm.ClearComboSelection(Combo_FundUnit)
        End If

        ' Combo_ParentFund
        If (thisDataRow.FundParentFund > 0) And (thisDataRow.FundParentFund <> thisDataRow.FundID) Then
          Me.Combo_ParentFund.SelectedValue = thisDataRow.FundParentFund
          Me.Combo_ParentFund.Text = Me.Combo_ParentFund.Text
        Else
          MainForm.ClearComboSelection(Combo_ParentFund)
        End If

        edit_MovementComission.Value = thisDataRow.FundMovementComission
        Edit_SubscriptionPeriodsToValueDate.Value = thisDataRow.FundSubscriptionNAVDatePeriod
        Edit_SubscriptionPeriodsToSettlementDate.Value = thisDataRow.FundSubscriptionSettlementDatePeriod
        Edit_RedemptionPeriodsToValueDate.Value = thisDataRow.FundRedemptionNAVDatePeriod
        Edit_RedemptionPeriodsToSettlementDate.Value = thisDataRow.FundRedemptionSettlementDatePeriod
        edit_FundNAVReconciliationPlusDays.Value = thisDataRow.FundNAVReconciliationPlusDays

        Combo_ManagementCompany.Text = thisDataRow.FundManagementCompany
        Text_FundManagers.Text = thisDataRow.FundManagers
        Text_ContactAddress.Text = thisDataRow.FundContactAddress
        Combo_ContactPhone.Text = thisDataRow.FundContactPhone
        Combo_ContactEMail.Text = thisDataRow.FundContactEmail
        If (thisDataRow.IsFundInvestmentStartDateNull) Then
          Date_InvestmentStartDate.Value = Now.Date
        Else
          Date_InvestmentStartDate.Value = thisDataRow.FundInvestmentStartDate
        End If

        If (thisDataRow.IsFundYearEndNull) Then
          Date_FundYearEnd.Value = New Date(Now.Date.Year, 12, 31)
        Else
          Date_FundYearEnd.Value = thisDataRow.FundYearEnd
        End If

        Combo_PricingFrequency.SelectedValue = thisDataRow.FundPricingPeriod
        'Combo_MgmtFeesAccruePeriod.SelectedValue = thisDataRow.FundManagementFeesAccruePeriod
        Combo_MgmtFeesPayPeriod.SelectedValue = thisDataRow.FundManagementFeesPaymentPeriod
        'Combo_PerfFeesAccruePeriod.SelectedValue = thisDataRow.FundPerformanceFeesAccruePeriod
        Combo_PerfFeesPayPeriod.SelectedValue = thisDataRow.FundPerformanceFeesPaymentPeriod

        Combo_ManagementFeeInstrument.SelectedValue = thisDataRow.FundManagementFeeInstrument
        Combo_PerformanceFeeInstrument.SelectedValue = thisDataRow.FundPerformanceFeeInstrument
        Combo_PerformanceFeeCrystalisedInstrument.SelectedValue = thisDataRow.FundPerformanceFeeCrystalisedInstrument

        Combo_MinimumInvestment.Text = thisDataRow.FundMinimumInvestment
        Combo_InitialSubscriptionFees.Text = thisDataRow.FundInitialSubscriptionFee
        Combo_SubscriptionFrequency.Text = thisDataRow.FundSubscriptionPeriod
        Combo_SubscriptionNotice.Text = thisDataRow.FundSubscriptionNotice
        Combo_RedemptionFrequency.Text = thisDataRow.FundRedemptionPeriod
        Combo_RedemptionNotice.Text = thisDataRow.FundRedemptionNotice
        Combo_Jurisdiction.Text = thisDataRow.FundJurisdiction
        Combo_Administrator.Text = thisDataRow.FundAdministrator
        Combo_Custodian.Text = thisDataRow.FundCustodian
        Combo_Listings.Text = thisDataRow.FundListings
        Combo_ManagementFees.Text = thisDataRow.FundManagementFeeText
        Combo_PerformandeFees.Text = thisDataRow.FundPerformanceFeeText

        If (thisDataRow.FundLinkedListedInstrument > 0) Then
          Me.Combo_ListedInstrument.SelectedValue = thisDataRow.FundLinkedListedInstrument
        Else
          MainForm.ClearComboSelection(Combo_ListedInstrument)
        End If

        If (thisDataRow.FundReviewGroup > 0) Then
          Me.Combo_ReviewGroup.SelectedValue = thisDataRow.FundReviewGroup
        Else
          If (Combo_ReviewGroup.Items.Count > 0) Then
            Combo_ReviewGroup.SelectedIndex = 0
          End If
        End If

        If (thisDataRow.IsFundReportGroupNull) Then
          Combo_FundReportGroup.SelectedIndex = 0
        Else
          Combo_FundReportGroup.SelectedValue = thisDataRow.FundReportGroup
        End If

        Text_ISIN.Text = thisDataRow.FundISIN
        Text_SEDOL.Text = thisDataRow.FundSEDOL
        Text_Bloomberg.Text = thisDataRow.FundBloomberg
        Text_Reuters.Text = thisDataRow.FundReutersCode

        If (thisDataRow.IsFundObjectiveNull) Then
          Text_Objective.Text = ""
        Else
          Text_Objective.Text = thisDataRow.FundObjective
        End If
        If (thisDataRow.IsFundDisclaimerNull) Then
          Text_Disclaimer.Text = ""
        Else
          Text_Disclaimer.Text = thisDataRow.FundDisclaimer
        End If

        AddNewRecord = False
        ' MainForm.SetComboSelectionLengths(Me)

        Me.btnCancel.Enabled = False
        Me.THIS_FORM_SelectingCombo.Enabled = True

      Catch ex As Exception

        Call MainForm.LogError(Me.Name & ", GetFormData", LOG_LEVELS.Error, ex.Message, "Error Showing Data", ex.StackTrace, True)
        Call GetFormData(Nothing)

      End Try

    End If

    ' Allow Field events to trigger before 'InPaint' Is re-set. 
    ' (Should) Prevent Validation errors during Form Draw.
    Application.DoEvents()

    ' Restore 'Paint' flag.
    InPaint = OrgInpaint
    FormChanged = False
    Me.btnSave.Enabled = False

    ' As it says on the can....
    Call SetButtonStatus()

  End Sub

  ''' <summary>
  ''' Sets the form data.
  ''' </summary>
  ''' <param name="pConfirm">if set to <c>true</c> [p confirm].</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function SetFormData(Optional ByVal pConfirm As Boolean = True) As Boolean
    ' *************************************************************
    '
    ' *************************************************************
    Dim ErrMessage As String
    Dim ErrFlag As Boolean
    Dim ErrStack As String

    ErrMessage = ""
    ErrStack = ""

    ' *************************************************************
    ' Appropriate Save permission :-
    ' *************************************************************

    If (Me.HasUpdatePermission = False) And ((Me.HasInsertPermission = False) Or (Me.AddNewRecord = False)) Then
      MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "You do not have permission to save this record.", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Function
    End If

    ' *************************************************************
    ' If Save button is disabled then should not be able to save, exit silently.
    ' *************************************************************
    If Me.btnSave.Enabled = False Then
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Function
    End If

    ' *************************************************************
    ' KnowledgeDate OK :-
    ' *************************************************************

    If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
      MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Changes to Venice are not allowed when the Knowledgedate is not set to `NOW`", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Function
    End If

    ' Confirm Save, if required.

    If (pConfirm = True) Then
      If MessageBox.Show("Save Changes ?", "Save Changes ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
        Return True
        Exit Function
      End If
    End If

    ' *************************************************************
    ' Procedure to Save the current form information.
    ' *************************************************************

    Dim StatusString As String
    Dim LogString As String
    Dim UpdateRows(0) As DataRow
    Dim Position As Integer

    If (FormChanged = False) Or (FormIsValid = False) Then
      Return False
      Exit Function
    End If

    ' Validation
    StatusString = ""
    If ValidateForm(StatusString) = False Then
      MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Validation Error." & vbCrLf & StatusString, "", True)
      Return False
      Exit Function
    End If

    ' Check current position in the table.
    Position = Get_Position(thisAuditID)

    ' Allow for new or missing ID.
    If Position < 0 Then
      If (AddNewRecord = True) OrElse (SortedRows.Length <= 0) Then
        thisDataRow = myTable.NewRow
        LogString = "Add: "
        AddNewRecord = True
      Else
        Return False
        Exit Function
      End If
    Else
      ' Row found OK.
      LogString = "Edit : AuditID = " & thisAuditID.ToString

      thisDataRow = Me.SortedRows(Position)

    End If

    ' Set 'Paint' flag.
    InPaint = True

    ' *************************************************************
    ' Lock the Data Table, to prevent update conflicts.
    ' *************************************************************
    SyncLock myTable

      ' Initiate Edit,
      thisDataRow.BeginEdit()

      ' Set Data Values
      thisDataRow.FundCode = Me.editFundCode.Text
      LogString &= ", FundCode = " & thisDataRow.FundCode

      thisDataRow.FundName = Me.editFundName.Text
      LogString &= ", FundName = " & thisDataRow.FundName

      thisDataRow.FundLegalEntity = Me.editFundLegalEntity.Text

      thisDataRow.FundAdministratorCode = editAdministratorCode.Text

      thisDataRow.FundMovementComission = edit_MovementComission.Value
      thisDataRow.FundSubscriptionNAVDatePeriod = Edit_SubscriptionPeriodsToValueDate.Value
      thisDataRow.FundSubscriptionSettlementDatePeriod = Edit_SubscriptionPeriodsToSettlementDate.Value
      thisDataRow.FundRedemptionNAVDatePeriod = Edit_RedemptionPeriodsToValueDate.Value
      thisDataRow.FundRedemptionSettlementDatePeriod = Edit_RedemptionPeriodsToSettlementDate.Value
      thisDataRow.FundNAVReconciliationPlusDays = edit_FundNAVReconciliationPlusDays.Value

      thisDataRow.FundUsesEqualisation = CInt(Check_FundUsesEqualisation.Checked)
      thisDataRow.FundForceMultiShareClassReconciliation = CInt(Check_ForceMultiSharClassReconciliation.Checked)
      thisDataRow.FundManagementFeesBeforeSubscriptions = CInt(Check_FundManagementFeesBeforeSubscriptions.Checked)
      thisDataRow.FundClosed = CInt(Check_FundClosed.Checked)
      thisDataRow.FundDefaultTradeFXToFundCurrency = Check_DefaultTradeFxToFundCurrency.Checked

      thisDataRow.FundPricingPeriod = Combo_PricingFrequency.SelectedValue
      thisDataRow.FundManagementFeesAccruePeriod = thisDataRow.FundPricingPeriod
      thisDataRow.FundManagementFeesPaymentPeriod = Combo_MgmtFeesPayPeriod.SelectedValue
      thisDataRow.FundPerformanceFeesAccruePeriod = thisDataRow.FundPricingPeriod
      thisDataRow.FundPerformanceFeesPaymentPeriod = Combo_PerfFeesPayPeriod.SelectedValue

      If (IsNumeric(Combo_ManagementFeeInstrument.SelectedValue)) Then
        thisDataRow.FundManagementFeeInstrument = Combo_ManagementFeeInstrument.SelectedValue
      Else
        thisDataRow.FundManagementFeeInstrument = 0
      End If

      If (IsNumeric(Combo_PerformanceFeeInstrument.SelectedValue)) Then
        thisDataRow.FundPerformanceFeeInstrument = Combo_PerformanceFeeInstrument.SelectedValue
      Else
        thisDataRow.FundPerformanceFeeInstrument = 0
      End If

      If (IsNumeric(Combo_PerformanceFeeCrystalisedInstrument.SelectedValue)) Then
        thisDataRow.FundPerformanceFeeCrystalisedInstrument = Combo_PerformanceFeeCrystalisedInstrument.SelectedValue
      Else
        thisDataRow.FundPerformanceFeeCrystalisedInstrument = 0
      End If

      ' Combo_FundBaseCurrency
      If Me.Combo_FundBaseCurrency.SelectedIndex >= 0 Then
        thisDataRow.FundBaseCurrency = CInt(Me.Combo_FundBaseCurrency.SelectedValue)
      Else
        thisDataRow.FundBaseCurrency = 1
      End If

      ' Combo_FundUnit
      If Me.Combo_FundUnit.SelectedIndex >= 0 Then
        thisDataRow.FundUnit = CInt(Me.Combo_FundUnit.SelectedValue)
      Else
        thisDataRow.FundUnit = 1
      End If

      ' Combo_ParentFund
      If (Me.Combo_ParentFund.SelectedIndex > 0) Then
        thisDataRow.FundParentFund = CInt(Me.Combo_ParentFund.SelectedValue)
      Else
        If (thisDataRow.IsFundIDNull) Then
          thisDataRow.FundParentFund = 0
        Else
          thisDataRow.FundParentFund = thisDataRow.FundID
        End If
      End If

      thisDataRow.FundYearEnd = RenaissanceUtilities.DatePeriodFunctions.FitDateToPeriod(DealingPeriod.Monthly, Date_FundYearEnd.Value, True)

      thisDataRow.FundManagementCompany = Combo_ManagementCompany.Text
      thisDataRow.FundManagers = Text_FundManagers.Text
      thisDataRow.FundContactAddress = Text_ContactAddress.Text
      thisDataRow.FundContactPhone = Combo_ContactPhone.Text
      thisDataRow.FundContactEmail = Combo_ContactEMail.Text
      thisDataRow.FundInvestmentStartDate = Date_InvestmentStartDate.Value
      thisDataRow.FundSubscriptionPeriod = Combo_SubscriptionFrequency.Text
      thisDataRow.FundSubscriptionNotice = Combo_SubscriptionNotice.Text
      thisDataRow.FundRedemptionPeriod = Combo_RedemptionFrequency.Text
      thisDataRow.FundRedemptionNotice = Combo_RedemptionNotice.Text
      thisDataRow.FundJurisdiction = Combo_Jurisdiction.Text
      thisDataRow.FundAdministrator = Combo_Administrator.Text
      thisDataRow.FundCustodian = Combo_Custodian.Text
      thisDataRow.FundListings = Combo_Listings.Text
      thisDataRow.FundMinimumInvestment = Combo_MinimumInvestment.Text
      thisDataRow.FundInitialSubscriptionFee = Combo_InitialSubscriptionFees.Text
      thisDataRow.FundManagementFeeText = Combo_ManagementFees.Text
      thisDataRow.FundPerformanceFeeText = Combo_PerformandeFees.Text

      If (Combo_ListedInstrument.SelectedIndex > 0) AndAlso (IsNumeric(Combo_ListedInstrument.SelectedValue)) Then
        thisDataRow.FundLinkedListedInstrument = CInt(Combo_ListedInstrument.SelectedValue)
      Else
        thisDataRow.FundLinkedListedInstrument = 0
      End If

      If (Combo_ReviewGroup.SelectedIndex > 0) AndAlso (IsNumeric(Combo_ReviewGroup.SelectedValue)) Then
        thisDataRow.FundReviewGroup = CInt(Combo_ReviewGroup.SelectedValue)
      Else
        thisDataRow.FundReviewGroup = 0
      End If

      thisDataRow.FundReportGroup = Combo_FundReportGroup.Text

      thisDataRow.FundISIN = Text_ISIN.Text
      thisDataRow.FundSEDOL = Text_SEDOL.Text
      thisDataRow.FundBloomberg = Text_Bloomberg.Text
      thisDataRow.FundReutersCode = Text_Reuters.Text
      If (Text_Objective.Text.Length > 0) Then
        thisDataRow.FundObjective = Text_Objective.Text
      Else
        thisDataRow.SetFundObjectiveNull()
      End If
      If (Text_Disclaimer.Text.Length > 0) Then
        thisDataRow.FundDisclaimer = Text_Disclaimer.Text
      Else
        thisDataRow.SetFundDisclaimerNull()
      End If

      thisDataRow.FundUser = DBNull.Value.ToString

      thisDataRow.EndEdit()
      InPaint = False

      ' Add and Update DataRow. 

      ErrFlag = False

      If AddNewRecord = True Then
        Try
          myTable.Rows.Add(thisDataRow)
        Catch ex As Exception
          ErrFlag = True
          ErrMessage = ex.Message
          ErrStack = ex.StackTrace
        End Try
      End If

      UpdateRows(0) = thisDataRow

      ' Post Additions / Updates to the underlying table :-
      Dim temp As Integer
      Try
        If (ErrFlag = False) Then
          myAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
          myAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

          temp = MainForm.AdaptorUpdate(Me.Name, myAdaptor, UpdateRows)
        End If

      Catch ex As Exception

        ErrMessage = ex.Message
        ErrFlag = True
        ErrStack = ex.StackTrace

      End Try

      thisAuditID = thisDataRow.AuditID


    End SyncLock

    ' *************************************************************
    ' Error report. Not done in the Try-Catch as that would serve to persist the SyncLock.
    ' *************************************************************

    If (ErrFlag = True) Then
      Call MainForm.LogError(Me.Name & ", SetFormData", LOG_LEVELS.Error, ErrMessage, "Error Saving Data", ErrStack, True)
    End If

    ' Finish off

    AddNewRecord = False
    FormChanged = False
    Me.THIS_FORM_SelectingCombo.Enabled = True

    ' Propogate changes

    If (thisDataRow IsNot Nothing) AndAlso (thisDataRow.IsFundIDNull = False) AndAlso (thisDataRow.FundID > 0) Then
      Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID, thisDataRow.FundID.ToString), True)
    Else
      Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID), True)
    End If

  End Function

  ''' <summary>
  ''' Sets the button status.
  ''' </summary>
  Private Sub SetButtonStatus()
    ' Sets the status of the form controlls appropriate to the current users 
    'permissions and the 'Changed' or 'New' status of the form.

    ' No Read permission :-

    If Me.HasReadPermission = False Then
      MainForm.LogError(Me.Name & ", SetButtonStatus()", 0, "", "You do not have Read permission for this Form", "", True)
      Me.Close()
      Exit Sub
    End If

    ' Has Insert Permission.
    If Me.HasInsertPermission Then
      Me.btnAdd.Enabled = True
    Else
      Me.btnAdd.Enabled = False
    End If

    ' Has Delete permission. 
    If (Me.HasDeletePermission) And (Me.AddNewRecord = False) And (thisPosition >= 0) Then
      Me.btnDelete.Enabled = True
    Else
      Me.btnDelete.Enabled = False
    End If

    If (Me.MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And _
     ((Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord)) Then

      Me.editFundCode.Enabled = True
      Me.editFundName.Enabled = True
      Me.editFundLegalEntity.Enabled = True
      Me.Combo_FundBaseCurrency.Enabled = True
      Me.Combo_FundUnit.Enabled = True
      Me.Combo_ParentFund.Enabled = True

      edit_MovementComission.Enabled = True
      Edit_SubscriptionPeriodsToValueDate.Enabled = True
      Edit_SubscriptionPeriodsToSettlementDate.Enabled = True
      Edit_RedemptionPeriodsToValueDate.Enabled = True
      Edit_RedemptionPeriodsToSettlementDate.Enabled = True
      edit_FundNAVReconciliationPlusDays.Enabled = True

      Me.editAdministratorCode.Enabled = True
      Check_FundUsesEqualisation.Enabled = True
      Check_ForceMultiSharClassReconciliation.Enabled = True
      Check_FundManagementFeesBeforeSubscriptions.Enabled = True
      Check_FundClosed.Enabled = True
      Check_DefaultTradeFxToFundCurrency.Enabled = True

      Combo_PricingFrequency.Enabled = True
      'Combo_MgmtFeesAccruePeriod.Enabled = True
      Combo_MgmtFeesPayPeriod.Enabled = True
      'Combo_PerfFeesAccruePeriod.Enabled = True
      Combo_PerfFeesPayPeriod.Enabled = True
      Combo_PerformanceFeeInstrument.Enabled = True
      Combo_PerformanceFeeCrystalisedInstrument.Enabled = True
      Combo_ManagementFeeInstrument.Enabled = True
      Date_FundYearEnd.Enabled = True

      Combo_ManagementCompany.Enabled = True
      Text_FundManagers.Enabled = True
      Text_ContactAddress.Enabled = True
      Combo_ContactPhone.Enabled = True
      Combo_ContactEMail.Enabled = True
      Date_InvestmentStartDate.Enabled = True
      Combo_MinimumInvestment.Enabled = True
      Combo_InitialSubscriptionFees.Enabled = True
      Combo_ManagementFees.Enabled = True
      Combo_PerformandeFees.Enabled = True
      Combo_SubscriptionFrequency.Enabled = True
      Combo_SubscriptionNotice.Enabled = True
      Combo_RedemptionFrequency.Enabled = True
      Combo_RedemptionNotice.Enabled = True
      Combo_Jurisdiction.Enabled = True
      Combo_Administrator.Enabled = True
      Combo_Custodian.Enabled = True
      Combo_Listings.Enabled = True
      Combo_ListedInstrument.Enabled = True
      Combo_ReviewGroup.Enabled = True
      Combo_FundReportGroup.Enabled = True
      Text_ISIN.Enabled = True
      Text_SEDOL.Enabled = True
      Text_Bloomberg.Enabled = True
      Text_Reuters.Enabled = True
      Text_Objective.Enabled = True
      Text_Disclaimer.Enabled = True

    Else

      Me.editFundCode.Enabled = False
      Me.editFundName.Enabled = False
      Me.editFundLegalEntity.Enabled = False
      Me.Combo_FundBaseCurrency.Enabled = False
      Me.Combo_FundUnit.Enabled = False
      Me.Combo_ParentFund.Enabled = False
      Me.editAdministratorCode.Enabled = False

      edit_MovementComission.Enabled = False
      Edit_SubscriptionPeriodsToValueDate.Enabled = False
      Edit_SubscriptionPeriodsToSettlementDate.Enabled = False
      Edit_RedemptionPeriodsToValueDate.Enabled = False
      Edit_RedemptionPeriodsToSettlementDate.Enabled = False
      edit_FundNAVReconciliationPlusDays.Enabled = False

      Check_FundUsesEqualisation.Enabled = False
      Check_ForceMultiSharClassReconciliation.Enabled = False
      Check_FundManagementFeesBeforeSubscriptions.Enabled = False
      Check_FundClosed.Enabled = False
      Check_DefaultTradeFxToFundCurrency.Enabled = False

      Combo_PricingFrequency.Enabled = False
      'Combo_MgmtFeesAccruePeriod.Enabled = False
      Combo_MgmtFeesPayPeriod.Enabled = False
      'Combo_PerfFeesAccruePeriod.Enabled = False
      Combo_PerfFeesPayPeriod.Enabled = False
      Combo_PerformanceFeeInstrument.Enabled = False
      Combo_PerformanceFeeCrystalisedInstrument.Enabled = False
      Combo_ManagementFeeInstrument.Enabled = False
      Date_FundYearEnd.Enabled = False

      Combo_ManagementCompany.Enabled = False
      Text_FundManagers.Enabled = False
      Text_ContactAddress.Enabled = False
      Combo_ContactPhone.Enabled = False
      Combo_ContactEMail.Enabled = False
      Date_InvestmentStartDate.Enabled = False
      Combo_MinimumInvestment.Enabled = False
      Combo_InitialSubscriptionFees.Enabled = False
      Combo_ManagementFees.Enabled = False
      Combo_PerformandeFees.Enabled = False
      Combo_SubscriptionFrequency.Enabled = False
      Combo_SubscriptionNotice.Enabled = False
      Combo_RedemptionFrequency.Enabled = False
      Combo_RedemptionNotice.Enabled = False
      Combo_Jurisdiction.Enabled = False
      Combo_Administrator.Enabled = False
      Combo_Custodian.Enabled = False
      Combo_Listings.Enabled = False
      Combo_ListedInstrument.Enabled = False
      Combo_ReviewGroup.Enabled = False
      Combo_FundReportGroup.Enabled = False
      Text_ISIN.Enabled = False
      Text_SEDOL.Enabled = False
      Text_Bloomberg.Enabled = False
      Text_Reuters.Enabled = False
      Text_Objective.Enabled = False
      Text_Disclaimer.Enabled = False

    End If

  End Sub

  ''' <summary>
  ''' Validates the form.
  ''' </summary>
  ''' <param name="pReturnString">The p return string.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function ValidateForm(ByRef pReturnString As String) As Boolean
    ' Form Validation code.
    ' 
    ' This code should be the final arbiter of what is allowed. no assumptions regarding 
    ' prior validation should be made.
    ' 
    ' This Code is called by the SetFormData routine before position changes.
    '
    Dim RVal As Boolean

    RVal = True
    pReturnString = ""

    ' Validate 

    If Me.editFundCode.Text.Length <= 0 Then
      pReturnString = "Fund Code must not be left blank."
      RVal = False
    End If

    If Me.editFundName.Text.Length <= 0 Then
      pReturnString = "Fund Name must not be left blank."
      RVal = False
    End If

    If Me.Combo_FundBaseCurrency.SelectedIndex < 0 Then
      pReturnString = "Base Currency must be selected"
      RVal = False
    End If

    If Me.Combo_FundUnit.SelectedIndex < 0 Then
      pReturnString = "Fund Unit must be selected"
      RVal = False
    End If

    Return RVal

  End Function

  ''' <summary>
  ''' Handles the MouseEnter event of the btnCancel control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnCancel_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseEnter
    ' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
    Me.IsOverCancelButton = True
  End Sub

  ''' <summary>
  ''' Handles the MouseLeave event of the btnCancel control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnCancel_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseLeave
    ' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
    Me.IsOverCancelButton = False
  End Sub


#End Region

#Region " Navigation Code / GetPosition() (Generic Code) "

  ''' <summary>
  ''' Handles the SelectComboChanged event of the Combo control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_SelectComboChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' Selection Combo. SelectedItem changed.
    '

    ' Don't react to changes made in paint routines etc.
    If InPaint = True Then Exit Sub

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    ' Find the correct data row, then show it...
    thisPosition = THIS_FORM_SelectingCombo.SelectedIndex

    If (thisPosition >= 0) Then
      thisDataRow = Me.SortedRows(thisPosition)
    Else
      thisDataRow = Nothing
    End If

    Call GetFormData(thisDataRow)

  End Sub


  ''' <summary>
  ''' Handles the Click event of the btnNavPrev control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnNavPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavPrev.Click
    ' 'Previous' Button.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    If thisPosition > 0 Then thisPosition -= 1
    If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
      thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
    End If

    THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

    Exit Sub

    If myTable.Rows.Count > thisPosition Then
      thisDataRow = myTable.Rows(thisPosition)
      Call GetFormData(thisDataRow)
    Else
      Call GetFormData(Nothing)
    End If

  End Sub

  ''' <summary>
  ''' Handles the Click event of the btnNavNext control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnNavNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavNext.Click
    ' 'Next' Button.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    If thisPosition < (myTable.Rows.Count - 1) Then thisPosition += 1
    If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
      thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
    End If

    THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

    Exit Sub

    If myTable.Rows.Count > thisPosition Then
      thisDataRow = myTable.Rows(thisPosition)
      Call GetFormData(thisDataRow)
    Else
      Call GetFormData(Nothing)
    End If

  End Sub

  ''' <summary>
  ''' Handles the Click event of the btnNavFirst control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnNavFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavFirst.Click
    ' 'First' Button.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    thisPosition = 0
    If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
      thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
    End If

    If (THIS_FORM_SelectingCombo.SelectedIndex = thisPosition) Then
      Combo_SelectComboChanged(THIS_FORM_SelectingCombo, New EventArgs)
    Else
      THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
    End If

  End Sub

  ''' <summary>
  ''' Handles the Click event of the btnLast control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLast.Click
    ' 'Last' Button.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    thisPosition = THIS_FORM_SelectingCombo.Items.Count - 1

    THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

  End Sub

  ''' <summary>
  ''' Get_s the position.
  ''' </summary>
  ''' <param name="pAuditID">The p audit ID.</param>
  ''' <returns>System.Int32.</returns>
  Private Function Get_Position(ByVal pAuditID As Integer) As Integer
    ' Returns the position in the current 'SortedRows' array of the entry corresponding to the given 
    ' AudidID.

    Dim MinIndex As Integer
    Dim MaxIndex As Integer
    Dim CurrentMin As Integer
    Dim CurrentMax As Integer
    Dim searchDataRow As DataRow

    Try
      ' SortedRows exists ?

      If SortedRows Is Nothing Then
        Return (-1)
        Exit Function
      End If

      ' Return (-1) if there are no rows in the 'SortedRows'

      If (SortedRows.GetLength(0) <= 0) Then
        Return (-1)
        Exit Function
      End If


      ' Use a modified Search moving outwards from the last returned value to locate the required value.
      ' Reflecting the fact that for the most part One looks for closely located records.

      MinIndex = 0
      MaxIndex = SortedRows.GetLength(0) - 1


      ' Check First and Last records (Incase 'First' or 'Last' was pressed).

      searchDataRow = SortedRows(MinIndex)

      If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(searchDataRow("AuditID")) = pAuditID) Then
        Return MinIndex
        Exit Function
      End If

      searchDataRow = SortedRows(MaxIndex)
      If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(searchDataRow("AuditID")) = pAuditID) Then
        Return MaxIndex
        Exit Function
      End If

      ' now search outwards from the last returned value.

      If (Me.thisPosition >= MinIndex) And (Me.thisPosition <= MaxIndex) Then
        CurrentMin = thisPosition
        CurrentMax = CurrentMin + 1
      Else
        CurrentMin = CInt((MinIndex + MaxIndex) / 2)
        CurrentMax = CurrentMin + 1
      End If

      While (CurrentMin >= MinIndex) Or (CurrentMax <= MaxIndex)
        If (CurrentMin >= MinIndex) Then
          searchDataRow = SortedRows(CurrentMin)

          If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(searchDataRow("AuditID")) = pAuditID) Then
            Return CurrentMin
            Exit Function
          End If

          CurrentMin -= 1
        End If

        If (CurrentMax <= MaxIndex) Then
          searchDataRow = SortedRows(CurrentMax)

          If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(searchDataRow("AuditID")) = pAuditID) Then
            Return CurrentMax
            Exit Function
          End If

          CurrentMax += 1
        End If

      End While

      Return (-1)
      Exit Function

    Catch ex As Exception
      MainForm.LogError(Me.Name & ", Get_Position()", LOG_LEVELS.Warning, ex.Message, "", ex.StackTrace, True)
      Return (-1)
      Exit Function
    End Try

  End Function

#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

  ''' <summary>
  ''' Handles the Click event of the btnCancel control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    ' Cancel Changes, redisplay form.

    FormChanged = False
    AddNewRecord = False
    Me.THIS_FORM_SelectingCombo.Enabled = True

    If (thisPosition >= 0) And (thisPosition < Me.SortedRows.GetLength(0)) Then
      thisDataRow = Me.SortedRows(thisPosition)
      Call GetFormData(thisDataRow)
    Else
      Call btnNavFirst_Click(Me, New System.EventArgs)
    End If

  End Sub

  ''' <summary>
  ''' Handles the Click event of the btnSave control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
    ' Save Changes, if any, without prompting.

    If (FormChanged = True) Then
      Call SetFormData(False)
    End If

  End Sub

  ''' <summary>
  ''' Handles the Click event of the btnDelete control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
    Dim Position As Integer
    Dim NextAuditID As Integer

    If (AddNewRecord = True) Then
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Sub
    End If

    ' No Appropriate Save permission :-

    If (Me.HasDeletePermission = False) Then
      MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "You do not have permission to Delete this record.", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Sub
    End If

    ' *************************************************************
    ' KnowledgeDate OK :-
    ' *************************************************************

    If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
      MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "Changes to Venice are not allowed when the Knowledgedate is not set to `NOW`", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Sub
    End If

    ' *************************************************************
    ' Confirm :-
    ' *************************************************************
    If MsgBox("Are you Sure ?", MsgBoxStyle.OkCancel, "Confirm Delete") = MsgBoxResult.Cancel Then
      Exit Sub
    End If

    ' Check Data position.

    Position = Get_Position(thisAuditID)

    If Position < 0 Then Exit Sub

    ' Check Referential Integrity 
    If CheckReferentialIntegrity(_MainForm, Me.SortedRows(Position)) = False Then
      MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "Deleting this record would voilate referential integrity.", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Sub
    End If

    ' Resolve row to show after deleting this one.

    NextAuditID = (-1)
    If (Position + 1) < Me.SortedRows.GetLength(0) Then
      NextAuditID = CInt(Me.SortedRows(Position + 1)("AuditID"))
    ElseIf (Position - 1) >= 0 Then
      NextAuditID = CInt(Me.SortedRows(Position - 1)("AuditID"))
    Else
      NextAuditID = (-1)
    End If

    ' Delete this row

    Try
      Me.SortedRows(Position).Delete()

      MainForm.AdaptorUpdate(Me.Name, myAdaptor, myTable)

    Catch ex As Exception

      Call MainForm.LogError(Me.Name & ", Delete()", LOG_LEVELS.Error, ex.Message, "Error Deleting Record.", ex.StackTrace, True)
    End Try

    ' Tidy Up.

    FormChanged = False

    thisAuditID = NextAuditID
    Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID), True)

  End Sub

  ''' <summary>
  ''' Handles the Click event of the btnAdd control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
    ' Prepare form to Add a new record.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    thisPosition = -1
    InPaint = True
    MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
    InPaint = False

    GetFormData(Nothing)
    AddNewRecord = True

    Me.THIS_FORM_SelectingCombo.Enabled = False
    Me.btnCancel.Enabled = True

    Call SetButtonStatus()

    THIS_FORM_NewMoveToControl.Focus()

  End Sub

  ''' <summary>
  ''' Handles the Click event of the btnClose control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
    ' Close Form

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    Me.Close()

  End Sub

#End Region

#Region " Bug hunting "

  'AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
  'AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
  'AddHandler myAdaptor.FillError, AddressOf OnRowFillError

    ''' <summary>
    ''' Called when [row updating].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
	Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
		Dim a As Integer

		If Not e.Command Is Nothing Then
			a = 1
		End If

		If Not e.Errors Is Nothing Then
			a = 2
		End If
	End Sub

    ''' <summary>
    ''' Called when [row updated].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
	Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
		Dim a As Integer

		If Not e.Command Is Nothing Then
			a = 1
		End If

		If Not e.Errors Is Nothing Then
			a = 2
		End If
	End Sub

    ''' <summary>
    ''' Called when [row fill error].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
	Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
		Dim a As Integer

		a = 1
	End Sub

#End Region






End Class
