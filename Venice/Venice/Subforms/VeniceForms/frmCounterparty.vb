' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmCounterparty.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals

Imports RenaissanceControls

''' <summary>
''' Class frmCounterparty
''' </summary>
Public Class frmCounterparty

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmCounterparty"/> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The edit audit ID
    ''' </summary>
  Friend WithEvents editAuditID As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The BTN nav first
    ''' </summary>
  Friend WithEvents btnNavFirst As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN nav prev
    ''' </summary>
  Friend WithEvents btnNavPrev As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN nav next
    ''' </summary>
  Friend WithEvents btnNavNext As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN last
    ''' </summary>
  Friend WithEvents btnLast As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN add
    ''' </summary>
  Friend WithEvents btnAdd As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN delete
    ''' </summary>
  Friend WithEvents btnDelete As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN cancel
    ''' </summary>
  Friend WithEvents btnCancel As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN save
    ''' </summary>
  Friend WithEvents btnSave As System.Windows.Forms.Button
    ''' <summary>
    ''' The panel1
    ''' </summary>
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The label1
    ''' </summary>
  Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The group box1
    ''' </summary>
  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The BTN close
    ''' </summary>
  Friend WithEvents btnClose As System.Windows.Forms.Button
    ''' <summary>
    ''' The combo_ select counterparty
    ''' </summary>
  Friend WithEvents Combo_SelectCounterparty As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The LBL counterparty
    ''' </summary>
  Friend WithEvents lblCounterparty As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ address1
    ''' </summary>
  Friend WithEvents edit_Address1 As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The label_ cpty is fund
    ''' </summary>
  Friend WithEvents label_CptyIsFund As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ investor group
    ''' </summary>
  Friend WithEvents Combo_InvestorGroup As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ fund ID
    ''' </summary>
  Friend WithEvents Combo_FundID As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The check_ cpty is fund
    ''' </summary>
	Friend WithEvents Check_CptyIsFund As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ cpty is A leverage provider
    ''' </summary>
	Friend WithEvents Check_CptyIsALeverageProvider As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The label_ cpty is leverage
    ''' </summary>
	Friend WithEvents Label_CptyIsLeverage As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ management fee
    ''' </summary>
	Friend WithEvents edit_ManagementFee As PercentageTextBox
    ''' <summary>
    ''' The edit_ performance fee threshold
    ''' </summary>
	Friend WithEvents edit_PerformanceFeeThreshold As PercentageTextBox
    ''' <summary>
    ''' The edit_ performance fee
    ''' </summary>
	Friend WithEvents edit_PerformanceFee As PercentageTextBox
    ''' <summary>
    ''' The edit_ address2
    ''' </summary>
	Friend WithEvents edit_Address2 As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The edit_ address3
    ''' </summary>
	Friend WithEvents edit_Address3 As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The edit_ address4
    ''' </summary>
	Friend WithEvents edit_Address4 As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The edit_ address5
    ''' </summary>
	Friend WithEvents edit_Address5 As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The label7
    ''' </summary>
	Friend WithEvents Label7 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ postcode
    ''' </summary>
	Friend WithEvents edit_Postcode As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The label8
    ''' </summary>
	Friend WithEvents Label8 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ account name
    ''' </summary>
	Friend WithEvents edit_AccountName As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The label9
    ''' </summary>
	Friend WithEvents Label9 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ email address
    ''' </summary>
	Friend WithEvents edit_EmailAddress As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The label10
    ''' </summary>
	Friend WithEvents Label10 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ phone number
    ''' </summary>
	Friend WithEvents edit_PhoneNumber As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The label11
    ''' </summary>
	Friend WithEvents Label11 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ fax number
    ''' </summary>
	Friend WithEvents edit_FaxNumber As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The label12
    ''' </summary>
	Friend WithEvents Label12 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ PFPCID
    ''' </summary>
	Friend WithEvents edit_PFPCID As C1.Win.C1Input.C1TextBox
    ''' <summary>
    ''' The edit_ counterparty
    ''' </summary>
	Friend WithEvents edit_Counterparty As C1.Win.C1Input.C1TextBox
    ''' <summary>
    ''' The edit_ contact name
    ''' </summary>
	Friend WithEvents edit_ContactName As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The LBL PFPC
    ''' </summary>
	Friend WithEvents lblPFPC As System.Windows.Forms.Label
    ''' <summary>
    ''' The LBL address
    ''' </summary>
	Friend WithEvents lblAddress As System.Windows.Forms.Label
    ''' <summary>
    ''' The root menu
    ''' </summary>
	Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
    ''' <summary>
    ''' The group box_ fees
    ''' </summary>
	Friend WithEvents GroupBox_Fees As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The label17
    ''' </summary>
	Friend WithEvents Label17 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label16
    ''' </summary>
	Friend WithEvents Label16 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label15
    ''' </summary>
	Friend WithEvents Label15 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label20
    ''' </summary>
	Friend WithEvents Label20 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ MGMT_ rebate on debt
    ''' </summary>
	Friend WithEvents edit_Mgmt_RebateOnDebt As RenaissanceControls.PercentageTextBox
    ''' <summary>
    ''' The edit_ perf_ rebate on debt
    ''' </summary>
	Friend WithEvents edit_Perf_RebateOnDebt As RenaissanceControls.PercentageTextBox
    ''' <summary>
    ''' The edit_ MGMT_ rebate on equity
    ''' </summary>
	Friend WithEvents edit_Mgmt_RebateOnEquity As RenaissanceControls.PercentageTextBox
    ''' <summary>
    ''' The edit_ perf_ rebate on equity
    ''' </summary>
	Friend WithEvents edit_Perf_RebateOnEquity As RenaissanceControls.PercentageTextBox
    ''' <summary>
    ''' The label18
    ''' </summary>
	Friend WithEvents Label18 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label24
    ''' </summary>
	Friend WithEvents Label24 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label23
    ''' </summary>
	Friend WithEvents Label23 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ MGMT_ marketing to others
    ''' </summary>
	Friend WithEvents edit_Mgmt_MarketingToOthers As RenaissanceControls.PercentageTextBox
    ''' <summary>
    ''' The edit_ perf_ marketing to others
    ''' </summary>
	Friend WithEvents edit_Perf_MarketingToOthers As RenaissanceControls.PercentageTextBox
    ''' <summary>
    ''' The label22
    ''' </summary>
	Friend WithEvents Label22 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ MGMT_ marketing to FCAM
    ''' </summary>
	Friend WithEvents edit_Mgmt_MarketingToFCAM As RenaissanceControls.PercentageTextBox
    ''' <summary>
    ''' The edit_ perf_ marketing to FCAM
    ''' </summary>
	Friend WithEvents edit_Perf_MarketingToFCAM As RenaissanceControls.PercentageTextBox
    ''' <summary>
    ''' The label19
    ''' </summary>
	Friend WithEvents Label19 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label21
    ''' </summary>
	Friend WithEvents Label21 As System.Windows.Forms.Label
    ''' <summary>
    ''' The panel3
    ''' </summary>
	Friend WithEvents Panel3 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The panel2
    ''' </summary>
	Friend WithEvents Panel2 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The label3
    ''' </summary>
	Friend WithEvents Label3 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label2
    ''' </summary>
	Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label4
    ''' </summary>
  Friend WithEvents Label4 As System.Windows.Forms.Label
  Friend WithEvents Check_IsMarketCpty As System.Windows.Forms.CheckBox
  Friend WithEvents Label5 As System.Windows.Forms.Label
    ''' <summary>
    ''' The LBL IG
    ''' </summary>
	Friend WithEvents lblIG As System.Windows.Forms.Label
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.lblCounterparty = New System.Windows.Forms.Label
    Me.lblPFPC = New System.Windows.Forms.Label
    Me.lblAddress = New System.Windows.Forms.Label
    Me.editAuditID = New System.Windows.Forms.TextBox
    Me.edit_Address1 = New System.Windows.Forms.TextBox
    Me.lblIG = New System.Windows.Forms.Label
    Me.label_CptyIsFund = New System.Windows.Forms.Label
    Me.btnNavFirst = New System.Windows.Forms.Button
    Me.btnNavPrev = New System.Windows.Forms.Button
    Me.btnNavNext = New System.Windows.Forms.Button
    Me.btnLast = New System.Windows.Forms.Button
    Me.btnAdd = New System.Windows.Forms.Button
    Me.btnDelete = New System.Windows.Forms.Button
    Me.btnCancel = New System.Windows.Forms.Button
    Me.btnSave = New System.Windows.Forms.Button
    Me.Combo_SelectCounterparty = New System.Windows.Forms.ComboBox
    Me.Panel1 = New System.Windows.Forms.Panel
    Me.Label1 = New System.Windows.Forms.Label
    Me.GroupBox1 = New System.Windows.Forms.GroupBox
    Me.Combo_InvestorGroup = New System.Windows.Forms.ComboBox
    Me.Combo_FundID = New System.Windows.Forms.ComboBox
    Me.btnClose = New System.Windows.Forms.Button
    Me.Check_CptyIsFund = New System.Windows.Forms.CheckBox
    Me.Check_CptyIsALeverageProvider = New System.Windows.Forms.CheckBox
    Me.Label_CptyIsLeverage = New System.Windows.Forms.Label
    Me.edit_ManagementFee = New RenaissanceControls.PercentageTextBox
    Me.edit_PerformanceFeeThreshold = New RenaissanceControls.PercentageTextBox
    Me.edit_PerformanceFee = New RenaissanceControls.PercentageTextBox
    Me.edit_Address2 = New System.Windows.Forms.TextBox
    Me.edit_Address3 = New System.Windows.Forms.TextBox
    Me.edit_Address4 = New System.Windows.Forms.TextBox
    Me.edit_Address5 = New System.Windows.Forms.TextBox
    Me.Label7 = New System.Windows.Forms.Label
    Me.edit_Postcode = New System.Windows.Forms.TextBox
    Me.edit_ContactName = New System.Windows.Forms.TextBox
    Me.Label8 = New System.Windows.Forms.Label
    Me.edit_AccountName = New System.Windows.Forms.TextBox
    Me.Label9 = New System.Windows.Forms.Label
    Me.edit_EmailAddress = New System.Windows.Forms.TextBox
    Me.Label10 = New System.Windows.Forms.Label
    Me.edit_PhoneNumber = New System.Windows.Forms.TextBox
    Me.Label11 = New System.Windows.Forms.Label
    Me.edit_FaxNumber = New System.Windows.Forms.TextBox
    Me.Label12 = New System.Windows.Forms.Label
    Me.edit_PFPCID = New C1.Win.C1Input.C1TextBox
    Me.edit_Counterparty = New C1.Win.C1Input.C1TextBox
    Me.RootMenu = New System.Windows.Forms.MenuStrip
    Me.GroupBox_Fees = New System.Windows.Forms.GroupBox
    Me.Label4 = New System.Windows.Forms.Label
    Me.Panel3 = New System.Windows.Forms.Panel
    Me.Panel2 = New System.Windows.Forms.Panel
    Me.Label3 = New System.Windows.Forms.Label
    Me.Label2 = New System.Windows.Forms.Label
    Me.Label24 = New System.Windows.Forms.Label
    Me.Label23 = New System.Windows.Forms.Label
    Me.edit_Mgmt_MarketingToOthers = New RenaissanceControls.PercentageTextBox
    Me.edit_Perf_MarketingToOthers = New RenaissanceControls.PercentageTextBox
    Me.Label22 = New System.Windows.Forms.Label
    Me.edit_Mgmt_MarketingToFCAM = New RenaissanceControls.PercentageTextBox
    Me.edit_Perf_MarketingToFCAM = New RenaissanceControls.PercentageTextBox
    Me.Label19 = New System.Windows.Forms.Label
    Me.Label21 = New System.Windows.Forms.Label
    Me.Label20 = New System.Windows.Forms.Label
    Me.edit_Mgmt_RebateOnDebt = New RenaissanceControls.PercentageTextBox
    Me.edit_Perf_RebateOnDebt = New RenaissanceControls.PercentageTextBox
    Me.edit_Mgmt_RebateOnEquity = New RenaissanceControls.PercentageTextBox
    Me.edit_Perf_RebateOnEquity = New RenaissanceControls.PercentageTextBox
    Me.Label18 = New System.Windows.Forms.Label
    Me.Label17 = New System.Windows.Forms.Label
    Me.Label16 = New System.Windows.Forms.Label
    Me.Label15 = New System.Windows.Forms.Label
    Me.Check_IsMarketCpty = New System.Windows.Forms.CheckBox
    Me.Label5 = New System.Windows.Forms.Label
    Me.Panel1.SuspendLayout()
    CType(Me.edit_PFPCID, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.edit_Counterparty, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.GroupBox_Fees.SuspendLayout()
    Me.SuspendLayout()
    '
    'lblCounterparty
    '
    Me.lblCounterparty.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.lblCounterparty.Location = New System.Drawing.Point(8, 67)
    Me.lblCounterparty.Name = "lblCounterparty"
    Me.lblCounterparty.Size = New System.Drawing.Size(100, 16)
    Me.lblCounterparty.TabIndex = 39
    Me.lblCounterparty.Text = "Counterparty"
    '
    'lblPFPC
    '
    Me.lblPFPC.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.lblPFPC.Location = New System.Drawing.Point(8, 91)
    Me.lblPFPC.Name = "lblPFPC"
    Me.lblPFPC.Size = New System.Drawing.Size(127, 16)
    Me.lblPFPC.TabIndex = 40
    Me.lblPFPC.Text = "Administrator Account ID"
    '
    'lblAddress
    '
    Me.lblAddress.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.lblAddress.Location = New System.Drawing.Point(8, 341)
    Me.lblAddress.Name = "lblAddress"
    Me.lblAddress.Size = New System.Drawing.Size(100, 16)
    Me.lblAddress.TabIndex = 41
    Me.lblAddress.Text = "Address"
    '
    'editAuditID
    '
    Me.editAuditID.Enabled = False
    Me.editAuditID.Location = New System.Drawing.Point(344, 32)
    Me.editAuditID.Name = "editAuditID"
    Me.editAuditID.Size = New System.Drawing.Size(64, 20)
    Me.editAuditID.TabIndex = 1
    '
    'edit_Address1
    '
    Me.edit_Address1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.edit_Address1.Location = New System.Drawing.Point(140, 338)
    Me.edit_Address1.Name = "edit_Address1"
    Me.edit_Address1.Size = New System.Drawing.Size(342, 20)
    Me.edit_Address1.TabIndex = 10
    '
    'lblIG
    '
    Me.lblIG.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.lblIG.Location = New System.Drawing.Point(8, 115)
    Me.lblIG.Name = "lblIG"
    Me.lblIG.Size = New System.Drawing.Size(106, 16)
    Me.lblIG.TabIndex = 48
    Me.lblIG.Text = "Investor Group"
    '
    'label_CptyIsFund
    '
    Me.label_CptyIsFund.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.label_CptyIsFund.Location = New System.Drawing.Point(8, 292)
    Me.label_CptyIsFund.Name = "label_CptyIsFund"
    Me.label_CptyIsFund.Size = New System.Drawing.Size(80, 16)
    Me.label_CptyIsFund.TabIndex = 49
    Me.label_CptyIsFund.Text = "Cpty is a Fund"
    '
    'btnNavFirst
    '
    Me.btnNavFirst.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNavFirst.Location = New System.Drawing.Point(8, 8)
    Me.btnNavFirst.Name = "btnNavFirst"
    Me.btnNavFirst.Size = New System.Drawing.Size(40, 28)
    Me.btnNavFirst.TabIndex = 0
    Me.btnNavFirst.Text = "<<"
    '
    'btnNavPrev
    '
    Me.btnNavPrev.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNavPrev.Location = New System.Drawing.Point(49, 8)
    Me.btnNavPrev.Name = "btnNavPrev"
    Me.btnNavPrev.Size = New System.Drawing.Size(35, 28)
    Me.btnNavPrev.TabIndex = 1
    Me.btnNavPrev.Text = "<"
    '
    'btnNavNext
    '
    Me.btnNavNext.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNavNext.Location = New System.Drawing.Point(88, 8)
    Me.btnNavNext.Name = "btnNavNext"
    Me.btnNavNext.Size = New System.Drawing.Size(35, 28)
    Me.btnNavNext.TabIndex = 2
    Me.btnNavNext.Text = ">"
    '
    'btnLast
    '
    Me.btnLast.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnLast.Location = New System.Drawing.Point(124, 8)
    Me.btnLast.Name = "btnLast"
    Me.btnLast.Size = New System.Drawing.Size(40, 28)
    Me.btnLast.TabIndex = 3
    Me.btnLast.Text = ">>"
    '
    'btnAdd
    '
    Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnAdd.Location = New System.Drawing.Point(176, 8)
    Me.btnAdd.Name = "btnAdd"
    Me.btnAdd.Size = New System.Drawing.Size(75, 28)
    Me.btnAdd.TabIndex = 4
    Me.btnAdd.Text = "&New"
    '
    'btnDelete
    '
    Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnDelete.Location = New System.Drawing.Point(165, 662)
    Me.btnDelete.Name = "btnDelete"
    Me.btnDelete.Size = New System.Drawing.Size(75, 28)
    Me.btnDelete.TabIndex = 23
    Me.btnDelete.Text = "&Delete"
    '
    'btnCancel
    '
    Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnCancel.Location = New System.Drawing.Point(249, 662)
    Me.btnCancel.Name = "btnCancel"
    Me.btnCancel.Size = New System.Drawing.Size(75, 28)
    Me.btnCancel.TabIndex = 24
    Me.btnCancel.Text = "&Cancel"
    '
    'btnSave
    '
    Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnSave.Location = New System.Drawing.Point(77, 662)
    Me.btnSave.Name = "btnSave"
    Me.btnSave.Size = New System.Drawing.Size(75, 28)
    Me.btnSave.TabIndex = 22
    Me.btnSave.Text = "&Save"
    '
    'Combo_SelectCounterparty
    '
    Me.Combo_SelectCounterparty.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectCounterparty.Location = New System.Drawing.Point(140, 32)
    Me.Combo_SelectCounterparty.Name = "Combo_SelectCounterparty"
    Me.Combo_SelectCounterparty.Size = New System.Drawing.Size(180, 21)
    Me.Combo_SelectCounterparty.TabIndex = 0
    '
    'Panel1
    '
    Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Panel1.Controls.Add(Me.btnNavFirst)
    Me.Panel1.Controls.Add(Me.btnNavPrev)
    Me.Panel1.Controls.Add(Me.btnNavNext)
    Me.Panel1.Controls.Add(Me.btnLast)
    Me.Panel1.Controls.Add(Me.btnAdd)
    Me.Panel1.Location = New System.Drawing.Point(111, 608)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(260, 48)
    Me.Panel1.TabIndex = 21
    '
    'Label1
    '
    Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label1.Location = New System.Drawing.Point(8, 32)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(100, 23)
    Me.Label1.TabIndex = 33
    Me.Label1.Text = "Select"
    '
    'GroupBox1
    '
    Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.GroupBox1.Location = New System.Drawing.Point(8, 56)
    Me.GroupBox1.Name = "GroupBox1"
    Me.GroupBox1.Size = New System.Drawing.Size(474, 4)
    Me.GroupBox1.TabIndex = 77
    Me.GroupBox1.TabStop = False
    Me.GroupBox1.Text = "GroupBox1"
    '
    'Combo_InvestorGroup
    '
    Me.Combo_InvestorGroup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_InvestorGroup.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_InvestorGroup.Location = New System.Drawing.Point(140, 112)
    Me.Combo_InvestorGroup.Name = "Combo_InvestorGroup"
    Me.Combo_InvestorGroup.Size = New System.Drawing.Size(342, 21)
    Me.Combo_InvestorGroup.TabIndex = 4
    '
    'Combo_FundID
    '
    Me.Combo_FundID.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_FundID.Enabled = False
    Me.Combo_FundID.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_FundID.Location = New System.Drawing.Point(140, 289)
    Me.Combo_FundID.Name = "Combo_FundID"
    Me.Combo_FundID.Size = New System.Drawing.Size(342, 21)
    Me.Combo_FundID.TabIndex = 8
    '
    'btnClose
    '
    Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnClose.Location = New System.Drawing.Point(333, 662)
    Me.btnClose.Name = "btnClose"
    Me.btnClose.Size = New System.Drawing.Size(75, 28)
    Me.btnClose.TabIndex = 25
    Me.btnClose.Text = "&Close"
    '
    'Check_CptyIsFund
    '
    Me.Check_CptyIsFund.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_CptyIsFund.Location = New System.Drawing.Point(118, 289)
    Me.Check_CptyIsFund.Name = "Check_CptyIsFund"
    Me.Check_CptyIsFund.Size = New System.Drawing.Size(16, 24)
    Me.Check_CptyIsFund.TabIndex = 7
    '
    'Check_CptyIsALeverageProvider
    '
    Me.Check_CptyIsALeverageProvider.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_CptyIsALeverageProvider.Location = New System.Drawing.Point(164, 311)
    Me.Check_CptyIsALeverageProvider.Name = "Check_CptyIsALeverageProvider"
    Me.Check_CptyIsALeverageProvider.Size = New System.Drawing.Size(16, 24)
    Me.Check_CptyIsALeverageProvider.TabIndex = 9
    '
    'Label_CptyIsLeverage
    '
    Me.Label_CptyIsLeverage.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label_CptyIsLeverage.Location = New System.Drawing.Point(8, 316)
    Me.Label_CptyIsLeverage.Name = "Label_CptyIsLeverage"
    Me.Label_CptyIsLeverage.Size = New System.Drawing.Size(140, 16)
    Me.Label_CptyIsLeverage.TabIndex = 86
    Me.Label_CptyIsLeverage.Text = "Cpty is a leverage provider"
    '
    'edit_ManagementFee
    '
    Me.edit_ManagementFee.Location = New System.Drawing.Point(138, 49)
    Me.edit_ManagementFee.MaxLength = 100
    Me.edit_ManagementFee.Name = "edit_ManagementFee"
    Me.edit_ManagementFee.RenaissanceTag = Nothing
    Me.edit_ManagementFee.SelectTextOnFocus = False
    Me.edit_ManagementFee.Size = New System.Drawing.Size(50, 20)
    Me.edit_ManagementFee.TabIndex = 11
    Me.edit_ManagementFee.Text = "0.00%"
    Me.edit_ManagementFee.TextFormat = "#,##0.00%"
    Me.edit_ManagementFee.Value = 0
    Me.edit_ManagementFee.WordWrap = False
    '
    'edit_PerformanceFeeThreshold
    '
    Me.edit_PerformanceFeeThreshold.Location = New System.Drawing.Point(194, 77)
    Me.edit_PerformanceFeeThreshold.MaxLength = 100
    Me.edit_PerformanceFeeThreshold.Name = "edit_PerformanceFeeThreshold"
    Me.edit_PerformanceFeeThreshold.RenaissanceTag = Nothing
    Me.edit_PerformanceFeeThreshold.SelectTextOnFocus = False
    Me.edit_PerformanceFeeThreshold.Size = New System.Drawing.Size(50, 20)
    Me.edit_PerformanceFeeThreshold.TabIndex = 18
    Me.edit_PerformanceFeeThreshold.Text = "0.00%"
    Me.edit_PerformanceFeeThreshold.TextFormat = "#,##0.00%"
    Me.edit_PerformanceFeeThreshold.Value = 0
    Me.edit_PerformanceFeeThreshold.WordWrap = False
    '
    'edit_PerformanceFee
    '
    Me.edit_PerformanceFee.Location = New System.Drawing.Point(138, 77)
    Me.edit_PerformanceFee.MaxLength = 100
    Me.edit_PerformanceFee.Name = "edit_PerformanceFee"
    Me.edit_PerformanceFee.RenaissanceTag = Nothing
    Me.edit_PerformanceFee.SelectTextOnFocus = False
    Me.edit_PerformanceFee.Size = New System.Drawing.Size(50, 20)
    Me.edit_PerformanceFee.TabIndex = 17
    Me.edit_PerformanceFee.Text = "0.00%"
    Me.edit_PerformanceFee.TextFormat = "#,##0.00%"
    Me.edit_PerformanceFee.Value = 0
    Me.edit_PerformanceFee.WordWrap = False
    '
    'edit_Address2
    '
    Me.edit_Address2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.edit_Address2.Location = New System.Drawing.Point(140, 362)
    Me.edit_Address2.Name = "edit_Address2"
    Me.edit_Address2.Size = New System.Drawing.Size(342, 20)
    Me.edit_Address2.TabIndex = 11
    '
    'edit_Address3
    '
    Me.edit_Address3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.edit_Address3.Location = New System.Drawing.Point(140, 386)
    Me.edit_Address3.Name = "edit_Address3"
    Me.edit_Address3.Size = New System.Drawing.Size(342, 20)
    Me.edit_Address3.TabIndex = 12
    '
    'edit_Address4
    '
    Me.edit_Address4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.edit_Address4.Location = New System.Drawing.Point(140, 410)
    Me.edit_Address4.Name = "edit_Address4"
    Me.edit_Address4.Size = New System.Drawing.Size(342, 20)
    Me.edit_Address4.TabIndex = 13
    '
    'edit_Address5
    '
    Me.edit_Address5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.edit_Address5.Location = New System.Drawing.Point(140, 434)
    Me.edit_Address5.Name = "edit_Address5"
    Me.edit_Address5.Size = New System.Drawing.Size(342, 20)
    Me.edit_Address5.TabIndex = 14
    '
    'Label7
    '
    Me.Label7.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label7.Location = New System.Drawing.Point(8, 462)
    Me.Label7.Name = "Label7"
    Me.Label7.Size = New System.Drawing.Size(100, 16)
    Me.Label7.TabIndex = 96
    Me.Label7.Text = "Postcode / Zip"
    '
    'edit_Postcode
    '
    Me.edit_Postcode.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.edit_Postcode.Location = New System.Drawing.Point(140, 458)
    Me.edit_Postcode.Name = "edit_Postcode"
    Me.edit_Postcode.Size = New System.Drawing.Size(342, 20)
    Me.edit_Postcode.TabIndex = 15
    '
    'edit_ContactName
    '
    Me.edit_ContactName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.edit_ContactName.Location = New System.Drawing.Point(140, 482)
    Me.edit_ContactName.Name = "edit_ContactName"
    Me.edit_ContactName.Size = New System.Drawing.Size(342, 20)
    Me.edit_ContactName.TabIndex = 16
    '
    'Label8
    '
    Me.Label8.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label8.Location = New System.Drawing.Point(8, 486)
    Me.Label8.Name = "Label8"
    Me.Label8.Size = New System.Drawing.Size(100, 16)
    Me.Label8.TabIndex = 98
    Me.Label8.Text = "Contact Name(s)"
    '
    'edit_AccountName
    '
    Me.edit_AccountName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.edit_AccountName.Location = New System.Drawing.Point(140, 506)
    Me.edit_AccountName.Name = "edit_AccountName"
    Me.edit_AccountName.Size = New System.Drawing.Size(342, 20)
    Me.edit_AccountName.TabIndex = 17
    '
    'Label9
    '
    Me.Label9.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label9.Location = New System.Drawing.Point(8, 510)
    Me.Label9.Name = "Label9"
    Me.Label9.Size = New System.Drawing.Size(100, 16)
    Me.Label9.TabIndex = 100
    Me.Label9.Text = "Account Name"
    '
    'edit_EmailAddress
    '
    Me.edit_EmailAddress.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.edit_EmailAddress.Location = New System.Drawing.Point(140, 530)
    Me.edit_EmailAddress.Name = "edit_EmailAddress"
    Me.edit_EmailAddress.Size = New System.Drawing.Size(342, 20)
    Me.edit_EmailAddress.TabIndex = 18
    '
    'Label10
    '
    Me.Label10.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label10.Location = New System.Drawing.Point(8, 534)
    Me.Label10.Name = "Label10"
    Me.Label10.Size = New System.Drawing.Size(100, 16)
    Me.Label10.TabIndex = 102
    Me.Label10.Text = "Email Address"
    '
    'edit_PhoneNumber
    '
    Me.edit_PhoneNumber.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.edit_PhoneNumber.Location = New System.Drawing.Point(140, 554)
    Me.edit_PhoneNumber.Name = "edit_PhoneNumber"
    Me.edit_PhoneNumber.Size = New System.Drawing.Size(342, 20)
    Me.edit_PhoneNumber.TabIndex = 19
    '
    'Label11
    '
    Me.Label11.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label11.Location = New System.Drawing.Point(8, 558)
    Me.Label11.Name = "Label11"
    Me.Label11.Size = New System.Drawing.Size(100, 16)
    Me.Label11.TabIndex = 104
    Me.Label11.Text = "Phone Number"
    '
    'edit_FaxNumber
    '
    Me.edit_FaxNumber.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.edit_FaxNumber.Location = New System.Drawing.Point(140, 578)
    Me.edit_FaxNumber.Name = "edit_FaxNumber"
    Me.edit_FaxNumber.Size = New System.Drawing.Size(342, 20)
    Me.edit_FaxNumber.TabIndex = 20
    '
    'Label12
    '
    Me.Label12.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label12.Location = New System.Drawing.Point(8, 582)
    Me.Label12.Name = "Label12"
    Me.Label12.Size = New System.Drawing.Size(100, 16)
    Me.Label12.TabIndex = 106
    Me.Label12.Text = "Fax Number"
    '
    'edit_PFPCID
    '
    Me.edit_PFPCID.AutoSize = False
    Me.edit_PFPCID.DisableOnNoData = False
    Me.edit_PFPCID.ErrorInfo.ErrorAction = C1.Win.C1Input.ErrorActionEnum.ResetValue
    Me.edit_PFPCID.ErrorInfo.ErrorMessage = "Validation Error"
    Me.edit_PFPCID.ErrorInfo.ShowErrorMessage = False
    Me.edit_PFPCID.Location = New System.Drawing.Point(140, 88)
    Me.edit_PFPCID.MaxLength = 100
    Me.edit_PFPCID.Name = "edit_PFPCID"
    Me.edit_PFPCID.ShowContextMenu = False
    Me.edit_PFPCID.Size = New System.Drawing.Size(104, 20)
    Me.edit_PFPCID.TabIndex = 3
    Me.edit_PFPCID.Tag = Nothing
    Me.edit_PFPCID.TrimStart = True
    Me.edit_PFPCID.WordWrap = False
    '
    'edit_Counterparty
    '
    Me.edit_Counterparty.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.edit_Counterparty.AutoSize = False
    Me.edit_Counterparty.DisableOnNoData = False
    Me.edit_Counterparty.ErrorInfo.ErrorAction = C1.Win.C1Input.ErrorActionEnum.ResetValue
    Me.edit_Counterparty.ErrorInfo.ErrorMessage = "Validation Error"
    Me.edit_Counterparty.ErrorInfo.ShowErrorMessage = False
    Me.edit_Counterparty.Location = New System.Drawing.Point(140, 64)
    Me.edit_Counterparty.MaxLength = 100
    Me.edit_Counterparty.Name = "edit_Counterparty"
    Me.edit_Counterparty.ShowContextMenu = False
    Me.edit_Counterparty.Size = New System.Drawing.Size(342, 20)
    Me.edit_Counterparty.TabIndex = 2
    Me.edit_Counterparty.Tag = Nothing
    Me.edit_Counterparty.TrimStart = True
    Me.edit_Counterparty.WordWrap = False
    '
    'RootMenu
    '
    Me.RootMenu.Location = New System.Drawing.Point(0, 0)
    Me.RootMenu.Name = "RootMenu"
    Me.RootMenu.Size = New System.Drawing.Size(494, 24)
    Me.RootMenu.TabIndex = 27
    Me.RootMenu.Text = " "
    '
    'GroupBox_Fees
    '
    Me.GroupBox_Fees.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.GroupBox_Fees.Controls.Add(Me.Label4)
    Me.GroupBox_Fees.Controls.Add(Me.Panel3)
    Me.GroupBox_Fees.Controls.Add(Me.Panel2)
    Me.GroupBox_Fees.Controls.Add(Me.Label3)
    Me.GroupBox_Fees.Controls.Add(Me.Label2)
    Me.GroupBox_Fees.Controls.Add(Me.Label24)
    Me.GroupBox_Fees.Controls.Add(Me.Label23)
    Me.GroupBox_Fees.Controls.Add(Me.edit_Mgmt_MarketingToOthers)
    Me.GroupBox_Fees.Controls.Add(Me.edit_Perf_MarketingToOthers)
    Me.GroupBox_Fees.Controls.Add(Me.Label22)
    Me.GroupBox_Fees.Controls.Add(Me.edit_Mgmt_MarketingToFCAM)
    Me.GroupBox_Fees.Controls.Add(Me.edit_Perf_MarketingToFCAM)
    Me.GroupBox_Fees.Controls.Add(Me.Label19)
    Me.GroupBox_Fees.Controls.Add(Me.Label21)
    Me.GroupBox_Fees.Controls.Add(Me.Label20)
    Me.GroupBox_Fees.Controls.Add(Me.edit_Mgmt_RebateOnDebt)
    Me.GroupBox_Fees.Controls.Add(Me.edit_Perf_RebateOnDebt)
    Me.GroupBox_Fees.Controls.Add(Me.edit_Mgmt_RebateOnEquity)
    Me.GroupBox_Fees.Controls.Add(Me.edit_Perf_RebateOnEquity)
    Me.GroupBox_Fees.Controls.Add(Me.Label18)
    Me.GroupBox_Fees.Controls.Add(Me.Label17)
    Me.GroupBox_Fees.Controls.Add(Me.Label16)
    Me.GroupBox_Fees.Controls.Add(Me.Label15)
    Me.GroupBox_Fees.Controls.Add(Me.edit_ManagementFee)
    Me.GroupBox_Fees.Controls.Add(Me.edit_PerformanceFee)
    Me.GroupBox_Fees.Controls.Add(Me.edit_PerformanceFeeThreshold)
    Me.GroupBox_Fees.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.GroupBox_Fees.Location = New System.Drawing.Point(3, 134)
    Me.GroupBox_Fees.Name = "GroupBox_Fees"
    Me.GroupBox_Fees.Size = New System.Drawing.Size(486, 120)
    Me.GroupBox_Fees.TabIndex = 5
    Me.GroupBox_Fees.TabStop = False
    Me.GroupBox_Fees.Text = "Fees"
    '
    'Label4
    '
    Me.Label4.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label4.Location = New System.Drawing.Point(138, 12)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(106, 17)
    Me.Label4.TabIndex = 25
    Me.Label4.Text = "Payable by Client"
    Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopCenter
    '
    'Panel3
    '
    Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Panel3.Location = New System.Drawing.Point(360, 12)
    Me.Panel3.Name = "Panel3"
    Me.Panel3.Size = New System.Drawing.Size(1, 102)
    Me.Panel3.TabIndex = 1
    '
    'Panel2
    '
    Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Panel2.ForeColor = System.Drawing.SystemColors.ControlText
    Me.Panel2.Location = New System.Drawing.Point(246, 12)
    Me.Panel2.Name = "Panel2"
    Me.Panel2.Size = New System.Drawing.Size(1, 102)
    Me.Panel2.TabIndex = 0
    '
    'Label3
    '
    Me.Label3.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label3.Location = New System.Drawing.Point(365, 100)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(106, 17)
    Me.Label3.TabIndex = 24
    Me.Label3.Text = "% of LLP Revenues"
    Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter
    '
    'Label2
    '
    Me.Label2.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label2.Location = New System.Drawing.Point(250, 100)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(106, 17)
    Me.Label2.TabIndex = 23
    Me.Label2.Text = "% of Gross Fees"
    Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter
    '
    'Label24
    '
    Me.Label24.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label24.Location = New System.Drawing.Point(365, 12)
    Me.Label24.Name = "Label24"
    Me.Label24.Size = New System.Drawing.Size(106, 17)
    Me.Label24.TabIndex = 8
    Me.Label24.Text = "Marketing Fees"
    Me.Label24.TextAlign = System.Drawing.ContentAlignment.TopCenter
    '
    'Label23
    '
    Me.Label23.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label23.Location = New System.Drawing.Point(421, 29)
    Me.Label23.Name = "Label23"
    Me.Label23.Size = New System.Drawing.Size(50, 17)
    Me.Label23.TabIndex = 7
    Me.Label23.Text = "to Others"
    Me.Label23.TextAlign = System.Drawing.ContentAlignment.TopCenter
    '
    'edit_Mgmt_MarketingToOthers
    '
    Me.edit_Mgmt_MarketingToOthers.Location = New System.Drawing.Point(421, 49)
    Me.edit_Mgmt_MarketingToOthers.MaxLength = 100
    Me.edit_Mgmt_MarketingToOthers.Name = "edit_Mgmt_MarketingToOthers"
    Me.edit_Mgmt_MarketingToOthers.RenaissanceTag = Nothing
    Me.edit_Mgmt_MarketingToOthers.SelectTextOnFocus = False
    Me.edit_Mgmt_MarketingToOthers.Size = New System.Drawing.Size(50, 20)
    Me.edit_Mgmt_MarketingToOthers.TabIndex = 15
    Me.edit_Mgmt_MarketingToOthers.Text = "0.00%"
    Me.edit_Mgmt_MarketingToOthers.TextFormat = "#,##0.00%"
    Me.edit_Mgmt_MarketingToOthers.Value = 0
    Me.edit_Mgmt_MarketingToOthers.WordWrap = False
    '
    'edit_Perf_MarketingToOthers
    '
    Me.edit_Perf_MarketingToOthers.Location = New System.Drawing.Point(421, 77)
    Me.edit_Perf_MarketingToOthers.MaxLength = 100
    Me.edit_Perf_MarketingToOthers.Name = "edit_Perf_MarketingToOthers"
    Me.edit_Perf_MarketingToOthers.RenaissanceTag = Nothing
    Me.edit_Perf_MarketingToOthers.SelectTextOnFocus = False
    Me.edit_Perf_MarketingToOthers.Size = New System.Drawing.Size(50, 20)
    Me.edit_Perf_MarketingToOthers.TabIndex = 22
    Me.edit_Perf_MarketingToOthers.Text = "0.00%"
    Me.edit_Perf_MarketingToOthers.TextFormat = "#,##0.00%"
    Me.edit_Perf_MarketingToOthers.Value = 0
    Me.edit_Perf_MarketingToOthers.WordWrap = False
    '
    'Label22
    '
    Me.Label22.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label22.Location = New System.Drawing.Point(365, 29)
    Me.Label22.Name = "Label22"
    Me.Label22.Size = New System.Drawing.Size(50, 17)
    Me.Label22.TabIndex = 6
    Me.Label22.Text = "to Group"
    Me.Label22.TextAlign = System.Drawing.ContentAlignment.TopCenter
    '
    'edit_Mgmt_MarketingToFCAM
    '
    Me.edit_Mgmt_MarketingToFCAM.Location = New System.Drawing.Point(365, 49)
    Me.edit_Mgmt_MarketingToFCAM.MaxLength = 100
    Me.edit_Mgmt_MarketingToFCAM.Name = "edit_Mgmt_MarketingToFCAM"
    Me.edit_Mgmt_MarketingToFCAM.RenaissanceTag = Nothing
    Me.edit_Mgmt_MarketingToFCAM.SelectTextOnFocus = False
    Me.edit_Mgmt_MarketingToFCAM.Size = New System.Drawing.Size(50, 20)
    Me.edit_Mgmt_MarketingToFCAM.TabIndex = 14
    Me.edit_Mgmt_MarketingToFCAM.Text = "0.00%"
    Me.edit_Mgmt_MarketingToFCAM.TextFormat = "#,##0.00%"
    Me.edit_Mgmt_MarketingToFCAM.Value = 0
    Me.edit_Mgmt_MarketingToFCAM.WordWrap = False
    '
    'edit_Perf_MarketingToFCAM
    '
    Me.edit_Perf_MarketingToFCAM.Location = New System.Drawing.Point(365, 77)
    Me.edit_Perf_MarketingToFCAM.MaxLength = 100
    Me.edit_Perf_MarketingToFCAM.Name = "edit_Perf_MarketingToFCAM"
    Me.edit_Perf_MarketingToFCAM.RenaissanceTag = Nothing
    Me.edit_Perf_MarketingToFCAM.SelectTextOnFocus = False
    Me.edit_Perf_MarketingToFCAM.Size = New System.Drawing.Size(50, 20)
    Me.edit_Perf_MarketingToFCAM.TabIndex = 21
    Me.edit_Perf_MarketingToFCAM.Text = "0.00%"
    Me.edit_Perf_MarketingToFCAM.TextFormat = "#,##0.00%"
    Me.edit_Perf_MarketingToFCAM.Value = 0
    Me.edit_Perf_MarketingToFCAM.WordWrap = False
    '
    'Label19
    '
    Me.Label19.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label19.Location = New System.Drawing.Point(251, 29)
    Me.Label19.Name = "Label19"
    Me.Label19.Size = New System.Drawing.Size(50, 17)
    Me.Label19.TabIndex = 4
    Me.Label19.Text = "on Equity"
    Me.Label19.TextAlign = System.Drawing.ContentAlignment.TopCenter
    '
    'Label21
    '
    Me.Label21.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label21.Location = New System.Drawing.Point(250, 12)
    Me.Label21.Name = "Label21"
    Me.Label21.Size = New System.Drawing.Size(106, 17)
    Me.Label21.TabIndex = 9
    Me.Label21.Text = "Direct Rebates"
    Me.Label21.TextAlign = System.Drawing.ContentAlignment.TopCenter
    '
    'Label20
    '
    Me.Label20.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label20.Location = New System.Drawing.Point(306, 29)
    Me.Label20.Name = "Label20"
    Me.Label20.Size = New System.Drawing.Size(50, 17)
    Me.Label20.TabIndex = 5
    Me.Label20.Text = "on Debt"
    Me.Label20.TextAlign = System.Drawing.ContentAlignment.TopCenter
    '
    'edit_Mgmt_RebateOnDebt
    '
    Me.edit_Mgmt_RebateOnDebt.Location = New System.Drawing.Point(306, 49)
    Me.edit_Mgmt_RebateOnDebt.MaxLength = 100
    Me.edit_Mgmt_RebateOnDebt.Name = "edit_Mgmt_RebateOnDebt"
    Me.edit_Mgmt_RebateOnDebt.RenaissanceTag = Nothing
    Me.edit_Mgmt_RebateOnDebt.SelectTextOnFocus = False
    Me.edit_Mgmt_RebateOnDebt.Size = New System.Drawing.Size(50, 20)
    Me.edit_Mgmt_RebateOnDebt.TabIndex = 13
    Me.edit_Mgmt_RebateOnDebt.Text = "0.00%"
    Me.edit_Mgmt_RebateOnDebt.TextFormat = "#,##0.00%"
    Me.edit_Mgmt_RebateOnDebt.Value = 0
    Me.edit_Mgmt_RebateOnDebt.WordWrap = False
    '
    'edit_Perf_RebateOnDebt
    '
    Me.edit_Perf_RebateOnDebt.Location = New System.Drawing.Point(306, 77)
    Me.edit_Perf_RebateOnDebt.MaxLength = 100
    Me.edit_Perf_RebateOnDebt.Name = "edit_Perf_RebateOnDebt"
    Me.edit_Perf_RebateOnDebt.RenaissanceTag = Nothing
    Me.edit_Perf_RebateOnDebt.SelectTextOnFocus = False
    Me.edit_Perf_RebateOnDebt.Size = New System.Drawing.Size(50, 20)
    Me.edit_Perf_RebateOnDebt.TabIndex = 20
    Me.edit_Perf_RebateOnDebt.Text = "0.00%"
    Me.edit_Perf_RebateOnDebt.TextFormat = "#,##0.00%"
    Me.edit_Perf_RebateOnDebt.Value = 0
    Me.edit_Perf_RebateOnDebt.WordWrap = False
    '
    'edit_Mgmt_RebateOnEquity
    '
    Me.edit_Mgmt_RebateOnEquity.Location = New System.Drawing.Point(250, 49)
    Me.edit_Mgmt_RebateOnEquity.MaxLength = 100
    Me.edit_Mgmt_RebateOnEquity.Name = "edit_Mgmt_RebateOnEquity"
    Me.edit_Mgmt_RebateOnEquity.RenaissanceTag = Nothing
    Me.edit_Mgmt_RebateOnEquity.SelectTextOnFocus = False
    Me.edit_Mgmt_RebateOnEquity.Size = New System.Drawing.Size(50, 20)
    Me.edit_Mgmt_RebateOnEquity.TabIndex = 12
    Me.edit_Mgmt_RebateOnEquity.Text = "0.00%"
    Me.edit_Mgmt_RebateOnEquity.TextFormat = "#,##0.00%"
    Me.edit_Mgmt_RebateOnEquity.Value = 0
    Me.edit_Mgmt_RebateOnEquity.WordWrap = False
    '
    'edit_Perf_RebateOnEquity
    '
    Me.edit_Perf_RebateOnEquity.Location = New System.Drawing.Point(250, 77)
    Me.edit_Perf_RebateOnEquity.MaxLength = 100
    Me.edit_Perf_RebateOnEquity.Name = "edit_Perf_RebateOnEquity"
    Me.edit_Perf_RebateOnEquity.RenaissanceTag = Nothing
    Me.edit_Perf_RebateOnEquity.SelectTextOnFocus = False
    Me.edit_Perf_RebateOnEquity.Size = New System.Drawing.Size(50, 20)
    Me.edit_Perf_RebateOnEquity.TabIndex = 19
    Me.edit_Perf_RebateOnEquity.Text = "0.00%"
    Me.edit_Perf_RebateOnEquity.TextFormat = "#,##0.00%"
    Me.edit_Perf_RebateOnEquity.Value = 0
    Me.edit_Perf_RebateOnEquity.WordWrap = False
    '
    'Label18
    '
    Me.Label18.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label18.Location = New System.Drawing.Point(194, 29)
    Me.Label18.Name = "Label18"
    Me.Label18.Size = New System.Drawing.Size(50, 17)
    Me.Label18.TabIndex = 3
    Me.Label18.Text = "Hurdle"
    Me.Label18.TextAlign = System.Drawing.ContentAlignment.TopCenter
    '
    'Label17
    '
    Me.Label17.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label17.Location = New System.Drawing.Point(138, 29)
    Me.Label17.Name = "Label17"
    Me.Label17.Size = New System.Drawing.Size(50, 17)
    Me.Label17.TabIndex = 2
    Me.Label17.Text = "Gross"
    Me.Label17.TextAlign = System.Drawing.ContentAlignment.TopCenter
    '
    'Label16
    '
    Me.Label16.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label16.Location = New System.Drawing.Point(5, 80)
    Me.Label16.Name = "Label16"
    Me.Label16.Size = New System.Drawing.Size(126, 17)
    Me.Label16.TabIndex = 16
    Me.Label16.Text = "Performance fees"
    '
    'Label15
    '
    Me.Label15.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label15.Location = New System.Drawing.Point(6, 52)
    Me.Label15.Name = "Label15"
    Me.Label15.Size = New System.Drawing.Size(126, 17)
    Me.Label15.TabIndex = 10
    Me.Label15.Text = "Management fees"
    '
    'Check_IsMarketCpty
    '
    Me.Check_IsMarketCpty.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_IsMarketCpty.Location = New System.Drawing.Point(118, 259)
    Me.Check_IsMarketCpty.Name = "Check_IsMarketCpty"
    Me.Check_IsMarketCpty.Size = New System.Drawing.Size(16, 24)
    Me.Check_IsMarketCpty.TabIndex = 6
    '
    'Label5
    '
    Me.Label5.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label5.Location = New System.Drawing.Point(8, 262)
    Me.Label5.Name = "Label5"
    Me.Label5.Size = New System.Drawing.Size(104, 16)
    Me.Label5.TabIndex = 51
    Me.Label5.Text = "Is a 'Market' Cpty"
    '
    'frmCounterparty
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.CancelButton = Me.btnCancel
    Me.ClientSize = New System.Drawing.Size(494, 738)
    Me.Controls.Add(Me.Check_IsMarketCpty)
    Me.Controls.Add(Me.Label5)
    Me.Controls.Add(Me.GroupBox_Fees)
    Me.Controls.Add(Me.RootMenu)
    Me.Controls.Add(Me.edit_Counterparty)
    Me.Controls.Add(Me.edit_PFPCID)
    Me.Controls.Add(Me.edit_FaxNumber)
    Me.Controls.Add(Me.Label12)
    Me.Controls.Add(Me.edit_PhoneNumber)
    Me.Controls.Add(Me.Label11)
    Me.Controls.Add(Me.edit_EmailAddress)
    Me.Controls.Add(Me.Label10)
    Me.Controls.Add(Me.edit_AccountName)
    Me.Controls.Add(Me.Label9)
    Me.Controls.Add(Me.edit_ContactName)
    Me.Controls.Add(Me.Label8)
    Me.Controls.Add(Me.edit_Postcode)
    Me.Controls.Add(Me.Label7)
    Me.Controls.Add(Me.edit_Address5)
    Me.Controls.Add(Me.edit_Address4)
    Me.Controls.Add(Me.edit_Address3)
    Me.Controls.Add(Me.edit_Address2)
    Me.Controls.Add(Me.Label_CptyIsLeverage)
    Me.Controls.Add(Me.Check_CptyIsFund)
    Me.Controls.Add(Me.btnClose)
    Me.Controls.Add(Me.Combo_FundID)
    Me.Controls.Add(Me.Combo_InvestorGroup)
    Me.Controls.Add(Me.GroupBox1)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.Panel1)
    Me.Controls.Add(Me.Combo_SelectCounterparty)
    Me.Controls.Add(Me.btnSave)
    Me.Controls.Add(Me.editAuditID)
    Me.Controls.Add(Me.edit_Address1)
    Me.Controls.Add(Me.lblCounterparty)
    Me.Controls.Add(Me.lblPFPC)
    Me.Controls.Add(Me.lblAddress)
    Me.Controls.Add(Me.lblIG)
    Me.Controls.Add(Me.label_CptyIsFund)
    Me.Controls.Add(Me.btnDelete)
    Me.Controls.Add(Me.btnCancel)
    Me.Controls.Add(Me.Check_CptyIsALeverageProvider)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frmCounterparty"
    Me.Text = "Add/Edit Counterparty"
    Me.Panel1.ResumeLayout(False)
    CType(Me.edit_PFPCID, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.edit_Counterparty, System.ComponentModel.ISupportInitialize).EndInit()
    Me.GroupBox_Fees.ResumeLayout(False)
    Me.GroupBox_Fees.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "


	' Form 'Parent', the Main Venice form.
	' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
	Private WithEvents _MainForm As VeniceMain

	' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
	Private FormTooltip As New ToolTip()

	' Form Menu


	' Form Constants, specific to the table being updated.

    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
	Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

	' Form Locals, initialised on 'New' defining what standard data items to use
    ''' <summary>
  ''' Variable holding the form's data table name. taken from the "ThisStandardDataset" object.
    ''' </summary>
	Private THIS_TABLENAME As String
    ''' <summary>
  ''' Variable holding the form's data adaptor name. taken from the "ThisStandardDataset" object.
    ''' </summary>
	Private THIS_ADAPTORNAME As String
    ''' <summary>
  ''' Variable holding the form's dataset name. taken from the "ThisStandardDataset" object.
    ''' </summary>
	Private THIS_DATASETNAME As String


	' The standard ChangeID for this form. e.g. tblFund
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
	Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

    ''' <summary>
  ''' Principal control used for selecting items on this form. 
    ''' </summary>
	Private THIS_FORM_SelectingCombo As ComboBox
    ''' <summary>
  ''' Control to select after proessing the "New" button.
    ''' </summary>
	Private THIS_FORM_NewMoveToControl As Control

	' Form Specific Order fields
    ''' <summary>
  ''' Field Name to show in the Select combo.
    ''' </summary>
	Private THIS_FORM_SelectBy As String
    ''' <summary>
  ''' Field Name to order the Select combo by.
    ''' </summary>
	Private THIS_FORM_OrderBy As String

    ''' <summary>
  ''' The field name that holds the unique identifier for each entry. e.g. FundID for the tblFund table.
    ''' </summary>
	Private THIS_FORM_ValueMember As String

	' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
	Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
	Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

	' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
	Private THIS_FORM_FormID As VeniceFormID

	' Data Structures

    ''' <summary>
    ''' My dataset
    ''' </summary>
	Private myDataset As RenaissanceDataClass.DSCounterparty										' Form Specific !!!!
    ''' <summary>
    ''' My table
    ''' </summary>
	Private myTable As RenaissanceDataClass.DSCounterparty.tblCounterpartyDataTable		' Form Specific !!!!
    ''' <summary>
    ''' My connection
    ''' </summary>
	Private myConnection As SqlConnection
    ''' <summary>
    ''' My adaptor
    ''' </summary>
	Private myAdaptor As SqlDataAdapter

    ''' <summary>
    ''' The this standard dataset
    ''' </summary>
	Private ThisStandardDataset As RenaissanceGlobals.StandardDataset


	' Active Element.

    ''' <summary>
    ''' The sorted rows
    ''' </summary>
	Private SortedRows() As DataRow
    ''' <summary>
    ''' The select by sorted rows
    ''' </summary>
	Private SelectBySortedRows() As DataRow
    ''' <summary>
    ''' The this data row
    ''' </summary>
	Private thisDataRow As RenaissanceDataClass.DSCounterparty.tblCounterpartyRow			' Form Specific !!!!
    ''' <summary>
    ''' The this audit ID
    ''' </summary>
	Private thisAuditID As Integer
    ''' <summary>
    ''' The this position
    ''' </summary>
	Private thisPosition As Integer
    ''' <summary>
    ''' The __ is over cancel button
    ''' </summary>
	Private __IsOverCancelButton As Boolean
    ''' <summary>
    ''' The _ in use
    ''' </summary>
	Private _InUse As Boolean

	' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
	Private FormIsValid As Boolean
    ''' <summary>
    ''' The form changed
    ''' </summary>
	Private FormChanged As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
	Private _FormOpenFailed As Boolean
    ''' <summary>
    ''' The in paint
    ''' </summary>
	Private InPaint As Boolean
    ''' <summary>
    ''' The add new record
    ''' </summary>
	Private AddNewRecord As Boolean

	' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
	Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
	Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
	Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
	Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
	Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
	Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return __IsOverCancelButton
		End Get
		Set(ByVal Value As Boolean)
			__IsOverCancelButton = Value
		End Set
	End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
	Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
		Get
			Return InPaint
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
		Get
			Return _InUse
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmCounterparty"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
	Public Sub New(ByVal pMainForm As VeniceMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()



		_MainForm = pMainForm
		AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

		_FormOpenFailed = False

		' ******************************************************
		' Form Specific Settings :
		' ******************************************************

		THIS_FORM_SelectingCombo = Me.Combo_SelectCounterparty
		THIS_FORM_NewMoveToControl = Me.edit_Counterparty

		' Default Select and Order fields.

		THIS_FORM_SelectBy = "CounterpartyName"
		THIS_FORM_OrderBy = "CounterpartyName"

		THIS_FORM_ValueMember = "CounterpartyID"

		' Form Permissioning :-

		THIS_FORM_PermissionArea = Me.Name
		THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

		' 'This' form ID

		THIS_FORM_FormID = VeniceFormID.frmCounterparty

		' This form's dataset type.

		ThisStandardDataset = RenaissanceStandardDatasets.tblCounterparty	 ' This Defines the Form Data !!! 

		' Format Event Handlers for form controls

		AddHandler edit_Counterparty.LostFocus, AddressOf MainForm.Text_NotNull
		AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

		' Form Control Changed events

		AddHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
		AddHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_InvestorGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_FundID.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		AddHandler Combo_InvestorGroup.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_InvestorGroup.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_InvestorGroup.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

		AddHandler Combo_FundID.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_FundID.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_FundID.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

		AddHandler edit_Counterparty.TextChanged, AddressOf Me.FormControlChanged
		AddHandler edit_PFPCID.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_InvestorGroup.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler edit_ManagementFee.TextChanged, AddressOf Me.FormControlChanged
		AddHandler edit_ManagementFee.ValueChanged, AddressOf Me.FormControlChanged
		AddHandler edit_PerformanceFee.TextChanged, AddressOf Me.FormControlChanged
		AddHandler edit_PerformanceFee.ValueChanged, AddressOf Me.FormControlChanged
		AddHandler edit_PerformanceFeeThreshold.TextChanged, AddressOf Me.FormControlChanged
		AddHandler edit_PerformanceFeeThreshold.ValueChanged, AddressOf Me.FormControlChanged
		AddHandler edit_Mgmt_RebateOnEquity.TextChanged, AddressOf Me.FormControlChanged
		AddHandler edit_Mgmt_RebateOnEquity.ValueChanged, AddressOf Me.FormControlChanged
		AddHandler edit_Mgmt_RebateOnDebt.TextChanged, AddressOf Me.FormControlChanged
		AddHandler edit_Mgmt_RebateOnDebt.ValueChanged, AddressOf Me.FormControlChanged
		AddHandler edit_Perf_RebateOnEquity.TextChanged, AddressOf Me.FormControlChanged
		AddHandler edit_Perf_RebateOnEquity.ValueChanged, AddressOf Me.FormControlChanged
		AddHandler edit_Perf_RebateOnDebt.TextChanged, AddressOf Me.FormControlChanged
		AddHandler edit_Perf_RebateOnDebt.ValueChanged, AddressOf Me.FormControlChanged
		AddHandler edit_Mgmt_MarketingToFCAM.TextChanged, AddressOf Me.FormControlChanged
		AddHandler edit_Mgmt_MarketingToFCAM.ValueChanged, AddressOf Me.FormControlChanged
		AddHandler edit_Mgmt_MarketingToOthers.TextChanged, AddressOf Me.FormControlChanged
		AddHandler edit_Mgmt_MarketingToOthers.ValueChanged, AddressOf Me.FormControlChanged
		AddHandler edit_Perf_MarketingToFCAM.TextChanged, AddressOf Me.FormControlChanged
		AddHandler edit_Perf_MarketingToFCAM.ValueChanged, AddressOf Me.FormControlChanged
		AddHandler edit_Perf_MarketingToOthers.TextChanged, AddressOf Me.FormControlChanged
		AddHandler edit_Perf_MarketingToOthers.ValueChanged, AddressOf Me.FormControlChanged
		AddHandler Check_CptyIsFund.CheckStateChanged, AddressOf Me.FormControlChanged
    AddHandler Check_IsMarketCpty.CheckStateChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_FundID.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Check_CptyIsALeverageProvider.CheckStateChanged, AddressOf Me.FormControlChanged
		AddHandler edit_Address1.TextChanged, AddressOf Me.FormControlChanged
		AddHandler edit_Address2.TextChanged, AddressOf Me.FormControlChanged
		AddHandler edit_Address3.TextChanged, AddressOf Me.FormControlChanged
		AddHandler edit_Address4.TextChanged, AddressOf Me.FormControlChanged
		AddHandler edit_Address5.TextChanged, AddressOf Me.FormControlChanged
		AddHandler edit_Postcode.TextChanged, AddressOf Me.FormControlChanged
		AddHandler edit_ContactName.TextChanged, AddressOf Me.FormControlChanged
		AddHandler edit_AccountName.TextChanged, AddressOf Me.FormControlChanged
		AddHandler edit_EmailAddress.TextChanged, AddressOf Me.FormControlChanged
		AddHandler edit_PhoneNumber.TextChanged, AddressOf Me.FormControlChanged
		AddHandler edit_FaxNumber.TextChanged, AddressOf Me.FormControlChanged

		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

		' ******************************************************
		' End Form Specific.
		' ******************************************************

		' Data object names standard to this Form type.

		THIS_TABLENAME = ThisStandardDataset.TableName
		THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
		THIS_DATASETNAME = ThisStandardDataset.DatasetName

		THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

		' Establish / Retrieve data objects for this form.

		myConnection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)
		myAdaptor = MainForm.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, VENICE_CONNECTION, THIS_TABLENAME)
		myDataset = MainForm.Load_Table(ThisStandardDataset, False)
		myTable = myDataset.Tables(0)

		MainForm.BuildStandardFormMenu(Me.RootMenu, myDataset.Tables(0), AddressOf SelectMenuEvent, AddressOf OrderMenuEvent, AddressOf AuditReportMenuEvent)

	End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

	' Form Initialisation code.
	'
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
	Public Sub ResetForm() Implements StandardVeniceForm.ResetForm
		THIS_FORM_SelectBy = "CounterpartyName"
		THIS_FORM_OrderBy = "CounterpartyName"

		Call Form_Load(Me, New System.EventArgs)
	End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
	Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
		_FormOpenFailed = False
		_InUse = True

		Try

			' Initialise Data structures. Connection, Adaptor and Dataset.

			If Not (MainForm Is Nothing) Then
				FormIsValid = True
			Else
				MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
				FormIsValid = False
				_FormOpenFailed = True
				Exit Sub
			End If

			If (myConnection Is Nothing) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

				FormIsValid = False
				_FormOpenFailed = True
				Exit Sub
			End If

			If (myAdaptor Is Nothing) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

				FormIsValid = False
				_FormOpenFailed = True
				Exit Sub
			End If

			If (myDataset Is Nothing) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

				FormIsValid = False
				_FormOpenFailed = True
				Exit Sub
			End If

		Catch ex As Exception
		End Try

		Try

			' Initialse form

			InPaint = True
			IsOverCancelButton = False

			' Check User permissions
			Call CheckPermissions()
			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

				FormIsValid = False
				_FormOpenFailed = True
				Exit Sub
			End If

			' Build Sorted data list from which this form operates
			Call SetSortedRows()

			Call SetInvestorGroupCombo()
			Call SetCptyIsAFundCombo()

			' Display initial record.

			thisPosition = 0
			If THIS_FORM_SelectingCombo.Items.Count > 0 Then
				Me.THIS_FORM_SelectingCombo.SelectedIndex = 0
				thisDataRow = SortedRows(thisPosition)
				Call GetFormData(thisDataRow)
			Else
				Me.THIS_FORM_SelectingCombo.SelectedIndex = (-1)
				Call GetFormData(Nothing)
			End If

		Catch ex As Exception

			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Initialising Form (Form.Load).", ex.StackTrace, True)

		Finally

			InPaint = False

		End Try

	End Sub

    ''' <summary>
    ''' Handles the Closing event of the frm control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
	Private Sub frm_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		Dim HideForm As Boolean

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...

		_InUse = False

		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else
			If (FormChanged = True) Then
				Call SetFormData()
			End If

			HideForm = True
			If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			'Me.WindowState = FormWindowState.Minimized
			'Me.ShowInTaskbar = False

			e.Cancel = True
		Else
			Try
				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler edit_Counterparty.LostFocus, AddressOf MainForm.Text_NotNull
				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

				RemoveHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
				RemoveHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_InvestorGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_FundID.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				RemoveHandler Combo_InvestorGroup.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_InvestorGroup.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_InvestorGroup.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

				RemoveHandler Combo_FundID.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_FundID.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_FundID.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

				RemoveHandler edit_Counterparty.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_PFPCID.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_InvestorGroup.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_ManagementFee.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_PerformanceFee.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_PerformanceFeeThreshold.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_ManagementFee.ValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_PerformanceFee.ValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_PerformanceFeeThreshold.ValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_Mgmt_RebateOnEquity.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_Mgmt_RebateOnEquity.ValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_Mgmt_RebateOnDebt.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_Mgmt_RebateOnDebt.ValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_Perf_RebateOnEquity.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_Perf_RebateOnEquity.ValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_Perf_RebateOnDebt.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_Perf_RebateOnDebt.ValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_Mgmt_MarketingToFCAM.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_Mgmt_MarketingToFCAM.ValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_Mgmt_MarketingToOthers.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_Mgmt_MarketingToOthers.ValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_Perf_MarketingToFCAM.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_Perf_MarketingToFCAM.ValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_Perf_MarketingToOthers.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_Perf_MarketingToOthers.ValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_CptyIsFund.CheckStateChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_IsMarketCpty.CheckStateChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_FundID.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_CptyIsALeverageProvider.CheckStateChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_Address1.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_Address2.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_Address3.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_Address4.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_Address5.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_Postcode.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_ContactName.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_AccountName.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_EmailAddress.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_PhoneNumber.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_FaxNumber.TextChanged, AddressOf Me.FormControlChanged
			Catch ex As Exception
			End Try
		End If

	End Sub


#End Region

	' Routine to handle changes / updates to tables by this and other windows.
	' If this, or any other, form posts a change to a table, then it will invoke an update event 
	' detailing what tables have been altered.
	' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
	' the 'VeniceAutoUpdate' event of the main Venice form.
	' Each form may them react as appropriate to changes in any table that might impact it.
	'
    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		Dim OrgInPaint As Boolean
		Dim KnowledgeDateChanged As Boolean
		Dim SetButtonStatus_Flag As Boolean
		Dim RefreshForm As Boolean = False

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		OrgInPaint = InPaint
		InPaint = True
		KnowledgeDateChanged = False
		SetButtonStatus_Flag = False

		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
			KnowledgeDateChanged = True
			RefreshForm = True
		End If

		' ****************************************************************
		' Check for changes relevant to this form
		' ****************************************************************

		' Changes to the tblFund table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFund) = True) Or KnowledgeDateChanged Then

			' ReBuild the Fund derived combos

			Call SetCptyIsAFundCombo()

		End If

		' Changes to the tblInvestorGroup table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInvestorGroup) = True) Or KnowledgeDateChanged Then

			' ReBuild the InvestorGroup derived combos

			Call SetInvestorGroupCombo()

		End If

		' Changes to the KnowledgeDate :-
		If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
			SetButtonStatus_Flag = True
		End If

		' Changes to the tblUserPermissions table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

			' Check ongoing permissions.

			Call CheckPermissions()
			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

				FormIsValid = False
				Me.Close()
				Exit Sub
			End If

			SetButtonStatus_Flag = True

		End If


		' ****************************************************************
		' Changes to the Main FORM table :-
		' ****************************************************************

		If (e.TableChanged(THIS_FORM_ChangeID) = True) Or KnowledgeDateChanged Then			 ' (Form Specific)

			' Re-Set Controls etc.
			Call SetSortedRows()

			RefreshForm = True

			' Move again to the correct item
			thisPosition = Get_Position(thisAuditID)

			' Validate current position.
			If thisPosition >= 0 And thisPosition < Me.SortedRows.GetLength(0) Then
				thisDataRow = Me.SortedRows(thisPosition)
			Else
				If (Me.SortedRows.GetLength(0) > 0) And (AddNewRecord = False) Then
					thisPosition = 0
					thisDataRow = Me.SortedRows(thisPosition)
					FormChanged = False
				Else
					thisDataRow = Nothing
				End If
			End If

			' Set Selecting Combo Index.
			If (thisPosition < 0) Then
				MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
			Else
				Me.THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
			End If

		End If

		' ****************************************************************
		' Repaint if not currently in Edit Mode
		'
		' ****************************************************************


		InPaint = OrgInPaint

		' Retrieve form data if appropriate, i.e. this data item is not already being edited.
		If (RefreshForm = True) And (FormChanged = False) And (AddNewRecord = False) Then
			GetFormData(thisDataRow) ' Includes a call to 'SetButtonStatus()'
		Else
			If SetButtonStatus_Flag Then
				Call SetButtonStatus()
			End If
		End If

	End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

	' Build Sorted list from the Source dataset and update the Select Combo
    ''' <summary>
    ''' Sets the sorted rows.
    ''' </summary>
	Private Sub SetSortedRows()

		Dim OrgInPaint As Boolean

		Dim thisrow As DataRow
		Dim thisDrowDownWidth As Integer
		Dim SizingBitmap As Bitmap
		Dim SizingGraphics As Graphics

		If (THIS_FORM_SelectingCombo Is Nothing) Then Exit Sub

		' Set paint local so that changes to the selection combo do not trigger form updates.

		OrgInPaint = InPaint
		InPaint = True

		' Get selected Row sets, or exit if no data is present.
		If myDataset Is Nothing Then
			ReDim SortedRows(0)
			ReDim SelectBySortedRows(0)
		Else
			SortedRows = myTable.Select("RN >= 0", THIS_FORM_OrderBy)
			SelectBySortedRows = myTable.Select("RN >= 0", THIS_FORM_SelectBy)
		End If

		' Set Combo data source
		THIS_FORM_SelectingCombo.DataSource = SortedRows
		THIS_FORM_SelectingCombo.DisplayMember = THIS_FORM_SelectBy
		THIS_FORM_SelectingCombo.ValueMember = THIS_FORM_ValueMember

		' Ensure that the Selection Combo's DrowDownWidth is wide enough to take the required strings

		thisDrowDownWidth = THIS_FORM_SelectingCombo.Width
		For Each thisrow In SortedRows

			' Compute the string dimensions in the given font
			SizingBitmap = New Bitmap(1, 1, System.Drawing.Imaging.PixelFormat.Format32bppArgb)
			SizingGraphics = Graphics.FromImage(SizingBitmap)
			Dim stringSize As SizeF = SizingGraphics.MeasureString(thisrow(THIS_FORM_SelectBy).ToString, THIS_FORM_SelectingCombo.Font)
			If (stringSize.Width) > thisDrowDownWidth Then
				thisDrowDownWidth = CInt(stringSize.Width)
			End If
		Next

		THIS_FORM_SelectingCombo.DropDownWidth = thisDrowDownWidth

		InPaint = OrgInPaint
	End Sub


	' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
	Private Sub CheckPermissions()

		Dim Permissions As Integer

		Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

		HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
		HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
		HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
		HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

	End Sub

	' Flag changes to form controls, Event Associations made in 'New' routine.
    ''' <summary>
  ''' Basic event handler, called when a form value is changed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

		If InPaint = False Then
			If (Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord) Then
				FormChanged = True
				Me.btnSave.Enabled = True
				Me.btnCancel.Enabled = True
			End If
		End If

	End Sub

    ''' <summary>
    ''' Selects the menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Public Sub SelectMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Select Menu
		' Event association is made during the dynamic menu creation

		THIS_FORM_SelectBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

		Call SetSortedRows()

	End Sub

    ''' <summary>
    ''' Orders the menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Public Sub OrderMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Order Menu
		' Event association is made during the dynamic menu creation

		THIS_FORM_OrderBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

		Call SetSortedRows()

	End Sub


    ''' <summary>
    ''' Audits the report menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Public Sub AuditReportMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Audit Report Menu
		' Event association is made during the dynamic menu creation

		Try
			Select Case CInt(CType(sender, System.Windows.Forms.ToolStripMenuItem).Tag)
				Case 0 ' This Record
					If (Me.AddNewRecord = False) AndAlso (thisAuditID >= 0) Then
						Run_AuditReport_ThisRecord(MainForm, ThisStandardDataset, thisAuditID, MainForm.VeniceProgressBar)
					End If

				Case 1 ' All Records
					Run_AuditReport_ALLRecords(MainForm, ThisStandardDataset, MainForm.VeniceProgressBar)

			End Select
		Catch ex As Exception
			Call MainForm.LogError(Me.Name & ", AuditReportMenuEvent()", 0, ex.Message, "Error calling Audit report.", ex.StackTrace, True)
		End Try

	End Sub




#End Region

#Region " Set Form Combos (Form Specific Code) "

    ''' <summary>
    ''' Sets the investor group combo.
    ''' </summary>
	Private Sub SetInvestorGroupCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_InvestorGroup, _
		RenaissanceStandardDatasets.tblInvestorGroup, _
		"IGName", _
		"IGID", _
		"", False, True, True, 0)

	End Sub

    ''' <summary>
    ''' Sets the cpty is A fund combo.
    ''' </summary>
	Private Sub SetCptyIsAFundCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_FundID, _
		RenaissanceStandardDatasets.tblFund, _
		"FundCode", _
		"FundID", _
		"")		' 

	End Sub


#End Region

#Region " Get & Set Form Data / SetButton / ValidateForm / btnCancel Events (Form Specific Code) "

    ''' <summary>
    ''' Gets the form data.
    ''' </summary>
    ''' <param name="ThisRow">The this row.</param>
	Private Sub GetFormData(ByRef ThisRow As RenaissanceDataClass.DSCounterparty.tblCounterpartyRow)
		' Routine to populate form Controls given a data row.
		' Form is cleared for invalid Datarows or Invalid Form.

		Dim OrgInpaint As Boolean

		' Set 'Paint' flag to prevent event actions caused by changing the value of form fields.
		OrgInpaint = InPaint
		InPaint = True


		If (ThisRow Is Nothing) Or (FormIsValid = False) Or (Me.InUse = False) Then
			' Bad / New Datarow - Clear Form.

			thisAuditID = (-1)

			Me.editAuditID.Text = ""

			Me.edit_Counterparty.Value = ""
			Me.edit_PFPCID.Value = ""

			If (Combo_InvestorGroup.Items.Count > 0) Then
				Combo_InvestorGroup.SelectedIndex = 0
			End If

			Me.edit_ManagementFee.Value = 0.01
			Me.edit_PerformanceFee.Value = 0.1
			Me.edit_PerformanceFeeThreshold.Value = 0
			Me.edit_Mgmt_RebateOnDebt.Value = 0
			Me.edit_Mgmt_RebateOnEquity.Value = 0
			Me.edit_Perf_RebateOnDebt.Value = 0
			Me.edit_Perf_RebateOnEquity.Value = 0
			Me.edit_Mgmt_MarketingToFCAM.Value = 0
			Me.edit_Mgmt_MarketingToOthers.Value = 0
			Me.edit_Perf_MarketingToFCAM.Value = 0
			Me.edit_Perf_MarketingToOthers.Value = 0

			Me.Check_CptyIsFund.CheckState = CheckState.Unchecked
      Me.Check_IsMarketCpty.CheckState = CheckState.Unchecked

			MainForm.ClearComboSelection(Combo_FundID)

			Me.Check_CptyIsALeverageProvider.CheckState = CheckState.Unchecked

			Me.edit_Address1.Text = ""
			Me.edit_Address2.Text = ""
			Me.edit_Address3.Text = ""
			Me.edit_Address4.Text = ""
			Me.edit_Address5.Text = ""
			Me.edit_Postcode.Text = ""
			Me.edit_ContactName.Text = ""
			Me.edit_AccountName.Text = ""
			Me.edit_EmailAddress.Text = ""
			Me.edit_PhoneNumber.Text = ""
			Me.edit_FaxNumber.Text = ""

			If AddNewRecord = True Then
				Me.btnCancel.Enabled = True
				Me.THIS_FORM_SelectingCombo.Enabled = False
			Else
				Me.btnCancel.Enabled = False
				Me.THIS_FORM_SelectingCombo.Enabled = True
			End If
		Else
			' Populate Form with given data.
			Try

				thisAuditID = thisDataRow.AuditID

				Me.editAuditID.Text = thisDataRow.AuditID.ToString

				Me.edit_Counterparty.Value = thisDataRow.CounterpartyName
				Me.edit_PFPCID.Value = thisDataRow.CounterpartyPFPCID

				' Combo_FundBaseCurrency
				If (thisDataRow.CounterpartyInvestorGroup > 0) Then
					Me.Combo_InvestorGroup.SelectedValue = thisDataRow.CounterpartyInvestorGroup
					Me.Combo_InvestorGroup.Text = Me.Combo_InvestorGroup.Text
				Else
					MainForm.ClearComboSelection(Combo_InvestorGroup)
					Me.Combo_InvestorGroup.Text = ""
				End If

				Me.edit_ManagementFee.Value = thisDataRow.CounterpartyManagementFee
				Me.edit_PerformanceFee.Value = thisDataRow.CounterpartyPerformanceFee
				Me.edit_PerformanceFeeThreshold.Value = thisDataRow.CounterpartyPerformanceFeeThreshold

				Me.edit_Mgmt_RebateOnDebt.Value = thisDataRow.Fee_ManagementDebtRebate
				Me.edit_Mgmt_RebateOnEquity.Value = thisDataRow.Fee_ManagementEquityRebate
				Me.edit_Perf_RebateOnDebt.Value = thisDataRow.Fee_PerformanceDebtRebate
				Me.edit_Perf_RebateOnEquity.Value = thisDataRow.Fee_PerformanceEquityRebate
				Me.edit_Mgmt_MarketingToFCAM.Value = thisDataRow.Fee_ManagementMarketingToFCAM
				Me.edit_Mgmt_MarketingToOthers.Value = thisDataRow.Fee_ManagementMarketingToOthers
				Me.edit_Perf_MarketingToFCAM.Value = thisDataRow.Fee_PerformanceMarketingToFCAM
				Me.edit_Perf_MarketingToOthers.Value = thisDataRow.Fee_PerformanceMarketingToOthers

        If (thisDataRow.CounterpartyIsMarketCounterparty) Then
          Check_IsMarketCpty.CheckState = CheckState.Checked
        Else
          Check_IsMarketCpty.CheckState = CheckState.Unchecked
        End If

        If (thisDataRow.CounterpartyFundID > 0) Then
          Me.Check_CptyIsFund.CheckState = CheckState.Checked
          Me.Combo_FundID.SelectedValue = thisDataRow.CounterpartyFundID
          Me.Combo_FundID.Text = Me.Combo_FundID.Text
        Else
          Me.Check_CptyIsFund.CheckState = CheckState.Unchecked
          Me.Combo_FundID.Text = ""
          MainForm.ClearComboSelection(Combo_FundID)
        End If

        If (thisDataRow.CounterpartyLeverageProvider = 0) Then
          Me.Check_CptyIsALeverageProvider.CheckState = CheckState.Unchecked
        Else
          Me.Check_CptyIsALeverageProvider.CheckState = CheckState.Checked
        End If

        Me.edit_Address1.Text = thisDataRow.CounterpartyAddress1
        Me.edit_Address2.Text = thisDataRow.CounterpartyAddress2
        Me.edit_Address3.Text = thisDataRow.CounterpartyAddress3
        Me.edit_Address4.Text = thisDataRow.CounterpartyAddress4
        Me.edit_Address5.Text = thisDataRow.CounterpartyAddress5
        Me.edit_Postcode.Text = thisDataRow.CounterpartyPostCode
        Me.edit_ContactName.Text = thisDataRow.CounterpartyContactName
        Me.edit_AccountName.Text = thisDataRow.CounterpartyAccountName
        Me.edit_EmailAddress.Text = thisDataRow.CounterpartyEMail
        Me.edit_PhoneNumber.Text = thisDataRow.CounterpartyPhoneNumber
        Me.edit_FaxNumber.Text = thisDataRow.CounterpartyFaxNumber

        AddNewRecord = False
        ' MainForm.SetComboSelectionLengths(Me)

        Me.btnCancel.Enabled = False
        Me.THIS_FORM_SelectingCombo.Enabled = True

      Catch ex As Exception

        Call MainForm.LogError(Me.Name & ", GetFormData", 0, ex.Message, "Error Showing Data", ex.StackTrace, True)
        Call GetFormData(Nothing)

      End Try
		End If

		' Allow Field events to trigger before 'InPaint' Is re-set. 
		' (Should) Prevent Validation errors during Form Draw.
		Application.DoEvents()

		' Restore 'Paint' flag.
		InPaint = OrgInpaint
		FormChanged = False
		Me.btnSave.Enabled = False

		' As it says on the can....
		Call SetButtonStatus()

	End Sub

    ''' <summary>
    ''' Sets the form data.
    ''' </summary>
    ''' <param name="pConfirm">if set to <c>true</c> [p confirm].</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	Private Function SetFormData(Optional ByVal pConfirm As Boolean = True) As Boolean
		' *************************************************************
		'
		' *************************************************************
		Dim ErrMessage As String = ""
		Dim ErrFlag As Boolean
		Dim ErrStack As String = ""
		Dim ProtectedItem As Boolean = False

		' *************************************************************
		' Appropriate Save permission :-
		' *************************************************************

		If (Me.HasUpdatePermission = False) And ((Me.HasInsertPermission = False) Or (Me.AddNewRecord = False)) Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "You do not have permission to save this record.", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' *************************************************************
		' If Save button is disabled then should not be able to save, exit silently.
		' *************************************************************
		If Me.btnSave.Enabled = False Then
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' *************************************************************
		' KnowledgeDate OK :-
		' *************************************************************

		If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Changes to Venice are not allowed when the Knowledgedate is not set to `NOW`", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' Confirm Save, if required.

		If (pConfirm = True) Then
			If MessageBox.Show("Save Changes ?", "Save Changes ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
				Return True
				Exit Function
			End If
		End If

		' *************************************************************
		' Procedure to Save the current form information.
		' *************************************************************

		Dim StatusString As String = ""
		Dim LogString As String
		Dim UpdateRows(0) As DataRow
		Dim Position As Integer

		If (FormChanged = False) Or (FormIsValid = False) Then
			Return False
			Exit Function
		End If

		' Validation
		If ValidateForm(StatusString) = False Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Validation Error." & vbCrLf & StatusString, "", True)
			Return False
			Exit Function
		End If

		' Check current position in the table.
		Position = Get_Position(thisAuditID)

		' Allow for new or missing ID.
		If Position < 0 Then
			If (AddNewRecord = True) OrElse (SortedRows.Length <= 0) Then
				thisDataRow = myTable.NewRow
				LogString = "Add: "
				AddNewRecord = True
			Else
				Return False
				Exit Function
			End If
		Else
			' Row found OK.
			LogString = "Edit : AuditID = " & thisAuditID.ToString

			thisDataRow = Me.SortedRows(Position)

			' Check for Protected Counterparty
			If Not thisDataRow.IsCounterpartyIDNull Then
				If (thisDataRow.CounterpartyID > 0) AndAlso (System.Enum.IsDefined(GetType(RenaissanceGlobals.Counterparty), thisDataRow.CounterpartyID) = True) Then

					ProtectedItem = True

				End If
			End If

		End If

		' Set 'Paint' flag.
		InPaint = True

		' *************************************************************
		' Lock the Data Table, to prevent update conflicts.
		' *************************************************************
		SyncLock myTable

			' Initiate Edit,
			thisDataRow.BeginEdit()

			' Set Data Values
			If (ProtectedItem = True) AndAlso (AddNewRecord = False) AndAlso (thisDataRow.CounterpartyName <> Me.edit_Counterparty.Text) Then
				MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "This Counterparty is protected, Counterparty Name may not be changed.", "", True)
			Else
				thisDataRow.CounterpartyName = Me.edit_Counterparty.Text
			End If

			thisDataRow.CounterpartyPFPCID = Me.edit_PFPCID.Text

			' Combo_InvestorGroup
			If Me.Combo_InvestorGroup.SelectedIndex >= 0 Then
				thisDataRow.CounterpartyInvestorGroup = CInt(Me.Combo_InvestorGroup.SelectedValue)
			Else
				thisDataRow.CounterpartyInvestorGroup = 0
			End If

			thisDataRow.CounterpartyManagementFee = Me.edit_ManagementFee.Value
			thisDataRow.CounterpartyPerformanceFee = Me.edit_PerformanceFee.Value
			thisDataRow.CounterpartyPerformanceFeeThreshold = Me.edit_PerformanceFeeThreshold.Value

			thisDataRow.Fee_ManagementEquityRebate = Me.edit_Mgmt_RebateOnEquity.Value
			thisDataRow.Fee_PerformanceEquityRebate = Me.edit_Perf_RebateOnEquity.Value
			thisDataRow.Fee_ManagementMarketingToFCAM = Me.edit_Mgmt_MarketingToFCAM.Value
			thisDataRow.Fee_ManagementMarketingToOthers = Me.edit_Mgmt_MarketingToOthers.Value
			thisDataRow.Fee_PerformanceMarketingToFCAM = Me.edit_Perf_MarketingToFCAM.Value
			thisDataRow.Fee_PerformanceMarketingToOthers = Me.edit_Perf_MarketingToOthers.Value

			If (Check_CptyIsALeverageProvider.Enabled) AndAlso (Check_CptyIsALeverageProvider.Checked) Then
				thisDataRow.Fee_PerformanceDebtRebate = Me.edit_Perf_RebateOnDebt.Value
				thisDataRow.Fee_ManagementDebtRebate = Me.edit_Mgmt_RebateOnDebt.Value
			Else
				thisDataRow.Fee_PerformanceDebtRebate = 0
				thisDataRow.Fee_ManagementDebtRebate = 0
			End If

      '
      If (Check_IsMarketCpty.CheckState = CheckState.Checked) Then
        thisDataRow.CounterpartyIsMarketCounterparty = True
      Else
        thisDataRow.CounterpartyIsMarketCounterparty = False
      End If

			' Combo_FundID
			If (Me.Check_CptyIsFund.CheckState = CheckState.Checked) AndAlso (Me.Combo_FundID.SelectedIndex >= 0) Then
				thisDataRow.CounterpartyFundID = CInt(Me.Combo_FundID.SelectedValue)
			Else
				thisDataRow.CounterpartyFundID = 0
			End If

			If (Me.Check_CptyIsALeverageProvider.CheckState = CheckState.Checked) Then
				thisDataRow.CounterpartyLeverageProvider = (-1)
			Else
				thisDataRow.CounterpartyLeverageProvider = 0
			End If

			thisDataRow.CounterpartyAddress1 = Me.edit_Address1.Text
			thisDataRow.CounterpartyAddress2 = Me.edit_Address2.Text
			thisDataRow.CounterpartyAddress3 = Me.edit_Address3.Text
			thisDataRow.CounterpartyAddress4 = Me.edit_Address4.Text
			thisDataRow.CounterpartyAddress5 = Me.edit_Address5.Text

			thisDataRow.CounterpartyPostCode = Me.edit_Postcode.Text
			thisDataRow.CounterpartyContactName = Me.edit_ContactName.Text
			thisDataRow.CounterpartyAccountName = Me.edit_AccountName.Text
			thisDataRow.CounterpartyEMail = Me.edit_EmailAddress.Text
			thisDataRow.CounterpartyPhoneNumber = Me.edit_PhoneNumber.Text
			thisDataRow.CounterpartyFaxNumber = Me.edit_FaxNumber.Text


			thisDataRow.EndEdit()
			InPaint = False

			' Add and Update DataRow. 

			ErrFlag = False

			If AddNewRecord = True Then
				Try
					myTable.Rows.Add(thisDataRow)
				Catch ex As Exception
					ErrFlag = True
					ErrMessage = ex.Message
					ErrStack = ex.StackTrace
				End Try
			End If

			UpdateRows(0) = thisDataRow

			' Post Additions / Updates to the underlying table :-
			Dim temp As Integer
			Try
				If (ErrFlag = False) Then
					myAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
					myAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

					temp = MainForm.AdaptorUpdate(Me.Name, myAdaptor, UpdateRows)

				End If

			Catch ex As Exception

				ErrMessage = ex.Message
				ErrFlag = True
				ErrStack = ex.StackTrace
				thisDataRow.RejectChanges()

			End Try

			thisAuditID = thisDataRow.AuditID


		End SyncLock

		' *************************************************************
		' Error report. Not done in the Try-Catch as that would serve to persist the SyncLock.
		' *************************************************************

		If (ErrFlag = True) Then
			Call MainForm.LogError(Me.Name & ", SetFormData", 0, ErrMessage, "Error Saving Data", ErrStack, True)

		End If

		' Finish off

		AddNewRecord = False
		FormChanged = False

		Me.THIS_FORM_SelectingCombo.Enabled = True

		' Propogate changes

    If (thisDataRow IsNot Nothing) AndAlso (thisDataRow.IsAuditIDNull = False) AndAlso (thisDataRow.AuditID > 0) Then
      Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID, thisDataRow.AuditID.ToString), True)
    Else
      Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID), True)
    End If

	End Function

    ''' <summary>
    ''' Sets the button status.
    ''' </summary>
	Private Sub SetButtonStatus()
		' Sets the status of the form controlls appropriate to the current users 
		'permissions and the 'Changed' or 'New' status of the form.

		' No Read permission :-

		If Me.HasReadPermission = False Then
			MainForm.LogError(Me.Name & ", SetButtonStatus()", 0, "", "You do not have Read permission for this Form", "", True)
			Me.Close()
			Exit Sub
		End If

		' Has Insert Permission.
		If Me.HasInsertPermission Then
			Me.btnAdd.Enabled = True
		Else
			Me.btnAdd.Enabled = False
		End If

		' Has Delete permission. 
		If (Me.HasDeletePermission) And (Me.AddNewRecord = False) And (thisPosition >= 0) Then
			Me.btnDelete.Enabled = True
		Else
			Me.btnDelete.Enabled = False
		End If

		If (Me.MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And _
		 ((Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord)) Then

			Me.edit_Counterparty.Enabled = True
			Me.edit_PFPCID.Enabled = True
			Me.Combo_InvestorGroup.Enabled = True
			Me.edit_ManagementFee.Enabled = True
			Me.edit_PerformanceFee.Enabled = True
			Me.edit_PerformanceFeeThreshold.Enabled = True
			Me.edit_Mgmt_RebateOnDebt.Enabled = Me.Check_CptyIsALeverageProvider.Checked
			Me.edit_Mgmt_RebateOnEquity.Enabled = True
			Me.edit_Perf_RebateOnDebt.Enabled = Me.Check_CptyIsALeverageProvider.Checked
			Me.edit_Perf_RebateOnEquity.Enabled = True
			Me.edit_Mgmt_MarketingToFCAM.Enabled = True
			Me.edit_Mgmt_MarketingToOthers.Enabled = True
			Me.edit_Perf_MarketingToFCAM.Enabled = True
			Me.edit_Perf_MarketingToOthers.Enabled = True
			Me.Check_CptyIsFund.Enabled = True
      Me.Check_IsMarketCpty.Enabled = True

			If Me.Check_CptyIsFund.CheckState = CheckState.Checked Then
				Me.Combo_FundID.Enabled = True
			Else
				Me.Combo_FundID.Enabled = False
			End If

			Me.Check_CptyIsALeverageProvider.Enabled = True
			Me.edit_Address1.Enabled = True
			Me.edit_Address2.Enabled = True
			Me.edit_Address3.Enabled = True
			Me.edit_Address4.Enabled = True
			Me.edit_Address5.Enabled = True
			Me.edit_Postcode.Enabled = True
			Me.edit_ContactName.Enabled = True
			Me.edit_AccountName.Enabled = True
			Me.edit_EmailAddress.Enabled = True
			Me.edit_PhoneNumber.Enabled = True
			Me.edit_FaxNumber.Enabled = True

		Else

			Me.edit_Counterparty.Enabled = False
			Me.edit_PFPCID.Enabled = False
			Me.Combo_InvestorGroup.Enabled = False
			Me.edit_ManagementFee.Enabled = False
			Me.edit_PerformanceFee.Enabled = False
			Me.edit_PerformanceFeeThreshold.Enabled = False
			Me.edit_Mgmt_RebateOnDebt.Enabled = False
			Me.edit_Mgmt_RebateOnEquity.Enabled = False
			Me.edit_Perf_RebateOnDebt.Enabled = False
			Me.edit_Perf_RebateOnEquity.Enabled = False
			Me.edit_Mgmt_MarketingToFCAM.Enabled = False
			Me.edit_Mgmt_MarketingToOthers.Enabled = False
			Me.edit_Perf_MarketingToFCAM.Enabled = False
			Me.edit_Perf_MarketingToOthers.Enabled = False
			Me.Check_CptyIsFund.Enabled = False
      Me.Check_IsMarketCpty.Enabled = False
      Me.Combo_FundID.Enabled = False
			Me.Check_CptyIsALeverageProvider.Enabled = False
			Me.edit_Address1.Enabled = False
			Me.edit_Address2.Enabled = False
			Me.edit_Address3.Enabled = False
			Me.edit_Address4.Enabled = False
			Me.edit_Address5.Enabled = False
			Me.edit_Postcode.Enabled = False
			Me.edit_ContactName.Enabled = False
			Me.edit_AccountName.Enabled = False
			Me.edit_EmailAddress.Enabled = False
			Me.edit_PhoneNumber.Enabled = False
			Me.edit_FaxNumber.Enabled = False

		End If

	End Sub

    ''' <summary>
    ''' Validates the form.
    ''' </summary>
    ''' <param name="pReturnString">The p return string.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	Private Function ValidateForm(ByRef pReturnString As String) As Boolean
		' Form Validation code.
		' 
		' This code should be the final arbiter of what is allowed. no assumptions regarding 
		' prior validation should be made.
		' 
		' This Code is called by the SetFormData routine before position changes.
		'
		Dim RVal As Boolean

		RVal = True
		pReturnString = ""

		' Validate 

		If Me.edit_Counterparty.Text.Length <= 0 Then
			pReturnString = "Counterparty Name must not be left blank."
			RVal = False
		End If

		Return RVal

	End Function

    ''' <summary>
    ''' Handles the MouseEnter event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnCancel_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseEnter
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = True
	End Sub

    ''' <summary>
    ''' Handles the MouseLeave event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnCancel_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseLeave
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = False
	End Sub


#End Region

#Region " Form Control Event Functions "

    ''' <summary>
    ''' Handles the CheckedChanged event of the Check_CptyIsFund control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Check_CptyIsFund_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_CptyIsFund.CheckedChanged
		If Me.Check_CptyIsFund.CheckState = CheckState.Checked Then
			Me.Combo_FundID.Enabled = True
		Else
			Me.Combo_FundID.Enabled = False
		End If
	End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the Check_CptyIsALeverageProvider control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Check_CptyIsALeverageProvider_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_CptyIsALeverageProvider.CheckedChanged
		If (Me.Disposing = False) AndAlso (Me.IsDisposed = False) Then
			If (InPaint = False) AndAlso (Check_CptyIsALeverageProvider.Enabled) Then
				Me.edit_Mgmt_RebateOnDebt.Enabled = Check_CptyIsALeverageProvider.Checked
				Me.edit_Perf_RebateOnDebt.Enabled = Check_CptyIsALeverageProvider.Checked
			End If
		End If
	End Sub

#End Region

#Region " Navigation Code / GetPosition() (Generic Code) "

    ''' <summary>
    ''' Handles the SelectComboChanged event of the Combo control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_SelectComboChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

		' Selection Combo. SelectedItem changed.
		'

		' Don't react to changes made in paint routines etc.
		If InPaint = True Then Exit Sub

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		' Find the correct data row, then show it...
		thisPosition = THIS_FORM_SelectingCombo.SelectedIndex

		If (thisPosition >= 0) Then
			thisDataRow = Me.SortedRows(thisPosition)
		Else
			thisDataRow = Nothing
		End If

		Call GetFormData(thisDataRow)

	End Sub


    ''' <summary>
    ''' Handles the Click event of the btnNavPrev control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnNavPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavPrev.Click
		' 'Previous' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		If thisPosition > 0 Then thisPosition -= 1
		If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
			thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
		End If

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

		Exit Sub

		If myTable.Rows.Count > thisPosition Then
			thisDataRow = myTable.Rows(thisPosition)
			Call GetFormData(thisDataRow)
		Else
			Call GetFormData(Nothing)
		End If

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnNavNext control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnNavNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavNext.Click
		' 'Next' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		If thisPosition < (myTable.Rows.Count - 1) Then thisPosition += 1
		If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
			thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
		End If

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

		Exit Sub

		If myTable.Rows.Count > thisPosition Then
			thisDataRow = myTable.Rows(thisPosition)
			Call GetFormData(thisDataRow)
		Else
			Call GetFormData(Nothing)
		End If

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnNavFirst control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnNavFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavFirst.Click
		' 'First' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		thisPosition = 0
		If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
			thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
		End If

    If (THIS_FORM_SelectingCombo.SelectedIndex = thisPosition) Then
      Combo_SelectComboChanged(THIS_FORM_SelectingCombo, New EventArgs)
    Else
      THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
    End If

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnLast control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLast.Click
		' 'Last' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		thisPosition = THIS_FORM_SelectingCombo.Items.Count - 1

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

	End Sub

    ''' <summary>
    ''' Get_s the position.
    ''' </summary>
    ''' <param name="pAuditID">The p audit ID.</param>
    ''' <returns>System.Int32.</returns>
	Private Function Get_Position(ByVal pAuditID As Integer) As Integer
		' Returns the position in the current 'SortedRows' array of the entry corresponding to the given 
		' AudidID.

		Dim MinIndex As Integer
		Dim MaxIndex As Integer
		Dim CurrentMin As Integer
		Dim CurrentMax As Integer
		Dim searchDataRow As DataRow

		Try
			' SortedRows exists ?

			If SortedRows Is Nothing Then
				Return (-1)
				Exit Function
			End If

			' Return (-1) if there are no rows in the 'SortedRows'

			If (SortedRows.GetLength(0) <= 0) Then
				Return (-1)
				Exit Function
			End If


			' Use a modified Search moving outwards from the last returned value to locate the required value.
			' Reflecting the fact that for the most part One looks for closely located records.

			MinIndex = 0
			MaxIndex = SortedRows.GetLength(0) - 1


			' Check First and Last records (Incase 'First' or 'Last' was pressed).

			searchDataRow = SortedRows(MinIndex)

			If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
				Return MinIndex
				Exit Function
			End If

			searchDataRow = SortedRows(MaxIndex)
			If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
				Return MaxIndex
				Exit Function
			End If

			' now search outwards from the last returned value.

			If (Me.thisPosition >= MinIndex) And (Me.thisPosition <= MaxIndex) Then
				CurrentMin = thisPosition
				CurrentMax = CurrentMin + 1
			Else
				CurrentMin = CInt((MinIndex + MaxIndex) / 2)
				CurrentMax = CurrentMin + 1
			End If

			While (CurrentMin >= MinIndex) Or (CurrentMax <= MaxIndex)
				If (CurrentMin >= MinIndex) Then
					searchDataRow = SortedRows(CurrentMin)

					If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
						Return CurrentMin
						Exit Function
					End If

					CurrentMin -= 1
				End If

				If (CurrentMax <= MaxIndex) Then
					searchDataRow = SortedRows(CurrentMax)

					If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
						Return CurrentMax
						Exit Function
					End If

					CurrentMax += 1
				End If

			End While

			Return (-1)
			Exit Function

		Catch ex As Exception
			MainForm.LogError(Me.Name & ", Get_Position()", LOG_LEVELS.Warning, ex.Message, "", ex.StackTrace, True)
			Return (-1)
			Exit Function
		End Try

	End Function

#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code)"

    ''' <summary>
    ''' Handles the Click event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
		' Cancel Changes, redisplay form.

		FormChanged = False
		AddNewRecord = False

		Me.THIS_FORM_SelectingCombo.Enabled = True

		If (thisPosition >= 0) And (thisPosition < Me.SortedRows.GetLength(0)) Then
			thisDataRow = Me.SortedRows(thisPosition)
			Call GetFormData(thisDataRow)
		Else
			Call btnNavFirst_Click(Me, New System.EventArgs)
		End If

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnSave control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
		' Save Changes, if any, without prompting.

		If (FormChanged = True) Then
			Call SetFormData(False)
		End If

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnDelete control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
		Dim Position As Integer
		Dim NextAuditID As Integer

		If (AddNewRecord = True) Then
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Sub
		End If

		' No Appropriate Save permission :-

		If (Me.HasDeletePermission = False) Then
			MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "You do not have permission to Delete this record.", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Sub
		End If

		' *************************************************************
		' KnowledgeDate OK :-
		' *************************************************************

		If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
			MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "Changes to Venice are not allowed when the Knowledgedate is not set to `NOW`", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Sub
		End If

		' *************************************************************
		' Confirm :-
		' *************************************************************
		If MsgBox("Are you Sure ?", MsgBoxStyle.OkCancel, "Confirm Delete") = MsgBoxResult.Cancel Then
			Exit Sub
		End If

		' Check Data position.

		Position = Get_Position(thisAuditID)

		If Position < 0 Then Exit Sub

		' Check Referential Integrity 
		If CheckReferentialIntegrity(_MainForm, Me.SortedRows(Position)) = False Then
			MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "Deleting this record would voilate referential integrity.", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Sub
		End If

		' Resolve row to show after deleting this one.

		NextAuditID = (-1)
		If (Position + 1) < Me.SortedRows.GetLength(0) Then
			NextAuditID = CInt(Me.SortedRows(Position + 1)("AuditID"))
		ElseIf (Position - 1) >= 0 Then
			NextAuditID = CInt(Me.SortedRows(Position - 1)("AuditID"))
		Else
			NextAuditID = (-1)
		End If

		' Apply a Synchronisation Lock and then Delete this row

		Try
			Me.SortedRows(Position).Delete()

			MainForm.AdaptorUpdate(Me.Name, myAdaptor, myTable)

		Catch ex As Exception

			Call MainForm.LogError(Me.Name & ", Delete()", LOG_LEVELS.Error, ex.Message, "Error Deleting Record.", ex.StackTrace, True)
		End Try

		' Tidy Up.

		FormChanged = False

		thisAuditID = NextAuditID
		Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID), True)

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnAdd control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
		' Prepare form to Add a new record.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		thisPosition = -1
		InPaint = True
		MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
		InPaint = False

		GetFormData(Nothing)
		AddNewRecord = True

		Me.THIS_FORM_SelectingCombo.Enabled = False
		Me.btnCancel.Enabled = True
		Call SetButtonStatus()

		THIS_FORM_NewMoveToControl.Focus()

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnClose control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
		' Close Form

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		Me.Close()

	End Sub

#End Region

#Region " Bug hunting "

	' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
	' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
	' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

    ''' <summary>
    ''' Called when [row updating].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
	Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
		Dim a As Integer

		If Not e.Command Is Nothing Then
			a = 1
		End If

		If Not e.Errors Is Nothing Then
			a = 2
		End If
	End Sub

    ''' <summary>
    ''' Called when [row updated].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
	Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
		Dim a As Integer

		If Not e.Command Is Nothing Then
			a = 1
		End If

		If Not e.Errors Is Nothing Then
			a = 2
		End If
	End Sub

    ''' <summary>
    ''' Called when [row fill error].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
	Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
		Dim a As Integer

		a = 1
	End Sub

#End Region


    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_SelectCounterparty control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_SelectCounterparty_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_SelectCounterparty.SelectedIndexChanged

	End Sub

    ''' <summary>
    ''' Handles the KeyPress event of the Combo_SelectCounterparty control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.KeyPressEventArgs"/> instance containing the event data.</param>
	Private Sub Combo_SelectCounterparty_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Combo_SelectCounterparty.KeyPress

	End Sub

    ''' <summary>
    ''' Handles the KeyUp event of the Combo_SelectCounterparty control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
	Private Sub Combo_SelectCounterparty_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Combo_SelectCounterparty.KeyUp

	End Sub


End Class
