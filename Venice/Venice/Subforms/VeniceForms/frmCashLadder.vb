﻿' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 08-15-2013
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmCashLadder.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals
Imports RenaissancePertracDataClass
Imports RenaissanceUtilities.DatePeriodFunctions

Imports RenaissanceDataClass
Imports C1.Win.C1FlexGrid

''' <summary>
''' Class frmCashLadder
''' </summary>
Public Class frmCashLadder

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm


#Region " Form Locals and Constants "

  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
  Private WithEvents _MainForm As VeniceMain

  ' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
  Private FormTooltip As New ToolTip()

  ' Form Menu


  ' Form Constants, specific to the table being updated.

    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
  Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

  ' Form Locals, initialised on 'New' defining what standard data items to use
    ''' <summary>
  ''' Variable holding the form's data table name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_TABLENAME As String
    ''' <summary>
  ''' Variable holding the form's data adaptor name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_ADAPTORNAME As String
    ''' <summary>
  ''' Variable holding the form's dataset name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_DATASETNAME As String


  ' The standard ChangeID for this form. e.g. tblFund
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

  ' Form Specific Order fields
    ''' <summary>
  ''' Field Name to order the Select combo by.
    ''' </summary>
  Private THIS_FORM_OrderBy As String

  ' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
  Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
  Private THIS_FORM_FormID As VeniceFormID

  ' Data Structures

    ''' <summary>
    ''' My connection
    ''' </summary>
  Private myConnection As SqlConnection

    ''' <summary>
    ''' The transaction dataset
    ''' </summary>
  Private TransactionDataset As DataSet
    ''' <summary>
    ''' The transaction table
    ''' </summary>
  Private TransactionTable As DataTable
    ''' <summary>
    ''' The transaction data view
    ''' </summary>
  Private TransactionDataView As DataView
    ''' <summary>
    ''' The contra transaction dataset
    ''' </summary>
  Private ContraTransactionDataset As RenaissanceDataClass.DSTransactionContra
    ''' <summary>
    ''' The contra transaction table
    ''' </summary>
  Private ContraTransactionTable As RenaissanceDataClass.DSTransactionContra.tblTransactionContraDataTable
    ''' <summary>
    ''' The contra transaction data view
    ''' </summary>
  Private ContraTransactionDataView As DataView

    ''' <summary>
    ''' The sub fund heirarchy table
    ''' </summary>
  Private SubFundHeirarchyTable As DataTable = Nothing
    ''' <summary>
    ''' The sub fund dictionary
    ''' </summary>
  Private subFundDictionary As New Dictionary(Of Integer, DataRow)
    ''' <summary>
    ''' The FX map
    ''' </summary>
  Private FXMap As New Dictionary(Of Integer, Double)

    ''' <summary>
    ''' The _ trade status dictionary
    ''' </summary>
  Friend _TradeStatusDictionary As New LookupCollection(Of Integer, String)     ' Dictionary(Of Integer, String)

    ''' <summary>
    ''' The last entered transfers grid row
    ''' </summary>
  Private LastEnteredTransfersGridRow As Integer = 0
    ''' <summary>
    ''' The last entered transfers grid col
    ''' </summary>
  Private LastEnteredTransfersGridCol As Integer = 0
    ''' <summary>
    ''' The last entered transactions grid row
    ''' </summary>
  Private LastEnteredTransactionsGridRow As Integer = 0
    ''' <summary>
    ''' The last entered transactions grid col
    ''' </summary>
  Private LastEnteredTransactionsGridCol As Integer = 0
    ''' <summary>
    ''' The last entered positions grid row
    ''' </summary>
  Private LastEnteredPositionsGridRow As Integer = 0
    ''' <summary>
    ''' The last entered positions grid col
    ''' </summary>
  Private LastEnteredPositionsGridCol As Integer = 0

    ''' <summary>
    ''' The fund valuations
    ''' </summary>
  Private FundValuations As New Dictionary(Of Integer, Double)

    ''' <summary>
    ''' The this standard dataset
    ''' </summary>
  Private ThisStandardDataset As RenaissanceGlobals.StandardDataset

  ' Active Element.

    ''' <summary>
    ''' The __ is over cancel button
    ''' </summary>
  Private __IsOverCancelButton As Boolean
    ''' <summary>
    ''' The _ in use
    ''' </summary>
  Private _InUse As Boolean

  ' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
  Private FormIsValid As Boolean
    ''' <summary>
    ''' The in paint
    ''' </summary>
  Private InPaint As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
  Private _FormOpenFailed As Boolean

  ' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
  Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
  Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
  Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
  Private HasDeletePermission As Boolean

  ' Bit Shift values for creating 'SubFundKey' in PaintCashGrid()

    ''' <summary>
    ''' The SHIF t_ SU b_ FUND
    ''' </summary>
  Private Const SHIFT_SUB_FUND As Integer = 25
    ''' <summary>
    ''' The SHIF t_ CURRENCYID
    ''' </summary>
  Private Const SHIFT_CURRENCYID As Integer = 50

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
  Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
    ' Public property to return handle to the 'Main' Venice form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
  Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
    ' Public property maintaining a value indicating if the cursor is over the 'Cancel'
    ' Button on this form.
    ' This property is specifically designed for use by the field formating Event functions
    ' In order that they do not impose format restrictions if the user is about to click the 
    ' 'Cancel' button.
    '
    Get
      Return __IsOverCancelButton
    End Get
    Set(ByVal Value As Boolean)
      __IsOverCancelButton = Value
    End Set
  End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
  Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
    Get
      Return InPaint
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
    Get
      Return _InUse
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmCashLadder"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  Public Sub New(ByVal pMainForm As VeniceMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()

    _MainForm = pMainForm
    AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

    _FormOpenFailed = False
    _InUse = True

    ' Default Select and Order fields.

    THIS_FORM_OrderBy = "TransactionFund, TransactionInstrument"

    ' Form Permissioning :-

    THIS_FORM_PermissionArea = "frmCashLadder"
    THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

    ' 'This' form ID

    THIS_FORM_FormID = VeniceFormID.frmCashLadder

    ' This form's dataset type.

    ThisStandardDataset = RenaissanceStandardDatasets.tblTransaction ' This Defines the Form Data !!! 


    ' Data object names standard to this Form type.

    THIS_TABLENAME = ThisStandardDataset.TableName
    THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
    THIS_DATASETNAME = ThisStandardDataset.DatasetName

    THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

    ' Establish / Retrieve data objects for this form.

    myConnection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)
    TransactionDataset = MainForm.Load_Table(ThisStandardDataset, False)
    TransactionTable = TransactionDataset.Tables(0)

    ContraTransactionDataset = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblTransactionContra, False), RenaissanceDataClass.DSTransactionContra)
    ContraTransactionTable = ContraTransactionDataset.tblTransactionContra

    Try
      ' Grid :- Grid_CashLadder

      Grid_CashLadder.Rows.Count = 1
      Grid_CashLadder.Cols.Fixed = 0
      '    Grid_CashLadder.ExtendLastCol = True

      'styles
      Dim cs As CellStyle = Grid_CashLadder.Styles.Normal
      cs.Border.Direction = BorderDirEnum.Vertical
      cs.WordWrap = False

      cs = Grid_CashLadder.Styles.Add("Data")
      cs.BackColor = SystemColors.Info
      cs.ForeColor = Color.Black

      cs = Grid_CashLadder.Styles.Add("DataNegative")
      cs.BackColor = SystemColors.Info
      cs.ForeColor = Color.Red

      cs = Grid_CashLadder.Styles.Add("SourceNode")
      cs.BackColor = Color.Yellow
      cs.Font = New Font(Grid_CashLadder.Font, FontStyle.Bold)

      cs = Grid_CashLadder.Styles.Add("NumberPositive")
      cs.ForeColor = Color.Black

      cs = Grid_CashLadder.Styles.Add("NumberNegative")
      cs.ForeColor = Color.Red

      cs = Grid_CashLadder.Styles.Add("NumberPositiveHighlight")
      cs.ForeColor = Color.Black
      cs.BackColor = Color.LightYellow

      cs = Grid_CashLadder.Styles.Add("NumberNegativeHighlight")
      cs.ForeColor = Color.Red
      cs.BackColor = Color.LightYellow

      cs = Grid_CashLadder.Styles.Add("NumberPositiveBold")
      cs.ForeColor = Color.Black
      cs.Font = New Font(Grid_CashLadder.Font, FontStyle.Bold)

      cs = Grid_CashLadder.Styles.Add("NumberNegativeBold")
      cs.ForeColor = Color.Red
      cs.Font = New Font(Grid_CashLadder.Font, FontStyle.Bold)

      cs = Grid_CashLadder.Styles.Add("FundNumberPositiveHighlight")
      cs.ForeColor = Color.Black
      cs.BackColor = Color.LightYellow
      cs.Font = New Font(Grid_CashLadder.Font, FontStyle.Bold)

      cs = Grid_CashLadder.Styles.Add("FundNumberNegativeHighlight")
      cs.ForeColor = Color.Red
      cs.BackColor = Color.LightYellow
      cs.Font = New Font(Grid_CashLadder.Font, FontStyle.Bold)

      cs = Grid_CashLadder.Styles.Add("NumberPositiveWarning")
      cs.ForeColor = Color.Black
      cs.BackColor = Color.MistyRose
      cs.Border.Color = Color.Red
      cs.Border.Style = BorderStyleEnum.Flat
      cs.Border.Direction = BorderDirEnum.Both
      cs.Border.Width = 2
      cs.Font = New Font(Grid_CashLadder.Font, FontStyle.Bold)

      cs = Grid_CashLadder.Styles.Add("NumberNegativeWarning")
      cs.ForeColor = Color.Red
      cs.BackColor = Color.MistyRose
      cs.Border.Color = Color.Red
      cs.Border.Style = BorderStyleEnum.Flat
      cs.Border.Style = BorderStyleEnum.Flat
      cs.Border.Direction = BorderDirEnum.Both
      cs.Border.Width = 2
      cs.Font = New Font(Grid_CashLadder.Font, FontStyle.Bold)

      cs = Grid_CashLadder.Styles.Add("Hidden")
      cs.Display = DisplayEnum.None

      cs = Grid_CashLadder.Styles.Add("Visible")

      'outline tree
      Grid_CashLadder.Tree.Column = 0
      Grid_CashLadder.AllowMerging = AllowMergingEnum.Nodes

      'other
      Grid_CashLadder.AllowResizing = AllowResizingEnum.Columns
      Grid_CashLadder.SelectionMode = SelectionModeEnum.Cell

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error initialising Sub Funds Form.", ex.StackTrace, True)
    End Try

    ' Establish initial DataView and Initialise Transactions Grig.

    Try
      TransactionDataView = New DataView(TransactionTable, "True", "", DataViewRowState.CurrentRows)
      ContraTransactionDataView = New DataView(ContraTransactionTable, "True", "", DataViewRowState.CurrentRows)
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error initialising Sub Funds Form.", ex.StackTrace, True)
    End Try

    ' Form Control Changed events

    Try

      AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

      AddHandler Combo_TransactionFund.SelectedValueChanged, AddressOf Me.FormControlChanged
      AddHandler Combo_SubFund.SelectedValueChanged, AddressOf Me.FormControlChanged
      AddHandler Combo_CashView.SelectedValueChanged, AddressOf Me.FormControlChanged
      AddHandler Combo_DisplayPeriod.SelectedValueChanged, AddressOf Me.FormControlChanged
      AddHandler Date_StartDate.ValueChanged, AddressOf Me.FormControlChanged
      AddHandler Date_EndDate.ValueChanged, AddressOf Me.FormControlChanged
      AddHandler Check_UseSubFunds.CheckedChanged, AddressOf Me.FormControlChanged
      AddHandler Check_ShowRunningTotals.CheckedChanged, AddressOf Me.FormControlChanged

      AddHandler Combo_TransactionFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
      AddHandler Combo_SubFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
      AddHandler Combo_CashView.KeyUp, AddressOf MainForm.ComboSelectAsYouType
      AddHandler Combo_DisplayPeriod.KeyUp, AddressOf MainForm.ComboSelectAsYouType

      AddHandler Combo_TransactionFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
      AddHandler Combo_SubFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
      AddHandler Combo_CashView.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
      AddHandler Combo_DisplayPeriod.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

      AddHandler Combo_TransactionFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
      AddHandler Combo_SubFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
      AddHandler Combo_CashView.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
      AddHandler Combo_DisplayPeriod.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

      AddHandler Combo_TransactionFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
      AddHandler Combo_SubFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
      AddHandler Combo_CashView.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
      AddHandler Combo_DisplayPeriod.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error initialising Sub Funds Form.", ex.StackTrace, True)
    End Try

    ' Set up the ToolTip
    MainForm.SetFormToolTip(Me, FormTooltip)


  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
  Public Sub ResetForm() Implements StandardVeniceForm.ResetForm
    THIS_FORM_OrderBy = "TransactionFund, TransactionInstrument"

    Call Form_Load(Me, New System.EventArgs)
  End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
  Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
    ALWAYS_CLOSE_THIS_FORM = True
    Me.Close()
  End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
    _FormOpenFailed = False
    _InUse = True

    ' Initialise Data structures. Connection, Adaptor and Dataset.

    If Not (MainForm Is Nothing) Then
      FormIsValid = True
    Else
      MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (myConnection Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (TransactionDataset Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If


    ' Check User permissions
    Try
      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form.", "", True)

        FormIsValid = False
        _FormOpenFailed = True

        Exit Sub
      End If
    Catch ex As Exception
      FormIsValid = False
      _FormOpenFailed = True

      Exit Sub
    End Try


    Try
      ' Initialse form

      InPaint = True
      IsOverCancelButton = False

      ' Initialise main select controls
      ' Build Sorted data list from which this form operates

      Call getSubFundHeirarchy()
      Call getFXRates(MainForm.Main_Knowledgedate.Date) ' Should give FXs for current date unless Knowledge date is set.
      Call SetFundCombo()
      Call SetPerodCombo()

      If (Combo_TransactionFund.Items.Count > 1) Then
        Me.Combo_TransactionFund.SelectedIndex = 1
      Else
        Me.Combo_TransactionFund.SelectedIndex = -1
      End If

      Call SetSubFundCombos()
      Call SetTradeStatusCombos()

      MainForm.ClearComboSelection(Combo_SubFund)

      Combo_DisplayPeriod.SelectedValue = REFERENCE_PERIOD
      Combo_CashView.SelectedIndex = 0
      Check_ShowRunningTotals.Checked = False

      Select Case REFERENCE_PERIOD
        Case DealingPeriod.Daily
          Date_StartDate.Value = FitDateToPeriod(DealingPeriod.Weekly, AddPeriodToDate(DealingPeriod.Daily, Now.Date, -1), False)
          Date_EndDate.Value = FitDateToPeriod(DealingPeriod.Daily, FitDateToPeriod(DealingPeriod.Weekly, AddPeriodToDate(DealingPeriod.Weekly, Now.Date, 1), True))

        Case Else
          Date_StartDate.Value = FitDateToPeriod(CInt(Combo_DisplayPeriod.SelectedValue), AddPeriodToDate(CInt(Combo_DisplayPeriod.SelectedValue), Now.Date, -1), False)
          Date_EndDate.Value = FitDateToPeriod(CInt(Combo_DisplayPeriod.SelectedValue), AddPeriodToDate(CInt(Combo_DisplayPeriod.SelectedValue), Now.Date, 5), True)

      End Select

      Call MainForm.SetComboSelectionLengths(Me)

      Call PaintCashGrid()

      Grid_CashLadder.Cols("DateIndex").Sort = SortFlags.Ascending
      Grid_CashLadder.Cols("TransactionParentID").Sort = SortFlags.Ascending
      Grid_CashLadder.Sort(SortFlags.UseColSort, Grid_CashLadder.Cols("DateIndex").Index, Grid_CashLadder.Cols("TransactionParentID").Index)

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error initialising Sub Funds Form.", ex.StackTrace, True)
    Finally
      InPaint = False
    End Try


  End Sub

    ''' <summary>
    ''' Handles the Closing event of the frm control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
  Private Sub frm_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    Dim HideForm As Boolean

    ' Hide or Close this form ?
    ' All depends on how many of this form type are Open or in Cache...

    _InUse = False

    If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
      HideForm = False
    Else
      HideForm = True
      If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
        HideForm = False
      End If
    End If

    If HideForm = True Then
      MainForm.HideInFormsCollection(Me)
      Me.Hide() ' NPP Fix

      e.Cancel = True
    Else
      Try
        MainForm.RemoveFromFormsCollection(Me)
        RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

        RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

        RemoveHandler Combo_TransactionFund.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_SubFund.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_CashView.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_DisplayPeriod.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Date_StartDate.ValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Date_EndDate.ValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_UseSubFunds.CheckedChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_ShowRunningTotals.CheckedChanged, AddressOf Me.FormControlChanged

        RemoveHandler Combo_TransactionFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_SubFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_CashView.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_DisplayPeriod.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        RemoveHandler Combo_TransactionFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_SubFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_CashView.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_DisplayPeriod.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

        RemoveHandler Combo_TransactionFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_SubFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_CashView.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_DisplayPeriod.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

        RemoveHandler Combo_TransactionFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_SubFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_CashView.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_DisplayPeriod.LostFocus, AddressOf MainForm.GenericCombo_LostFocus


      Catch ex As Exception
      End Try
    End If

  End Sub


#End Region


  ' Routine to handle changes / updates to tables by this and other windows.
  ' If this, or any other, form posts a change to a table, then it will invoke an update event 
  ' detailing what tables have been altered.
  ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
  ' the 'VeniceAutoUpdate' event of the main Venice form.
  ' Each form may them react as appropriate to changes in any table that might impact it.
  '
    ''' <summary>
  ''' Process update messages.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    Dim OrgInPaint As Boolean
    Dim KnowledgeDateChanged As Boolean
    Dim RefreshGrid As Boolean

    If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

    OrgInPaint = InPaint

    Try
      InPaint = True
      KnowledgeDateChanged = False
      RefreshGrid = False

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
        KnowledgeDateChanged = True
      End If
    Catch ex As Exception
    End Try

    ' ****************************************************************
    ' Check for changes relevant to this form
    ' ****************************************************************

    ' Changes to the tblPrice table :-
    ' Clear 'Best Prices' cache.

    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFX) = True) Or KnowledgeDateChanged Then
        Call getFXRates(MainForm.Main_Knowledgedate.Date) ' Should give FXs for current date unless Knowledge date is set.
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblFX", ex.StackTrace, True)
    End Try

    ' Changes to the tblFund table :-

    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFund) = True) Or KnowledgeDateChanged Then
        Call SetFundCombo()
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblFund", ex.StackTrace, True)
    End Try

    ' Changes to the tblCurrency table :-

    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblCurrency) = True) Or KnowledgeDateChanged Then
        Call SetFundCombo()
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblCurrency", ex.StackTrace, True)
    End Try

    ' Changes to the tblSubFund table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblSubFund) = True) Or KnowledgeDateChanged Then
        getSubFundHeirarchy()
        Call SetSubFundCombos()

        ' Clear the grid for SubFund changes. The Draw routine does not handle Sub Funds disappearing or moving.
        'gridsubFundMap.Clear() ' Important, if setting grid rows to 1 !!!
        Grid_CashLadder.Rows.Count = 1

        RefreshGrid = True
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblSubFund", ex.StackTrace, True)
    End Try

    ' Changes to the tblTradeStatus table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblTradeStatus) = True) Or KnowledgeDateChanged Then

        Call SetTradeStatusCombos()

        RefreshGrid = True
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblSubFund", ex.StackTrace, True)
    End Try

    ' Changes to the tblTransaction table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblTransaction) = True) Or KnowledgeDateChanged Then

        ' RefreshGrid = True ' Wait for the tblTransactionContra Update. Code is written such that the tblTransaction update should arrive before the tblTransactionContra update.
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblTransaction", ex.StackTrace, True)
    End Try

    ' Changes to the tblTransactionContra table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblTransactionContra) = True) Or KnowledgeDateChanged Then

        RefreshGrid = True
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblTransaction", ex.StackTrace, True)
    End Try


    ' Changes to the KnowledgeDate :-
    Try
      If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
        RefreshGrid = True
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: KnowledgeDate", ex.StackTrace, True)
    End Try


    ' Changes to the tblUserPermissions table :-

    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

        ' Check ongoing permissions.

        Call CheckPermissions()
        If (HasReadPermission = False) Then
          Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

          FormIsValid = False
          InPaint = OrgInPaint
          Me.Close()
          Exit Sub
        End If

        RefreshGrid = True

      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: tblUserPermissions", ex.StackTrace, True)
    End Try


    ' ****************************************************************
    ' Changes to the Main FORM table :-
    ' (or the tblTransactions table)
    ' ****************************************************************

    Try
      If (RefreshGrid = True) Or _
        KnowledgeDateChanged Then

        If (MainForm.Extra_Debug) Then
          Call MainForm.LogError("CashLadder, AutoUpdate()", LOG_LEVELS.Audit, "", "GridRefresh, " & e.TablesChanged(0).ToString, "", False)
        End If

        ' Re-Set Controls etc.
        Call PaintCashGrid()

      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: Main Form table", ex.StackTrace, True)
    End Try

    InPaint = OrgInPaint

  End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

    ''' <summary>
    ''' Gets the FX to fund.
    ''' </summary>
    ''' <param name="InstrumentCurrencyID">The instrument currency ID.</param>
    ''' <param name="FundCurrencyID">The fund currency ID.</param>
    ''' <returns>System.Double.</returns>
  Private Function GetFXToFund(ByVal InstrumentCurrencyID As Integer, ByVal FundCurrencyID As Integer) As Double
    ' *******************************************************************************
    '
    ' *******************************************************************************
    Dim RVal As Double = 1.0#

    Try
      Dim InstrumentFX As Double = 1.0#
      Dim FundFX As Double = 1.0#

      If (InstrumentCurrencyID = FundCurrencyID) Then
        Return 1.0#
      End If

      If (FXMap IsNot Nothing) Then

        If (FXMap.ContainsKey(InstrumentCurrencyID)) Then
          InstrumentFX = FXMap(InstrumentCurrencyID)
        End If

        If (FXMap.ContainsKey(FundCurrencyID)) Then
          FundFX = FXMap(FundCurrencyID)
        End If

        RVal = InstrumentFX / FundFX

      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in GetFXToFund()", ex.StackTrace, True)
    End Try

    Return RVal

  End Function

    ''' <summary>
    ''' Gets the FX rates.
    ''' </summary>
    ''' <param name="ValueDate">The value date.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function getFXRates(ByVal ValueDate As Date) As Boolean
    ' *******************************************************************************
    '
    ' *******************************************************************************
    Dim FXTable As New DataTable
    Dim FXRow As DataRow
    Dim DBConnection As SqlConnection = Nothing
    Dim ThisCurrency As Integer

    Try
      If (ValueDate <= RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW) Then
        ValueDate = Now.Date
      End If

      If (FXMap Is Nothing) Then
        FXMap = New Dictionary(Of Integer, Double)
      End If

      FXMap.Clear()

      Dim thisSelectCommand As New SqlCommand

      Try
        DBConnection = MainForm.GetVeniceConnection()

        thisSelectCommand.Connection = DBConnection
        thisSelectCommand.CommandType = CommandType.StoredProcedure
        thisSelectCommand.CommandText = "adp_tblBestFX_SelectCommand"
        thisSelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT
        thisSelectCommand.Parameters.Add(New SqlParameter("@ValueDate", SqlDbType.DateTime)).Value = ValueDate
        thisSelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", SqlDbType.DateTime)).Value = MainForm.Main_Knowledgedate

        MainForm.LoadTable_Custom(FXTable, thisSelectCommand)
        'FXTable.Load(thisSelectCommand.ExecuteReader)

      Catch ex As Exception
        MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error loading Sub-Fund Tree in getFXRates().", ex.StackTrace, True)

      Finally
        If (DBConnection IsNot Nothing) AndAlso (DBConnection.State And ConnectionState.Open) Then
          DBConnection.Close()
        End If
        thisSelectCommand.Connection = Nothing
      End Try

      If (FXTable IsNot Nothing) AndAlso (FXTable.Rows.Count > 0) Then
        For Each FXRow In FXTable.Rows
          If (IsNumeric(FXRow("FXCurrencyCode"))) Then
            ThisCurrency = CInt(FXRow("FXCurrencyCode"))

            If (FXMap.ContainsKey(ThisCurrency) = False) Then
              FXMap.Add(ThisCurrency, CDbl(FXRow("FXRate")))
            End If
          End If
        Next
      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in getFXRates()", ex.StackTrace, True)
    End Try

    Return True

  End Function

  ''' <summary>
  ''' Gets the sub fund heirarchy.
  ''' </summary>
  ''' <returns>Dictionary{System.Int32DataRow}.</returns>
  Private Function getSubFundHeirarchy() As Dictionary(Of Integer, DataRow)
    ' *******************************************************************************
    '
    ' *******************************************************************************
    ' Dim SubFundDS As DSSubFund
    Dim SubFundRow As DataRow
    Dim DBConnection As SqlConnection = Nothing

    Try
      ' Initialise

      subFundDictionary.Clear()
      If (SubFundHeirarchyTable Is Nothing) Then
        SubFundHeirarchyTable = New DataTable
      Else
        SubFundHeirarchyTable.Clear()
      End If

      '' Get Sub Fund table
      'SubFundDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblSubFund)

      'If (SubFundDS Is Nothing) Then
      '  Return subFundDictionary
      'End If

      'SyncLock SubFundDS
      '  MainForm.CopyTable(SubFundDS.tblSubFund, SubFundTable)
      'End SyncLock

      ' Get Sub Fund Tree
      Dim thisSelectCommand As New SqlCommand

      Try
        DBConnection = MainForm.GetVeniceConnection()


        thisSelectCommand.Connection = DBConnection
        thisSelectCommand.CommandType = CommandType.StoredProcedure
        thisSelectCommand.CommandText = "adp_tblSubFund_Heirarchy"
        thisSelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT
        thisSelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", SqlDbType.DateTime)).Value = MainForm.Main_Knowledgedate

        MainForm.LoadTable_Custom(SubFundHeirarchyTable, thisSelectCommand)
        'SubFundHeirarchyTable.Load(thisSelectCommand.ExecuteReader)

      Catch ex As Exception
        MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error loading Sub-Fund Tree in getSubFundHeirarchy().", ex.StackTrace, True)

      Finally
        If (DBConnection IsNot Nothing) AndAlso (DBConnection.State And ConnectionState.Open) Then
          DBConnection.Close()
        End If
        thisSelectCommand.Connection = Nothing
      End Try

      If (SubFundHeirarchyTable.Rows.Count > 0) Then
        For Each SubFundRow In SubFundHeirarchyTable.Rows
          subFundDictionary.Add(CInt(SubFundRow("EquivalentFundID")), SubFundRow)
        Next
      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in getSubFundHeirarchy()", ex.StackTrace, True)
    End Try

    Return subFundDictionary

  End Function

  ' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
  Private Sub CheckPermissions()
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Dim Permissions As Integer

    Try
      Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

      HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
      HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
      HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
      HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in CheckPermissions()", ex.StackTrace, True)
      HasReadPermission = False
      HasUpdatePermission = False
      HasInsertPermission = False
      HasDeletePermission = False
    End Try

  End Sub

    ''' <summary>
  ''' Basic event handler, called when a form value is changed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' *******************************************************************************
    ' In the event of one of the Form Controls changing, refresh the data grid.
    ' *******************************************************************************

    If (Me.Created) AndAlso (Not Me.IsDisposed) AndAlso (Not InPaint) Then
      Dim ReSort As Boolean = True
      Dim GridFocus As Boolean = False

      Try

        InPaint = True

        If (TypeOf sender Is ComboBox) Then
          MainForm.GenericCombo_SelectedIndexChanged(sender, Nothing)
        End If

        If sender Is Combo_TransactionFund Then
          Call SetSubFundCombos()
          GridFocus = True
        End If

        If sender Is Combo_DisplayPeriod Then
          ' Clear existing Date columns if the Date period changes.

          Dim thisGridcolumn As C1.Win.C1FlexGrid.Column
          Dim removeArray As New ArrayList

          For Each thisGridcolumn In Grid_CashLadder.Cols
            If (thisGridcolumn.Name.Length = 12) AndAlso (thisGridcolumn.Name.StartsWith("col_")) AndAlso (IsNumeric(thisGridcolumn.Name.Substring(4, 8))) Then
              removeArray.Add(thisGridcolumn)
            End If
          Next

          If removeArray.Count > 0 Then
            For Each thisGridcolumn In removeArray
              Grid_CashLadder.Cols.Remove(thisGridcolumn)
            Next
            removeArray.Clear()
          End If
        End If

        Dim thisDisplayPeriod As DealingPeriod = REFERENCE_PERIOD
        If (IsNumeric(Combo_DisplayPeriod.SelectedValue)) Then
          thisDisplayPeriod = CType(Combo_DisplayPeriod.SelectedValue, DealingPeriod)
        End If

        If sender Is Date_StartDate Then
          Date_StartDate.Value = FitDateToPeriod(thisDisplayPeriod, Date_StartDate.Value, False)
        End If

        If sender Is Date_EndDate Then
          Date_EndDate.Value = FitDateToPeriod(thisDisplayPeriod, Date_EndDate.Value, False)
        End If

        If sender Is Check_UseSubFunds Then
          ' gridSubFundMap.Clear() ' Important, if setting grid rows to 1 !!!
          Grid_CashLadder.Rows.Count = 1
        End If

      Catch ex As Exception
        MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in FormControlChanged()", ex.StackTrace, True)
      Finally
        InPaint = False
      End Try

      Call PaintCashGrid()

      Try
        If (ReSort) Then
          Grid_CashLadder.Cols("DateIndex").Sort = SortFlags.Ascending
          Grid_CashLadder.Cols("TransactionParentID").Sort = SortFlags.Ascending
          Grid_CashLadder.Sort(SortFlags.UseColSort, Grid_CashLadder.Cols("DateIndex").Index)
          'Grid_CashLadder.Sort(SortFlags.UseColSort, Grid_CashLadder.Cols("DateIndex").Index, Grid_CashLadder.Cols("TransactionParentID").Index)
        End If
      Catch ex As Exception
      End Try

      If (GridFocus) Then
        Grid_CashLadder.Focus()
      End If

    End If
  End Sub

#End Region

#Region " Set Form Combos (Form Specific Code) "

    ''' <summary>
    ''' Sets the fund combo.
    ''' </summary>
  Private Sub SetFundCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_TransactionFund, _
    RenaissanceStandardDatasets.tblFund, _
    "FundName", _
    "FundID", _
    "FundClosed=0", False, True, True)   ' 

  End Sub

    ''' <summary>
    ''' Sets the perod combo.
    ''' </summary>
  Private Sub SetPerodCombo()

    Call MainForm.SetTblGenericCombo(Me.Combo_DisplayPeriod, GetType(DealingPeriod), False)   ' 

  End Sub

    ''' <summary>
    ''' Sets the trade status combos.
    ''' </summary>
  Private Sub SetTradeStatusCombos()

    Try
      Dim TradeStatusDS As DSTradeStatus
      Dim TradeStatusRow As DSTradeStatus.tblTradeStatusRow

      TradeStatusDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblTradeStatus)

      _TradeStatusDictionary.Clear()

      For Each TradeStatusRow In TradeStatusDS.tblTradeStatus
        _TradeStatusDictionary.Add(TradeStatusRow.TradeStatusID, TradeStatusRow.TradeStatusName)
      Next

    Catch ex As Exception
    Finally
      _TradeStatusDictionary.Sort()
    End Try



  End Sub

    ''' <summary>
    ''' Sets the sub fund combos.
    ''' </summary>
  Private Sub SetSubFundCombos()

    Dim OrgInPaint As Boolean = InPaint
    Try
      Dim FundID As Integer = 0

      InPaint = True

      If (IsNumeric(Combo_TransactionFund.SelectedValue)) Then
        FundID = CInt(Combo_TransactionFund.SelectedValue)
      End If

      Call MainForm.SetTblGenericCombo( _
      Me.Combo_SubFund, _
      SubFundHeirarchyTable.Select("TopFundID=" & FundID.ToString(), "SubFundName"), _
      "SubFundName", _
      "SubFundID", _
      "", False, True, True)   ' 

    Catch ex As Exception
    Finally
      InPaint = OrgInPaint
    End Try

  End Sub

#End Region

#Region " Paint Form, Calculate Transaction, totals etc."

    ''' <summary>
    ''' Paints the cash grid.
    ''' </summary>
  Private Sub PaintCashGrid()
    ' **********************************************************************************
    '
    ' **********************************************************************************
    Const DATE_COLUMN_OFFSET As Integer = 2

    Try
      Dim TransactionDataset As RenaissanceDataClass.DSTransaction = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblTransaction), RenaissanceDataClass.DSTransaction)
      Dim TransactionDataView As DataView
      Dim ContraTransactionDataset As RenaissanceDataClass.DSTransactionContra = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblTransactionContra), RenaissanceDataClass.DSTransactionContra)
      Dim ContraTransactionDataView As DataView
      Dim thisInstrumentRow As RenaissanceDataClass.DSInstrument.tblInstrumentRow = Nothing
      Dim SubFundHeirarchySelection() As DataRow

      Dim gridSubFundMap As New Dictionary(Of ULong, C1.Win.C1FlexGrid.Row)
      Dim SubFundKey As ULong
      Dim LastSubFundKey As ULong

      Dim thisGridcolumn As C1.Win.C1FlexGrid.Column
      Dim thisGridRow As C1.Win.C1FlexGrid.Row = Nothing
      Dim NewGridRow As C1.Win.C1FlexGrid.Row = Nothing
      Dim SubFundNode As C1.Win.C1FlexGrid.Row = Nothing
      Dim thisDate As Date
      Dim removeArray As New ArrayList
      Dim Counter As Integer
      Dim ColCounter As Integer
      Dim StartIndex As Integer
      Dim EndIndex As Integer
      Dim thisPriceIndex As Integer
      Dim thisColName As String

      Dim thisFundID As Integer
      Dim thisSubFundID As Integer
      Dim thisTransactionTypeID As Integer
      Dim thisCurrencyID As Integer
      Dim thisCurrencyIndex As Integer

      Dim FundID As Integer
      Dim SubFundID As Integer
      Dim StartDate As Date = Date_StartDate.Value
      Dim EndDate As Date = Date_EndDate.Value
      Dim CashPeriod As DealingPeriod = IIf((Combo_DisplayPeriod.SelectedIndex >= 0) And IsNumeric(Combo_DisplayPeriod.SelectedValue), CType(Combo_DisplayPeriod.SelectedValue, DealingPeriod), DealingPeriod.Daily)
      Dim ShowSubFunds As Boolean = Check_UseSubFunds.Checked
      Dim UseValueDates As Boolean = IIf(Combo_CashView.SelectedIndex = 1, True, False)
      Dim ShowDeltaOnSummary As Boolean = True

      ' Get Sub Fund information.

      If (subFundDictionary Is Nothing) OrElse (subFundDictionary.Count = 0) Then
        subFundDictionary = getSubFundHeirarchy()
      End If

      ' Get FundID and SubFundID

      If (IsNumeric(Combo_TransactionFund.SelectedValue)) Then
        FundID = CInt(Combo_TransactionFund.SelectedValue)

        SubFundHeirarchySelection = SubFundHeirarchyTable.Select("(TopFundID=" & FundID.ToString("###0") & ") AND (IsClosed=false)", "TreeOrder")
      Else
        SubFundHeirarchySelection = SubFundHeirarchyTable.Select("IsClosed=false", "TreeOrder")
        FundID = 0
      End If

      If (IsNumeric(Combo_SubFund.SelectedValue)) Then
        SubFundID = CInt(Combo_SubFund.SelectedValue)
      Else
        SubFundID = 0
      End If

      ' 

      Grid_CashLadder.Redraw = False
      Grid_CashLadder.Cols.Fixed = 0
      Grid_CashLadder.Tree.Column = 0
      Grid_CashLadder.Cols.Frozen = 1

      ' Validate

      Try
        If StartDate > EndDate Then
          Dim tmpDate As Date
          tmpDate = StartDate
          StartDate = EndDate
          EndDate = tmpDate
        End If

        StartDate = FitDateToPeriod(CashPeriod, StartDate, False)
        EndDate = FitDateToPeriod(CashPeriod, EndDate, True)
      Catch ex As Exception
      End Try

      ' Initialise Grid

      For Each thisGridcolumn In Grid_CashLadder.Cols
        Select Case thisGridcolumn.Name

          Case "FirstCol"
            thisGridcolumn.Move(0)
            thisGridcolumn.Width = 250
          Case "FundID"

          Case "SubFundID"

          Case "InstrumentID"

          Case "CurrencyID"

          Case "TransactionParentID"

          Case "OpeningBalance"

          Case "ClosingBalance"

          Case "InUse"

          Case "DateIndex"

          Case Else
            ' Cash columns have the format col_YYYYMMDD
            If (thisGridcolumn.Name.Length = 12) AndAlso (thisGridcolumn.Name.StartsWith("col_")) AndAlso (IsNumeric(thisGridcolumn.Name.Substring(4, 8))) Then
              Try
                thisDate = New Date(CInt(thisGridcolumn.Name.Substring(4, 4)), CInt(thisGridcolumn.Name.Substring(8, 2)), CInt(thisGridcolumn.Name.Substring(10, 2)))

                If (thisDate < StartDate) OrElse (thisDate > EndDate) Then
                  removeArray.Add(thisGridcolumn)
                End If
              Catch ex As Exception
                removeArray.Add(thisGridcolumn)
              End Try

            Else
              removeArray.Add(thisGridcolumn)
            End If

        End Select
      Next

      ' Clear unused columns

      If removeArray.Count > 0 Then
        For Each thisGridcolumn In removeArray
          Grid_CashLadder.Cols.Remove(thisGridcolumn)
        Next
        removeArray.Clear()
      End If

      ' Add Necessary Columns

      If (Not Grid_CashLadder.Cols.Contains("FirstCol")) Then
        thisGridcolumn = Grid_CashLadder.Cols.Add()
        thisGridcolumn.Name = "FirstCol"
        thisGridcolumn.AllowEditing = False
        thisGridcolumn.AllowDragging = False
        thisGridcolumn.DataType = GetType(String)
        thisGridcolumn.Width = 250
        thisGridcolumn.Move(0)
        thisGridcolumn.Visible = True

      End If

      If (Not Grid_CashLadder.Cols.Contains("FundID")) Then
        thisGridcolumn = Grid_CashLadder.Cols.Add()
        thisGridcolumn.Name = "FundID"
        thisGridcolumn.AllowEditing = False
        thisGridcolumn.AllowDragging = False
        thisGridcolumn.DataType = GetType(Integer)
        thisGridcolumn.Visible = False
      End If

      If (Not Grid_CashLadder.Cols.Contains("SubFundID")) Then
        thisGridcolumn = Grid_CashLadder.Cols.Add()
        thisGridcolumn.Name = "SubFundID"
        thisGridcolumn.AllowEditing = False
        thisGridcolumn.AllowDragging = False
        thisGridcolumn.DataType = GetType(Integer)
        thisGridcolumn.Visible = False
      End If

      If (Not Grid_CashLadder.Cols.Contains("InstrumentID")) Then
        thisGridcolumn = Grid_CashLadder.Cols.Add()
        thisGridcolumn.Name = "InstrumentID"
        thisGridcolumn.AllowEditing = False
        thisGridcolumn.AllowDragging = False
        thisGridcolumn.DataType = GetType(Integer)
        thisGridcolumn.Visible = False
      End If

      If (Not Grid_CashLadder.Cols.Contains("CurrencyID")) Then
        thisGridcolumn = Grid_CashLadder.Cols.Add()
        thisGridcolumn.Name = "CurrencyID"
        thisGridcolumn.AllowEditing = False
        thisGridcolumn.AllowDragging = False
        thisGridcolumn.DataType = GetType(Integer)
        thisGridcolumn.Visible = False
      End If

      If (Not Grid_CashLadder.Cols.Contains("TransactionParentID")) Then
        thisGridcolumn = Grid_CashLadder.Cols.Add()
        thisGridcolumn.Name = "TransactionParentID"
        thisGridcolumn.AllowEditing = False
        thisGridcolumn.AllowSorting = True
        thisGridcolumn.AllowDragging = False
        thisGridcolumn.DataType = GetType(Integer)
        thisGridcolumn.Visible = False
      End If

      If (Not Grid_CashLadder.Cols.Contains("OpeningBalance")) Then
        thisGridcolumn = Grid_CashLadder.Cols.Add()
        thisGridcolumn.Name = "OpeningBalance"
        thisGridcolumn.Caption = "Opening"
        thisGridcolumn.AllowEditing = False
        thisGridcolumn.AllowDragging = False
        thisGridcolumn.DataType = GetType(Double)
        thisGridcolumn.Format = "N0"
        thisGridcolumn.Visible = True
        thisGridcolumn.Move(1)
      End If

      If (Not Grid_CashLadder.Cols.Contains("ClosingBalance")) Then
        thisGridcolumn = Grid_CashLadder.Cols.Add()
        thisGridcolumn.Name = "ClosingBalance"
        thisGridcolumn.Caption = "Closing"
        thisGridcolumn.AllowEditing = False
        thisGridcolumn.AllowDragging = False
        thisGridcolumn.DataType = GetType(Double)
        thisGridcolumn.Format = "N0"
        thisGridcolumn.Visible = True
        thisGridcolumn.Move(2)
      End If

      If (Not Grid_CashLadder.Cols.Contains("InUse")) Then
        thisGridcolumn = Grid_CashLadder.Cols.Add()
        thisGridcolumn.Name = "InUse"
        thisGridcolumn.AllowEditing = False
        thisGridcolumn.AllowDragging = False
        thisGridcolumn.DataType = GetType(Boolean)
        thisGridcolumn.Visible = False
      End If

      If (Not Grid_CashLadder.Cols.Contains("DateIndex")) Then
        thisGridcolumn = Grid_CashLadder.Cols.Add()
        thisGridcolumn.Name = "DateIndex"
        thisGridcolumn.AllowEditing = False
        thisGridcolumn.AllowSorting = True
        thisGridcolumn.AllowDragging = False
        thisGridcolumn.DataType = GetType(Integer)
        thisGridcolumn.Visible = False
      End If


      StartIndex = 0
      EndIndex = GetPriceIndex(CashPeriod, StartDate, EndDate)

      For Counter = StartIndex To EndIndex
        thisDate = FitDateToPeriod(CashPeriod, AddPeriodToDate(CashPeriod, StartDate, Counter))
        thisColName = "col_" & thisDate.ToString("yyyyMMdd")

        If (Grid_CashLadder.Cols.Contains(thisColName)) Then
          thisGridcolumn = Grid_CashLadder.Cols(thisColName)
        Else
          thisGridcolumn = Grid_CashLadder.Cols.Add()
          thisGridcolumn.Name = thisColName
          thisGridcolumn.AllowEditing = False
          thisGridcolumn.AllowDragging = False
          thisGridcolumn.DataType = GetType(Double)
        End If

        Select Case CashPeriod

          Case DealingPeriod.Weekly
            thisGridcolumn.Caption = "W/E " & thisDate.ToString("dd MMM yyyy")

          Case DealingPeriod.Fortnightly
            thisGridcolumn.Caption = "F/E " & thisDate.ToString("dd MMM yyyy")

          Case DealingPeriod.Monthly
            thisGridcolumn.Caption = thisDate.ToString("MMM yyyy")

          Case DealingPeriod.Quarterly
            thisGridcolumn.Caption = "Q" & (((thisDate.Month - 1) \ 3) + 1).ToString & " " & thisDate.ToString("yyyy")

          Case DealingPeriod.SemiAnnually
            thisGridcolumn.Caption = "H" & (((thisDate.Month - 1) \ 6) + 1).ToString & " " & thisDate.ToString("yyyy")

          Case DealingPeriod.Annually
            thisGridcolumn.Caption = thisDate.ToString("yyyy")

          Case Else ' and DealingPeriod.Daily
            thisGridcolumn.Caption = thisDate.ToString("dd MMM yyyy")

        End Select
        thisGridcolumn.Visible = True
        thisGridcolumn.Format = "N0"
        thisGridcolumn.Move(Counter + DATE_COLUMN_OFFSET)

      Next

      Dim col_InUse As Integer = Grid_CashLadder.Cols("InUse").Index
      Dim col_ClosingBalance As Integer = Grid_CashLadder.Cols("ClosingBalance").Index
      Dim col_OpeningBalance As Integer = Grid_CashLadder.Cols("OpeningBalance").Index
      Dim col_TransactionParentID As Integer = Grid_CashLadder.Cols("TransactionParentID").Index
      Dim col_InstrumentID As Integer = Grid_CashLadder.Cols("InstrumentID").Index
      Dim col_SubFundID As Integer = Grid_CashLadder.Cols("SubFundID").Index
      Dim col_FundID As Integer = Grid_CashLadder.Cols("FundID").Index
      Dim col_CurrencyID As Integer = Grid_CashLadder.Cols("CurrencyID").Index
      Dim col_FirstCol As Integer = Grid_CashLadder.Cols("FirstCol").Index
      Dim col_DateIndex As Integer = Grid_CashLadder.Cols("DateIndex").Index

      ' Styles
      Dim cs As CellStyle

      If (Not Grid_CashLadder.Styles.Contains("NumberPositive")) Then
        cs = Grid_CashLadder.Styles.Add("NumberPositive")
        cs.ForeColor = Color.Black
      End If

      If (Not Grid_CashLadder.Styles.Contains("NumberNegative")) Then
        cs = Grid_CashLadder.Styles.Add("NumberNegative")
        cs.ForeColor = Color.Red
      End If

      ' OK, Init done, On we go.

      If FundID <= 0 Then
        'gridSubFundMap.Clear() ' Important, if setting grid rows to 1 !!!
        Grid_CashLadder.Rows.Count = 1
        'Exit Sub
      End If

      ' Get Transaction sets.

      If (TransactionDataset Is Nothing) OrElse (ContraTransactionDataset Is Nothing) Then
        'gridSubFundMap.Clear() ' Important, if setting grid rows to 1 !!!
        Grid_CashLadder.Rows.Count = 1
        Exit Sub
      End If

      Dim SelectString As String = "true"
      Dim OrderString As String = "TransactionFund"

      If (ShowSubFunds) Then
        OrderString &= ",TransactionsubFund"
      End If
      If (UseValueDates) Then
        OrderString &= ",TransactionValueDate, TransactionParentID"
      Else
        OrderString &= ",TransactionSettlementDate, TransactionParentID"
      End If

      If (FundID > 0) Then
        SelectString = "TransactionFund=" & FundID.ToString("###0")
      End If

      ' 
      TransactionDataView = New DataView(TransactionDataset.tblTransaction, SelectString, OrderString, DataViewRowState.CurrentRows)
      ContraTransactionDataView = New DataView(ContraTransactionDataset.tblTransactionContra, SelectString, OrderString, DataViewRowState.CurrentRows)

      ' Get List of Cash Instruments

      Dim CashInstrumentsDictionary As New Dictionary(Of Integer, Integer)
      Try
        Dim InstrumentsDS As RenaissanceDataClass.DSInstrument = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblInstrument), RenaissanceDataClass.DSInstrument)

        If (InstrumentsDS IsNot Nothing) Then
          For Each thisInstrumentRow In InstrumentsDS.tblInstrument.Rows
            If (thisInstrumentRow.InstrumentType = InstrumentTypes.Cash) Then
              CashInstrumentsDictionary.Add(thisInstrumentRow.InstrumentID, thisInstrumentRow.InstrumentCurrencyID)
            End If
          Next
        End If
      Catch ex As Exception
      End Try

      ' Get List of Currencies. Normalises CurrencyIDs down to make SubFundKey generation safer.
      ' Could do this for FundID and SubFundID also if necessary.

      Dim CurrencyIndexDictionary As New Dictionary(Of Integer, Integer)
      Dim CurrencyIDList As New ArrayList ' List of CurrencyIDs ordered by Currency Code, used in building CashGrid Tree
      Dim CurrencyCodeDictionary As New Dictionary(Of Integer, String) ' Just a convenience

      Try
        Dim CurrencysDS As RenaissanceDataClass.DSCurrency = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblCurrency), RenaissanceDataClass.DSCurrency)
        Dim thisCurrencyRow As RenaissanceDataClass.DSCurrency.tblCurrencyRow

        CurrencyIndexDictionary.Add(0, 0)
        CurrencyIDList.Add(0)
        CurrencyCodeDictionary.Add(0, "")

        If (CurrencysDS IsNot Nothing) Then
          For Each thisCurrencyRow In CurrencysDS.tblCurrency.Select("", "CurrencyCode") ' Use Sorted select so that CurrencyList is in CurrencyCode order.
            CurrencyIndexDictionary.Add(thisCurrencyRow.CurrencyID, CurrencyIndexDictionary.Count + 1)
            CurrencyIDList.Add(thisCurrencyRow.CurrencyID)
            CurrencyCodeDictionary.Add(thisCurrencyRow.CurrencyID, thisCurrencyRow.CurrencyCode)
          Next
        End If
      Catch ex As Exception
        MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error building CurrencyDictionary. Could there be a duplicate or Currency ID of Zero?.", ex.Message, ex.StackTrace, True)
        Exit Sub
      End Try

      ' Build SubFundMap, Mark Existing Rows

      For Counter = 0 To (Grid_CashLadder.Rows.Count - 1)
        thisGridRow = Grid_CashLadder.Rows(Counter)

        If (Counter > 0) AndAlso (thisGridRow.IsNode = False) Then
          thisGridRow(col_InUse) = False
        End If

        If (thisGridRow.IsNode) Then
          thisFundID = CInt(thisGridRow(col_FundID))
          thisSubFundID = CInt(thisGridRow(col_SubFundID))
          thisCurrencyIndex = CurrencyIndexDictionary(CInt(thisGridRow(col_CurrencyID)))

          ' Want Sub-Fund nodes, not currency nodes.

          If (thisCurrencyID = 0) Then
            SubFundKey = CULng(thisFundID) Or (CULng(thisSubFundID) << SHIFT_SUB_FUND) Or (CULng(thisCurrencyIndex) << SHIFT_CURRENCYID)

            If (Not gridSubFundMap.ContainsKey(SubFundKey)) Then
              gridSubFundMap.Add(SubFundKey, thisGridRow)
            End If

          End If

        End If ' Isnode

        ' Clear cash values

        If (Counter > 0) Then
          thisGridRow(col_OpeningBalance) = Nothing
          thisGridRow(col_ClosingBalance) = Nothing
          For ColCounter = StartIndex To EndIndex
            thisGridRow(ColCounter + DATE_COLUMN_OFFSET) = Nothing
          Next
        End If

      Next

      ' Initialise Totals Structures.

      Dim SubFundTotalsDictionary As New Dictionary(Of ULong, Dictionary(Of Integer, Double()))
      Dim thisSubFundTotalsDictionary As Dictionary(Of Integer, Double()) = Nothing
      Dim TransactionIDsDictionary As New Dictionary(Of Integer, ULong)

      Dim thisTotalsArray() As Double = Nothing

      Dim DataViewCount As Integer
      Dim thisDataView As DataView
      Dim thisTransactionRow As DataRow = Nothing
      Dim thisTransactionParentID As Integer
      Dim thisInstrumentID As Integer
      Dim tableDateIndex As Integer

      Dim tableFundIDOrdinal As Integer
      Dim tableSubFundIDOrdinal As Integer
      Dim tableInstrumentIDOrdinal As Integer
      Dim tableParentIDOrdinal As Integer
      Dim tableSignedUnitsOrdinal As Integer
      Dim tableTransactionTypeOrdinal As Integer

      LastSubFundKey = 0L

      ' Work through the transactions tables (Std & Contra), building structures to hold the Opening and Delta balance figures for each currency
      ' in each sub fund. Also build lists of what transactions go in each sub fund between the start and end dates.

      For DataViewCount = 0 To 1

        If (DataViewCount = 0) Then
          thisDataView = TransactionDataView
        Else
          thisDataView = ContraTransactionDataView
        End If

        If (UseValueDates) Then
          tableDateIndex = thisDataView.Table.Columns("TransactionValueDate").Ordinal
        Else
          tableDateIndex = thisDataView.Table.Columns("TransactionSettlementDate").Ordinal
        End If

        tableFundIDOrdinal = thisDataView.Table.Columns("TransactionFund").Ordinal
        tableSubFundIDOrdinal = thisDataView.Table.Columns("TransactionSubFund").Ordinal
        tableInstrumentIDOrdinal = thisDataView.Table.Columns("TransactionInstrument").Ordinal
        tableParentIDOrdinal = thisDataView.Table.Columns("TransactionParentID").Ordinal
        tableSignedUnitsOrdinal = thisDataView.Table.Columns("TransactionSignedUnits").Ordinal

        ' For each Transaction....

        For Counter = 0 To (thisDataView.Count - 1) ' 

          thisTransactionRow = thisDataView.Item(Counter).Row

          thisFundID = CInt(thisTransactionRow(tableFundIDOrdinal))
          thisSubFundID = CInt(thisTransactionRow(tableSubFundIDOrdinal))
          If (Not ShowSubFunds) OrElse (thisFundID = thisSubFundID) Then
            thisSubFundID = 0
          End If

          thisTransactionParentID = CInt(thisTransactionRow(tableParentIDOrdinal))

          thisInstrumentID = CInt(thisTransactionRow(tableInstrumentIDOrdinal))

          ' If this Transaction is in a Cash Instrument (Some Leg1, Most Leg2 transactions) get the Currency ID and continue ....

          If (CashInstrumentsDictionary.TryGetValue(thisInstrumentID, thisCurrencyID)) AndAlso (CDbl(thisTransactionRow(tableSignedUnitsOrdinal)) <> 0.0#) Then

            thisCurrencyIndex = CurrencyIndexDictionary(thisCurrencyID)
            SubFundKey = CULng(thisFundID) Or (CULng(thisSubFundID) << SHIFT_SUB_FUND) Or (CULng(thisCurrencyIndex) << SHIFT_CURRENCYID)

            ' Get Totals structures. Check vs LastSubFundKey for performance 

            If (SubFundKey <> LastSubFundKey) Then
              If (Not (SubFundTotalsDictionary.TryGetValue(SubFundKey, thisSubFundTotalsDictionary))) Then
                thisSubFundTotalsDictionary = New Dictionary(Of Integer, Double())
                SubFundTotalsDictionary.Add(SubFundKey, thisSubFundTotalsDictionary)
              End If
            End If

            ' Get Transaction Date

            'If (UseValueDates) Then
            '  thisDate = CDate(thisTransactionRow("TransactionValueDate"))
            'Else
            '  thisDate = CDate(thisTransactionRow("TransactionSettlementDate"))
            'End If

            thisDate = CDate(thisTransactionRow(tableDateIndex))

            ' If Before Start date, then add to Opening balance

            If (thisDate < StartDate) Then
              If (Not thisSubFundTotalsDictionary.TryGetValue(thisCurrencyID, thisTotalsArray)) Then
                thisTotalsArray = Array.CreateInstance(GetType(Double), (EndIndex + 2))
                thisTotalsArray.Initialize()
                thisSubFundTotalsDictionary.Add(thisCurrencyID, thisTotalsArray)
              End If

              thisTotalsArray(0) += CDbl(thisTransactionRow(tableSignedUnitsOrdinal))

            ElseIf (thisDate <= EndDate) Then
              ' If in Date Range, then Add to 'Delta' balance value and save TransactionID

              If (Not thisSubFundTotalsDictionary.TryGetValue(thisCurrencyID, thisTotalsArray)) Then
                thisTotalsArray = Array.CreateInstance(GetType(Double), (EndIndex + 2))
                thisSubFundTotalsDictionary.Add(thisCurrencyID, thisTotalsArray)
              End If

              thisTotalsArray(GetPriceIndex(CashPeriod, StartDate, thisDate) + 1) += CDbl(thisTransactionRow(tableSignedUnitsOrdinal))

              If (Not TransactionIDsDictionary.ContainsKey(thisTransactionParentID)) Then
                TransactionIDsDictionary.Add(thisTransactionParentID, SubFundKey)
              End If
            End If

            LastSubFundKey = SubFundKey

          End If ' Is a cash Instrument

        Next Counter

      Next DataViewCount

      ' Draw Grid

      If (Grid_CashLadder.Rows.Count <= 1) Then
        ' Add Header Line.
        thisGridRow = Grid_CashLadder.Rows.Add()
        thisGridRow(col_FirstCol) = "Primonial"
        thisGridRow.IsNode = True
        thisGridRow.Node.Level = 0
        thisGridRow.AllowEditing = False
        thisGridRow(col_FundID) = 0
        thisGridRow(col_SubFundID) = 0
        If (Not gridSubFundMap.ContainsKey(0)) Then gridSubFundMap.Add(0, thisGridRow)
      End If

      ' Build Outline
      ' Add Fund / Sub Fund Nodes.

      Dim lastGridRow As C1.Win.C1FlexGrid.Row
      Dim SubFundHeirarchyRow As DataRow = Nothing
      Dim ThisLevel As Integer
      Dim ChildRange As C1.Win.C1FlexGrid.CellRange = Nothing
      Dim thisNodeLevel As Integer
      Dim NodeInsertIndex As Integer
      Dim InsertTransactionIndex As Integer
      Dim RowFound As Boolean
      Dim ChildCounter As Integer

      lastGridRow = gridSubFundMap(0)

      For Each SubFundHeirarchyRow In SubFundHeirarchySelection
        thisFundID = CInt(SubFundHeirarchyRow("TopFundID"))
        ThisLevel = CInt(SubFundHeirarchyRow("Level"))

        ' Skip Root, if presented.
        If (ThisLevel <= 0) Then
          Continue For
        End If

        ' SubFundID = 0 for top level funds.
        If (ThisLevel = 1) Then
          thisSubFundID = 0
        Else
          thisSubFundID = CInt(SubFundHeirarchyRow("SubFundID"))
        End If

        If (ThisLevel = 1) OrElse (ShowSubFunds) Then

          ' CurrencyList
          For Counter = 0 To (CurrencyIDList.Count - 1)

            thisCurrencyID = CurrencyIDList(Counter)
            thisCurrencyIndex = CurrencyIndexDictionary(thisCurrencyID)
            thisNodeLevel = ThisLevel + IIf(thisCurrencyID = 0, 0, 1)

            SubFundKey = CULng(thisFundID) Or (CULng(thisSubFundID) << SHIFT_SUB_FUND) Or (CULng(thisCurrencyIndex) << SHIFT_CURRENCYID)

            If (gridSubFundMap.ContainsKey(SubFundKey)) Then
              lastGridRow = gridSubFundMap(SubFundKey)
            Else
              'Check for existing range....
              ChildRange = lastGridRow.Node.GetCellRange
              ChildRange.Normalize()

              If (ChildRange.TopRow = ChildRange.BottomRow) Then ' Empty, Insert
                ' Empty, does not matter

                NodeInsertIndex = lastGridRow.Index + 1

              ElseIf (thisNodeLevel <= lastGridRow.Node.Level) Then
                ' Inserting Sibling or higher

                NodeInsertIndex = ChildRange.BottomRow + 1
              ElseIf (lastGridRow.Node.Children > 0) Then
                ' Inserting a Child node
                ' If this node has children, then we want to insert this new node before the first node child, after any non-node children.
                NodeInsertIndex = lastGridRow.Node.GetNode(NodeTypeEnum.FirstChild).Row.SafeIndex
              Else
                NodeInsertIndex = ChildRange.BottomRow + 1
              End If

              lastGridRow = Grid_CashLadder.Rows.InsertNode(NodeInsertIndex, thisNodeLevel).Row
              lastGridRow.AllowEditing = False
              Grid_CashLadder.SetCellStyle(lastGridRow.Index, col_FirstCol, "NumberPositiveBold")

              gridSubFundMap.Add(SubFundKey, lastGridRow)
            End If

            If (lastGridRow.Node.Level <> thisNodeLevel) Then
              lastGridRow.Node.Level = thisNodeLevel
            End If

            If (thisCurrencyID = 0) Then
              lastGridRow(col_FirstCol) = SubFundHeirarchyRow("SubFundName")
            Else
              lastGridRow(col_FirstCol) = CurrencyCodeDictionary(thisCurrencyID)
            End If

            lastGridRow(col_FundID) = thisFundID
            lastGridRow(col_SubFundID) = thisSubFundID
            lastGridRow(col_CurrencyID) = thisCurrencyID
            lastGridRow(col_InstrumentID) = (0UI)
            lastGridRow(col_DateIndex) = 0
            lastGridRow.Visible = True

          Next Counter

        End If

      Next SubFundHeirarchyRow

      ' Fill Transactions

      For DataViewCount = 0 To 1

        If (DataViewCount = 0) Then
          thisDataView = TransactionDataView
        Else
          thisDataView = ContraTransactionDataView
        End If

        If (UseValueDates) Then
          tableDateIndex = thisDataView.Table.Columns("TransactionValueDate").Ordinal
        Else
          tableDateIndex = thisDataView.Table.Columns("TransactionSettlementDate").Ordinal
        End If

        tableFundIDOrdinal = thisDataView.Table.Columns("TransactionFund").Ordinal
        tableSubFundIDOrdinal = thisDataView.Table.Columns("TransactionSubFund").Ordinal
        tableInstrumentIDOrdinal = thisDataView.Table.Columns("TransactionInstrument").Ordinal
        tableParentIDOrdinal = thisDataView.Table.Columns("TransactionParentID").Ordinal
        tableSignedUnitsOrdinal = thisDataView.Table.Columns("TransactionSignedUnits").Ordinal
        tableTransactionTypeOrdinal = thisDataView.Table.Columns("TransactionType_Contra").Ordinal ' Leg 1 Trans. Type.

        Dim TransactionInstrumentLeg1 As Integer
        LastSubFundKey = 0L

        ' For each Transaction....

        For Counter = 0 To (thisDataView.Count - 1) ' 

          thisTransactionRow = thisDataView.Item(Counter).Row

          thisDate = CDate(thisTransactionRow(tableDateIndex))

          If (thisDate >= StartDate) AndAlso (thisDate <= EndDate) Then

            thisInstrumentID = CInt(thisTransactionRow(tableInstrumentIDOrdinal))

            If (CashInstrumentsDictionary.TryGetValue(thisInstrumentID, thisCurrencyID)) AndAlso (CDbl(thisTransactionRow(tableSignedUnitsOrdinal)) <> 0.0#) Then

              ' OK, this transaction is in Date Range and is a Cash instrument

              thisPriceIndex = GetPriceIndex(CashPeriod, StartDate, thisDate) + DATE_COLUMN_OFFSET

              thisTransactionParentID = CInt(thisTransactionRow(tableParentIDOrdinal))
              thisFundID = CInt(thisTransactionRow(tableFundIDOrdinal))
              thisSubFundID = CInt(thisTransactionRow(tableSubFundIDOrdinal))
              thisTransactionTypeID = CInt(thisTransactionRow(tableTransactionTypeOrdinal))
              If (Not ShowSubFunds) OrElse (thisSubFundID = thisFundID) Then
                thisSubFundID = 0
              End If

              If (DataViewCount = 0) Then
                TransactionInstrumentLeg1 = thisInstrumentID
              Else
                ' Get Leg 1 details

                If (Counter < TransactionDataView.Count) AndAlso (TransactionDataView.Item(Counter).Row("TransactionParentID") = thisTransactionParentID) Then
                  ' Quick
                  ' Transaction and ContraTransaction DataViews should be in step, the transactions should match on an Index by index basis.
                  TransactionInstrumentLeg1 = CInt(TransactionDataView.Item(Counter).Row("TransactionInstrument"))
                Else
                  ' Slow
                  ' Mis-Match between Transactions and Contra transactions dataviews. A problem ?
                  TransactionInstrumentLeg1 = CInt(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblTransaction, thisTransactionParentID, "TransactionInstrument"))
                End If
              End If

              thisCurrencyIndex = CurrencyIndexDictionary(thisCurrencyID)
              SubFundKey = CULng(thisFundID) Or (CULng(thisSubFundID) << SHIFT_SUB_FUND) Or (CULng(thisCurrencyIndex) << SHIFT_CURRENCYID)

              ' Get SubFund Node

              If (SubFundKey <> LastSubFundKey) Then
                If (gridSubFundMap.TryGetValue(SubFundKey, SubFundNode)) Then

                  ' Get Children
                  ChildRange = SubFundNode.Node.GetCellRange
                  ChildRange.Normalize()

                  InsertTransactionIndex = ChildRange.TopRow + 1
                  'If (SubFundNode.Node.Children > 0) Then
                  '  InsertTransactionIndex = SubFundNode.Node.GetNode(NodeTypeEnum.FirstChild).Row.SafeIndex
                  'Else
                  '  InsertTransactionIndex = ChildRange.BottomRow + 1
                  'End If

                Else
                  ' Transaction seems to have appeared that was not there previously.
                  Continue For
                End If
              End If

              ' Find Existing Row if it exists, or set InsertTransactionIndex for after other transaction on or before this date

              RowFound = False
              For ChildCounter = (ChildRange.TopRow + 1) To ChildRange.BottomRow
                thisGridRow = Grid_CashLadder.Rows(ChildCounter)

                If (thisGridRow.IsNode = False) Then
                  If (CInt(thisGridRow(col_TransactionParentID)) = thisTransactionParentID) Then
                    RowFound = True
                    NewGridRow = thisGridRow
                    Exit For
                  ElseIf (CInt(thisGridRow(col_DateIndex)) <= thisPriceIndex) Then
                    InsertTransactionIndex = ChildCounter + 1
                  End If
                Else
                  Exit For
                End If
              Next ChildCounter

              If (RowFound = False) Then
                ' Add New Row if necessary.

                NewGridRow = Grid_CashLadder.Rows.Insert(InsertTransactionIndex)

                NewGridRow(col_FundID) = thisFundID
                NewGridRow(col_SubFundID) = thisSubFundID
                NewGridRow(col_CurrencyID) = thisCurrencyID
                NewGridRow(col_TransactionParentID) = thisTransactionParentID

                Select Case CType(thisTransactionTypeID, TransactionTypes)
                  Case TransactionTypes.BuyFX, TransactionTypes.SellFX
                    NewGridRow(col_FirstCol) = "FX Trade"

                  Case TransactionTypes.BuyFXForward, TransactionTypes.SellFXForward
                    NewGridRow(col_FirstCol) = "Forward FX Trade"

                  Case TransactionTypes.Subscribe
                    NewGridRow(col_FirstCol) = "Subscription"

                  Case TransactionTypes.Redeem
                    NewGridRow(col_FirstCol) = "Redemption"

                  Case Else
                    NewGridRow(col_FirstCol) = CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, TransactionInstrumentLeg1, "InstrumentDescription"))

                End Select
                NewGridRow(col_InstrumentID) = TransactionInstrumentLeg1

                InsertTransactionIndex += 1
              End If

              ' Update Row Details

              If (NewGridRow(col_InstrumentID) <> TransactionInstrumentLeg1) Then
                Select Case CType(thisTransactionTypeID, TransactionTypes)
                  Case TransactionTypes.BuyFX, TransactionTypes.SellFX
                    NewGridRow(col_FirstCol) = "FX Trade"

                  Case TransactionTypes.BuyFXForward, TransactionTypes.SellFXForward
                    NewGridRow(col_FirstCol) = "Forward FX Trade"

                  Case Else
                    NewGridRow(col_FirstCol) = CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, TransactionInstrumentLeg1, "InstrumentDescription"))

                End Select
                NewGridRow(col_InstrumentID) = TransactionInstrumentLeg1
              End If

              NewGridRow(col_InUse) = True
              NewGridRow.Visible = True
 
              NewGridRow(thisPriceIndex) = Nz(NewGridRow(thisPriceIndex), 0.0#) + CDbl(thisTransactionRow(tableSignedUnitsOrdinal))
              SubFundNode(thisPriceIndex) = Nz(SubFundNode(thisPriceIndex), 0.0#) + CDbl(thisTransactionRow(tableSignedUnitsOrdinal))
              NewGridRow(col_DateIndex) = thisPriceIndex ' For Sorting if we want.

              ' Format

              If CDbl(NewGridRow(thisPriceIndex)) < 0.0# Then
                Grid_CashLadder.SetCellStyle(NewGridRow.Index, thisPriceIndex, "NumberNegative")
              Else
                Grid_CashLadder.SetCellStyle(NewGridRow.Index, thisPriceIndex, "NumberPositive")
              End If

              ' Important.

              LastSubFundKey = SubFundKey

            End If ' Cash Instrument

          End If ' Date in Range

        Next Counter

      Next DataViewCount

      ' Clear Unused

      ' Remove obsolete lines

      For Counter = (Grid_CashLadder.Rows.Count - 1) To 2 Step -1
        thisGridRow = Grid_CashLadder.Rows(Counter)

        If (Not thisGridRow.IsNode) AndAlso (thisGridRow(col_InUse) = False) Then
          Grid_CashLadder.RemoveItem(Counter)
        End If
      Next

      ' Set Totals

      If (gridSubFundMap.Count > 0) Then
        Dim SubFundKeys() As ULong = gridSubFundMap.Keys.ToArray()
        Dim RunningTotal As Double

        For Counter = 0 To (SubFundKeys.Length - 1)

          If (gridSubFundMap.TryGetValue(SubFundKeys(Counter), SubFundNode)) Then

            If (CInt(SubFundNode(col_CurrencyID)) > 0) Then
              ' Currency Node

              thisFundID = CInt(SubFundNode(col_FundID))
              thisSubFundID = CInt(SubFundNode(col_SubFundID))
              thisCurrencyID = CInt(SubFundNode(col_CurrencyID))
              thisCurrencyIndex = CurrencyIndexDictionary(thisCurrencyID)

              SubFundKey = CULng(thisFundID) Or (CULng(thisSubFundID) << SHIFT_SUB_FUND) Or (CULng(thisCurrencyIndex) << SHIFT_CURRENCYID)

              ' Get SubFundTotalsDictionary

              If ((SubFundTotalsDictionary.TryGetValue(SubFundKey, thisSubFundTotalsDictionary))) Then
                If (thisSubFundTotalsDictionary.TryGetValue(thisCurrencyID, thisTotalsArray)) Then

                  SubFundNode(col_OpeningBalance) = thisTotalsArray(0)
                  RunningTotal = thisTotalsArray(0)

                  For ColCounter = StartIndex To EndIndex
                    RunningTotal += thisTotalsArray(ColCounter + 1) ' CDbl(Nz(SubFundNode(ColCounter + DATE_COLUMN_OFFSET), 0.0#))

                    If (Check_ShowRunningTotals.Checked) Then
                      SubFundNode(ColCounter + DATE_COLUMN_OFFSET) = RunningTotal
                    Else
                      SubFundNode(ColCounter + DATE_COLUMN_OFFSET) = thisTotalsArray(ColCounter + 1)
                    End If
                  Next

                  SubFundNode(col_ClosingBalance) = RunningTotal

                  ' Formatting 

                  For ColCounter = col_OpeningBalance To col_ClosingBalance
                    If (Grid_CashLadder.Cols(ColCounter).Visible) Then

                      If CDbl(SubFundNode(ColCounter)) < 0.0# Then
                        Grid_CashLadder.SetCellStyle(SubFundNode.Index, ColCounter, "NumberNegativeBold")
                      Else
                        Grid_CashLadder.SetCellStyle(SubFundNode.Index, ColCounter, "NumberPositiveBold")
                      End If

                    End If
                  Next

                End If
              End If ' SubFundTotalsDictionary

            End If

          End If ' gridSubFundMap

        Next

      End If

      Call HideEmptyNodes(Grid_CashLadder, gridSubFundMap(0).Node, col_OpeningBalance)

      ' Remove hidden lines

      For Counter = (Grid_CashLadder.Rows.Count - 1) To 2 Step -1
        thisGridRow = Grid_CashLadder.Rows(Counter)

        If (Not thisGridRow.Visible) Then
          Grid_CashLadder.RemoveItem(Counter)
        End If
      Next

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in SetGrid_CashLadder.", ex.StackTrace, True)
    Finally
      Grid_CashLadder.Redraw = True
    End Try

  End Sub

#End Region

#Region " Button And Grid events."

    ''' <summary>
    ''' Handles the MouseEnterCell event of the Grid_CashLadder control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="C1.Win.C1FlexGrid.RowColEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_CashLadder_MouseEnterCell(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles Grid_CashLadder.MouseEnterCell
    ' *******************************************************************************
    '
    ' *******************************************************************************
    Try
      LastEnteredPositionsGridRow = e.Row
      LastEnteredPositionsGridCol = e.Col

      'If (e.Row <= 0) Then
      '  Grid_CashLadder.ContextMenuStrip = Nothing
      'Else
      '  Grid_CashLadder.ContextMenuStrip = ContextMenu_Positions
      'End If

    Catch ex As Exception
    End Try
  End Sub

    ''' <summary>
    ''' Handles the KeyDown event of the Grid_CashLadder control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_CashLadder_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Grid_CashLadder.KeyDown
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        Try
          If e.Control And (e.KeyCode = Keys.C) Then
            Call MainForm.CopyGridSelection(CType(sender, C1.Win.C1FlexGrid.C1FlexGrid), True, False, True)
          End If
        Catch ex As Exception
        End Try

      End If
    Catch ex As Exception
    End Try
  End Sub

    ''' <summary>
    ''' Hides the empty nodes.
    ''' </summary>
    ''' <param name="thisGrid">The this grid.</param>
    ''' <param name="thisStartingNode">The this starting node.</param>
    ''' <param name="col_ShowIfNotZero">The col_ show if not zero.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function HideEmptyNodes(ByVal thisGrid As C1.Win.C1FlexGrid.C1FlexGrid, ByVal thisStartingNode As C1.Win.C1FlexGrid.Node, ByVal col_ShowIfNotZero As Integer) As Boolean
    ' *****************************************************************************************
    ' Return Index of first child node in the given range, or Index after range if no Nodes.
    '
    ' *****************************************************************************************
    Dim RVal As Boolean = False

    Try
      Dim ChildRange As C1.Win.C1FlexGrid.CellRange
      Dim ChildCounter As Integer
      Dim ThisLevel As Integer
      Dim FoundNode As Boolean = False
      Dim ThisGridRow As C1.Win.C1FlexGrid.Row = Nothing

      If (thisGrid Is Nothing) OrElse (thisStartingNode Is Nothing) Then
        Return True
      End If

      ThisLevel = thisStartingNode.Level

      ChildRange = thisStartingNode.GetCellRange
      ChildRange.Normalize()

      If (col_ShowIfNotZero >= 0) Then
        If (IsNumeric(thisStartingNode.Row(col_ShowIfNotZero))) AndAlso (CDbl(Nz(thisStartingNode.Row(col_ShowIfNotZero), 0.0#)) <> 0.0#) Then
          RVal = True
        End If
      End If

      For ChildCounter = (ChildRange.TopRow + 1) To ChildRange.BottomRow
        ThisGridRow = thisGrid.Rows(ChildCounter)

        If (ThisGridRow.IsNode) Then
          FoundNode = True

          If (ThisGridRow.Node.Level = (ThisLevel + 1)) Then
            ' Recurse

            RVal = RVal Or HideEmptyNodes(thisGrid, ThisGridRow.Node, col_ShowIfNotZero)
          End If
        ElseIf (Not FoundNode) Then
          RVal = True
          ThisGridRow.Visible = True
        End If
      Next

      If (thisStartingNode.Level = 0) Then
        thisStartingNode.Row.Visible = True
      Else
        thisStartingNode.Row.Visible = RVal
      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in HideEmptyNodes().", ex.StackTrace, True)
      Return 0
    End Try

    Return RVal
  End Function

    ''' <summary>
    ''' Handles the DoubleClick event of the Grid_CashLadder control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Grid_CashLadder_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Grid_CashLadder.DoubleClick
    ' ***************************************************************************************
    ' Spawn an Add/Edit Transactions form reflecting the chosen selection of transactions
    ' and pointing to the transaction which was double clicked.
    ' ***************************************************************************************

    Try

      Dim ParentID As Integer
      Dim thisFormHandle As RenaissanceGlobals.RenaissanceFormHandle

      ParentID = CInt(Me.Grid_CashLadder.Rows(Grid_CashLadder.RowSel)("TransactionParentID"))

      If (ParentID > 0) Then
        thisFormHandle = MainForm.New_VeniceForm(VeniceFormID.frmTransaction)
        CType(thisFormHandle.Form, frmTransaction).FormSelectCriteria = "true"
        CType(thisFormHandle.Form, frmTransaction).MoveToAuditID(ParentID)
      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error opening Transactions Form.", ex.StackTrace, True)
    End Try
  End Sub


  Private Sub Button_ForceTransactionsUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_ForceTransactionsUpdate.Click

    Try

      Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID), False)

    Catch ex As Exception
    End Try
  End Sub

#End Region


#Region " Bug hunting "

  ' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
  ' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
  ' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

    ''' <summary>
    ''' Called when [row updating].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

    ''' <summary>
    ''' Called when [row updated].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

    ''' <summary>
    ''' Called when [row fill error].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
    Dim a As Integer

    a = 1
  End Sub

#End Region




End Class
