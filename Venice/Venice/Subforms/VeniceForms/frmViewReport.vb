' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 11-05-2012
' ***********************************************************************
' <copyright file="frmViewReport.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports RenaissanceControls

''' <summary>
''' Class frmViewReport
''' </summary>
Public Class frmViewReport
  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmViewReport"/> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub
    ''' <summary>
    ''' The report preview
    ''' </summary>
	Friend WithEvents ReportPreview As C1.Win.C1Preview.C1PrintPreviewControl

	'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Me.ReportPreview = New C1.Win.C1Preview.C1PrintPreviewControl
		CType(Me.ReportPreview.PreviewPane, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.ReportPreview.SuspendLayout()
		Me.SuspendLayout()
		'
		'ReportPreview
		'
		Me.ReportPreview.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
								Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.ReportPreview.Location = New System.Drawing.Point(1, 0)
		Me.ReportPreview.Name = "ReportPreview"
		'
		'ReportPreview.OutlineView
		'
		Me.ReportPreview.PreviewOutlineView.Dock = System.Windows.Forms.DockStyle.Fill
		Me.ReportPreview.PreviewOutlineView.Location = New System.Drawing.Point(0, 0)
		Me.ReportPreview.PreviewOutlineView.Name = "OutlineView"
		Me.ReportPreview.PreviewOutlineView.Size = New System.Drawing.Size(165, 427)
		Me.ReportPreview.PreviewOutlineView.TabIndex = 0
		'
		'ReportPreview.PreviewPane
		'
		Me.ReportPreview.PreviewPane.IntegrateExternalTools = True
		Me.ReportPreview.PreviewPane.TabIndex = 0
		'
		'ReportPreview.PreviewTextSearchPanel
		'
		Me.ReportPreview.PreviewTextSearchPanel.Dock = System.Windows.Forms.DockStyle.Right
		Me.ReportPreview.PreviewTextSearchPanel.Location = New System.Drawing.Point(550, 0)
		Me.ReportPreview.PreviewTextSearchPanel.MinimumSize = New System.Drawing.Size(180, 240)
		Me.ReportPreview.PreviewTextSearchPanel.Name = "PreviewTextSearchPanel"
		Me.ReportPreview.PreviewTextSearchPanel.Size = New System.Drawing.Size(180, 453)
		Me.ReportPreview.PreviewTextSearchPanel.TabIndex = 0
		Me.ReportPreview.PreviewTextSearchPanel.Visible = False
		'
		'ReportPreview.ThumbnailView
		'
		Me.ReportPreview.PreviewThumbnailView.Dock = System.Windows.Forms.DockStyle.Fill
		Me.ReportPreview.PreviewThumbnailView.HideSelection = False
		Me.ReportPreview.PreviewThumbnailView.Location = New System.Drawing.Point(0, 0)
		Me.ReportPreview.PreviewThumbnailView.Name = "ThumbnailView"
		Me.ReportPreview.PreviewThumbnailView.OwnerDraw = True
		Me.ReportPreview.PreviewThumbnailView.Size = New System.Drawing.Size(165, 680)
		Me.ReportPreview.PreviewThumbnailView.TabIndex = 0
		Me.ReportPreview.PreviewThumbnailView.ThumbnailSize = New System.Drawing.Size(96, 96)
		Me.ReportPreview.PreviewThumbnailView.UseImageAsThumbnail = False
		Me.ReportPreview.Size = New System.Drawing.Size(885, 731)
		Me.ReportPreview.StatusBarVisible = False
		Me.ReportPreview.TabIndex = 0
		Me.ReportPreview.Text = "C1PrintPreviewControl1"
		'
		'frmViewReport
		'
		Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
		Me.ClientSize = New System.Drawing.Size(888, 729)
		Me.Controls.Add(Me.ReportPreview)
		Me.Name = "frmViewReport"
		Me.Text = "Venice Report"
		CType(Me.ReportPreview.PreviewPane, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ReportPreview.ResumeLayout(False)
		Me.ReportPreview.PerformLayout()
		Me.ResumeLayout(False)

	End Sub

#End Region

#Region " Local Variable Declaration"

    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
	Private _MainForm As VeniceMain
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
	Private _FormOpenFailed As Boolean

	' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
	Private FormTooltip As New ToolTip()

#End Region

#Region " Form Properties"

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
	Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
		Get
			Return False
		End Get
		Set(ByVal Value As Boolean)

		End Set
	End Property

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
	Public Sub CloseForm() Implements Globals.StandardVeniceForm.CloseForm
		Me.Close()
	End Sub

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
	Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
		Get
			Return False
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
		Get
			Return True
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
	Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
		Get
			Return _MainForm
		End Get
	End Property

    ''' <summary>
    ''' Resets the form.
    ''' </summary>
	Public Sub ResetForm() Implements Globals.StandardVeniceForm.ResetForm
		ReportPreview.Document = Nothing
	End Sub

    ''' <summary>
    ''' Gets or sets the report document.
    ''' </summary>
    ''' <value>The report document.</value>
	Public Property ReportDocument() As System.Drawing.Printing.PrintDocument
		Get
			Return ReportPreview.Document
		End Get
		Set(ByVal Value As System.Drawing.Printing.PrintDocument)
			ReportPreview.Document = Value
		End Set
	End Property

#End Region

#Region " threadSafe Methods"

    ''' <summary>
    ''' Delegate SetOpacityDelegate
    ''' </summary>
    ''' <param name="OpacityLevel">The opacity level.</param>
	Private Delegate Sub SetOpacityDelegate(ByVal OpacityLevel As Double)

    ''' <summary>
    ''' Sets the opacity.
    ''' </summary>
    ''' <param name="OpacityLevel">The opacity level.</param>
	Public Sub SetOpacity(ByVal OpacityLevel As Double)

		' InvokeRequired required compares the thread ID of the
		' calling thread to the thread ID of the creating thread.
		' If these threads are different, it returns true.

		If Me.InvokeRequired Then
			Dim d As New SetOpacityDelegate(AddressOf SetOpacity)
			Me.Invoke(d, New Object() {OpacityLevel})
		Else
			Me.Opacity = OpacityLevel
		End If
	End Sub

    ''' <summary>
    ''' Delegate SetTextDelegate
    ''' </summary>
    ''' <param name="TextValue">The text value.</param>
	Private Delegate Sub SetTextDelegate(ByVal TextValue As String)

    ''' <summary>
    ''' Sets the text.
    ''' </summary>
    ''' <param name="TextValue">The text value.</param>
	Public Sub SetText(ByVal TextValue As String)
		If Me.InvokeRequired Then
			Dim d As New SetTextDelegate(AddressOf SetText)
			Me.Invoke(d, New Object() {TextValue})
		Else
			Me.Text = TextValue
		End If
	End Sub

    ''' <summary>
    ''' Delegate SetCursorDelegate
    ''' </summary>
    ''' <param name="CursorValue">The cursor value.</param>
	Private Delegate Sub SetCursorDelegate(ByVal CursorValue As Cursor)

    ''' <summary>
    ''' Sets the cursor.
    ''' </summary>
    ''' <param name="CursorValue">The cursor value.</param>
	Public Sub SetCursor(ByVal CursorValue As Cursor)
		If Me.InvokeRequired Then
			Dim d As New SetCursorDelegate(AddressOf SetCursor)
			Me.Invoke(d, New Object() {CursorValue})
		Else
			Me.Cursor = CursorValue
		End If
	End Sub

	'SetDocument
    ''' <summary>
    ''' Delegate SetDocumentDelegate
    ''' </summary>
    ''' <param name="DocumentValue">The document value.</param>
	Private Delegate Sub SetDocumentDelegate(ByVal DocumentValue As System.Drawing.Printing.PrintDocument)

    ''' <summary>
    ''' Sets the document.
    ''' </summary>
    ''' <param name="DocumentValue">The document value.</param>
	Public Sub SetDocument(ByVal DocumentValue As System.Drawing.Printing.PrintDocument)
		If (Me.Created) AndAlso (Not Me.IsDisposed) Then
			If Me.IsHandleCreated = False Then
				Me.CreateControl()
			End If

			If Me.InvokeRequired Then
				Dim d As New SetDocumentDelegate(AddressOf SetDocument)
				Me.Invoke(d, New Object() {DocumentValue})
			Else
				Me.ReportPreview.Document = DocumentValue
			End If
		End If
	End Sub

    ''' <summary>
    ''' Delegate ShowDelegate
    ''' </summary>
	Private Delegate Sub ShowDelegate()

    ''' <summary>
    ''' Displays the control to the user.
    ''' </summary>
    ''' <PermissionSet>
    '''   <IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
    '''   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
    '''   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode, ControlEvidence" />
    '''   <IPermission class="System.Diagnostics.PerformanceCounterPermission, System, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
    '''   </PermissionSet>
	Public Shadows Sub Show()
		If Me.IsHandleCreated = False Then
			Me.CreateControl()
		End If

		If Me.InvokeRequired Then
			Dim d As New ShowDelegate(AddressOf Show)
			Me.Invoke(d, New Object() {})
		Else
			MyBase.Show()
		End If
	End Sub

    ''' <summary>
    ''' Delegate CreateControlDelegate
    ''' </summary>
	Private Delegate Sub CreateControlDelegate()

    ''' <summary>
    ''' Forces the creation of the visible control, including the creation of the handle and any visible child controls.
    ''' </summary>
    ''' <PermissionSet>
    '''   <IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
    '''   <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
    '''   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode, ControlEvidence" />
    '''   <IPermission class="System.Diagnostics.PerformanceCounterPermission, System, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true" />
    '''   </PermissionSet>
	Public Shadows Sub CreateControl()
		If Me.IsHandleCreated = False Then
			Dim d As New CreateControlDelegate(AddressOf RunBaseCreateControl)
			Me.Invoke(d, New Object() {})
		End If
	End Sub

    ''' <summary>
    ''' Runs the base create control.
    ''' </summary>
	Private Sub RunBaseCreateControl()
		Me.Visible = True
		MyBase.CreateControl()
	End Sub

#End Region


    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmViewReport"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
	Public Sub New(ByRef pMainForm As VeniceMain)
		Me.New()

		_MainForm = pMainForm
		_FormOpenFailed = False

		'Me.ReportPreview.PreviewProgressBar.Dispose()
		'Me.ReportPreview.PreviewProgressBar = Nothing
		'Me.ReportPreview.PreviewProgressBar = New RenaissanceControls.ProgressBar

		Me.ReportPreview.PreviewProgressBar.Minimum = 0
		Me.ReportPreview.PreviewProgressBar.Maximum = Integer.MaxValue
		Me.ReportPreview.PreviewProgressBar.Step = 1

		' Set up the ToolTip
		' MainForm.SetFormToolTip(Me, FormTooltip)

	End Sub

    ''' <summary>
    ''' Handles the Closing event of the frmViewReport control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
	Private Sub frmViewReport_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing

		Try
			If (ReportPreview.PreviewPane.Busy) Then
				e.Cancel = True
				Exit Sub
			End If

			MainForm.RemoveFromFormsCollection(Me)
		Catch ex As Exception
		End Try

	End Sub

    ''' <summary>
    ''' Handles the FormClosing event of the frmViewReport control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.FormClosingEventArgs"/> instance containing the event data.</param>
	Private Sub frmViewReport_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

	End Sub
End Class
