﻿' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 01-29-2013
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmSelectFuturesNotional.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals

Imports RenaissanceDataClass
Imports C1.Win.C1FlexGrid

''' <summary>
''' Class frmSelectFuturesNotional
''' </summary>
Public Class frmSelectFuturesNotional

	Inherits System.Windows.Forms.Form
	Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmSelectFuturesNotional"/> class from being created.
    ''' </summary>
	Private Sub New()
		MyBase.New()

		'This call is required by the Windows Form Designer.
		InitializeComponent()

		'Add any initialization after the InitializeComponent() call

	End Sub

	'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
    ''' <summary>
    ''' The combo_ notional fund
    ''' </summary>
	Friend WithEvents Combo_NotionalFund As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label_ cpty is fund
    ''' </summary>
	Friend WithEvents label_CptyIsFund As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ futures instrument
    ''' </summary>
	Friend WithEvents Combo_FuturesInstrument As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label2
    ''' </summary>
	Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label3
    ''' </summary>
	Friend WithEvents Label3 As System.Windows.Forms.Label
    ''' <summary>
    ''' The date_ value date
    ''' </summary>
	Friend WithEvents Date_ValueDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The grid_ transactions
    ''' </summary>
	Friend WithEvents Grid_Transactions As C1.Win.C1FlexGrid.C1FlexGrid
    ''' <summary>
    ''' The root menu
    ''' </summary>
	Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
    ''' <summary>
    ''' The form_ status strip
    ''' </summary>
	Friend WithEvents Form_StatusStrip As System.Windows.Forms.StatusStrip
    ''' <summary>
    ''' The form_ progress bar
    ''' </summary>
	Friend WithEvents Form_ProgressBar As System.Windows.Forms.ToolStripProgressBar
    ''' <summary>
    ''' The button_ trade selected realised
    ''' </summary>
	Friend WithEvents Button_TradeSelectedRealised As System.Windows.Forms.Button
    ''' <summary>
    ''' The button_ trade selected un realised
    ''' </summary>
	Friend WithEvents Button_TradeSelectedUnRealised As System.Windows.Forms.Button
    ''' <summary>
    ''' The combo_ un realised calculation type
    ''' </summary>
	Friend WithEvents Combo_UnRealisedCalculationType As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label1
    ''' </summary>
	Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ transaction group
    ''' </summary>
	Friend WithEvents Combo_TransactionGroup As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label8
    ''' </summary>
	Friend WithEvents Label8 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ status
    ''' </summary>
	Friend WithEvents Label_Status As System.Windows.Forms.ToolStripStatusLabel
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Me.Grid_Transactions = New C1.Win.C1FlexGrid.C1FlexGrid
		Me.Combo_NotionalFund = New System.Windows.Forms.ComboBox
		Me.label_CptyIsFund = New System.Windows.Forms.Label
		Me.Combo_FuturesInstrument = New System.Windows.Forms.ComboBox
		Me.Label2 = New System.Windows.Forms.Label
		Me.Label3 = New System.Windows.Forms.Label
		Me.Date_ValueDate = New System.Windows.Forms.DateTimePicker
		Me.RootMenu = New System.Windows.Forms.MenuStrip
		Me.Form_StatusStrip = New System.Windows.Forms.StatusStrip
		Me.Form_ProgressBar = New System.Windows.Forms.ToolStripProgressBar
		Me.Label_Status = New System.Windows.Forms.ToolStripStatusLabel
		Me.Button_TradeSelectedRealised = New System.Windows.Forms.Button
		Me.Button_TradeSelectedUnRealised = New System.Windows.Forms.Button
		Me.Combo_UnRealisedCalculationType = New System.Windows.Forms.ComboBox
		Me.Label1 = New System.Windows.Forms.Label
		Me.Combo_TransactionGroup = New System.Windows.Forms.ComboBox
		Me.Label8 = New System.Windows.Forms.Label
		CType(Me.Grid_Transactions, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.Form_StatusStrip.SuspendLayout()
		Me.SuspendLayout()
		'
		'Grid_Transactions
		'
		Me.Grid_Transactions.AllowEditing = False
		Me.Grid_Transactions.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
								Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Grid_Transactions.AutoClipboard = True
		Me.Grid_Transactions.CausesValidation = False
		Me.Grid_Transactions.ColumnInfo = "5,1,0,0,0,85,Columns:"
		Me.Grid_Transactions.EditOptions = C1.Win.C1FlexGrid.EditFlags.None
		Me.Grid_Transactions.KeyActionTab = C1.Win.C1FlexGrid.KeyActionEnum.MoveAcross
		Me.Grid_Transactions.Location = New System.Drawing.Point(4, 218)
		Me.Grid_Transactions.Name = "Grid_Transactions"
		Me.Grid_Transactions.Rows.DefaultSize = 17
		Me.Grid_Transactions.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.ListBox
		Me.Grid_Transactions.Size = New System.Drawing.Size(1033, 307)
		Me.Grid_Transactions.TabIndex = 7
		'
		'Combo_NotionalFund
		'
		Me.Combo_NotionalFund.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_NotionalFund.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_NotionalFund.Location = New System.Drawing.Point(151, 54)
		Me.Combo_NotionalFund.Name = "Combo_NotionalFund"
		Me.Combo_NotionalFund.Size = New System.Drawing.Size(882, 21)
		Me.Combo_NotionalFund.TabIndex = 1
		'
		'label_CptyIsFund
		'
		Me.label_CptyIsFund.Location = New System.Drawing.Point(11, 57)
		Me.label_CptyIsFund.Name = "label_CptyIsFund"
		Me.label_CptyIsFund.Size = New System.Drawing.Size(104, 16)
		Me.label_CptyIsFund.TabIndex = 82
		Me.label_CptyIsFund.Text = "Fund"
		'
		'Combo_FuturesInstrument
		'
		Me.Combo_FuturesInstrument.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_FuturesInstrument.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_FuturesInstrument.Location = New System.Drawing.Point(151, 82)
		Me.Combo_FuturesInstrument.Name = "Combo_FuturesInstrument"
		Me.Combo_FuturesInstrument.Size = New System.Drawing.Size(882, 21)
		Me.Combo_FuturesInstrument.TabIndex = 2
		'
		'Label2
		'
		Me.Label2.Location = New System.Drawing.Point(11, 86)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(104, 16)
		Me.Label2.TabIndex = 84
		Me.Label2.Text = "Instrument"
		'
		'Label3
		'
		Me.Label3.Location = New System.Drawing.Point(11, 32)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(104, 16)
		Me.Label3.TabIndex = 88
		Me.Label3.Text = "ValueDate"
		'
		'Date_ValueDate
		'
		Me.Date_ValueDate.Location = New System.Drawing.Point(151, 28)
		Me.Date_ValueDate.Name = "Date_ValueDate"
		Me.Date_ValueDate.Size = New System.Drawing.Size(227, 20)
		Me.Date_ValueDate.TabIndex = 0
		'
		'RootMenu
		'
		Me.RootMenu.Location = New System.Drawing.Point(0, 0)
		Me.RootMenu.Name = "RootMenu"
		Me.RootMenu.Size = New System.Drawing.Size(1041, 24)
		Me.RootMenu.TabIndex = 104
		Me.RootMenu.Text = " "
		'
		'Form_StatusStrip
		'
		Me.Form_StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Form_ProgressBar, Me.Label_Status})
		Me.Form_StatusStrip.Location = New System.Drawing.Point(0, 530)
		Me.Form_StatusStrip.Name = "Form_StatusStrip"
		Me.Form_StatusStrip.Size = New System.Drawing.Size(1041, 22)
		Me.Form_StatusStrip.TabIndex = 105
		Me.Form_StatusStrip.Text = " "
		'
		'Form_ProgressBar
		'
		Me.Form_ProgressBar.Maximum = 20
		Me.Form_ProgressBar.Name = "Form_ProgressBar"
		Me.Form_ProgressBar.Size = New System.Drawing.Size(150, 17)
		Me.Form_ProgressBar.Step = 1
		Me.Form_ProgressBar.Visible = False
		'
		'Label_Status
		'
		Me.Label_Status.Name = "Label_Status"
		Me.Label_Status.Size = New System.Drawing.Size(10, 17)
		Me.Label_Status.Text = " "
		'
		'Button_TradeSelectedRealised
		'
		Me.Button_TradeSelectedRealised.Location = New System.Drawing.Point(151, 175)
		Me.Button_TradeSelectedRealised.Name = "Button_TradeSelectedRealised"
		Me.Button_TradeSelectedRealised.Size = New System.Drawing.Size(250, 24)
		Me.Button_TradeSelectedRealised.TabIndex = 5
		Me.Button_TradeSelectedRealised.Text = "Zero Selected Notional (Realised P&&L)"
		Me.Button_TradeSelectedRealised.UseVisualStyleBackColor = True
		'
		'Button_TradeSelectedUnRealised
		'
		Me.Button_TradeSelectedUnRealised.Location = New System.Drawing.Point(443, 175)
		Me.Button_TradeSelectedUnRealised.Name = "Button_TradeSelectedUnRealised"
		Me.Button_TradeSelectedUnRealised.Size = New System.Drawing.Size(250, 24)
		Me.Button_TradeSelectedUnRealised.TabIndex = 6
		Me.Button_TradeSelectedUnRealised.Text = "Zero Selected Notional (Un-Realised P&&L)"
		Me.Button_TradeSelectedUnRealised.UseVisualStyleBackColor = True
		'
		'Combo_UnRealisedCalculationType
		'
		Me.Combo_UnRealisedCalculationType.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_UnRealisedCalculationType.Location = New System.Drawing.Point(151, 136)
		Me.Combo_UnRealisedCalculationType.Name = "Combo_UnRealisedCalculationType"
		Me.Combo_UnRealisedCalculationType.Size = New System.Drawing.Size(229, 21)
		Me.Combo_UnRealisedCalculationType.TabIndex = 4
		'
		'Label1
		'
		Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label1.Location = New System.Drawing.Point(11, 141)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(126, 16)
		Me.Label1.TabIndex = 143
		Me.Label1.Text = "Unrealised P&L calculation"
		'
		'Combo_TransactionGroup
		'
		Me.Combo_TransactionGroup.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_TransactionGroup.Location = New System.Drawing.Point(151, 109)
		Me.Combo_TransactionGroup.Name = "Combo_TransactionGroup"
		Me.Combo_TransactionGroup.Size = New System.Drawing.Size(229, 21)
		Me.Combo_TransactionGroup.TabIndex = 3
		'
		'Label8
		'
		Me.Label8.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label8.Location = New System.Drawing.Point(11, 114)
		Me.Label8.Name = "Label8"
		Me.Label8.Size = New System.Drawing.Size(126, 16)
		Me.Label8.TabIndex = 141
		Me.Label8.Text = "Transaction Group"
		'
		'frmSelectFuturesNotional
		'
		Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
		Me.ClientSize = New System.Drawing.Size(1041, 552)
		Me.Controls.Add(Me.Combo_UnRealisedCalculationType)
		Me.Controls.Add(Me.Label1)
		Me.Controls.Add(Me.Combo_TransactionGroup)
		Me.Controls.Add(Me.Label8)
		Me.Controls.Add(Me.Button_TradeSelectedUnRealised)
		Me.Controls.Add(Me.Button_TradeSelectedRealised)
		Me.Controls.Add(Me.Form_StatusStrip)
		Me.Controls.Add(Me.RootMenu)
		Me.Controls.Add(Me.Date_ValueDate)
		Me.Controls.Add(Me.Label3)
		Me.Controls.Add(Me.Combo_FuturesInstrument)
		Me.Controls.Add(Me.Label2)
		Me.Controls.Add(Me.Combo_NotionalFund)
		Me.Controls.Add(Me.label_CptyIsFund)
		Me.Controls.Add(Me.Grid_Transactions)
		Me.Name = "frmSelectFuturesNotional"
		Me.Text = "Sweep Futures P&L"
		CType(Me.Grid_Transactions, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Form_StatusStrip.ResumeLayout(False)
		Me.Form_StatusStrip.PerformLayout()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

#End Region

#Region " Form Locals and Constants "

	' Form 'Parent', the Main Venice form.
	' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
	Private WithEvents _MainForm As VeniceMain

	' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
	Private FormTooltip As New ToolTip()

	' Form Menu


	' Form Constants, specific to the table being updated.

    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
	Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

	' Form Locals, initialised on 'New' defining what standard data items to use
    ''' <summary>
  ''' Variable holding the form's data table name. taken from the "ThisStandardDataset" object.
    ''' </summary>
	Private THIS_TABLENAME As String
    ''' <summary>
  ''' Variable holding the form's data adaptor name. taken from the "ThisStandardDataset" object.
    ''' </summary>
	Private THIS_ADAPTORNAME As String
    ''' <summary>
  ''' Variable holding the form's dataset name. taken from the "ThisStandardDataset" object.
    ''' </summary>
	Private THIS_DATASETNAME As String


	' The standard ChangeID for this form. e.g. tblFund
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
	Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

	' Form Specific Order fields
    ''' <summary>
  ''' Field Name to order the Select combo by.
    ''' </summary>
	Private THIS_FORM_OrderBy As String

	' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
	Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
	Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

	' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
	Private THIS_FORM_FormID As VeniceFormID

	' Data Structures

    ''' <summary>
    ''' My dataset
    ''' </summary>
	Private myDataset As DSSelectFuturesNotional
    ''' <summary>
    ''' My table
    ''' </summary>
	Private myTable As DSSelectFuturesNotional.tblSelectFuturesNotionalDataTable
    ''' <summary>
    ''' My connection
    ''' </summary>
	Private myConnection As SqlConnection
    ''' <summary>
    ''' My adaptor
    ''' </summary>
	Private myAdaptor As New SqlDataAdapter
    ''' <summary>
    ''' My data view
    ''' </summary>
	Private myDataView As DataView

    ''' <summary>
    ''' The this standard dataset
    ''' </summary>
	Private ThisStandardDataset As RenaissanceGlobals.StandardDataset

	' Active Element.

    ''' <summary>
    ''' The __ is over cancel button
    ''' </summary>
	Private __IsOverCancelButton As Boolean
    ''' <summary>
    ''' The _ in use
    ''' </summary>
	Private _InUse As Boolean

	' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
	Private FormIsValid As Boolean
    ''' <summary>
    ''' The in paint
    ''' </summary>
	Private InPaint As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
	Private _FormOpenFailed As Boolean

	' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
	Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
	Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
	Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
	Private HasDeletePermission As Boolean
    ''' <summary>
    ''' The has transaction insert permission
    ''' </summary>
	Private HasTransactionInsertPermission As Boolean

	' Futures-Type InstrumentTypes

    ''' <summary>
    ''' The _ futures instrument types
    ''' </summary>
	Private _FuturesInstrumentTypes As String = ""

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
	Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
	Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return __IsOverCancelButton
		End Get
		Set(ByVal Value As Boolean)
			__IsOverCancelButton = Value
		End Set
	End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
	Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
		Get
			Return InPaint
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
		Get
			Return _InUse
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmSelectFuturesNotional"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
	Public Sub New(ByVal pMainForm As VeniceMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()


		_MainForm = pMainForm
		AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

		_FormOpenFailed = False
		_InUse = True

		' Default Select and Order fields.

		THIS_FORM_OrderBy = "FundID, InstrumentDescription"

		' Form Permissioning :-

		THIS_FORM_PermissionArea = Me.Name
		THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

		' 'This' form ID

		THIS_FORM_FormID = VeniceFormID.frmSelectFuturesNotional

		' This form's dataset type.

		ThisStandardDataset = RenaissanceStandardDatasets.tblSelectFuturesNotional ' This Defines the Form Data !!! 


		' Data object names standard to this Form type.

		THIS_TABLENAME = ThisStandardDataset.TableName
		THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
		THIS_DATASETNAME = ThisStandardDataset.DatasetName

		THIS_FORM_ChangeID = ThisStandardDataset.ChangeID


		' Establish / Retrieve data objects for this form.

		myConnection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)
		Call MainForm.MainAdaptorHandler.Set_AdaptorCommands(myConnection, myAdaptor, THIS_TABLENAME)
		myDataset = New DSSelectFuturesNotional
		myTable = myDataset.tblSelectFuturesNotional

		' Report
		SetTransactionReportMenu(RootMenu)

		' Grid Fields
		Dim FieldsMenu As ToolStripMenuItem = SetFieldSelectMenu(RootMenu)

		' Establish initial DataView and Initialise Transactions Grig.

		Dim thisCol As C1.Win.C1FlexGrid.Column
		Dim Counter As Integer

		'
		Dim NewStyle As C1.Win.C1FlexGrid.CellStyle

		' NewStyle = Grid_PortfolioData.Styles.Add("VeniceInstruments", Grid_PortfolioData.GetCellStyle(0, Grid_Instrument))
		NewStyle = Grid_Transactions.Styles.Add("DataNegative", Grid_Transactions.Styles.Normal)
		NewStyle.ForeColor = Color.Red
		'

		Try
			myDataView = New DataView(myTable, "True", "", DataViewRowState.CurrentRows)

			Grid_Transactions.DataSource = myDataView

			' Format Transactions Grid, hide unwanted columns.

			Grid_Transactions.Cols("FundName").Move(1)
			Grid_Transactions.Cols("FuturesInstrumentDescription").Move(2)
			Grid_Transactions.Cols("FuturesInstrumentDescription").Caption = "Futures Instrument"
			Grid_Transactions.Cols("FuturesCurrencyDescription").Move(3)
			Grid_Transactions.Cols("FuturesCurrencyDescription").Caption = "Futures Ccy"
			Grid_Transactions.Cols("OpenPosition").Move(4)
			Grid_Transactions.Cols("BookedNotional").Move(5)
			Grid_Transactions.Cols("Local_Value").Move(6)
			Grid_Transactions.Cols("Local_Value").Caption = "Value (Futures Ccy)"
			Grid_Transactions.Cols("Local_UnrealisedProfit").Move(7)
			Grid_Transactions.Cols("Local_UnrealisedProfit").Caption = "Unrealised Profit (Futures CCy)"
			Grid_Transactions.Cols("UnrealisedProfit").Move(8)
			Grid_Transactions.Cols("UnrealisedProfit").Caption = "Unrealised Profit (Fund CCy)"
			Grid_Transactions.Cols("NotionalTradeToZeroRealisedPnL").Move(9)
			Grid_Transactions.Cols("NotionalTradeToZeroRealisedPnL").Caption = "Trade To Zero Realised PnL"
			Grid_Transactions.Cols("NotionalTradeToZeroUnRealisedPnL").Move(10)
			Grid_Transactions.Cols("NotionalTradeToZeroUnRealisedPnL").Caption = "Trade To Zero UnRealised PnL"
		Catch ex As Exception
		End Try

		For Each thisCol In Grid_Transactions.Cols
			Try
				Select Case thisCol.Name
					Case "FundName"
					Case "FuturesInstrumentDescription"
					Case "FuturesCurrencyDescription"
					Case "OpenPosition"
						thisCol.Format = "#,##0.00"
					Case "BookedNotional"
						thisCol.Format = "#,##0.00"
					Case "Value"
						thisCol.Format = "#,##0.00"
					Case "UnrealisedProfit"
						thisCol.Format = "#,##0.00"
					Case "NotionalTradeToZeroRealisedPnL"
						thisCol.Format = "#,##0.00"
					Case "NotionalTradeToZeroUnRealisedPnL"
						thisCol.Format = "#,##0.00"
					Case "FundFXRate"
						thisCol.Format = "#,##0.00000"
						thisCol.Visible = False

					Case "FuturesInstrumentFXRate"
						thisCol.Format = "#,##0.00000"
						thisCol.Visible = False

					Case "CompoundFXRate"
						thisCol.Format = "#,##0.00000"
						thisCol.Visible = False

					Case Else
						thisCol.Visible = False

						Try
							If thisCol.DataType Is GetType(Date) Then
								thisCol.Format = DISPLAYMEMBER_DATEFORMAT
							ElseIf thisCol.DataType Is GetType(Integer) Then
								thisCol.Format = "#,##0"
							ElseIf thisCol.DataType Is GetType(Double) Then
								thisCol.Format = "#,##0.00"
							End If
						Catch ex As Exception
						End Try
				End Select

				If (thisCol.Caption.ToUpper.StartsWith("TRANSACTION")) Then
					thisCol.Caption = thisCol.Caption.Substring(11)
				End If

				If (thisCol.Visible) Then
					Try
						CType(FieldsMenu.DropDownItems("Menu_GridField_" & thisCol.Name), ToolStripMenuItem).Checked = True
					Catch ex As Exception
					End Try
				End If
			Catch ex As Exception
			End Try
		Next

		Try
			For Counter = 0 To (Grid_Transactions.Cols.Count - 1)
				If (Grid_Transactions.Cols(Counter).Caption.ToUpper.StartsWith("TRANSACTION")) Then
					Grid_Transactions.Cols(Counter).Caption = Grid_Transactions.Cols(Counter).Caption.Substring(11)
				End If
			Next
		Catch ex As Exception
		End Try

		' Form Control Changed events
		AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

		AddHandler Combo_NotionalFund.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_FuturesInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Date_ValueDate.ValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_TransactionGroup.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_UnRealisedCalculationType.SelectedValueChanged, AddressOf Me.FormControlChanged

		AddHandler Combo_NotionalFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_TransactionGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_UnRealisedCalculationType.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_FuturesInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		AddHandler Combo_NotionalFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_TransactionGroup.KeyUp, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_UnRealisedCalculationType.KeyUp, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_FuturesInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

		AddHandler Combo_NotionalFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_TransactionGroup.KeyUp, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_UnRealisedCalculationType.KeyUp, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_FuturesInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

		AddHandler Combo_NotionalFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_TransactionGroup.KeyUp, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_UnRealisedCalculationType.KeyUp, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_FuturesInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

	End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

	' Form Initialisation code.
	'
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
	Public Sub ResetForm() Implements StandardVeniceForm.ResetForm
		THIS_FORM_OrderBy = "FundID, InstrumentDescription"

		Call Form_Load(Me, New System.EventArgs)
	End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
	Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
		_FormOpenFailed = False
		_InUse = True

		' Initialise Data structures. Connection, Adaptor and Dataset.

		If Not (MainForm Is Nothing) Then
			FormIsValid = True
		Else
			MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myConnection Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myAdaptor Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myDataset Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If


		' Check User permissions
		Try
			Call CheckPermissions()
			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

				FormIsValid = False
				_FormOpenFailed = True

				Exit Sub
			End If
		Catch ex As Exception
			FormIsValid = False
			_FormOpenFailed = True

			Exit Sub
		End Try

		' Initialse form

		InPaint = True
		IsOverCancelButton = False

		' Initialise main select controls
		' Build Sorted data list from which this form operates

		Try
			Call SetFundCombo()
			Call SetInstrumentCombo()
			Call SetTransactionGroupCombo()
			Call SetUnRealisedCalculationTypeCombo()

			Me.Combo_NotionalFund.SelectedIndex = -1
			Me.Combo_FuturesInstrument.SelectedIndex = -1
			Me.Date_ValueDate.Value = RenaissanceUtilities.DatePeriodFunctions.AddPeriodToDate(DealingPeriod.Daily, Now.Date, -1)
			Me.Combo_UnRealisedCalculationType.SelectedValue = CalculationTypes_Profit.FIFO
			Me.Combo_TransactionGroup.Text = DEFAULT_STATUSGROUPFILTER_EOD

			Call SetSortedRows(True)

			Call MainForm.SetComboSelectionLengths(Me)
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error initialising Select Transaction Form.", ex.StackTrace, True)
		End Try

		InPaint = False

	End Sub

    ''' <summary>
    ''' Handles the Closing event of the frm control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
	Private Sub frm_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		Dim HideForm As Boolean

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...

		_InUse = False

		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else
			HideForm = True
			If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			e.Cancel = True
		Else
			Try
				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

				RemoveHandler Combo_NotionalFund.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_FuturesInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Date_ValueDate.ValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_TransactionGroup.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_UnRealisedCalculationType.SelectedValueChanged, AddressOf Me.FormControlChanged

				RemoveHandler Combo_NotionalFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_TransactionGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_UnRealisedCalculationType.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_FuturesInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				RemoveHandler Combo_NotionalFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_TransactionGroup.KeyUp, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_UnRealisedCalculationType.KeyUp, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_FuturesInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

				RemoveHandler Combo_NotionalFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_TransactionGroup.KeyUp, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_UnRealisedCalculationType.KeyUp, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_FuturesInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

				RemoveHandler Combo_NotionalFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_TransactionGroup.KeyUp, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_UnRealisedCalculationType.KeyUp, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_FuturesInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

			Catch ex As Exception
			End Try
		End If

	End Sub


#End Region

	' Routine to handle changes / updates to tables by this and other windows.
	' If this, or any other, form posts a change to a table, then it will invoke an update event 
	' detailing what tables have been altered.
	' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
	' the 'VeniceAutoUpdate' event of the main Venice form.
	' Each form may them react as appropriate to changes in any table that might impact it.
	'
    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		Dim OrgInPaint As Boolean
		Dim KnowledgeDateChanged As Boolean
		Dim RefreshGrid As Boolean

		If (Not Me.Created) OrElse (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then
			Exit Sub
		End If

		OrgInPaint = InPaint

		Try
			InPaint = True
			KnowledgeDateChanged = False
			RefreshGrid = False

			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
				KnowledgeDateChanged = True
			End If
		Catch ex As Exception
		End Try

		' ****************************************************************
		' Check for changes relevant to this form
		' ****************************************************************


		' Changes to the tblFund table :-
		Try
			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFund) = True) Or KnowledgeDateChanged Then
				Call SetFundCombo()
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblFund", ex.StackTrace, True)
		End Try

		' Changes to the tblInstrumentType table :-
		Try
			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrumentType) = True) Or KnowledgeDateChanged Then
				_FuturesInstrumentTypes = ""
				Call SetInstrumentCombo()
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblInstrumentType", ex.StackTrace, True)
		End Try

		' Changes to the tblInstrument table :-
		Try
			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrument) = True) Or KnowledgeDateChanged Then
				Call SetInstrumentCombo()
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblInstrument", ex.StackTrace, True)
		End Try

		' Changes to the tblTransaction table :-
		Try
			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblTransaction) = True) Or KnowledgeDateChanged Then
				RefreshGrid = True
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblTransaction", ex.StackTrace, True)
		End Try

		' Changes to the tblSophisStatusGroups table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblSophisStatusGroups) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			Call SetTransactionGroupCombo()
		End If

		' Changes to the KnowledgeDate :-
		Try
			If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
				RefreshGrid = True
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: KnowledgeDate", ex.StackTrace, True)
		End Try

		' Changes to the tblUserPermissions table :-

		Try
			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

				' Check ongoing permissions.

				Call CheckPermissions()
				If (HasReadPermission = False) Then
					Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

					FormIsValid = False
					InPaint = OrgInPaint
					Me.Close()
					Exit Sub
				End If

				RefreshGrid = True

			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: tblUserPermissions", ex.StackTrace, True)
		End Try


		' ****************************************************************
		' Changes to the Main FORM table :-
		' (or the tblTransactions table)
		' ****************************************************************

		Try
			If (e.TableChanged(THIS_FORM_ChangeID) = True) Or _
			 (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblTransaction) = True) Or _
			 (RefreshGrid = True) Or _
			 KnowledgeDateChanged Then

				' Re-Set grid etc.
				Call SetSortedRows(True)

			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: Main Form table", ex.StackTrace, True)
		End Try

		InPaint = OrgInPaint

	End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

    ''' <summary>
    ''' Gets the transaction select string.
    ''' </summary>
    ''' <param name="OnlyUsertblTransactionFields">if set to <c>true</c> [only usertbl transaction fields].</param>
    ''' <returns>System.String.</returns>
	Private Function GetTransactionSelectString(Optional ByVal OnlyUsertblTransactionFields As Boolean = False) As String
		' *******************************************************************************
		' Build a Select String appropriate to the form status.
		' *******************************************************************************

		Dim SelectString As String
		Dim FieldSelectString As String

		SelectString = ""
		FieldSelectString = ""
		Me.MainForm.SetToolStripText(Label_Status, "")

		Try

			' Select on Fund...

			If (Me.Combo_NotionalFund.SelectedIndex > 0) Then
				If IsFieldNotionalField("FundID") = True Then
					SelectString = "(FundID=" & Combo_NotionalFund.SelectedValue.ToString & ")"
				End If
			End If

			' Select on Instrument

			If (Me.Combo_FuturesInstrument.SelectedIndex > 0) Then
				If IsFieldNotionalField("FuturesInstrumentID") = True Then
					If (SelectString.Length > 0) Then
						SelectString &= " AND "
					End If
					SelectString &= "(FuturesInstrumentID=" & Combo_FuturesInstrument.SelectedValue.ToString & ")"
				End If
			End If

			If SelectString.Length <= 0 Then
				SelectString = "true"
			End If

		Catch ex As Exception
			SelectString = "true"
		End Try

		Return SelectString

	End Function

    ''' <summary>
    ''' Determines whether [is field notional field] [the specified field name].
    ''' </summary>
    ''' <param name="FieldName">Name of the field.</param>
    ''' <returns><c>true</c> if [is field notional field] [the specified field name]; otherwise, <c>false</c>.</returns>
	Private Function IsFieldNotionalField(ByVal FieldName As String) As Boolean
		' *******************************************************************************
		' Simple function to return a boolean value indicating whether or not a column name is
		' part of the tblTransactions table.
		' *******************************************************************************

		Dim NotionalDS As New RenaissanceDataClass.DSSelectFuturesNotional

		Try
			If (NotionalDS Is Nothing) Then
				Return False
			Else
				Return NotionalDS.tblSelectFuturesNotional.Columns.Contains(FieldName)
			End If
		Catch ex As Exception
			Return (False)
		End Try

		Return (False)
	End Function

    ''' <summary>
    ''' Sets the sorted rows.
    ''' </summary>
    ''' <param name="pForceRefresh">if set to <c>true</c> [p force refresh].</param>
	Private Sub SetSortedRows(ByVal pForceRefresh As Boolean)
		' *******************************************************************************
		' Build a Select String appropriate to the form status and apply it to the DataView object.
		' *******************************************************************************

		' Permissions (Add trades button)

		If (HasTransactionInsertPermission) Then
			Me.Button_TradeSelectedRealised.Enabled = True
			Me.Button_TradeSelectedUnRealised.Enabled = True
		Else
			Me.Button_TradeSelectedRealised.Enabled = False
			Me.Button_TradeSelectedUnRealised.Enabled = False
		End If

		'

		Dim SelectString As String

		If (Me.Created) And (Not Me.IsDisposed) Then

			Dim RefreshTable As Boolean = True
			If (myTable.Rows.Count = 0) OrElse (pForceRefresh) Then

				myTable.Clear()

				Dim FundID As Integer = 0
				Dim ValueDate As Date = Date_ValueDate.Value
				Dim StatusGroupFilter As String = Combo_TransactionGroup.Text
				Dim AdministratorDatesFilter As Integer = 0
				Dim UnRealisedCalculationType As RenaissanceGlobals.CalculationTypes_Profit = CalculationTypes_Profit.FIFO

				If (Combo_UnRealisedCalculationType.SelectedIndex >= 0) AndAlso (IsNumeric(Combo_UnRealisedCalculationType.SelectedValue)) Then
					UnRealisedCalculationType = CInt(Combo_UnRealisedCalculationType.SelectedValue)
				End If

				If (myAdaptor.SelectCommand.Parameters.Contains("@FundID")) Then
					myAdaptor.SelectCommand.Parameters("@FundID").Value = FundID
				End If

				If (myAdaptor.SelectCommand.Parameters.Contains("@ValueDate")) Then
					myAdaptor.SelectCommand.Parameters("@ValueDate").Value = ValueDate
				End If
				If (myAdaptor.SelectCommand.Parameters.Contains("@StatusGroupFilter")) Then
					myAdaptor.SelectCommand.Parameters("@StatusGroupFilter").Value = StatusGroupFilter
				End If
				If (myAdaptor.SelectCommand.Parameters.Contains("@AdministratorDatesFilter")) Then
					myAdaptor.SelectCommand.Parameters("@AdministratorDatesFilter").Value = AdministratorDatesFilter
				End If
				If (myAdaptor.SelectCommand.Parameters.Contains("@UnRealisedCalculationType")) Then
					myAdaptor.SelectCommand.Parameters("@UnRealisedCalculationType").Value = UnRealisedCalculationType
				End If

				If (myAdaptor.SelectCommand.Parameters.Contains("@Knowledgedate")) Then
					myAdaptor.SelectCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
				End If

				SyncLock myAdaptor.SelectCommand.Connection
					SyncLock myDataset
						myAdaptor.Fill(myDataset, myDataset.tblSelectFuturesNotional.TableName)
					End SyncLock
				End SyncLock


			End If

			SelectString = GetTransactionSelectString()

			Try

				If (myDataView IsNot Nothing) AndAlso (Not (myDataView.RowFilter = SelectString)) Then
					myDataView.RowFilter = SelectString
				End If

			Catch ex As Exception
				SelectString = "true"
				myDataView.RowFilter = SelectString
			End Try

			Me.Label_Status.Text = "(" & myDataView.Count.ToString & " Records) " & SelectString

		End If

	End Sub


	' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
	Private Sub CheckPermissions()
		' *******************************************************************************
		'
		' *******************************************************************************

		Dim Permissions As Integer

		Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

		HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
		HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
		HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
		HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

		' Transactions

		Permissions = MainForm.CheckPermissions(VeniceFormID.frmTransaction.ToString, RenaissanceGlobals.PermissionFeatureType.TypeForm)
		HasTransactionInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)

	End Sub

    ''' <summary>
  ''' Basic event handler, called when a form value is changed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
		' *******************************************************************************
		' In the event of one of the Form Controls changing, refresh the data grid.
		' *******************************************************************************

		If InPaint = False Then

			Dim ForceRefresh As Boolean = False

			If (sender IsNot Combo_FuturesInstrument) Then
				ForceRefresh = True
			End If

			Call SetSortedRows(ForceRefresh)

		End If
	End Sub


    ''' <summary>
    ''' Handles the Click event of the Button_TradeSelectedRealised control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Button_TradeSelectedRealised_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_TradeSelectedRealised.Click, Button_TradeSelectedUnRealised.Click
		' *******************************************************************************
		'
		'
		' *******************************************************************************

		Try
			Dim MessageString As String = ""
			Dim TradeSelector As Integer = 0
			Dim Trade_Realised As Integer = 1
			Dim Trade_UnRealised As Integer = 2
			Dim CommentString As String = ""

			If sender Is Me.Button_TradeSelectedRealised Then
				MessageString = "Zero exposure for Realised P&L for the Selected Instruments ?"
				TradeSelector = Trade_Realised
				CommentString = "Realised P&L"
			ElseIf sender Is Me.Button_TradeSelectedUnRealised Then
				MessageString = "Zero exposure for Un-Realised P&L for the Selected Instruments ?"
				TradeSelector = Trade_UnRealised
				CommentString = "Un-Realised P&L"
			End If

			If MessageBox.Show(MessageString, "Notional Trade", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.OK Then

				Dim gridRow As C1.Win.C1FlexGrid.Row
				Dim TransactionDS As New DSTransaction
				Dim TransactionTable As DSTransaction.tblTransactionDataTable = TransactionDS.tblTransaction
				Dim newTransaction As DSTransaction.tblTransactionRow
				Dim ThisDataSourceRow As DSSelectFuturesNotional.tblSelectFuturesNotionalRow

				For Each gridRow In Me.Grid_Transactions.Rows
					If gridRow.Selected Then
						Try
							ThisDataSourceRow = CType(CType(gridRow.DataSource, DataRowView).Row, RenaissanceDataClass.DSSelectFuturesNotional.tblSelectFuturesNotionalRow)

							newTransaction = TransactionTable.NewtblTransactionRow

							newTransaction.TransactionFund = ThisDataSourceRow.FundID
							newTransaction.TransactionSubFund = ThisDataSourceRow.FundID
							newTransaction.TransactionInstrument = ThisDataSourceRow.NotionalInstrumentID
							newTransaction.TransactionVersusInstrument = ThisDataSourceRow.FuturesInstrumentID

							If (TradeSelector = Trade_Realised) Then
								newTransaction.TransactionUnits = ThisDataSourceRow.NotionalTradeToZeroRealisedPnL
							ElseIf (TradeSelector = Trade_UnRealised) Then
								newTransaction.TransactionUnits = ThisDataSourceRow.NotionalTradeToZeroUnRealisedPnL
							End If

							If (newTransaction.TransactionUnits < 0) Then
								newTransaction.TransactionType = RenaissanceGlobals.TransactionTypes.Sell
							Else
								newTransaction.TransactionType = RenaissanceGlobals.TransactionTypes.Buy
							End If
							newTransaction.TransactionUnits = Math.Abs(newTransaction.TransactionUnits)


              newTransaction.TransactionTradeStatusID = DEFAULT_EOD_TRADESTATUS
              newTransaction.TransactionCounterparty = 1 ' Market

							newTransaction.TransactionGroup = ""
							newTransaction.TransactionInvestment = ""
							newTransaction.TransactionExecution = ""
							newTransaction.TransactionDataEntry = ""
							newTransaction.TransactionDecisionDate = Now.Date
							newTransaction.TransactionValueDate = Me.Date_ValueDate.Value
							newTransaction.TransactionSettlementDate = Me.Date_ValueDate.Value
							newTransaction.TransactionConfirmationDate = Me.Date_ValueDate.Value
							newTransaction.TransactionValueorAmount = "Amount"
							newTransaction.TransactionPrice = 1
							newTransaction.TransactionCosts = 0
							newTransaction.TransactionCostPercent = 0
							newTransaction.TransactionSettlement = newTransaction.TransactionUnits

							' Transfer Details.

							newTransaction.TransactionIsTransfer = False
							newTransaction.TransactionEffectivePrice = newTransaction.TransactionPrice
							newTransaction.TransactionEffectiveValueDate = newTransaction.TransactionValueDate
							newTransaction.TransactionSpecificInitialEqualisationFlag = False
							newTransaction.TransactionSpecificInitialEqualisationValue = 0
							newTransaction.TransactionEffectiveWatermark = 0



							newTransaction.TransactionCostIsPercent = True
							newTransaction.TransactionExemptFromUpdate = True
							newTransaction.TransactionFinalAmount = (-1)
							newTransaction.TransactionFinalPrice = (-1)
							newTransaction.TransactionFinalCosts = (-1)
							newTransaction.TransactionFinalSettlement = (-1)
							newTransaction.TransactionInstructionFlag = (-1)
							newTransaction.TransactionRedeemWholeHoldingFlag = False
							newTransaction.TransactionBankConfirmation = 0
							newTransaction.TransactionInitialDataEntry = 0
							newTransaction.TransactionAmountConfirmed = 0
							newTransaction.TransactionSettlementConfirmed = 0
							newTransaction.TransactionFinalDataEntry = 0
              newTransaction.TransactionRedemptionID = 0
							newTransaction.TransactionComment = "Notionals Grid trade to zero " & CommentString
							newTransaction.TransactionCTFLAGS = 0
							newTransaction.TransactionCompoundRN = 0

							If (Math.Abs(newTransaction.TransactionUnits) >= 0.001) Then
								TransactionTable.AddtblTransactionRow(newTransaction)
							End If

						Catch ex As Exception
							MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error setting transaction details, Notional Transactions.", ex.StackTrace, True)
						End Try

					End If

				Next

				MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblTransaction, TransactionTable.Select())

				Dim UpdateString As String = ""
				Dim Counter As Integer

				For Counter = 0 To (TransactionTable.Rows.Count - 1)
					newTransaction = TransactionTable.Rows(Counter)

					If (newTransaction.IsAuditIDNull = False) Then
						If (Counter = 0) Then
							UpdateString = newTransaction.AuditID.ToString()
						Else
							UpdateString &= "," & newTransaction.AuditID.ToString()
						End If

					End If

				Next

				Call MainForm.ReloadTable_Background(RenaissanceStandardDatasets.tblTransaction.ChangeID, UpdateString, True)
				'Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceStandardDatasets.tblTransaction.ChangeID))
				MainForm.ReloadTable_Background(THIS_FORM_ChangeID, "", True)	' I don't use this here, but just in case it is used elsewhere...

				Call SetSortedRows(True)
			End If

		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error saving Notional Transactions.", ex.StackTrace, True)
		End Try
	End Sub

#End Region

#Region " Set Form Combos (Form Specific Code) "

    ''' <summary>
    ''' Sets the fund combo.
    ''' </summary>
	Private Sub SetFundCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_NotionalFund, _
    RenaissanceStandardDatasets.tblFund, _
    "FundName", _
    "FundID", _
    "FundClosed=0", False, True, True)   ' 

	End Sub

    ''' <summary>
    ''' Sets the transaction group combo.
    ''' </summary>
	Private Sub SetTransactionGroupCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_TransactionGroup, _
		RenaissanceStandardDatasets.tblSophisStatusGroups, _
		"GROUP_NAME", _
		"GROUP_NAME", _
		"", True, True, True)		' 

	End Sub

    ''' <summary>
    ''' Sets the un realised calculation type combo.
    ''' </summary>
	Private Sub SetUnRealisedCalculationTypeCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_UnRealisedCalculationType, _
		GetType(CalculationTypes_Profit))		' 

	End Sub

    ''' <summary>
    ''' Sets the instrument combo.
    ''' </summary>
	Private Sub SetInstrumentCombo()

		' Get Futures-Style Instrument Types, if not done already

		If (_FuturesInstrumentTypes.Length >= 0) Then
			Dim TypeDS As RenaissanceDataClass.DSInstrumentType = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblInstrumentType, False)

			Dim TypeRows() As RenaissanceDataClass.DSInstrumentType.tblInstrumentTypeRow = TypeDS.tblInstrumentType.Select("InstrumentTypeFuturesStylePricing<>0")
			Dim TypeRow As RenaissanceDataClass.DSInstrumentType.tblInstrumentTypeRow

			For Each TypeRow In TypeRows
				If (_FuturesInstrumentTypes.Length > 0) Then
					_FuturesInstrumentTypes &= ","
				End If
				_FuturesInstrumentTypes &= TypeRow.InstrumentTypeID
			Next

		End If

		' Set Combo.

		Call MainForm.SetTblGenericCombo( _
		 Me.Combo_FuturesInstrument, _
		 RenaissanceStandardDatasets.tblInstrument, _
		 "InstrumentDescription", _
		 "InstrumentID", _
		 "InstrumentType IN (" & _FuturesInstrumentTypes & ")", False, True, True)	 ' 

	End Sub


#End Region

#Region " Notional report Menu"

    ''' <summary>
    ''' Sets the transaction report menu.
    ''' </summary>
    ''' <param name="RootMenu">The root menu.</param>
    ''' <returns>MenuStrip.</returns>
	Private Function SetTransactionReportMenu(ByRef RootMenu As MenuStrip) As MenuStrip
		' ***************************************************************************************
		' Setup the Transaction Reports menu with appropriate items and callbacks
		'
		' ***************************************************************************************

		Dim ReportMenu As New ToolStripMenuItem("Notional &Reports")
		Dim newMenuItem As ToolStripMenuItem

		newMenuItem = ReportMenu.DropDownItems.Add("Notional Management Report", Nothing, AddressOf Me.rptNotionalManagementReport)

		RootMenu.Items.Add(ReportMenu)
		Return RootMenu

	End Function

    ''' <summary>
    ''' Sets the field select menu.
    ''' </summary>
    ''' <param name="RootMenu">The root menu.</param>
    ''' <returns>ToolStripMenuItem.</returns>
	Private Function SetFieldSelectMenu(ByRef RootMenu As MenuStrip) As ToolStripMenuItem
		' ***************************************************************************************
		' Build the FieldSelect Menu.
		'
		' Add an item for each field in the SelectTransactions table, when selected the item
		' will show or hide the associated field on the transactions grid.
		' ***************************************************************************************

		Dim ColumnNames(-1) As String
		Dim ColumnCount As Integer

		Dim FieldsMenu As New ToolStripMenuItem("Grid &Fields")
		FieldsMenu.Name = "Menu_GridField"

		Dim newMenuItem As ToolStripMenuItem

		ReDim ColumnNames(myTable.Columns.Count - 1)
		For ColumnCount = 0 To (myTable.Columns.Count - 1)
			ColumnNames(ColumnCount) = myTable.Columns(ColumnCount).ColumnName
			If (ColumnNames(ColumnCount).StartsWith("Transaction")) Then
				ColumnNames(ColumnCount) = ColumnNames(ColumnCount).Substring(11) & "."
			End If
		Next
		Array.Sort(ColumnNames)

		For ColumnCount = 0 To (ColumnNames.Length - 1)
			newMenuItem = FieldsMenu.DropDownItems.Add(ColumnNames(ColumnCount), Nothing, AddressOf Me.ShowGridField)

			If (ColumnNames(ColumnCount).EndsWith(".")) Then
				newMenuItem.Name = "Menu_GridField_Transaction" & ColumnNames(ColumnCount).Substring(0, ColumnNames(ColumnCount).Length - 1)
			Else
				newMenuItem.Name = "Menu_GridField_" & ColumnNames(ColumnCount)
			End If
		Next

		RootMenu.Items.Add(FieldsMenu)
		Return FieldsMenu

	End Function

    ''' <summary>
    ''' Shows the grid field.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Private Sub ShowGridField(ByVal sender As Object, ByVal e As EventArgs)
		' ***************************************************************************************
		' Callback function for the Grid Fields Menu items.
		'
		' ***************************************************************************************

		Try
			If TypeOf (sender) Is ToolStripMenuItem Then
				Dim thisMenuItem As ToolStripMenuItem
				Dim FieldName As String

				thisMenuItem = CType(sender, ToolStripMenuItem)

				FieldName = thisMenuItem.Name
				If (FieldName.StartsWith("Menu_GridField_")) Then
					FieldName = FieldName.Substring(15)
				End If

				thisMenuItem.Checked = Not thisMenuItem.Checked

				If (thisMenuItem.Checked) Then
					Grid_Transactions.Cols(FieldName).Visible = True
				Else
					Grid_Transactions.Cols(FieldName).Visible = False
				End If
			End If
		Catch ex As Exception

		End Try
	End Sub

    ''' <summary>
    ''' RPTs the notional management report.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Private Sub rptNotionalManagementReport(ByVal sender As Object, ByVal e As EventArgs)
		' ***************************************************************************************
		'
		'
		' ***************************************************************************************

		
	End Sub

#End Region

#Region " Bug hunting "

	' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
	' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
	' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

    ''' <summary>
    ''' Called when [row updating].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
	Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
		Dim a As Integer

		If Not e.Command Is Nothing Then
			a = 1
		End If

		If Not e.Errors Is Nothing Then
			a = 2
		End If
	End Sub

    ''' <summary>
    ''' Called when [row updated].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
	Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
		Dim a As Integer

		If Not e.Command Is Nothing Then
			a = 1
		End If

		If Not e.Errors Is Nothing Then
			a = 2
		End If
	End Sub

    ''' <summary>
    ''' Called when [row fill error].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
	Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
		Dim a As Integer

		a = 1
	End Sub

#End Region







	'Private Sub Grid_Transactions_CellChanged(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles Grid_Transactions.CellChanged

	'	Try
	'		If e.Col = Grid_Transactions.
	'			If _flex(e.Row, e.Col).ToString >= 50000 Then
	'				cs = _flex.Styles("LargeValue")
	'				_flex.SetCellStyle(e.Row, e.Col, cs)
	'			End If
	'	Catch ex As Exception

	'	End Try
	'End Sub
End Class
