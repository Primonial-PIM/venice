﻿' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 02-13-2013
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmReconciliationProcess.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************

Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals
Imports RenaissanceUtilities.DatePeriodFunctions

Imports RenaissanceDataClass
Imports C1.Win.C1FlexGrid
Imports System.Threading
Imports System.Globalization

''' <summary>
''' Class frmReconciliationProcess
''' </summary>
Public Class frmReconciliationProcess

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmReconciliationProcess"/> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The combo_ fund
    ''' </summary>
  Friend WithEvents Combo_Fund As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label_ cpty is fund
    ''' </summary>
  Friend WithEvents label_CptyIsFund As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ futures instrument
    ''' </summary>
  Friend WithEvents Combo_FuturesInstrument As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label2
    ''' </summary>
  Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label3
    ''' </summary>
  Friend WithEvents Label3 As System.Windows.Forms.Label
    ''' <summary>
    ''' The date_ value date
    ''' </summary>
  Friend WithEvents Date_ValueDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The grid_ notional exposure
    ''' </summary>
  Friend WithEvents Grid_NotionalExposure As C1.Win.C1FlexGrid.C1FlexGrid
    ''' <summary>
    ''' The root menu
    ''' </summary>
  Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
    ''' <summary>
    ''' The form_ status strip
    ''' </summary>
  Friend WithEvents Form_StatusStrip As System.Windows.Forms.StatusStrip
    ''' <summary>
    ''' The form_ progress bar
    ''' </summary>
  Friend WithEvents Form_ProgressBar As System.Windows.Forms.ToolStripProgressBar
    ''' <summary>
    ''' The button_ trade selected realised
    ''' </summary>
  Friend WithEvents Button_TradeSelectedRealised As System.Windows.Forms.Button
    ''' <summary>
    ''' The button_ trade selected un realised
    ''' </summary>
  Friend WithEvents Button_TradeSelectedUnRealised As System.Windows.Forms.Button
    ''' <summary>
    ''' The combo_ un realised calculation type
    ''' </summary>
  Friend WithEvents Combo_UnRealisedCalculationType As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label1
    ''' </summary>
  Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ transaction group
    ''' </summary>
  Friend WithEvents Combo_TransactionGroup As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label8
    ''' </summary>
  Friend WithEvents Label8 As System.Windows.Forms.Label
    ''' <summary>
    ''' The tab control_ reconciliation
    ''' </summary>
  Friend WithEvents TabControl_Reconciliation As System.Windows.Forms.TabControl
    ''' <summary>
    ''' The tab_ notional
    ''' </summary>
  Friend WithEvents Tab_Notional As System.Windows.Forms.TabPage
    ''' <summary>
    ''' The tab_ valuation
    ''' </summary>
  Friend WithEvents Tab_Valuation As System.Windows.Forms.TabPage
    ''' <summary>
    ''' The tab_ fees
    ''' </summary>
  Friend WithEvents Tab_Fees As System.Windows.Forms.TabPage
    ''' <summary>
    ''' The tab_ milestone
    ''' </summary>
  Friend WithEvents Tab_Milestone As System.Windows.Forms.TabPage
    ''' <summary>
    ''' The date_ fees since
    ''' </summary>
  Friend WithEvents Date_FeesSince As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The label4
    ''' </summary>
  Friend WithEvents Label4 As System.Windows.Forms.Label
    ''' <summary>
    ''' The group box1
    ''' </summary>
  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The check_ S r_ ignore todays trades
    ''' </summary>
  Friend WithEvents Check_SR_IgnoreTodaysTrades As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ S r_ ignore status
    ''' </summary>
  Friend WithEvents Check_SR_IgnoreStatus As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The panel1
    ''' </summary>
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The label12
    ''' </summary>
  Friend WithEvents Label12 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label10
    ''' </summary>
  Friend WithEvents Label10 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ MGMT fee NAV
    ''' </summary>
  Friend WithEvents Label_MgmtFeeNAV As System.Windows.Forms.Label
    ''' <summary>
    ''' The label11
    ''' </summary>
  Friend WithEvents Label11 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ MGMT fees rate
    ''' </summary>
  Friend WithEvents Label_MgmtFeesRate As System.Windows.Forms.Label
    ''' <summary>
    ''' The label9
    ''' </summary>
  Friend WithEvents Label9 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label7
    ''' </summary>
  Friend WithEvents Label7 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label16
    ''' </summary>
  Friend WithEvents Label16 As System.Windows.Forms.Label
    ''' <summary>
    ''' The button_ trade management fees
    ''' </summary>
  Friend WithEvents Button_TradeManagementFees As System.Windows.Forms.Button
    ''' <summary>
    ''' The label15
    ''' </summary>
  Friend WithEvents Label15 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label14
    ''' </summary>
  Friend WithEvents Label14 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label13
    ''' </summary>
  Friend WithEvents Label13 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label17
    ''' </summary>
  Friend WithEvents Label17 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label18
    ''' </summary>
  Friend WithEvents Label18 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label19
    ''' </summary>
  Friend WithEvents Label19 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label20
    ''' </summary>
  Friend WithEvents Label20 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label21
    ''' </summary>
  Friend WithEvents Label21 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ fund performance
    ''' </summary>
  Friend WithEvents Label_FundPerformance As System.Windows.Forms.Label
    ''' <summary>
    ''' The label23
    ''' </summary>
  Friend WithEvents Label23 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ perf fees rate
    ''' </summary>
  Friend WithEvents Label_PerfFeesRate As System.Windows.Forms.Label
    ''' <summary>
    ''' The label25
    ''' </summary>
  Friend WithEvents Label25 As System.Windows.Forms.Label
    ''' <summary>
    ''' The date_ perf fees settlement date
    ''' </summary>
  Friend WithEvents Date_PerfFeesSettlementDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The date_ MGMT fees settlement date
    ''' </summary>
  Friend WithEvents Date_MgmtFeesSettlementDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The label_ MGMT fees already taken
    ''' </summary>
  Friend WithEvents Label_MgmtFeesAlreadyTaken As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ management fees to take
    ''' </summary>
  Friend WithEvents edit_ManagementFeesToTake As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The label_ perf fees already taken
    ''' </summary>
  Friend WithEvents Label_PerfFeesAlreadyTaken As System.Windows.Forms.Label
    ''' <summary>
    ''' The numeric_ perf fees to take
    ''' </summary>
  Friend WithEvents Numeric_PerfFeesToTake As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The label_ benchmark performance
    ''' </summary>
  Friend WithEvents Label_BenchmarkPerformance As System.Windows.Forms.Label
    ''' <summary>
    ''' The label26
    ''' </summary>
  Friend WithEvents Label26 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ management fee instrument
    ''' </summary>
  Friend WithEvents Combo_ManagementFeeInstrument As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ performance fee instrument
    ''' </summary>
  Friend WithEvents Combo_PerformanceFeeInstrument As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The check_ administrator price dates
    ''' </summary>
  Friend WithEvents Check_AdministratorPriceDates As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The panel2
    ''' </summary>
  Friend WithEvents Panel2 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The label_ fees_ year start NAV
    ''' </summary>
  Friend WithEvents Label_Fees_YearStartNAV As System.Windows.Forms.Label
    ''' <summary>
    ''' The label29
    ''' </summary>
  Friend WithEvents Label29 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ fees_ financial year
    ''' </summary>
  Friend WithEvents Label_Fees_FinancialYear As System.Windows.Forms.Label
    ''' <summary>
    ''' The label28
    ''' </summary>
  Friend WithEvents Label28 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label24
    ''' </summary>
  Friend WithEvents Label24 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ fees_ best benchmark
    ''' </summary>
  Friend WithEvents Label_Fees_BestBenchmark As System.Windows.Forms.Label
    ''' <summary>
    ''' The label51
    ''' </summary>
  Friend WithEvents Label51 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ fees_ GNAV
    ''' </summary>
  Friend WithEvents Label_Fees_GNAV As System.Windows.Forms.Label
    ''' <summary>
    ''' The label37
    ''' </summary>
  Friend WithEvents Label37 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ fees_ perf fees this year
    ''' </summary>
  Friend WithEvents Label_Fees_PerfFeesThisYear As System.Windows.Forms.Label
    ''' <summary>
    ''' The label39
    ''' </summary>
  Friend WithEvents Label39 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ fees_ year redemptions
    ''' </summary>
  Friend WithEvents Label_Fees_YearRedemptions As System.Windows.Forms.Label
    ''' <summary>
    ''' The label33
    ''' </summary>
  Friend WithEvents Label33 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ fees_ year subscriptions
    ''' </summary>
  Friend WithEvents Label_Fees_YearSubscriptions As System.Windows.Forms.Label
    ''' <summary>
    ''' The label31
    ''' </summary>
  Friend WithEvents Label31 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label36
    ''' </summary>
  Friend WithEvents Label36 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ performance fee crystalised instrument
    ''' </summary>
  Friend WithEvents Combo_PerformanceFeeCrystalisedInstrument As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label34
    ''' </summary>
  Friend WithEvents Label34 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ perf fees crystalised already taken
    ''' </summary>
  Friend WithEvents Label_PerfFeesCrystalisedAlreadyTaken As System.Windows.Forms.Label
    ''' <summary>
    ''' The numeric_ perf fees crystalised to take
    ''' </summary>
  Friend WithEvents Numeric_PerfFeesCrystalisedToTake As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The label27
    ''' </summary>
  Friend WithEvents Label27 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label30
    ''' </summary>
  Friend WithEvents Label30 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label32
    ''' </summary>
  Friend WithEvents Label32 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ fees_ additional performance trade
    ''' </summary>
  Friend WithEvents Label_Fees_AdditionalPerformanceTrade As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ fees_ additional crystalised trade
    ''' </summary>
  Friend WithEvents Label_Fees_AdditionalCrystalisedTrade As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ fees_ additional management trade
    ''' </summary>
  Friend WithEvents Label_Fees_AdditionalManagementTrade As System.Windows.Forms.Label
    ''' <summary>
    ''' The button_ trade crystalised fees
    ''' </summary>
  Friend WithEvents Button_TradeCrystalisedFees As System.Windows.Forms.Button
    ''' <summary>
    ''' The button_ trade performance fees
    ''' </summary>
  Friend WithEvents Button_TradePerformanceFees As System.Windows.Forms.Button
    ''' <summary>
    ''' The grid_ valuations
    ''' </summary>
  Friend WithEvents Grid_Valuations As C1.Win.C1FlexGrid.C1FlexGrid
    ''' <summary>
    ''' The timer_ update
    ''' </summary>
  Friend WithEvents Timer_Update As System.Windows.Forms.Timer
    ''' <summary>
    ''' The status label
    ''' </summary>
  Friend WithEvents StatusLabel As System.Windows.Forms.ToolStripStatusLabel
    ''' <summary>
    ''' The context menu_ valuation
    ''' </summary>
  Friend WithEvents ContextMenu_Valuation As System.Windows.Forms.ContextMenuStrip
    ''' <summary>
    ''' The menu_ valuation_ accept price
    ''' </summary>
  Friend WithEvents Menu_Valuation_AcceptPrice As System.Windows.Forms.ToolStripMenuItem
    ''' <summary>
    ''' The menu_ valuation_ accept FX rate
    ''' </summary>
  Friend WithEvents Menu_Valuation_AcceptFXRate As System.Windows.Forms.ToolStripMenuItem
    ''' <summary>
    ''' The menu_ valuation_ separator
    ''' </summary>
  Friend WithEvents Menu_Valuation_Separator As System.Windows.Forms.ToolStripSeparator
    ''' <summary>
    ''' The menu_ valuation_ view transactions
    ''' </summary>
  Friend WithEvents Menu_Valuation_ViewTransactions As System.Windows.Forms.ToolStripMenuItem
    ''' <summary>
    ''' The menu_ valuation_ description
    ''' </summary>
  Friend WithEvents Menu_Valuation_Description As System.Windows.Forms.ToolStripMenuItem
    ''' <summary>
    ''' The tool strip separator1
    ''' </summary>
  Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    ''' <summary>
    ''' The check_ include unbooked fees
    ''' </summary>
  Friend WithEvents Check_IncludeUnbookedFees As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The label_ fees_ crystalised today
    ''' </summary>
  Friend WithEvents Label_Fees_CrystalisedToday As System.Windows.Forms.Label
    ''' <summary>
    ''' The label22
    ''' </summary>
  Friend WithEvents Label22 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ fees_ fund NAV
    ''' </summary>
  Friend WithEvents Label_Fees_FundNAV As System.Windows.Forms.Label
    ''' <summary>
    ''' The label6
    ''' </summary>
  Friend WithEvents Label6 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ fees_ previous performance fees
    ''' </summary>
  Friend WithEvents Label_Fees_PreviousPerformanceFees As System.Windows.Forms.Label
    ''' <summary>
    ''' The label41
    ''' </summary>
  Friend WithEvents Label41 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ fees_ previous units issued
    ''' </summary>
  Friend WithEvents Label_Fees_PreviousUnitsIssued As System.Windows.Forms.Label
    ''' <summary>
    ''' The label40
    ''' </summary>
  Friend WithEvents Label40 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ fees_ todays unit redemptions
    ''' </summary>
  Friend WithEvents Label_Fees_TodaysUnitRedemptions As System.Windows.Forms.Label
    ''' <summary>
    ''' The label38
    ''' </summary>
  Friend WithEvents Label38 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ fees_ benchmark NAV
    ''' </summary>
  Friend WithEvents Label_Fees_BenchmarkNAV As System.Windows.Forms.Label
    ''' <summary>
    ''' The label35
    ''' </summary>
  Friend WithEvents Label35 As System.Windows.Forms.Label
    ''' <summary>
    ''' The group box2
    ''' </summary>
  Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The label46
    ''' </summary>
  Friend WithEvents Label46 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label45
    ''' </summary>
  Friend WithEvents Label45 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ milestone_ administrator units
    ''' </summary>
  Friend WithEvents Label_Milestone_AdministratorUnits As System.Windows.Forms.Label
    ''' <summary>
    ''' The label42
    ''' </summary>
  Friend WithEvents Label42 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ milestone_ NA v_ administrator
    ''' </summary>
  Friend WithEvents edit_Milestone_NAV_Administrator As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The label5
    ''' </summary>
  Friend WithEvents Label5 As System.Windows.Forms.Label
    ''' <summary>
    ''' The group box3
    ''' </summary>
  Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The label49
    ''' </summary>
  Friend WithEvents Label49 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ milestone_ trading units
    ''' </summary>
  Friend WithEvents Label_Milestone_TradingUnits As System.Windows.Forms.Label
    ''' <summary>
    ''' The label53
    ''' </summary>
  Friend WithEvents Label53 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ milestone_ NA v_ trading
    ''' </summary>
  Friend WithEvents edit_Milestone_NAV_Trading As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The label54
    ''' </summary>
  Friend WithEvents Label54 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label55
    ''' </summary>
  Friend WithEvents Label55 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ milestone_ GNA v_ administrator
    ''' </summary>
  Friend WithEvents edit_Milestone_GNAV_Administrator As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The label47
    ''' </summary>
  Friend WithEvents Label47 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label56
    ''' </summary>
  Friend WithEvents Label56 As System.Windows.Forms.Label
    ''' <summary>
    ''' The button_ set milestone
    ''' </summary>
  Friend WithEvents button_SetMilestone As System.Windows.Forms.Button
    ''' <summary>
    ''' The edit_ milestone_ GNAV unit price_ valuation
    ''' </summary>
  Friend WithEvents edit_Milestone_GNAVUnitPrice_Valuation As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The edit_ milestone_ NAV unit price_ valuation
    ''' </summary>
  Friend WithEvents edit_Milestone_NAVUnitPrice_Valuation As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The edit_ milestone_ NAV unit price_ trading
    ''' </summary>
  Friend WithEvents edit_Milestone_NAVUnitPrice_Trading As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The label43
    ''' </summary>
  Friend WithEvents Label43 As System.Windows.Forms.Label
    ''' <summary>
    ''' The group box4
    ''' </summary>
  Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The label_ milestone_ additional management trade
    ''' </summary>
  Friend WithEvents Label_Milestone_AdditionalManagementTrade As System.Windows.Forms.Label
    ''' <summary>
    ''' The label48
    ''' </summary>
  Friend WithEvents Label48 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ milestone_ additional crystalised trade
    ''' </summary>
  Friend WithEvents Label_Milestone_AdditionalCrystalisedTrade As System.Windows.Forms.Label
    ''' <summary>
    ''' The label58
    ''' </summary>
  Friend WithEvents Label58 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ milestone_ additional performance trade
    ''' </summary>
  Friend WithEvents Label_Milestone_AdditionalPerformanceTrade As System.Windows.Forms.Label
    ''' <summary>
    ''' The label52
    ''' </summary>
  Friend WithEvents Label52 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ MGMT fees administrator value
    ''' </summary>
  Friend WithEvents Label_MgmtFeesAdministratorValue As System.Windows.Forms.Label
    ''' <summary>
    ''' The label50
    ''' </summary>
  Friend WithEvents Label50 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ perf fees administrator
    ''' </summary>
  Friend WithEvents Label_PerfFeesAdministrator As System.Windows.Forms.Label
    ''' <summary>
    ''' The label57
    ''' </summary>
  Friend WithEvents Label57 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ perf fees crystalised administrator
    ''' </summary>
  Friend WithEvents Label_PerfFeesCrystalisedAdministrator As System.Windows.Forms.Label
    ''' <summary>
    ''' The label59
    ''' </summary>
  Friend WithEvents Label59 As System.Windows.Forms.Label
    ''' <summary>
    ''' The tab_ benchmark
    ''' </summary>
  Friend WithEvents Tab_Benchmark As System.Windows.Forms.TabPage
    ''' <summary>
    ''' The grid_ benchmark
    ''' </summary>
  Friend WithEvents Grid_Benchmark As C1.Win.C1FlexGrid.C1FlexGrid
    ''' <summary>
    ''' The tab_ subscriptions
    ''' </summary>
  Friend WithEvents Tab_Subscriptions As System.Windows.Forms.TabPage
    ''' <summary>
    ''' The grid_ transactions
    ''' </summary>
  Friend WithEvents Grid_Subscriptions As C1.Win.C1FlexGrid.C1FlexGrid
    ''' <summary>
    ''' The panel3
    ''' </summary>
  Friend WithEvents Panel3 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The combo_ trade status
    ''' </summary>
  Friend WithEvents Combo_TradeStatus As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label44
    ''' </summary>
  Friend WithEvents Label44 As System.Windows.Forms.Label
    ''' <summary>
    ''' The BTN_ updatesubscriptions
    ''' </summary>
  Friend WithEvents btn_Updatesubscriptions As System.Windows.Forms.Button
    ''' <summary>
    ''' The edit_ subscription price
    ''' </summary>
  Friend WithEvents edit_SubscriptionPrice As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The label_ price
    ''' </summary>
  Friend WithEvents Label_Price As System.Windows.Forms.Label
    ''' <summary>
    ''' The label61
    ''' </summary>
  Friend WithEvents Label61 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label60
    ''' </summary>
  Friend WithEvents Label60 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ administrator NAV
    ''' </summary>
  Friend WithEvents Label_AdministratorNAV As System.Windows.Forms.Label
    ''' <summary>
    ''' The group box5
    ''' </summary>
  Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The radio_ transactions_ selected
    ''' </summary>
  Friend WithEvents Radio_Transactions_Selected As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The radio_ transactions_ all
    ''' </summary>
  Friend WithEvents Radio_Transactions_All As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The label_ administrator units issued
    ''' </summary>
  Friend WithEvents Label_AdministratorUnitsIssued As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ administrator value
    ''' </summary>
  Friend WithEvents Label_AdministratorValue As System.Windows.Forms.Label
    ''' <summary>
    ''' The label62
    ''' </summary>
  Friend WithEvents Label62 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label63
    ''' </summary>
  Friend WithEvents Label63 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ administrator NA v2
    ''' </summary>
  Friend WithEvents Label_AdministratorNAV2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The group box6
    ''' </summary>
  Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The check_ update orders automatically
    ''' </summary>
  Friend WithEvents Check_UpdateOrdersAutomatically As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The edit_ fixed administrator price
    ''' </summary>
  Friend WithEvents edit_FixedAdministratorPrice As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The button_ book retrocessions
    ''' </summary>
  Friend WithEvents Button_BookRetrocessions As System.Windows.Forms.Button
    ''' <summary>
    ''' The tab_ availabilities
    ''' </summary>
  Friend WithEvents Tab_Availabilities As System.Windows.Forms.TabPage
    ''' <summary>
    ''' The label64
    ''' </summary>
  Friend WithEvents Label64 As System.Windows.Forms.Label
    ''' <summary>
    ''' The grid_ retrocessions
    ''' </summary>
  Friend WithEvents Grid_Retrocessions As C1.Win.C1FlexGrid.C1FlexGrid
    ''' <summary>
    ''' The BTN_ test
    ''' </summary>
  Friend WithEvents btn_Test As System.Windows.Forms.Button
    ''' <summary>
    ''' The button_ book margin deposits
    ''' </summary>
  Friend WithEvents Button_BookMarginDeposits As System.Windows.Forms.Button
    ''' <summary>
    ''' The label_ fees_ last benchmark date
    ''' </summary>
  Friend WithEvents Label_Fees_LastBenchmarkDate As System.Windows.Forms.Label
    ''' <summary>
    ''' The label66
    ''' </summary>
  Friend WithEvents Label66 As System.Windows.Forms.Label
  Friend WithEvents Label65 As System.Windows.Forms.Label
  Friend WithEvents Label_PerfFeesCrystalisedTotalBooked As System.Windows.Forms.Label
  Friend WithEvents Menu_Valuation_ViewPricesForm As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Tab_FeesMultiClass As System.Windows.Forms.TabPage
  Friend WithEvents Grid_FeesMultiClass As C1.Win.C1FlexGrid.C1FlexGrid
  Friend WithEvents TabControl_Hidden As System.Windows.Forms.TabControl
  Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
  Friend WithEvents Check_fee_UpdateOrders As System.Windows.Forms.CheckBox
  Friend WithEvents Button_Fee_SetMilestone As System.Windows.Forms.Button
  Friend WithEvents Button_Fee_BookCrystalised As System.Windows.Forms.Button
  Friend WithEvents Button_Fee_BookPerformance As System.Windows.Forms.Button
  Friend WithEvents Button_Fee_BookManagement As System.Windows.Forms.Button
  Friend WithEvents Benchmark_Label_BenchmarkName As System.Windows.Forms.Label
  Friend WithEvents Label67 As System.Windows.Forms.Label
  Friend WithEvents Grid_PreviousSubscriptions As C1.Win.C1FlexGrid.C1FlexGrid
  Friend WithEvents Panel4 As System.Windows.Forms.Panel
  Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents Menu_Valuation_ViewInstrument As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Tab_AutomatedFees As System.Windows.Forms.TabPage
  Friend WithEvents Grid_AutomatedFees As C1.Win.C1FlexGrid.C1FlexGrid
  Friend WithEvents Button_BookSelectedAutomatedFees As System.Windows.Forms.Button
  Friend WithEvents Button_AutomatedFees_SelectNone As System.Windows.Forms.Button
  Friend WithEvents Button_AutomatedFees_SelectAll As System.Windows.Forms.Button
  Friend WithEvents ContextMenu_AutomatedFees As System.Windows.Forms.ContextMenuStrip
  Friend WithEvents Menu_AutomatedFees_ShowTransaction As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_AutomatedFees_ShowFeeDefenition As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_AutomatedFees_Description As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
  ''' <summary>
  ''' The label_ status
  ''' </summary>
  Friend WithEvents Label_Status As System.Windows.Forms.ToolStripStatusLabel
  ''' <summary>
  ''' Initializes the component.
  ''' </summary>
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReconciliationProcess))
    Me.Grid_NotionalExposure = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.Combo_Fund = New System.Windows.Forms.ComboBox
    Me.label_CptyIsFund = New System.Windows.Forms.Label
    Me.Combo_FuturesInstrument = New System.Windows.Forms.ComboBox
    Me.Label2 = New System.Windows.Forms.Label
    Me.Label3 = New System.Windows.Forms.Label
    Me.Date_ValueDate = New System.Windows.Forms.DateTimePicker
    Me.RootMenu = New System.Windows.Forms.MenuStrip
    Me.Form_StatusStrip = New System.Windows.Forms.StatusStrip
    Me.Form_ProgressBar = New System.Windows.Forms.ToolStripProgressBar
    Me.Label_Status = New System.Windows.Forms.ToolStripStatusLabel
    Me.StatusLabel = New System.Windows.Forms.ToolStripStatusLabel
    Me.Button_TradeSelectedRealised = New System.Windows.Forms.Button
    Me.Button_TradeSelectedUnRealised = New System.Windows.Forms.Button
    Me.Combo_UnRealisedCalculationType = New System.Windows.Forms.ComboBox
    Me.Label1 = New System.Windows.Forms.Label
    Me.Combo_TransactionGroup = New System.Windows.Forms.ComboBox
    Me.Label8 = New System.Windows.Forms.Label
    Me.TabControl_Reconciliation = New System.Windows.Forms.TabControl
    Me.Tab_Valuation = New System.Windows.Forms.TabPage
    Me.Check_IncludeUnbookedFees = New System.Windows.Forms.CheckBox
    Me.Grid_Valuations = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.ContextMenu_Valuation = New System.Windows.Forms.ContextMenuStrip(Me.components)
    Me.Menu_Valuation_Description = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
    Me.Menu_Valuation_AcceptPrice = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Valuation_ViewPricesForm = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
    Me.Menu_Valuation_AcceptFXRate = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Valuation_Separator = New System.Windows.Forms.ToolStripSeparator
    Me.Menu_Valuation_ViewTransactions = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Valuation_ViewInstrument = New System.Windows.Forms.ToolStripMenuItem
    Me.Tab_Notional = New System.Windows.Forms.TabPage
    Me.Tab_Availabilities = New System.Windows.Forms.TabPage
    Me.Button_BookMarginDeposits = New System.Windows.Forms.Button
    Me.Grid_Retrocessions = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.Label64 = New System.Windows.Forms.Label
    Me.Button_BookRetrocessions = New System.Windows.Forms.Button
    Me.Tab_AutomatedFees = New System.Windows.Forms.TabPage
    Me.Button_AutomatedFees_SelectNone = New System.Windows.Forms.Button
    Me.Button_AutomatedFees_SelectAll = New System.Windows.Forms.Button
    Me.Button_BookSelectedAutomatedFees = New System.Windows.Forms.Button
    Me.Grid_AutomatedFees = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.Tab_FeesMultiClass = New System.Windows.Forms.TabPage
    Me.GroupBox7 = New System.Windows.Forms.GroupBox
    Me.Check_fee_UpdateOrders = New System.Windows.Forms.CheckBox
    Me.Button_Fee_SetMilestone = New System.Windows.Forms.Button
    Me.Button_Fee_BookCrystalised = New System.Windows.Forms.Button
    Me.Button_Fee_BookPerformance = New System.Windows.Forms.Button
    Me.Button_Fee_BookManagement = New System.Windows.Forms.Button
    Me.Grid_FeesMultiClass = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.Tab_Fees = New System.Windows.Forms.TabPage
    Me.Panel1 = New System.Windows.Forms.Panel
    Me.Label65 = New System.Windows.Forms.Label
    Me.Label_PerfFeesCrystalisedTotalBooked = New System.Windows.Forms.Label
    Me.Label30 = New System.Windows.Forms.Label
    Me.Label_PerfFeesCrystalisedAdministrator = New System.Windows.Forms.Label
    Me.Label59 = New System.Windows.Forms.Label
    Me.Label_PerfFeesAdministrator = New System.Windows.Forms.Label
    Me.Label57 = New System.Windows.Forms.Label
    Me.Label_MgmtFeesAdministratorValue = New System.Windows.Forms.Label
    Me.Label50 = New System.Windows.Forms.Label
    Me.Button_TradeCrystalisedFees = New System.Windows.Forms.Button
    Me.Button_TradePerformanceFees = New System.Windows.Forms.Button
    Me.Label_Fees_AdditionalCrystalisedTrade = New System.Windows.Forms.Label
    Me.Label_Fees_AdditionalManagementTrade = New System.Windows.Forms.Label
    Me.Label_Fees_AdditionalPerformanceTrade = New System.Windows.Forms.Label
    Me.Label36 = New System.Windows.Forms.Label
    Me.Combo_PerformanceFeeCrystalisedInstrument = New System.Windows.Forms.ComboBox
    Me.Label34 = New System.Windows.Forms.Label
    Me.Label_PerfFeesCrystalisedAlreadyTaken = New System.Windows.Forms.Label
    Me.Numeric_PerfFeesCrystalisedToTake = New RenaissanceControls.NumericTextBox
    Me.Label27 = New System.Windows.Forms.Label
    Me.Label32 = New System.Windows.Forms.Label
    Me.Panel2 = New System.Windows.Forms.Panel
    Me.Label_Fees_LastBenchmarkDate = New System.Windows.Forms.Label
    Me.Label66 = New System.Windows.Forms.Label
    Me.Label_Fees_BenchmarkNAV = New System.Windows.Forms.Label
    Me.Label35 = New System.Windows.Forms.Label
    Me.Label_Fees_PreviousPerformanceFees = New System.Windows.Forms.Label
    Me.Label41 = New System.Windows.Forms.Label
    Me.Label_Fees_PreviousUnitsIssued = New System.Windows.Forms.Label
    Me.Label40 = New System.Windows.Forms.Label
    Me.Label_Fees_TodaysUnitRedemptions = New System.Windows.Forms.Label
    Me.Label38 = New System.Windows.Forms.Label
    Me.Label_Fees_CrystalisedToday = New System.Windows.Forms.Label
    Me.Label22 = New System.Windows.Forms.Label
    Me.Label_Fees_FundNAV = New System.Windows.Forms.Label
    Me.Label6 = New System.Windows.Forms.Label
    Me.Label_Fees_BestBenchmark = New System.Windows.Forms.Label
    Me.Label51 = New System.Windows.Forms.Label
    Me.Label_Fees_GNAV = New System.Windows.Forms.Label
    Me.Label37 = New System.Windows.Forms.Label
    Me.Label_Fees_PerfFeesThisYear = New System.Windows.Forms.Label
    Me.Label39 = New System.Windows.Forms.Label
    Me.Label_Fees_YearRedemptions = New System.Windows.Forms.Label
    Me.Label33 = New System.Windows.Forms.Label
    Me.Label_Fees_YearSubscriptions = New System.Windows.Forms.Label
    Me.Label31 = New System.Windows.Forms.Label
    Me.Label_Fees_YearStartNAV = New System.Windows.Forms.Label
    Me.Label29 = New System.Windows.Forms.Label
    Me.Label_Fees_FinancialYear = New System.Windows.Forms.Label
    Me.Label28 = New System.Windows.Forms.Label
    Me.Label24 = New System.Windows.Forms.Label
    Me.Combo_PerformanceFeeInstrument = New System.Windows.Forms.ComboBox
    Me.Combo_ManagementFeeInstrument = New System.Windows.Forms.ComboBox
    Me.Label_BenchmarkPerformance = New System.Windows.Forms.Label
    Me.Label26 = New System.Windows.Forms.Label
    Me.Label_PerfFeesAlreadyTaken = New System.Windows.Forms.Label
    Me.Numeric_PerfFeesToTake = New RenaissanceControls.NumericTextBox
    Me.Date_PerfFeesSettlementDate = New System.Windows.Forms.DateTimePicker
    Me.Date_MgmtFeesSettlementDate = New System.Windows.Forms.DateTimePicker
    Me.Label_MgmtFeesAlreadyTaken = New System.Windows.Forms.Label
    Me.edit_ManagementFeesToTake = New RenaissanceControls.NumericTextBox
    Me.Label17 = New System.Windows.Forms.Label
    Me.Label18 = New System.Windows.Forms.Label
    Me.Label19 = New System.Windows.Forms.Label
    Me.Label20 = New System.Windows.Forms.Label
    Me.Label21 = New System.Windows.Forms.Label
    Me.Label_FundPerformance = New System.Windows.Forms.Label
    Me.Label23 = New System.Windows.Forms.Label
    Me.Label_PerfFeesRate = New System.Windows.Forms.Label
    Me.Label25 = New System.Windows.Forms.Label
    Me.Label16 = New System.Windows.Forms.Label
    Me.Button_TradeManagementFees = New System.Windows.Forms.Button
    Me.Label15 = New System.Windows.Forms.Label
    Me.Label14 = New System.Windows.Forms.Label
    Me.Label13 = New System.Windows.Forms.Label
    Me.Label12 = New System.Windows.Forms.Label
    Me.Label10 = New System.Windows.Forms.Label
    Me.Label_MgmtFeeNAV = New System.Windows.Forms.Label
    Me.Label11 = New System.Windows.Forms.Label
    Me.Label_MgmtFeesRate = New System.Windows.Forms.Label
    Me.Label9 = New System.Windows.Forms.Label
    Me.Label7 = New System.Windows.Forms.Label
    Me.Tab_Benchmark = New System.Windows.Forms.TabPage
    Me.Benchmark_Label_BenchmarkName = New System.Windows.Forms.Label
    Me.Grid_Benchmark = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.Tab_Milestone = New System.Windows.Forms.TabPage
    Me.GroupBox6 = New System.Windows.Forms.GroupBox
    Me.edit_FixedAdministratorPrice = New RenaissanceControls.NumericTextBox
    Me.Check_UpdateOrdersAutomatically = New System.Windows.Forms.CheckBox
    Me.button_SetMilestone = New System.Windows.Forms.Button
    Me.GroupBox4 = New System.Windows.Forms.GroupBox
    Me.Label_Milestone_AdditionalCrystalisedTrade = New System.Windows.Forms.Label
    Me.Label58 = New System.Windows.Forms.Label
    Me.Label_Milestone_AdditionalPerformanceTrade = New System.Windows.Forms.Label
    Me.Label52 = New System.Windows.Forms.Label
    Me.Label_Milestone_AdditionalManagementTrade = New System.Windows.Forms.Label
    Me.Label48 = New System.Windows.Forms.Label
    Me.GroupBox3 = New System.Windows.Forms.GroupBox
    Me.edit_Milestone_NAVUnitPrice_Trading = New RenaissanceControls.NumericTextBox
    Me.Label43 = New System.Windows.Forms.Label
    Me.Label56 = New System.Windows.Forms.Label
    Me.Label49 = New System.Windows.Forms.Label
    Me.Label_Milestone_TradingUnits = New System.Windows.Forms.Label
    Me.Label53 = New System.Windows.Forms.Label
    Me.edit_Milestone_NAV_Trading = New RenaissanceControls.NumericTextBox
    Me.Label54 = New System.Windows.Forms.Label
    Me.GroupBox2 = New System.Windows.Forms.GroupBox
    Me.Label_AdministratorUnitsIssued = New System.Windows.Forms.Label
    Me.Label_AdministratorValue = New System.Windows.Forms.Label
    Me.Label62 = New System.Windows.Forms.Label
    Me.Label_AdministratorNAV = New System.Windows.Forms.Label
    Me.edit_Milestone_GNAVUnitPrice_Valuation = New RenaissanceControls.NumericTextBox
    Me.edit_Milestone_NAVUnitPrice_Valuation = New RenaissanceControls.NumericTextBox
    Me.Label55 = New System.Windows.Forms.Label
    Me.edit_Milestone_GNAV_Administrator = New RenaissanceControls.NumericTextBox
    Me.Label47 = New System.Windows.Forms.Label
    Me.Label46 = New System.Windows.Forms.Label
    Me.Label45 = New System.Windows.Forms.Label
    Me.Label_Milestone_AdministratorUnits = New System.Windows.Forms.Label
    Me.Label42 = New System.Windows.Forms.Label
    Me.edit_Milestone_NAV_Administrator = New RenaissanceControls.NumericTextBox
    Me.Label5 = New System.Windows.Forms.Label
    Me.Tab_Subscriptions = New System.Windows.Forms.TabPage
    Me.Label67 = New System.Windows.Forms.Label
    Me.Grid_PreviousSubscriptions = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.Panel4 = New System.Windows.Forms.Panel
    Me.Panel3 = New System.Windows.Forms.Panel
    Me.Label_AdministratorNAV2 = New System.Windows.Forms.Label
    Me.Label63 = New System.Windows.Forms.Label
    Me.GroupBox5 = New System.Windows.Forms.GroupBox
    Me.Radio_Transactions_Selected = New System.Windows.Forms.RadioButton
    Me.Radio_Transactions_All = New System.Windows.Forms.RadioButton
    Me.Label60 = New System.Windows.Forms.Label
    Me.Combo_TradeStatus = New System.Windows.Forms.ComboBox
    Me.Label44 = New System.Windows.Forms.Label
    Me.btn_Updatesubscriptions = New System.Windows.Forms.Button
    Me.edit_SubscriptionPrice = New RenaissanceControls.NumericTextBox
    Me.Label_Price = New System.Windows.Forms.Label
    Me.Label61 = New System.Windows.Forms.Label
    Me.Grid_Subscriptions = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.Check_AdministratorPriceDates = New System.Windows.Forms.CheckBox
    Me.Date_FeesSince = New System.Windows.Forms.DateTimePicker
    Me.Label4 = New System.Windows.Forms.Label
    Me.GroupBox1 = New System.Windows.Forms.GroupBox
    Me.Check_SR_IgnoreTodaysTrades = New System.Windows.Forms.CheckBox
    Me.Check_SR_IgnoreStatus = New System.Windows.Forms.CheckBox
    Me.Timer_Update = New System.Windows.Forms.Timer(Me.components)
    Me.btn_Test = New System.Windows.Forms.Button
    Me.TabControl_Hidden = New System.Windows.Forms.TabControl
    Me.ContextMenu_AutomatedFees = New System.Windows.Forms.ContextMenuStrip(Me.components)
    Me.Menu_AutomatedFees_ShowTransaction = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_AutomatedFees_ShowFeeDefenition = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_AutomatedFees_Description = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
    CType(Me.Grid_NotionalExposure, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Form_StatusStrip.SuspendLayout()
    Me.TabControl_Reconciliation.SuspendLayout()
    Me.Tab_Valuation.SuspendLayout()
    CType(Me.Grid_Valuations, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.ContextMenu_Valuation.SuspendLayout()
    Me.Tab_Notional.SuspendLayout()
    Me.Tab_Availabilities.SuspendLayout()
    CType(Me.Grid_Retrocessions, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_AutomatedFees.SuspendLayout()
    CType(Me.Grid_AutomatedFees, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_FeesMultiClass.SuspendLayout()
    Me.GroupBox7.SuspendLayout()
    CType(Me.Grid_FeesMultiClass, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_Fees.SuspendLayout()
    Me.Panel1.SuspendLayout()
    Me.Panel2.SuspendLayout()
    Me.Tab_Benchmark.SuspendLayout()
    CType(Me.Grid_Benchmark, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_Milestone.SuspendLayout()
    Me.GroupBox6.SuspendLayout()
    Me.GroupBox4.SuspendLayout()
    Me.GroupBox3.SuspendLayout()
    Me.GroupBox2.SuspendLayout()
    Me.Tab_Subscriptions.SuspendLayout()
    CType(Me.Grid_PreviousSubscriptions, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Panel4.SuspendLayout()
    Me.Panel3.SuspendLayout()
    Me.GroupBox5.SuspendLayout()
    CType(Me.Grid_Subscriptions, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.GroupBox1.SuspendLayout()
    Me.ContextMenu_AutomatedFees.SuspendLayout()
    Me.SuspendLayout()
    '
    'Grid_NotionalExposure
    '
    Me.Grid_NotionalExposure.AllowEditing = False
    Me.Grid_NotionalExposure.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_NotionalExposure.CausesValidation = False
    Me.Grid_NotionalExposure.ColumnInfo = "5,1,0,0,0,85,Columns:"
    Me.Grid_NotionalExposure.EditOptions = C1.Win.C1FlexGrid.EditFlags.None
    Me.Grid_NotionalExposure.KeyActionTab = C1.Win.C1FlexGrid.KeyActionEnum.MoveAcross
    Me.Grid_NotionalExposure.Location = New System.Drawing.Point(3, 72)
    Me.Grid_NotionalExposure.Name = "Grid_NotionalExposure"
    Me.Grid_NotionalExposure.Rows.DefaultSize = 17
    Me.Grid_NotionalExposure.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.ListBox
    Me.Grid_NotionalExposure.Size = New System.Drawing.Size(1050, 436)
    Me.Grid_NotionalExposure.TabIndex = 7
    '
    'Combo_Fund
    '
    Me.Combo_Fund.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Fund.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Fund.Location = New System.Drawing.Point(151, 28)
    Me.Combo_Fund.Name = "Combo_Fund"
    Me.Combo_Fund.Size = New System.Drawing.Size(917, 21)
    Me.Combo_Fund.TabIndex = 1
    '
    'label_CptyIsFund
    '
    Me.label_CptyIsFund.Location = New System.Drawing.Point(11, 31)
    Me.label_CptyIsFund.Name = "label_CptyIsFund"
    Me.label_CptyIsFund.Size = New System.Drawing.Size(104, 16)
    Me.label_CptyIsFund.TabIndex = 82
    Me.label_CptyIsFund.Text = "Fund"
    '
    'Combo_FuturesInstrument
    '
    Me.Combo_FuturesInstrument.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_FuturesInstrument.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_FuturesInstrument.Location = New System.Drawing.Point(154, 6)
    Me.Combo_FuturesInstrument.Name = "Combo_FuturesInstrument"
    Me.Combo_FuturesInstrument.Size = New System.Drawing.Size(541, 21)
    Me.Combo_FuturesInstrument.TabIndex = 2
    '
    'Label2
    '
    Me.Label2.Location = New System.Drawing.Point(14, 10)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(104, 16)
    Me.Label2.TabIndex = 84
    Me.Label2.Text = "Instrument"
    '
    'Label3
    '
    Me.Label3.Location = New System.Drawing.Point(11, 59)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(104, 16)
    Me.Label3.TabIndex = 88
    Me.Label3.Text = "Fee Date"
    '
    'Date_ValueDate
    '
    Me.Date_ValueDate.Location = New System.Drawing.Point(151, 55)
    Me.Date_ValueDate.Name = "Date_ValueDate"
    Me.Date_ValueDate.Size = New System.Drawing.Size(227, 20)
    Me.Date_ValueDate.TabIndex = 0
    '
    'RootMenu
    '
    Me.RootMenu.Location = New System.Drawing.Point(0, 0)
    Me.RootMenu.Name = "RootMenu"
    Me.RootMenu.Size = New System.Drawing.Size(1076, 24)
    Me.RootMenu.TabIndex = 104
    Me.RootMenu.Text = " "
    '
    'Form_StatusStrip
    '
    Me.Form_StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Form_ProgressBar, Me.Label_Status, Me.StatusLabel})
    Me.Form_StatusStrip.Location = New System.Drawing.Point(0, 674)
    Me.Form_StatusStrip.Name = "Form_StatusStrip"
    Me.Form_StatusStrip.Size = New System.Drawing.Size(1076, 22)
    Me.Form_StatusStrip.TabIndex = 105
    Me.Form_StatusStrip.Text = " "
    '
    'Form_ProgressBar
    '
    Me.Form_ProgressBar.Maximum = 20
    Me.Form_ProgressBar.Name = "Form_ProgressBar"
    Me.Form_ProgressBar.Size = New System.Drawing.Size(150, 17)
    Me.Form_ProgressBar.Step = 1
    Me.Form_ProgressBar.Visible = False
    '
    'Label_Status
    '
    Me.Label_Status.Name = "Label_Status"
    Me.Label_Status.Size = New System.Drawing.Size(10, 17)
    Me.Label_Status.Text = " "
    '
    'StatusLabel
    '
    Me.StatusLabel.Name = "StatusLabel"
    Me.StatusLabel.Size = New System.Drawing.Size(10, 17)
    Me.StatusLabel.Text = " "
    '
    'Button_TradeSelectedRealised
    '
    Me.Button_TradeSelectedRealised.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Button_TradeSelectedRealised.Location = New System.Drawing.Point(800, 6)
    Me.Button_TradeSelectedRealised.Name = "Button_TradeSelectedRealised"
    Me.Button_TradeSelectedRealised.Size = New System.Drawing.Size(250, 24)
    Me.Button_TradeSelectedRealised.TabIndex = 5
    Me.Button_TradeSelectedRealised.Text = "Zero Selected Notional (Realised P&&L)"
    Me.Button_TradeSelectedRealised.UseVisualStyleBackColor = True
    Me.Button_TradeSelectedRealised.Visible = False
    '
    'Button_TradeSelectedUnRealised
    '
    Me.Button_TradeSelectedUnRealised.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Button_TradeSelectedUnRealised.Location = New System.Drawing.Point(800, 38)
    Me.Button_TradeSelectedUnRealised.Name = "Button_TradeSelectedUnRealised"
    Me.Button_TradeSelectedUnRealised.Size = New System.Drawing.Size(250, 24)
    Me.Button_TradeSelectedUnRealised.TabIndex = 6
    Me.Button_TradeSelectedUnRealised.Text = "Zero Selected Notional (Un-Realised P&&L)"
    Me.Button_TradeSelectedUnRealised.UseVisualStyleBackColor = True
    '
    'Combo_UnRealisedCalculationType
    '
    Me.Combo_UnRealisedCalculationType.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_UnRealisedCalculationType.Location = New System.Drawing.Point(154, 33)
    Me.Combo_UnRealisedCalculationType.Name = "Combo_UnRealisedCalculationType"
    Me.Combo_UnRealisedCalculationType.Size = New System.Drawing.Size(195, 21)
    Me.Combo_UnRealisedCalculationType.TabIndex = 4
    '
    'Label1
    '
    Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label1.Location = New System.Drawing.Point(14, 38)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(126, 16)
    Me.Label1.TabIndex = 143
    Me.Label1.Text = "Unrealised P&L calculation"
    '
    'Combo_TransactionGroup
    '
    Me.Combo_TransactionGroup.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_TransactionGroup.Location = New System.Drawing.Point(151, 81)
    Me.Combo_TransactionGroup.Name = "Combo_TransactionGroup"
    Me.Combo_TransactionGroup.Size = New System.Drawing.Size(229, 21)
    Me.Combo_TransactionGroup.TabIndex = 3
    '
    'Label8
    '
    Me.Label8.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label8.Location = New System.Drawing.Point(11, 86)
    Me.Label8.Name = "Label8"
    Me.Label8.Size = New System.Drawing.Size(126, 16)
    Me.Label8.TabIndex = 141
    Me.Label8.Text = "Transaction Group"
    '
    'TabControl_Reconciliation
    '
    Me.TabControl_Reconciliation.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.TabControl_Reconciliation.Controls.Add(Me.Tab_Valuation)
    Me.TabControl_Reconciliation.Controls.Add(Me.Tab_Notional)
    Me.TabControl_Reconciliation.Controls.Add(Me.Tab_Availabilities)
    Me.TabControl_Reconciliation.Controls.Add(Me.Tab_AutomatedFees)
    Me.TabControl_Reconciliation.Controls.Add(Me.Tab_FeesMultiClass)
    Me.TabControl_Reconciliation.Controls.Add(Me.Tab_Fees)
    Me.TabControl_Reconciliation.Controls.Add(Me.Tab_Benchmark)
    Me.TabControl_Reconciliation.Controls.Add(Me.Tab_Milestone)
    Me.TabControl_Reconciliation.Controls.Add(Me.Tab_Subscriptions)
    Me.TabControl_Reconciliation.Location = New System.Drawing.Point(6, 134)
    Me.TabControl_Reconciliation.Name = "TabControl_Reconciliation"
    Me.TabControl_Reconciliation.SelectedIndex = 0
    Me.TabControl_Reconciliation.Size = New System.Drawing.Size(1064, 537)
    Me.TabControl_Reconciliation.TabIndex = 144
    '
    'Tab_Valuation
    '
    Me.Tab_Valuation.Controls.Add(Me.Check_IncludeUnbookedFees)
    Me.Tab_Valuation.Controls.Add(Me.Grid_Valuations)
    Me.Tab_Valuation.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Valuation.Name = "Tab_Valuation"
    Me.Tab_Valuation.Padding = New System.Windows.Forms.Padding(3)
    Me.Tab_Valuation.Size = New System.Drawing.Size(1056, 511)
    Me.Tab_Valuation.TabIndex = 1
    Me.Tab_Valuation.Text = "Valuation"
    Me.Tab_Valuation.UseVisualStyleBackColor = True
    '
    'Check_IncludeUnbookedFees
    '
    Me.Check_IncludeUnbookedFees.AutoSize = True
    Me.Check_IncludeUnbookedFees.Location = New System.Drawing.Point(4, 4)
    Me.Check_IncludeUnbookedFees.Name = "Check_IncludeUnbookedFees"
    Me.Check_IncludeUnbookedFees.Size = New System.Drawing.Size(140, 17)
    Me.Check_IncludeUnbookedFees.TabIndex = 9
    Me.Check_IncludeUnbookedFees.Text = "Include Unbooked Fees"
    Me.Check_IncludeUnbookedFees.UseVisualStyleBackColor = True
    '
    'Grid_Valuations
    '
    Me.Grid_Valuations.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_Valuations.AutoGenerateColumns = False
    Me.Grid_Valuations.BorderStyle = C1.Win.C1FlexGrid.Util.BaseControls.BorderStyleEnum.Light3D
    Me.Grid_Valuations.CausesValidation = False
    Me.Grid_Valuations.ColumnInfo = resources.GetString("Grid_Valuations.ColumnInfo")
    Me.Grid_Valuations.ContextMenuStrip = Me.ContextMenu_Valuation
    Me.Grid_Valuations.EditOptions = C1.Win.C1FlexGrid.EditFlags.None
    Me.Grid_Valuations.KeyActionTab = C1.Win.C1FlexGrid.KeyActionEnum.MoveAcross
    Me.Grid_Valuations.Location = New System.Drawing.Point(3, 26)
    Me.Grid_Valuations.Name = "Grid_Valuations"
    Me.Grid_Valuations.Rows.DefaultSize = 17
    Me.Grid_Valuations.Size = New System.Drawing.Size(1050, 482)
    Me.Grid_Valuations.StyleInfo = resources.GetString("Grid_Valuations.StyleInfo")
    Me.Grid_Valuations.TabIndex = 8
    Me.Grid_Valuations.Tree.Column = 0
    Me.Grid_Valuations.Tree.Style = CType(((C1.Win.C1FlexGrid.TreeStyleFlags.Lines Or C1.Win.C1FlexGrid.TreeStyleFlags.Symbols) _
                Or C1.Win.C1FlexGrid.TreeStyleFlags.ButtonBar), C1.Win.C1FlexGrid.TreeStyleFlags)
    '
    'ContextMenu_Valuation
    '
    Me.ContextMenu_Valuation.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_Valuation_Description, Me.ToolStripSeparator1, Me.Menu_Valuation_AcceptPrice, Me.Menu_Valuation_ViewPricesForm, Me.ToolStripSeparator2, Me.Menu_Valuation_AcceptFXRate, Me.Menu_Valuation_Separator, Me.Menu_Valuation_ViewTransactions, Me.Menu_Valuation_ViewInstrument})
    Me.ContextMenu_Valuation.Name = "ContextMenu_Valuation"
    Me.ContextMenu_Valuation.Size = New System.Drawing.Size(258, 154)
    '
    'Menu_Valuation_Description
    '
    Me.Menu_Valuation_Description.Name = "Menu_Valuation_Description"
    Me.Menu_Valuation_Description.Size = New System.Drawing.Size(257, 22)
    Me.Menu_Valuation_Description.Text = "."
    '
    'ToolStripSeparator1
    '
    Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
    Me.ToolStripSeparator1.Size = New System.Drawing.Size(254, 6)
    '
    'Menu_Valuation_AcceptPrice
    '
    Me.Menu_Valuation_AcceptPrice.Name = "Menu_Valuation_AcceptPrice"
    Me.Menu_Valuation_AcceptPrice.Size = New System.Drawing.Size(257, 22)
    Me.Menu_Valuation_AcceptPrice.Text = "Accept Administrator Price"
    '
    'Menu_Valuation_ViewPricesForm
    '
    Me.Menu_Valuation_ViewPricesForm.Name = "Menu_Valuation_ViewPricesForm"
    Me.Menu_Valuation_ViewPricesForm.Size = New System.Drawing.Size(257, 22)
    Me.Menu_Valuation_ViewPricesForm.Text = "View Prices Form"
    '
    'ToolStripSeparator2
    '
    Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
    Me.ToolStripSeparator2.Size = New System.Drawing.Size(254, 6)
    '
    'Menu_Valuation_AcceptFXRate
    '
    Me.Menu_Valuation_AcceptFXRate.Name = "Menu_Valuation_AcceptFXRate"
    Me.Menu_Valuation_AcceptFXRate.Size = New System.Drawing.Size(257, 22)
    Me.Menu_Valuation_AcceptFXRate.Text = "Accept ALL Administrator FX Rates"
    '
    'Menu_Valuation_Separator
    '
    Me.Menu_Valuation_Separator.Name = "Menu_Valuation_Separator"
    Me.Menu_Valuation_Separator.Size = New System.Drawing.Size(254, 6)
    '
    'Menu_Valuation_ViewTransactions
    '
    Me.Menu_Valuation_ViewTransactions.Name = "Menu_Valuation_ViewTransactions"
    Me.Menu_Valuation_ViewTransactions.Size = New System.Drawing.Size(257, 22)
    Me.Menu_Valuation_ViewTransactions.Text = "View Transactions"
    '
    'Menu_Valuation_ViewInstrument
    '
    Me.Menu_Valuation_ViewInstrument.Name = "Menu_Valuation_ViewInstrument"
    Me.Menu_Valuation_ViewInstrument.Size = New System.Drawing.Size(257, 22)
    Me.Menu_Valuation_ViewInstrument.Text = "View Instrument definition"
    '
    'Tab_Notional
    '
    Me.Tab_Notional.Controls.Add(Me.Grid_NotionalExposure)
    Me.Tab_Notional.Controls.Add(Me.Combo_UnRealisedCalculationType)
    Me.Tab_Notional.Controls.Add(Me.Label1)
    Me.Tab_Notional.Controls.Add(Me.Button_TradeSelectedRealised)
    Me.Tab_Notional.Controls.Add(Me.Button_TradeSelectedUnRealised)
    Me.Tab_Notional.Controls.Add(Me.Combo_FuturesInstrument)
    Me.Tab_Notional.Controls.Add(Me.Label2)
    Me.Tab_Notional.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Notional.Name = "Tab_Notional"
    Me.Tab_Notional.Padding = New System.Windows.Forms.Padding(3)
    Me.Tab_Notional.Size = New System.Drawing.Size(1056, 511)
    Me.Tab_Notional.TabIndex = 0
    Me.Tab_Notional.Text = "Futures Exposure"
    Me.Tab_Notional.UseVisualStyleBackColor = True
    '
    'Tab_Availabilities
    '
    Me.Tab_Availabilities.Controls.Add(Me.Button_BookMarginDeposits)
    Me.Tab_Availabilities.Controls.Add(Me.Grid_Retrocessions)
    Me.Tab_Availabilities.Controls.Add(Me.Label64)
    Me.Tab_Availabilities.Controls.Add(Me.Button_BookRetrocessions)
    Me.Tab_Availabilities.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Availabilities.Name = "Tab_Availabilities"
    Me.Tab_Availabilities.Size = New System.Drawing.Size(1056, 511)
    Me.Tab_Availabilities.TabIndex = 7
    Me.Tab_Availabilities.Text = "Availabilities"
    Me.Tab_Availabilities.UseVisualStyleBackColor = True
    '
    'Button_BookMarginDeposits
    '
    Me.Button_BookMarginDeposits.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Button_BookMarginDeposits.Location = New System.Drawing.Point(212, 472)
    Me.Button_BookMarginDeposits.Name = "Button_BookMarginDeposits"
    Me.Button_BookMarginDeposits.Size = New System.Drawing.Size(176, 25)
    Me.Button_BookMarginDeposits.TabIndex = 215
    Me.Button_BookMarginDeposits.Text = "Book Margin Deposits"
    Me.Button_BookMarginDeposits.UseVisualStyleBackColor = True
    '
    'Grid_Retrocessions
    '
    Me.Grid_Retrocessions.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_Retrocessions.AutoGenerateColumns = False
    Me.Grid_Retrocessions.ColumnInfo = resources.GetString("Grid_Retrocessions.ColumnInfo")
    Me.Grid_Retrocessions.Cursor = System.Windows.Forms.Cursors.Default
    Me.Grid_Retrocessions.DropMode = C1.Win.C1FlexGrid.DropModeEnum.Manual
    Me.Grid_Retrocessions.Location = New System.Drawing.Point(11, 36)
    Me.Grid_Retrocessions.Name = "Grid_Retrocessions"
    Me.Grid_Retrocessions.Rows.DefaultSize = 17
    Me.Grid_Retrocessions.Size = New System.Drawing.Size(1042, 418)
    Me.Grid_Retrocessions.TabIndex = 214
    Me.Grid_Retrocessions.Tree.Column = 0
    Me.Grid_Retrocessions.Tree.Style = CType(((C1.Win.C1FlexGrid.TreeStyleFlags.Lines Or C1.Win.C1FlexGrid.TreeStyleFlags.Symbols) _
                Or C1.Win.C1FlexGrid.TreeStyleFlags.ButtonBar), C1.Win.C1FlexGrid.TreeStyleFlags)
    '
    'Label64
    '
    Me.Label64.AutoSize = True
    Me.Label64.Location = New System.Drawing.Point(8, 9)
    Me.Label64.Name = "Label64"
    Me.Label64.Size = New System.Drawing.Size(104, 13)
    Me.Label64.TabIndex = 213
    Me.Label64.Text = "Retrocession Details"
    '
    'Button_BookRetrocessions
    '
    Me.Button_BookRetrocessions.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Button_BookRetrocessions.Location = New System.Drawing.Point(11, 472)
    Me.Button_BookRetrocessions.Name = "Button_BookRetrocessions"
    Me.Button_BookRetrocessions.Size = New System.Drawing.Size(176, 25)
    Me.Button_BookRetrocessions.TabIndex = 160
    Me.Button_BookRetrocessions.Text = "Book Retrocessions"
    Me.Button_BookRetrocessions.UseVisualStyleBackColor = True
    '
    'Tab_AutomatedFees
    '
    Me.Tab_AutomatedFees.Controls.Add(Me.Button_AutomatedFees_SelectNone)
    Me.Tab_AutomatedFees.Controls.Add(Me.Button_AutomatedFees_SelectAll)
    Me.Tab_AutomatedFees.Controls.Add(Me.Button_BookSelectedAutomatedFees)
    Me.Tab_AutomatedFees.Controls.Add(Me.Grid_AutomatedFees)
    Me.Tab_AutomatedFees.Location = New System.Drawing.Point(4, 22)
    Me.Tab_AutomatedFees.Name = "Tab_AutomatedFees"
    Me.Tab_AutomatedFees.Size = New System.Drawing.Size(1056, 511)
    Me.Tab_AutomatedFees.TabIndex = 9
    Me.Tab_AutomatedFees.Text = "Automated Fees"
    Me.Tab_AutomatedFees.UseVisualStyleBackColor = True
    '
    'Button_AutomatedFees_SelectNone
    '
    Me.Button_AutomatedFees_SelectNone.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Button_AutomatedFees_SelectNone.Location = New System.Drawing.Point(133, 486)
    Me.Button_AutomatedFees_SelectNone.Name = "Button_AutomatedFees_SelectNone"
    Me.Button_AutomatedFees_SelectNone.Size = New System.Drawing.Size(123, 22)
    Me.Button_AutomatedFees_SelectNone.TabIndex = 93
    Me.Button_AutomatedFees_SelectNone.Text = "Select None"
    Me.Button_AutomatedFees_SelectNone.UseVisualStyleBackColor = True
    '
    'Button_AutomatedFees_SelectAll
    '
    Me.Button_AutomatedFees_SelectAll.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Button_AutomatedFees_SelectAll.Location = New System.Drawing.Point(4, 486)
    Me.Button_AutomatedFees_SelectAll.Name = "Button_AutomatedFees_SelectAll"
    Me.Button_AutomatedFees_SelectAll.Size = New System.Drawing.Size(123, 22)
    Me.Button_AutomatedFees_SelectAll.TabIndex = 92
    Me.Button_AutomatedFees_SelectAll.Text = "Select All"
    Me.Button_AutomatedFees_SelectAll.UseVisualStyleBackColor = True
    '
    'Button_BookSelectedAutomatedFees
    '
    Me.Button_BookSelectedAutomatedFees.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Button_BookSelectedAutomatedFees.Location = New System.Drawing.Point(877, 483)
    Me.Button_BookSelectedAutomatedFees.Name = "Button_BookSelectedAutomatedFees"
    Me.Button_BookSelectedAutomatedFees.Size = New System.Drawing.Size(176, 25)
    Me.Button_BookSelectedAutomatedFees.TabIndex = 28
    Me.Button_BookSelectedAutomatedFees.Text = "Book Selected Fees."
    Me.Button_BookSelectedAutomatedFees.UseVisualStyleBackColor = True
    '
    'Grid_AutomatedFees
    '
    Me.Grid_AutomatedFees.AllowEditing = False
    Me.Grid_AutomatedFees.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_AutomatedFees.CausesValidation = False
    Me.Grid_AutomatedFees.ColumnInfo = "5,1,0,0,0,85,Columns:"
    Me.Grid_AutomatedFees.EditOptions = C1.Win.C1FlexGrid.EditFlags.None
    Me.Grid_AutomatedFees.KeyActionTab = C1.Win.C1FlexGrid.KeyActionEnum.MoveAcross
    Me.Grid_AutomatedFees.Location = New System.Drawing.Point(4, 5)
    Me.Grid_AutomatedFees.Name = "Grid_AutomatedFees"
    Me.Grid_AutomatedFees.Rows.DefaultSize = 17
    Me.Grid_AutomatedFees.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.ListBox
    Me.Grid_AutomatedFees.Size = New System.Drawing.Size(1049, 472)
    Me.Grid_AutomatedFees.TabIndex = 16
    '
    'Tab_FeesMultiClass
    '
    Me.Tab_FeesMultiClass.Controls.Add(Me.GroupBox7)
    Me.Tab_FeesMultiClass.Controls.Add(Me.Button_Fee_BookCrystalised)
    Me.Tab_FeesMultiClass.Controls.Add(Me.Button_Fee_BookPerformance)
    Me.Tab_FeesMultiClass.Controls.Add(Me.Button_Fee_BookManagement)
    Me.Tab_FeesMultiClass.Controls.Add(Me.Grid_FeesMultiClass)
    Me.Tab_FeesMultiClass.Location = New System.Drawing.Point(4, 22)
    Me.Tab_FeesMultiClass.Name = "Tab_FeesMultiClass"
    Me.Tab_FeesMultiClass.Padding = New System.Windows.Forms.Padding(3)
    Me.Tab_FeesMultiClass.Size = New System.Drawing.Size(1056, 511)
    Me.Tab_FeesMultiClass.TabIndex = 8
    Me.Tab_FeesMultiClass.Text = "Fees"
    Me.Tab_FeesMultiClass.UseVisualStyleBackColor = True
    '
    'GroupBox7
    '
    Me.GroupBox7.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.GroupBox7.Controls.Add(Me.Check_fee_UpdateOrders)
    Me.GroupBox7.Controls.Add(Me.Button_Fee_SetMilestone)
    Me.GroupBox7.Location = New System.Drawing.Point(243, 418)
    Me.GroupBox7.Name = "GroupBox7"
    Me.GroupBox7.Size = New System.Drawing.Size(305, 87)
    Me.GroupBox7.TabIndex = 31
    Me.GroupBox7.TabStop = False
    Me.GroupBox7.Text = "End Of Day"
    '
    'Check_fee_UpdateOrders
    '
    Me.Check_fee_UpdateOrders.AutoSize = True
    Me.Check_fee_UpdateOrders.Location = New System.Drawing.Point(19, 20)
    Me.Check_fee_UpdateOrders.Name = "Check_fee_UpdateOrders"
    Me.Check_fee_UpdateOrders.Size = New System.Drawing.Size(272, 17)
    Me.Check_fee_UpdateOrders.TabIndex = 0
    Me.Check_fee_UpdateOrders.Text = "Automatically Update Orders with Administrator Price"
    Me.Check_fee_UpdateOrders.UseVisualStyleBackColor = True
    '
    'Button_Fee_SetMilestone
    '
    Me.Button_Fee_SetMilestone.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Button_Fee_SetMilestone.Location = New System.Drawing.Point(18, 43)
    Me.Button_Fee_SetMilestone.Name = "Button_Fee_SetMilestone"
    Me.Button_Fee_SetMilestone.Size = New System.Drawing.Size(168, 38)
    Me.Button_Fee_SetMilestone.TabIndex = 14
    Me.Button_Fee_SetMilestone.Text = "Set EOD / Milestone"
    '
    'Button_Fee_BookCrystalised
    '
    Me.Button_Fee_BookCrystalised.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Button_Fee_BookCrystalised.Location = New System.Drawing.Point(6, 480)
    Me.Button_Fee_BookCrystalised.Name = "Button_Fee_BookCrystalised"
    Me.Button_Fee_BookCrystalised.Size = New System.Drawing.Size(176, 25)
    Me.Button_Fee_BookCrystalised.TabIndex = 30
    Me.Button_Fee_BookCrystalised.Text = "Book Crystalised Fees"
    Me.Button_Fee_BookCrystalised.UseVisualStyleBackColor = True
    '
    'Button_Fee_BookPerformance
    '
    Me.Button_Fee_BookPerformance.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Button_Fee_BookPerformance.Location = New System.Drawing.Point(6, 449)
    Me.Button_Fee_BookPerformance.Name = "Button_Fee_BookPerformance"
    Me.Button_Fee_BookPerformance.Size = New System.Drawing.Size(176, 25)
    Me.Button_Fee_BookPerformance.TabIndex = 29
    Me.Button_Fee_BookPerformance.Text = "Book Performance Fees"
    Me.Button_Fee_BookPerformance.UseVisualStyleBackColor = True
    '
    'Button_Fee_BookManagement
    '
    Me.Button_Fee_BookManagement.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Button_Fee_BookManagement.Location = New System.Drawing.Point(6, 418)
    Me.Button_Fee_BookManagement.Name = "Button_Fee_BookManagement"
    Me.Button_Fee_BookManagement.Size = New System.Drawing.Size(176, 25)
    Me.Button_Fee_BookManagement.TabIndex = 28
    Me.Button_Fee_BookManagement.Text = "Book Management Fees"
    Me.Button_Fee_BookManagement.UseVisualStyleBackColor = True
    '
    'Grid_FeesMultiClass
    '
    Me.Grid_FeesMultiClass.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None
    Me.Grid_FeesMultiClass.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_FeesMultiClass.AutoGenerateColumns = False
    Me.Grid_FeesMultiClass.ColumnInfo = resources.GetString("Grid_FeesMultiClass.ColumnInfo")
    Me.Grid_FeesMultiClass.ExtendLastCol = True
    Me.Grid_FeesMultiClass.Location = New System.Drawing.Point(6, 6)
    Me.Grid_FeesMultiClass.Name = "Grid_FeesMultiClass"
    Me.Grid_FeesMultiClass.Rows.DefaultSize = 17
    Me.Grid_FeesMultiClass.ShowSort = False
    Me.Grid_FeesMultiClass.Size = New System.Drawing.Size(1044, 394)
    Me.Grid_FeesMultiClass.StyleInfo = resources.GetString("Grid_FeesMultiClass.StyleInfo")
    Me.Grid_FeesMultiClass.TabIndex = 0
    Me.Grid_FeesMultiClass.Tree.Column = 0
    Me.Grid_FeesMultiClass.Tree.Style = CType(((C1.Win.C1FlexGrid.TreeStyleFlags.Lines Or C1.Win.C1FlexGrid.TreeStyleFlags.Symbols) _
                Or C1.Win.C1FlexGrid.TreeStyleFlags.ButtonBar), C1.Win.C1FlexGrid.TreeStyleFlags)
    '
    'Tab_Fees
    '
    Me.Tab_Fees.Controls.Add(Me.Panel1)
    Me.Tab_Fees.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Fees.Name = "Tab_Fees"
    Me.Tab_Fees.Size = New System.Drawing.Size(1056, 511)
    Me.Tab_Fees.TabIndex = 3
    Me.Tab_Fees.Text = "Fee Calculation"
    Me.Tab_Fees.UseVisualStyleBackColor = True
    '
    'Panel1
    '
    Me.Panel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Panel1.AutoScroll = True
    Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Panel1.Controls.Add(Me.Label65)
    Me.Panel1.Controls.Add(Me.Label_PerfFeesCrystalisedTotalBooked)
    Me.Panel1.Controls.Add(Me.Label30)
    Me.Panel1.Controls.Add(Me.Label_PerfFeesCrystalisedAdministrator)
    Me.Panel1.Controls.Add(Me.Label59)
    Me.Panel1.Controls.Add(Me.Label_PerfFeesAdministrator)
    Me.Panel1.Controls.Add(Me.Label57)
    Me.Panel1.Controls.Add(Me.Label_MgmtFeesAdministratorValue)
    Me.Panel1.Controls.Add(Me.Label50)
    Me.Panel1.Controls.Add(Me.Button_TradeCrystalisedFees)
    Me.Panel1.Controls.Add(Me.Button_TradePerformanceFees)
    Me.Panel1.Controls.Add(Me.Label_Fees_AdditionalCrystalisedTrade)
    Me.Panel1.Controls.Add(Me.Label_Fees_AdditionalManagementTrade)
    Me.Panel1.Controls.Add(Me.Label_Fees_AdditionalPerformanceTrade)
    Me.Panel1.Controls.Add(Me.Label36)
    Me.Panel1.Controls.Add(Me.Combo_PerformanceFeeCrystalisedInstrument)
    Me.Panel1.Controls.Add(Me.Label34)
    Me.Panel1.Controls.Add(Me.Label_PerfFeesCrystalisedAlreadyTaken)
    Me.Panel1.Controls.Add(Me.Numeric_PerfFeesCrystalisedToTake)
    Me.Panel1.Controls.Add(Me.Label27)
    Me.Panel1.Controls.Add(Me.Label32)
    Me.Panel1.Controls.Add(Me.Panel2)
    Me.Panel1.Controls.Add(Me.Combo_PerformanceFeeInstrument)
    Me.Panel1.Controls.Add(Me.Combo_ManagementFeeInstrument)
    Me.Panel1.Controls.Add(Me.Label_BenchmarkPerformance)
    Me.Panel1.Controls.Add(Me.Label26)
    Me.Panel1.Controls.Add(Me.Label_PerfFeesAlreadyTaken)
    Me.Panel1.Controls.Add(Me.Numeric_PerfFeesToTake)
    Me.Panel1.Controls.Add(Me.Date_PerfFeesSettlementDate)
    Me.Panel1.Controls.Add(Me.Date_MgmtFeesSettlementDate)
    Me.Panel1.Controls.Add(Me.Label_MgmtFeesAlreadyTaken)
    Me.Panel1.Controls.Add(Me.edit_ManagementFeesToTake)
    Me.Panel1.Controls.Add(Me.Label17)
    Me.Panel1.Controls.Add(Me.Label18)
    Me.Panel1.Controls.Add(Me.Label19)
    Me.Panel1.Controls.Add(Me.Label20)
    Me.Panel1.Controls.Add(Me.Label21)
    Me.Panel1.Controls.Add(Me.Label_FundPerformance)
    Me.Panel1.Controls.Add(Me.Label23)
    Me.Panel1.Controls.Add(Me.Label_PerfFeesRate)
    Me.Panel1.Controls.Add(Me.Label25)
    Me.Panel1.Controls.Add(Me.Label16)
    Me.Panel1.Controls.Add(Me.Button_TradeManagementFees)
    Me.Panel1.Controls.Add(Me.Label15)
    Me.Panel1.Controls.Add(Me.Label14)
    Me.Panel1.Controls.Add(Me.Label13)
    Me.Panel1.Controls.Add(Me.Label12)
    Me.Panel1.Controls.Add(Me.Label10)
    Me.Panel1.Controls.Add(Me.Label_MgmtFeeNAV)
    Me.Panel1.Controls.Add(Me.Label11)
    Me.Panel1.Controls.Add(Me.Label_MgmtFeesRate)
    Me.Panel1.Controls.Add(Me.Label9)
    Me.Panel1.Controls.Add(Me.Label7)
    Me.Panel1.Location = New System.Drawing.Point(4, 3)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(1059, 505)
    Me.Panel1.TabIndex = 3
    '
    'Label65
    '
    Me.Label65.Location = New System.Drawing.Point(327, 356)
    Me.Label65.Name = "Label65"
    Me.Label65.Size = New System.Drawing.Size(112, 16)
    Me.Label65.TabIndex = 150
    Me.Label65.Text = "Total booked."
    '
    'Label_PerfFeesCrystalisedTotalBooked
    '
    Me.Label_PerfFeesCrystalisedTotalBooked.Location = New System.Drawing.Point(445, 356)
    Me.Label_PerfFeesCrystalisedTotalBooked.Name = "Label_PerfFeesCrystalisedTotalBooked"
    Me.Label_PerfFeesCrystalisedTotalBooked.Size = New System.Drawing.Size(117, 16)
    Me.Label_PerfFeesCrystalisedTotalBooked.TabIndex = 149
    Me.Label_PerfFeesCrystalisedTotalBooked.Text = "."
    Me.Label_PerfFeesCrystalisedTotalBooked.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Label30
    '
    Me.Label30.Location = New System.Drawing.Point(327, 310)
    Me.Label30.Name = "Label30"
    Me.Label30.Size = New System.Drawing.Size(149, 16)
    Me.Label30.TabIndex = 130
    Me.Label30.Text = "Already booked, today."
    '
    'Label_PerfFeesCrystalisedAdministrator
    '
    Me.Label_PerfFeesCrystalisedAdministrator.Location = New System.Drawing.Point(445, 287)
    Me.Label_PerfFeesCrystalisedAdministrator.Name = "Label_PerfFeesCrystalisedAdministrator"
    Me.Label_PerfFeesCrystalisedAdministrator.Size = New System.Drawing.Size(117, 16)
    Me.Label_PerfFeesCrystalisedAdministrator.TabIndex = 18
    Me.Label_PerfFeesCrystalisedAdministrator.Text = "."
    Me.Label_PerfFeesCrystalisedAdministrator.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Label59
    '
    Me.Label59.Location = New System.Drawing.Point(327, 287)
    Me.Label59.Name = "Label59"
    Me.Label59.Size = New System.Drawing.Size(115, 16)
    Me.Label59.TabIndex = 148
    Me.Label59.Text = "Administrator Value"
    '
    'Label_PerfFeesAdministrator
    '
    Me.Label_PerfFeesAdministrator.Location = New System.Drawing.Point(445, 124)
    Me.Label_PerfFeesAdministrator.Name = "Label_PerfFeesAdministrator"
    Me.Label_PerfFeesAdministrator.Size = New System.Drawing.Size(117, 16)
    Me.Label_PerfFeesAdministrator.TabIndex = 12
    Me.Label_PerfFeesAdministrator.Text = "."
    Me.Label_PerfFeesAdministrator.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Label57
    '
    Me.Label57.Location = New System.Drawing.Point(327, 124)
    Me.Label57.Name = "Label57"
    Me.Label57.Size = New System.Drawing.Size(115, 16)
    Me.Label57.TabIndex = 146
    Me.Label57.Text = "Administrator Value"
    '
    'Label_MgmtFeesAdministratorValue
    '
    Me.Label_MgmtFeesAdministratorValue.Location = New System.Drawing.Point(140, 101)
    Me.Label_MgmtFeesAdministratorValue.Name = "Label_MgmtFeesAdministratorValue"
    Me.Label_MgmtFeesAdministratorValue.Size = New System.Drawing.Size(117, 16)
    Me.Label_MgmtFeesAdministratorValue.TabIndex = 3
    Me.Label_MgmtFeesAdministratorValue.Text = "."
    Me.Label_MgmtFeesAdministratorValue.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Label50
    '
    Me.Label50.Location = New System.Drawing.Point(20, 101)
    Me.Label50.Name = "Label50"
    Me.Label50.Size = New System.Drawing.Size(120, 16)
    Me.Label50.TabIndex = 144
    Me.Label50.Text = "Administrator Value"
    '
    'Button_TradeCrystalisedFees
    '
    Me.Button_TradeCrystalisedFees.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Button_TradeCrystalisedFees.Location = New System.Drawing.Point(23, 408)
    Me.Button_TradeCrystalisedFees.Name = "Button_TradeCrystalisedFees"
    Me.Button_TradeCrystalisedFees.Size = New System.Drawing.Size(176, 25)
    Me.Button_TradeCrystalisedFees.TabIndex = 29
    Me.Button_TradeCrystalisedFees.Text = "Book Crystalised Fees"
    Me.Button_TradeCrystalisedFees.UseVisualStyleBackColor = True
    '
    'Button_TradePerformanceFees
    '
    Me.Button_TradePerformanceFees.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Button_TradePerformanceFees.Location = New System.Drawing.Point(23, 366)
    Me.Button_TradePerformanceFees.Name = "Button_TradePerformanceFees"
    Me.Button_TradePerformanceFees.Size = New System.Drawing.Size(176, 25)
    Me.Button_TradePerformanceFees.TabIndex = 28
    Me.Button_TradePerformanceFees.Text = "Book Performance Fees"
    Me.Button_TradePerformanceFees.UseVisualStyleBackColor = True
    '
    'Label_Fees_AdditionalCrystalisedTrade
    '
    Me.Label_Fees_AdditionalCrystalisedTrade.Location = New System.Drawing.Point(445, 333)
    Me.Label_Fees_AdditionalCrystalisedTrade.Name = "Label_Fees_AdditionalCrystalisedTrade"
    Me.Label_Fees_AdditionalCrystalisedTrade.Size = New System.Drawing.Size(117, 16)
    Me.Label_Fees_AdditionalCrystalisedTrade.TabIndex = 20
    Me.Label_Fees_AdditionalCrystalisedTrade.Text = "."
    Me.Label_Fees_AdditionalCrystalisedTrade.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Label_Fees_AdditionalManagementTrade
    '
    Me.Label_Fees_AdditionalManagementTrade.Location = New System.Drawing.Point(140, 147)
    Me.Label_Fees_AdditionalManagementTrade.Name = "Label_Fees_AdditionalManagementTrade"
    Me.Label_Fees_AdditionalManagementTrade.Size = New System.Drawing.Size(117, 16)
    Me.Label_Fees_AdditionalManagementTrade.TabIndex = 5
    Me.Label_Fees_AdditionalManagementTrade.Text = "."
    Me.Label_Fees_AdditionalManagementTrade.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Label_Fees_AdditionalPerformanceTrade
    '
    Me.Label_Fees_AdditionalPerformanceTrade.Location = New System.Drawing.Point(445, 170)
    Me.Label_Fees_AdditionalPerformanceTrade.Name = "Label_Fees_AdditionalPerformanceTrade"
    Me.Label_Fees_AdditionalPerformanceTrade.Size = New System.Drawing.Size(117, 16)
    Me.Label_Fees_AdditionalPerformanceTrade.TabIndex = 14
    Me.Label_Fees_AdditionalPerformanceTrade.Text = "."
    Me.Label_Fees_AdditionalPerformanceTrade.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Label36
    '
    Me.Label36.Location = New System.Drawing.Point(313, 243)
    Me.Label36.Name = "Label36"
    Me.Label36.Size = New System.Drawing.Size(221, 16)
    Me.Label36.TabIndex = 137
    Me.Label36.Text = "Performance Fees, Crystalised"
    '
    'Combo_PerformanceFeeCrystalisedInstrument
    '
    Me.Combo_PerformanceFeeCrystalisedInstrument.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_PerformanceFeeCrystalisedInstrument.Location = New System.Drawing.Point(445, 379)
    Me.Combo_PerformanceFeeCrystalisedInstrument.Name = "Combo_PerformanceFeeCrystalisedInstrument"
    Me.Combo_PerformanceFeeCrystalisedInstrument.Size = New System.Drawing.Size(117, 21)
    Me.Combo_PerformanceFeeCrystalisedInstrument.TabIndex = 21
    '
    'Label34
    '
    Me.Label34.Location = New System.Drawing.Point(327, 382)
    Me.Label34.Name = "Label34"
    Me.Label34.Size = New System.Drawing.Size(112, 16)
    Me.Label34.TabIndex = 135
    Me.Label34.Text = "Instrument"
    '
    'Label_PerfFeesCrystalisedAlreadyTaken
    '
    Me.Label_PerfFeesCrystalisedAlreadyTaken.Location = New System.Drawing.Point(445, 310)
    Me.Label_PerfFeesCrystalisedAlreadyTaken.Name = "Label_PerfFeesCrystalisedAlreadyTaken"
    Me.Label_PerfFeesCrystalisedAlreadyTaken.Size = New System.Drawing.Size(117, 16)
    Me.Label_PerfFeesCrystalisedAlreadyTaken.TabIndex = 19
    Me.Label_PerfFeesCrystalisedAlreadyTaken.Text = "."
    Me.Label_PerfFeesCrystalisedAlreadyTaken.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Numeric_PerfFeesCrystalisedToTake
    '
    Me.Numeric_PerfFeesCrystalisedToTake.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
    Me.Numeric_PerfFeesCrystalisedToTake.Location = New System.Drawing.Point(445, 262)
    Me.Numeric_PerfFeesCrystalisedToTake.Name = "Numeric_PerfFeesCrystalisedToTake"
    Me.Numeric_PerfFeesCrystalisedToTake.RenaissanceTag = Nothing
    Me.Numeric_PerfFeesCrystalisedToTake.SelectTextOnFocus = False
    Me.Numeric_PerfFeesCrystalisedToTake.Size = New System.Drawing.Size(117, 20)
    Me.Numeric_PerfFeesCrystalisedToTake.TabIndex = 17
    Me.Numeric_PerfFeesCrystalisedToTake.Text = "0.00"
    Me.Numeric_PerfFeesCrystalisedToTake.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.Numeric_PerfFeesCrystalisedToTake.TextFormat = "#,##0.00"
    Me.Numeric_PerfFeesCrystalisedToTake.Value = 0
    '
    'Label27
    '
    Me.Label27.Location = New System.Drawing.Point(327, 333)
    Me.Label27.Name = "Label27"
    Me.Label27.Size = New System.Drawing.Size(112, 16)
    Me.Label27.TabIndex = 131
    Me.Label27.Text = "Additional Trade"
    '
    'Label32
    '
    Me.Label32.Location = New System.Drawing.Point(327, 265)
    Me.Label32.Name = "Label32"
    Me.Label32.Size = New System.Drawing.Size(112, 16)
    Me.Label32.TabIndex = 129
    Me.Label32.Text = "Todays Fees to take"
    '
    'Panel2
    '
    Me.Panel2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Panel2.Controls.Add(Me.Label_Fees_LastBenchmarkDate)
    Me.Panel2.Controls.Add(Me.Label66)
    Me.Panel2.Controls.Add(Me.Label_Fees_BenchmarkNAV)
    Me.Panel2.Controls.Add(Me.Label35)
    Me.Panel2.Controls.Add(Me.Label_Fees_PreviousPerformanceFees)
    Me.Panel2.Controls.Add(Me.Label41)
    Me.Panel2.Controls.Add(Me.Label_Fees_PreviousUnitsIssued)
    Me.Panel2.Controls.Add(Me.Label40)
    Me.Panel2.Controls.Add(Me.Label_Fees_TodaysUnitRedemptions)
    Me.Panel2.Controls.Add(Me.Label38)
    Me.Panel2.Controls.Add(Me.Label_Fees_CrystalisedToday)
    Me.Panel2.Controls.Add(Me.Label22)
    Me.Panel2.Controls.Add(Me.Label_Fees_FundNAV)
    Me.Panel2.Controls.Add(Me.Label6)
    Me.Panel2.Controls.Add(Me.Label_Fees_BestBenchmark)
    Me.Panel2.Controls.Add(Me.Label51)
    Me.Panel2.Controls.Add(Me.Label_Fees_GNAV)
    Me.Panel2.Controls.Add(Me.Label37)
    Me.Panel2.Controls.Add(Me.Label_Fees_PerfFeesThisYear)
    Me.Panel2.Controls.Add(Me.Label39)
    Me.Panel2.Controls.Add(Me.Label_Fees_YearRedemptions)
    Me.Panel2.Controls.Add(Me.Label33)
    Me.Panel2.Controls.Add(Me.Label_Fees_YearSubscriptions)
    Me.Panel2.Controls.Add(Me.Label31)
    Me.Panel2.Controls.Add(Me.Label_Fees_YearStartNAV)
    Me.Panel2.Controls.Add(Me.Label29)
    Me.Panel2.Controls.Add(Me.Label_Fees_FinancialYear)
    Me.Panel2.Controls.Add(Me.Label28)
    Me.Panel2.Controls.Add(Me.Label24)
    Me.Panel2.Location = New System.Drawing.Point(690, 2)
    Me.Panel2.Name = "Panel2"
    Me.Panel2.Size = New System.Drawing.Size(358, 498)
    Me.Panel2.TabIndex = 128
    '
    'Label_Fees_LastBenchmarkDate
    '
    Me.Label_Fees_LastBenchmarkDate.Location = New System.Drawing.Point(174, 255)
    Me.Label_Fees_LastBenchmarkDate.Name = "Label_Fees_LastBenchmarkDate"
    Me.Label_Fees_LastBenchmarkDate.Size = New System.Drawing.Size(165, 16)
    Me.Label_Fees_LastBenchmarkDate.TabIndex = 142
    Me.Label_Fees_LastBenchmarkDate.Text = "."
    Me.Label_Fees_LastBenchmarkDate.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Label66
    '
    Me.Label66.Location = New System.Drawing.Point(15, 255)
    Me.Label66.Name = "Label66"
    Me.Label66.Size = New System.Drawing.Size(162, 16)
    Me.Label66.TabIndex = 141
    Me.Label66.Text = "Last Benchmark Date"
    '
    'Label_Fees_BenchmarkNAV
    '
    Me.Label_Fees_BenchmarkNAV.Location = New System.Drawing.Point(174, 235)
    Me.Label_Fees_BenchmarkNAV.Name = "Label_Fees_BenchmarkNAV"
    Me.Label_Fees_BenchmarkNAV.Size = New System.Drawing.Size(165, 16)
    Me.Label_Fees_BenchmarkNAV.TabIndex = 140
    Me.Label_Fees_BenchmarkNAV.Text = "."
    Me.Label_Fees_BenchmarkNAV.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Label35
    '
    Me.Label35.Location = New System.Drawing.Point(15, 235)
    Me.Label35.Name = "Label35"
    Me.Label35.Size = New System.Drawing.Size(162, 16)
    Me.Label35.TabIndex = 139
    Me.Label35.Text = "Benchmark NAV"
    '
    'Label_Fees_PreviousPerformanceFees
    '
    Me.Label_Fees_PreviousPerformanceFees.Location = New System.Drawing.Point(174, 335)
    Me.Label_Fees_PreviousPerformanceFees.Name = "Label_Fees_PreviousPerformanceFees"
    Me.Label_Fees_PreviousPerformanceFees.Size = New System.Drawing.Size(165, 16)
    Me.Label_Fees_PreviousPerformanceFees.TabIndex = 138
    Me.Label_Fees_PreviousPerformanceFees.Text = "."
    Me.Label_Fees_PreviousPerformanceFees.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Label41
    '
    Me.Label41.Location = New System.Drawing.Point(15, 335)
    Me.Label41.Name = "Label41"
    Me.Label41.Size = New System.Drawing.Size(162, 16)
    Me.Label41.TabIndex = 137
    Me.Label41.Text = "Previous Performance Fees"
    '
    'Label_Fees_PreviousUnitsIssued
    '
    Me.Label_Fees_PreviousUnitsIssued.Location = New System.Drawing.Point(174, 315)
    Me.Label_Fees_PreviousUnitsIssued.Name = "Label_Fees_PreviousUnitsIssued"
    Me.Label_Fees_PreviousUnitsIssued.Size = New System.Drawing.Size(165, 16)
    Me.Label_Fees_PreviousUnitsIssued.TabIndex = 136
    Me.Label_Fees_PreviousUnitsIssued.Text = "."
    Me.Label_Fees_PreviousUnitsIssued.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Label40
    '
    Me.Label40.Location = New System.Drawing.Point(15, 315)
    Me.Label40.Name = "Label40"
    Me.Label40.Size = New System.Drawing.Size(162, 16)
    Me.Label40.TabIndex = 135
    Me.Label40.Text = "Previous Units in issue"
    '
    'Label_Fees_TodaysUnitRedemptions
    '
    Me.Label_Fees_TodaysUnitRedemptions.Location = New System.Drawing.Point(174, 296)
    Me.Label_Fees_TodaysUnitRedemptions.Name = "Label_Fees_TodaysUnitRedemptions"
    Me.Label_Fees_TodaysUnitRedemptions.Size = New System.Drawing.Size(165, 16)
    Me.Label_Fees_TodaysUnitRedemptions.TabIndex = 134
    Me.Label_Fees_TodaysUnitRedemptions.Text = "."
    Me.Label_Fees_TodaysUnitRedemptions.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Label38
    '
    Me.Label38.Location = New System.Drawing.Point(15, 296)
    Me.Label38.Name = "Label38"
    Me.Label38.Size = New System.Drawing.Size(162, 16)
    Me.Label38.TabIndex = 133
    Me.Label38.Text = "Units Redeemed today"
    '
    'Label_Fees_CrystalisedToday
    '
    Me.Label_Fees_CrystalisedToday.Location = New System.Drawing.Point(174, 195)
    Me.Label_Fees_CrystalisedToday.Name = "Label_Fees_CrystalisedToday"
    Me.Label_Fees_CrystalisedToday.Size = New System.Drawing.Size(165, 16)
    Me.Label_Fees_CrystalisedToday.TabIndex = 132
    Me.Label_Fees_CrystalisedToday.Text = "."
    Me.Label_Fees_CrystalisedToday.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Label22
    '
    Me.Label22.Location = New System.Drawing.Point(15, 195)
    Me.Label22.Name = "Label22"
    Me.Label22.Size = New System.Drawing.Size(162, 16)
    Me.Label22.TabIndex = 131
    Me.Label22.Text = "Crystalised Perf Fees (today)"
    '
    'Label_Fees_FundNAV
    '
    Me.Label_Fees_FundNAV.Location = New System.Drawing.Point(174, 156)
    Me.Label_Fees_FundNAV.Name = "Label_Fees_FundNAV"
    Me.Label_Fees_FundNAV.Size = New System.Drawing.Size(165, 16)
    Me.Label_Fees_FundNAV.TabIndex = 130
    Me.Label_Fees_FundNAV.Text = "."
    Me.Label_Fees_FundNAV.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Label6
    '
    Me.Label6.Location = New System.Drawing.Point(15, 156)
    Me.Label6.Name = "Label6"
    Me.Label6.Size = New System.Drawing.Size(162, 16)
    Me.Label6.TabIndex = 129
    Me.Label6.Text = "Fund NAV"
    '
    'Label_Fees_BestBenchmark
    '
    Me.Label_Fees_BestBenchmark.Location = New System.Drawing.Point(174, 106)
    Me.Label_Fees_BestBenchmark.Name = "Label_Fees_BestBenchmark"
    Me.Label_Fees_BestBenchmark.Size = New System.Drawing.Size(165, 39)
    Me.Label_Fees_BestBenchmark.TabIndex = 126
    Me.Label_Fees_BestBenchmark.Text = "."
    Me.Label_Fees_BestBenchmark.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Label51
    '
    Me.Label51.Location = New System.Drawing.Point(15, 106)
    Me.Label51.Name = "Label51"
    Me.Label51.Size = New System.Drawing.Size(162, 16)
    Me.Label51.TabIndex = 125
    Me.Label51.Text = "Best benchmark."
    '
    'Label_Fees_GNAV
    '
    Me.Label_Fees_GNAV.Location = New System.Drawing.Point(174, 215)
    Me.Label_Fees_GNAV.Name = "Label_Fees_GNAV"
    Me.Label_Fees_GNAV.Size = New System.Drawing.Size(165, 16)
    Me.Label_Fees_GNAV.TabIndex = 122
    Me.Label_Fees_GNAV.Text = "."
    Me.Label_Fees_GNAV.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Label37
    '
    Me.Label37.Location = New System.Drawing.Point(15, 215)
    Me.Label37.Name = "Label37"
    Me.Label37.Size = New System.Drawing.Size(162, 16)
    Me.Label37.TabIndex = 121
    Me.Label37.Text = "Fund GNAV (for Perf. calc)"
    '
    'Label_Fees_PerfFeesThisYear
    '
    Me.Label_Fees_PerfFeesThisYear.Location = New System.Drawing.Point(174, 176)
    Me.Label_Fees_PerfFeesThisYear.Name = "Label_Fees_PerfFeesThisYear"
    Me.Label_Fees_PerfFeesThisYear.Size = New System.Drawing.Size(165, 16)
    Me.Label_Fees_PerfFeesThisYear.TabIndex = 120
    Me.Label_Fees_PerfFeesThisYear.Text = "."
    Me.Label_Fees_PerfFeesThisYear.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Label39
    '
    Me.Label39.Location = New System.Drawing.Point(15, 176)
    Me.Label39.Name = "Label39"
    Me.Label39.Size = New System.Drawing.Size(162, 16)
    Me.Label39.TabIndex = 119
    Me.Label39.Text = "Perf. fees booked this Year"
    '
    'Label_Fees_YearRedemptions
    '
    Me.Label_Fees_YearRedemptions.Location = New System.Drawing.Point(174, 80)
    Me.Label_Fees_YearRedemptions.Name = "Label_Fees_YearRedemptions"
    Me.Label_Fees_YearRedemptions.Size = New System.Drawing.Size(165, 16)
    Me.Label_Fees_YearRedemptions.TabIndex = 118
    Me.Label_Fees_YearRedemptions.Text = "."
    Me.Label_Fees_YearRedemptions.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Label33
    '
    Me.Label33.Location = New System.Drawing.Point(15, 80)
    Me.Label33.Name = "Label33"
    Me.Label33.Size = New System.Drawing.Size(162, 16)
    Me.Label33.TabIndex = 117
    Me.Label33.Text = "Redemptions this Year"
    '
    'Label_Fees_YearSubscriptions
    '
    Me.Label_Fees_YearSubscriptions.Location = New System.Drawing.Point(174, 62)
    Me.Label_Fees_YearSubscriptions.Name = "Label_Fees_YearSubscriptions"
    Me.Label_Fees_YearSubscriptions.Size = New System.Drawing.Size(165, 16)
    Me.Label_Fees_YearSubscriptions.TabIndex = 116
    Me.Label_Fees_YearSubscriptions.Text = "."
    Me.Label_Fees_YearSubscriptions.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Label31
    '
    Me.Label31.Location = New System.Drawing.Point(15, 62)
    Me.Label31.Name = "Label31"
    Me.Label31.Size = New System.Drawing.Size(162, 16)
    Me.Label31.TabIndex = 115
    Me.Label31.Text = "Subscriptions This Year"
    '
    'Label_Fees_YearStartNAV
    '
    Me.Label_Fees_YearStartNAV.Location = New System.Drawing.Point(174, 44)
    Me.Label_Fees_YearStartNAV.Name = "Label_Fees_YearStartNAV"
    Me.Label_Fees_YearStartNAV.Size = New System.Drawing.Size(165, 16)
    Me.Label_Fees_YearStartNAV.TabIndex = 114
    Me.Label_Fees_YearStartNAV.Text = "."
    Me.Label_Fees_YearStartNAV.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Label29
    '
    Me.Label29.Location = New System.Drawing.Point(15, 44)
    Me.Label29.Name = "Label29"
    Me.Label29.Size = New System.Drawing.Size(162, 16)
    Me.Label29.TabIndex = 113
    Me.Label29.Text = "Fund Year-Start NAV"
    '
    'Label_Fees_FinancialYear
    '
    Me.Label_Fees_FinancialYear.Location = New System.Drawing.Point(174, 25)
    Me.Label_Fees_FinancialYear.Name = "Label_Fees_FinancialYear"
    Me.Label_Fees_FinancialYear.Size = New System.Drawing.Size(165, 16)
    Me.Label_Fees_FinancialYear.TabIndex = 112
    Me.Label_Fees_FinancialYear.Text = "."
    Me.Label_Fees_FinancialYear.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Label28
    '
    Me.Label28.Location = New System.Drawing.Point(15, 25)
    Me.Label28.Name = "Label28"
    Me.Label28.Size = New System.Drawing.Size(162, 16)
    Me.Label28.TabIndex = 111
    Me.Label28.Text = "Financial Year"
    '
    'Label24
    '
    Me.Label24.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label24.Location = New System.Drawing.Point(7, 6)
    Me.Label24.Name = "Label24"
    Me.Label24.Size = New System.Drawing.Size(346, 16)
    Me.Label24.TabIndex = 109
    Me.Label24.Text = "Performance Fee Information"
    '
    'Combo_PerformanceFeeInstrument
    '
    Me.Combo_PerformanceFeeInstrument.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_PerformanceFeeInstrument.Location = New System.Drawing.Point(445, 215)
    Me.Combo_PerformanceFeeInstrument.Name = "Combo_PerformanceFeeInstrument"
    Me.Combo_PerformanceFeeInstrument.Size = New System.Drawing.Size(117, 21)
    Me.Combo_PerformanceFeeInstrument.TabIndex = 16
    '
    'Combo_ManagementFeeInstrument
    '
    Me.Combo_ManagementFeeInstrument.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_ManagementFeeInstrument.Location = New System.Drawing.Point(140, 192)
    Me.Combo_ManagementFeeInstrument.Name = "Combo_ManagementFeeInstrument"
    Me.Combo_ManagementFeeInstrument.Size = New System.Drawing.Size(117, 21)
    Me.Combo_ManagementFeeInstrument.TabIndex = 7
    '
    'Label_BenchmarkPerformance
    '
    Me.Label_BenchmarkPerformance.Location = New System.Drawing.Point(445, 74)
    Me.Label_BenchmarkPerformance.Name = "Label_BenchmarkPerformance"
    Me.Label_BenchmarkPerformance.Size = New System.Drawing.Size(117, 16)
    Me.Label_BenchmarkPerformance.TabIndex = 10
    Me.Label_BenchmarkPerformance.Text = "."
    Me.Label_BenchmarkPerformance.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Label26
    '
    Me.Label26.Location = New System.Drawing.Point(327, 77)
    Me.Label26.Name = "Label26"
    Me.Label26.Size = New System.Drawing.Size(112, 16)
    Me.Label26.TabIndex = 124
    Me.Label26.Text = "Benchmark Perf."
    '
    'Label_PerfFeesAlreadyTaken
    '
    Me.Label_PerfFeesAlreadyTaken.Location = New System.Drawing.Point(445, 147)
    Me.Label_PerfFeesAlreadyTaken.Name = "Label_PerfFeesAlreadyTaken"
    Me.Label_PerfFeesAlreadyTaken.Size = New System.Drawing.Size(117, 16)
    Me.Label_PerfFeesAlreadyTaken.TabIndex = 13
    Me.Label_PerfFeesAlreadyTaken.Text = "."
    Me.Label_PerfFeesAlreadyTaken.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Numeric_PerfFeesToTake
    '
    Me.Numeric_PerfFeesToTake.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
    Me.Numeric_PerfFeesToTake.Location = New System.Drawing.Point(445, 97)
    Me.Numeric_PerfFeesToTake.Name = "Numeric_PerfFeesToTake"
    Me.Numeric_PerfFeesToTake.RenaissanceTag = Nothing
    Me.Numeric_PerfFeesToTake.SelectTextOnFocus = False
    Me.Numeric_PerfFeesToTake.Size = New System.Drawing.Size(117, 20)
    Me.Numeric_PerfFeesToTake.TabIndex = 11
    Me.Numeric_PerfFeesToTake.Text = "0.00"
    Me.Numeric_PerfFeesToTake.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.Numeric_PerfFeesToTake.TextFormat = "#,##0.00"
    Me.Numeric_PerfFeesToTake.Value = 0
    '
    'Date_PerfFeesSettlementDate
    '
    Me.Date_PerfFeesSettlementDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
    Me.Date_PerfFeesSettlementDate.Location = New System.Drawing.Point(445, 191)
    Me.Date_PerfFeesSettlementDate.Name = "Date_PerfFeesSettlementDate"
    Me.Date_PerfFeesSettlementDate.Size = New System.Drawing.Size(117, 20)
    Me.Date_PerfFeesSettlementDate.TabIndex = 15
    '
    'Date_MgmtFeesSettlementDate
    '
    Me.Date_MgmtFeesSettlementDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
    Me.Date_MgmtFeesSettlementDate.Location = New System.Drawing.Point(140, 168)
    Me.Date_MgmtFeesSettlementDate.Name = "Date_MgmtFeesSettlementDate"
    Me.Date_MgmtFeesSettlementDate.Size = New System.Drawing.Size(117, 20)
    Me.Date_MgmtFeesSettlementDate.TabIndex = 6
    '
    'Label_MgmtFeesAlreadyTaken
    '
    Me.Label_MgmtFeesAlreadyTaken.Location = New System.Drawing.Point(140, 124)
    Me.Label_MgmtFeesAlreadyTaken.Name = "Label_MgmtFeesAlreadyTaken"
    Me.Label_MgmtFeesAlreadyTaken.Size = New System.Drawing.Size(117, 16)
    Me.Label_MgmtFeesAlreadyTaken.TabIndex = 4
    Me.Label_MgmtFeesAlreadyTaken.Text = "."
    Me.Label_MgmtFeesAlreadyTaken.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'edit_ManagementFeesToTake
    '
    Me.edit_ManagementFeesToTake.BackColor = System.Drawing.SystemColors.GradientActiveCaption
    Me.edit_ManagementFeesToTake.Location = New System.Drawing.Point(140, 74)
    Me.edit_ManagementFeesToTake.Name = "edit_ManagementFeesToTake"
    Me.edit_ManagementFeesToTake.RenaissanceTag = Nothing
    Me.edit_ManagementFeesToTake.SelectTextOnFocus = False
    Me.edit_ManagementFeesToTake.Size = New System.Drawing.Size(117, 20)
    Me.edit_ManagementFeesToTake.TabIndex = 2
    Me.edit_ManagementFeesToTake.Text = "0.00"
    Me.edit_ManagementFeesToTake.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.edit_ManagementFeesToTake.TextFormat = "#,##0.00"
    Me.edit_ManagementFeesToTake.Value = 0
    '
    'Label17
    '
    Me.Label17.Location = New System.Drawing.Point(327, 218)
    Me.Label17.Name = "Label17"
    Me.Label17.Size = New System.Drawing.Size(112, 16)
    Me.Label17.TabIndex = 115
    Me.Label17.Text = "Instrument"
    '
    'Label18
    '
    Me.Label18.Location = New System.Drawing.Point(327, 194)
    Me.Label18.Name = "Label18"
    Me.Label18.Size = New System.Drawing.Size(112, 16)
    Me.Label18.TabIndex = 114
    Me.Label18.Text = "Settlement Date"
    '
    'Label19
    '
    Me.Label19.Location = New System.Drawing.Point(327, 170)
    Me.Label19.Name = "Label19"
    Me.Label19.Size = New System.Drawing.Size(112, 16)
    Me.Label19.TabIndex = 113
    Me.Label19.Text = "Additional Trade"
    '
    'Label20
    '
    Me.Label20.Location = New System.Drawing.Point(327, 147)
    Me.Label20.Name = "Label20"
    Me.Label20.Size = New System.Drawing.Size(112, 16)
    Me.Label20.TabIndex = 112
    Me.Label20.Text = "Already booked"
    '
    'Label21
    '
    Me.Label21.Location = New System.Drawing.Point(327, 100)
    Me.Label21.Name = "Label21"
    Me.Label21.Size = New System.Drawing.Size(112, 16)
    Me.Label21.TabIndex = 111
    Me.Label21.Text = "Fees to take (YTD)"
    '
    'Label_FundPerformance
    '
    Me.Label_FundPerformance.Location = New System.Drawing.Point(445, 51)
    Me.Label_FundPerformance.Name = "Label_FundPerformance"
    Me.Label_FundPerformance.Size = New System.Drawing.Size(117, 16)
    Me.Label_FundPerformance.TabIndex = 9
    Me.Label_FundPerformance.Text = "."
    Me.Label_FundPerformance.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Label23
    '
    Me.Label23.Location = New System.Drawing.Point(327, 54)
    Me.Label23.Name = "Label23"
    Me.Label23.Size = New System.Drawing.Size(112, 16)
    Me.Label23.TabIndex = 109
    Me.Label23.Text = "Fund Performance"
    '
    'Label_PerfFeesRate
    '
    Me.Label_PerfFeesRate.Location = New System.Drawing.Point(445, 31)
    Me.Label_PerfFeesRate.Name = "Label_PerfFeesRate"
    Me.Label_PerfFeesRate.Size = New System.Drawing.Size(117, 16)
    Me.Label_PerfFeesRate.TabIndex = 8
    Me.Label_PerfFeesRate.Text = "."
    Me.Label_PerfFeesRate.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Label25
    '
    Me.Label25.Location = New System.Drawing.Point(327, 31)
    Me.Label25.Name = "Label25"
    Me.Label25.Size = New System.Drawing.Size(112, 16)
    Me.Label25.TabIndex = 107
    Me.Label25.Text = "Fee Rate"
    '
    'Label16
    '
    Me.Label16.Location = New System.Drawing.Point(313, 9)
    Me.Label16.Name = "Label16"
    Me.Label16.Size = New System.Drawing.Size(221, 16)
    Me.Label16.TabIndex = 106
    Me.Label16.Text = "Performance Fees"
    '
    'Button_TradeManagementFees
    '
    Me.Button_TradeManagementFees.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Button_TradeManagementFees.Location = New System.Drawing.Point(23, 325)
    Me.Button_TradeManagementFees.Name = "Button_TradeManagementFees"
    Me.Button_TradeManagementFees.Size = New System.Drawing.Size(176, 25)
    Me.Button_TradeManagementFees.TabIndex = 27
    Me.Button_TradeManagementFees.Text = "Book Management Fees"
    Me.Button_TradeManagementFees.UseVisualStyleBackColor = True
    '
    'Label15
    '
    Me.Label15.Location = New System.Drawing.Point(20, 198)
    Me.Label15.Name = "Label15"
    Me.Label15.Size = New System.Drawing.Size(120, 16)
    Me.Label15.TabIndex = 104
    Me.Label15.Text = "Instrument"
    '
    'Label14
    '
    Me.Label14.Location = New System.Drawing.Point(20, 172)
    Me.Label14.Name = "Label14"
    Me.Label14.Size = New System.Drawing.Size(120, 16)
    Me.Label14.TabIndex = 103
    Me.Label14.Text = "Settlement Date"
    '
    'Label13
    '
    Me.Label13.Location = New System.Drawing.Point(20, 147)
    Me.Label13.Name = "Label13"
    Me.Label13.Size = New System.Drawing.Size(120, 16)
    Me.Label13.TabIndex = 102
    Me.Label13.Text = "Additional Trade"
    '
    'Label12
    '
    Me.Label12.Location = New System.Drawing.Point(20, 124)
    Me.Label12.Name = "Label12"
    Me.Label12.Size = New System.Drawing.Size(120, 16)
    Me.Label12.TabIndex = 101
    Me.Label12.Text = "Already booked"
    '
    'Label10
    '
    Me.Label10.Location = New System.Drawing.Point(20, 77)
    Me.Label10.Name = "Label10"
    Me.Label10.Size = New System.Drawing.Size(120, 16)
    Me.Label10.TabIndex = 100
    Me.Label10.Text = "Fees to Take"
    '
    'Label_MgmtFeeNAV
    '
    Me.Label_MgmtFeeNAV.Location = New System.Drawing.Point(140, 51)
    Me.Label_MgmtFeeNAV.Name = "Label_MgmtFeeNAV"
    Me.Label_MgmtFeeNAV.Size = New System.Drawing.Size(117, 16)
    Me.Label_MgmtFeeNAV.TabIndex = 1
    Me.Label_MgmtFeeNAV.Text = "."
    Me.Label_MgmtFeeNAV.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Label11
    '
    Me.Label11.Location = New System.Drawing.Point(20, 54)
    Me.Label11.Name = "Label11"
    Me.Label11.Size = New System.Drawing.Size(120, 16)
    Me.Label11.TabIndex = 98
    Me.Label11.Text = "NAV Used"
    '
    'Label_MgmtFeesRate
    '
    Me.Label_MgmtFeesRate.Location = New System.Drawing.Point(140, 31)
    Me.Label_MgmtFeesRate.Name = "Label_MgmtFeesRate"
    Me.Label_MgmtFeesRate.Size = New System.Drawing.Size(117, 16)
    Me.Label_MgmtFeesRate.TabIndex = 0
    Me.Label_MgmtFeesRate.Text = "."
    Me.Label_MgmtFeesRate.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Label9
    '
    Me.Label9.Location = New System.Drawing.Point(20, 31)
    Me.Label9.Name = "Label9"
    Me.Label9.Size = New System.Drawing.Size(120, 16)
    Me.Label9.TabIndex = 96
    Me.Label9.Text = "Fee Rate"
    '
    'Label7
    '
    Me.Label7.Location = New System.Drawing.Point(8, 9)
    Me.Label7.Name = "Label7"
    Me.Label7.Size = New System.Drawing.Size(221, 16)
    Me.Label7.TabIndex = 95
    Me.Label7.Text = "Management Fees"
    '
    'Tab_Benchmark
    '
    Me.Tab_Benchmark.Controls.Add(Me.Benchmark_Label_BenchmarkName)
    Me.Tab_Benchmark.Controls.Add(Me.Grid_Benchmark)
    Me.Tab_Benchmark.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Benchmark.Name = "Tab_Benchmark"
    Me.Tab_Benchmark.Size = New System.Drawing.Size(1056, 511)
    Me.Tab_Benchmark.TabIndex = 5
    Me.Tab_Benchmark.Text = "Benchmark"
    Me.Tab_Benchmark.UseVisualStyleBackColor = True
    '
    'Benchmark_Label_BenchmarkName
    '
    Me.Benchmark_Label_BenchmarkName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Benchmark_Label_BenchmarkName.Location = New System.Drawing.Point(6, 4)
    Me.Benchmark_Label_BenchmarkName.Name = "Benchmark_Label_BenchmarkName"
    Me.Benchmark_Label_BenchmarkName.Size = New System.Drawing.Size(1044, 20)
    Me.Benchmark_Label_BenchmarkName.TabIndex = 10
    Me.Benchmark_Label_BenchmarkName.Text = "Label67"
    '
    'Grid_Benchmark
    '
    Me.Grid_Benchmark.AllowEditing = False
    Me.Grid_Benchmark.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_Benchmark.AutoGenerateColumns = False
    Me.Grid_Benchmark.BorderStyle = C1.Win.C1FlexGrid.Util.BaseControls.BorderStyleEnum.Light3D
    Me.Grid_Benchmark.CausesValidation = False
    Me.Grid_Benchmark.ColumnInfo = resources.GetString("Grid_Benchmark.ColumnInfo")
    Me.Grid_Benchmark.ContextMenuStrip = Me.ContextMenu_Valuation
    Me.Grid_Benchmark.EditOptions = C1.Win.C1FlexGrid.EditFlags.None
    Me.Grid_Benchmark.KeyActionTab = C1.Win.C1FlexGrid.KeyActionEnum.MoveAcross
    Me.Grid_Benchmark.Location = New System.Drawing.Point(4, 26)
    Me.Grid_Benchmark.Name = "Grid_Benchmark"
    Me.Grid_Benchmark.Rows.Count = 13
    Me.Grid_Benchmark.Rows.DefaultSize = 17
    Me.Grid_Benchmark.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.ListBox
    Me.Grid_Benchmark.Size = New System.Drawing.Size(1049, 482)
    Me.Grid_Benchmark.StyleInfo = resources.GetString("Grid_Benchmark.StyleInfo")
    Me.Grid_Benchmark.TabIndex = 9
    Me.Grid_Benchmark.Tree.Column = 0
    Me.Grid_Benchmark.Tree.LineColor = System.Drawing.SystemColors.HotTrack
    Me.Grid_Benchmark.Tree.Style = CType((((C1.Win.C1FlexGrid.TreeStyleFlags.Lines Or C1.Win.C1FlexGrid.TreeStyleFlags.Symbols) _
                Or C1.Win.C1FlexGrid.TreeStyleFlags.ButtonBar) _
                Or C1.Win.C1FlexGrid.TreeStyleFlags.Leaf), C1.Win.C1FlexGrid.TreeStyleFlags)
    '
    'Tab_Milestone
    '
    Me.Tab_Milestone.Controls.Add(Me.GroupBox6)
    Me.Tab_Milestone.Controls.Add(Me.GroupBox4)
    Me.Tab_Milestone.Controls.Add(Me.GroupBox3)
    Me.Tab_Milestone.Controls.Add(Me.GroupBox2)
    Me.Tab_Milestone.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Milestone.Name = "Tab_Milestone"
    Me.Tab_Milestone.Size = New System.Drawing.Size(1056, 511)
    Me.Tab_Milestone.TabIndex = 4
    Me.Tab_Milestone.Text = "Milestone"
    Me.Tab_Milestone.UseVisualStyleBackColor = True
    '
    'GroupBox6
    '
    Me.GroupBox6.Controls.Add(Me.edit_FixedAdministratorPrice)
    Me.GroupBox6.Controls.Add(Me.Check_UpdateOrdersAutomatically)
    Me.GroupBox6.Controls.Add(Me.button_SetMilestone)
    Me.GroupBox6.Location = New System.Drawing.Point(14, 358)
    Me.GroupBox6.Name = "GroupBox6"
    Me.GroupBox6.Size = New System.Drawing.Size(969, 102)
    Me.GroupBox6.TabIndex = 16
    Me.GroupBox6.TabStop = False
    Me.GroupBox6.Text = "End Of Day"
    '
    'edit_FixedAdministratorPrice
    '
    Me.edit_FixedAdministratorPrice.Enabled = False
    Me.edit_FixedAdministratorPrice.Location = New System.Drawing.Point(324, 21)
    Me.edit_FixedAdministratorPrice.Name = "edit_FixedAdministratorPrice"
    Me.edit_FixedAdministratorPrice.RenaissanceTag = Nothing
    Me.edit_FixedAdministratorPrice.SelectTextOnFocus = False
    Me.edit_FixedAdministratorPrice.Size = New System.Drawing.Size(114, 20)
    Me.edit_FixedAdministratorPrice.TabIndex = 15
    Me.edit_FixedAdministratorPrice.Text = "0.00"
    Me.edit_FixedAdministratorPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.edit_FixedAdministratorPrice.TextFormat = "#,##0.00"
    Me.edit_FixedAdministratorPrice.Value = 0
    '
    'Check_UpdateOrdersAutomatically
    '
    Me.Check_UpdateOrdersAutomatically.AutoSize = True
    Me.Check_UpdateOrdersAutomatically.Location = New System.Drawing.Point(19, 23)
    Me.Check_UpdateOrdersAutomatically.Name = "Check_UpdateOrdersAutomatically"
    Me.Check_UpdateOrdersAutomatically.Size = New System.Drawing.Size(281, 17)
    Me.Check_UpdateOrdersAutomatically.TabIndex = 0
    Me.Check_UpdateOrdersAutomatically.Text = "Automatically Update Orders with Administrator Price : "
    Me.Check_UpdateOrdersAutomatically.UseVisualStyleBackColor = True
    '
    'button_SetMilestone
    '
    Me.button_SetMilestone.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.button_SetMilestone.Location = New System.Drawing.Point(19, 51)
    Me.button_SetMilestone.Name = "button_SetMilestone"
    Me.button_SetMilestone.Size = New System.Drawing.Size(168, 38)
    Me.button_SetMilestone.TabIndex = 14
    Me.button_SetMilestone.Text = "Set EOD / Milestone"
    '
    'GroupBox4
    '
    Me.GroupBox4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.GroupBox4.Controls.Add(Me.Label_Milestone_AdditionalCrystalisedTrade)
    Me.GroupBox4.Controls.Add(Me.Label58)
    Me.GroupBox4.Controls.Add(Me.Label_Milestone_AdditionalPerformanceTrade)
    Me.GroupBox4.Controls.Add(Me.Label52)
    Me.GroupBox4.Controls.Add(Me.Label_Milestone_AdditionalManagementTrade)
    Me.GroupBox4.Controls.Add(Me.Label48)
    Me.GroupBox4.Location = New System.Drawing.Point(14, 252)
    Me.GroupBox4.Name = "GroupBox4"
    Me.GroupBox4.Size = New System.Drawing.Size(1027, 89)
    Me.GroupBox4.TabIndex = 15
    Me.GroupBox4.TabStop = False
    Me.GroupBox4.Text = "Fee Status"
    '
    'Label_Milestone_AdditionalCrystalisedTrade
    '
    Me.Label_Milestone_AdditionalCrystalisedTrade.Location = New System.Drawing.Point(217, 62)
    Me.Label_Milestone_AdditionalCrystalisedTrade.Name = "Label_Milestone_AdditionalCrystalisedTrade"
    Me.Label_Milestone_AdditionalCrystalisedTrade.Size = New System.Drawing.Size(117, 16)
    Me.Label_Milestone_AdditionalCrystalisedTrade.TabIndex = 145
    Me.Label_Milestone_AdditionalCrystalisedTrade.Text = "."
    Me.Label_Milestone_AdditionalCrystalisedTrade.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Label58
    '
    Me.Label58.Location = New System.Drawing.Point(19, 62)
    Me.Label58.Name = "Label58"
    Me.Label58.Size = New System.Drawing.Size(192, 16)
    Me.Label58.TabIndex = 144
    Me.Label58.Text = "Crystalised Fees still to book "
    '
    'Label_Milestone_AdditionalPerformanceTrade
    '
    Me.Label_Milestone_AdditionalPerformanceTrade.Location = New System.Drawing.Point(217, 44)
    Me.Label_Milestone_AdditionalPerformanceTrade.Name = "Label_Milestone_AdditionalPerformanceTrade"
    Me.Label_Milestone_AdditionalPerformanceTrade.Size = New System.Drawing.Size(117, 16)
    Me.Label_Milestone_AdditionalPerformanceTrade.TabIndex = 143
    Me.Label_Milestone_AdditionalPerformanceTrade.Text = "."
    Me.Label_Milestone_AdditionalPerformanceTrade.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Label52
    '
    Me.Label52.Location = New System.Drawing.Point(19, 44)
    Me.Label52.Name = "Label52"
    Me.Label52.Size = New System.Drawing.Size(192, 16)
    Me.Label52.TabIndex = 142
    Me.Label52.Text = "Performance Fees still to book "
    '
    'Label_Milestone_AdditionalManagementTrade
    '
    Me.Label_Milestone_AdditionalManagementTrade.Location = New System.Drawing.Point(217, 26)
    Me.Label_Milestone_AdditionalManagementTrade.Name = "Label_Milestone_AdditionalManagementTrade"
    Me.Label_Milestone_AdditionalManagementTrade.Size = New System.Drawing.Size(117, 16)
    Me.Label_Milestone_AdditionalManagementTrade.TabIndex = 141
    Me.Label_Milestone_AdditionalManagementTrade.Text = "."
    Me.Label_Milestone_AdditionalManagementTrade.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Label48
    '
    Me.Label48.Location = New System.Drawing.Point(19, 26)
    Me.Label48.Name = "Label48"
    Me.Label48.Size = New System.Drawing.Size(192, 16)
    Me.Label48.TabIndex = 140
    Me.Label48.Text = "Management Fees still to book "
    '
    'GroupBox3
    '
    Me.GroupBox3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.GroupBox3.Controls.Add(Me.edit_Milestone_NAVUnitPrice_Trading)
    Me.GroupBox3.Controls.Add(Me.Label43)
    Me.GroupBox3.Controls.Add(Me.Label56)
    Me.GroupBox3.Controls.Add(Me.Label49)
    Me.GroupBox3.Controls.Add(Me.Label_Milestone_TradingUnits)
    Me.GroupBox3.Controls.Add(Me.Label53)
    Me.GroupBox3.Controls.Add(Me.edit_Milestone_NAV_Trading)
    Me.GroupBox3.Controls.Add(Me.Label54)
    Me.GroupBox3.Location = New System.Drawing.Point(14, 153)
    Me.GroupBox3.Name = "GroupBox3"
    Me.GroupBox3.Size = New System.Drawing.Size(1027, 93)
    Me.GroupBox3.TabIndex = 1
    Me.GroupBox3.TabStop = False
    Me.GroupBox3.Text = "Trader Valuation"
    '
    'edit_Milestone_NAVUnitPrice_Trading
    '
    Me.edit_Milestone_NAVUnitPrice_Trading.Location = New System.Drawing.Point(404, 40)
    Me.edit_Milestone_NAVUnitPrice_Trading.Name = "edit_Milestone_NAVUnitPrice_Trading"
    Me.edit_Milestone_NAVUnitPrice_Trading.RenaissanceTag = Nothing
    Me.edit_Milestone_NAVUnitPrice_Trading.SelectTextOnFocus = False
    Me.edit_Milestone_NAVUnitPrice_Trading.Size = New System.Drawing.Size(125, 20)
    Me.edit_Milestone_NAVUnitPrice_Trading.TabIndex = 135
    Me.edit_Milestone_NAVUnitPrice_Trading.Text = "0.00"
    Me.edit_Milestone_NAVUnitPrice_Trading.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.edit_Milestone_NAVUnitPrice_Trading.TextFormat = "#,##0.00"
    Me.edit_Milestone_NAVUnitPrice_Trading.Value = 0
    '
    'Label43
    '
    Me.Label43.Location = New System.Drawing.Point(404, 17)
    Me.Label43.Name = "Label43"
    Me.Label43.Size = New System.Drawing.Size(125, 20)
    Me.Label43.TabIndex = 134
    Me.Label43.Text = "Unit Price"
    Me.Label43.TextAlign = System.Drawing.ContentAlignment.TopCenter
    '
    'Label56
    '
    Me.Label56.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label56.AutoEllipsis = True
    Me.Label56.Location = New System.Drawing.Point(552, 11)
    Me.Label56.Name = "Label56"
    Me.Label56.Size = New System.Drawing.Size(469, 91)
    Me.Label56.TabIndex = 132
    Me.Label56.Text = "These values reflect the Trader, or `Live`, valuation of the fund. They are calcu" & _
        "lated using all transactions."
    '
    'Label49
    '
    Me.Label49.Location = New System.Drawing.Point(270, 16)
    Me.Label49.Name = "Label49"
    Me.Label49.Size = New System.Drawing.Size(120, 20)
    Me.Label49.TabIndex = 125
    Me.Label49.Text = "Units in issue"
    Me.Label49.TextAlign = System.Drawing.ContentAlignment.TopCenter
    '
    'Label_Milestone_TradingUnits
    '
    Me.Label_Milestone_TradingUnits.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Label_Milestone_TradingUnits.Location = New System.Drawing.Point(270, 40)
    Me.Label_Milestone_TradingUnits.Name = "Label_Milestone_TradingUnits"
    Me.Label_Milestone_TradingUnits.Size = New System.Drawing.Size(120, 20)
    Me.Label_Milestone_TradingUnits.TabIndex = 123
    Me.Label_Milestone_TradingUnits.Text = "0"
    Me.Label_Milestone_TradingUnits.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'Label53
    '
    Me.Label53.Location = New System.Drawing.Point(159, 16)
    Me.Label53.Name = "Label53"
    Me.Label53.Size = New System.Drawing.Size(62, 20)
    Me.Label53.TabIndex = 122
    Me.Label53.Text = "Valuation"
    '
    'edit_Milestone_NAV_Trading
    '
    Me.edit_Milestone_NAV_Trading.Location = New System.Drawing.Point(130, 40)
    Me.edit_Milestone_NAV_Trading.Name = "edit_Milestone_NAV_Trading"
    Me.edit_Milestone_NAV_Trading.RenaissanceTag = Nothing
    Me.edit_Milestone_NAV_Trading.SelectTextOnFocus = False
    Me.edit_Milestone_NAV_Trading.Size = New System.Drawing.Size(125, 20)
    Me.edit_Milestone_NAV_Trading.TabIndex = 120
    Me.edit_Milestone_NAV_Trading.Text = "0"
    Me.edit_Milestone_NAV_Trading.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.edit_Milestone_NAV_Trading.TextFormat = "#,##0"
    Me.edit_Milestone_NAV_Trading.Value = 0
    '
    'Label54
    '
    Me.Label54.Location = New System.Drawing.Point(6, 43)
    Me.Label54.Name = "Label54"
    Me.Label54.Size = New System.Drawing.Size(120, 20)
    Me.Label54.TabIndex = 121
    Me.Label54.Text = "Final Fund NAV"
    '
    'GroupBox2
    '
    Me.GroupBox2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.GroupBox2.Controls.Add(Me.Label_AdministratorUnitsIssued)
    Me.GroupBox2.Controls.Add(Me.Label_AdministratorValue)
    Me.GroupBox2.Controls.Add(Me.Label62)
    Me.GroupBox2.Controls.Add(Me.Label_AdministratorNAV)
    Me.GroupBox2.Controls.Add(Me.edit_Milestone_GNAVUnitPrice_Valuation)
    Me.GroupBox2.Controls.Add(Me.edit_Milestone_NAVUnitPrice_Valuation)
    Me.GroupBox2.Controls.Add(Me.Label55)
    Me.GroupBox2.Controls.Add(Me.edit_Milestone_GNAV_Administrator)
    Me.GroupBox2.Controls.Add(Me.Label47)
    Me.GroupBox2.Controls.Add(Me.Label46)
    Me.GroupBox2.Controls.Add(Me.Label45)
    Me.GroupBox2.Controls.Add(Me.Label_Milestone_AdministratorUnits)
    Me.GroupBox2.Controls.Add(Me.Label42)
    Me.GroupBox2.Controls.Add(Me.edit_Milestone_NAV_Administrator)
    Me.GroupBox2.Controls.Add(Me.Label5)
    Me.GroupBox2.Location = New System.Drawing.Point(14, 13)
    Me.GroupBox2.Name = "GroupBox2"
    Me.GroupBox2.Size = New System.Drawing.Size(1027, 134)
    Me.GroupBox2.TabIndex = 0
    Me.GroupBox2.TabStop = False
    Me.GroupBox2.Text = "Administrator Valuation"
    '
    'Label_AdministratorUnitsIssued
    '
    Me.Label_AdministratorUnitsIssued.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Label_AdministratorUnitsIssued.Location = New System.Drawing.Point(270, 96)
    Me.Label_AdministratorUnitsIssued.Name = "Label_AdministratorUnitsIssued"
    Me.Label_AdministratorUnitsIssued.Size = New System.Drawing.Size(120, 20)
    Me.Label_AdministratorUnitsIssued.TabIndex = 138
    Me.Label_AdministratorUnitsIssued.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Label_AdministratorValue
    '
    Me.Label_AdministratorValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Label_AdministratorValue.Location = New System.Drawing.Point(130, 96)
    Me.Label_AdministratorValue.Name = "Label_AdministratorValue"
    Me.Label_AdministratorValue.Size = New System.Drawing.Size(125, 20)
    Me.Label_AdministratorValue.TabIndex = 137
    Me.Label_AdministratorValue.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Label62
    '
    Me.Label62.Location = New System.Drawing.Point(6, 97)
    Me.Label62.Name = "Label62"
    Me.Label62.Size = New System.Drawing.Size(120, 20)
    Me.Label62.TabIndex = 136
    Me.Label62.Text = "Administrator Values"
    '
    'Label_AdministratorNAV
    '
    Me.Label_AdministratorNAV.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Label_AdministratorNAV.Location = New System.Drawing.Point(404, 96)
    Me.Label_AdministratorNAV.Name = "Label_AdministratorNAV"
    Me.Label_AdministratorNAV.Size = New System.Drawing.Size(125, 20)
    Me.Label_AdministratorNAV.TabIndex = 135
    Me.Label_AdministratorNAV.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'edit_Milestone_GNAVUnitPrice_Valuation
    '
    Me.edit_Milestone_GNAVUnitPrice_Valuation.Location = New System.Drawing.Point(404, 66)
    Me.edit_Milestone_GNAVUnitPrice_Valuation.Name = "edit_Milestone_GNAVUnitPrice_Valuation"
    Me.edit_Milestone_GNAVUnitPrice_Valuation.RenaissanceTag = Nothing
    Me.edit_Milestone_GNAVUnitPrice_Valuation.SelectTextOnFocus = False
    Me.edit_Milestone_GNAVUnitPrice_Valuation.Size = New System.Drawing.Size(125, 20)
    Me.edit_Milestone_GNAVUnitPrice_Valuation.TabIndex = 133
    Me.edit_Milestone_GNAVUnitPrice_Valuation.Text = "0.00"
    Me.edit_Milestone_GNAVUnitPrice_Valuation.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.edit_Milestone_GNAVUnitPrice_Valuation.TextFormat = "#,##0.00"
    Me.edit_Milestone_GNAVUnitPrice_Valuation.Value = 0
    '
    'edit_Milestone_NAVUnitPrice_Valuation
    '
    Me.edit_Milestone_NAVUnitPrice_Valuation.Location = New System.Drawing.Point(404, 39)
    Me.edit_Milestone_NAVUnitPrice_Valuation.Name = "edit_Milestone_NAVUnitPrice_Valuation"
    Me.edit_Milestone_NAVUnitPrice_Valuation.RenaissanceTag = Nothing
    Me.edit_Milestone_NAVUnitPrice_Valuation.SelectTextOnFocus = False
    Me.edit_Milestone_NAVUnitPrice_Valuation.Size = New System.Drawing.Size(125, 20)
    Me.edit_Milestone_NAVUnitPrice_Valuation.TabIndex = 132
    Me.edit_Milestone_NAVUnitPrice_Valuation.Text = "0.00"
    Me.edit_Milestone_NAVUnitPrice_Valuation.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.edit_Milestone_NAVUnitPrice_Valuation.TextFormat = "#,##0.00"
    Me.edit_Milestone_NAVUnitPrice_Valuation.Value = 0
    '
    'Label55
    '
    Me.Label55.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label55.AutoEllipsis = True
    Me.Label55.Location = New System.Drawing.Point(549, 12)
    Me.Label55.Name = "Label55"
    Me.Label55.Size = New System.Drawing.Size(472, 91)
    Me.Label55.TabIndex = 131
    Me.Label55.Text = "These values are calculated to reconcile with the Administrator. They are the val" & _
        "ues used on the other tabs of this form and they are the values used, where nece" & _
        "ssary, to calculate performance fees."
    '
    'edit_Milestone_GNAV_Administrator
    '
    Me.edit_Milestone_GNAV_Administrator.Location = New System.Drawing.Point(130, 67)
    Me.edit_Milestone_GNAV_Administrator.Name = "edit_Milestone_GNAV_Administrator"
    Me.edit_Milestone_GNAV_Administrator.RenaissanceTag = Nothing
    Me.edit_Milestone_GNAV_Administrator.SelectTextOnFocus = False
    Me.edit_Milestone_GNAV_Administrator.Size = New System.Drawing.Size(125, 20)
    Me.edit_Milestone_GNAV_Administrator.TabIndex = 127
    Me.edit_Milestone_GNAV_Administrator.Text = "0"
    Me.edit_Milestone_GNAV_Administrator.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.edit_Milestone_GNAV_Administrator.TextFormat = "#,##0"
    Me.edit_Milestone_GNAV_Administrator.Value = 0
    '
    'Label47
    '
    Me.Label47.Location = New System.Drawing.Point(6, 70)
    Me.Label47.Name = "Label47"
    Me.Label47.Size = New System.Drawing.Size(120, 20)
    Me.Label47.TabIndex = 128
    Me.Label47.Text = "Final Fund GNAV"
    '
    'Label46
    '
    Me.Label46.Location = New System.Drawing.Point(404, 16)
    Me.Label46.Name = "Label46"
    Me.Label46.Size = New System.Drawing.Size(125, 20)
    Me.Label46.TabIndex = 126
    Me.Label46.Text = "Unit Price"
    Me.Label46.TextAlign = System.Drawing.ContentAlignment.TopCenter
    '
    'Label45
    '
    Me.Label45.Location = New System.Drawing.Point(270, 16)
    Me.Label45.Name = "Label45"
    Me.Label45.Size = New System.Drawing.Size(120, 20)
    Me.Label45.TabIndex = 125
    Me.Label45.Text = "Units in issue"
    Me.Label45.TextAlign = System.Drawing.ContentAlignment.TopCenter
    '
    'Label_Milestone_AdministratorUnits
    '
    Me.Label_Milestone_AdministratorUnits.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Label_Milestone_AdministratorUnits.Location = New System.Drawing.Point(270, 40)
    Me.Label_Milestone_AdministratorUnits.Name = "Label_Milestone_AdministratorUnits"
    Me.Label_Milestone_AdministratorUnits.Size = New System.Drawing.Size(120, 20)
    Me.Label_Milestone_AdministratorUnits.TabIndex = 123
    Me.Label_Milestone_AdministratorUnits.Text = "0"
    Me.Label_Milestone_AdministratorUnits.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'Label42
    '
    Me.Label42.Location = New System.Drawing.Point(159, 16)
    Me.Label42.Name = "Label42"
    Me.Label42.Size = New System.Drawing.Size(62, 20)
    Me.Label42.TabIndex = 122
    Me.Label42.Text = "Valuation"
    '
    'edit_Milestone_NAV_Administrator
    '
    Me.edit_Milestone_NAV_Administrator.Location = New System.Drawing.Point(130, 40)
    Me.edit_Milestone_NAV_Administrator.Name = "edit_Milestone_NAV_Administrator"
    Me.edit_Milestone_NAV_Administrator.RenaissanceTag = Nothing
    Me.edit_Milestone_NAV_Administrator.SelectTextOnFocus = False
    Me.edit_Milestone_NAV_Administrator.Size = New System.Drawing.Size(125, 20)
    Me.edit_Milestone_NAV_Administrator.TabIndex = 120
    Me.edit_Milestone_NAV_Administrator.Text = "0"
    Me.edit_Milestone_NAV_Administrator.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.edit_Milestone_NAV_Administrator.TextFormat = "#,##0"
    Me.edit_Milestone_NAV_Administrator.Value = 0
    '
    'Label5
    '
    Me.Label5.Location = New System.Drawing.Point(6, 43)
    Me.Label5.Name = "Label5"
    Me.Label5.Size = New System.Drawing.Size(120, 20)
    Me.Label5.TabIndex = 121
    Me.Label5.Text = "Final Fund NAV"
    '
    'Tab_Subscriptions
    '
    Me.Tab_Subscriptions.Controls.Add(Me.Label67)
    Me.Tab_Subscriptions.Controls.Add(Me.Grid_PreviousSubscriptions)
    Me.Tab_Subscriptions.Controls.Add(Me.Panel4)
    Me.Tab_Subscriptions.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Subscriptions.Name = "Tab_Subscriptions"
    Me.Tab_Subscriptions.Size = New System.Drawing.Size(1056, 511)
    Me.Tab_Subscriptions.TabIndex = 6
    Me.Tab_Subscriptions.Text = "Subscriptions"
    Me.Tab_Subscriptions.UseVisualStyleBackColor = True
    '
    'Label67
    '
    Me.Label67.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label67.AutoSize = True
    Me.Label67.Location = New System.Drawing.Point(14, 268)
    Me.Label67.Name = "Label67"
    Me.Label67.Size = New System.Drawing.Size(193, 13)
    Me.Label67.TabIndex = 215
    Me.Label67.Text = "Previous Subscriptions / Redemptions :"
    '
    'Grid_PreviousSubscriptions
    '
    Me.Grid_PreviousSubscriptions.AllowEditing = False
    Me.Grid_PreviousSubscriptions.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_PreviousSubscriptions.CausesValidation = False
    Me.Grid_PreviousSubscriptions.ColumnInfo = "5,1,0,0,0,85,Columns:"
    Me.Grid_PreviousSubscriptions.EditOptions = C1.Win.C1FlexGrid.EditFlags.None
    Me.Grid_PreviousSubscriptions.KeyActionTab = C1.Win.C1FlexGrid.KeyActionEnum.MoveAcross
    Me.Grid_PreviousSubscriptions.Location = New System.Drawing.Point(9, 289)
    Me.Grid_PreviousSubscriptions.Name = "Grid_PreviousSubscriptions"
    Me.Grid_PreviousSubscriptions.Rows.DefaultSize = 17
    Me.Grid_PreviousSubscriptions.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.ListBox
    Me.Grid_PreviousSubscriptions.Size = New System.Drawing.Size(1039, 216)
    Me.Grid_PreviousSubscriptions.TabIndex = 214
    '
    'Panel4
    '
    Me.Panel4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Panel4.Controls.Add(Me.Panel3)
    Me.Panel4.Controls.Add(Me.Label61)
    Me.Panel4.Controls.Add(Me.Grid_Subscriptions)
    Me.Panel4.Location = New System.Drawing.Point(4, 3)
    Me.Panel4.Name = "Panel4"
    Me.Panel4.Size = New System.Drawing.Size(1049, 251)
    Me.Panel4.TabIndex = 213
    '
    'Panel3
    '
    Me.Panel3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Panel3.Controls.Add(Me.Label_AdministratorNAV2)
    Me.Panel3.Controls.Add(Me.Label63)
    Me.Panel3.Controls.Add(Me.GroupBox5)
    Me.Panel3.Controls.Add(Me.Label60)
    Me.Panel3.Controls.Add(Me.Combo_TradeStatus)
    Me.Panel3.Controls.Add(Me.Label44)
    Me.Panel3.Controls.Add(Me.btn_Updatesubscriptions)
    Me.Panel3.Controls.Add(Me.edit_SubscriptionPrice)
    Me.Panel3.Controls.Add(Me.Label_Price)
    Me.Panel3.Location = New System.Drawing.Point(3, 151)
    Me.Panel3.Name = "Panel3"
    Me.Panel3.Size = New System.Drawing.Size(1039, 93)
    Me.Panel3.TabIndex = 16
    '
    'Label_AdministratorNAV2
    '
    Me.Label_AdministratorNAV2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Label_AdministratorNAV2.Location = New System.Drawing.Point(84, 67)
    Me.Label_AdministratorNAV2.Name = "Label_AdministratorNAV2"
    Me.Label_AdministratorNAV2.Size = New System.Drawing.Size(114, 20)
    Me.Label_AdministratorNAV2.TabIndex = 214
    '
    'Label63
    '
    Me.Label63.Location = New System.Drawing.Point(3, 69)
    Me.Label63.Name = "Label63"
    Me.Label63.Size = New System.Drawing.Size(75, 18)
    Me.Label63.TabIndex = 213
    Me.Label63.Text = "Administrator"
    Me.Label63.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'GroupBox5
    '
    Me.GroupBox5.Controls.Add(Me.Radio_Transactions_Selected)
    Me.GroupBox5.Controls.Add(Me.Radio_Transactions_All)
    Me.GroupBox5.Location = New System.Drawing.Point(717, 32)
    Me.GroupBox5.Name = "GroupBox5"
    Me.GroupBox5.Size = New System.Drawing.Size(165, 34)
    Me.GroupBox5.TabIndex = 212
    Me.GroupBox5.TabStop = False
    '
    'Radio_Transactions_Selected
    '
    Me.Radio_Transactions_Selected.AutoSize = True
    Me.Radio_Transactions_Selected.Location = New System.Drawing.Point(77, 11)
    Me.Radio_Transactions_Selected.Name = "Radio_Transactions_Selected"
    Me.Radio_Transactions_Selected.Size = New System.Drawing.Size(67, 17)
    Me.Radio_Transactions_Selected.TabIndex = 1
    Me.Radio_Transactions_Selected.TabStop = True
    Me.Radio_Transactions_Selected.Text = "Selected"
    Me.Radio_Transactions_Selected.UseVisualStyleBackColor = True
    '
    'Radio_Transactions_All
    '
    Me.Radio_Transactions_All.AutoSize = True
    Me.Radio_Transactions_All.Location = New System.Drawing.Point(17, 11)
    Me.Radio_Transactions_All.Name = "Radio_Transactions_All"
    Me.Radio_Transactions_All.Size = New System.Drawing.Size(36, 17)
    Me.Radio_Transactions_All.TabIndex = 0
    Me.Radio_Transactions_All.TabStop = True
    Me.Radio_Transactions_All.Text = "All"
    Me.Radio_Transactions_All.UseVisualStyleBackColor = True
    '
    'Label60
    '
    Me.Label60.AutoSize = True
    Me.Label60.Location = New System.Drawing.Point(5, 9)
    Me.Label60.Name = "Label60"
    Me.Label60.Size = New System.Drawing.Size(232, 13)
    Me.Label60.TabIndex = 211
    Me.Label60.Text = "Update Selected Subscriptions / Redemptions :"
    '
    'Combo_TradeStatus
    '
    Me.Combo_TradeStatus.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_TradeStatus.Location = New System.Drawing.Point(318, 42)
    Me.Combo_TradeStatus.Name = "Combo_TradeStatus"
    Me.Combo_TradeStatus.Size = New System.Drawing.Size(184, 21)
    Me.Combo_TradeStatus.TabIndex = 12
    '
    'Label44
    '
    Me.Label44.Location = New System.Drawing.Point(219, 46)
    Me.Label44.Name = "Label44"
    Me.Label44.Size = New System.Drawing.Size(91, 16)
    Me.Label44.TabIndex = 210
    Me.Label44.Text = "Trade Status"
    Me.Label44.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'btn_Updatesubscriptions
    '
    Me.btn_Updatesubscriptions.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btn_Updatesubscriptions.Location = New System.Drawing.Point(531, 38)
    Me.btn_Updatesubscriptions.Name = "btn_Updatesubscriptions"
    Me.btn_Updatesubscriptions.Size = New System.Drawing.Size(179, 28)
    Me.btn_Updatesubscriptions.TabIndex = 13
    Me.btn_Updatesubscriptions.Text = "Update Transactions"
    '
    'edit_SubscriptionPrice
    '
    Me.edit_SubscriptionPrice.Location = New System.Drawing.Point(84, 42)
    Me.edit_SubscriptionPrice.Name = "edit_SubscriptionPrice"
    Me.edit_SubscriptionPrice.RenaissanceTag = Nothing
    Me.edit_SubscriptionPrice.SelectTextOnFocus = False
    Me.edit_SubscriptionPrice.Size = New System.Drawing.Size(114, 20)
    Me.edit_SubscriptionPrice.TabIndex = 4
    Me.edit_SubscriptionPrice.Text = "0.00"
    Me.edit_SubscriptionPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.edit_SubscriptionPrice.TextFormat = "#,##0.00"
    Me.edit_SubscriptionPrice.Value = 0
    '
    'Label_Price
    '
    Me.Label_Price.Location = New System.Drawing.Point(10, 46)
    Me.Label_Price.Name = "Label_Price"
    Me.Label_Price.Size = New System.Drawing.Size(68, 13)
    Me.Label_Price.TabIndex = 5
    Me.Label_Price.Text = "Price"
    Me.Label_Price.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Label61
    '
    Me.Label61.AutoSize = True
    Me.Label61.Location = New System.Drawing.Point(8, 4)
    Me.Label61.Name = "Label61"
    Me.Label61.Size = New System.Drawing.Size(211, 13)
    Me.Label61.TabIndex = 212
    Me.Label61.Text = "Subscriptions / Redemptions in this period :"
    '
    'Grid_Subscriptions
    '
    Me.Grid_Subscriptions.AllowEditing = False
    Me.Grid_Subscriptions.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_Subscriptions.CausesValidation = False
    Me.Grid_Subscriptions.ColumnInfo = "5,1,0,0,0,85,Columns:"
    Me.Grid_Subscriptions.EditOptions = C1.Win.C1FlexGrid.EditFlags.None
    Me.Grid_Subscriptions.KeyActionTab = C1.Win.C1FlexGrid.KeyActionEnum.MoveAcross
    Me.Grid_Subscriptions.Location = New System.Drawing.Point(3, 26)
    Me.Grid_Subscriptions.Name = "Grid_Subscriptions"
    Me.Grid_Subscriptions.Rows.DefaultSize = 17
    Me.Grid_Subscriptions.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.ListBox
    Me.Grid_Subscriptions.Size = New System.Drawing.Size(1039, 119)
    Me.Grid_Subscriptions.TabIndex = 15
    '
    'Check_AdministratorPriceDates
    '
    Me.Check_AdministratorPriceDates.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Check_AdministratorPriceDates.Location = New System.Drawing.Point(403, 109)
    Me.Check_AdministratorPriceDates.Name = "Check_AdministratorPriceDates"
    Me.Check_AdministratorPriceDates.Size = New System.Drawing.Size(198, 17)
    Me.Check_AdministratorPriceDates.TabIndex = 1
    Me.Check_AdministratorPriceDates.Text = "Use Administrator Price Dates"
    Me.Check_AdministratorPriceDates.UseVisualStyleBackColor = True
    '
    'Date_FeesSince
    '
    Me.Date_FeesSince.Location = New System.Drawing.Point(151, 108)
    Me.Date_FeesSince.Name = "Date_FeesSince"
    Me.Date_FeesSince.Size = New System.Drawing.Size(227, 20)
    Me.Date_FeesSince.TabIndex = 0
    '
    'Label4
    '
    Me.Label4.Location = New System.Drawing.Point(11, 112)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(134, 16)
    Me.Label4.TabIndex = 90
    Me.Label4.Text = "Calculate Fees Since :"
    '
    'GroupBox1
    '
    Me.GroupBox1.Controls.Add(Me.Check_SR_IgnoreTodaysTrades)
    Me.GroupBox1.Controls.Add(Me.Check_SR_IgnoreStatus)
    Me.GroupBox1.Location = New System.Drawing.Point(401, 55)
    Me.GroupBox1.Name = "GroupBox1"
    Me.GroupBox1.Size = New System.Drawing.Size(320, 48)
    Me.GroupBox1.TabIndex = 145
    Me.GroupBox1.TabStop = False
    Me.GroupBox1.Text = "Subscriptions and Redemptions"
    '
    'Check_SR_IgnoreTodaysTrades
    '
    Me.Check_SR_IgnoreTodaysTrades.AutoSize = True
    Me.Check_SR_IgnoreTodaysTrades.Location = New System.Drawing.Point(172, 22)
    Me.Check_SR_IgnoreTodaysTrades.Name = "Check_SR_IgnoreTodaysTrades"
    Me.Check_SR_IgnoreTodaysTrades.Size = New System.Drawing.Size(130, 17)
    Me.Check_SR_IgnoreTodaysTrades.TabIndex = 1
    Me.Check_SR_IgnoreTodaysTrades.Text = "Ignore 'Todays' trades"
    Me.Check_SR_IgnoreTodaysTrades.UseVisualStyleBackColor = True
    '
    'Check_SR_IgnoreStatus
    '
    Me.Check_SR_IgnoreStatus.AutoSize = True
    Me.Check_SR_IgnoreStatus.Location = New System.Drawing.Point(14, 22)
    Me.Check_SR_IgnoreStatus.Name = "Check_SR_IgnoreStatus"
    Me.Check_SR_IgnoreStatus.Size = New System.Drawing.Size(120, 17)
    Me.Check_SR_IgnoreStatus.TabIndex = 0
    Me.Check_SR_IgnoreStatus.Text = "Ignore Trade Status"
    Me.Check_SR_IgnoreStatus.UseVisualStyleBackColor = True
    '
    'Timer_Update
    '
    Me.Timer_Update.Interval = 50
    '
    'btn_Test
    '
    Me.btn_Test.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btn_Test.BackColor = System.Drawing.SystemColors.Control
    Me.btn_Test.Location = New System.Drawing.Point(966, 55)
    Me.btn_Test.Name = "btn_Test"
    Me.btn_Test.Size = New System.Drawing.Size(102, 48)
    Me.btn_Test.TabIndex = 146
    Me.btn_Test.Text = "Refresh Data"
    Me.btn_Test.UseVisualStyleBackColor = False
    '
    'TabControl_Hidden
    '
    Me.TabControl_Hidden.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.TabControl_Hidden.Location = New System.Drawing.Point(1043, 125)
    Me.TabControl_Hidden.Name = "TabControl_Hidden"
    Me.TabControl_Hidden.SelectedIndex = 0
    Me.TabControl_Hidden.Size = New System.Drawing.Size(25, 25)
    Me.TabControl_Hidden.TabIndex = 147
    Me.TabControl_Hidden.Visible = False
    '
    'ContextMenu_AutomatedFees
    '
    Me.ContextMenu_AutomatedFees.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_AutomatedFees_Description, Me.ToolStripSeparator3, Me.Menu_AutomatedFees_ShowTransaction, Me.Menu_AutomatedFees_ShowFeeDefenition})
    Me.ContextMenu_AutomatedFees.Name = "ContextMenu_Valuation"
    Me.ContextMenu_AutomatedFees.Size = New System.Drawing.Size(180, 98)
    '
    'Menu_AutomatedFees_ShowTransaction
    '
    Me.Menu_AutomatedFees_ShowTransaction.Name = "Menu_AutomatedFees_ShowTransaction"
    Me.Menu_AutomatedFees_ShowTransaction.Size = New System.Drawing.Size(179, 22)
    Me.Menu_AutomatedFees_ShowTransaction.Text = "Show Transaction"
    '
    'Menu_AutomatedFees_ShowFeeDefenition
    '
    Me.Menu_AutomatedFees_ShowFeeDefenition.Name = "Menu_AutomatedFees_ShowFeeDefenition"
    Me.Menu_AutomatedFees_ShowFeeDefenition.Size = New System.Drawing.Size(179, 22)
    Me.Menu_AutomatedFees_ShowFeeDefenition.Text = "Show Fee Definition"
    '
    'Menu_AutomatedFees_Description
    '
    Me.Menu_AutomatedFees_Description.Name = "Menu_AutomatedFees_Description"
    Me.Menu_AutomatedFees_Description.Size = New System.Drawing.Size(179, 22)
    Me.Menu_AutomatedFees_Description.Text = "."
    '
    'ToolStripSeparator3
    '
    Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
    Me.ToolStripSeparator3.Size = New System.Drawing.Size(176, 6)
    '
    'frmReconciliationProcess
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(1076, 696)
    Me.Controls.Add(Me.Check_AdministratorPriceDates)
    Me.Controls.Add(Me.TabControl_Hidden)
    Me.Controls.Add(Me.GroupBox1)
    Me.Controls.Add(Me.btn_Test)
    Me.Controls.Add(Me.TabControl_Reconciliation)
    Me.Controls.Add(Me.Date_FeesSince)
    Me.Controls.Add(Me.Combo_TransactionGroup)
    Me.Controls.Add(Me.Label4)
    Me.Controls.Add(Me.Label8)
    Me.Controls.Add(Me.Form_StatusStrip)
    Me.Controls.Add(Me.RootMenu)
    Me.Controls.Add(Me.Date_ValueDate)
    Me.Controls.Add(Me.Label3)
    Me.Controls.Add(Me.Combo_Fund)
    Me.Controls.Add(Me.label_CptyIsFund)
    Me.MinimumSize = New System.Drawing.Size(970, 630)
    Me.Name = "frmReconciliationProcess"
    Me.Text = "Fund Reconciliation"
    CType(Me.Grid_NotionalExposure, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Form_StatusStrip.ResumeLayout(False)
    Me.Form_StatusStrip.PerformLayout()
    Me.TabControl_Reconciliation.ResumeLayout(False)
    Me.Tab_Valuation.ResumeLayout(False)
    Me.Tab_Valuation.PerformLayout()
    CType(Me.Grid_Valuations, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ContextMenu_Valuation.ResumeLayout(False)
    Me.Tab_Notional.ResumeLayout(False)
    Me.Tab_Availabilities.ResumeLayout(False)
    Me.Tab_Availabilities.PerformLayout()
    CType(Me.Grid_Retrocessions, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_AutomatedFees.ResumeLayout(False)
    CType(Me.Grid_AutomatedFees, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_FeesMultiClass.ResumeLayout(False)
    Me.GroupBox7.ResumeLayout(False)
    Me.GroupBox7.PerformLayout()
    CType(Me.Grid_FeesMultiClass, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_Fees.ResumeLayout(False)
    Me.Panel1.ResumeLayout(False)
    Me.Panel1.PerformLayout()
    Me.Panel2.ResumeLayout(False)
    Me.Tab_Benchmark.ResumeLayout(False)
    CType(Me.Grid_Benchmark, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_Milestone.ResumeLayout(False)
    Me.GroupBox6.ResumeLayout(False)
    Me.GroupBox6.PerformLayout()
    Me.GroupBox4.ResumeLayout(False)
    Me.GroupBox3.ResumeLayout(False)
    Me.GroupBox3.PerformLayout()
    Me.GroupBox2.ResumeLayout(False)
    Me.GroupBox2.PerformLayout()
    Me.Tab_Subscriptions.ResumeLayout(False)
    Me.Tab_Subscriptions.PerformLayout()
    CType(Me.Grid_PreviousSubscriptions, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Panel4.ResumeLayout(False)
    Me.Panel4.PerformLayout()
    Me.Panel3.ResumeLayout(False)
    Me.Panel3.PerformLayout()
    Me.GroupBox5.ResumeLayout(False)
    Me.GroupBox5.PerformLayout()
    CType(Me.Grid_Subscriptions, System.ComponentModel.ISupportInitialize).EndInit()
    Me.GroupBox1.ResumeLayout(False)
    Me.GroupBox1.PerformLayout()
    Me.ContextMenu_AutomatedFees.ResumeLayout(False)
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "

  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
  Private WithEvents _MainForm As VeniceMain

  ' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
  Private FormTooltip As New ToolTip()

  ' Form Constants, specific to the table being updated.

    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
  Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

  ' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
  Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
  Private THIS_FORM_FormID As VeniceFormID

  ' Data Structures

  'Private myConnection As SqlConnection

    ''' <summary>
    ''' My notionals dataset
    ''' </summary>
  Private myNotionalsDataset As DSSelectFuturesNotional
    ''' <summary>
    ''' My notionals table
    ''' </summary>
  Private myNotionalsTable As DSSelectFuturesNotional.tblSelectFuturesNotionalDataTable = Nothing
  'Private myNotionalsAdaptor As New SqlDataAdapter
    ''' <summary>
    ''' My notionals data view
    ''' </summary>
  Private myNotionalsDataView As DataView = Nothing

  ' Private myValuationsDataset As DataSet
    ''' <summary>
    ''' My valuations table
    ''' </summary>
  Private myValuationsTable As DataTable = Nothing
  'Private myValuationsAdaptor As New SqlDataAdapter
    ''' <summary>
    ''' My valuations data view
    ''' </summary>
  Private myValuationsDataView As DataView = Nothing

  Private myFundValuationUnitCounts As Dictionary(Of Integer, SR_InformationClass) = Nothing

  ''' <summary>
  ''' My transactions table
  ''' </summary>
  Private mySubscriptionsTable As DataTable
  ''' <summary>
  ''' My myPreviousSubscriptionsTable table
  ''' </summary>
  Private myPreviousSubscriptionsTable As DataTable
  ''' <summary>
  ''' My myAutomaticFeesTable table
  ''' </summary>
  Private myAutomaticFeesTable As DataTable
  ''' <summary>
  ''' The lock_tbl fund valuation
  ''' </summary>
  Private lock_tblFundValuation As New ReaderWriterLockSlim
  ''' <summary>
  ''' The lock_tbl fund valuation
  ''' </summary>
  Private lock_tblFundValuationAll As New ReaderWriterLockSlim
  ''' <summary>
  ''' The lock_tbl futures notionals
  ''' </summary>
  Private lock_tblFuturesNotionals As New ReaderWriterLockSlim
  ''' <summary>
  ''' The lock_tbl availabilities
  ''' </summary>
  Private lock_tblAvailabilities As New ReaderWriterLockSlim
  ''' <summary>
  ''' The custom management fees
  ''' </summary>
  Private CustomManagementFees As Boolean
  ''' <summary>
  ''' The custom performance fees
  ''' </summary>
  Private CustomPerformanceFees As Boolean
  ''' <summary>
  ''' The custom crystalised fees
  ''' </summary>
  Private CustomCrystalisedFees As Boolean
  ''' <summary>
  ''' The custom subscription price
  ''' </summary>
  Private CustomSubscriptionPrice As Boolean

  ''' <summary>
  ''' The custom_ M s_ valuation_ NAV
  ''' </summary>
  Private Custom_MS_Valuation_NAV As Boolean
  ''' <summary>
  ''' The custom_ M s_ valuation_ NA v_ unit price
  ''' </summary>
  Private Custom_MS_Valuation_NAV_UnitPrice As Boolean
  ''' <summary>
  ''' The custom_ M s_ valuation_ GNAV
  ''' </summary>
  Private Custom_MS_Valuation_GNAV As Boolean
  ''' <summary>
  ''' The custom_ M s_ valuation_ GNA v_ unit price
  ''' </summary>
  Private Custom_MS_Valuation_GNAV_UnitPrice As Boolean
  ''' <summary>
  ''' The custom_ M s_ trading_ NAV
  ''' </summary>
  Private Custom_MS_Trading_NAV As Boolean
  ''' <summary>
  ''' The custom_ M s_ trading_ NA v_ unit price
  ''' </summary>
  Private Custom_MS_Trading_NAV_UnitPrice As Boolean
  ''' <summary>
  ''' The custom_ M s_ trading_ GNAV
  ''' </summary>
  Private Custom_MS_Trading_GNAV As Boolean
  ''' <summary>
  ''' The custom_ M s_ trading_ GNA v_ unit price
  ''' </summary>
  Private Custom_MS_Trading_GNAV_UnitPrice As Boolean
  ''' <summary>
  ''' The price row cache
  ''' </summary>
  Private PriceRowCache As New Dictionary(Of ULong, DataRow)
  ''' <summary>
  ''' The best prices table
  ''' </summary>
  Private BestPricesTable As DataTable
  ''' <summary>
  ''' The sorted prices
  ''' </summary>
  Private SortedPrices As DataRow()

	Private ClassSpecificPnLTable As DataTable
	''' <summary>
	''' The FX map
	''' </summary>
  Private FXMap As New Dictionary(Of Integer, Double)
  ''' <summary>
  ''' The administrator inventory cache
  ''' </summary>
  Friend AdministratorInventoryCache As New Dictionary(Of Integer, DataTable)
  ''' <summary>
  ''' The administrator holdings cache
  ''' </summary>
  Friend AdministratorHoldingsCache As New Dictionary(Of Integer, DataTable)

  'Private ThisStandardDataset As RenaissanceGlobals.StandardDataset

  ' Active Element.

  ''' <summary>
  ''' The __ is over cancel button
  ''' </summary>
  Private __IsOverCancelButton As Boolean
  ''' <summary>
  ''' The _ in use
  ''' </summary>
  Private _InUse As Boolean

  ' Form Status Flags

  ''' <summary>
  ''' The form is valid
  ''' </summary>
  Private FormIsValid As Boolean
  ''' <summary>
  ''' The in paint
  ''' </summary>
  Private InPaint As Boolean
  ''' <summary>
  ''' The is loading data
  ''' </summary>
  Private IsLoadingData As Boolean = False
  ''' <summary>
  ''' The _ form open failed
  ''' </summary>
  Private _FormOpenFailed As Boolean

  ''' <summary>
  ''' The tab data status
  ''' </summary>
  Private TabDataStatus As Dictionary(Of String, Boolean) ' Effectively a simple flag variable to indicate if the data for a given tab has been loaded. Cleared to force refresh.
  ''' <summary>
  ''' The tab initial status
  ''' </summary>
  Private TabInitialStatus As Dictionary(Of String, Boolean)

  ''' <summary>
  ''' The flag_ refresh tab
  ''' </summary>
  Private Flag_RefreshTab As Integer = 0
  ''' <summary>
  ''' The last entered valuation grid row
  ''' </summary>
  Private LastEnteredValuationGridRow As Integer = 0
  ''' <summary>
  ''' The last entered valuation grid col
  ''' </summary>
  Private LastEnteredValuationGridCol As Integer = 0
  ''' <summary>
  ''' The last entered valuation grid row
  ''' </summary>     
  Private LastEnteredAutomatedFeesGridRow As Integer = 0
  ''' <summary>
  ''' The last entered valuation grid col
  ''' </summary>
  Private LastEnteredAutomatedFeesGridCol As Integer = 0
  ''' <summary>
  ''' The fees object
  ''' </summary>
  Dim FeesObject As FundFeeCalculator = Nothing
  ''' <summary>
  ''' The trading valuation object
  ''' </summary>
  Dim TradingValuationObject As FundFeeCalculator = Nothing

  ''' <summary>
  ''' The fund availabilities
  ''' </summary>
  Private FundAvailabilities As DSRecAvailabilities.tblRecAvailabilitiesDataTable = Nothing
  ''' <summary>
  ''' The futures notionals table
  ''' </summary>
  Private FuturesNotionalsTable As RenaissanceDataClass.DSFuturesNotional.tblFuturesNotionalDataTable = Nothing

  ' User Permission Flags

  ''' <summary>
  ''' The has read permission
  ''' </summary>
  Private HasReadPermission As Boolean
  ''' <summary>
  ''' The has update permission
  ''' </summary>
  Private HasUpdatePermission As Boolean
  ''' <summary>
  ''' The has insert permission
  ''' </summary>
  Private HasInsertPermission As Boolean
  ''' <summary>
  ''' The has delete permission
  ''' </summary>
  Private HasDeletePermission As Boolean
  ''' <summary>
  ''' The has transaction insert permission
  ''' </summary>
  Private HasTransactionInsertPermission As Boolean
  ''' <summary>
  ''' The has milestone insert permission
  ''' </summary>
  Private HasMilestoneInsertPermission As Boolean

  ' Futures-Type InstrumentTypes

  ''' <summary>
  ''' The _ futures instrument types
  ''' </summary>
  Private _FuturesInstrumentTypes As String = ""

  ' Update Status Misc flags

  ''' <summary>
  ''' The _ date_ value date_ changed
  ''' </summary>
  Private _Date_ValueDate_Changed As Boolean = False

#End Region

#Region " Form 'Properties' "

  ''' <summary>
  ''' Gets the main form.
  ''' </summary>
  ''' <value>The main form.</value>
  Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
    ' Public property to return handle to the 'Main' Venice form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

  ''' <summary>
  ''' Gets or sets a value indicating whether this instance is over cancel button.
  ''' </summary>
  ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
  Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
    ' Public property maintaining a value indicating if the cursor is over the 'Cancel'
    ' Button on this form.
    ' This property is specifically designed for use by the field formating Event functions
    ' In order that they do not impose format restrictions if the user is about to click the 
    ' 'Cancel' button.
    '
    Get
      Return __IsOverCancelButton
    End Get
    Set(ByVal Value As Boolean)
      __IsOverCancelButton = Value
    End Set
  End Property

  ''' <summary>
  ''' Gets a value indicating whether this instance is in paint.
  ''' </summary>
  ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
  Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
    Get
      Return InPaint
    End Get
  End Property

  ''' <summary>
  ''' Gets a value indicating whether [in use].
  ''' </summary>
  ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
    Get
      Return _InUse
    End Get
  End Property

  ''' <summary>
  ''' Gets a value indicating whether [form open failed].
  ''' </summary>
  ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

#End Region

  ''' <summary>
  ''' Initializes a new instance of the <see cref="frmReconciliationProcess"/> class.
  ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  Public Sub New(ByVal pMainForm As VeniceMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()


    _MainForm = pMainForm
    AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

    _FormOpenFailed = False
    _InUse = True
    TabDataStatus = New Dictionary(Of String, Boolean)
    TabInitialStatus = New Dictionary(Of String, Boolean)

    ' Form Permissioning :-

    THIS_FORM_PermissionArea = Me.Name
    THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

    ' 'This' form ID

    THIS_FORM_FormID = VeniceFormID.frmReconciliationProcess

    ' Establish / Retrieve data objects for this form.

    'myConnection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)

    ' Notionals Initialisation - Structures required to initialise grid, etc

    'Call MainForm.MainAdaptorHandler.Set_AdaptorCommands(myConnection, myNotionalsAdaptor, RenaissanceStandardDatasets.tblSelectFuturesNotional.TableName)
    myNotionalsDataset = New DSSelectFuturesNotional
    myNotionalsTable = myNotionalsDataset.tblSelectFuturesNotional

    ' Valuation Initialisation

    ' Call MainForm.MainAdaptorHandler.Set_AdaptorCommands(myConnection, myValuationsAdaptor, "rpt_Venice_ReconcileValuation")
    myValuationsTable = New DataTable("tblReconcileValuation")

    myValuationsDataView = New DataView(myValuationsTable, "True", "", DataViewRowState.CurrentRows)

    myFundValuationUnitCounts = New Dictionary(Of Integer, SR_InformationClass)

    ' Grid Fields
    Dim FieldsMenu As ToolStripMenuItem = SetFieldSelectMenu(RootMenu)

    ' Establish initial DataView and Initialise Transactions Grig.

    Dim thisCol As C1.Win.C1FlexGrid.Column
    Dim Counter As Integer

    '
    Dim NewStyle As C1.Win.C1FlexGrid.CellStyle

    ' Initialise Notionals Grid

    ' NewStyle = Grid_PortfolioData.Styles.Add("VeniceInstruments", Grid_PortfolioData.GetCellStyle(0, Grid_Instrument))
    NewStyle = Grid_NotionalExposure.Styles.Add("DataNegative", Grid_NotionalExposure.Styles.Normal)
    NewStyle.ForeColor = Color.Red
    '

    Try
      myNotionalsDataView = New DataView(myNotionalsTable, "True", "", DataViewRowState.CurrentRows)

      Grid_NotionalExposure.DataSource = myNotionalsDataView

      ' Format Transactions Grid, hide unwanted columns.

      Grid_NotionalExposure.Cols("FundName").Move(1)
      Grid_NotionalExposure.Cols("FuturesInstrumentDescription").Move(2)
      Grid_NotionalExposure.Cols("FuturesInstrumentDescription").Caption = "Futures Instrument"
      Grid_NotionalExposure.Cols("FuturesCurrencyDescription").Move(3)
      Grid_NotionalExposure.Cols("FuturesCurrencyDescription").Caption = "Futures Ccy"
      Grid_NotionalExposure.Cols("OpenPosition").Move(4)
      Grid_NotionalExposure.Cols("BookedNotional").Move(5)
      Grid_NotionalExposure.Cols("Local_Value").Move(6)
      Grid_NotionalExposure.Cols("Local_Value").Caption = "Value (Futures Ccy)"
      Grid_NotionalExposure.Cols("Local_UnrealisedProfit").Move(7)
      Grid_NotionalExposure.Cols("Local_UnrealisedProfit").Caption = "Unrealised Profit (Futures CCy)"
      Grid_NotionalExposure.Cols("UnrealisedProfit").Move(8)
      Grid_NotionalExposure.Cols("UnrealisedProfit").Caption = "Unrealised Profit (Fund CCy)"
      Grid_NotionalExposure.Cols("NotionalTradeToZeroRealisedPnL").Move(9)
      Grid_NotionalExposure.Cols("NotionalTradeToZeroRealisedPnL").Caption = "Trade To Zero Realised PnL"
      Grid_NotionalExposure.Cols("NotionalTradeToZeroUnRealisedPnL").Move(10)
      Grid_NotionalExposure.Cols("NotionalTradeToZeroUnRealisedPnL").Caption = "Trade To Zero UnRealised PnL"
    Catch ex As Exception
    End Try

    For Each thisCol In Grid_NotionalExposure.Cols
      Try
        Select Case thisCol.Name
          Case "FundName"
          Case "FuturesInstrumentDescription"
          Case "FuturesCurrencyDescription"
          Case "OpenPosition"
            thisCol.Format = "#,##0.00"
          Case "BookedNotional"
            thisCol.Format = "#,##0.00"
          Case "Value"
            thisCol.Format = "#,##0.00"
          Case "UnrealisedProfit"
            thisCol.Format = "#,##0.00"
            thisCol.Visible = False
          Case "NotionalTradeToZeroRealisedPnL"
            thisCol.Format = "#,##0.00"
            thisCol.Style.BackColor = Color.LightCyan
            thisCol.Visible = False

          Case "NotionalTradeToZeroUnRealisedPnL"
            thisCol.Format = "#,##0.00"
            thisCol.Style.BackColor = Color.LightCyan
            thisCol.Visible = True

          Case "FundFXRate"
            thisCol.Format = "#,##0.00000"
            thisCol.Visible = False

          Case "FuturesInstrumentFXRate"
            thisCol.Format = "#,##0.00000"
            thisCol.Visible = False

          Case "CompoundFXRate"
            thisCol.Format = "#,##0.00000"
            thisCol.Visible = False

          Case Else
            thisCol.Visible = False

            Try
              If thisCol.DataType Is GetType(Date) Then
                thisCol.Format = DISPLAYMEMBER_DATEFORMAT
              ElseIf thisCol.DataType Is GetType(Integer) Then
                thisCol.Format = "#,##0"
              ElseIf thisCol.DataType Is GetType(Double) Then
                thisCol.Format = "#,##0.00"
              End If
            Catch ex As Exception
            End Try
        End Select

        If (thisCol.Caption.ToUpper.StartsWith("TRANSACTION")) Then
          thisCol.Caption = thisCol.Caption.Substring(11)
        End If

        If (thisCol.Visible) Then
          Try
            CType(FieldsMenu.DropDownItems("Menu_GridField_" & thisCol.Name), ToolStripMenuItem).Checked = True
          Catch ex As Exception
          End Try
        End If
      Catch ex As Exception
      End Try
    Next

    Try
      For Counter = 0 To (Grid_NotionalExposure.Cols.Count - 1)
        If (Grid_NotionalExposure.Cols(Counter).Caption.ToUpper.StartsWith("TRANSACTION")) Then
          Grid_NotionalExposure.Cols(Counter).Caption = Grid_NotionalExposure.Cols(Counter).Caption.Substring(11)
        End If
      Next
    Catch ex As Exception
    End Try

    ' Valuation Grid

    NewStyle = Grid_Valuations.Styles.Add("DataNegative", Grid_Valuations.Styles.Normal)
    NewStyle.ForeColor = Color.Red

    NewStyle = Grid_Valuations.Styles.Add("HighlightPositive", Grid_Valuations.Styles.Normal)
    NewStyle.ForeColor = Color.Black
    NewStyle.BackColor = Color.LightCyan
    NewStyle.Font = New System.Drawing.Font(Grid_Valuations.Styles.Normal.Font, FontStyle.Bold)

    NewStyle = Grid_Valuations.Styles.Add("HighlightNegative", Grid_Valuations.Styles.Normal)
    NewStyle.ForeColor = Color.Red
    NewStyle.BackColor = Color.LightCyan
    NewStyle.Font = New System.Drawing.Font(Grid_Valuations.Styles.Normal.Font, FontStyle.Bold)

    NewStyle = Grid_Valuations.Styles.Add("DifferenceNegative", Grid_Valuations.Styles.Normal)
    NewStyle.ForeColor = Color.Red
    NewStyle.BackColor = Color.LemonChiffon

    NewStyle = Grid_Valuations.Styles.Add("DifferencePositive", Grid_Valuations.Styles.Normal)
    NewStyle.ForeColor = Color.Black
    NewStyle.BackColor = Color.LemonChiffon

    NewStyle = Grid_Valuations.Styles.Add("TotalsPositive", Grid_Valuations.Styles("VeniceTotals"))
    NewStyle.ForeColor = Color.Black

    NewStyle = Grid_Valuations.Styles.Add("TotalsNegative", Grid_Valuations.Styles("VeniceTotals"))
    NewStyle.ForeColor = Color.Red

    Grid_Valuations.Cols("colInstrument").Visible = False
    Grid_Valuations.Cols("colCaptureID").Visible = False

    ' Subscriptions Grid

    Try
      Dim thisTable As DataTable

      mySubscriptionsTable = New DataTable
      myPreviousSubscriptionsTable = New DataTable

      For Each thisTable In New DataTable() {mySubscriptionsTable, myPreviousSubscriptionsTable}
        thisTable.Columns.Add(New DataColumn("TransactionInstrument", GetType(Integer)))
        thisTable.Columns.Add(New DataColumn("TransactionType", GetType(Integer)))
        thisTable.Columns.Add(New DataColumn("FransactionFund", GetType(Integer)))
        thisTable.Columns.Add(New DataColumn("TransactionParentID", GetType(Integer)))
        thisTable.Columns.Add(New DataColumn("TradeStatusName", GetType(String)))
        thisTable.Columns.Add(New DataColumn("TransactionTicket", GetType(String)))
        thisTable.Columns.Add(New DataColumn("FundName", GetType(String)))
        thisTable.Columns.Add(New DataColumn("InstrumentDescription", GetType(String)))
        thisTable.Columns.Add(New DataColumn("TransactionTypeName", GetType(String)))
        thisTable.Columns.Add(New DataColumn("TransactionValueDate", GetType(Date)))
        thisTable.Columns.Add(New DataColumn("TransactionSettlementDate", GetType(Date)))
        thisTable.Columns.Add(New DataColumn("TransactionUnits", GetType(Double)))
        thisTable.Columns.Add(New DataColumn("TransactionPrice", GetType(Double)))
        thisTable.Columns.Add(New DataColumn("TransactionSettlement", GetType(Double)))
        thisTable.Columns.Add(New DataColumn("TransactionCosts", GetType(Double)))
        thisTable.Columns.Add(New DataColumn("TransactionBrokerFees", GetType(Double)))
      Next

      Dim thisGrid As C1.Win.C1FlexGrid.C1FlexGrid

      Grid_Subscriptions.AutoGenerateColumns = True
      Grid_Subscriptions.DataSource = mySubscriptionsTable
      Grid_Subscriptions.AutoGenerateColumns = False

      Grid_PreviousSubscriptions.AutoGenerateColumns = True
      Grid_PreviousSubscriptions.DataSource = myPreviousSubscriptionsTable
      Grid_PreviousSubscriptions.AutoGenerateColumns = False

      For Each thisGrid In New C1.Win.C1FlexGrid.C1FlexGrid() {Grid_Subscriptions, Grid_PreviousSubscriptions}

        ' Format Transactions Grid, hide unwanted columns.

        thisGrid.Cols("TransactionTicket").Move(1)
        thisGrid.Cols("FundName").Move(2)
        thisGrid.Cols("InstrumentDescription").Move(3)
        thisGrid.Cols("TransactionTypeName").Move(4)
        thisGrid.Cols("TransactionUnits").Move(5)
        thisGrid.Cols("TransactionPrice").Move(6)
        thisGrid.Cols("TransactionCosts").Move(7)
        thisGrid.Cols("TransactionBrokerFees").Move(8)
        thisGrid.Cols("TransactionSettlement").Move(9)
        thisGrid.Cols("TransactionValueDate").Move(10)
        thisGrid.Cols("TransactionSettlementDate").Move(11)
        thisGrid.Cols("TradeStatusName").Move(12)

        For Each thisCol In thisGrid.Cols
          Try
            Select Case thisCol.Name
              Case "TransactionTicket"
                thisCol.Width = 75
              Case "FundName"
                thisCol.Width = 200
                thisCol.Visible = False
              Case "InstrumentDescription"
                thisCol.Width = 300
              Case "TransactionTypeName"
                thisCol.Width = 90
              Case "TransactionUnits"
                thisCol.Format = "#,##0.0000"
                thisCol.Width = 90
              Case "TransactionPrice"
                thisCol.Format = "#,##0.0000"
                thisCol.Width = 90
              Case "TransactionSettlement"
                thisCol.Format = "#,##0.00"
                thisCol.Visible = True
                thisCol.Width = 90
              Case "TransactionCosts"
                thisCol.Format = "#,##0.00"
                thisCol.Visible = True
                thisCol.Width = 90
              Case "TransactionBrokerFees"
                thisCol.Format = "#,##0.00"
                thisCol.Visible = True
                thisCol.Width = 90
              Case "TransactionValueDate"
                thisCol.Format = DISPLAYMEMBER_DATEFORMAT
                thisCol.Caption = "NAV Date"
                thisCol.Width = 90
              Case "TransactionSettlementDate"
                thisCol.Format = DISPLAYMEMBER_DATEFORMAT
                thisCol.Width = 90
              Case "TradeStatusName"
                thisCol.Width = 90

              Case Else
                thisCol.Visible = False

                Try
                  If thisCol.DataType Is GetType(Date) Then
                    thisCol.Format = DISPLAYMEMBER_DATEFORMAT
                  ElseIf thisCol.DataType Is GetType(Integer) Then
                    thisCol.Format = "#,##0"
                  ElseIf thisCol.DataType Is GetType(Double) Then
                    thisCol.Format = "#,##0.0000"
                  End If
                Catch ex As Exception
                End Try
            End Select

            If (thisCol.Caption.ToUpper.StartsWith("TRANSACTION")) Then
              thisCol.Caption = thisCol.Caption.Substring(11)
            End If

          Catch ex As Exception
          End Try
        Next

        '' Transaction Grid

        'Try
        '  For Counter = 0 To (thisGrid.Cols.Count - 1)
        '    If (thisGrid.Cols(Counter).Caption.ToUpper.StartsWith("TRANSACTION")) Then
        '      thisGrid.Cols(Counter).Caption = thisGrid.Cols(Counter).Caption.Substring(11)
        '    End If
        '  Next
        'Catch ex As Exception
        'End Try

      Next

    Catch ex As Exception
    End Try

    ' Fees Grid

    Try
      Dim thisTable As DataTable

      myAutomaticFeesTable = New DataTable

      For Each thisTable In New DataTable() {myAutomaticFeesTable}
        thisTable.Columns.Add(New DataColumn("FeeID", GetType(Integer)))
        thisTable.Columns.Add(New DataColumn("ToBook", GetType(Double)))
        thisTable.Columns.Add(New DataColumn("FeeName", GetType(String)))
        thisTable.Columns.Add(New DataColumn("InstrumentDescription", GetType(String)))
        thisTable.Columns.Add(New DataColumn("TransactionFundID", GetType(Integer)))
        thisTable.Columns.Add(New DataColumn("TransactionSubFundID", GetType(Integer)))
        thisTable.Columns.Add(New DataColumn("TransactionInstrumentID", GetType(Integer)))
        thisTable.Columns.Add(New DataColumn("TransactionSpecificShareClassID", GetType(Integer)))
        thisTable.Columns.Add(New DataColumn("TransactionValueDate", GetType(Date)))
        thisTable.Columns.Add(New DataColumn("TransactionSettlementDate", GetType(Date)))
        thisTable.Columns.Add(New DataColumn("TransactionAmount", GetType(Double)))
        thisTable.Columns.Add(New DataColumn("FeeRateType", GetType(Integer)))
        thisTable.Columns.Add(New DataColumn("FeeCalculationMethod", GetType(Integer)))
        thisTable.Columns.Add(New DataColumn("FeeDescription", GetType(String)))
        thisTable.Columns.Add(New DataColumn("ExistingTicket", GetType(String)))
        thisTable.Columns.Add(New DataColumn("ExistingParentID", GetType(Integer)))
        thisTable.Columns.Add(New DataColumn("ExistingInstrumentID", GetType(Integer)))
        thisTable.Columns.Add(New DataColumn("ExistingValueDate", GetType(Date)))
        thisTable.Columns.Add(New DataColumn("ExistingAmount", GetType(Double)))
        thisTable.Columns.Add(New DataColumn("NotUsed", GetType(Boolean)))
      Next

      Dim thisGrid As C1.Win.C1FlexGrid.C1FlexGrid

      Grid_AutomatedFees.AutoGenerateColumns = True
      Grid_AutomatedFees.DataSource = myAutomaticFeesTable
      Grid_AutomatedFees.AutoGenerateColumns = False

      For Each thisGrid In New C1.Win.C1FlexGrid.C1FlexGrid() {Grid_AutomatedFees}

        ' Format Transactions Grid, hide unwanted columns.

        thisGrid.Cols("FeeID").Move(1)
        thisGrid.Cols("ToBook").Move(2)
        thisGrid.Cols("ExistingAmount").Move(3)
        thisGrid.Cols("FeeName").Move(4)
        thisGrid.Cols("InstrumentDescription").Move(5)
        thisGrid.Cols("TransactionFundID").Move(6)
        thisGrid.Cols("TransactionSubFundID").Move(7)
        thisGrid.Cols("TransactionInstrumentID").Move(8)
        thisGrid.Cols("TransactionSpecificShareClassID").Move(9)
        thisGrid.Cols("TransactionValueDate").Move(10)
        thisGrid.Cols("TransactionSettlementDate").Move(11)
        thisGrid.Cols("TransactionAmount").Move(12)
        thisGrid.Cols("FeeRateType").Move(13)
        thisGrid.Cols("FeeCalculationMethod").Move(14)
        thisGrid.Cols("FeeDescription").Move(15)
        thisGrid.Cols("ExistingTicket").Move(16)
        thisGrid.Cols("ExistingParentID").Move(17)
        thisGrid.Cols("ExistingInstrumentID").Move(18)
        thisGrid.Cols("ExistingValueDate").Move(19)
        thisGrid.Cols("NotUsed").Move(20)

        For Each thisCol In thisGrid.Cols
          Try
            Select Case thisCol.Name
              Case "ToBook"
                thisCol.Width = 90
                thisCol.Format = "#,##0.00"
                thisCol.Caption = "To Book"
              Case "ExistingTicket"
                thisCol.Width = 75
                thisCol.Caption = "Ticket"
              Case "FeeName"
                thisCol.Width = 200
                thisCol.Caption = "Fee Name"
              Case "InstrumentDescription"
                thisCol.Width = 300
                thisCol.Caption = "Instrument"
              Case "TransactionFundID"
                thisCol.Width = 90
                thisCol.Visible = False
              Case "FeeID"
                thisCol.Width = 45
                thisCol.Visible = False
              Case "TransactionSubFundID"
                thisCol.Width = 45
                thisCol.Visible = False
              Case "TransactionInstrumentID"
                thisCol.Width = 45
                thisCol.Visible = False
              Case "TransactionSpecificShareClassID"
                thisCol.Width = 45
                thisCol.Visible = False
              Case "TransactionValueDate"
                thisCol.Width = 90
                thisCol.Caption = "Value Date"
              Case "TransactionSettlementDate"
                thisCol.Width = 90
                thisCol.Caption = "Sett. Date"
              Case "TransactionAmount"
                thisCol.Width = 90
                thisCol.Format = "#,##0.00"
                thisCol.Caption = "Amount"
              Case "FeeRateType"
                thisCol.Width = 45
                thisCol.Visible = False
              Case "FeeCalculationMethod"
                thisCol.Width = 45
                thisCol.Visible = False
              Case "FeeDescription"
                thisCol.Width = 300
                thisCol.Caption = "Fee Description"
              Case "ExistingParentID"
                thisCol.Width = 100
                thisCol.Caption = "Booked ID"
              Case "ExistingInstrumentID"
                thisCol.Width = 100
                thisCol.Visible = False
              Case "ExistingValueDate"
                thisCol.Width = 100
                thisCol.Caption = "Booked Value Date"
              Case "ExistingAmount"
                thisCol.Width = 100
                thisCol.Format = "#,##0.00"
                thisCol.Caption = "Booked Amount"

              Case Else
                thisCol.Visible = False

                Try
                  If thisCol.DataType Is GetType(Date) Then
                    thisCol.Format = DISPLAYMEMBER_DATEFORMAT
                  ElseIf thisCol.DataType Is GetType(Integer) Then
                    thisCol.Format = "#,##0"
                  ElseIf thisCol.DataType Is GetType(Double) Then
                    thisCol.Format = "#,##0.0000"
                  End If
                Catch ex As Exception
                End Try
            End Select

            If (thisCol.Caption.ToUpper.StartsWith("TRANSACTION")) Then
              thisCol.Caption = thisCol.Caption.Substring(11)
            End If

          Catch ex As Exception
          End Try
        Next

      Next

    Catch ex As Exception
    End Try

    ' Availabilities Grid

    Grid_Retrocessions.Rows.Count = 1
    Grid_Retrocessions.Cols.Fixed = 0

    'styles
    Dim cs As CellStyle = Grid_Retrocessions.Styles.Normal
    cs.Border.Direction = BorderDirEnum.Vertical
    cs.WordWrap = False

    cs = Grid_Retrocessions.Styles.Add("NumberPositive")
    cs.ForeColor = Color.Black

    cs = Grid_Retrocessions.Styles.Add("NumberNegative")
    cs.ForeColor = Color.Red

    cs = Grid_Retrocessions.Styles.Add("NumberPositiveHighlight")
    cs.ForeColor = Color.Black
    cs.BackColor = Color.LightYellow

    cs = Grid_Retrocessions.Styles.Add("NumberNegativeHighlight")
    cs.ForeColor = Color.Red
    cs.BackColor = Color.LightYellow

    cs = Grid_Retrocessions.Styles.Add("FundNumberPositive")
    cs.ForeColor = Color.Black
    cs.Font = New Font(Grid_Retrocessions.Font, FontStyle.Bold)

    cs = Grid_Retrocessions.Styles.Add("FundNumberNegative")
    cs.ForeColor = Color.Red
    cs.Font = New Font(Grid_Retrocessions.Font, FontStyle.Bold)

    cs = Grid_Retrocessions.Styles.Add("FundNumberPositiveHighlight")
    cs.ForeColor = Color.Black
    cs.BackColor = Color.LightYellow
    cs.Font = New Font(Grid_Retrocessions.Font, FontStyle.Bold)

    cs = Grid_Retrocessions.Styles.Add("FundNumberNegativeHighlight")
    cs.ForeColor = Color.Red
    cs.BackColor = Color.LightYellow
    cs.Font = New Font(Grid_Retrocessions.Font, FontStyle.Bold)

    cs = Grid_Retrocessions.Styles.Add("Hidden")
    cs.Display = DisplayEnum.None

    cs = Grid_Retrocessions.Styles.Add("Visible")

    'outline tree
    Grid_Retrocessions.Tree.Column = 0
    Grid_Retrocessions.AllowMerging = AllowMergingEnum.Nodes

    'other
    Grid_Retrocessions.AllowResizing = AllowResizingEnum.Columns
    Grid_Retrocessions.SelectionMode = SelectionModeEnum.Cell

    ' ShareClass grid - Grid_FeesMultiClass

    Dim StyleVariant As String
    Dim VariantBackGround As System.Drawing.Color

    For Each StyleVariant In New String() {"", "Admin_", "Spreadsheet_"}

      Select Case StyleVariant
        Case "Admin_"
          VariantBackGround = System.Drawing.Color.FromArgb(200, 230, 200)

        Case "Spreadsheet_"
          VariantBackGround = System.Drawing.Color.FromArgb(237, 206, 216)

        Case Else
          VariantBackGround = Color.Transparent

      End Select

      cs = Grid_FeesMultiClass.Styles.Add(StyleVariant & "Separator", Grid_Valuations.Styles.Normal)
      cs.BackColor = Color.LightGray

      cs = Grid_FeesMultiClass.Styles.Add(StyleVariant & "Integer", Grid_Valuations.Styles.Normal)
      If (VariantBackGround <> Color.Transparent) Then cs.BackColor = VariantBackGround
      cs.Format = "#,##0"

      cs = Grid_FeesMultiClass.Styles.Add(StyleVariant & "Integer_Negative", Grid_Valuations.Styles.Normal)
      cs.ForeColor = Color.Red
      If (VariantBackGround <> Color.Transparent) Then cs.BackColor = VariantBackGround
      cs.Format = "#,##0"

      cs = Grid_FeesMultiClass.Styles.Add(StyleVariant & "Numeric2Editable", Grid_Valuations.Styles.Normal)
      cs.Format = "#,##0.00"
      cs.BackColor = CUSTOM_BACKGROUND_UNCHANGED
      cs.Border.Color = Color.Black

      cs = Grid_FeesMultiClass.Styles.Add(StyleVariant & "Numeric2Edited", Grid_Valuations.Styles.Normal)
      cs.Format = "#,##0.00"
      cs.BackColor = CUSTOM_BACKGROUND_CHANGED
      cs.Border.Color = Color.Black

      cs = Grid_FeesMultiClass.Styles.Add(StyleVariant & "Percentage", Grid_Valuations.Styles.Normal)
      If (VariantBackGround <> Color.Transparent) Then cs.BackColor = VariantBackGround
      cs.Format = "#,##0.00%"

      cs = Grid_FeesMultiClass.Styles.Add(StyleVariant & "Percentage_Negative", Grid_Valuations.Styles.Normal)
      cs.ForeColor = Color.Red
      If (VariantBackGround <> Color.Transparent) Then cs.BackColor = VariantBackGround
      cs.Format = "#,##0.00%"

      cs = Grid_FeesMultiClass.Styles.Add(StyleVariant & "Numeric2", Grid_Valuations.Styles.Normal)
      If (VariantBackGround <> Color.Transparent) Then cs.BackColor = VariantBackGround
      cs.Format = "#,##0.00"

      cs = Grid_FeesMultiClass.Styles.Add(StyleVariant & "Numeric2_Negative", Grid_Valuations.Styles.Normal)
      cs.ForeColor = Color.Red
      If (VariantBackGround <> Color.Transparent) Then cs.BackColor = VariantBackGround
      cs.Format = "#,##0.00"

      cs = Grid_FeesMultiClass.Styles.Add(StyleVariant & "Numeric4", Grid_Valuations.Styles.Normal)
      If (VariantBackGround <> Color.Transparent) Then cs.BackColor = VariantBackGround
      cs.Format = "#,##0.0000"

      cs = Grid_FeesMultiClass.Styles.Add(StyleVariant & "Numeric4_Negative", Grid_Valuations.Styles.Normal)
      cs.ForeColor = Color.Red
      If (VariantBackGround <> Color.Transparent) Then cs.BackColor = VariantBackGround
      cs.Format = "#,##0.0000"

      cs = Grid_FeesMultiClass.Styles.Add(StyleVariant & "Numeric6", Grid_Valuations.Styles.Normal)
      If (VariantBackGround <> Color.Transparent) Then cs.BackColor = VariantBackGround
      cs.Format = "#,##0.000000"

      cs = Grid_FeesMultiClass.Styles.Add(StyleVariant & "Numeric6_Negative", Grid_Valuations.Styles.Normal)
      cs.ForeColor = Color.Red
      If (VariantBackGround <> Color.Transparent) Then cs.BackColor = VariantBackGround
      cs.Format = "#,##0.000000"

      cs = Grid_FeesMultiClass.Styles.Add(StyleVariant & "TextLeft", Grid_Valuations.Styles.Normal)
      If (VariantBackGround <> Color.Transparent) Then cs.BackColor = VariantBackGround
      cs.TextAlign = TextAlignEnum.LeftCenter

      cs = Grid_FeesMultiClass.Styles.Add(StyleVariant & "TextRight", Grid_Valuations.Styles.Normal)
      If (VariantBackGround <> Color.Transparent) Then cs.BackColor = VariantBackGround
      cs.TextAlign = TextAlignEnum.RightCenter

      cs = Grid_FeesMultiClass.Styles.Add(StyleVariant & "TextMiddle", Grid_Valuations.Styles.Normal)
      If (VariantBackGround <> Color.Transparent) Then cs.BackColor = VariantBackGround
      cs.TextAlign = TextAlignEnum.CenterCenter
    Next

    '
    '
    cs = Grid_Benchmark.Styles.Add("NumericDouble2", Grid_Valuations.Styles.Normal)
    cs.Format = "#,##0.00"

    ' Form Control Changed events
    AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

    AddHandler Combo_Fund.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_FuturesInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Date_ValueDate.ValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_TransactionGroup.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_UnRealisedCalculationType.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Check_SR_IgnoreStatus.CheckedChanged, AddressOf Me.FormControlChanged
    AddHandler Check_SR_IgnoreTodaysTrades.CheckedChanged, AddressOf Me.FormControlChanged
    AddHandler Check_AdministratorPriceDates.CheckedChanged, AddressOf Me.FormControlChanged
    AddHandler Date_FeesSince.ValueChanged, AddressOf Me.FormControlChanged
    AddHandler Check_IncludeUnbookedFees.CheckedChanged, AddressOf Me.FormControlChanged

    AddHandler Combo_Fund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_TransactionGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_UnRealisedCalculationType.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_FuturesInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_ManagementFeeInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_PerformanceFeeInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_PerformanceFeeCrystalisedInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType

    AddHandler Combo_Fund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_TransactionGroup.KeyUp, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_UnRealisedCalculationType.KeyUp, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_FuturesInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_ManagementFeeInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_PerformanceFeeInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_PerformanceFeeCrystalisedInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

    AddHandler Combo_Fund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_TransactionGroup.KeyUp, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_UnRealisedCalculationType.KeyUp, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_FuturesInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_ManagementFeeInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_PerformanceFeeInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_PerformanceFeeCrystalisedInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

    AddHandler Combo_Fund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_TransactionGroup.KeyUp, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_UnRealisedCalculationType.KeyUp, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_FuturesInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_ManagementFeeInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_PerformanceFeeInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_PerformanceFeeCrystalisedInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

    ' Set up the ToolTip
    MainForm.SetFormToolTip(Me, FormTooltip)

    BestPricesTable = Nothing
    ClassSpecificPnLTable = Nothing

  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
  ''' <summary>
  ''' Resets the form.
  ''' </summary>
  Public Sub ResetForm() Implements StandardVeniceForm.ResetForm
    Call Form_Load(Me, New System.EventArgs)
  End Sub

  ''' <summary>
  ''' Closes the form.
  ''' </summary>
  Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
    ALWAYS_CLOSE_THIS_FORM = True
    Me.Close()
  End Sub

  ''' <summary>
  ''' Handles the Load event of the Form control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
    _FormOpenFailed = False
    _InUse = True

    ' Initialise Data structures. Connection, Adaptor and Dataset.

    If Not (MainForm Is Nothing) Then
      FormIsValid = True
    Else
      MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    ' Check User permissions
    Try
      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

        FormIsValid = False
        _FormOpenFailed = True

        Exit Sub
      End If
    Catch ex As Exception
      FormIsValid = False
      _FormOpenFailed = True

      Exit Sub
    End Try

    ' Initialse form

    IsOverCancelButton = False

    CustomManagementFees = False
    CustomPerformanceFees = False
    CustomCrystalisedFees = False
    CustomSubscriptionPrice = False

    Custom_MS_Valuation_NAV = False
    Custom_MS_Valuation_NAV_UnitPrice = False
    Custom_MS_Valuation_GNAV = False
    Custom_MS_Valuation_GNAV_UnitPrice = False
    Custom_MS_Trading_NAV = False
    Custom_MS_Trading_NAV_UnitPrice = False
    Custom_MS_Trading_GNAV = False
    Custom_MS_Trading_GNAV_UnitPrice = False

    ' Initialise main select controls
    ' Build Sorted data list from which this form operates

    Try
      InPaint = True

      Call SetFundCombo()
      Call SetInstrumentCombo()
      Call SetTransactionGroupCombo()
      Call SetTradeStatusCombo()
      Call SetUnRealisedCalculationTypeCombo()

      Me.Combo_Fund.SelectedIndex = -1
      Me.Combo_FuturesInstrument.SelectedIndex = -1
      Me.Date_ValueDate.Value = RenaissanceUtilities.DatePeriodFunctions.AddPeriodToDate(DealingPeriod.Daily, Now.Date, -1)
      Me.Combo_UnRealisedCalculationType.SelectedValue = CalculationTypes_Profit.FIFO
      Me.Combo_TransactionGroup.Text = DEFAULT_STATUSGROUPFILTER_EOD
      Me.Check_IncludeUnbookedFees.Checked = False

      Call getFXRates(Date_ValueDate.Value.Date) '

      Me.Check_SR_IgnoreStatus.Checked = True
      Me.Check_SR_IgnoreTodaysTrades.Checked = True
      Me.Check_AdministratorPriceDates.Checked = True

      TabControl_Reconciliation.SelectedTab = Tab_Valuation

      Me.Radio_Transactions_All.Checked = False
      Me.Radio_Transactions_Selected.Checked = True

      Me.Check_UpdateOrdersAutomatically.Checked = True

      ' Refresh 
      RefreshCurrentTab(False, True)

      Call MainForm.SetComboSelectionLengths(Me)

      Try
        Combo_TradeStatus.SelectedValue = TradeStatus.CheckedEOD
      Catch ex As Exception
      End Try

      ShowHideReconciliationTabs(0)

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error initialising Select Transaction Form.", ex.StackTrace, True)
    Finally
      InPaint = False
    End Try


    Try
      If (Not Timer_Update.Enabled) Then
        Timer_Update.Start()
      End If
    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Handles the Closing event of the frm control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
  Private Sub frm_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    Dim HideForm As Boolean

    ' Hide or Close this form ?
    ' All depends on how many of this form type are Open or in Cache...

    Try
      If (Timer_Update.Enabled) Then
        Timer_Update.Stop()
        Timer_Update.Enabled = False
      End If
    Catch ex As Exception
    End Try

    _InUse = False

    If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
      HideForm = False
    Else
      HideForm = True
      If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
        HideForm = False
      End If
    End If

    If HideForm = True Then
      MainForm.HideInFormsCollection(Me)
      Me.Hide() ' NPP Fix

      e.Cancel = True
    Else
      Try
        MainForm.RemoveFromFormsCollection(Me)
        RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

        RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

        RemoveHandler Combo_Fund.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_FuturesInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Date_ValueDate.ValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_TransactionGroup.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_UnRealisedCalculationType.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_SR_IgnoreStatus.CheckedChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_SR_IgnoreTodaysTrades.CheckedChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_AdministratorPriceDates.CheckedChanged, AddressOf Me.FormControlChanged
        RemoveHandler Date_FeesSince.ValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_IncludeUnbookedFees.CheckedChanged, AddressOf Me.FormControlChanged

        RemoveHandler Combo_Fund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_TransactionGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_UnRealisedCalculationType.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_FuturesInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_ManagementFeeInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_PerformanceFeeInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_PerformanceFeeCrystalisedInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        RemoveHandler Combo_Fund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_TransactionGroup.KeyUp, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_UnRealisedCalculationType.KeyUp, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_FuturesInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_ManagementFeeInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_PerformanceFeeInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_PerformanceFeeCrystalisedInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

        RemoveHandler Combo_Fund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_TransactionGroup.KeyUp, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_UnRealisedCalculationType.KeyUp, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_FuturesInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_ManagementFeeInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_PerformanceFeeInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_PerformanceFeeCrystalisedInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

        RemoveHandler Combo_Fund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_TransactionGroup.KeyUp, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_UnRealisedCalculationType.KeyUp, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_FuturesInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_ManagementFeeInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_PerformanceFeeInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_PerformanceFeeCrystalisedInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus


      Catch ex As Exception
      End Try
    End If

  End Sub


#End Region

  ' Routine to handle changes / updates to tables by this and other windows.
  ' If this, or any other, form posts a change to a table, then it will invoke an update event 
  ' detailing what tables have been altered.
  ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
  ' the 'VeniceAutoUpdate' event of the main Venice form.
  ' Each form may them react as appropriate to changes in any table that might impact it.
  '
  ''' <summary>
  ''' Autoes the update.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    Dim OrgInPaint As Boolean
    Dim KnowledgeDateChanged As Boolean
    Dim RefreshGrid As Boolean
    Dim RefreshValuation As Boolean = False
    Dim RefreshNotionals As Boolean = False

    If (Not Me.Created) OrElse (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then
      Exit Sub
    End If

    OrgInPaint = InPaint

    Try
      InPaint = True
      KnowledgeDateChanged = False
      RefreshGrid = False

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
        KnowledgeDateChanged = True
      End If
    Catch ex As Exception
    End Try

    ' ****************************************************************
    ' Check for changes relevant to this form
    ' ****************************************************************

    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFX) = True) Or KnowledgeDateChanged Then
        Call getFXRates(Date_ValueDate.Value.Date) ' Should give FXs for current Value.

				RefreshGrid = True
				RefreshValuation = True
			End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblFX", ex.StackTrace, True)
    End Try

    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblRecInventory) = True) Or KnowledgeDateChanged Then
        If (AdministratorInventoryCache IsNot Nothing) Then
          Dim UpdateDetail As String = e.UpdateDetail(RenaissanceGlobals.RenaissanceChangeID.tblRecInventory).Trim

          If (UpdateDetail.Length = 0) OrElse (KnowledgeDateChanged) Then
            ' Clear whole cash
            AdministratorInventoryCache.Clear()
          Else
            Dim FundIDs() As String = UpdateDetail.Split(New Char() {",", "|"}, StringSplitOptions.RemoveEmptyEntries)
            Dim ThisAdminFundID As String
            Dim thisFundID As Integer

            If (FundIDs Is Nothing) OrElse (FundIDs.Length = 0) Then
              ' Clear whole cash
              AdministratorInventoryCache.Clear()
            Else
              For Each ThisAdminFundID In FundIDs
                If (ThisAdminFundID = "-") Then
                  AdministratorInventoryCache.Clear()
                  Exit For
                End If

                thisFundID = CInt(Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblFund, 0, "FundID", "RN", "FundAdministratorCode='" & ThisAdminFundID & "'"), 0))

                If (thisFundID > 0) Then
                  If (AdministratorInventoryCache.ContainsKey(thisFundID)) Then
                    AdministratorInventoryCache.Remove(thisFundID)
                  End If
                End If
              Next
            End If
          End If

        End If
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblRecInventory", ex.StackTrace, True)
    End Try

    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblRecHoldings) = True) Or KnowledgeDateChanged Then
        If (AdministratorHoldingsCache IsNot Nothing) Then

          Dim UpdateDetail As String = e.UpdateDetail(RenaissanceGlobals.RenaissanceChangeID.tblRecHoldings).Trim

          If (UpdateDetail.Length = 0) Then
            ' Clear whole cash
            AdministratorHoldingsCache.Clear()
          Else
            Dim FundIDs() As String = UpdateDetail.Split(New Char() {",", "|"}, StringSplitOptions.RemoveEmptyEntries)
            Dim ThisAdminFundID As String
            Dim thisFundID As Integer

            If (FundIDs Is Nothing) OrElse (FundIDs.Length = 0) Then
              ' Clear whole cash
              AdministratorHoldingsCache.Clear()
            Else
              For Each ThisAdminFundID In FundIDs
                If (ThisAdminFundID = "-") Then
                  AdministratorHoldingsCache.Clear()
                  Exit For
                End If

                thisFundID = CInt(Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblFund, 0, "FundID", "RN", "FundAdministratorCode='" & ThisAdminFundID & "'"), 0))

                If (thisFundID > 0) Then
                  If (AdministratorHoldingsCache.ContainsKey(thisFundID)) Then
                    AdministratorHoldingsCache.Remove(thisFundID)
                  End If
                End If
              Next
            End If
          End If

        End If
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblRecHoldings", ex.StackTrace, True)
    End Try

    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblPrice) = True) Or KnowledgeDateChanged Then
        Call GetInstrumentPrice(-1) ' Flush Static Variables

        If (BestPricesTable IsNot Nothing) Then
          BestPricesTable.Clear()
        End If

        BestPricesTable = Nothing
        PriceRowCache.Clear()

        RefreshGrid = True

        RefreshValuation = True
        RefreshNotionals = True
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblPrice", ex.StackTrace, True)
    End Try

    ' Changes to the tblFund table :-

    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFund) = True) Or KnowledgeDateChanged Then
        Dim CheckFundRefresh As Boolean = True
        Dim UpdateDetail As String = e.UpdateDetail(RenaissanceGlobals.RenaissanceChangeID.tblFund)
        Dim FundID As Integer = 0

        Call SetFundCombo()

        If (UpdateDetail.Length > 0) Then
          If IsNumeric(Combo_Fund.SelectedValue) Then
            FundID = CInt(Combo_Fund.SelectedValue)

            If UpdateDetail.Split(New Char() {",", "|"}, StringSplitOptions.RemoveEmptyEntries).Contains(FundID.ToString) = False Then
              CheckFundRefresh = False
            End If
          End If
        End If

        If (CheckFundRefresh) Then
          RefreshValuation = True
        End If
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblFund", ex.StackTrace, True)
    End Try

    ' Changes to the tblInstrumentType table :-

    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrumentType) = True) Or KnowledgeDateChanged Then
        _FuturesInstrumentTypes = ""
        Call SetInstrumentCombo()

        RefreshValuation = True
        RefreshNotionals = True
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblInstrumentType", ex.StackTrace, True)
    End Try

    ' Changes to the tblRecAvailablilities table :-

    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblRecAvailablilities) = True) Or KnowledgeDateChanged Then
        If (FundAvailabilities IsNot Nothing) Then
          FundAvailabilities.Clear()
        End If
        FundAvailabilities = Nothing

        RefreshValuation = False
        RefreshGrid = True
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblInstrumentType", ex.StackTrace, True)
    End Try

    ' Changes to the tblInstrument table :-

    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrument) = True) Or KnowledgeDateChanged Then
        Dim CheckInstrumentsRefresh As Boolean = True
        Dim UpdateDetail As String = e.UpdateDetail(RenaissanceGlobals.RenaissanceChangeID.tblInstrument)

        Call SetInstrumentCombo()

        ' Is it possible to tell if this Instrument change affects the selected fund ?

        If (UpdateDetail.Length > 0) Then
          Dim FundID As Integer = 0
          Dim TransactionDS As RenaissanceDataClass.DSTransaction
          Dim TransactionTbl As RenaissanceDataClass.DSTransaction.tblTransactionDataTable

          If IsNumeric(Combo_Fund.SelectedValue) Then
            FundID = CInt(Combo_Fund.SelectedValue)

            TransactionDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblTransaction)

            If (TransactionDS IsNot Nothing) Then
              TransactionTbl = TransactionDS.tblTransaction

              ' Purify Update Detail (Will remove leading or trailing commas)
              Dim InstrumentIDs As String = String.Join(",", UpdateDetail.Split(New Char() {",", "|"}, StringSplitOptions.RemoveEmptyEntries))

              ' Are the Transactions in the selected Fund ?
              If (TransactionTbl.Select("(TransactionFund=" & FundID.ToString() & ") AND (TransactionInstrument IN (" & InstrumentIDs & "))").Length = 0) Then
                CheckInstrumentsRefresh = False
              End If

            End If ' (TransactionDS IsNot Nothing)

          End If ' IsNumeric(Combo_Fund.SelectedValue) 

        End If ' (UpdateDetail.Length > 0)

        ' Is the Update necessary ?

        If (CheckInstrumentsRefresh) Then
          RefreshValuation = True
          RefreshNotionals = True
        End If

      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblInstrument", ex.StackTrace, True)
    End Try

    ' Changes to the tblTransaction table :-

    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblTransactionContra) = True) Or KnowledgeDateChanged Then
        Dim CheckTransactionRefresh As Boolean = True
        Dim UpdateDetail As String = e.UpdateDetail(RenaissanceGlobals.RenaissanceChangeID.tblTransactionContra)

        ' Is it possible to tell if this transaction change affects the selected fund ?

        If (UpdateDetail.Length > 0) Then

          Dim FundID As Integer = 0
          Dim TransactionDS As RenaissanceDataClass.DSTransaction
          Dim TransactionTbl As RenaissanceDataClass.DSTransaction.tblTransactionDataTable

          If IsNumeric(Combo_Fund.SelectedValue) Then
            FundID = CInt(Combo_Fund.SelectedValue)

            TransactionDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblTransaction)

            If (TransactionDS IsNot Nothing) Then
              TransactionTbl = TransactionDS.tblTransaction

              ' Purify Update Detail (Will remove leading or trailing commas)
              Dim TransactionIDs As String = String.Join(",", UpdateDetail.Split(New Char() {",", "|"}, StringSplitOptions.RemoveEmptyEntries))

							' Are the Transactions in the selected Fund ?

              If (TransactionIDs.Length = 0) OrElse (TransactionTbl.Select("(TransactionFund=" & FundID.ToString() & ") AND (TransactionParentID IN (" & TransactionIDs & "))").Length = 0) Then
                CheckTransactionRefresh = False
							ElseIf (TransactionIDs.Length = 0) OrElse (TransactionTbl.Select("(TransactionFund=" & FundID.ToString() & ") AND (TransactionParentID IN (" & TransactionIDs & ")) AND (TransactionSpecificToFundUnitID > 0)").Length > 0) Then
								If (ClassSpecificPnLTable IsNot Nothing) Then
									ClassSpecificPnLTable.Clear()
									ClassSpecificPnLTable = Nothing
								End If
							End If

            End If
          End If
        End If


        If (CheckTransactionRefresh) Then
          RefreshGrid = True
          RefreshValuation = True
          RefreshNotionals = True
				End If

				If (KnowledgeDateChanged) Then
					If (ClassSpecificPnLTable IsNot Nothing) Then
						ClassSpecificPnLTable.Clear()
						ClassSpecificPnLTable = Nothing
					End If
				End If

      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblTransaction", ex.StackTrace, True)
    End Try

    ' Changes to the tblSophisStatusGroups table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblSophisStatusGroups) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Call SetTransactionGroupCombo()

      RefreshValuation = True
      RefreshNotionals = True
    End If

    ' Changes to the tblTradeStatus table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblTradeStatus) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Call SetTradeStatusCombo()

    End If

    ' Changes to the tblFeeDefinitions table :-

    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFeeDefinitions) = True) Or KnowledgeDateChanged Then

      If (TabControl_Reconciliation.SelectedTab Is Tab_AutomatedFees) Then
        RefreshCurrentTab(False, False)
      Else
        TabDataStatus(Tab_Fees.Text) = False
      End If

    End If

    ' Changes to the Administrator_Disponsbilities :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Administrator_Disponsbilities) = True) Or KnowledgeDateChanged Then
      Dim UpdateDetail As String = e.UpdateDetail(RenaissanceGlobals.RenaissanceChangeID.Administrator_Holdings)
      Dim UpdateItem As String
      Dim FundID As Integer = 0

      Try
        If (Not lock_tblFundValuation.IsWriteLockHeld) Then
          lock_tblFundValuation.EnterWriteLock() ' Don't Clear FundAvailabilities while the FundValuation process is running !
        End If

        If (UpdateDetail.Length > 0) Then
          If IsNumeric(Combo_Fund.SelectedValue) Then
            FundID = CInt(Combo_Fund.SelectedValue)

            For Each UpdateItem In UpdateDetail.Split(New Char() {",", "|"}, StringSplitOptions.RemoveEmptyEntries)
              If (IsNumeric(UpdateItem)) Then
                If (CInt(UpdateItem) = 0) OrElse (CInt(UpdateItem) = FundID) Then
                  RefreshValuation = True
                  RefreshGrid = True
                  If (FundAvailabilities IsNot Nothing) Then
                    FundAvailabilities.Clear()
                    FundAvailabilities = Nothing
                  End If
                  Exit For
                End If
              End If
            Next

          End If
        Else
          RefreshValuation = True
          RefreshGrid = True
          If (FundAvailabilities IsNot Nothing) Then
            FundAvailabilities.Clear()
            FundAvailabilities = Nothing
          End If
        End If

      Catch ex As Exception
      Finally
        If (lock_tblFundValuation.IsWriteLockHeld) Then
          lock_tblFundValuation.ExitWriteLock()
        End If
      End Try
    End If

    ' Changes to the Administrator_Holdings :-

    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Administrator_Holdings) = True) Or KnowledgeDateChanged Then
        Dim UpdateDetail As String = e.UpdateDetail(RenaissanceGlobals.RenaissanceChangeID.Administrator_Holdings)
        Dim UpdateItem As String
        Dim FundID As Integer = 0

        If (Not lock_tblFundValuation.IsWriteLockHeld) Then
          lock_tblFundValuation.EnterWriteLock() ' Don't Clear FundAvailabilities while the FundValuation process is running !
        End If

        If (UpdateDetail.Length > 0) Then
          If IsNumeric(Combo_Fund.SelectedValue) Then
            FundID = CInt(Combo_Fund.SelectedValue)

            For Each UpdateItem In UpdateDetail.Split(New Char() {",", "|"}, StringSplitOptions.RemoveEmptyEntries)
              If (IsNumeric(UpdateItem)) Then
                If (CInt(UpdateItem) = 0) Then
                  AdministratorInventoryCache.Clear()
                  AdministratorHoldingsCache.Clear()
                  RefreshValuation = True
                  RefreshGrid = True
                  Exit For
                Else
                  Try
                    If (AdministratorInventoryCache.ContainsKey(CInt(UpdateItem))) Then
                      AdministratorInventoryCache.Remove(CInt(UpdateItem))
                      RefreshValuation = True
                      RefreshGrid = True
                    End If
                    If (AdministratorHoldingsCache.ContainsKey(CInt(UpdateItem))) Then
                      AdministratorHoldingsCache.Remove(CInt(UpdateItem))
                      RefreshValuation = True
                      RefreshGrid = True
                    End If
                  Catch ex As Exception
                  End Try

                  If (CInt(UpdateItem) = FundID) Then
                    RefreshValuation = True
                  End If
                End If
              End If
            Next
          End If
        Else
          AdministratorInventoryCache.Clear()
          AdministratorHoldingsCache.Clear()
          RefreshValuation = True
          RefreshGrid = True
        End If
      End If
    Catch ex As Exception
      If (lock_tblFundValuation.IsWriteLockHeld) Then
        lock_tblFundValuation.ExitWriteLock()
      End If

      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblFund", ex.StackTrace, True)
    Finally
      If (lock_tblFundValuation.IsWriteLockHeld) Then
        lock_tblFundValuation.ExitWriteLock()
      End If
    End Try

    ' Changes to the KnowledgeDate :-
    Try
      If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
        RefreshGrid = True
        RefreshValuation = True
        RefreshNotionals = True
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: KnowledgeDate", ex.StackTrace, True)
    End Try

    ' Changes to the tblUserPermissions table :-

    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

        ' Check ongoing permissions.

        Call CheckPermissions()
        If (HasReadPermission = False) Then
          Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

          FormIsValid = False
          InPaint = OrgInPaint
          Me.Close()
          Exit Sub
        End If

        RefreshGrid = True

      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: tblUserPermissions", ex.StackTrace, True)
    End Try


    ' ****************************************************************
    ' Changes to the Main FORM table :-
    ' (or the tblTransactions table)
    ' ****************************************************************

    Try
      ' Refresh Valuation ?
      If RefreshValuation Then
        If (FeesObject IsNot Nothing) Then
          FeesObject.ValuationNeedsUpdate = True
        End If
        If (TradingValuationObject IsNot Nothing) Then
          TradingValuationObject.ValuationNeedsUpdate = True
        End If
      End If
    Catch ex As Exception
    End Try

    Try
      If (RefreshNotionals) Then
        If (myNotionalsTable IsNot Nothing) Then
          myNotionalsTable.Clear()
        End If

        If (FuturesNotionalsTable IsNot Nothing) Then
          Try
            FuturesNotionalsTable.Clear()
            FuturesNotionalsTable = Nothing
          Catch ex As Exception
          End Try
        End If
      End If
    Catch ex As Exception
    End Try

    Try
      If (RefreshGrid = True) Or KnowledgeDateChanged Then

        ' Re-Set grid etc.
        TabDataStatus.Clear()

        Call RefreshCurrentTab(False, True)

      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: Main Form table", ex.StackTrace, True)
    End Try

    InPaint = OrgInPaint

  End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

  ''' <summary>
  ''' Gets the transaction select string.
  ''' </summary>
  ''' <returns>System.String.</returns>
  Private Function GetTransactionSelectString() As String
    ' *******************************************************************************
    ' Build a Select String appropriate to the form status.
    ' *******************************************************************************

    Dim SelectString As String
    Dim FieldSelectString As String

    SelectString = ""
    FieldSelectString = ""
    Me.MainForm.SetToolStripText(Label_Status, "")

    Try

      ' Select on Fund...

      If (Me.Combo_Fund.SelectedIndex > 0) Then
        If IsFieldNotionalField("FundID") = True Then
          SelectString = "(FundID=" & Combo_Fund.SelectedValue.ToString & ")"
        End If
      End If

      ' Select on Instrument

      If (Me.Combo_FuturesInstrument.SelectedIndex > 0) Then
        If IsFieldNotionalField("FuturesInstrumentID") = True Then
          If (SelectString.Length > 0) Then
            SelectString &= " AND "
          End If
          SelectString &= "(FuturesInstrumentID=" & Combo_FuturesInstrument.SelectedValue.ToString & ")"
        End If
      End If

      If SelectString.Length <= 0 Then
        SelectString = "true"
      End If

    Catch ex As Exception
      SelectString = "true"
    End Try

    Return SelectString

  End Function

  ''' <summary>
  ''' Determines whether [is field notional field] [the specified field name].
  ''' </summary>
  ''' <param name="FieldName">Name of the field.</param>
  ''' <returns><c>true</c> if [is field notional field] [the specified field name]; otherwise, <c>false</c>.</returns>
  Private Function IsFieldNotionalField(ByVal FieldName As String) As Boolean
    ' *******************************************************************************
    ' Simple function to return a boolean value indicating whether or not a column name is
    ' part of the tblTransactions table.
    ' *******************************************************************************

    Dim NotionalDS As New RenaissanceDataClass.DSSelectFuturesNotional

    Try
      If (NotionalDS Is Nothing) Then
        Return False
      Else
        Return NotionalDS.tblSelectFuturesNotional.Columns.Contains(FieldName)
      End If
    Catch ex As Exception
      Return (False)
    End Try

    Return (False)
  End Function

  ''' <summary>
  ''' Sets the sorted rows_ notional.
  ''' </summary>
  ''' <param name="pForceRefresh">if set to <c>true</c> [p force refresh].</param>
  Private Sub SetSortedRows_Notional(ByVal pForceRefresh As Boolean)
    ' *******************************************************************************
    ' Build a Select String appropriate to the form status and apply it to the DataView object.
    ' *******************************************************************************

    Dim myNotionalsAdaptor As SqlDataAdapter = Nothing
    Dim myNotionalsConnection As SqlConnection = Nothing

    Try

      ' Permissions (Add trades button)

      If (HasTransactionInsertPermission) Then
        Me.Button_TradeSelectedRealised.Enabled = True
        Me.Button_TradeSelectedUnRealised.Enabled = True
      Else
        Me.Button_TradeSelectedRealised.Enabled = False
        Me.Button_TradeSelectedUnRealised.Enabled = False
      End If

      '

      Dim SelectString As String

      If (Me.Created) And (Not Me.IsDisposed) Then

        If (myNotionalsTable.Rows.Count = 0) OrElse (pForceRefresh) Then

          myNotionalsTable.Clear()

          Dim FundID As Integer = 0
          Dim ValueDate As Date = Date_ValueDate.Value
          Dim StatusGroupFilter As String = Combo_TransactionGroup.Text
          Dim AdministratorDatesFilter As Integer = 0
          Dim UnRealisedCalculationType As RenaissanceGlobals.CalculationTypes_Profit = CalculationTypes_Profit.FIFO

          myNotionalsAdaptor = New SqlDataAdapter
          myNotionalsConnection = MainForm.GetVeniceConnection()
          Call MainForm.MainAdaptorHandler.Set_AdaptorCommands(myNotionalsConnection, myNotionalsAdaptor, RenaissanceStandardDatasets.tblSelectFuturesNotional.TableName)

          If (Combo_UnRealisedCalculationType.SelectedIndex >= 0) AndAlso (IsNumeric(Combo_UnRealisedCalculationType.SelectedValue)) Then
            UnRealisedCalculationType = CInt(Combo_UnRealisedCalculationType.SelectedValue)
          End If

          If (myNotionalsAdaptor.SelectCommand.Parameters.Contains("@FundID")) Then
            myNotionalsAdaptor.SelectCommand.Parameters("@FundID").Value = FundID
          End If

          If (myNotionalsAdaptor.SelectCommand.Parameters.Contains("@ValueDate")) Then
            myNotionalsAdaptor.SelectCommand.Parameters("@ValueDate").Value = ValueDate
          End If
          If (myNotionalsAdaptor.SelectCommand.Parameters.Contains("@StatusGroupFilter")) Then
            myNotionalsAdaptor.SelectCommand.Parameters("@StatusGroupFilter").Value = StatusGroupFilter
          End If
          If (myNotionalsAdaptor.SelectCommand.Parameters.Contains("@AdministratorDatesFilter")) Then
            myNotionalsAdaptor.SelectCommand.Parameters("@AdministratorDatesFilter").Value = AdministratorDatesFilter
          End If
          If (myNotionalsAdaptor.SelectCommand.Parameters.Contains("@UnRealisedCalculationType")) Then
            myNotionalsAdaptor.SelectCommand.Parameters("@UnRealisedCalculationType").Value = UnRealisedCalculationType
          End If

          If (myNotionalsAdaptor.SelectCommand.Parameters.Contains("@Knowledgedate")) Then
            myNotionalsAdaptor.SelectCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
          End If

          MainForm.SetToolStripText(StatusLabel, "Loading Notionals Data, Fund " & FundID.ToString() & ", " & ValueDate.ToString("dd MMM yyyy"))

          Dim thisGetReader As New VeniceMain.GetDataReaderClass
          thisGetReader.MyCommand = myNotionalsAdaptor.SelectCommand

          SyncLock myNotionalsAdaptor.SelectCommand.Connection

            MainForm.RunDataReader(thisGetReader)

            SyncLock myNotionalsDataset
              'myNotionalsAdaptor.Fill(myNotionalsDataset, myNotionalsDataset.tblSelectFuturesNotional.TableName)

              myNotionalsTable.Load(thisGetReader.myReader)
            End SyncLock
          End SyncLock
          thisGetReader.MyCommand = Nothing
          thisGetReader.myReader = Nothing

        End If

        SelectString = GetTransactionSelectString()

        Try

          If (myNotionalsDataView IsNot Nothing) AndAlso (Not (myNotionalsDataView.RowFilter = SelectString)) Then
            myNotionalsDataView.RowFilter = SelectString
          End If

        Catch ex As Exception
          SelectString = "true"
          myNotionalsDataView.RowFilter = SelectString
        End Try

        Me.Label_Status.Text = "(" & myNotionalsDataView.Count.ToString & " Records) " & SelectString

      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error getting Futures Notional information.", ex.StackTrace, True)
    Finally
      If (myNotionalsConnection IsNot Nothing) AndAlso (myNotionalsConnection.State And ConnectionState.Open) Then
        myNotionalsConnection.Close()
      End If

      MainForm.SetToolStripText(StatusLabel, "")
    End Try

  End Sub

  ''' <summary>
  ''' Sets the Automated fees tab.
  ''' </summary>
  Private Sub SetSortedRows_AutomatedFees()
    ' *******************************************************************************
    '
    '
    ' *******************************************************************************
    Dim NAVBeforeFees As Double = 0.0# ' NOT YET Implimented !!!!!!

    If (myAutomaticFeesTable Is Nothing) Then
      Exit Sub
    End If

    Try
      Dim FundID As Integer
      Dim FeeFromDate As Date
      Dim FeeToDate As Date
      Dim FeeShareClassInstrumentID As Integer
      Dim FeeRow As RenaissanceDataClass.DSFeeDefinitions.tblFeeDefinitionsRow = Nothing

      Dim TransactionsDataset As DSTransaction
      Dim TransactionsTable As DSTransaction.tblTransactionDataTable
      Dim SelectedTransactions() As DSTransaction.tblTransactionRow
      Dim thisTransaction As DSTransaction.tblTransactionRow
      Dim FundRow As RenaissanceDataClass.DSFund.tblFundRow = Nothing

      Dim SingleShareClassFlag As Boolean
      Dim PreviousNAVDictionary As New Dictionary(Of String, Double)
      Dim NAVAfterSubscriptionsDictionary As New Dictionary(Of String, Double)
      Dim FXToFund As Double

      Dim thisPreviousNAV As Double
      Dim PreviousNAVFundTotal As Double
      Dim SubscriptionsTotal As Double
      Dim thisSubscriptionValue As Double

      If (IsNumeric(Combo_Fund.SelectedValue)) Then
        FundID = CInt(Combo_Fund.SelectedValue)
        FundRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, FundID)
      Else
        If (myAutomaticFeesTable IsNot Nothing) Then
          myAutomaticFeesTable.Clear()
        End If
        Exit Sub
      End If

      FeeFromDate = Date_FeesSince.Value.Date
      FeeToDate = Date_ValueDate.Value.Date

      If (TabControl_Reconciliation.TabPages.Contains(Tab_Fees)) Then
        ' Single Share Class
        SingleShareClassFlag = True
      Else
        ' Multi Share Class
        SingleShareClassFlag = False
      End If

      ' 0) Get required NAV Numbers

      Dim FundValuation As FundFeeCalculator.FundValuationDetails = Nothing
      Dim SubscritptionsToFeeStartDate As Dictionary(Of Integer, SR_InformationClass) = Nothing
      Dim SubscritptionsToValueDate As Dictionary(Of Integer, SR_InformationClass) = Nothing
      Dim thisSubscritptionsToFeeStartDate As SR_InformationClass = Nothing
      Dim thisSubscritptionsToValueDate As SR_InformationClass = Nothing

      ' A) Previous NAV

      Dim AdminShareClassDetails As New DataTable
      Dim AdminShareRow As DataRow
      Dim AdminInstrument As DSInstrument.tblInstrumentRow
      Dim SubscriptionInstrumentID As Integer

      Dim thisSQLConnection As SqlConnection = MainForm.GetVeniceConnection()
      Try
        Dim thisSQLCommand As SqlCommand = Nothing
        Dim RetryCounter As Integer

        ' Fee Start Date Values
        ' Get Data and populate
        thisSQLCommand = New SqlCommand
        thisSQLCommand.CommandType = CommandType.StoredProcedure
        thisSQLCommand.CommandText = "adp_tblRecValuation_SelectCommand"
        thisSQLCommand.Parameters.Add(New SqlParameter("@NavDate", SqlDbType.Date)).Value = FeeFromDate
        thisSQLCommand.Parameters.Add(New SqlParameter("@FundCode", SqlDbType.NVarChar, 50)).Value = FundRow.FundAdministratorCode
        thisSQLCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", SqlDbType.DateTime)).Value = MainForm.Main_Knowledgedate

        For RetryCounter = 0 To 1
          thisSQLCommand.Connection = thisSQLConnection

          Try

            AdminShareClassDetails.Load(thisSQLCommand.ExecuteReader)
            Exit For
          Catch ex As Exception

            Try
              thisSQLConnection.Close()
            Catch Inner_ex As Exception
            End Try

            thisSQLConnection = MainForm.GetVeniceConnection()
            AdminShareClassDetails.Clear()
          End Try
        Next
      Catch ex As Exception
      Finally
        Try
          If (thisSQLConnection IsNot Nothing) Then
            thisSQLConnection.Close()
          End If
        Catch ex As Exception
        End Try
      End Try

      PreviousNAVFundTotal = 0.0#
      SubscriptionsTotal = 0.0#

      If (AdminShareClassDetails IsNot Nothing) Then
        For Each AdminShareRow In AdminShareClassDetails.Select
          If CStr(Nz(AdminShareRow("ISINCode"), "")).Length > 10 Then
            AdminInstrument = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, 0, "", "RN", "(InstrumentISIN='" & Nz(AdminShareRow("ISINCode"), "MissingISIN") & "') AND (InstrumentType=" & CInt(InstrumentTypes.Unit).ToString & ")")

            If (AdminInstrument IsNot Nothing) Then
              PreviousNAVDictionary.Add(AdminInstrument.InstrumentID, CDbl(Nz(AdminShareRow("NetAssets_Value"), 0.0#)))
            End If

            PreviousNAVFundTotal += CDbl(Nz(AdminShareRow("NetAssets_Value"), 0.0#))
          End If
        Next
      End If
      PreviousNAVDictionary.Add(0, PreviousNAVFundTotal)

      ' B) NAV After Subscriptions.

      Try
        Dim AdministratorDatesFilter As RenaissanceGlobals.AdministratorDatesFilter = RenaissanceGlobals.AdministratorDatesFilter.None

        MainForm.SetToolStripText(StatusLabel, "Calculating Fund Valuation.")

        If (MainForm.GetCheckBoxChecked(Me.Check_SR_IgnoreStatus)) Then
          AdministratorDatesFilter += RenaissanceGlobals.AdministratorDatesFilter.SubscriptionRedemption_IgnoreTransactionStatus
        End If
        If (MainForm.GetCheckBoxChecked(Me.Check_SR_IgnoreTodaysTrades)) Then
          AdministratorDatesFilter += RenaissanceGlobals.AdministratorDatesFilter.SubscriptionRedemption_IgnoreTodaysTrades
        End If

        FundValuation = GetFeesValuation(FeesObject, FundID, FeeToDate, FeeFromDate, Nz(MainForm.GetComboSelectedValue(Combo_TransactionGroup), ""), AdministratorDatesFilter, Check_AdministratorPriceDates.Checked)

        SubscritptionsToFeeStartDate = FeesObject.SubscriptionsToDate(FeeFromDate, Not Check_SR_IgnoreTodaysTrades.Checked)
        SubscritptionsToValueDate = FeesObject.SubscriptionsToDate(FeeToDate, Not Check_SR_IgnoreTodaysTrades.Checked)

      Catch ex As Exception
        MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error getting Valuation or Fee Calculations.", ex.StackTrace, True)
      Finally
        MainForm.SetToolStripText(StatusLabel, "")
      End Try

      If (SubscritptionsToValueDate IsNot Nothing) AndAlso (SubscritptionsToFeeStartDate IsNot Nothing) Then
        For Each SubscriptionInstrumentID In SubscritptionsToValueDate.Keys

          AdminInstrument = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, SubscriptionInstrumentID)
          If (AdminInstrument Is Nothing) Then
            FXToFund = 1.0#
          Else
            FXToFund = GetFXToFund(AdminInstrument.InstrumentCurrencyID, FundRow.FundBaseCurrency)
          End If

          If (SubscritptionsToFeeStartDate IsNot Nothing) Then SubscritptionsToFeeStartDate.TryGetValue(SubscriptionInstrumentID, thisSubscritptionsToFeeStartDate)
          If (SubscritptionsToValueDate IsNot Nothing) Then SubscritptionsToValueDate.TryGetValue(SubscriptionInstrumentID, thisSubscritptionsToValueDate)

          thisSubscriptionValue = ((thisSubscritptionsToValueDate.ValueSubscribed - thisSubscritptionsToValueDate.ValueRedeemed) - (thisSubscritptionsToFeeStartDate.ValueSubscribed - thisSubscritptionsToFeeStartDate.ValueRedeemed)) * IIf(FXToFund = 0.0#, 1.0#, FXToFund)

          If (Not PreviousNAVDictionary.TryGetValue(SubscriptionInstrumentID, thisPreviousNAV)) Then
            thisPreviousNAV = 0.0#
          End If

          NAVAfterSubscriptionsDictionary.Add(SubscriptionInstrumentID, (thisPreviousNAV + thisSubscriptionValue))
          SubscriptionsTotal += (thisSubscriptionValue)
        Next
      End If

      NAVAfterSubscriptionsDictionary.Add(0, (PreviousNAVFundTotal + SubscriptionsTotal))

      ' 1) Get Expected Fee Transactions

      Dim FeesTable As DataTable = GetFeeTransactions(MainForm, FundID, FeeFromDate, FeeToDate)
      Dim thisFeeRow As DataRow

      ' 2) Get Existing Transactions

      TransactionsDataset = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblTransaction), DSTransaction)
      If (TransactionsDataset Is Nothing) Then
        If (myAutomaticFeesTable IsNot Nothing) Then
          myAutomaticFeesTable.Clear()
        End If
        Exit Sub
      End If

      TransactionsTable = TransactionsDataset.tblTransaction
      SelectedTransactions = TransactionsTable.Select("(TransactionFund=" & FundID.ToString & ") AND (TransactionFeeID>0) AND (TransactionValueDate>'" & FeeFromDate.ToString(QUERY_SHORTDATEFORMAT) & "') AND (TransactionValueDate<='" & FeeToDate.ToString(QUERY_SHORTDATEFORMAT) & "')")

      ' Stitch together for the display table.

      Dim thisDisplayRow As DataRow
      Dim RowMatched As Boolean = False
      Dim DetalsString As String

      For Each thisDisplayRow In myAutomaticFeesTable.Select
        thisDisplayRow("NotUsed") = True
      Next

      For Each thisFeeRow In FeesTable.Select
        RowMatched = False

        FeeRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFeeDefinitions, CInt(thisFeeRow("FeeID")))

        For Each thisDisplayRow In myAutomaticFeesTable.Select
          If (thisDisplayRow.RowState <> DataRowState.Added) AndAlso (CInt(thisFeeRow("FeeID")) = CInt(thisDisplayRow("FeeID"))) And (CDate(thisFeeRow("TransactionValueDate")) = CDate(thisDisplayRow("TransactionValueDate"))) Then
            RowMatched = True

            FeeShareClassInstrumentID = CInt(Nz(thisFeeRow("TransactionSpecificShareClassID"), 0))

            Select Case CType(thisFeeRow("FeeRateType"), FeesType)
              Case FeesType.PercentageValue ' Percentage

                DetalsString = FeeRow.FeeRate.ToString(DISPLAYMEMBER_PERCENTAGEFORMAT) & " of "

                Select Case CType(thisFeeRow("FeeCalculationMethod"), FeesPercentageType)

                  Case FeesPercentageType.ValueAfterSubscriptions  ' % After Subscriptions

                    DetalsString &= "value after Subscriptions"

                    If (FeeShareClassInstrumentID = 0) OrElse (Not NAVAfterSubscriptionsDictionary.ContainsKey(FeeShareClassInstrumentID)) Then
                      thisDisplayRow("TransactionAmount") = CDbl(thisFeeRow("TransactionAmount")) * NAVAfterSubscriptionsDictionary(0)
                      DetalsString &= " (" & NAVAfterSubscriptionsDictionary(0).ToString(DISPLAYMEMBER_INTEGERFORMAT) & ")"
                    Else
                      thisDisplayRow("TransactionAmount") = CDbl(thisFeeRow("TransactionAmount")) * NAVAfterSubscriptionsDictionary(FeeShareClassInstrumentID)
                      DetalsString &= " (" & NAVAfterSubscriptionsDictionary(FeeShareClassInstrumentID).ToString(DISPLAYMEMBER_INTEGERFORMAT) & ")"
                    End If


                  Case FeesPercentageType.ValueBeforeFees  ' % Before Fees

                    DetalsString &= "value before fees"
                    thisDisplayRow("TransactionAmount") = CDbl(thisFeeRow("TransactionAmount")) * NAVBeforeFees
                    DetalsString &= " (" & NAVBeforeFees.ToString(DISPLAYMEMBER_INTEGERFORMAT) & ")"

                  Case Else ' % of Last NAV

                    DetalsString &= "last NAV"

                    If (FeeShareClassInstrumentID = 0) OrElse (Not PreviousNAVDictionary.ContainsKey(FeeShareClassInstrumentID)) Then
                      thisDisplayRow("TransactionAmount") = CDbl(thisFeeRow("TransactionAmount")) * PreviousNAVDictionary(0)
                      DetalsString &= " (" & PreviousNAVDictionary(0).ToString(DISPLAYMEMBER_INTEGERFORMAT) & ")"
                    Else
                      thisDisplayRow("TransactionAmount") = CDbl(thisFeeRow("TransactionAmount")) * PreviousNAVDictionary(FeeShareClassInstrumentID)
                      DetalsString &= " (" & PreviousNAVDictionary(FeeShareClassInstrumentID).ToString(DISPLAYMEMBER_INTEGERFORMAT) & ")"
                    End If
                End Select

                If (FeeRow IsNot Nothing) Then
                  DetalsString &= (" " & CType(FeeRow.FeeCalculationPeriod, DealingPeriod).ToString) & ", paid " & CType(FeeRow.FeePaymentPeriod, DealingPeriod).ToString
                End If

              Case Else ' Amount

                If (FeeRow IsNot Nothing) Then
                  DetalsString = "Fixed Value of " & FeeRow.FeeRate.ToString(DISPLAYMEMBER_DOUBLEFORMAT) & " per " & RenaissanceUtilities.DatePeriodFunctions.SinglePeriodName(FeeRow.FeeCalculationPeriod) & ", paid " & CType(FeeRow.FeePaymentPeriod, DealingPeriod).ToString
                Else
                  DetalsString = "Fixed Amount"
                End If

                thisDisplayRow("TransactionAmount") = thisFeeRow("TransactionAmount")

            End Select

            thisDisplayRow("FeeName") = IIf(FeeRow IsNot Nothing, FeeRow.Description, "")
            thisDisplayRow("InstrumentDescription") = Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, CInt(Nz(thisFeeRow("TransactionInstrumentID"), 0)), "InstrumentDescription"), "Unknown")
            thisDisplayRow("TransactionFundID") = thisFeeRow("TransactionFundID")
            thisDisplayRow("TransactionSubFundID") = thisFeeRow("TransactionSubFundID")
            thisDisplayRow("TransactionInstrumentID") = thisFeeRow("TransactionInstrumentID")
            thisDisplayRow("TransactionSpecificShareClassID") = thisFeeRow("TransactionSpecificShareClassID")
            thisDisplayRow("TransactionSettlementDate") = thisFeeRow("TransactionSettlementDate")
            thisDisplayRow("FeeRateType") = thisFeeRow("FeeRateType")
            thisDisplayRow("FeeCalculationMethod") = thisFeeRow("FeeCalculationMethod")
            thisDisplayRow("FeeDescription") = DetalsString
            thisDisplayRow("ExistingTicket") = ""
            thisDisplayRow("ExistingParentID") = 0
            thisDisplayRow("ExistingInstrumentID") = 0
            thisDisplayRow("ExistingValueDate") = #1/1/1900#
            thisDisplayRow("ExistingAmount") = 0.0#
            thisDisplayRow("ToBook") = thisDisplayRow("TransactionAmount")
            thisDisplayRow("NotUsed") = False

            Exit For
          End If

        Next

        If (RowMatched = False) Then
          ' Add Row

          thisDisplayRow = myAutomaticFeesTable.NewRow


          Select Case CType(thisFeeRow("FeeRateType"), FeesType)
            Case FeesType.PercentageValue ' Percentage

              DetalsString = FeeRow.FeeRate.ToString(DISPLAYMEMBER_PERCENTAGEFORMAT) & " of "

              Select Case CType(thisFeeRow("FeeCalculationMethod"), FeesPercentageType)

                Case FeesPercentageType.ValueAfterSubscriptions  ' % After Subscriptions

                  DetalsString &= "value after Subscriptions"

                  If (FeeShareClassInstrumentID = 0) OrElse (Not NAVAfterSubscriptionsDictionary.ContainsKey(FeeShareClassInstrumentID)) Then
                    thisDisplayRow("TransactionAmount") = CDbl(thisFeeRow("TransactionAmount")) * NAVAfterSubscriptionsDictionary(0)
                    DetalsString &= " (" & NAVAfterSubscriptionsDictionary(0).ToString(DISPLAYMEMBER_INTEGERFORMAT) & ")"
                  Else
                    thisDisplayRow("TransactionAmount") = CDbl(thisFeeRow("TransactionAmount")) * NAVAfterSubscriptionsDictionary(FeeShareClassInstrumentID)
                    DetalsString &= " (" & NAVAfterSubscriptionsDictionary(FeeShareClassInstrumentID).ToString(DISPLAYMEMBER_INTEGERFORMAT) & ")"
                  End If

                Case FeesPercentageType.ValueBeforeFees  ' % Before Fees

                  DetalsString &= "value before fees"
                  thisDisplayRow("TransactionAmount") = CDbl(thisFeeRow("TransactionAmount")) * NAVBeforeFees
                  DetalsString &= " (" & NAVBeforeFees.ToString(DISPLAYMEMBER_INTEGERFORMAT) & ")"

                Case Else ' % of Last NAV

                  DetalsString &= "last NAV"
                  If (FeeShareClassInstrumentID = 0) OrElse (Not PreviousNAVDictionary.ContainsKey(FeeShareClassInstrumentID)) Then
                    thisDisplayRow("TransactionAmount") = CDbl(thisFeeRow("TransactionAmount")) * PreviousNAVDictionary(0)
                    DetalsString &= " (" & PreviousNAVDictionary(0).ToString(DISPLAYMEMBER_INTEGERFORMAT) & ")"
                  Else
                    thisDisplayRow("TransactionAmount") = CDbl(thisFeeRow("TransactionAmount")) * PreviousNAVDictionary(FeeShareClassInstrumentID)
                    DetalsString &= " (" & PreviousNAVDictionary(FeeShareClassInstrumentID).ToString(DISPLAYMEMBER_INTEGERFORMAT) & ")"
                  End If

              End Select

              If (FeeRow IsNot Nothing) Then
                DetalsString &= (" " & CType(FeeRow.FeeCalculationPeriod, DealingPeriod).ToString) & ", paid " & CType(FeeRow.FeePaymentPeriod, DealingPeriod).ToString
              End If

            Case Else ' Amount

              If (FeeRow IsNot Nothing) Then
                DetalsString = "Fixed Value of " & FeeRow.FeeRate.ToString(DISPLAYMEMBER_DOUBLEFORMAT) & " per " & RenaissanceUtilities.DatePeriodFunctions.SinglePeriodName(FeeRow.FeeCalculationPeriod) & ", paid " & CType(FeeRow.FeePaymentPeriod, DealingPeriod).ToString
              Else
                DetalsString = "Fixed Amount"
              End If

              thisDisplayRow("TransactionAmount") = thisFeeRow("TransactionAmount")

          End Select

          thisDisplayRow("FeeID") = thisFeeRow("FeeID")
          thisDisplayRow("FeeName") = IIf(FeeRow IsNot Nothing, FeeRow.Description, "")
          thisDisplayRow("TransactionValueDate") = thisFeeRow("TransactionValueDate")
          thisDisplayRow("InstrumentDescription") = Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, CInt(Nz(thisFeeRow("TransactionInstrumentID"), 0)), "InstrumentDescription"), "Unknown")
          thisDisplayRow("TransactionFundID") = thisFeeRow("TransactionFundID")
          thisDisplayRow("TransactionSubFundID") = thisFeeRow("TransactionSubFundID")
          thisDisplayRow("TransactionInstrumentID") = thisFeeRow("TransactionInstrumentID")
          thisDisplayRow("TransactionSpecificShareClassID") = thisFeeRow("TransactionSpecificShareClassID")
          thisDisplayRow("TransactionSettlementDate") = thisFeeRow("TransactionSettlementDate")
          thisDisplayRow("FeeRateType") = thisFeeRow("FeeRateType")
          thisDisplayRow("FeeCalculationMethod") = thisFeeRow("FeeCalculationMethod")
          thisDisplayRow("FeeDescription") = DetalsString
          thisDisplayRow("ExistingTicket") = ""
          thisDisplayRow("ExistingParentID") = 0
          thisDisplayRow("ExistingInstrumentID") = 0
          thisDisplayRow("ExistingValueDate") = #1/1/1900#
          thisDisplayRow("ExistingAmount") = 0.0#
          thisDisplayRow("ToBook") = thisDisplayRow("TransactionAmount")
          thisDisplayRow("NotUsed") = False

          myAutomaticFeesTable.Rows.Add(thisDisplayRow)
        End If
      Next

      ' Add In already-booked Transactions

      For Each thisTransaction In SelectedTransactions

        RowMatched = False

        For Each thisDisplayRow In myAutomaticFeesTable.Select

          If (CInt(thisTransaction("TransactionFeeID")) = CInt(thisDisplayRow("FeeID"))) And (CDate(thisTransaction("TransactionValueDate")) = CDate(thisDisplayRow("TransactionValueDate"))) Then
            ' Update Row.
            RowMatched = True

            thisDisplayRow("ExistingTicket") = thisTransaction("TransactionTicket")
            thisDisplayRow("ExistingParentID") = thisTransaction("TransactionParentID")
            thisDisplayRow("ExistingInstrumentID") = thisTransaction("TransactionInstrument")
            thisDisplayRow("ExistingValueDate") = thisTransaction("TransactionValueDate")
            thisDisplayRow("ExistingAmount") = thisTransaction("TransactionUnits")

            If CBool(thisDisplayRow("NotUsed")) Then
              ' Clear Fee Record details as they do not exist any more.

              thisDisplayRow("FeeName") = ""
              thisDisplayRow("InstrumentDescription") = Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, CInt(Nz(thisTransaction("TransactionInstrument"), 0)), "InstrumentDescription"), "Unknown")
              thisDisplayRow("TransactionFundID") = 0
              thisDisplayRow("TransactionSubFundID") = 0
              thisDisplayRow("TransactionInstrumentID") = 0
              thisDisplayRow("TransactionSpecificShareClassID") = 0
              thisDisplayRow("TransactionSettlementDate") = #1/1/1900#
              thisDisplayRow("TransactionAmount") = 0.0#
              thisDisplayRow("FeeRateType") = 0
              thisDisplayRow("FeeCalculationMethod") = 0
              thisDisplayRow("FeeDescription") = "Unmatched Transaction"
              thisDisplayRow("ToBook") = -thisTransaction("TransactionUnits")
            Else
              thisDisplayRow("ToBook") = CDbl(thisDisplayRow("TransactionAmount")) - CDbl(thisTransaction("TransactionUnits"))
            End If
            thisDisplayRow("NotUsed") = False

            Exit For
          End If
        Next thisDisplayRow

        If (RowMatched = False) AndAlso (CDbl(thisTransaction("TransactionUnits")) <> 0.0#) Then

          ' Add Row

          thisDisplayRow = myAutomaticFeesTable.NewRow

          thisDisplayRow("FeeID") = thisTransaction("TransactionFeeID")
          thisDisplayRow("TransactionValueDate") = #1/1/1900#
          thisDisplayRow("InstrumentDescription") = Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, CInt(Nz(thisTransaction("TransactionInstrument"), 0)), "InstrumentDescription"), "Unknown")
          thisDisplayRow("TransactionFundID") = 0
          thisDisplayRow("TransactionSubFundID") = 0
          thisDisplayRow("TransactionInstrumentID") = 0
          thisDisplayRow("TransactionSpecificShareClassID") = 0
          thisDisplayRow("TransactionSettlementDate") = #1/1/1900#
          thisDisplayRow("TransactionAmount") = 0.0#
          thisDisplayRow("FeeRateType") = 0
          thisDisplayRow("FeeCalculationMethod") = 0
          thisDisplayRow("FeeName") = ""
          thisDisplayRow("FeeDescription") = "Unmatched Transaction"
          thisDisplayRow("ExistingTicket") = thisTransaction("TransactionTicket")
          thisDisplayRow("ExistingParentID") = thisTransaction("TransactionParentID")
          thisDisplayRow("ExistingInstrumentID") = thisTransaction("TransactionInstrument")
          thisDisplayRow("ExistingValueDate") = thisTransaction("TransactionValueDate")
          thisDisplayRow("ExistingAmount") = thisTransaction("TransactionUnits")
          thisDisplayRow("ToBook") = -thisTransaction("TransactionUnits")
          thisDisplayRow("NotUsed") = False

          myAutomaticFeesTable.Rows.Add(thisDisplayRow)

        End If
      Next thisTransaction

      ' Remove unused rows.

      For Each thisDisplayRow In myAutomaticFeesTable.Select
        If CBool(thisDisplayRow("NotUsed")) Then
          myAutomaticFeesTable.Rows.Remove(thisDisplayRow)
        End If
      Next

      myAutomaticFeesTable.AcceptChanges()

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in SetSortedRows_AutomatedFees()", ex.StackTrace, True)
    End Try

  End Sub


  ''' <summary>
  ''' Sets the sorted rows_ subscriptions.
  ''' </summary>
  ''' <param name="pForceRefresh">if set to <c>true</c> [p force refresh].</param>
  Private Sub SetSortedRows_Subscriptions(ByVal pForceRefresh As Boolean)
    ' *******************************************************************************
    '
    '
    ' *******************************************************************************

    Dim TransactionsDataset As DSTransaction
    Dim TransactionsTable As DSTransaction.tblTransactionDataTable
    Dim SelectedRows() As DSTransaction.tblTransactionRow
    Dim thisTransactionRow As DSTransaction.tblTransactionRow
    Dim newTransactionRow As DataRow
    Dim SelectedTransactions As New ArrayList
    Dim FundID As Integer
    Dim FeeDate As Date = Date_ValueDate.Value

    Try
      For Each thisRow As C1.Win.C1FlexGrid.Row In Grid_Subscriptions.Rows.Selected
        SelectedTransactions.Add(CInt(thisRow("TransactionParentID")))
      Next

      mySubscriptionsTable.Clear()
      myPreviousSubscriptionsTable.Clear()
    Catch ex As Exception
    End Try

    Try
      Dim FundPeriod As DealingPeriod
      Dim LastFeeDate As Date = Date_FeesSince.Value
      Dim PreviouaFeeDate As Date = Date_FeesSince.Value
      Dim RowCount As Integer
      Dim thisDate As Date

      If (IsNumeric(Combo_Fund.SelectedValue)) Then
        FundID = CInt(Combo_Fund.SelectedValue)
      Else
        Exit Sub
      End If

      FundPeriod = LookupTableValue(MainForm, RenaissanceStandardDatasets.tblFund, FundID, "FundPricingPeriod")

      If (FundPeriod = DealingPeriod.Daily) Then
        PreviouaFeeDate = AddPeriodToDate(DealingPeriod.Weekly, FeeDate, -1)
      Else
        PreviouaFeeDate = AddPeriodToDate(FundPeriod, FeeDate, -1)
      End If


      TransactionsDataset = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblTransaction), DSTransaction)
      If (TransactionsDataset Is Nothing) Then Exit Sub
      TransactionsTable = TransactionsDataset.tblTransaction
      'SelectedRows = TransactionsTable.Select("(TransactionFund=" & FundID.ToString("###0") & ") AND ((TransactionType=" & CInt(TransactionTypes.Subscribe).ToString() & ") OR (TransactionType=" & CInt(TransactionTypes.Redeem).ToString() & ")) AND (TransactionValueDate>'" & LastFeeDate.ToString("yyyy-MM-dd") & "') AND (TransactionValueDate<='" & FeeDate.ToString("yyyy-MM-dd") & "')", "TransactionType, TransactionValueDate, TransactionParentID")
      SelectedRows = TransactionsTable.Select("(TransactionFund=" & FundID.ToString("###0") & ") AND ((TransactionType=" & CInt(TransactionTypes.Subscribe).ToString() & ") OR (TransactionType=" & CInt(TransactionTypes.Redeem).ToString() & ")) AND (TransactionValueDate>'" & PreviouaFeeDate.ToString("yyyy-MM-dd") & "') AND (TransactionValueDate<='" & FeeDate.ToString("yyyy-MM-dd") & "')", "TransactionType, TransactionValueDate Desc, TransactionParentID")

      For RowCount = 0 To (SelectedRows.Length - 1)
        thisTransactionRow = SelectedRows(RowCount)
        thisDate = CDate(thisTransactionRow("TransactionValueDate"))

        If (thisDate > LastFeeDate) Then
          newTransactionRow = mySubscriptionsTable.NewRow()
        Else
          newTransactionRow = myPreviousSubscriptionsTable.NewRow()
        End If

        newTransactionRow("TransactionInstrument") = thisTransactionRow("TransactionInstrument")
        newTransactionRow("TransactionType") = thisTransactionRow("TransactionType")
        newTransactionRow("FransactionFund") = thisTransactionRow("TransactionFund")
        newTransactionRow("TransactionParentID") = thisTransactionRow("TransactionParentID")
        newTransactionRow("TradeStatusName") = LookupTableValue(MainForm, RenaissanceStandardDatasets.tblTradeStatus, CInt(thisTransactionRow("TransactionTradeStatusID")), "TradeStatusName")
        newTransactionRow("TransactionTicket") = thisTransactionRow("TransactionTicket")
        newTransactionRow("FundName") = LookupTableValue(MainForm, RenaissanceStandardDatasets.tblFund, CInt(thisTransactionRow("TransactionFund")), "FundName")
        newTransactionRow("InstrumentDescription") = LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, CInt(thisTransactionRow("TransactionInstrument")), "InstrumentDescription")
        newTransactionRow("TransactionTypeName") = LookupTableValue(MainForm, RenaissanceStandardDatasets.tblTransactionType, CInt(thisTransactionRow("TransactionType")), "TransactionType")
        newTransactionRow("TransactionValueDate") = thisTransactionRow("TransactionValueDate")
        newTransactionRow("TransactionSettlementDate") = thisTransactionRow("TransactionSettlementDate")
        newTransactionRow("TransactionUnits") = thisTransactionRow("TransactionUnits")
        newTransactionRow("TransactionPrice") = thisTransactionRow("TransactionPrice")
        newTransactionRow("TransactionSettlement") = thisTransactionRow("TransactionSettlement")
        newTransactionRow("TransactionCosts") = thisTransactionRow("TransactionCosts")
        newTransactionRow("TransactionBrokerFees") = thisTransactionRow("TransactionBrokerFees")

        If (newTransactionRow.Table Is mySubscriptionsTable) Then
          mySubscriptionsTable.Rows.Add(newTransactionRow)
        Else
          myPreviousSubscriptionsTable.Rows.Add(newTransactionRow)
        End If
      Next
      mySubscriptionsTable.AcceptChanges()
      myPreviousSubscriptionsTable.AcceptChanges()

      ' Re-Select, selected rows.

      For Each thisRow As C1.Win.C1FlexGrid.Row In Grid_Subscriptions.Rows
        thisRow.Selected = False
        If (thisRow.Index > 0) Then
          If (SelectedTransactions.Contains(CInt(thisRow("TransactionParentID")))) Then
            thisRow.Selected = True
          End If
        End If
      Next

    Catch ex As Exception
    End Try

    Try
      Grid_PreviousSubscriptions.Sort(SortFlags.Descending, Grid_PreviousSubscriptions.Cols("TransactionValueDate").Index)
    Catch ex As Exception
    End Try


    ' 
    Try
      If (CustomSubscriptionPrice) Then
        Me.edit_SubscriptionPrice.BackColor = CUSTOM_BACKGROUND_CHANGED
      Else
        Me.edit_SubscriptionPrice.BackColor = CUSTOM_BACKGROUND_UNCHANGED
      End If
    Catch ex As Exception
    End Try

    Dim myViewByFundCommand As SqlCommand = Nothing
    Dim AdministratorNAV As Double = 0.0#
    Dim AdministratorUnitsIssued As Double = 0.0#
    Dim AdministratorValue As Double = 0.0#

    Try
      InPaint = True

      ' Get Administrator NAV

      myViewByFundCommand = New SqlCommand

      Try
        Dim AdministratorFunds As New DataTable
        Dim AdminFundCode As String = CStr(Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblFund, FundID, "FundAdministratorCode"), ""))

        If (AdminFundCode.Length > 0) Then

          myViewByFundCommand.Connection = MainForm.GetVeniceConnection
          myViewByFundCommand.CommandType = CommandType.StoredProcedure
          myViewByFundCommand.CommandText = "adp_tblRecViewByFund_SelectKD"

          myViewByFundCommand.Parameters.Add(New SqlParameter("@NeoFundcode", SqlDbType.NVarChar, 50)).Value = AdminFundCode
          myViewByFundCommand.Parameters.Add(New SqlParameter("@ValueDate", SqlDbType.Date)).Value = FeeDate
          myViewByFundCommand.Parameters.Add(New SqlParameter("@MaxCaptureID", SqlDbType.Int)).Value = 0
          myViewByFundCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", SqlDbType.DateTime)).Value = MainForm.Main_Knowledgedate

          AdministratorFunds.Load(myViewByFundCommand.ExecuteReader())

          If (AdministratorFunds.Rows.Count > 0) Then
            If (AdministratorFunds.Columns.Contains("neo_nav_value")) Then
              AdministratorNAV = CDbl(AdministratorFunds.Rows(0)("neo_nav_value"))
            End If
            If (AdministratorFunds.Columns.Contains("neo_numberofshares_value")) Then
              AdministratorUnitsIssued = CDbl(AdministratorFunds.Rows(0)("neo_numberofshares_value"))
            End If
            If (AdministratorFunds.Columns.Contains("neo_assets_value")) Then
              AdministratorValue = CDbl(AdministratorFunds.Rows(0)("neo_assets_value"))
            End If

          End If
          AdministratorFunds.Clear()

        Else

          AdministratorNAV = 0.0#
          AdministratorUnitsIssued = 0.0#
          AdministratorValue = 0.0#

        End If

        Label_AdministratorValue.Text = AdministratorValue.ToString(DISPLAYMEMBER_INTEGERFORMAT)
        Label_AdministratorNAV.Text = AdministratorNAV.ToString(DISPLAYMEMBER_LONGDOUBLEFORMAT)
        Label_AdministratorUnitsIssued.Text = AdministratorUnitsIssued.ToString(DISPLAYMEMBER_LONGDOUBLEFORMAT)

        If (Not CustomSubscriptionPrice) Then edit_SubscriptionPrice.Value = AdministratorNAV

        Label_AdministratorNAV2.Text = AdministratorNAV.ToString(DISPLAYMEMBER_LONGDOUBLEFORMAT)

      Catch ex As Exception
        AdministratorNAV = 0.0#
      Finally
        Try
          myViewByFundCommand.Connection.Close()
        Catch ex As Exception
        End Try
      End Try

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in SetSortedRows_Subscriptions()", ex.StackTrace, True)
    Finally
      InPaint = False
    End Try
  End Sub

  ''' <summary>
  ''' Sets the sorted rows_ availabilities.
  ''' </summary>
  ''' <param name="pForceRefresh">if set to <c>true</c> [p force refresh].</param>
  Private Sub SetSortedRows_Availabilities(ByVal pForceRefresh As Boolean)
    ' *******************************************************************************
    '
    '
    ' *******************************************************************************

    Dim FundID As Integer = 0
    Dim FeeDate As Date = Date_ValueDate.Value
    Dim FeeDateSince As Date = Date_FeesSince.Value
    Dim LastCurrency As String = ""
    Dim ThisGridRow As C1.Win.C1FlexGrid.Row = Nothing
    Dim thisDescription As String

    Dim col_First As Integer = Grid_Retrocessions.Cols("FirstCol").SafeIndex
    Dim col_NetAssetPercent As Integer = Grid_Retrocessions.Cols("col_NetAsset").SafeIndex
    Dim col_AdminBalance As Integer = Grid_Retrocessions.Cols("col_AdminBalance").SafeIndex
    Dim col_VeniceBalance As Integer = Grid_Retrocessions.Cols("col_VeniceBalance").SafeIndex
    Dim col_AdminBalanceLocal As Integer = Grid_Retrocessions.Cols("col_AdminBalanceLocal").SafeIndex
    Dim col_VeniceBalanceLocal As Integer = Grid_Retrocessions.Cols("col_VeniceBalanceLocal").SafeIndex
    Dim col_AdminVariation As Integer = Grid_Retrocessions.Cols("col_AdminVariation").SafeIndex
    Dim col_VeniceVariation As Integer = Grid_Retrocessions.Cols("col_VeniceVariation").SafeIndex
    Dim col_VeniceBooked As Integer = Grid_Retrocessions.Cols("col_VeniceBooked").SafeIndex
    Dim col_Currency As Integer = Grid_Retrocessions.Cols("col_Currency").SafeIndex
    Dim col_RN As Integer = Grid_Retrocessions.Cols("RN").SafeIndex

    Dim Valuations_InstrumentID As Integer = Grid_Valuations.Cols("colInstrument").SafeIndex
    Dim Valuations_FXVenice As Integer = Grid_Valuations.Cols("colFXVenice").SafeIndex
    Dim Valuations_PriceVenice As Integer = Grid_Valuations.Cols("colPriceVenice").SafeIndex
    Dim PriceCache As New Dictionary(Of Integer, Double)
    Dim FXCache As New Dictionary(Of String, Double)
    'Dim thisPrice As Double
    Dim thisCompoundFX As Double

    Dim RetrocessionInstrumentMap As New Dictionary(Of Integer, String)
    Dim RetrocessionTotalsMap As New Dictionary(Of String, Double)
    Dim RetrocessionTotalsMap_Previous As New Dictionary(Of String, Double)
    Dim thisRetrocessionCurrency As String = ""

    Dim MarginDepositInstrumentMap As New Dictionary(Of Integer, String)
    Dim MarginDepositTotalsMap As New Dictionary(Of String, Double)
    Dim MarginDepositTotalsMap_Previous As New Dictionary(Of String, Double)
    Dim thisMarginDepositCurrency As String = ""

    Dim CurrencyDS As DSCurrency = MainForm.Load_Table(RenaissanceStandardDatasets.tblCurrency)
    Dim CurrencyRow As DSCurrency.tblCurrencyRow
    Dim TransactionDS As DSTransaction = MainForm.Load_Table(RenaissanceStandardDatasets.tblTransaction)
    Dim SelectedTransactions() As DSTransaction.tblTransactionRow
    Dim thisTransaction As DSTransaction.tblTransactionRow
    Dim thisInstrumentID As Integer
    Dim thisFundRow As RenaissanceDataClass.DSFund.tblFundRow = Nothing
    Dim thisInstrumentRow As DSInstrument.tblInstrumentRow = Nothing
    Dim thisInstrumentPrice As Double

    Try
      Dim Counter As Integer

      If IsNumeric(Combo_Fund.SelectedValue) Then
        FundID = CInt(Combo_Fund.SelectedValue)
      End If

      If (FundAvailabilities Is Nothing) Then
        FundAvailabilities = GetAvailabilities(lock_tblAvailabilities, FundID, FeeDate)
      End If

      Grid_Retrocessions.Rows.Count = 1

      If (FundID <= 0) Then
        Exit Sub
      End If

      thisFundRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, FundID)

      ' Set Administrator Values

      Grid_Retrocessions.Rows.Add()
      Grid_Retrocessions(Grid_Retrocessions.Rows.Count - 1, col_First) = "Primonial"
      Grid_Retrocessions.Rows(Grid_Retrocessions.Rows.Count - 1).IsNode = True
      Grid_Retrocessions.Rows(Grid_Retrocessions.Rows.Count - 1).Node.Level = 0
      Grid_Retrocessions.Rows(Grid_Retrocessions.Rows.Count - 1).AllowEditing = False

      Dim ThisCurrency As String

      Try
        Dim SelectedAvailabilities() As DSRecAvailabilities.tblRecAvailabilitiesRow
        Dim thisAvailability As DSRecAvailabilities.tblRecAvailabilitiesRow

        lock_tblAvailabilities.EnterReadLock()

        SelectedAvailabilities = FundAvailabilities.Select("True", "Currency")

        If (SelectedAvailabilities IsNot Nothing) AndAlso (SelectedAvailabilities.Length > 0) Then
          For Counter = 0 To (SelectedAvailabilities.Length - 1)
            thisAvailability = SelectedAvailabilities(Counter)
            ThisCurrency = thisAvailability.Currency

            ' Ignore Summary, Total etc...
            If (ThisCurrency.Length > 3) Then
              Continue For
            End If

            If (ThisCurrency <> LastCurrency) Then
              ThisGridRow = Grid_Retrocessions.Rows.Add()

              ThisGridRow(col_First) = ThisCurrency
              ThisGridRow.IsNode = True
              ThisGridRow.Node.Level = 1
              ThisGridRow.AllowEditing = False
            End If

            ThisGridRow = Grid_Retrocessions.Rows.Add()

            ThisGridRow(col_First) = thisAvailability.Description
            ThisGridRow.IsNode = False
            ThisGridRow.AllowEditing = False
            ThisGridRow(col_NetAssetPercent) = thisAvailability.PercentAssets_Value / 100.0#
            ThisGridRow(col_AdminBalance) = thisAvailability.Balance_Value
            ThisGridRow(col_AdminBalanceLocal) = thisAvailability.CurrentBalance_Value
            ThisGridRow(col_AdminVariation) = thisAvailability.BalanceVariation_Value
            ThisGridRow(col_Currency) = ThisCurrency
            ThisGridRow(col_RN) = thisAvailability.RN

            ' Formating

            Dim colList As Integer() = {col_NetAssetPercent, col_AdminBalance, col_AdminBalanceLocal, col_AdminVariation}

            For Each colIndex As Integer In colList
              If (CDbl(ThisGridRow(colIndex)) < 0.0#) Then
                Grid_Retrocessions.SetCellStyle(ThisGridRow.SafeIndex, colIndex, "NumberNegative")
              Else
                Grid_Retrocessions.SetCellStyle(ThisGridRow.SafeIndex, colIndex, "NumberPositive")
              End If
            Next

            LastCurrency = ThisCurrency
          Next
        End If

      Catch ex As Exception

      Finally
        lock_tblAvailabilities.ExitReadLock()
      End Try


      ' Figure out some Venice equivalents

      If (CurrencyDS IsNot Nothing) Then
        For Each CurrencyRow In CurrencyDS.tblCurrency
          If (CurrencyRow.CurrencyCode.Length > 0) AndAlso (CurrencyRow.DefaultRetrocessionInstrument > 0) AndAlso (Not RetrocessionInstrumentMap.ContainsKey(CurrencyRow.DefaultRetrocessionInstrument)) Then
            RetrocessionInstrumentMap.Add(CurrencyRow.DefaultRetrocessionInstrument, CurrencyRow.CurrencyCode)

            If (Not RetrocessionTotalsMap.ContainsKey(CurrencyRow.CurrencyCode)) Then
              RetrocessionTotalsMap.Add(CurrencyRow.CurrencyCode, 0.0#)
              RetrocessionTotalsMap_Previous.Add(CurrencyRow.CurrencyCode, 0.0#)
            End If
          End If

          If (CurrencyRow.CurrencyCode.Length > 0) AndAlso (CurrencyRow.DefaultInitialMarginInstrument > 0) AndAlso (Not MarginDepositInstrumentMap.ContainsKey(CurrencyRow.DefaultInitialMarginInstrument)) Then
            MarginDepositInstrumentMap.Add(CurrencyRow.DefaultInitialMarginInstrument, CurrencyRow.CurrencyCode)

            If (Not MarginDepositTotalsMap.ContainsKey(CurrencyRow.CurrencyCode)) Then
              MarginDepositTotalsMap.Add(CurrencyRow.CurrencyCode, 0.0#)
              MarginDepositTotalsMap_Previous.Add(CurrencyRow.CurrencyCode, 0.0#)
            End If
          End If

        Next
      End If

      '' Get available prices and FXs from Valuations Grid

      'For Counter = 1 To (Grid_Valuations.Rows.Count - 1)
      '  ThisGridRow = Grid_Valuations.Rows(Counter)

      '  If (IsNumeric(ThisGridRow(Valuations_InstrumentID)) AndAlso (CInt(ThisGridRow(Valuations_InstrumentID)) > 0)) Then
      '    thisInstrumentID = CInt(ThisGridRow(Valuations_InstrumentID))
      '    thisInstrumentRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, thisInstrumentID), DSInstrument.tblInstrumentRow)

      '    If (Not PriceCache.ContainsKey(thisInstrumentID)) Then
      '      PriceCache.Add(thisInstrumentID, CDbl(ThisGridRow(Valuations_PriceVenice)))
      '    End If

      '    If (Not FXCache.ContainsKey(thisInstrumentRow.InstrumentCurrency)) Then
      '      FXCache.Add(thisInstrumentRow.InstrumentCurrency, CDbl(ThisGridRow(Valuations_FXVenice)))
      '    End If
      '  End If
      'Next

      ' Pass Through the Transactions table ....

      If (TransactionDS IsNot Nothing) Then
        SelectedTransactions = TransactionDS.tblTransaction.Select("TransactionFund=" & FundID.ToString("###0"), "TransactionInstrument", DataViewRowState.CurrentRows)

        If (SelectedTransactions IsNot Nothing) AndAlso (SelectedTransactions.Length > 0) Then

          For Counter = 0 To (SelectedTransactions.Length - 1)
            thisTransaction = SelectedTransactions(Counter)

            If (thisTransaction.TransactionValueDate > FeeDate) Then
              Continue For
            End If

            thisInstrumentID = thisTransaction.TransactionInstrument

            ' Retrocessions / Deposits

            Try
              thisRetrocessionCurrency = ""
              thisMarginDepositCurrency = ""

              If (thisTransaction.TransactionValueDate <= FeeDate) AndAlso ((RetrocessionInstrumentMap.TryGetValue(thisInstrumentID, thisRetrocessionCurrency)) OrElse (MarginDepositInstrumentMap.TryGetValue(thisInstrumentID, thisMarginDepositCurrency))) Then

                If (thisInstrumentRow Is Nothing) OrElse (thisInstrumentRow.InstrumentID <> thisInstrumentID) Then
                  thisInstrumentRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, thisInstrumentID), DSInstrument.tblInstrumentRow)
                End If

                Try
                  If (Not PriceCache.TryGetValue(thisInstrumentID, thisInstrumentPrice)) Then
                    ' Get Price...
                    thisInstrumentPrice = Nz(GetInstrumentPrice(thisInstrumentID), 0.0#)
                    PriceCache.Add(thisInstrumentID, thisInstrumentPrice)
                  End If

                  ' Note that FXs taken from the Valuation grid are inverse to normal Venice convention. (Normal Venice is USD per unit of other currency)

                  If (Not FXCache.TryGetValue(thisInstrumentRow.InstrumentCurrency, thisCompoundFX)) Then

                    thisCompoundFX = 1.0# / GetFXToFund(thisInstrumentRow.InstrumentCurrencyID, thisFundRow.FundBaseCurrency)
                    FXCache.Add(thisInstrumentRow.InstrumentCurrency, thisCompoundFX)
                  End If

                  ' Sum Retrocessions or Deposits

                  If (CStr(Nz(thisRetrocessionCurrency, "")).Length > 0) Then

                    RetrocessionTotalsMap(thisRetrocessionCurrency) += thisTransaction.TransactionSignedUnits * thisInstrumentPrice * thisInstrumentRow.InstrumentContractSize * thisInstrumentRow.InstrumentMultiplier

                    If (thisTransaction.TransactionValueDate <= FeeDateSince) Then
                      RetrocessionTotalsMap_Previous(thisRetrocessionCurrency) += thisTransaction.TransactionSignedUnits * thisInstrumentPrice * thisInstrumentRow.InstrumentContractSize * thisInstrumentRow.InstrumentMultiplier
                    End If

                  ElseIf (CStr(Nz(thisMarginDepositCurrency, "")).Length > 0) Then

                    MarginDepositTotalsMap(thisMarginDepositCurrency) += thisTransaction.TransactionSignedUnits * thisInstrumentPrice * thisInstrumentRow.InstrumentContractSize * thisInstrumentRow.InstrumentMultiplier

                    If (thisTransaction.TransactionValueDate <= FeeDateSince) Then
                      MarginDepositTotalsMap_Previous(thisMarginDepositCurrency) += thisTransaction.TransactionSignedUnits * thisInstrumentPrice * thisInstrumentRow.InstrumentContractSize * thisInstrumentRow.InstrumentMultiplier
                    End If

                  End If

                Catch ex As Exception
                End Try
              End If

            Catch ex As Exception
            End Try


            ' Others....

          Next Counter ' For Counter = 0 To (SelectedTransactions.Length - 1)

          For Counter = 1 To (Grid_Retrocessions.Rows.Count - 1)
            ThisGridRow = Grid_Retrocessions.Rows(Counter)


            If (ThisGridRow.IsNode = False) Then
              thisDescription = CStr(ThisGridRow(col_First)).ToUpper
              ThisCurrency = ThisGridRow(col_Currency)

              If (Not FXCache.TryGetValue(ThisCurrency, thisCompoundFX)) Then
                thisCompoundFX = 1.0# / GetFXToFund(CInt(Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblCurrency, 0, "CurrencyID", "RN", "CurrencyCode='" & ThisCurrency & "'"), thisFundRow.FundBaseCurrency)), thisFundRow.FundBaseCurrency)
                FXCache.Add(ThisCurrency, thisCompoundFX)
              End If

              If (thisDescription.StartsWith("REC.TRAILER FEES ")) Then
                thisDescription = "REC.TRAILER FEES ALL"
              End If

              Select Case thisDescription.ToUpper

                Case "FEES RETROCESSION", "REC.TRAILER FEES ALL"

                  If (RetrocessionTotalsMap.ContainsKey(ThisCurrency)) Then
                    ThisGridRow(col_VeniceBalance) = RetrocessionTotalsMap(ThisCurrency) / thisCompoundFX
                    ThisGridRow(col_VeniceBalanceLocal) = RetrocessionTotalsMap(ThisCurrency)
                    ThisGridRow(col_VeniceVariation) = (RetrocessionTotalsMap(ThisCurrency) - RetrocessionTotalsMap_Previous(ThisCurrency))

                    RetrocessionTotalsMap.Remove(ThisCurrency)
                  End If

                Case "CASH DEPOSIT", "DEPOTS DE GARANTIE"

                  If (MarginDepositTotalsMap.ContainsKey(ThisCurrency)) Then
                    ThisGridRow(col_VeniceBalance) = MarginDepositTotalsMap(ThisCurrency) / thisCompoundFX
                    ThisGridRow(col_VeniceBalanceLocal) = MarginDepositTotalsMap(ThisCurrency)
                    ThisGridRow(col_VeniceVariation) = (MarginDepositTotalsMap(ThisCurrency) - MarginDepositTotalsMap_Previous(ThisCurrency))
                  End If


              End Select

            End If ' IsNode = False

          Next ' Grid_Retrocessions.Rows.Count

        End If ' If (SelectedTransactions IsNot Nothing) AndAlso (SelectedTransactions.Length > 0) Then

      End If ' TransactionDS IsNot Nothing

      ' Set Venice grid values...

      ' More_Code_Here()

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in SetSortedRows_Availabilities()", ex.StackTrace, True)
    End Try

  End Sub

  ''' <summary>
  ''' Sets the sorted rows_ valuation.
  ''' </summary>
  ''' <param name="pForceRefresh">if set to <c>true</c> [p force refresh].</param>
  Private Sub SetSortedRows_Valuation(ByVal pForceRefresh As Boolean)
    ' *******************************************************************************
    '
    '
    ' *******************************************************************************

    Dim myValuationsAdaptor As SqlDataAdapter = Nothing
    Dim myValuationsConnection As SqlConnection = Nothing

    Try

      Dim SelectString As String
      Dim SortString As String

      If (Me.Created) And (Not Me.IsDisposed) Then

        ' Establish parameters :

        Dim FundID As Integer = 0
        Dim ValueDate As Date = Date_ValueDate.Value
        Dim StatusGroupFilter As String = Combo_TransactionGroup.Text
        Dim AdministratorDatesFilter As Integer = 0

        If (MainForm.GetCheckBoxChecked(Me.Check_SR_IgnoreStatus)) Then
          AdministratorDatesFilter += RenaissanceGlobals.AdministratorDatesFilter.SubscriptionRedemption_IgnoreTransactionStatus
        End If
        If (MainForm.GetCheckBoxChecked(Me.Check_SR_IgnoreTodaysTrades)) Then
          AdministratorDatesFilter += RenaissanceGlobals.AdministratorDatesFilter.SubscriptionRedemption_IgnoreTodaysTrades
        End If

        If IsNumeric(Combo_Fund.SelectedValue) Then
          FundID = CInt(Combo_Fund.SelectedValue)
        End If

        'Grid_Valuations.Rows.Count = 1	' (The Header Row)

        ' Get Valuation Data...

        If (myValuationsTable.Rows.Count = 0) OrElse (pForceRefresh) Then

          myValuationsTable.Clear()
          If (IsNumeric(Combo_Fund.SelectedValue) AndAlso IsDate(Date_ValueDate.Value)) Then

            Call CalculateFundValuation(myValuationsTable, myFundValuationUnitCounts, Combo_Fund.SelectedValue, Date_ValueDate.Value, Nz(Combo_TransactionGroup.SelectedValue, ""), AdministratorDatesFilter, Check_AdministratorPriceDates.Checked)

          End If

        End If

        ' Display Data
        SelectString = "true"
        SortString = "rpt_InstrumentType, SectionSort, rpt_Description"

        Try

          Try

            If (myValuationsDataView IsNot Nothing) AndAlso (Not (myValuationsDataView.RowFilter = SelectString)) Then
              myValuationsDataView.RowFilter = SelectString
            End If

            If (myValuationsDataView IsNot Nothing) AndAlso (myValuationsDataView.Table.Rows.Count > 0) AndAlso (Not (myValuationsDataView.Sort = SortString)) Then
              myValuationsDataView.Sort = SortString
            End If

          Catch ex As Exception
            SelectString = "true"
            myValuationsDataView.RowFilter = SelectString
          End Try

          PaintValuationsGrid()

        Catch ex As Exception
          MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in SetSortedRows_Valuation()", ex.StackTrace, True)
        End Try

      End If ' Created

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in SetSortedRows_Valuation()", ex.StackTrace, True)
    Finally
      If (myValuationsConnection IsNot Nothing) AndAlso (myValuationsConnection.State And ConnectionState.Open) Then
        myValuationsConnection.Close()
      End If
    End Try

  End Sub

  ''' <summary>
  ''' Sets the fees fields.
  ''' </summary>
  Private Sub SetFeesFields()
    ' *******************************************************************************
    ' Function to initialise Fee tab fields, Rate and Dates etc.
    ' To be called, for example, after the selected Fund is changed.
    ' *******************************************************************************

    Dim FundID As Integer
    Dim FundRow As DSFund.tblFundRow
    Dim FundPricingPeriod As DealingPeriod = RenaissanceGlobals.DealingPeriod.Daily
    Dim PricingPeriod As RenaissanceGlobals.DealingPeriod = RenaissanceGlobals.Globals.DEFAULT_DATA_PERIOD
    Dim FeeDate As Date
    Dim InstrumentRow As DSInstrument.tblInstrumentRow = Nothing

    Try
      ' If this Tab has not been shown before, or needs updating, then ...

      If (TabInitialStatus.ContainsKey(Tab_Fees.Name) = False) OrElse (TabInitialStatus(Tab_Fees.Name) = True) Then

        ' Clear Tab to start

        Call ClearFeeTabFields()

        FeeDate = Date_ValueDate.Value

        If IsNumeric(Combo_Fund.SelectedValue) Then
          FundID = CInt(Combo_Fund.SelectedValue)

          ' Get Fund Info.

          FundRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, FundID), DSFund.tblFundRow)

          If (FundRow Is Nothing) Then
            Call ClearFeeTabFields()
            Exit Sub
          End If

          If (FundRow.FundUnit <> 0) Then
            InstrumentRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, FundRow.FundUnit), DSInstrument.tblInstrumentRow)
          Else
            InstrumentRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, 0, "", "InstrumentUnitManagementFees DESC", "InstrumentFundID=" & FundRow.FundID.ToString), DSInstrument.tblInstrumentRow)
          End If

        Else
          Exit Sub
        End If

        If (InstrumentRow IsNot Nothing) Then
          Label_MgmtFeesRate.Text = InstrumentRow.InstrumentUnitManagementFees.ToString(DISPLAYMEMBER_PERCENTAGEFORMAT)
        Else
          Label_MgmtFeesRate.Text = "No Unit"
        End If

        ' Calculate Dates.

        Dim MgmtFeePaymentPeriod As DealingPeriod = CType(FundRow.FundManagementFeesPaymentPeriod, DealingPeriod)
        Dim PerfFeePaymentPeriod As DealingPeriod = CType(FundRow.FundPerformanceFeesPaymentPeriod, DealingPeriod)
        Dim FundYearEnd As Date

        ' Clean Fund Year End 

        If FundRow.IsFundYearEndNull Then
          FundYearEnd = New Date(Now.Year, 12, 31) ' Default to calendar year
        Else
          FundYearEnd = FitDateToPeriod(DealingPeriod.Monthly, New Date(FeeDate.Year, FundRow.FundYearEnd.Month, 28), True)

          If (FundYearEnd < FeeDate) Then
            FundYearEnd = FundYearEnd.AddYears(1)
          End If
        End If

        '

        If (MgmtFeePaymentPeriod = DealingPeriod.Annually) Then
          ' Get Year end

          Date_MgmtFeesSettlementDate.Value = FundYearEnd
        Else
          Date_MgmtFeesSettlementDate.Value = RenaissanceUtilities.DatePeriodFunctions.FitDateToPeriod(MgmtFeePaymentPeriod, FeeDate, True)
        End If

        ' Performance Fees

        If (InstrumentRow IsNot Nothing) Then
          Label_PerfFeesRate.Text = InstrumentRow.InstrumentUnitPerformanceFees.ToString(DISPLAYMEMBER_PERCENTAGEFORMAT)
        Else
          Label_PerfFeesRate.Text = "No Unit"
        End If

        ' Performance Dates

        If (PerfFeePaymentPeriod = DealingPeriod.Annually) Then
          ' Get Year End

          Date_PerfFeesSettlementDate.Value = FundYearEnd
        Else
          Date_PerfFeesSettlementDate.Value = RenaissanceUtilities.DatePeriodFunctions.FitDateToPeriod(PerfFeePaymentPeriod, FeeDate, True)
        End If


        ' Fee Instruments

        Try

          If (FundRow.IsFundManagementFeeInstrumentNull = False) Then
            Combo_ManagementFeeInstrument.SelectedValue = FundRow.FundManagementFeeInstrument
          Else
            MainForm.ClearComboSelection(Combo_ManagementFeeInstrument)
          End If

          If (FundRow.IsFundPerformanceFeeInstrumentNull = False) Then
            Combo_PerformanceFeeInstrument.SelectedValue = FundRow.FundPerformanceFeeInstrument
          Else
            MainForm.ClearComboSelection(Combo_PerformanceFeeInstrument)
          End If
          If (FundRow.IsFundPerformanceFeeCrystalisedInstrumentNull = False) Then
            Combo_PerformanceFeeCrystalisedInstrument.SelectedValue = FundRow.FundPerformanceFeeCrystalisedInstrument
          Else
            MainForm.ClearComboSelection(Combo_PerformanceFeeCrystalisedInstrument)
          End If

        Catch ex As Exception
        End Try

        TabInitialStatus(Tab_Fees.Name) = False
      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in SetFeesFields", ex.StackTrace, True)
    End Try

  End Sub

  ''' <summary>
  ''' Clears the fee tab fields.
  ''' </summary>
  Private Sub ClearFeeTabFields()
    ' *******************************************************************************
    ' Function to clear fields on the Fees Tab.
    ' *******************************************************************************

    Try
      InPaint = True

      'Date_FeesSince.Value = RenaissanceUtilities.DatePeriodFunctions.AddPeriodToDate(RenaissanceGlobals.Globals.DEFAULT_DATA_PERIOD, Date_ValueDate.Value, -1)
      Label_MgmtFeesRate.Text = "0%"
      Label_MgmtFeeNAV.Text = "0"
      edit_ManagementFeesToTake.Value = 0.0#
      Label_MgmtFeesAlreadyTaken.Text = "0"
      Label_Fees_AdditionalManagementTrade.Text = "0"
      Label_Milestone_AdditionalManagementTrade.Text = "0"
      Date_MgmtFeesSettlementDate.Value = RenaissanceUtilities.DatePeriodFunctions.FitDateToPeriod(DealingPeriod.Monthly, Date_ValueDate.Value, True)
      MainForm.ClearComboSelection(Combo_ManagementFeeInstrument)


      Label_PerfFeesRate.Text = "0"
      Label_FundPerformance.Text = "0%"
      Label_BenchmarkPerformance.Text = "0%"
      Numeric_PerfFeesToTake.Value = 0.0#
      Label_PerfFeesAlreadyTaken.Text = "0"
      Label_Fees_AdditionalPerformanceTrade.Text = "0"
      Label_Milestone_AdditionalPerformanceTrade.Text = Label_Fees_AdditionalPerformanceTrade.Text
      Date_PerfFeesSettlementDate.Value = RenaissanceUtilities.DatePeriodFunctions.FitDateToPeriod(DealingPeriod.Annually, Date_ValueDate.Value, True)
      MainForm.ClearComboSelection(Combo_PerformanceFeeInstrument)
      MainForm.ClearComboSelection(Combo_PerformanceFeeCrystalisedInstrument)
      Numeric_PerfFeesCrystalisedToTake.Value = 0.0#
      Label_PerfFeesCrystalisedAlreadyTaken.Text = "0"
      Label_PerfFeesCrystalisedTotalBooked.Text = "0"
      Label_Fees_AdditionalCrystalisedTrade.Text = "0"
      Label_Milestone_AdditionalCrystalisedTrade.Text = Label_Fees_AdditionalCrystalisedTrade.Text

      Label_MgmtFeesAdministratorValue.Text = ""
      Label_PerfFeesAdministrator.Text = ""
      Label_PerfFeesCrystalisedAdministrator.Text = ""

      Grid_Benchmark.Rows.Count = 1

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in ClearFeeTabFields()", ex.StackTrace, True)
    Finally
      InPaint = False
    End Try
  End Sub

  ''' <summary>
  ''' Gets the fees valuation.
  ''' </summary>
  ''' <param name="FeesObjectToUse">The fees object to use.</param>
  ''' <param name="FundID">The fund ID.</param>
  ''' <param name="FeeDate">The fee date.</param>
  ''' <param name="StatusGroupFilter">The status group filter.</param>
  ''' <param name="AdministratorDatesFilter">The administrator dates filter.</param>
  ''' <param name="UseAdministratorPriceDates">if set to <c>true</c> [use administrator price dates].</param>
  ''' <returns>FundFeeCalculator.FundValuationDetails.</returns>
  Private Function GetFeesValuation(ByRef FeesObjectToUse As FundFeeCalculator, ByVal FundID As Integer, ByVal FeeDate As Date, ByVal FeesSinceDate As Date, ByVal StatusGroupFilter As String, ByVal AdministratorDatesFilter As Integer, ByVal UseAdministratorPriceDates As Boolean) As FundFeeCalculator.FundValuationDetails
    ' *******************************************************************************
    '
    '
    ' *******************************************************************************

    Try
      MainForm.SetToolStripText(StatusLabel, "Getting Fund Valuation...")

      ' Instantiate Fees Calculator

      If FeesObjectToUse Is Nothing Then
        FeesObjectToUse = New FundFeeCalculator(MainForm, Me, FundID, FeeDate, StatusGroupFilter, AdministratorDatesFilter)
      Else
        FeesObjectToUse.FundID = FundID
        FeesObjectToUse.FeeDate = FeeDate
        FeesObjectToUse.StatusGroupFilter = StatusGroupFilter
        FeesObjectToUse.AdministratorDatesFilter = AdministratorDatesFilter
      End If

      FeesObjectToUse.FeeStartDate = Date_FeesSince.Value

      If Check_SR_IgnoreTodaysTrades.Checked Then
        FeesObjectToUse.ExcludeSubscriptionsSinceDate = FeesSinceDate
      Else
        FeesObjectToUse.ExcludeSubscriptionsSinceDate = FeeDate
      End If

      FeesObjectToUse.ValuationManagementFeeStartDate = FeesSinceDate
      FeesObjectToUse.ValuationPerformanceFeeStartDate = FeesSinceDate
      FeesObjectToUse.ValuationCrystalisedPerformanceFeeStartDate = FeesSinceDate

      FeesObjectToUse.UseAdministratorPriceDates = UseAdministratorPriceDates

      ' Get Fund Valuation

      Return FeesObjectToUse.GetFundValuation()

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in GetFeesValuation()", ex.StackTrace, True)
    Finally
      MainForm.SetToolStripText(StatusLabel, "")
    End Try

    Return Nothing

  End Function

  ''' <summary>
  ''' Sets the sorted rows_ fees.
  ''' </summary>
  ''' <param name="pForceRefresh">if set to <c>true</c> [p force refresh].</param>
  Private Sub SetSortedRows_Fees(ByVal pForceRefresh As Boolean)
    ' *******************************************************************************
    ' Function to populate the Fees tab. Calculate fees etc....
    '
    ' *******************************************************************************

    Dim FundID As Integer
    Dim FundRow As DSFund.tblFundRow
    Dim FundPricingPeriod As DealingPeriod = RenaissanceGlobals.DealingPeriod.Daily
    Dim PricingPeriod As RenaissanceGlobals.DealingPeriod = RenaissanceGlobals.Globals.DEFAULT_DATA_PERIOD
    Dim FeeDate As Date
    Dim StatusGroupFilter As String
    Dim AdministratorDatesFilter As RenaissanceGlobals.AdministratorDatesFilter = RenaissanceGlobals.AdministratorDatesFilter.None
    Dim RowCounter As Integer
    Dim BenchmarkItemRow As DSBenchmarkItems.tblBenchmarkItemsRow

    Try
      InPaint = True

      FeeDate = Date_ValueDate.Value
      StatusGroupFilter = Nz(MainForm.GetComboSelectedValue(Combo_TransactionGroup), "")

      ' AdministratorDatesFilter (bitmap)
      '
      '--	1		- Ignore the transaction status for Subscriptions and Redemptions
      '--	2		- Ignore the Subscriptions and Redemptions on (or after of course) the given Value Date

      If (MainForm.GetCheckBoxChecked(Me.Check_SR_IgnoreStatus)) Then
        AdministratorDatesFilter += RenaissanceGlobals.AdministratorDatesFilter.SubscriptionRedemption_IgnoreTransactionStatus
      End If
      If (MainForm.GetCheckBoxChecked(Me.Check_SR_IgnoreTodaysTrades)) Then
        AdministratorDatesFilter += RenaissanceGlobals.AdministratorDatesFilter.SubscriptionRedemption_IgnoreTodaysTrades
      End If

      If IsNumeric(Combo_Fund.SelectedValue) Then
        FundID = CInt(Combo_Fund.SelectedValue)

        FundRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, FundID), DSFund.tblFundRow)

        If (FundRow Is Nothing) Then
          Call ClearFeeTabFields()
          Exit Sub
        End If
      Else
        Call ClearFeeTabFields()
        Exit Sub
      End If

      ' Get Fund Valuation

      Dim FundValuation As FundFeeCalculator.FundValuationDetails = Nothing
      Dim FundFees As FundFeeCalculator.FundFeeDetails = Nothing

      Try

        If (FundAvailabilities Is Nothing) Then
          FundAvailabilities = GetAvailabilities(lock_tblAvailabilities, FundID, FeeDate)
        End If

        MainForm.SetToolStripText(StatusLabel, "Calculating Fund Valuation, Fund " & FundID.ToString & ", " & FeeDate.ToString("dd MMM yyyy"))

        FundValuation = GetFeesValuation(FeesObject, FundID, FeeDate, Date_FeesSince.Value, StatusGroupFilter, AdministratorDatesFilter, Check_AdministratorPriceDates.Checked)

        MainForm.SetToolStripText(StatusLabel, "Calculating Fund Fees, Fund " & FundID.ToString & ", " & FeeDate.ToString("dd MMM yyyy"))

        ' Get Fund Fees

        FundFees = FeesObject.GetFundFees()

      Catch ex As Exception
        MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error getting Valuation or Fee Calculations.", ex.StackTrace, True)
      Finally
        MainForm.SetToolStripText(StatusLabel, "")
      End Try

      If (Me.Disposing) OrElse (Me.IsDisposed) Then
        Exit Sub
      End If

      Label_MgmtFeeNAV.Text = FundValuation.FundBNAV.ToString(DISPLAYMEMBER_INTEGERFORMAT)
      Label_MgmtFeesAlreadyTaken.Text = CDbl(-FundValuation.PendingManagementFees).ToString(DISPLAYMEMBER_DOUBLEFORMAT)

      If (CustomManagementFees) Then
        Me.edit_ManagementFeesToTake.BackColor = CUSTOM_BACKGROUND_CHANGED
      Else
        edit_ManagementFeesToTake.Value = Math.Round(FundFees.ManagementFees, 2)
        Me.edit_ManagementFeesToTake.BackColor = CUSTOM_BACKGROUND_UNCHANGED
      End If

      Label_Fees_AdditionalManagementTrade.Text = (edit_ManagementFeesToTake.Value + FundValuation.PendingManagementFees).ToString(DISPLAYMEMBER_DOUBLEFORMAT)
      Label_Milestone_AdditionalManagementTrade.Text = Label_Fees_AdditionalManagementTrade.Text

      ' Perf Fee

      Label_PerfFeesRate.Text = FundFees.PerformanceFeesPercent.ToString(DISPLAYMEMBER_PERCENTAGEFORMAT)

      Label_BenchmarkPerformance.Text = FundFees.BestBenchmarkReturn.ToString(DISPLAYMEMBER_PERCENTAGEFORMATLong)

      If (CustomPerformanceFees) Then
        Me.Numeric_PerfFeesToTake.BackColor = CUSTOM_BACKGROUND_CHANGED
      Else
        Numeric_PerfFeesToTake.Value = Math.Round(FundFees.PerformanceFees, 2)
        Me.Numeric_PerfFeesToTake.BackColor = CUSTOM_BACKGROUND_UNCHANGED
      End If
      Label_PerfFeesAlreadyTaken.Text = CDbl(-(FundValuation.PreviousPerformanceFees + FundValuation.PendingPerformanceFees)).ToString(DISPLAYMEMBER_DOUBLEFORMAT)
      Label_Fees_AdditionalPerformanceTrade.Text = CDbl(Numeric_PerfFeesToTake.Value + (FundValuation.PreviousPerformanceFees + FundValuation.PendingPerformanceFees)).ToString(DISPLAYMEMBER_DOUBLEFORMAT)
      Label_Milestone_AdditionalPerformanceTrade.Text = Label_Fees_AdditionalPerformanceTrade.Text

      If (CustomCrystalisedFees) Then
        Me.Numeric_PerfFeesCrystalisedToTake.BackColor = CUSTOM_BACKGROUND_CHANGED
      Else
        Numeric_PerfFeesCrystalisedToTake.Value = Math.Round(FundFees.PerformanceFeesCrystalised, 2)
        Me.Numeric_PerfFeesCrystalisedToTake.BackColor = CUSTOM_BACKGROUND_UNCHANGED
      End If

      Label_PerfFeesCrystalisedAlreadyTaken.Text = CDbl(-FundValuation.PendingPerformanceFeesCrystalised).ToString(DISPLAYMEMBER_DOUBLEFORMAT)

      Label_Fees_AdditionalCrystalisedTrade.Text = (Numeric_PerfFeesCrystalisedToTake.Value + FundValuation.PendingPerformanceFeesCrystalised).ToString(DISPLAYMEMBER_DOUBLEFORMAT)
      Label_Milestone_AdditionalCrystalisedTrade.Text = Label_Fees_AdditionalCrystalisedTrade.Text

      Label_PerfFeesCrystalisedTotalBooked.Text = CDbl(-(FundValuation.PreviousPerformanceFeesCrystalised + FundValuation.PendingPerformanceFeesCrystalised)).ToString(DISPLAYMEMBER_DOUBLEFORMAT)

      ' Perf Info.

      Label_Fees_FinancialYear.Text = FundFees.FundYearStart.ToString("MMM yyyy") & " - " & FundFees.FundYearEnd.ToString("MMM yyyy")

      Label_Fees_YearStartNAV.Text = FundFees.FundStartValue.ToString(DISPLAYMEMBER_INTEGERFORMAT)

      Label_Fees_YearSubscriptions.Text = Math.Abs(FundValuation.FundValueSubscriptionsThisYear).ToString(DISPLAYMEMBER_INTEGERFORMAT)

      Label_Fees_YearRedemptions.Text = Math.Abs(FundValuation.FundValueRedemptionsThisYear).ToString(DISPLAYMEMBER_INTEGERFORMAT)

      Label_Fees_BestBenchmark.Text = FundFees.BestBenchmarkDescription

      Label_Fees_FundNAV.Text = FundValuation.FundNAV.ToString(DISPLAYMEMBER_INTEGERFORMAT)

      Label_Fees_PerfFeesThisYear.Text = CDbl(Math.Abs(FundValuation.AllPerformanceFees)).ToString(DISPLAYMEMBER_INTEGERFORMAT)

      Label_Fees_CrystalisedToday.Text = FundFees.PerformanceFeesCrystalised.ToString(DISPLAYMEMBER_INTEGERFORMAT)

      Label_Fees_GNAV.Text = FundFees.FundPerformanceGnavUsed.ToString(DISPLAYMEMBER_INTEGERFORMAT)

      Label_Fees_BenchmarkNAV.Text = FundFees.BenchmarkNAV.ToString(DISPLAYMEMBER_INTEGERFORMAT)

      Label_Fees_TodaysUnitRedemptions.Text = FundFees.TodaysRedemptions.ToString(DISPLAYMEMBER_INTEGERFORMAT)

      Label_Fees_PreviousUnitsIssued.Text = Math.Abs(FundValuation.FundUnits).ToString(DISPLAYMEMBER_INTEGERFORMAT)

      Label_Fees_PreviousPerformanceFees.Text = Math.Abs(FundValuation.PreviousPerformanceFees).ToString(DISPLAYMEMBER_INTEGERFORMAT)

      Try
        If Not (lock_tblAvailabilities.IsReadLockHeld) Then
          lock_tblAvailabilities.EnterReadLock()
        End If

        If (FundAvailabilities IsNot Nothing) AndAlso (FundAvailabilities.Rows.Count > 0) Then
          Dim ThisAvailabilityRow As DataRow


          ThisAvailabilityRow = LookupTableRow(FundAvailabilities, "Description", "FIXED MANAGEMENT FEES")

          If (ThisAvailabilityRow IsNot Nothing) AndAlso ThisAvailabilityRow.Table.Columns.Contains("BalanceVariation_Value") Then
            Label_MgmtFeesAdministratorValue.Text = CDbl(Nz(ThisAvailabilityRow("BalanceVariation_Value"), 0)).ToString(DISPLAYMEMBER_DOUBLEFORMAT)
          Else
            Label_MgmtFeesAdministratorValue.Text = "<not found>"
          End If

          ThisAvailabilityRow = LookupTableRow(FundAvailabilities, "Description", "PERFORMANCE FEES")

          If (ThisAvailabilityRow IsNot Nothing) AndAlso ThisAvailabilityRow.Table.Columns.Contains("CurrentBalance_Value") Then
            Label_PerfFeesAdministrator.Text = Math.Abs(CDbl(Nz(ThisAvailabilityRow("CurrentBalance_Value"), 0))).ToString(DISPLAYMEMBER_DOUBLEFORMAT)
          Else
            Label_PerfFeesAdministrator.Text = "<not found>"
          End If

          ThisAvailabilityRow = LookupTableRow(FundAvailabilities, "Description", "PERF FEES ACQUIRED")

          If (ThisAvailabilityRow IsNot Nothing) AndAlso ThisAvailabilityRow.Table.Columns.Contains("BalanceVariation_Value") Then
            Label_PerfFeesCrystalisedAdministrator.Text = Math.Abs(CDbl(Nz(ThisAvailabilityRow("BalanceVariation_Value"), 0))).ToString(DISPLAYMEMBER_DOUBLEFORMAT)
          Else
            Label_PerfFeesCrystalisedAdministrator.Text = "<not found>"
          End If

        Else
          Label_MgmtFeesAdministratorValue.Text = "<not found>"
          Label_PerfFeesAdministrator.Text = "<not found>"
          Label_PerfFeesCrystalisedAdministrator.Text = "<not found>"

        End If
      Catch ex As Exception
      Finally
        If (lock_tblAvailabilities.IsReadLockHeld) Then
          lock_tblAvailabilities.ExitReadLock()
        End If
      End Try


      ' Benchmark Grid

      If (FundFees.DatesArray IsNot Nothing) AndAlso (FundFees.DatesArray.Length > 0) Then
        Dim thisGridRow As C1.Win.C1FlexGrid.Row

        If (Grid_Benchmark.IsDisposed) OrElse (Grid_Benchmark.Disposing) Then
          Exit Sub
        End If

        Grid_Benchmark.Rows.Count = 1

        Benchmark_Label_BenchmarkName.Text = Label_Fees_BestBenchmark.Text

        For RowCounter = 0 To (FundFees.DatesArray.Length - 1)
          thisGridRow = Grid_Benchmark.Rows.Add()

          thisGridRow("col_Date") = FundFees.DatesArray(RowCounter)
          If (FundFees.BenchmarkNAVArray.Length > RowCounter) Then thisGridRow("col_BenchmarkNAV") = FundFees.BenchmarkNAVArray(RowCounter)
          If (FundFees.UnitsSubscribed.Length > RowCounter) Then thisGridRow("col_Subscriptions") = FundFees.UnitsSubscribed(RowCounter)
          If (FundFees.UnitsRedeemed.Length > RowCounter) Then thisGridRow("col_Redemptions") = FundFees.UnitsRedeemed(RowCounter)
          If (FundFees.UnitsRunningTotal.Length > RowCounter) Then thisGridRow("col_Units") = FundFees.UnitsRunningTotal(RowCounter)
          If (FundFees.BenchmarkValueArray.Length > RowCounter) Then thisGridRow("col_BenchmarkValue") = FundFees.BenchmarkValueArray(RowCounter)

        Next

        If (Grid_Benchmark.Cols.Count > (Grid_Benchmark.Cols("col_BenchmarkValue").SafeIndex + 1)) Then
          Grid_Benchmark.Cols.Count = (Grid_Benchmark.Cols("col_BenchmarkValue").SafeIndex + 1)
        End If

        If (FundFees.BestBenchmarkItemID > 0) Then
          BenchmarkItemRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblBenchmarkItems, FundFees.BestBenchmarkItemID), DSBenchmarkItems.tblBenchmarkItemsRow)

          Select Case CType(BenchmarkItemRow.BenchmarkItemType, BenchmarkItemType)

            Case BenchmarkItemType.FixedReturn

            Case BenchmarkItemType.CTA_Simulation

            Case BenchmarkItemType.PertracInstrument

            Case BenchmarkItemType.VeniceInstrument

            Case BenchmarkItemType.Group_Mean, BenchmarkItemType.Group_Median, BenchmarkItemType.Group_Weighted

              Dim GroupRow As DSGroupList.tblGroupListRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblGroupList, CInt(RenaissancePertracDataClass.PertracDataClass.GetInstrumentFromID(BenchmarkItemRow.BenchmarkInstrumentID))), DSGroupList.tblGroupListRow)
              Dim GroupItemsDS As DSGroupItems = MainForm.Load_Table(RenaissanceStandardDatasets.tblGroupItems)
              Dim SelectedItems() As DSGroupItems.tblGroupItemsRow
              Dim GroupItem As DSGroupItems.tblGroupItemsRow
              Dim GroupItemData As DSGroupItemData.tblGroupItemDataRow
              Dim ItemCounter As Integer
              Dim Date_Series() As Date
              Dim NAV_Series() As Double
              Dim DateIndex As Integer
              Dim ColIndex As Integer
              Dim thisCol As C1.Win.C1FlexGrid.Column

              If (GroupRow IsNot Nothing) AndAlso (GroupItemsDS IsNot Nothing) Then

                SelectedItems = GroupItemsDS.tblGroupItems.Select("GroupID=" & GroupRow.GroupListID, "GroupItemID")

                If (SelectedItems IsNot Nothing) AndAlso (SelectedItems.Length > 0) Then

                  While (Grid_Benchmark.Cols.Count < (Grid_Benchmark.Cols("col_BenchmarkValue").SafeIndex + 1 + SelectedItems.Length))
                    thisCol = Grid_Benchmark.Cols.Insert(Grid_Benchmark.Cols.Count)
                    thisCol.Style = Grid_Benchmark.Styles("NumericDouble2")
                    thisCol.Width = 125
                  End While

                  For ItemCounter = 0 To (SelectedItems.Length - 1)
                    ColIndex = Grid_Benchmark.Cols("col_BenchmarkValue").SafeIndex + ItemCounter + 1
                    Grid_Benchmark.Cols(ColIndex).Style.Format = "###0.00"
                    GroupItem = SelectedItems(ItemCounter)
                    GroupItemData = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblGroupItemData, GroupItem.GroupItemID)

                    If (GroupItemData Is Nothing) Then
                      Grid_Benchmark.SetData(0, ColIndex, MainForm.PertracData.GetInformationValue(GroupItem.GroupPertracCode, PertracInformationFields.Mastername))
                    Else
                      Grid_Benchmark.SetData(0, ColIndex, MainForm.PertracData.GetInformationValue(GroupItem.GroupPertracCode, PertracInformationFields.Mastername) & " (" & CDbl(GroupItemData.GroupPercent * 100.0#).ToString("###0.00") & "%)")
                    End If

                    Date_Series = MainForm.StatFunctions.DateSeries(FundRow.FundPricingPeriod, GroupItem.GroupPertracCode, False, 0, 1.0#, False, FundFees.FundYearStart, FeeDate)
                    NAV_Series = MainForm.StatFunctions.NAVSeries(FundRow.FundPricingPeriod, GroupItem.GroupPertracCode, False, 0, 1.0#, False, FundFees.FundYearStart, FeeDate, 1.0#, False, 100.0#)

                    If (Date_Series IsNot Nothing) AndAlso (Date_Series.Length > 0) AndAlso (NAV_Series IsNot Nothing) Then
                      For RowCounter = 0 To (FundFees.DatesArray.Length - 1)
                        DateIndex = GetPriceIndex(FundRow.FundPricingPeriod, Date_Series(0), FundFees.DatesArray(RowCounter))

                        If (DateIndex >= 0) AndAlso (DateIndex < NAV_Series.Length) AndAlso ((DateIndex + 1) < Grid_Benchmark.Rows.Count) Then
                          Grid_Benchmark.SetData(DateIndex + 1, ColIndex, NAV_Series(DateIndex))
                        End If
                      Next
                    End If
                  Next

                End If

              End If

            Case Else

          End Select

        End If



      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in SetSortedRows_Fees().", ex.StackTrace, True)
    Finally
      InPaint = False
    End Try
  End Sub

  ''' <summary>
  ''' Sets the sorted rows_ milestone.
  ''' </summary>
  ''' <param name="pForceRefresh">if set to <c>true</c> [p force refresh].</param>
  Private Sub SetSortedRows_Milestone(ByVal pForceRefresh As Boolean)
    ' *******************************************************************************
    ' Function to populate the Milestones tab. Calculate valuations etc....
    '
    ' *******************************************************************************

    Dim FundID As Integer = 0
    Dim FeeDate As Date
    Dim FundValuation As FundFeeCalculator.FundValuationDetails = Nothing
    Dim FundFees As FundFeeCalculator.FundFeeDetails = Nothing
    Dim TradingValuation As FundFeeCalculator.FundValuationDetails = Nothing
    Dim StatusGroupFilter As String
    Dim AdministratorDatesFilter As RenaissanceGlobals.AdministratorDatesFilter = RenaissanceGlobals.AdministratorDatesFilter.None

    Dim myViewByFundCommand As SqlCommand = Nothing
    Dim AdministratorNAV As Double = 0.0#
    Dim AdministratorUnitsIssued As Double = 0.0#
    Dim AdministratorValue As Double = 0.0#

    Try
      button_SetMilestone.Enabled = HasMilestoneInsertPermission
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in SetSortedRows_Milestone().", ex.StackTrace, True)
    End Try

    Try
      InPaint = True

      If IsNumeric(Combo_Fund.SelectedValue) Then
        FundID = CInt(Combo_Fund.SelectedValue)
      End If

      FeeDate = Date_ValueDate.Value

      ' Get Fund Fees
      StatusGroupFilter = Nz(MainForm.GetComboSelectedValue(Combo_TransactionGroup), "")

      ' AdministratorDatesFilter (bitmap)
      '
      '--	1		- Ignore the transaction status for Subscriptions and Redemptions
      '--	2		- Ignore the Subscriptions and Redemptions on (or after of course) the given Value Date

      If (MainForm.GetCheckBoxChecked(Me.Check_SR_IgnoreStatus)) Then
        AdministratorDatesFilter += RenaissanceGlobals.AdministratorDatesFilter.SubscriptionRedemption_IgnoreTransactionStatus
      End If
      If (MainForm.GetCheckBoxChecked(Me.Check_SR_IgnoreTodaysTrades)) Then
        AdministratorDatesFilter += RenaissanceGlobals.AdministratorDatesFilter.SubscriptionRedemption_IgnoreTodaysTrades
      End If

      '

      Try
        MainForm.SetToolStripText(StatusLabel, "Calculating Fund Administrator Valuation.")

        FundValuation = GetFeesValuation(FeesObject, FundID, FeeDate, Date_FeesSince.Value, StatusGroupFilter, AdministratorDatesFilter, Check_AdministratorPriceDates.Checked)

        MainForm.SetToolStripText(StatusLabel, "Calculating Fund Fees.")

        ' Get Fund Fees

        FundFees = FeesObject.GetFundFees()

        ' Trading Valuation.

        MainForm.SetToolStripText(StatusLabel, "Calculating Fund Trading Valuation.")

        TradingValuation = GetFeesValuation(TradingValuationObject, FundID, FeeDate, Date_FeesSince.Value, "", 0, False)

      Catch ex As Exception
        MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error getting Valuation or Fee Calculations.", ex.StackTrace, True)
      Finally
        MainForm.SetToolStripText(StatusLabel, "")
      End Try

      ' Get Administrator NAV

      myViewByFundCommand = New SqlCommand

      Try
        Dim AdministratorFunds As New DataTable
        Dim AdminFundCode As String = CStr(Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblFund, FundID, "FundAdministratorCode"), ""))

        If (AdminFundCode.Length > 0) Then

          myViewByFundCommand.Connection = MainForm.GetVeniceConnection
          myViewByFundCommand.CommandType = CommandType.StoredProcedure
          myViewByFundCommand.CommandText = "adp_tblRecViewByFund_SelectKD"

          myViewByFundCommand.Parameters.Add(New SqlParameter("@NeoFundcode", SqlDbType.NVarChar, 50)).Value = AdminFundCode
          myViewByFundCommand.Parameters.Add(New SqlParameter("@ValueDate", SqlDbType.Date)).Value = FeeDate
          myViewByFundCommand.Parameters.Add(New SqlParameter("@MaxCaptureID", SqlDbType.Int)).Value = 0
          myViewByFundCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", SqlDbType.DateTime)).Value = MainForm.Main_Knowledgedate

          AdministratorFunds.Load(myViewByFundCommand.ExecuteReader())

          If (AdministratorFunds.Rows.Count > 0) Then
            If (AdministratorFunds.Columns.Contains("neo_nav_value")) Then
              AdministratorNAV = CDbl(AdministratorFunds.Rows(0)("neo_nav_value"))
            End If
            If (AdministratorFunds.Columns.Contains("neo_numberofshares_value")) Then
              AdministratorUnitsIssued = CDbl(AdministratorFunds.Rows(0)("neo_numberofshares_value"))
            End If
            If (AdministratorFunds.Columns.Contains("neo_assets_value")) Then
              AdministratorValue = CDbl(AdministratorFunds.Rows(0)("neo_assets_value"))
            End If

          End If
          AdministratorFunds.Clear()

        Else

          AdministratorNAV = 0.0#
          AdministratorUnitsIssued = 0.0#
          AdministratorValue = 0.0#

        End If

        Label_AdministratorValue.Text = AdministratorValue.ToString(DISPLAYMEMBER_INTEGERFORMAT)
        Label_AdministratorNAV.Text = AdministratorNAV.ToString(DISPLAYMEMBER_LONGDOUBLEFORMAT)
        Label_AdministratorUnitsIssued.Text = AdministratorUnitsIssued.ToString(DISPLAYMEMBER_LONGDOUBLEFORMAT)
        If (Not CustomSubscriptionPrice) Then edit_SubscriptionPrice.Value = AdministratorNAV
        Label_AdministratorNAV2.Text = AdministratorNAV.ToString(DISPLAYMEMBER_LONGDOUBLEFORMAT)
        edit_FixedAdministratorPrice.Value = AdministratorNAV

      Catch ex As Exception
        AdministratorNAV = 0.0#
      Finally
        Try
          myViewByFundCommand.Connection.Close()
        Catch ex As Exception
        End Try
      End Try



      ' Populate Form Fields.

      If (Custom_MS_Valuation_NAV = False) Then
        edit_Milestone_NAV_Administrator.Value = FundValuation.FundNAV
        edit_Milestone_NAV_Administrator.BackColor = CUSTOM_BACKGROUND_UNCHANGED
      Else
        edit_Milestone_NAV_Administrator.BackColor = CUSTOM_BACKGROUND_CHANGED
      End If
      Label_Milestone_AdministratorUnits.Text = Math.Abs(FundValuation.FundUnits).ToString(DISPLAYMEMBER_INTEGERFORMAT)
      If (Custom_MS_Valuation_NAV_UnitPrice = False) Then
        If Math.Abs(FundValuation.FundUnits) <> 0.0# Then
          edit_Milestone_NAVUnitPrice_Valuation.Value = Math.Round(edit_Milestone_NAV_Administrator.Value / Math.Abs(FundValuation.FundUnits), 4)
        Else
          edit_Milestone_NAVUnitPrice_Valuation.Value = 0.0#
        End If
        edit_Milestone_NAVUnitPrice_Valuation.BackColor = CUSTOM_BACKGROUND_UNCHANGED
      Else
        edit_Milestone_NAVUnitPrice_Valuation.BackColor = CUSTOM_BACKGROUND_CHANGED
      End If

      If (Custom_MS_Valuation_GNAV = False) Then
        If (FundFees IsNot Nothing) Then
          edit_Milestone_GNAV_Administrator.Value = FundFees.FundPerformanceGnavUsed
        Else
          edit_Milestone_GNAV_Administrator.Value = 0
        End If
        edit_Milestone_GNAV_Administrator.BackColor = CUSTOM_BACKGROUND_UNCHANGED
      Else
        edit_Milestone_GNAV_Administrator.BackColor = CUSTOM_BACKGROUND_CHANGED
      End If
      If (Custom_MS_Valuation_GNAV_UnitPrice = False) Then
        If (Math.Abs(FundValuation.FundUnits) <> 0.0#) Then
          edit_Milestone_GNAVUnitPrice_Valuation.Text = Math.Round(edit_Milestone_GNAV_Administrator.Value / Math.Abs(FundValuation.FundUnits), 4)
        Else
          edit_Milestone_GNAVUnitPrice_Valuation.Text = 0.0#
        End If
        edit_Milestone_GNAVUnitPrice_Valuation.BackColor = CUSTOM_BACKGROUND_UNCHANGED
      Else
        edit_Milestone_GNAVUnitPrice_Valuation.BackColor = CUSTOM_BACKGROUND_CHANGED
      End If

      If (Custom_MS_Trading_NAV = False) Then
        edit_Milestone_NAV_Trading.Value = TradingValuation.FundNAV
        edit_Milestone_NAV_Trading.BackColor = CUSTOM_BACKGROUND_UNCHANGED
      Else
        edit_Milestone_NAV_Trading.BackColor = CUSTOM_BACKGROUND_CHANGED
      End If
      Label_Milestone_TradingUnits.Text = Math.Abs(TradingValuation.FundUnits).ToString(DISPLAYMEMBER_INTEGERFORMAT)
      If (Custom_MS_Trading_NAV_UnitPrice = False) Then
        If (Math.Abs(TradingValuation.FundUnits) <> 0.0#) Then
          edit_Milestone_NAVUnitPrice_Trading.Value = Math.Round(edit_Milestone_NAV_Trading.Value / Math.Abs(TradingValuation.FundUnits), 4)
        Else
          edit_Milestone_NAVUnitPrice_Trading.Value = 0.0#
        End If
        edit_Milestone_NAVUnitPrice_Trading.BackColor = CUSTOM_BACKGROUND_UNCHANGED
      Else
        edit_Milestone_NAVUnitPrice_Trading.BackColor = CUSTOM_BACKGROUND_CHANGED
      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in SetSortedRows_Milestone().", ex.StackTrace, True)
    Finally
      InPaint = False
    End Try


  End Sub

  ''' <summary>
  ''' Checks the permissions.
  ''' </summary>
  Private Sub CheckPermissions()
    ' *******************************************************************************
    ' Check User permissions
    '
    ' *******************************************************************************

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

    ' Transactions

    Permissions = MainForm.CheckPermissions(VeniceFormID.frmTransaction.ToString, RenaissanceGlobals.PermissionFeatureType.TypeForm)
    HasTransactionInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)

    ' Milestones

    Permissions = MainForm.CheckPermissions(VeniceFormID.frmSetFundMilestone.ToString, RenaissanceGlobals.PermissionFeatureType.TypeForm)
    HasMilestoneInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)

  End Sub

  ''' <summary>
  ''' Basic event handler, called when a form value is changed.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' *******************************************************************************
    ' In the event of one of the Form Controls changing, refresh the data grid.
    ' *******************************************************************************

    ' Fees valuation to update ?

    If InPaint = False Then

      Try
        InPaint = True

        If (sender Is Combo_Fund) OrElse _
         (sender Is Combo_TransactionGroup) OrElse _
         (sender Is Date_ValueDate) OrElse _
         (sender Is Date_FeesSince) OrElse _
         (sender Is Check_SR_IgnoreStatus) OrElse _
         (sender Is Check_SR_IgnoreTodaysTrades) OrElse _
         (sender Is Check_AdministratorPriceDates) Then

          Me.Grid_Valuations.Rows.Count = 1

          If (FeesObject IsNot Nothing) Then
            FeesObject.ValuationNeedsUpdate = True

            CustomManagementFees = False
            CustomPerformanceFees = False
            CustomCrystalisedFees = False
            CustomSubscriptionPrice = False

          End If

          ' BestPricesTable

          If (sender Is Date_ValueDate) Then
            If (BestPricesTable IsNot Nothing) Then
              BestPricesTable.Clear()
              BestPricesTable = Nothing
              PriceRowCache.Clear()
            End If

            Call getFXRates(Date_ValueDate.Value.Date) '

            If (AdministratorInventoryCache IsNot Nothing) Then
              AdministratorInventoryCache.Clear()
						End If

					End If

          ' Flag TradingValuationObject if Fund or Date has changed.

          If (sender Is Combo_Fund) OrElse (sender Is Date_ValueDate) Then

            If (FundAvailabilities IsNot Nothing) Then
              Try
                FundAvailabilities.Clear()
                FundAvailabilities = Nothing
              Catch ex As Exception
              End Try
            End If

            If (FuturesNotionalsTable IsNot Nothing) Then
              Try
                FuturesNotionalsTable.Clear()
                FuturesNotionalsTable = Nothing
              Catch ex As Exception
              End Try
            End If

						If (ClassSpecificPnLTable IsNot Nothing) Then
							ClassSpecificPnLTable.Clear()
							ClassSpecificPnLTable = Nothing
						End If

            If (TradingValuationObject IsNot Nothing) Then
              TradingValuationObject.ValuationNeedsUpdate = True

              Custom_MS_Valuation_NAV = False
              Custom_MS_Valuation_NAV_UnitPrice = False
              Custom_MS_Valuation_GNAV = False
              Custom_MS_Valuation_GNAV_UnitPrice = False
              Custom_MS_Trading_NAV = False
              Custom_MS_Trading_NAV_UnitPrice = False
              Custom_MS_Trading_GNAV = False
              Custom_MS_Trading_GNAV_UnitPrice = False

            End If
          End If

          ' Clear Notionals and Valuation tables so that they are refreshed after a significant control has changed
          Try
            If (myNotionalsTable IsNot Nothing) AndAlso (sender IsNot Date_FeesSince) Then myNotionalsTable.Clear()
            If (myValuationsTable IsNot Nothing) Then myValuationsTable.Clear()
          Catch ex As Exception
          End Try

        End If

				If (sender Is Check_AdministratorPriceDates) Then
					If (ClassSpecificPnLTable IsNot Nothing) Then
						ClassSpecificPnLTable.Clear()
						ClassSpecificPnLTable = Nothing
					End If
				End If

        If (sender Is Combo_Fund) OrElse (sender Is Date_ValueDate) Then
          Dim FundID As Integer = 0

          If IsNumeric(Combo_Fund.SelectedValue) Then
            Dim FundRow As DSFund.tblFundRow
            Dim FeeDate As Date = Date_ValueDate.Value

            If IsNumeric(Combo_Fund.SelectedValue) Then
              FundID = CInt(Combo_Fund.SelectedValue)
            End If

            ' Get Fund Info.

            FundRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, FundID), DSFund.tblFundRow)

            If (FundRow IsNot Nothing) Then

              Dim FundYearEnd As Date

              ' Clean Fund Year End 

              If FundRow.IsFundYearEndNull Then
                FundYearEnd = New Date(Now.Year, 12, 31) ' Default to calendar year
              Else
                FundYearEnd = FitDateToPeriod(DealingPeriod.Monthly, New Date(FeeDate.Year, FundRow.FundYearEnd.Month, 28), True)

                If (FundYearEnd < FeeDate) Then
                  FundYearEnd = FundYearEnd.AddYears(1)
                End If
              End If

              ' Fees Start Date

              If (CType(FundRow.FundPricingPeriod, DealingPeriod) = DealingPeriod.Annually) Then
                Date_FeesSince.Value = AddPeriodToDate(DealingPeriod.Annually, FundYearEnd, -1)
              Else
                Date_FeesSince.Value = AddPeriodToDate(CType(FundRow.FundPricingPeriod, DealingPeriod), FeeDate, -1)
              End If

            End If

            Call InitialiseFeesByClassGrid(0)
            Call InitialiseFeesByClassGrid(FundID)

          Else
            Call InitialiseFeesByClassGrid(0)
          End If

          ShowHideReconciliationTabs(FundID)

        End If

        'If (sender Is Combo_Fund) Then
        '  TabControl_Reconciliation.SelectedTab = Tab_Valuation
        'End If

        TabInitialStatus.Clear()

        ' Refresh 
        Call RefreshCurrentTab(True, False)

        'If (sender Is Combo_Fund) Then
        '  TabControl_Reconciliation.SelectedTab = Tab_Valuation
        'End If

      Catch ex As Exception
        MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in FormControlChanged().", ex.StackTrace, True)
      Finally
        InPaint = False
      End Try

    End If ' IF InPaint = False
  End Sub

  ''' <summary>
  ''' Handles the Click event of the Button_TradeSelectedRealised control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Button_TradeSelectedRealised_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_TradeSelectedRealised.Click, Button_TradeSelectedUnRealised.Click
    ' *******************************************************************************
    '
    '
    ' *******************************************************************************

    Try
      ' Check permissions.
      Dim Permissions As Integer = MainForm.CheckPermissions(Me.Name(), PermissionFeatureType.TypeForm)

      If ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) = 0) AndAlso ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) = 0) Then
        MessageBox.Show("You do not have Permissions to change transactions.")
        Exit Sub
      End If
    Catch ex As Exception
    End Try

    Try
      ' Check permissions.
      Dim Permissions As Integer = MainForm.CheckPermissions(Me.Name(), PermissionFeatureType.TypeForm)

      If ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) = 0) AndAlso ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) = 0) Then
        MessageBox.Show("You do not have Permissions to change transactions.")
        Exit Sub
      End If
    Catch ex As Exception
    End Try

    Try
      Dim MessageString As String = ""
      Dim TradeSelector As Integer = 0
      Dim Trade_Realised As Integer = 1
      Dim Trade_UnRealised As Integer = 2
      Dim CommentString As String = ""

      If sender Is Me.Button_TradeSelectedRealised Then
        MessageString = "Zero exposure for Realised P&L for the Selected Instruments ?"
        TradeSelector = Trade_Realised
        CommentString = "Realised P&L"
      ElseIf sender Is Me.Button_TradeSelectedUnRealised Then
        MessageString = "Zero exposure for Un-Realised P&L for the Selected Instruments ?"
        TradeSelector = Trade_UnRealised
        CommentString = "Un-Realised P&L"
      End If

      If MessageBox.Show(MessageString, "Notional Trade", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.OK Then

        Dim gridRow As C1.Win.C1FlexGrid.Row
        Dim TransactionDS As New DSTransaction
        Dim TransactionTable As DSTransaction.tblTransactionDataTable = TransactionDS.tblTransaction
        Dim newTransaction As DSTransaction.tblTransactionRow
        Dim ThisDataSourceRow As DSSelectFuturesNotional.tblSelectFuturesNotionalRow

        For Each gridRow In Me.Grid_NotionalExposure.Rows
          If gridRow.Selected Then
            Try
              ThisDataSourceRow = CType(CType(gridRow.DataSource, DataRowView).Row, RenaissanceDataClass.DSSelectFuturesNotional.tblSelectFuturesNotionalRow)

              newTransaction = TransactionTable.NewtblTransactionRow

              newTransaction.TransactionFund = ThisDataSourceRow.FundID
              newTransaction.TransactionSubFund = ThisDataSourceRow.FundID
              newTransaction.TransactionInstrument = ThisDataSourceRow.NotionalInstrumentID
              newTransaction.TransactionVersusInstrument = ThisDataSourceRow.FuturesInstrumentID

              If (TradeSelector = Trade_Realised) Then
                newTransaction.TransactionUnits = ThisDataSourceRow.NotionalTradeToZeroRealisedPnL
              ElseIf (TradeSelector = Trade_UnRealised) Then
                newTransaction.TransactionUnits = ThisDataSourceRow.NotionalTradeToZeroUnRealisedPnL
              End If

              If (newTransaction.TransactionUnits < 0) Then
                newTransaction.TransactionType = RenaissanceGlobals.TransactionTypes.Sell
              Else
                newTransaction.TransactionType = RenaissanceGlobals.TransactionTypes.Buy
              End If
              newTransaction.TransactionUnits = Math.Abs(newTransaction.TransactionUnits)


              newTransaction.TransactionTradeStatusID = DEFAULT_EOD_TRADESTATUS
              newTransaction.TransactionCounterparty = 1 ' Market

              newTransaction.TransactionGroup = ""
              newTransaction.TransactionInvestment = ""
              newTransaction.TransactionExecution = ""
              newTransaction.TransactionDataEntry = ""
              newTransaction.TransactionDecisionDate = Now.Date
              newTransaction.TransactionValueDate = Me.Date_ValueDate.Value
              newTransaction.TransactionSettlementDate = Me.Date_ValueDate.Value
              newTransaction.TransactionConfirmationDate = Me.Date_ValueDate.Value
              newTransaction.TransactionValueorAmount = "Amount"
              newTransaction.TransactionPrice = 1
              newTransaction.TransactionCosts = 0
              newTransaction.TransactionCostPercent = 0
              newTransaction.TransactionSettlement = newTransaction.TransactionUnits

              ' Transfer Details.

              newTransaction.TransactionIsTransfer = False
              newTransaction.TransactionEffectivePrice = newTransaction.TransactionPrice
              newTransaction.TransactionEffectiveValueDate = newTransaction.TransactionValueDate
              newTransaction.TransactionSpecificInitialEqualisationFlag = False
              newTransaction.TransactionSpecificInitialEqualisationValue = 0
              newTransaction.TransactionEffectiveWatermark = 0



              newTransaction.TransactionCostIsPercent = True
              newTransaction.TransactionExemptFromUpdate = True
              newTransaction.TransactionFinalAmount = (-1)
              newTransaction.TransactionFinalPrice = (-1)
              newTransaction.TransactionFinalCosts = (-1)
              newTransaction.TransactionFinalSettlement = (-1)
              newTransaction.TransactionInstructionFlag = (-1)
              newTransaction.TransactionRedeemWholeHoldingFlag = False
              newTransaction.TransactionBankConfirmation = 0
              newTransaction.TransactionInitialDataEntry = 0
              newTransaction.TransactionAmountConfirmed = 0
              newTransaction.TransactionSettlementConfirmed = 0
              newTransaction.TransactionFinalDataEntry = 0
              newTransaction.TransactionRedemptionID = 0
              newTransaction.TransactionComment = "Notionals Grid trade to zero " & CommentString
              newTransaction.TransactionCTFLAGS = 0
              newTransaction.TransactionCompoundRN = 0

              If (Math.Abs(newTransaction.TransactionUnits) >= 0.001) Then
                TransactionTable.AddtblTransactionRow(newTransaction)
              End If

            Catch ex As Exception
              MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error setting transaction details, Notional Transactions.", ex.StackTrace, True)
            End Try

          End If

        Next

        MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblTransaction, TransactionTable.Select())

        Dim UpdateString As String = ""
        Dim Counter As Integer

        For Counter = 0 To (TransactionTable.Rows.Count - 1)
          newTransaction = TransactionTable.Rows(Counter)

          If (newTransaction.IsAuditIDNull = False) Then
            If (Counter = 0) Then
              UpdateString = newTransaction.AuditID.ToString()
            Else
              UpdateString &= "," & newTransaction.AuditID.ToString()
            End If

          End If

        Next

        MainForm.ReloadTable_Background(RenaissanceStandardDatasets.tblTransaction.ChangeID, UpdateString, True) ' I don't use this here, but just in case it is used elsewhere...
        'Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceStandardDatasets.tblTransaction.ChangeID), True)
        MainForm.ReloadTable_Background(RenaissanceStandardDatasets.tblSelectFuturesNotional.ChangeID, "", True) ' I don't use this here, but just in case it is used elsewhere...

        RefreshCurrentTab(False, True)
      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error saving Notional Transactions.", ex.StackTrace, True)
    End Try
  End Sub

  ''' <summary>
  ''' Paints the valuations grid.
  ''' </summary>
  Private Sub PaintValuationsGrid()
    ' **********************************************************************************
    '
    ' **********************************************************************************

    If (Me.Disposing) OrElse (Me.IsDisposed) OrElse (Grid_Valuations Is Nothing) OrElse (Grid_Valuations.Cols Is Nothing) Then
      Exit Sub
    End If

    Try

      lock_tblFundValuation.EnterReadLock()

      Dim FundID As Integer = 0
      Dim FundRow As RenaissanceDataClass.DSFund.tblFundRow = Nothing
      Dim thisFundHasMultipleShareClasses As Boolean = False

      Dim UnitRowDictionary As New Dictionary(Of Integer, C1.Win.C1FlexGrid.Row)
      Dim NewGridRow As C1.Win.C1FlexGrid.Row = Nothing
      Dim LastTotalsRow As C1.Win.C1FlexGrid.Row = Nothing
      Dim thisRowView As DataRowView
      Dim thisRow As DataRow
      Dim thisSRInfoClass As SR_InformationClass = Nothing
      Dim LastInstrumentType As String = "_Duff_"
      Dim LastInstrumentTypeID As Integer = (-1)
      Dim Counter As Integer

      Dim PendingFeesAdded As Boolean = False

      Dim index_FirstCol As Integer = Grid_Valuations.Cols("colInstrumentType").SafeIndex
      Dim index_InstrumentType As Integer = Grid_Valuations.Cols("colInstrumentType").SafeIndex
      Dim index_ISIN As Integer = Grid_Valuations.Cols("colInstrumentType").SafeIndex
      Dim index_Description As Integer = Grid_Valuations.Cols("col_InstrumentDescription").SafeIndex
      Dim index_Currency As Integer = Grid_Valuations.Cols("colCurrency").SafeIndex
      Dim index_ValueDifference As Integer = Grid_Valuations.Cols("colValueDifference").SafeIndex
      Dim index_UnitsDifference As Integer = Grid_Valuations.Cols("colUnitsDifference").SafeIndex
      Dim index_PriceDifference As Integer = Grid_Valuations.Cols("colPriceDifference").SafeIndex
      Dim index_FXDifference As Integer = Grid_Valuations.Cols("colFXDifference").SafeIndex
      Dim index_ValueVenice As Integer = Grid_Valuations.Cols("colValueVenice").SafeIndex
      Dim index_UnitsVenice As Integer = Grid_Valuations.Cols("colUnitsVenice").SafeIndex
      Dim index_PriceVenice As Integer = Grid_Valuations.Cols("colPriceVenice").SafeIndex
      Dim index_FXVenice As Integer = Grid_Valuations.Cols("colFXVenice").SafeIndex
      Dim index_ValueBNP As Integer = Grid_Valuations.Cols("colValueBNP").SafeIndex
      Dim index_UnitsBNP As Integer = Grid_Valuations.Cols("colUnitsBNP").SafeIndex
      Dim index_PriceBNP As Integer = Grid_Valuations.Cols("colPriceBNP").SafeIndex
      Dim index_FXBNP As Integer = Grid_Valuations.Cols("colFXBNP").SafeIndex
      Dim index_DateVenice As Integer = Grid_Valuations.Cols("colDateVenice").SafeIndex
      Dim index_DateBNP As Integer = Grid_Valuations.Cols("colDateBNP").SafeIndex

      Dim ColumnCounter As Integer
      Dim DiffCounter As Integer
      Dim DifferenceIndexArray() As Integer = {index_ValueDifference, index_UnitsDifference, index_PriceDifference, index_FXDifference}
      Dim VeniceIndexArray() As Integer = {index_ValueVenice, index_UnitsVenice, index_PriceVenice, index_FXVenice}
      Dim BNPIndexArray() As Integer = {index_ValueBNP, index_UnitsBNP, index_PriceBNP, index_FXBNP}

      Dim index_InstrumentID As Integer = Grid_Valuations.Cols("colInstrument").SafeIndex
      Dim index_CaptureID As Integer = Grid_Valuations.Cols("colCaptureID").SafeIndex

      Dim GridStyle_DifferencePositive As C1.Win.C1FlexGrid.CellStyle = Grid_Valuations.Styles("DifferencePositive")
      Dim GridStyle_DifferenceNegative As C1.Win.C1FlexGrid.CellStyle = Grid_Valuations.Styles("DifferenceNegative")
      Dim GridStyle_HighlightPositive As C1.Win.C1FlexGrid.CellStyle = Grid_Valuations.Styles("HighlightPositive")
      Dim GridStyle_HighlightNegative As C1.Win.C1FlexGrid.CellStyle = Grid_Valuations.Styles("HighlightNegative")
      Dim GridStyle_DataNegative As C1.Win.C1FlexGrid.CellStyle = Grid_Valuations.Styles("DataNegative")
      Dim GridStyle_DataPositive As C1.Win.C1FlexGrid.CellStyle = Grid_Valuations.Styles("DataPositive")
      Dim GridStyle_TotalNegative As C1.Win.C1FlexGrid.CellStyle = Grid_Valuations.Styles("TotalsNegative")
      Dim GridStyle_TotalPositive As C1.Win.C1FlexGrid.CellStyle = Grid_Valuations.Styles("TotalsPositive")

      Dim Total_ValueDifference As Double = 0.0#
      Dim Total_ValueVenice As Double = 0.0#
      Dim Total_ValueBNP As Double = 0.0#

      Dim Section_ValueDifference As Double = 0.0#
      Dim Section_ValueVenice As Double = 0.0#
      Dim Section_ValueBNP As Double = 0.0#
      Dim FundValuation As FundFeeCalculator.FundValuationDetails = Nothing
      Dim FundFees As FundFeeCalculator.FundFeeDetails = Nothing


      Grid_Valuations.Redraw = False

      ' Clear Grid, to start
      Grid_Valuations.Rows.Count = 1  ' (The Header Row)

      ' Get Valuation object, should be established by now, so can get fees OK.
      If IsNumeric(Combo_Fund.SelectedValue) Then
        FundID = CInt(Nz(Combo_Fund.SelectedValue, 0))
        FundRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, FundID)
      End If


      If ((FundRow IsNot Nothing) AndAlso (FundRow.FundHasMultipleShareClasses = False)) AndAlso (Check_IncludeUnbookedFees.Checked) AndAlso (FeesObject Is Nothing) Then
        Dim FeeDate As Date
        Dim StatusGroupFilter As String
        Dim AdministratorDatesFilter As RenaissanceGlobals.AdministratorDatesFilter = RenaissanceGlobals.AdministratorDatesFilter.None


        FeeDate = Date_ValueDate.Value
        StatusGroupFilter = Nz(MainForm.GetComboSelectedValue(Combo_TransactionGroup), "")
        If (MainForm.GetCheckBoxChecked(Me.Check_SR_IgnoreStatus)) Then
          AdministratorDatesFilter += RenaissanceGlobals.AdministratorDatesFilter.SubscriptionRedemption_IgnoreTransactionStatus
        End If
        If (MainForm.GetCheckBoxChecked(Me.Check_SR_IgnoreTodaysTrades)) Then
          AdministratorDatesFilter += RenaissanceGlobals.AdministratorDatesFilter.SubscriptionRedemption_IgnoreTodaysTrades
        End If

        MainForm.SetToolStripText(StatusLabel, "Calculating Fund Administrator Valuation.")

        FundValuation = GetFeesValuation(FeesObject, FundID, FeeDate, Date_FeesSince.Value, StatusGroupFilter, AdministratorDatesFilter, Check_AdministratorPriceDates.Checked)

      End If

      If (FeesObject IsNot Nothing) Then

        Try
          MainForm.SetToolStripText(StatusLabel, "Getting Fund Valuation, " & FeesObject.FeeDate.ToString("dd MMM yyyy"))

          FundValuation = FeesObject.GetFundValuation()

          If (Check_IncludeUnbookedFees.Checked) Then
            MainForm.SetToolStripText(StatusLabel, "Calculating Fund Fees...")

            FundFees = FeesObject.GetFundFees()
          End If

        Catch ex As Exception
        Finally
          MainForm.SetToolStripText(StatusLabel, "")
        End Try

      End If

      Try

        ' Add Header Line.
        NewGridRow = Grid_Valuations.Rows.Add()
        NewGridRow(index_FirstCol) = "Primonial"
        NewGridRow.IsNode = True
        NewGridRow.Node.Level = 0

      Catch ex As Exception
      End Try

      InPaint = True

      ' Add Unit Section
      UnitRowDictionary.Clear()

      NewGridRow = Grid_Valuations.Rows.Add()
      NewGridRow(index_InstrumentType) = "Fund Units"
      NewGridRow(index_InstrumentID) = 0
      NewGridRow(index_CaptureID) = 0

      NewGridRow.IsNode = True
      NewGridRow.Node.Level = 1

      UnitRowDictionary.Add(0, NewGridRow)

      For ColumnCounter = (index_Currency + 1) To (Grid_Valuations.Cols.Count - 1)
        Grid_Valuations.SetCellStyle(NewGridRow.Index, ColumnCounter, GridStyle_TotalPositive)
      Next

      ' Fund Units section.

      Dim InstrumentsDataset As RenaissanceDataClass.DSInstrument = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblInstrument), RenaissanceDataClass.DSInstrument)
      Dim InstrumentsTable As RenaissanceDataClass.DSInstrument.tblInstrumentDataTable = InstrumentsDataset.tblInstrument
      Dim FundUnitInstruments() As RenaissanceDataClass.DSInstrument.tblInstrumentRow

      If (FundID = 0) Then
        FundUnitInstruments = InstrumentsTable.Select("false", "InstrumentDescription")
      Else
        FundUnitInstruments = InstrumentsTable.Select("InstrumentFundID=" & FundID.ToString, "InstrumentDescription")

        If ((FundUnitInstruments Is Nothing) OrElse (FundUnitInstruments.Length = 0)) AndAlso (FundRow IsNot Nothing) Then
          thisFundHasMultipleShareClasses = FundRow.FundHasMultipleShareClasses

          FundUnitInstruments = InstrumentsTable.Select("InstrumentFundID=" & FundRow.FundUnit.ToString, "InstrumentDescription")
        End If
      End If

      If (FundUnitInstruments IsNot Nothing) AndAlso (FundUnitInstruments.Length > 0) Then
        If (FundUnitInstruments.Length > 1) Then
          thisFundHasMultipleShareClasses = True
        End If

        For Counter = 0 To (FundUnitInstruments.Length - 1)

          NewGridRow = Grid_Valuations.Rows.Add()
          NewGridRow.IsNode = False

          NewGridRow(index_ISIN) = FundUnitInstruments(Counter).InstrumentISIN
          NewGridRow(index_Description) = FundUnitInstruments(Counter).InstrumentDescription
          NewGridRow(index_Currency) = FundUnitInstruments(Counter).InstrumentCurrency

          If myFundValuationUnitCounts.TryGetValue(FundUnitInstruments(Counter).InstrumentID, thisSRInfoClass) Then
            NewGridRow(index_UnitsVenice) = (thisSRInfoClass.UnitsSubscribed - thisSRInfoClass.UnitsRedeemed)
          Else
            NewGridRow(index_UnitsVenice) = 0.0#
          End If
          NewGridRow(index_UnitsBNP) = 0.0#
          NewGridRow(index_UnitsDifference) = 0.0#
          NewGridRow(index_PriceBNP) = 0.0#

          UnitRowDictionary.Add(FundUnitInstruments(Counter).InstrumentID, NewGridRow)

          Grid_Valuations.SetCellStyle(NewGridRow.Index, index_UnitsDifference, GridStyle_DifferencePositive)
          Grid_Valuations.SetCellStyle(NewGridRow.Index, index_PriceDifference, GridStyle_DifferencePositive)
          Grid_Valuations.SetCellStyle(NewGridRow.Index, index_ValueDifference, GridStyle_DifferencePositive)
          Grid_Valuations.SetCellStyle(NewGridRow.Index, index_FXDifference, GridStyle_DifferencePositive)

        Next
      Else
        NewGridRow = Grid_Valuations.Rows.Add()
        NewGridRow.IsNode = False

        NewGridRow(index_ISIN) = ""
        NewGridRow(index_Description) = "No Fund units Defined."
        NewGridRow(index_Currency) = ""
      End If

      '

      For Counter = 0 To (myValuationsDataView.Count - 1)

        ' Resolve Transaction Row.
        thisRowView = myValuationsDataView.Item(Counter)
        thisRow = thisRowView.Row

        ' Add Group Row?

        If (CStr(thisRow("rpt_InstrumentType")) <> LastInstrumentType) OrElse (CInt(thisRow("InstrumentTypeID")) <> LastInstrumentTypeID) Then

          ' Tagg on extra lines, eg Pending fees to the Cash Group

          Select Case LastInstrumentTypeID

            Case CInt(RenaissanceGlobals.InstrumentTypes.Cash)

              ' Don't allow 'IncludeUnbookedFees' for Multi Share classes because, at the moment, the FundFees object will not be correct.

              If (Check_IncludeUnbookedFees.Checked) AndAlso (FundFees IsNot Nothing) AndAlso (FundValuation IsNot Nothing) AndAlso (thisFundHasMultipleShareClasses = False) Then

                Dim ManagementFeesToBook As Double = 0.0#
                Dim PerformanceFeesToBook As Double = 0.0#
                Dim CrystalisedFeesToBook As Double = 0.0#

                PendingFeesAdded = True

                If (Not CustomManagementFees) Then
                  edit_ManagementFeesToTake.Value = Math.Round(FundFees.ManagementFees, 2)
                End If
                ManagementFeesToBook = -(edit_ManagementFeesToTake.Value + FundValuation.PendingManagementFees)

                If (Not CustomPerformanceFees) Then
                  Numeric_PerfFeesToTake.Value = Math.Round(FundFees.PerformanceFees, 2)
                End If
                PerformanceFeesToBook = -(Numeric_PerfFeesToTake.Value + (FundValuation.PreviousPerformanceFees + FundValuation.PendingPerformanceFees))

                If (Not CustomCrystalisedFees) Then
                  Numeric_PerfFeesCrystalisedToTake.Value = Math.Round(FundFees.PerformanceFeesCrystalised, 2)
                End If
                CrystalisedFeesToBook = -(Numeric_PerfFeesCrystalisedToTake.Value + FundValuation.PendingPerformanceFeesCrystalised)

                Section_ValueDifference += (ManagementFeesToBook + PerformanceFeesToBook + CrystalisedFeesToBook)
                Section_ValueVenice += (ManagementFeesToBook + PerformanceFeesToBook + CrystalisedFeesToBook)

                Total_ValueDifference += (ManagementFeesToBook + PerformanceFeesToBook + CrystalisedFeesToBook)
                Total_ValueVenice += (ManagementFeesToBook + PerformanceFeesToBook + CrystalisedFeesToBook)

                ' Update 'LastTotalsRow'

                UpdateValuationTotalsRow(LastTotalsRow, index_ValueDifference, index_ValueVenice, index_ValueBNP, Section_ValueDifference, Section_ValueVenice, Section_ValueBNP, GridStyle_TotalNegative, GridStyle_TotalPositive)

                ' Add Rows : 

                If (Math.Abs(ManagementFeesToBook) >= 1.0#) Then

                  NewGridRow = Grid_Valuations.Rows.Add()
                  NewGridRow.IsNode = False

                  NewGridRow(index_Description) = "Unbooked Management Fees"
                  NewGridRow(index_ValueDifference) = ManagementFeesToBook
                  NewGridRow(index_ValueVenice) = ManagementFeesToBook

                  Grid_Valuations.SetCellStyle(NewGridRow.Index, index_UnitsDifference, GridStyle_DifferencePositive)
                  Grid_Valuations.SetCellStyle(NewGridRow.Index, index_PriceDifference, GridStyle_DifferencePositive)
                  Grid_Valuations.SetCellStyle(NewGridRow.Index, index_ValueDifference, GridStyle_DifferencePositive)

                  If (ManagementFeesToBook < 0.0#) Then
                    Grid_Valuations.SetCellStyle(NewGridRow.Index, index_ValueDifference, GridStyle_DifferenceNegative)
                    Grid_Valuations.SetCellStyle(NewGridRow.Index, index_ValueVenice, GridStyle_DataNegative)
                  End If

                End If

                If (Math.Abs(PerformanceFeesToBook) >= 1.0#) Then

                  NewGridRow = Grid_Valuations.Rows.Add()
                  NewGridRow.IsNode = False

                  NewGridRow(index_Description) = "Unbooked Performance Fees"
                  NewGridRow(index_ValueDifference) = PerformanceFeesToBook
                  NewGridRow(index_ValueVenice) = PerformanceFeesToBook

                  Grid_Valuations.SetCellStyle(NewGridRow.Index, index_UnitsDifference, GridStyle_DifferencePositive)
                  Grid_Valuations.SetCellStyle(NewGridRow.Index, index_PriceDifference, GridStyle_DifferencePositive)
                  Grid_Valuations.SetCellStyle(NewGridRow.Index, index_ValueDifference, GridStyle_DifferencePositive)

                  If (PerformanceFeesToBook < 0.0#) Then
                    Grid_Valuations.SetCellStyle(NewGridRow.Index, index_ValueDifference, GridStyle_DifferenceNegative)
                    Grid_Valuations.SetCellStyle(NewGridRow.Index, index_ValueVenice, GridStyle_DataNegative)
                  End If

                End If

                If (Math.Abs(CrystalisedFeesToBook) >= 1.0#) Then

                  NewGridRow = Grid_Valuations.Rows.Add()
                  NewGridRow.IsNode = False

                  NewGridRow(index_Description) = "Unbooked Crystalised Fees"
                  NewGridRow(index_ValueDifference) = CrystalisedFeesToBook
                  NewGridRow(index_ValueVenice) = CrystalisedFeesToBook

                  Grid_Valuations.SetCellStyle(NewGridRow.Index, index_UnitsDifference, GridStyle_DifferencePositive)
                  Grid_Valuations.SetCellStyle(NewGridRow.Index, index_PriceDifference, GridStyle_DifferencePositive)
                  Grid_Valuations.SetCellStyle(NewGridRow.Index, index_ValueDifference, GridStyle_DifferencePositive)

                  If (CrystalisedFeesToBook < 0.0#) Then
                    Grid_Valuations.SetCellStyle(NewGridRow.Index, index_ValueDifference, GridStyle_DifferenceNegative)
                    Grid_Valuations.SetCellStyle(NewGridRow.Index, index_ValueVenice, GridStyle_DataNegative)
                  End If

                End If

                'Mode_Code_Here()

              End If

              PendingFeesAdded = True

          End Select

          Section_ValueDifference = 0.0#
          Section_ValueVenice = 0.0#
          Section_ValueBNP = 0.0#

          ' 

          NewGridRow = Grid_Valuations.Rows.Add()
          NewGridRow(index_InstrumentType) = thisRow("InstrumentType")
          NewGridRow(index_InstrumentID) = 0
          NewGridRow(index_CaptureID) = 0

          NewGridRow.IsNode = True
          NewGridRow.Node.Level = 1

          For ColumnCounter = (index_Currency + 1) To (Grid_Valuations.Cols.Count - 1)
            Grid_Valuations.SetCellStyle(NewGridRow.Index, ColumnCounter, GridStyle_TotalPositive)
          Next

          LastInstrumentType = CStr(thisRow("rpt_InstrumentType"))
          LastInstrumentTypeID = CInt(thisRow("InstrumentTypeID"))

          LastTotalsRow = NewGridRow

        End If

        ' Add Valuation Row

        NewGridRow = Grid_Valuations.Rows.Add()
        NewGridRow.IsNode = False

        NewGridRow(index_ISIN) = CStr(thisRow("rpt_ISIN"))
        NewGridRow(index_Description) = CStr(thisRow("rpt_Description"))
        NewGridRow(index_Currency) = CStr(thisRow("rpt_Currency"))

        If (CDbl(Nz(thisRow("neo_marketvalue_rc_value"), 0.0#)) = 0.0#) AndAlso (CDbl(Nz(thisRow("neo_quantity_value"), 0.0#)) <> 0.0#) Then
          If (CInt(Nz(thisRow("neo_captureID"), 0)) > 0) Then

            If (CDbl(thisRow("neo_exchangerate_value")) = 0.0#) Then
              NewGridRow(index_ValueBNP) = CDbl(Nz(thisRow("neo_quantity_value"), 0.0#)) * CDbl(thisRow("neo_price_value")) * CDbl(Nz(thisRow("CompoundFXRate"), 0.0#))
            Else
              NewGridRow(index_ValueBNP) = 0.0#
            End If

            NewGridRow(index_ValueDifference) = (CDbl(Nz(thisRow("Value"), 0)) - CDbl(NewGridRow(index_ValueBNP)))

          Else
            NewGridRow(index_ValueDifference) = CDbl(Nz(thisRow("Value"), 0))
          End If
        Else
          NewGridRow(index_ValueDifference) = (CDbl(Nz(thisRow("Value"), 0)) - CDbl(Nz(thisRow("neo_marketvalue_rc_value"), 0.0#)))
        End If

        NewGridRow(index_UnitsDifference) = Math.Abs(CDbl(Nz(thisRow("SignedUnits"), 0)) - CDbl(Nz(thisRow("neo_quantity_value"), 0.0#)))
        NewGridRow(index_PriceDifference) = Math.Abs(CDbl(Nz(thisRow("PriceLevel"), 0)) - CDbl(Nz(thisRow("neo_price_value"), 0.0#)))
        NewGridRow(index_FXDifference) = Math.Abs((IIf(CDbl(Nz(thisRow("CompoundFXRate"), 1.0#)) = 0.0#, 0.0#, 1.0#) / IIf(CDbl(Nz(thisRow("CompoundFXRate"), 1.0#)) = 0.0#, 1.0#, CDbl(Nz(thisRow("CompoundFXRate"), 1.0#)))) - CDbl(Nz(thisRow("neo_exchangerate_value"), 0.0#)))

        Section_ValueDifference += NewGridRow(index_ValueDifference)
        Total_ValueDifference += NewGridRow(index_ValueDifference)

        NewGridRow(index_InstrumentID) = CInt(Nz(thisRow("Instrument"), 0))

        If (NewGridRow(index_InstrumentID) > 0) Then
          NewGridRow(index_ValueVenice) = CDbl(Nz(thisRow("Value"), 0))
          NewGridRow(index_UnitsVenice) = CDbl(Nz(thisRow("SignedUnits"), 0))
          NewGridRow(index_PriceVenice) = CDbl(Nz(thisRow("PriceLevel"), 0))
          NewGridRow(index_FXVenice) = (1.0# / CDbl(Nz(thisRow("CompoundFXRate"), 1.0#)))
          NewGridRow(index_DateVenice) = CDate(Nz(thisRow("PriceDate"), Renaissance_BaseDate))

          Section_ValueVenice += NewGridRow(index_ValueVenice)

          Total_ValueVenice += NewGridRow(index_ValueVenice)

          ' Set styles on 'Venice' Columns
          For DiffCounter = 0 To (VeniceIndexArray.Length - 1)
            If (NewGridRow(VeniceIndexArray(DiffCounter)) < 0.0#) Then
              Grid_Valuations.SetCellStyle(NewGridRow.Index, VeniceIndexArray(DiffCounter), GridStyle_DataNegative)
            Else
              Grid_Valuations.SetCellStyle(NewGridRow.Index, VeniceIndexArray(DiffCounter), GridStyle_DataPositive)
            End If
          Next

        End If

        NewGridRow(index_CaptureID) = CInt(Nz(thisRow("neo_captureID"), 0))

        If (NewGridRow(index_CaptureID) > 0) Then
          ' If Neolink FX is Zero, use Venice one for valuation (particularly for the Fonds Intern)

          If ((CDbl(thisRow("neo_marketvalue_rc_value")) = 0.0#) AndAlso (CDbl(thisRow("neo_exchangerate_value")) = 0.0#)) Then
            NewGridRow(index_ValueBNP) = CDbl(thisRow("neo_quantity_value")) * CDbl(thisRow("neo_price_value")) * CDbl(Nz(thisRow("CompoundFXRate"), 0.0#))
          Else
            NewGridRow(index_ValueBNP) = CDbl(thisRow("neo_marketvalue_rc_value"))
          End If

          NewGridRow(index_UnitsBNP) = CDbl(thisRow("neo_quantity_value"))
          NewGridRow(index_PriceBNP) = CDbl(thisRow("neo_price_value"))
          NewGridRow(index_FXBNP) = CDbl(thisRow("neo_exchangerate_value"))
          NewGridRow(index_DateBNP) = CDate(thisRow("neo_pricedate_value"))

          Section_ValueBNP += NewGridRow(index_ValueBNP)
          Total_ValueBNP += NewGridRow(index_ValueBNP)

          ' Set styles on 'BNP' Columns

          For DiffCounter = 0 To (BNPIndexArray.Length - 1)
            If (NewGridRow(BNPIndexArray(DiffCounter)) < 0.0#) Then
              Grid_Valuations.SetCellStyle(NewGridRow.Index, BNPIndexArray(DiffCounter), GridStyle_DataNegative)
            End If
          Next
        End If

        If (NewGridRow(index_InstrumentID) > 0) AndAlso (NewGridRow(index_CaptureID) > 0) Then
          ' Highlight different Values

          If Math.Abs(NewGridRow(index_ValueVenice) - NewGridRow(index_ValueBNP)) >= 1.0# Then
            If (NewGridRow(index_ValueVenice) < 0.0#) Then
              Grid_Valuations.SetCellStyle(NewGridRow.Index, index_ValueVenice, GridStyle_HighlightNegative)
            Else
              Grid_Valuations.SetCellStyle(NewGridRow.Index, index_ValueVenice, GridStyle_HighlightPositive)
            End If

            If (NewGridRow(index_ValueBNP) < 0.0#) Then
              Grid_Valuations.SetCellStyle(NewGridRow.Index, index_ValueBNP, GridStyle_HighlightNegative)
            Else
              Grid_Valuations.SetCellStyle(NewGridRow.Index, index_ValueBNP, GridStyle_HighlightPositive)
            End If
          End If

          If Math.Abs(NewGridRow(index_UnitsVenice) - NewGridRow(index_UnitsBNP)) >= 1.0# Then
            If (NewGridRow(index_UnitsVenice) < 0.0#) Then
              Grid_Valuations.SetCellStyle(NewGridRow.Index, index_UnitsVenice, GridStyle_HighlightNegative)
            Else
              Grid_Valuations.SetCellStyle(NewGridRow.Index, index_UnitsVenice, GridStyle_HighlightPositive)
            End If

            If (NewGridRow(index_UnitsBNP) < 0.0#) Then
              Grid_Valuations.SetCellStyle(NewGridRow.Index, index_UnitsBNP, GridStyle_HighlightNegative)
            Else
              Grid_Valuations.SetCellStyle(NewGridRow.Index, index_UnitsBNP, GridStyle_HighlightPositive)
            End If
          End If


          If Math.Abs(NewGridRow(index_PriceVenice) - NewGridRow(index_PriceBNP)) >= 0.01# Then
            If (NewGridRow(index_PriceVenice) < 0.0#) Then
              Grid_Valuations.SetCellStyle(NewGridRow.Index, index_PriceVenice, GridStyle_HighlightNegative)
            Else
              Grid_Valuations.SetCellStyle(NewGridRow.Index, index_PriceVenice, GridStyle_HighlightPositive)
            End If

            If (NewGridRow(index_PriceBNP) < 0.0#) Then
              Grid_Valuations.SetCellStyle(NewGridRow.Index, index_PriceBNP, GridStyle_HighlightNegative)
            Else
              Grid_Valuations.SetCellStyle(NewGridRow.Index, index_PriceBNP, GridStyle_HighlightPositive)
            End If
          End If

          If Math.Abs(NewGridRow(index_FXVenice) - NewGridRow(index_FXBNP)) >= 0.0001# Then
            If (NewGridRow(index_FXVenice) < 0.0#) Then
              Grid_Valuations.SetCellStyle(NewGridRow.Index, index_FXVenice, GridStyle_HighlightNegative)
            Else
              Grid_Valuations.SetCellStyle(NewGridRow.Index, index_FXVenice, GridStyle_HighlightPositive)
            End If

            If (NewGridRow(index_FXBNP) < 0.0#) Then
              Grid_Valuations.SetCellStyle(NewGridRow.Index, index_FXBNP, GridStyle_HighlightNegative)
            Else
              Grid_Valuations.SetCellStyle(NewGridRow.Index, index_FXBNP, GridStyle_HighlightPositive)
            End If
          End If

          If (Math.Abs((CDate(NewGridRow(index_DateVenice)) - CDate(NewGridRow(index_DateBNP))).TotalDays) >= 0.5#) OrElse (Math.Abs((CDate(NewGridRow(index_DateVenice)) - Date_ValueDate.Value).TotalDays) >= 7.0#) Then
            Grid_Valuations.SetCellStyle(NewGridRow.Index, index_DateVenice, GridStyle_HighlightPositive)
            Grid_Valuations.SetCellStyle(NewGridRow.Index, index_DateBNP, GridStyle_HighlightPositive)
          End If
        End If

        ' Set styles on 'Difference' Columns

        For DiffCounter = 0 To (DifferenceIndexArray.Length - 1)
          If (NewGridRow(DifferenceIndexArray(DiffCounter)) < 0.0#) Then
            Grid_Valuations.SetCellStyle(NewGridRow.Index, DifferenceIndexArray(DiffCounter), GridStyle_DifferenceNegative)
          Else
            Grid_Valuations.SetCellStyle(NewGridRow.Index, DifferenceIndexArray(DiffCounter), GridStyle_DifferencePositive)
          End If
        Next

        ' Update 'LastTotalsRow'

        UpdateValuationTotalsRow(LastTotalsRow, index_ValueDifference, index_ValueVenice, index_ValueBNP, Section_ValueDifference, Section_ValueVenice, Section_ValueBNP, GridStyle_TotalNegative, GridStyle_TotalPositive)

      Next ' 

      'If (Check_IncludeUnbookedFees.Checked = True) AndAlso (Not PendingFeesAdded) Then

      '  ' Add them if necessary....

      '  If LastInstrumentTypeID <> CInt(RenaissanceGlobals.InstrumentTypes.Cash) Then
      '    ' Add Summary Line

      '  End If

      '  ' Add Fee Lines


      '  ' Update Totals


      'End If

      ' Update 'Fund Totals Row'

      Grid_Valuations.Rows(1)(index_ValueDifference) = Total_ValueDifference
      Grid_Valuations.Rows(1)(index_ValueVenice) = Total_ValueVenice
      Grid_Valuations.Rows(1)(index_ValueBNP) = Total_ValueBNP

      If (Total_ValueDifference < 0.0#) Then
        Grid_Valuations.SetCellStyle(1, index_ValueDifference, GridStyle_TotalNegative)
      Else
        Grid_Valuations.SetCellStyle(1, index_ValueDifference, GridStyle_TotalPositive)
      End If
      If (Total_ValueVenice < 0.0#) Then
        Grid_Valuations.SetCellStyle(1, index_ValueVenice, GridStyle_TotalNegative)
      Else
        Grid_Valuations.SetCellStyle(1, index_ValueVenice, GridStyle_TotalPositive)
      End If
      If (Total_ValueBNP < 0.0#) Then
        Grid_Valuations.SetCellStyle(1, index_ValueBNP, GridStyle_TotalNegative)
      Else
        Grid_Valuations.SetCellStyle(1, index_ValueBNP, GridStyle_TotalPositive)
      End If

      ' Fill in BNP Unit details

      Dim thisSQLCommand As SqlCommand = Nothing
      Dim thisSQLConnection As SqlConnection = Nothing
      Dim AdminShareClassDetails As New DataTable
      Dim AdminShareClassRow As DataRow

      Try
        Dim RetryCounter As Integer

        If (FundRow IsNot Nothing) Then
          ' Fee Start Date Values
          ' Get Data and populate
          thisSQLCommand = New SqlCommand
          thisSQLCommand.CommandType = CommandType.StoredProcedure
          thisSQLCommand.CommandText = "adp_tblRecValuation_SelectCommand"
          thisSQLCommand.Parameters.Add(New SqlParameter("@NavDate", SqlDbType.Date)).Value = Date_ValueDate.Value
          thisSQLCommand.Parameters.Add(New SqlParameter("@FundCode", SqlDbType.NVarChar, 50)).Value = FundRow.FundAdministratorCode
          thisSQLCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", SqlDbType.DateTime)).Value = MainForm.Main_Knowledgedate

          thisSQLConnection = MainForm.GetVeniceConnection

          For RetryCounter = 0 To 1
            thisSQLCommand.Connection = thisSQLConnection

            Try

              AdminShareClassDetails.Load(thisSQLCommand.ExecuteReader)
              Exit For
            Catch ex As Exception

              Try
                thisSQLConnection.Close()
              Catch Inner_ex As Exception
              End Try

              thisSQLConnection = MainForm.GetVeniceConnection()
              AdminShareClassDetails.Clear()
            End Try
          Next
        End If

        If (AdminShareClassDetails IsNot Nothing) AndAlso (AdminShareClassDetails.Rows.Count > 0) Then

          For Each AdminShareClassRow In AdminShareClassDetails.Rows

            For Each NewGridRow In UnitRowDictionary.Values

              If NewGridRow(index_ISIN) = CStr(Nz(AdminShareClassRow("ISINCode"), "")) Then

                NewGridRow(index_UnitsBNP) = CDbl(Nz(AdminShareClassRow("NumberOfShares_Value"), 0.0#))
                NewGridRow(index_UnitsDifference) = Math.Abs(CDbl(Nz(NewGridRow(index_UnitsVenice), 0)) - CDbl(Nz(AdminShareClassRow("NumberOfShares_Value"), 0.0#)))
                NewGridRow(index_PriceBNP) = CDbl(Nz(AdminShareClassRow("NAV_Value"), 0.0#))

                Exit For
              End If

            Next

          Next

        End If

      Catch ex As Exception
      Finally
        Try
          If (thisSQLConnection IsNot Nothing) Then
            thisSQLConnection.Close()
            thisSQLConnection = Nothing
          End If
        Catch ex As Exception
        End Try
      End Try

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in PaintValuationsGrid()", ex.StackTrace, True)
    Finally
      If (lock_tblFundValuation.IsReadLockHeld) Then
        lock_tblFundValuation.ExitReadLock()
      End If

      Grid_Valuations.Redraw = True
      InPaint = False
    End Try

  End Sub

  ''' <summary>
  ''' Updates the valuation totals row.
  ''' </summary>
  ''' <param name="LastTotalsRow">The last totals row.</param>
  ''' <param name="index_ValueDifference">The index_ value difference.</param>
  ''' <param name="index_ValueVenice">The index_ value venice.</param>
  ''' <param name="index_ValueBNP">The index_ value BNP.</param>
  ''' <param name="Section_ValueDifference">The section_ value difference.</param>
  ''' <param name="Section_ValueVenice">The section_ value venice.</param>
  ''' <param name="Section_ValueBNP">The section_ value BNP.</param>
  ''' <param name="GridStyle_TotalNegative">The grid style_ total negative.</param>
  ''' <param name="GridStyle_TotalPositive">The grid style_ total positive.</param>
  Private Sub UpdateValuationTotalsRow(ByVal LastTotalsRow As C1.Win.C1FlexGrid.Row, ByVal index_ValueDifference As Integer, ByVal index_ValueVenice As Integer, ByVal index_ValueBNP As Integer, ByVal Section_ValueDifference As Double, ByVal Section_ValueVenice As Double, ByVal Section_ValueBNP As Double, ByVal GridStyle_TotalNegative As C1.Win.C1FlexGrid.CellStyle, ByVal GridStyle_TotalPositive As C1.Win.C1FlexGrid.CellStyle)

    Try

      ' Update 'LastTotalsRow'

      LastTotalsRow(index_ValueDifference) = Section_ValueDifference
      LastTotalsRow(index_ValueVenice) = Section_ValueVenice
      LastTotalsRow(index_ValueBNP) = Section_ValueBNP

      If (Section_ValueDifference < 0.0#) Then
        Grid_Valuations.SetCellStyle(LastTotalsRow.Index, index_ValueDifference, GridStyle_TotalNegative)
      Else
        Grid_Valuations.SetCellStyle(LastTotalsRow.Index, index_ValueDifference, GridStyle_TotalPositive)
      End If
      If (Section_ValueVenice < 0.0#) Then
        Grid_Valuations.SetCellStyle(LastTotalsRow.Index, index_ValueVenice, GridStyle_TotalNegative)
      Else
        Grid_Valuations.SetCellStyle(LastTotalsRow.Index, index_ValueVenice, GridStyle_TotalPositive)
      End If
      If (Section_ValueBNP < 0.0#) Then
        Grid_Valuations.SetCellStyle(LastTotalsRow.Index, index_ValueBNP, GridStyle_TotalNegative)
      Else
        Grid_Valuations.SetCellStyle(LastTotalsRow.Index, index_ValueBNP, GridStyle_TotalPositive)
      End If

    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Loads the best prices.
  ''' </summary>
  Private Sub LoadBestPrices()

    ' Check the Prices table is loaded and the sorted Prices array is established

    If (BestPricesTable Is Nothing) OrElse (BestPricesTable.Rows.Count = 0) Then
      BestPricesTable = New DataTable
      Dim thisCommand As New SqlCommand

      Try

        thisCommand.CommandType = CommandType.Text
        thisCommand.CommandText = "SELECT InstrumentID, PriceDate, PriceLevel FROM dbo.fn_BestPrice(@ValueDate, @OnlyFinalPrices, @KnowledgeDate)"
        thisCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

        thisCommand.Parameters.Add(New SqlParameter("@ValueDate", SqlDbType.DateTime)).Value = Date_ValueDate.Value.Date
        thisCommand.Parameters.Add(New SqlParameter("@OnlyFinalPrices", SqlDbType.Int)).Value = 0
        thisCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", SqlDbType.DateTime)).Value = MainForm.Main_Knowledgedate

        thisCommand.Connection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)

        If (thisCommand IsNot Nothing) AndAlso (thisCommand.Connection IsNot Nothing) Then
          SyncLock thisCommand.Connection
            MainForm.LoadTable_Custom(BestPricesTable, thisCommand)
            'BestPricesTable.Load(thisCommand.ExecuteReader)
          End SyncLock
        End If

        SortedPrices = BestPricesTable.Select("True", "InstrumentID, PriceDate")

      Catch ex As Exception

        MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error Getting Best Prices.", ex.Message, ex.StackTrace, True)

        If (BestPricesTable IsNot Nothing) Then
          BestPricesTable.Clear()
        End If

        BestPricesTable = Nothing
        SortedPrices = Nothing

      Finally

        If (thisCommand IsNot Nothing) Then
          thisCommand.Connection = Nothing
        End If
        thisCommand = Nothing

      End Try

    End If

  End Sub

  ''' <summary>
  ''' Gets the best price row.
  ''' </summary>
  ''' <param name="InstrumentID">The instrument ID.</param>
  ''' <returns>DataRow.</returns>
  Friend Function GetBestPriceRow(ByVal InstrumentID As Integer) As DataRow
    ' ******************************************************************************************************
    ' ******************************************************************************************************

    If (BestPricesTable Is Nothing) OrElse (BestPricesTable.Rows.Count = 0) Then
      Call LoadBestPrices()
    End If

    ' Find Latest Price

    ' Code changed to use the fn_BestPrice function , thus there is only One Price per instrument.

    Dim FirstIndex As Integer
    Dim MidIndex As Integer
    Dim LastIndex As Integer
    Dim thisPriceRow As DataRow

    Dim InstrumentIdOrdinal As Integer
    Dim PriceLevelOrdinal As Integer

    If (BestPricesTable.Columns.Contains("InstrumentID")) Then
      InstrumentIdOrdinal = BestPricesTable.Columns.IndexOf("InstrumentID")
    Else
      Return Nothing
    End If

    If (BestPricesTable.Columns.Contains("PriceLevel")) Then
      PriceLevelOrdinal = BestPricesTable.Columns.IndexOf("PriceLevel")
    Else
      Return Nothing
    End If

    Try
      FirstIndex = 0
      LastIndex = SortedPrices.Length - 1

      ' Check the First and last Items

      thisPriceRow = SortedPrices(FirstIndex)
      If CInt(thisPriceRow(InstrumentIdOrdinal)) = InstrumentID Then
        Return thisPriceRow
        Exit Function
      End If

      thisPriceRow = SortedPrices(LastIndex)
      If CInt(thisPriceRow(InstrumentIdOrdinal)) = InstrumentID Then
        Return thisPriceRow
        Exit Function
      End If

      ' Chop the array to find the Instrument.

      While (LastIndex > (FirstIndex + 1))

        MidIndex = CInt((LastIndex + FirstIndex) / 2)
        thisPriceRow = SortedPrices(MidIndex)

        If CInt(thisPriceRow(InstrumentIdOrdinal)) = InstrumentID Then
          Return thisPriceRow
          Exit Function
        ElseIf CInt(thisPriceRow(InstrumentIdOrdinal)) < InstrumentID Then
          FirstIndex = MidIndex
        Else
          LastIndex = MidIndex
        End If

      End While
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error returning Best Price.", ex.Message, ex.StackTrace, True)
    End Try

    Return Nothing

  End Function

	''' <summary>
	''' Loads the best prices.
	''' </summary>
	Private Sub LoadClassSpecificPnL()

		Dim thisCommand As New SqlCommand

		Try
			If (ClassSpecificPnLTable IsNot Nothing) Then
				ClassSpecificPnLTable.Clear()
			End If

			Dim FundID As Integer = 0
			Dim AdministratorDatesFilter As RenaissanceGlobals.AdministratorDatesFilter = RenaissanceGlobals.AdministratorDatesFilter.None

			If IsNumeric(Combo_Fund.SelectedValue) Then
				FundID = CInt(Combo_Fund.SelectedValue)
			End If

			' AdministratorDatesFilter (bitmap)
			'
			'--	1		- Ignore the transaction status for Subscriptions and Redemptions
			'--	2		- Ignore the Subscriptions and Redemptions on (or after of course) the given Value Date

			If (MainForm.GetCheckBoxChecked(Me.Check_SR_IgnoreStatus)) Then
				AdministratorDatesFilter += RenaissanceGlobals.AdministratorDatesFilter.SubscriptionRedemption_IgnoreTransactionStatus
			End If
			If (MainForm.GetCheckBoxChecked(Me.Check_SR_IgnoreTodaysTrades)) Then
				AdministratorDatesFilter += RenaissanceGlobals.AdministratorDatesFilter.SubscriptionRedemption_IgnoreTodaysTrades
			End If

			' *******

			ClassSpecificPnLTable = New DataTable

			thisCommand.CommandType = CommandType.StoredProcedure
			thisCommand.CommandText = "spu_ProfitAndLoss_ClassSpecific"
			thisCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

			thisCommand.Parameters.Add(New SqlParameter("@FundID", SqlDbType.Int)).Value = FundID
			thisCommand.Parameters.Add(New SqlParameter("@SpecificUnitID", SqlDbType.Int)).Value = 0
			thisCommand.Parameters.Add(New SqlParameter("@ValueDateFrom", SqlDbType.DateTime)).Value = Date_FeesSince.Value.Date
			thisCommand.Parameters.Add(New SqlParameter("@ValueDateTo", SqlDbType.DateTime)).Value = Date_ValueDate.Value.Date
			thisCommand.Parameters.Add(New SqlParameter("@UnitPriceVariant", SqlDbType.Int)).Value = 2 ' Ex Mgmt & Perf fees
			thisCommand.Parameters.Add(New SqlParameter("@StatusGroupFilter", SqlDbType.VarChar, 50)).Value = Nz(MainForm.GetComboSelectedValue(Combo_TransactionGroup), "")
			thisCommand.Parameters.Add(New SqlParameter("@AdministratorDatesFilter", SqlDbType.Int)).Value = CInt(AdministratorDatesFilter)
			thisCommand.Parameters.Add(New SqlParameter("@ReferenceFXID", SqlDbType.Int)).Value = REFERENCE_REPORT_CURRENCY
			thisCommand.Parameters.Add(New SqlParameter("@UseAdministratorPriceDates", SqlDbType.Int)).Value = IIf(Check_AdministratorPriceDates.Checked, 1, 0)
			thisCommand.Parameters.Add(New SqlParameter("@KnowledgeDateFrom", SqlDbType.DateTime)).Value = MainForm.Main_Knowledgedate
			thisCommand.Parameters.Add(New SqlParameter("@KnowledgeDateTo", SqlDbType.DateTime)).Value = MainForm.Main_Knowledgedate
			thisCommand.Parameters.Add(New SqlParameter("@KnowledgeDateLinkedTables", SqlDbType.DateTime)).Value = MainForm.Main_Knowledgedate
			thisCommand.Connection = MainForm.GetVeniceConnection

			If (thisCommand IsNot Nothing) AndAlso (thisCommand.Connection IsNot Nothing) Then
				SyncLock thisCommand.Connection
					MainForm.LoadTable_Custom(ClassSpecificPnLTable, thisCommand)
				End SyncLock
			End If

		Catch ex As Exception

			MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error Loading Class specific P&L.", ex.Message, ex.StackTrace, True)

			If (ClassSpecificPnLTable IsNot Nothing) Then
				ClassSpecificPnLTable.Clear()
			End If

			ClassSpecificPnLTable = Nothing

		Finally

			Try
				If (thisCommand IsNot Nothing) Then
					thisCommand.Connection.Close()
					thisCommand.Connection = Nothing
				End If
			Catch ex As Exception
			End Try

			thisCommand = Nothing

		End Try

	End Sub


  ''' <summary>
  ''' Gets the instrument price row.
  ''' </summary>
  ''' <param name="InstrumentID">The instrument ID.</param>
  ''' <param name="PriceDate">The price date.</param>
  ''' <returns>DataRow.</returns>
  Private Function GetInstrumentPriceRow(ByVal InstrumentID As Integer, ByVal PriceDate As Date) As DataRow
    ' ******************************************************************************************************
    '
    ' ******************************************************************************************************

    Try

      Dim thisPriceRow As DataRow = GetBestPriceRow(InstrumentID)

      If (thisPriceRow Is Nothing) Then
        Return Nothing
      End If

      Dim PriceDS As DSPrice = Nothing
      Dim SelectedPrices() As DSPrice.tblPriceRow = Nothing
      Dim CacheKey As ULong = CULng(InstrumentID) Or (CULng(PriceDate.Date.ToOADate) << 32)

      If (CDate(thisPriceRow("PriceDate")) <= PriceDate) Then
        Return thisPriceRow
      Else

        ' Check this cache
        If (PriceRowCache.TryGetValue(CacheKey, thisPriceRow)) Then
          Return thisPriceRow
        End If


        ' Use the Prices table, if it is loaded, else use a separate query.

        PriceDS = MainForm.MainDataHandler.Get_Dataset(RenaissanceStandardDatasets.GetStandardDataset(RenaissanceChangeID.tblPrice).DatasetName)

        If (PriceDS IsNot Nothing) Then
          SelectedPrices = PriceDS.tblPrice.Select("(PriceInstrument=" & InstrumentID.ToString("###0") & ") AND (PriceDate<=#" & PriceDate.ToString(QUERY_SHORTDATEFORMAT) & "#)", "PriceDate DESC")

          If (SelectedPrices IsNot Nothing) AndAlso (SelectedPrices.Length > 0) Then
            PriceRowCache.Add(CacheKey, SelectedPrices(0))
            Return SelectedPrices(0)
            Exit Function
          End If

        Else
          ' Specific query.

          'ALTER PROCEDURE [dbo].[adp_tblPrice_BestRow]
          '  (
          '   @InstrumentID int = 0,
          '   @ValueDate datetime = '1900-01-01',
          '   @OnlyFinalPrices int = 0, 
          '   @KnowledgeDate datetime = Null
          '  )

          Dim SelectCommand As New SqlCommand
          Dim PricesTable As RenaissanceDataClass.DSPrice.tblPriceDataTable = Nothing

          Try
            PricesTable = New RenaissanceDataClass.DSPrice.tblPriceDataTable

            SelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT
            SelectCommand.Connection = MainForm.GetVeniceConnection()
            SelectCommand.CommandType = CommandType.StoredProcedure
            SelectCommand.CommandText = "adp_tblPrice_BestRow"

            SelectCommand.Parameters.Add(New SqlParameter("@InstrumentID", SqlDbType.Int)).Value = InstrumentID
            SelectCommand.Parameters.Add(New SqlParameter("@ValueDate", SqlDbType.DateTime)).Value = PriceDate
            SelectCommand.Parameters.Add(New SqlParameter("@OnlyFinalPrices", SqlDbType.Int)).Value = 0
            SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", SqlDbType.DateTime)).Value = MainForm.Main_Knowledgedate

            MainForm.LoadTable_Custom(PricesTable, SelectCommand)
            'PricesTable.Load(SelectCommand.ExecuteReader)

          Catch ex As Exception
          Finally
            If (SelectCommand.Connection IsNot Nothing) AndAlso (SelectCommand.Connection.State And ConnectionState.Open) Then
              SelectCommand.Connection.Close()
            End If
            SelectCommand.Connection = Nothing
          End Try

          If (PricesTable IsNot Nothing) AndAlso (PricesTable.Rows.Count > 0) Then
            PriceRowCache.Add(CacheKey, PricesTable.Rows(0))
            Return PricesTable.Rows(0)
          End If

        End If

      End If

    Catch ex As Exception
    End Try

    Return Nothing
  End Function

  ''' <summary>
  ''' Gets the instrument price.
  ''' </summary>
  ''' <param name="InstrumentID">The instrument ID.</param>
  ''' <returns>System.Double.</returns>
  Private Function GetInstrumentPrice(ByVal InstrumentID As Integer) As Double
    ' ******************************************************************************************************
    '
    ' Return the Latest Price for a given Instrument from the sorted array of
    ' Instrument prices.
    '
    ' ******************************************************************************************************

    Static LastInstrumentID As Integer = 0
    Static LastInstrumentPrice As Double = 0

    Try
      If (InstrumentID = LastInstrumentID) OrElse (InstrumentID <= 0) Then
        If (InstrumentID <= 0) Then ' Facilitate the explicit clearing of Static variables
          LastInstrumentID = 0
          LastInstrumentPrice = 0
        End If

        Return LastInstrumentPrice
      End If
    Catch ex As Exception
    End Try

    Try

      Dim thisPriceRow As DataRow = GetBestPriceRow(InstrumentID)

      If (thisPriceRow Is Nothing) Then
        LastInstrumentID = 0
        LastInstrumentPrice = 0.0#
        Return 0.0#
      Else
        LastInstrumentID = InstrumentID
        LastInstrumentPrice = CDbl(thisPriceRow("PriceLevel"))
        Return LastInstrumentPrice
      End If

    Catch ex As Exception
      LastInstrumentID = 0
      LastInstrumentPrice = 0
      Return 0
      Exit Function
    End Try

  End Function

  ''' <summary>
  ''' Gets the FX to fund.
  ''' </summary>
  ''' <param name="InstrumentCurrencyID">The instrument currency ID.</param>
  ''' <param name="FundCurrencyID">The fund currency ID.</param>
  ''' <returns>System.Double.</returns>
  Friend Function GetFXToFund(ByVal InstrumentCurrencyID As Integer, ByVal FundCurrencyID As Integer) As Double
    ' *******************************************************************************
    '
    ' *******************************************************************************
    Dim RVal As Double = 1.0#

    Try
      Dim InstrumentFX As Double = 1.0#
      Dim FundFX As Double = 1.0#

      If (InstrumentCurrencyID = FundCurrencyID) Then
        Return 1.0#
      End If

      If (FXMap IsNot Nothing) Then

        If (FXMap.ContainsKey(InstrumentCurrencyID)) Then
          InstrumentFX = FXMap(InstrumentCurrencyID)
        End If

        If (FXMap.ContainsKey(FundCurrencyID)) Then
          FundFX = FXMap(FundCurrencyID)
        End If

        RVal = InstrumentFX / FundFX

      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in GetFXToFund()", ex.StackTrace, True)
    End Try

    Return RVal

  End Function

  ''' <summary>
  ''' Gets the FX rates.
  ''' </summary>
  ''' <param name="ValueDate">The value date.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function getFXRates(ByVal ValueDate As Date) As Boolean
    ' *******************************************************************************
    '
    ' *******************************************************************************
    Dim FXTable As New DataTable
    Dim FXRow As DataRow
    Dim DBConnection As SqlConnection = Nothing
    Dim ThisCurrency As Integer

    Try
      If (ValueDate <= RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW) Then
        ValueDate = Now.Date
      End If

      If (FXMap Is Nothing) Then
        FXMap = New Dictionary(Of Integer, Double)
      End If

      FXMap.Clear()

      Dim thisSelectCommand As New SqlCommand

      Try
        DBConnection = MainForm.GetVeniceConnection()


        thisSelectCommand.Connection = MainForm.GetVeniceConnection()
        thisSelectCommand.CommandType = CommandType.StoredProcedure
        thisSelectCommand.CommandText = "adp_tblBestFX_SelectCommand"
        thisSelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT
        thisSelectCommand.Parameters.Add(New SqlParameter("@ValueDate", SqlDbType.DateTime)).Value = ValueDate
        thisSelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", SqlDbType.DateTime)).Value = MainForm.Main_Knowledgedate

        MainForm.LoadTable_Custom(FXTable, thisSelectCommand)
        'FXTable.Load(thisSelectCommand.ExecuteReader)

      Catch ex As Exception
        MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error loading Sub-Fund Tree in getFXRates().", ex.StackTrace, True)

      Finally
        If (DBConnection IsNot Nothing) AndAlso (DBConnection.State And ConnectionState.Open) Then
          DBConnection.Close()
        End If
        thisSelectCommand.Connection = Nothing
      End Try

      If (FXTable IsNot Nothing) AndAlso (FXTable.Rows.Count > 0) Then
        For Each FXRow In FXTable.Rows
          If (IsNumeric(FXRow("FXCurrencyCode"))) Then
            ThisCurrency = CInt(FXRow("FXCurrencyCode"))

            If (FXMap.ContainsKey(ThisCurrency) = False) Then
              FXMap.Add(ThisCurrency, CDbl(FXRow("FXRate")))
            End If
          End If
        Next
      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in getFXRates()", ex.StackTrace, True)
    End Try

    Return True

  End Function


#End Region

#Region " Set Form Combos (Form Specific Code) "

  ''' <summary>
  ''' Sets the fund combo.
  ''' </summary>
  Private Sub SetFundCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_Fund, _
    RenaissanceStandardDatasets.tblFund, _
    "FundName", _
    "FundID", _
    "FundClosed=0", False, True, True)   ' 

  End Sub

  ''' <summary>
  ''' Sets the transaction group combo.
  ''' </summary>
  Private Sub SetTransactionGroupCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_TransactionGroup, _
    RenaissanceStandardDatasets.tblSophisStatusGroups, _
    "GROUP_NAME", _
    "GROUP_NAME", _
    "", True, True, True)   ' 

  End Sub

  ''' <summary>
  ''' Sets the trade status combo.
  ''' </summary>
  Private Sub SetTradeStatusCombo()
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_TradeStatus, _
    RenaissanceStandardDatasets.tblTradeStatus, _
    "tradeStatusName", _
    "tradeStatusID", _
    "", False, True, True, 0)   ' 

  End Sub

  ''' <summary>
  ''' Sets the un realised calculation type combo.
  ''' </summary>
  Private Sub SetUnRealisedCalculationTypeCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_UnRealisedCalculationType, _
    GetType(CalculationTypes_Profit))   ' 

  End Sub

  ''' <summary>
  ''' Sets the instrument combo.
  ''' </summary>
  Private Sub SetInstrumentCombo()

    ' Get Futures-Style Instrument Types, if not done already

    If (_FuturesInstrumentTypes.Length >= 0) Then
      Dim TypeDS As RenaissanceDataClass.DSInstrumentType = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblInstrumentType, False)

      Dim TypeRows() As RenaissanceDataClass.DSInstrumentType.tblInstrumentTypeRow = TypeDS.tblInstrumentType.Select("InstrumentTypeFuturesStylePricing<>0")
      Dim TypeRow As RenaissanceDataClass.DSInstrumentType.tblInstrumentTypeRow

      For Each TypeRow In TypeRows
        If (_FuturesInstrumentTypes.Length > 0) Then
          _FuturesInstrumentTypes &= ","
        End If
        _FuturesInstrumentTypes &= TypeRow.InstrumentTypeID
      Next

    End If

    ' Set Combo.

    Call MainForm.SetTblGenericCombo( _
     Me.Combo_FuturesInstrument, _
     RenaissanceStandardDatasets.tblInstrument, _
     "InstrumentDescription", _
     "InstrumentID", _
     "InstrumentType IN (" & _FuturesInstrumentTypes & ")", False, True, True)   ' 

    Call MainForm.SetTblGenericCombo( _
     Me.Combo_ManagementFeeInstrument, _
     RenaissanceStandardDatasets.tblInstrument, _
     "InstrumentDescription", _
     "InstrumentID", _
     "InstrumentIsManagementFee <> 0", False, True, True, 0)  ' 

    Call MainForm.SetTblGenericCombo( _
     Me.Combo_PerformanceFeeInstrument, _
     RenaissanceStandardDatasets.tblInstrument, _
     "InstrumentDescription", _
     "InstrumentID", _
     "InstrumentIsIncentiveFee <> 0", False, True, True, 0) ' 

    Call MainForm.SetTblGenericCombo( _
     Me.Combo_PerformanceFeeCrystalisedInstrument, _
     RenaissanceStandardDatasets.tblInstrument, _
     "InstrumentDescription", _
     "InstrumentID", _
     "InstrumentIsIncentiveFee <> 0", False, True, True, 0) ' 

  End Sub



#End Region

  ''' <summary>
  ''' Gets the best NAV.
  ''' </summary>
  ''' <param name="pInstrumentID">The p instrument ID.</param>
  ''' <param name="pValueDate">The p value date.</param>
  ''' <returns>System.Double.</returns>
  Private Function GetBestNAV(ByVal pInstrumentID As Integer, ByVal pValueDate As Date) As Double
    ' ****************************************************************************
    '
    '
    ' ****************************************************************************

    Dim myConnection As SqlConnection
    Dim QueryCommand As New SqlCommand
    Dim InstrumentNAV As Object

    Try

      myConnection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)

      QueryCommand.Connection = myConnection
      QueryCommand.CommandText = "SELECT dbo.fn_SingleInstrumentBestNAV(@InstrumentID, @ValueDate, 0, Null)"
      QueryCommand.Parameters.Clear()
      QueryCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@InstrumentID", System.Data.SqlDbType.Int, 4))
      QueryCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValueDate", System.Data.SqlDbType.DateTime, 8))
      QueryCommand.Parameters("@InstrumentID").Value = pInstrumentID
      QueryCommand.Parameters("@ValueDate").Value = pValueDate

      SyncLock QueryCommand.Connection
        InstrumentNAV = QueryCommand.ExecuteScalar
      End SyncLock

      If IsNumeric(InstrumentNAV) Then
        Return CDbl(InstrumentNAV)
      End If
    Catch ex As Exception
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error getting an Instrument Price.", ex.StackTrace, True)
    End Try

    Return 0
  End Function

  ''' <summary>
  ''' Handles the SelectedIndexChanged event of the TabControl_Reconciliation control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub TabControl_Reconciliation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabControl_Reconciliation.SelectedIndexChanged
    ' ************************************************************************************
    '
    '
    '
    ' ************************************************************************************

    Try
      RefreshCurrentTab(False, False)
    Catch ex As Exception
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error setting reconciliation tab.", ex.StackTrace, True)
    End Try

  End Sub

  ''' <summary>
  ''' Handles the Tick event of the Timer_Update control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Timer_Update_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer_Update.Tick
    ' ************************************************************************************
    ' OK, to stop excessive updating if a form control updates rapidly, I have implimented this
    ' Timed-Update process.
    '
    ' When an update is required, the 'Flag_RefreshTab' global is set to the number of miliseconds
    ' until update. When the counter hits zero, this routine performs the update.
    '
    ' The Timer 'Tag' property is used to convey the 'Force Update' flag from the usual event.
    ' ************************************************************************************

    Try
      ' Stops the timer on Disposed forms.

      If (Me.IsDisposed) OrElse (Me.Disposing) Then
        Timer_Update.Stop()
        Exit Sub
      End If

      If (Me.InUse = False) Then
        Exit Sub
      End If

      ' Process.

      ' Defer tick processing if we are in a 'RunDataReader' process.
      If (IsLoadingData) Then
        Exit Sub
      End If

      ' OK, Continue...
      If (Flag_RefreshTab > 0) Then

        Flag_RefreshTab = Math.Max(0, Flag_RefreshTab - Timer_Update.Interval)

        If Flag_RefreshTab <= 0 Then

          Try
            ' Use IsLoadingData to avoid nested data loading
            IsLoadingData = True

            Me.Combo_Fund.Enabled = False
            Me.Date_ValueDate.Enabled = False
            Me.Combo_TransactionGroup.Enabled = False
            Me.Date_FeesSince.Enabled = False
            Me.Check_AdministratorPriceDates.Enabled = False
            Me.Check_SR_IgnoreStatus.Enabled = False
            Me.Check_SR_IgnoreTodaysTrades.Enabled = False

            ' Code from the Original 'Update Tabs' function :-

            Dim LoadData As Boolean = CBool(Timer_Update.Tag)
            Timer_Update.Tag = False

            If (LoadData = False) Then
              If (TabDataStatus.ContainsKey(TabControl_Reconciliation.SelectedTab.Text)) Then
                LoadData = TabDataStatus(TabControl_Reconciliation.SelectedTab.Text)
              Else
                LoadData = True
              End If
            End If

            Try
              ' If this form has the focus, move it to the tab control. Attempts to stop accidental changing of the Fund Combo etc...
              If (Me.ActiveControl.Focused) Then
                TabControl_Reconciliation.Focus()
              End If

              If (TabControl_Reconciliation.SelectedTab Is Tab_Valuation) OrElse (TabControl_Reconciliation.SelectedTab Is Tab_FeesMultiClass) Then
                ' Combined Valuation and Multi-Class fees into the same event, this is so that the Valuation tab can reference the Multi-Class fees tab if necessary.
                ' There will of course be someimpact on responsiveness.

                Call SetFeesFields()

                If (TabControl_Reconciliation.TabPages.Contains(Tab_FeesMultiClass)) Then
                  Call PaintFeesByClassGrid(CInt(Combo_Fund.SelectedValue))
                End If

                Call SetSortedRows_Valuation(LoadData)

                'ElseIf (TabControl_Reconciliation.SelectedTab Is Tab_FeesMultiClass) Then

              ElseIf TabControl_Reconciliation.SelectedTab Is Tab_Notional Then

                Call SetSortedRows_Notional(LoadData)

              ElseIf (TabControl_Reconciliation.SelectedTab Is Tab_Fees) OrElse (TabControl_Reconciliation.SelectedTab Is Tab_Benchmark) Then

                Call SetFeesFields()
                Call SetSortedRows_Fees(LoadData)

              ElseIf TabControl_Reconciliation.SelectedTab Is Tab_Milestone Then

                ' Ensure the Fees are up to date.

                Call SetFeesFields()
                Call SetSortedRows_Fees(LoadData)

                ' Populate M/S fields.

                Call SetSortedRows_Milestone(LoadData)

              ElseIf TabControl_Reconciliation.SelectedTab Is Tab_Subscriptions Then

                Call SetSortedRows_Subscriptions(LoadData)

              ElseIf TabControl_Reconciliation.SelectedTab Is Tab_AutomatedFees Then

                Call SetSortedRows_AutomatedFees()

              ElseIf TabControl_Reconciliation.SelectedTab Is Tab_Availabilities Then

                Call SetSortedRows_Availabilities(LoadData)

              End If

              TabDataStatus(TabControl_Reconciliation.SelectedTab.Text) = False

            Catch ex As Exception
            End Try

          Catch ex As Exception
          Finally
            Me.Combo_Fund.Enabled = True
            Me.Date_ValueDate.Enabled = True
            Me.Combo_TransactionGroup.Enabled = True
            Me.Date_FeesSince.Enabled = True
            Me.Check_AdministratorPriceDates.Enabled = True
            Me.Check_SR_IgnoreStatus.Enabled = True
            Me.Check_SR_IgnoreTodaysTrades.Enabled = True

            IsLoadingData = False
          End Try

        End If
      End If

    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Refreshes the current tab.
  ''' </summary>
  ''' <param name="FormControlChanged">if set to <c>true</c> [form control changed].</param>
  ''' <param name="ForceRefresh">if set to <c>true</c> [force refresh].</param>
  Private Sub RefreshCurrentTab(ByVal FormControlChanged As Boolean, ByVal ForceRefresh As Boolean)

    ' ************************************************************************************
    '
    '
    '
    ' ************************************************************************************

    Try

      Timer_Update.Tag = CBool(ForceRefresh Or Timer_Update.Tag)

      If (ForceRefresh) Then
        Flag_RefreshTab = 100
      Else
        Flag_RefreshTab = 200
      End If

    Catch ex As Exception
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in RefreshCurrentTab().", ex.StackTrace, True)
    End Try

  End Sub

  ''' <summary>
  ''' Gets the availabilities.
  ''' </summary>
  ''' <param name="thisLockObject">The this lock object.</param>
  ''' <param name="FundID">The fund ID.</param>
  ''' <param name="NAVDate">The NAV date.</param>
  ''' <returns>DSRecAvailabilities.tblRecAvailabilitiesDataTable.</returns>
  Private Function GetAvailabilities(ByVal thisLockObject As ReaderWriterLockSlim, ByVal FundID As Integer, ByVal NAVDate As Date) As DSRecAvailabilities.tblRecAvailabilitiesDataTable
    ' ************************************************************************************
    '
    '
    '
    ' ************************************************************************************

    Dim RVal As DSRecAvailabilities.tblRecAvailabilitiesDataTable = New DSRecAvailabilities.tblRecAvailabilitiesDataTable
    Dim FundCode As String
    Dim AdaptorAvailabilities As New SqlDataAdapter
    Dim myAvailabilitiesConnection As SqlConnection = Nothing

    Try

      If (Not thisLockObject.IsWriteLockHeld) Then
        thisLockObject.EnterWriteLock()
      End If

      Try

        FundCode = CStr(Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblFund, FundID, "FundAdministratorCode"), ""))

        myAvailabilitiesConnection = MainForm.GetVeniceConnection()
        Call MainForm.MainAdaptorHandler.Set_AdaptorCommands(myAvailabilitiesConnection, AdaptorAvailabilities, "tblRecAvailabilitites")

        AdaptorAvailabilities.SelectCommand.Parameters("@MaxCaptureID").Value = 0
        AdaptorAvailabilities.SelectCommand.Parameters("@NavDate").Value = NAVDate
        AdaptorAvailabilities.SelectCommand.Parameters("@FundCode").Value = FundCode
        AdaptorAvailabilities.SelectCommand.Parameters("@Currency").Value = ""
        AdaptorAvailabilities.SelectCommand.Parameters("@Description").Value = ""
        AdaptorAvailabilities.SelectCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate

        MainForm.SetToolStripText(StatusLabel, "Loading Availabilities Data, Fund " & FundCode.ToString & ", " & NAVDate.ToString("dd MMM yyyy"))

        If (FundCode <> "") Then
          SyncLock AdaptorAvailabilities.SelectCommand.Connection
            SyncLock AdaptorAvailabilities
              AdaptorAvailabilities.Fill(RVal)
            End SyncLock
          End SyncLock
        End If

      Catch ex As Exception
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in GetAvailabilities().", ex.StackTrace, True)

      Finally
        If (AdaptorAvailabilities.SelectCommand IsNot Nothing) Then
          AdaptorAvailabilities.SelectCommand.Connection = Nothing
          AdaptorAvailabilities.SelectCommand.Parameters.Clear()
        End If
        If (myAvailabilitiesConnection IsNot Nothing) AndAlso (myAvailabilitiesConnection.State And ConnectionState.Open) Then
          myAvailabilitiesConnection.Close()
        End If
        MainForm.SetToolStripText(StatusLabel, "")
      End Try
    Catch ex As Exception
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in GetAvailabilities().", ex.StackTrace, True)
    Finally
      If (thisLockObject.IsWriteLockHeld) Then
        thisLockObject.ExitWriteLock()
      End If
    End Try


    Return RVal

  End Function

#Region " Valuation Tab"

  ''' <summary>
  ''' Handles the KeyDown event of the Grid_Valuations control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Valuations_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Grid_Valuations.KeyDown
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        Try
          If e.Control And (e.KeyCode = Keys.C) Then
            Call MainForm.CopyGridSelection(CType(sender, C1.Win.C1FlexGrid.C1FlexGrid), True, False, False)
          End If
        Catch ex As Exception
        End Try

      End If
    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Handles the MouseEnterCell event of the Grid_Valuations control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="C1.Win.C1FlexGrid.RowColEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Valuations_MouseEnterCell(ByVal sender As System.Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles Grid_Valuations.MouseEnterCell
    ' **********************************************************************************
    ' Record cell just entered and re-arrange the context menu according to the type
    ' of Grid Row.
    ' **********************************************************************************

    Try
      LastEnteredValuationGridRow = e.Row
      LastEnteredValuationGridCol = e.Col

      Dim enableAcceptOptions As Boolean = False
      Dim enableViewTransactions As Boolean = False

      If (e.Row <= 0) Then
        Grid_Valuations.ContextMenuStrip = Nothing
      Else
        Dim thisRow As C1.Win.C1FlexGrid.Row = Grid_Valuations.Rows(LastEnteredValuationGridRow)

        If (Grid_Valuations.ContextMenuStrip Is Nothing) Then
          Grid_Valuations.ContextMenuStrip = Me.ContextMenu_Valuation
        End If

        If (thisRow.IsNode) Then
          If CStr(Nz(thisRow("col_InstrumentDescription"), "")).Length > 0 Then
            Menu_Valuation_Description.Text = thisRow("col_InstrumentDescription")
          Else
            Menu_Valuation_Description.Text = thisRow("colInstrumentType")
          End If

          If (thisRow.Node.Level <= 0) Then
            ' Node Level = 0 : Primonial

            Grid_Valuations.ContextMenuStrip = Nothing

          ElseIf (thisRow.Node.Level <= 1) Then
            ' Node Level = 1 : Instrument Type

            Grid_Valuations.ContextMenuStrip = Nothing

          Else
            ' Node Level = 2 : Transactions

            Grid_Valuations.ContextMenuStrip = Nothing

          End If
        Else
          ' Not a Node : Instrument

          Menu_Valuation_Description.Text = thisRow("col_InstrumentDescription")

          If (thisRow("colInstrument") > 0) Then


            enableViewTransactions = True

            If (thisRow("colCaptureID") > 0) Then

              enableAcceptOptions = True

            End If

          End If

        End If
      End If

      ' Show / Hide menu options.

      Menu_Valuation_AcceptPrice.Visible = enableAcceptOptions
      Menu_Valuation_AcceptFXRate.Visible = enableAcceptOptions
      'Menu_Valuation_Separator.Visible = (enableAcceptOptions And enableViewTransactions)

      Menu_Valuation_ViewTransactions.Visible = enableViewTransactions

    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Handles the Click event of the Menu_Valuation_AcceptPrice control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Menu_Valuation_AcceptPrice_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_Valuation_AcceptPrice.Click
    ' **********************************************************************************
    ' 
    '
    ' **********************************************************************************

    Try
      If (LastEnteredValuationGridRow > 0) And (LastEnteredValuationGridRow < Grid_Valuations.Rows.Count) Then

        ' More_Code_Here()

        Dim ThisPrice As Double
        Dim ThisInstrument As Integer
        Dim PriceDate As Date = Date_ValueDate.Value

        If IsNumeric(Grid_Valuations.GetData(LastEnteredValuationGridRow, Grid_Valuations.Cols("colPriceBNP").SafeIndex)) Then
          ThisPrice = CDbl(Grid_Valuations.GetData(LastEnteredValuationGridRow, Grid_Valuations.Cols("colPriceBNP").SafeIndex))
        Else
          Exit Sub
        End If

        If IsNumeric(Grid_Valuations.GetData(LastEnteredValuationGridRow, Grid_Valuations.Cols("colInstrument").SafeIndex)) Then
          ThisInstrument = CInt(Grid_Valuations.GetData(LastEnteredValuationGridRow, Grid_Valuations.Cols("colInstrument").SafeIndex))
        Else
          Exit Sub
        End If

        If IsDate(Grid_Valuations.GetData(LastEnteredValuationGridRow, Grid_Valuations.Cols("colDateBNP").SafeIndex)) Then
          PriceDate = CDate(Grid_Valuations.GetData(LastEnteredValuationGridRow, Grid_Valuations.Cols("colDateBNP").SafeIndex))
        End If

        If (ThisInstrument > 0.0#) Then

          Dim InstrumentType As RenaissanceGlobals.InstrumentTypes

          InstrumentType = CType(CInt(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, ThisInstrument, "InstrumentType")), RenaissanceGlobals.InstrumentTypes)

          If (InstrumentType <> InstrumentTypes.Cash) Then
            If MessageBox.Show("Accept BNP Price of " & ThisPrice.ToString(RenaissanceGlobals.Globals.DISPLAYMEMBER_DOUBLEFORMAT) & vbCrLf & "for Instrument " & Grid_Valuations.GetData(LastEnteredValuationGridRow, Grid_Valuations.Cols("col_InstrumentDescription").SafeIndex).ToString & " ?", "Set Venice Price", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.OK Then
              ' 
              ' Save Price

              SavePrice(MainForm, ThisInstrument, PriceDate, ThisPrice, False, 0, PriceDate, 1, "Price Saved from Fund Reconciliation")

              ' Always Re-Load the Prices table since a price cascade may have altered other prices.
              Me.MainForm.ReloadTable_Background(RenaissanceStandardDatasets.tblPrice.ChangeID, "", True)

              ' Propagate changes
              'Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblPrice))

            End If
          End If
        End If

      End If

    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Handles the Click event of the Menu_Valuation_AcceptFX control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Menu_Valuation_AcceptFX_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_Valuation_AcceptFXRate.Click
    ' **********************************************************************************
    ' 
    '
    ' **********************************************************************************

    Try

      Call BookAdministratorFXs()

    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Handles the Click event of the Menu_Valuation_AcceptFX control.
  ''' Display frlPriceGrid for the current Instrument.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Menu_Valuation_ViewPricesForm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_Valuation_ViewPricesForm.Click
    ' **********************************************************************************
    ' 
    '
    ' **********************************************************************************

    Try
      If (LastEnteredValuationGridRow > 0) And (LastEnteredValuationGridRow < Grid_Valuations.Rows.Count) Then
        Dim ThisInstrument As Integer = 0
        Dim thisFormHandle As RenaissanceGlobals.RenaissanceFormHandle

        If IsNumeric(Grid_Valuations.GetData(LastEnteredValuationGridRow, Grid_Valuations.Cols("colInstrument").SafeIndex)) Then
          ThisInstrument = CInt(Grid_Valuations.GetData(LastEnteredValuationGridRow, Grid_Valuations.Cols("colInstrument").SafeIndex))
        Else
          Exit Sub
        End If

        thisFormHandle = MainForm.New_VeniceForm(VeniceFormID.frmPriceGrid)
        CType(thisFormHandle.Form, frmPriceGrid).SetCurrentInstrument(ThisInstrument, True)

      End If

    Catch ex As Exception
    End Try
  End Sub
  ''' <summary>
  ''' Handles the Click event of the Menu_Valuation_ViewTransactions control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Menu_Valuation_ViewTransactions_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_Valuation_ViewTransactions.Click
    ' **********************************************************************************
    ' 
    '
    ' **********************************************************************************

    Try
      If (LastEnteredValuationGridRow > 0) And (LastEnteredValuationGridRow < Grid_Valuations.Rows.Count) Then
        Dim ThisFundID As Integer = 0
        Dim ThisInstrument As Integer = 0
        Dim InstrumentType As InstrumentTypes
        Dim thisFormHandle As RenaissanceGlobals.RenaissanceFormHandle

        If IsNumeric(Combo_Fund.SelectedValue) Then
          ThisFundID = CInt(Combo_Fund.SelectedValue)
        End If

        If IsNumeric(Grid_Valuations.GetData(LastEnteredValuationGridRow, Grid_Valuations.Cols("colInstrument").SafeIndex)) Then
          ThisInstrument = CInt(Grid_Valuations.GetData(LastEnteredValuationGridRow, Grid_Valuations.Cols("colInstrument").SafeIndex))
        Else
          Exit Sub
        End If


        InstrumentType = CType(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, ThisInstrument, "InstrumentType"), InstrumentTypes)

        Select Case InstrumentType

          Case InstrumentTypes.Future, InstrumentTypes.Option

            thisFormHandle = MainForm.New_VeniceForm(VeniceFormID.frmUpdateTransactions)
            CType(thisFormHandle.Form, frmUpdateTransactions).SetFormSelectCriteria(ThisFundID, ThisInstrument)

          Case Else

            thisFormHandle = MainForm.New_VeniceForm(VeniceFormID.frmSelectTransaction)
            CType(thisFormHandle.Form, frmSelectTransaction).SetFormSelectCriteria(ThisFundID, ThisInstrument, Date_ValueDate.Value.AddDays(-1))

        End Select

      End If

    Catch ex As Exception
    End Try
  End Sub

  Private Sub Menu_Valuation_ViewInstrument_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_Valuation_ViewInstrument.Click
    ' **********************************************************************************
    ' 
    '
    ' **********************************************************************************

    Try
      If (LastEnteredValuationGridRow > 0) And (LastEnteredValuationGridRow < Grid_Valuations.Rows.Count) Then
        Dim ThisFundID As Integer = 0
        Dim ThisInstrument As Integer = 0
        Dim thisFormHandle As RenaissanceGlobals.RenaissanceFormHandle

        If IsNumeric(Combo_Fund.SelectedValue) Then
          ThisFundID = CInt(Combo_Fund.SelectedValue)
        End If

        If IsNumeric(Grid_Valuations.GetData(LastEnteredValuationGridRow, Grid_Valuations.Cols("colInstrument").SafeIndex)) Then
          ThisInstrument = CInt(Grid_Valuations.GetData(LastEnteredValuationGridRow, Grid_Valuations.Cols("colInstrument").SafeIndex))
        Else
          Exit Sub
        End If

        If (ThisInstrument > 0) Then

          thisFormHandle = MainForm.New_VeniceForm(VeniceFormID.frmInstrument)

          CType(thisFormHandle.Form, frmInstrument).MoveToAuditID(ThisInstrument)

        End If

      End If

    Catch ex As Exception
    End Try
  End Sub

#End Region

#Region " Futures Tab"

  ''' <summary>
  ''' Handles the KeyDown event of the Grid_NotionalExposure control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_NotionalExposure_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Grid_NotionalExposure.KeyDown
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        Try
          If e.Control And (e.KeyCode = Keys.C) Then
            Call MainForm.CopyGridSelection(CType(sender, C1.Win.C1FlexGrid.C1FlexGrid), True, False, True)
          End If
        Catch ex As Exception
        End Try

      End If
    Catch ex As Exception
    End Try

  End Sub

#End Region

#Region " Availabilities Tab"

  ''' <summary>
  ''' Handles the Click event of the Button_BookRetrocessions control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Button_BookRetrocessions_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_BookRetrocessions.Click
    ' *******************************************************************************
    '
    '
    ' *******************************************************************************
    Dim UpdateString As String = ""

    Try
      Dim ThisGridRow As C1.Win.C1FlexGrid.Row = Nothing
      Dim Counter As Integer
      Dim thisDescription As String
      Dim ThisCurrency As String
      Dim FeesToBook As Double
      Dim AdminBalance As Double = 0.0#
      Dim VeniceBalance As Double = 0.0#
      Dim RetrocessionInstrument As Integer
      Dim RetrocessionTotals As New Dictionary(Of String, Double)

      Button_BookRetrocessions.Enabled = False

      Dim col_First As Integer = Grid_Retrocessions.Cols("FirstCol").SafeIndex
      Dim col_Currency As Integer = Grid_Retrocessions.Cols("col_Currency").SafeIndex
      Dim col_AdminBalanceLocal As Integer = Grid_Retrocessions.Cols("col_AdminBalanceLocal").SafeIndex
      Dim col_VeniceBalanceLocal As Integer = Grid_Retrocessions.Cols("col_VeniceBalanceLocal").SafeIndex

      For Counter = 1 To (Grid_Retrocessions.Rows.Count - 1)
        ThisGridRow = Grid_Retrocessions.Rows(Counter)

        If (ThisGridRow.IsNode = False) Then
          thisDescription = CStr(ThisGridRow(col_First)).ToUpper
          ThisCurrency = ThisGridRow(col_Currency)

          If (thisDescription.StartsWith("REC.TRAILER FEES ")) Then
            thisDescription = "REC.TRAILER FEES ALL"
          End If

          If (thisDescription = "FEES RETROCESSION") OrElse (thisDescription = "REC.TRAILER FEES ALL") Then
            If (IsNumeric(ThisGridRow(col_AdminBalanceLocal))) Then
              AdminBalance = CDbl(ThisGridRow(col_AdminBalanceLocal))
            End If
            If (IsNumeric(ThisGridRow(col_VeniceBalanceLocal))) Then
              VeniceBalance = CDbl(ThisGridRow(col_VeniceBalanceLocal))
            End If

            ' This figure is a credit at positive, so reverse sign....
            FeesToBook = VeniceBalance - AdminBalance

            If (RetrocessionTotals.ContainsKey(ThisCurrency)) Then
              RetrocessionTotals(ThisCurrency) += FeesToBook
            Else
              RetrocessionTotals.Add(ThisCurrency, FeesToBook)
            End If

            'If (Math.Abs(FeesToBook) >= 0.1#) Then
            '  RetrocessionInstrument = Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblCurrency, 0, "DefaultRetrocessionInstrument", "RN", "CurrencyCode='" & ThisCurrency & "'"), 0)

            '  If (RetrocessionInstrument > 0) Then
            '    ' Book as Provision, not Fee - See BookFees() parameters.
            '    UpdateString &= BookFees(CInt(Combo_Fund.SelectedValue), CInt(Combo_Fund.SelectedValue), RetrocessionInstrument, FeesToBook, Date_ValueDate.Value, Date_MgmtFeesSettlementDate.Value, False, True, True, True, False) & ","
            '  End If
            'End If
          End If

        End If

      Next

      If (RetrocessionTotals.Count > 0) Then
        For Each ThisCurrency In RetrocessionTotals.Keys
          FeesToBook = RetrocessionTotals(ThisCurrency)

          If (Math.Abs(FeesToBook) >= 0.1#) Then
            RetrocessionInstrument = Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblCurrency, 0, "DefaultRetrocessionInstrument", "RN", "CurrencyCode='" & ThisCurrency & "'"), 0)

            If (RetrocessionInstrument > 0) Then
              ' Book as Provision, not Fee - See BookFees() parameters.
              ' Booked as Not-Share-Class-Specific
              UpdateString &= BookFees(0, 0, CInt(Combo_Fund.SelectedValue), CInt(Combo_Fund.SelectedValue), RetrocessionInstrument, FeesToBook, Date_ValueDate.Value, Date_MgmtFeesSettlementDate.Value, False, True, True, True, False, 0, "") & ","
            End If
          End If

        Next
      End If


    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error saving Retrocession Fee Transaction.", ex.StackTrace, True)
    Finally
      Button_BookRetrocessions.Enabled = True

      Try
        If (UpdateString.Length > 0) Then
          MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceStandardDatasets.tblTransaction.ChangeID, UpdateString), True)
        End If
      Catch ex As Exception
      End Try

    End Try
  End Sub

  ''' <summary>
  ''' Handles the Click event of the Button_BookMarginDeposits control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Button_BookMarginDeposits_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_BookMarginDeposits.Click
    ' *******************************************************************************
    '
    '
    ' *******************************************************************************
    Dim UpdateString As String = ""

    Try
      Dim ThisGridRow As C1.Win.C1FlexGrid.Row = Nothing
      Dim Counter As Integer
      Dim thisDescription As String
      Dim ThisCurrency As String
      Dim FeesToBook As Double
      Dim AdminBalance As Double = 0.0#
      Dim VeniceBalance As Double = 0.0#
      Dim InitialMarginInstrument As Integer

      Button_BookMarginDeposits.Enabled = False

      Dim col_First As Integer = Grid_Retrocessions.Cols("FirstCol").SafeIndex
      Dim col_Currency As Integer = Grid_Retrocessions.Cols("col_Currency").SafeIndex
      Dim col_AdminBalanceLocal As Integer = Grid_Retrocessions.Cols("col_AdminBalanceLocal").SafeIndex
      Dim col_VeniceBalanceLocal As Integer = Grid_Retrocessions.Cols("col_VeniceBalanceLocal").SafeIndex

      For Counter = 1 To (Grid_Retrocessions.Rows.Count - 1)
        ThisGridRow = Grid_Retrocessions.Rows(Counter)

        If (ThisGridRow.IsNode = False) Then
          thisDescription = ThisGridRow(col_First)
          ThisCurrency = ThisGridRow(col_Currency)

          If (thisDescription.ToUpper = "CASH DEPOSIT") OrElse (thisDescription.ToUpper = "DEPOTS DE GARANTIE") Then
            If (IsNumeric(ThisGridRow(col_AdminBalanceLocal))) Then
              AdminBalance = CDbl(ThisGridRow(col_AdminBalanceLocal))
            End If
            If (IsNumeric(ThisGridRow(col_VeniceBalanceLocal))) Then
              VeniceBalance = CDbl(ThisGridRow(col_VeniceBalanceLocal))
            End If

            ' This figure is a credit at positive, so reverse sign....
            FeesToBook = AdminBalance - VeniceBalance

            If (Math.Abs(FeesToBook) >= 0.1#) Then
              InitialMarginInstrument = Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblCurrency, 0, "DefaultInitialMarginInstrument", "RN", "CurrencyCode='" & ThisCurrency & "'"), 0)

              If (InitialMarginInstrument > 0) Then
                ' Book as Fee, not Provision - See BookFees() parameters.
                ' Booked as Not-Share-Class-Specific
                UpdateString &= BookFees(0, 0, CInt(Combo_Fund.SelectedValue), CInt(Combo_Fund.SelectedValue), InitialMarginInstrument, FeesToBook, Date_ValueDate.Value, Date_ValueDate.Value, False, True, False, False, True, 0, "") & ","
              End If
            End If
          End If

        End If

      Next

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error saving InitialMargin Transaction.", ex.StackTrace, True)
    Finally
      Button_BookMarginDeposits.Enabled = True

      Try
        If (UpdateString.Length > 0) Then
          MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceStandardDatasets.tblTransaction.ChangeID, UpdateString), True)
        End If
      Catch ex As Exception
      End Try

    End Try
  End Sub

  ''' <summary>
  ''' Handles the KeyDown event of the Grid_Retrocessions control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Retrocessions_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Grid_Retrocessions.KeyDown
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        Try
          If e.Control And (e.KeyCode = Keys.C) Then
            Call MainForm.CopyGridSelection(CType(sender, C1.Win.C1FlexGrid.C1FlexGrid), True, False, True)
          End If
        Catch ex As Exception
        End Try

      End If
    Catch ex As Exception
    End Try
  End Sub

#End Region

#Region " Fees Tab"


  ''' <summary>
  ''' Handles the Click event of the Button_TradeManagementFees control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Button_TradeManagementFees_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_TradeManagementFees.Click
    ' *******************************************************************************
    '
    '
    ' *******************************************************************************

    Try
      Button_TradeManagementFees.Enabled = False

      ' Check permissions.
      Dim Permissions As Integer = MainForm.CheckPermissions(Me.Name(), PermissionFeatureType.TypeForm)

      If ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) = 0) AndAlso ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) = 0) Then
        MessageBox.Show("You do not have Permissions to change transactions.")
        Exit Sub
      End If

      Dim FundFees As FundFeeCalculator.FundFeeDetails
      Dim FundValuation As FundFeeCalculator.FundValuationDetails
      Dim FeesToTake As Double = 0.0#
      Dim FeeDescription As String = ""
      Dim FundUnitID As Integer = 0
      Dim FundID As Integer = CInt(Nz(Combo_Fund.SelectedValue, 0))
      Dim FundRow As DSFund.tblFundRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, FundID)

      If (FundRow IsNot Nothing) Then
        FundUnitID = FundRow.FundUnit
      End If

      If (FeesObject IsNot Nothing) Then

        FundValuation = FeesObject.GetFundValuation()
        FundFees = FeesObject.GetFundFees()

        If (FundFees IsNot Nothing) AndAlso (FundValuation IsNot Nothing) AndAlso IsNumeric(edit_ManagementFeesToTake.Text) Then

          FeesToTake = Math.Round(CDbl(edit_ManagementFeesToTake.Text) + FundValuation.PendingManagementFees, 2)

        End If

      End If

      Call BookFees(0, 0, FundID, FundID, CInt(Combo_ManagementFeeInstrument.SelectedValue), FeesToTake, Date_ValueDate.Value, Date_MgmtFeesSettlementDate.Value, False, False, True, True, False, FundUnitID, "")


    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error saving Management Fee Transaction.", ex.StackTrace, True)
    Finally
      Button_TradeManagementFees.Enabled = True
    End Try
  End Sub

  ''' <summary>
  ''' Handles the Click event of the Button_TradePerformanceFees control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Button_TradePerformanceFees_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_TradePerformanceFees.Click
    ' *******************************************************************************
    '
    '
    ' *******************************************************************************

    Try
      Button_TradePerformanceFees.Enabled = False

      ' Check permissions.
      Dim Permissions As Integer = MainForm.CheckPermissions(Me.Name(), PermissionFeatureType.TypeForm)

      If ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) = 0) AndAlso ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) = 0) Then
        MessageBox.Show("You do not have Permissions to change transactions.")
        Exit Sub
      End If

      Dim FundFees As FundFeeCalculator.FundFeeDetails
      Dim FundValuation As FundFeeCalculator.FundValuationDetails
      Dim FeesToTake As Double = 0.0#
      Dim FeeDescription As String = ""

      Dim FundUnitID As Integer = 0
      Dim FundID As Integer = CInt(Nz(Combo_Fund.SelectedValue, 0))
      Dim FundRow As DSFund.tblFundRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, FundID)

      If (FundRow IsNot Nothing) Then
        FundUnitID = FundRow.FundUnit
      End If

      If (FeesObject IsNot Nothing) Then

        FundValuation = FeesObject.GetFundValuation()
        FundFees = FeesObject.GetFundFees()

        If (FundFees IsNot Nothing) AndAlso (FundValuation IsNot Nothing) AndAlso IsNumeric(edit_ManagementFeesToTake.Text) Then

          FeesToTake = Math.Round(CDbl(CDbl(Numeric_PerfFeesToTake.Text) + (FundValuation.PreviousPerformanceFees + FundValuation.PendingPerformanceFees)), 2)

        End If

      End If

      Call BookFees(0, 0, FundID, FundID, CInt(Combo_PerformanceFeeInstrument.SelectedValue), FeesToTake, Date_ValueDate.Value, Date_PerfFeesSettlementDate.Value, False, False, True, False, False, FundUnitID, "")

      'Dim TransactionDS As DSTransaction
      'Dim TransactionTable As DSTransaction.tblTransactionDataTable
      'Dim newTransaction As DSTransaction.tblTransactionRow
      'Dim TransactionTableIsLoaded As Boolean = True

      '' Use the Transactions table, if it is loaded, else use a temporary one.

      'TransactionDS = MainForm.MainDataHandler.Get_Dataset(RenaissanceStandardDatasets.GetStandardDataset(RenaissanceChangeID.tblTransaction).DatasetName)
      'If (TransactionDS Is Nothing) Then
      '  TransactionDS = New DSTransaction
      '  TransactionTableIsLoaded = False
      'End If

      '' OK, Onwards.....

      'TransactionTable = TransactionDS.tblTransaction
      'newTransaction = TransactionTable.NewtblTransactionRow

      'newTransaction.TransactionFund = Combo_Fund.SelectedValue
      'newTransaction.TransactionSubFund = newTransaction.TransactionFund
      'newTransaction.TransactionInstrument = Combo_PerformanceFeeInstrument.SelectedValue
      'newTransaction.TransactionVersusInstrument = 0

      '' Check price of Fee instrument, will determine how to book the trade

      '' Non Zero priced instruments imply a trade vs provision approach rather than a trade vs cash approach implied by a zero price.

      'Dim FeePrice As Double = GetBestNAV(newTransaction.TransactionInstrument, Date_ValueDate.Value)

      'If Math.Abs(FeePrice) <= 0.0001# Then ' Eliminate rounding error, unlikely with a zero value !

      '  newTransaction.TransactionUnits = FeesToTake
      '  newTransaction.TransactionPrice = 1

      '  If CType(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, newTransaction.TransactionInstrument, "InstrumentType"), RenaissanceGlobals.InstrumentTypes) = RenaissanceGlobals.InstrumentTypes.Fee Then
      '    newTransaction.TransactionType = RenaissanceGlobals.TransactionTypes.Fee
      '  ElseIf CType(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, newTransaction.TransactionInstrument, "InstrumentType"), RenaissanceGlobals.InstrumentTypes) = RenaissanceGlobals.InstrumentTypes.Expense Then
      '    If (FeesToTake >= 0.0#) Then
      '      newTransaction.TransactionType = RenaissanceGlobals.TransactionTypes.Expense_Increase
      '    Else
      '      newTransaction.TransactionType = RenaissanceGlobals.TransactionTypes.Expense_Reduce
      '    End If
      '  Else
      '    If (newTransaction.TransactionUnits > 0) Then
      '      newTransaction.TransactionType = RenaissanceGlobals.TransactionTypes.Buy
      '    Else
      '      newTransaction.TransactionType = RenaissanceGlobals.TransactionTypes.Sell
      '    End If
      '    newTransaction.TransactionUnits = Math.Abs(newTransaction.TransactionUnits)
      '  End If

      '  FeeDescription = "Fees booked vs Cash" & vbCrLf & "Purchase vs Zero value for Fee Instrument."

      'ElseIf FeePrice > 0.0# Then

      '  ' To enact a fee, you must sell for zero, something with a positive value

      '  newTransaction.TransactionUnits = -FeesToTake / FeePrice
      '  newTransaction.TransactionPrice = 0
      '  FeeDescription = "Fees booked as provision." & vbCrLf & "Sale vs Positive value for Fee instrument."

      '  If CType(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, newTransaction.TransactionInstrument, "InstrumentType"), RenaissanceGlobals.InstrumentTypes) = RenaissanceGlobals.InstrumentTypes.Fee Then
      '    ' Fee
      '    newTransaction.TransactionType = RenaissanceGlobals.TransactionTypes.Fee

      '  ElseIf CType(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, newTransaction.TransactionInstrument, "InstrumentType"), RenaissanceGlobals.InstrumentTypes) = RenaissanceGlobals.InstrumentTypes.Expense Then
      '    ' Expense
      '    If (newTransaction.TransactionUnits >= 0.0#) Then
      '      newTransaction.TransactionType = RenaissanceGlobals.TransactionTypes.Expense_Increase
      '    Else
      '      newTransaction.TransactionType = RenaissanceGlobals.TransactionTypes.Expense_Reduce
      '    End If
      '    newTransaction.TransactionUnits = Math.Abs(newTransaction.TransactionUnits)

      '  Else
      '    ' Other
      '    If (newTransaction.TransactionUnits < 0.0#) Then
      '      newTransaction.TransactionType = RenaissanceGlobals.TransactionTypes.Provision_Reduce
      '    Else
      '      newTransaction.TransactionType = RenaissanceGlobals.TransactionTypes.Provision_Increase
      '    End If
      '    newTransaction.TransactionUnits = Math.Abs(newTransaction.TransactionUnits)
      '  End If


      'ElseIf FeePrice < 0.0# Then

      '  ' To enact a fee, you must buy for zero something with a negative value

      '  newTransaction.TransactionUnits = FeesToTake / FeePrice
      '  newTransaction.TransactionPrice = 0
      '  FeeDescription = "Fees booked as provision." & vbCrLf & "Purchase vs Negative value for Fee instrument."

      '  If CType(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, newTransaction.TransactionInstrument, "InstrumentType"), RenaissanceGlobals.InstrumentTypes) = RenaissanceGlobals.InstrumentTypes.Fee Then
      '    ' Fee
      '    newTransaction.TransactionType = RenaissanceGlobals.TransactionTypes.Fee

      '  ElseIf CType(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, newTransaction.TransactionInstrument, "InstrumentType"), RenaissanceGlobals.InstrumentTypes) = RenaissanceGlobals.InstrumentTypes.Expense Then
      '    ' Expense
      '    If (newTransaction.TransactionUnits >= 0.0#) Then
      '      newTransaction.TransactionType = RenaissanceGlobals.TransactionTypes.Expense_Increase
      '    Else
      '      newTransaction.TransactionType = RenaissanceGlobals.TransactionTypes.Expense_Reduce
      '    End If
      '    newTransaction.TransactionUnits = Math.Abs(newTransaction.TransactionUnits)

      '  Else
      '    ' Other
      '    If (newTransaction.TransactionUnits < 0.0#) Then
      '      newTransaction.TransactionType = RenaissanceGlobals.TransactionTypes.Sell
      '    Else
      '      newTransaction.TransactionType = RenaissanceGlobals.TransactionTypes.Buy
      '    End If
      '    newTransaction.TransactionUnits = Math.Abs(newTransaction.TransactionUnits)
      '  End If

      'End If

      'newTransaction.TransactionTradeStatusID = DEFAULT_EOD_TRADESTATUS
      'newTransaction.TransactionCounterparty = 1 ' Market

      'newTransaction.TransactionGroup = ""
      'newTransaction.TransactionInvestment = ""
      'newTransaction.TransactionExecution = ""
      'newTransaction.TransactionDataEntry = ""
      'newTransaction.TransactionDecisionDate = Now.Date
      'newTransaction.TransactionValueDate = Date_ValueDate.Value
      'newTransaction.TransactionSettlementDate = Date_MgmtFeesSettlementDate.Value
      'newTransaction.TransactionConfirmationDate = Date_MgmtFeesSettlementDate.Value
      'newTransaction.TransactionValueorAmount = "Amount"
      'newTransaction.TransactionCosts = 0
      'newTransaction.TransactionCostPercent = 0
      'newTransaction.TransactionSettlement = newTransaction.TransactionUnits

      '' Transfer Details.

      'newTransaction.TransactionIsTransfer = False
      'newTransaction.TransactionEffectivePrice = newTransaction.TransactionPrice
      'newTransaction.TransactionEffectiveValueDate = newTransaction.TransactionValueDate
      'newTransaction.TransactionSpecificInitialEqualisationFlag = False
      'newTransaction.TransactionSpecificInitialEqualisationValue = 0
      'newTransaction.TransactionEffectiveWatermark = 0

      'newTransaction.TransactionCostIsPercent = False
      'newTransaction.TransactionExemptFromUpdate = True
      'newTransaction.TransactionFinalAmount = (-1)
      'newTransaction.TransactionFinalPrice = (-1)
      'newTransaction.TransactionFinalCosts = (-1)
      'newTransaction.TransactionFinalSettlement = (-1)
      'newTransaction.TransactionInstructionFlag = (-1)
      'newTransaction.TransactionRedeemWholeHoldingFlag = False
      'newTransaction.TransactionBankConfirmation = 0
      'newTransaction.TransactionInitialDataEntry = 0
      'newTransaction.TransactionAmountConfirmed = 0
      'newTransaction.TransactionSettlementConfirmed = 0
      'newTransaction.TransactionFinalDataEntry = 0
      'newTransaction.TransactionRedemptionID = 0
      'newTransaction.TransactionComment = "Automated Management Fee booking"
      'newTransaction.TransactionCTFLAGS = 0
      'newTransaction.TransactionCompoundRN = 0

      'If (Math.Abs(newTransaction.TransactionUnits) >= 0.01) Then

      '  TransactionTable.AddtblTransactionRow(newTransaction)

      '  If MessageBox.Show("Book Management Fee for " & FeesToTake.ToString(DISPLAYMEMBER_DOUBLEFORMAT) & " ?" & vbCrLf & FeeDescription, "Management Fees Trade", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.OK Then

      '    MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblTransaction, New DSTransaction.tblTransactionRow() {newTransaction})

      '    ' Reload the transactions table if it is not loaded. (This Should never happen)
      '    If (TransactionTableIsLoaded = False) Then
      '      MainForm.ReloadTable_Background(RenaissanceStandardDatasets.tblTransaction.ChangeID, newTransaction.AuditID.ToString, False)
      '    End If

      '    MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceStandardDatasets.tblTransaction.ChangeID, newTransaction.AuditID.ToString), True)

      '  End If

      'End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error saving Management Fee Transaction.", ex.StackTrace, True)
    Finally
      Button_TradePerformanceFees.Enabled = True
    End Try
  End Sub

  ''' <summary>
  ''' Handles the Click event of the Button_TradeCrystalisedFees control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Button_TradeCrystalisedFees_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_TradeCrystalisedFees.Click
    ' *******************************************************************************
    '
    '
    ' *******************************************************************************

    Try
      Button_TradeCrystalisedFees.Enabled = False

      Dim Permissions As Integer = MainForm.CheckPermissions(Me.Name(), PermissionFeatureType.TypeForm)

      If ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) = 0) AndAlso ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) = 0) Then
        MessageBox.Show("You do not have Permissions to save transactions.")
        Exit Sub
      End If

      Dim FundFees As FundFeeCalculator.FundFeeDetails
      Dim FundValuation As FundFeeCalculator.FundValuationDetails
      Dim FeesToTake As Double = 0.0#
      Dim FeeDescription As String = ""

      Dim FundUnitID As Integer = 0
      Dim FundID As Integer = CInt(Nz(Combo_Fund.SelectedValue, 0))
      Dim FundRow As DSFund.tblFundRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, FundID)

      If (FundRow IsNot Nothing) Then
        FundUnitID = FundRow.FundUnit
      End If

      If (FeesObject IsNot Nothing) Then

        FundValuation = FeesObject.GetFundValuation()
        FundFees = FeesObject.GetFundFees()

        If (FundFees IsNot Nothing) AndAlso (FundValuation IsNot Nothing) AndAlso IsNumeric(edit_ManagementFeesToTake.Text) Then

          FeesToTake = Math.Round(CDbl(Numeric_PerfFeesCrystalisedToTake.Text) + FundValuation.PendingPerformanceFeesCrystalised, 2)

        End If

      End If

      Call BookFees(0, 0, FundID, FundID, CInt(Combo_PerformanceFeeCrystalisedInstrument.SelectedValue), FeesToTake, Date_ValueDate.Value, Date_PerfFeesSettlementDate.Value, False, False, True, True, False, FundUnitID, "")

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error saving Management Fee Transaction.", ex.StackTrace, True)
    Finally
      Button_TradeCrystalisedFees.Enabled = True
    End Try
  End Sub

  ''' <summary>
  ''' Handles the ValueChanged event of the Numeric_PerfFeesToTake control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Numeric_PerfFeesToTake_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Numeric_PerfFeesToTake.ValueChanged

    ' ************************************************************************************
    '
    '
    '
    ' ************************************************************************************

    Try

      If (Not InPaint) Then
        If (sender Is Numeric_PerfFeesToTake) Then
          CustomPerformanceFees = True
          Me.Numeric_PerfFeesToTake.BackColor = CUSTOM_BACKGROUND_CHANGED
        End If

        Dim FundFees As FundFeeCalculator.FundFeeDetails = Nothing
        Dim FundValuation As FundFeeCalculator.FundValuationDetails = Nothing

        If (FeesObject IsNot Nothing) Then

          FundValuation = FeesObject.GetFundValuation()
          FundFees = FeesObject.GetFundFees()

          If (FundFees IsNot Nothing) AndAlso (FundValuation IsNot Nothing) Then

            Label_Fees_AdditionalPerformanceTrade.Text = CDbl(Numeric_PerfFeesToTake.Value + (FundValuation.PreviousPerformanceFees + FundValuation.PendingPerformanceFees)).ToString(DISPLAYMEMBER_DOUBLEFORMAT)
            Label_Milestone_AdditionalPerformanceTrade.Text = Label_Fees_AdditionalPerformanceTrade.Text

          End If

        End If


      End If

    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Handles the TextChanged event of the Numeric_PerfFeesToTake control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Numeric_PerfFeesToTake_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Numeric_PerfFeesToTake.TextChanged
    ' ************************************************************************************
    '
    '
    '
    ' ************************************************************************************

    Try

      If (Not InPaint) Then
        If (sender Is Numeric_PerfFeesToTake) Then
          CustomPerformanceFees = True
          Me.Numeric_PerfFeesToTake.BackColor = CUSTOM_BACKGROUND_CHANGED
        End If

        Dim FundFees As FundFeeCalculator.FundFeeDetails
        Dim FundValuation As FundFeeCalculator.FundValuationDetails

        If (FeesObject IsNot Nothing) Then

          FundValuation = FeesObject.GetFundValuation()
          FundFees = FeesObject.GetFundFees()

          If (FundFees IsNot Nothing) AndAlso (FundValuation IsNot Nothing) AndAlso IsNumeric(Numeric_PerfFeesToTake.Text) Then

            Label_Fees_AdditionalPerformanceTrade.Text = CDbl(CDbl(Numeric_PerfFeesToTake.Text) + (FundValuation.PreviousPerformanceFees + FundValuation.PendingPerformanceFees)).ToString(DISPLAYMEMBER_DOUBLEFORMAT)
            Label_Milestone_AdditionalPerformanceTrade.Text = Label_Fees_AdditionalPerformanceTrade.Text

          End If

        End If


      End If

    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Handles the ValueChanged event of the Numeric_PerfFeesCrystalisedToTake control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Numeric_PerfFeesCrystalisedToTake_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Numeric_PerfFeesCrystalisedToTake.ValueChanged
    ' ************************************************************************************
    '
    '
    '
    ' ************************************************************************************

    Try

      If (Not InPaint) Then
        If (sender Is Numeric_PerfFeesCrystalisedToTake) Then
          CustomCrystalisedFees = True
          Me.Numeric_PerfFeesCrystalisedToTake.BackColor = CUSTOM_BACKGROUND_CHANGED
        End If

        Dim FundFees As FundFeeCalculator.FundFeeDetails
        Dim FundValuation As FundFeeCalculator.FundValuationDetails

        If (FeesObject IsNot Nothing) Then

          FundValuation = FeesObject.GetFundValuation()
          FundFees = FeesObject.GetFundFees()

          If (FundFees IsNot Nothing) AndAlso (FundValuation IsNot Nothing) Then

            Label_Fees_AdditionalCrystalisedTrade.Text = (Numeric_PerfFeesCrystalisedToTake.Value + FundValuation.PendingPerformanceFeesCrystalised).ToString(DISPLAYMEMBER_DOUBLEFORMAT)
            Label_Milestone_AdditionalCrystalisedTrade.Text = Label_Fees_AdditionalCrystalisedTrade.Text

          End If

        End If


      End If

    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Handles the TextChanged event of the Numeric_PerfFeesCrystalisedToTake control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Numeric_PerfFeesCrystalisedToTake_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Numeric_PerfFeesCrystalisedToTake.TextChanged
    ' ************************************************************************************
    '
    '
    '
    ' ************************************************************************************

    Try

      If (Not InPaint) Then
        If (sender Is Numeric_PerfFeesCrystalisedToTake) Then
          CustomCrystalisedFees = True
          Me.Numeric_PerfFeesCrystalisedToTake.BackColor = CUSTOM_BACKGROUND_CHANGED
        End If

        Dim FundFees As FundFeeCalculator.FundFeeDetails
        Dim FundValuation As FundFeeCalculator.FundValuationDetails

        If (FeesObject IsNot Nothing) Then

          FundValuation = FeesObject.GetFundValuation()
          FundFees = FeesObject.GetFundFees()

          If (FundFees IsNot Nothing) AndAlso (FundValuation IsNot Nothing) AndAlso IsNumeric(Numeric_PerfFeesCrystalisedToTake.Text) Then

            Label_Fees_AdditionalCrystalisedTrade.Text = (CDbl(Numeric_PerfFeesCrystalisedToTake.Text) + FundValuation.PendingPerformanceFeesCrystalised).ToString(DISPLAYMEMBER_DOUBLEFORMAT)
            Label_Milestone_AdditionalCrystalisedTrade.Text = Label_Fees_AdditionalCrystalisedTrade.Text

          End If

        End If


      End If

    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Handles the ValueChanged event of the edit_ManagementFeesToTake control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub edit_ManagementFeesToTake_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles edit_ManagementFeesToTake.ValueChanged
    ' ************************************************************************************
    '
    '
    '
    ' ************************************************************************************

    Try

      If (Not InPaint) Then
        If (sender Is edit_ManagementFeesToTake) Then
          CustomManagementFees = True
          Me.edit_ManagementFeesToTake.BackColor = CUSTOM_BACKGROUND_CHANGED
        End If

        Dim FundFees As FundFeeCalculator.FundFeeDetails
        Dim FundValuation As FundFeeCalculator.FundValuationDetails

        If (FeesObject IsNot Nothing) Then

          FundValuation = FeesObject.GetFundValuation()
          FundFees = FeesObject.GetFundFees()

          If (FundFees IsNot Nothing) AndAlso (FundValuation IsNot Nothing) Then

            Label_Fees_AdditionalManagementTrade.Text = (edit_ManagementFeesToTake.Value + FundValuation.PendingManagementFees).ToString(DISPLAYMEMBER_DOUBLEFORMAT)
            Label_Milestone_AdditionalManagementTrade.Text = Label_Fees_AdditionalManagementTrade.Text

          End If

        End If


      End If

    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Handles the TextChanged event of the edit_ManagementFeesToTake control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub edit_ManagementFeesToTake_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles edit_ManagementFeesToTake.TextChanged
    ' ************************************************************************************
    '
    '
    '
    ' ************************************************************************************

    Try

      If (Not InPaint) Then

        If (sender Is edit_ManagementFeesToTake) Then
          CustomManagementFees = True
          Me.edit_ManagementFeesToTake.BackColor = CUSTOM_BACKGROUND_CHANGED
        End If

        Dim FundFees As FundFeeCalculator.FundFeeDetails
        Dim FundValuation As FundFeeCalculator.FundValuationDetails

        If (FeesObject IsNot Nothing) Then

          FundValuation = FeesObject.GetFundValuation()
          FundFees = FeesObject.GetFundFees()

          If (FundFees IsNot Nothing) AndAlso (FundValuation IsNot Nothing) AndAlso IsNumeric(edit_ManagementFeesToTake.Text) Then

            Label_Fees_AdditionalManagementTrade.Text = (CDbl(edit_ManagementFeesToTake.Text) + FundValuation.PendingManagementFees).ToString(DISPLAYMEMBER_DOUBLEFORMAT)
            Label_Milestone_AdditionalManagementTrade.Text = Label_Fees_AdditionalManagementTrade.Text

          End If

        End If


      End If

    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Books the fees.
  ''' </summary>
  ''' <param name="FundID">The fund ID.</param>
  ''' <param name="SubFundID">The sub fund ID.</param>
  ''' <param name="InstrumentID">The instrument ID.</param>
  ''' <param name="FeesToTake">The fees to take.</param>
  ''' <param name="ValueDate">The value date.</param>
  ''' <param name="SettlementDate">The settlement date.</param>
  ''' <param name="NoMessage">if set to <c>true</c> [no message].</param>
  ''' <param name="NoUpdate">if set to <c>true</c> [no update].</param>
  ''' <param name="BookAsProvisionRatherThanFee">if set to <c>true</c> [book as provision rather than fee].</param>
  ''' <param name="TakeReductionInProvisionsAsPayment">if set to <c>true</c> [take reduction in provisions as payment].</param>
  ''' <param name="NoPAndL">if set to <c>true</c> [no P and L].</param>
  ''' <returns>System.String.</returns>
  Private Function BookFees(ByVal ExistingTransactionID As Integer, ByVal FeeID As Integer, ByVal FundID As Integer, ByVal SubFundID As Integer, ByVal InstrumentID As Integer, ByVal FeesToTake As Double, ByVal ValueDate As Date, ByVal SettlementDate As Date, ByVal NoMessage As Boolean, ByVal NoUpdate As Boolean, ByVal BookAsProvisionRatherThanFee As Boolean, ByVal TakeReductionInProvisionsAsPayment As Boolean, ByVal NoPAndL As Boolean, ByVal FundUnitID As Integer, ByVal TransactionComment As String) As String
    ' *******************************************************************************
    ' FeesToTake : +ve : Cost, -ve Credit to fund.
    '
    ' NoPAndL : Not a fee, just using up or releasing cash,  +ve Fees = Use Cash, -ve Fees = Release Cash.
    '
    ' *******************************************************************************
    Dim FeeDescription As String = ""

    Try
      ' Check permissions.
      Dim Permissions As Integer = MainForm.CheckPermissions(Me.Name(), PermissionFeatureType.TypeForm)

      If ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) = 0) AndAlso ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) = 0) Then
        MessageBox.Show("You do not have Permissions to change transactions.")
        Return ""
        Exit Function
      End If
    Catch ex As Exception
    End Try

    Try

      Dim TransactionDS As DSTransaction
      Dim TransactionTable As DSTransaction.tblTransactionDataTable
      Dim newTransaction As DSTransaction.tblTransactionRow
      Dim TransactionTableIsLoaded As Boolean = True
      Dim thisInstrumentRow As DSInstrument.tblInstrumentRow = Nothing
      Dim ThisInstrumentDescription As String = "<Unknown>"
      Dim ThisCurrency As String = "<Unknown>"

      ' Use the Transactions table, if it is loaded, else use a temporary one.

      TransactionDS = MainForm.MainDataHandler.Get_Dataset(RenaissanceStandardDatasets.GetStandardDataset(RenaissanceChangeID.tblTransaction).DatasetName)
      If (TransactionDS Is Nothing) Then
        TransactionDS = New DSTransaction
        TransactionTableIsLoaded = False
      End If

      thisInstrumentRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, InstrumentID), DSInstrument.tblInstrumentRow)
      If (thisInstrumentRow IsNot Nothing) Then
        ThisInstrumentDescription = thisInstrumentRow.InstrumentDescription
        ThisCurrency = thisInstrumentRow.InstrumentCurrency
      End If

      ' OK, Onwards.....
      ' Get existing transaction if a TransactionID was given and the Transactions table is loaded.
      ' If the table is not loaded, then updating the Transaction by adding a new row to a temporary table will be OK.

      TransactionTable = TransactionDS.tblTransaction

      If (ExistingTransactionID > 0) AndAlso (TransactionTableIsLoaded) Then
        newTransaction = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblTransaction, ExistingTransactionID)

        If (newTransaction Is Nothing) Then
          newTransaction = TransactionTable.NewtblTransactionRow
        End If
      Else
        newTransaction = TransactionTable.NewtblTransactionRow
      End If

      newTransaction.TransactionFund = FundID
      newTransaction.TransactionSubFund = IIf(Nz(SubFundID, 0) <= 0, FundID, SubFundID)
      newTransaction.TransactionInstrument = InstrumentID
      newTransaction.TransactionVersusInstrument = 0
      newTransaction.TransactionSpecificToFundUnitID = FundUnitID
      newTransaction.TransactionFeeID = FeeID

      ' Check price of Fee instrument, will determine how to book the trade

      ' Non Zero priced instruments imply a trade vs provision approach rather than a trade vs cash approach implied by a zero price.

      Dim FeePrice As Double = GetBestNAV(newTransaction.TransactionInstrument, ValueDate)

      ' Can't book 'NoPAndL' trades against an instrument with no price.
      If (Math.Abs(FeePrice) <= 0.0001#) AndAlso (NoPAndL = False) Then ' Eliminate rounding error, unlikely with a zero value !

        newTransaction.TransactionUnits = FeesToTake
        newTransaction.TransactionPrice = 1

        If (CType(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, newTransaction.TransactionInstrument, "InstrumentType"), RenaissanceGlobals.InstrumentTypes) = RenaissanceGlobals.InstrumentTypes.Fee) Then
          newTransaction.TransactionType = RenaissanceGlobals.TransactionTypes.Fee
        ElseIf CType(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, newTransaction.TransactionInstrument, "InstrumentType"), RenaissanceGlobals.InstrumentTypes) = RenaissanceGlobals.InstrumentTypes.Expense Then

          If (FeesToTake >= 0.0#) Then
            newTransaction.TransactionType = RenaissanceGlobals.TransactionTypes.Expense_Increase
          Else
            newTransaction.TransactionType = RenaissanceGlobals.TransactionTypes.Expense_Reduce
          End If

          newTransaction.TransactionUnits = Math.Abs(newTransaction.TransactionUnits)
        Else
          If (newTransaction.TransactionUnits > 0) Then
            newTransaction.TransactionType = RenaissanceGlobals.TransactionTypes.Buy
          Else
            newTransaction.TransactionType = RenaissanceGlobals.TransactionTypes.Sell
          End If

          newTransaction.TransactionUnits = Math.Abs(newTransaction.TransactionUnits)
        End If

        FeeDescription = "Fees booked vs Cash" & vbCrLf & "Purchase vs Zero value for Fee Instrument."

      ElseIf FeePrice > 0.0# Then

        ' To enact a fee, you must sell for zero, something with a positive value

        If (NoPAndL) Then
          newTransaction.TransactionUnits = FeesToTake / FeePrice
        Else
          newTransaction.TransactionUnits = -FeesToTake / FeePrice
        End If

        newTransaction.TransactionPrice = 0

        If (BookAsProvisionRatherThanFee = False) AndAlso (CType(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, newTransaction.TransactionInstrument, "InstrumentType"), RenaissanceGlobals.InstrumentTypes) = RenaissanceGlobals.InstrumentTypes.Fee) Then
          ' Fee
          newTransaction.TransactionType = RenaissanceGlobals.TransactionTypes.Fee
          FeeDescription = "Fees booked as Fee." & vbCrLf & "Sale vs Positive value for Fee instrument."

        ElseIf CType(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, newTransaction.TransactionInstrument, "InstrumentType"), RenaissanceGlobals.InstrumentTypes) = RenaissanceGlobals.InstrumentTypes.Expense Then
          ' Expense
          If (newTransaction.TransactionUnits >= 0.0#) Then
            newTransaction.TransactionType = RenaissanceGlobals.TransactionTypes.Expense_Increase
            FeeDescription = "Fees booked as Expense." & vbCrLf & "Buy vs Positive value for Expense instrument."
          Else
            newTransaction.TransactionType = RenaissanceGlobals.TransactionTypes.Expense_Reduce
            FeeDescription = "Fees booked as Expense." & vbCrLf & "Sell vs Positive value for Expense instrument."
          End If
          newTransaction.TransactionUnits = Math.Abs(newTransaction.TransactionUnits)

        Else
          ' Other

          If (NoPAndL) Then
            If (newTransaction.TransactionUnits < 0.0#) Then
              newTransaction.TransactionType = RenaissanceGlobals.TransactionTypes.Sell
            Else
              newTransaction.TransactionType = RenaissanceGlobals.TransactionTypes.Buy
            End If
            newTransaction.TransactionPrice = FeePrice

          Else

            If (newTransaction.TransactionUnits < 0.0#) Then
              newTransaction.TransactionType = RenaissanceGlobals.TransactionTypes.Provision_Reduce

              If (TakeReductionInProvisionsAsPayment) Then
                newTransaction.TransactionPrice = FeePrice
              End If
            Else
              newTransaction.TransactionType = RenaissanceGlobals.TransactionTypes.Provision_Increase
            End If
          End If

          newTransaction.TransactionUnits = Math.Abs(newTransaction.TransactionUnits)

        End If


      ElseIf FeePrice < 0.0# Then

        ' To enact a fee, you must buy for zero something with a negative value

        If (NoPAndL) Then
          newTransaction.TransactionUnits = -FeesToTake / FeePrice
        Else
          newTransaction.TransactionUnits = FeesToTake / FeePrice
        End If

        newTransaction.TransactionPrice = 0.0#
        FeeDescription = "Fees booked as provision." & vbCrLf & "Purchase vs Negative value for Fee instrument."

        If (BookAsProvisionRatherThanFee = False) AndAlso (CType(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, newTransaction.TransactionInstrument, "InstrumentType"), RenaissanceGlobals.InstrumentTypes) = RenaissanceGlobals.InstrumentTypes.Fee) Then
          ' Fee
          newTransaction.TransactionType = RenaissanceGlobals.TransactionTypes.Fee
          FeeDescription = "Fees booked as Fee." & vbCrLf & "Buy vs Negative value for Fee instrument."

        ElseIf CType(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, newTransaction.TransactionInstrument, "InstrumentType"), RenaissanceGlobals.InstrumentTypes) = RenaissanceGlobals.InstrumentTypes.Expense Then
          ' Expense
          If (newTransaction.TransactionUnits >= 0.0#) Then
            newTransaction.TransactionType = RenaissanceGlobals.TransactionTypes.Expense_Increase
            FeeDescription = "Fees booked as Expense." & vbCrLf & "Buy vs Negative value for Expense instrument."
          Else
            newTransaction.TransactionType = RenaissanceGlobals.TransactionTypes.Expense_Reduce
            FeeDescription = "Fees booked as Expense." & vbCrLf & "Sell vs Negative value for Expense instrument."
          End If
          newTransaction.TransactionUnits = Math.Abs(newTransaction.TransactionUnits)

        Else
          ' Other

          If (NoPAndL) Then
            If (newTransaction.TransactionUnits < 0.0#) Then
              newTransaction.TransactionType = RenaissanceGlobals.TransactionTypes.Buy
            Else
              newTransaction.TransactionType = RenaissanceGlobals.TransactionTypes.Sell
            End If
            newTransaction.TransactionPrice = FeePrice

          Else
            If (newTransaction.TransactionUnits < 0.0#) Then
              newTransaction.TransactionType = RenaissanceGlobals.TransactionTypes.Provision_Reduce

              If (TakeReductionInProvisionsAsPayment) Then
                newTransaction.TransactionPrice = FeePrice
              End If
            Else
              newTransaction.TransactionType = RenaissanceGlobals.TransactionTypes.Provision_Increase
            End If

          End If

          newTransaction.TransactionUnits = Math.Abs(newTransaction.TransactionUnits)

        End If

      End If

      newTransaction.TransactionTradeStatusID = DEFAULT_EOD_TRADESTATUS
      newTransaction.TransactionCounterparty = 1 ' Market

      newTransaction.TransactionGroup = ""
      newTransaction.TransactionInvestment = ""
      newTransaction.TransactionExecution = ""
      newTransaction.TransactionDataEntry = ""
      newTransaction.TransactionDecisionDate = Now.Date
      newTransaction.TransactionValueDate = ValueDate
      newTransaction.TransactionSettlementDate = SettlementDate
      newTransaction.TransactionConfirmationDate = SettlementDate
      newTransaction.TransactionValueorAmount = "Amount"
      newTransaction.TransactionCosts = 0.0#
      newTransaction.TransactionCostPercent = 0.0#
      newTransaction.TransactionSettlement = newTransaction.TransactionUnits

      ' Transfer Details.

      newTransaction.TransactionIsTransfer = False
      newTransaction.TransactionEffectivePrice = newTransaction.TransactionPrice
      newTransaction.TransactionEffectiveValueDate = newTransaction.TransactionValueDate
      newTransaction.TransactionSpecificInitialEqualisationFlag = False
      newTransaction.TransactionSpecificInitialEqualisationValue = 0
      newTransaction.TransactionEffectiveWatermark = 0

      newTransaction.TransactionCostIsPercent = False
      newTransaction.TransactionExemptFromUpdate = True
      newTransaction.TransactionFinalAmount = (-1)
      newTransaction.TransactionFinalPrice = (-1)
      newTransaction.TransactionFinalCosts = (-1)
      newTransaction.TransactionFinalSettlement = (-1)
      newTransaction.TransactionInstructionFlag = (-1)
      newTransaction.TransactionRedeemWholeHoldingFlag = False
      newTransaction.TransactionBankConfirmation = 0
      newTransaction.TransactionInitialDataEntry = 0
      newTransaction.TransactionAmountConfirmed = 0
      newTransaction.TransactionSettlementConfirmed = 0
      newTransaction.TransactionFinalDataEntry = 0
      newTransaction.TransactionRedemptionID = 0
      newTransaction.TransactionComment = "Automated Fee booking " & FundID.ToString("###0") & ", " & ValueDate.ToString(DISPLAYMEMBER_DATEFORMAT) & vbCrLf & CStr(Nz(TransactionComment, ""))
      newTransaction.TransactionCTFLAGS = 0
      newTransaction.TransactionCompoundRN = 0

      If (Math.Abs(newTransaction.TransactionUnits) >= 0.01) OrElse (ExistingTransactionID > 0) Then

        If (NoMessage) OrElse (MessageBox.Show("Book " & ThisInstrumentDescription & vbCrLf & " for " & FeesToTake.ToString(DISPLAYMEMBER_DOUBLEFORMAT) & ThisCurrency & " ?" & vbCrLf & FeeDescription, "Fees / Expense / Provision Trade", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.OK) Then

          If (newTransaction.RowState And DataRowState.Detached) Then
            TransactionTable.AddtblTransactionRow(newTransaction)
          End If

          MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblTransaction, New DSTransaction.tblTransactionRow() {newTransaction})

          ' Reload the transactions table if it is not loaded. (This Should never happen)
          If (TransactionTableIsLoaded = False) Then
            MainForm.ReloadTable_Background(RenaissanceStandardDatasets.tblTransaction.ChangeID, newTransaction.AuditID.ToString(), False)
          End If

          If (Not NoUpdate) Then
            MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceStandardDatasets.tblTransaction.ChangeID, newTransaction.AuditID.ToString), True)
          End If

          Return newTransaction.AuditID.ToString
        End If

      End If


    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error saving Fee Transaction.", ex.StackTrace, True)

    End Try

    Return ""

  End Function


#End Region

#Region " Milestone Tab"

  ''' <summary>
  ''' Handles the Click event of the button_SetMilestone control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub button_SetMilestone_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_SetMilestone.Click
    ' *******************************************************************************
    '
    '
    ' *******************************************************************************

    Dim FundID As Integer = 0
    Dim MilestoneDate As Date
    Dim FundPricingPeriod As DealingPeriod = RenaissanceGlobals.DealingPeriod.Daily
    Dim FundName As String
    Dim StatusGroupFilter As String
    Dim AdministratorDatesFilter As RenaissanceGlobals.AdministratorDatesFilter = RenaissanceGlobals.AdministratorDatesFilter.None
    Dim FundValuation As FundFeeCalculator.FundValuationDetails = Nothing
    Dim FundInstrument As Integer = 0

    Dim MilestonesDS As DSFundMilestones
    Dim MilestonesTable As DSFundMilestones.tblFundMilestonesDataTable
    Dim MilestonesSelectedRows() As DSFundMilestones.tblFundMilestonesRow

    Try
      ' Check permissions.
      Dim Permissions As Integer = MainForm.CheckPermissions(Me.Name(), PermissionFeatureType.TypeForm)

      If ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) = 0) AndAlso ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) = 0) Then
        MessageBox.Show("You do not have Permissions to action End of Day.")
        Exit Sub
      End If
    Catch ex As Exception
      Exit Sub
    End Try

    ' 

    Try

      If IsNumeric(Combo_Fund.SelectedValue) Then
        FundID = CInt(Combo_Fund.SelectedValue)
      End If

      MilestoneDate = Date_ValueDate.Value

      ' Get Fund Fees
      StatusGroupFilter = Nz(MainForm.GetComboSelectedValue(Combo_TransactionGroup), "")

      ' AdministratorDatesFilter (bitmap)
      '
      '--	1		- Ignore the transaction status for Subscriptions and Redemptions
      '--	2		- Ignore the Subscriptions and Redemptions on (or after of course) the given Value Date

      If (MainForm.GetCheckBoxChecked(Me.Check_SR_IgnoreStatus)) Then
        AdministratorDatesFilter += RenaissanceGlobals.AdministratorDatesFilter.SubscriptionRedemption_IgnoreTransactionStatus
      End If
      If (MainForm.GetCheckBoxChecked(Me.Check_SR_IgnoreTodaysTrades)) Then
        AdministratorDatesFilter += RenaissanceGlobals.AdministratorDatesFilter.SubscriptionRedemption_IgnoreTodaysTrades
      End If

      '

      MainForm.SetToolStripText(StatusLabel, "Calculating Fund Administrator Valuation.")

      FundValuation = GetFeesValuation(FeesObject, FundID, MilestoneDate, Date_FeesSince.Value, StatusGroupFilter, AdministratorDatesFilter, Check_AdministratorPriceDates.Checked)

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error getting Valuation or Fee Calculations.", ex.StackTrace, True)
    Finally
      MainForm.SetToolStripText(StatusLabel, "")
    End Try

    Try

      FundPricingPeriod = CType(Nz(LookupTableValue(MainForm, RenaissanceGlobals.RenaissanceStandardDatasets.tblFund, FundID, "FundPricingPeriod"), RenaissanceGlobals.DealingPeriod.Daily), RenaissanceGlobals.DealingPeriod)
      FundName = Nz(LookupTableValue(MainForm, RenaissanceGlobals.RenaissanceStandardDatasets.tblFund, FundID, "FundName"), FundID.ToString())
      FundInstrument = CInt(Nz(LookupTableValue(MainForm, RenaissanceGlobals.RenaissanceStandardDatasets.tblFund, FundID, "FundUnit"), 0))

      ' MilestoneDate, Ensure that it is the last day of the period (day, week, month etc..)

      MilestoneDate = FitDateToPeriod(FundPricingPeriod, MilestoneDate, True)

      ' 

      If (HasMilestoneInsertPermission) AndAlso (FundID > 0) Then

        If MessageBox.Show("Set Milestone" & vbCrLf & "Fund : " & FundName & vbCrLf & "Date : " & MilestoneDate.ToString(DISPLAYMEMBER_DATEFORMAT), "Set Milestone", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.OK Then

          MainForm.LogError(Me.Name & ", button_SetMilestone_Click()", LOG_LEVELS.Audit, "Reconciliation process, Set Milestone. " & FundID.ToString, "Reconciliation process, Set Milestone. " & FundID.ToString, "", False)

          ' Check for later Milestones.

          MilestonesDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblFundMilestones, True)

          If (MilestonesDS IsNot Nothing) Then
            MilestonesTable = MilestonesDS.tblFundMilestones
            MilestonesSelectedRows = MilestonesTable.Select("(MilestoneFundID=" & FundID.ToString & ") AND (MilestoneDate > '" & MilestoneDate.ToString(QUERY_SHORTDATEFORMAT) & "')", "MilestoneDate Desc")

            If MilestonesSelectedRows.Length > 0 Then
              If MessageBox.Show("There are already Milestones entered after " & MilestoneDate.ToString(DISPLAYMEMBER_DATEFORMAT) & vbCrLf & "Do you wish to continue ?", "Existing Milestones", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) <> Windows.Forms.DialogResult.OK Then
                Exit Sub
              End If
            End If

          End If

          ' Save Milestone record.

          Try
            ' Save Milestone 

            MainForm.SetToolStripText(Label_Status, "Saving Milestone")

            MilestonesTable = MilestonesDS.tblFundMilestones

            Dim MilestoneAdaptor As SqlDataAdapter
            Dim NewMilestoneRow As RenaissanceDataClass.DSFundMilestones.tblFundMilestonesRow

            MilestoneAdaptor = MainForm.MainDataHandler.Get_Adaptor(RenaissanceStandardDatasets.tblFundMilestones.Adaptorname, VENICE_CONNECTION, RenaissanceStandardDatasets.tblFundMilestones.TableName)

            SyncLock MilestonesDS

              MilestonesSelectedRows = MilestonesTable.Select("(MilestoneFundID=" & FundID.ToString & ") AND (MilestoneDate='" & MilestoneDate.ToString(QUERY_SHORTDATEFORMAT) & "')", "MilestoneDate")

              If (Not (MilestonesSelectedRows Is Nothing)) AndAlso (MilestonesSelectedRows.Length > 0) Then
                ' Edit Existing Record

                MilestonesSelectedRows(0).MilestoneKnowledgeDate = Now()

                MilestonesSelectedRows(0).Administrator_NAV = edit_Milestone_NAV_Administrator.Value
                MilestonesSelectedRows(0).Administrator_GNAV = edit_Milestone_GNAV_Administrator.Value
                MilestonesSelectedRows(0).Fund_NAV = edit_Milestone_NAV_Trading.Value
                MilestonesSelectedRows(0).Administrator_UnitsSubscribed = Math.Abs(FundValuation.FundUnitsSubscribed)
                MilestonesSelectedRows(0).Administrator_UnitsRedeemed = Math.Abs(FundValuation.FundUnitsRedeemed)

              Else
                NewMilestoneRow = MilestonesTable.NewtblFundMilestonesRow

                NewMilestoneRow.MilestoneFundID = FundID
                NewMilestoneRow.MilestoneDate = MilestoneDate
                NewMilestoneRow.MilestoneDateString = MilestoneDate.ToString(DISPLAYMEMBER_DATEFORMAT)
                NewMilestoneRow.MilestoneKnowledgeDate = Now()

                NewMilestoneRow.Administrator_NAV = edit_Milestone_NAV_Administrator.Value
                NewMilestoneRow.Administrator_GNAV = edit_Milestone_GNAV_Administrator.Value
                NewMilestoneRow.Fund_NAV = edit_Milestone_NAV_Trading.Value
                NewMilestoneRow.Administrator_UnitsSubscribed = Math.Abs(FundValuation.FundUnitsSubscribed)
                NewMilestoneRow.Administrator_UnitsRedeemed = Math.Abs(FundValuation.FundUnitsRedeemed)

                MilestonesTable.Rows.Add(NewMilestoneRow)
              End If

              MilestoneAdaptor.InsertCommand.Parameters("@KnowledgeDate").Value = KNOWLEDGEDATE_NOW
              MilestoneAdaptor.UpdateCommand.Parameters("@KnowledgeDate").Value = KNOWLEDGEDATE_NOW

              MainForm.AdaptorUpdate(Me.Name, MilestoneAdaptor, MilestonesTable)

            End SyncLock

            Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblFundMilestones), True)

          Catch ex As Exception
            MainForm.LogError(Me.Name & ", button_SetMilestone_Click()", LOG_LEVELS.Error, ex.Message, "Error Saving Milestone entry.", ex.StackTrace, True)
            Exit Sub
          End Try


          ' Save Fund Price

          If (FundInstrument > 0) Then
            SavePrice(MainForm, FundInstrument, MilestoneDate, edit_Milestone_NAVUnitPrice_Valuation.Value, 0.0#, edit_Milestone_GNAVUnitPrice_Valuation.Value, 0.0#, False, 0.0#, MilestoneDate, 1, 0, True, Now(), "Milestone Price.", 0, 0, 0, 1, 0)

            Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblPrice), True)
          End If

          ' Save Historic Valuation data

          MainForm.LogError(Me.Name & ", button_SetMilestone_Click()", LOG_LEVELS.Audit, "Reconciliation process, Set Historic Valuations. " & FundID.ToString & ", " & MilestoneDate.ToString(RenaissanceGlobals.Globals.QUERY_SHORTDATEFORMAT), "Reconciliation process, Set Historic Valuations. " & FundID.ToString & ", " & MilestoneDate.ToString(RenaissanceGlobals.Globals.QUERY_SHORTDATEFORMAT), "", False)

          Dim SaveAttributionCommand As New SqlCommand

          Try
            SaveAttributionCommand.Connection = MainForm.GetVeniceConnection
            SaveAttributionCommand.CommandType = CommandType.StoredProcedure
            SaveAttributionCommand.CommandText = "[adp_tblHistoricValuations_Populate]"
            SaveAttributionCommand.CommandTimeout = ATTRIBUTION_SQLCOMMAND_TIMEOUT

            SaveAttributionCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
            SaveAttributionCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@StartDate", System.Data.SqlDbType.Date)).Value = MilestoneDate
            SaveAttributionCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EndDate", System.Data.SqlDbType.Date)).Value = MilestoneDate
            SaveAttributionCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FundID", System.Data.SqlDbType.Int)).Value = FundID
            SaveAttributionCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime)).Value = KNOWLEDGEDATE_NOW

            If (Not (SaveAttributionCommand.Connection Is Nothing)) Then
              SaveAttributionCommand.ExecuteNonQuery()
            End If

            Call AllocationSetReturns(MainForm, FundID, AddPeriodToDate(FundPricingPeriod, MilestoneDate, IIf(FundPricingPeriod = DealingPeriod.Daily, -5, -1)), MilestoneDate, 0, Nothing)

          Catch ex As Exception
            MainForm.LogError(Me.Name & ", button_SetMilestone_Click()", LOG_LEVELS.Error, ex.Message, "Error Saving Historic Valuation.", ex.StackTrace, True)
          Finally
            If (SaveAttributionCommand.Connection IsNot Nothing) Then
              SaveAttributionCommand.Connection.Close()
              SaveAttributionCommand.Connection = Nothing
            End If
          End Try

        End If ' Messagebox 'OK'

      End If ' Permissions OK
    Catch ex As Exception
      MainForm.LogError(Me.Name & ", button_SetMilestone_Click()", LOG_LEVELS.Error, ex.Message, "Error Saving Milestone entry.", ex.StackTrace, True)
    Finally
      Me.MainForm.SetToolStripText(Label_Status, "")
    End Try

    Try
      ' Update relevant Subscriptions / Redemptions.
      ' If Selected and the price is non-zero.

      If (Me.Check_UpdateOrdersAutomatically.Checked) AndAlso (edit_FixedAdministratorPrice.Value <> 0.0#) Then

        Dim TransactionDS As RenaissanceDataClass.DSTransaction
        Dim TransactionTable As DSTransaction.tblTransactionDataTable
        Dim SelectedTransactions() As RenaissanceDataClass.DSTransaction.tblTransactionRow = Nothing
        Dim TransactionRow As RenaissanceDataClass.DSTransaction.tblTransactionRow = Nothing
        Dim LastFeeDate As Date = Date_FeesSince.Value
        Dim UpdateString As String = ""
        Dim PostedUpdate As Boolean = False
        Dim TransactionChanged As Boolean = False

        TransactionDS = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblTransaction), DSTransaction)
        If (TransactionDS IsNot Nothing) Then

          ' Get Selected Orders.

          TransactionTable = TransactionDS.tblTransaction
          SelectedTransactions = TransactionTable.Select("(TransactionFund=" & FundID.ToString("###0") & ") AND ((TransactionType=" & CInt(TransactionTypes.Subscribe).ToString() & ") OR (TransactionType=" & CInt(TransactionTypes.Redeem).ToString() & ")) AND (TransactionValueDate>'" & LastFeeDate.ToString("yyyy-MM-dd") & "') AND (TransactionValueDate<='" & MilestoneDate.ToString("yyyy-MM-dd") & "')", "TransactionType, TransactionValueDate, TransactionParentID")

          ' If there are any...

          If (SelectedTransactions IsNot Nothing) AndAlso (SelectedTransactions.Length > 0) Then




            ' Check....

            If MessageBox.Show("Update the " & SelectedTransactions.Length.ToString() & " relevant Subscriptions / Redemptions ?", "Update Orders", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then

              ' Update.

              For Each TransactionRow In SelectedTransactions
                TransactionChanged = False

                If ((Math.Abs(TransactionRow.TransactionPrice - edit_FixedAdministratorPrice.Value) < 0.000001) OrElse (TransactionRow.TransactionTradeStatusID <> TradeStatus.CheckedEOD)) Then

                  TransactionRow.TransactionPrice = edit_FixedAdministratorPrice.Value
                  TransactionRow.TransactionTradeStatusID = TradeStatus.CheckedEOD

                  TransactionChanged = True
                End If

                If (TransactionChanged) Then
                  MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblTransaction, New RenaissanceDataClass.DSTransaction.tblTransactionRow() {TransactionRow})

                  UpdateString &= TransactionRow.TransactionParentID.ToString("###0") & ","
                  PostedUpdate = True
                End If
              Next

              If (PostedUpdate) Then
                MainForm.Main_RaiseEvent(New RenaissanceUpdateEventArgs(RenaissanceChangeID.tblTransaction, UpdateString))
              End If

            End If ' MessageBox.Show

          End If ' SelectedTransactions IsNot Nothing

        End If ' TransactionDS IsNot Nothing

      End If ' Check_UpdateOrdersAutomatically.Checked

    Catch ex As Exception
      MainForm.LogError(Me.Name & ", button_SetMilestone_Click()", LOG_LEVELS.Error, ex.Message, "Error updating Subscriptions.", ex.StackTrace, True)
    Finally

    End Try

    MessageBox.Show("Milestone Saved", "Milestone Saved", MessageBoxButtons.OK, MessageBoxIcon.Information)

  End Sub

  ''' <summary>
  ''' Handles the TextChanged event of the edit_Milestone_NAV_Administrator control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub edit_Milestone_NAV_Administrator_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles edit_Milestone_NAV_Administrator.TextChanged, edit_Milestone_NAV_Administrator.ValueChanged
    ' ************************************************************************************
    '
    ' ************************************************************************************

    Try
      If (Not InPaint) Then
        If (sender Is edit_Milestone_NAV_Administrator) Then
          Custom_MS_Valuation_NAV = True
          edit_Milestone_NAV_Administrator.BackColor = CUSTOM_BACKGROUND_CHANGED
        End If
      End If
    Catch ex As Exception

    End Try

  End Sub

  ''' <summary>
  ''' Handles the TextChanged event of the edit_Milestone_NAVUnitPrice_Valuation control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub edit_Milestone_NAVUnitPrice_Valuation_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles edit_Milestone_NAVUnitPrice_Valuation.TextChanged, edit_Milestone_NAVUnitPrice_Valuation.ValueChanged
    ' ************************************************************************************
    '
    ' ************************************************************************************

    Try
      If (Not InPaint) Then
        If (sender Is edit_Milestone_NAVUnitPrice_Valuation) Then
          Custom_MS_Valuation_NAV_UnitPrice = True
          edit_Milestone_NAVUnitPrice_Valuation.BackColor = CUSTOM_BACKGROUND_CHANGED
        End If
      End If
    Catch ex As Exception

    End Try
  End Sub

  ''' <summary>
  ''' Handles the TextChanged event of the edit_Milestone_GNAV_Administrator control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub edit_Milestone_GNAV_Administrator_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles edit_Milestone_GNAV_Administrator.TextChanged, edit_Milestone_GNAV_Administrator.ValueChanged
    ' ************************************************************************************
    '
    ' ************************************************************************************

    Try
      If (Not InPaint) Then
        If (sender Is edit_Milestone_GNAV_Administrator) Then
          Custom_MS_Valuation_GNAV = True
          edit_Milestone_GNAV_Administrator.BackColor = CUSTOM_BACKGROUND_CHANGED
        End If
      End If
    Catch ex As Exception

    End Try
  End Sub

  ''' <summary>
  ''' Handles the TextChanged event of the edit_Milestone_GNAVUnitPrice_Valuation control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub edit_Milestone_GNAVUnitPrice_Valuation_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles edit_Milestone_GNAVUnitPrice_Valuation.TextChanged, edit_Milestone_GNAVUnitPrice_Valuation.ValueChanged
    ' ************************************************************************************
    '
    ' ************************************************************************************

    Try
      If (Not InPaint) Then
        If (sender Is edit_Milestone_GNAVUnitPrice_Valuation) Then
          Custom_MS_Valuation_GNAV_UnitPrice = True
          edit_Milestone_GNAVUnitPrice_Valuation.BackColor = CUSTOM_BACKGROUND_CHANGED
        End If
      End If
    Catch ex As Exception

    End Try
  End Sub

  ''' <summary>
  ''' Handles the TextChanged event of the edit_Milestone_NAV_Trading control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub edit_Milestone_NAV_Trading_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles edit_Milestone_NAV_Trading.TextChanged, edit_Milestone_NAV_Trading.ValueChanged
    ' ************************************************************************************
    '
    ' ************************************************************************************

    Try
      If (Not InPaint) Then
        If (sender Is edit_Milestone_NAV_Trading) Then
          Custom_MS_Trading_NAV = True
          edit_Milestone_NAV_Trading.BackColor = CUSTOM_BACKGROUND_CHANGED
        End If
      End If
    Catch ex As Exception

    End Try
  End Sub

  ''' <summary>
  ''' Handles the TextChanged event of the edit_Milestone_NAVUnitPrice_Trading control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub edit_Milestone_NAVUnitPrice_Trading_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles edit_Milestone_NAVUnitPrice_Trading.TextChanged, edit_Milestone_NAVUnitPrice_Trading.ValueChanged
    ' ************************************************************************************
    '
    ' ************************************************************************************

    Try
      If (Not InPaint) Then
        If (sender Is edit_Milestone_NAVUnitPrice_Trading) Then
          Custom_MS_Trading_NAV_UnitPrice = True
          edit_Milestone_NAVUnitPrice_Trading.BackColor = CUSTOM_BACKGROUND_CHANGED
        End If
      End If
    Catch ex As Exception

    End Try
  End Sub


#End Region

#Region " Benchmark Tab"

  ''' <summary>
  ''' Handles the KeyDown event of the Grid_Benchmark control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Benchmark_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Grid_Benchmark.KeyDown
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        Try
          If e.Control And (e.KeyCode = Keys.C) Then
            Call MainForm.CopyGridSelection(CType(sender, C1.Win.C1FlexGrid.C1FlexGrid), True, False, True)
          End If
        Catch ex As Exception
        End Try

      End If
    Catch ex As Exception
    End Try
  End Sub

#End Region

#Region " Subscriptions Tab"

  ''' <summary>
  ''' Handles the TextChanged event of the edit_SubscriptionPrice control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub edit_SubscriptionPrice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles edit_SubscriptionPrice.TextChanged
    ' ************************************************************************************
    '
    '
    '
    ' ************************************************************************************

    Try

      If (Not InPaint) Then
        If (sender Is edit_SubscriptionPrice) Then
          CustomSubscriptionPrice = True
          Me.edit_SubscriptionPrice.BackColor = CUSTOM_BACKGROUND_CHANGED
        End If
      End If

    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Handles the ValueChanged event of the edit_SubscriptionPrice control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub edit_SubscriptionPrice_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles edit_SubscriptionPrice.ValueChanged
    ' ************************************************************************************
    '
    '
    '
    ' ************************************************************************************

    Try

      If (Not InPaint) Then

        If (sender Is edit_SubscriptionPrice) Then
          CustomSubscriptionPrice = True
          Me.edit_SubscriptionPrice.BackColor = CUSTOM_BACKGROUND_CHANGED
        End If

      End If

    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Handles the Click event of the btn_Updatesubscriptions control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btn_Updatesubscriptions_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Updatesubscriptions.Click
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Try

      Call SaveSubscriptionsChanges()

    Catch ex As Exception
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Updating Subscriptions.", ex.StackTrace, True)
    End Try
  End Sub

  ''' <summary>
  ''' Saves the subscriptions changes.
  ''' </summary>
  Private Sub SaveSubscriptionsChanges()
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Try
      If (Me.Disposing) OrElse (Me.IsDisposed) Then '
        Exit Sub
      End If
    Catch ex As Exception
    End Try

    Try
      ' Check permissions.
      Dim Permissions As Integer = MainForm.CheckPermissions(Me.Name(), PermissionFeatureType.TypeForm)

      If ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) = 0) AndAlso ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) = 0) Then
        MessageBox.Show("You do not have Permissions to change transactions.")
        Exit Sub
      End If
    Catch ex As Exception
      Exit Sub
    End Try

    Try
      Dim TransactionDS As RenaissanceDataClass.DSTransaction
      Dim TransactionTable As DSTransaction.tblTransactionDataTable
      Dim SelectedTransactions() As RenaissanceDataClass.DSTransaction.tblTransactionRow = Nothing
      Dim TransactionRow As RenaissanceDataClass.DSTransaction.tblTransactionRow = Nothing
      Dim Selection As C1.Win.C1FlexGrid.RowCollection
      Dim DResult As DialogResult
      Dim thisRow As C1.Win.C1FlexGrid.Row
      Dim thisTransactionParentID As Integer

      If Radio_Transactions_All.Checked Then
        If Grid_Subscriptions.Rows.Count > 1 Then
          DResult = MessageBox.Show("Update ALL of the pending Subscriptions and Redemptions ?", "Update ALL Orders", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2)
          Selection = Grid_Subscriptions.Rows
        Else
          Exit Sub
        End If
      Else
        Selection = Grid_Subscriptions.Rows.Selected

        If (Selection.Count >= 2) OrElse ((Selection.Count = 1) AndAlso (Selection(0).SafeIndex > 0)) Then
          DResult = MessageBox.Show("Update the " & Selection.Count.ToString & " selected pending Subscription" & IIf(Selection.Count.ToString > 1, "s and ", " or ") & "Redemption" & IIf(Selection.Count.ToString > 1, "s", "") & " ?", "Update Selected Orders", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2)
        Else
          DResult = MessageBox.Show("There are no Transaction trades selected.", "Commit ALL Transactions", MessageBoxButtons.OK, MessageBoxIcon.Information)
          Exit Sub
        End If
      End If

      If (DResult = Windows.Forms.DialogResult.OK) AndAlso (Selection IsNot Nothing) AndAlso (Selection.Count > 0) Then

        TransactionDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblTransaction)

        If (TransactionDS Is Nothing) Then
          Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, "", "Failed to load tblTransaction, can't update.", "", True)
          Exit Sub
        End If

        TransactionTable = TransactionDS.tblTransaction

        Dim UpdateString As String = ""
        Dim PostedUpdate As Boolean = False
        Dim TransactionChanged As Boolean = False

        For Each thisRow In Selection

          Try
            ' Ignore first Row (Headings)
            If (thisRow.Index > 0) Then

              thisTransactionParentID = CInt(thisRow("TransactionParentID"))

              SelectedTransactions = TransactionTable.Select("TransactionParentID=" & thisTransactionParentID.ToString())
              TransactionChanged = False

              If (SelectedTransactions IsNot Nothing) AndAlso (SelectedTransactions.Length > 0) Then

                If (SelectedTransactions(0).TransactionPrice <> edit_SubscriptionPrice.Value) Then
                  SelectedTransactions(0).TransactionPrice = edit_SubscriptionPrice.Value
                  TransactionChanged = True
                End If

                If (Combo_TradeStatus.Items.Count > 1) AndAlso (IsNumeric(Combo_TradeStatus.SelectedValue)) AndAlso (CInt(Combo_TradeStatus.SelectedValue) > 0) Then
                  If (SelectedTransactions(0).TransactionTradeStatusID <> CInt(Combo_TradeStatus.SelectedValue)) Then
                    SelectedTransactions(0).TransactionTradeStatusID = CInt(Combo_TradeStatus.SelectedValue)
                    TransactionChanged = True
                  End If
                End If

                If (TransactionChanged) Then

                  MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblTransaction, SelectedTransactions)

                  UpdateString &= thisTransactionParentID.ToString("###0") & ","
                  PostedUpdate = True

                End If

              End If

            End If

          Catch ex As Exception
            Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in SaveSubscriptionsChanges().", ex.StackTrace, True)
          End Try

        Next

        If (PostedUpdate) Then
          MainForm.Main_RaiseEvent(New RenaissanceUpdateEventArgs(RenaissanceChangeID.tblTransaction, UpdateString))
        End If

      End If

    Catch ex As Exception
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in SaveSubscriptionsChanges().", ex.StackTrace, True)
    End Try
  End Sub

  ''' <summary>
  ''' Handles the DoubleClick event of the Grid_Transactions control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Transactions_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid_Subscriptions.DoubleClick, Grid_PreviousSubscriptions.DoubleClick
    ' ***************************************************************************************
    ' Spawn an Add/Edit Transactions form reflecting the chosen selection of transactions
    ' and pointing to the transaction which was double clicked.
    ' ***************************************************************************************

    Try

      Dim ParentID As Integer
      Dim thisFormHandle As RenaissanceGlobals.RenaissanceFormHandle

      ParentID = CType(sender, C1.Win.C1FlexGrid.C1FlexGrid).Rows(CType(sender, C1.Win.C1FlexGrid.C1FlexGrid).RowSel).DataSource("TransactionParentID")

      thisFormHandle = MainForm.New_VeniceForm(VeniceFormID.frmTransaction)
      CType(thisFormHandle.Form, frmTransaction).FormSelectCriteria = "TransactionParentID=" & ParentID.ToString
      CType(thisFormHandle.Form, frmTransaction).MoveToAuditID(ParentID)


    Catch ex As Exception
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error opening Transactions Form.", ex.StackTrace, True)
    End Try
  End Sub

  ''' <summary>
  ''' Handles the KeyDown event of the Grid_Transactions control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Transactions_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Grid_Subscriptions.KeyDown
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        Try
          If e.Control And (e.KeyCode = Keys.C) Then
            Call MainForm.CopyGridSelection(CType(sender, C1.Win.C1FlexGrid.C1FlexGrid), True, False, True)
          End If
        Catch ex As Exception
        End Try

      End If
    Catch ex As Exception
    End Try
  End Sub

#End Region

#Region " Notional report Menu"

  ''' <summary>
  ''' Sets the field select menu.
  ''' </summary>
  ''' <param name="RootMenu">The root menu.</param>
  ''' <returns>ToolStripMenuItem.</returns>
  Private Function SetFieldSelectMenu(ByRef RootMenu As MenuStrip) As ToolStripMenuItem
    ' ***************************************************************************************
    ' Build the FieldSelect Menu.
    '
    ' Add an item for each field in the SelectTransactions table, when selected the item
    ' will show or hide the associated field on the transactions grid.
    ' ***************************************************************************************

    Dim ColumnNames(-1) As String
    Dim ColumnCount As Integer

    Dim FieldsMenu As New ToolStripMenuItem("Grid &Fields")
    FieldsMenu.Name = "Menu_GridField"

    Dim newMenuItem As ToolStripMenuItem

    ReDim ColumnNames(myNotionalsTable.Columns.Count - 1)
    For ColumnCount = 0 To (myNotionalsTable.Columns.Count - 1)
      ColumnNames(ColumnCount) = myNotionalsTable.Columns(ColumnCount).ColumnName
      If (ColumnNames(ColumnCount).StartsWith("Transaction")) Then
        ColumnNames(ColumnCount) = ColumnNames(ColumnCount).Substring(11) & "."
      End If
    Next
    Array.Sort(ColumnNames)

    For ColumnCount = 0 To (ColumnNames.Length - 1)
      newMenuItem = FieldsMenu.DropDownItems.Add(ColumnNames(ColumnCount), Nothing, AddressOf Me.ShowGridField)

      If (ColumnNames(ColumnCount).EndsWith(".")) Then
        newMenuItem.Name = "Menu_GridField_Transaction" & ColumnNames(ColumnCount).Substring(0, ColumnNames(ColumnCount).Length - 1)
      Else
        newMenuItem.Name = "Menu_GridField_" & ColumnNames(ColumnCount)
      End If
    Next

    RootMenu.Items.Add(FieldsMenu)
    Return FieldsMenu

  End Function

  ''' <summary>
  ''' Shows the grid field.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
  Private Sub ShowGridField(ByVal sender As Object, ByVal e As EventArgs)
    ' ***************************************************************************************
    ' Callback function for the Grid Fields Menu items.
    '
    ' ***************************************************************************************

    Try
      If TypeOf (sender) Is ToolStripMenuItem Then
        Dim thisMenuItem As ToolStripMenuItem
        Dim FieldName As String

        thisMenuItem = CType(sender, ToolStripMenuItem)

        FieldName = thisMenuItem.Name
        If (FieldName.StartsWith("Menu_GridField_")) Then
          FieldName = FieldName.Substring(15)
        End If

        thisMenuItem.Checked = Not thisMenuItem.Checked

        If (thisMenuItem.Checked) Then
          Grid_NotionalExposure.Cols(FieldName).Visible = True
        Else
          Grid_NotionalExposure.Cols(FieldName).Visible = False
        End If
      End If
    Catch ex As Exception

    End Try
  End Sub

#End Region

#Region " Bug hunting "

  ' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
  ' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
  ' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

  ''' <summary>
  ''' Called when [row updating].
  ''' </summary>
  ''' <param name="Sender">The sender.</param>
  ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  ''' <summary>
  ''' Called when [row updated].
  ''' </summary>
  ''' <param name="Sender">The sender.</param>
  ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  ''' <summary>
  ''' Called when [row fill error].
  ''' </summary>
  ''' <param name="Sender">The sender.</param>
  ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
    Dim a As Integer

    a = 1
  End Sub

#End Region

  ''' <summary>
  ''' Class AdministratorPositions
  ''' </summary>
  Private Class AdministratorPositions

  End Class

  ''' <summary>
  ''' Class PositionSummaryClass
  ''' </summary>
  Private Class PositionSummaryClass
    ''' <summary>
    ''' The fund ID
    ''' </summary>
    Public FundID As Integer
    ''' <summary>
    ''' The fund name
    ''' </summary>
    Public FundName As String
    ''' <summary>
    ''' The sub fund ID
    ''' </summary>
    Public SubFundID As Integer
    ''' <summary>
    ''' The sub fund name
    ''' </summary>
    Public SubFundName As String
    ''' <summary>
    ''' The sub fund level
    ''' </summary>
    Public SubFundLevel As Integer
    ''' <summary>
    ''' The instrument ID
    ''' </summary>
    Public InstrumentID As UInteger
    ''' <summary>
    ''' The instrument description
    ''' </summary>
    Public InstrumentDescription As String
    ''' <summary>
    ''' The instrument ISIN
    ''' </summary>
    Public InstrumentISIN As String
    ''' <summary>
    ''' The instrument type
    ''' </summary>
    Public InstrumentType As Integer
    ''' <summary>
    ''' The instrument type futures style pricing
    ''' </summary>
    Public InstrumentTypeFuturesStylePricing As Boolean
    ''' <summary>
    ''' The instrument currency ID
    ''' </summary>
    Public InstrumentCurrencyID As Integer
    ''' <summary>
    ''' The instrument price
    ''' </summary>
    Public InstrumentPrice As Double
    ''' <summary>
    ''' The FX to fund
    ''' </summary>
    Public FXToFund As Double
    ''' <summary>
    ''' The booked notional
    ''' </summary>
    Public BookedNotional As Double
    ''' <summary>
    ''' The parent ID
    ''' </summary>
    Public ParentID As Integer
    ''' <summary>
    ''' The counter
    ''' </summary>
    Public Counter As Integer
    ''' <summary>
    ''' The instrument currency
    ''' </summary>
    Public InstrumentCurrency As String
    ''' <summary>
    ''' The instrument position
    ''' </summary>
    Public InstrumentPosition As Double
    ''' <summary>
    ''' The instrument local value
    ''' </summary>
    Public InstrumentLocalValue As Double
    ''' <summary>
    ''' The instrument fund value
    ''' </summary>
    Public InstrumentFundValue As Double
    ''' <summary>
    ''' The instrument multiplier
    ''' </summary>
    Public InstrumentMultiplier As Double
    ''' <summary>
    ''' The dummy trade
    ''' </summary>
    Public DummyTrade As Boolean
    ''' <summary>
    ''' The table row
    ''' </summary>
    Public TableRow As DataRow
    ''' <summary>
    ''' The matched futures position
    ''' </summary>
    Public MatchedFuturesPosition As Boolean

    ''' <summary>
    ''' Initializes a new instance of the <see cref="PositionSummaryClass"/> class.
    ''' </summary>
    Public Sub New()
      FundID = 0
      FundName = ""
      SubFundID = 0
      SubFundName = ""
      SubFundLevel = 0
      InstrumentID = 0
      InstrumentDescription = ""
      InstrumentISIN = ""
      InstrumentType = 0
      InstrumentTypeFuturesStylePricing = False
      InstrumentCurrencyID = 0
      InstrumentPrice = 0.0#
      FXToFund = 1.0#
      BookedNotional = 0.0#
      ParentID = 0
      Counter = 0
      InstrumentCurrency = ""
      InstrumentPosition = 0.0#
      'InstrumentSettlementValue = 0.0#
      InstrumentLocalValue = 0.0#
      InstrumentFundValue = 0.0#
      InstrumentMultiplier = 1.0#
      TableRow = Nothing
      DummyTrade = False
      MatchedFuturesPosition = False
    End Sub

  End Class

  ''' <summary>
  ''' Class PositionSummaryComparerByAbsInstrumentPosition
  ''' </summary>
  Private Class PositionSummaryComparerByAbsInstrumentPosition
    ' **********************************************************************************
    ' Custom Comparer calss 
    '  Sorts by InstrumentPosition DESCENDING
    ' **********************************************************************************

    Implements IComparer

    ''' <summary>
    ''' Compares two objects and returns a value indicating whether one is less than, equal to, or greater than the other.
    ''' </summary>
    ''' <param name="x">The first object to compare.</param>
    ''' <param name="y">The second object to compare.</param>
    ''' <returns>Value Condition Less than zero <paramref name="x" /> is less than <paramref name="y" />. Zero <paramref name="x" /> equals <paramref name="y" />. Greater than zero <paramref name="x" /> is greater than <paramref name="y" />.</returns>
    Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare
      Dim X_Instrument As PositionSummaryClass
      Dim Y_Instrument As PositionSummaryClass

      Dim RVal As Integer = 0

      Try
        If (TypeOf x Is PositionSummaryClass) Then
          X_Instrument = CType(x, PositionSummaryClass)
        Else
          Return 0
        End If

        If (TypeOf y Is PositionSummaryClass) Then
          Y_Instrument = CType(y, PositionSummaryClass)
        Else
          Return 0
        End If

        RVal = Math.Abs(Y_Instrument.InstrumentPosition).CompareTo(Math.Abs(X_Instrument.InstrumentPosition))

      Catch ex As Exception

        Return 0
      End Try

      Return RVal

    End Function
  End Class

  ''' <summary>
  ''' Calculates the fund valuation.
  ''' </summary>
  ''' <param name="ReturnTable">The DataTable to return the Fund Valuation details in, may be given as Null, in which case a new DataTable is created. Also the return value of this function.</param>
  ''' <param name="FundID">The Venice Fund ID.</param>
  ''' <param name="ValueDate">The Value date.</param>
  ''' <param name="StatusGroupFilter">The Statusgroup filter.</param>
  ''' <param name="AdministratorDatesFilter">The administrator dates filter.</param>
  ''' <param name="UseAdministratorPriceDates">if set to <c>true</c> then use administrator price dates.</param>
  ''' <returns>DataTable.</returns>
  Private Function CalculateFundValuation(ByVal ReturnTable As DataTable, ByVal FundValuationUnitCounts As Dictionary(Of Integer, SR_InformationClass), ByVal FundID As Integer, ByVal ValueDate As Date, ByVal StatusGroupFilter As String, ByVal AdministratorDatesFilter As Integer, ByVal UseAdministratorPriceDates As Boolean) As DataTable
    ' *******************************************************************************
    '
    '
    ' @AdministratorDatesFilter : Refine transaction selection according to the given value.
    '--										This Field is a bitmap, the following bit values modify behaviour to match 
    '--										various selection criteria required for administration and reconcilliation.
    '--										1		- Ignore the transaction status for Subscriptions and Redemptions (Should a
    '--										2		- Ignore the Subscriptions and Redemptions on (or after of course) the given Value Date
    '
    ' The locking in this function is largely un-necessary at present, but I was thinking of executing this on a worker thread which would require the locking that is in place.
    '
    ' *******************************************************************************

    Const OmitFXForwardsFromCash As Boolean = True

    Try
      If ReturnTable Is Nothing Then
        ReturnTable = New DataTable
      End If

      If FundValuationUnitCounts Is Nothing Then
        FundValuationUnitCounts = New Dictionary(Of Integer, SR_InformationClass)
      End If

      ReturnTable.Clear()
      FundValuationUnitCounts.Clear()

      If FundID = 0 Then
        Return ReturnTable
      End If

      ' Lock 

      If (Not lock_tblFundValuation.IsWriteLockHeld) Then
        lock_tblFundValuation.EnterWriteLock()
      End If

      ' Reference :

      Dim FXForward_SubTypes() As String = {"ZFX", "26000"} ' Used to identify FX Forward Administrator entries.

      ' Variables.
      Dim TransactionDataset As RenaissanceDataClass.DSTransaction = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblTransaction), RenaissanceDataClass.DSTransaction)
      Dim TransactionDataView As DataView
      Dim ContraTransactionDataset As RenaissanceDataClass.DSTransactionContra = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblTransactionContra), RenaissanceDataClass.DSTransactionContra)
      Dim ContraTransactionDataView As DataView
      Dim SelectString As String
      Dim OrderString As String

      Dim FuturesNotionalsRow As RenaissanceDataClass.DSFuturesNotional.tblFuturesNotionalRow
      Dim thisInstrumentRow As RenaissanceDataClass.DSInstrument.tblInstrumentRow = Nothing
      Dim CurrencyCodeDictionary As New Dictionary(Of String, DSCurrency.tblCurrencyRow)
      Dim thisCurrencyRow As DSCurrency.tblCurrencyRow = Nothing
      Dim VariationMarginModifierDictionary As New Dictionary(Of String, Double)
      Dim VariationMarginModifierRows As New Dictionary(Of String, DataRow)
      Dim MarginDepositMarginModifierRows As New Dictionary(Of String, DataRow)

      Dim HasStatusGroupFilter As Boolean = False
      Dim StatusGroupID As Integer
      Dim StatusGroupHash As New HashSet(Of Integer)
      Dim ExcludeTodaysSubscriptions As Boolean = False

      Dim PositionDictionary As New Dictionary(Of Integer, PositionSummaryClass)
      Dim thisPosition As PositionSummaryClass = Nothing
      Dim InstrumentTypeFuturesStyleDictionary As New Dictionary(Of Integer, Boolean)

      Dim VeniceFXForwardsDictionary As New Dictionary(Of Integer, FXForwardPosition)
      Dim thisVeniceFXForward As FXForwardPosition = Nothing
      Dim thisAdminFXForward As FXForwardPosition = Nothing
      Dim AdministratorFXForwardsDictionary As New Dictionary(Of String, FXForwardPosition)
      Dim VeniceProvisionsTotals As New Dictionary(Of String, Double)

      Dim SelectedAvailabilities() As DSRecAvailabilities.tblRecAvailabilitiesRow
      Dim thisAvailability As DSRecAvailabilities.tblRecAvailabilitiesRow

      Dim FundCurrencyID As Integer = 0
      Dim FundCurrencyCode As String = ""

      Dim NewReturnRow As DataRow = Nothing
      Dim CashRecordReturnRow As DataRow = Nothing
      Dim AvailabilityReturnRow As DataRow = Nothing
      Dim SelectedReturnRows() As DataRow = Nothing

      Dim ReturnTable_rptISIN As Integer
      Dim ReturnTable_rptCurrency As Integer
      Dim ReturnTable_rptDescription As Integer
      Dim ReturnTable_rptInstrumentType As Integer
      Dim ReturnTable_InstrumentTypeID As Integer
      Dim ReturnTable_InstrumentType As Integer
      Dim ReturnTable_Instrument As Integer
      Dim ReturnTable_CompoundFXRate As Integer
      Dim ReturnTable_Value As Integer
      Dim ReturnTable_SignedUnits As Integer
      Dim ReturnTable_PriceLevel As Integer
      Dim ReturnTable_PriceDate As Integer
      Dim ReturnTable_BookedNotional As Integer
      Dim ReturnTable_neo_captureID As Integer
      Dim ReturnTable_neo_marketvalue_rc_value As Integer
      Dim ReturnTable_neo_quantity_value As Integer
      Dim ReturnTable_neo_exchangerate_value As Integer
      Dim ReturnTable_neo_price_value As Integer
      Dim ReturnTable_neo_pricedate_value As Integer
      Dim ReturnTable_SectionSort As Integer

      ' ************************************************************************
      ' Initialise table if necessary.
      ' ************************************************************************

      Try
        If (Not ReturnTable.Columns.Contains("rpt_ISIN")) Then ReturnTable.Columns.Add("rpt_ISIN", GetType(String))
        If (Not ReturnTable.Columns.Contains("rpt_Currency")) Then ReturnTable.Columns.Add("rpt_Currency", GetType(String))
        If (Not ReturnTable.Columns.Contains("rpt_Description")) Then ReturnTable.Columns.Add("rpt_Description", GetType(String))
        If (Not ReturnTable.Columns.Contains("rpt_InstrumentType")) Then ReturnTable.Columns.Add("rpt_InstrumentType", GetType(String))
        If (Not ReturnTable.Columns.Contains("InstrumentTypeID")) Then ReturnTable.Columns.Add("InstrumentTypeID", GetType(Integer))
        If (Not ReturnTable.Columns.Contains("InstrumentType")) Then ReturnTable.Columns.Add("InstrumentType", GetType(String))
        If (Not ReturnTable.Columns.Contains("Instrument")) Then ReturnTable.Columns.Add("Instrument", GetType(Integer))
        If (Not ReturnTable.Columns.Contains("CompoundFXRate")) Then ReturnTable.Columns.Add("CompoundFXRate", GetType(Double))
        If (Not ReturnTable.Columns.Contains("Value")) Then ReturnTable.Columns.Add("Value", GetType(Double))
        If (Not ReturnTable.Columns.Contains("SignedUnits")) Then ReturnTable.Columns.Add("SignedUnits", GetType(Double))
        If (Not ReturnTable.Columns.Contains("BookedNotional")) Then ReturnTable.Columns.Add("BookedNotional", GetType(Double))
        If (Not ReturnTable.Columns.Contains("PriceLevel")) Then ReturnTable.Columns.Add("PriceLevel", GetType(Double))
        If (Not ReturnTable.Columns.Contains("PriceDate")) Then ReturnTable.Columns.Add("PriceDate", GetType(Date))
        If (Not ReturnTable.Columns.Contains("neo_captureID")) Then ReturnTable.Columns.Add("neo_captureID", GetType(Integer))
        If (Not ReturnTable.Columns.Contains("neo_marketvalue_rc_value")) Then ReturnTable.Columns.Add("neo_marketvalue_rc_value", GetType(Double))
        If (Not ReturnTable.Columns.Contains("neo_quantity_value")) Then ReturnTable.Columns.Add("neo_quantity_value", GetType(Double))
        If (Not ReturnTable.Columns.Contains("neo_exchangerate_value")) Then ReturnTable.Columns.Add("neo_exchangerate_value", GetType(Double))
        If (Not ReturnTable.Columns.Contains("neo_price_value")) Then ReturnTable.Columns.Add("neo_price_value", GetType(Double))
        If (Not ReturnTable.Columns.Contains("neo_pricedate_value")) Then ReturnTable.Columns.Add("neo_pricedate_value", GetType(Date))
        If (Not ReturnTable.Columns.Contains("SectionSort")) Then ReturnTable.Columns.Add("SectionSort", GetType(Integer))

        ReturnTable_rptISIN = ReturnTable.Columns("rpt_ISIN").Ordinal
        ReturnTable_rptCurrency = ReturnTable.Columns("rpt_Currency").Ordinal
        ReturnTable_rptDescription = ReturnTable.Columns("rpt_Description").Ordinal
        ReturnTable_rptInstrumentType = ReturnTable.Columns("rpt_InstrumentType").Ordinal
        ReturnTable_InstrumentTypeID = ReturnTable.Columns("InstrumentTypeID").Ordinal
        ReturnTable_InstrumentType = ReturnTable.Columns("InstrumentType").Ordinal
        ReturnTable_Instrument = ReturnTable.Columns("Instrument").Ordinal
        ReturnTable_CompoundFXRate = ReturnTable.Columns("CompoundFXRate").Ordinal
        ReturnTable_Value = ReturnTable.Columns("Value").Ordinal
        ReturnTable_SignedUnits = ReturnTable.Columns("SignedUnits").Ordinal
        ReturnTable_PriceLevel = ReturnTable.Columns("PriceLevel").Ordinal
        ReturnTable_PriceDate = ReturnTable.Columns("PriceDate").Ordinal
        ReturnTable_BookedNotional = ReturnTable.Columns("BookedNotional").Ordinal
        ReturnTable_neo_captureID = ReturnTable.Columns("neo_captureID").Ordinal
        ReturnTable_neo_marketvalue_rc_value = ReturnTable.Columns("neo_marketvalue_rc_value").Ordinal
        ReturnTable_neo_quantity_value = ReturnTable.Columns("neo_quantity_value").Ordinal
        ReturnTable_neo_exchangerate_value = ReturnTable.Columns("neo_exchangerate_value").Ordinal
        ReturnTable_neo_price_value = ReturnTable.Columns("neo_price_value").Ordinal
        ReturnTable_neo_pricedate_value = ReturnTable.Columns("neo_pricedate_value").Ordinal
        ReturnTable_SectionSort = ReturnTable.Columns("SectionSort").Ordinal

      Catch ex As Exception
      End Try

      ' ************************************************************************
      ' 'Parameter' valuse
      ' ************************************************************************

      FundCurrencyID = CInt(Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblFund, FundID, "FundBaseCurrency"), 0))
      ' Select this Fund excluding Subscriptions and redemptions (but includes the cash leg)
      SelectString = "(TransactionFund=" & FundID.ToString("###0") & ")" ' AND (TransactionType<>2) AND (TransactionType<>3)"
      OrderString = "TransactionFund, TransactionInstrument"

      TransactionDataView = New DataView(TransactionDataset.tblTransaction, SelectString, OrderString, DataViewRowState.CurrentRows)
      ContraTransactionDataView = New DataView(ContraTransactionDataset.tblTransactionContra, SelectString, OrderString, DataViewRowState.CurrentRows)

      ' OK, Some Prep work...

      Try
        Dim InstrumentTypeDS As DSInstrumentType = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblInstrumentType), DSInstrumentType)
        Dim InstrumentTypeRow As DSInstrumentType.tblInstrumentTypeRow

        If (InstrumentTypeDS IsNot Nothing) Then
          For Each InstrumentTypeRow In InstrumentTypeDS.tblInstrumentType.Rows
            InstrumentTypeFuturesStyleDictionary.Add(InstrumentTypeRow.InstrumentTypeID, InstrumentTypeRow.InstrumentTypeFuturesStylePricing)
          Next
        End If
      Catch ex As Exception
      End Try

      Try
        Dim CurrencyDS As DSCurrency = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblCurrency), DSCurrency)
        Dim CurrencyRow As DSCurrency.tblCurrencyRow

        If (CurrencyDS IsNot Nothing) Then
          For Each CurrencyRow In CurrencyDS.tblCurrency.Rows
            CurrencyCodeDictionary.Add(CurrencyRow.CurrencyCode, CurrencyRow)

            If (FundCurrencyID = CurrencyRow.CurrencyID) Then
              FundCurrencyCode = CurrencyRow.CurrencyCode
            End If
          Next
        End If
      Catch ex As Exception
      End Try

      If (AdministratorDatesFilter And RenaissanceGlobals.AdministratorDatesFilter.SubscriptionRedemption_IgnoreTodaysTrades) Then
        ExcludeTodaysSubscriptions = True
      End If

      If (StatusGroupFilter.Length > 0) Then
        StatusGroupID = CInt(Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblTradeStatusGroup, 0, "TradeStatusGroupID", "RN", "TradeStatusGroupName='" & StatusGroupFilter & "'"), 0))

        If (StatusGroupID) > 0 Then

          Dim StatusItemsDS As DSTradeStatusGroupItems = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblTradeStatusGroupItems), DSTradeStatusGroupItems)
          Dim StatusItemsRow As DSTradeStatusGroupItems.tblTradeStatusGroupItemsRow

          HasStatusGroupFilter = True

          If (StatusItemsDS IsNot Nothing) Then

            For Each StatusItemsRow In StatusItemsDS.tblTradeStatusGroupItems.Rows
              If (CInt(StatusItemsRow("GroupID")) = StatusGroupID) Then
                StatusGroupHash.Add(StatusItemsRow.StatusID)
              End If
            Next

          End If

        End If ' (StatusGroupID) > 0

      End If ' StatusGroupFilter.Length > 0

      ' ************************************************************************
      ' Fund Availabilities.
      ' ************************************************************************

      If (FundAvailabilities Is Nothing) Then
        FundAvailabilities = GetAvailabilities(lock_tblAvailabilities, FundID, ValueDate)
      End If

      Dim AvailabilityCurrencies As New HashSet(Of String)

      Try
        lock_tblAvailabilities.EnterReadLock()

        For Each thisAvailability In FundAvailabilities.Rows
          If (thisAvailability.Currency.Trim.Length > 2) AndAlso (thisAvailability.Currency.Trim.Length <= 4) Then
            AvailabilityCurrencies.Add(thisAvailability.Currency.Trim)
          End If
        Next

      Catch ex As Exception
      Finally
        If (lock_tblAvailabilities.IsReadLockHeld) Then
          lock_tblAvailabilities.ExitReadLock()
        End If
      End Try


      ' ************************************************************************
      ' Futures Notionals.
      ' ************************************************************************

      If (FuturesNotionalsTable Is Nothing) Then
        FuturesNotionalsTable = New RenaissanceDataClass.DSFuturesNotional.tblFuturesNotionalDataTable

        Call LoadFuturesNotional(MainForm, lock_tblFuturesNotionals, FuturesNotionalsTable, FundID, ValueDate, StatusGroupFilter, AdministratorDatesFilter, CalculationTypes_Profit.FIFO)
      End If


      ' ************************************************************************
      ' Get Relevant transaction sets.
      ' ************************************************************************

      Dim DataViewCount As Integer
      Dim thisDataView As DataView
      Dim thisRowView As DataRowView
      Dim thisTransactionRow As DataRow = Nothing
      Dim Counter As Integer
      Dim thisSRInfoClass As SR_InformationClass = Nothing

      Dim Ordinal_Fund As Integer
      Dim Ordinal_Instrument As Integer
      Dim Ordinal_TransactionType As Integer
      Dim Ordinal_TransactionType_Contra As Integer
      Dim Ordinal_SignedUnits As Integer
      Dim Ordinal_SignedSettlement As Integer
      Dim Ordinal_TradeStatusID As Integer
      Dim Ordinal_ValueDate As Integer
      Dim Ordinal_SettlementDate As Integer
      Dim Ordinal_TransactionParentID As Integer

      Dim thisValueDate As Date
      Dim thisSettlementDate As Date
      Dim thisTransactionParentID As Integer
      Dim thisFundID As Integer
      Dim thisTransactionType As Integer
      Dim thisTransactionType_Contra As Integer
      Dim thisTradeStatusID As Integer
      Dim thisInstrumentID As Integer
      Dim thisSignedUnits As Double
      Dim thisSignedSettlement As Double
      Dim thisIsFxForward As Boolean
      Dim thisIsProvision As Boolean
      Dim SaveThisTransaction As Boolean

      For DataViewCount = 0 To 1

        If (DataViewCount = 0) Then
          thisDataView = TransactionDataView
        Else
          thisDataView = ContraTransactionDataView
        End If

        Ordinal_Fund = thisDataView.Table.Columns("TransactionFund").Ordinal
        Ordinal_Instrument = thisDataView.Table.Columns("TransactionInstrument").Ordinal
        Ordinal_TransactionType = thisDataView.Table.Columns("TransactionType").Ordinal
        Ordinal_TransactionType_Contra = thisDataView.Table.Columns("TransactionType_Contra").Ordinal
        Ordinal_SignedUnits = thisDataView.Table.Columns("TransactionSignedUnits").Ordinal
        Ordinal_SignedSettlement = thisDataView.Table.Columns("TransactionSignedSettlement").Ordinal
        Ordinal_TradeStatusID = thisDataView.Table.Columns("TransactionTradeStatusID").Ordinal
        Ordinal_ValueDate = thisDataView.Table.Columns("TransactionValueDate").Ordinal
        Ordinal_SettlementDate = thisDataView.Table.Columns("TransactionSettlementDate").Ordinal
        Ordinal_TransactionParentID = thisDataView.Table.Columns("TransactionParentID").Ordinal

        ' Loop through the selected data (DataView) building an ArrayList of aggregated Instrument positions.

        For Counter = 0 To (thisDataView.Count - 1) ' 

          thisRowView = thisDataView.Item(Counter)
          thisTransactionRow = thisRowView.Row

          thisTransactionParentID = CInt(thisTransactionRow(Ordinal_TransactionParentID))
          thisFundID = CInt(thisTransactionRow(Ordinal_Fund))
          thisTransactionType = CInt(thisTransactionRow(Ordinal_TransactionType))
          thisTransactionType_Contra = CInt(thisTransactionRow(Ordinal_TransactionType_Contra))
          thisValueDate = CDate(thisTransactionRow(Ordinal_ValueDate)).Date
          thisSettlementDate = CDate(thisTransactionRow(Ordinal_SettlementDate)).Date
          thisInstrumentID = CInt(thisTransactionRow(Ordinal_Instrument))
          thisSignedUnits = CDbl(thisTransactionRow(Ordinal_SignedUnits))
          thisSignedSettlement = CDbl(thisTransactionRow(Ordinal_SignedSettlement))

          ' Check Fund
          If (thisFundID <> FundID) Then
            Continue For
          End If

          If (thisValueDate > ValueDate) Then
            Continue For
          End If

          ' Exclude todays S / R
          If (ExcludeTodaysSubscriptions) AndAlso ((thisTransactionType_Contra = TransactionTypes.Subscribe) OrElse (thisTransactionType_Contra = TransactionTypes.Redeem)) AndAlso (thisValueDate >= ValueDate) Then
            Continue For
          End If

          ' Subscriptions and Redemptions.
          ' Exclude from valuation, but use to record unit counts.

          If ((thisTransactionType = TransactionTypes.Subscribe) OrElse (thisTransactionType = TransactionTypes.Redeem)) Then

            If Not FundValuationUnitCounts.TryGetValue(thisInstrumentID, thisSRInfoClass) Then
              thisSRInfoClass = New SR_InformationClass
              thisSRInfoClass.InstrumentID = thisInstrumentID
              FundValuationUnitCounts.Add(thisInstrumentID, thisSRInfoClass)
            End If

            If (thisTransactionType = TransactionTypes.Subscribe) Then
              thisSRInfoClass.UnitsSubscribed += Math.Abs(thisSignedUnits)
              thisSRInfoClass.ValueSubscribed += Math.Abs(thisSignedSettlement)
            Else
              thisSRInfoClass.UnitsRedeemed += Math.Abs(thisSignedUnits)
              thisSRInfoClass.ValueRedeemed += Math.Abs(thisSignedSettlement)
            End If

            Continue For
          End If

          ' Check Status Group (if not a subscription / redemption)
          If (HasStatusGroupFilter) AndAlso (thisTransactionType_Contra <> TransactionTypes.Subscribe) AndAlso (thisTransactionType_Contra <> TransactionTypes.Redeem) Then
            thisTradeStatusID = CInt(thisTransactionRow(Ordinal_TradeStatusID))

            If (thisTradeStatusID > 0) AndAlso (Not StatusGroupHash.Contains(thisTradeStatusID)) Then
              Continue For
            End If
          End If


          ' ************************************************************************
          ' Trade Type ...
          ' ************************************************************************

          ' FX Forward. (Only interested if it has not expired)

          thisIsFxForward = False
          If (thisTransactionType_Contra = TransactionTypes.BuyFXForward) OrElse (thisTransactionType_Contra = TransactionTypes.SellFXForward) Then
            If (thisSettlementDate > ValueDate) Then
              thisIsFxForward = True
            End If
          End If

          ' Provisions

          thisIsProvision = False
          If (thisTransactionType_Contra = TransactionTypes.Provision_Increase) OrElse (thisTransactionType_Contra = TransactionTypes.Provision_Reduce) Then
            thisIsProvision = True
          End If


          ' ************************************************************************
          ' Get Transaction & Instrument details 
          ' ************************************************************************

          If (thisInstrumentRow Is Nothing) OrElse (thisInstrumentRow.InstrumentID <> thisInstrumentID) Then
            thisInstrumentRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, thisInstrumentID)
            'thisInstrumentPrice = GetInstrumentPrice(thisInstrumentID)
          End If

          ' Skip 'NOTIONAL' Instruments, will be added back later from the Futures Notional table.
          If (thisInstrumentRow.InstrumentType = InstrumentTypes.Notional) Then
            Continue For
          End If

          ' Aggregate this transaction ?

          SaveThisTransaction = True
          If ((OmitFXForwardsFromCash) AndAlso (thisIsFxForward)) Then
            SaveThisTransaction = False
          End If

          ' ************************************************************************
          ' Aggregate
          ' ************************************************************************

          If (SaveThisTransaction) Then

            If (thisPosition Is Nothing) OrElse ((thisPosition.InstrumentID <> thisInstrumentID) AndAlso (Not (PositionDictionary.TryGetValue(thisInstrumentID, thisPosition)))) Then
              thisPosition = New PositionSummaryClass

              thisPosition.FundID = thisFundID
              thisPosition.InstrumentID = thisInstrumentID

              thisPosition.InstrumentType = thisInstrumentRow.InstrumentType
              thisPosition.InstrumentTypeFuturesStylePricing = InstrumentTypeFuturesStyleDictionary(thisInstrumentRow.InstrumentType)
              thisPosition.InstrumentISIN = thisInstrumentRow.InstrumentISIN.ToUpper
              thisPosition.InstrumentCurrency = thisInstrumentRow.InstrumentCurrency.ToUpper
              thisPosition.InstrumentCurrencyID = thisInstrumentRow.InstrumentCurrencyID
              thisPosition.InstrumentDescription = thisInstrumentRow.InstrumentDescription
              thisPosition.InstrumentMultiplier = thisInstrumentRow.InstrumentContractSize * thisInstrumentRow.InstrumentMultiplier
              thisPosition.BookedNotional = 0.0#
              thisPosition.Counter += 1

              PositionDictionary.Add(thisInstrumentID, thisPosition)
            End If

            thisPosition.InstrumentPosition += thisSignedUnits

          End If

          ' ************************************************************************
          ' Record FX Forwards, only those that have not expired though.
          ' ************************************************************************

          If (thisIsFxForward) Then

            If (Not VeniceFXForwardsDictionary.TryGetValue(thisTransactionParentID, thisVeniceFXForward)) Then
              thisVeniceFXForward = New FXForwardPosition

              thisVeniceFXForward.TransactionParentID = thisTransactionParentID
              thisVeniceFXForward.ValueDate = thisSettlementDate

              VeniceFXForwardsDictionary.Add(thisTransactionParentID, thisVeniceFXForward)
            End If

            If (DataViewCount = 0) Then
              thisVeniceFXForward.InstrumentLeg1 = thisInstrumentID
              thisVeniceFXForward.CurrencyLeg1 = thisInstrumentRow.InstrumentCurrencyID
              thisVeniceFXForward.CurrencyCodeLeg1 = thisInstrumentRow.InstrumentCurrency
              thisVeniceFXForward.UnitsLeg1 = thisSignedUnits
            Else
              thisVeniceFXForward.InstrumentLeg2 = thisInstrumentID
              thisVeniceFXForward.CurrencyLeg2 = thisInstrumentRow.InstrumentCurrencyID
              thisVeniceFXForward.CurrencyCodeLeg2 = thisInstrumentRow.InstrumentCurrency
              thisVeniceFXForward.UnitsLeg2 = thisSignedUnits
            End If

          End If ' thisIsFxForward

          '' ************************************************************************
          '' Record Provisions
          '' ************************************************************************

          'If (thisIsProvision) Then
          '  thisInstrumentValue = thisInstrumentPrice * thisSignedUnits * thisInstrumentRow.InstrumentContractSize * thisInstrumentRow.InstrumentMultiplier

          '  If (Not VeniceProvisionsTotals.ContainsKey(thisInstrumentRow.InstrumentCurrency)) Then
          '    VeniceProvisionsTotals.Add(thisInstrumentRow.InstrumentCurrency, thisInstrumentValue)
          '  Else
          '    VeniceProvisionsTotals(thisInstrumentRow.InstrumentCurrency) += thisInstrumentValue
          '  End If

          'End If ' thisIsProvision

        Next ' Counter = 0 To (thisDataView.Count - 1) ' 

      Next ' DataViewCount

      ' Match Booked Notional to each Position
      ' Add Position for Unmatched Notionals
      ' Update NPP 24 Jul 2013
      ' If the Notional booking is changed to Zero Unrealised profit, in order to be able to match the cash correctly, 
      ' then the Futures value is for Information only and we should match to the Unrealised Profit figure, not the Booked Notional.
      ' Also, somehow, the Reconciliation grid must Omit the value for these instruments from the Fund total
      Dim MatchToUnrealisedProfit As Boolean = True

      Try
        lock_tblFuturesNotionals.EnterReadLock()

        For Each FuturesNotionalsRow In FuturesNotionalsTable.Rows
          If (PositionDictionary.TryGetValue(FuturesNotionalsRow.InstrumentID, thisPosition)) Then

            If (MatchToUnrealisedProfit) Then
              thisPosition.BookedNotional = 0.0#
              thisPosition.InstrumentLocalValue += (FuturesNotionalsRow.Local_UnrealisedProfit + FuturesNotionalsRow.NotionalTradeToZeroUnRealisedPnL)

              If (VariationMarginModifierDictionary.ContainsKey(thisPosition.InstrumentCurrency)) Then
                VariationMarginModifierDictionary(thisPosition.InstrumentCurrency) -= FuturesNotionalsRow.Local_UnrealisedProfit + (2 * FuturesNotionalsRow.NotionalTradeToZeroUnRealisedPnL)
              Else
                VariationMarginModifierDictionary.Add(thisPosition.InstrumentCurrency, -(FuturesNotionalsRow.Local_UnrealisedProfit + (2 * FuturesNotionalsRow.NotionalTradeToZeroUnRealisedPnL)))
              End If

              thisPosition.MatchedFuturesPosition = True
            Else
              thisPosition.BookedNotional += FuturesNotionalsRow.BookedNotional
            End If
          Else
            ' Add / Create aggregated Notional Instrument position

            thisInstrumentID = FuturesNotionalsRow.NotionalInstrument

            If (Not PositionDictionary.TryGetValue(thisInstrumentID, thisPosition)) Then
              thisPosition = New PositionSummaryClass

              thisPosition.FundID = FuturesNotionalsRow.FundID
              thisPosition.InstrumentID = thisInstrumentID
              thisPosition.InstrumentType = InstrumentTypes.Future ' Fudge to make it appear in the 'Futures' section.
              thisPosition.InstrumentTypeFuturesStylePricing = False
              thisPosition.BookedNotional = 0.0#
              thisPosition.DummyTrade = True
              thisPosition.InstrumentISIN = thisInstrumentRow.InstrumentISIN.ToUpper
              thisPosition.InstrumentCurrency = thisInstrumentRow.InstrumentCurrency.ToUpper
              thisPosition.InstrumentCurrencyID = thisInstrumentRow.InstrumentCurrencyID
              thisPosition.InstrumentDescription = thisInstrumentRow.InstrumentDescription
              thisPosition.InstrumentMultiplier = thisInstrumentRow.InstrumentContractSize * thisInstrumentRow.InstrumentMultiplier

              PositionDictionary.Add(thisInstrumentID, thisPosition)
            End If

            thisPosition.InstrumentPosition -= FuturesNotionalsRow.BookedNotional

          End If

        Next

      Catch ex As Exception
        MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in CalculateFundValuation().", ex.StackTrace, True)
      Finally
        If (lock_tblFuturesNotionals.IsReadLockHeld) Then
          lock_tblFuturesNotionals.ExitReadLock()
        End If
      End Try

      ' Get Administrator Data

      Dim ThisAdminFundCode As String = CStr(Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblFund, FundID, "FundAdministratorCode"), ""))
      Dim AdministratorInventoryTable As DataTable = Nothing
      Dim AdministratorInventoryTableIsHoldings As Boolean = False

      If (ThisAdminFundCode.Length > 0) Then

        If (Not AdministratorInventoryCache.TryGetValue(FundID, AdministratorInventoryTable)) Then
          Dim SelectCommand As SqlCommand = Nothing

          AdministratorInventoryTable = New DataTable

          Try

            'CREATE PROCEDURE [dbo].[adp_tblRecInventory_SelectKD]
            '   @NeoFundcode nvarchar(50) = '',
            '   @ValueDate date = Null,
            '   @MaxCaptureID int = 0,
            '   @KnowledgeDate datetime = Null

            SelectCommand = New SqlCommand
            SelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT
            SelectCommand.Connection = MainForm.GetVeniceConnection()
            SelectCommand.CommandType = CommandType.StoredProcedure
            SelectCommand.CommandText = "adp_tblRecInventory_SelectKD"

            SelectCommand.Parameters.Add(New SqlParameter("@NeoFundcode", SqlDbType.NVarChar, 50)).Value = ThisAdminFundCode
            SelectCommand.Parameters.Add(New SqlParameter("@ValueDate", SqlDbType.DateTime)).Value = ValueDate
            SelectCommand.Parameters.Add(New SqlParameter("@MaxCaptureID", SqlDbType.Int)).Value = 0
            SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", SqlDbType.DateTime)).Value = MainForm.Main_Knowledgedate

            MainForm.LoadTable_Custom(AdministratorInventoryTable, SelectCommand)
            'AdministratorInventoryTable.Load(SelectCommand.ExecuteReader)

            If (AdministratorInventoryTable.Rows.Count > 0) Then
              If (AdministratorInventoryCache.ContainsKey(FundID)) Then
                AdministratorInventoryCache(FundID) = AdministratorInventoryTable
              Else
                AdministratorInventoryCache.Add(FundID, AdministratorInventoryTable)
              End If
            Else


              If (Not AdministratorHoldingsCache.TryGetValue(FundID, AdministratorInventoryTable)) Then
                AdministratorInventoryTable = New DataTable

                SelectCommand.CommandText = "adp_tblRecHoldingsAsInventory"

                AdministratorInventoryTableIsHoldings = True

                MainForm.LoadTable_Custom(AdministratorInventoryTable, SelectCommand)
                'AdministratorInventoryTable.Load(SelectCommand.ExecuteReader)

                If (AdministratorInventoryTable.Rows.Count > 0) Then
                  If (AdministratorHoldingsCache.ContainsKey(FundID)) Then
                    AdministratorHoldingsCache(FundID) = AdministratorInventoryTable
                  Else
                    AdministratorHoldingsCache.Add(FundID, AdministratorInventoryTable)
                  End If
                End If

              End If

            End If

          Catch ex As Exception
            MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in LoadAdministratorInventory().", ex.StackTrace, False)
          Finally
            If (SelectCommand IsNot Nothing) Then
              If (SelectCommand.Connection IsNot Nothing) AndAlso (SelectCommand.Connection.State And ConnectionState.Open) Then
                SelectCommand.Connection.Close()
              End If
              SelectCommand.Connection = Nothing
            End If
            SelectCommand = Nothing
          End Try

        End If

      End If

      ' ************************************************************************
      ' Get Prices
      ' Use GetInstrumentPriceRow()
      ' ************************************************************************

      ' Stitch it together

      Dim PositionArray() As PositionSummaryClass = PositionDictionary.Values.ToArray
      Dim PricesRow As DataRow
      Dim AdministratorRow As DataRow
      Dim thisFXRate As Double
      Dim thisPrice As Double
      Dim thisPriceDate As Date

      If (PositionArray.Length > 0) Then
        For Counter = 0 To (PositionArray.Length - 1)
          thisPosition = PositionArray(Counter)

          PricesRow = GetBestPriceRow(thisPosition.InstrumentID)
          If (PricesRow Is Nothing) Then
            thisPrice = 0.0#
            thisPriceDate = Renaissance_BaseDate
          Else
            thisPrice = CDbl(Nz(PricesRow("PriceLevel"), 0.0#))
            thisPriceDate = CDate(Nz(PricesRow("PriceDate"), Renaissance_BaseDate))
          End If
          thisPosition.InstrumentPrice = thisPrice
          thisFXRate = GetFXToFund(thisPosition.InstrumentCurrencyID, FundCurrencyID)
          thisPosition.FXToFund = thisFXRate

          ' Fudge Margin to appear in Cash section

          If thisPosition.InstrumentType = InstrumentTypes.Margin Then
            thisPosition.InstrumentType = InstrumentTypes.Cash
          End If

          ' OK, Continue

          NewReturnRow = ReturnTable.NewRow

          NewReturnRow(ReturnTable_rptISIN) = thisPosition.InstrumentISIN
          NewReturnRow(ReturnTable_rptCurrency) = thisPosition.InstrumentCurrency
          NewReturnRow(ReturnTable_rptDescription) = thisPosition.InstrumentDescription
          NewReturnRow(ReturnTable_rptInstrumentType) = CStr(Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrumentType, thisPosition.InstrumentType, "InstrumentTypeDescription"), ""))
          NewReturnRow(ReturnTable_InstrumentTypeID) = thisPosition.InstrumentType

          ' Force Fees to appear in the Cash section (For Provisions)
          If (thisPosition.InstrumentType = InstrumentTypes.Fee) Then
            NewReturnRow(ReturnTable_rptInstrumentType) = InstrumentTypes.Cash.ToString
            NewReturnRow(ReturnTable_InstrumentTypeID) = InstrumentTypes.Cash
          End If

          NewReturnRow(ReturnTable_InstrumentType) = NewReturnRow(ReturnTable_rptInstrumentType)
          NewReturnRow(ReturnTable_Instrument) = thisPosition.InstrumentID

          NewReturnRow(ReturnTable_PriceLevel) = thisPrice
          NewReturnRow(ReturnTable_PriceDate) = thisPriceDate
          NewReturnRow(ReturnTable_BookedNotional) = thisPosition.BookedNotional
          NewReturnRow(ReturnTable_CompoundFXRate) = thisFXRate
          NewReturnRow(ReturnTable_SignedUnits) = thisPosition.InstrumentPosition

          If (thisPosition.MatchedFuturesPosition) Then
            NewReturnRow(ReturnTable_Value) = (thisPosition.InstrumentLocalValue) * thisFXRate
          Else
            NewReturnRow(ReturnTable_Value) = ((thisPosition.InstrumentPosition * thisPrice * thisPosition.InstrumentMultiplier) + (IIf(thisPosition.InstrumentTypeFuturesStylePricing, thisPosition.BookedNotional, 0.0#))) * thisFXRate
          End If

          NewReturnRow(ReturnTable_neo_captureID) = 0
          NewReturnRow(ReturnTable_neo_marketvalue_rc_value) = 0.0#
          NewReturnRow(ReturnTable_neo_quantity_value) = 0.0#
          NewReturnRow(ReturnTable_neo_exchangerate_value) = 1.0#
          NewReturnRow(ReturnTable_neo_price_value) = 0.0#
          NewReturnRow(ReturnTable_neo_pricedate_value) = Renaissance_BaseDate

          NewReturnRow(ReturnTable_SectionSort) = IIf(thisPosition.DummyTrade, 1, 0)

          ReturnTable.Rows.Add(NewReturnRow)
          thisPosition.TableRow = NewReturnRow
        Next
      End If

      ' ******************************************************************************************
      ' Now Add in Administrator values, matching on ISIN & Currency, or Adding new Row
      ' Do not match Availability Currency Rows, these will be apportioned later.
      ' ******************************************************************************************

      ' Sort Position Array by Abs(Holding) to match active positions first.

      Array.Sort(PositionArray, New PositionSummaryComparerByAbsInstrumentPosition())

      ' OK, Continue.

      Dim thisISIN As String
      Dim thisCurrency As String
      Dim PositionCounter As Integer
      Dim foundPosition As Boolean = False
      Dim Ordinal_Admin_captureID As Integer
      Dim Ordinal_Admin_ISIN As Integer
      Dim Ordinal_Admin_Currency As Integer
      Dim Ordinal_Admin_Description As Integer
      Dim Ordinal_Admin_InstrumentType As Integer
      Dim Ordinal_Admin_Value As Integer
      Dim Ordinal_Admin_Quantity As Integer
      Dim Ordinal_Admin_FXRate As Integer
      Dim Ordinal_Admin_Price As Integer
      Dim Ordinal_Admin_PriceDate As Integer
      Dim Ordinal_Admin_SubAssetType As Integer
      Dim Ordinal_Admin_MaturityDate As Integer
      Dim Ordinal_Admin_QuoteCurrency As Integer

      If (AdministratorInventoryTable IsNot Nothing) AndAlso (AdministratorInventoryTable.Rows.Count > 0) Then
        Ordinal_Admin_captureID = AdministratorInventoryTable.Columns("captureID").Ordinal
        Ordinal_Admin_ISIN = AdministratorInventoryTable.Columns("neo_isin").Ordinal
        Ordinal_Admin_Currency = AdministratorInventoryTable.Columns("neo_quotecurrency").Ordinal
        Ordinal_Admin_Description = AdministratorInventoryTable.Columns("neo_description").Ordinal
        Ordinal_Admin_InstrumentType = AdministratorInventoryTable.Columns("neo_instrumenttype").Ordinal
        Ordinal_Admin_Value = AdministratorInventoryTable.Columns("neo_marketvalue_rc_value").Ordinal
        Ordinal_Admin_Quantity = AdministratorInventoryTable.Columns("neo_quantity_value").Ordinal
        Ordinal_Admin_FXRate = AdministratorInventoryTable.Columns("neo_exchangerate_value").Ordinal
        Ordinal_Admin_Price = AdministratorInventoryTable.Columns("neo_price_value").Ordinal
        Ordinal_Admin_PriceDate = AdministratorInventoryTable.Columns("neo_pricedate_value").Ordinal
        Ordinal_Admin_SubAssetType = AdministratorInventoryTable.Columns("neo_subassettype").Ordinal
        Ordinal_Admin_MaturityDate = AdministratorInventoryTable.Columns("neo_maturitydate").Ordinal
        Ordinal_Admin_QuoteCurrency = AdministratorInventoryTable.Columns("neo_quotecurrency").Ordinal

        Dim Admin_SettlementDate_String As String
        Dim Admin_SettlementDate As Date
        Dim Admin_Position As Double
        Dim Admin_SubType_String As String
        Dim Admin_QuoteCurrency As String


        For Counter = 0 To (AdministratorInventoryTable.Rows.Count - 1)
          AdministratorRow = AdministratorInventoryTable.Rows(Counter)

          thisISIN = AdministratorRow(Ordinal_Admin_ISIN).ToString.ToUpper
          thisCurrency = AdministratorRow(Ordinal_Admin_Currency).ToString.ToUpper
          foundPosition = False
          thisPosition = Nothing

          Admin_SettlementDate_String = AdministratorRow(Ordinal_Admin_MaturityDate).ToString.Trim
          Admin_SubType_String = AdministratorRow(Ordinal_Admin_SubAssetType).ToString.Trim
          Admin_Position = CDbl(AdministratorRow(Ordinal_Admin_Quantity))
          Admin_QuoteCurrency = AdministratorRow(Ordinal_Admin_QuoteCurrency).ToString.Trim

          If (Admin_SettlementDate_String.Length = 10) Then
            Try
              If (Not IsNumeric(Admin_SettlementDate_String.Substring(2, 1))) Then
                ' Assume dd/MM/yyyy
                Admin_SettlementDate = New Date(CInt(Admin_SettlementDate_String.Substring(6, 4)), CInt(Admin_SettlementDate_String.Substring(3, 2)), CInt(Admin_SettlementDate_String.Substring(0, 2)))
              Else
                ' Assume yyyy-MM-dd
                Admin_SettlementDate = New Date(CInt(Admin_SettlementDate_String.Substring(0, 4)), CInt(Admin_SettlementDate_String.Substring(5, 2)), CInt(Admin_SettlementDate_String.Substring(8, 2)))
              End If
            Catch ex As Exception
              Admin_SettlementDate = Renaissance_BaseDate
            End Try
          End If

          ' Currency Row to Ignore ?

          If (thisISIN = thisCurrency) AndAlso (AvailabilityCurrencies.Contains(thisISIN)) Then
            Continue For
          End If

          ' FX Forward ?

          ' AdministratorForwardsDictionary

          If (Admin_SettlementDate_String.Length = 10) AndAlso (FXForward_SubTypes.Contains(Admin_SubType_String.ToUpper())) AndAlso (CurrencyCodeDictionary.ContainsKey(Admin_QuoteCurrency)) Then

            If (Not AdministratorFXForwardsDictionary.TryGetValue(thisISIN, thisAdminFXForward)) Then
              thisAdminFXForward = New FXForwardPosition

              thisAdminFXForward.ValueDate = Admin_SettlementDate
              thisAdminFXForward.ISIN = thisISIN

              AdministratorFXForwardsDictionary.Add(thisISIN, thisAdminFXForward)
            End If

            thisCurrencyRow = CurrencyCodeDictionary(Admin_QuoteCurrency)

            If (Admin_Position > 0.0#) Then
              thisAdminFXForward.InstrumentLeg1 = thisCurrencyRow.CurrencyCashInstrumentID
              thisAdminFXForward.CurrencyLeg1 = thisCurrencyRow.CurrencyID
              thisAdminFXForward.CurrencyCodeLeg1 = thisCurrencyRow.CurrencyCode
              thisAdminFXForward.UnitsLeg1 = Admin_Position
              thisAdminFXForward.FXRate = CDbl(AdministratorRow(Ordinal_Admin_Price))
              thisAdminFXForward.AdminRowLeg1 = AdministratorRow
              thisAdminFXForward.ValueLeg1 = CDbl(AdministratorRow(Ordinal_Admin_Value))
            Else
              thisAdminFXForward.InstrumentLeg2 = thisCurrencyRow.CurrencyCashInstrumentID
              thisAdminFXForward.CurrencyLeg2 = thisCurrencyRow.CurrencyID
              thisAdminFXForward.CurrencyCodeLeg2 = thisCurrencyRow.CurrencyCode
              thisAdminFXForward.UnitsLeg2 = Admin_Position
              thisAdminFXForward.AdminRowLeg2 = AdministratorRow
              thisAdminFXForward.ValueLeg2 = CDbl(AdministratorRow(Ordinal_Admin_Value))
            End If

            Continue For

          Else

            For PositionCounter = 0 To (PositionArray.Length - 1)
              thisPosition = PositionArray(PositionCounter)

              If (thisPosition.InstrumentISIN = thisISIN) AndAlso (thisPosition.InstrumentCurrency = thisCurrency) AndAlso (thisPosition.TableRow IsNot Nothing) Then

                NewReturnRow = thisPosition.TableRow

                NewReturnRow(ReturnTable_neo_captureID) = CInt(AdministratorRow(Ordinal_Admin_captureID))
                NewReturnRow(ReturnTable_neo_marketvalue_rc_value) = CDbl(AdministratorRow(Ordinal_Admin_Value))
                NewReturnRow(ReturnTable_neo_quantity_value) = CDbl(AdministratorRow(Ordinal_Admin_Quantity))
                NewReturnRow(ReturnTable_neo_exchangerate_value) = CDbl(AdministratorRow(Ordinal_Admin_FXRate))
                NewReturnRow(ReturnTable_neo_price_value) = CDbl(AdministratorRow(Ordinal_Admin_Price))
                NewReturnRow(ReturnTable_neo_pricedate_value) = CDate(AdministratorRow(Ordinal_Admin_PriceDate))

                ' Adjust Price, PriceDate and Value to reflect administrator Price date if necessary.

                If (UseAdministratorPriceDates) AndAlso (CDate(NewReturnRow(ReturnTable_PriceDate)) <> CDate(NewReturnRow(ReturnTable_neo_pricedate_value))) Then
                  PricesRow = GetInstrumentPriceRow(thisPosition.InstrumentID, CDate(NewReturnRow(ReturnTable_neo_pricedate_value)))
                  If (PricesRow IsNot Nothing) Then
                    thisPrice = CDbl(Nz(PricesRow("PriceLevel"), 0.0#))
                    thisPriceDate = CDate(Nz(PricesRow("PriceDate"), Renaissance_BaseDate))

                    NewReturnRow(ReturnTable_PriceLevel) = thisPrice
                    NewReturnRow(ReturnTable_PriceDate) = thisPriceDate

                    ' Set Position value according to new price.
                    ' Note : Futures style value is entered as an adjustment because the value is the result of an earlier calculation, but it can safely be adjusted by the price differential.
                    ' Note : thisPosition.InstrumentPrice is only set for this purpose.

                    If (thisPosition.InstrumentTypeFuturesStylePricing = False) Then
                      NewReturnRow(ReturnTable_Value) = thisPosition.InstrumentPosition * thisPrice * thisPosition.InstrumentMultiplier * thisPosition.FXToFund
                    Else
                      NewReturnRow(ReturnTable_Value) += thisPosition.InstrumentPosition * (thisPrice - thisPosition.InstrumentPrice) * thisPosition.InstrumentMultiplier * thisPosition.FXToFund
                    End If
                  End If

                End If

                thisPosition.TableRow = Nothing ' Help the garbage collector.
                foundPosition = True
                Exit For
              End If
            Next

          End If

          ' ....

          thisPosition = Nothing

          If (Not foundPosition) Then

            NewReturnRow = ReturnTable.NewRow

            NewReturnRow(ReturnTable_rptISIN) = thisISIN
            NewReturnRow(ReturnTable_rptCurrency) = thisCurrency
            NewReturnRow(ReturnTable_rptDescription) = CStr(AdministratorRow(Ordinal_Admin_Description))
            NewReturnRow(ReturnTable_rptInstrumentType) = CStr(AdministratorRow(Ordinal_Admin_InstrumentType))
            NewReturnRow(ReturnTable_InstrumentTypeID) = 0
            NewReturnRow(ReturnTable_InstrumentType) = CStr(AdministratorRow(Ordinal_Admin_InstrumentType))
            NewReturnRow(ReturnTable_Instrument) = 0

            NewReturnRow(ReturnTable_PriceLevel) = 0.0#
            NewReturnRow(ReturnTable_PriceDate) = Renaissance_BaseDate
            NewReturnRow(ReturnTable_CompoundFXRate) = 0.0#
            NewReturnRow(ReturnTable_SignedUnits) = 0.0#
            NewReturnRow(ReturnTable_Value) = 0.0#
            NewReturnRow(ReturnTable_BookedNotional) = 0.0#
            NewReturnRow(ReturnTable_neo_captureID) = CInt(AdministratorRow(Ordinal_Admin_captureID))
            NewReturnRow(ReturnTable_neo_marketvalue_rc_value) = CDbl(AdministratorRow(Ordinal_Admin_Value))
            NewReturnRow(ReturnTable_neo_quantity_value) = CDbl(AdministratorRow(Ordinal_Admin_Quantity))
            NewReturnRow(ReturnTable_neo_exchangerate_value) = CDbl(AdministratorRow(Ordinal_Admin_FXRate))
            NewReturnRow(ReturnTable_neo_price_value) = CDbl(AdministratorRow(Ordinal_Admin_Price))
            NewReturnRow(ReturnTable_neo_pricedate_value) = CDate(AdministratorRow(Ordinal_Admin_PriceDate))

            NewReturnRow(ReturnTable_SectionSort) = 0

            ReturnTable.Rows.Add(NewReturnRow)
          End If
        Next
      End If

      ' ************************************************************************
      ' FX Forwards :-
      ' Match if possible, else add part lines.
      ' ************************************************************************

      ' Try to Match....
      Dim foundFXForward As Boolean = False

      For Each thisVeniceFXForward In VeniceFXForwardsDictionary.Values
        foundFXForward = False

        ' Put the calculated Value (P&L) on the long side (to match Administrator)
        If (thisVeniceFXForward.UnitsLeg1 >= 0.0#) Then ' (True) OrElse (thisFXForward.CurrencyLeg1 = FundCurrencyID) Then
          thisVeniceFXForward.ValueLeg1 = thisVeniceFXForward.UnitsLeg1 + (thisVeniceFXForward.UnitsLeg2 * GetFXToFund(thisVeniceFXForward.CurrencyLeg2, thisVeniceFXForward.CurrencyLeg1))
          thisVeniceFXForward.ValueLeg2 = 0.0#
        Else
          thisVeniceFXForward.ValueLeg1 = 0.0#
          thisVeniceFXForward.ValueLeg2 = thisVeniceFXForward.UnitsLeg2 + (thisVeniceFXForward.UnitsLeg1 * GetFXToFund(thisVeniceFXForward.CurrencyLeg1, thisVeniceFXForward.CurrencyLeg2))
        End If

        For Each thisAdminFXForward In AdministratorFXForwardsDictionary.Values

          ' First compare dates, it's the easy place to start
          If (thisVeniceFXForward.ValueDate = thisAdminFXForward.ValueDate) Then

            ' Admin FX Forwards : Long side is always leg 1, Venice can be ither way round.

            If (thisVeniceFXForward.UnitsLeg1 >= 0.0#) Then
              If (thisVeniceFXForward.CurrencyLeg1 = thisAdminFXForward.CurrencyLeg1) AndAlso _
              (thisVeniceFXForward.CurrencyLeg2 = thisAdminFXForward.CurrencyLeg2) AndAlso _
              (Math.Abs(thisVeniceFXForward.UnitsLeg1 - thisAdminFXForward.UnitsLeg1) < 0.01#) AndAlso _
              (Math.Abs(thisVeniceFXForward.UnitsLeg2 - thisAdminFXForward.UnitsLeg2) < 0.01#) Then

                foundFXForward = True
              End If
            Else
              If (thisVeniceFXForward.CurrencyLeg1 = thisAdminFXForward.CurrencyLeg2) AndAlso _
              (thisVeniceFXForward.CurrencyLeg2 = thisAdminFXForward.CurrencyLeg1) AndAlso _
              (Math.Abs(thisVeniceFXForward.UnitsLeg1 - thisAdminFXForward.UnitsLeg2) < 0.01#) AndAlso _
              (Math.Abs(thisVeniceFXForward.UnitsLeg2 - thisAdminFXForward.UnitsLeg1) < 0.01#) Then

                foundFXForward = True
              End If
            End If
          End If

          If (foundFXForward) Then

            ' Position, Leg 1

            thisFXRate = GetFXToFund(thisVeniceFXForward.CurrencyLeg1, FundCurrencyID)

            NewReturnRow = ReturnTable.NewRow

            NewReturnRow(ReturnTable_rptISIN) = thisAdminFXForward.ISIN
            NewReturnRow(ReturnTable_rptCurrency) = thisVeniceFXForward.CurrencyCodeLeg1

            If (thisAdminFXForward.AdminRowLeg1 Is Nothing) Then
              NewReturnRow(ReturnTable_rptDescription) = "Fwd " & thisVeniceFXForward.CurrencyCodeLeg1 & "/" & thisVeniceFXForward.CurrencyCodeLeg2 & " " & thisVeniceFXForward.ValueDate.ToString(DISPLAYMEMBER_DATEFORMAT)
            Else
              NewReturnRow(ReturnTable_rptDescription) = CStr(thisAdminFXForward.AdminRowLeg1(Ordinal_Admin_Description))
            End If

            NewReturnRow(ReturnTable_rptInstrumentType) = InstrumentTypes.Forward.ToString
            NewReturnRow(ReturnTable_InstrumentTypeID) = InstrumentTypes.Forward
            NewReturnRow(ReturnTable_InstrumentType) = InstrumentTypes.Forward.ToString
            NewReturnRow(ReturnTable_Instrument) = thisVeniceFXForward.InstrumentLeg1

            NewReturnRow(ReturnTable_PriceLevel) = 1.0#
            NewReturnRow(ReturnTable_PriceDate) = Renaissance_BaseDate
            NewReturnRow(ReturnTable_BookedNotional) = 0.0#
            NewReturnRow(ReturnTable_CompoundFXRate) = thisFXRate
            NewReturnRow(ReturnTable_SignedUnits) = thisVeniceFXForward.UnitsLeg1
            NewReturnRow(ReturnTable_Value) = (thisVeniceFXForward.ValueLeg1) * thisFXRate

            If (thisVeniceFXForward.UnitsLeg1 >= 0.0#) Then
              NewReturnRow(ReturnTable_neo_captureID) = 1 ' Dummy, non-Zero
              NewReturnRow(ReturnTable_neo_marketvalue_rc_value) = thisAdminFXForward.ValueLeg1
              NewReturnRow(ReturnTable_neo_quantity_value) = thisAdminFXForward.UnitsLeg1
              NewReturnRow(ReturnTable_neo_exchangerate_value) = thisAdminFXForward.FXRate
              NewReturnRow(ReturnTable_neo_price_value) = 1.0#
              NewReturnRow(ReturnTable_neo_pricedate_value) = Renaissance_BaseDate
            Else
              NewReturnRow(ReturnTable_neo_captureID) = 1 ' Dummy, non-Zero
              NewReturnRow(ReturnTable_neo_marketvalue_rc_value) = thisAdminFXForward.ValueLeg2
              NewReturnRow(ReturnTable_neo_quantity_value) = thisAdminFXForward.UnitsLeg2
              NewReturnRow(ReturnTable_neo_exchangerate_value) = thisAdminFXForward.FXRate
              NewReturnRow(ReturnTable_neo_price_value) = 1.0#
              NewReturnRow(ReturnTable_neo_pricedate_value) = Renaissance_BaseDate
            End If

            NewReturnRow(ReturnTable_SectionSort) = 1

            ReturnTable.Rows.Add(NewReturnRow)

            ' Position, Leg 2

            thisFXRate = GetFXToFund(thisVeniceFXForward.CurrencyLeg2, FundCurrencyID)

            NewReturnRow = ReturnTable.NewRow

            NewReturnRow(ReturnTable_rptISIN) = thisAdminFXForward.ISIN
            NewReturnRow(ReturnTable_rptCurrency) = thisVeniceFXForward.CurrencyCodeLeg2

            If (thisAdminFXForward.AdminRowLeg1 Is Nothing) Then
              NewReturnRow(ReturnTable_rptDescription) = "Fwd " & thisVeniceFXForward.CurrencyCodeLeg1 & "/" & thisVeniceFXForward.CurrencyCodeLeg2 & " " & thisVeniceFXForward.ValueDate.ToString(DISPLAYMEMBER_DATEFORMAT)
            Else
              NewReturnRow(ReturnTable_rptDescription) = CStr(thisAdminFXForward.AdminRowLeg1(Ordinal_Admin_Description))
            End If

            NewReturnRow(ReturnTable_rptInstrumentType) = InstrumentTypes.Forward.ToString
            NewReturnRow(ReturnTable_InstrumentTypeID) = InstrumentTypes.Forward
            NewReturnRow(ReturnTable_InstrumentType) = InstrumentTypes.Forward.ToString
            NewReturnRow(ReturnTable_Instrument) = thisVeniceFXForward.InstrumentLeg2

            NewReturnRow(ReturnTable_PriceLevel) = 1.0#
            NewReturnRow(ReturnTable_PriceDate) = Renaissance_BaseDate
            NewReturnRow(ReturnTable_BookedNotional) = 0.0#
            NewReturnRow(ReturnTable_CompoundFXRate) = thisFXRate
            NewReturnRow(ReturnTable_SignedUnits) = thisVeniceFXForward.UnitsLeg2
            NewReturnRow(ReturnTable_Value) = (thisVeniceFXForward.ValueLeg2) * thisFXRate

            If (thisVeniceFXForward.UnitsLeg1 < 0.0#) Then
              NewReturnRow(ReturnTable_neo_captureID) = 1 ' Dummy, non-Zero
              NewReturnRow(ReturnTable_neo_marketvalue_rc_value) = thisAdminFXForward.ValueLeg1
              NewReturnRow(ReturnTable_neo_quantity_value) = thisAdminFXForward.UnitsLeg1
              NewReturnRow(ReturnTable_neo_exchangerate_value) = thisAdminFXForward.FXRate
              NewReturnRow(ReturnTable_neo_price_value) = 1.0#
              NewReturnRow(ReturnTable_neo_pricedate_value) = Renaissance_BaseDate
            Else
              NewReturnRow(ReturnTable_neo_captureID) = 1 ' Dummy, non-Zero
              NewReturnRow(ReturnTable_neo_marketvalue_rc_value) = thisAdminFXForward.ValueLeg2
              NewReturnRow(ReturnTable_neo_quantity_value) = thisAdminFXForward.UnitsLeg2
              NewReturnRow(ReturnTable_neo_exchangerate_value) = thisAdminFXForward.FXRate
              NewReturnRow(ReturnTable_neo_price_value) = 1.0#
              NewReturnRow(ReturnTable_neo_pricedate_value) = Renaissance_BaseDate
            End If

            NewReturnRow(ReturnTable_SectionSort) = 1

            ReturnTable.Rows.Add(NewReturnRow)

            AdministratorFXForwardsDictionary.Remove(thisAdminFXForward.ISIN)

            Exit For ' thisAdminFXForward

          End If ' If (foundFXForward) Then

        Next thisAdminFXForward

        If (Not foundFXForward) Then
          ' Venice Forward, not matched

          ' Position, Leg 1

          thisFXRate = GetFXToFund(thisVeniceFXForward.CurrencyLeg1, FundCurrencyID)

          NewReturnRow = ReturnTable.NewRow

          NewReturnRow(ReturnTable_rptISIN) = ""
          NewReturnRow(ReturnTable_rptCurrency) = thisVeniceFXForward.CurrencyCodeLeg1
          NewReturnRow(ReturnTable_rptDescription) = "Fwd " & thisVeniceFXForward.CurrencyCodeLeg1 & "/" & thisVeniceFXForward.CurrencyCodeLeg2 & " " & thisVeniceFXForward.ValueDate.ToString(DISPLAYMEMBER_DATEFORMAT)
          NewReturnRow(ReturnTable_rptInstrumentType) = InstrumentTypes.Forward.ToString
          NewReturnRow(ReturnTable_InstrumentTypeID) = InstrumentTypes.Forward
          NewReturnRow(ReturnTable_InstrumentType) = InstrumentTypes.Forward.ToString
          NewReturnRow(ReturnTable_Instrument) = thisVeniceFXForward.InstrumentLeg1

          NewReturnRow(ReturnTable_PriceLevel) = 1.0#
          NewReturnRow(ReturnTable_PriceDate) = Renaissance_BaseDate
          NewReturnRow(ReturnTable_BookedNotional) = 0.0#
          NewReturnRow(ReturnTable_CompoundFXRate) = thisFXRate
          NewReturnRow(ReturnTable_SignedUnits) = thisVeniceFXForward.UnitsLeg1
          NewReturnRow(ReturnTable_Value) = (thisVeniceFXForward.ValueLeg1) * thisFXRate

          NewReturnRow(ReturnTable_neo_captureID) = 0
          NewReturnRow(ReturnTable_neo_marketvalue_rc_value) = 0.0#
          NewReturnRow(ReturnTable_neo_quantity_value) = 0.0#
          NewReturnRow(ReturnTable_neo_exchangerate_value) = 1.0#
          NewReturnRow(ReturnTable_neo_price_value) = 0.0#
          NewReturnRow(ReturnTable_neo_pricedate_value) = Renaissance_BaseDate

          NewReturnRow(ReturnTable_SectionSort) = 1

          ReturnTable.Rows.Add(NewReturnRow)

          ' Position, Leg 2

          thisFXRate = GetFXToFund(thisVeniceFXForward.CurrencyLeg2, FundCurrencyID)

          NewReturnRow = ReturnTable.NewRow

          NewReturnRow(ReturnTable_rptISIN) = ""
          NewReturnRow(ReturnTable_rptCurrency) = thisVeniceFXForward.CurrencyCodeLeg2
          NewReturnRow(ReturnTable_rptDescription) = "Fwd " & thisVeniceFXForward.CurrencyCodeLeg1 & "/" & thisVeniceFXForward.CurrencyCodeLeg2 & " " & thisVeniceFXForward.ValueDate.ToString(DISPLAYMEMBER_DATEFORMAT)
          NewReturnRow(ReturnTable_rptInstrumentType) = InstrumentTypes.Forward.ToString
          NewReturnRow(ReturnTable_InstrumentTypeID) = InstrumentTypes.Forward
          NewReturnRow(ReturnTable_InstrumentType) = InstrumentTypes.Forward.ToString
          NewReturnRow(ReturnTable_Instrument) = thisVeniceFXForward.InstrumentLeg2

          NewReturnRow(ReturnTable_PriceLevel) = 1.0#
          NewReturnRow(ReturnTable_PriceDate) = Renaissance_BaseDate
          NewReturnRow(ReturnTable_BookedNotional) = 0.0#
          NewReturnRow(ReturnTable_CompoundFXRate) = thisFXRate
          NewReturnRow(ReturnTable_SignedUnits) = thisVeniceFXForward.UnitsLeg2
          NewReturnRow(ReturnTable_Value) = (thisVeniceFXForward.ValueLeg2) * thisFXRate

          NewReturnRow(ReturnTable_neo_captureID) = 0
          NewReturnRow(ReturnTable_neo_marketvalue_rc_value) = 0.0#
          NewReturnRow(ReturnTable_neo_quantity_value) = 0.0#
          NewReturnRow(ReturnTable_neo_exchangerate_value) = 1.0#
          NewReturnRow(ReturnTable_neo_price_value) = 0.0#
          NewReturnRow(ReturnTable_neo_pricedate_value) = Renaissance_BaseDate

          NewReturnRow(ReturnTable_SectionSort) = 1

          ReturnTable.Rows.Add(NewReturnRow)

        End If '  (foundFXForward)

        '    End If ' Dates Match

        'Next thisAdminFXForward

      Next thisVeniceFXForward

      ' Add Table Entries for unmatched Administrator FX Forwards

      For Each thisAdminFXForward In AdministratorFXForwardsDictionary.Values

        ' Leg 1
        'azerty
        'If (thisAdminFXForward.AdminRowLeg2 IsNot Nothing) Then
        If (thisAdminFXForward.AdminRowLeg1 IsNot Nothing) Then

          AdministratorRow = thisAdminFXForward.AdminRowLeg1

          thisISIN = AdministratorRow(Ordinal_Admin_ISIN).ToString.ToUpper
          thisCurrency = AdministratorRow(Ordinal_Admin_Currency).ToString.ToUpper

          NewReturnRow = ReturnTable.NewRow

          NewReturnRow(ReturnTable_rptISIN) = thisISIN
          NewReturnRow(ReturnTable_rptCurrency) = thisCurrency
          NewReturnRow(ReturnTable_rptDescription) = CStr(AdministratorRow(Ordinal_Admin_Description))
          NewReturnRow(ReturnTable_rptInstrumentType) = CStr(AdministratorRow(Ordinal_Admin_InstrumentType))
          NewReturnRow(ReturnTable_InstrumentTypeID) = 0
          NewReturnRow(ReturnTable_InstrumentType) = CStr(AdministratorRow(Ordinal_Admin_InstrumentType))
          NewReturnRow(ReturnTable_Instrument) = 0

          NewReturnRow(ReturnTable_PriceLevel) = 0.0#
          NewReturnRow(ReturnTable_PriceDate) = Renaissance_BaseDate
          NewReturnRow(ReturnTable_CompoundFXRate) = 0.0#
          NewReturnRow(ReturnTable_SignedUnits) = 0.0#
          NewReturnRow(ReturnTable_Value) = 0.0#
          NewReturnRow(ReturnTable_BookedNotional) = 0.0#
          NewReturnRow(ReturnTable_neo_captureID) = CInt(AdministratorRow(Ordinal_Admin_captureID))
          NewReturnRow(ReturnTable_neo_marketvalue_rc_value) = CDbl(AdministratorRow(Ordinal_Admin_Value))
          NewReturnRow(ReturnTable_neo_quantity_value) = CDbl(AdministratorRow(Ordinal_Admin_Quantity))
          NewReturnRow(ReturnTable_neo_exchangerate_value) = CDbl(AdministratorRow(Ordinal_Admin_FXRate))
          NewReturnRow(ReturnTable_neo_price_value) = CDbl(AdministratorRow(Ordinal_Admin_Price))
          NewReturnRow(ReturnTable_neo_pricedate_value) = CDate(AdministratorRow(Ordinal_Admin_PriceDate))

          NewReturnRow(ReturnTable_SectionSort) = 0

          ReturnTable.Rows.Add(NewReturnRow)


        End If

        ' Leg 2

        If (thisAdminFXForward.AdminRowLeg2 IsNot Nothing) Then

          AdministratorRow = thisAdminFXForward.AdminRowLeg2

          thisISIN = AdministratorRow(Ordinal_Admin_ISIN).ToString.ToUpper
          thisCurrency = AdministratorRow(Ordinal_Admin_Currency).ToString.ToUpper

          NewReturnRow = ReturnTable.NewRow

          NewReturnRow(ReturnTable_rptISIN) = thisISIN
          NewReturnRow(ReturnTable_rptCurrency) = thisCurrency
          NewReturnRow(ReturnTable_rptDescription) = CStr(AdministratorRow(Ordinal_Admin_Description))
          NewReturnRow(ReturnTable_rptInstrumentType) = CStr(AdministratorRow(Ordinal_Admin_InstrumentType))
          NewReturnRow(ReturnTable_InstrumentTypeID) = 0
          NewReturnRow(ReturnTable_InstrumentType) = CStr(AdministratorRow(Ordinal_Admin_InstrumentType))
          NewReturnRow(ReturnTable_Instrument) = 0

          NewReturnRow(ReturnTable_PriceLevel) = 0.0#
          NewReturnRow(ReturnTable_PriceDate) = Renaissance_BaseDate
          NewReturnRow(ReturnTable_CompoundFXRate) = 0.0#
          NewReturnRow(ReturnTable_SignedUnits) = 0.0#
          NewReturnRow(ReturnTable_Value) = 0.0#
          NewReturnRow(ReturnTable_BookedNotional) = 0.0#
          NewReturnRow(ReturnTable_neo_captureID) = CInt(AdministratorRow(Ordinal_Admin_captureID))
          NewReturnRow(ReturnTable_neo_marketvalue_rc_value) = CDbl(AdministratorRow(Ordinal_Admin_Value))
          NewReturnRow(ReturnTable_neo_quantity_value) = CDbl(AdministratorRow(Ordinal_Admin_Quantity))
          NewReturnRow(ReturnTable_neo_exchangerate_value) = CDbl(AdministratorRow(Ordinal_Admin_FXRate))
          NewReturnRow(ReturnTable_neo_price_value) = CDbl(AdministratorRow(Ordinal_Admin_Price))
          NewReturnRow(ReturnTable_neo_pricedate_value) = CDate(AdministratorRow(Ordinal_Admin_PriceDate))

          NewReturnRow(ReturnTable_SectionSort) = 0

          ReturnTable.Rows.Add(NewReturnRow)

        End If

      Next

      ' ************************************************************************
      ' Split out Cash availabilities.
      ' ************************************************************************

      ' Add lines for each Venice Variation margin entry

      If (VariationMarginModifierDictionary.Count > 0) Then

        For Each thisCurrency In VariationMarginModifierDictionary.Keys
          thisCurrencyRow = CurrencyCodeDictionary(thisCurrency)

          thisFXRate = GetFXToFund(thisCurrencyRow.CurrencyID, FundCurrencyID)

          NewReturnRow = ReturnTable.NewRow

          NewReturnRow(ReturnTable_rptISIN) = ""
          NewReturnRow(ReturnTable_rptCurrency) = thisCurrency
          NewReturnRow(ReturnTable_rptDescription) = "Variation Adjustment " & thisCurrency
          NewReturnRow(ReturnTable_rptInstrumentType) = InstrumentTypes.Cash.ToString
          NewReturnRow(ReturnTable_InstrumentTypeID) = InstrumentTypes.Cash
          NewReturnRow(ReturnTable_InstrumentType) = InstrumentTypes.Cash.ToString
          NewReturnRow(ReturnTable_Instrument) = thisCurrencyRow.CurrencyCashInstrumentID

          NewReturnRow(ReturnTable_PriceLevel) = 1.0#
          NewReturnRow(ReturnTable_PriceDate) = Renaissance_BaseDate
          NewReturnRow(ReturnTable_BookedNotional) = 0.0#
          NewReturnRow(ReturnTable_CompoundFXRate) = thisFXRate
          NewReturnRow(ReturnTable_SignedUnits) = VariationMarginModifierDictionary(thisCurrency)
          NewReturnRow(ReturnTable_Value) = VariationMarginModifierDictionary(thisCurrency) * thisFXRate

          NewReturnRow(ReturnTable_neo_captureID) = 0
          NewReturnRow(ReturnTable_neo_marketvalue_rc_value) = 0.0#
          NewReturnRow(ReturnTable_neo_quantity_value) = 0.0#
          NewReturnRow(ReturnTable_neo_exchangerate_value) = 1.0#
          NewReturnRow(ReturnTable_neo_price_value) = 0.0#
          NewReturnRow(ReturnTable_neo_pricedate_value) = Renaissance_BaseDate

          NewReturnRow(ReturnTable_SectionSort) = 1

          ReturnTable.Rows.Add(NewReturnRow)

          VariationMarginModifierRows.Add(thisCurrency, NewReturnRow)
        Next
      End If

      ' ************************************************************************
      ' Hmm, a new approach 
      ' What I want to do is to allocate all Neolink Availabilities to existing, or new, table rows.
      ' I want the Cash line to be available cash only and for otherwise un-allocated Availabilities to go to a catch all line.
      ' ************************************************************************

      ' Now process each Availability currency in order.

      Dim AvailabilityDescription As String
      Dim TotalAvailabilityValue_Local As Double = 0.0#
      Dim TotalAvailabilityValue_RC As Double = 0.0#
      Dim TotalCash_Local As Double = 0.0#
      Dim TotalCash_RC As Double = 0.0#
      Dim TotalVariation_Local As Double = 0.0#
      Dim TotalVariation_RC As Double = 0.0#
      Dim TotalMarginDeposit_Local As Double = 0.0#
      Dim TotalMarginDeposit_RC As Double = 0.0#
      Dim TotalRetrocession_Local As Double = 0.0#
      Dim TotalRetrocession_RC As Double = 0.0#
      Dim TotalCatchAll_Local As Double = 0.0#
      Dim TotalCatchAll_RC As Double = 0.0#
      Dim thisAvailabilityValue_Local As Double = 0.0#
      Dim thisAvailabilityValue_RC As Double = 0.0#

      For Each thisCurrency In AvailabilityCurrencies

        TotalAvailabilityValue_Local = 0.0#
        TotalAvailabilityValue_RC = 0.0#
        TotalCash_Local = 0.0#
        TotalCash_RC = 0.0#
        TotalMarginDeposit_Local = 0.0#
        TotalMarginDeposit_RC = 0.0#
        TotalVariation_Local = 0.0#
        TotalVariation_RC = 0.0#
        TotalRetrocession_Local = 0.0#
        TotalRetrocession_RC = 0.0#
        TotalCatchAll_Local = 0.0#
        TotalCatchAll_RC = 0.0#
        thisCurrencyRow = Nothing

        CurrencyCodeDictionary.TryGetValue(thisCurrency, thisCurrencyRow)

        SelectedAvailabilities = FundAvailabilities.Select("Currency='" & thisCurrency & "'")

        If (SelectedAvailabilities IsNot Nothing) AndAlso (SelectedAvailabilities.Length > 0) Then

          For Each thisAvailability In SelectedAvailabilities

            thisAvailabilityValue_Local = thisAvailability.CurrentBalance_Value
            thisAvailabilityValue_RC = thisAvailability.Balance_Value
            If (thisAvailabilityValue_Local = 0.0#) Then ' RC lines do not have a 'Local' value.
              thisAvailabilityValue_Local = thisAvailabilityValue_RC
            End If

            TotalAvailabilityValue_Local += thisAvailabilityValue_Local
            TotalAvailabilityValue_RC += thisAvailabilityValue_RC

            AvailabilityDescription = thisAvailability.Description.Trim.ToUpper

            If (AvailabilityDescription.StartsWith("CASH ") AndAlso (AvailabilityDescription.Length <= 9)) Then
              AvailabilityDescription = "CASH"
            ElseIf (AvailabilityDescription.StartsWith("PAYABLE ")) Then ' e.g. 'Payable Ucits', 'Payable Redemptions'
              AvailabilityDescription = "PAYABLE CASH"
            ElseIf (AvailabilityDescription.StartsWith("RECEIVABLE ")) Then ' e.g. 'Receivable UCITS', 'Receivable cash'
              AvailabilityDescription = "RECEIVABLE CASH"
            ElseIf (AvailabilityDescription.StartsWith("REC.TRAILER FEES ")) Then ' e.g. 'Receivable UCITS', 'Receivable cash'
              AvailabilityDescription = "REC.TRAILER FEES ALL"
            End If

            Select Case AvailabilityDescription

              Case "CASH", "CASH AT SIGHT DE - BP2S LUXEMB", "RECEIVABLE CASH", "PAYABLE CASH"
                TotalCash_Local += thisAvailabilityValue_Local
                TotalCash_RC += thisAvailabilityValue_RC

              Case "FEES RETROCESSION", "REC.TRAILER FEES ALL", "FDG RETRO COM"
                ' These relate to a provision instrument defined on the Currency table.

                If (thisCurrencyRow IsNot Nothing) AndAlso (thisCurrencyRow.DefaultRetrocessionInstrument > 0) Then
                  TotalRetrocession_Local += thisAvailabilityValue_Local
                  TotalRetrocession_RC += thisAvailabilityValue_RC
                Else
                  TotalCatchAll_Local += thisAvailabilityValue_Local
                  TotalCatchAll_RC += thisAvailabilityValue_RC
                End If

              Case "CASH DEPOSIT", "DEPOTS DE GARANTIE"

                TotalMarginDeposit_Local += thisAvailabilityValue_Local
                TotalMarginDeposit_RC += thisAvailabilityValue_RC

              Case "MARGIN CALL", "APPELS DE MARGE"

                If (VariationMarginModifierRows.ContainsKey(thisCurrency)) Then
                  TotalVariation_Local += thisAvailabilityValue_Local
                  TotalVariation_RC += thisAvailabilityValue_RC
                Else
                  TotalCatchAll_Local += thisAvailabilityValue_Local
                  TotalCatchAll_RC += thisAvailabilityValue_RC
                End If

              Case Else

                TotalCatchAll_Local += thisAvailabilityValue_Local
                TotalCatchAll_RC += thisAvailabilityValue_RC

            End Select

          Next thisAvailability

        End If

        ' Stitch.

        ' OK, Now apportion value to required rows, or create as necessary.

        ' Cash

        If (TotalCash_Local <> 0.0#) OrElse (TotalCash_RC <> 0.0#) Then

          If (thisCurrencyRow IsNot Nothing) Then
            SelectedReturnRows = ReturnTable.Select("(rpt_ISIN='" & thisCurrency & "') AND (InstrumentTypeID=" & CInt(InstrumentTypes.Cash).ToString("###0") & ") AND (Instrument=" & thisCurrencyRow.CurrencyCashInstrumentID.ToString("###0") & ")")
          Else
            SelectedReturnRows = Nothing
          End If

          If (SelectedReturnRows IsNot Nothing) AndAlso (SelectedReturnRows.Length > 0) Then
            AvailabilityReturnRow = SelectedReturnRows(0)

            AvailabilityReturnRow(ReturnTable_neo_quantity_value) = CDbl(Nz(AvailabilityReturnRow(ReturnTable_neo_quantity_value), 0.0#)) + TotalCash_Local
            AvailabilityReturnRow(ReturnTable_neo_marketvalue_rc_value) = CDbl(Nz(AvailabilityReturnRow(ReturnTable_neo_marketvalue_rc_value), 0.0#)) + TotalCash_RC
            AvailabilityReturnRow(ReturnTable_neo_captureID) = 1
            AvailabilityReturnRow(ReturnTable_neo_price_value) = 1.0#

          Else

            ' Add Row

            NewReturnRow = ReturnTable.NewRow

            NewReturnRow(ReturnTable_rptISIN) = thisCurrency
            NewReturnRow(ReturnTable_rptCurrency) = thisCurrency

            If (thisCurrencyRow IsNot Nothing) Then
              NewReturnRow(ReturnTable_Instrument) = thisCurrencyRow.CurrencyCashInstrumentID
            Else
              NewReturnRow(ReturnTable_Instrument) = 0
            End If

            NewReturnRow(ReturnTable_rptDescription) = thisCurrency & " Cash"
            NewReturnRow(ReturnTable_rptInstrumentType) = InstrumentTypes.Cash.ToString
            NewReturnRow(ReturnTable_InstrumentTypeID) = InstrumentTypes.Cash
            NewReturnRow(ReturnTable_InstrumentType) = InstrumentTypes.Cash.ToString
            NewReturnRow(ReturnTable_PriceLevel) = 1.0#
            NewReturnRow(ReturnTable_PriceDate) = Renaissance_BaseDate
            NewReturnRow(ReturnTable_CompoundFXRate) = 0.0#
            NewReturnRow(ReturnTable_SignedUnits) = 0.0#
            NewReturnRow(ReturnTable_Value) = 0.0#
            NewReturnRow(ReturnTable_BookedNotional) = 0.0#
            NewReturnRow(ReturnTable_neo_captureID) = 1
            NewReturnRow(ReturnTable_neo_marketvalue_rc_value) = TotalCash_RC
            NewReturnRow(ReturnTable_neo_quantity_value) = TotalCash_Local
            NewReturnRow(ReturnTable_neo_exchangerate_value) = 0.0#
            NewReturnRow(ReturnTable_neo_price_value) = 1.0#
            NewReturnRow(ReturnTable_neo_pricedate_value) = Renaissance_BaseDate

            NewReturnRow(ReturnTable_SectionSort) = 0

            ReturnTable.Rows.Add(NewReturnRow)

          End If

        End If

        ' Retrocessions
        If (TotalRetrocession_Local <> 0.0#) OrElse (TotalRetrocession_RC <> 0.0#) Then

          If (thisCurrencyRow IsNot Nothing) AndAlso (thisCurrencyRow.DefaultRetrocessionInstrument > 0) Then
            SelectedReturnRows = ReturnTable.Select("Instrument=" & thisCurrencyRow.DefaultRetrocessionInstrument.ToString("###0"))

            If (SelectedReturnRows IsNot Nothing) AndAlso (SelectedReturnRows.Length = 1) Then
              AvailabilityReturnRow = SelectedReturnRows(0)

              AvailabilityReturnRow(ReturnTable_neo_quantity_value) = CDbl(Nz(AvailabilityReturnRow(ReturnTable_neo_quantity_value), 0.0#)) + TotalRetrocession_Local
              AvailabilityReturnRow(ReturnTable_neo_marketvalue_rc_value) = CDbl(Nz(AvailabilityReturnRow(ReturnTable_neo_marketvalue_rc_value), 0.0#)) + TotalRetrocession_RC
              AvailabilityReturnRow(ReturnTable_neo_captureID) = 1
              AvailabilityReturnRow(ReturnTable_neo_price_value) = 1.0#

            Else
              ' New Row.

              NewReturnRow = ReturnTable.NewRow

              NewReturnRow(ReturnTable_rptISIN) = ""
              NewReturnRow(ReturnTable_rptCurrency) = thisCurrency

              NewReturnRow(ReturnTable_Instrument) = thisCurrencyRow.DefaultRetrocessionInstrument
              thisInstrumentRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, thisCurrencyRow.DefaultRetrocessionInstrument)

              If (thisInstrumentRow IsNot Nothing) Then
                NewReturnRow(ReturnTable_rptDescription) = thisInstrumentRow.InstrumentDescription
              Else
                NewReturnRow(ReturnTable_rptDescription) = "Retrocession " & thisCurrency
              End If
              NewReturnRow(ReturnTable_rptInstrumentType) = InstrumentTypes.Cash.ToString ' Make it appear in the Cash group.
              NewReturnRow(ReturnTable_InstrumentTypeID) = InstrumentTypes.Cash
              NewReturnRow(ReturnTable_InstrumentType) = InstrumentTypes.Cash.ToString
              NewReturnRow(ReturnTable_PriceLevel) = 1.0#
              NewReturnRow(ReturnTable_PriceDate) = Renaissance_BaseDate
              NewReturnRow(ReturnTable_CompoundFXRate) = 0.0#
              NewReturnRow(ReturnTable_SignedUnits) = 0.0#
              NewReturnRow(ReturnTable_Value) = 0.0#
              NewReturnRow(ReturnTable_BookedNotional) = 0.0#
              NewReturnRow(ReturnTable_neo_captureID) = 1
              NewReturnRow(ReturnTable_neo_marketvalue_rc_value) = TotalRetrocession_RC
              NewReturnRow(ReturnTable_neo_quantity_value) = TotalRetrocession_Local
              NewReturnRow(ReturnTable_neo_exchangerate_value) = 0.0#
              NewReturnRow(ReturnTable_neo_price_value) = 1.0#
              NewReturnRow(ReturnTable_neo_pricedate_value) = Renaissance_BaseDate

              NewReturnRow(ReturnTable_SectionSort) = 1

              ReturnTable.Rows.Add(NewReturnRow)

            End If

          Else
            ' Unknown currency to Venice, add to Catchall.

            TotalCatchAll_Local += TotalRetrocession_Local
            TotalCatchAll_RC += TotalRetrocession_RC

            TotalRetrocession_Local = 0.0#
            TotalRetrocession_RC = 0.0#
          End If

        End If

        ' Cash Deposits - Initial Margin

        If (TotalMarginDeposit_Local <> 0.0#) OrElse (TotalMarginDeposit_RC <> 0.0#) Then

          If (thisCurrencyRow IsNot Nothing) AndAlso (thisCurrencyRow.DefaultInitialMarginInstrument > 0) Then
            SelectedReturnRows = ReturnTable.Select("Instrument=" & thisCurrencyRow.DefaultInitialMarginInstrument.ToString("###0"))

            If (SelectedReturnRows IsNot Nothing) AndAlso (SelectedReturnRows.Length = 1) Then
              AvailabilityReturnRow = SelectedReturnRows(0)

              AvailabilityReturnRow(ReturnTable_neo_quantity_value) = CDbl(Nz(AvailabilityReturnRow(ReturnTable_neo_quantity_value), 0.0#)) + TotalMarginDeposit_Local
              AvailabilityReturnRow(ReturnTable_neo_marketvalue_rc_value) = CDbl(Nz(AvailabilityReturnRow(ReturnTable_neo_marketvalue_rc_value), 0.0#)) + TotalMarginDeposit_RC
              AvailabilityReturnRow(ReturnTable_neo_captureID) = 1
              AvailabilityReturnRow(ReturnTable_neo_price_value) = 1.0#
              AvailabilityReturnRow(ReturnTable_SectionSort) = 1

            Else
              ' New Row.

              NewReturnRow = ReturnTable.NewRow

              NewReturnRow(ReturnTable_rptISIN) = ""
              NewReturnRow(ReturnTable_rptCurrency) = thisCurrency

              NewReturnRow(ReturnTable_Instrument) = thisCurrencyRow.DefaultInitialMarginInstrument
              thisInstrumentRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, thisCurrencyRow.DefaultInitialMarginInstrument)

              If (thisInstrumentRow IsNot Nothing) Then
                NewReturnRow(ReturnTable_rptDescription) = thisInstrumentRow.InstrumentDescription
              Else
                NewReturnRow(ReturnTable_rptDescription) = "Deposit " & thisCurrency
              End If
              NewReturnRow(ReturnTable_rptInstrumentType) = InstrumentTypes.Cash.ToString ' Make it appear in the Cash group.
              NewReturnRow(ReturnTable_InstrumentTypeID) = InstrumentTypes.Cash
              NewReturnRow(ReturnTable_InstrumentType) = InstrumentTypes.Cash.ToString
              NewReturnRow(ReturnTable_PriceLevel) = 1.0#
              NewReturnRow(ReturnTable_PriceDate) = Renaissance_BaseDate
              NewReturnRow(ReturnTable_CompoundFXRate) = 0.0#
              NewReturnRow(ReturnTable_SignedUnits) = 0.0#
              NewReturnRow(ReturnTable_Value) = 0.0#
              NewReturnRow(ReturnTable_BookedNotional) = 0.0#
              NewReturnRow(ReturnTable_neo_captureID) = 1
              NewReturnRow(ReturnTable_neo_marketvalue_rc_value) = TotalMarginDeposit_RC
              NewReturnRow(ReturnTable_neo_quantity_value) = TotalMarginDeposit_Local
              NewReturnRow(ReturnTable_neo_exchangerate_value) = 0.0#
              NewReturnRow(ReturnTable_neo_price_value) = 1.0#
              NewReturnRow(ReturnTable_neo_pricedate_value) = Renaissance_BaseDate

              NewReturnRow(ReturnTable_SectionSort) = 1

              ReturnTable.Rows.Add(NewReturnRow)

            End If

          Else
            ' Unknown currency to Venice, add to Catchall.

            TotalCatchAll_Local += TotalMarginDeposit_Local
            TotalCatchAll_RC += TotalMarginDeposit_RC

            TotalMarginDeposit_Local = 0.0#
            TotalMarginDeposit_RC = 0.0#
          End If

        End If

        ' Variation Margin 

        If (TotalVariation_Local <> 0.0#) OrElse (TotalVariation_RC <> 0.0#) Then

          If (thisCurrencyRow IsNot Nothing) Then

            If (VariationMarginModifierRows.TryGetValue(thisCurrency, AvailabilityReturnRow)) Then

              AvailabilityReturnRow(ReturnTable_neo_quantity_value) = CDbl(Nz(AvailabilityReturnRow(ReturnTable_neo_quantity_value), 0.0#)) + TotalVariation_Local
              AvailabilityReturnRow(ReturnTable_neo_marketvalue_rc_value) = CDbl(Nz(AvailabilityReturnRow(ReturnTable_neo_marketvalue_rc_value), 0.0#)) + TotalVariation_RC
              AvailabilityReturnRow(ReturnTable_neo_captureID) = 1
              AvailabilityReturnRow(ReturnTable_neo_price_value) = 1.0#
              AvailabilityReturnRow(ReturnTable_SectionSort) = 1

            Else
              ' New Row.

              NewReturnRow = ReturnTable.NewRow

              NewReturnRow(ReturnTable_rptISIN) = ""
              NewReturnRow(ReturnTable_rptCurrency) = thisCurrency
              NewReturnRow(ReturnTable_rptDescription) = "Variation Adjustment " & thisCurrency
              NewReturnRow(ReturnTable_Instrument) = thisCurrencyRow.CurrencyCashInstrumentID
              NewReturnRow(ReturnTable_rptInstrumentType) = InstrumentTypes.Cash.ToString ' Make it appear in the Cash group.
              NewReturnRow(ReturnTable_InstrumentTypeID) = InstrumentTypes.Cash
              NewReturnRow(ReturnTable_InstrumentType) = InstrumentTypes.Cash.ToString
              NewReturnRow(ReturnTable_PriceLevel) = 1.0#
              NewReturnRow(ReturnTable_PriceDate) = Renaissance_BaseDate
              NewReturnRow(ReturnTable_CompoundFXRate) = 0.0#
              NewReturnRow(ReturnTable_SignedUnits) = 0.0#
              NewReturnRow(ReturnTable_Value) = 0.0#
              NewReturnRow(ReturnTable_BookedNotional) = 0.0#
              NewReturnRow(ReturnTable_neo_captureID) = 1
              NewReturnRow(ReturnTable_neo_marketvalue_rc_value) = TotalVariation_RC
              NewReturnRow(ReturnTable_neo_quantity_value) = TotalVariation_Local
              NewReturnRow(ReturnTable_neo_exchangerate_value) = 0.0#
              NewReturnRow(ReturnTable_neo_price_value) = 1.0#
              NewReturnRow(ReturnTable_neo_pricedate_value) = Renaissance_BaseDate

              NewReturnRow(ReturnTable_SectionSort) = 1

              ReturnTable.Rows.Add(NewReturnRow)

            End If

          Else
            ' Unknown currency to Venice, add to Catchall.

            TotalCatchAll_Local += TotalMarginDeposit_Local
            TotalCatchAll_RC += TotalMarginDeposit_RC

            TotalMarginDeposit_Local = 0.0#
            TotalMarginDeposit_RC = 0.0#
          End If

        End If

        ' Catch all

        If (TotalCatchAll_Local <> 0.0#) OrElse (TotalCatchAll_RC <> 0.0#) Then

          ' Add Catchall Row.

          NewReturnRow = ReturnTable.NewRow

          NewReturnRow(ReturnTable_rptISIN) = ""
          NewReturnRow(ReturnTable_rptCurrency) = thisCurrency
          NewReturnRow(ReturnTable_rptDescription) = "Other Availabilities " & thisCurrency
          NewReturnRow(ReturnTable_Instrument) = 0
          NewReturnRow(ReturnTable_rptInstrumentType) = InstrumentTypes.Cash.ToString ' Make it appear in the Cash group.
          NewReturnRow(ReturnTable_InstrumentTypeID) = InstrumentTypes.Cash
          NewReturnRow(ReturnTable_InstrumentType) = InstrumentTypes.Cash.ToString
          NewReturnRow(ReturnTable_PriceLevel) = 1.0#
          NewReturnRow(ReturnTable_PriceDate) = Renaissance_BaseDate
          NewReturnRow(ReturnTable_CompoundFXRate) = 0.0#
          NewReturnRow(ReturnTable_SignedUnits) = 0.0#
          NewReturnRow(ReturnTable_Value) = 0.0#
          NewReturnRow(ReturnTable_BookedNotional) = 0.0#
          NewReturnRow(ReturnTable_neo_captureID) = 1
          NewReturnRow(ReturnTable_neo_marketvalue_rc_value) = TotalCatchAll_RC
          NewReturnRow(ReturnTable_neo_quantity_value) = TotalCatchAll_Local
          NewReturnRow(ReturnTable_neo_exchangerate_value) = 0.0#
          NewReturnRow(ReturnTable_neo_price_value) = 1.0#
          NewReturnRow(ReturnTable_neo_pricedate_value) = Renaissance_BaseDate

          NewReturnRow(ReturnTable_SectionSort) = 2

          ReturnTable.Rows.Add(NewReturnRow)

        End If

      Next thisCurrency

      ' Just for a laugh...
      ' Order the Cash Section by currency using NewReturnRow(ReturnTable_SectionSort)

      SelectedReturnRows = ReturnTable.Select("rpt_InstrumentType='" & InstrumentTypes.Cash.ToString & "'")

      If (SelectedReturnRows IsNot Nothing) AndAlso (SelectedReturnRows.Length > 0) Then
        Dim CurrencyCodes() As String = CurrencyCodeDictionary.Keys.ToArray
        Array.Sort(CurrencyCodes)

        For Each AvailabilityReturnRow In SelectedReturnRows
          thisCurrency = AvailabilityReturnRow(ReturnTable_rptCurrency)

          If (thisCurrency = FundCurrencyCode) Then
            AvailabilityReturnRow(ReturnTable_SectionSort) = 1
          ElseIf CurrencyCodes.Contains(thisCurrency) Then
            AvailabilityReturnRow(ReturnTable_SectionSort) = (Array.IndexOf(CurrencyCodes, thisCurrency) + 1) * 2
          Else
            AvailabilityReturnRow(ReturnTable_SectionSort) = 999
          End If

          If (AvailabilityReturnRow(ReturnTable_rptISIN) = thisCurrency) Then
            AvailabilityReturnRow(ReturnTable_SectionSort) -= 1
          End If
        Next
      End If

      ' ************************************************************************
      ' Clear out zero value rows
      ' ************************************************************************
      Dim RemoveRowList As New ArrayList

      For Counter = 0 To (ReturnTable.Rows.Count - 1)
        NewReturnRow = ReturnTable.Rows(Counter)

        ' SQL filters on SignedSettlement. Should I ?
        If ((Math.Abs(CInt(NewReturnRow(ReturnTable_neo_captureID))) = 0) OrElse ((CDbl(NewReturnRow(ReturnTable_neo_quantity_value)) = 0.0#) AndAlso ((Math.Abs(CDbl(NewReturnRow(ReturnTable_neo_marketvalue_rc_value))) <= 0.1#)))) AndAlso _
          (((Math.Abs(CDbl(NewReturnRow(ReturnTable_SignedUnits))) <= 0.01#) OrElse (CInt(NewReturnRow(ReturnTable_InstrumentTypeID)) = InstrumentTypes.Expense) OrElse (CInt(NewReturnRow(ReturnTable_InstrumentTypeID)) = InstrumentTypes.Fee)) AndAlso (Math.Abs(CDbl(NewReturnRow(ReturnTable_Value))) <= 1.0#)) Then
          RemoveRowList.Add(NewReturnRow)
        End If

        'If (Math.Abs(CDbl(NewReturnRow(ReturnTable_SignedUnits))) <= 0.000001#) AndAlso _
        '  (Math.Abs(CDbl(NewReturnRow(ReturnTable_BookedNotional))) < 1.0#) AndAlso _
        '  (Math.Abs(CInt(NewReturnRow(ReturnTable_neo_captureID))) = 0) Then
        '  RemoveRowList.Add(NewReturnRow)
        'End If
      Next Counter

      For Each NewReturnRow In RemoveRowList
        ReturnTable.Rows.Remove(NewReturnRow)
      Next

    Catch ex As Exception
      If (lock_tblFundValuation.IsWriteLockHeld) Then
        lock_tblFundValuation.ExitWriteLock()
      End If

      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in CalculateFundValuation().", ex.StackTrace, True)
    Finally

      If (lock_tblFundValuation.IsWriteLockHeld) Then
        lock_tblFundValuation.ExitWriteLock()
      End If
    End Try

    Return ReturnTable

  End Function

  ''' <summary>
  ''' Loads the futures notional.
  ''' </summary>
  ''' <param name="MainForm">The main form.</param>
  ''' <param name="thisLockObject">The this lock object.</param>
  ''' <param name="FuturesNotionalsTable">The futures notionals table.</param>
  ''' <param name="FundID">The fund ID.</param>
  ''' <param name="ValueDate">The value date.</param>
  ''' <param name="StatusGroupFilter">The status group filter.</param>
  ''' <param name="AdministratorDatesFilter">The administrator dates filter.</param>
  ''' <param name="UnRealisedCalculationType">Type of the un realised calculation.</param>
  Private Sub LoadFuturesNotional(ByVal MainForm As VeniceMain, ByRef thisLockObject As ReaderWriterLockSlim, ByRef FuturesNotionalsTable As RenaissanceDataClass.DSFuturesNotional.tblFuturesNotionalDataTable, ByVal FundID As Integer, ByVal ValueDate As Date, ByVal StatusGroupFilter As String, ByVal AdministratorDatesFilter As Integer, ByVal UnRealisedCalculationType As CalculationTypes_Profit)
    ' *******************************************************************************
    '
    '
    ' *******************************************************************************
    Try
      Dim SelectCommand As SqlCommand = Nothing

      If (FuturesNotionalsTable Is Nothing) Then
        Exit Sub
      End If

      If (Not thisLockObject.IsWriteLockHeld) Then
        thisLockObject.EnterWriteLock()
      End If

      Try

        FuturesNotionalsTable = New RenaissanceDataClass.DSFuturesNotional.tblFuturesNotionalDataTable

        SelectCommand = New SqlCommand
        SelectCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT
        SelectCommand.Connection = MainForm.GetVeniceConnection()
        SelectCommand.CommandType = CommandType.StoredProcedure
        SelectCommand.CommandText = "adp_tblFuturesNotional"

        SelectCommand.Parameters.Add(New SqlParameter("@FundID", SqlDbType.Int)).Value = FundID
        SelectCommand.Parameters.Add(New SqlParameter("@ValueDate", SqlDbType.DateTime)).Value = ValueDate
        SelectCommand.Parameters.Add(New SqlParameter("@StatusGroupFilter", SqlDbType.NVarChar, 50)).Value = StatusGroupFilter
        SelectCommand.Parameters.Add(New SqlParameter("@AdministratorDatesFilter", SqlDbType.Int)).Value = AdministratorDatesFilter
        SelectCommand.Parameters.Add(New SqlParameter("@UnRealisedCalculationType", SqlDbType.Int)).Value = 0 ' Default to FIFO.
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", SqlDbType.DateTime)).Value = MainForm.Main_Knowledgedate

        MainForm.LoadTable_Custom(FuturesNotionalsTable, SelectCommand)
        'FuturesNotionalsTable.Load(SelectCommand.ExecuteReader)

      Catch ex As Exception
        MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in LoadFuturesNotional().", ex.StackTrace, False)
      Finally
        If (SelectCommand IsNot Nothing) Then
          If (SelectCommand.Connection IsNot Nothing) AndAlso (SelectCommand.Connection.State And ConnectionState.Open) Then
            SelectCommand.Connection.Close()
          End If
          SelectCommand.Connection = Nothing
        End If
        SelectCommand = Nothing
      End Try

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in LoadFuturesNotional().", ex.StackTrace, False)
    Finally
      If (thisLockObject.IsWriteLockHeld) Then
        thisLockObject.ExitWriteLock()
      End If
    End Try

  End Sub

 

    ''' <summary>
    ''' Handles the Click event of the Button1 control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Test.Click

    'Call InitialiseFeesByClassGrid(CInt(Combo_Fund.SelectedValue))

    'Call PaintFeesByClassGrid(CInt(Combo_Fund.SelectedValue))

    Try
      ' **********************************************************************
      ' Use the Connection Update Event to refresh all tables
      ' **********************************************************************

      MainForm.PertracData.ClearDataCache()
      MainForm.StatFunctions.ClearCache()

      If (FundAvailabilities IsNot Nothing) Then FundAvailabilities.Clear()
      If (FuturesNotionalsTable IsNot Nothing) Then FuturesNotionalsTable.Clear()
			If (BestPricesTable IsNot Nothing) Then BestPricesTable.Clear()
			If (ClassSpecificPnLTable IsNot Nothing) Then
				ClassSpecificPnLTable.Clear()
				ClassSpecificPnLTable = Nothing
			End If

      AdministratorInventoryCache.Clear()
      AdministratorHoldingsCache.Clear()
      PriceRowCache.Clear()

      Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.Connection))

    Catch ex As Exception
    End Try

  End Sub


  Private Enum FeeGrid_GreenLines As Integer
    ' Lines that need to be green. (Administrator)

    YesterdayAdmin_UnitCount
    YesterdayAdmin_UnitPrice
    YesterdayAdmin_Value
    YesterdayAdmin_Percent
    Modified_AdminPercent
    YesterdayAdmin_Currency
    YesterdayAdmin_CurrencyID
    YesterdayAdmin_FXRate

    ClassSpecific_ManagementFeesAdministrator
    ClassSpecific_PerformanceFeesAdministrator
    ClassSpecific_CrystalisedFeesAdministrator

    TodaysAdmin_UnitCount
    TodaysAdmin_UnitPrice
    TodaysAdmin_Value
    TodaysAdmin_Percent

  End Enum

  Private Enum FeeGrid_PinkLines As Integer
    ' Lines that need to be pink. (Highlighted for fees spreadsheet)

    Venice_BNAV

  End Enum

  Private Enum FeeGrid_Section1Headers As Integer
    ' 
    InstrumentDetails
    YesterdayDetails
    General_Header
    ClassSpecific_Header
    VeniceTotals_Header
    TodaysAdmin_Header
    FeesToBook_Header
  End Enum

  Private Enum FeeGrid_RowIndex As Integer

    Header = 0
    InstrumentDetails
    InstrumentID
    InstrumentClass
    ISIN
    Separator_0
    YesterdayDetails
    YesterdayAdmin_UnitCount
    YesterdayAdmin_UnitPrice
    YesterdayAdmin_Value
    YesterdayAdmin_Percent
    Period_SubscriptionValue
    Modified_AdminPercent
    YesterdayAdmin_Currency
    YesterdayAdmin_CurrencyID
    YesterdayAdmin_FXRate

    Separator_1
    General_Header
    General_Profit
		ClassSpecific_Profit
		Venice_BNAV
		Separator_2
		ClassSpecific_Header
    ClassSpecific_ManagementFeesRate
    ClassSpecific_ManagementFeesVenice
    ClassSpecific_ManagementFeesChanged
    ClassSpecific_ManagementFeesAdministrator
    ClassSpecific_ManagementFeesValue

    Performance_Start
    ClassSpecific_Performance_BestBenchmark
    ClassSpecific_Performance_BenchmarkPerformance
    ClassSpecific_Performance_BenchmarkValueIncrease
    ClassSpecific_Performance_StartValue
    ClassSpecific_Performance_EndValue
    ClassSpecific_Performance_ClassPerformance
    ClassSpecific_Performance_ClassValueIncrease
    ClassSpecific_Performance_ClassOutperformance
    Separator_Performance_End

    ClassSpecific_PreviousPerformanceFees
    ClassSpecific_Performance_FeeRate
    ClassSpecific_PerformanceFeesVenice
    ClassSpecific_PerformanceFeesChanged
    ClassSpecific_PerformanceFeesAdministrator
    ClassSpecific_PerformanceFeesValue
    ClassSpecific_CrystalisedFeesVenice
    ClassSpecific_CrystalisedFeesChanged
    ClassSpecific_CrystalisedFeesAdministrator
    ClassSpecific_CrystalisedFeesValue
    Separator_10
    VeniceTotals_Header
    VeniceOpening_Units
    Venice_UnitsSubscribed
    Venice_UnitsRedeemed
    VeniceTotals_Units
    VeniceTotals_Value
    VeniceTotals_Price
    CurrentFXRate
    Separator_11
    TodaysAdmin_Header
    TodaysAdmin_UnitCount
    TodaysAdmin_UnitPrice
    TodaysAdmin_Value
    TodaysAdmin_Percent
    Separator_12
    Discrepancy
    Separator_20
    FeesToBook_Header
    ManagementFeesBooked
    ManagementFeesToBook
    PerformanceFeesBooked
    PerformanceFeesToBook
    CrystalisedFeesBooked
    CrystalisedFeesToBook
    Separator_22


    MaxValue
  End Enum

  Private Function GetFeeGridStyle(ByVal pStyleName As String, ByVal RowIndex As FeeGrid_RowIndex) As C1.Win.C1FlexGrid.CellStyle

    Dim RVal As C1.Win.C1FlexGrid.CellStyle = Nothing

    Try
      Dim RowName As String = RowIndex.ToString

      If [Enum].GetNames(GetType(FeeGrid_GreenLines)).Contains(RowName) Then
        RVal = Grid_FeesMultiClass.Styles("Admin_" & pStyleName)
      ElseIf [Enum].GetNames(GetType(FeeGrid_PinkLines)).Contains(RowName) Then
        RVal = Grid_FeesMultiClass.Styles("Spreadsheet_" & pStyleName)
      Else
        RVal = Grid_FeesMultiClass.Styles(pStyleName)
      End If

    Catch ex As Exception
    End Try

    If (RVal IsNot Nothing) Then
      Return RVal
    End If

    Return Grid_FeesMultiClass.Styles.Normal

  End Function

  Private Sub InitialiseFeesByClassGrid(ByVal FundID As Integer)

    Dim Col_Descriptions As Integer
    Dim Col_Totals As Integer
    Dim Col_Difference As Integer
    Dim Col_Venice As Integer
    Dim Level As Integer = 0

    Try
      Dim ShareClassCount As Integer = 0
      Dim RowCounter As Integer
      Dim ColCounter As Integer
      Dim RowIsHidden As Boolean = False
      Dim thisRowIsHidden As Boolean = False
      Dim RowName As String
      Dim thisGridRow As C1.Win.C1FlexGrid.Row

      Col_Descriptions = Grid_FeesMultiClass.Cols("col_Descriptions").Index
      Col_Totals = Grid_FeesMultiClass.Cols("col_Totals").Index
      Col_Difference = Grid_FeesMultiClass.Cols("col_Difference").Index
      Col_Venice = Grid_FeesMultiClass.Cols("col_Venice").Index

      Grid_FeesMultiClass.Rows.Count = 1
      Grid_FeesMultiClass.Tree.Column = 0
      Level = 1

      For RowCounter = 1 To (FeeGrid_RowIndex.MaxValue - 1)
        RowName = CType(RowCounter, FeeGrid_RowIndex).ToString

        If ([Enum].GetNames(GetType(FeeGrid_Section1Headers)).Contains(RowName)) Then
          Level = 0
          thisGridRow = Grid_FeesMultiClass.Rows.InsertNode(RowCounter, Level).Row
          Level += 1
        Else
          thisGridRow = Grid_FeesMultiClass.Rows.InsertNode(RowCounter, Level).Row

          If Level = 0 Then Level = 1
        End If

        If RowName.StartsWith("Separator_") Then
          ' Set class for Separator lines
          Grid_FeesMultiClass.Rows(RowCounter).Style = Grid_FeesMultiClass.Styles("Separator")
          Grid_FeesMultiClass.Rows(RowCounter).Height = 3
        Else
          Grid_FeesMultiClass.Rows(RowCounter).Height = 20
        End If

        ' Colapsed ?
        If RowName.EndsWith("_Start") Then
          Level += 1
          RowIsHidden = True
        ElseIf RowName.EndsWith("_End") Then
          Level -= 1
          RowIsHidden = False
        End If

        ' Editable ? (In part)

        Select Case CType(RowCounter, FeeGrid_RowIndex)

          Case FeeGrid_RowIndex.ClassSpecific_ManagementFeesValue, FeeGrid_RowIndex.ClassSpecific_PerformanceFeesValue, FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesValue
            Grid_FeesMultiClass.Rows(RowCounter).AllowEditing = True

          Case FeeGrid_RowIndex.ClassSpecific_ManagementFeesChanged, FeeGrid_RowIndex.ClassSpecific_PerformanceFeesChanged, FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesChanged
            Grid_FeesMultiClass.Rows(RowCounter).AllowEditing = False
            Grid_FeesMultiClass.Rows(RowCounter).Height = 1
            thisRowIsHidden = True

          Case Else
            Grid_FeesMultiClass.Rows(RowCounter).AllowEditing = False

        End Select

        'If (RowIsHidden Or thisRowIsHidden) Then
        '  Grid_FeesMultiClass.Rows(RowCounter).Height = 1
        'End If
      Next

      'Grid_FeesMultiClass.Rows.Count = FeeGrid_RowIndex.MaxValue

      'For RowCounter = 0 To (FeeGrid_RowIndex.MaxValue - 1)
      '  RowName = CType(RowCounter, FeeGrid_RowIndex).ToString
      '  thisRowIsHidden = False

      '  If RowName.StartsWith("Separator_") Then
      '    ' Set class for Separator lines
      '    Grid_FeesMultiClass.Rows(RowCounter).Style = Grid_FeesMultiClass.Styles("Separator")
      '    Grid_FeesMultiClass.Rows(RowCounter).Height = 3

      '    ' Colapsed ?
      '    If RowName.EndsWith("_Start") Then
      '      RowIsHidden = True
      '    ElseIf RowName.EndsWith("_End") Then
      '      RowIsHidden = False
      '    End If

      '  Else
      '    Grid_FeesMultiClass.Rows(RowCounter).Height = 20

      '  End If

      '  ' Editable ? (In part)

      '  Select Case CType(RowCounter, FeeGrid_RowIndex)

      '    Case FeeGrid_RowIndex.ClassSpecific_ManagementFeesValue, FeeGrid_RowIndex.ClassSpecific_PerformanceFeesValue, FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesValue
      '      Grid_FeesMultiClass.Rows(RowCounter).AllowEditing = True

      '    Case FeeGrid_RowIndex.ClassSpecific_ManagementFeesChanged, FeeGrid_RowIndex.ClassSpecific_PerformanceFeesChanged, FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesChanged
      '      Grid_FeesMultiClass.Rows(RowCounter).AllowEditing = False
      '      thisRowIsHidden = True

      '    Case Else
      '      Grid_FeesMultiClass.Rows(RowCounter).AllowEditing = False

      '  End Select

      '  Grid_FeesMultiClass.Rows(RowCounter).Visible = Not (RowIsHidden Or thisRowIsHidden)
      'Next

      ' Set Fixed text

      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.InstrumentDetails)(Col_Descriptions) = "Instrument Details"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.InstrumentID)(Col_Descriptions) = "InstrumentID"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.YesterdayDetails)(Col_Descriptions) = "Opening status (Previous close)."
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.YesterdayAdmin_UnitCount)(Col_Descriptions) = "Opening Units (Admin)"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.YesterdayAdmin_UnitPrice)(Col_Descriptions) = "Opening Price (Admin)"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.YesterdayAdmin_Value)(Col_Descriptions) = "Opening Value (Admin)"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.YesterdayAdmin_Percent)(Col_Descriptions) = "Weighting Percentage"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.Period_SubscriptionValue)(Col_Descriptions) = "Net Value Subscribed"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.Modified_AdminPercent)(Col_Descriptions) = "Modified Weighting"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.YesterdayAdmin_Currency)(Col_Descriptions) = "Unit Currency"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.YesterdayAdmin_CurrencyID)(Col_Descriptions) = "Currency ID"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.YesterdayAdmin_FXRate)(Col_Descriptions) = "FX Rate"

      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.General_Header)(Col_Descriptions) = "P&L today"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.General_Profit)(Col_Descriptions) = "General Profit / Loss"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.ClassSpecific_Profit)(Col_Descriptions) = "Specific P&L"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.Venice_BNAV)(Col_Descriptions) = "NAV before Fees"

      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.ClassSpecific_Header)(Col_Descriptions) = "Today's Fees"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.ClassSpecific_ManagementFeesRate)(Col_Descriptions) = "Fees % (Venice)"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.ClassSpecific_ManagementFeesVenice)(Col_Descriptions) = "Management Fees (Venice)"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.ClassSpecific_Performance_FeeRate)(Col_Descriptions) = "Fees % (Venice)"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesVenice)(Col_Descriptions) = "Performance Fees (Venice)"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesVenice)(Col_Descriptions) = "Crystalised Fees (Venice)"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.ClassSpecific_ManagementFeesAdministrator)(Col_Descriptions) = "Management Fees (Admin)"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesAdministrator)(Col_Descriptions) = "Performance Fees (Admin)"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesAdministrator)(Col_Descriptions) = "Crystalised Fees (Admin)"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.ClassSpecific_ManagementFeesValue)(Col_Descriptions) = "Management Fees to book"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesValue)(Col_Descriptions) = "Performance Fees to book"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesValue)(Col_Descriptions) = "Crystalised Fees to book"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.ClassSpecific_PreviousPerformanceFees)(Col_Descriptions) = "Previous Performance Fees (Venice)"

      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.VeniceTotals_Header)(Col_Descriptions) = "Venice Valuations."
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.VeniceOpening_Units)(Col_Descriptions) = "Venice Opening Units"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.Venice_UnitsSubscribed)(Col_Descriptions) = "Venice Units Subscribed"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.Venice_UnitsRedeemed)(Col_Descriptions) = "Venice Units Redeemed"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.VeniceTotals_Units)(Col_Descriptions) = "Closing Units"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.VeniceTotals_Value)(Col_Descriptions) = "Closing Value"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.VeniceTotals_Price)(Col_Descriptions) = "Closing Price"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.CurrentFXRate)(Col_Descriptions) = "Current FX Rate"

      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.TodaysAdmin_Header)(Col_Descriptions) = "Administrator Valuation"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.TodaysAdmin_UnitCount)(Col_Descriptions) = "Administrator Units"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.TodaysAdmin_UnitPrice)(Col_Descriptions) = "Administrator Price"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.TodaysAdmin_Value)(Col_Descriptions) = "Administrator Value"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.TodaysAdmin_Percent)(Col_Descriptions) = "Percent"

      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.Discrepancy)(Col_Descriptions) = "Difference"

      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.FeesToBook_Header)(Col_Descriptions) = "Fees Pending and Booked"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.ManagementFeesBooked)(Col_Descriptions) = "Management Fees Booked"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.ManagementFeesToBook)(Col_Descriptions) = "Management Fees To Book"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.PerformanceFeesBooked)(Col_Descriptions) = "Performance Fees Booked"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.PerformanceFeesToBook)(Col_Descriptions) = "Performance Fees To Book"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.CrystalisedFeesBooked)(Col_Descriptions) = "Crystalised Fees Booked"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.CrystalisedFeesToBook)(Col_Descriptions) = "Crystalised Fees To Book"

      '

      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.Header)(Col_Totals) = "Difference"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.Header)(Col_Totals) = "Totals"

      Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.TodaysAdmin_Value, Col_Venice, 0.0#)
      Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.TodaysAdmin_Value, Col_Totals, 0.0#)
      Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.TodaysAdmin_Value, Col_Difference, 0.0#)

      Grid_FeesMultiClass.Cols(Col_Difference)(FeeGrid_RowIndex.ManagementFeesBooked) = 0.0#
      Grid_FeesMultiClass.Cols(Col_Difference)(FeeGrid_RowIndex.PerformanceFeesBooked) = 0.0#
      Grid_FeesMultiClass.Cols(Col_Difference)(FeeGrid_RowIndex.CrystalisedFeesBooked) = 0.0#

      Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesAdministrator, Col_Totals, 0.0#)

      ' Columns

      Dim InstrumentsDataset As RenaissanceDataClass.DSInstrument = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblInstrument), RenaissanceDataClass.DSInstrument)
      Dim InstrumentsTable As RenaissanceDataClass.DSInstrument.tblInstrumentDataTable = InstrumentsDataset.tblInstrument
      Dim FundUnitInstruments() As RenaissanceDataClass.DSInstrument.tblInstrumentRow

      If (FundID = 0) Then
        FundUnitInstruments = InstrumentsTable.Select("false", "InstrumentDescription")
      Else
        FundUnitInstruments = InstrumentsTable.Select("InstrumentFundID=" & FundID.ToString, "InstrumentDescription")
      End If

      Dim ColsToDelete As New ArrayList
      Dim NewCol As C1.Win.C1FlexGrid.Column

      ColCounter = 1 ' first Unit column in the grid

      For RowCounter = 0 To (FundUnitInstruments.Length - 1)
        While (CInt(Nz(Grid_FeesMultiClass.GetData(FeeGrid_RowIndex.InstrumentID, ColCounter), 0)) <> FundUnitInstruments(RowCounter).InstrumentID) And (ColCounter < Col_Totals)
          ColsToDelete.Add(Grid_FeesMultiClass.Cols(ColCounter))
          ColCounter += 1
        End While

        If (CInt(Nz(Grid_FeesMultiClass.GetData(FeeGrid_RowIndex.InstrumentID, ColCounter), 0)) <> FundUnitInstruments(RowCounter).InstrumentID) Then ' Insert Row
          NewCol = Grid_FeesMultiClass.Cols.Insert(ColCounter)

          NewCol.Width = 100
        Else
          NewCol = Grid_FeesMultiClass.Cols(ColCounter)
        End If

        NewCol(FeeGrid_RowIndex.InstrumentID) = FundUnitInstruments(RowCounter).InstrumentID
        NewCol(FeeGrid_RowIndex.Header) = FundUnitInstruments(RowCounter).InstrumentClass
        NewCol(FeeGrid_RowIndex.InstrumentClass) = FundUnitInstruments(RowCounter).InstrumentClass
        NewCol(FeeGrid_RowIndex.ISIN) = FundUnitInstruments(RowCounter).InstrumentISIN

        ' Formatting

        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.YesterdayAdmin_UnitCount, ColCounter, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.YesterdayAdmin_UnitCount))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.VeniceOpening_Units, ColCounter, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.VeniceOpening_Units))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.YesterdayAdmin_UnitPrice, ColCounter, GetFeeGridStyle("Numeric4", FeeGrid_RowIndex.YesterdayAdmin_UnitPrice))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.YesterdayAdmin_Value, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.YesterdayAdmin_Value))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.YesterdayAdmin_Percent, ColCounter, GetFeeGridStyle("Percentage", FeeGrid_RowIndex.YesterdayAdmin_Percent))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.Period_SubscriptionValue, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.Period_SubscriptionValue))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.Venice_UnitsSubscribed, ColCounter, GetFeeGridStyle("Numeric4", FeeGrid_RowIndex.Venice_UnitsSubscribed))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.Venice_UnitsRedeemed, ColCounter, GetFeeGridStyle("Numeric4", FeeGrid_RowIndex.Venice_UnitsRedeemed))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.Modified_AdminPercent, ColCounter, GetFeeGridStyle("Percentage", FeeGrid_RowIndex.Modified_AdminPercent))

        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.YesterdayAdmin_Currency, ColCounter, GetFeeGridStyle("TextRight", FeeGrid_RowIndex.YesterdayAdmin_Currency))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.YesterdayAdmin_CurrencyID, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.YesterdayAdmin_CurrencyID))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.YesterdayAdmin_FXRate, ColCounter, GetFeeGridStyle("Numeric6", FeeGrid_RowIndex.YesterdayAdmin_FXRate))

        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.TodaysAdmin_UnitCount, ColCounter, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.TodaysAdmin_UnitCount))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.TodaysAdmin_UnitPrice, ColCounter, GetFeeGridStyle("Numeric4", FeeGrid_RowIndex.TodaysAdmin_UnitPrice))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.TodaysAdmin_Value, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.TodaysAdmin_Value))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.TodaysAdmin_Percent, ColCounter, GetFeeGridStyle("Percentage", FeeGrid_RowIndex.TodaysAdmin_Percent))

        ''Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.Header, ColCounter, Style_TextMiddle)
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.InstrumentClass, ColCounter, GetFeeGridStyle("TextMiddle", FeeGrid_RowIndex.InstrumentClass)) 'Style_TextMiddle)
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ISIN, ColCounter, GetFeeGridStyle("TextRight", FeeGrid_RowIndex.ISIN))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.InstrumentClass, ColCounter, GetFeeGridStyle("TextRight", FeeGrid_RowIndex.InstrumentClass))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.FundID, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.FundID))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.InstrumentID, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.InstrumentID))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.General_Profit, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.General_Profit))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.Venice_BNAV, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.Venice_BNAV))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Profit, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.ClassSpecific_Profit))

        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_ManagementFeesRate, ColCounter, GetFeeGridStyle("Percentage", FeeGrid_RowIndex.ClassSpecific_ManagementFeesRate))

        If (CInt(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_ManagementFeesChanged), 0))) = 0 Then
          Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_ManagementFeesValue, ColCounter, GetFeeGridStyle("Numeric2Editable", FeeGrid_RowIndex.ClassSpecific_ManagementFeesValue))
        End If
        If (CInt(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesChanged), 0))) = 0 Then
          Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesValue, ColCounter, GetFeeGridStyle("Numeric2Editable", FeeGrid_RowIndex.ClassSpecific_PerformanceFeesValue))
        End If
        If (CInt(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesChanged), 0))) = 0 Then
          Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesValue, ColCounter, GetFeeGridStyle("Numeric2Editable", FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesValue))
        End If

        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Performance_FeeRate, ColCounter, GetFeeGridStyle("Percentage", FeeGrid_RowIndex.ClassSpecific_Performance_FeeRate))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Performance_BestBenchmark, ColCounter, GetFeeGridStyle("TextRight", FeeGrid_RowIndex.ClassSpecific_Performance_BestBenchmark)) ' Style_TextRight)
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Performance_BenchmarkPerformance, ColCounter, GetFeeGridStyle("Percentage", FeeGrid_RowIndex.ClassSpecific_Performance_BenchmarkPerformance))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Performance_BenchmarkValueIncrease, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.ClassSpecific_Performance_BenchmarkValueIncrease))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Performance_StartValue, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.ClassSpecific_Performance_StartValue))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Performance_EndValue, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.ClassSpecific_Performance_EndValue))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Performance_ClassPerformance, ColCounter, GetFeeGridStyle("Percentage", FeeGrid_RowIndex.ClassSpecific_Performance_ClassPerformance))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Performance_ClassValueIncrease, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.ClassSpecific_Performance_ClassValueIncrease))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Performance_ClassOutperformance, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.ClassSpecific_Performance_ClassOutperformance))

        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.VeniceTotals_Value, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.VeniceTotals_Value))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.VeniceTotals_Units, ColCounter, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.VeniceTotals_Units))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.VeniceTotals_Price, ColCounter, GetFeeGridStyle("Numeric4", FeeGrid_RowIndex.VeniceTotals_Price))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.CurrentFXRate, ColCounter, GetFeeGridStyle("Numeric6", FeeGrid_RowIndex.CurrentFXRate))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.Discrepancy, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.Discrepancy))

        ' 

        ColCounter += 1
      Next

      If (ColCounter < Col_Totals) Then
        While (ColCounter < Col_Totals)
          ColsToDelete.Add(Grid_FeesMultiClass.Cols(ColCounter))
          ColCounter += 1
        End While
      End If

      For Each NewCol In ColsToDelete
        Grid_FeesMultiClass.Cols.Remove(NewCol)
      Next

      Col_Difference = Grid_FeesMultiClass.Cols("col_Difference").Index

      For ColCounter = (Col_Descriptions + 1) To (Col_Difference)
        ' Formatting

        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.YesterdayAdmin_UnitCount, ColCounter, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.YesterdayAdmin_UnitCount))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.VeniceOpening_Units, ColCounter, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.VeniceOpening_Units))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.YesterdayAdmin_UnitPrice, ColCounter, GetFeeGridStyle("Numeric4", FeeGrid_RowIndex.YesterdayAdmin_UnitPrice))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.YesterdayAdmin_Value, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.YesterdayAdmin_Value))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.YesterdayAdmin_Percent, ColCounter, GetFeeGridStyle("Percentage", FeeGrid_RowIndex.YesterdayAdmin_Percent))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.Period_SubscriptionValue, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.Period_SubscriptionValue))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.Venice_UnitsSubscribed, ColCounter, GetFeeGridStyle("Numeric4", FeeGrid_RowIndex.Venice_UnitsSubscribed))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.Venice_UnitsRedeemed, ColCounter, GetFeeGridStyle("Numeric4", FeeGrid_RowIndex.Venice_UnitsRedeemed))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.Modified_AdminPercent, ColCounter, GetFeeGridStyle("Percentage", FeeGrid_RowIndex.Modified_AdminPercent))

        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.YesterdayAdmin_Currency, ColCounter, GetFeeGridStyle("TextRight", FeeGrid_RowIndex.YesterdayAdmin_Currency))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.YesterdayAdmin_CurrencyID, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.YesterdayAdmin_CurrencyID))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.YesterdayAdmin_FXRate, ColCounter, GetFeeGridStyle("Numeric6", FeeGrid_RowIndex.YesterdayAdmin_FXRate))

        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.TodaysAdmin_UnitCount, ColCounter, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.TodaysAdmin_UnitCount))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.TodaysAdmin_UnitPrice, ColCounter, GetFeeGridStyle("Numeric4", FeeGrid_RowIndex.TodaysAdmin_UnitPrice))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.TodaysAdmin_Value, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.TodaysAdmin_Value))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.TodaysAdmin_Percent, ColCounter, GetFeeGridStyle("Percentage", FeeGrid_RowIndex.TodaysAdmin_Percent))

        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.Header, ColCounter, Style_TextMiddle)
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.InstrumentClass, ColCounter, GetFeeGridStyle("TextMiddle", FeeGrid_RowIndex.InstrumentClass)) 'Style_TextMiddle)
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ISIN, ColCounter, GetFeeGridStyle("TextRight", FeeGrid_RowIndex.ISIN))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.InstrumentClass, ColCounter, GetFeeGridStyle("TextRight", FeeGrid_RowIndex.InstrumentClass))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.InstrumentID, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.InstrumentID))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.General_Profit, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.General_Profit))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.Venice_BNAV, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.Venice_BNAV))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Profit, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.ClassSpecific_Profit))

        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_ManagementFeesRate, ColCounter, GetFeeGridStyle("Percentage", FeeGrid_RowIndex.ClassSpecific_ManagementFeesRate))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Performance_FeeRate, ColCounter, GetFeeGridStyle("Percentage", FeeGrid_RowIndex.ClassSpecific_Performance_FeeRate))

        'If (ColCounter < Col_Totals) Then
        '  If (CInt(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_ManagementFeesChanged), 0))) = 0 Then
        '    Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_ManagementFees, ColCounter, GetFeeGridStyle("Numeric2Editable", FeeGrid_RowIndex.ClassSpecific_ManagementFees))
        '  End If
        '  If (CInt(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesChanged), 0))) = 0 Then
        '    Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_PerformanceFees, ColCounter, GetFeeGridStyle("Numeric2Editable", FeeGrid_RowIndex.ClassSpecific_PerformanceFees))
        '  End If
        '  If (CInt(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesChanged), 0))) = 0 Then
        '    Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_CrystalisedFees, ColCounter, GetFeeGridStyle("Numeric2Editable", FeeGrid_RowIndex.ClassSpecific_CrystalisedFees))
        '  End If
        'End If

        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_ManagementFeesAdministrator, ColCounter, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.ClassSpecific_ManagementFeesAdministrator))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesAdministrator, ColCounter, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.ClassSpecific_PerformanceFeesAdministrator))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesAdministrator, ColCounter, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesAdministrator))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_ManagementFeesVenice, ColCounter, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.ClassSpecific_ManagementFeesVenice))

        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Performance_FeeRate, ColCounter, GetFeeGridStyle("Percentage", FeeGrid_RowIndex.ClassSpecific_Performance_FeeRate))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Performance_BestBenchmark, ColCounter, GetFeeGridStyle("TextRight", FeeGrid_RowIndex.ClassSpecific_Performance_BestBenchmark)) ' Style_TextRight)
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Performance_BenchmarkPerformance, ColCounter, GetFeeGridStyle("Percentage", FeeGrid_RowIndex.ClassSpecific_Performance_BenchmarkPerformance))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Performance_BenchmarkValueIncrease, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.ClassSpecific_Performance_BenchmarkValueIncrease))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Performance_StartValue, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.ClassSpecific_Performance_StartValue))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Performance_EndValue, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.ClassSpecific_Performance_EndValue))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Performance_ClassPerformance, ColCounter, GetFeeGridStyle("Percentage", FeeGrid_RowIndex.ClassSpecific_Performance_ClassPerformance))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Performance_ClassValueIncrease, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.ClassSpecific_Performance_ClassValueIncrease))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Performance_ClassOutperformance, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.ClassSpecific_Performance_ClassOutperformance))

        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.VeniceTotals_Value, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.VeniceTotals_Value))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.VeniceTotals_Units, ColCounter, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.VeniceTotals_Units))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.VeniceTotals_Price, ColCounter, GetFeeGridStyle("Numeric4", FeeGrid_RowIndex.VeniceTotals_Price))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.CurrentFXRate, ColCounter, GetFeeGridStyle("Numeric6", FeeGrid_RowIndex.CurrentFXRate))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.Discrepancy, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.Discrepancy))

      Next

      '

      'InstrumentDetails()
      'YesterdayDetails()
      'General_Header()
      'ClassSpecific_Header()
      'VeniceTotals_Header()
      'TodaysAdmin_Header()
      'FeesToBook_Header()

      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.VeniceTotals_Header).Node.Collapsed = True
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.General_Header).Node.Collapsed = True
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.YesterdayDetails).Node.Collapsed = True
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.InstrumentDetails).Node.Collapsed = True
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.Performance_Start).Node.Collapsed = True

    Catch ex As Exception

    End Try


  End Sub

  Private Sub InitialiseFeesByClassGrid_org(ByVal FundID As Integer)

    Dim Col_Descriptions As Integer
    Dim Col_Totals As Integer
    Dim Col_Difference As Integer
    Dim Col_Venice As Integer

    Try
      Dim ShareClassCount As Integer = 0
      Dim RowCounter As Integer
      Dim ColCounter As Integer
      Dim RowIsHidden As Boolean = False
      Dim thisRowIsHidden As Boolean = False
      Dim RowName As String

      Col_Descriptions = Grid_FeesMultiClass.Cols("col_Descriptions").Index
      Col_Totals = Grid_FeesMultiClass.Cols("col_Totals").Index
      Col_Difference = Grid_FeesMultiClass.Cols("col_Difference").Index
      Col_Venice = Grid_FeesMultiClass.Cols("col_Venice").Index

      Grid_FeesMultiClass.Rows.Count = FeeGrid_RowIndex.MaxValue

      For RowCounter = 0 To (FeeGrid_RowIndex.MaxValue - 1)
        RowName = CType(RowCounter, FeeGrid_RowIndex).ToString
        thisRowIsHidden = False

        If RowName.StartsWith("Separator_") Then
          ' Set class for Separator lines
          Grid_FeesMultiClass.Rows(RowCounter).Style = Grid_FeesMultiClass.Styles("Separator")
          Grid_FeesMultiClass.Rows(RowCounter).Height = 3

          ' Colapsed ?
          If RowName.EndsWith("_Start") Then
            RowIsHidden = True
          ElseIf RowName.EndsWith("_End") Then
            RowIsHidden = False
          End If

        Else
          Grid_FeesMultiClass.Rows(RowCounter).Height = 20

        End If

        ' Editable ? (In part)

        Select Case CType(RowCounter, FeeGrid_RowIndex)

          Case FeeGrid_RowIndex.ClassSpecific_ManagementFeesValue, FeeGrid_RowIndex.ClassSpecific_PerformanceFeesValue, FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesValue
            Grid_FeesMultiClass.Rows(RowCounter).AllowEditing = True

          Case FeeGrid_RowIndex.ClassSpecific_ManagementFeesChanged, FeeGrid_RowIndex.ClassSpecific_PerformanceFeesChanged, FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesChanged
            Grid_FeesMultiClass.Rows(RowCounter).AllowEditing = False
            Grid_FeesMultiClass.Rows(RowCounter).Height = 1
            thisRowIsHidden = True

          Case Else
            Grid_FeesMultiClass.Rows(RowCounter).AllowEditing = False

        End Select

        Grid_FeesMultiClass.Rows(RowCounter).Visible = Not (RowIsHidden Or thisRowIsHidden)
      Next

      ' Set Fixed text

      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.InstrumentID)(Col_Descriptions) = "InstrumentID"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.YesterdayAdmin_UnitCount)(Col_Descriptions) = "Opening Units"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.YesterdayAdmin_UnitPrice)(Col_Descriptions) = "Opening Price"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.YesterdayAdmin_Value)(Col_Descriptions) = "Opening Value"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.YesterdayAdmin_Percent)(Col_Descriptions) = "Weighting Percentage"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.Period_SubscriptionValue)(Col_Descriptions) = "Net Value Subscribed"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.Modified_AdminPercent)(Col_Descriptions) = "Modified Weighting"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.YesterdayAdmin_Currency)(Col_Descriptions) = "Unit Currency"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.YesterdayAdmin_CurrencyID)(Col_Descriptions) = "Currency ID"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.YesterdayAdmin_FXRate)(Col_Descriptions) = "FX Rate"

      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.General_Header)(Col_Descriptions) = "P&L"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.General_Profit)(Col_Descriptions) = "General Profit / Loss"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.ClassSpecific_Profit)(Col_Descriptions) = "Specific P&L"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.Venice_BNAV)(Col_Descriptions) = "NAV before Fees"

      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.ClassSpecific_Header)(Col_Descriptions) = "Class Fees"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.ClassSpecific_ManagementFeesRate)(Col_Descriptions) = "Fees % (Venice)"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.ClassSpecific_ManagementFeesVenice)(Col_Descriptions) = "Management Fees (Venice)"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesVenice)(Col_Descriptions) = "Performance Fees (Venice)"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesVenice)(Col_Descriptions) = "Crystalised Fees (Venice)"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.ClassSpecific_ManagementFeesAdministrator)(Col_Descriptions) = "Management Fees (Admin)"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesAdministrator)(Col_Descriptions) = "Performance Fees (Admin)"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesAdministrator)(Col_Descriptions) = "Crystalised Fees (Admin)"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.ClassSpecific_ManagementFeesValue)(Col_Descriptions) = "Management Fees to book"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesValue)(Col_Descriptions) = "Performance Fees to book"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesValue)(Col_Descriptions) = "Crystalised Fees to book"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.ClassSpecific_PreviousPerformanceFees)(Col_Descriptions) = "Previous Performance Fees (Venice)"

      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.VeniceTotals_Header)(Col_Descriptions) = "Venice"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.VeniceOpening_Units)(Col_Descriptions) = "Venice Opening Units"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.Venice_UnitsSubscribed)(Col_Descriptions) = "Venice Units Subscribed"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.Venice_UnitsRedeemed)(Col_Descriptions) = "Venice Units Redeemed"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.VeniceTotals_Units)(Col_Descriptions) = "Closing Units"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.VeniceTotals_Value)(Col_Descriptions) = "Closing Value"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.VeniceTotals_Price)(Col_Descriptions) = "Closing Price"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.CurrentFXRate)(Col_Descriptions) = "Current FX Rate"

      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.TodaysAdmin_UnitCount)(Col_Descriptions) = "Administrator Units"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.TodaysAdmin_UnitPrice)(Col_Descriptions) = "Administrator Price"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.TodaysAdmin_Value)(Col_Descriptions) = "Administrator Value"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.TodaysAdmin_Percent)(Col_Descriptions) = "Percent"

      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.Discrepancy)(Col_Descriptions) = "Difference"

      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.ManagementFeesBooked)(Col_Descriptions) = "Management Fees Booked"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.ManagementFeesToBook)(Col_Descriptions) = "Management Fees To Book"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.PerformanceFeesBooked)(Col_Descriptions) = "Performance Fees Booked"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.PerformanceFeesToBook)(Col_Descriptions) = "Performance Fees To Book"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.CrystalisedFeesBooked)(Col_Descriptions) = "Crystalised Fees Booked"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.CrystalisedFeesToBook)(Col_Descriptions) = "Crystalised Fees To Book"

      '

      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.Header)(Col_Totals) = "Difference"
      Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.Header)(Col_Totals) = "Totals"

      Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.TodaysAdmin_Value, Col_Venice, 0.0#)
      Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.TodaysAdmin_Value, Col_Totals, 0.0#)
      Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.TodaysAdmin_Value, Col_Difference, 0.0#)

      Grid_FeesMultiClass.Cols(Col_Difference)(FeeGrid_RowIndex.ManagementFeesBooked) = 0.0#
      Grid_FeesMultiClass.Cols(Col_Difference)(FeeGrid_RowIndex.PerformanceFeesBooked) = 0.0#
      Grid_FeesMultiClass.Cols(Col_Difference)(FeeGrid_RowIndex.CrystalisedFeesBooked) = 0.0#

      Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesAdministrator, Col_Totals, 0.0#)

      ' Columns

      Dim InstrumentsDataset As RenaissanceDataClass.DSInstrument = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblInstrument), RenaissanceDataClass.DSInstrument)
      Dim InstrumentsTable As RenaissanceDataClass.DSInstrument.tblInstrumentDataTable = InstrumentsDataset.tblInstrument
      Dim FundUnitInstruments() As RenaissanceDataClass.DSInstrument.tblInstrumentRow

      If (FundID = 0) Then
        FundUnitInstruments = InstrumentsTable.Select("false", "InstrumentDescription")
      Else
        FundUnitInstruments = InstrumentsTable.Select("InstrumentFundID=" & FundID.ToString, "InstrumentDescription")
      End If

      Dim ColsToDelete As New ArrayList
      Dim NewCol As C1.Win.C1FlexGrid.Column

      ColCounter = 1 ' first Unit column in the grid

      For RowCounter = 0 To (FundUnitInstruments.Length - 1)
        While (CInt(Nz(Grid_FeesMultiClass.GetData(FeeGrid_RowIndex.InstrumentID, ColCounter), 0)) <> FundUnitInstruments(RowCounter).InstrumentID) And (ColCounter < Col_Totals)
          ColsToDelete.Add(Grid_FeesMultiClass.Cols(ColCounter))
          ColCounter += 1
        End While

        If (CInt(Nz(Grid_FeesMultiClass.GetData(FeeGrid_RowIndex.InstrumentID, ColCounter), 0)) <> FundUnitInstruments(RowCounter).InstrumentID) Then ' Insert Row
          NewCol = Grid_FeesMultiClass.Cols.Insert(ColCounter)

          NewCol.Width = 100
        Else
          NewCol = Grid_FeesMultiClass.Cols(ColCounter)
        End If

        NewCol(FeeGrid_RowIndex.InstrumentID) = FundUnitInstruments(RowCounter).InstrumentID
        NewCol(FeeGrid_RowIndex.Header) = FundUnitInstruments(RowCounter).InstrumentClass
        NewCol(FeeGrid_RowIndex.InstrumentClass) = FundUnitInstruments(RowCounter).InstrumentClass
        NewCol(FeeGrid_RowIndex.ISIN) = FundUnitInstruments(RowCounter).InstrumentISIN

        ' Formatting

        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.YesterdayAdmin_UnitCount, ColCounter, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.YesterdayAdmin_UnitCount))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.VeniceOpening_Units, ColCounter, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.VeniceOpening_Units))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.YesterdayAdmin_UnitPrice, ColCounter, GetFeeGridStyle("Numeric4", FeeGrid_RowIndex.YesterdayAdmin_UnitPrice))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.YesterdayAdmin_Value, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.YesterdayAdmin_Value))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.YesterdayAdmin_Percent, ColCounter, GetFeeGridStyle("Percentage", FeeGrid_RowIndex.YesterdayAdmin_Percent))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.Period_SubscriptionValue, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.Period_SubscriptionValue))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.Venice_UnitsSubscribed, ColCounter, GetFeeGridStyle("Numeric4", FeeGrid_RowIndex.Venice_UnitsSubscribed))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.Venice_UnitsRedeemed, ColCounter, GetFeeGridStyle("Numeric4", FeeGrid_RowIndex.Venice_UnitsRedeemed))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.Modified_AdminPercent, ColCounter, GetFeeGridStyle("Percentage", FeeGrid_RowIndex.Modified_AdminPercent))

        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.YesterdayAdmin_Currency, ColCounter, GetFeeGridStyle("TextRight", FeeGrid_RowIndex.YesterdayAdmin_Currency))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.YesterdayAdmin_CurrencyID, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.YesterdayAdmin_CurrencyID))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.YesterdayAdmin_FXRate, ColCounter, GetFeeGridStyle("Numeric6", FeeGrid_RowIndex.YesterdayAdmin_FXRate))

        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.TodaysAdmin_UnitCount, ColCounter, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.TodaysAdmin_UnitCount))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.TodaysAdmin_UnitPrice, ColCounter, GetFeeGridStyle("Numeric4", FeeGrid_RowIndex.TodaysAdmin_UnitPrice))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.TodaysAdmin_Value, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.TodaysAdmin_Value))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.TodaysAdmin_Percent, ColCounter, GetFeeGridStyle("Percentage", FeeGrid_RowIndex.TodaysAdmin_Percent))

        ''Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.Header, ColCounter, Style_TextMiddle)
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.InstrumentClass, ColCounter, GetFeeGridStyle("TextMiddle", FeeGrid_RowIndex.InstrumentClass)) 'Style_TextMiddle)
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ISIN, ColCounter, GetFeeGridStyle("TextRight", FeeGrid_RowIndex.ISIN))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.InstrumentClass, ColCounter, GetFeeGridStyle("TextRight", FeeGrid_RowIndex.InstrumentClass))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.FundID, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.FundID))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.InstrumentID, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.InstrumentID))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.General_Profit, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.General_Profit))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.Venice_BNAV, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.Venice_BNAV))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Profit, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.ClassSpecific_Profit))

        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_ManagementFeesRate, ColCounter, GetFeeGridStyle("Percentage", FeeGrid_RowIndex.ClassSpecific_ManagementFeesRate))

        If (CInt(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_ManagementFeesChanged), 0))) = 0 Then
          Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_ManagementFeesValue, ColCounter, GetFeeGridStyle("Numeric2Editable", FeeGrid_RowIndex.ClassSpecific_ManagementFeesValue))
        End If
        If (CInt(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesChanged), 0))) = 0 Then
          Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesValue, ColCounter, GetFeeGridStyle("Numeric2Editable", FeeGrid_RowIndex.ClassSpecific_PerformanceFeesValue))
        End If
        If (CInt(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesChanged), 0))) = 0 Then
          Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesValue, ColCounter, GetFeeGridStyle("Numeric2Editable", FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesValue))
        End If

        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Performance_FeeRate, ColCounter, GetFeeGridStyle("Percentage", FeeGrid_RowIndex.ClassSpecific_Performance_FeeRate))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Performance_BestBenchmark, ColCounter, GetFeeGridStyle("TextRight", FeeGrid_RowIndex.ClassSpecific_Performance_BestBenchmark)) ' Style_TextRight)
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Performance_BenchmarkPerformance, ColCounter, GetFeeGridStyle("Percentage", FeeGrid_RowIndex.ClassSpecific_Performance_BenchmarkPerformance))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Performance_BenchmarkValueIncrease, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.ClassSpecific_Performance_BenchmarkValueIncrease))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Performance_StartValue, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.ClassSpecific_Performance_StartValue))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Performance_EndValue, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.ClassSpecific_Performance_EndValue))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Performance_ClassPerformance, ColCounter, GetFeeGridStyle("Percentage", FeeGrid_RowIndex.ClassSpecific_Performance_ClassPerformance))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Performance_ClassValueIncrease, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.ClassSpecific_Performance_ClassValueIncrease))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Performance_ClassOutperformance, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.ClassSpecific_Performance_ClassOutperformance))

        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.VeniceTotals_Value, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.VeniceTotals_Value))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.VeniceTotals_Units, ColCounter, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.VeniceTotals_Units))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.VeniceTotals_Price, ColCounter, GetFeeGridStyle("Numeric4", FeeGrid_RowIndex.VeniceTotals_Price))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.CurrentFXRate, ColCounter, GetFeeGridStyle("Numeric6", FeeGrid_RowIndex.CurrentFXRate))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.Discrepancy, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.Discrepancy))

        ' 

        ColCounter += 1
      Next

      If (ColCounter < Col_Totals) Then
        While (ColCounter < Col_Totals)
          ColsToDelete.Add(Grid_FeesMultiClass.Cols(ColCounter))
          ColCounter += 1
        End While
      End If

      For Each NewCol In ColsToDelete
        Grid_FeesMultiClass.Cols.Remove(NewCol)
      Next

      Col_Difference = Grid_FeesMultiClass.Cols("col_Difference").Index

      For ColCounter = (Col_Descriptions + 1) To (Col_Difference)
        ' Formatting

        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.YesterdayAdmin_UnitCount, ColCounter, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.YesterdayAdmin_UnitCount))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.VeniceOpening_Units, ColCounter, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.VeniceOpening_Units))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.YesterdayAdmin_UnitPrice, ColCounter, GetFeeGridStyle("Numeric4", FeeGrid_RowIndex.YesterdayAdmin_UnitPrice))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.YesterdayAdmin_Value, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.YesterdayAdmin_Value))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.YesterdayAdmin_Percent, ColCounter, GetFeeGridStyle("Percentage", FeeGrid_RowIndex.YesterdayAdmin_Percent))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.Period_SubscriptionValue, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.Period_SubscriptionValue))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.Venice_UnitsSubscribed, ColCounter, GetFeeGridStyle("Numeric4", FeeGrid_RowIndex.Venice_UnitsSubscribed))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.Venice_UnitsRedeemed, ColCounter, GetFeeGridStyle("Numeric4", FeeGrid_RowIndex.Venice_UnitsRedeemed))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.Modified_AdminPercent, ColCounter, GetFeeGridStyle("Percentage", FeeGrid_RowIndex.Modified_AdminPercent))

        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.YesterdayAdmin_Currency, ColCounter, GetFeeGridStyle("TextRight", FeeGrid_RowIndex.YesterdayAdmin_Currency))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.YesterdayAdmin_CurrencyID, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.YesterdayAdmin_CurrencyID))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.YesterdayAdmin_FXRate, ColCounter, GetFeeGridStyle("Numeric6", FeeGrid_RowIndex.YesterdayAdmin_FXRate))

        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.TodaysAdmin_UnitCount, ColCounter, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.TodaysAdmin_UnitCount))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.TodaysAdmin_UnitPrice, ColCounter, GetFeeGridStyle("Numeric4", FeeGrid_RowIndex.TodaysAdmin_UnitPrice))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.TodaysAdmin_Value, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.TodaysAdmin_Value))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.TodaysAdmin_Percent, ColCounter, GetFeeGridStyle("Percentage", FeeGrid_RowIndex.TodaysAdmin_Percent))

        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.Header, ColCounter, Style_TextMiddle)
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.InstrumentClass, ColCounter, GetFeeGridStyle("TextMiddle", FeeGrid_RowIndex.InstrumentClass)) 'Style_TextMiddle)
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ISIN, ColCounter, GetFeeGridStyle("TextRight", FeeGrid_RowIndex.ISIN))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.InstrumentClass, ColCounter, GetFeeGridStyle("TextRight", FeeGrid_RowIndex.InstrumentClass))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.InstrumentID, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.InstrumentID))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.General_Profit, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.General_Profit))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.Venice_BNAV, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.Venice_BNAV))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Profit, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.ClassSpecific_Profit))

        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_ManagementFeesRate, ColCounter, GetFeeGridStyle("Percentage", FeeGrid_RowIndex.ClassSpecific_ManagementFeesRate))

        'If (ColCounter < Col_Totals) Then
        '  If (CInt(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_ManagementFeesChanged), 0))) = 0 Then
        '    Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_ManagementFees, ColCounter, GetFeeGridStyle("Numeric2Editable", FeeGrid_RowIndex.ClassSpecific_ManagementFees))
        '  End If
        '  If (CInt(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesChanged), 0))) = 0 Then
        '    Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_PerformanceFees, ColCounter, GetFeeGridStyle("Numeric2Editable", FeeGrid_RowIndex.ClassSpecific_PerformanceFees))
        '  End If
        '  If (CInt(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesChanged), 0))) = 0 Then
        '    Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_CrystalisedFees, ColCounter, GetFeeGridStyle("Numeric2Editable", FeeGrid_RowIndex.ClassSpecific_CrystalisedFees))
        '  End If
        'End If

        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_ManagementFeesAdministrator, ColCounter, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.ClassSpecific_ManagementFeesAdministrator))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesAdministrator, ColCounter, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.ClassSpecific_PerformanceFeesAdministrator))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesAdministrator, ColCounter, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesAdministrator))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_ManagementFeesVenice, ColCounter, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.ClassSpecific_ManagementFeesVenice))

        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Performance_FeeRate, ColCounter, GetFeeGridStyle("Percentage", FeeGrid_RowIndex.ClassSpecific_Performance_FeeRate))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Performance_BestBenchmark, ColCounter, GetFeeGridStyle("TextRight", FeeGrid_RowIndex.ClassSpecific_Performance_BestBenchmark)) ' Style_TextRight)
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Performance_BenchmarkPerformance, ColCounter, GetFeeGridStyle("Percentage", FeeGrid_RowIndex.ClassSpecific_Performance_BenchmarkPerformance))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Performance_BenchmarkValueIncrease, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.ClassSpecific_Performance_BenchmarkValueIncrease))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Performance_StartValue, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.ClassSpecific_Performance_StartValue))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Performance_EndValue, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.ClassSpecific_Performance_EndValue))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Performance_ClassPerformance, ColCounter, GetFeeGridStyle("Percentage", FeeGrid_RowIndex.ClassSpecific_Performance_ClassPerformance))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Performance_ClassValueIncrease, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.ClassSpecific_Performance_ClassValueIncrease))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Performance_ClassOutperformance, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.ClassSpecific_Performance_ClassOutperformance))

        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.VeniceTotals_Value, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.VeniceTotals_Value))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.VeniceTotals_Units, ColCounter, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.VeniceTotals_Units))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.VeniceTotals_Price, ColCounter, GetFeeGridStyle("Numeric4", FeeGrid_RowIndex.VeniceTotals_Price))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.CurrentFXRate, ColCounter, GetFeeGridStyle("Numeric6", FeeGrid_RowIndex.CurrentFXRate))
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.Discrepancy, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.Discrepancy))

      Next


    Catch ex As Exception

    End Try


  End Sub

  Private Sub PaintFeesByClassGrid(ByVal FundID As Integer, Optional ByVal UpdateAdminValues As Boolean = True, Optional ByVal UpdateVeniceValues As Boolean = True)

    ' ***************************************************************************
    ' 1) Get Administrator Values
    ' 2) Get details for each Share class
    ' 3) Calculate as necessary
    ' ***************************************************************************

    Dim FundRow As RenaissanceDataClass.DSFund.tblFundRow = Nothing
    Dim InstrumentRow As RenaissanceDataClass.DSInstrument.tblInstrumentRow = Nothing
		Dim ThisInstrumentID As Integer
		Dim thisSQLCommand As SqlCommand = Nothing
    Dim thisSQLConnection As SqlConnection = MainForm.GetVeniceConnection()
    Dim RetryCounter As Integer
    Dim Col_Descriptions As Integer
    Dim Col_Totals As Integer
    Dim Col_Difference As Integer
    Dim Col_Venice As Integer
    Dim ColCounter As Integer
    Dim RowCounter As Integer
    Dim ThisInstrumentClass As String
    Dim FXToFund As Double
		Dim DaysInFeeDateYear As Integer = (FitDateToPeriod(DealingPeriod.Annually, Date_ValueDate.Value.Date, True) - FitDateToPeriod(DealingPeriod.Annually, Date_ValueDate.Value.Date, False)).TotalDays + 1

    Try

      Col_Descriptions = Grid_FeesMultiClass.Cols("col_Descriptions").Index
      Col_Totals = Grid_FeesMultiClass.Cols("col_Totals").Index
      Col_Difference = Grid_FeesMultiClass.Cols("col_Difference").Index
      Col_Venice = Grid_FeesMultiClass.Cols("col_Venice").Index

      Try
        FundRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, FundID)

        If FundRow Is Nothing Then
          InitialiseFeesByClassGrid(0)
          Exit Sub
        End If

      Catch ex As Exception
      End Try

      Try
        Dim AdminShareClassDetails As New DataTable
        Dim FeeAmountTable As New DataTable

        ' 1) Get Administrator Values

        If (UpdateAdminValues) Then

          If (FundRow IsNot Nothing) Then

            ' Fee Start Date Values
            ' Get Data and populate
            thisSQLCommand = New SqlCommand
            thisSQLCommand.CommandType = CommandType.StoredProcedure
            thisSQLCommand.CommandText = "adp_tblRecValuation_SelectCommand"
            thisSQLCommand.Parameters.Add(New SqlParameter("@NavDate", SqlDbType.Date)).Value = Date_FeesSince.Value
            thisSQLCommand.Parameters.Add(New SqlParameter("@FundCode", SqlDbType.NVarChar, 50)).Value = FundRow.FundAdministratorCode
            thisSQLCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", SqlDbType.DateTime)).Value = MainForm.Main_Knowledgedate

            For RetryCounter = 0 To 1
              thisSQLCommand.Connection = thisSQLConnection

              Try

                AdminShareClassDetails.Load(thisSQLCommand.ExecuteReader)
                Exit For
              Catch ex As Exception

                Try
                  thisSQLConnection.Close()
                Catch Inner_ex As Exception
                End Try

                thisSQLConnection = MainForm.GetVeniceConnection()
                AdminShareClassDetails.Clear()
              End Try
            Next

            ' Get Data and populate
            thisSQLCommand = New SqlCommand
            thisSQLCommand.CommandType = CommandType.StoredProcedure
            thisSQLCommand.CommandText = "adp_tblRecFeesAmount_SelectCommand"
            thisSQLCommand.Parameters.Add(New SqlParameter("@NavDate", SqlDbType.Date)).Value = Date_ValueDate.Value
            thisSQLCommand.Parameters.Add(New SqlParameter("@FundCode", SqlDbType.NVarChar, 50)).Value = FundRow.FundAdministratorCode
            thisSQLCommand.Parameters.Add(New SqlParameter("@Class", SqlDbType.NVarChar, 50)).Value = ""
            thisSQLCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", SqlDbType.DateTime)).Value = MainForm.Main_Knowledgedate

            For RetryCounter = 0 To 1
              thisSQLCommand.Connection = thisSQLConnection

              Try

                FeeAmountTable.Load(thisSQLCommand.ExecuteReader)
                Exit For
              Catch ex As Exception

                Try
                  thisSQLConnection.Close()
                Catch Inner_ex As Exception
                End Try

                thisSQLConnection = MainForm.GetVeniceConnection()
                FeeAmountTable.Clear()
              End Try
            Next

          End If ' If (FundRow IsNot Nothing) Then

          ' Set Admin Values

          Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.ClassSpecific_ManagementFeesAdministrator, Col_Totals, 0.0#)
          Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesAdministrator, Col_Totals, 0.0#)
          Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesAdministrator, Col_Totals, 0.0#)
          Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.YesterdayAdmin_UnitCount, Col_Totals, 0.0#)


          Dim TotalValue As Double = 0.0#

          For ColCounter = (Col_Descriptions + 1) To (Col_Totals - 1)

            ThisInstrumentClass = CStr(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.InstrumentClass), "")).Trim.ToUpper

            ' Admin Unit Count / Price

            Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.YesterdayAdmin_UnitCount) = 0.0#
            Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.YesterdayAdmin_UnitPrice) = 0.0#
            Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.YesterdayAdmin_Percent) = 0.0#
            Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.Modified_AdminPercent) = 0.0#
            Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.YesterdayAdmin_Value) = 0.0#
            Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.YesterdayAdmin_Currency) = ""
            Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.YesterdayAdmin_CurrencyID) = 0.0#
            Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.YesterdayAdmin_FXRate) = 0.0#

            ' Set Basic FX Rate and Currency information from Venice.
            ' It is checked below vs the Administrator data, but if a Class did not exist yesterday then there is no Administrator record.

            ThisInstrumentID = CInt(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.InstrumentID), 0))
            InstrumentRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, ThisInstrumentID), DSInstrument.tblInstrumentRow)

            If (InstrumentRow IsNot Nothing) Then
              Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.YesterdayAdmin_Currency) = InstrumentRow.InstrumentCurrency
              Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.YesterdayAdmin_CurrencyID) = InstrumentRow.InstrumentCurrencyID
              Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.YesterdayAdmin_FXRate) = GetFXToFund(InstrumentRow.InstrumentCurrencyID, FundRow.FundBaseCurrency)
            End If

            ' Set Instrument details from (Yesterdays) Admin data.

            If (AdminShareClassDetails IsNot Nothing) AndAlso (AdminShareClassDetails.Rows.Count > 0) Then
              For RowCounter = 0 To (AdminShareClassDetails.Rows.Count - 1)
                If (CStr(Nz(AdminShareClassDetails.Rows(RowCounter)("ISINCode"), "")).ToUpper = CStr(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ISIN), "")).ToUpper) Then

                  Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.YesterdayAdmin_UnitCount) = CDbl(Nz(AdminShareClassDetails.Rows(RowCounter)("NumberOfShares_Value"), 0.0#))
                  Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.YesterdayAdmin_UnitCount, Col_Totals, CDbl(Grid_FeesMultiClass.GetData(FeeGrid_RowIndex.YesterdayAdmin_UnitCount, Col_Totals)) + CDbl(Nz(AdminShareClassDetails.Rows(RowCounter)("NumberOfShares_Value"), 0.0#)))
                  Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.YesterdayAdmin_UnitPrice) = CDbl(Nz(AdminShareClassDetails.Rows(RowCounter)("NAV_Value"), 0.0#))
                  Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.YesterdayAdmin_Currency) = CStr(Nz(AdminShareClassDetails.Rows(RowCounter)("Currency"), ""))
                  Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.YesterdayAdmin_CurrencyID) = CInt(Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblCurrency, 0, "CurrencyID", "RN", "CurrencyCode='" & CStr(Nz(AdminShareClassDetails.Rows(RowCounter)("Currency"), "")) & "'"), 0))

                  If CInt(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.YesterdayAdmin_CurrencyID), 0)) > 0 Then
                    ' This is not quite accurate. We really need the FX from the Knowledgedated Fee start date.
                    ' At this point, there are no Currency classes and this is really just a placeholder for better code later.
                    FXToFund = GetFXToFund(CInt(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.YesterdayAdmin_CurrencyID), 0)), FundRow.FundBaseCurrency)
                  Else
                    FXToFund = 1.0#
                  End If

                  Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.YesterdayAdmin_FXRate) = FXToFund

                  Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.YesterdayAdmin_Value) = CInt(FXToFund * CDbl(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.YesterdayAdmin_UnitCount)) * CDbl(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.YesterdayAdmin_UnitPrice)))

                  TotalValue += CDbl(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.YesterdayAdmin_Value))

                  Exit For
                End If
              Next RowCounter
            End If

            If (CDbl(Grid_FeesMultiClass.GetData(FeeGrid_RowIndex.YesterdayAdmin_UnitCount, Col_Totals)) < 0.0#) Then
              Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.YesterdayAdmin_UnitCount, Col_Totals, GetFeeGridStyle("Numeric2_Negative", FeeGrid_RowIndex.YesterdayAdmin_UnitCount)) 'Style_Numeric2_Negative)
            Else
              Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.YesterdayAdmin_UnitCount, Col_Totals, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.YesterdayAdmin_UnitCount)) 'Style_Numeric2)
            End If


            ' Set Admin Values for Fees
            ' Are these fees in Euros or Share class currency ?
            ' Does not matter yet, but it will one day!!!!!

            Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_ManagementFeesAdministrator) = 0.0#
            Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesAdministrator) = 0.0#
            Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesAdministrator) = 0.0#

            If (FeeAmountTable IsNot Nothing) AndAlso (FeeAmountTable.Rows.Count > 0) Then
              For RowCounter = 0 To (FeeAmountTable.Rows.Count - 1)
                If (CStr(Nz(FeeAmountTable.Rows(RowCounter)("Class"), "")).Trim.ToUpper = ThisInstrumentClass) Then

                  Select Case CStr(Nz(FeeAmountTable.Rows(RowCounter)("FeesType"), "")).Trim.ToUpper

                    Case "FRAIS DE GEST.ADMINISTRATIVE", "MANAGEMENT FEE", "GENERAL EXPENSES"

                      Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_ManagementFeesAdministrator) = CDbl(Nz(FeeAmountTable.Rows(RowCounter)("ProvisionDaily_Value"), 0)) + CDbl(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_ManagementFeesAdministrator), 0))
                      Grid_FeesMultiClass(FeeGrid_RowIndex.ClassSpecific_ManagementFeesAdministrator, Col_Totals) = CDbl(Nz(FeeAmountTable.Rows(RowCounter)("ProvisionDaily_Value"), 0)) + CDbl(Grid_FeesMultiClass(FeeGrid_RowIndex.ClassSpecific_ManagementFeesAdministrator, Col_Totals))

                    Case "FDG VARIABLES"

                      Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesAdministrator) = CDbl(Nz(FeeAmountTable.Rows(RowCounter)("ProvisionDaily_Value"), 0)) + CDbl(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesAdministrator), 0))
                      Grid_FeesMultiClass(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesAdministrator, Col_Totals) = CDbl(Nz(FeeAmountTable.Rows(RowCounter)("ProvisionDaily_Value"), 0)) + CDbl(Grid_FeesMultiClass(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesAdministrator, Col_Totals))

                    Case "FDG VARIABLES ACQUIS SDG", "FDG VARIABLES ACQUIS OPCVM"

                      Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesAdministrator) = CDbl(Nz(FeeAmountTable.Rows(RowCounter)("ProvisionDaily_Value"), 0)) + CDbl(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesAdministrator), 0))
                      Grid_FeesMultiClass(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesAdministrator, Col_Totals) = CDbl(Nz(FeeAmountTable.Rows(RowCounter)("ProvisionDaily_Value"), 0)) + CDbl(Grid_FeesMultiClass(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesAdministrator, Col_Totals))

                  End Select
                End If
              Next RowCounter
            End If

            Try
              If (CDbl(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_ManagementFeesAdministrator)) < 0) Then
                Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_ManagementFeesAdministrator, ColCounter, GetFeeGridStyle("Numeric2_Negative", FeeGrid_RowIndex.ClassSpecific_ManagementFeesAdministrator)) 'Style_Numeric2_Negative)
              Else
                Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_ManagementFeesAdministrator, ColCounter, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.ClassSpecific_ManagementFeesAdministrator)) 'Style_Numeric2)
              End If

              If (CDbl(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesAdministrator)) < 0) Then
                Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesAdministrator, ColCounter, GetFeeGridStyle("Numeric2_Negative", FeeGrid_RowIndex.ClassSpecific_PerformanceFeesAdministrator)) 'Style_Numeric2_Negative)
              Else
                Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesAdministrator, ColCounter, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.ClassSpecific_PerformanceFeesAdministrator)) 'Style_Numeric2)
              End If

              If (CDbl(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesAdministrator)) < 0) Then
                Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesAdministrator, ColCounter, GetFeeGridStyle("Numeric2_Negative", FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesAdministrator)) 'Style_Numeric2_Negative)
              Else
                Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesAdministrator, ColCounter, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesAdministrator)) 'Style_Numeric2)
              End If
            Catch ex As Exception
            End Try

          Next ColCounter

          ' Format Admin Fee totals.

          Try
            If (CDbl(Grid_FeesMultiClass.Cols(Col_Totals)(FeeGrid_RowIndex.ClassSpecific_ManagementFeesAdministrator)) < 0) Then
              Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_ManagementFeesAdministrator, Col_Totals, GetFeeGridStyle("Numeric2_Negative", FeeGrid_RowIndex.ClassSpecific_ManagementFeesAdministrator)) 'Style_Numeric2_Negative)
            Else
              Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_ManagementFeesAdministrator, Col_Totals, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.ClassSpecific_ManagementFeesAdministrator)) 'Style_Numeric2)
            End If

            If (CDbl(Grid_FeesMultiClass.Cols(Col_Totals)(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesAdministrator)) < 0) Then
              Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesAdministrator, Col_Totals, GetFeeGridStyle("Numeric2_Negative", FeeGrid_RowIndex.ClassSpecific_PerformanceFeesAdministrator)) 'Style_Numeric2_Negative)
            Else
              Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesAdministrator, Col_Totals, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.ClassSpecific_PerformanceFeesAdministrator)) 'Style_Numeric2)
            End If

            If (CDbl(Grid_FeesMultiClass.Cols(Col_Totals)(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesAdministrator)) < 0) Then
              Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesAdministrator, Col_Totals, GetFeeGridStyle("Numeric2_Negative", FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesAdministrator)) 'Style_Numeric2_Negative)
            Else
              Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesAdministrator, Col_Totals, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesAdministrator)) 'Style_Numeric2)
            End If
          Catch ex As Exception
          End Try

          '

          If (TotalValue > 0.0#) Then
            For ColCounter = (Col_Descriptions + 1) To (Col_Totals - 1)
              Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.YesterdayAdmin_Percent) = CDbl(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.YesterdayAdmin_Value)) / TotalValue
            Next
          End If

          ' Today's values

          thisSQLCommand = New SqlCommand
          thisSQLCommand.CommandType = CommandType.StoredProcedure
          thisSQLCommand.CommandText = "adp_tblRecValuation_SelectCommand"
          thisSQLCommand.Parameters.Add(New SqlParameter("@NavDate", SqlDbType.Date)).Value = Date_ValueDate.Value
          thisSQLCommand.Parameters.Add(New SqlParameter("@FundCode", SqlDbType.NVarChar, 50)).Value = CStr(Nz(FundRow.FundAdministratorCode, ""))
          thisSQLCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", SqlDbType.DateTime)).Value = MainForm.Main_Knowledgedate
          AdminShareClassDetails = New DataTable

          For RetryCounter = 0 To 1
            thisSQLCommand.Connection = thisSQLConnection

            Try

              AdminShareClassDetails.Load(thisSQLCommand.ExecuteReader)
              Exit For
            Catch ex As Exception

              Try
                thisSQLConnection.Close()
              Catch Inner_ex As Exception
              End Try

              thisSQLConnection = MainForm.GetVeniceConnection()
              AdminShareClassDetails.Clear()
            End Try
          Next

          TotalValue = 0.0#

          For ColCounter = (Col_Descriptions + 1) To (Col_Totals - 1)

            Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.TodaysAdmin_UnitCount) = 0.0# ' Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.YesterdayAdmin_UnitCount)
            Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.TodaysAdmin_UnitPrice) = 0.0# ' Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.YesterdayAdmin_UnitPrice)
            Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.TodaysAdmin_Percent) = 0.0# ' Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.YesterdayAdmin_Percent)
            Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.TodaysAdmin_Value) = 0.0# ' Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.YesterdayAdmin_Value)

            If (AdminShareClassDetails IsNot Nothing) AndAlso (AdminShareClassDetails.Rows.Count > 0) Then
              For RowCounter = 0 To (AdminShareClassDetails.Rows.Count - 1)
                If (CStr(Nz(AdminShareClassDetails.Rows(RowCounter)("ISINCode"), "")).ToUpper = CStr(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ISIN), "")).ToUpper) Then

                  Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.TodaysAdmin_UnitCount) = CDbl(Nz(AdminShareClassDetails.Rows(RowCounter)("NumberOfShares_Value"), 0.0#))
                  Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.TodaysAdmin_UnitPrice) = CDbl(Nz(AdminShareClassDetails.Rows(RowCounter)("NAV_Value"), 0.0#))

                  If CInt(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.YesterdayAdmin_CurrencyID), 0)) > 0 Then
                    ' This is not quite accurate. We really need the FX from the Fee end date.
                    ' At this point, there are no Currency classes and this is relly just a placeholder for better code later.
                    FXToFund = GetFXToFund(CInt(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.YesterdayAdmin_CurrencyID), 0)), FundRow.FundBaseCurrency)
                  Else
                    FXToFund = 1.0#
                  End If

                  Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.TodaysAdmin_Value) = CInt(FXToFund * CDbl(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.TodaysAdmin_UnitCount)) * CDbl(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.TodaysAdmin_UnitPrice)))

                  Exit For
                End If
              Next
            End If

            TotalValue += CDbl(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.TodaysAdmin_Value))

          Next

          If (TotalValue > 0.0#) Then
            For ColCounter = (Col_Descriptions + 1) To (Col_Totals - 1)
              Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.TodaysAdmin_Percent) = CDbl(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.TodaysAdmin_Value)) / TotalValue
            Next
          End If

          Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.TodaysAdmin_Value, Col_Totals, TotalValue)
          If (TotalValue < 0.0) Then
            Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.TodaysAdmin_Value, Col_Totals, GetFeeGridStyle("Integer_Negative", FeeGrid_RowIndex.TodaysAdmin_Value)) ' Style_Integer_Negative)
          Else
            Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.TodaysAdmin_Value, Col_Totals, GetFeeGridStyle("Integer", FeeGrid_RowIndex.TodaysAdmin_Value))
          End If

        End If ' If (UpdateAdminValues) Then

      Catch ex As Exception
        MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in MultiClass Fees grid, Setting Administrator Values.", ex.StackTrace, True)
      Finally

      End Try

      Dim BookedManagementFees As New Dictionary(Of Integer, Double)
      Dim BookedPerformanceFees As New Dictionary(Of Integer, Double)
      Dim BookedCrystalisedFees As New Dictionary(Of Integer, Double)

      Dim BookedManagementFeesYTD As New Dictionary(Of Integer, Double)
      Dim BookedPerformanceFeesYTD As New Dictionary(Of Integer, Double)
      Dim BookedCrystalisedFeesYTD As New Dictionary(Of Integer, Double)


      Dim FundValuation As FundFeeCalculator.FundValuationDetails = Nothing
      Try
        ' 2) Get details for each Share class

        If (UpdateVeniceValues) Then

          ' Stage 1, Insert Venice totals

          Try
            Dim SubscritptionsToFeeStartDate As Dictionary(Of Integer, SR_InformationClass) = Nothing
            Dim SubscritptionsToValueDate As Dictionary(Of Integer, SR_InformationClass) = Nothing
            Dim thisSubscritptionsToFeeStartDate As SR_InformationClass
            Dim thisSubscritptionsToValueDate As SR_InformationClass

            Dim FeeDate As Date
            Dim StatusGroupFilter As String
            Dim AdministratorDatesFilter As RenaissanceGlobals.AdministratorDatesFilter = RenaissanceGlobals.AdministratorDatesFilter.None

            FeeDate = Date_ValueDate.Value
            StatusGroupFilter = Nz(MainForm.GetComboSelectedValue(Combo_TransactionGroup), "")

            ' AdministratorDatesFilter (bitmap)
            '
            '--	1		- Ignore the transaction status for Subscriptions and Redemptions
            '--	2		- Ignore the Subscriptions and Redemptions on (or after of course) the given Value Date

            If (MainForm.GetCheckBoxChecked(Me.Check_SR_IgnoreStatus)) Then
              AdministratorDatesFilter += RenaissanceGlobals.AdministratorDatesFilter.SubscriptionRedemption_IgnoreTransactionStatus
            End If
            If (MainForm.GetCheckBoxChecked(Me.Check_SR_IgnoreTodaysTrades)) Then
              AdministratorDatesFilter += RenaissanceGlobals.AdministratorDatesFilter.SubscriptionRedemption_IgnoreTodaysTrades
            End If

            ' 

            For ColCounter = (Col_Descriptions + 1) To (Col_Totals - 1)
              Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.VeniceOpening_Units) = 0.0#
            Next

            Try
              MainForm.SetToolStripText(StatusLabel, "Calculating Fund Valuation.")

              FundValuation = GetFeesValuation(FeesObject, FundID, FeeDate, Date_FeesSince.Value, StatusGroupFilter, AdministratorDatesFilter, Check_AdministratorPriceDates.Checked)

              SubscritptionsToFeeStartDate = FeesObject.SubscriptionsToDate(Date_FeesSince.Value, Not Check_SR_IgnoreTodaysTrades.Checked)
              SubscritptionsToValueDate = FeesObject.SubscriptionsToDate(FeeDate, Not Check_SR_IgnoreTodaysTrades.Checked)

            Catch ex As Exception
              MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error getting Valuation or Fee Calculations.", ex.StackTrace, True)
            Finally
              MainForm.SetToolStripText(StatusLabel, "")
            End Try

            ' Units : Subscriptions / Redemptions

            Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.VeniceOpening_Units, Col_Totals, 0.0#)
            Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.YesterdayAdmin_UnitCount, Col_Venice, 0.0#)


            For ColCounter = (Col_Descriptions + 1) To (Col_Totals - 1)
              ThisInstrumentID = CInt(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.InstrumentID), 0))
              thisSubscritptionsToFeeStartDate = Nothing
              thisSubscritptionsToValueDate = Nothing

              If (SubscritptionsToFeeStartDate IsNot Nothing) Then SubscritptionsToFeeStartDate.TryGetValue(ThisInstrumentID, thisSubscritptionsToFeeStartDate)
              If (SubscritptionsToValueDate IsNot Nothing) Then SubscritptionsToValueDate.TryGetValue(ThisInstrumentID, thisSubscritptionsToValueDate)

              If (thisSubscritptionsToFeeStartDate IsNot Nothing) Then
                Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.VeniceOpening_Units, ColCounter, thisSubscritptionsToFeeStartDate.UnitsSubscribed - thisSubscritptionsToFeeStartDate.UnitsRedeemed)
                Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.VeniceOpening_Units, Col_Totals, CDbl(Grid_FeesMultiClass.GetData(FeeGrid_RowIndex.VeniceOpening_Units, Col_Totals)) + (thisSubscritptionsToFeeStartDate.UnitsSubscribed - thisSubscritptionsToFeeStartDate.UnitsRedeemed))
              End If

              If (thisSubscritptionsToValueDate IsNot Nothing) Then
                Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.VeniceTotals_Units, ColCounter, thisSubscritptionsToValueDate.UnitsSubscribed - thisSubscritptionsToValueDate.UnitsRedeemed)
              End If

              If (thisSubscritptionsToFeeStartDate IsNot Nothing) AndAlso (thisSubscritptionsToValueDate IsNot Nothing) Then
                FXToFund = CDbl(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.YesterdayAdmin_FXRate))

                Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.Period_SubscriptionValue, ColCounter, ((thisSubscritptionsToValueDate.ValueSubscribed - thisSubscritptionsToValueDate.ValueRedeemed) - (thisSubscritptionsToFeeStartDate.ValueSubscribed - thisSubscritptionsToFeeStartDate.ValueRedeemed)) * IIf(FXToFund = 0.0#, 1.0#, FXToFund))
                Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.Venice_UnitsSubscribed, ColCounter, ((thisSubscritptionsToValueDate.UnitsSubscribed) - (thisSubscritptionsToFeeStartDate.UnitsSubscribed)))
                Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.Venice_UnitsRedeemed, ColCounter, ((thisSubscritptionsToValueDate.UnitsRedeemed) - (thisSubscritptionsToFeeStartDate.UnitsRedeemed)))
              Else
                Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.Period_SubscriptionValue, ColCounter, 0.0#)
                Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.Venice_UnitsSubscribed, ColCounter, 0.0#)
                Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.Venice_UnitsRedeemed, ColCounter, 0.0#)
              End If
            Next

            Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.YesterdayAdmin_UnitCount, Col_Venice, CDbl(Grid_FeesMultiClass.GetData(FeeGrid_RowIndex.VeniceOpening_Units, Col_Totals)))
            If (CDbl(Grid_FeesMultiClass.GetData(FeeGrid_RowIndex.VeniceOpening_Units, Col_Totals)) < 0.0#) Then
              Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.VeniceOpening_Units, Col_Totals, GetFeeGridStyle("Numeric2_Negative", FeeGrid_RowIndex.VeniceOpening_Units)) 'Style_Numeric2_Negative)
              Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.YesterdayAdmin_UnitCount, Col_Venice, GetFeeGridStyle("Numeric2_Negative", FeeGrid_RowIndex.YesterdayAdmin_UnitCount)) 'Style_Numeric2_Negative)
            Else
              Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.VeniceOpening_Units, Col_Totals, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.VeniceOpening_Units)) 'Style_Numeric2)
              Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.YesterdayAdmin_UnitCount, Col_Venice, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.YesterdayAdmin_UnitCount)) 'Style_Numeric2)
            End If

            ' Share specific P&L

            Dim SelectedPnLRows() As DataRow
            Dim SpecificPnLRow As DataRow
            Dim TotalSpecifiPnL As Double
            Dim FundTotalSpecifiPnL As Double = 0.0#

            If (ClassSpecificPnLTable Is Nothing) Then
              LoadClassSpecificPnL()
            End If

            For ColCounter = (Col_Descriptions + 1) To (Col_Totals - 1)
              TotalSpecifiPnL = 0.0#

              If (ClassSpecificPnLTable IsNot Nothing) Then
                ThisInstrumentID = CInt(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.InstrumentID), 0))

                SelectedPnLRows = ClassSpecificPnLTable.Select("(Fund=" & FundID.ToString & ") AND (SpecificToFundUnitID=" & ThisInstrumentID.ToString & ")")

                For Each SpecificPnLRow In SelectedPnLRows
                  TotalSpecifiPnL += CDbl(Nz(SpecificPnLRow("Profit"), 0.0#))
                Next

                Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.ClassSpecific_Profit, ColCounter, TotalSpecifiPnL)
                FundTotalSpecifiPnL += TotalSpecifiPnL

                If (TotalSpecifiPnL < 0.0#) Then
                  Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Profit, ColCounter, GetFeeGridStyle("Numeric2_Negative", FeeGrid_RowIndex.ClassSpecific_Profit)) 'Style_Numeric2_Negative)
                Else
                  Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Profit, ColCounter, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.ClassSpecific_Profit)) 'Style_Numeric2)
                End If

              Else
                Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.ClassSpecific_Profit, ColCounter, 0.0#)
                Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Profit, ColCounter, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.ClassSpecific_Profit)) 'Style_Numeric2)
              End If
            Next

            Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.ClassSpecific_Profit, Col_Totals, FundTotalSpecifiPnL)
            If (FundTotalSpecifiPnL < 0.0#) Then
              Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Profit, Col_Totals, GetFeeGridStyle("Numeric2_Negative", FeeGrid_RowIndex.ClassSpecific_Profit)) 'Style_Numeric2_Negative)
            Else
              Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_Profit, Col_Totals, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.ClassSpecific_Profit)) 'Style_Numeric2)
            End If

            ' General P&L
            ' OK, FundValuation BNAV = Nav before Mgmt, Perf fees.
            ' Gross Profit = BNAV - (Starting NAV + Subscriptions)

            Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.Venice_BNAV, Col_Totals, FundValuation.FundBNAV)
            Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.Venice_BNAV, Col_Totals, GetFeeGridStyle("Integer", FeeGrid_RowIndex.Venice_BNAV)) 'Style_Integer)

            ' Booked Fees

            Try

              Dim ThisFee As Double
              Dim UnallocatedManageFees As Double = 0.0#
              Dim UnallocatedPerformanceFees As Double = 0.0#
              Dim UnallocatedCrystalisedFees As Double = 0.0#
              Dim TotalBookedManageFees As Double = 0.0#
              Dim TotalBookedPerformanceFees As Double = 0.0#
              Dim TotalBookedCrystalisedFees As Double = 0.0#
              Dim OnlyOneShareClass As Boolean = False

              Call GetBookedFees(FundID, Date_FeesSince.Value.Date, Date_ValueDate.Value.Date, StatusGroupFilter, BookedManagementFees, BookedPerformanceFees, BookedCrystalisedFees)

              'azerty
              'calculate bookedFeesYTD
              If FundRow IsNot Nothing Then
                ' Calculate Dates.

                Dim MgmtFeePaymentPeriod As DealingPeriod = CType(FundRow.FundManagementFeesPaymentPeriod, DealingPeriod)
                Dim PerfFeePaymentPeriod As DealingPeriod = CType(FundRow.FundPerformanceFeesPaymentPeriod, DealingPeriod)
                Dim FundYearEnd As Date

                ' Clean Fund Year End 

                If FundRow.IsFundYearEndNull Then
                  FundYearEnd = New Date(Now.Year, 12, 31) ' Default to calendar year
                Else
                  FundYearEnd = FitDateToPeriod(DealingPeriod.Monthly, New Date(FeeDate.Year, FundRow.FundYearEnd.Month, 28), True)
                  If (FundYearEnd < FeeDate) Then
                    FundYearEnd = FundYearEnd.AddYears(1)
                  End If
                End If

                ' Performance Dates

                If (PerfFeePaymentPeriod = DealingPeriod.Annually) Then
                  ' Get Year End
                  Date_PerfFeesSettlementDate.Value = FundYearEnd
                Else
                  Date_PerfFeesSettlementDate.Value = RenaissanceUtilities.DatePeriodFunctions.FitDateToPeriod(PerfFeePaymentPeriod, FeeDate, True)
                End If

                'set the since year as the same of the value date year
                Dim Date_FeesSinceYTD As Date = FundYearEnd
                Date_FeesSinceYTD = Date_FeesSinceYTD.AddYears(Year(FeeDate) - Year(Date_FeesSinceYTD))

                ' if the fundYearEnd is not reached then the laat FundYearEnd is the one of the last year
                If FeeDate < Date_FeesSinceYTD Then
                  Date_FeesSinceYTD = Date_FeesSinceYTD.AddYears(-1)
                End If

                Call GetBookedFees(FundID, Date_FeesSinceYTD, Date_ValueDate.Value.Date, StatusGroupFilter, BookedManagementFeesYTD, BookedPerformanceFeesYTD, BookedCrystalisedFeesYTD)

              End If


              If (BookedManagementFees.ContainsKey(0)) Then
                UnallocatedManageFees = BookedManagementFees(0)
              End If
              If (BookedPerformanceFees.ContainsKey(0)) Then
                UnallocatedPerformanceFees = BookedManagementFees(0)
              End If
              If (BookedCrystalisedFees.ContainsKey(0)) Then
                UnallocatedCrystalisedFees = BookedManagementFees(0)
              End If

              TotalBookedManageFees = UnallocatedManageFees
              TotalBookedPerformanceFees = UnallocatedPerformanceFees
              TotalBookedCrystalisedFees = UnallocatedCrystalisedFees

              If ((Col_Totals - 1) - (Col_Descriptions + 1)) = 0 Then
                OnlyOneShareClass = True
              End If

              For ColCounter = (Col_Descriptions + 1) To (Col_Totals - 1)

                ThisInstrumentID = CInt(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.InstrumentID), 0))
                ThisFee = 0.0#

                If CInt(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.YesterdayAdmin_CurrencyID), 0)) > 0 Then
                  ' This is not quite accurate. We really need the FX from the live Fee End date.
                  ' At this point, there are no Currency classes and this is relly just a placeholder for better code later.
                  FXToFund = GetFXToFund(CInt(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.YesterdayAdmin_CurrencyID), 0)), FundRow.FundBaseCurrency)
                Else
                  FXToFund = 1.0#
                End If
                Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.CurrentFXRate) = FXToFund

                '' Backfill 
                'If CDbl(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.YesterdayAdmin_FXRate), 0.0#)) = 0.0# Then
                '  Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.YesterdayAdmin_FXRate) = FXToFund
                'End If

                If (BookedManagementFees.TryGetValue(ThisInstrumentID, ThisFee)) Then
                  Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ManagementFeesBooked) = ThisFee + IIf(OnlyOneShareClass, UnallocatedManageFees, 0.0#)
                  TotalBookedManageFees += ThisFee * FXToFund
                Else
                  Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ManagementFeesBooked) = IIf(OnlyOneShareClass, TotalBookedManageFees, 0.0#)
                End If

                If (BookedPerformanceFees.TryGetValue(ThisInstrumentID, ThisFee)) Then
                  Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.PerformanceFeesBooked) = ThisFee + IIf(OnlyOneShareClass, TotalBookedPerformanceFees, 0.0#)
                  TotalBookedPerformanceFees += ThisFee * FXToFund
                Else
                  Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.PerformanceFeesBooked) = IIf(OnlyOneShareClass, TotalBookedPerformanceFees, 0.0#)
                End If

                If (BookedCrystalisedFees.TryGetValue(ThisInstrumentID, ThisFee)) Then
                  Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.CrystalisedFeesBooked) = ThisFee + IIf(OnlyOneShareClass, TotalBookedCrystalisedFees, 0.0#)
                  TotalBookedCrystalisedFees += ThisFee * FXToFund
                Else
                  Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.CrystalisedFeesBooked) = IIf(OnlyOneShareClass, TotalBookedCrystalisedFees, 0.0#)
                End If

                If CDbl(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ManagementFeesBooked)) < 0.0# Then
                  Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ManagementFeesBooked, ColCounter, GetFeeGridStyle("Integer_Negative", FeeGrid_RowIndex.ManagementFeesBooked)) 'Style_Integer_Negative)
                Else
                  Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ManagementFeesBooked, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.ManagementFeesBooked)) 'Style_Integer)
                End If

                If CDbl(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.PerformanceFeesBooked)) < 0.0# Then
                  Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.PerformanceFeesBooked, ColCounter, GetFeeGridStyle("Integer_Negative", FeeGrid_RowIndex.PerformanceFeesBooked)) 'Style_Integer_Negative)
                Else
                  Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.PerformanceFeesBooked, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.PerformanceFeesBooked)) 'Style_Integer)
                End If

                If CDbl(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.CrystalisedFeesBooked)) < 0.0# Then
                  Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.CrystalisedFeesBooked, ColCounter, GetFeeGridStyle("Integer_Negative", FeeGrid_RowIndex.CrystalisedFeesBooked)) 'Style_Integer_Negative)
                Else
                  Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.CrystalisedFeesBooked, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.CrystalisedFeesBooked)) 'Style_Integer)
                End If

              Next ColCounter

              Grid_FeesMultiClass.Cols(Col_Venice)(FeeGrid_RowIndex.ManagementFeesBooked) = TotalBookedManageFees
              Grid_FeesMultiClass.Cols(Col_Venice)(FeeGrid_RowIndex.PerformanceFeesBooked) = TotalBookedPerformanceFees
              Grid_FeesMultiClass.Cols(Col_Venice)(FeeGrid_RowIndex.CrystalisedFeesBooked) = TotalBookedCrystalisedFees

              Grid_FeesMultiClass.Cols(Col_Difference)(FeeGrid_RowIndex.ManagementFeesBooked) = UnallocatedManageFees
              Grid_FeesMultiClass.Cols(Col_Difference)(FeeGrid_RowIndex.PerformanceFeesBooked) = UnallocatedPerformanceFees
              Grid_FeesMultiClass.Cols(Col_Difference)(FeeGrid_RowIndex.CrystalisedFeesBooked) = UnallocatedCrystalisedFees

              If CDbl(Grid_FeesMultiClass.Cols(Col_Venice)(FeeGrid_RowIndex.ManagementFeesBooked)) < 0.0# Then
                Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ManagementFeesBooked, Col_Venice, GetFeeGridStyle("Integer_Negative", FeeGrid_RowIndex.ManagementFeesBooked)) 'Style_Integer_Negative)
              Else
                Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ManagementFeesBooked, Col_Venice, GetFeeGridStyle("Integer", FeeGrid_RowIndex.ManagementFeesBooked)) 'Style_Integer)
              End If

              If CDbl(Grid_FeesMultiClass.Cols(Col_Venice)(FeeGrid_RowIndex.PerformanceFeesBooked)) < 0.0# Then
                Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.PerformanceFeesBooked, Col_Venice, GetFeeGridStyle("Integer_Negative", FeeGrid_RowIndex.PerformanceFeesBooked)) 'Style_Integer_Negative)
              Else
                Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.PerformanceFeesBooked, Col_Venice, GetFeeGridStyle("Integer", FeeGrid_RowIndex.PerformanceFeesBooked)) 'Style_Integer)
              End If

              If CDbl(Grid_FeesMultiClass.Cols(Col_Venice)(FeeGrid_RowIndex.CrystalisedFeesBooked)) < 0.0# Then
                Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.CrystalisedFeesBooked, Col_Venice, GetFeeGridStyle("Integer_Negative", FeeGrid_RowIndex.CrystalisedFeesBooked)) 'Style_Integer_Negative)
              Else
                Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.CrystalisedFeesBooked, Col_Venice, GetFeeGridStyle("Integer", FeeGrid_RowIndex.CrystalisedFeesBooked)) 'Style_Integer)
              End If

            Catch ex As Exception
            End Try

          Catch ex As Exception
          End Try

        End If

      Catch ex As Exception

      End Try


      Try
        ' 3) Calculate as necessary

        ' Modified Weighting

        Dim AdminValue As Double
				Dim AdminStartingValueTotalBeforeSubscriptions As Double = 0.0#
				Dim AdminStartingValueTotalAfterSubscriptions As Double = 0.0#
				Dim PercentageWeighting As Double
				Dim PercentageTotal As Double = 0.0#

				For ColCounter = (Col_Descriptions + 1) To (Col_Totals - 1)

					AdminValue = CDbl(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.YesterdayAdmin_Value))

					AdminStartingValueTotalBeforeSubscriptions += AdminValue
					AdminStartingValueTotalAfterSubscriptions += AdminValue + CDbl(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.Period_SubscriptionValue))

				Next

				Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.YesterdayAdmin_Value, Col_Totals, AdminStartingValueTotalBeforeSubscriptions)
				If (AdminStartingValueTotalBeforeSubscriptions >= 0.0#) Then
          Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.YesterdayAdmin_Value, Col_Totals, GetFeeGridStyle("Integer", FeeGrid_RowIndex.YesterdayAdmin_Value))
				Else
          Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.YesterdayAdmin_Value, Col_Totals, GetFeeGridStyle("Integer_Negative", FeeGrid_RowIndex.YesterdayAdmin_Value)) 'Style_Integer_Negative)
				End If

				If (AdminStartingValueTotalAfterSubscriptions > 0.0#) Then

					For ColCounter = (Col_Descriptions + 1) To (Col_Totals - 1)

						AdminValue = CDbl(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.YesterdayAdmin_Value))

						PercentageWeighting = (AdminValue + CDbl(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.Period_SubscriptionValue))) / AdminStartingValueTotalAfterSubscriptions
						PercentageTotal += PercentageWeighting

						Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.Modified_AdminPercent) = PercentageWeighting

						If (PercentageWeighting < 0.0#) Then
              Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.Modified_AdminPercent, ColCounter, GetFeeGridStyle("Percentage_Negative", FeeGrid_RowIndex.Modified_AdminPercent)) 'Style_Percentage_Negative)
						Else
              Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.Modified_AdminPercent, ColCounter, GetFeeGridStyle("Percentage", FeeGrid_RowIndex.Modified_AdminPercent)) 'Style_Percentage)
						End If
					Next

					Grid_FeesMultiClass.Cols(Col_Totals)(FeeGrid_RowIndex.Modified_AdminPercent) = PercentageTotal

					If (PercentageTotal < 0.0#) Then
            Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.Modified_AdminPercent, Col_Totals, GetFeeGridStyle("Percentage_Negative", FeeGrid_RowIndex.Modified_AdminPercent)) 'Style_Percentage_Negative)
					Else
            Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.Modified_AdminPercent, Col_Totals, GetFeeGridStyle("Percentage", FeeGrid_RowIndex.Modified_AdminPercent)) 'Style_Percentage)
					End If

				End If

				' Gross / General / Class Profit -> Class BNAV

				Dim FundOpeningNAV As Double = Nz(Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.YesterdayAdmin_Value)(Col_Totals), 0.0#)
				Dim ClassOpeningNAV As Double
        Dim ClassOpeningNAVAfterSubscriptions As Double
        Dim FundBNAV As Double = Nz(Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.Venice_BNAV)(Col_Totals), 0.0#)
				Dim ClassBNAV As Double
				Dim GrossProfit As Double = 0.0#
				Dim GeneralProfit As Double = 0.0#
				Dim ClassProfit As Double = Nz(Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.ClassSpecific_Profit)(Col_Totals), 0.0#)
				Dim ClassManagementFees As Double = 0.0#

        Dim ClassPerformanceFees As Double = 0.0#

        GrossProfit = FundBNAV - AdminStartingValueTotalAfterSubscriptions
				GeneralProfit = GrossProfit - ClassProfit

				Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.General_Profit, Col_Totals, GeneralProfit)
				If (GeneralProfit >= 0.0#) Then
          Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.General_Profit, Col_Totals, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.General_Profit)) 'Style_Numeric2)
				Else
          Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.General_Profit, Col_Totals, GetFeeGridStyle("Numeric2_Negative", FeeGrid_RowIndex.General_Profit)) 'Style_Numeric2_Negative)
				End If

        Grid_FeesMultiClass(FeeGrid_RowIndex.ClassSpecific_ManagementFeesVenice, Col_Totals) = 0.0#

        'azerty
        Grid_FeesMultiClass(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesVenice, Col_Totals) = 0.0#
        Grid_FeesMultiClass(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesVenice, Col_Totals) = 0.0#
        Grid_FeesMultiClass(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesAdministrator, Col_Totals) = 0.0#
        Grid_FeesMultiClass(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesAdministrator, Col_Totals) = 0.0#


        For ColCounter = (Col_Descriptions + 1) To (Col_Totals - 1)
          PercentageWeighting = CDbl(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.Modified_AdminPercent), 0.0#))
          ClassProfit = CDbl(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_Profit), 0.0#))

          Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.General_Profit, ColCounter, GeneralProfit * PercentageWeighting)
          If (GeneralProfit * PercentageWeighting) >= 0.0# Then
            Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.General_Profit, ColCounter, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.General_Profit)) 'Style_Numeric2)
          Else
            Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.General_Profit, ColCounter, GetFeeGridStyle("Numeric2_Negative", FeeGrid_RowIndex.General_Profit)) 'Style_Numeric2_Negative)
          End If

          ClassOpeningNAV = CDbl(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.YesterdayAdmin_Value))
          ClassOpeningNAVAfterSubscriptions = ClassOpeningNAV + CDbl(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.Period_SubscriptionValue))
          ClassBNAV = ClassOpeningNAVAfterSubscriptions + ClassProfit + (GeneralProfit * PercentageWeighting)

          Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.Venice_BNAV, ColCounter, ClassBNAV)
          If (ClassBNAV) >= 0.0# Then
            Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.Venice_BNAV, ColCounter, GetFeeGridStyle("Integer", FeeGrid_RowIndex.Venice_BNAV))
          Else
            Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.Venice_BNAV, ColCounter, GetFeeGridStyle("Integer_Negative", FeeGrid_RowIndex.Venice_BNAV))
          End If

          ' Mgmt Fees

          ThisInstrumentID = CInt(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.InstrumentID), 0))
          InstrumentRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, ThisInstrumentID), DSInstrument.tblInstrumentRow)

          If (InstrumentRow IsNot Nothing) Then

            ' Mngmnt Fees
            If (FundRow.FundManagementFeesBeforeSubscriptions) Then
              ClassManagementFees = ClassOpeningNAV * InstrumentRow.InstrumentUnitManagementFees * CDbl((Date_ValueDate.Value.Date - Date_FeesSince.Value.Date).Days) / CDbl(DaysInFeeDateYear) ' 
            Else
              ClassManagementFees = ClassOpeningNAVAfterSubscriptions * InstrumentRow.InstrumentUnitManagementFees * CDbl((Date_ValueDate.Value.Date - Date_FeesSince.Value.Date).Days) / CDbl(DaysInFeeDateYear) ' 
            End If
            Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_ManagementFeesVenice) = ClassManagementFees
            Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_ManagementFeesRate) = InstrumentRow.InstrumentUnitManagementFees

            If (ClassManagementFees < 0.0#) Then
              Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_ManagementFeesVenice, ColCounter, GetFeeGridStyle("Numeric2_Negative", FeeGrid_RowIndex.ClassSpecific_ManagementFeesVenice)) 'Style_Numeric2_Negative)
            Else
              Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_ManagementFeesVenice, ColCounter, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.ClassSpecific_ManagementFeesVenice)) 'Style_Numeric2)
            End If

            Grid_FeesMultiClass(FeeGrid_RowIndex.ClassSpecific_ManagementFeesVenice, Col_Totals) = ClassManagementFees + CDbl(Grid_FeesMultiClass(FeeGrid_RowIndex.ClassSpecific_ManagementFeesVenice, Col_Totals))

            Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_Performance_FeeRate) = InstrumentRow.InstrumentUnitPerformanceFees

            'azerty

            ' Perf Fees Venice YTD
            If (BookedPerformanceFeesYTD Is Nothing OrElse Not BookedPerformanceFeesYTD.TryGetValue(ThisInstrumentID, ClassPerformanceFees)) Then
              ClassPerformanceFees = 0.0#
            End If

            Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesVenice) = ClassPerformanceFees

            If (ClassPerformanceFees < 0.0#) Then
              Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesVenice, ColCounter, GetFeeGridStyle("Numeric2_Negative", FeeGrid_RowIndex.ClassSpecific_PerformanceFeesVenice)) 'Style_Numeric2_Negative)
            Else
              Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesVenice, ColCounter, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.ClassSpecific_PerformanceFeesVenice)) 'Style_Numeric2)
            End If

            Grid_FeesMultiClass(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesVenice, Col_Totals) = ClassPerformanceFees + CDbl(Grid_FeesMultiClass(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesVenice, Col_Totals))

            ' Perf Fees crystelized Venice YTD
            If (BookedCrystalisedFeesYTD Is Nothing OrElse Not BookedCrystalisedFeesYTD.TryGetValue(ThisInstrumentID, ClassPerformanceFees)) Then
              ClassPerformanceFees = 0.0#
            End If

            Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesVenice) = ClassPerformanceFees

            If (ClassPerformanceFees < 0.0#) Then
              Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesVenice, ColCounter, GetFeeGridStyle("Numeric2_Negative", FeeGrid_RowIndex.ClassSpecific_PerformanceFeesVenice)) 'Style_Numeric2_Negative)
            Else
              Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesVenice, ColCounter, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.ClassSpecific_PerformanceFeesVenice)) 'Style_Numeric2)
            End If

            Grid_FeesMultiClass(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesVenice, Col_Totals) = ClassPerformanceFees + CDbl(Grid_FeesMultiClass(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesVenice, Col_Totals))

            'lotfi : Add PriceArrayReturns the admin fees

          Else
            Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_ManagementFeesVenice) = 0.0#
            Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_ManagementFeesRate) = 0.0#

            Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesVenice) = 0.0#
            Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_Performance_FeeRate) = 0.0#
          End If


        Next

        If (CDbl(Grid_FeesMultiClass(FeeGrid_RowIndex.ClassSpecific_ManagementFeesVenice, Col_Totals)) < 0.0#) Then
          Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_ManagementFeesVenice, Col_Totals, GetFeeGridStyle("Numeric2_Negative", FeeGrid_RowIndex.ClassSpecific_ManagementFeesVenice))
        Else
          Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_ManagementFeesVenice, Col_Totals, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.ClassSpecific_ManagementFeesVenice))
        End If


        If (CDbl(Grid_FeesMultiClass(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesVenice, Col_Totals)) < 0.0#) Then
          Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesVenice, Col_Totals, GetFeeGridStyle("Numeric2_Negative", FeeGrid_RowIndex.ClassSpecific_PerformanceFeesVenice))
        Else
          Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesVenice, Col_Totals, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.ClassSpecific_PerformanceFeesVenice))
        End If

        If (CDbl(Grid_FeesMultiClass(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesVenice, Col_Totals)) < 0.0#) Then
          Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesVenice, Col_Totals, GetFeeGridStyle("Numeric2_Negative", FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesVenice))
        Else
          Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesVenice, Col_Totals, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesVenice))
        End If

        ' Set values for 'To Book; Rows.

        For ColCounter = (Col_Descriptions + 1) To (Col_Totals - 1)

          If (CInt(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_ManagementFeesChanged), 0))) = 0 Then
            Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_ManagementFeesValue) = CDbl(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_ManagementFeesAdministrator), 0))
          End If
          If (CInt(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesChanged), 0))) = 0 Then
            Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesValue) = CDbl(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesAdministrator), 0))
          End If
          If (CInt(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesChanged), 0))) = 0 Then
            Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesValue) = CDbl(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesAdministrator), 0))
          End If

        Next

        ' Add up fee row totals.

        Dim RowTotal As Double
        Dim EditorRow As Integer

        For Each EditorRow In New FeeGrid_RowIndex() {FeeGrid_RowIndex.ClassSpecific_ManagementFeesValue, FeeGrid_RowIndex.ClassSpecific_PerformanceFeesValue, FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesValue}
          RowTotal = 0.0#

          For ColCounter = (Col_Descriptions + 1) To (Col_Totals - 1)
            RowTotal += CDbl(Grid_FeesMultiClass.Cols(ColCounter)(EditorRow))
          Next
          Grid_FeesMultiClass.SetData(EditorRow, Col_Totals, RowTotal)
          If (RowTotal < 0.0#) Then
            Grid_FeesMultiClass.SetCellStyle(EditorRow, Col_Totals, GetFeeGridStyle("Numeric2_Negative", CType(EditorRow, FeeGrid_RowIndex)))
          Else
            Grid_FeesMultiClass.SetCellStyle(EditorRow, Col_Totals, GetFeeGridStyle("Numeric2", CType(EditorRow, FeeGrid_RowIndex)))
          End If
        Next

        ' Fill in Venice total and Difference columns.

        Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.ClassSpecific_ManagementFeesValue, Col_Venice, Grid_FeesMultiClass.Cols(Col_Totals)(FeeGrid_RowIndex.ClassSpecific_ManagementFeesVenice))
        If (CDbl(Nz(Grid_FeesMultiClass.Cols(Col_Venice)(FeeGrid_RowIndex.ClassSpecific_ManagementFeesValue), 0.0#)) < 0.0#) Then
          Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_ManagementFeesValue, Col_Venice, GetFeeGridStyle("Numeric2_Negative", CType(EditorRow, FeeGrid_RowIndex)))
        Else
          Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_ManagementFeesValue, Col_Venice, GetFeeGridStyle("Numeric2", CType(EditorRow, FeeGrid_RowIndex)))
        End If

        Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.ClassSpecific_ManagementFeesValue, Col_Difference, CDbl(Grid_FeesMultiClass.Cols(Col_Totals)(FeeGrid_RowIndex.ClassSpecific_ManagementFeesValue)) - CDbl(Grid_FeesMultiClass.Cols(Col_Venice)(FeeGrid_RowIndex.ClassSpecific_ManagementFeesValue)))
        If (CDbl(Nz(Grid_FeesMultiClass.Cols(Col_Difference)(FeeGrid_RowIndex.ClassSpecific_ManagementFeesValue), 0.0#)) < 0.0#) Then
          Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_ManagementFeesValue, Col_Difference, GetFeeGridStyle("Numeric2_Negative", CType(EditorRow, FeeGrid_RowIndex)))
        Else
          Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_ManagementFeesValue, Col_Difference, GetFeeGridStyle("Numeric2", CType(EditorRow, FeeGrid_RowIndex)))
        End If

        ' Unit Totals and formatting.

        Dim OpeningUnitDifferenceTotal As Double = 0.0#
        Dim UnitDifferenceTotal As Double = 0.0#

        Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.TodaysAdmin_UnitCount, Col_Totals, 0.0#)
        Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.VeniceTotals_Units, Col_Totals, 0.0#)

        For ColCounter = (Col_Descriptions + 1) To (Col_Totals - 1)

          OpeningUnitDifferenceTotal += Math.Abs(CDbl(Nz(Grid_FeesMultiClass.GetData(FeeGrid_RowIndex.YesterdayAdmin_UnitCount, ColCounter), 0)) - CDbl(Nz(Grid_FeesMultiClass.GetData(FeeGrid_RowIndex.VeniceOpening_Units, ColCounter), 0.0#)))
          UnitDifferenceTotal += Math.Abs(CDbl(Nz(Grid_FeesMultiClass.GetData(FeeGrid_RowIndex.TodaysAdmin_UnitCount, ColCounter), 0)) - CDbl(Nz(Grid_FeesMultiClass.GetData(FeeGrid_RowIndex.VeniceTotals_Units, ColCounter), 0.0#)))

          Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.TodaysAdmin_UnitCount, Col_Totals, CDbl(Nz(Grid_FeesMultiClass.GetData(FeeGrid_RowIndex.TodaysAdmin_UnitCount, ColCounter), 0)) + CDbl(Nz(Grid_FeesMultiClass.GetData(FeeGrid_RowIndex.TodaysAdmin_UnitCount, Col_Totals), 0)))
          Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.VeniceTotals_Units, Col_Totals, CDbl(Nz(Grid_FeesMultiClass.GetData(FeeGrid_RowIndex.VeniceTotals_Units, ColCounter), 0)) + CDbl(Nz(Grid_FeesMultiClass.GetData(FeeGrid_RowIndex.VeniceTotals_Units, Col_Totals), 0)))

        Next

        Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.YesterdayAdmin_UnitCount, Col_Difference, OpeningUnitDifferenceTotal)
        Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.TodaysAdmin_UnitCount, Col_Difference, UnitDifferenceTotal)

        If (OpeningUnitDifferenceTotal > 0.0#) Then
          Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.YesterdayAdmin_UnitCount, Col_Difference, GetFeeGridStyle("Numeric2_Negative", FeeGrid_RowIndex.YesterdayAdmin_UnitCount)) 'Style_Numeric2_Negative)
        Else
          Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.YesterdayAdmin_UnitCount, Col_Difference, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.YesterdayAdmin_UnitCount)) 'Style_Numeric2)
        End If

        If (UnitDifferenceTotal > 0.0#) Then
          Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.TodaysAdmin_UnitCount, Col_Difference, GetFeeGridStyle("Numeric2_Negative", FeeGrid_RowIndex.YesterdayAdmin_UnitCount)) 'Style_Numeric2_Negative)
        Else
          Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.TodaysAdmin_UnitCount, Col_Difference, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.YesterdayAdmin_UnitCount)) 'Style_Numeric2)
        End If

        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.TodaysAdmin_UnitCount, Col_Totals, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.YesterdayAdmin_UnitCount)) 'Style_Numeric2)
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.VeniceTotals_Units, Col_Totals, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.VeniceTotals_Units)) 'Style_Numeric2)

        Call FeesGrid_CalculateVeniceUnitPrice()

        ' Value Difference and formatting

        Dim Result As Double

        Result = CDbl(Nz(Grid_FeesMultiClass.GetData(FeeGrid_RowIndex.TodaysAdmin_Value, Col_Totals), 0)) - CDbl(Nz(Grid_FeesMultiClass.GetData(FeeGrid_RowIndex.TodaysAdmin_Value, Col_Venice), 0))
        Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.TodaysAdmin_Value, Col_Difference, Result)

        If (Result < 0.0#) Then
          Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.TodaysAdmin_Value, Col_Difference, GetFeeGridStyle("Integer_Negative", FeeGrid_RowIndex.TodaysAdmin_Value))
        Else
          Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.TodaysAdmin_Value, Col_Difference, GetFeeGridStyle("Integer", FeeGrid_RowIndex.TodaysAdmin_Value))
        End If

        CalculateFeesToBook(0)

      Catch ex As Exception

      End Try

    Catch ex As Exception

    Finally
      Try
        thisSQLConnection.Close()
      Catch ex As Exception
      End Try
    End Try

  End Sub


  Private Sub Grid_FeesMultiClass_BeforeEdit(ByVal sender As System.Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles Grid_FeesMultiClass.BeforeEdit

    Try
      Dim Col_Descriptions As Integer = Grid_FeesMultiClass.Cols("col_Descriptions").Index
      Dim Col_Totals As Integer = Grid_FeesMultiClass.Cols("col_Totals").Index

      If (e.Col = Col_Descriptions) OrElse (e.Col >= Col_Totals) Then
        e.Cancel = True
      End If

    Catch ex As Exception

    End Try
  End Sub

  Private Sub Grid_FeesMultiClass_AfterEdit(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles Grid_FeesMultiClass.AfterEdit

    Try

      Dim EditorRow As FeeGrid_RowIndex
      EditorRow = CType(e.Row, FeeGrid_RowIndex)
      Dim InstrumentId As Integer = CInt(Nz(Grid_FeesMultiClass.GetData(FeeGrid_RowIndex.InstrumentID, e.Col), 0))
      Dim RowTotal As Double = 0.0#
      Dim ColCounter As Integer
      Dim Col_Descriptions As Integer = Grid_FeesMultiClass.Cols("col_Descriptions").Index
      Dim Col_Totals As Integer = Grid_FeesMultiClass.Cols("col_Totals").Index
      Dim Col_Difference As Integer = Grid_FeesMultiClass.Cols("col_Difference").Index
      Dim Col_Venice As Integer = Grid_FeesMultiClass.Cols("col_Venice").Index

      Select Case EditorRow

        Case FeeGrid_RowIndex.ClassSpecific_ManagementFeesValue

          Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.ClassSpecific_ManagementFeesChanged, e.Col, 1)
          Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_ManagementFeesValue, e.Col, GetFeeGridStyle("Numeric2Edited", FeeGrid_RowIndex.ClassSpecific_ManagementFeesValue)) ' Style_Numeric2Edited)

          CalculateFeesToBook(InstrumentId)
          FeesGrid_CalculateVeniceUnitPrice()

        Case FeeGrid_RowIndex.ClassSpecific_PerformanceFeesValue

          Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesChanged, e.Col, 1)
          Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesValue, e.Col, GetFeeGridStyle("Numeric2Edited", FeeGrid_RowIndex.ClassSpecific_PerformanceFeesValue)) 'Style_Numeric2Edited)

          CalculateFeesToBook(InstrumentId)
          FeesGrid_CalculateVeniceUnitPrice()

        Case FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesValue

          Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesChanged, e.Col, 1)
          Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesValue, e.Col, GetFeeGridStyle("Numeric2Edited", FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesValue)) 'Style_Numeric2Edited)

          CalculateFeesToBook(InstrumentId)
          FeesGrid_CalculateVeniceUnitPrice()

      End Select

      ' Totals.

      Select Case EditorRow

        Case FeeGrid_RowIndex.ClassSpecific_ManagementFeesValue, FeeGrid_RowIndex.ClassSpecific_PerformanceFeesValue, FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesValue

          For ColCounter = (Col_Descriptions + 1) To (Col_Totals - 1)
            RowTotal += CDbl(Grid_FeesMultiClass.Cols(ColCounter)(EditorRow))
          Next
          Grid_FeesMultiClass.SetData(EditorRow, Col_Totals, RowTotal)
          If (RowTotal < 0.0#) Then
            Grid_FeesMultiClass.SetCellStyle(EditorRow, Col_Totals, GetFeeGridStyle("Numeric2_Negative", CType(EditorRow, FeeGrid_RowIndex)))
          Else
            Grid_FeesMultiClass.SetCellStyle(EditorRow, Col_Totals, GetFeeGridStyle("Numeric2", CType(EditorRow, FeeGrid_RowIndex)))
          End If

      End Select

      Select Case EditorRow

        Case FeeGrid_RowIndex.ClassSpecific_ManagementFeesValue

          Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.ClassSpecific_ManagementFeesValue, Col_Difference, CDbl(Grid_FeesMultiClass.Cols(Col_Totals)(FeeGrid_RowIndex.ClassSpecific_ManagementFeesValue)) - CDbl(Grid_FeesMultiClass.Cols(Col_Venice)(FeeGrid_RowIndex.ClassSpecific_ManagementFeesValue)))
          If (CDbl(Nz(Grid_FeesMultiClass.Cols(Col_Difference)(FeeGrid_RowIndex.ClassSpecific_ManagementFeesValue), 0.0#)) < 0.0#) Then
            Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_ManagementFeesValue, Col_Difference, GetFeeGridStyle("Numeric2_Negative", CType(EditorRow, FeeGrid_RowIndex)))
          Else
            Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ClassSpecific_ManagementFeesValue, Col_Difference, GetFeeGridStyle("Numeric2", CType(EditorRow, FeeGrid_RowIndex)))
          End If

      End Select

    Catch ex As Exception
    End Try




  End Sub

  Private Enum UnitFeeType As Integer
    None = 0
    ManagementFee = 1
    IncentiveFee = 2
    CrystalisedFee = 6
  End Enum

  Private Sub GetBookedFees(ByVal FundID As Integer, ByVal FeesAfterDate As Date, ByVal FeesUpToDate As Date, ByVal StatusGroupFilter As String, ByVal BookedManagementFees As Dictionary(Of Integer, Double), ByVal BookedPerformanceFees As Dictionary(Of Integer, Double), ByVal BookedCrystalisedFees As Dictionary(Of Integer, Double))
    ' ************************************************************************
    ' Returns Dictionaries for Fee values for each Fund Unit (Unit ID will be Zero for unallocated expenses)
    ' Fee values are returned in the currency of the Fund Unit, or in Fund currency for unallocated fees.
    '
    ' ************************************************************************

    Dim HasStatusGroupFilter As Boolean = False
    Dim StatusGroupID As Integer
    Dim StatusGroupHash As New HashSet(Of Integer)
    Dim DataViewCount As Integer
    Dim thisDataView As DataView
    Dim thisRowView As DataRowView
    Dim thisTransactionRow As DataRow = Nothing
    Dim Counter As Integer
    Dim InstrumentDS As DSInstrument
    Dim InstrumentTable As DSInstrument.tblInstrumentDataTable
    Dim thisInstrumentRow As DSInstrument.tblInstrumentRow = Nothing
    Dim thisUnitInstrumentRow As DSInstrument.tblInstrumentRow = Nothing

    Dim FeeInstrumentIDS As New Dictionary(Of Integer, DSInstrument.tblInstrumentRow)
    Dim UnitInstrumentIDS As New Dictionary(Of Integer, DSInstrument.tblInstrumentRow)
    Dim FeeInstrumentPrices As New Dictionary(Of Integer, Double)
    Dim FeeTransactionIDs As New Dictionary(Of Integer, UnitFeeType)
    Dim FundCurrencyID As Integer = 0

    Try

      FundCurrencyID = CInt(Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblFund, FundID, "FundBaseCurrency"), 0))

      ' Get Fee Instrument Details

      Try

        InstrumentDS = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblInstrument), DSInstrument)

        If (InstrumentDS IsNot Nothing) AndAlso (InstrumentDS.tblInstrument.Rows.Count > 0) Then
          InstrumentTable = InstrumentDS.tblInstrument

          For Counter = 0 To (InstrumentTable.Rows.Count - 1)
            thisInstrumentRow = InstrumentTable.Rows(Counter)

            If (thisInstrumentRow.InstrumentIsManagementFee) OrElse (thisInstrumentRow.InstrumentIsIncentiveFee) OrElse (thisInstrumentRow.InstrumentIsCrystalisedIncentive) Then
              If (Not FeeInstrumentIDS.ContainsKey(thisInstrumentRow.InstrumentID)) Then
                FeeInstrumentIDS.Add(thisInstrumentRow.InstrumentID, thisInstrumentRow)
              End If
            End If

            If (thisInstrumentRow.InstrumentType = InstrumentTypes.Unit) Then
              If (Not UnitInstrumentIDS.ContainsKey(thisInstrumentRow.InstrumentID)) Then
                UnitInstrumentIDS.Add(thisInstrumentRow.InstrumentID, thisInstrumentRow)
              End If
            End If

          Next

        End If

      Catch ex As Exception

      End Try

      ' Get Status Group info 

      If (StatusGroupFilter.Length > 0) Then
        StatusGroupID = CInt(Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblTradeStatusGroup, 0, "TradeStatusGroupID", "RN", "TradeStatusGroupName='" & StatusGroupFilter & "'"), 0))

        If (StatusGroupID) > 0 Then

          Dim StatusItemsDS As DSTradeStatusGroupItems = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblTradeStatusGroupItems), DSTradeStatusGroupItems)
          Dim StatusItemsRow As DSTradeStatusGroupItems.tblTradeStatusGroupItemsRow

          HasStatusGroupFilter = True

          If (StatusItemsDS IsNot Nothing) Then

            For Each StatusItemsRow In StatusItemsDS.tblTradeStatusGroupItems.Rows
              If (CInt(StatusItemsRow("GroupID")) = StatusGroupID) Then
                StatusGroupHash.Add(StatusItemsRow.StatusID)
              End If
            Next

          End If

        End If ' (StatusGroupID) > 0

      End If ' StatusGroupFilter.Length > 0

      ' 
      Dim TransactionDataset As RenaissanceDataClass.DSTransaction = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblTransaction), RenaissanceDataClass.DSTransaction)
      Dim TransactionDataView As DataView
      Dim ContraTransactionDataset As RenaissanceDataClass.DSTransactionContra = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblTransactionContra), RenaissanceDataClass.DSTransactionContra)
      Dim ContraTransactionDataView As DataView
      Dim SelectString As String
      Dim OrderString As String
      Dim Ordinal_Fund As Integer
      Dim Ordinal_Instrument As Integer
      Dim Ordinal_TransactionType As Integer
      Dim Ordinal_TransactionType_Contra As Integer
      Dim Ordinal_SignedUnits As Integer
      Dim Ordinal_SignedSettlement As Integer
      Dim Ordinal_TradeStatusID As Integer
      Dim Ordinal_ValueDate As Integer
      Dim Ordinal_SettlementDate As Integer
      Dim Ordinal_TransactionParentID As Integer
      Dim Ordinal_TransactionSpecificToFundUnitID As Integer

      Dim thisTransactionParentID As Integer
      Dim SpecificFundUnitID As Integer
      Dim thisTransactionType_Contra As Integer
      Dim thisInstrumentID As Integer
      Dim thisValueDate As Date
      Dim thisSignedUnits As Double
      Dim thisTradeStatusID As Integer
      Dim FeeValue As Double
      Dim thisPrice As Double
      Dim PricesRow As DataRow
      Dim thisFXRate As Double
      Dim thisUnitFeeType As UnitFeeType

      ' Select this Fund excluding Subscriptions and redemptions.
      SelectString = "(TransactionFund=" & FundID.ToString("###0") & ") AND (TransactionType<>2) AND (TransactionType<>3) AND (TransactionValueDate > '" & FeesAfterDate.ToString(QUERY_SHORTDATEFORMAT) & "')"
      OrderString = "TransactionFund, TransactionInstrument"

      TransactionDataView = New DataView(TransactionDataset.tblTransaction, SelectString, OrderString, DataViewRowState.CurrentRows)
      ContraTransactionDataView = New DataView(ContraTransactionDataset.tblTransactionContra, SelectString, OrderString, DataViewRowState.CurrentRows)

      For DataViewCount = 0 To 1

        If (DataViewCount = 0) Then
          thisDataView = TransactionDataView
        Else
          thisDataView = ContraTransactionDataView
        End If

        Ordinal_Fund = thisDataView.Table.Columns("TransactionFund").Ordinal
        Ordinal_Instrument = thisDataView.Table.Columns("TransactionInstrument").Ordinal
        Ordinal_TransactionType = thisDataView.Table.Columns("TransactionType").Ordinal
        Ordinal_TransactionType_Contra = thisDataView.Table.Columns("TransactionType_Contra").Ordinal
        Ordinal_SignedUnits = thisDataView.Table.Columns("TransactionSignedUnits").Ordinal
        Ordinal_SignedSettlement = thisDataView.Table.Columns("TransactionSignedSettlement").Ordinal
        Ordinal_TradeStatusID = thisDataView.Table.Columns("TransactionTradeStatusID").Ordinal
        Ordinal_ValueDate = thisDataView.Table.Columns("TransactionValueDate").Ordinal
        Ordinal_SettlementDate = thisDataView.Table.Columns("TransactionSettlementDate").Ordinal
        Ordinal_TransactionParentID = thisDataView.Table.Columns("TransactionParentID").Ordinal
        Ordinal_TransactionSpecificToFundUnitID = thisDataView.Table.Columns("TransactionSpecificToFundUnitID").Ordinal

        ' Loop through the selected data (DataView) building an ArrayList of aggregated Instrument positions.
        thisInstrumentRow = Nothing

        For Counter = 0 To (thisDataView.Count - 1) ' 

          thisRowView = thisDataView.Item(Counter)
          thisTransactionRow = thisRowView.Row

          thisTransactionParentID = CInt(thisTransactionRow(Ordinal_TransactionParentID))
          SpecificFundUnitID = CInt(thisTransactionRow(Ordinal_TransactionSpecificToFundUnitID))
          thisInstrumentID = CInt(thisTransactionRow(Ordinal_Instrument))
          thisValueDate = CDate(thisTransactionRow(Ordinal_ValueDate)).Date
          thisSignedUnits = CDbl(thisTransactionRow(Ordinal_SignedUnits))
          thisTransactionType_Contra = CInt(thisTransactionRow(Ordinal_TransactionType_Contra))

          ' Check Status Group (if not a subscription / redemption)
          If (HasStatusGroupFilter) AndAlso (thisTransactionType_Contra <> TransactionTypes.Subscribe) AndAlso (thisTransactionType_Contra <> TransactionTypes.Redeem) Then
            thisTradeStatusID = CInt(thisTransactionRow(Ordinal_TradeStatusID))

            If (thisTradeStatusID > 0) AndAlso (Not StatusGroupHash.Contains(thisTradeStatusID)) Then
              Continue For
            End If
          End If

          '

          If (SpecificFundUnitID > 0) Then
            UnitInstrumentIDS.TryGetValue(SpecificFundUnitID, thisUnitInstrumentRow)
          Else
            thisUnitInstrumentRow = Nothing
          End If

          If (thisValueDate > FeesUpToDate) Then
            Continue For
          End If

          If (DataViewCount = 0) Then
            ' Leg 1
            ' Collect TransactionIDs of Fee transactions
            If ((thisInstrumentRow IsNot Nothing) AndAlso (thisInstrumentRow.InstrumentID = thisInstrumentID)) OrElse (FeeInstrumentIDS.TryGetValue(thisInstrumentID, thisInstrumentRow)) Then

              If Not (FeeTransactionIDs.ContainsKey(thisTransactionParentID)) Then

                If (thisInstrumentRow.InstrumentIsManagementFee) Then
                  FeeTransactionIDs.Add(thisTransactionParentID, UnitFeeType.ManagementFee)
                ElseIf (thisInstrumentRow.InstrumentIsCrystalisedIncentive) Then
                  FeeTransactionIDs.Add(thisTransactionParentID, UnitFeeType.CrystalisedFee)
                ElseIf (thisInstrumentRow.InstrumentIsIncentiveFee) Then
                  FeeTransactionIDs.Add(thisTransactionParentID, UnitFeeType.IncentiveFee)
                Else
                  FeeTransactionIDs.Add(thisTransactionParentID, UnitFeeType.None)
                End If

              End If

              ' Save Fee value

              PricesRow = GetBestPriceRow(thisInstrumentID)
              If (PricesRow Is Nothing) Then
                thisPrice = 0.0#
              Else
                thisPrice = CDbl(Nz(PricesRow("PriceLevel"), 0.0#))
              End If

              If (thisUnitInstrumentRow IsNot Nothing) Then
                thisFXRate = GetFXToFund(thisInstrumentRow.InstrumentCurrencyID, thisUnitInstrumentRow.InstrumentCurrencyID)
              Else
                thisFXRate = GetFXToFund(thisInstrumentRow.InstrumentCurrencyID, FundCurrencyID)
              End If

              FeeValue = ((thisSignedUnits * thisPrice * thisInstrumentRow.InstrumentContractSize * thisInstrumentRow.InstrumentMultiplier)) * thisFXRate

              If (FeeValue <> 0.0#) Then
                If (thisInstrumentRow.InstrumentIsManagementFee) Then
                  If (Not BookedManagementFees.ContainsKey(SpecificFundUnitID)) Then
                    BookedManagementFees.Add(SpecificFundUnitID, FeeValue)
                  Else
                    BookedManagementFees(SpecificFundUnitID) += FeeValue
                  End If

                ElseIf thisInstrumentRow.InstrumentIsCrystalisedIncentive Then
                  If (Not BookedCrystalisedFees.ContainsKey(SpecificFundUnitID)) Then
                    BookedCrystalisedFees.Add(SpecificFundUnitID, FeeValue)
                  Else
                    BookedCrystalisedFees(SpecificFundUnitID) += FeeValue
                  End If

                ElseIf thisInstrumentRow.InstrumentIsIncentiveFee Then
                  If (Not BookedPerformanceFees.ContainsKey(SpecificFundUnitID)) Then
                    BookedPerformanceFees.Add(SpecificFundUnitID, FeeValue)
                  Else
                    BookedPerformanceFees(SpecificFundUnitID) += FeeValue
                  End If

                End If
              End If

            End If
          Else
            ' Leg 2
            ' Collect TransactionIDs of Fee transactions
            If (FeeTransactionIDs.TryGetValue(thisTransactionParentID, thisUnitFeeType)) Then

              If (thisInstrumentRow Is Nothing) OrElse (thisInstrumentRow.InstrumentID <> thisInstrumentID) Then
                thisInstrumentRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, thisInstrumentID), DSInstrument.tblInstrumentRow)
              End If

              ' Save Fee value

              PricesRow = GetBestPriceRow(thisInstrumentID)
              If (PricesRow Is Nothing) Then
                thisPrice = 0.0#
              Else
                thisPrice = CDbl(Nz(PricesRow("PriceLevel"), 0.0#))
              End If

              If (thisUnitInstrumentRow IsNot Nothing) Then
                thisFXRate = GetFXToFund(thisInstrumentRow.InstrumentCurrencyID, thisUnitInstrumentRow.InstrumentCurrencyID)
              Else
                thisFXRate = GetFXToFund(thisInstrumentRow.InstrumentCurrencyID, FundCurrencyID)
              End If

              FeeValue = -((thisSignedUnits * thisPrice * thisInstrumentRow.InstrumentContractSize * thisInstrumentRow.InstrumentMultiplier)) * thisFXRate

              If (FeeValue <> 0.0#) Then

                Select Case thisUnitFeeType

                  Case UnitFeeType.ManagementFee
                    If (Not BookedManagementFees.ContainsKey(SpecificFundUnitID)) Then
                      BookedManagementFees.Add(SpecificFundUnitID, FeeValue)
                    Else
                      BookedManagementFees(SpecificFundUnitID) += FeeValue
                    End If

                  Case UnitFeeType.IncentiveFee
                    If (Not BookedPerformanceFees.ContainsKey(SpecificFundUnitID)) Then
                      BookedPerformanceFees.Add(SpecificFundUnitID, FeeValue)
                    Else
                      BookedPerformanceFees(SpecificFundUnitID) += FeeValue
                    End If

                  Case UnitFeeType.CrystalisedFee
                    If (Not BookedCrystalisedFees.ContainsKey(SpecificFundUnitID)) Then
                      BookedCrystalisedFees.Add(SpecificFundUnitID, FeeValue)
                    Else
                      BookedCrystalisedFees(SpecificFundUnitID) += FeeValue
                    End If

                End Select

              End If ' (FeeValue <> 0.0#)

            End If ' thisTransactionParentID matches

          End If ' Leg 1 or 2

        Next Counter

      Next DataViewCount

    Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in GetBookedFees().", ex.StackTrace, True)
    End Try



  End Sub

	Private Sub FeesGrid_CalculateVeniceUnitPrice()

		Dim ColCounter As Integer
		Dim Col_Descriptions As Integer
		Dim Col_Totals As Integer
		Dim Col_Difference As Integer
		Dim Col_Venice As Integer
		Dim VeniceClosingValueTotal As Double
		Dim ClassBNAV As Double
		Dim ClassManagementFees As Double
		Dim VeniceClassUnitCount As Double

		Try
      Col_Descriptions = Grid_FeesMultiClass.Cols("col_Descriptions").Index
      Col_Totals = Grid_FeesMultiClass.Cols("col_Totals").Index
      Col_Difference = Grid_FeesMultiClass.Cols("col_Difference").Index
      Col_Venice = Grid_FeesMultiClass.Cols("col_Venice").Index

      ' Closing Venice values and Price
      VeniceClosingValueTotal = 0.0#

      For ColCounter = (Col_Descriptions + 1) To (Col_Totals - 1)

        ClassBNAV = CDbl(Nz(Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.Venice_BNAV)(ColCounter), 0.0#))
        'ClassManagementFees = CDbl(Nz(Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.ClassSpecific_ManagementFeesValue)(ColCounter), 0.0#))
        'ClassManagementFees += CDbl(Nz(Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesValue)(ColCounter), 0.0#))
        'ClassManagementFees += CDbl(Nz(Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesValue)(ColCounter), 0.0#))
        ClassManagementFees = CDbl(Nz(Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.ManagementFeesBooked)(ColCounter), 0.0#))
        ClassManagementFees += CDbl(Nz(Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.PerformanceFeesBooked)(ColCounter), 0.0#))
        ClassManagementFees += CDbl(Nz(Grid_FeesMultiClass.Rows(FeeGrid_RowIndex.CrystalisedFeesBooked)(ColCounter), 0.0#))

        Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.VeniceTotals_Value, ColCounter, (ClassBNAV - ClassManagementFees))

        VeniceClosingValueTotal += (ClassBNAV - ClassManagementFees)

        VeniceClassUnitCount = CDbl(Nz(Grid_FeesMultiClass.GetData(FeeGrid_RowIndex.VeniceTotals_Units, ColCounter), 0))
        'Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.VeniceTotals_Units, ColCounter, Style_Numeric2)

        If (Math.Abs(VeniceClassUnitCount) < 0.001#) OrElse (Math.Abs((ClassBNAV - ClassManagementFees)) < 1.0#) Then
          Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.VeniceTotals_Price, ColCounter, 0.0#)
          Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.VeniceTotals_Price, ColCounter, GetFeeGridStyle("Numeric4", FeeGrid_RowIndex.VeniceTotals_Price)) 'Style_Numeric4)
        Else
          Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.VeniceTotals_Price, ColCounter, (ClassBNAV - ClassManagementFees) / VeniceClassUnitCount)

          If ((ClassBNAV - ClassManagementFees) / VeniceClassUnitCount < 0.0#) Then
            Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.VeniceTotals_Price, ColCounter, GetFeeGridStyle("Numeric4_Negative", FeeGrid_RowIndex.VeniceTotals_Price)) 'Style_Numeric4_Negative)
          Else
            Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.VeniceTotals_Price, ColCounter, GetFeeGridStyle("Numeric4", FeeGrid_RowIndex.VeniceTotals_Price)) 'Style_Numeric4)
          End If
        End If
      Next

      Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.VeniceTotals_Value, Col_Totals, VeniceClosingValueTotal)
      Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.VeniceTotals_Value, Col_Totals, GetFeeGridStyle("Integer", FeeGrid_RowIndex.VeniceTotals_Value)) 'Style_Integer)

      Grid_FeesMultiClass.SetData(FeeGrid_RowIndex.TodaysAdmin_Value, Col_Venice, VeniceClosingValueTotal) ' FundValuation.FundNAV)

      If (VeniceClosingValueTotal < 0.0#) Then
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.TodaysAdmin_Value, Col_Venice, GetFeeGridStyle("Integer_Negative", FeeGrid_RowIndex.TodaysAdmin_Value)) 'Style_Integer_Negative)
      Else
        Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.TodaysAdmin_Value, Col_Venice, GetFeeGridStyle("Integer", FeeGrid_RowIndex.TodaysAdmin_Value)) 'Style_Integer)
      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in FeesGrid_CalculateVeniceUnitPrice().", ex.StackTrace, True)
    End Try
	End Sub

  Private Sub CalculateFeesToBook(ByVal InstrumentId As Integer)

    Dim ColCounter As Integer
    Dim Col_Descriptions As Integer
    Dim Col_Totals As Integer
    Dim Col_Difference As Integer
    Dim Col_Venice As Integer

    Dim VeniceFees As Double
    Dim FeesBooked As Double
    Dim FeesToBook As Double

    Try
      Col_Descriptions = Grid_FeesMultiClass.Cols("col_Descriptions").Index
      Col_Totals = Grid_FeesMultiClass.Cols("col_Totals").Index
      Col_Difference = Grid_FeesMultiClass.Cols("col_Difference").Index
      Col_Venice = Grid_FeesMultiClass.Cols("col_Venice").Index

      Dim ThisInstrumentID As Integer

      For ColCounter = (Col_Descriptions + 1) To (Col_Totals - 1)

        ThisInstrumentID = CInt(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.InstrumentID), 0))

        If (InstrumentId <> 0) AndAlso (InstrumentId <> ThisInstrumentID) Then
          Continue For
        End If

        ' Management Fees
        ' Should be in Fund Currency already ?

        VeniceFees = CDbl(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_ManagementFeesValue), 0.0#))
        FeesBooked = CDbl(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ManagementFeesBooked), 0.0#))

        FeesToBook = VeniceFees - FeesBooked
        Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ManagementFeesToBook) = FeesToBook

        If FeesToBook < 0.0# Then
          Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ManagementFeesToBook, ColCounter, GetFeeGridStyle("Numeric2_Negative", FeeGrid_RowIndex.ManagementFeesToBook)) 'Style_Numeric2_Negative)
        Else
          Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.ManagementFeesToBook, ColCounter, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.ManagementFeesToBook)) 'Style_Numeric2)
        End If

        ' Management Fees

        VeniceFees = CDbl(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesValue), 0.0#))
        FeesBooked = CDbl(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.PerformanceFeesBooked), 0.0#))

        FeesToBook = VeniceFees - FeesBooked
        Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.PerformanceFeesToBook) = FeesToBook

        If FeesToBook < 0.0# Then
          Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.PerformanceFeesToBook, ColCounter, GetFeeGridStyle("Numeric2_Negative", FeeGrid_RowIndex.PerformanceFeesToBook)) 'Style_Numeric2_Negative)
        Else
          Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.PerformanceFeesToBook, ColCounter, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.PerformanceFeesToBook)) 'Style_Numeric2)
        End If

        ' Crystalised Fees

        VeniceFees = CDbl(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.ClassSpecific_CrystalisedFeesValue), 0.0#))
        FeesBooked = CDbl(Nz(Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.CrystalisedFeesBooked), 0.0#))

        FeesToBook = VeniceFees - FeesBooked
        Grid_FeesMultiClass.Cols(ColCounter)(FeeGrid_RowIndex.CrystalisedFeesToBook) = FeesToBook

        If FeesToBook < 0.0# Then
          Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.CrystalisedFeesToBook, ColCounter, GetFeeGridStyle("Numeric2_Negative", FeeGrid_RowIndex.CrystalisedFeesToBook)) 'Style_Numeric2_Negative)
        Else
          Grid_FeesMultiClass.SetCellStyle(FeeGrid_RowIndex.CrystalisedFeesToBook, ColCounter, GetFeeGridStyle("Numeric2", FeeGrid_RowIndex.CrystalisedFeesToBook)) 'Style_Numeric2)
        End If

			Next

    Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in CalculateFeesToBook().", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub ShowHideReconciliationTabs(ByVal FundID As Integer)

    Try
      Dim PagesToHide() As System.Windows.Forms.TabPage
      Dim PagesToShow() As System.Windows.Forms.TabPage

      If FundID = 0 Then
        PagesToHide = New TabPage() {Tab_Benchmark, Tab_Fees, Tab_FeesMultiClass, Tab_Milestone}
        PagesToShow = New TabPage() {}
      Else
        Dim InstrumentDS As DSInstrument = MainForm.Load_Table(RenaissanceStandardDatasets.tblInstrument)
        Dim InstrumentRows() As DSInstrument.tblInstrumentRow = Nothing
        Dim FundRow As DSFund.tblFundRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, FundID)

        If (InstrumentDS IsNot Nothing) Then
          InstrumentRows = InstrumentDS.tblInstrument.Select("InstrumentFundID=" & FundID.ToString)
        End If

        If ((FundRow Is Nothing) OrElse (FundRow.FundForceMultiShareClassReconciliation = False)) AndAlso ((InstrumentRows Is Nothing) OrElse (InstrumentRows.Length <= 1)) Then
          ' Single Class
          PagesToHide = New TabPage() {Tab_FeesMultiClass}
          PagesToShow = New TabPage() {Tab_Benchmark, Tab_Fees, Tab_Milestone}
        Else
          ' Multi Class
          PagesToHide = New TabPage() {Tab_Benchmark, Tab_Fees, Tab_Milestone}
          PagesToShow = New TabPage() {Tab_FeesMultiClass}
        End If
      End If

      For Each ThisPageName As System.Windows.Forms.TabPage In PagesToHide
        Try
          If (TabControl_Reconciliation.TabPages.Contains(ThisPageName)) Then
            TabControl_Reconciliation.TabPages.Remove(ThisPageName)
          End If
          If (Not TabControl_Hidden.TabPages.Contains(ThisPageName)) Then
            TabControl_Hidden.TabPages.Add(ThisPageName)
          End If
        Catch ex As Exception
        End Try
      Next

      For Each ThisPageName As System.Windows.Forms.TabPage In PagesToShow
        Try
          If (TabControl_Hidden.TabPages.Contains(ThisPageName)) Then
            TabControl_Hidden.TabPages.Remove(ThisPageName)
          End If
          If (Not TabControl_Reconciliation.TabPages.Contains(ThisPageName)) Then
            TabControl_Reconciliation.TabPages.Insert(1, ThisPageName)
          End If
        Catch ex As Exception
        End Try
      Next


    Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in ShowHideReconciliationTabs().", ex.StackTrace, True)
		End Try
  End Sub

  Private Sub Button_Fee_BookManagement_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Fee_BookManagement.Click
    ' *******************************************************************************
    '
    '
    ' *******************************************************************************

    Try
      Button_Fee_BookManagement.Enabled = False

      ' Check permissions.
      Dim Permissions As Integer = MainForm.CheckPermissions(Me.Name(), PermissionFeatureType.TypeForm)
      Dim UpdateString As String = ""
      Dim NewTransactionID As String

      If ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) = 0) AndAlso ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) = 0) Then
        MessageBox.Show("You do not have Permissions to change transactions.")
        Exit Sub
      End If

      Dim FeesToTake As Double = 0.0#
      Dim FeeDescription As String = ""
      Dim FundUnitID As Integer = 0
      Dim FundID As Integer = CInt(Nz(Combo_Fund.SelectedValue, 0))
      Dim FundRow As DSFund.tblFundRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, FundID)
      Dim ColCounter As Integer
      Dim Col_Descriptions As Integer
      Dim Col_Totals As Integer
      Dim Col_Difference As Integer
      Dim Col_Venice As Integer
      Dim FundYearEnd As Date
      Dim MgmtFeesSettlementDate As Date
      Dim ManagementFeeInstrument As Integer

      If (FundID <= 0) OrElse (FundRow Is Nothing) Then
        Exit Sub
      End If

      Dim MgmtFeePaymentPeriod As DealingPeriod = CType(FundRow.FundManagementFeesPaymentPeriod, DealingPeriod)

      Col_Descriptions = Grid_FeesMultiClass.Cols("col_Descriptions").Index
      Col_Totals = Grid_FeesMultiClass.Cols("col_Totals").Index
      Col_Difference = Grid_FeesMultiClass.Cols("col_Difference").Index
      Col_Venice = Grid_FeesMultiClass.Cols("col_Venice").Index

      ' Dates

      If FundRow.IsFundYearEndNull Then
        FundYearEnd = New Date(Now.Year, 12, 31) ' Default to calendar year
      Else
        FundYearEnd = FitDateToPeriod(DealingPeriod.Monthly, New Date(Date_ValueDate.Value.Year, FundRow.FundYearEnd.Month, 28), True)

        If (FundYearEnd < Date_ValueDate.Value.Date) Then
          FundYearEnd = FundYearEnd.AddYears(1)
        End If
      End If

      If (MgmtFeePaymentPeriod = DealingPeriod.Annually) Then
        MgmtFeesSettlementDate = FundYearEnd
      Else
        MgmtFeesSettlementDate = RenaissanceUtilities.DatePeriodFunctions.FitDateToPeriod(MgmtFeePaymentPeriod, Date_ValueDate.Value, True)
      End If

      ' Instrument 

      ManagementFeeInstrument = FundRow.FundManagementFeeInstrument

      'lotfi: TODO: check all old BNP prices dates and alert if one is prcie dzate is older the 3 days or 7adaus

      For ColCounter = (Col_Descriptions + 1) To (Col_Totals - 1)

        FundUnitID = CInt(Nz(Grid_FeesMultiClass.GetData(FeeGrid_RowIndex.InstrumentID, ColCounter), 0))

        ' Management Fees

        FeesToTake = CDbl(Nz(Grid_FeesMultiClass.GetData(FeeGrid_RowIndex.ManagementFeesToBook, ColCounter), 0.0#))

        If (FundUnitID <> 0) AndAlso (ManagementFeeInstrument <> 0) AndAlso (Math.Abs(FeesToTake) > 0.1) Then
          NewTransactionID = BookFees(0, 0, FundID, FundID, ManagementFeeInstrument, FeesToTake, Date_ValueDate.Value, MgmtFeesSettlementDate, False, True, True, True, False, FundUnitID, "")

          If (NewTransactionID.Length > 0) Then
            UpdateString &= NewTransactionID & ","
          End If
        End If

      Next

      If (UpdateString.Length > 0) Then
        MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceStandardDatasets.tblTransaction.ChangeID, UpdateString), True)
      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error saving Management Fee Transaction.", ex.StackTrace, True)
    Finally
      Button_Fee_BookManagement.Enabled = True
    End Try

  End Sub

  Private Sub Button_Fee_BookPerformance_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Fee_BookPerformance.Click
    ' *******************************************************************************
    '
    '
    ' *******************************************************************************

    Try
      Button_Fee_BookPerformance.Enabled = False

      ' Check permissions.
      Dim Permissions As Integer = MainForm.CheckPermissions(Me.Name(), PermissionFeatureType.TypeForm)
      Dim UpdateString As String = ""
      Dim NewTransactionID As String

      If ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) = 0) AndAlso ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) = 0) Then
        MessageBox.Show("You do not have Permissions to change transactions.")
        Exit Sub
      End If

      Dim FeesToTake As Double = 0.0#
      Dim FeeDescription As String = ""
      Dim FundUnitID As Integer = 0
      Dim FundID As Integer = CInt(Nz(Combo_Fund.SelectedValue, 0))
      Dim FundRow As DSFund.tblFundRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, FundID)
      Dim ColCounter As Integer
      Dim Col_Descriptions As Integer
      Dim Col_Totals As Integer
      Dim Col_Difference As Integer
      Dim Col_Venice As Integer
      Dim FundYearEnd As Date
      Dim PerfFeesSettlementDate As Date
      Dim PerformanceFeeInstrument As Integer

      If (FundID <= 0) OrElse (FundRow Is Nothing) Then
        Exit Sub
      End If

      Dim PerfFeePaymentPeriod As DealingPeriod = CType(FundRow.FundPerformanceFeesPaymentPeriod, DealingPeriod)

      Col_Descriptions = Grid_FeesMultiClass.Cols("col_Descriptions").Index
      Col_Totals = Grid_FeesMultiClass.Cols("col_Totals").Index
      Col_Difference = Grid_FeesMultiClass.Cols("col_Difference").Index
      Col_Venice = Grid_FeesMultiClass.Cols("col_Venice").Index

      ' Dates

      If FundRow.IsFundYearEndNull Then
        FundYearEnd = New Date(Now.Year, 12, 31) ' Default to calendar year
      Else
        FundYearEnd = FitDateToPeriod(DealingPeriod.Monthly, New Date(Date_ValueDate.Value.Year, FundRow.FundYearEnd.Month, 28), True)

        If (FundYearEnd < Date_ValueDate.Value.Date) Then
          FundYearEnd = FundYearEnd.AddYears(1)
        End If
      End If

      If (Not FundRow.IsFundInvestmentStartDateNull) Then
        If (FundYearEnd < FundRow.FundInvestmentStartDate.AddYears(1)) Then
          FundYearEnd = FundYearEnd.AddYears(1)
        End If
      End If

      If (PerfFeePaymentPeriod = DealingPeriod.Annually) Then
        PerfFeesSettlementDate = FundYearEnd
      Else
        PerfFeesSettlementDate = RenaissanceUtilities.DatePeriodFunctions.FitDateToPeriod(PerfFeePaymentPeriod, Date_ValueDate.Value, True)
      End If

      ' Instrument 

      PerformanceFeeInstrument = FundRow.FundPerformanceFeeInstrument

      For ColCounter = (Col_Descriptions + 1) To (Col_Totals - 1)

        FundUnitID = CInt(Nz(Grid_FeesMultiClass.GetData(FeeGrid_RowIndex.InstrumentID, ColCounter), 0))

        ' Management Fees

        FeesToTake = CDbl(Nz(Grid_FeesMultiClass.GetData(FeeGrid_RowIndex.PerformanceFeesToBook, ColCounter), 0.0#))

        If (FundUnitID <> 0) AndAlso (PerformanceFeeInstrument <> 0) AndAlso (Math.Abs(FeesToTake) > 0.1) Then
          NewTransactionID = BookFees(0, 0, FundID, FundID, PerformanceFeeInstrument, FeesToTake, Date_ValueDate.Value, PerfFeesSettlementDate, False, True, True, False, False, FundUnitID, "")

          If (NewTransactionID.Length > 0) Then
            UpdateString &= NewTransactionID & ","
          End If
        End If

      Next

      If (UpdateString.Length > 0) Then
        MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceStandardDatasets.tblTransaction.ChangeID, UpdateString), True)
      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error saving Performance Fee Transaction.", ex.StackTrace, True)
    Finally
      Button_Fee_BookPerformance.Enabled = True
    End Try
  End Sub

  Private Sub Button_Fee_BookCrystalised_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Fee_BookCrystalised.Click
    ' *******************************************************************************
    '
    '
    ' *******************************************************************************

    Try
      Button_Fee_BookCrystalised.Enabled = False

      ' Check permissions.
      Dim Permissions As Integer = MainForm.CheckPermissions(Me.Name(), PermissionFeatureType.TypeForm)
      Dim UpdateString As String = ""
      Dim NewTransactionID As String

      If ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) = 0) AndAlso ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) = 0) Then
        MessageBox.Show("You do not have Permissions to change transactions.")
        Exit Sub
      End If

      Dim FeesToTake As Double = 0.0#
      Dim FeeDescription As String = ""
      Dim FundUnitID As Integer = 0
      Dim FundID As Integer = CInt(Nz(Combo_Fund.SelectedValue, 0))
      Dim FundRow As DSFund.tblFundRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, FundID)
      Dim ColCounter As Integer
      Dim Col_Descriptions As Integer
      Dim Col_Totals As Integer
      Dim Col_Difference As Integer
      Dim Col_Venice As Integer
      Dim FundYearEnd As Date
      Dim PerfFeesSettlementDate As Date
      Dim PerformanceFeeInstrument As Integer

      If (FundID <= 0) OrElse (FundRow Is Nothing) Then
        Exit Sub
      End If

      Dim PerfFeePaymentPeriod As DealingPeriod = CType(FundRow.FundPerformanceFeesPaymentPeriod, DealingPeriod)

      Col_Descriptions = Grid_FeesMultiClass.Cols("col_Descriptions").Index
      Col_Totals = Grid_FeesMultiClass.Cols("col_Totals").Index
      Col_Difference = Grid_FeesMultiClass.Cols("col_Difference").Index
      Col_Venice = Grid_FeesMultiClass.Cols("col_Venice").Index

      ' Dates

      If FundRow.IsFundYearEndNull Then
        FundYearEnd = New Date(Now.Year, 12, 31) ' Default to calendar year
      Else
        FundYearEnd = FitDateToPeriod(DealingPeriod.Monthly, New Date(Date_ValueDate.Value.Year, FundRow.FundYearEnd.Month, 28), True)

        If (FundYearEnd < Date_ValueDate.Value.Date) Then
          FundYearEnd = FundYearEnd.AddYears(1)
        End If
      End If

      If (PerfFeePaymentPeriod = DealingPeriod.Annually) Then
        PerfFeesSettlementDate = FundYearEnd
      Else
        PerfFeesSettlementDate = RenaissanceUtilities.DatePeriodFunctions.FitDateToPeriod(PerfFeePaymentPeriod, Date_ValueDate.Value, True)
      End If

      ' Instrument 

      PerformanceFeeInstrument = FundRow.FundPerformanceFeeCrystalisedInstrument

      For ColCounter = (Col_Descriptions + 1) To (Col_Totals - 1)

        FundUnitID = CInt(Nz(Grid_FeesMultiClass.GetData(FeeGrid_RowIndex.InstrumentID, ColCounter), 0))

        ' Management Fees

        FeesToTake = CDbl(Nz(Grid_FeesMultiClass.GetData(FeeGrid_RowIndex.CrystalisedFeesToBook, ColCounter), 0.0#))

        If (FundUnitID <> 0) AndAlso (PerformanceFeeInstrument <> 0) AndAlso (Math.Abs(FeesToTake) > 0.1) Then
          NewTransactionID = BookFees(0, 0, FundID, FundID, PerformanceFeeInstrument, FeesToTake, Date_ValueDate.Value, PerfFeesSettlementDate, False, True, True, True, False, FundUnitID, "")

          If (NewTransactionID.Length > 0) Then
            UpdateString &= NewTransactionID & ","
          End If
        End If

      Next

      If (UpdateString.Length > 0) Then
        MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceStandardDatasets.tblTransaction.ChangeID, UpdateString), True)
      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error saving Performance Fee Transaction.", ex.StackTrace, True)
    Finally
      Button_Fee_BookCrystalised.Enabled = True
    End Try
  End Sub

  Private Sub Button_Fee_SetMilestone_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Fee_SetMilestone.Click
    ' *******************************************************************************
    '
    '
    ' *******************************************************************************

    Try
      ' Check permissions.
      Dim Permissions As Integer = MainForm.CheckPermissions(Me.Name(), PermissionFeatureType.TypeForm)

      If ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) = 0) AndAlso ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) = 0) Then
        MessageBox.Show("You do not have Permissions to action End of Day.")
        Exit Sub
      End If
    Catch ex As Exception
      Exit Sub
    End Try

    Try
      Button_Fee_SetMilestone.Enabled = False

      Dim FundUnitID As Integer = 0
      Dim FundID As Integer = CInt(Nz(Combo_Fund.SelectedValue, 0))
      Dim FundRow As DSFund.tblFundRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, FundID)
      Dim ColCounter As Integer
      Dim Col_Descriptions As Integer
      Dim Col_Totals As Integer
      Dim Col_Difference As Integer
      Dim Col_Venice As Integer
      Dim MilestoneDate As Date
      Dim FundPricingPeriod As RenaissanceGlobals.DealingPeriod
      Dim StatusGroupFilter As String
      Dim AdministratorDatesFilter As RenaissanceGlobals.AdministratorDatesFilter = RenaissanceGlobals.AdministratorDatesFilter.None
      Dim FundValuation As FundFeeCalculator.FundValuationDetails = Nothing
      Dim SavedUnitPrices As New Dictionary(Of Integer, Double) ' Save prices during milestone to use when updating transactions. (Saves looking them up again).

      If (FundID <= 0) OrElse (FundRow Is Nothing) Then
        Exit Sub
      End If

      Col_Descriptions = Grid_FeesMultiClass.Cols("col_Descriptions").Index
      Col_Totals = Grid_FeesMultiClass.Cols("col_Totals").Index
      Col_Difference = Grid_FeesMultiClass.Cols("col_Difference").Index
      Col_Venice = Grid_FeesMultiClass.Cols("col_Venice").Index

      FundPricingPeriod = CType(FundRow.FundPricingPeriod, RenaissanceGlobals.DealingPeriod)

      MilestoneDate = FitDateToPeriod(FundPricingPeriod, Date_ValueDate.Value.Date, True)

      If MessageBox.Show("Set Milestone" & vbCrLf & "Fund : " & FundRow.FundName & vbCrLf & "Date : " & MilestoneDate.ToString(DISPLAYMEMBER_DATEFORMAT), "Set Milestone", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) <> Windows.Forms.DialogResult.OK Then
        Exit Sub
      End If

      ' 

      Try

        ' Get Fund Fees
        StatusGroupFilter = Nz(MainForm.GetComboSelectedValue(Combo_TransactionGroup), "")

        ' AdministratorDatesFilter (bitmap)
        '
        '--	1		- Ignore the transaction status for Subscriptions and Redemptions
        '--	2		- Ignore the Subscriptions and Redemptions on (or after of course) the given Value Date

        If (MainForm.GetCheckBoxChecked(Me.Check_SR_IgnoreStatus)) Then
          AdministratorDatesFilter += RenaissanceGlobals.AdministratorDatesFilter.SubscriptionRedemption_IgnoreTransactionStatus
        End If
        If (MainForm.GetCheckBoxChecked(Me.Check_SR_IgnoreTodaysTrades)) Then
          AdministratorDatesFilter += RenaissanceGlobals.AdministratorDatesFilter.SubscriptionRedemption_IgnoreTodaysTrades
        End If

        '

        MainForm.SetToolStripText(StatusLabel, "Calculating Fund Administrator Valuation.")

        FundValuation = GetFeesValuation(FeesObject, FundID, Date_ValueDate.Value.Date, Date_FeesSince.Value, StatusGroupFilter, AdministratorDatesFilter, Check_AdministratorPriceDates.Checked)

      Catch ex As Exception
        MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error getting Valuation or Fee Calculations.", ex.StackTrace, True)
      Finally
        MainForm.SetToolStripText(StatusLabel, "")
      End Try

      ' Save Milestone record.

      Try
        ' Save Milestone 
        Dim MilestonesDS As DSFundMilestones = MainForm.Load_Table(RenaissanceStandardDatasets.tblFundMilestones, False)
        Dim MilestonesTable As DSFundMilestones.tblFundMilestonesDataTable
        Dim MilestonesSelectedRows() As DSFundMilestones.tblFundMilestonesRow

        MainForm.SetToolStripText(Label_Status, "Saving Milestone")

        If (MilestonesDS IsNot Nothing) Then
          MilestonesTable = MilestonesDS.tblFundMilestones
        Else
          MainForm.LogError(Me.Name & ", button_Fee_SetMilestone_Click()", LOG_LEVELS.Error, "", "Failed to load Milestone Dataset.", "", True)
          Exit Sub
        End If

        Dim MilestoneAdaptor As SqlDataAdapter
        Dim NewMilestoneRow As RenaissanceDataClass.DSFundMilestones.tblFundMilestonesRow

        MilestoneAdaptor = MainForm.MainDataHandler.Get_Adaptor(RenaissanceStandardDatasets.tblFundMilestones.Adaptorname, VENICE_CONNECTION, RenaissanceStandardDatasets.tblFundMilestones.TableName)

        Dim AdministratorNAV As Double = CDbl(Nz(Grid_FeesMultiClass.GetData(FeeGrid_RowIndex.TodaysAdmin_Value, Col_Totals), 0.0#))
        Dim AdministratorGNAV As Double = CDbl(Nz(Grid_FeesMultiClass.GetData(FeeGrid_RowIndex.TodaysAdmin_Value, Col_Totals), 0.0#)) - CDbl(Nz(Grid_FeesMultiClass.GetData(FeeGrid_RowIndex.ClassSpecific_PerformanceFeesAdministrator, Col_Totals), 0.0#))
        Dim FundNAV As Double = CDbl(Nz(Grid_FeesMultiClass.GetData(FeeGrid_RowIndex.VeniceTotals_Value, Col_Totals), 0.0#))

        SyncLock MilestonesDS

          MilestonesSelectedRows = MilestonesTable.Select("(MilestoneFundID=" & FundID.ToString & ") AND (MilestoneDate='" & MilestoneDate.ToString(QUERY_SHORTDATEFORMAT) & "')", "MilestoneDate")

          If (Not (MilestonesSelectedRows Is Nothing)) AndAlso (MilestonesSelectedRows.Length > 0) Then
            ' Edit Existing Record

            MilestonesSelectedRows(0).MilestoneKnowledgeDate = Now()

            MilestonesSelectedRows(0).Administrator_NAV = AdministratorNAV
            MilestonesSelectedRows(0).Administrator_GNAV = AdministratorGNAV
            MilestonesSelectedRows(0).Fund_NAV = FundNAV
            MilestonesSelectedRows(0).Administrator_UnitsSubscribed = Math.Abs(FundValuation.FundUnitsSubscribed)
            MilestonesSelectedRows(0).Administrator_UnitsRedeemed = Math.Abs(FundValuation.FundUnitsRedeemed)

          Else
            NewMilestoneRow = MilestonesTable.NewtblFundMilestonesRow

            NewMilestoneRow.MilestoneFundID = FundID
            NewMilestoneRow.MilestoneDate = MilestoneDate
            NewMilestoneRow.MilestoneDateString = MilestoneDate.ToString(DISPLAYMEMBER_DATEFORMAT)
            NewMilestoneRow.MilestoneKnowledgeDate = Now()

            NewMilestoneRow.Administrator_NAV = AdministratorNAV
            NewMilestoneRow.Administrator_GNAV = AdministratorGNAV
            NewMilestoneRow.Fund_NAV = FundNAV
            NewMilestoneRow.Administrator_UnitsSubscribed = Math.Abs(FundValuation.FundUnitsSubscribed)
            NewMilestoneRow.Administrator_UnitsRedeemed = Math.Abs(FundValuation.FundUnitsRedeemed)

            MilestonesTable.Rows.Add(NewMilestoneRow)
          End If

          MilestoneAdaptor.InsertCommand.Parameters("@KnowledgeDate").Value = KNOWLEDGEDATE_NOW
          MilestoneAdaptor.UpdateCommand.Parameters("@KnowledgeDate").Value = KNOWLEDGEDATE_NOW

          MainForm.AdaptorUpdate(Me.Name, MilestoneAdaptor, MilestonesTable)

        End SyncLock

        Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblFundMilestones), True)

      Catch ex As Exception
        MainForm.LogError(Me.Name & ", button_SetMilestone_Click()", LOG_LEVELS.Error, ex.Message, "Error Saving Milestone entry.", ex.StackTrace, True)
        Exit Sub
      End Try

      Dim UnitPrice As Double
      Dim GNAVUnitPrice As Double

      MainForm.SetToolStripText(Label_Status, "Saving Unit prices")

      For ColCounter = (Col_Descriptions + 1) To (Col_Totals - 1)

        FundUnitID = CInt(Nz(Grid_FeesMultiClass.GetData(FeeGrid_RowIndex.InstrumentID, ColCounter), 0))
        UnitPrice = CDbl(Nz(Grid_FeesMultiClass.GetData(FeeGrid_RowIndex.TodaysAdmin_UnitPrice, ColCounter), 0))

        ' Temporary measure. Probably only a problem when equalisation is used.
        GNAVUnitPrice = UnitPrice

        ' Save Fund Price

        If (FundUnitID > 0) AndAlso (UnitPrice <> 0.0#) Then
          SavePrice(MainForm, FundUnitID, MilestoneDate, UnitPrice, 0.0#, GNAVUnitPrice, 0.0#, False, 0.0#, MilestoneDate, 1, 0, True, Now(), "Milestone Price.", 0, 0, 0, 1, 0)

          If (Not SavedUnitPrices.ContainsKey(FundUnitID)) Then
            SavedUnitPrices.Add(FundUnitID, UnitPrice)
          End If
        End If

      Next

      Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblPrice), True)

      Try
        ' Update relevant Subscriptions / Redemptions.
        ' If Selected and the price is non-zero.

        MainForm.SetToolStripText(Label_Status, "Update Subscriptions")

        If (Me.Check_UpdateOrdersAutomatically.Checked) Then

          Dim TransactionDS As RenaissanceDataClass.DSTransaction
          Dim TransactionTable As DSTransaction.tblTransactionDataTable
          Dim SelectedTransactions() As RenaissanceDataClass.DSTransaction.tblTransactionRow = Nothing
          Dim TransactionRow As RenaissanceDataClass.DSTransaction.tblTransactionRow = Nothing
          Dim LastFeeDate As Date = Date_FeesSince.Value
          Dim UpdateString As String = ""
          Dim PostedUpdate As Boolean = False

          TransactionDS = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblTransaction), DSTransaction)
          If (TransactionDS IsNot Nothing) Then

            ' Get Selected Orders.

            TransactionTable = TransactionDS.tblTransaction
            SelectedTransactions = TransactionTable.Select("(TransactionFund=" & FundID.ToString("###0") & ") AND ((TransactionType=" & CInt(TransactionTypes.Subscribe).ToString() & ") OR (TransactionType=" & CInt(TransactionTypes.Redeem).ToString() & ")) AND (TransactionValueDate>'" & LastFeeDate.ToString("yyyy-MM-dd") & "') AND (TransactionValueDate<='" & MilestoneDate.ToString("yyyy-MM-dd") & "')", "TransactionInstrument, TransactionValueDate")

            ' If there are any...

            If (SelectedTransactions IsNot Nothing) AndAlso (SelectedTransactions.Length > 0) Then

              ' Check....

              If MessageBox.Show("Update the " & SelectedTransactions.Length.ToString() & " relevant Subscriptions / Redemptions ?", "Update Orders", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then

                ' Update.

                For Each TransactionRow In SelectedTransactions

                  If (SavedUnitPrices.TryGetValue(TransactionRow.TransactionInstrument, UnitPrice)) Then

                    If ((Math.Abs(TransactionRow.TransactionPrice - UnitPrice) < 0.000001) OrElse (TransactionRow.TransactionTradeStatusID <> TradeStatus.CheckedEOD)) Then

                      TransactionRow.TransactionPrice = UnitPrice
                      TransactionRow.TransactionTradeStatusID = TradeStatus.CheckedEOD

                      MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblTransaction, New RenaissanceDataClass.DSTransaction.tblTransactionRow() {TransactionRow})

                      UpdateString &= TransactionRow.TransactionParentID.ToString("###0") & ","
                      PostedUpdate = True

                    End If

                  End If
                Next

                If (PostedUpdate) Then
                  MainForm.Main_RaiseEvent(New RenaissanceUpdateEventArgs(RenaissanceChangeID.tblTransaction, UpdateString))
                End If

              End If ' MessageBox.Show

            End If ' SelectedTransactions IsNot Nothing

          End If ' TransactionDS IsNot Nothing

        End If ' Check_UpdateOrdersAutomatically.Checked

      Catch ex As Exception
        MainForm.LogError(Me.Name & ", button_SetMilestone_Click()", LOG_LEVELS.Error, ex.Message, "Error updating Subscriptions.", ex.StackTrace, True)
      Finally

      End Try

      ' Save Historic Valuation data

      Dim SaveAttributionCommand As New SqlCommand

      MainForm.SetToolStripText(Label_Status, "Saving Historic Valuation data")

      Try
        SaveAttributionCommand.Connection = MainForm.GetVeniceConnection
        SaveAttributionCommand.CommandType = CommandType.StoredProcedure
        SaveAttributionCommand.CommandText = "[adp_tblHistoricValuations_Populate]"
        SaveAttributionCommand.CommandTimeout = ATTRIBUTION_SQLCOMMAND_TIMEOUT

        SaveAttributionCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SaveAttributionCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@StartDate", System.Data.SqlDbType.Date)).Value = MilestoneDate
        SaveAttributionCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EndDate", System.Data.SqlDbType.Date)).Value = MilestoneDate
        SaveAttributionCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FundID", System.Data.SqlDbType.Int)).Value = FundID
        SaveAttributionCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime)).Value = KNOWLEDGEDATE_NOW

        If (Not (SaveAttributionCommand.Connection Is Nothing)) Then
          Try
            SaveAttributionCommand.ExecuteNonQuery()
          Catch ex As Exception

            SaveAttributionCommand.Connection.Close()
            SqlConnection.ClearPool(SaveAttributionCommand.Connection)

            SaveAttributionCommand.Connection = MainForm.GetVeniceConnection
            SaveAttributionCommand.ExecuteNonQuery()
          End Try
        End If

        Call AllocationSetReturns(MainForm, FundID, AddPeriodToDate(FundPricingPeriod, MilestoneDate, IIf(FundPricingPeriod = DealingPeriod.Daily, -5, -1)), MilestoneDate, 0, Nothing)

      Catch ex As Exception
        MainForm.LogError(Me.Name & ", button_SetMilestone_Click()", LOG_LEVELS.Error, ex.Message, "Error Saving Historic Valuation.", ex.StackTrace, True)
      Finally
        If (SaveAttributionCommand.Connection IsNot Nothing) Then
          SaveAttributionCommand.Connection.Close()
          SaveAttributionCommand.Connection = Nothing
        End If
      End Try

    Catch ex As Exception
      MainForm.LogError(Me.Name & ", button_Fee_SetMilestone_Click()", LOG_LEVELS.Error, ex.Message, "Error Saving Milestone(s).", ex.StackTrace, True)
    Finally
      MainForm.SetToolStripText(Label_Status, "")

      Button_Fee_SetMilestone.Enabled = True
    End Try
  End Sub

  Private Sub BookAdministratorFXs()
    ' *******************************************************************************
    '
    '
    ' *******************************************************************************

    Dim FX_Dictionary As New Dictionary(Of String, Double)
    Dim thisRow As DataRow
    Dim Ordinal_neo_exchangerate_value As Integer
    Dim Ordinal_Instrument As Integer
    Dim Ordinal_rptCurrency As Integer
    Dim Ordinal_InstrumentTypeID As Integer
    Dim FundCurrencyID As Integer = 0
    Dim thisCurrencyID As Integer = 0
    Dim thisInstrumentTypeID As Integer = 0
    Dim FundCurrencyCode As String = ""
    Dim thisCurrencyCode As String = ""
    Dim thisFXValue As Double
    Dim AdminRateIsUnitsPerFundCurrency As Boolean = True ' I.e. for a EUR Fund, all rates are shown as amount per Euro - JPY would be 100 ish, NOk would be 8 ish, etc.
    Dim USDFXRate As Double = 0.0#
    Dim ValueDate As Date

    Try
      ' Fund Selected ?

      If (CInt(Nz(Combo_Fund.SelectedValue, 0)) = 0) Then
        Exit Sub
      End If

      ValueDate = Date_ValueDate.Value.Date

      ' Valuations table populated ?

      If (myValuationsTable.Rows.Count = 0) Then

        If (IsNumeric(Combo_Fund.SelectedValue) AndAlso IsDate(Date_ValueDate.Value)) Then

          Dim AdministratorDatesFilter As Integer = 0

          If (MainForm.GetCheckBoxChecked(Me.Check_SR_IgnoreStatus)) Then
            AdministratorDatesFilter += RenaissanceGlobals.AdministratorDatesFilter.SubscriptionRedemption_IgnoreTransactionStatus
          End If
          If (MainForm.GetCheckBoxChecked(Me.Check_SR_IgnoreTodaysTrades)) Then
            AdministratorDatesFilter += RenaissanceGlobals.AdministratorDatesFilter.SubscriptionRedemption_IgnoreTodaysTrades
          End If

          Call CalculateFundValuation(myValuationsTable, myFundValuationUnitCounts, Combo_Fund.SelectedValue, Date_ValueDate.Value, Nz(Combo_TransactionGroup.SelectedValue, ""), AdministratorDatesFilter, Check_AdministratorPriceDates.Checked)

        End If

      End If

      ' OK.

      If (myValuationsTable.Rows.Count > 0) Then
        FundCurrencyID = CInt(Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblFund, CInt(Nz(Combo_Fund.SelectedValue, 0)), "FundBaseCurrency"), 0))
        FundCurrencyCode = CStr(Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblCurrency, FundCurrencyID, "CurrencyCode"), ""))

        Ordinal_rptCurrency = myValuationsTable.Columns("rpt_Currency").Ordinal
        Ordinal_Instrument = myValuationsTable.Columns("Instrument").Ordinal
        Ordinal_neo_exchangerate_value = myValuationsTable.Columns("neo_exchangerate_value").Ordinal
        Ordinal_InstrumentTypeID = myValuationsTable.Columns("InstrumentTypeID").Ordinal

        If (Ordinal_rptCurrency < 0) OrElse (Ordinal_Instrument < 0) OrElse (Ordinal_neo_exchangerate_value < 0) Then
          Exit Sub
        End If

        For Each thisRow In myValuationsTable.Rows
          thisCurrencyCode = CStr(Nz(thisRow(Ordinal_rptCurrency), ""))
          thisFXValue = CDbl(Nz(thisRow(Ordinal_neo_exchangerate_value), 0.0#))
          thisInstrumentTypeID = CInt(Nz(thisRow(Ordinal_InstrumentTypeID), 0))

          ' Add FXRate to dictionary if it is not Zero.
          ' If a rate already exists, but the existing rate = 1.0 and the new rate does not, then assume the 1.0 value is 'default' (as is the case for FXs from BNP) and add the <> 1.0 rate.
          If (thisInstrumentTypeID <> InstrumentTypes.Cash) AndAlso (thisInstrumentTypeID <> InstrumentTypes.Fee) AndAlso (thisInstrumentTypeID <> InstrumentTypes.Expense) AndAlso (thisInstrumentTypeID <> InstrumentTypes.Unit) AndAlso (thisInstrumentTypeID <> InstrumentTypes.Expense) Then
            If (thisFXValue > 0.0#) AndAlso (thisCurrencyCode.Length > 0) Then
              If (Not FX_Dictionary.ContainsKey(thisCurrencyCode)) Then
                FX_Dictionary.Add(thisCurrencyCode, thisFXValue)
              ElseIf (FX_Dictionary(thisCurrencyCode) = 1.0#) AndAlso (thisFXValue <> 1.0#) Then
                FX_Dictionary(thisCurrencyCode) = thisFXValue
              End If
            End If
          End If
        Next

        ' OK, now we have collected the available FXs, check that we have the USD-To-Fund FX rate that is essential for calculating the other FX Crosses.

        If (FX_Dictionary.Count = 0) Then
          MessageBox.Show("No FXs found")
          Exit Sub
        End If

        If Not FX_Dictionary.ContainsKey("USD") Then
          Dim USDFXText As String

          If AdminRateIsUnitsPerFundCurrency Then
            USDFXText = InputBox("Please enter a FX Rate for " & FundCurrencyCode & " vs USD" & vbCrLf & "The number should be the Number of USD to a single " & FundCurrencyCode & vbCrLf & "Enter 0 or blank to exit.", FundCurrencyCode & " FX Rate")
          Else
            USDFXText = InputBox("Please enter a FX Rate for " & FundCurrencyCode & " vs USD" & vbCrLf & "The number should be the Number of " & FundCurrencyCode & " to a single USD" & vbCrLf & "Enter 0 or blank to exit.", FundCurrencyCode & " FX Rate")
          End If

          ' Try to parse number with local culture and then invariant 'english' culture.

          Try
            USDFXRate = Double.Parse(USDFXText, CultureInfo.CurrentCulture)
          Catch ex As Exception
            Try
              USDFXRate = Double.Parse(USDFXText, CultureInfo.InvariantCulture)
            Catch ex1 As Exception
              USDFXRate = 0.0#
            End Try
          End Try

          If (USDFXText.Length <= 0) OrElse (USDFXRate = 0.0#) Then
            Exit Sub
          End If

          FX_Dictionary.Add("USD", USDFXRate)
        End If

        ' FX to Fund Currency must be 1.

        If (Not FX_Dictionary.ContainsKey(FundCurrencyCode)) Then
          FX_Dictionary.Add(FundCurrencyCode, 1.0#)
        Else
          FX_Dictionary(FundCurrencyCode) = 1.0#
        End If

        ' OK....

        Dim FXDS As RenaissanceDataClass.DSFX
        Dim FXTable As RenaissanceDataClass.DSFX.tblFXDataTable
        Dim FXTableIsLoaded As Boolean = True
        Dim SelectedFXRows() As RenaissanceDataClass.DSFX.tblFXRow
        Dim NewFXRate As Double

        Dim NewFXRow As DSFX.tblFXRow
        Dim UpdateString As String = ""

        USDFXRate = FX_Dictionary("USD")

        ' Use the FX table, if it is loaded, else use a temporary one.

        FXDS = MainForm.MainDataHandler.Get_Dataset(RenaissanceStandardDatasets.GetStandardDataset(RenaissanceChangeID.tblFX).DatasetName)
        If (FXDS Is Nothing) Then
          FXDS = New DSFX
          FXTableIsLoaded = False
        End If
        FXTable = FXDS.tblFX

        If AdminRateIsUnitsPerFundCurrency Then
          USDFXRate = 1.0# / USDFXRate
        End If

        ' For each FX rate captured...

        For Each thisCurrencyCode In FX_Dictionary.Keys

          ' Get Venice Currency ID.

          thisCurrencyID = CInt(Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblCurrency, 0, "CurrencyID", "RN", "CurrencyCode='" & thisCurrencyCode & "'"), 0))

          ' IF OK :

          If (thisCurrencyID > 0) Then

            ' Existing FX Rate entered ?
            ' Add or Edit accordingly.

            If (FXTableIsLoaded) Then
              SelectedFXRows = FXTable.Select("(FXCurrencyCode=" & thisCurrencyID & ") AND (FXDate='" & ValueDate.ToString(QUERY_SHORTDATEFORMAT) & "')")

              If (SelectedFXRows.Length > 0) Then
                NewFXRow = SelectedFXRows(0)
              Else
                NewFXRow = FXTable.NewtblFXRow
                NewFXRow.FXID = 0
                NewFXRow.FXCurrencyCode = thisCurrencyID
                NewFXRow.FXDate = ValueDate
              End If
            Else
              NewFXRow = FXTable.NewtblFXRow
              NewFXRow.FXID = 0
              NewFXRow.FXCurrencyCode = thisCurrencyID
              NewFXRow.FXDate = ValueDate
            End If

            ' Rate for Reference currency to itself must always be 1#
            ' All Venice rates are stored vs USD.
            ' Rates are stored internally as USD per unit of other currency.

            NewFXRate = 0.0#
            If (CInt(NewFXRow.FXCurrencyCode) = FundCurrencyID) Then
              NewFXRate = 1.0# / USDFXRate
            Else
              If (AdminRateIsUnitsPerFundCurrency) Then
                NewFXRate = Math.Round(1.0# / (FX_Dictionary(thisCurrencyCode) * USDFXRate), 8)
              Else
                NewFXRate = Math.Round(FX_Dictionary(thisCurrencyCode) / USDFXRate, 8)
              End If
            End If

            ' 

            If (NewFXRow.RowState And DataRowState.Detached) Then
              NewFXRow.FXRate = NewFXRate
            Else
              If (Math.Abs(NewFXRow.FXRate - NewFXRate) > 0.000000001) Then
                NewFXRow.FXRate = NewFXRate
              End If
            End If

            ' Add new row if not an edit.

            If (NewFXRow.RowState And DataRowState.Detached) Then
              FXTable.Rows.Add(NewFXRow)
            End If

          End If ' CurrencyID OK.

        Next ' thisCurrencyCode

        ' Update FX Table.

        Try

          MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblFX, FXTable)

          If (Not FXTableIsLoaded) Then
            For Each NewFXRow In FXTable.Rows
              UpdateString &= NewFXRow.FXID.ToString() & ","
            Next
          End If

        Catch ex As Exception
          MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Saving FX Rates.", ex.StackTrace, True)
        End Try

        ' Propogate changes

        Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblFX, UpdateString), True)

      Else
        ' no Valuation table.

      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Saving FX Rates.", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub Button_AutomatedFees_SelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_AutomatedFees_SelectAll.Click
    ' *******************************************************************************
    '
    ' *******************************************************************************
    Try
      If (Grid_AutomatedFees.Rows.Count > 1) Then
        Grid_AutomatedFees.Select(1, 0, Grid_AutomatedFees.Rows.Count - 1, Grid_AutomatedFees.Cols.Count - 1)
      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Button_AutomatedFees_SelectNone_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_AutomatedFees_SelectNone.Click
    ' *******************************************************************************
    '
    ' *******************************************************************************
    Try
      Grid_AutomatedFees.Select(0, 0)
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Button_BookSelectedAutomatedFees_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_BookSelectedAutomatedFees.Click
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Dim DResult As DialogResult
    Dim Selection As C1.Win.C1FlexGrid.RowCollection
    Dim thisRow As C1.Win.C1FlexGrid.Row

    Try
      If (HasInsertPermission = False) Then
        MessageBox.Show("You do not have permissions to insert Transactions.", "Permission Denied", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Exit Sub
      End If
    Catch ex As Exception
    End Try

    Try
      Selection = Grid_AutomatedFees.Rows.Selected

      If (Selection.Count >= 2) OrElse ((Selection.Count = 1) AndAlso (Selection(0).SafeIndex > 0)) Then
        DResult = MessageBox.Show("Commit the " & Selection.Count.ToString & " selected fee trade" & IIf(Selection.Count.ToString > 1, "s", "") & " ?", "Commit Selected Fee Transactions", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2)
      Else
        DResult = MessageBox.Show("There are no Fee Transactions selected.", "Commit Fee Transactions", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Exit Sub
      End If


      If (DResult = Windows.Forms.DialogResult.OK) AndAlso (Selection IsNot Nothing) AndAlso (Selection.Count > 0) Then

        Dim UpdateString As String = ""
        Dim NewTransactionID As String

        Dim thisFeeID As Integer
        Dim thisAmount As Double
        Dim thisFundID As Integer
        Dim thisSubFundID As Integer
        Dim thisInstrumentID As Integer
        Dim thisShareClassID As Integer
        Dim thisValueDate As Date
        Dim thisSettlementDate As Date
        Dim thisComment As String
        Dim thisExistingParentID As Integer

        For Each thisRow In Selection
          If (thisRow.SafeIndex > 0) Then

            ' OK, Onwards.....

            thisFeeID = CInt(Nz(thisRow("FeeID"), 0))
            thisAmount = CDbl(Nz(thisRow("TransactionAmount"), 0))
            thisFundID = CInt(Nz(thisRow("TransactionFundID"), 0))
            thisSubFundID = CInt(Nz(thisRow("TransactionSubFundID"), 0))
            thisInstrumentID = CInt(Nz(thisRow("TransactionInstrumentID"), 0))
            thisShareClassID = CInt(Nz(thisRow("TransactionSpecificShareClassID"), 0))
            thisValueDate = CDate(Nz(thisRow("TransactionValueDate"), Renaissance_BaseDate))
            thisSettlementDate = CDate(Nz(thisRow("TransactionSettlementDate"), Renaissance_BaseDate))
            thisComment = CStr(Nz(thisRow("FeeDescription"), "")) & vbCrLf & CStr(Nz(thisRow("FeeName"), ""))
            thisExistingParentID = CInt(Nz(thisRow("ExistingParentID"), 0))

            If (thisFundID = 0) AndAlso (IsNumeric(Combo_Fund.SelectedValue)) Then
              thisFundID = CInt(Combo_Fund.SelectedValue)
              thisAmount = 0.0# ' If there is no FundID, then this must be an Un-Matched transaction, so clear the amount.
            End If
            If (thisInstrumentID = 0) Then
              thisInstrumentID = CInt(Nz(thisRow("ExistingInstrumentID"), 0))
            End If
            If (thisValueDate <= Renaissance_BaseDate) Then
              thisValueDate = CDate(Nz(thisRow("ExistingValueDate"), Renaissance_BaseDate))
            End If
            If (thisSettlementDate <= Renaissance_BaseDate) Then
              thisSettlementDate = thisValueDate
            End If
            If (thisAmount = 0.0#) Then
              thisComment = "Cancelled Auto-Fee" & vbCrLf & thisComment
            End If

            If (thisFundID <> 0) AndAlso (thisInstrumentID <> 0) AndAlso ((Math.Abs(thisAmount) > 0.1) Or (thisExistingParentID > 0)) Then
              NewTransactionID = BookFees(thisExistingParentID, thisFeeID, thisFundID, thisSubFundID, thisInstrumentID, thisAmount, thisValueDate, thisSettlementDate, True, True, False, False, False, thisShareClassID, thisComment)

              If (NewTransactionID.Length > 0) Then
                UpdateString &= NewTransactionID & ","
              End If
            End If

          End If
        Next

        If (UpdateString.Length > 0) Then
          MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceStandardDatasets.tblTransaction.ChangeID, UpdateString), True)
        End If

        Grid_AutomatedFees.Select(0, 0)
      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error submitting fee trades.", ex.Message, ex.StackTrace, True)

    End Try

  End Sub

  Private Sub Grid_AutomatedFees_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Grid_AutomatedFees.DoubleClick

  End Sub

  Private Sub Grid_AutomatedFees_MouseEnterCell(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles Grid_AutomatedFees.MouseEnterCell
    ' **********************************************************************************
    ' Record cell just entered and re-arrange the context menu according to the type
    ' of Grid Row.
    ' **********************************************************************************

    Try
      LastEnteredAutomatedFeesGridRow = e.Row
      LastEnteredAutomatedFeesGridCol = e.Col

      Dim enableViewFee As Boolean = False
      Dim enableViewTransactions As Boolean = False

      If (e.Row <= 0) Then
        Grid_AutomatedFees.ContextMenuStrip = Nothing
      Else
        Dim thisRow As C1.Win.C1FlexGrid.Row = Grid_AutomatedFees.Rows(LastEnteredAutomatedFeesGridRow)

        If (Grid_AutomatedFees.ContextMenuStrip Is Nothing) Then
          Grid_AutomatedFees.ContextMenuStrip = Me.ContextMenu_AutomatedFees
        End If

        If CStr(Nz(thisRow("FeeName"), "")).Length > 0 Then
          Menu_AutomatedFees_Description.Text = thisRow("FeeName")
        Else
          Menu_AutomatedFees_Description.Text = thisRow("ExistingTicket")
        End If

        If CInt(Nz(thisRow("FeeID"), 0)) > 0 Then
          enableViewFee = True
        End If
        If CInt(Nz(thisRow("ExistingParentID"), 0)) > 0 Then
          enableViewTransactions = True
        End If

      End If

      ' Show / Hide menu options.

      Menu_AutomatedFees_ShowFeeDefenition.Visible = enableViewFee
      Menu_AutomatedFees_ShowTransaction.Visible = enableViewTransactions

    Catch ex As Exception
    End Try
  End Sub

  Private Sub Menu_AutomatedFees_ShowTransaction_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_AutomatedFees_ShowTransaction.Click
    ' **********************************************************************************
    ' 
    '
    ' **********************************************************************************

    Try
      If (LastEnteredAutomatedFeesGridRow > 0) And (LastEnteredAutomatedFeesGridRow < Grid_AutomatedFees.Rows.Count) Then
        Dim ThisTransactionID As Integer = 0

        ThisTransactionID = CInt(Grid_AutomatedFees.GetData(LastEnteredAutomatedFeesGridRow, Grid_AutomatedFees.Cols("ExistingParentID").SafeIndex))

        Dim thisFormHandle As RenaissanceGlobals.RenaissanceFormHandle

        thisFormHandle = MainForm.New_VeniceForm(VeniceFormID.frmTransaction)
        CType(thisFormHandle.Form, frmTransaction).MoveToAuditID(ThisTransactionID)

      End If

    Catch ex As Exception
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error opening Transactions Form.", ex.StackTrace, True)
    End Try
  End Sub

  Private Sub Menu_AutomatedFees_ShowFeeDefenition_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_AutomatedFees_ShowFeeDefenition.Click
    ' **********************************************************************************
    ' 
    '
    ' **********************************************************************************

    Try
      If (LastEnteredAutomatedFeesGridRow > 0) And (LastEnteredAutomatedFeesGridRow < Grid_AutomatedFees.Rows.Count) Then
        Dim ThisFeeID As Integer = 0

        ThisFeeID = CInt(Grid_AutomatedFees.GetData(LastEnteredAutomatedFeesGridRow, Grid_AutomatedFees.Cols("FeeID").SafeIndex))

        Dim thisFormHandle As RenaissanceGlobals.RenaissanceFormHandle

        thisFormHandle = MainForm.New_VeniceForm(VeniceFormID.frmFees)
        CType(thisFormHandle.Form, frmFees).MoveToAuditID(ThisFeeID)

      End If

    Catch ex As Exception
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error opening Fees Form.", ex.StackTrace, True)
    End Try
  End Sub
End Class
