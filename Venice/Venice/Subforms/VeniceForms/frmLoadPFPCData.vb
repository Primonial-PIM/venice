' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmLoadPFPCData.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Option Compare Text

Imports System.Data.SqlClient
Imports System.IO
Imports System.Windows.Forms
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals

''' <summary>
''' Class frmLoadPFPCData
''' </summary>
Public Class frmLoadPFPCData

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmLoadPFPCData"/> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The BTN load data
    ''' </summary>
  Friend WithEvents btnLoadData As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN close
    ''' </summary>
  Friend WithEvents btnClose As System.Windows.Forms.Button
    ''' <summary>
    ''' The label2
    ''' </summary>
  Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ fund
    ''' </summary>
  Friend WithEvents Combo_Fund As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The date_ value date
    ''' </summary>
  Friend WithEvents Date_ValueDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The label_ trade date
    ''' </summary>
  Friend WithEvents Label_TradeDate As System.Windows.Forms.Label
    ''' <summary>
    ''' The open file dialog1
    ''' </summary>
  Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    ''' <summary>
    ''' The LBL person user name
    ''' </summary>
  Friend WithEvents lblPersonUserName As System.Windows.Forms.Label
    ''' <summary>
    ''' The label1
    ''' </summary>
  Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label3
    ''' </summary>
  Friend WithEvents Label3 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label4
    ''' </summary>
  Friend WithEvents Label4 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label5
    ''' </summary>
  Friend WithEvents Label5 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label6
    ''' </summary>
  Friend WithEvents Label6 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label7
    ''' </summary>
  Friend WithEvents Label7 As System.Windows.Forms.Label
    ''' <summary>
    ''' The BTN_ choose portfolio
    ''' </summary>
  Friend WithEvents btn_ChoosePortfolio As System.Windows.Forms.Button
    ''' <summary>
    ''' The edit_ portfolio report
    ''' </summary>
  Friend WithEvents edit_PortfolioReport As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The edit_ expenses report
    ''' </summary>
  Friend WithEvents edit_ExpensesReport As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The edit_ transaction report
    ''' </summary>
  Friend WithEvents edit_TransactionReport As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The edit_ custody record
    ''' </summary>
  Friend WithEvents edit_CustodyRecord As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The edit_ share allocation record
    ''' </summary>
  Friend WithEvents edit_ShareAllocationRecord As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The combo_ expenses worksheet
    ''' </summary>
  Friend WithEvents Combo_ExpensesWorksheet As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ share allocation worksheet
    ''' </summary>
  Friend WithEvents Combo_ShareAllocationWorksheet As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The BTN_ choose expenses report
    ''' </summary>
  Friend WithEvents btn_ChooseExpensesReport As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN_ choose transaction report
    ''' </summary>
  Friend WithEvents btn_ChooseTransactionReport As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN_ choose custody record
    ''' </summary>
  Friend WithEvents btn_ChooseCustodyRecord As System.Windows.Forms.Button
    ''' <summary>
    ''' The form_ status strip
    ''' </summary>
  Friend WithEvents Form_StatusStrip As System.Windows.Forms.StatusStrip
    ''' <summary>
    ''' The form_ progress bar
    ''' </summary>
  Friend WithEvents Form_ProgressBar As System.Windows.Forms.ToolStripProgressBar
    ''' <summary>
    ''' The label_ status
    ''' </summary>
  Friend WithEvents Label_Status As System.Windows.Forms.ToolStripStatusLabel
    ''' <summary>
    ''' The BTN_ choose share allocation record
    ''' </summary>
  Friend WithEvents btn_ChooseShareAllocationRecord As System.Windows.Forms.Button
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.btnLoadData = New System.Windows.Forms.Button
    Me.btnClose = New System.Windows.Forms.Button
    Me.Label2 = New System.Windows.Forms.Label
    Me.Combo_Fund = New System.Windows.Forms.ComboBox
    Me.Date_ValueDate = New System.Windows.Forms.DateTimePicker
    Me.Label_TradeDate = New System.Windows.Forms.Label
    Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
    Me.edit_PortfolioReport = New System.Windows.Forms.TextBox
    Me.lblPersonUserName = New System.Windows.Forms.Label
    Me.edit_ExpensesReport = New System.Windows.Forms.TextBox
    Me.Label1 = New System.Windows.Forms.Label
    Me.Label3 = New System.Windows.Forms.Label
    Me.edit_TransactionReport = New System.Windows.Forms.TextBox
    Me.Label4 = New System.Windows.Forms.Label
    Me.edit_CustodyRecord = New System.Windows.Forms.TextBox
    Me.Label5 = New System.Windows.Forms.Label
    Me.edit_ShareAllocationRecord = New System.Windows.Forms.TextBox
    Me.Label6 = New System.Windows.Forms.Label
    Me.Label7 = New System.Windows.Forms.Label
    Me.btn_ChoosePortfolio = New System.Windows.Forms.Button
    Me.btn_ChooseExpensesReport = New System.Windows.Forms.Button
    Me.btn_ChooseTransactionReport = New System.Windows.Forms.Button
    Me.btn_ChooseCustodyRecord = New System.Windows.Forms.Button
    Me.btn_ChooseShareAllocationRecord = New System.Windows.Forms.Button
    Me.Combo_ExpensesWorksheet = New System.Windows.Forms.ComboBox
    Me.Combo_ShareAllocationWorksheet = New System.Windows.Forms.ComboBox
    Me.Form_StatusStrip = New System.Windows.Forms.StatusStrip
    Me.Form_ProgressBar = New System.Windows.Forms.ToolStripProgressBar
    Me.Label_Status = New System.Windows.Forms.ToolStripStatusLabel
    Me.Form_StatusStrip.SuspendLayout()
    Me.SuspendLayout()
    '
    'btnLoadData
    '
    Me.btnLoadData.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnLoadData.Location = New System.Drawing.Point(112, 308)
    Me.btnLoadData.Name = "btnLoadData"
    Me.btnLoadData.Size = New System.Drawing.Size(148, 28)
    Me.btnLoadData.TabIndex = 2
    Me.btnLoadData.Text = "Load Data"
    '
    'btnClose
    '
    Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnClose.Location = New System.Drawing.Point(328, 308)
    Me.btnClose.Name = "btnClose"
    Me.btnClose.Size = New System.Drawing.Size(75, 28)
    Me.btnClose.TabIndex = 3
    Me.btnClose.Text = "&Close"
    '
    'Label2
    '
    Me.Label2.Location = New System.Drawing.Point(12, 16)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(120, 20)
    Me.Label2.TabIndex = 53
    Me.Label2.Text = "Fund Name"
    '
    'Combo_Fund
    '
    Me.Combo_Fund.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Fund.Location = New System.Drawing.Point(140, 12)
    Me.Combo_Fund.Name = "Combo_Fund"
    Me.Combo_Fund.Size = New System.Drawing.Size(364, 21)
    Me.Combo_Fund.TabIndex = 0
    '
    'Date_ValueDate
    '
    Me.Date_ValueDate.Location = New System.Drawing.Point(140, 44)
    Me.Date_ValueDate.Name = "Date_ValueDate"
    Me.Date_ValueDate.Size = New System.Drawing.Size(160, 20)
    Me.Date_ValueDate.TabIndex = 1
    '
    'Label_TradeDate
    '
    Me.Label_TradeDate.Location = New System.Drawing.Point(12, 48)
    Me.Label_TradeDate.Name = "Label_TradeDate"
    Me.Label_TradeDate.Size = New System.Drawing.Size(100, 16)
    Me.Label_TradeDate.TabIndex = 102
    Me.Label_TradeDate.Text = "Valuation Date"
    '
    'edit_PortfolioReport
    '
    Me.edit_PortfolioReport.Location = New System.Drawing.Point(140, 76)
    Me.edit_PortfolioReport.Name = "edit_PortfolioReport"
    Me.edit_PortfolioReport.Size = New System.Drawing.Size(344, 20)
    Me.edit_PortfolioReport.TabIndex = 103
    '
    'lblPersonUserName
    '
    Me.lblPersonUserName.Location = New System.Drawing.Point(12, 80)
    Me.lblPersonUserName.Name = "lblPersonUserName"
    Me.lblPersonUserName.Size = New System.Drawing.Size(100, 20)
    Me.lblPersonUserName.TabIndex = 104
    Me.lblPersonUserName.Text = "Portfolio Report"
    '
    'edit_ExpensesReport
    '
    Me.edit_ExpensesReport.Location = New System.Drawing.Point(140, 108)
    Me.edit_ExpensesReport.Name = "edit_ExpensesReport"
    Me.edit_ExpensesReport.Size = New System.Drawing.Size(344, 20)
    Me.edit_ExpensesReport.TabIndex = 105
    '
    'Label1
    '
    Me.Label1.Location = New System.Drawing.Point(12, 112)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(100, 20)
    Me.Label1.TabIndex = 106
    Me.Label1.Text = "Expenses Report"
    '
    'Label3
    '
    Me.Label3.Location = New System.Drawing.Point(140, 144)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(136, 20)
    Me.Label3.TabIndex = 108
    Me.Label3.Text = "Expenses Worksheet"
    '
    'edit_TransactionReport
    '
    Me.edit_TransactionReport.Enabled = False
    Me.edit_TransactionReport.Location = New System.Drawing.Point(140, 172)
    Me.edit_TransactionReport.Name = "edit_TransactionReport"
    Me.edit_TransactionReport.Size = New System.Drawing.Size(344, 20)
    Me.edit_TransactionReport.TabIndex = 109
    '
    'Label4
    '
    Me.Label4.Location = New System.Drawing.Point(12, 176)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(100, 20)
    Me.Label4.TabIndex = 110
    Me.Label4.Text = "Transaction Report"
    '
    'edit_CustodyRecord
    '
    Me.edit_CustodyRecord.Enabled = False
    Me.edit_CustodyRecord.Location = New System.Drawing.Point(140, 204)
    Me.edit_CustodyRecord.Name = "edit_CustodyRecord"
    Me.edit_CustodyRecord.Size = New System.Drawing.Size(344, 20)
    Me.edit_CustodyRecord.TabIndex = 111
    '
    'Label5
    '
    Me.Label5.Location = New System.Drawing.Point(12, 208)
    Me.Label5.Name = "Label5"
    Me.Label5.Size = New System.Drawing.Size(100, 20)
    Me.Label5.TabIndex = 112
    Me.Label5.Text = "Custody Record"
    '
    'edit_ShareAllocationRecord
    '
    Me.edit_ShareAllocationRecord.Location = New System.Drawing.Point(140, 236)
    Me.edit_ShareAllocationRecord.Name = "edit_ShareAllocationRecord"
    Me.edit_ShareAllocationRecord.Size = New System.Drawing.Size(344, 20)
    Me.edit_ShareAllocationRecord.TabIndex = 113
    '
    'Label6
    '
    Me.Label6.Location = New System.Drawing.Point(12, 240)
    Me.Label6.Name = "Label6"
    Me.Label6.Size = New System.Drawing.Size(128, 20)
    Me.Label6.TabIndex = 114
    Me.Label6.Text = "Share Allocation Record"
    '
    'Label7
    '
    Me.Label7.Location = New System.Drawing.Point(140, 272)
    Me.Label7.Name = "Label7"
    Me.Label7.Size = New System.Drawing.Size(148, 20)
    Me.Label7.TabIndex = 116
    Me.Label7.Text = "Share Allocation Worksheet"
    '
    'btn_ChoosePortfolio
    '
    Me.btn_ChoosePortfolio.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btn_ChoosePortfolio.Location = New System.Drawing.Point(484, 76)
    Me.btn_ChoosePortfolio.Name = "btn_ChoosePortfolio"
    Me.btn_ChoosePortfolio.Size = New System.Drawing.Size(20, 20)
    Me.btn_ChoosePortfolio.TabIndex = 117
    Me.btn_ChoosePortfolio.Text = "..."
    '
    'btn_ChooseExpensesReport
    '
    Me.btn_ChooseExpensesReport.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btn_ChooseExpensesReport.Location = New System.Drawing.Point(484, 108)
    Me.btn_ChooseExpensesReport.Name = "btn_ChooseExpensesReport"
    Me.btn_ChooseExpensesReport.Size = New System.Drawing.Size(20, 20)
    Me.btn_ChooseExpensesReport.TabIndex = 118
    Me.btn_ChooseExpensesReport.Text = "..."
    '
    'btn_ChooseTransactionReport
    '
    Me.btn_ChooseTransactionReport.Enabled = False
    Me.btn_ChooseTransactionReport.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btn_ChooseTransactionReport.Location = New System.Drawing.Point(484, 172)
    Me.btn_ChooseTransactionReport.Name = "btn_ChooseTransactionReport"
    Me.btn_ChooseTransactionReport.Size = New System.Drawing.Size(20, 20)
    Me.btn_ChooseTransactionReport.TabIndex = 119
    Me.btn_ChooseTransactionReport.Text = "..."
    '
    'btn_ChooseCustodyRecord
    '
    Me.btn_ChooseCustodyRecord.Enabled = False
    Me.btn_ChooseCustodyRecord.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btn_ChooseCustodyRecord.Location = New System.Drawing.Point(484, 204)
    Me.btn_ChooseCustodyRecord.Name = "btn_ChooseCustodyRecord"
    Me.btn_ChooseCustodyRecord.Size = New System.Drawing.Size(20, 20)
    Me.btn_ChooseCustodyRecord.TabIndex = 120
    Me.btn_ChooseCustodyRecord.Text = "..."
    '
    'btn_ChooseShareAllocationRecord
    '
    Me.btn_ChooseShareAllocationRecord.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btn_ChooseShareAllocationRecord.Location = New System.Drawing.Point(484, 236)
    Me.btn_ChooseShareAllocationRecord.Name = "btn_ChooseShareAllocationRecord"
    Me.btn_ChooseShareAllocationRecord.Size = New System.Drawing.Size(20, 20)
    Me.btn_ChooseShareAllocationRecord.TabIndex = 121
    Me.btn_ChooseShareAllocationRecord.Text = "..."
    '
    'Combo_ExpensesWorksheet
    '
    Me.Combo_ExpensesWorksheet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_ExpensesWorksheet.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_ExpensesWorksheet.Location = New System.Drawing.Point(288, 140)
    Me.Combo_ExpensesWorksheet.Name = "Combo_ExpensesWorksheet"
    Me.Combo_ExpensesWorksheet.Size = New System.Drawing.Size(216, 21)
    Me.Combo_ExpensesWorksheet.TabIndex = 122
    '
    'Combo_ShareAllocationWorksheet
    '
    Me.Combo_ShareAllocationWorksheet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_ShareAllocationWorksheet.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_ShareAllocationWorksheet.Location = New System.Drawing.Point(288, 268)
    Me.Combo_ShareAllocationWorksheet.Name = "Combo_ShareAllocationWorksheet"
    Me.Combo_ShareAllocationWorksheet.Size = New System.Drawing.Size(216, 21)
    Me.Combo_ShareAllocationWorksheet.TabIndex = 123
    '
    'Form_StatusStrip
    '
    Me.Form_StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Form_ProgressBar, Me.Label_Status})
    Me.Form_StatusStrip.Location = New System.Drawing.Point(0, 345)
    Me.Form_StatusStrip.Name = "Form_StatusStrip"
    Me.Form_StatusStrip.Size = New System.Drawing.Size(514, 23)
    Me.Form_StatusStrip.TabIndex = 124
    Me.Form_StatusStrip.Text = " "
    '
    'Form_ProgressBar
    '
    Me.Form_ProgressBar.Maximum = 20
    Me.Form_ProgressBar.Name = "Form_ProgressBar"
    Me.Form_ProgressBar.Size = New System.Drawing.Size(150, 17)
    Me.Form_ProgressBar.Step = 1
    Me.Form_ProgressBar.Visible = False
    '
    'Label_Status
    '
    Me.Label_Status.Name = "Label_Status"
    Me.Label_Status.Size = New System.Drawing.Size(10, 18)
    Me.Label_Status.Text = " "
    '
    'frmLoadPFPCData
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(514, 368)
    Me.Controls.Add(Me.Form_StatusStrip)
    Me.Controls.Add(Me.Combo_ShareAllocationWorksheet)
    Me.Controls.Add(Me.Combo_ExpensesWorksheet)
    Me.Controls.Add(Me.btn_ChooseShareAllocationRecord)
    Me.Controls.Add(Me.btn_ChooseCustodyRecord)
    Me.Controls.Add(Me.btn_ChooseTransactionReport)
    Me.Controls.Add(Me.btn_ChooseExpensesReport)
    Me.Controls.Add(Me.btn_ChoosePortfolio)
    Me.Controls.Add(Me.Label7)
    Me.Controls.Add(Me.edit_ShareAllocationRecord)
    Me.Controls.Add(Me.edit_CustodyRecord)
    Me.Controls.Add(Me.edit_TransactionReport)
    Me.Controls.Add(Me.edit_ExpensesReport)
    Me.Controls.Add(Me.edit_PortfolioReport)
    Me.Controls.Add(Me.Label6)
    Me.Controls.Add(Me.Label5)
    Me.Controls.Add(Me.Label4)
    Me.Controls.Add(Me.Label3)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.lblPersonUserName)
    Me.Controls.Add(Me.Date_ValueDate)
    Me.Controls.Add(Me.Label_TradeDate)
    Me.Controls.Add(Me.Combo_Fund)
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.btnClose)
    Me.Controls.Add(Me.btnLoadData)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frmLoadPFPCData"
    Me.Text = "Load PFPC Data"
    Me.Form_StatusStrip.ResumeLayout(False)
    Me.Form_StatusStrip.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "


  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
  Private WithEvents _MainForm As VeniceMain

  ' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
  Private FormTooltip As New ToolTip()

  ' Form Constants, specific to the table being updated.

    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
  Private ALWAYS_CLOSE_THIS_FORM As Boolean = True

  ' The standard ChangeID for this form. e.g. tblPerson
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

  ' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
  Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
  Private THIS_FORM_FormID As VeniceFormID

  ' Form Status Flags

    ''' <summary>
    ''' The in paint
    ''' </summary>
  Private InPaint As Boolean
    ''' <summary>
    ''' The form is valid
    ''' </summary>
  Private FormIsValid As Boolean
    ''' <summary>
    ''' The form changed
    ''' </summary>
  Private FormChanged As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
  Private _FormOpenFailed As Boolean

  ' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
  Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
  Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
  Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
  Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
  Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
    ' Public property to return handle to the 'Main' Venice form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
  Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
    ' Public property maintaining a value indicating if the cursor is over the 'Cancel'
    ' Button on this form.
    ' This property is specifically designed for use by the field formating Event functions
    ' In order that they do not impose format restrictions if the user is about to click the 
    ' 'Cancel' button.
    '
    Get
      Return False
    End Get
    Set(ByVal Value As Boolean)
    End Set
  End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
  Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
    Get
      Return False
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
    Get
      Return True
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmLoadPFPCData"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  Public Sub New(ByVal pMainForm As VeniceMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()

    _MainForm = pMainForm
    AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

    _FormOpenFailed = False

    ' ******************************************************
    ' Form Specific Settings :
    ' ******************************************************

    ' Form Permissioning :-

    THIS_FORM_PermissionArea = Me.Name
    THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

    ' 'This' form ID

    THIS_FORM_FormID = VeniceFormID.frmLoadPFPCData

    ' Format Event Handlers for form controls

    AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

    AddHandler Combo_Fund.KeyUp, AddressOf MainForm.ComboSelectAsYouType

    ' Set up the ToolTip
    MainForm.SetFormToolTip(Me, FormTooltip)

    ' ******************************************************
    ' End Form Specific.
    ' ******************************************************

    THIS_FORM_ChangeID = RenaissanceChangeID.KnowledgeDate

  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
  Public Sub ResetForm() Implements StandardVeniceForm.ResetForm

    Call Form_Load(Me, New System.EventArgs)
  End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
  Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
    ALWAYS_CLOSE_THIS_FORM = True
    Me.Close()
  End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
    _FormOpenFailed = False

    If Not (MainForm Is Nothing) Then
      FormIsValid = True
    Else
      MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If


    ' Check User permissions
    Call CheckPermissions()
    If (HasReadPermission = False) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    ' Build Combos

    Try
      Me.Date_ValueDate.Value = Now.Date.AddDays(0 - Now.Date.Day)

      Call SetFundCombo()

      MainForm.SetComboSelectionLengths(Me)

    Catch ex As Exception
    End Try

  End Sub

    ''' <summary>
    ''' Handles the Closing event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
  Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    Dim HideForm As Boolean

    ' Hide or Close this form ?
    ' All depends on how many of this form type are Open or in Cache...


    If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
      HideForm = False
    Else
      HideForm = True
      If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.SingleCache_CacheCount Then
        HideForm = False
      End If
    End If

    If HideForm = True Then
      MainForm.HideInFormsCollection(Me)
      Me.Hide() ' NPP Fix

      e.Cancel = True
    Else
      Try
        MainForm.RemoveFromFormsCollection(Me)
        RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

        RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

        RemoveHandler Combo_Fund.KeyUp, AddressOf MainForm.ComboSelectAsYouType

      Catch ex As Exception
      End Try
    End If

  End Sub


#End Region

  ' Routine to handle changes / updates to tables by this and other windows.
  ' If this, or any other, form posts a change to a table, then it will invoke an update event 
  ' detailing what tables have been altered.
  ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
  ' the 'VeniceAutoUpdate' event of the main Venice form.
  ' Each form may them react as appropriate to changes in any table that might impact it.
  '
    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    Dim KnowledgeDateChanged As Boolean
    Dim SetButtonStatus_Flag As Boolean

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

    KnowledgeDateChanged = False
    SetButtonStatus_Flag = False

    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
      KnowledgeDateChanged = True
    End If

    ' ****************************************************************
    ' Check for changes relevant to this form
    ' ****************************************************************

    ' Changes to the KnowledgeDate :-
    If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
      SetButtonStatus_Flag = True
    End If

    ' Changes to the tblFund table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFund) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Call SetFundCombo()
    End If


    ' Changes to the tblUserPermissions table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

      ' Check ongoing permissions.

      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

        FormIsValid = False
        Me.Close()
        Exit Sub
      End If

      SetButtonStatus_Flag = True

    End If

    ' ****************************************************************
    '
    ' ****************************************************************


  End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

  ' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
  Private Sub CheckPermissions()
    ' *****************************************************************************
    '
    ' *****************************************************************************

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

  End Sub


#End Region

#Region " Set Form Combos (Form Specific Code) "

    ''' <summary>
    ''' Sets the fund combo.
    ''' </summary>
  Private Sub SetFundCombo()

    Dim OrgInpaint As Boolean

    OrgInpaint = InPaint
    InPaint = True

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_Fund, _
    RenaissanceStandardDatasets.tblFund, _
    "FundName", _
    "FundID", _
    "FundClosed=0", False, True, True)  ' 

    InPaint = OrgInpaint

  End Sub



#End Region

#Region " PFPC Data Reading Routines"

    ''' <summary>
    ''' Reads the PFPC fund valuation worksheet.
    ''' </summary>
    ''' <param name="Permission_Area">The permission_ area.</param>
    ''' <param name="pWorkbookName">Name of the p workbook.</param>
    ''' <param name="p_FundID">The p_ fund ID.</param>
    ''' <param name="p_ReportDate">The p_ report date.</param>
  Sub ReadPFPCFundValuationWorksheet(ByVal Permission_Area As String, ByVal pWorkbookName As String, ByVal p_FundID As Long, ByVal p_ReportDate As Date)
    ' ********************************************************************************************
    ' Purpose:  To Import portfolio data from the given Excel spreadsheet.
    '
    ' Accepts     : Permission_Area - Used for Error Reporting
    '             : pWorkbookName   - Excel workbook file to read from.
    '             : p_FundID        - Fund ID to save information under
    '             : p_ReportDate    - Value Date to save data under.
    '
    ' This Routine Expects there to be a headings line containing several known titles, These title
    ' do not have to be in any particular order, nor must they be on the first line - However, they must all exist
    ' and multiple occurences to the 'SECURITY ID' label may cause problems.
    ' This routine should ignore blank lines.
    ' ********************************************************************************************

    Dim ExcelApp As New Excel.Application
    Dim wsValuation As Excel.Worksheet = Nothing
    Dim wbSpreadsheet As Excel.Workbook = Nothing
    Dim Temp_Range As Excel.Range

    Dim Heading_Row As Long
    Dim Last_Row As Long

    Dim Row_Count As Long
    Dim Col_Count As Long

    Dim InstrumentName_Col As Integer
    Dim InstrumentID_Col As Integer
    Dim InstrumentCurrency_Col As Integer
    Dim Amount_Col As Integer
    Dim LocalCcyPrice_Col As Integer
    Dim FXRate_Col As Integer
    Dim LocalCcyValue_Col As Integer
    Dim BookCcyValue_Col As Integer

    Dim InstrumentName_Value As String
    Dim InstrumentID_Value As String
    Dim InstrumentCurrency_Value As String
    Dim Amount_Value As String
    Dim LocalCcyPrice_Value As String
    Dim FXRate_Value As String
    Dim LocalCcyValue_Value As String
    Dim BookCcyValue_Value As String

    ' Clear Table
    Dim thisAdaptor As SqlDataAdapter
		Dim Tbl_PFPCPortfolioValuation As RenaissanceDataClass.DSPFPCPortfolioValuation.tblPFPCPortfolioValuationDataTable
		Dim newValuatioRow As RenaissanceDataClass.DSPFPCPortfolioValuation.tblPFPCPortfolioValuationRow
		Dim ThisStandardDataset As StandardDataset
		ThisStandardDataset = RenaissanceStandardDatasets.tblPFPCPortfolioValuation

		' Get Adaptor
		thisAdaptor = MainForm.MainDataHandler.Get_Adaptor(ThisStandardDataset.Adaptorname, VENICE_CONNECTION, ThisStandardDataset.TableName)
		If (thisAdaptor Is Nothing) Then
			MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Failed to initialise tblPFPCPortfolioValuation Adaptor.", "", True)
			Exit Sub
		End If

		' Delete existing Records
		' ****************************************************************
		'
		' Clear existing data for this Fund and Value Date combination
		' Delete existing Records
		'
		' ****************************************************************

		Dim DeleteCommand As New SqlCommand
		Try
			DeleteCommand.CommandType = CommandType.Text
			DeleteCommand.CommandText = "DELETE FROM tblPFPCPortfolioValuation WHERE (FundID=@FundID) AND (ValuationDate=@ValuationDate)"
			DeleteCommand.Parameters.Add("@FundID", SqlDbType.Int).Value = p_FundID
			DeleteCommand.Parameters.Add("@ValuationDate", SqlDbType.DateTime).Value = p_ReportDate
			DeleteCommand.Connection = MainForm.GetVeniceConnection()

			SyncLock DeleteCommand.Connection
				DeleteCommand.ExecuteNonQuery()
			End SyncLock

		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Deleting Old Portfolio Valuation records.", ex.StackTrace, True)
		Finally

			If (DeleteCommand.Connection IsNot Nothing) Then
				Try
					DeleteCommand.Connection.Close()
				Catch ex As Exception
				Finally
					DeleteCommand.Connection = Nothing
				End Try
			End If

		End Try

		Tbl_PFPCPortfolioValuation = New RenaissanceDataClass.DSPFPCPortfolioValuation.tblPFPCPortfolioValuationDataTable

		' Open Workbook


		wbSpreadsheet = ExcelApp.Workbooks.Open(pWorkbookName, False, True, , , , True)
		wsValuation = wbSpreadsheet.Worksheets(1)

		' Find Start Row
		Temp_Range = wsValuation.Range("A1:BA500").Find("SECURITY ID", , Excel.XlFindLookIn.xlValues)
		If (Temp_Range Is Nothing) Then
			Call MainForm.LogError(Me.Name & ", ReadPFPCFundValuationWorksheet()", LOG_LEVELS.Warning, "", "Heading Description (`SECURITY ID`) not found in Portfolio file.", "", True)

			GoTo ReadPFPCFundValuationWorksheet_Exit
		End If

		Heading_Row = Temp_Range.Row
		Last_Row = wbSpreadsheet.Application.ActiveCell.SpecialCells(Excel.XlCellType.xlCellTypeLastCell).Row
		Temp_Range = Nothing

		' Collect Column Numbers for each Column Required.
		Dim ColumnNames() As String = New String() {"SECURITY ID", "SECURITY NAME", "CURRENCY", "CURRENCY FX RATE", "QUANTITY", "MARKET PRICE", "MARKET VALUE LOCAL", "MARKET VALUE BOOK"}


		Col_Count = 1
		While (Col_Count < 256)

			Select Case UCase(wsValuation.Cells(Heading_Row, Col_Count).Text.ToString.Trim)

				Case "SECURITY ID"
					InstrumentID_Col = Col_Count
					Try
						ColumnNames(0) = ""
					Catch ex As Exception
					End Try

				Case "SECURITY NAME"
					InstrumentName_Col = Col_Count
					Try
						ColumnNames(1) = ""
					Catch ex As Exception
					End Try

				Case "CURRENCY"
					InstrumentCurrency_Col = Col_Count
					Try
						ColumnNames(2) = ""
					Catch ex As Exception
					End Try

				Case "CURRENCY FX RATE"
					FXRate_Col = Col_Count
					Try
						ColumnNames(3) = ""
					Catch ex As Exception
					End Try

				Case "QUANTITY"
					Amount_Col = Col_Count
					Try
						ColumnNames(4) = ""
					Catch ex As Exception
					End Try

				Case "MARKET PRICE"
					LocalCcyPrice_Col = Col_Count
					Try
						ColumnNames(5) = ""
					Catch ex As Exception
					End Try

				Case "MARKET VALUE LOCAL"
					LocalCcyValue_Col = Col_Count
					Try
						ColumnNames(6) = ""
					Catch ex As Exception
					End Try

				Case "MARKET VALUE BOOK"
					BookCcyValue_Col = Col_Count
					Try
						ColumnNames(7) = ""
					Catch ex As Exception
					End Try

			End Select

			Col_Count = Col_Count + 1
		End While

		' Error if a column has not been found
		If (InstrumentName_Col = 0) Or _
			 (InstrumentID_Col = 0) Or _
			 (InstrumentCurrency_Col = 0) Or _
			 (Amount_Col = 0) Or _
			 (LocalCcyPrice_Col = 0) Or _
			 (FXRate_Col = 0) Or _
			 (LocalCcyValue_Col = 0) Or _
			 (BookCcyValue_Col = 0) Then
			' Problem

			Dim MissingColumnNames As String
			Dim MissingColumnCounter As Integer
			MissingColumnNames = "<Not Known>"

			' Resolve missing column name(s)
			If (Not (ColumnNames Is Nothing)) AndAlso (ColumnNames.Length > 0) Then
				MissingColumnNames = ""

				Try
					For MissingColumnCounter = 0 To (ColumnNames.Length - 1)
						If ColumnNames(MissingColumnCounter).Length > 0 Then
							If (MissingColumnNames.Length > 0) Then
								MissingColumnNames &= ", "
							End If
							MissingColumnNames &= "`" & ColumnNames(MissingColumnCounter) & "`"
						End If
					Next
				Catch ex As Exception
				End Try

				If MissingColumnNames.Length <= 0 Then
					MissingColumnNames = "<Not Known>"
				End If
			End If

			Call MainForm.LogError(Me.Name & ", ReadPFPCFundValuationWorksheet()", LOG_LEVELS.Warning, "", "There appear to be some columns missing from the Portfolio Report Excel workbook. Aborting operation." & vbCrLf & "Columns : " & MissingColumnNames, "", True)

			GoTo ReadPFPCFundValuationWorksheet_Exit
		End If

		' OK, Work through the sheet, saving values.

		Row_Count = (Heading_Row + 1)
		While (Row_Count <= Last_Row)
			If (Len(wsValuation.Cells(Row_Count, InstrumentCurrency_Col).Text) > 0) Then

				InstrumentName_Value = Trim(wsValuation.Cells(Row_Count, InstrumentName_Col).Text)
				InstrumentID_Value = Trim(wsValuation.Cells(Row_Count, InstrumentID_Col).Text)

				If IsNumeric(InstrumentID_Value) Then
					If InStr(InstrumentID_Value, ".") = 0 Then
						If (Len(InstrumentID_Value) < 7) Then
							InstrumentID_Value = "0000000" & InstrumentID_Value
							InstrumentID_Value = InstrumentID_Value.Substring(InstrumentID_Value.Length - 7)
						End If
					End If
				End If

				' Fudge
				If (Len(InstrumentID_Value) = 3) And (UCase(InstrumentName_Value).EndsWith("RECEIVABLE")) Then
					InstrumentID_Value = InstrumentID_Value & "_Receivable"
				End If

				InstrumentCurrency_Value = Trim(wsValuation.Cells(Row_Count, InstrumentCurrency_Col).Text)
				Amount_Value = ParseNumber(wsValuation.Cells(Row_Count, Amount_Col).Text.ToString.Trim)
				If IsNumeric(Amount_Value) = False Then Amount_Value = 0

				LocalCcyPrice_Value = ParseNumber(Trim(wsValuation.Cells(Row_Count, LocalCcyPrice_Col).Text))
				If IsNumeric(LocalCcyPrice_Value) = False Then LocalCcyPrice_Value = 0

				FXRate_Value = ParseNumber(Trim(wsValuation.Cells(Row_Count, FXRate_Col).Text))
				If IsNumeric(FXRate_Value) = False Then FXRate_Value = 0

				LocalCcyValue_Value = ParseNumber(Trim(wsValuation.Cells(Row_Count, LocalCcyValue_Col).Text))
				If IsNumeric(LocalCcyValue_Value) = False Then LocalCcyValue_Value = 0

				BookCcyValue_Value = ParseNumber(Trim(wsValuation.Cells(Row_Count, BookCcyValue_Col).Text))
				If IsNumeric(BookCcyValue_Value) = False Then BookCcyValue_Value = 0



				If IsNumeric(Amount_Value) And _
					 IsNumeric(LocalCcyPrice_Value) And _
					 IsNumeric(FXRate_Value) And _
					 IsNumeric(LocalCcyValue_Value) And _
					 IsNumeric(BookCcyValue_Value) Then

					Try
						newValuatioRow = Tbl_PFPCPortfolioValuation.NewRow
						newValuatioRow.ValuationDate = p_ReportDate
						newValuatioRow.FundID = p_FundID
						newValuatioRow.FundName = ""
						newValuatioRow.FundCurrency = ""
						newValuatioRow.InstrumentID = InstrumentID_Value
						newValuatioRow.InstrumentDescription = InstrumentName_Value
						newValuatioRow.InstrumentCurrency = InstrumentCurrency_Value
						newValuatioRow.Amount = CDbl(Amount_Value)
						newValuatioRow.LocalCcyPrice = CDbl(LocalCcyPrice_Value)
						newValuatioRow.FxRate = CDbl(FXRate_Value)
						newValuatioRow.LocalCcyValue = CDbl(LocalCcyValue_Value)
						newValuatioRow.BookCcyValue = CDbl(BookCcyValue_Value)

						Tbl_PFPCPortfolioValuation.Rows.Add(newValuatioRow)

					Catch ex As Exception
						Call MainForm.LogError(Me.Name & ", ReadPFPCFundValuationWorksheet()", LOG_LEVELS.Error, ex.Message, "Error saving record to tblPFPCPortfolioValuation.", ex.StackTrace, True)
						GoTo ReadPFPCFundValuationWorksheet_Exit
					End Try
				Else
				End If
			End If

			Row_Count = Row_Count + 1
		End While

ReadPFPCFundValuationWorksheet_Exit:

		' Save Values.

		If (Tbl_PFPCPortfolioValuation IsNot Nothing) AndAlso (Tbl_PFPCPortfolioValuation.Rows.Count > 0) Then
			MainForm.AdaptorUpdate(Me.Name, thisAdaptor, Tbl_PFPCPortfolioValuation)
			MainForm.Main_RaiseEvent_Background(New RenaissanceUpdateEventArgs(ThisStandardDataset.ChangeID), True)
		End If

		Try
			Tbl_PFPCPortfolioValuation.Rows.Clear()
		Catch ex As Exception
		Finally
			Tbl_PFPCPortfolioValuation = Nothing
		End Try

		' Tidy Up.

		wsValuation = Nothing
		Try
			If (Not (wbSpreadsheet Is Nothing)) Then wbSpreadsheet.Close(False)
		Catch ex As Exception
		Finally
			wbSpreadsheet = Nothing
		End Try

		Try
			If (Not (ExcelApp Is Nothing)) Then ExcelApp.Quit()
		Catch ex As Exception
		Finally
			ExcelApp = Nothing
		End Try


	End Sub

    ''' <summary>
    ''' Reads the PFPC expenses worksheet.
    ''' </summary>
    ''' <param name="Permission_Area">The permission_ area.</param>
    ''' <param name="pWorkbookName">Name of the p workbook.</param>
    ''' <param name="pWorksheetName">Name of the p worksheet.</param>
    ''' <param name="p_FundID">The p_ fund ID.</param>
    ''' <param name="p_ReportDate">The p_ report date.</param>
	Sub ReadPFPCExpensesWorksheet(ByVal Permission_Area As String, ByVal pWorkbookName As String, ByVal pWorksheetName As String, ByVal p_FundID As Long, ByVal p_ReportDate As Date)
		' ********************************************************************************************
		' Purpose:  To Import Expenses data from the given Excel spreadsheet.
		'
		' Accepts     : Permission_Area - Used for Error Reporting
		'             : pWorkbookName   - Excel workbook file to read from.
		'             : p_FundID        - Fund ID to save information under
		'             : p_ReportDate    - Value Date to save data under.
		'
		' This Routine Expects there to be a headings line containing several known titles, These title
		' do not have to be in any particular order, nor must they be on the first line - However, they must all exist.
		' This routine is somewhat sensitive to changes in the spreadsheet format.
		' ********************************************************************************************

		Dim ExcelApp As Excel.Application
		Dim wsValuation As Excel.Worksheet = Nothing
		Dim wbSpreadsheet As Excel.Workbook = Nothing
		Dim Temp_Range As Excel.Range
		Dim Temp_Column As Excel.Range

		Dim Totals_Row As Long

		Dim Row_Count As Long

		Const ManagementFee_PFPCID As String = "MgmtFeeExpense"
		Const AuditFee_PFPCID As String = "Audit Fee Expense"
		Const LegalFee_PFPCID As String = "Legal Fee Expense"
		Const ListingFee_PFPCID As String = "Listing Fee Expense"
		Const DirectorsFee_PFPCID As String = "Directors Fee Expense"
		Const OOP_PFPCID As String = "OOP Expense"
		Const OrganisationFee_PFPCID As String = "Organisational fees"	'  Establishment Costs
		Const Misc_PFPCID As String = "Miscellaneous Expense"
		Const CustodianAssetFee_PFPCID As String = "asset based fee"
		Const CustodianAssetFee2_PFPCID As String = "Accrued Custody Fee"
		Const CustodianTransactionFee_PFPCID As String = "Custody Transaction chg"
		Const AdminFee_PFPCID As String = "Accrued Admin Fee"
		Const BankCharges_PFPCID As String = "Bank Charges"
		Const PerformanceFee_PFPCID As String = "PerformanceFee"
		Const ExpensesPaid_PFPCID As String = "Expenses Paid"
		Const DirectorsFees_PFPCID As String = "Liability Insurance"

		Dim FundCurrency As String


		'SQL_InsertHeader = "INSERT INTO tblPFPCPortfolioValuation(ValuationDate, FundID, FundName, FundCurrency, " & _
		'                       "InstrumentID, InstrumentDescription, InstrumentCurrency, Amount, LocalCcyPrice, " & _
		'                       "FxRate, LocalCcyValue, BookCcyValue) "

		' Retrieve Fund Currency
		FundCurrency = "USD"
		Try
			Dim DS_Fund As RenaissanceDataClass.DSFund
			Dim Tbl_Fund As RenaissanceDataClass.DSFund.tblFundDataTable
			Dim SelectedFundRows As RenaissanceDataClass.DSFund.tblFundRow() = Nothing

			Dim DS_Instruments As RenaissanceDataClass.DSInstrument
			Dim Tbl_Instruments As RenaissanceDataClass.DSInstrument.tblInstrumentDataTable
			Dim SelectedInstrumentRows As RenaissanceDataClass.DSInstrument.tblInstrumentRow()

			Dim FundCurrencyID As Integer

			FundCurrencyID = -1

			DS_Fund = MainForm.Load_Table(RenaissanceStandardDatasets.tblFund, False)
			If Not (DS_Fund Is Nothing) Then
				Tbl_Fund = DS_Fund.tblFund

				SelectedFundRows = Tbl_Fund.Select("FundID=" & p_FundID.ToString)
			End If
			If (Not (SelectedFundRows Is Nothing)) AndAlso (SelectedFundRows.Length > 0) Then
				FundCurrencyID = SelectedFundRows(0).FundBaseCurrency
			End If

			If (FundCurrencyID >= 0) Then
				DS_Instruments = MainForm.Load_Table(RenaissanceStandardDatasets.tblInstrument, False)
				Tbl_Instruments = Nothing
				SelectedInstrumentRows = Nothing

				If Not (DS_Instruments Is Nothing) Then
					Tbl_Instruments = DS_Instruments.tblInstrument
					SelectedInstrumentRows = Tbl_Instruments.Select("InstrumentID=" & FundCurrencyID.ToString)
				End If

				If (Not (SelectedInstrumentRows Is Nothing)) AndAlso (SelectedInstrumentRows.Length > 0) Then
					FundCurrency = SelectedInstrumentRows(0).InstrumentCode
				End If
			End If

		Catch ex As Exception
		End Try

		' Gather Instrument IDs
		' Clear Table
		Dim thisAdaptor As SqlDataAdapter
		Dim DS_tblPFPCPortfolioValuation As RenaissanceDataClass.DSPFPCPortfolioValuation
		Dim Tbl_PFPCPortfolioValuation As RenaissanceDataClass.DSPFPCPortfolioValuation.tblPFPCPortfolioValuationDataTable
		Dim thisValuatioRow As RenaissanceDataClass.DSPFPCPortfolioValuation.tblPFPCPortfolioValuationRow
		Dim SelectedRows As RenaissanceDataClass.DSPFPCPortfolioValuation.tblPFPCPortfolioValuationRow()
		Dim ThisStandardDataset As StandardDataset
		ThisStandardDataset = RenaissanceStandardDatasets.tblPFPCPortfolioValuation

		' Get Adaptor
		thisAdaptor = MainForm.MainDataHandler.Get_Adaptor(ThisStandardDataset.Adaptorname, VENICE_CONNECTION, ThisStandardDataset.TableName)
		If (thisAdaptor Is Nothing) Then
			MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Failed to initialise tblPFPCPortfolioValuation Adaptor.", "", True)
			Exit Sub
		End If

		' Delete existing Records

		DS_tblPFPCPortfolioValuation = MainForm.Load_Table(ThisStandardDataset, False)
		If (DS_tblPFPCPortfolioValuation Is Nothing) Then
			MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Failed to get tblPFPCPortfolioValuation table.", "", True)
			Exit Sub
		End If
		Tbl_PFPCPortfolioValuation = DS_tblPFPCPortfolioValuation.tblPFPCPortfolioValuation
		SelectedRows = Tbl_PFPCPortfolioValuation.Select("(FundID=" & p_FundID & ") AND (ValuationDate='" & p_ReportDate.ToString(QUERY_SHORTDATEFORMAT) & "')")

		Try
			SyncLock Tbl_PFPCPortfolioValuation
				For Row_Count = 0 To (SelectedRows.Length - 1)
					thisValuatioRow = SelectedRows(Row_Count)

					Select Case thisValuatioRow.InstrumentID
						Case ManagementFee_PFPCID, AuditFee_PFPCID, LegalFee_PFPCID, ListingFee_PFPCID, DirectorsFee_PFPCID, OOP_PFPCID, OrganisationFee_PFPCID, Misc_PFPCID, CustodianAssetFee_PFPCID, CustodianAssetFee2_PFPCID, CustodianTransactionFee_PFPCID, PerformanceFee_PFPCID, BankCharges_PFPCID, ExpensesPaid_PFPCID, DirectorsFees_PFPCID, "ADMIN FEE"
							thisValuatioRow.Delete()
					End Select
				Next
				thisAdaptor.Update(SelectedRows)
			End SyncLock

		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error deleting old entries in tblPFPCPortfolioValuation table.", ex.StackTrace, True)
			Exit Sub
		End Try



		' Open Spreadsheet
		ExcelApp = New Excel.Application
		Try
			wbSpreadsheet = ExcelApp.Workbooks.Open(pWorkbookName, False, True, , , , True)
			wsValuation = wbSpreadsheet.Worksheets(Nz(pWorksheetName, ""))
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error opening Worksheet `" & pWorksheetName & "`.", ex.StackTrace, True)
			GoTo ReadPFPCExpensesWorksheet_Exit
		End Try

		' Establish the Totals Row.
		Temp_Range = wsValuation.Range("A1:BA500").Find(ManagementFee_PFPCID, , Excel.XlFindLookIn.xlValues, Excel.XlLookAt.xlWhole)
		If (Temp_Range Is Nothing) Then
			MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Heading Description (`" & ManagementFee_PFPCID & "`) not found in Expenses file.", "", True)
			GoTo ReadPFPCExpensesWorksheet_Exit
		End If

		Temp_Column = wsValuation.Columns(Temp_Range.Column)
		Temp_Range = Nothing
		Temp_Range = Temp_Column.Find("=SUM(", , Excel.XlFindLookIn.xlFormulas, Excel.XlLookAt.xlPart)
		If (Temp_Range Is Nothing) Then
			MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Problem locating the Totals Row in the Expenses sheet.", "", True)
			GoTo ReadPFPCExpensesWorksheet_Exit
		End If
		Totals_Row = Temp_Range.Row

		' ManagementFee_PFPCID
		If Save_PFPCExpense(Permission_Area, wsValuation, Totals_Row, 0, p_FundID, FundCurrency, p_ReportDate, ManagementFee_PFPCID, ManagementFee_PFPCID, Tbl_PFPCPortfolioValuation) = False Then
			MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Heading Description (`" & ManagementFee_PFPCID & "`) not found in Expenses file.", "", True)
		End If

		' AuditFee_PFPCID
		If Save_PFPCExpense(Permission_Area, wsValuation, Totals_Row, 0, p_FundID, FundCurrency, p_ReportDate, AuditFee_PFPCID, AuditFee_PFPCID, Tbl_PFPCPortfolioValuation) = False Then
			MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Heading Description (`" & AuditFee_PFPCID & "`) not found in Expenses file.", "", True)
		End If

		' LegalFee_PFPCID
		If Save_PFPCExpense(Permission_Area, wsValuation, Totals_Row, 0, p_FundID, FundCurrency, p_ReportDate, LegalFee_PFPCID, LegalFee_PFPCID, Tbl_PFPCPortfolioValuation) = False Then
			MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Heading Description (`" & LegalFee_PFPCID & "`) not found in Expenses file.", "", True)
		End If

		' ListingFee_PFPCID
		If Save_PFPCExpense(Permission_Area, wsValuation, Totals_Row, 0, p_FundID, FundCurrency, p_ReportDate, ListingFee_PFPCID, ListingFee_PFPCID, Tbl_PFPCPortfolioValuation) = False Then
			MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Heading Description (`" & ListingFee_PFPCID & "`) not found in Expenses file.", "", True)
		End If

		' DirectorsFee_PFPCID
		If Save_PFPCExpense(Permission_Area, wsValuation, Totals_Row, 0, p_FundID, FundCurrency, p_ReportDate, DirectorsFee_PFPCID, DirectorsFee_PFPCID, Tbl_PFPCPortfolioValuation) = False Then
			MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Heading Description (`" & DirectorsFee_PFPCID & "`) not found in Expenses file.", "", True)
		End If

		' OOP_PFPCID
		If Save_PFPCExpense(Permission_Area, wsValuation, Totals_Row, 0, p_FundID, FundCurrency, p_ReportDate, OOP_PFPCID, OOP_PFPCID, Tbl_PFPCPortfolioValuation) = False Then
			MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Heading Description (`" & OOP_PFPCID & "`) not found in Expenses file.", "", True)
		End If

		' OrganisationFee_PFPCID
		If Save_PFPCExpense(Permission_Area, wsValuation, Totals_Row, 0, p_FundID, FundCurrency, p_ReportDate, OrganisationFee_PFPCID, OrganisationFee_PFPCID, Tbl_PFPCPortfolioValuation) = False Then
			MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Heading Description (`" & OrganisationFee_PFPCID & "`) not found in Expenses file.", "", True)
		End If

		' Misc_PFPCID
		If Save_PFPCExpense(Permission_Area, wsValuation, Totals_Row, 0, p_FundID, FundCurrency, p_ReportDate, Misc_PFPCID, Misc_PFPCID, Tbl_PFPCPortfolioValuation) = False Then
			MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Heading Description (`" & Misc_PFPCID & "`) not found in Expenses file.", "", True)
		End If

		' Bank Charges
		If Save_PFPCExpense(Permission_Area, wsValuation, Totals_Row, 0, p_FundID, FundCurrency, p_ReportDate, BankCharges_PFPCID, BankCharges_PFPCID, Tbl_PFPCPortfolioValuation) = False Then
			MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Heading Description (`" & BankCharges_PFPCID & "`) not found in Expenses file.", "", True)
		End If

		' PerformanceFee_PFPCID
		If Save_PFPCExpense(Permission_Area, wsValuation, Totals_Row, 1, p_FundID, FundCurrency, p_ReportDate, BankCharges_PFPCID, PerformanceFee_PFPCID, Tbl_PFPCPortfolioValuation) = False Then
			MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Heading Description (`" & BankCharges_PFPCID & "`) not found in Expenses file.", "", True)
		End If

		' CustodianAssetFee_PFPCID
		If Save_PFPCExpense(Permission_Area, wsValuation, Totals_Row, 1, p_FundID, FundCurrency, p_ReportDate, CustodianAssetFee_PFPCID, CustodianAssetFee_PFPCID, Tbl_PFPCPortfolioValuation) = False Then
			If Save_PFPCExpense(Permission_Area, wsValuation, Totals_Row, 0, p_FundID, FundCurrency, p_ReportDate, CustodianAssetFee2_PFPCID, CustodianAssetFee_PFPCID, Tbl_PFPCPortfolioValuation) = False Then
				MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Heading Description (`" & CustodianAssetFee_PFPCID & "`) not found in Expenses file.", "", True)
			End If
		End If

		' CustodianTransactionFee_PFPCID
		If Save_PFPCExpense(Permission_Area, wsValuation, Totals_Row, 0, p_FundID, FundCurrency, p_ReportDate, CustodianTransactionFee_PFPCID, CustodianTransactionFee_PFPCID, Tbl_PFPCPortfolioValuation) = False Then
			MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Heading Description (`" & CustodianTransactionFee_PFPCID & "`) not found in Expenses file.", "", True)
		End If

		' AdminFee_PFPCID
		If Save_PFPCExpense(Permission_Area, wsValuation, Totals_Row, 1, p_FundID, FundCurrency, p_ReportDate, AdminFee_PFPCID, "ADMIN FEE", Tbl_PFPCPortfolioValuation) = False Then
			MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Heading Description (`" & AdminFee_PFPCID & "`) not found in Expenses file.", "", True)
		End If

		' ExpensesPaid_PFPCID
		If Save_PFPCExpense(Permission_Area, wsValuation, Totals_Row, 0, p_FundID, FundCurrency, p_ReportDate, ExpensesPaid_PFPCID, ExpensesPaid_PFPCID, Tbl_PFPCPortfolioValuation, 1) = False Then
			MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Heading Description (`" & ExpensesPaid_PFPCID & "`) not found in Expenses file.", "", True)
		End If

		' DirectorsFees_PFPCID
		If Save_PFPCExpense(Permission_Area, wsValuation, Totals_Row, 0, p_FundID, FundCurrency, p_ReportDate, DirectorsFees_PFPCID, DirectorsFees_PFPCID, Tbl_PFPCPortfolioValuation, 1) = False Then
			MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Heading Description (`" & DirectorsFees_PFPCID & "`) not found in Expenses file.", "", True)
		End If

		Try
			SyncLock Tbl_PFPCPortfolioValuation
				thisAdaptor.Update(Tbl_PFPCPortfolioValuation)
			End SyncLock
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error updating tblPFPCPortfolioValuation table.", ex.StackTrace, True)
			Exit Sub
		End Try



ReadPFPCExpensesWorksheet_Exit:

		' Tidy Up.

		Try
			If (Not (wbSpreadsheet Is Nothing)) Then wbSpreadsheet.Close()
		Catch ex As Exception
		End Try
		Temp_Range = Nothing
		wsValuation = Nothing
		wbSpreadsheet = Nothing
		Try
			If (Not (ExcelApp Is Nothing)) Then ExcelApp.Quit()
		Catch ex As Exception
		End Try
		ExcelApp = Nothing

	End Sub


    ''' <summary>
    ''' Reads the PFPC share allocation worksheet.
    ''' </summary>
    ''' <param name="Permission_Area">The permission_ area.</param>
    ''' <param name="pWorkbookName">Name of the p workbook.</param>
    ''' <param name="pWorksheetName">Name of the p worksheet.</param>
    ''' <param name="p_FundID">The p_ fund ID.</param>
    ''' <param name="p_ReportDate">The p_ report date.</param>
	Sub ReadPFPCShareAllocationWorksheet(ByVal Permission_Area As String, ByVal pWorkbookName As String, ByVal pWorksheetName As String, ByVal p_FundID As Long, ByVal p_ReportDate As Date)
		' ********************************************************************************************
		' Purpose:  To Import portfolio data from the given Excel spreadsheet.
		'
		' Accepts     : Permission_Area - Used for Error Reporting
		'             : pWorkbookName   - Excel workbook file to read from.
		'             : p_FundID        - Fund ID to save information under
		'             : p_ReportDate    - Value Date to save data under.
		'
		' ********************************************************************************************

		Dim ExcelApp As Excel.Application = Nothing
		Dim wsAllocation As Excel.Worksheet = Nothing
		Dim wbSpreadsheet As Excel.Workbook = Nothing
		Dim Temp_Range As Excel.Range

		Dim Row_Count As Long
		Dim Heading_Row As Long
		Dim Last_Row As Long

		Dim Col_TradeDate As Integer
		Dim Col_AccountID As Integer
		Dim Col_TransactionType As Integer
		Dim Col_IssuedShares As Integer
		Dim Col_AmountInvested As Integer

		Dim Value_TradeDate As Date
		Dim Value_AccountID As String
		Dim Value_TransactionType As String
		Dim Value_IssuedShares As Double
		Dim Value_AmountInvested As Double

		' Clear Table
		Dim thisAdaptor As SqlDataAdapter
		Dim DS_tblPFPCShareAllocation As RenaissanceDataClass.DSPFPCShareAllocation
		Dim Tbl_PFPCShareAllocation As RenaissanceDataClass.DSPFPCShareAllocation.tblPFPCShareAllocationDataTable
		Dim thisValuatioRow As RenaissanceDataClass.DSPFPCShareAllocation.tblPFPCShareAllocationRow
		Dim newValuatioRow As RenaissanceDataClass.DSPFPCShareAllocation.tblPFPCShareAllocationRow
		Dim SelectedRows As RenaissanceDataClass.DSPFPCShareAllocation.tblPFPCShareAllocationRow()
		Dim ThisStandardDataset As StandardDataset

		ThisStandardDataset = RenaissanceStandardDatasets.tblPFPCShareAllocation

		' Get Adaptor
		thisAdaptor = MainForm.MainDataHandler.Get_Adaptor(ThisStandardDataset.Adaptorname, VENICE_CONNECTION, ThisStandardDataset.TableName)
		If (thisAdaptor Is Nothing) Then
			MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Failed to initialise tblPFPCShareAllocation Adaptor.", "", True)
			Exit Sub
		End If

		' Delete existing Records

		DS_tblPFPCShareAllocation = MainForm.Load_Table(ThisStandardDataset, False)
		If (DS_tblPFPCShareAllocation Is Nothing) Then
			MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Failed to get tblPFPCShareAllocation table.", "", True)
			Exit Sub
		End If
		Tbl_PFPCShareAllocation = DS_tblPFPCShareAllocation.tblPFPCShareAllocation
		SelectedRows = Tbl_PFPCShareAllocation.Select("(FundID=" & p_FundID & ") AND (ValuationDate='" & p_ReportDate.ToString(QUERY_SHORTDATEFORMAT) & "')")

		Try
			If (Not (SelectedRows Is Nothing)) AndAlso (SelectedRows.Length > 0) Then
				SyncLock Tbl_PFPCShareAllocation
					For Row_Count = 0 To (SelectedRows.Length - 1)
						thisValuatioRow = SelectedRows(Row_Count)
						thisValuatioRow.Delete()
					Next
					thisAdaptor.Update(SelectedRows)
				End SyncLock
			End If

		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error deleting old entries in tblPFPCShareAllocation table.", ex.StackTrace, True)
			Exit Sub
		End Try


		Call MainForm.Set_SystemString("PFPCIssuedShares_" & p_FundID & "_" & p_ReportDate.ToString(QUERY_SHORTDATEFORMAT), "0")

		Try
			ExcelApp = New Excel.Application
			wbSpreadsheet = ExcelApp.Workbooks.Open(pWorkbookName, False, True, , , , True)
			wsAllocation = wbSpreadsheet.Worksheets(pWorksheetName)
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error initialising Excel in ReadPFPCShareAllocationWorksheet().", ex.StackTrace, True)
			GoTo ReadPFPCShareAllocationWorksheet_Exit
		End Try

		wsAllocation.Activate()
		Last_Row = wbSpreadsheet.Application.ActiveCell.SpecialCells(Excel.XlCellType.xlCellTypeLastCell).Row

		' *************************************
		' Find Data columns and the Start Row
		' *************************************

		Try
			' Trade Date
			Temp_Range = wsAllocation.Range("A1:BA500").Find("Trade date", , Excel.XlFindLookIn.xlValues)
			If (Temp_Range Is Nothing) Then
				MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Trade date heading (`Trade date`) not found in Share Allocation file.", "", True)
				GoTo ReadPFPCShareAllocationWorksheet_Exit
			End If

			Heading_Row = Temp_Range.Row
			Col_TradeDate = Temp_Range.Column

			' Account ID
			Temp_Range = wsAllocation.Range("A1:BA500").Find("Account ID", , Excel.XlFindLookIn.xlValues)
			If (Temp_Range Is Nothing) Then
				MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Account ID heading (`Account ID`) not found in Share Allocation file.", "", True)
				GoTo ReadPFPCShareAllocationWorksheet_Exit
			End If

			Col_AccountID = Temp_Range.Column

			' Transaction Type
			Temp_Range = wsAllocation.Range("A1:BA500").Find("Trans", , Excel.XlFindLookIn.xlValues)
			If (Temp_Range Is Nothing) Then
				MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Transaction Type heading (`Trans`) not found in Share Allocation file.", "", True)
				GoTo ReadPFPCShareAllocationWorksheet_Exit
			End If

			Col_TransactionType = Temp_Range.Column

			' Issued Shares
			Temp_Range = wsAllocation.Range("A1:BA500").Find("Issued Shares", , Excel.XlFindLookIn.xlValues)
			If (Temp_Range Is Nothing) Then
				MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Issued Shares heading (`Issued Shares`) not found in Share Allocation file.", "", True)
				GoTo ReadPFPCShareAllocationWorksheet_Exit
			End If

			Col_IssuedShares = Temp_Range.Column

			' Issued Shares
			Temp_Range = wsAllocation.Range("A1:BA500").Find("Due / Received", , Excel.XlFindLookIn.xlValues)
			If (Temp_Range Is Nothing) Then
				MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Amount Invested / Redeemed (`Due / Received`) not found in Share Allocation file.", "", True)
				GoTo ReadPFPCShareAllocationWorksheet_Exit
			End If

			Col_AmountInvested = Temp_Range.Column

		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error resolving Column headings in ReadPFPCShareAllocationWorksheet().", ex.StackTrace, True)
			GoTo ReadPFPCShareAllocationWorksheet_Exit
		End Try

		If (Col_TradeDate = 0) Or _
			 (Col_AccountID = 0) Or _
			 (Col_TransactionType = 0) Or _
			 (Col_IssuedShares = 0) Or _
			 (Col_AmountInvested = 0) Then
			' Problem

			MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "One of the required columns was not found in Share Allocation file.", "", True)
			GoTo ReadPFPCShareAllocationWorksheet_Exit
		End If

		' OK, Work through the sheet, saving values.

		Row_Count = (Heading_Row + 1)
		While (Row_Count <= Last_Row)
			Dim CellText As String

			CellText = wsAllocation.Cells(Row_Count, Col_TradeDate).Text.ToString

			If IsDate(Nz(CellText, "")) Then
				Value_TradeDate = Renaissance_BaseDate
				Value_AccountID = ""
				Value_TransactionType = ""
				Value_IssuedShares = 0
				Value_AmountInvested = 0

				Try

					Value_TradeDate = CDate(Nz(wsAllocation.Cells(Row_Count, Col_TradeDate).Text.ToString, ""))

					Value_AccountID = Nz(wsAllocation.Cells(Row_Count, Col_AccountID).Text.ToString, "")

					Value_TransactionType = Nz(wsAllocation.Cells(Row_Count, Col_TransactionType).Text.ToString, "")

					If IsNumeric(Nz(wsAllocation.Cells(Row_Count, Col_IssuedShares).Text.ToString, "")) Then
						Value_IssuedShares = CDbl(Nz(wsAllocation.Cells(Row_Count, Col_IssuedShares).Text.ToString, ""))
					End If

					If IsNumeric(Nz(wsAllocation.Cells(Row_Count, Col_AmountInvested).Text.ToString, "")) Then
						Value_AmountInvested = CDbl(Nz(wsAllocation.Cells(Row_Count, Col_AmountInvested).Text.ToString, ""))
					End If

					' Save Value
					newValuatioRow = Tbl_PFPCShareAllocation.NewtblPFPCShareAllocationRow
					newValuatioRow.ValuationDate = p_ReportDate
					newValuatioRow.FundID = p_FundID
					newValuatioRow.TradeDate = Value_TradeDate
					newValuatioRow.AccountID = Value_AccountID
					newValuatioRow.TransactionType = Value_TransactionType
					newValuatioRow.IssuedShares = Value_IssuedShares
					newValuatioRow.CashInvested = Value_AmountInvested

					SyncLock Tbl_PFPCShareAllocation
						Tbl_PFPCShareAllocation.Rows.Add(newValuatioRow)
						thisAdaptor.Update(New DataRow() {newValuatioRow})
					End SyncLock
				Catch ex As Exception
					MainForm.LogError(Me.Name, LOG_LEVELS.Warning, ex.Message, "Error Saving value to the tblPFPCShareAllocation table.", ex.StackTrace, True)
					GoTo ReadPFPCShareAllocationWorksheet_Exit
				End Try

			Else
				' Check for Total Shares
				CellText = wsAllocation.Cells(Row_Count, Col_IssuedShares).Text.ToString
				If CStr(Nz(CellText, "")) = "Total Shares" Then

					CellText = wsAllocation.Cells(Row_Count + 1, Col_IssuedShares).Text.ToString
					If IsNumeric(Nz(CellText, "")) Then
						Value_IssuedShares = CDbl(Nz(CellText, ""))

						Call MainForm.Set_SystemString("PFPCIssuedShares_" & p_FundID & "_" & p_ReportDate.ToString(QUERY_SHORTDATEFORMAT), Value_IssuedShares.ToString)
					End If

				End If
			End If

			Row_Count = Row_Count + 1
		End While


ReadPFPCShareAllocationWorksheet_Exit:

		Try
			wsAllocation = Nothing
			If (Not (wbSpreadsheet Is Nothing)) Then wbSpreadsheet.Close(False)
		Catch ex As Exception
		End Try

		Try
			If (Not (ExcelApp Is Nothing)) Then ExcelApp.Quit()
		Catch ex As Exception
		End Try

		ExcelApp = Nothing
		wbSpreadsheet = Nothing
		ExcelApp = Nothing

	End Sub


    ''' <summary>
    ''' Save_s the PFPC expense.
    ''' </summary>
    ''' <param name="Permission_Area">The permission_ area.</param>
    ''' <param name="wsValuation">The ws valuation.</param>
    ''' <param name="Totals_Row">The totals_ row.</param>
    ''' <param name="p_ColumnModifier">The p_ column modifier.</param>
    ''' <param name="p_FundID">The p_ fund ID.</param>
    ''' <param name="FundCurrency">The fund currency.</param>
    ''' <param name="p_ReportDate">The p_ report date.</param>
    ''' <param name="Temp_PFPCID">The temp_ PFPCID.</param>
    ''' <param name="PFPC_ID">The PFP c_ ID.</param>
    ''' <param name="pPFPCValuationTable">The p PFPC valuation table.</param>
    ''' <param name="pLocalPrice">The p local price.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	<CLSCompliant(False)> Function Save_PFPCExpense(ByVal Permission_Area As String, ByVal wsValuation As Excel.Worksheet, ByVal Totals_Row As Long, ByVal p_ColumnModifier As Long, ByVal p_FundID As Long, ByVal FundCurrency As String, ByVal p_ReportDate As Date, ByVal Temp_PFPCID As String, ByVal PFPC_ID As String, ByRef pPFPCValuationTable As RenaissanceDataClass.DSPFPCPortfolioValuation.tblPFPCPortfolioValuationDataTable, Optional ByVal pLocalPrice As Double = (-1)) As Boolean
		Dim Temp_Range As Excel.Range
		Dim temp_value As Object
		Dim LocalPrice As Double
		Dim newValuationRow As RenaissanceDataClass.DSPFPCPortfolioValuation.tblPFPCPortfolioValuationRow

		LocalPrice = pLocalPrice

		Try

			Temp_Range = wsValuation.Range("A1:CA500").Find(Temp_PFPCID, , Excel.XlFindLookIn.xlValues, Excel.XlLookAt.xlWhole)
			If (Temp_Range Is Nothing) Then
				Save_PFPCExpense = False
				Exit Function
			End If
			temp_value = wsValuation.Cells(Totals_Row, Temp_Range.Column + p_ColumnModifier).Value
			If (Not (temp_value Is Nothing)) AndAlso IsNumeric(temp_value) Then
				temp_value = CDbl(temp_value)
			Else
				temp_value = 0
			End If

		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error getting value for " & PFPC_ID, ex.StackTrace, True)
			Return False
		End Try

		'SQL_InsertHeader = "INSERT INTO tblPFPCPortfolioValuation(ValuationDate, FundID, FundName, FundCurrency, " & _
		'                   "InstrumentID, InstrumentDescription, InstrumentCurrency, Amount, LocalCcyPrice, " & _
		'                   "FxRate, LocalCcyValue, BookCcyValue) "

		Try
			newValuationRow = pPFPCValuationTable.NewtblPFPCPortfolioValuationRow
			newValuationRow.ValuationDate = p_ReportDate
			newValuationRow.FundID = p_FundID
			newValuationRow.FundName = ""
			newValuationRow.FundCurrency = ""
			newValuationRow.InstrumentID = PFPC_ID
			newValuationRow.InstrumentDescription = Temp_PFPCID
			newValuationRow.InstrumentCurrency = FundCurrency
			newValuationRow.Amount = temp_value
			newValuationRow.LocalCcyPrice = LocalPrice
			newValuationRow.FxRate = 1
			newValuationRow.LocalCcyValue = (-CDbl(temp_value))
			newValuationRow.BookCcyValue = (-CDbl(temp_value))

			pPFPCValuationTable.Rows.Add(newValuationRow)

		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error saving value for " & PFPC_ID, ex.StackTrace, True)
			Return False
		End Try

		Return True

	End Function

    ''' <summary>
    ''' Parses the number.
    ''' </summary>
    ''' <param name="pNumericString">The p numeric string.</param>
    ''' <returns>System.String.</returns>
  Function ParseNumber(ByVal pNumericString As String) As String
    Dim NewString As String

    If Len(pNumericString) <= 0 Then
      ParseNumber = pNumericString
      Exit Function
    End If

    NewString = Replace(pNumericString, ",", "")
    NewString = Trim(NewString)

    If NewString.Length >= 3 Then
      If (NewString.StartsWith("(") And NewString.EndsWith(")")) Then
        If IsNumeric(Mid(NewString, 2, Len(NewString) - 2)) = True Then
          NewString = "-" & Format(CDbl(Mid(NewString, 2, Len(NewString) - 2)), "###0.0######")
        End If
      End If
    End If

    ParseNumber = NewString
    Exit Function
  End Function

#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "


    ''' <summary>
    ''' Handles the Click event of the btnLoadData control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnLoadData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLoadData.Click
    ' *****************************************************************************
    ' Run Report
    ' *****************************************************************************

    Dim FundID As Integer
    Dim ValueDate As Date


    ' Validate

    If (Me.Combo_Fund.SelectedValue Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Fund must be selected.", "", True)
      Exit Sub
    End If

    If (Me.Combo_Fund.SelectedIndex <= 0) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Fund must be selected.", "", True)
      Exit Sub
    End If
    FundID = Me.Combo_Fund.SelectedValue

    ValueDate = Me.Date_ValueDate.Value.Date

    Me.MainForm.SetToolStripText(Label_Status, "")
    Me.Refresh()
    System.Windows.Forms.Application.DoEvents()

    ' Save existing values as defaults
    Try
      Dim StringName As String

      Me.btnLoadData.Enabled = False
      Me.MainForm.SetToolStripText(Label_Status, "")
      Me.Form_StatusStrip.Refresh()
      System.Windows.Forms.Application.DoEvents()

      StringName = "LoadData_PortfolioReport_" & FundID.ToString & "_" & ValueDate.ToString(QUERY_SHORTDATEFORMAT)
      MainForm.Set_SystemString(StringName, edit_PortfolioReport.Text)

      StringName = "LoadData_ExpensesReport_" & FundID.ToString & "_" & ValueDate.ToString(QUERY_SHORTDATEFORMAT)
      MainForm.Set_SystemString(StringName, edit_ExpensesReport.Text)

      StringName = "LoadData_TransactionReport_" & FundID.ToString & "_" & ValueDate.ToString(QUERY_SHORTDATEFORMAT)
      MainForm.Set_SystemString(StringName, edit_TransactionReport.Text)

      StringName = "LoadData_CustodyRecord_" & FundID.ToString & "_" & ValueDate.ToString(QUERY_SHORTDATEFORMAT)
      MainForm.Set_SystemString(StringName, edit_CustodyRecord.Text)

      StringName = "LoadData_ShareAllocationRecord_" & FundID.ToString & "_" & ValueDate.ToString(QUERY_SHORTDATEFORMAT)
      MainForm.Set_SystemString(StringName, edit_ShareAllocationRecord.Text)

      StringName = "LoadData_ExpensesWorksheet_" & FundID.ToString & "_" & ValueDate.ToString(QUERY_SHORTDATEFORMAT)
      If Combo_ExpensesWorksheet.SelectedIndex >= 0 Then
        MainForm.Set_SystemString(StringName, Me.Combo_ExpensesWorksheet.SelectedItem.ToString)
      Else
        MainForm.Set_SystemString(StringName, "")
      End If

      StringName = "LoadData_ShareAllocationWorksheet_" & FundID.ToString & "_" & ValueDate.ToString(QUERY_SHORTDATEFORMAT)
      If Me.Combo_ShareAllocationWorksheet.SelectedIndex >= 0 Then
        MainForm.Set_SystemString(StringName, Me.Combo_ShareAllocationWorksheet.SelectedItem.ToString)
      Else
        MainForm.Set_SystemString(StringName, "")
      End If
    Catch ex As Exception
    End Try

    ' Load PFPC Data.
    Try

      Me.btnLoadData.Enabled = False
      Me.Label_Status.Text = "Loading Data..."
      Me.Form_StatusStrip.Refresh()
      System.Windows.Forms.Application.DoEvents()

			' Portfolio Report

      If Me.edit_PortfolioReport.Text.Length > 0 Then
        If File.Exists(Me.edit_PortfolioReport.Text) Then
          ' OK, File seems to exist.
          Try
            Me.Label_Status.Text = "Loading from Portfolio Report"
            Me.Refresh()

            Call ReadPFPCFundValuationWorksheet(Me.Name, Me.edit_PortfolioReport.Text, FundID, ValueDate)
          Catch ex As Exception
            MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in ReadPFPCFundValuationWorksheet()", ex.StackTrace, True)
          End Try

        Else
          ' MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Portfolio Report file does not exist.", "", True)
        End If
      End If

			' Expenses Report

      If Me.edit_ExpensesReport.Text.Length > 0 Then
        If File.Exists(Me.edit_ExpensesReport.Text) Then
          If Me.Combo_ExpensesWorksheet.SelectedIndex >= 0 Then
            ' OK, File seems to exist.
            Try
              Me.Label_Status.Text = "Loading from Expenses Report"
              Me.Refresh()

              Call ReadPFPCExpensesWorksheet(Me.Name, Me.edit_ExpensesReport.Text, Me.Combo_ExpensesWorksheet.SelectedItem, FundID, ValueDate)
            Catch ex As Exception
              MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in ReadPFPCExpensesWorksheet()", ex.StackTrace, True)
            End Try
          Else
            MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Expenses Worksheet name has not been selected.", "", True)
          End If

        Else
          ' MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Expenses Report file does not exist.", "", True)
        End If
      End If

			' Transaction Report

      If Me.edit_TransactionReport.Text.Length > 0 Then
        If File.Exists(Me.edit_TransactionReport.Text) Then
          ' OK, File seems to exist.
          Try
            Me.Label_Status.Text = "Loading from Transaction Report"
            Me.Refresh()

            ' Call ReadPFPCTransactionReport(Me.Name, Me.edit_TransactionReport.Text, FundID, ValueDate)
          Catch ex As Exception
            MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in ReadPFPCTransactionReport()", ex.StackTrace, True)
          End Try

        Else
          '  MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Transaction Report file does not exist.", "", True)
        End If
      End If

			' Custody record

      If Me.edit_CustodyRecord.Text.Length > 0 Then
        If File.Exists(Me.edit_CustodyRecord.Text) Then
          ' OK, File seems to exist.
          Try
            Me.Label_Status.Text = "Loading from Custody record"
            Me.Refresh()

            ' Call ReadPFPCCustodyRecord(Me.Name, Me.edit_CustodyRecord.Text, FundID, ValueDate)
          Catch ex As Exception
            MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in ReadPFPCCustodyRecord()", ex.StackTrace, True)
          End Try

        Else
          '  MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Transaction Report file does not exist.", "", True)
        End If
      End If

			' Share Allocation Record

      If Me.edit_ShareAllocationRecord.Text.Length > 0 Then
        If File.Exists(Me.edit_ShareAllocationRecord.Text) Then
          If Me.Combo_ShareAllocationWorksheet.SelectedIndex >= 0 Then
            ' OK, File seems to exist.
            Try
              Me.Label_Status.Text = "Loading from Share Allocation Record"
              Me.Refresh()

              Call ReadPFPCShareAllocationWorksheet(Me.Name, Me.edit_ShareAllocationRecord.Text, Me.Combo_ShareAllocationWorksheet.SelectedItem, FundID, ValueDate)
            Catch ex As Exception
              MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in PFPCShareAllocationWorksheet()", ex.StackTrace, True)
            End Try
          Else
            MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Share Allocation Worksheet name has not been selected.", "", True)
          End If

        Else
          ' MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Share Allocation spreadsheet file does not exist.", "", True)
        End If
      End If


		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Loading PFPC Data", ex.StackTrace, True)
		End Try

    Me.MainForm.SetToolStripText(Label_Status, "")
    Me.btnLoadData.Enabled = True

  End Sub


    ''' <summary>
    ''' Handles the Click event of the btnClose control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
    ' *****************************************************************************
    ' Close Form
    ' *****************************************************************************

    Me.Close()

  End Sub

#End Region

#Region " Control Event Code"

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_Fund control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_Fund_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_Fund.SelectedIndexChanged
    ' *****************************************************************************************************
    ' 
    '
    '
    ' *****************************************************************************************************

    Dim FundID As Integer
    Dim StringName As String
    Dim ValueDate As Date

    If InPaint = True Then
      Exit Sub
    End If

    ' Set Default file names

    Me.edit_PortfolioReport.Text = ""
    Me.edit_ExpensesReport.Text = ""
    Me.edit_TransactionReport.Text = ""
    Me.edit_CustodyRecord.Text = ""
    Me.edit_ShareAllocationRecord.Text = ""

    If Me.Combo_Fund.SelectedIndex < 0 Then
      Exit Sub
    End If

    Try
      If IsNumeric(Combo_Fund.SelectedValue) Then
        FundID = Me.Combo_Fund.SelectedValue
      Else
        FundID = 0
      End If
    Catch ex As Exception
      Exit Sub
    End Try

    ValueDate = Me.Date_ValueDate.Value.Date

    ' Portfolio Report
    StringName = "LoadData_PortfolioReport_" & FundID.ToString & "_" & ValueDate.ToString(QUERY_SHORTDATEFORMAT)
    Me.edit_PortfolioReport.Text = MainForm.Get_SystemString(StringName)

    ' Expenses Report
    StringName = "LoadData_ExpensesReport_" & FundID.ToString & "_" & ValueDate.ToString(QUERY_SHORTDATEFORMAT)
    Me.edit_ExpensesReport.Text = MainForm.Get_SystemString(StringName)

    ' Transaction Report
    StringName = "LoadData_TransactionReport_" & FundID.ToString & "_" & ValueDate.ToString(QUERY_SHORTDATEFORMAT)
    Me.edit_TransactionReport.Text = MainForm.Get_SystemString(StringName)

    ' Custody Record
    StringName = "LoadData_CustodyRecord_" & FundID.ToString & "_" & ValueDate.ToString(QUERY_SHORTDATEFORMAT)
    Me.edit_CustodyRecord.Text = MainForm.Get_SystemString(StringName)

    ' Share Allocation Record
    StringName = "LoadData_ShareAllocationRecord_" & FundID.ToString & "_" & ValueDate.ToString(QUERY_SHORTDATEFORMAT)
    Me.edit_ShareAllocationRecord.Text = MainForm.Get_SystemString(StringName)

  End Sub

    ''' <summary>
    ''' Handles the TextChanged event of the edit_ExpensesReport control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub edit_ExpensesReport_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles edit_ExpensesReport.TextChanged
    ' *****************************************************************************************************
    ' Open the given Expenses spreadsheet and populate the Worksheet combo, preserving the current
    ' worksheet name if it exists in the given workbook.
    '
    '
    ' *****************************************************************************************************

    Dim FileName As String
    Dim Counter As Integer
    Dim LastSelection As String = ""
    Dim FundID As Integer
    Dim StringName As String

    FileName = Me.edit_ExpensesReport.Text

    Try
      FundID = Me.Combo_Fund.SelectedValue
    Catch ex As Exception
      Exit Sub
    End Try

    Try
      ' Expenses Report
      StringName = "LoadData_ExpensesWorksheet_" & FundID.ToString & "_" & Me.Date_ValueDate.Value.Date.ToString(QUERY_SHORTDATEFORMAT)
      LastSelection = MainForm.Get_SystemString(StringName)

      Combo_ExpensesWorksheet.Items.Clear()

      If (FileName.Length <= 0) Then
        Exit Sub
      End If
    Catch ex As Exception
    End Try

    ' Interrogate the specified spreadsheet for Worksheets

    Dim ExcelApp As Excel.Application
    Dim WorkBook As Excel.Workbook = Nothing

    Try
      ExcelApp = New Excel.Application
    Catch ex As Exception
      Exit Sub
    End Try

    ' Open workbook 

    Try
      WorkBook = ExcelApp.Workbooks.Open(FileName, False, False, , , , True)
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error opening Excel Worksheet", ex.StackTrace, True)
    End Try

    ' Populate Worksheet combo.

    Try
      If Not (WorkBook Is Nothing) Then
        For Counter = 1 To WorkBook.Worksheets.Count
          Combo_ExpensesWorksheet.Items.Add(Nz((WorkBook.Worksheets(Counter).Name), ""))
          If LastSelection = CStr(Nz((WorkBook.Worksheets(Counter).Name), "")) Then
            Combo_ExpensesWorksheet.SelectedIndex = (Counter - 1)
          End If
        Next Counter

        WorkBook.Close(False)
      End If
    Catch ex As Exception
    End Try

    Try
      ExcelApp.Quit()
      ExcelApp = Nothing
    Catch ex As Exception
    End Try
  End Sub

    ''' <summary>
    ''' Handles the TextChanged event of the edit_ShareAllocationRecord control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub edit_ShareAllocationRecord_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles edit_ShareAllocationRecord.TextChanged
    ' *****************************************************************************************************
    ' Open the given Allocation spreadsheet and populate the Worksheet combo, preserving the current
    ' worksheet name if it exists in the given workbook.
    '
    '
    ' *****************************************************************************************************

    Dim FileName As String
    Dim Counter As Integer
    Dim LastSelection As String = ""
    Dim FundID As Integer
    Dim StringName As String

    FileName = Me.edit_ShareAllocationRecord.Text

    Try
      FundID = Me.Combo_Fund.SelectedValue
    Catch ex As Exception
      Exit Sub
    End Try

    ' Get the current worksheet name.

    Try
      StringName = "LoadData_ShareAllocationWorksheet_" & FundID.ToString & "_" & Me.Date_ValueDate.Value.Date.ToString(QUERY_SHORTDATEFORMAT)
      LastSelection = MainForm.Get_SystemString(StringName)

      Combo_ShareAllocationWorksheet.Items.Clear()

      If (FileName.Length <= 0) Then
        Exit Sub
      End If
    Catch ex As Exception

    End Try

    ' Interrogate the specified spreadsheet for Worksheets

    Dim ExcelApp As Excel.Application
    Dim WorkBook As Excel.Workbook

    Try
      ExcelApp = New Excel.Application
    Catch ex As Exception
      Exit Sub
    End Try

    Try
      WorkBook = ExcelApp.Workbooks.Open(FileName, False, False, , , , True)
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error opening Excel Worksheet", ex.StackTrace, True)

      ExcelApp.Quit()
      ExcelApp = Nothing
      Exit Sub
    End Try

    Try
      If Not (WorkBook Is Nothing) Then
        For Counter = 1 To WorkBook.Worksheets.Count
          Combo_ShareAllocationWorksheet.Items.Add(Nz((WorkBook.Worksheets(Counter).Name), ""))
          If LastSelection = CStr(Nz((WorkBook.Worksheets(Counter).Name), "")) Then
            Combo_ShareAllocationWorksheet.SelectedIndex = (Counter - 1)
          End If
        Next Counter

        WorkBook.Close(False)
      End If
    Catch ex As Exception
    End Try

    Try
      ExcelApp.Quit()
      ExcelApp = Nothing
    Catch ex As Exception
    End Try


  End Sub

    ''' <summary>
    ''' Handles the Click event of the btn_ChoosePortfolio control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btn_ChoosePortfolio_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_ChoosePortfolio.Click
    ' *****************************************************************************************************
    ' 
    '
    '
    ' *****************************************************************************************************

    Dim ExistingDirectory As String
    Dim ExistingFile As String
    Dim ExistingText As String

    ExistingDirectory = Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath)
    ExistingFile = ""

    Try
      ExistingText = edit_PortfolioReport.Text
      If ExistingText.Length > 0 Then
        If Directory.Exists(ExistingText) Then
          ExistingDirectory = ExistingText
        ElseIf File.Exists(ExistingText) Then
          ExistingDirectory = Path.GetDirectoryName(ExistingText)
          ExistingFile = Path.GetFileName(ExistingText)
        Else
          ExistingDirectory = Path.GetDirectoryName(ExistingText)
          If Directory.Exists(ExistingText) = False Then
            ExistingDirectory = Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath)
          End If
        End If
      End If
    Catch ex As Exception
    End Try

    Try
      Me.edit_PortfolioReport.Text = ChooseFileName(ExistingDirectory, ExistingFile)
    Catch ex As Exception
    End Try

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btn_ChooseExpensesReport control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btn_ChooseExpensesReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_ChooseExpensesReport.Click
    ' *****************************************************************************************************
    ' 
    '
    '
    ' *****************************************************************************************************

    Dim ExistingDirectory As String
    Dim ExistingFile As String
    Dim ExistingText As String

    ExistingDirectory = Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath)
    ExistingFile = ""

    Try
      ExistingText = edit_ExpensesReport.Text
      If ExistingText.Length > 0 Then
        If Directory.Exists(ExistingText) Then
          ExistingDirectory = ExistingText
        ElseIf File.Exists(ExistingText) Then
          ExistingDirectory = Path.GetDirectoryName(ExistingText)
          ExistingFile = Path.GetFileName(ExistingText)
        Else
          ExistingDirectory = Path.GetDirectoryName(ExistingText)
          If Directory.Exists(ExistingText) = False Then
            ExistingDirectory = Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath)
          End If
        End If
      End If
    Catch ex As Exception
    End Try

    Try
      edit_ExpensesReport.Text = ChooseFileName(ExistingDirectory, ExistingFile)
    Catch ex As Exception
    End Try

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btn_ChooseTransactionReport control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btn_ChooseTransactionReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_ChooseTransactionReport.Click
    ' *****************************************************************************************************
    ' 
    '
    '
    ' *****************************************************************************************************

    Dim ExistingDirectory As String
    Dim ExistingFile As String
    Dim ExistingText As String

    ExistingDirectory = Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath)
    ExistingFile = ""

    Try
      ExistingText = edit_TransactionReport.Text
      If ExistingText.Length > 0 Then
        If Directory.Exists(ExistingText) Then
          ExistingDirectory = ExistingText
        ElseIf File.Exists(ExistingText) Then
          ExistingDirectory = Path.GetDirectoryName(ExistingText)
          ExistingFile = Path.GetFileName(ExistingText)
        Else
          ExistingDirectory = Path.GetDirectoryName(ExistingText)
          If Directory.Exists(ExistingText) = False Then
            ExistingDirectory = Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath)
          End If
        End If
      End If
    Catch ex As Exception
    End Try

    Try
      edit_TransactionReport.Text = ChooseFileName(ExistingDirectory, ExistingFile)
    Catch ex As Exception
    End Try

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btn_ChooseCustodyRecord control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btn_ChooseCustodyRecord_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_ChooseCustodyRecord.Click
    ' *****************************************************************************************************
    ' 
    '
    '
    ' *****************************************************************************************************

    Dim ExistingDirectory As String
    Dim ExistingFile As String
    Dim ExistingText As String

    ExistingDirectory = Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath)
    ExistingFile = ""

    Try
      ExistingText = edit_CustodyRecord.Text
      If ExistingText.Length > 0 Then
        If Directory.Exists(ExistingText) Then
          ExistingDirectory = ExistingText
        ElseIf File.Exists(ExistingText) Then
          ExistingDirectory = Path.GetDirectoryName(ExistingText)
          ExistingFile = Path.GetFileName(ExistingText)
        Else
          ExistingDirectory = Path.GetDirectoryName(ExistingText)
          If Directory.Exists(ExistingText) = False Then
            ExistingDirectory = Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath)
          End If
        End If
      End If
    Catch ex As Exception
    End Try

    Try
      edit_CustodyRecord.Text = ChooseFileName(ExistingDirectory, ExistingFile)
    Catch ex As Exception
    End Try

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btn_ChooseShareAllocationRecord control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btn_ChooseShareAllocationRecord_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_ChooseShareAllocationRecord.Click
    ' *****************************************************************************************************
    ' 
    '
    '
    ' *****************************************************************************************************

    Dim ExistingDirectory As String
    Dim ExistingFile As String
    Dim ExistingText As String

    ExistingDirectory = Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath)
    ExistingFile = ""

    Try
      ExistingText = edit_ShareAllocationRecord.Text
      If ExistingText.Length > 0 Then
        If Directory.Exists(ExistingText) Then
          ExistingDirectory = ExistingText
        ElseIf File.Exists(ExistingText) Then
          ExistingDirectory = Path.GetDirectoryName(ExistingText)
          ExistingFile = Path.GetFileName(ExistingText)
        Else
          ExistingDirectory = Path.GetDirectoryName(ExistingText)
          If Directory.Exists(ExistingText) = False Then
            ExistingDirectory = Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath)
          End If
        End If
      End If
    Catch ex As Exception
    End Try

    Try
      edit_ShareAllocationRecord.Text = ChooseFileName(ExistingDirectory, ExistingFile)
    Catch ex As Exception
    End Try

  End Sub

    ''' <summary>
    ''' Chooses the name of the file.
    ''' </summary>
    ''' <param name="pDirectoryName">Name of the p directory.</param>
    ''' <param name="pFileName">Name of the p file.</param>
    ''' <returns>System.String.</returns>
  Private Function ChooseFileName(ByVal pDirectoryName As String, ByVal pFileName As String) As String
    ' *****************************************************************************************************
    ' Generic function used to access the OpenFileDialog control.
    '
    '
    ' *****************************************************************************************************

    Try
      OpenFileDialog1.CheckFileExists = True
      OpenFileDialog1.CheckPathExists = True
      OpenFileDialog1.Filter = "All Files|*.*"
      OpenFileDialog1.FilterIndex = 1
      OpenFileDialog1.InitialDirectory = pDirectoryName
      OpenFileDialog1.FileName = pFileName
      OpenFileDialog1.Multiselect = False
      OpenFileDialog1.Title = "PFPC : Select Data"
      OpenFileDialog1.ValidateNames = True

      If (OpenFileDialog1.ShowDialog = System.Windows.Forms.DialogResult.OK) Then
        Return OpenFileDialog1.FileName
      Else
        Return Path.Combine(pDirectoryName, pFileName)
      End If
    Catch ex As Exception
      Return ""
    End Try
  End Function

    ''' <summary>
    ''' Handles the ValueChanged event of the Date_ValueDate control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Date_ValueDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Date_ValueDate.ValueChanged
    ' *****************************************************************************************************
    ' If the report Date is changed, then refresh the PFPC data file names.
    '
    '
    ' *****************************************************************************************************
    Static LastDate As Date

    Dim ValueDate As Date
    Dim MonthEndValueDate As Date
    Dim StringName As String
    Dim FundID As Integer

    ' Resolve Value Date.

    ValueDate = Me.Date_ValueDate.Value
    MonthEndValueDate = ValueDate.AddMonths(1)
    MonthEndValueDate = MonthEndValueDate.AddDays(-MonthEndValueDate.Day)

    ' Do not update the fields again if the 'Current' Date value has simply been set again.
    If LastDate = MonthEndValueDate Then
      Exit Sub
    End If
    Me.Date_ValueDate.Value = MonthEndValueDate

    ' Save the new Date value an continue.
    LastDate = MonthEndValueDate
    ValueDate = MonthEndValueDate
    FundID = 0
    If (Me.Combo_Fund.SelectedIndex >= 0) Then
      If IsNumeric(Me.Combo_Fund.SelectedValue) Then
        FundID = CInt(Me.Combo_Fund.SelectedValue)
      End If
    End If

    ' Initialise values.

    Me.edit_PortfolioReport.Text = ""
    Me.edit_ExpensesReport.Text = ""
    Me.edit_TransactionReport.Text = ""
    Me.edit_CustodyRecord.Text = ""
    Me.edit_ShareAllocationRecord.Text = ""

    ' Load appropriate values from the System Strings table.

    ' Portfolio Report
    StringName = "LoadData_PortfolioReport_" & FundID.ToString & "_" & ValueDate.ToString(QUERY_SHORTDATEFORMAT)
    Me.edit_PortfolioReport.Text = MainForm.Get_SystemString(StringName)

    ' Expenses Report
    StringName = "LoadData_ExpensesReport_" & FundID.ToString & "_" & ValueDate.ToString(QUERY_SHORTDATEFORMAT)
    Me.edit_ExpensesReport.Text = MainForm.Get_SystemString(StringName)

    ' Transaction Report
    StringName = "LoadData_TransactionReport_" & FundID.ToString & "_" & ValueDate.ToString(QUERY_SHORTDATEFORMAT)
    Me.edit_TransactionReport.Text = MainForm.Get_SystemString(StringName)

    ' Custody Record
    StringName = "LoadData_CustodyRecord_" & FundID.ToString & "_" & ValueDate.ToString(QUERY_SHORTDATEFORMAT)
    Me.edit_CustodyRecord.Text = MainForm.Get_SystemString(StringName)

    ' Share Allocation Record
    StringName = "LoadData_ShareAllocationRecord_" & FundID.ToString & "_" & ValueDate.ToString(QUERY_SHORTDATEFORMAT)
    Me.edit_ShareAllocationRecord.Text = MainForm.Get_SystemString(StringName)

  End Sub


#End Region

End Class
