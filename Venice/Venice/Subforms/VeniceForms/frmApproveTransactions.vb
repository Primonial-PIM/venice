﻿' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 03-08-2013
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmApproveTransactions.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals

Imports RenaissanceDataClass
Imports C1.Win.C1FlexGrid

''' <summary>
''' Class frmApproveTransactions
''' </summary>
Public Class frmApproveTransactions

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

  ''' <summary>
  ''' Prevents a default instance of the <see cref="frmApproveTransactions"/> class from being created.
  ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The combo_ transaction fund
    ''' </summary>
  Friend WithEvents Combo_TransactionFund As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label_ cpty is fund
    ''' </summary>
  Friend WithEvents label_CptyIsFund As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ transaction instrument
    ''' </summary>
  Friend WithEvents Combo_TransactionInstrument As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label2
    ''' </summary>
  Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The grid_ transactions
    ''' </summary>
  Friend WithEvents Grid_Transactions As C1.Win.C1FlexGrid.C1FlexGrid
    ''' <summary>
    ''' The root menu
    ''' </summary>
  Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
    ''' <summary>
    ''' The form_ status strip
    ''' </summary>
  Friend WithEvents Form_StatusStrip As System.Windows.Forms.StatusStrip
    ''' <summary>
    ''' The form_ progress bar
    ''' </summary>
  Friend WithEvents Form_ProgressBar As System.Windows.Forms.ToolStripProgressBar
    ''' <summary>
    ''' The combo_ status group
    ''' </summary>
  Friend WithEvents Combo_StatusGroup As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label1
    ''' </summary>
  Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The button_ approve trades
    ''' </summary>
  Friend WithEvents Button_ApproveTrades As System.Windows.Forms.Button
    ''' <summary>
    ''' The button_ reject trades
    ''' </summary>
  Friend WithEvents Button_RejectTrades As System.Windows.Forms.Button
    ''' <summary>
    ''' The tab control_ transactions
    ''' </summary>
  Friend WithEvents TabControl_Transactions As System.Windows.Forms.TabControl
    ''' <summary>
    ''' The tab_ approval
    ''' </summary>
  Friend WithEvents Tab_Approval As System.Windows.Forms.TabPage
    ''' <summary>
    ''' The tab_ awaiting file
    ''' </summary>
  Friend WithEvents Tab_AwaitingFile As System.Windows.Forms.TabPage
    ''' <summary>
    ''' The grid_ to be written
    ''' </summary>
  Friend WithEvents Grid_ToBeWritten As C1.Win.C1FlexGrid.C1FlexGrid
    ''' <summary>
    ''' The button_ cancel approval
    ''' </summary>
  Friend WithEvents Button_CancelApproval As System.Windows.Forms.Button
    ''' <summary>
    ''' The button_ mark as sent
    ''' </summary>
	Friend WithEvents Button_MarkAsSent As System.Windows.Forms.Button
    ''' <summary>
    ''' The check_ show percent of NAV
    ''' </summary>
  Friend WithEvents Check_ShowPercentOfNAV As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The label3
    ''' </summary>
  Friend WithEvents Label3 As System.Windows.Forms.Label
    ''' <summary>
    ''' The date_ trigger date
    ''' </summary>
  Friend WithEvents Date_TriggerDate As System.Windows.Forms.DateTimePicker
  Friend WithEvents Tab_WorkingTrades As System.Windows.Forms.TabPage
  Friend WithEvents Grid_WorkingTrades As C1.Win.C1FlexGrid.C1FlexGrid
  Friend WithEvents Text_KansasStatus As System.Windows.Forms.TextBox
  Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
  ''' <summary>
  ''' The label_ status
  ''' </summary>
  Friend WithEvents Label_Status As System.Windows.Forms.ToolStripStatusLabel
  ''' <summary>
  ''' Initializes the component.
  ''' </summary>
  ''' 
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.Grid_Transactions = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.Combo_TransactionFund = New System.Windows.Forms.ComboBox
    Me.label_CptyIsFund = New System.Windows.Forms.Label
    Me.Combo_TransactionInstrument = New System.Windows.Forms.ComboBox
    Me.Label2 = New System.Windows.Forms.Label
    Me.RootMenu = New System.Windows.Forms.MenuStrip
    Me.Form_StatusStrip = New System.Windows.Forms.StatusStrip
    Me.Form_ProgressBar = New System.Windows.Forms.ToolStripProgressBar
    Me.Label_Status = New System.Windows.Forms.ToolStripStatusLabel
    Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel
    Me.Combo_StatusGroup = New System.Windows.Forms.ComboBox
    Me.Label1 = New System.Windows.Forms.Label
    Me.Button_ApproveTrades = New System.Windows.Forms.Button
    Me.Button_RejectTrades = New System.Windows.Forms.Button
    Me.TabControl_Transactions = New System.Windows.Forms.TabControl
    Me.Tab_Approval = New System.Windows.Forms.TabPage
    Me.Tab_AwaitingFile = New System.Windows.Forms.TabPage
    Me.Grid_ToBeWritten = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.Tab_WorkingTrades = New System.Windows.Forms.TabPage
    Me.Grid_WorkingTrades = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.Text_KansasStatus = New System.Windows.Forms.TextBox
    Me.Button_CancelApproval = New System.Windows.Forms.Button
    Me.Button_MarkAsSent = New System.Windows.Forms.Button
    Me.Check_ShowPercentOfNAV = New System.Windows.Forms.CheckBox
    Me.Label3 = New System.Windows.Forms.Label
    Me.Date_TriggerDate = New System.Windows.Forms.DateTimePicker
    CType(Me.Grid_Transactions, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Form_StatusStrip.SuspendLayout()
    Me.TabControl_Transactions.SuspendLayout()
    Me.Tab_Approval.SuspendLayout()
    Me.Tab_AwaitingFile.SuspendLayout()
    CType(Me.Grid_ToBeWritten, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_WorkingTrades.SuspendLayout()
    CType(Me.Grid_WorkingTrades, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'Grid_Transactions
    '
    Me.Grid_Transactions.AllowEditing = False
    Me.Grid_Transactions.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_Transactions.CausesValidation = False
    Me.Grid_Transactions.ColumnInfo = "5,1,0,0,0,85,Columns:"
    Me.Grid_Transactions.EditOptions = C1.Win.C1FlexGrid.EditFlags.None
    Me.Grid_Transactions.KeyActionTab = C1.Win.C1FlexGrid.KeyActionEnum.MoveAcross
    Me.Grid_Transactions.Location = New System.Drawing.Point(3, 2)
    Me.Grid_Transactions.Name = "Grid_Transactions"
    Me.Grid_Transactions.Rows.DefaultSize = 17
    Me.Grid_Transactions.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.ListBox
    Me.Grid_Transactions.Size = New System.Drawing.Size(953, 373)
    Me.Grid_Transactions.TabIndex = 0
    '
    'Combo_TransactionFund
    '
    Me.Combo_TransactionFund.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_TransactionFund.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_TransactionFund.Location = New System.Drawing.Point(128, 32)
    Me.Combo_TransactionFund.Name = "Combo_TransactionFund"
    Me.Combo_TransactionFund.Size = New System.Drawing.Size(831, 21)
    Me.Combo_TransactionFund.TabIndex = 0
    '
    'label_CptyIsFund
    '
    Me.label_CptyIsFund.Location = New System.Drawing.Point(16, 36)
    Me.label_CptyIsFund.Name = "label_CptyIsFund"
    Me.label_CptyIsFund.Size = New System.Drawing.Size(104, 16)
    Me.label_CptyIsFund.TabIndex = 82
    Me.label_CptyIsFund.Text = "Fund"
    '
    'Combo_TransactionInstrument
    '
    Me.Combo_TransactionInstrument.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_TransactionInstrument.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_TransactionInstrument.Location = New System.Drawing.Point(128, 60)
    Me.Combo_TransactionInstrument.Name = "Combo_TransactionInstrument"
    Me.Combo_TransactionInstrument.Size = New System.Drawing.Size(831, 21)
    Me.Combo_TransactionInstrument.TabIndex = 1
    '
    'Label2
    '
    Me.Label2.Location = New System.Drawing.Point(16, 64)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(104, 16)
    Me.Label2.TabIndex = 84
    Me.Label2.Text = "Instrument"
    '
    'RootMenu
    '
    Me.RootMenu.Location = New System.Drawing.Point(0, 0)
    Me.RootMenu.Name = "RootMenu"
    Me.RootMenu.Size = New System.Drawing.Size(967, 24)
    Me.RootMenu.TabIndex = 10
    Me.RootMenu.Text = " "
    '
    'Form_StatusStrip
    '
    Me.Form_StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Form_ProgressBar, Me.Label_Status, Me.ToolStripStatusLabel1})
    Me.Form_StatusStrip.Location = New System.Drawing.Point(0, 653)
    Me.Form_StatusStrip.Name = "Form_StatusStrip"
    Me.Form_StatusStrip.Size = New System.Drawing.Size(967, 22)
    Me.Form_StatusStrip.TabIndex = 105
    Me.Form_StatusStrip.Text = " "
    '
    'Form_ProgressBar
    '
    Me.Form_ProgressBar.Maximum = 20
    Me.Form_ProgressBar.Name = "Form_ProgressBar"
    Me.Form_ProgressBar.Size = New System.Drawing.Size(150, 17)
    Me.Form_ProgressBar.Step = 1
    Me.Form_ProgressBar.Visible = False
    '
    'Label_Status
    '
    Me.Label_Status.Name = "Label_Status"
    Me.Label_Status.Size = New System.Drawing.Size(10, 17)
    Me.Label_Status.Text = " "
    '
    'ToolStripStatusLabel1
    '
    Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
    Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(121, 17)
    Me.ToolStripStatusLabel1.Text = "ToolStripStatusLabel1"
    '
    'Combo_StatusGroup
    '
    Me.Combo_StatusGroup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_StatusGroup.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_StatusGroup.Location = New System.Drawing.Point(128, 87)
    Me.Combo_StatusGroup.Name = "Combo_StatusGroup"
    Me.Combo_StatusGroup.Size = New System.Drawing.Size(831, 21)
    Me.Combo_StatusGroup.TabIndex = 2
    '
    'Label1
    '
    Me.Label1.Location = New System.Drawing.Point(16, 91)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(104, 16)
    Me.Label1.TabIndex = 107
    Me.Label1.Text = "Status Group"
    '
    'Button_ApproveTrades
    '
    Me.Button_ApproveTrades.Location = New System.Drawing.Point(128, 169)
    Me.Button_ApproveTrades.Name = "Button_ApproveTrades"
    Me.Button_ApproveTrades.Size = New System.Drawing.Size(176, 25)
    Me.Button_ApproveTrades.TabIndex = 5
    Me.Button_ApproveTrades.Text = "Approve Selected Trades"
    Me.Button_ApproveTrades.UseVisualStyleBackColor = True
    '
    'Button_RejectTrades
    '
    Me.Button_RejectTrades.Location = New System.Drawing.Point(401, 169)
    Me.Button_RejectTrades.Name = "Button_RejectTrades"
    Me.Button_RejectTrades.Size = New System.Drawing.Size(176, 25)
    Me.Button_RejectTrades.TabIndex = 7
    Me.Button_RejectTrades.Text = "Reject Selected Trades"
    Me.Button_RejectTrades.UseVisualStyleBackColor = True
    '
    'TabControl_Transactions
    '
    Me.TabControl_Transactions.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.TabControl_Transactions.Controls.Add(Me.Tab_Approval)
    Me.TabControl_Transactions.Controls.Add(Me.Tab_AwaitingFile)
    Me.TabControl_Transactions.Controls.Add(Me.Tab_WorkingTrades)
    Me.TabControl_Transactions.Location = New System.Drawing.Point(1, 201)
    Me.TabControl_Transactions.Name = "TabControl_Transactions"
    Me.TabControl_Transactions.SelectedIndex = 0
    Me.TabControl_Transactions.Size = New System.Drawing.Size(967, 404)
    Me.TabControl_Transactions.TabIndex = 9
    '
    'Tab_Approval
    '
    Me.Tab_Approval.Controls.Add(Me.Grid_Transactions)
    Me.Tab_Approval.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Approval.Name = "Tab_Approval"
    Me.Tab_Approval.Padding = New System.Windows.Forms.Padding(3)
    Me.Tab_Approval.Size = New System.Drawing.Size(959, 378)
    Me.Tab_Approval.TabIndex = 0
    Me.Tab_Approval.Text = "Awaiting Approval"
    Me.Tab_Approval.UseVisualStyleBackColor = True
    '
    'Tab_AwaitingFile
    '
    Me.Tab_AwaitingFile.Controls.Add(Me.Grid_ToBeWritten)
    Me.Tab_AwaitingFile.Location = New System.Drawing.Point(4, 22)
    Me.Tab_AwaitingFile.Name = "Tab_AwaitingFile"
    Me.Tab_AwaitingFile.Padding = New System.Windows.Forms.Padding(3)
    Me.Tab_AwaitingFile.Size = New System.Drawing.Size(959, 378)
    Me.Tab_AwaitingFile.TabIndex = 1
    Me.Tab_AwaitingFile.Text = "To be written to File"
    Me.Tab_AwaitingFile.UseVisualStyleBackColor = True
    '
    'Grid_ToBeWritten
    '
    Me.Grid_ToBeWritten.AllowEditing = False
    Me.Grid_ToBeWritten.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_ToBeWritten.AutoClipboard = True
    Me.Grid_ToBeWritten.CausesValidation = False
    Me.Grid_ToBeWritten.ColumnInfo = "5,1,0,0,0,85,Columns:"
    Me.Grid_ToBeWritten.EditOptions = C1.Win.C1FlexGrid.EditFlags.None
    Me.Grid_ToBeWritten.KeyActionTab = C1.Win.C1FlexGrid.KeyActionEnum.MoveAcross
    Me.Grid_ToBeWritten.Location = New System.Drawing.Point(3, 2)
    Me.Grid_ToBeWritten.Name = "Grid_ToBeWritten"
    Me.Grid_ToBeWritten.Rows.DefaultSize = 17
    Me.Grid_ToBeWritten.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.ListBox
    Me.Grid_ToBeWritten.Size = New System.Drawing.Size(953, 373)
    Me.Grid_ToBeWritten.TabIndex = 6
    '
    'Tab_WorkingTrades
    '
    Me.Tab_WorkingTrades.Controls.Add(Me.Grid_WorkingTrades)
    Me.Tab_WorkingTrades.Location = New System.Drawing.Point(4, 22)
    Me.Tab_WorkingTrades.Name = "Tab_WorkingTrades"
    Me.Tab_WorkingTrades.Size = New System.Drawing.Size(959, 378)
    Me.Tab_WorkingTrades.TabIndex = 2
    Me.Tab_WorkingTrades.Text = "Working Trades"
    Me.Tab_WorkingTrades.UseVisualStyleBackColor = True
    '
    'Grid_WorkingTrades
    '
    Me.Grid_WorkingTrades.AllowEditing = False
    Me.Grid_WorkingTrades.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_WorkingTrades.AutoClipboard = True
    Me.Grid_WorkingTrades.CausesValidation = False
    Me.Grid_WorkingTrades.ColumnInfo = "5,1,0,0,0,85,Columns:"
    Me.Grid_WorkingTrades.EditOptions = C1.Win.C1FlexGrid.EditFlags.None
    Me.Grid_WorkingTrades.KeyActionTab = C1.Win.C1FlexGrid.KeyActionEnum.MoveAcross
    Me.Grid_WorkingTrades.Location = New System.Drawing.Point(3, 2)
    Me.Grid_WorkingTrades.Name = "Grid_WorkingTrades"
    Me.Grid_WorkingTrades.Rows.DefaultSize = 17
    Me.Grid_WorkingTrades.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.ListBox
    Me.Grid_WorkingTrades.Size = New System.Drawing.Size(953, 373)
    Me.Grid_WorkingTrades.TabIndex = 7
    '
    'Text_KansasStatus
    '
    Me.Text_KansasStatus.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Text_KansasStatus.BackColor = System.Drawing.SystemColors.Menu
    Me.Text_KansasStatus.Location = New System.Drawing.Point(4, 607)
    Me.Text_KansasStatus.Multiline = True
    Me.Text_KansasStatus.Name = "Text_KansasStatus"
    Me.Text_KansasStatus.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.Text_KansasStatus.Size = New System.Drawing.Size(959, 44)
    Me.Text_KansasStatus.TabIndex = 1
    '
    'Button_CancelApproval
    '
    Me.Button_CancelApproval.Location = New System.Drawing.Point(128, 169)
    Me.Button_CancelApproval.Name = "Button_CancelApproval"
    Me.Button_CancelApproval.Size = New System.Drawing.Size(449, 25)
    Me.Button_CancelApproval.TabIndex = 6
    Me.Button_CancelApproval.Text = "Revoke Approval on Selected Trades"
    Me.Button_CancelApproval.UseVisualStyleBackColor = True
    Me.Button_CancelApproval.Visible = False
    '
    'Button_MarkAsSent
    '
    Me.Button_MarkAsSent.Location = New System.Drawing.Point(604, 169)
    Me.Button_MarkAsSent.Name = "Button_MarkAsSent"
    Me.Button_MarkAsSent.Size = New System.Drawing.Size(176, 25)
    Me.Button_MarkAsSent.TabIndex = 8
    Me.Button_MarkAsSent.Text = "Mark Selected Trades as sent"
    Me.Button_MarkAsSent.UseVisualStyleBackColor = True
    '
    'Check_ShowPercentOfNAV
    '
    Me.Check_ShowPercentOfNAV.AutoSize = True
    Me.Check_ShowPercentOfNAV.Location = New System.Drawing.Point(128, 114)
    Me.Check_ShowPercentOfNAV.Name = "Check_ShowPercentOfNAV"
    Me.Check_ShowPercentOfNAV.Size = New System.Drawing.Size(206, 17)
    Me.Check_ShowPercentOfNAV.TabIndex = 3
    Me.Check_ShowPercentOfNAV.Text = "Show Transaction percentage of NAV"
    Me.Check_ShowPercentOfNAV.UseVisualStyleBackColor = True
    '
    'Label3
    '
    Me.Label3.Location = New System.Drawing.Point(16, 142)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(104, 16)
    Me.Label3.TabIndex = 108
    Me.Label3.Text = "Hold Trades until"
    '
    'Date_TriggerDate
    '
    Me.Date_TriggerDate.CustomFormat = "dd MMM yyyy HH:mm:ss"
    Me.Date_TriggerDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
    Me.Date_TriggerDate.Location = New System.Drawing.Point(128, 138)
    Me.Date_TriggerDate.Name = "Date_TriggerDate"
    Me.Date_TriggerDate.Size = New System.Drawing.Size(234, 20)
    Me.Date_TriggerDate.TabIndex = 4
    '
    'frmApproveTransactions
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(967, 675)
    Me.Controls.Add(Me.Text_KansasStatus)
    Me.Controls.Add(Me.Date_TriggerDate)
    Me.Controls.Add(Me.Label3)
    Me.Controls.Add(Me.Check_ShowPercentOfNAV)
    Me.Controls.Add(Me.Button_MarkAsSent)
    Me.Controls.Add(Me.TabControl_Transactions)
    Me.Controls.Add(Me.Button_RejectTrades)
    Me.Controls.Add(Me.Button_ApproveTrades)
    Me.Controls.Add(Me.Combo_StatusGroup)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.Form_StatusStrip)
    Me.Controls.Add(Me.RootMenu)
    Me.Controls.Add(Me.Combo_TransactionInstrument)
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.Combo_TransactionFund)
    Me.Controls.Add(Me.label_CptyIsFund)
    Me.Controls.Add(Me.Button_CancelApproval)
    Me.Name = "frmApproveTransactions"
    Me.Text = "Approve Transactions"
    CType(Me.Grid_Transactions, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Form_StatusStrip.ResumeLayout(False)
    Me.Form_StatusStrip.PerformLayout()
    Me.TabControl_Transactions.ResumeLayout(False)
    Me.Tab_Approval.ResumeLayout(False)
    Me.Tab_AwaitingFile.ResumeLayout(False)
    CType(Me.Grid_ToBeWritten, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_WorkingTrades.ResumeLayout(False)
    CType(Me.Grid_WorkingTrades, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "

  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
  ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
  ''' </summary>
  Private WithEvents _MainForm As VeniceMain

  ' Form ToolTip
  ''' <summary>
  ''' Control to provide tooltip functionality.
  ''' </summary>
  Private FormTooltip As New ToolTip()

  ' Form Menu


  ' Form Constants, specific to the table being updated.

  ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
  ''' </summary>
  Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

  ' Form Locals, initialised on 'New' defining what standard data items to use
  'Private THIS_TABLENAME As String
  'Private THIS_DATASETNAME As String


  ' The standard ChangeID for this form. e.g. tblFund
  'Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

  ' Form Specific Order fields
  ''' <summary>
  ''' Field Name to order the Select combo by.
  ''' </summary>
  Private THIS_FORM_OrderBy As String

  ' Form specific Permissioning variables
  ''' <summary>
  ''' The THI s_ FOR m_ permission area
  ''' </summary>
  Private THIS_FORM_PermissionArea As String
  ''' <summary>
  ''' The THI s_ FOR m_ permission type
  ''' </summary>
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
  ''' <summary>
  ''' The THI s_ FOR m_ form ID
  ''' </summary>
  Private THIS_FORM_FormID As VeniceFormID

  ' Data Structures

  ''' <summary>
  ''' The approval dataset
  ''' </summary>
  Private approvalDataset As RenaissanceDataClass.DSSelectTransaction
  ''' <summary>
  ''' The approval table
  ''' </summary>
  Private approvalTable As RenaissanceDataClass.DSSelectTransaction.tblSelectTransactionDataTable = Nothing
  ''' <summary>
  ''' My connection
  ''' </summary>
  Private myConnection As SqlConnection
  ''' <summary>
  ''' The approval adaptor
  ''' </summary>
  Private approvalAdaptor As SqlDataAdapter
  ''' <summary>
  ''' The approval data view
  ''' </summary>
  Private approvalDataView As DataView

  ''' <summary>
  ''' The file dataset
  ''' </summary>
  Private fileDataset As RenaissanceDataClass.DSSelectTransaction
  ''' <summary>
  ''' The file table
  ''' </summary>
  Private fileTable As RenaissanceDataClass.DSSelectTransaction.tblSelectTransactionDataTable = Nothing
  ''' <summary>
  ''' The file adaptor
  ''' </summary>
  Private fileAdaptor As SqlDataAdapter
  ''' <summary>
  ''' The file data view
  ''' </summary>
  Private fileDataView As DataView

  ''' <summary>
  ''' Dataset used to manage the Emsx Status table
  ''' </summary>
  Private EmsxStatusDataset As RenaissanceDataClass.DSTradeFileEmsxStatus
  ''' <summary>
  ''' 
  ''' Emsx Status data table
  ''' </summary>
  Private EmsxStatusTable As RenaissanceDataClass.DSTradeFileEmsxStatus.tblTradeFileEmsxStatusDataTable = Nothing
  ''' <summary>
  ''' SQL Command used to manage the Emsx Status table
  ''' </summary>
  Private EmsxStatusCommand As SqlCommand
  ''' <summary>
  ''' DataView used to manaage the Emsx Status data table
  ''' </summary>
  Private EmsxStatusDataView As DataView

  'Private ThisStandardDataset As RenaissanceGlobals.StandardDataset

  ' Active Element.

  ''' <summary>
  ''' The __ is over cancel button
  ''' </summary>
  Private __IsOverCancelButton As Boolean
  ''' <summary>
  ''' The _ in use
  ''' </summary>
  Private _InUse As Boolean

  ' Form Status Flags

  ''' <summary>
  ''' The form is valid
  ''' </summary>
  Private FormIsValid As Boolean
  ''' <summary>
  ''' The in paint
  ''' </summary>
  Private InPaint As Boolean
  ''' <summary>
  ''' The _ form open failed
  ''' </summary>
  Private _FormOpenFailed As Boolean

  ' User Permission Flags

  ''' <summary>
  ''' The has read permission
  ''' </summary>
  Private HasReadPermission As Boolean
  ''' <summary>
  ''' The has update permission
  ''' </summary>
  Private HasUpdatePermission As Boolean
  ''' <summary>
  ''' The has insert permission
  ''' </summary>
  Private HasInsertPermission As Boolean
  ''' <summary>
  ''' The has delete permission
  ''' </summary>
  Private HasDeletePermission As Boolean

  ''' <summary>
  ''' The has_ F m_ permissions
  ''' </summary>
  Private Has_FM_Permissions As Boolean = False
  ''' <summary>
  ''' The has_ M o_ permissions
  ''' </summary>
  Private Has_MO_Permissions As Boolean = False

  ''' <summary>
  ''' The loading NA vs
  ''' </summary>
  Private LoadingNAVs As Boolean = False
  ''' <summary>
  ''' The euro NA vs
  ''' </summary>
  Private EuroNAVs As Dictionary(Of Integer, Double) = Nothing

  ''' <summary>
  ''' The FX rates
  ''' </summary>
  Private FXRates As Dictionary(Of Integer, Double) = Nothing

  Private KansasIsOk As Boolean = False

#End Region

#Region " Form 'Properties' "

  ''' <summary>
  ''' Gets the main form.
  ''' </summary>
  ''' <value>The main form.</value>
  Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
    ' Public property to return handle to the 'Main' Venice form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

  ''' <summary>
  ''' Gets or sets a value indicating whether this instance is over cancel button.
  ''' </summary>
  ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
  Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
    ' Public property maintaining a value indicating if the cursor is over the 'Cancel'
    ' Button on this form.
    ' This property is specifically designed for use by the field formating Event functions
    ' In order that they do not impose format restrictions if the user is about to click the 
    ' 'Cancel' button.
    '
    Get
      Return __IsOverCancelButton
    End Get
    Set(ByVal Value As Boolean)
      __IsOverCancelButton = Value
    End Set
  End Property

  ''' <summary>
  ''' Gets a value indicating whether this instance is in paint.
  ''' </summary>
  ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
  Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
    Get
      Return InPaint
    End Get
  End Property

  ''' <summary>
  ''' Gets a value indicating whether [in use].
  ''' </summary>
  ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
    Get
      Return _InUse
    End Get
  End Property

  ''' <summary>
  ''' Gets a value indicating whether [form open failed].
  ''' </summary>
  ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

#End Region


  ''' <summary>
  ''' The allow_ mark as sent
  ''' </summary>

  Private Const Allow_MarkAsSent As Boolean = True

  ''' <summary>
  ''' Initializes a new instance of the <see cref="frmApproveTransactions"/> class.
  ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  Public Sub New(ByVal pMainForm As VeniceMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()


    _MainForm = pMainForm
    AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

    _FormOpenFailed = False
    _InUse = True

    ' Default Select and Order fields.

    THIS_FORM_OrderBy = "TransactionFund, TransactionInstrument"

    ' Form Permissioning :-

    THIS_FORM_PermissionArea = Me.Name
    THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

    ' 'This' form ID

    THIS_FORM_FormID = VeniceFormID.frmApproveTransactions

    ' This form's dataset type.

    'ThisStandardDataset = RenaissanceStandardDatasets.tblSelectTransaction ' This Defines the Form Data !!! 


    ' Data object names standard to this Form type.

    'THIS_TABLENAME = ThisStandardDataset.TableName
    'THIS_DATASETNAME = ThisStandardDataset.DatasetName

    'THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

    ' Report
    SetTransactionReportMenu(RootMenu)

    ' Establish / Retrieve data objects for this form.

    myConnection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)

    'System.Console.Out.WriteLine("***BEFORE Load approve: " & CStr(Now))

    LoadApprovalsTableData()
    LoadFileTableData()
    LoadEmsxStatusTableData()

    'System.Console.Out.WriteLine("***AFTER Load approve: " & CStr(Now))
    ' Grid Fields
    Dim FieldsMenu As ToolStripMenuItem = SetFieldSelectMenu(RootMenu)

    ' Establish initial DataView and Initialise Transactions Grids.

    Dim thisCol As C1.Win.C1FlexGrid.Column
    Dim Counter As Integer

    Try
      ' Set Dummy table, for grid purposes.

      approvalDataView = New DataView(approvalTable, "True", "FundName, InstrumentDescription", DataViewRowState.CurrentRows)
      Grid_Transactions.AutoGenerateColumns = True
      Grid_Transactions.DataSource = approvalDataView
      Grid_Transactions.AutoGenerateColumns = False

      If (Grid_Transactions.Cols.Contains("TransactionNAVPercentage") = False) Then
        Grid_Transactions.Cols.Add().Name = "TransactionNAVPercentage"
        Grid_Transactions.Cols("TransactionNAVPercentage").DataType = GetType(Double)
      End If
      If (Grid_Transactions.Cols.Contains("ExpectedInstrumentWeight") = False) Then
        Grid_Transactions.Cols.Add().Name = "ExpectedInstrumentWeight"
        Grid_Transactions.Cols("ExpectedInstrumentWeight").DataType = GetType(Double)
      End If

      If (Grid_Transactions.Cols.Contains("ExpectedInstrumentPosition") = False) Then
        Grid_Transactions.Cols.Add().Name = "ExpectedInstrumentPosition"
        Grid_Transactions.Cols("ExpectedInstrumentPosition").DataType = GetType(String)
      End If

      ' Format Transactions Grid, hide unwanted columns.
      Dim Ind As Integer = 1

      Grid_Transactions.Cols("TransactionUser").Move(Ind)
      Ind = Ind + 1
      Grid_Transactions.Cols("FundName").Move(Ind)
      Ind = Ind + 1
      Grid_Transactions.Cols("InstrumentDescription").Move(Ind)
      Ind = Ind + 1
      Grid_Transactions.Cols("ExpectedInstrumentPosition").Move(Ind)
      Ind = Ind + 1
      Grid_Transactions.Cols("ExpectedInstrumentWeight").Move(Ind)
      Ind = Ind + 1
      Grid_Transactions.Cols("InstrumentISIN").Move(Ind)
      Ind = Ind + 1
      Grid_Transactions.Cols("TransactionTypeName").Move(Ind)
      Ind = Ind + 1
      Grid_Transactions.Cols("TransactionNAVPercentage").Move(Ind)
      Ind = Ind + 1
      Grid_Transactions.Cols("TransactionUnits").Move(Ind)
      Ind = Ind + 1
      Grid_Transactions.Cols("TransactionPrice").Move(Ind)
      Ind = Ind + 1
      Grid_Transactions.Cols("TransactionSettlement").Move(Ind)
      Ind = Ind + 1
      Grid_Transactions.Cols("TransactionValueDate").Move(Ind)
      Ind = Ind + 1
      Grid_Transactions.Cols("TransactionValueorAmount").Move(Ind)
      Ind = Ind + 1
      '      Grid_Transactions.Cols("TransactionBroker").Move(Ind)
      Grid_Transactions.Cols("BrokerDescription").Move(Ind)
      Ind = Ind + 1
      Grid_Transactions.Cols("TransactionTicket").Move(Ind)
      Ind = Ind + 1
    Catch ex As Exception
    End Try

    For Each thisCol In Grid_Transactions.Cols
      Try
        Select Case thisCol.Name
          Case "TransactionUser"
            thisCol.Width = 75
          Case "ExpectedInstrumentPosition"
            thisCol.Width = 94
            thisCol.Caption = "Expected Position"
          Case "ExpectedInstrumentWeight"
            thisCol.Format = "#,##0.00%"
            thisCol.Width = 88
            thisCol.Caption = "Expected Weight"
          Case "TransactionTicket"
            thisCol.Width = 100
          Case "FundName"
          Case "InstrumentDescription"
          Case "InstrumentISIN"
          Case "TransactionTypeName"
            thisCol.Width = 100
          Case "TransactionUnits"
            thisCol.Format = "#,##0.0000"
          Case "TransactionPrice"
            thisCol.Format = "#,##0.0000"
          Case "TransactionSettlement"
            thisCol.Format = "#,##0.0000"
          Case "TransactionValueDate"
            thisCol.Format = DISPLAYMEMBER_DATEFORMAT
          Case "TransactionNAVPercentage"
            thisCol.Format = "#,##0.00%"
            thisCol.Width = 50
            thisCol.Caption = "Trans. NAV %"
          Case "TransactionValueorAmount"
            thisCol.Caption = "Trade By"
          Case "BrokerDescription"
            thisCol.Caption = "Broker"
          Case Else
            thisCol.Visible = False

            Try
              If thisCol.DataType Is GetType(Date) Then
                thisCol.Format = DISPLAYMEMBER_DATEFORMAT
              ElseIf thisCol.DataType Is GetType(Integer) Then
                thisCol.Format = "#,##0"
              ElseIf thisCol.DataType Is GetType(Double) Then
                thisCol.Format = "#,##0.0000"
              End If
            Catch ex As Exception
            End Try
        End Select

        If (thisCol.Caption.ToUpper.StartsWith("TRANSACTION")) Then
          thisCol.Caption = thisCol.Caption.Substring(11)
        End If

        If (thisCol.Visible) Then
          Try
            If FieldsMenu.DropDownItems.ContainsKey("Menu_GridField_" & thisCol.Name) Then
              CType(FieldsMenu.DropDownItems("Menu_GridField_" & thisCol.Name), ToolStripMenuItem).Checked = True
            End If
          Catch ex As Exception
          End Try
        End If
      Catch ex As Exception
      End Try
    Next

    Dim cs As CellStyle = Grid_Transactions.Styles.Normal

    cs = Grid_Transactions.Styles.Add("ShortWarning")
    cs.ForeColor = Color.Black
    cs.BackColor = Color.MistyRose
    cs.Border.Color = Color.Red
    cs.Border.Style = BorderStyleEnum.Flat
    cs.Border.Direction = BorderDirEnum.Both
    cs.Border.Width = 2
    cs.Font = New Font(Grid_Transactions.Font, FontStyle.Bold)
    cs.TextAlign = TextAlignEnum.RightCenter

    Try
      For Counter = 0 To (Grid_Transactions.Cols.Count - 1)
        If (Grid_Transactions.Cols(Counter).Caption.ToUpper.StartsWith("TRANSACTION")) Then
          Grid_Transactions.Cols(Counter).Caption = Grid_Transactions.Cols(Counter).Caption.Substring(11)
        End If
      Next
    Catch ex As Exception
    End Try

    Try

      fileDataView = New DataView(fileTable, "True", "FundName, InstrumentDescription", DataViewRowState.CurrentRows)

      Grid_ToBeWritten.DataSource = fileDataView

      ' Format Approved Transactions Grid, hide unwanted columns.

      Grid_ToBeWritten.Cols("BrokerDescription").Move(1)
      Grid_ToBeWritten.Cols("TransactionUser").Move(2)
      Grid_ToBeWritten.Cols("TransactionTicket").Move(3)
      Grid_ToBeWritten.Cols("TriggerDate").Move(4)
      Grid_ToBeWritten.Cols("FundName").Move(5)
      Grid_ToBeWritten.Cols("InstrumentDescription").Move(6)
      Grid_ToBeWritten.Cols("TransactionTypeName").Move(7)
      Grid_ToBeWritten.Cols("TransactionUnits").Move(8)
      Grid_ToBeWritten.Cols("TransactionPrice").Move(9)
      Grid_ToBeWritten.Cols("TransactionSettlement").Move(10)
      Grid_ToBeWritten.Cols("TransactionValueDate").Move(11)
    Catch ex As Exception
    End Try

    For Each thisCol In Grid_ToBeWritten.Cols
      Try
        Select Case thisCol.Name
          Case "TransactionUser"
            thisCol.Width = 80
          Case "TransactionTicket"
            thisCol.Width = 80
          Case "FundName"
          Case "InstrumentDescription"
          Case "TransactionTypeName"
          Case "TransactionUnits"
            thisCol.Format = "#,##0.0000"
          Case "TransactionPrice"
            thisCol.Format = "#,##0.0000"
          Case "TransactionSettlement"
            thisCol.Format = "#,##0.0000"
          Case "BrokerDescription"
            thisCol.Format = DISPLAYMEMBER_DATEFORMAT
          Case "TriggerDate"
            thisCol.Format = "dd-MM-yyyy HH:mm:ss"
            thisCol.Caption = "Hold Trade Until"
            thisCol.Width = 120

          Case Else
            thisCol.Visible = False

            Try
              If thisCol.DataType Is GetType(Date) Then
                thisCol.Format = DISPLAYMEMBER_DATEFORMAT
              ElseIf thisCol.DataType Is GetType(Integer) Then
                thisCol.Format = "#,##0"
              ElseIf thisCol.DataType Is GetType(Double) Then
                thisCol.Format = "#,##0.0000"
              End If
            Catch ex As Exception
            End Try
        End Select

        If (thisCol.Caption.ToUpper.StartsWith("TRANSACTION")) Then
          thisCol.Caption = thisCol.Caption.Substring(11)
        End If

        If (thisCol.Visible) Then
          Try
            CType(FieldsMenu.DropDownItems("Menu_GridField_" & thisCol.Name), ToolStripMenuItem).Checked = True
          Catch ex As Exception
          End Try
        End If

      Catch ex As Exception
      End Try
    Next

    Dim cs1 As CellStyle = Grid_WorkingTrades.Styles.Normal()
    cs1 = Grid_WorkingTrades.Styles.Add("Completed")
    cs1.BackColor = Color.LightGreen
    'cs1.ForeColor = Color.Black
    'cs1.Font = New Font(Grid_WorkingTrades.Font, FontStyle.Bold)

    cs1 = Grid_WorkingTrades.Styles.Add("Working")
    'cs1.BackColor = Color.LightYellow
    cs1.BackColor = Color.FromArgb(247, 247, 106)
    'cs1.ForeColor = Color.Black
    'cs1.Font = New Font(Grid_WorkingTrades.Font, FontStyle.Bold)

    cs1 = Grid_WorkingTrades.Styles.Add("Cancelled")
    'cs1.BackColor = Color.Red
    cs1.BackColor = Color.FromArgb(253, 70, 70)
    cs1.ForeColor = Color.White
    cs1.Font = New Font(Grid_WorkingTrades.Font, FontStyle.Bold)

    cs1 = Grid_WorkingTrades.Styles.Add("Deleted") ' red also
    'cs1.BackColor = Color.Red
    cs1.BackColor = Color.FromArgb(253, 70, 70)
    cs1.ForeColor = Color.White
    cs1.Font = New Font(Grid_WorkingTrades.Font, FontStyle.Bold)

    '  status value set when order creation is rejected ( complete with no seqence number ) . It's only displayed in the gui 
    cs1 = Grid_WorkingTrades.Styles.Add("Rejected") ' red also
    'cs1.BackColor = Color.Red
    cs1.BackColor = Color.FromArgb(253, 70, 70)
    cs1.ForeColor = Color.White
    cs1.Font = New Font(Grid_WorkingTrades.Font, FontStyle.Bold)

    cs1 = Grid_WorkingTrades.Styles.Add("Error") ' red also
    'cs1.BackColor = Color.Red
    cs1.BackColor = Color.FromArgb(253, 70, 70)
    cs1.ForeColor = Color.White
    cs1.Font = New Font(Grid_WorkingTrades.Font, FontStyle.Bold)

    Try
      For Counter = 0 To (Grid_ToBeWritten.Cols.Count - 1)
        If (Grid_ToBeWritten.Cols(Counter).Caption.ToUpper.StartsWith("TRANSACTION")) Then
          Grid_ToBeWritten.Cols(Counter).Caption = Grid_ToBeWritten.Cols(Counter).Caption.Substring(11)
        End If
      Next
    Catch ex As Exception
    End Try

    '

    Try

      EmsxStatusDataView = New DataView(EmsxStatusTable, "True", "DatePartOnly DESC, OrderStatus, InstrumentDescription", DataViewRowState.CurrentRows)

      Grid_WorkingTrades.DataSource = EmsxStatusDataView

      ' Format Transactions Grid, hide unwanted columns.

      Grid_WorkingTrades.Cols("TransactionParentID").Move(1)
      Grid_WorkingTrades.Cols("SequenceNumber").Move(2)
      Grid_WorkingTrades.Cols("DateEntered").Move(3)
      Grid_WorkingTrades.Cols("EMSX_BrokerID").Move(4)
      Grid_WorkingTrades.Cols("EMSX_Ticker").Move(5)
      Grid_WorkingTrades.Cols("TransactionTypeName").Move(6)
      Grid_WorkingTrades.Cols("OrderAmount").Move(7)
      Grid_WorkingTrades.Cols("FilledAmount").Move(8)
      Grid_WorkingTrades.Cols("AveragePrice").Move(9)
      Grid_WorkingTrades.Cols("OrderStatusName").Move(10)
      Grid_WorkingTrades.Cols("BrokerStrategy").Move(11)
      Grid_WorkingTrades.Cols("Cancelled").Move(12)
      Grid_WorkingTrades.Cols("StatusString").Move(13)
      Grid_WorkingTrades.Cols("InstrumentDescription").Move(14)
      Grid_WorkingTrades.Cols("FundName").Move(15)

    Catch ex As Exception
    End Try

    For Each thisCol In Grid_WorkingTrades.Cols
      Try
        Select Case thisCol.Name
          Case "SequenceNumber"
            thisCol.Width = 80
            thisCol.Format = "###0"
            thisCol.Caption = "EMSX ID"
          Case "TransactionParentID"
            thisCol.Width = 80
            thisCol.Format = "###0"
            thisCol.Caption = "Venice ID"
          Case "FundName"
            thisCol.Width = 200
          Case "InstrumentDescription"
            thisCol.Width = 200
          Case "EMSX_BrokerID"
            thisCol.Width = 80
            thisCol.Caption = "Broker"
          Case "EMSX_Ticker"
            thisCol.Width = 80
            thisCol.Caption = "Ticker"
          Case "OrderAmount"
            'thisCol.Width = 60
            thisCol.Format = "#,##0"
            thisCol.Caption = "Order"
          Case "FilledAmount"
            'thisCol.Width = 60
            thisCol.Format = "#,##0"
            thisCol.Caption = "Filled"
          Case "AveragePrice"
            thisCol.Format = "#,##0.0000"
            thisCol.Caption = "AveragePrice"
            'thisCol.Width = 80
          Case "TransactionTypeName"
            thisCol.Width = 80
          Case "BrokerStrategy"
          Case "OrderStatusName"
          Case "StatusString"
            thisCol.Width = 400
          Case "DateEntered"
            thisCol.Format = "dd-MM-yy HH:mm:ss"
            thisCol.Caption = "Last Update"
            thisCol.Width = 120
          Case "Cancelled"
            thisCol.DataType = GetType(Boolean)
            thisCol.Width = 40

          Case Else
            thisCol.Visible = False

            Try
              If thisCol.DataType Is GetType(Date) Then
                thisCol.Format = DISPLAYMEMBER_DATEFORMAT
              ElseIf thisCol.DataType Is GetType(Integer) Then
                thisCol.Format = "#,##0"
              ElseIf thisCol.DataType Is GetType(Double) Then
                thisCol.Format = "#,##0.0000"
              End If
            Catch ex As Exception
            End Try
        End Select

        If (thisCol.Caption.ToUpper.StartsWith("TRANSACTION")) Then
          thisCol.Caption = thisCol.Caption.Substring(11)
        End If

      Catch ex As Exception
      End Try
    Next

    Try
      For Counter = 0 To (Grid_WorkingTrades.Cols.Count - 1)
        If (Grid_WorkingTrades.Cols(Counter).Caption.ToUpper.StartsWith("TRANSACTION")) Then
          Grid_WorkingTrades.Cols(Counter).Caption = Grid_WorkingTrades.Cols(Counter).Caption.Substring(11)
        End If
      Next
    Catch ex As Exception
    End Try

    ' Form Control Changed events
    AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

    AddHandler Combo_TransactionFund.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_TransactionInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_StatusGroup.SelectedValueChanged, AddressOf Me.FormControlChanged

    AddHandler Combo_TransactionFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_TransactionInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_StatusGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType

    AddHandler Combo_TransactionFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_TransactionInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_StatusGroup.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

    AddHandler Combo_TransactionFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_TransactionInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_StatusGroup.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

    AddHandler Combo_TransactionFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_TransactionInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_StatusGroup.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

    'lotfi:
    AddHandler MainForm.NoKansasMsgReceived, AddressOf ShowKansasStatusMessages

    ' Set up the ToolTip
    MainForm.SetFormToolTip(Me, FormTooltip)

    'System.Console.Out.WriteLine("***END OF ApproveTrans form Load: " & CStr(Now))

  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
  ''' <summary>
  ''' Resets the form.
  ''' </summary>
  Public Sub ResetForm() Implements StandardVeniceForm.ResetForm
    THIS_FORM_OrderBy = "TransactionFund, TransactionInstrument"

    Call Form_Load(Me, New System.EventArgs)
  End Sub

  ''' <summary>
  ''' Closes the form.
  ''' </summary>
  Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
    ALWAYS_CLOSE_THIS_FORM = True
    Me.Close()
  End Sub

  ''' <summary>
  ''' Handles the Load event of the Form control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
    _FormOpenFailed = False
    _InUse = True

    ' Initialise Data structures. Connection, Adaptor and Dataset.

    If Not (MainForm Is Nothing) Then
      FormIsValid = True
    Else
      MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (myConnection Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (approvalAdaptor Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (approvalDataset Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (fileAdaptor Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (fileDataset Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (EuroNAVs IsNot Nothing) Then
      EuroNAVs.Clear()
      EuroNAVs = Nothing
    End If

    If (FXRates IsNot Nothing) Then
      FXRates.Clear()
      FXRates = Nothing
    End If

    ' Check User permissions
    Try
      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

        FormIsValid = False
        _FormOpenFailed = True

        Exit Sub
      End If
    Catch ex As Exception
      FormIsValid = False
      _FormOpenFailed = True

      Exit Sub
    End Try

    ' Initialse form

    InPaint = True
    IsOverCancelButton = False

    ' Initialise main select controls
    ' Build Sorted data list from which this form operates

    Try
      TabControl_Transactions.SelectedTab = Tab_Approval

      Call SetFundCombo()
      Call SetGroupCombo()
      Call SetInstrumentCombo()
      ' lotfi: commented call replaced by text: Trying to connect...
      'Call ShowKansasStatusMessages()
      Text_KansasStatus.Text = "Trying to connect to Kansas..."
      Text_KansasStatus.ReadOnly = False
      Text_KansasStatus.ForeColor = Color.Red
      Text_KansasStatus.ReadOnly = True

      Me.Combo_TransactionFund.SelectedIndex = -1
      Me.Combo_TransactionInstrument.SelectedIndex = -1
      Me.Date_TriggerDate.Value = Now.Date.AddSeconds(1)

      If (Has_MO_Permissions And Has_FM_Permissions) Then
        Me.Combo_StatusGroup.SelectedValue = CInt(RenaissanceGlobals.StatusGroups.Pending_All)
      ElseIf (Has_MO_Permissions) Then
        Me.Combo_StatusGroup.SelectedValue = CInt(RenaissanceGlobals.StatusGroups.Pending_MO)
      ElseIf (Has_FM_Permissions) Then
        Me.Combo_StatusGroup.SelectedValue = CInt(RenaissanceGlobals.StatusGroups.Pending_FM)
      End If

      Call SetSortedRows()

      Call MainForm.SetComboSelectionLengths(Me)

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error initialising Select Transaction Form.", ex.StackTrace, True)
    End Try

    InPaint = False

    Grid_Transactions.Select(0, 0)

  End Sub

  ''' <summary>
  ''' Handles the Closing event of the frm control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
  Private Sub frm_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    Dim HideForm As Boolean

    ' Hide or Close this form ?
    ' All depends on how many of this form type are Open or in Cache...

    _InUse = False

    If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
      HideForm = False
    Else
      HideForm = True
      If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
        HideForm = False
      End If
    End If

    If HideForm = True Then
      MainForm.HideInFormsCollection(Me)
      Me.Hide() ' NPP Fix

      e.Cancel = True
    Else
      Try
        MainForm.RemoveFromFormsCollection(Me)
        RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

        RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

        RemoveHandler Combo_TransactionFund.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_TransactionInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_StatusGroup.SelectedValueChanged, AddressOf Me.FormControlChanged

        RemoveHandler Combo_TransactionFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_TransactionInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_StatusGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        RemoveHandler Combo_TransactionFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_TransactionInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_StatusGroup.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

        RemoveHandler Combo_TransactionFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_TransactionInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_StatusGroup.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

        RemoveHandler Combo_TransactionFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_TransactionInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_StatusGroup.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

      Catch ex As Exception
      End Try
    End If

  End Sub


#End Region

  ' Routine to handle changes / updates to tables by this and other windows.
  ' If this, or any other, form posts a change to a table, then it will invoke an update event 
  ' detailing what tables have been altered.
  ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
  ' the 'VeniceAutoUpdate' event of the main Venice form.
  ' Each form may them react as appropriate to changes in any table that might impact it.
  '
  ''' <summary>
  ''' Autoes the update.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    Dim OrgInPaint As Boolean
    Dim KnowledgeDateChanged As Boolean
    Dim RefreshGrid As Boolean
    Dim RefreshPendingData As Boolean = False
    Dim RefreshData As Boolean = False

    If (Not Me.Created) OrElse (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then
      Exit Sub
    End If

    OrgInPaint = InPaint

    Try
      InPaint = True
      KnowledgeDateChanged = False
      RefreshGrid = False

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
        KnowledgeDateChanged = True
      End If
    Catch ex As Exception
    End Try

    ' ****************************************************************
    ' Check for changes relevant to this form
    ' ****************************************************************


    ' Changes to the tblFund table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFund) = True) Or KnowledgeDateChanged Then
        Call SetFundCombo()
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblFund", ex.StackTrace, True)
    End Try

    ' Changes to the tblInstrument table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrument) = True) Or KnowledgeDateChanged Then
        Call SetInstrumentCombo()
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblInstrument", ex.StackTrace, True)
    End Try

    ' Changes to the tblSophisStatusGroups table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblSophisStatusGroups) = True) Or KnowledgeDateChanged Then
        Call SetGroupCombo()
        RefreshData = True
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblSophisStatusGroups", ex.StackTrace, True)
    End Try

    ' Changes to the tblTransaction table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblTransaction) = True) Or KnowledgeDateChanged Then
        RefreshGrid = True
        RefreshData = True
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblTransaction", ex.StackTrace, True)
    End Try

    ' Changes to the tblTradeFilePending table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblTradeFilePending) = True) Or KnowledgeDateChanged Then
        RefreshPendingData = True
        RefreshData = True
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblTradeFilePending", ex.StackTrace, True)
    End Try

    ' Changes to the tblTradeFileEmsxStatus table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblTradeFileEmsxStatus) = True) Or KnowledgeDateChanged Then
        Call LoadEmsxStatusTableData()
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblTradeFileEmsxStatus", ex.StackTrace, True)
    End Try

    ' Changes to the KansasStatusMessage table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KansasStatusMessage) = True) Or KnowledgeDateChanged Then
        ShowKansasStatusMessages()
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : KansasStatusMessage", ex.StackTrace, True)
    End Try

    ' Changes to the KnowledgeDate :-
    Try
      If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
        RefreshGrid = True
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: KnowledgeDate", ex.StackTrace, True)
    End Try

    ' Changes to the tblUserPermissions table :-

    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

        ' Check ongoing permissions.

        Call CheckPermissions()
        If (HasReadPermission = False) Then
          Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

          FormIsValid = False
          InPaint = OrgInPaint
          Me.Close()
          Exit Sub
        End If

        RefreshGrid = True

      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: tblUserPermissions", ex.StackTrace, True)
    End Try


    ' ****************************************************************
    ' Changes to the Main FORM table :-
    ' (or the tblTransactions table)
    ' ****************************************************************

    Try
      If (RefreshData) Then

        'lotfi: to avoid first call of loadApproval caused by pendingtrades relaod
        If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblTransaction) = True) Then
          Call LoadApprovalsTableData()
        End If
      End If

      If (RefreshPendingData) Then
        Call LoadFileTableData()
      End If

      If (RefreshGrid) Or (RefreshData) Or (RefreshPendingData) Or KnowledgeDateChanged Then

        ' Re-Set Controls etc.
        Call SetSortedRows()

      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: Main Form table", ex.StackTrace, True)
    End Try

    InPaint = OrgInPaint

  End Sub

  ''' <summary>
  ''' Loads the file table data.
  ''' </summary>
  Private Sub LoadFileTableData()
    ' *******************************************************************************
    ' Function to populate the underlying data table.
    '
    ' This table is fetched on a bespoke basis, it is not part of the central managed-table
    ' system.
    ' *******************************************************************************


    Try
      If (fileAdaptor Is Nothing) Then
        fileAdaptor = New SqlDataAdapter()
        MainForm.MainAdaptorHandler.Set_AdaptorCommands(myConnection, fileAdaptor, "tblSelectTransactionFilePending")
      End If

      ' Set Parameters

      If (fileAdaptor.SelectCommand.Parameters.Contains("@KnowledgeDate")) Then
        fileAdaptor.SelectCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate
      End If

      ' Check DS & Table.

      If (fileDataset Is Nothing) Then
        fileDataset = New RenaissanceDataClass.DSSelectTransaction
      End If

      fileTable = fileDataset.tblSelectTransaction

      If fileTable.Rows.Count > 0 Then
        fileTable.Rows.Clear()
      End If

      '

      SyncLock fileAdaptor.SelectCommand.Connection
        MainForm.LoadTable_Custom(fileTable, fileAdaptor.SelectCommand)
        'fileTable.Load(fileAdaptor.SelectCommand.ExecuteReader)
      End SyncLock

      ' Add Calculated fields

      If fileTable.Columns.Contains("BrokerAccount") = False Then
        fileTable.Columns.Add(New DataColumn("BrokerAccount", GetType(String)))
      End If

      If fileTable.Rows.Count > 0 Then
        Dim ThisBrokerAccount As RenaissanceDataClass.DSBrokerAccounts.tblBrokerAccountsRow = Nothing
        Dim TransactionFund As Integer
        Dim TransactionInstrument As Integer
        Dim InstrumentTypeID As Integer
        Dim thisInstrumentRow As RenaissanceDataClass.DSInstrument.tblInstrumentRow = Nothing
        Dim InstrumentCurrencyID As Integer = 0
        Dim TransactionExchangeID As Integer = 0
        Dim BrokerID As Integer = 0
        Dim thisRow As RenaissanceDataClass.DSSelectTransaction.tblSelectTransactionRow

        For Each thisRow In fileTable.Rows

          TransactionFund = thisRow.TransactionFund
          InstrumentTypeID = thisRow.InstrumentType
          BrokerID = thisRow.TransactionBroker

          If (thisRow.Table.Columns.Contains("TransactionInstrument")) Then
            TransactionInstrument = CInt(Nz(thisRow("TransactionInstrument"), 0))
          ElseIf (thisRow.Table.Columns.Contains("InstrumentID")) Then
            TransactionInstrument = CInt(Nz(thisRow("InstrumentID"), 0))
          End If

          thisInstrumentRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, TransactionInstrument), RenaissanceDataClass.DSInstrument.tblInstrumentRow)

          InstrumentCurrencyID = Nz(thisInstrumentRow.InstrumentCurrencyID, 0)

          ThisBrokerAccount = GetExpectedBrokerAccount(MainForm, TransactionFund, InstrumentTypeID, BrokerID, TransactionExchangeID, InstrumentCurrencyID, TransactionInstrument)

          Select Case CType(ThisBrokerAccount.BrokerID, ExecutionBrokers)

            Case ExecutionBrokers.BNP_Paris

              thisRow("BrokerAccount") = ThisBrokerAccount.AccountNumber

            Case ExecutionBrokers.BNP_Luxembourg

              thisRow("BrokerAccount") = ThisBrokerAccount.AccountNumber

          End Select

        Next

      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in LoadFileTableData().", ex.StackTrace, True)
    End Try

  End Sub

  ''' <summary>
  ''' Loads the file table data.
  ''' </summary>
  Private Sub LoadEmsxStatusTableData()
    ' *******************************************************************************
    ' Function to populate the underlying data table.
    '
    ' This table is fetched on a bespoke basis, it is not part of the central managed-table
    ' system.
    ' *******************************************************************************


    Try
      If (EmsxStatusCommand Is Nothing) Then
        EmsxStatusCommand = New SqlCommand()

        EmsxStatusCommand.CommandType = CommandType.StoredProcedure
        EmsxStatusCommand.CommandText = "adp_tblTradeFileEmsxStatus_SelectForApproveTransactions"
        EmsxStatusCommand.Connection = myConnection
        EmsxStatusCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime)

      End If

      ' Set Parameters

      If (EmsxStatusCommand.Parameters.Contains("@KnowledgeDate")) Then
        EmsxStatusCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate
      End If

      ' Check DS & Table.

      If (EmsxStatusDataset Is Nothing) Then
        EmsxStatusDataset = New RenaissanceDataClass.DSTradeFileEmsxStatus
      End If

      EmsxStatusTable = EmsxStatusDataset.tblTradeFileEmsxStatus

      If EmsxStatusTable.Rows.Count > 0 Then
        EmsxStatusTable.Rows.Clear()
      End If

      '

      SyncLock EmsxStatusCommand.Connection

        MainForm.LoadTable_Custom(EmsxStatusTable, EmsxStatusCommand)
        ' EmsxStatusTable.Load(EmsxStatusCommand.ExecuteReader)
      End SyncLock

      ' Add Calculated fields

      If EmsxStatusTable.Columns.Contains("OrderStatusName") = False Then
        EmsxStatusTable.Columns.Add(New DataColumn("OrderStatusName", GetType(String)))
      End If

      If EmsxStatusTable.Columns.Contains("FundName") = False Then
        EmsxStatusTable.Columns.Add(New DataColumn("FundName", GetType(String)))
      End If

      If EmsxStatusTable.Columns.Contains("InstrumentDescription") = False Then
        EmsxStatusTable.Columns.Add(New DataColumn("InstrumentDescription", GetType(String)))
      End If

      If EmsxStatusTable.Columns.Contains("TransactionTypeName") = False Then
        EmsxStatusTable.Columns.Add(New DataColumn("TransactionTypeName", GetType(String)))
      End If
      If EmsxStatusTable.Columns.Contains("BrokerStrategy") = False Then
        EmsxStatusTable.Columns.Add(New DataColumn("BrokerStrategy", GetType(String)))
      End If

      If EmsxStatusTable.Rows.Count > 0 Then
        Dim TransactionParentID As Integer
        Dim TransactionRow As DSSelectTransaction.tblSelectTransactionRow
        '  Dim InstrumentTypeID As Integer
        '  Dim thisInstrumentRow As RenaissanceDataClass.DSInstrument.tblInstrumentRow = Nothing
        '  Dim InstrumentCurrencyID As Integer = 0
        '  Dim TransactionExchangeID As Integer = 0
        '  Dim BrokerID As Integer = 0
        Dim thisRow As RenaissanceDataClass.DSTradeFileEmsxStatus.tblTradeFileEmsxStatusRow


        For Each thisRow In EmsxStatusTable.Rows

          TransactionParentID = thisRow.TransactionParentID

          TransactionRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblSelectTransaction, TransactionParentID), DSSelectTransaction.tblSelectTransactionRow)

          If ([Enum].IsDefined(GetType(EMSX_OrderStatus), thisRow.OrderStatus)) Then
            thisRow("OrderStatusName") = CType(thisRow.OrderStatus, EMSX_OrderStatus).ToString()
          Else
            thisRow("OrderStatusName") = thisRow.OrderStatus.ToString
          End If

          If (TransactionRow IsNot Nothing) Then
            thisRow("FundName") = TransactionRow.FundName
            thisRow("InstrumentDescription") = TransactionRow.InstrumentDescription
            thisRow("TransactionTypeName") = TransactionRow.TransactionTypeName
            thisRow("BrokerStrategy") = TransactionRow.TransactionBrokerStrategy
          End If

        Next

      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in LoadEmsxStatusTableData().", ex.StackTrace, True)
    End Try

  End Sub

  ''' <summary>
  ''' Loads all 'Pending approval' transactions.
  ''' </summary>
  Private Sub LoadApprovalsTableData()
    ' *******************************************************************************
    ' Function to populate the underlying data table.
    '
    ' This table is fetched on a bespoke basis, it is not part of the central managed-table
    ' system.
    ' *******************************************************************************

    'System.Console.Out.WriteLine("***LoadApprovalsTableData start: " & CStr(Now))
    Try
      If (approvalAdaptor Is Nothing) Then
        approvalAdaptor = New SqlDataAdapter()
        MainForm.MainAdaptorHandler.Set_AdaptorCommands(myConnection, approvalAdaptor, "tblSelectTransactionByGroup")
      End If

      ' Set Parameters

      If (approvalAdaptor.SelectCommand.Parameters.Contains("@GroupID")) Then
        approvalAdaptor.SelectCommand.Parameters("@GroupID").Value = CInt(RenaissanceGlobals.StatusGroups.Pending_All)
      End If
      If (approvalAdaptor.SelectCommand.Parameters.Contains("@KnowledgeDate")) Then
        approvalAdaptor.SelectCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate
      End If

      ' Check DS & Table.

      If (approvalDataset Is Nothing) Then
        approvalDataset = New RenaissanceDataClass.DSSelectTransaction
      End If

      approvalTable = approvalDataset.tblSelectTransaction

      If (approvalTable.Columns.Contains("TransactionNAVPercentage") = False) Then
        approvalTable.Columns.Add("TransactionNAVPercentage", GetType(Double))
      End If
      If (approvalTable.Columns.Contains("ExpectedInstrumentWeight") = False) Then
        approvalTable.Columns.Add("ExpectedInstrumentWeight", GetType(Double))
      End If

      If (approvalTable.Columns.Contains("ExpectedInstrumentPosition") = False) Then
        approvalTable.Columns.Add("ExpectedInstrumentPosition", GetType(String))
      End If

      If approvalTable.Rows.Count > 0 Then
        approvalTable.Rows.Clear()
      End If

      '

      SyncLock approvalAdaptor.SelectCommand.Connection
        MainForm.LoadTable_Custom(approvalTable, approvalAdaptor.SelectCommand)
        'approvalTable.Load(approvalAdaptor.SelectCommand.ExecuteReader)
      End SyncLock

      '            CalculateShortPositions()
      CalculatePositions()

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in LoadApprovalsTableData().", ex.StackTrace, True)
    End Try
    'System.Console.Out.WriteLine("***LoadApprovalsTableData end: " & CStr(Now))

  End Sub

  Private Sub CalculatePositions(Optional ByVal doRecalculateEuroFundNAVs As Boolean = True)
    ' *******************************************************************************
    ' *******************************************************************************

    'System.Console.Out.WriteLine("***CALCULATE POSITION start: " & CStr(Now))
    'Dim tempEuroNAVs As Dictionary(Of Integer, Double)
    Try
      Dim thisRow As DSSelectTransaction.tblSelectTransactionRow
      Dim RowCounter As Integer
      Dim FundID As Integer
      Dim InstrumentID As Integer
      'Dim PositionKey As ULong
      'Dim TradeDictionary As New Dictionary(Of ULong, Double)
      Dim ExistingTrade As Double = 0
      Dim ExistingPosition As Double = 0
      Dim TransactionsDS As RenaissanceDataClass.DSTransaction = Nothing
      Dim SelectedTransactions() As RenaissanceDataClass.DSTransaction.tblTransactionRow = Nothing
      Dim thisTransaction As RenaissanceDataClass.DSTransaction.tblTransactionRow

      Dim InstrumentRow As DSInstrument.tblInstrumentRow


      'tempEuroNAVs = New Dictionary(Of Integer, Double)
      If doRecalculateEuroFundNAVs Then
        EuroNAVs = Nothing
        EuroNAVs = New Dictionary(Of Integer, Double)
      End If

      TransactionsDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblTransaction)

      ' set EuroNavs
      'lotfi: to gain time set only concerned funds FundEuroNAVs ( in 99% of cases, all the pending trades to approve in one time are in the same fund...)
      If (approvalTable IsNot Nothing) AndAlso (approvalTable.Rows.Count > 0) Then
        For RowCounter = 0 To (approvalTable.Rows.Count - 1)
          thisRow = approvalTable.Rows(RowCounter)
          FundID = thisRow.TransactionFund
          'If Not tempEuroNAVs.ContainsKey(FundID) Then
          '  tempEuroNAVs.Add(FundID, getEuroFundNAV(FundID))
          'End If
          If Not EuroNAVs.ContainsKey(FundID) Then
            EuroNAVs.Add(FundID, getEuroFundNAV(FundID))
          End If
        Next RowCounter
      End If

      'get euroFX
      If FXRates Is Nothing Then
        FXRates = New Dictionary(Of Integer, Double)
      End If

      Dim EuroFX As Double = MainForm.GetBestFX(3, Now().Date)
      Dim InstFX As Double
      Dim FXRate As Double
      Dim FundEuroValue As Double

      Dim InstEuroValue As Double

      FXRates(3) = EuroFX

      If (EuroFX = 0.0#) Then
        MainForm.LogError(Me.Name, LOG_LEVELS.Error, "", "Euro FX Rate is zero.", "", False)
      End If

      ' Populate expected instrument weight column

      If (TransactionsDS IsNot Nothing) AndAlso (approvalTable IsNot Nothing) AndAlso (approvalTable.Rows.Count > 0) Then

        SyncLock approvalTable

          Try

            For RowCounter = 0 To (approvalTable.Rows.Count - 1)
              thisRow = approvalTable.Rows(RowCounter)
              FundID = thisRow.TransactionFund

              InstrumentID = thisRow.TransactionInstrument

              ' Get existing position

              SelectedTransactions = TransactionsDS.tblTransaction.Select("TransactionFund=" & FundID.ToString & " AND TransactionInstrument=" & InstrumentID.ToString)

              ExistingPosition = 0.0#
              If (SelectedTransactions IsNot Nothing) AndAlso (SelectedTransactions.Length > 0) Then
                For Each thisTransaction In SelectedTransactions
                  ExistingPosition += thisTransaction.TransactionSignedUnits
                Next
              End If
              ' display position

              ' to avoid the short warning for very tiny position  (normally a position cant be smaller than +- one unit )
              If Math.Abs(ExistingPosition) < 0.001 Then
                ExistingPosition = 0
              End If

              If (ExistingPosition < 0.0#) Then
                thisRow("ExpectedInstrumentPosition") = "Short  " & Math.Abs(ExistingPosition).ToString("#,##0.0000")
              Else
                thisRow("ExpectedInstrumentPosition") = ExistingPosition.ToString("#,##0.0000")
              End If

              'get instrument weight
              'If (tempEuroNAVs.ContainsKey(thisRow.TransactionFund)) Then
              'FundEuroValue = tempEuroNAVs(thisRow.TransactionFund)
              If (EuroNAVs.ContainsKey(thisRow.TransactionFund)) Then
                FundEuroValue = EuroNAVs(thisRow.TransactionFund)

                InstrumentRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, thisRow.TransactionInstrument)

                If (FXRates.ContainsKey(InstrumentRow.InstrumentCurrencyID) = False) Then
                  InstFX = MainForm.GetBestFX(InstrumentRow.InstrumentCurrencyID, Now().Date)
                  FXRates(InstrumentRow.InstrumentCurrencyID) = InstFX
                Else
                  InstFX = FXRates(InstrumentRow.InstrumentCurrencyID)
                End If

                FXRate = InstFX / EuroFX

                Dim transEuroValue As Double

                Dim InstPriceValue As Double
                Dim InstPriceDate As Date
                Call GetInstrumentPrice(thisRow.TransactionInstrument, InstPriceValue, InstPriceDate)

                If (InstPriceValue = 0) Then
                  MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", " Expected Instrument weight calculation warning: price not found for Instr. ID=" + CStr(thisRow.TransactionInstrument) + "), entered transaction price is considered ", "", False)
                  InstPriceValue = thisRow.TransactionPrice

                End If

                If (thisRow.TransactionType = TransactionTypes.BuyFX) Or (thisRow.TransactionType = TransactionTypes.SellFX) OrElse (thisRow.TransactionType = TransactionTypes.BuyFXForward) Or (thisRow.TransactionType = TransactionTypes.SellFXForward) Then
                  InstEuroValue = ExistingPosition * FXRate
                  transEuroValue = thisRow.TransactionUnits * FXRate
                Else
                  InstEuroValue = ExistingPosition * InstPriceValue * InstrumentRow.InstrumentContractSize * InstrumentRow.InstrumentMultiplier * FXRate
                  transEuroValue = thisRow.TransactionUnits * InstPriceValue * InstrumentRow.InstrumentContractSize * InstrumentRow.InstrumentMultiplier * FXRate
                End If

                'Console.WriteLine(" Instrumnt weight calculation: Instrument Id=" + CStr(thisRow.TransactionInstrument) + " priceLevel=" + CStr(InstPriceValue) + ", FxRate=" + CStr(FXRate) + ", InstEuroValue=" + CStr(InstEuroValue) + ", FundEuroValue=" + CStr(FundEuroValue))
                If InstEuroValue = 0 Then
                  thisRow("ExpectedInstrumentWeight") = 0.0#
                ElseIf FundEuroValue <> 0 Then
                  thisRow("ExpectedInstrumentWeight") = InstEuroValue / FundEuroValue
                Else
                  thisRow("ExpectedInstrumentWeight") = 0.0#
                  MainForm.LogError(Me.Name, LOG_LEVELS.Error, "", " EuroFundNav is 0 for fund (" + CStr(thisRow.TransactionFund) + ") !! although containing non zero poisition ", "", False)
                End If

                ' show Transaction NAV %
                If (Grid_Transactions.Cols.Contains("TransactionNAVPercentage")) Then
                  If transEuroValue = 0 Then
                    thisRow("TransactionNAVPercentage") = 0.0#
                  ElseIf FundEuroValue <> 0 Then
                    thisRow("TransactionNAVPercentage") = transEuroValue / FundEuroValue
                  Else
                    thisRow("TransactionNAVPercentage") = 0.0#
                    MainForm.LogError(Me.Name, LOG_LEVELS.Error, "", " EuroFundNav is 0 for fund (" + CStr(thisRow.TransactionFund) + ") !! although containing non zero poisition ", "", False)
                  End If

                End If


              Else
                thisRow("ExpectedInstrumentWeight") = 0.0#
                If (Grid_Transactions.Cols.Contains("TransactionNAVPercentage")) Then
                  thisRow("TransactionNAVPercentage") = 0.0#
                End If
                MainForm.LogError(Me.Name, LOG_LEVELS.Error, "", " EuroNavs doesnt contain Transaction Fund !! (" + CStr(thisRow.TransactionFund) + ")", "", False)
              End If

            Next

          Catch ex As Exception
          End Try

        End SyncLock

      End If


    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in CalculatePositions().", ex.StackTrace, True)
    End Try
    'If tempEuroNAVs IsNot Nothing Then
    '  tempEuroNAVs.Clear()
    '  tempEuroNAVs = Nothing
    'End If

    'System.Console.Out.WriteLine("***CALCULATE POSITION end: " & CStr(Now))

  End Sub


  Private Sub CalculatePositions_old()
    ' *******************************************************************************
    ' *******************************************************************************

    Try
      Dim thisRow As DSSelectTransaction.tblSelectTransactionRow
      Dim RowCounter As Integer
      Dim FundID As Integer
      Dim InstrumentID As Integer
      'Dim PositionKey As ULong
      'Dim TradeDictionary As New Dictionary(Of ULong, Double)
      Dim ExistingTrade As Double = 0
      Dim ExistingPosition As Double = 0
      Dim TransactionsDS As RenaissanceDataClass.DSTransaction = Nothing
      Dim SelectedTransactions() As RenaissanceDataClass.DSTransaction.tblTransactionRow = Nothing
      Dim thisTransaction As RenaissanceDataClass.DSTransaction.tblTransactionRow

      Dim InstrumentRow As DSInstrument.tblInstrumentRow
      ' set EuroNavs
      If (EuroNAVs Is Nothing) OrElse (EuroNAVs.Count = 0) Then
        Dim NAVCommand As New SqlCommand
        Dim DataReader As New VeniceMain.GetDataReaderClass
        Dim NAVTable As New DataTable
        Dim NAVRow As DataRow

        ' 
        If (EuroNAVs Is Nothing) Then
          EuroNAVs = New Dictionary(Of Integer, Double)
        End If

        If (EuroNAVs.Count = 0) Then
          Try
            NAVCommand.CommandType = CommandType.StoredProcedure
            NAVCommand.CommandText = "spu_GetFundNAVs"
            NAVCommand.Parameters.Add(New SqlParameter("@FundID", SqlDbType.Int)).Value = 0
            NAVCommand.Parameters.Add(New SqlParameter("@ValueDate", SqlDbType.DateTime)).Value = Now()
            NAVCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", SqlDbType.DateTime)).Value = RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW

            NAVCommand.Connection = MainForm.GetVeniceConnection

            DataReader.MyCommand = NAVCommand

            MainForm.RunDataReader(DataReader)

            If (DataReader.myReader Is Nothing) Then
              ' MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "DataReader.myReader is nothing in Check_ShowPercentOfNAV.", "", False)
              MainForm.LoadTable_Custom(NAVTable, NAVCommand)
              'NAVTable.Load(NAVCommand.ExecuteReader)
            Else
              'MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "DataReader.myReader OK in Check_ShowPercentOfNAV.", "", False)
              NAVTable.Load(DataReader.myReader)
            End If

            If (NAVTable.Rows.Count > 0) Then
              ' MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "NAVTable has " & NAVTable.Rows.Count.ToString & " rows.", "", False)
              For Each NAVRow In NAVTable.Rows
                EuroNAVs.Add(CInt(NAVRow("FundID")), CDbl(NAVRow("EURO_VALUE")))
              Next
              NAVTable.Rows.Clear()
              '  Else
              ' MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "NAVTable has no rows.", "", False)
            End If

            NAVTable = Nothing

          Catch ex As Exception
            EuroNAVs.Clear()
            MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error execing spu_GetFundNAVs in Check_ShowExpectedInstrumentWeight.", ex.StackTrace, False)
          Finally
            If (NAVCommand.Connection IsNot Nothing) Then
              If (NAVCommand.Connection.State And ConnectionState.Open) Then
                NAVCommand.Connection.Close()
              End If

              NAVCommand.Connection = Nothing
            End If
          End Try
        End If

      End If

      'get euroFX
      If FXRates Is Nothing Then
        FXRates = New Dictionary(Of Integer, Double)
      End If

      Dim EuroFX As Double = MainForm.GetBestFX(3, Now().Date)
      Dim InstFX As Double
      Dim FXRate As Double
      Dim FundEuroValue As Double

      Dim InstEuroValue As Double

      FXRates(3) = EuroFX

      If (EuroFX = 0.0#) Then
        MainForm.LogError(Me.Name, LOG_LEVELS.Error, "", "Euro FX Rate is zero.", "", False)
      End If

      ' Populate expected instrument weight column

      TransactionsDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblTransaction)

      If (TransactionsDS IsNot Nothing) AndAlso (approvalTable IsNot Nothing) AndAlso (approvalTable.Rows.Count > 0) Then

        SyncLock approvalTable

          Try

            For RowCounter = 0 To (approvalTable.Rows.Count - 1)
              thisRow = approvalTable.Rows(RowCounter)
              FundID = thisRow.TransactionFund

              InstrumentID = thisRow.TransactionInstrument

              ' Get existing position

              SelectedTransactions = TransactionsDS.tblTransaction.Select("TransactionFund=" & FundID.ToString & " AND TransactionInstrument=" & InstrumentID.ToString)

              ExistingPosition = 0.0#
              If (SelectedTransactions IsNot Nothing) AndAlso (SelectedTransactions.Length > 0) Then
                For Each thisTransaction In SelectedTransactions
                  ExistingPosition += thisTransaction.TransactionSignedUnits
                Next
              End If
              ' display position

              ' to avoid the short warning for very tiny position  (normally a position cant be smaller than +- one unit )
              If Math.Abs(ExistingPosition) < 0.001 Then
                ExistingPosition = 0
              End If

              If (ExistingPosition < 0.0#) Then
                thisRow("ExpectedInstrumentPosition") = "Short " & Math.Abs(ExistingPosition).ToString("#,##0.0000")
              Else
                thisRow("ExpectedInstrumentPosition") = ExistingPosition.ToString("#,##0.0000")
              End If

              'get instrument weight
              If (EuroNAVs.ContainsKey(thisRow.TransactionFund)) Then
                FundEuroValue = EuroNAVs(thisRow.TransactionFund)

                InstrumentRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, thisRow.TransactionInstrument)

                If (FXRates.ContainsKey(InstrumentRow.InstrumentCurrencyID) = False) Then
                  InstFX = MainForm.GetBestFX(InstrumentRow.InstrumentCurrencyID, Now().Date)
                  FXRates(InstrumentRow.InstrumentCurrencyID) = InstFX
                Else
                  InstFX = FXRates(InstrumentRow.InstrumentCurrencyID)
                End If

                FXRate = InstFX / EuroFX

                Dim InstPriceValue As Double
                Dim InstPriceDate As Date
                Call GetInstrumentPrice(thisRow.TransactionInstrument, InstPriceValue, InstPriceDate)

                If (InstPriceValue = 0) Then
                  MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", " Expected Instrument weight calculation warning: price not found for Instr. ID=" + CStr(thisRow.TransactionInstrument) + "), entered transaction price is considered ", "", False)
                  InstPriceValue = thisRow.TransactionPrice

                End If

                If (thisRow.TransactionType = TransactionTypes.BuyFX) Or (thisRow.TransactionType = TransactionTypes.SellFX) OrElse (thisRow.TransactionType = TransactionTypes.BuyFXForward) Or (thisRow.TransactionType = TransactionTypes.SellFXForward) Then
                  InstEuroValue = ExistingPosition * FXRate
                Else
                  InstEuroValue = ExistingPosition * InstPriceValue * InstrumentRow.InstrumentContractSize * InstrumentRow.InstrumentMultiplier * FXRate
                End If

                'Console.WriteLine(" Instrumnt weight calculation: Instrument Id=" + CStr(thisRow.TransactionInstrument) + " priceLevel=" + CStr(InstPriceValue) + ", FxRate=" + CStr(FXRate) + ", InstEuroValue=" + CStr(InstEuroValue) + ", FundEuroValue=" + CStr(FundEuroValue))
                If InstEuroValue = 0 Then
                  thisRow("ExpectedInstrumentWeight") = 0.0#
                ElseIf FundEuroValue <> 0 Then
                  thisRow("ExpectedInstrumentWeight") = InstEuroValue / FundEuroValue
                Else
                  thisRow("ExpectedInstrumentWeight") = 0.0#
                  MainForm.LogError(Me.Name, LOG_LEVELS.Error, "", " EuroFundNav is 0 for fund (" + CStr(thisRow.TransactionFund) + ") !! although containing non zero poisition ", "", False)
                End If

              Else
                thisRow("ExpectedInstrumentWeight") = 0.0#
                MainForm.LogError(Me.Name, LOG_LEVELS.Error, "", " EuroNavs doesnt contain Transaction Fund !! (" + CStr(thisRow.TransactionFund) + ")", "", False)
              End If

            Next

          Catch ex As Exception
          End Try

        End SyncLock

      End If


    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in CalculatePositions().", ex.StackTrace, True)
    End Try



  End Sub



#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

  ''' <summary>
  ''' Gets the transaction select string.
  ''' </summary>
  ''' <param name="OnlyUsertblTransactionFields">if set to <c>true</c> [only usertbl transaction fields].</param>
  ''' <returns>System.String.</returns>
  Private Function GetTransactionSelectString(Optional ByVal OnlyUsertblTransactionFields As Boolean = False) As String
    ' *******************************************************************************
    ' Build a Select String appropriate to the form status.
    ' *******************************************************************************

    Dim SelectString As String
    Dim FieldSelectString As String

    SelectString = ""
    FieldSelectString = ""
    Me.MainForm.SetToolStripText(Label_Status, "")

    Try

      ' Select on Fund...

      If (Me.Combo_TransactionFund.SelectedIndex > 0) Then
        If IsFieldTransactionField("TransactionFund") = True Then
          SelectString = "(TransactionFund=" & Combo_TransactionFund.SelectedValue.ToString & ")"
        End If
      End If

      ' Select on Instrument

      If (Me.Combo_TransactionInstrument.SelectedIndex > 0) Then
        If IsFieldTransactionField("TransactionInstrument") = True Then
          If (SelectString.Length > 0) Then
            SelectString &= " AND "
          End If
          SelectString &= "(TransactionInstrument=" & Combo_TransactionInstrument.SelectedValue.ToString & ")"
        End If
      End If

      ' Select on TransactionGroup

      If (IsNumeric(Me.Combo_StatusGroup.SelectedValue)) AndAlso (CInt(Combo_StatusGroup.SelectedValue) > 0) Then
        If IsFieldTransactionField("TransactionTradeStatusID") = True Then
          If (SelectString.Length > 0) Then
            SelectString &= " AND "
          End If

          Dim StatusItemsDS As RenaissanceDataClass.DSTradeStatusGroupItems
          Dim SelectedRows() As RenaissanceDataClass.DSTradeStatusGroupItems.tblTradeStatusGroupItemsRow = Nothing
          Dim thisItemRow As RenaissanceDataClass.DSTradeStatusGroupItems.tblTradeStatusGroupItemsRow = Nothing
          Dim SelectedIDs As String = ""

          StatusItemsDS = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblTradeStatusGroupItems, False), RenaissanceDataClass.DSTradeStatusGroupItems)

          If (StatusItemsDS IsNot Nothing) Then
            SelectedRows = StatusItemsDS.tblTradeStatusGroupItems.Select("GroupID=" & CInt(Combo_StatusGroup.SelectedValue).ToString)
          End If

          If (SelectedRows IsNot Nothing) AndAlso (SelectedRows.Length > 0) Then
            For Each thisItemRow In SelectedRows
              If (SelectedIDs.Length > 0) Then SelectedIDs &= ","
              SelectedIDs &= thisItemRow.StatusID.ToString
            Next
          End If

          SelectString &= "(TransactionTradeStatusID IN (" & SelectedIDs & "))"
        End If
      End If


      If SelectString.Length <= 0 Then
        SelectString = "true"
      End If

    Catch ex As Exception
      SelectString = "true"
    End Try

    Return SelectString

  End Function

  ''' <summary>
  ''' Determines whether [is field transaction field] [the specified field name].
  ''' </summary>
  ''' <param name="FieldName">Name of the field.</param>
  ''' <returns><c>true</c> if [is field transaction field] [the specified field name]; otherwise, <c>false</c>.</returns>
  Private Function IsFieldTransactionField(ByVal FieldName As String) As Boolean

    ' *******************************************************************************
    ' Simple function to return a boolean value indicating whether or not a column name is
    ' part of the tblTransactions table.
    ' *******************************************************************************

    Dim TransactionsDS As New RenaissanceDataClass.DSTransaction

    Try
      ' TransactionsDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblTransaction)

      If (TransactionsDS Is Nothing) Then
        Return False
      Else
        Return TransactionsDS.tblTransaction.Columns.Contains(FieldName)
      End If
    Catch ex As Exception
      Return (False)
    End Try

    Return (False)
  End Function

  Private Sub ShowKansasStatusMessages()
    ' *******************************************************************************
    ' Populate kansas status area with recieved messages. 
    ' Should be one message per available kansas server (Which should be only one!)
    ' *******************************************************************************

    Dim TextMessage As String = ""

    Try

      If (MainForm.KansasStatusMessages Is Nothing) Then
        TextMessage = "Error : KansasStatusMessages not defined."
      Else

        If (MainForm.KansasStatusMessages.Count <= 0) Then
          TextMessage = "No Kansas trade server messages to report. (No available server)."
        Else
          Dim Messages() As String = MainForm.KansasStatusMessages.Values.ToArray
          Dim MessageCount As Integer

          For MessageCount = 0 To (Messages.Length - 1)
            TextMessage = TextMessage & Messages(MessageCount) & IIf(MessageCount < (Messages.Length - 1), vbCrLf, "")
          Next

        End If

      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in ShowKansasStatusMessages().", ex.StackTrace, True)
    Finally
      Try

        ' try to set the textColrto red if we have an error but dosent work :-(  ...to continue...
        Text_KansasStatus.ReadOnly = False

        If TextMessage.StartsWith("No Kansas") Or TextMessage.StartsWith("Error ") Then
          Text_KansasStatus.ForeColor = Color.Red
          KansasIsOk = False

        Else
          'Text_KansasStatus.ForeColor = Color.Black
          Text_KansasStatus.ForeColor = Color.Green
          KansasIsOk = True
        End If

        Text_KansasStatus.Text = "Kansas Status: " & TextMessage
        Text_KansasStatus.ReadOnly = True

      Catch ex As Exception
      End Try
    End Try

  End Sub
  ''' <summary>
  ''' Sets the sorted rows.
  ''' </summary>
  Private Sub SetSortedRows()

    ' *******************************************************************************
    ' Build a Select String appropriate to the form status and apply it to the DataView object.
    ' *******************************************************************************

    Dim SelectString As String

    If (Me.Created) And (Not Me.IsDisposed) Then

      ' Button Visibility.

      If (Has_FM_Permissions Or Has_MO_Permissions) Then
        If TabControl_Transactions.SelectedTab Is Tab_Approval Then
          Button_ApproveTrades.Visible = True
          Button_RejectTrades.Visible = True
          Button_CancelApproval.Visible = False
          Button_MarkAsSent.Visible = False
        Else
          Button_ApproveTrades.Visible = False
          Button_RejectTrades.Visible = False
          If (Has_MO_Permissions) Then
            Button_CancelApproval.Visible = True
            If (Allow_MarkAsSent) Then Button_MarkAsSent.Visible = True
          End If
        End If
      Else
        Button_ApproveTrades.Visible = False
        Button_RejectTrades.Visible = False
        Button_CancelApproval.Visible = False
        Button_MarkAsSent.Visible = False
      End If

      If TabControl_Transactions.SelectedTab Is Tab_Approval Then
        RootMenu.Items("Menu_TransactionReports").Visible = False
      Else
        RootMenu.Items("Menu_TransactionReports").Visible = True
      End If

      ' Set DataView Filter

      SelectString = GetTransactionSelectString()

      Try

        If (approvalDataView IsNot Nothing) AndAlso (Not (approvalDataView.RowFilter = SelectString)) Then
          approvalDataView.RowFilter = SelectString
        End If

      Catch ex As Exception
        SelectString = "true"
        approvalDataView.RowFilter = SelectString
      End Try

      ' Set NAV data if necessary
      Check_ShowPercentOfNAV_CheckedChanged(Nothing, Nothing)

      Me.Label_Status.Text = "(" & approvalDataView.Count.ToString & " Records) " & SelectString

    End If

  End Sub


  ' Check User permissions
  ''' <summary>
  ''' Checks the permissions.
  ''' </summary>
  Private Sub CheckPermissions()
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

    Dim FM_Permissions As Integer
    Dim MO_Permissions As Integer

    'Trades_FundManagerApproval
    'Trades_MiddleOfficeApproval

    FM_Permissions = MainForm.CheckPermissions("Trades_FundManagerApproval", RenaissanceGlobals.PermissionFeatureType.TypeForm)
    Has_FM_Permissions = ((FM_Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0) Or ((FM_Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)

    MO_Permissions = MainForm.CheckPermissions("Trades_MiddleOfficeApproval", RenaissanceGlobals.PermissionFeatureType.TypeForm)
    Has_MO_Permissions = ((MO_Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0) Or ((MO_Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)

  End Sub

  ''' <summary>
  ''' Basic event handler, called when a form value is changed.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' *******************************************************************************
    ' In the event of one of the Form Controls changing, refresh the data grid.
    ' *******************************************************************************

    If InPaint = False Then

      Call SetSortedRows()

    End If
  End Sub

  ''' <summary>
  ''' Handles the KeyUp event of the Combo_FieldSelect control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
  Private Sub Combo_FieldSelect_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
    ' *******************************************************************************
    ' In the event of one of the Form Controls changing, refresh the data grid.
    ' *******************************************************************************

    If InPaint = False Then

      Call SetSortedRows()

    End If
  End Sub

  Private Sub Grid_Transactions_AfterDataRefresh(ByVal sender As Object, ByVal e As System.ComponentModel.ListChangedEventArgs) Handles Grid_Transactions.AfterDataRefresh
    ' *******************************************************************************
    ' *******************************************************************************

    Try

      Dim thisGridRow As C1.Win.C1FlexGrid.Row
      Dim RowCounter As Integer
      Dim ColID As Integer

      If (Grid_Transactions IsNot Nothing) AndAlso (Grid_Transactions.Rows.Count > 1) Then
        ColID = Grid_Transactions.Cols("ExpectedInstrumentPosition").SafeIndex

        For RowCounter = 1 To (Grid_Transactions.Rows.Count - 1)
          thisGridRow = Grid_Transactions.Rows(RowCounter)

          'If CStr(Nz(thisGridRow(ColID), "")).Length > 0 Then
          If CStr(Nz(thisGridRow(ColID), "")).StartsWith("Short") Then
            Grid_Transactions.SetCellStyle(RowCounter, ColID, Grid_Transactions.Styles("ShortWarning"))
          Else
            Grid_Transactions.SetCellStyle(RowCounter, ColID, Grid_Transactions.Styles.Normal)
          End If
        Next
      End If
    Catch ex As Exception

    End Try

  End Sub


  ''' <summary>
  ''' Handles the DoubleClick event of the Grid_Transactions control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Transactions_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid_Transactions.DoubleClick
    ' ***************************************************************************************
    ' Spawn an Add/Edit Transactions form reflecting the chosen selection of transactions
    ' and pointing to the transaction which was double clicked.
    ' ***************************************************************************************

    Try

      Dim ParentID As Integer
      Dim thisFormHandle As RenaissanceGlobals.RenaissanceFormHandle

      If (Grid_Transactions.RowSel > 0) AndAlso (Grid_Transactions.RowSel < Grid_Transactions.Rows.Count) Then
        ParentID = Me.Grid_Transactions.Rows(Grid_Transactions.RowSel).DataSource("TransactionParentID")

        thisFormHandle = MainForm.New_VeniceForm(VeniceFormID.frmTransaction)
        CType(thisFormHandle.Form, frmTransaction).FormSelectCriteria = GetTransactionSelectString(True)
        CType(thisFormHandle.Form, frmTransaction).MoveToAuditID(ParentID)
      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error opening Transactions Form.", ex.StackTrace, True)
    End Try

  End Sub



  ''' <summary>
  ''' Handles the KeyDown event of the Grid_Transactions control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Transactions_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Grid_Transactions.KeyDown
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        Try
          If e.Control And (e.KeyCode = Keys.C) Then
            Call MainForm.CopyGridSelection(CType(sender, C1.Win.C1FlexGrid.C1FlexGrid), True, False, True)
          End If
        Catch ex As Exception
        End Try

      End If
    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Handles the DoubleClick event of the Grid_WorkingTrades control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Grid_WorkingTrades_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid_WorkingTrades.DoubleClick
    ' ***************************************************************************************
    ' Spawn an Add/Edit Transactions form reflecting the chosen selection of transactions
    ' and pointing to the transaction which was double clicked.
    ' ***************************************************************************************

    Try

      Dim ParentID As Integer
      Dim thisFormHandle As RenaissanceGlobals.RenaissanceFormHandle

      If (Grid_WorkingTrades.RowSel > 0) AndAlso (Grid_WorkingTrades.RowSel < Grid_WorkingTrades.Rows.Count) Then
        ParentID = Me.Grid_WorkingTrades.Rows(Grid_WorkingTrades.RowSel).DataSource("TransactionParentID")

        thisFormHandle = MainForm.New_VeniceForm(VeniceFormID.frmTransaction)
        CType(thisFormHandle.Form, frmTransaction).MoveToAuditID(ParentID)
      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error opening Transactions Form.", ex.StackTrace, True)
    End Try

  End Sub

  Private Grid_WorkingTrades_AfterDataRefresh_handleAdded As Boolean = True

  Private Sub Grid_WorkingTrades_AfterDataRefresh(ByVal sender As Object, ByVal e As System.ComponentModel.ListChangedEventArgs) Handles Grid_WorkingTrades.AfterDataRefresh
    ' *******************************************************************************
    ' *******************************************************************************
    Try
      ' lotfi: grid data are modified inside this sub, so we remove the handler and readd it at the end in order not to recursivly call this refresh sub
      If Grid_WorkingTrades_AfterDataRefresh_handleAdded Then
        Try
          RemoveHandler Grid_WorkingTrades.AfterDataRefresh, AddressOf Me.Grid_WorkingTrades_AfterDataRefresh
          Grid_WorkingTrades_AfterDataRefresh_handleAdded = False
        Catch ex As Exception
        End Try
      End If

      Dim thisGridRow As C1.Win.C1FlexGrid.Row
      Dim RowCounter As Integer
      Dim ColID As Integer
      Dim orderSeqNumberColID As Integer
      Dim orderFilledAmountColID As Integer
      Dim orderStatusStringColID As Integer
      Dim orderStatus As String
      Dim orderStatusString As String
      Dim orderSeqNumber As String
      Dim orderFilledAmount As Double

      If (Grid_WorkingTrades IsNot Nothing) AndAlso (Grid_WorkingTrades.Rows.Count > 1) Then
        ' to fit the default columns
        If (Me.Width < 1100) Then
          Me.Width = 1100
        End If

        If (Not Grid_WorkingTrades.Cols("DateEntered") Is Nothing) Then
          Grid_WorkingTrades.Sort(SortFlags.Descending, Grid_WorkingTrades.Cols("DateEntered").SafeIndex)
        End If

        If (Not Grid_WorkingTrades.Cols("TransactionTypeName") Is Nothing) Then
          Grid_WorkingTrades.Cols("TransactionTypeName").Style.TextAlign = TextAlignEnum.CenterCenter
        End If

        ColID = Grid_WorkingTrades.Cols("OrderStatusName").SafeIndex
        orderSeqNumberColID = Grid_WorkingTrades.Cols("SequenceNumber").SafeIndex
        orderFilledAmountColID = Grid_WorkingTrades.Cols("FilledAmount").SafeIndex
        orderStatusStringColID = Grid_WorkingTrades.Cols("StatusString").SafeIndex

        For RowCounter = 1 To (Grid_WorkingTrades.Rows.Count - 1)
          thisGridRow = Grid_WorkingTrades.Rows(RowCounter)
          thisGridRow.Selected = False

          orderStatus = CStr(Nz(thisGridRow(ColID), ""))
          orderSeqNumber = CStr(Nz(thisGridRow(orderSeqNumberColID), "0"))
          orderFilledAmount = thisGridRow(orderFilledAmountColID)
          orderStatusString = thisGridRow(orderStatusStringColID)
          'Attention : Some "completed" orders are actually rejected or in error and that'xs whey they are completed in the sens of closed/terminated.
          If orderStatus = "Completed" And orderFilledAmount = 0 Then
            If LCase(orderStatusString).Contains("reject") Then
              orderStatus = "Rejected"
            ElseIf LCase(orderStatusString).Contains("cancel") Then
              orderStatus = "Cancelled"
            Else
              orderStatus = "Error"
            End If
            thisGridRow(ColID) = orderStatus
          End If

          If orderStatus.StartsWith("Cancelled") Then
            Grid_WorkingTrades.SetCellStyle(RowCounter, ColID, Grid_WorkingTrades.Styles("Cancelled"))
          ElseIf orderStatus = "Deleted" Then
            Grid_WorkingTrades.SetCellStyle(RowCounter, ColID, Grid_WorkingTrades.Styles("Deleted"))
          ElseIf orderStatus = "Rejected" Then
            Grid_WorkingTrades.SetCellStyle(RowCounter, ColID, Grid_WorkingTrades.Styles("Rejected"))
          ElseIf orderStatus = "Error" Then
            Grid_WorkingTrades.SetCellStyle(RowCounter, ColID, Grid_WorkingTrades.Styles("Error"))
          ElseIf orderStatus = "Completed" Then
            Grid_WorkingTrades.SetCellStyle(RowCounter, ColID, Grid_WorkingTrades.Styles("Completed"))
          Else
            Grid_WorkingTrades.SetCellStyle(RowCounter, ColID, Grid_WorkingTrades.Styles("Working"))
            'Else
            '  Grid_WorkingTrades.SetCellStyle(RowCounter, ColID, Grid_WorkingTrades.Styles.Normal)
          End If

          If orderStatusString <> "" Then
            Dim msg As String = orderStatusString
            Dim i, j As Integer
            i = msg.IndexOf("ERROR_MESSAGE")
            If i >= 0 Then
              j = orderStatusString.IndexOf("}", i)
              If j >= 0 Then
                msg = orderStatusString.Substring(i, j - i)
              Else
                msg = orderStatusString.Substring(i)
              End If
            Else
            End If

            msg = msg.Replace(vbCrLf, " ").Replace("\r\n", " ").Replace("\n", " ")

            If Len(msg) > 100 Then
              msg = msg.Substring(0, 100) & " ..."
            End If

            thisGridRow(orderStatusStringColID) = msg

          End If

        Next
      End If
    Catch ex As Exception
    Finally
      'lotfi: readd the handler
      If Grid_WorkingTrades_AfterDataRefresh_handleAdded = False Then
        Try
          AddHandler Grid_WorkingTrades.AfterDataRefresh, AddressOf Me.Grid_WorkingTrades_AfterDataRefresh
          Grid_WorkingTrades_AfterDataRefresh_handleAdded = True
        Catch ex As Exception
        End Try
      End If
    End Try


  End Sub

#End Region


#Region " Set Form Combos (Form Specific Code) "
  ''' <summary>
  ''' Sets the fund combo.
  ''' </summary>
  Private Sub SetFundCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_TransactionFund, _
    RenaissanceStandardDatasets.tblFund, _
    "FundName", _
    "FundID", _
    "FundClosed=0", False, True, True)   ' 

  End Sub

  ''' <summary>
  ''' Sets the group combo.
  ''' </summary>
  Private Sub SetGroupCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_StatusGroup, _
    RenaissanceStandardDatasets.tblSophisStatusGroups, _
    "GROUP_NAME", _
    "KERNEL_STATUS_GROUP_ID", _
    "KERNEL_STATUS_GROUP_ID IN (1,2,10)", True, True, True, 0)    ' 

  End Sub

  ''' <summary>
  ''' Sets the instrument combo.
  ''' </summary>
  Private Sub SetInstrumentCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_TransactionInstrument, _
    RenaissanceStandardDatasets.tblInstrument, _
    "InstrumentDescription", _
    "InstrumentID", _
    "", False, True, True)    ' 

  End Sub

#End Region

#Region " Form Control Events"

  Private Sub Check_ShowPercentOfNAV_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_ShowPercentOfNAV.CheckedChanged

    If (Check_ShowPercentOfNAV.Checked) Then
      If (Grid_Transactions.Cols.Contains("TransactionNAVPercentage")) Then
        Grid_Transactions.Cols("TransactionNAVPercentage").Visible = True
      End If
    Else
      If (Grid_Transactions.Cols.Contains("TransactionNAVPercentage")) Then
        Grid_Transactions.Cols("TransactionNAVPercentage").Visible = False
      End If
    End If

  End Sub


  ''' <summary>
  ''' Handles the CheckedChanged event of the Check_ShowPercentOfNAV control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Check_ShowPercentOfNAV_CheckedChanged_old(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_ShowPercentOfNAV.CheckedChanged
    ' ***************************************************************************************
    '
    '
    ' ***************************************************************************************
    Console.WriteLine(" Check_ShowPercentOfNAV_CheckedChanged to " + CStr(Check_ShowPercentOfNAV.Checked) + " and LoadingNAVs =" + CStr(LoadingNAVs))
    Try
      Dim ApprovalRow As RenaissanceDataClass.DSSelectTransaction.tblSelectTransactionRow
      Dim InstrumentRow As DSInstrument.tblInstrumentRow

      If (Check_ShowPercentOfNAV.Checked) Then
        If (LoadingNAVs = False) Then
          Try
            LoadingNAVs = True

            If (Grid_Transactions.Cols.Contains("TransactionNAVPercentage")) Then

              If (EuroNAVs Is Nothing) OrElse (EuroNAVs.Count = 0) Then
                Dim NAVCommand As New SqlCommand
                Dim DataReader As New VeniceMain.GetDataReaderClass
                Dim NAVTable As New DataTable
                Dim NAVRow As DataRow

                ' Show Column

                ' Lotfi: moved outside of the if to show it in all cases ifthe checkbox is checked 
                'Grid_Transactions.Cols("TransactionNAVPercentage").Visible = True

                If (Grid_Transactions.Cols.Contains("InstrumentISIN")) Then
                  Grid_Transactions.Cols("InstrumentISIN").Visible = True
                End If

                ' 
                If (EuroNAVs Is Nothing) Then
                  EuroNAVs = New Dictionary(Of Integer, Double)
                End If

                If (EuroNAVs.Count = 0) Then
                  Try
                    NAVCommand.CommandType = CommandType.StoredProcedure
                    NAVCommand.CommandText = "spu_GetFundNAVs"
                    NAVCommand.Parameters.Add(New SqlParameter("@FundID", SqlDbType.Int)).Value = 0
                    NAVCommand.Parameters.Add(New SqlParameter("@ValueDate", SqlDbType.DateTime)).Value = Now()
                    NAVCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", SqlDbType.DateTime)).Value = RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW

                    NAVCommand.Connection = MainForm.GetVeniceConnection

                    DataReader.MyCommand = NAVCommand

                    MainForm.RunDataReader(DataReader)

                    If (DataReader.myReader Is Nothing) Then
                      ' MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "DataReader.myReader is nothing in Check_ShowPercentOfNAV.", "", False)
                      MainForm.LoadTable_Custom(NAVTable, NAVCommand)
                      'NAVTable.Load(NAVCommand.ExecuteReader)
                    Else
                      'MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "DataReader.myReader OK in Check_ShowPercentOfNAV.", "", False)
                      NAVTable.Load(DataReader.myReader)
                    End If

                    If (NAVTable.Rows.Count > 0) Then
                      ' MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "NAVTable has " & NAVTable.Rows.Count.ToString & " rows.", "", False)
                      For Each NAVRow In NAVTable.Rows
                        EuroNAVs.Add(CInt(NAVRow("FundID")), CDbl(NAVRow("EURO_VALUE")))
                      Next

                      NAVTable.Rows.Clear()
                      '  Else
                      ' MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "NAVTable has no rows.", "", False)
                    End If

                  Catch ex As Exception
                    EuroNAVs.Clear()
                    MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error execing spu_GetFundNAVs in Check_ShowPercentOfNAV.", ex.StackTrace, False)
                  Finally
                    If (NAVCommand.Connection IsNot Nothing) Then
                      If (NAVCommand.Connection.State And ConnectionState.Open) Then
                        NAVCommand.Connection.Close()
                      End If

                      NAVCommand.Connection = Nothing
                    End If
                  End Try
                End If

              End If

              '
              If FXRates Is Nothing Then
                FXRates = New Dictionary(Of Integer, Double)
              End If

              Dim EuroFX As Double = MainForm.GetBestFX(3, Now().Date)
              Dim InstFX As Double
              Dim FXRate As Double
              Dim FundEuroValue As Double

              FXRates(3) = EuroFX

              If (EuroFX = 0.0#) Then
                MainForm.LogError(Me.Name, LOG_LEVELS.Error, "", "Euro FX Rate is zero.", "", False)
              Else

                ' Populate NAV Column

                For Each ApprovalRow In approvalTable


                  If (EuroNAVs.ContainsKey(ApprovalRow.TransactionFund)) Then
                    FundEuroValue = EuroNAVs(ApprovalRow.TransactionFund)

                    InstrumentRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, ApprovalRow.TransactionInstrument)

                    ' ApprovalRow("InstrumentISIN") = InstrumentRow.InstrumentISIN

                    If (FXRates.ContainsKey(InstrumentRow.InstrumentCurrencyID) = False) Then
                      InstFX = MainForm.GetBestFX(InstrumentRow.InstrumentCurrencyID, Now().Date)
                      FXRates(InstrumentRow.InstrumentCurrencyID) = InstFX
                    Else
                      InstFX = FXRates(InstrumentRow.InstrumentCurrencyID)
                    End If

                    FXRate = InstFX / EuroFX

                    If (ApprovalRow.TransactionType = TransactionTypes.BuyFX) Or (ApprovalRow.TransactionType = TransactionTypes.SellFX) OrElse (ApprovalRow.TransactionType = TransactionTypes.BuyFXForward) Or (ApprovalRow.TransactionType = TransactionTypes.SellFXForward) Then
                      ApprovalRow("TransactionNAVPercentage") = ApprovalRow.TransactionUnits * FXRate / FundEuroValue
                    Else
                      ApprovalRow("TransactionNAVPercentage") = ApprovalRow.TransactionUnits * ApprovalRow.TransactionPrice * InstrumentRow.InstrumentContractSize * InstrumentRow.InstrumentMultiplier * FXRate / FundEuroValue
                    End If
                  Else
                    ApprovalRow("TransactionNAVPercentage") = 0.0#
                    MainForm.LogError(Me.Name, LOG_LEVELS.Error, "", "Value not in EuroNAVs() : '" & ApprovalRow.TransactionFund.ToString & "'", "", False)

                  End If

                Next

              End If

            Else
              MainForm.LogError(Me.Name, LOG_LEVELS.Error, "", "TransactionNAVPercentage not exist in grid.", "", False)
            End If

          Catch ex As Exception
            MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Check_ShowPercentOfNAV.", ex.StackTrace, False)
          Finally
            LoadingNAVs = False
          End Try

        End If

        If (Grid_Transactions.Cols.Contains("TransactionNAVPercentage")) Then
          Grid_Transactions.Cols("TransactionNAVPercentage").Visible = True
        End If
      Else
        If (Grid_Transactions.Cols.Contains("TransactionNAVPercentage")) Then
          Grid_Transactions.Cols("TransactionNAVPercentage").Visible = False
        End If
      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Check_ShowPercentOfNAV.", ex.StackTrace, False)
    End Try

  End Sub

  ''' <summary>
  ''' Handles the SelectedIndexChanged event of the TabControl_Transactions control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub TabControl_Transactions_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TabControl_Transactions.SelectedIndexChanged
    ' ***************************************************************************************
    '
    '
    ' ***************************************************************************************

    Try
      If (Has_FM_Permissions Or Has_MO_Permissions) Then
        If TabControl_Transactions.SelectedTab Is Tab_Approval Then
          Button_ApproveTrades.Visible = True
          Button_RejectTrades.Visible = True
          Button_CancelApproval.Visible = False
          Button_MarkAsSent.Visible = False
        Else
          Button_ApproveTrades.Visible = False
          Button_RejectTrades.Visible = False
          If (Has_MO_Permissions) Then
            Button_CancelApproval.Visible = True
            If (Allow_MarkAsSent) Then Button_MarkAsSent.Visible = True
          End If
        End If
      Else
        Button_ApproveTrades.Visible = False
        Button_RejectTrades.Visible = False
        Button_CancelApproval.Visible = False
        Button_MarkAsSent.Visible = False
      End If

      If TabControl_Transactions.SelectedTab Is Tab_Approval Then
        RootMenu.Items("Menu_TransactionReports").Visible = False
      Else
        RootMenu.Items("Menu_TransactionReports").Visible = True
      End If


    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error changing selected tab.", ex.StackTrace, False)
    End Try

  End Sub

  ''' <summary>
  ''' Handles the Click event of the Button_RejectTrades control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Button_RejectTrades_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_RejectTrades.Click
    ' ***************************************************************************************
    '
    '
    ' ***************************************************************************************
    Dim SomeTransactionChanged As Boolean = False
    Dim TransactionIDs As String = ""

    Try
      Dim SelectedRows As C1.Win.C1FlexGrid.RowCollection = Nothing
      Dim ThisRow As C1.Win.C1FlexGrid.Row = Nothing
      Dim ItemCount As Integer

      Dim TransactionParentID As Integer
      Dim TransactionTradeStatusID As Integer

      SelectedRows = Grid_Transactions.Rows.Selected

      If (SelectedRows Is Nothing) OrElse (SelectedRows.Count <= 0) Then
        Exit Sub
      End If

      ' Confirm.

      If (MessageBox.Show("Cancel Selected Trades", "Cancel Selected Trades", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) <> Windows.Forms.DialogResult.OK) Then
        Exit Sub
      End If

      ' Ok, Check each selected transaction.

      For ItemCount = 0 To (SelectedRows.Count - 1)
        If (ItemCount < SelectedRows.Count) Then
          ThisRow = SelectedRows(ItemCount)

          If (ThisRow IsNot Nothing) Then

            TransactionParentID = CInt(Nz(ThisRow("TransactionParentID"), 0))
            TransactionTradeStatusID = CInt(Nz(ThisRow("TransactionTradeStatusID"), 0))

            If (TransactionIDs.Length > 0) Then TransactionIDs &= ","
            TransactionIDs &= TransactionParentID.ToString()

            If (Has_FM_Permissions And Has_MO_Permissions) Then
              SetTransactionStatus(MainForm, TransactionParentID, RenaissanceGlobals.TradeStatus.RefusedMO, True, False, RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW, "Cancelled")
              SomeTransactionChanged = True
            ElseIf (Has_FM_Permissions) AndAlso (TradeStatusIsInGroup(MainForm, TransactionTradeStatusID, RenaissanceGlobals.StatusGroups.Pending_FM)) Then
              SetTransactionStatus(MainForm, TransactionParentID, RenaissanceGlobals.TradeStatus.Cancelled, True, False, RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW, "Cancelled")
              SomeTransactionChanged = True
            ElseIf (Has_MO_Permissions) AndAlso (TradeStatusIsInGroup(MainForm, TransactionTradeStatusID, RenaissanceGlobals.StatusGroups.Pending_MO)) Then
              SetTransactionStatus(MainForm, TransactionParentID, RenaissanceGlobals.TradeStatus.RefusedMO, True, False, RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW, "Cancelled")
              SomeTransactionChanged = True
            End If

          End If
        End If
      Next

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error processing transactions.", ex.StackTrace, True)
    End Try

    Try
      ' Propogate changes and Tidy up.

      If (SomeTransactionChanged) Then
        Call MainForm.ReloadTable(RenaissanceChangeID.tblTradeFilePending, TransactionIDs, True) ' Raise Event from Reload function
        Call MainForm.ReloadTable_Background(RenaissanceChangeID.tblTransaction, TransactionIDs, False)
        Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblTransaction, TransactionIDs), True)
        'Lotfi: comment  cause done in the updateEvent
        'Call LoadApprovalsTableData()
        Call LoadFileTableData()
        SetSortedRows()
      End If
    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Handles the Click event of the Button_ApproveTrades control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Button_ApproveTrades_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_ApproveTrades.Click
    ' ***************************************************************************************
    '
    '
    ' ***************************************************************************************

    Dim SomeTransactionChanged As Boolean = False
    Dim TransactionIDs As String = ""
    Dim SuggestedWorkflowRule As RenaissanceDataClass.DSWorkflowRules.tblWorkflowRulesRow = Nothing
    Dim ResultingWorkflow As RenaissanceDataClass.DSWorkflows.tblWorkflowsRow = Nothing
    Dim ExecutionStatus As String = ""

    ' Ok, Check each selected transaction.
    Dim nbEmsxTrades As Integer = 0

    Dim warn As Boolean = False
    Dim nbrWarn As Integer = 0
    Dim nbrOk As Integer = 0

    Try
      Dim SelectedRows As C1.Win.C1FlexGrid.RowCollection = Nothing
      Dim ThisRow As C1.Win.C1FlexGrid.Row = Nothing
      Dim ItemCount As Integer

      Dim TransactionParentID As Integer
      Dim TransactionFundID As Integer
      Dim TransactionType As TransactionTypes
      Dim TransactionTradeStatusID As Integer
      Dim InstrumentType As Integer
      Dim CurrencyID As Integer
      Dim InstrumentID As Integer
      Dim TransactionUnits As Double

      Dim NewTransactionStatus As Integer

      Dim HasPermissionToApprove As Boolean
      Dim DontChangeThisStatus As Boolean = False

      SelectedRows = Grid_Transactions.Rows.Selected

      If (SelectedRows Is Nothing) OrElse (SelectedRows.Count <= 0) Then
        Exit Sub
      End If

      ' Confirm.

      If (MessageBox.Show("Approve Selected Trades", "Approve Selected Trades", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) <> Windows.Forms.DialogResult.OK) Then
        Exit Sub
      End If

      For ItemCount = 0 To (SelectedRows.Count - 1)
        warn = False
        If (ItemCount < SelectedRows.Count) Then
          ThisRow = SelectedRows(ItemCount)

          If (ThisRow IsNot Nothing) Then
            HasPermissionToApprove = False
            DontChangeThisStatus = False ' All is OK.

            TransactionParentID = CInt(Nz(ThisRow("TransactionParentID"), 0))
            TransactionFundID = CInt(Nz(ThisRow("TransactionFund"), 0))
            InstrumentType = CInt(Nz(ThisRow("InstrumentType"), 0))
            TransactionType = CType(Nz(ThisRow("TransactionType"), 0), TransactionTypes)
            CurrencyID = CInt(Nz(ThisRow("InstrumentCurrencyID"), 0))
            InstrumentID = CInt(Nz(ThisRow("TransactionInstrument"), 0))
            TransactionTradeStatusID = CInt(Nz(ThisRow("TransactionTradeStatusID"), 0))
            TransactionUnits = CInt(Nz(ThisRow("TransactionUnits"), 0))
            ' Permission to approve this trade ?

            If (Has_FM_Permissions And Has_MO_Permissions) Then
              HasPermissionToApprove = True
            ElseIf (Has_FM_Permissions) AndAlso (TradeStatusIsInGroup(MainForm, TransactionTradeStatusID, RenaissanceGlobals.StatusGroups.Pending_FM)) Then
              HasPermissionToApprove = True
            ElseIf (Has_MO_Permissions) AndAlso (TradeStatusIsInGroup(MainForm, TransactionTradeStatusID, RenaissanceGlobals.StatusGroups.Pending_MO)) Then
              HasPermissionToApprove = True
            End If

            If (TransactionType <> TransactionTypes.Buy) AndAlso (TransactionType <> TransactionTypes.Sell) Then
              ' Reject trades that are not a BUY or SELL.
              ExecutionStatus &= TransactionParentID.ToString & " : This trade is neither a BUY or SELL." & vbCrLf
              warn = True
            ElseIf TransactionUnits = 0 Then
              ' Reject trades with amount zero
              ExecutionStatus &= TransactionParentID.ToString & " : This trade has ZERO units !!." & vbCrLf
              warn = True
            ElseIf (HasPermissionToApprove) Then

              ' What status will the trade have after approval ?

              SuggestedWorkflowRule = GetExpectedWorkflowRule(MainForm, TransactionFundID, InstrumentType, 0, CurrencyID, InstrumentID, TransactionTradeStatusID, False, True)
              ResultingWorkflow = Nothing

              If (SuggestedWorkflowRule IsNot Nothing) Then
                ResultingWorkflow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblWorkflows, CInt(Nz(SuggestedWorkflowRule.ResultingWorkflow, 0)))
              End If

              ' What action must be taken, Broker execution, upon approval.

              If (ResultingWorkflow IsNot Nothing) Then
                NewTransactionStatus = ResultingWorkflow.ResultingTradeStatus

                ' Save Trades file details...

                If (Trim(ResultingWorkflow.CreateTradeFile.ToUpper) = "YES") Then

                  Dim thisTableRow As RenaissanceDataClass.DSSelectTransaction.tblSelectTransactionRow = Nothing
                  Dim tempRowSelection() As RenaissanceDataClass.DSSelectTransaction.tblSelectTransactionRow

                  If (approvalTable IsNot Nothing) Then
                    tempRowSelection = approvalTable.Select("TransactionParentID = " & CStr(TransactionParentID))

                    If (tempRowSelection IsNot Nothing) AndAlso (tempRowSelection.Length > 0) Then
                      thisTableRow = tempRowSelection(0)
                    End If
                  End If

                  If (thisTableRow IsNot Nothing) Then
                    Dim SuccessFlag As Boolean = False

                    ' **************************************************
                    ' Save Trades File Entry.
                    ' **************************************************
                    ExecutionStatus &= SaveTradeFileDetails(MainForm, TransactionParentID, thisTableRow, SuggestedWorkflowRule.RuleID, ResultingWorkflow.WorkflowID, Date_TriggerDate.Value, SuccessFlag) & vbCrLf

                    If (SuccessFlag = False) Then
                      DontChangeThisStatus = True
                      warn = True
                    End If
                  Else
                    ExecutionStatus &= TransactionParentID.ToString & " : Could not find TransactionID in pending trades table." & vbCrLf
                    DontChangeThisStatus = True
                    warn = True

                  End If

                End If ' Write to Trade file

              Else
                ExecutionStatus &= TransactionParentID.ToString & " : No suitable workflow found. Trade not sent to file." & vbCrLf
                DontChangeThisStatus = True
                warn = True
              End If

              ' Set new Trade status...

              If (DontChangeThisStatus = False) Then

                If (TransactionIDs.Length > 0) Then TransactionIDs &= ","
                TransactionIDs &= TransactionParentID.ToString()

                'lotfi: set the value date as today and the settelmentDate in consequence...
                '-------------
                Dim newValueDate As Date
                Dim newSettlementDate As Date

                Dim thisInstrument As RenaissanceDataClass.DSInstrument.tblInstrumentRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, InstrumentID), RenaissanceDataClass.DSInstrument.tblInstrumentRow)
                If (thisInstrument IsNot Nothing) Then
                  ' Set Value and settlement dates to default if the instrument is changed.
                  If (thisInstrument.InstrumentDealingCutOffTime > 0) AndAlso (thisInstrument.InstrumentDealingCutOffTime < CInt(Now.TimeOfDay.TotalSeconds)) Then
                    ' After Cutoff
                    newValueDate = RenaissanceUtilities.DatePeriodFunctions.AddPeriodToDate(DealingPeriod.Daily, Now.Date, 1)
                  Else
                    newValueDate = Now.Date
                  End If

                  If (TransactionType = TransactionTypes.Sell) Then
                    newSettlementDate = RenaissanceUtilities.DatePeriodFunctions.AddPeriodToDate(thisInstrument.InstrumentDealingPeriod, newValueDate, thisInstrument.InstrumentDefaultSettlementDays_Sell)
                  Else
                    newSettlementDate = RenaissanceUtilities.DatePeriodFunctions.AddPeriodToDate(thisInstrument.InstrumentDealingPeriod, newValueDate, thisInstrument.InstrumentDefaultSettlementDays_Buy)
                  End If
                Else
                  newValueDate = Now.Date
                  newSettlementDate = newValueDate
                End If

                '-------------

                If (Has_FM_Permissions And Has_MO_Permissions) Then
                  SetTransactionStatusAndValueDate(MainForm, TransactionParentID, NewTransactionStatus, False, False, RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW, "Approved", newValueDate, newSettlementDate)
                  SomeTransactionChanged = True
                ElseIf (Has_FM_Permissions) AndAlso (TradeStatusIsInGroup(MainForm, TransactionTradeStatusID, RenaissanceGlobals.StatusGroups.Pending_FM)) Then
                  SetTransactionStatusAndValueDate(MainForm, TransactionParentID, NewTransactionStatus, False, False, RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW, "Approved", newValueDate, newSettlementDate)
                  SomeTransactionChanged = True
                ElseIf (Has_MO_Permissions) AndAlso (TradeStatusIsInGroup(MainForm, TransactionTradeStatusID, RenaissanceGlobals.StatusGroups.Pending_MO)) Then
                  SetTransactionStatusAndValueDate(MainForm, TransactionParentID, NewTransactionStatus, False, False, RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW, "Approved", newValueDate, newSettlementDate)
                  SomeTransactionChanged = True
                End If
                'Lotfi: si le resulting workflow est EMSX Trade ( workflowId=6)
                If ResultingWorkflow IsNot Nothing AndAlso ResultingWorkflow.WorkflowID = 6 Then
                  nbEmsxTrades += 1
                End If

              End If

            Else

              ExecutionStatus &= TransactionParentID.ToString & " : Insufficient permissions to Approve this trade" & vbCrLf
              warn = True

            End If ' Has Permission

          End If ' thisRow <> Nothing

        End If ' (ItemCount < SelectedRows.Count)

        If warn Then
          nbrWarn += 1
        Else
          nbrOk += 1
        End If

      Next

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error processing transactions.", ex.StackTrace, True)
      warn = True
    End Try

    Try
      ' Propogate changes and Tidy up.

      If (SomeTransactionChanged) Then

        Call MainForm.ReloadTable(RenaissanceChangeID.tblTradeFilePending, TransactionIDs, True) ' Raise Event from Reload function
        Call MainForm.ReloadTable_Background(RenaissanceChangeID.tblTransaction, TransactionIDs, False)
        Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblTransaction, TransactionIDs), True)

        'Lotfi: comment  cause done in the updateEvent
        'Call LoadApprovalsTableData()
        Call LoadFileTableData()
        SetSortedRows()

        'Lotfi: enhance tradesstatus alert : adfd a line mentionnig numbers of trades ok and ko 
        Dim s1 As String = ""
        If nbrOk + nbrWarn > 1 Then
          s1 = "Trades status"
        Else
          s1 = "Trade status"
        End If

        Dim s2 As String = ""
        If nbrWarn > 0 Then
          s2 = CStr(nbrWarn) & " trade(s) on Warning"
        End If
        Dim s3 As String = ""
        If nbrOk > 0 Then
          s3 = CStr(nbrOk) & " trade(s) are OK"
        End If

        Dim header As String = s2
        If s3 <> "" Then
          If header = "" Then
            header = s3
          Else
            header = header & " and " & s3
          End If
        End If
        header = header & vbCrLf & "-----------------------------------------"

        If nbrWarn = 0 Then
          MessageBox.Show(header & vbCrLf & ExecutionStatus, s1, MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        Else
          MessageBox.Show(header & vbCrLf & ExecutionStatus, s1, MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1)
        End If

        'lotfi: if thers is some approved EMSX trades and the kansas is KO, we alert the user to make restart the Kansas service
        If nbEmsxTrades > 0 And KansasIsOk = False Then
          Dim str As String = "EMSX order"
          If nbEmsxTrades > 1 Then
            str = CStr(nbEmsxTrades) & str & "s"
          End If
          str = "the " & str

          MessageBox.Show("Attention, Kansas service is down !" & vbCrLf & " Please restart the service to get " & str & " sent to bloomberg", "Kansas service is down", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If

      ElseIf ExecutionStatus.Length > 0 Then
        MessageBox.Show(ExecutionStatus, "Trade Status", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
      End If
    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Handles the Click event of the Button_CancelApproval control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Button_CancelApproval_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_CancelApproval.Click
    ' ***************************************************************************************
    '
    '
    ' ***************************************************************************************

    Try
      Dim SelectedRows As C1.Win.C1FlexGrid.RowCollection = Nothing
      Dim ThisRow As C1.Win.C1FlexGrid.Row = Nothing
      Dim ItemCount As Integer
      Dim TradeFileRN As Integer
      Dim TransactionParentID As Integer
      Dim SomeTransactionChanged As Boolean = False
      Dim TransactionIDs As String = ""

      Dim HasPermissionToApprove As Boolean = False
      Dim DontChangeThisStatus As Boolean = False

      If (fileTable Is Nothing) OrElse (fileTable.Columns.Contains("TradeFileRN") = False) Then
        MainForm.LogError(Me.Name, LOG_LEVELS.Error, "", "TradeFileRN not on Transaction grid, can't tell which trade to cancel!", "", True)
        Exit Sub
      End If

      If (Has_MO_Permissions = False) Then
        MessageBox.Show("You need Middle Office permissions to revoke a trade approval.", "Insufficient Permissions", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
        Exit Sub
      End If

      SelectedRows = Grid_ToBeWritten.Rows.Selected

      If (SelectedRows Is Nothing) OrElse (SelectedRows.Count <= 0) Then
        Exit Sub
      End If

      ' Confirm.

      If (MessageBox.Show("Revoke Selected Trades", "Revoke Selected Trades", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) <> Windows.Forms.DialogResult.OK) Then
        Exit Sub
      End If

      ' Ok, Check each selected transaction.

      ' Clear Pending table entry.

      For ItemCount = 0 To (SelectedRows.Count - 1)

        If (ItemCount < SelectedRows.Count) Then

          ThisRow = SelectedRows(ItemCount)

          TradeFileRN = CInt(Nz(ThisRow("TradeFileRN"), 0))

          RevokeTradeApproval(MainForm, TradeFileRN)

          SomeTransactionChanged = True
        End If

      Next

      ' Change Trade Status, Done after the pending entries are resolved as this takes a bit longer and time here is of the essence.

      For ItemCount = 0 To (SelectedRows.Count - 1)

        If (ItemCount < SelectedRows.Count) Then

          ThisRow = SelectedRows(ItemCount)

          TransactionParentID = CInt(Nz(ThisRow("TransactionParentID"), 0))


          If (TransactionParentID > 0) Then

            If (TransactionIDs.Length > 0) Then TransactionIDs &= ","
            TransactionIDs &= TransactionParentID.ToString()

            SetTransactionStatus(MainForm, TransactionParentID, CInt(RenaissanceGlobals.TradeStatus.SentToMiddleOffice), False, False, RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW, "Revoked")
          End If

          SomeTransactionChanged = True
        End If

      Next

      If (SomeTransactionChanged) Then

        Call MainForm.ReloadTable(RenaissanceChangeID.tblTradeFilePending, TransactionIDs, True) ' Raise Event from Reload function
        Call MainForm.ReloadTable_Background(RenaissanceChangeID.tblTransaction, TransactionIDs, False)
        Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblTransaction, TransactionIDs), True)

        'Lotfi: comment  cause done in the updateEvent
        'Call LoadApprovalsTableData()
        Call LoadFileTableData()
        SetSortedRows()

      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error revoking transaction approval.", ex.StackTrace, True)
    End Try

  End Sub

  ''' <summary>
  ''' Handles the Click event of the Button_MarkAsSent control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Button_MarkAsSent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_MarkAsSent.Click
    ' ***************************************************************************************
    '
    '
    ' ***************************************************************************************

    Try
      Dim SelectedRows As C1.Win.C1FlexGrid.RowCollection = Nothing
      Dim ThisRow As C1.Win.C1FlexGrid.Row = Nothing
      Dim ItemCount As Integer
      Dim TradeFileRN As Integer
      Dim SomeTransactionChanged As Boolean = False
      Dim TransactionIDs As String = ""
      Dim TransactionParentID As Integer

      Dim HasPermissionToApprove As Boolean = False
      Dim DontChangeThisStatus As Boolean = False

      If (fileTable Is Nothing) OrElse (fileTable.Columns.Contains("TradeFileRN") = False) Then
        MainForm.LogError(Me.Name, LOG_LEVELS.Error, "", "TradeFileRN not on Transaction grid, can't tell which trade to use!", "", True)
        Exit Sub
      End If

      If (Has_MO_Permissions = False) Then
        MessageBox.Show("You need Middle Office permissions to mark as sent.", "Insufficient Permissions", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
        Exit Sub
      End If

      SelectedRows = Grid_ToBeWritten.Rows.Selected

      If (SelectedRows Is Nothing) OrElse (SelectedRows.Count <= 0) Then
        Exit Sub
      End If

      ' Confirm.

      If (MessageBox.Show("Mark Selected Trades as sent", "Mark Selected Trades as sent", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) <> Windows.Forms.DialogResult.OK) Then
        Exit Sub
      End If

      ' Ok, Check each selected transaction.

      ' Clear Pending table entry.

      For ItemCount = 0 To (SelectedRows.Count - 1)

        If (ItemCount < SelectedRows.Count) Then

          ThisRow = SelectedRows(ItemCount)

          TradeFileRN = CInt(Nz(ThisRow("TradeFileRN"), 0))

          MarkTradeAsSent(MainForm, TradeFileRN)

          SomeTransactionChanged = True
        End If

      Next

      ' Change Trade Status, Done after the pending entries are resolved as this takes a bit longer and time here is of the essence.

      For ItemCount = 0 To (SelectedRows.Count - 1)

        If (ItemCount < SelectedRows.Count) Then

          ThisRow = SelectedRows(ItemCount)

          TransactionParentID = CInt(Nz(ThisRow("TransactionParentID"), 0))


          If (TransactionParentID > 0) Then

            If (TransactionIDs.Length > 0) Then TransactionIDs &= ","
            TransactionIDs &= TransactionParentID.ToString()

            SetTransactionStatus(MainForm, TransactionParentID, CInt(RenaissanceGlobals.TradeStatus.ElectronicMarket), False, False, RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW, "Revoked")
          End If

          SomeTransactionChanged = True
        End If

      Next

      If (SomeTransactionChanged) Then

        MainForm.ReloadTable(RenaissanceChangeID.tblTradeFilePending, TransactionIDs, True) ' Raise Event from Reload function
        Call MainForm.ReloadTable_Background(RenaissanceChangeID.tblTransaction, TransactionIDs, False)
        Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblTransaction, TransactionIDs), True)

        'Lotfi: comment  cause done in the updateEvent
        'Call LoadApprovalsTableData()
        Call LoadFileTableData()
        SetSortedRows()

      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error revoking transaction approval.", ex.StackTrace, True)
    End Try

  End Sub

#End Region

#Region " Report Menu"

  ''' <summary>
  ''' Sets the field select menu.
  ''' </summary>
  ''' <param name="RootMenu">The root menu.</param>
  ''' <returns>ToolStripMenuItem.</returns>
  Private Function SetFieldSelectMenu(ByRef RootMenu As MenuStrip) As ToolStripMenuItem
    ' ***************************************************************************************
    ' Build the FieldSelect Menu.
    '
    ' Add an item for each field in the SelectTransactions table, when selected the item
    ' will show or hide the associated field on the transactions grid.
    ' ***************************************************************************************

    Dim ColumnNames(-1) As String
    Dim ColumnCount As Integer

    Dim FieldsMenu As New ToolStripMenuItem("Grid &Fields")
    FieldsMenu.Name = "Menu_GridField"

    Dim newMenuItem As ToolStripMenuItem

    ReDim ColumnNames(fileTable.Columns.Count - 1)
    For ColumnCount = 0 To (fileTable.Columns.Count - 1)
      ColumnNames(ColumnCount) = fileTable.Columns(ColumnCount).ColumnName
      If (ColumnNames(ColumnCount).StartsWith("Transaction")) Then
        ColumnNames(ColumnCount) = ColumnNames(ColumnCount).Substring(11) & "."
      End If
    Next
    Array.Sort(ColumnNames)

    For ColumnCount = 0 To (ColumnNames.Length - 1)
      newMenuItem = FieldsMenu.DropDownItems.Add(ColumnNames(ColumnCount), Nothing, AddressOf Me.ShowGridField)

      If (ColumnNames(ColumnCount).EndsWith(".")) Then
        newMenuItem.Name = "Menu_GridField_Transaction" & ColumnNames(ColumnCount).Substring(0, ColumnNames(ColumnCount).Length - 1)
      Else
        newMenuItem.Name = "Menu_GridField_" & ColumnNames(ColumnCount)
      End If
    Next

    RootMenu.Items.Add(FieldsMenu)
    Return FieldsMenu

  End Function

  ''' <summary>
  ''' Shows the grid field.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
  Private Sub ShowGridField(ByVal sender As Object, ByVal e As EventArgs)
    ' ***************************************************************************************
    ' Callback function for the Grid Fields Menu items.
    '
    ' ***************************************************************************************

    Try
      If TypeOf (sender) Is ToolStripMenuItem Then
        Dim thisMenuItem As ToolStripMenuItem
        Dim FieldName As String

        thisMenuItem = CType(sender, ToolStripMenuItem)

        FieldName = thisMenuItem.Name
        If (FieldName.StartsWith("Menu_GridField_")) Then
          FieldName = FieldName.Substring(15)
        End If

        thisMenuItem.Checked = Not thisMenuItem.Checked

        If (Grid_Transactions.Cols.Contains(FieldName)) Then
          If (thisMenuItem.Checked) Then
            Grid_Transactions.Cols(FieldName).Visible = True
          Else
            Grid_Transactions.Cols(FieldName).Visible = False
          End If
        End If

        If (Grid_ToBeWritten.Cols.Contains(FieldName)) Then
          If (thisMenuItem.Checked) Then
            Grid_ToBeWritten.Cols(FieldName).Visible = True
          Else
            Grid_ToBeWritten.Cols(FieldName).Visible = False
          End If
        End If
      End If
    Catch ex As Exception

    End Try
  End Sub

  ''' <summary>
  ''' Sets the transaction report menu.
  ''' </summary>
  ''' <param name="RootMenu">The root menu.</param>
  ''' <returns>MenuStrip.</returns>
  Private Function SetTransactionReportMenu(ByRef RootMenu As MenuStrip) As MenuStrip
    ' ***************************************************************************************
    ' Setup the Transaction Reports menu with appropriate items and callbacks
    '
    ' ***************************************************************************************

    Dim ReportMenu As New ToolStripMenuItem("Transaction &Reports")
    Dim newMenuItem As ToolStripMenuItem
    ReportMenu.Name = "Menu_TransactionReports"

    'newMenuItem = ReportMenu.DropDownItems.Add("Transaction &Ticket", Nothing, AddressOf Me.rptTransactionTicket)

    'ReportMenu.DropDownItems.Add(New ToolStripSeparator)

    newMenuItem = ReportMenu.DropDownItems.Add("&Trade Blotter", Nothing, AddressOf Me.rptFinalTradeBlotter)
    'newMenuItem = ReportMenu.DropDownItems.Add("Trade &Revision Blotter", Nothing, AddressOf Me.rptTradeRevisionBlotter)
    'newMenuItem = ReportMenu.DropDownItems.Add("Final Trade &Revision Blotter", Nothing, AddressOf Me.rptFinalTradeRevisionBlotter)

    'ReportMenu.DropDownItems.Add(New ToolStripSeparator)

    'newMenuItem = ReportMenu.DropDownItems.Add("F&X Blotter", Nothing, AddressOf Me.rptFXBlotter)
    'newMenuItem = ReportMenu.DropDownItems.Add("Final FX Blotter", Nothing, AddressOf Me.rptFinalFXBlotter)

    RootMenu.Items.Add(ReportMenu)
    Return RootMenu

  End Function


  ''' <summary>
  ''' RPTs the final trade blotter.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
  Private Sub rptFinalTradeBlotter(ByVal sender As Object, ByVal e As EventArgs)
    ' ***************************************************************************************
    '
    '
    ' ***************************************************************************************

    Call ShowTradeBlotter(True)
  End Sub

  ''' <summary>
  ''' Shows the trade blotter.
  ''' </summary>
  ''' <param name="pFinal">if set to <c>true</c> [p final].</param>
  Private Sub ShowTradeBlotter(Optional ByVal pFinal As Boolean = False)
    ' ***************************************************************************************
    '
    '
    ' ***************************************************************************************

    Dim TradeBlotterTicketNumber As String
    Dim ReportTable As New DataTable
    Dim ReportRow As DataRow

    Try
      TradeBlotterTicketNumber = MainForm.Get_SystemString("TradeBlotterTicketNumber")
      If IsNumeric(TradeBlotterTicketNumber) = False Then
        TradeBlotterTicketNumber = "000001"
        MainForm.Set_SystemString("TradeBlotterTicketNumber", TradeBlotterTicketNumber)
      ElseIf pFinal Then
        TradeBlotterTicketNumber = (CInt(TradeBlotterTicketNumber) + 1).ToString("000000")
        MainForm.Set_SystemString("TradeBlotterTicketNumber", TradeBlotterTicketNumber)
      End If

    Catch ex As Exception
      MainForm.LogError("ShowTradeBlotter()", LOG_LEVELS.Error, ex.Message, "Error getting header information.", ex.StackTrace, True)
      Exit Sub
    End Try

    ' Create reporting table

    ReportTable.Columns.Add(New System.Data.DataColumn("FundName", GetType(String)))
    ReportTable.Columns.Add(New System.Data.DataColumn("AccountNumber", GetType(String)))
    ReportTable.Columns.Add(New System.Data.DataColumn("TransactionType_Description", GetType(String)))
    ReportTable.Columns.Add(New System.Data.DataColumn("ISIN", GetType(String)))
    ReportTable.Columns.Add(New System.Data.DataColumn("InstrumentCurrency_Description", GetType(String)))
    ReportTable.Columns.Add(New System.Data.DataColumn("InstrumentDescription", GetType(String)))
    ReportTable.Columns.Add(New System.Data.DataColumn("SettlementCurrency_Description", GetType(String)))
    ReportTable.Columns.Add(New System.Data.DataColumn("Quantity_Text", GetType(String)))
    ReportTable.Columns.Add(New System.Data.DataColumn("Amount_Text", GetType(String)))
    ReportTable.Columns.Add(New System.Data.DataColumn("AmountCurrency_Description", GetType(String)))
    ReportTable.Columns.Add(New System.Data.DataColumn("TransactionParentID", GetType(Integer)))
    ReportTable.Columns.Add(New System.Data.DataColumn("TransactionID", GetType(Integer)))

    Try
      Dim SelectedRows As C1.Win.C1FlexGrid.RowCollection = Nothing
      Dim ThisRow As C1.Win.C1FlexGrid.Row = Nothing
      Dim ThisDataRow As System.Data.DataRow
      Dim ItemCount As Integer
      Dim TransactionParentID As Integer
      Dim TransactionFundID As Integer
      Dim TransactionTradeStatusID As Integer
      Dim InstrumentType As Integer
      Dim CurrencyID As Integer
      Dim InstrumentID As Integer
      Dim TransactionExchangeID As Integer = 0
      Dim SuggestedWorkflowRule As RenaissanceDataClass.DSWorkflowRules.tblWorkflowRulesRow = Nothing
      Dim ResultingWorkflow As RenaissanceDataClass.DSWorkflows.tblWorkflowsRow = Nothing
      Dim ThisBrokerAccount As RenaissanceDataClass.DSBrokerAccounts.tblBrokerAccountsRow = Nothing
      Dim BrokerID As Integer
      Dim VeniceAccountID As Integer
      Dim BrokerAccountDescription As String = ""
      Dim BrokerAccountNumber As String = ""

      SelectedRows = Grid_ToBeWritten.Rows.Selected

      If (SelectedRows IsNot Nothing) AndAlso (SelectedRows.Count > 0) Then

        For ItemCount = 0 To (SelectedRows.Count - 1)

          If (ItemCount < SelectedRows.Count) Then

            ThisRow = SelectedRows(ItemCount)
            ThisDataRow = fileDataView(ThisRow.DataIndex).Row

            TransactionParentID = CInt(Nz(ThisRow("TransactionParentID"), 0))
            TransactionFundID = CInt(Nz(ThisRow("TransactionFund"), 0))
            InstrumentType = CInt(Nz(ThisRow("InstrumentType"), 0))
            CurrencyID = CInt(Nz(ThisRow("InstrumentCurrencyID"), 0))
            InstrumentID = CInt(Nz(ThisRow("TransactionInstrument"), 0))
            BrokerID = CInt(Nz(ThisRow("TransactionBroker"), 0))
            TransactionTradeStatusID = CInt(Nz(ThisRow("TransactionTradeStatusID"), 0))

            SuggestedWorkflowRule = GetExpectedWorkflowRule(MainForm, TransactionFundID, InstrumentType, 0, CurrencyID, InstrumentID, TransactionTradeStatusID, False, True)
            ResultingWorkflow = Nothing

            If (SuggestedWorkflowRule IsNot Nothing) Then
              ResultingWorkflow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblWorkflows, CInt(Nz(SuggestedWorkflowRule.ResultingWorkflow, 0)))
            End If

            ThisBrokerAccount = GetExpectedBrokerAccount(MainForm, TransactionFundID, InstrumentType, BrokerID, TransactionExchangeID, CurrencyID, InstrumentID)

            BrokerID = ThisBrokerAccount.BrokerID
            BrokerAccountDescription = ThisBrokerAccount.AccountDescription
            BrokerAccountNumber = ThisBrokerAccount.AccountNumber

            ReportRow = ReportTable.NewRow

            Select Case CType(BrokerID, ExecutionBrokers)

              Case ExecutionBrokers.BNP_Paris

                Select Case CType(InstrumentType, RenaissanceGlobals.InstrumentTypes)

                  Case InstrumentTypes.Fund

                    Dim thisTradeObject As ITradeFileEntry

                    thisTradeObject = GetTradeObject(MainForm, TransactionParentID, ThisDataRow, SuggestedWorkflowRule.RuleID, CInt(Nz(SuggestedWorkflowRule.ResultingWorkflow, 0)), BrokerID, VeniceAccountID, BrokerAccountDescription, BrokerAccountNumber)

                    ReportRow("AccountNumber") = BrokerAccountNumber

                    Select Case thisTradeObject.TransactionType
                      Case TransactionTypes.Buy
                        ReportRow("TransactionType_Description") = "Souscription"

                      Case TransactionTypes.Sell
                        ReportRow("TransactionType_Description") = "Rachat"

                      Case Else
                        ReportRow("TransactionType_Description") = "ANOMALY !!!"

                    End Select

                    ReportRow("FundName") = Nz(ThisDataRow("FundName"), "")
                    ReportRow("ISIN") = thisTradeObject.ISIN
                    ReportRow("InstrumentCurrency_Description") = Nz(ThisDataRow("InstrumentCurrency"), "")
                    ReportRow("InstrumentDescription") = Nz(ThisDataRow("InstrumentDescription"), "")
                    ReportRow("Quantity_Text") = thisTradeObject.Quantity
                    ReportRow("Amount_Text") = thisTradeObject.Amount

                    If (Trim(thisTradeObject.Amount).Length > 0) Then
                      ReportRow("AmountCurrency_Description") = thisTradeObject.AmountCurrency
                    Else
                      ReportRow("AmountCurrency_Description") = ""
                    End If

                    ReportRow("TransactionParentID") = thisTradeObject.CustomerReference
                    ReportRow("TransactionID") = CInt(Nz(ThisDataRow("TransactionID"), 0))
                    ReportRow("SettlementCurrency_Description") = Nz(ThisDataRow("SettlementCurrencyCode"), "")

                  Case Else

                    ReportRow = Nothing

                End Select


              Case ExecutionBrokers.BNP_Luxembourg

                Select Case CType(InstrumentType, RenaissanceGlobals.InstrumentTypes)

                  Case InstrumentTypes.Fund

                    Dim thisTradeObject As ITradeFileEntry

                    thisTradeObject = GetTradeObject(MainForm, TransactionParentID, ThisDataRow, SuggestedWorkflowRule.RuleID, CInt(Nz(SuggestedWorkflowRule.ResultingWorkflow, 0)), BrokerID, VeniceAccountID, BrokerAccountDescription, BrokerAccountNumber)

                    ReportRow("AccountNumber") = BrokerAccountNumber

                    Select Case thisTradeObject.TransactionType
                      Case TransactionTypes.Buy
                        ReportRow("TransactionType_Description") = "Souscription"

                      Case TransactionTypes.Sell
                        ReportRow("TransactionType_Description") = "Rachat"

                      Case Else
                        ReportRow("TransactionType_Description") = "ANOMALY !!!"

                    End Select

                    ReportRow("FundName") = Nz(ThisDataRow("FundName"), "")
                    ReportRow("ISIN") = thisTradeObject.ISIN
                    ReportRow("InstrumentCurrency_Description") = Nz(ThisDataRow("InstrumentCurrency"), "")
                    ReportRow("InstrumentDescription") = Nz(ThisDataRow("InstrumentDescription"), "")
                    ReportRow("Quantity_Text") = thisTradeObject.Quantity
                    ReportRow("Amount_Text") = thisTradeObject.Amount

                    If (Trim(thisTradeObject.Amount).Length > 0) Then
                      ReportRow("AmountCurrency_Description") = thisTradeObject.AmountCurrency
                    Else
                      ReportRow("AmountCurrency_Description") = ""
                    End If

                    ReportRow("TransactionParentID") = thisTradeObject.CustomerReference
                    ReportRow("TransactionID") = CInt(Nz(ThisDataRow("TransactionID"), 0))
                    ReportRow("SettlementCurrency_Description") = Nz(ThisDataRow("SettlementCurrencyCode"), "")

                  Case Else

                    ReportRow = Nothing

                End Select

            End Select


            ' Add Report Row

            If (ReportRow IsNot Nothing) Then
              ReportTable.Rows.Add(ReportRow)
            End If

          End If

        Next

      End If

    Catch ex As Exception

    End Try

    ' 

    Call MainForm.MainReportHandler.rptTradeBlotterReport_UCITS("rptTransactionTradeUCITSFAX", TradeBlotterTicketNumber, New DataView(ReportTable), pFinal, Form_ProgressBar)

  End Sub

#End Region

#Region " Bug hunting "

  ' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
  ' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
  ' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

  ''' <summary>
  ''' Called when [row updating].
  ''' </summary>
  ''' <param name="Sender">The sender.</param>
  ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  ''' <summary>
  ''' Called when [row updated].
  ''' </summary>
  ''' <param name="Sender">The sender.</param>
  ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  ''' <summary>
  ''' Called when [row fill error].
  ''' </summary>
  ''' <param name="Sender">The sender.</param>
  ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
    Dim a As Integer

    a = 1
  End Sub

#End Region






  ' Lotfi: to get the instrument price of an instrument
  ' It's called to calculate the Instruiment weight in the fund
  ' duplicated from an other form frmManageSubFunds.vb :-( ( should be written once in some utilities module )
  Private BestPricesTable As DataTable = Nothing
  Private SortedPrices As DataRow()

  ''' <summary>
  ''' Gets the instrument price.
  ''' </summary>
  ''' <param name="InstrumentID">The instrument ID.</param>
  ''' <param name="PriceValue">(Output) Returns the PriceValue.</param>
  ''' <param name="PriceDate">(Output) Returns the PriceDate.</param>
  Private Sub GetInstrumentPrice(ByVal InstrumentID As Integer, ByRef PriceValue As Double, ByRef PriceDate As Date)
    ' ******************************************************************************************************
    '
    ' Return the Latest Price for a given Instrument from the sorted array of
    ' Instrument prices.
    '
    ' ******************************************************************************************************
    Static LastInstrumentID As Integer = 0
    Static LastInstrumentPrice As Double = 0
    Static LastInstrumentPriceDate As Date = Renaissance_BaseDate

    Try
      If (InstrumentID = LastInstrumentID) OrElse (InstrumentID <= 0) Then
        If (InstrumentID <= 0) Then ' Facilitate the explicit clearing of Static variables
          LastInstrumentID = 0
          LastInstrumentPrice = 0
          LastInstrumentPriceDate = Renaissance_BaseDate
        End If

        PriceValue = LastInstrumentPrice
        PriceDate = LastInstrumentPriceDate
        Exit Sub
      End If
    Catch ex As Exception
    End Try

    Try

      ' Check the Prices table is loaded and the sorted Prices array is established

      If (BestPricesTable Is Nothing) Then
        BestPricesTable = New DataTable
        Dim thisCommand As New SqlCommand

        Try

          thisCommand.CommandType = CommandType.Text
          thisCommand.CommandText = "SELECT InstrumentID, PriceDate, PriceLevel FROM dbo.fn_BestPrice(@ValueDate, @OnlyFinalPrices, @KnowledgeDate)"
          thisCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

          thisCommand.Parameters.Add(New SqlParameter("@ValueDate", SqlDbType.DateTime)).Value = RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW
          thisCommand.Parameters.Add(New SqlParameter("@OnlyFinalPrices", SqlDbType.Int)).Value = 0
          thisCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", SqlDbType.DateTime)).Value = MainForm.Main_Knowledgedate

          thisCommand.Connection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)

          If (thisCommand IsNot Nothing) AndAlso (thisCommand.Connection IsNot Nothing) Then

            SyncLock thisCommand.Connection
              MainForm.LoadTable_Custom(BestPricesTable, thisCommand)
            End SyncLock
          End If

          SortedPrices = BestPricesTable.Select("True", "InstrumentID, PriceDate")

        Catch ex As Exception

          MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error Getting Best Prices.", ex.Message, ex.StackTrace, True)

          If (BestPricesTable IsNot Nothing) Then
            BestPricesTable.Clear()
          End If

          BestPricesTable = Nothing
          SortedPrices = Nothing

        Finally

          If (thisCommand IsNot Nothing) Then
            thisCommand.Connection = Nothing
          End If
          thisCommand = Nothing

        End Try

      End If

      If (BestPricesTable Is Nothing) OrElse (SortedPrices.Length <= 0) Then
        LastInstrumentID = 0
        LastInstrumentPrice = 0
        LastInstrumentPriceDate = Renaissance_BaseDate

        PriceValue = LastInstrumentPrice
        PriceDate = LastInstrumentPriceDate
        Exit Sub
      End If

    Catch ex As Exception
      LastInstrumentID = 0
      LastInstrumentPrice = 0
      LastInstrumentPriceDate = Renaissance_BaseDate

      PriceValue = LastInstrumentPrice
      PriceDate = LastInstrumentPriceDate

      Exit Sub
    End Try

    ' Find Latest Price

    ' Code changed to use the fn_BestPrice function , thus there is only One Price per instrument.

    Dim FirstIndex As Integer
    Dim MidIndex As Integer
    Dim LastIndex As Integer
    Dim thisPriceRow As DataRow

    Dim InstrumentIdOrdinal As Integer = BestPricesTable.Columns.IndexOf("InstrumentID")
    Dim PriceLevelOrdinal As Integer = BestPricesTable.Columns.IndexOf("PriceLevel")
    Dim PriceDateOrdinal As Integer = BestPricesTable.Columns.IndexOf("PriceDate")

    Try
      FirstIndex = 0
      LastIndex = SortedPrices.Length - 1

      ' Check the First and last Items

      thisPriceRow = SortedPrices(FirstIndex)
      If CInt(thisPriceRow(InstrumentIdOrdinal)) = InstrumentID Then
        LastInstrumentID = InstrumentID
        LastInstrumentPrice = CDbl(thisPriceRow(PriceLevelOrdinal))
        LastInstrumentPriceDate = CDate(thisPriceRow(PriceDateOrdinal))

        PriceValue = LastInstrumentPrice
        PriceDate = LastInstrumentPriceDate

        Exit Sub
      End If

      thisPriceRow = SortedPrices(LastIndex)
      If CInt(thisPriceRow(InstrumentIdOrdinal)) = InstrumentID Then
        LastInstrumentID = InstrumentID
        LastInstrumentPrice = CDbl(thisPriceRow(PriceLevelOrdinal))
        LastInstrumentPriceDate = CDate(thisPriceRow(PriceDateOrdinal))

        PriceValue = LastInstrumentPrice
        PriceDate = LastInstrumentPriceDate

        Exit Sub
      End If

      ' Chop the array to find the Instrument.

      While (LastIndex > (FirstIndex + 1))

        MidIndex = CInt((LastIndex + FirstIndex) / 2)
        thisPriceRow = SortedPrices(MidIndex)

        If CInt(thisPriceRow(InstrumentIdOrdinal)) = InstrumentID Then
          LastInstrumentID = InstrumentID
          LastInstrumentPrice = CDbl(thisPriceRow(PriceLevelOrdinal))
          LastInstrumentPriceDate = CDate(thisPriceRow(PriceDateOrdinal))

          PriceValue = LastInstrumentPrice
          PriceDate = LastInstrumentPriceDate

          Exit Sub
        ElseIf CInt(thisPriceRow(InstrumentIdOrdinal)) < InstrumentID Then
          FirstIndex = MidIndex
        Else
          LastIndex = MidIndex
        End If

      End While
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error returning Best Price.", ex.Message, ex.StackTrace, True)
    End Try

    LastInstrumentID = 0
    LastInstrumentPrice = 0.0#
    LastInstrumentPriceDate = Renaissance_BaseDate

    PriceValue = LastInstrumentPrice
    PriceDate = LastInstrumentPriceDate

    Exit Sub

  End Sub

  Private Sub Text_KansasStatus_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Text_KansasStatus.KeyPress
    e.Handled = True
    Return
  End Sub


  'return the Fund NAVA
  Private Function getEuroFundNAV(ByVal FundID As Integer) As Double

    Dim NAVCommand As New SqlCommand
    Dim DataReader As New VeniceMain.GetDataReaderClass
    Dim NAVTable As New DataTable
    Try
      NAVCommand.CommandType = CommandType.StoredProcedure
      NAVCommand.CommandText = "spu_GetFundNAVs"
      NAVCommand.Parameters.Add(New SqlParameter("@FundID", SqlDbType.Int)).Value = FundID
      NAVCommand.Parameters.Add(New SqlParameter("@ValueDate", SqlDbType.DateTime)).Value = Now()
      NAVCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", SqlDbType.DateTime)).Value = RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW

      NAVCommand.Connection = MainForm.GetVeniceConnection

      DataReader.MyCommand = NAVCommand

      MainForm.RunDataReader(DataReader)

      If (DataReader.myReader Is Nothing) Then
        ' MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "DataReader.myReader is nothing in Check_ShowPercentOfNAV.", "", False)
        MainForm.LoadTable_Custom(NAVTable, NAVCommand)
        'NAVTable.Load(NAVCommand.ExecuteReader)
      Else
        'MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "DataReader.myReader OK in Check_ShowPercentOfNAV.", "", False)
        NAVTable.Load(DataReader.myReader)
      End If

      If (NAVTable.Rows.Count > 0) Then
        getEuroFundNAV = CDbl(NAVTable.Rows(0)("EURO_VALUE"))
        NAVTable.Rows.Clear()
      Else
        getEuroFundNAV = 0
      End If

      NAVTable = Nothing

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error geeting FundEuroNAV for fundId " + CStr(FundID), ex.StackTrace, False)
    Finally
      If (NAVCommand.Connection IsNot Nothing) Then
        If (NAVCommand.Connection.State And ConnectionState.Open) Then
          NAVCommand.Connection.Close()
        End If

        NAVCommand.Connection = Nothing
      End If
    End Try

  End Function

End Class


