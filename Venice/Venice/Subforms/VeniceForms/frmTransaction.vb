' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmTransaction.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals
Imports RenaissanceUtilities.DatePeriodFunctions


''' <summary>
''' Class frmTransaction
''' </summary>
Public Class frmTransaction

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmTransaction"/> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The LBL transaction
    ''' </summary>
  Friend WithEvents lblTransaction As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit audit ID
    ''' </summary>
  Friend WithEvents editAuditID As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The edit transaction comment
    ''' </summary>
  Friend WithEvents editTransactionComment As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The BTN nav first
    ''' </summary>
  Friend WithEvents btnNavFirst As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN nav prev
    ''' </summary>
  Friend WithEvents btnNavPrev As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN nav next
    ''' </summary>
  Friend WithEvents btnNavNext As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN last
    ''' </summary>
  Friend WithEvents btnLast As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN add
    ''' </summary>
  Friend WithEvents btnAdd As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN delete
    ''' </summary>
  Friend WithEvents btnDelete As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN cancel
    ''' </summary>
  Friend WithEvents btnCancel As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN save
    ''' </summary>
  Friend WithEvents btnSave As System.Windows.Forms.Button
    ''' <summary>
    ''' The combo_ select transaction
    ''' </summary>
  Friend WithEvents Combo_SelectTransaction As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The panel1
    ''' </summary>
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The label1
    ''' </summary>
  Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The group box1
    ''' </summary>
  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The BTN close
    ''' </summary>
  Friend WithEvents btnClose As System.Windows.Forms.Button
    ''' <summary>
    ''' The combo_ transaction fund
    ''' </summary>
  Friend WithEvents Combo_TransactionFund As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label_ cpty is fund
    ''' </summary>
  Friend WithEvents label_CptyIsFund As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ transaction instrument
    ''' </summary>
  Friend WithEvents Combo_TransactionInstrument As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label2
    ''' </summary>
  Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label3
    ''' </summary>
  Friend WithEvents Label3 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ transaction type
    ''' </summary>
  Friend WithEvents Combo_TransactionType As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ transaction counterparty
    ''' </summary>
  Friend WithEvents Combo_TransactionCounterparty As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ transaction group
    ''' </summary>
  Friend WithEvents Combo_TransactionGroup As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label5
    ''' </summary>
  Friend WithEvents Label5 As System.Windows.Forms.Label
    ''' <summary>
    ''' The group box2
    ''' </summary>
  Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The combo_ data entry manager
    ''' </summary>
	Friend WithEvents Combo_DataEntryManager As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ transaction manager
    ''' </summary>
	Friend WithEvents Combo_TransactionManager As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ investment manager
    ''' </summary>
	Friend WithEvents Combo_InvestmentManager As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The date_ decision date
    ''' </summary>
	Friend WithEvents Date_DecisionDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The date_ value date
    ''' </summary>
	Friend WithEvents Date_ValueDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The date_ confirmation date
    ''' </summary>
	Friend WithEvents Date_ConfirmationDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The date_ settlement date
    ''' </summary>
	Friend WithEvents Date_SettlementDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The group box4
    ''' </summary>
	Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The combo_ value or amount
    ''' </summary>
	Friend WithEvents Combo_ValueOrAmount As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label12
    ''' </summary>
	Friend WithEvents Label12 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ amount
    ''' </summary>
	Friend WithEvents edit_Amount As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The edit_ price
    ''' </summary>
	Friend WithEvents edit_Price As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The edit_settlement
    ''' </summary>
	Friend WithEvents edit_settlement As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The edit_ costs
    ''' </summary>
	Friend WithEvents edit_Costs As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The edit_ cost percentage
    ''' </summary>
	Friend WithEvents edit_CostPercentage As RenaissanceControls.PercentageTextBox
    ''' <summary>
    ''' The label17
    ''' </summary>
	Friend WithEvents Label17 As System.Windows.Forms.Label
    ''' <summary>
    ''' The check_ initial data entry
    ''' </summary>
	Friend WithEvents Check_InitialDataEntry As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ bank confirmation
    ''' </summary>
	Friend WithEvents Check_BankConfirmation As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ instruction given
    ''' </summary>
	Friend WithEvents Check_InstructionGiven As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ final data entry
    ''' </summary>
	Friend WithEvents Check_FinalDataEntry As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ settlement confirmed
    ''' </summary>
	Friend WithEvents Check_SettlementConfirmed As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ price confirmation
    ''' </summary>
	Friend WithEvents Check_PriceConfirmation As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The button_ get price
    ''' </summary>
	Friend WithEvents Button_GetPrice As System.Windows.Forms.Button
    ''' <summary>
    ''' The check_ final amount
    ''' </summary>
	Friend WithEvents Check_FinalAmount As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ final price
    ''' </summary>
	Friend WithEvents Check_FinalPrice As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ final settlement
    ''' </summary>
	Friend WithEvents Check_FinalSettlement As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ final costs
    ''' </summary>
	Friend WithEvents Check_FinalCosts As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The button_ view CT
    ''' </summary>
	Friend WithEvents Button_ViewCT As System.Windows.Forms.Button
    ''' <summary>
    ''' The label_ compound transaction
    ''' </summary>
	Friend WithEvents Label_CompoundTransaction As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ amount
    ''' </summary>
	Friend WithEvents Label_Amount As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ price
    ''' </summary>
	Friend WithEvents Label_Price As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ settlement
    ''' </summary>
	Friend WithEvents Label_Settlement As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ costs
    ''' </summary>
	Friend WithEvents Label_Costs As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ decision date
    ''' </summary>
	Friend WithEvents Label_DecisionDate As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ trade date
    ''' </summary>
	Friend WithEvents Label_TradeDate As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ confirmation date
    ''' </summary>
	Friend WithEvents Label_ConfirmationDate As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ settlement date
    ''' </summary>
	Friend WithEvents Label_SettlementDate As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ counterparty
    ''' </summary>
	Friend WithEvents Label_Counterparty As System.Windows.Forms.Label
    ''' <summary>
    ''' The BTN new using
    ''' </summary>
	Friend WithEvents btnNewUsing As System.Windows.Forms.Button
    ''' <summary>
    ''' The combo_ redemption ID
    ''' </summary>
	Friend WithEvents Combo_RedemptionID As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The root menu
    ''' </summary>
	Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
    ''' <summary>
    ''' The form_ status strip
    ''' </summary>
	Friend WithEvents Form_StatusStrip As System.Windows.Forms.StatusStrip
    ''' <summary>
    ''' The form_ progress bar
    ''' </summary>
	Friend WithEvents Form_ProgressBar As System.Windows.Forms.ToolStripProgressBar
    ''' <summary>
    ''' The label_ status
    ''' </summary>
	Friend WithEvents Label_Status As System.Windows.Forms.ToolStripStatusLabel
    ''' <summary>
    ''' The check_ dont update price
    ''' </summary>
	Friend WithEvents Check_DontUpdatePrice As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ costs as percent
    ''' </summary>
	Friend WithEvents Check_CostsAsPercent As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The button_ get amount
    ''' </summary>
	Friend WithEvents Button_GetAmount As System.Windows.Forms.Button
    ''' <summary>
    ''' The check_ blotter redeem whole holding
    ''' </summary>
	Friend WithEvents Check_BlotterRedeemWholeHolding As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The tab_ transaction details
    ''' </summary>
	Friend WithEvents Tab_TransactionDetails As System.Windows.Forms.TabControl
    ''' <summary>
    ''' The tab_ basic details
    ''' </summary>
	Friend WithEvents Tab_BasicDetails As System.Windows.Forms.TabPage
    ''' <summary>
    ''' The tab_ transfer details
    ''' </summary>
	Friend WithEvents Tab_TransferDetails As System.Windows.Forms.TabPage
    ''' <summary>
    ''' The check_ is transfer
    ''' </summary>
	Friend WithEvents Check_IsTransfer As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The date_ effective value date
    ''' </summary>
	Friend WithEvents Date_EffectiveValueDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The label4
    ''' </summary>
	Friend WithEvents Label4 As System.Windows.Forms.Label
    ''' <summary>
    ''' The check_ specific initial equalisation
    ''' </summary>
	Friend WithEvents Check_SpecificInitialEqualisation As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The label10
    ''' </summary>
	Friend WithEvents Label10 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ effective watermark
    ''' </summary>
	Friend WithEvents Edit_EffectiveWatermark As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The label9
    ''' </summary>
	Friend WithEvents Label9 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ effective price
    ''' </summary>
	Friend WithEvents Edit_EffectivePrice As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The label11
    ''' </summary>
	Friend WithEvents Label11 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ specific initial equalisation
    ''' </summary>
	Friend WithEvents Edit_SpecificInitialEqualisation As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The label13
    ''' </summary>
	Friend WithEvents Label13 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label14
    ''' </summary>
	Friend WithEvents Label14 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label15
    ''' </summary>
	Friend WithEvents Label15 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label16
    ''' </summary>
	Friend WithEvents Label16 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ versusinstrument
    ''' </summary>
	Friend WithEvents Combo_Versusinstrument As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ trade status
    ''' </summary>
	Friend WithEvents Combo_TradeStatus As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label6
    ''' </summary>
	Friend WithEvents Label6 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ versus futures instrument
    ''' </summary>
	Friend WithEvents Combo_VersusFuturesInstrument As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ workflow
    ''' </summary>
	Friend WithEvents Combo_Workflow As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label7
    ''' </summary>
	Friend WithEvents Label7 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label18
    ''' </summary>
	Friend WithEvents Label18 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ FX rate
    ''' </summary>
	Friend WithEvents edit_FXRate As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The combo_ settlement currency
    ''' </summary>
	Friend WithEvents Combo_SettlementCurrency As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The check_ FX settlement
    ''' </summary>
	Friend WithEvents Check_FXSettlement As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The label_ FX settlement amount
    ''' </summary>
	Friend WithEvents Label_FXSettlementAmount As System.Windows.Forms.Label
    ''' <summary>
    ''' The label19
    ''' </summary>
	Friend WithEvents Label19 As System.Windows.Forms.Label
    ''' <summary>
    ''' The tab_ legacy
    ''' </summary>
  Friend WithEvents Tab_Legacy As System.Windows.Forms.TabPage
    ''' <summary>
    ''' The button_ get FX
    ''' </summary>
  Friend WithEvents Button_GetFX As System.Windows.Forms.Button
    ''' <summary>
    ''' The label_ instrument currency
    ''' </summary>
  Friend WithEvents Label_InstrumentCurrency As System.Windows.Forms.Label
    ''' <summary>
    ''' The check_ by ISIN
    ''' </summary>
  Friend WithEvents Check_ByISIN As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The label_ transaction add or update
    ''' </summary>
  Friend WithEvents Label_TransactionAddOrUpdate As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ transaction status label
    ''' </summary>
  Friend WithEvents Label_TransactionStatusLabel As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_percent
    ''' </summary>
  Friend WithEvents Label_percent As System.Windows.Forms.Label
    ''' <summary>
    ''' The button_ get percent
    ''' </summary>
  Friend WithEvents Button_GetPercent As System.Windows.Forms.Button
  Friend WithEvents Combo_Broker As System.Windows.Forms.ComboBox
  Friend WithEvents Label8 As System.Windows.Forms.Label
  Friend WithEvents Combo_BrokerOrderType As System.Windows.Forms.ComboBox
  Friend WithEvents Label20 As System.Windows.Forms.Label
  Friend WithEvents Combo_SubFund As System.Windows.Forms.ComboBox
  Friend WithEvents Label21 As System.Windows.Forms.Label
  Friend WithEvents Label_BrokerInfo As System.Windows.Forms.Label
  Friend WithEvents Combo_OpeningOrClosingTrade As System.Windows.Forms.ComboBox
  Friend WithEvents Label_OpeningOrClosingTrade As System.Windows.Forms.Label
  Friend WithEvents Combo_SpecificShareClass As System.Windows.Forms.ComboBox
  Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
  Friend WithEvents Label_SpecificShareClass As System.Windows.Forms.Label
  Friend WithEvents ComboBox3 As System.Windows.Forms.ComboBox
  Friend WithEvents Form_StatusLabel As System.Windows.Forms.ToolStripStatusLabel
  Friend WithEvents Label_AccruedInterest As System.Windows.Forms.Label
  Friend WithEvents edit_AccruedInterest As RenaissanceControls.NumericTextBox
  Friend WithEvents Label_CleanPrice As System.Windows.Forms.Label
  Friend WithEvents edit_CleanPrice As RenaissanceControls.NumericTextBox
  Friend WithEvents Label_BrokerFees As System.Windows.Forms.Label
  Friend WithEvents edit_BrokerFees As RenaissanceControls.NumericTextBox
  Friend WithEvents Label_Principal As System.Windows.Forms.Label
  Friend WithEvents edit_Principal As RenaissanceControls.NumericTextBox
  Friend WithEvents Radio_BondByPrincipal As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_BondByPrice As System.Windows.Forms.RadioButton
  Friend WithEvents Check_ShowExpired As System.Windows.Forms.CheckBox
  Friend WithEvents Check_IsForwardPayment As System.Windows.Forms.CheckBox
  ''' <summary>
  ''' The label_ redemption ID
  ''' </summary>
  Friend WithEvents Label_RedemptionID As System.Windows.Forms.Label
  ''' <summary>
  ''' Initializes the component.
  ''' </summary>
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.lblTransaction = New System.Windows.Forms.Label
    Me.editAuditID = New System.Windows.Forms.TextBox
    Me.editTransactionComment = New System.Windows.Forms.TextBox
    Me.btnNavFirst = New System.Windows.Forms.Button
    Me.btnNavPrev = New System.Windows.Forms.Button
    Me.btnNavNext = New System.Windows.Forms.Button
    Me.btnLast = New System.Windows.Forms.Button
    Me.btnAdd = New System.Windows.Forms.Button
    Me.btnDelete = New System.Windows.Forms.Button
    Me.btnCancel = New System.Windows.Forms.Button
    Me.btnSave = New System.Windows.Forms.Button
    Me.Combo_SelectTransaction = New System.Windows.Forms.ComboBox
    Me.Panel1 = New System.Windows.Forms.Panel
    Me.btnNewUsing = New System.Windows.Forms.Button
    Me.Button_ViewCT = New System.Windows.Forms.Button
    Me.Label1 = New System.Windows.Forms.Label
    Me.GroupBox1 = New System.Windows.Forms.GroupBox
    Me.btnClose = New System.Windows.Forms.Button
    Me.Combo_TransactionFund = New System.Windows.Forms.ComboBox
    Me.label_CptyIsFund = New System.Windows.Forms.Label
    Me.Combo_TransactionInstrument = New System.Windows.Forms.ComboBox
    Me.Label2 = New System.Windows.Forms.Label
    Me.Combo_TransactionType = New System.Windows.Forms.ComboBox
    Me.Label3 = New System.Windows.Forms.Label
    Me.Combo_TransactionCounterparty = New System.Windows.Forms.ComboBox
    Me.Label_Counterparty = New System.Windows.Forms.Label
    Me.Combo_TransactionGroup = New System.Windows.Forms.ComboBox
    Me.Label5 = New System.Windows.Forms.Label
    Me.GroupBox2 = New System.Windows.Forms.GroupBox
    Me.Combo_DataEntryManager = New System.Windows.Forms.ComboBox
    Me.Combo_TransactionManager = New System.Windows.Forms.ComboBox
    Me.Combo_InvestmentManager = New System.Windows.Forms.ComboBox
    Me.Date_DecisionDate = New System.Windows.Forms.DateTimePicker
    Me.Label_DecisionDate = New System.Windows.Forms.Label
    Me.Date_ValueDate = New System.Windows.Forms.DateTimePicker
    Me.Label_TradeDate = New System.Windows.Forms.Label
    Me.Date_ConfirmationDate = New System.Windows.Forms.DateTimePicker
    Me.Label_ConfirmationDate = New System.Windows.Forms.Label
    Me.Date_SettlementDate = New System.Windows.Forms.DateTimePicker
    Me.Label_SettlementDate = New System.Windows.Forms.Label
    Me.GroupBox4 = New System.Windows.Forms.GroupBox
    Me.Combo_ValueOrAmount = New System.Windows.Forms.ComboBox
    Me.Label12 = New System.Windows.Forms.Label
    Me.Label_Amount = New System.Windows.Forms.Label
    Me.Label_Price = New System.Windows.Forms.Label
    Me.Label_Settlement = New System.Windows.Forms.Label
    Me.Label_Costs = New System.Windows.Forms.Label
    Me.Check_FinalAmount = New System.Windows.Forms.CheckBox
    Me.Check_FinalPrice = New System.Windows.Forms.CheckBox
    Me.Check_FinalSettlement = New System.Windows.Forms.CheckBox
    Me.Check_FinalCosts = New System.Windows.Forms.CheckBox
    Me.Label17 = New System.Windows.Forms.Label
    Me.Check_InitialDataEntry = New System.Windows.Forms.CheckBox
    Me.Check_BankConfirmation = New System.Windows.Forms.CheckBox
    Me.Check_InstructionGiven = New System.Windows.Forms.CheckBox
    Me.Check_FinalDataEntry = New System.Windows.Forms.CheckBox
    Me.Check_SettlementConfirmed = New System.Windows.Forms.CheckBox
    Me.Check_PriceConfirmation = New System.Windows.Forms.CheckBox
    Me.Button_GetPrice = New System.Windows.Forms.Button
    Me.Label_CompoundTransaction = New System.Windows.Forms.Label
    Me.Combo_RedemptionID = New System.Windows.Forms.ComboBox
    Me.Label_RedemptionID = New System.Windows.Forms.Label
    Me.RootMenu = New System.Windows.Forms.MenuStrip
    Me.Form_StatusStrip = New System.Windows.Forms.StatusStrip
    Me.Form_ProgressBar = New System.Windows.Forms.ToolStripProgressBar
    Me.Label_Status = New System.Windows.Forms.ToolStripStatusLabel
    Me.Form_StatusLabel = New System.Windows.Forms.ToolStripStatusLabel
    Me.Check_DontUpdatePrice = New System.Windows.Forms.CheckBox
    Me.Check_CostsAsPercent = New System.Windows.Forms.CheckBox
    Me.Button_GetAmount = New System.Windows.Forms.Button
    Me.Check_BlotterRedeemWholeHolding = New System.Windows.Forms.CheckBox
    Me.Tab_TransactionDetails = New System.Windows.Forms.TabControl
    Me.Tab_BasicDetails = New System.Windows.Forms.TabPage
    Me.Check_IsForwardPayment = New System.Windows.Forms.CheckBox
    Me.Radio_BondByPrincipal = New System.Windows.Forms.RadioButton
    Me.Radio_BondByPrice = New System.Windows.Forms.RadioButton
    Me.Label_BrokerFees = New System.Windows.Forms.Label
    Me.edit_BrokerFees = New RenaissanceControls.NumericTextBox
    Me.Label_AccruedInterest = New System.Windows.Forms.Label
    Me.edit_AccruedInterest = New RenaissanceControls.NumericTextBox
    Me.Label_CleanPrice = New System.Windows.Forms.Label
    Me.edit_CleanPrice = New RenaissanceControls.NumericTextBox
    Me.Button_GetPercent = New System.Windows.Forms.Button
    Me.Label_InstrumentCurrency = New System.Windows.Forms.Label
    Me.Button_GetFX = New System.Windows.Forms.Button
    Me.Check_FXSettlement = New System.Windows.Forms.CheckBox
    Me.Label_FXSettlementAmount = New System.Windows.Forms.Label
    Me.Label19 = New System.Windows.Forms.Label
    Me.Label18 = New System.Windows.Forms.Label
    Me.edit_FXRate = New RenaissanceControls.NumericTextBox
    Me.Combo_SettlementCurrency = New System.Windows.Forms.ComboBox
    Me.edit_Amount = New RenaissanceControls.NumericTextBox
    Me.edit_Price = New RenaissanceControls.NumericTextBox
    Me.edit_CostPercentage = New RenaissanceControls.PercentageTextBox
    Me.edit_Costs = New RenaissanceControls.NumericTextBox
    Me.edit_settlement = New RenaissanceControls.NumericTextBox
    Me.Label_percent = New System.Windows.Forms.Label
    Me.Label_Principal = New System.Windows.Forms.Label
    Me.edit_Principal = New RenaissanceControls.NumericTextBox
    Me.Tab_TransferDetails = New System.Windows.Forms.TabPage
    Me.Label16 = New System.Windows.Forms.Label
    Me.Label13 = New System.Windows.Forms.Label
    Me.Label14 = New System.Windows.Forms.Label
    Me.Label15 = New System.Windows.Forms.Label
    Me.Check_SpecificInitialEqualisation = New System.Windows.Forms.CheckBox
    Me.Check_IsTransfer = New System.Windows.Forms.CheckBox
    Me.Date_EffectiveValueDate = New System.Windows.Forms.DateTimePicker
    Me.Label10 = New System.Windows.Forms.Label
    Me.Label9 = New System.Windows.Forms.Label
    Me.Label4 = New System.Windows.Forms.Label
    Me.Label11 = New System.Windows.Forms.Label
    Me.Edit_SpecificInitialEqualisation = New RenaissanceControls.NumericTextBox
    Me.Edit_EffectiveWatermark = New RenaissanceControls.NumericTextBox
    Me.Edit_EffectivePrice = New RenaissanceControls.NumericTextBox
    Me.Tab_Legacy = New System.Windows.Forms.TabPage
    Me.Combo_Versusinstrument = New System.Windows.Forms.ComboBox
    Me.Combo_TradeStatus = New System.Windows.Forms.ComboBox
    Me.Label6 = New System.Windows.Forms.Label
    Me.Combo_VersusFuturesInstrument = New System.Windows.Forms.ComboBox
    Me.Combo_Workflow = New System.Windows.Forms.ComboBox
    Me.Label7 = New System.Windows.Forms.Label
    Me.Check_ByISIN = New System.Windows.Forms.CheckBox
    Me.Label_TransactionAddOrUpdate = New System.Windows.Forms.Label
    Me.Label_TransactionStatusLabel = New System.Windows.Forms.Label
    Me.Combo_Broker = New System.Windows.Forms.ComboBox
    Me.Label8 = New System.Windows.Forms.Label
    Me.Combo_BrokerOrderType = New System.Windows.Forms.ComboBox
    Me.Label20 = New System.Windows.Forms.Label
    Me.Combo_SubFund = New System.Windows.Forms.ComboBox
    Me.Label21 = New System.Windows.Forms.Label
    Me.Label_BrokerInfo = New System.Windows.Forms.Label
    Me.Combo_OpeningOrClosingTrade = New System.Windows.Forms.ComboBox
    Me.Label_OpeningOrClosingTrade = New System.Windows.Forms.Label
    Me.Combo_SpecificShareClass = New System.Windows.Forms.ComboBox
    Me.ComboBox2 = New System.Windows.Forms.ComboBox
    Me.Label_SpecificShareClass = New System.Windows.Forms.Label
    Me.ComboBox3 = New System.Windows.Forms.ComboBox
    Me.Check_ShowExpired = New System.Windows.Forms.CheckBox
    Me.Panel1.SuspendLayout()
    Me.Form_StatusStrip.SuspendLayout()
    Me.Tab_TransactionDetails.SuspendLayout()
    Me.Tab_BasicDetails.SuspendLayout()
    Me.Tab_TransferDetails.SuspendLayout()
    Me.Tab_Legacy.SuspendLayout()
    Me.SuspendLayout()
    '
    'lblTransaction
    '
    Me.lblTransaction.Location = New System.Drawing.Point(8, 624)
    Me.lblTransaction.Name = "lblTransaction"
    Me.lblTransaction.Size = New System.Drawing.Size(100, 20)
    Me.lblTransaction.TabIndex = 43
    Me.lblTransaction.Text = "Comment"
    '
    'editAuditID
    '
    Me.editAuditID.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.editAuditID.Enabled = False
    Me.editAuditID.Location = New System.Drawing.Point(535, 32)
    Me.editAuditID.Name = "editAuditID"
    Me.editAuditID.Size = New System.Drawing.Size(68, 20)
    Me.editAuditID.TabIndex = 1
    '
    'editTransactionComment
    '
    Me.editTransactionComment.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.editTransactionComment.Location = New System.Drawing.Point(117, 611)
    Me.editTransactionComment.Multiline = True
    Me.editTransactionComment.Name = "editTransactionComment"
    Me.editTransactionComment.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.editTransactionComment.Size = New System.Drawing.Size(483, 52)
    Me.editTransactionComment.TabIndex = 18
    '
    'btnNavFirst
    '
    Me.btnNavFirst.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNavFirst.Location = New System.Drawing.Point(104, 6)
    Me.btnNavFirst.Name = "btnNavFirst"
    Me.btnNavFirst.Size = New System.Drawing.Size(40, 28)
    Me.btnNavFirst.TabIndex = 1
    Me.btnNavFirst.Text = "<<"
    '
    'btnNavPrev
    '
    Me.btnNavPrev.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNavPrev.Location = New System.Drawing.Point(145, 6)
    Me.btnNavPrev.Name = "btnNavPrev"
    Me.btnNavPrev.Size = New System.Drawing.Size(35, 28)
    Me.btnNavPrev.TabIndex = 2
    Me.btnNavPrev.Text = "<"
    '
    'btnNavNext
    '
    Me.btnNavNext.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNavNext.Location = New System.Drawing.Point(184, 6)
    Me.btnNavNext.Name = "btnNavNext"
    Me.btnNavNext.Size = New System.Drawing.Size(35, 28)
    Me.btnNavNext.TabIndex = 3
    Me.btnNavNext.Text = ">"
    '
    'btnLast
    '
    Me.btnLast.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnLast.Location = New System.Drawing.Point(220, 6)
    Me.btnLast.Name = "btnLast"
    Me.btnLast.Size = New System.Drawing.Size(40, 28)
    Me.btnLast.TabIndex = 4
    Me.btnLast.Text = ">>"
    '
    'btnAdd
    '
    Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnAdd.Location = New System.Drawing.Point(272, 6)
    Me.btnAdd.Name = "btnAdd"
    Me.btnAdd.Size = New System.Drawing.Size(75, 28)
    Me.btnAdd.TabIndex = 5
    Me.btnAdd.Text = "&New"
    '
    'btnDelete
    '
    Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnDelete.Location = New System.Drawing.Point(520, 739)
    Me.btnDelete.Name = "btnDelete"
    Me.btnDelete.Size = New System.Drawing.Size(75, 28)
    Me.btnDelete.TabIndex = 24
    Me.btnDelete.Text = "&Delete"
    Me.btnDelete.Visible = False
    '
    'btnCancel
    '
    Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnCancel.Location = New System.Drawing.Point(345, 739)
    Me.btnCancel.Name = "btnCancel"
    Me.btnCancel.Size = New System.Drawing.Size(75, 28)
    Me.btnCancel.TabIndex = 22
    Me.btnCancel.Text = "&Cancel"
    '
    'btnSave
    '
    Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnSave.Location = New System.Drawing.Point(72, 739)
    Me.btnSave.Name = "btnSave"
    Me.btnSave.Size = New System.Drawing.Size(75, 28)
    Me.btnSave.TabIndex = 21
    Me.btnSave.Text = "&Save"
    '
    'Combo_SelectTransaction
    '
    Me.Combo_SelectTransaction.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_SelectTransaction.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectTransaction.Location = New System.Drawing.Point(81, 32)
    Me.Combo_SelectTransaction.Name = "Combo_SelectTransaction"
    Me.Combo_SelectTransaction.Size = New System.Drawing.Size(27, 21)
    Me.Combo_SelectTransaction.TabIndex = 0
    Me.Combo_SelectTransaction.Visible = False
    '
    'Panel1
    '
    Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Panel1.Controls.Add(Me.btnNewUsing)
    Me.Panel1.Controls.Add(Me.btnNavFirst)
    Me.Panel1.Controls.Add(Me.btnNavPrev)
    Me.Panel1.Controls.Add(Me.btnNavNext)
    Me.Panel1.Controls.Add(Me.btnLast)
    Me.Panel1.Controls.Add(Me.btnAdd)
    Me.Panel1.Controls.Add(Me.Button_ViewCT)
    Me.Panel1.Location = New System.Drawing.Point(68, 690)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(444, 43)
    Me.Panel1.TabIndex = 20
    '
    'btnNewUsing
    '
    Me.btnNewUsing.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNewUsing.Location = New System.Drawing.Point(360, 6)
    Me.btnNewUsing.Name = "btnNewUsing"
    Me.btnNewUsing.Size = New System.Drawing.Size(75, 28)
    Me.btnNewUsing.TabIndex = 6
    Me.btnNewUsing.Text = "New &Using"
    '
    'Button_ViewCT
    '
    Me.Button_ViewCT.Enabled = False
    Me.Button_ViewCT.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Button_ViewCT.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Button_ViewCT.Location = New System.Drawing.Point(3, 6)
    Me.Button_ViewCT.Name = "Button_ViewCT"
    Me.Button_ViewCT.Size = New System.Drawing.Size(94, 28)
    Me.Button_ViewCT.TabIndex = 0
    Me.Button_ViewCT.Text = "View Compound Transaction"
    Me.Button_ViewCT.Visible = False
    '
    'Label1
    '
    Me.Label1.Location = New System.Drawing.Point(8, 32)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(100, 23)
    Me.Label1.TabIndex = 42
    Me.Label1.Text = "Select"
    Me.Label1.Visible = False
    '
    'GroupBox1
    '
    Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.GroupBox1.Location = New System.Drawing.Point(8, 57)
    Me.GroupBox1.Name = "GroupBox1"
    Me.GroupBox1.Size = New System.Drawing.Size(591, 4)
    Me.GroupBox1.TabIndex = 77
    Me.GroupBox1.TabStop = False
    Me.GroupBox1.Text = "GroupBox1"
    '
    'btnClose
    '
    Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnClose.Location = New System.Drawing.Point(429, 739)
    Me.btnClose.Name = "btnClose"
    Me.btnClose.Size = New System.Drawing.Size(75, 28)
    Me.btnClose.TabIndex = 23
    Me.btnClose.Text = "&Close"
    '
    'Combo_TransactionFund
    '
    Me.Combo_TransactionFund.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_TransactionFund.Enabled = False
    Me.Combo_TransactionFund.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_TransactionFund.Location = New System.Drawing.Point(120, 66)
    Me.Combo_TransactionFund.Name = "Combo_TransactionFund"
    Me.Combo_TransactionFund.Size = New System.Drawing.Size(483, 21)
    Me.Combo_TransactionFund.TabIndex = 2
    '
    'label_CptyIsFund
    '
    Me.label_CptyIsFund.Location = New System.Drawing.Point(8, 70)
    Me.label_CptyIsFund.Name = "label_CptyIsFund"
    Me.label_CptyIsFund.Size = New System.Drawing.Size(80, 16)
    Me.label_CptyIsFund.TabIndex = 80
    Me.label_CptyIsFund.Text = "Fund"
    '
    'Combo_TransactionInstrument
    '
    Me.Combo_TransactionInstrument.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_TransactionInstrument.Enabled = False
    Me.Combo_TransactionInstrument.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_TransactionInstrument.Location = New System.Drawing.Point(120, 118)
    Me.Combo_TransactionInstrument.Name = "Combo_TransactionInstrument"
    Me.Combo_TransactionInstrument.Size = New System.Drawing.Size(392, 21)
    Me.Combo_TransactionInstrument.TabIndex = 4
    '
    'Label2
    '
    Me.Label2.Location = New System.Drawing.Point(8, 122)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(104, 16)
    Me.Label2.TabIndex = 82
    Me.Label2.Text = "Instrument"
    '
    'Combo_TransactionType
    '
    Me.Combo_TransactionType.Enabled = False
    Me.Combo_TransactionType.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_TransactionType.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Combo_TransactionType.Location = New System.Drawing.Point(120, 144)
    Me.Combo_TransactionType.Name = "Combo_TransactionType"
    Me.Combo_TransactionType.Size = New System.Drawing.Size(174, 21)
    Me.Combo_TransactionType.TabIndex = 6
    '
    'Label3
    '
    Me.Label3.Location = New System.Drawing.Point(8, 148)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(102, 16)
    Me.Label3.TabIndex = 84
    Me.Label3.Text = "Transaction Type"
    '
    'Combo_TransactionCounterparty
    '
    Me.Combo_TransactionCounterparty.Enabled = False
    Me.Combo_TransactionCounterparty.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_TransactionCounterparty.Location = New System.Drawing.Point(120, 170)
    Me.Combo_TransactionCounterparty.Name = "Combo_TransactionCounterparty"
    Me.Combo_TransactionCounterparty.Size = New System.Drawing.Size(174, 21)
    Me.Combo_TransactionCounterparty.TabIndex = 7
    '
    'Label_Counterparty
    '
    Me.Label_Counterparty.Location = New System.Drawing.Point(8, 174)
    Me.Label_Counterparty.Name = "Label_Counterparty"
    Me.Label_Counterparty.Size = New System.Drawing.Size(104, 16)
    Me.Label_Counterparty.TabIndex = 86
    Me.Label_Counterparty.Text = "Counterparty"
    '
    'Combo_TransactionGroup
    '
    Me.Combo_TransactionGroup.Enabled = False
    Me.Combo_TransactionGroup.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_TransactionGroup.Location = New System.Drawing.Point(406, 156)
    Me.Combo_TransactionGroup.Name = "Combo_TransactionGroup"
    Me.Combo_TransactionGroup.Size = New System.Drawing.Size(174, 21)
    Me.Combo_TransactionGroup.TabIndex = 6
    Me.Combo_TransactionGroup.Visible = False
    '
    'Label5
    '
    Me.Label5.Location = New System.Drawing.Point(294, 160)
    Me.Label5.Name = "Label5"
    Me.Label5.Size = New System.Drawing.Size(104, 16)
    Me.Label5.TabIndex = 88
    Me.Label5.Text = "Transaction Group"
    Me.Label5.Visible = False
    '
    'GroupBox2
    '
    Me.GroupBox2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.GroupBox2.Location = New System.Drawing.Point(8, 310)
    Me.GroupBox2.Name = "GroupBox2"
    Me.GroupBox2.Size = New System.Drawing.Size(591, 4)
    Me.GroupBox2.TabIndex = 89
    Me.GroupBox2.TabStop = False
    Me.GroupBox2.Text = "GroupBox2"
    '
    'Combo_DataEntryManager
    '
    Me.Combo_DataEntryManager.Enabled = False
    Me.Combo_DataEntryManager.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_DataEntryManager.Location = New System.Drawing.Point(59, 44)
    Me.Combo_DataEntryManager.Name = "Combo_DataEntryManager"
    Me.Combo_DataEntryManager.Size = New System.Drawing.Size(22, 21)
    Me.Combo_DataEntryManager.TabIndex = 202
    Me.Combo_DataEntryManager.Visible = False
    '
    'Combo_TransactionManager
    '
    Me.Combo_TransactionManager.Enabled = False
    Me.Combo_TransactionManager.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_TransactionManager.Location = New System.Drawing.Point(31, 44)
    Me.Combo_TransactionManager.Name = "Combo_TransactionManager"
    Me.Combo_TransactionManager.Size = New System.Drawing.Size(22, 21)
    Me.Combo_TransactionManager.TabIndex = 201
    Me.Combo_TransactionManager.Visible = False
    '
    'Combo_InvestmentManager
    '
    Me.Combo_InvestmentManager.Enabled = False
    Me.Combo_InvestmentManager.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_InvestmentManager.Location = New System.Drawing.Point(3, 43)
    Me.Combo_InvestmentManager.Name = "Combo_InvestmentManager"
    Me.Combo_InvestmentManager.Size = New System.Drawing.Size(22, 21)
    Me.Combo_InvestmentManager.TabIndex = 200
    Me.Combo_InvestmentManager.Visible = False
    '
    'Date_DecisionDate
    '
    Me.Date_DecisionDate.CustomFormat = "dd MMM yyyy HH:mm:ss"
    Me.Date_DecisionDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
    Me.Date_DecisionDate.Location = New System.Drawing.Point(108, 11)
    Me.Date_DecisionDate.Name = "Date_DecisionDate"
    Me.Date_DecisionDate.Size = New System.Drawing.Size(153, 20)
    Me.Date_DecisionDate.TabIndex = 12
    '
    'Label_DecisionDate
    '
    Me.Label_DecisionDate.Location = New System.Drawing.Point(9, 11)
    Me.Label_DecisionDate.Name = "Label_DecisionDate"
    Me.Label_DecisionDate.Size = New System.Drawing.Size(90, 20)
    Me.Label_DecisionDate.TabIndex = 98
    Me.Label_DecisionDate.Text = "Decision Date"
    '
    'Date_ValueDate
    '
    Me.Date_ValueDate.Location = New System.Drawing.Point(98, 319)
    Me.Date_ValueDate.Name = "Date_ValueDate"
    Me.Date_ValueDate.Size = New System.Drawing.Size(200, 20)
    Me.Date_ValueDate.TabIndex = 15
    '
    'Label_TradeDate
    '
    Me.Label_TradeDate.Location = New System.Drawing.Point(8, 323)
    Me.Label_TradeDate.Name = "Label_TradeDate"
    Me.Label_TradeDate.Size = New System.Drawing.Size(84, 16)
    Me.Label_TradeDate.TabIndex = 100
    Me.Label_TradeDate.Text = "NAV Date"
    '
    'Date_ConfirmationDate
    '
    Me.Date_ConfirmationDate.Location = New System.Drawing.Point(131, 45)
    Me.Date_ConfirmationDate.Name = "Date_ConfirmationDate"
    Me.Date_ConfirmationDate.Size = New System.Drawing.Size(16, 20)
    Me.Date_ConfirmationDate.TabIndex = 15
    Me.Date_ConfirmationDate.Visible = False
    '
    'Label_ConfirmationDate
    '
    Me.Label_ConfirmationDate.Location = New System.Drawing.Point(115, 46)
    Me.Label_ConfirmationDate.Name = "Label_ConfirmationDate"
    Me.Label_ConfirmationDate.Size = New System.Drawing.Size(16, 16)
    Me.Label_ConfirmationDate.TabIndex = 104
    Me.Label_ConfirmationDate.Text = "Confirmation Date"
    Me.Label_ConfirmationDate.Visible = False
    '
    'Date_SettlementDate
    '
    Me.Date_SettlementDate.Location = New System.Drawing.Point(400, 319)
    Me.Date_SettlementDate.Name = "Date_SettlementDate"
    Me.Date_SettlementDate.Size = New System.Drawing.Size(200, 20)
    Me.Date_SettlementDate.TabIndex = 16
    '
    'Label_SettlementDate
    '
    Me.Label_SettlementDate.Location = New System.Drawing.Point(307, 323)
    Me.Label_SettlementDate.Name = "Label_SettlementDate"
    Me.Label_SettlementDate.Size = New System.Drawing.Size(89, 16)
    Me.Label_SettlementDate.TabIndex = 102
    Me.Label_SettlementDate.Text = "Settlement Date"
    '
    'GroupBox4
    '
    Me.GroupBox4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.GroupBox4.Location = New System.Drawing.Point(8, 344)
    Me.GroupBox4.Name = "GroupBox4"
    Me.GroupBox4.Size = New System.Drawing.Size(591, 4)
    Me.GroupBox4.TabIndex = 105
    Me.GroupBox4.TabStop = False
    Me.GroupBox4.Text = "GroupBox4"
    '
    'Combo_ValueOrAmount
    '
    Me.Combo_ValueOrAmount.Enabled = False
    Me.Combo_ValueOrAmount.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_ValueOrAmount.Items.AddRange(New Object() {"Value", "Number of Shares"})
    Me.Combo_ValueOrAmount.Location = New System.Drawing.Point(122, 5)
    Me.Combo_ValueOrAmount.Name = "Combo_ValueOrAmount"
    Me.Combo_ValueOrAmount.Size = New System.Drawing.Size(140, 21)
    Me.Combo_ValueOrAmount.TabIndex = 0
    '
    'Label12
    '
    Me.Label12.Location = New System.Drawing.Point(10, 9)
    Me.Label12.Name = "Label12"
    Me.Label12.Size = New System.Drawing.Size(104, 16)
    Me.Label12.TabIndex = 27
    Me.Label12.Text = "Value or Amount"
    '
    'Label_Amount
    '
    Me.Label_Amount.Location = New System.Drawing.Point(10, 36)
    Me.Label_Amount.Name = "Label_Amount"
    Me.Label_Amount.Size = New System.Drawing.Size(68, 16)
    Me.Label_Amount.TabIndex = 22
    Me.Label_Amount.Text = "Units or %"
    '
    'Label_Price
    '
    Me.Label_Price.Location = New System.Drawing.Point(10, 60)
    Me.Label_Price.Name = "Label_Price"
    Me.Label_Price.Size = New System.Drawing.Size(68, 16)
    Me.Label_Price.TabIndex = 24
    Me.Label_Price.Text = "Price"
    '
    'Label_Settlement
    '
    Me.Label_Settlement.Location = New System.Drawing.Point(10, 202)
    Me.Label_Settlement.Name = "Label_Settlement"
    Me.Label_Settlement.Size = New System.Drawing.Size(108, 16)
    Me.Label_Settlement.TabIndex = 26
    Me.Label_Settlement.Text = "Settlement"
    '
    'Label_Costs
    '
    Me.Label_Costs.Location = New System.Drawing.Point(10, 178)
    Me.Label_Costs.Name = "Label_Costs"
    Me.Label_Costs.Size = New System.Drawing.Size(108, 16)
    Me.Label_Costs.TabIndex = 25
    Me.Label_Costs.Text = "Other Costs"
    '
    'Check_FinalAmount
    '
    Me.Check_FinalAmount.BackColor = System.Drawing.SystemColors.Control
    Me.Check_FinalAmount.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_FinalAmount.Location = New System.Drawing.Point(13, 89)
    Me.Check_FinalAmount.Name = "Check_FinalAmount"
    Me.Check_FinalAmount.Size = New System.Drawing.Size(16, 16)
    Me.Check_FinalAmount.TabIndex = 10
    Me.Check_FinalAmount.UseVisualStyleBackColor = False
    Me.Check_FinalAmount.Visible = False
    '
    'Check_FinalPrice
    '
    Me.Check_FinalPrice.BackColor = System.Drawing.SystemColors.Control
    Me.Check_FinalPrice.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_FinalPrice.Location = New System.Drawing.Point(13, 113)
    Me.Check_FinalPrice.Name = "Check_FinalPrice"
    Me.Check_FinalPrice.Size = New System.Drawing.Size(16, 16)
    Me.Check_FinalPrice.TabIndex = 11
    Me.Check_FinalPrice.UseVisualStyleBackColor = False
    Me.Check_FinalPrice.Visible = False
    '
    'Check_FinalSettlement
    '
    Me.Check_FinalSettlement.BackColor = System.Drawing.SystemColors.Control
    Me.Check_FinalSettlement.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_FinalSettlement.Location = New System.Drawing.Point(13, 161)
    Me.Check_FinalSettlement.Name = "Check_FinalSettlement"
    Me.Check_FinalSettlement.Size = New System.Drawing.Size(16, 16)
    Me.Check_FinalSettlement.TabIndex = 13
    Me.Check_FinalSettlement.UseVisualStyleBackColor = False
    Me.Check_FinalSettlement.Visible = False
    '
    'Check_FinalCosts
    '
    Me.Check_FinalCosts.BackColor = System.Drawing.SystemColors.Control
    Me.Check_FinalCosts.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_FinalCosts.Location = New System.Drawing.Point(13, 137)
    Me.Check_FinalCosts.Name = "Check_FinalCosts"
    Me.Check_FinalCosts.Size = New System.Drawing.Size(16, 16)
    Me.Check_FinalCosts.TabIndex = 12
    Me.Check_FinalCosts.UseVisualStyleBackColor = False
    Me.Check_FinalCosts.Visible = False
    '
    'Label17
    '
    Me.Label17.Location = New System.Drawing.Point(9, 70)
    Me.Label17.Name = "Label17"
    Me.Label17.Size = New System.Drawing.Size(32, 15)
    Me.Label17.TabIndex = 9
    Me.Label17.Text = "Final"
    Me.Label17.Visible = False
    '
    'Check_InitialDataEntry
    '
    Me.Check_InitialDataEntry.BackColor = System.Drawing.SystemColors.Control
    Me.Check_InitialDataEntry.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_InitialDataEntry.Location = New System.Drawing.Point(44, 161)
    Me.Check_InitialDataEntry.Name = "Check_InitialDataEntry"
    Me.Check_InitialDataEntry.Size = New System.Drawing.Size(132, 16)
    Me.Check_InitialDataEntry.TabIndex = 17
    Me.Check_InitialDataEntry.Text = "Initial Data Entry"
    Me.Check_InitialDataEntry.UseVisualStyleBackColor = False
    Me.Check_InitialDataEntry.Visible = False
    '
    'Check_BankConfirmation
    '
    Me.Check_BankConfirmation.BackColor = System.Drawing.SystemColors.Control
    Me.Check_BankConfirmation.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_BankConfirmation.Location = New System.Drawing.Point(44, 137)
    Me.Check_BankConfirmation.Name = "Check_BankConfirmation"
    Me.Check_BankConfirmation.Size = New System.Drawing.Size(132, 16)
    Me.Check_BankConfirmation.TabIndex = 16
    Me.Check_BankConfirmation.Text = "Bank Confirmation"
    Me.Check_BankConfirmation.UseVisualStyleBackColor = False
    Me.Check_BankConfirmation.Visible = False
    '
    'Check_InstructionGiven
    '
    Me.Check_InstructionGiven.BackColor = System.Drawing.SystemColors.Control
    Me.Check_InstructionGiven.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_InstructionGiven.Location = New System.Drawing.Point(44, 113)
    Me.Check_InstructionGiven.Name = "Check_InstructionGiven"
    Me.Check_InstructionGiven.Size = New System.Drawing.Size(132, 16)
    Me.Check_InstructionGiven.TabIndex = 15
    Me.Check_InstructionGiven.Text = "Instruction Given"
    Me.Check_InstructionGiven.UseVisualStyleBackColor = False
    Me.Check_InstructionGiven.Visible = False
    '
    'Check_FinalDataEntry
    '
    Me.Check_FinalDataEntry.BackColor = System.Drawing.SystemColors.Control
    Me.Check_FinalDataEntry.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_FinalDataEntry.Location = New System.Drawing.Point(184, 161)
    Me.Check_FinalDataEntry.Name = "Check_FinalDataEntry"
    Me.Check_FinalDataEntry.Size = New System.Drawing.Size(107, 16)
    Me.Check_FinalDataEntry.TabIndex = 20
    Me.Check_FinalDataEntry.Text = "Final Data Entry"
    Me.Check_FinalDataEntry.UseVisualStyleBackColor = False
    Me.Check_FinalDataEntry.Visible = False
    '
    'Check_SettlementConfirmed
    '
    Me.Check_SettlementConfirmed.BackColor = System.Drawing.SystemColors.Control
    Me.Check_SettlementConfirmed.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_SettlementConfirmed.Location = New System.Drawing.Point(184, 137)
    Me.Check_SettlementConfirmed.Name = "Check_SettlementConfirmed"
    Me.Check_SettlementConfirmed.Size = New System.Drawing.Size(136, 16)
    Me.Check_SettlementConfirmed.TabIndex = 19
    Me.Check_SettlementConfirmed.Text = "Settlement Confirmed"
    Me.Check_SettlementConfirmed.UseVisualStyleBackColor = False
    Me.Check_SettlementConfirmed.Visible = False
    '
    'Check_PriceConfirmation
    '
    Me.Check_PriceConfirmation.BackColor = System.Drawing.SystemColors.Control
    Me.Check_PriceConfirmation.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_PriceConfirmation.Location = New System.Drawing.Point(184, 113)
    Me.Check_PriceConfirmation.Name = "Check_PriceConfirmation"
    Me.Check_PriceConfirmation.Size = New System.Drawing.Size(136, 16)
    Me.Check_PriceConfirmation.TabIndex = 18
    Me.Check_PriceConfirmation.Text = "Price Confirmation"
    Me.Check_PriceConfirmation.UseVisualStyleBackColor = False
    Me.Check_PriceConfirmation.Visible = False
    '
    'Button_GetPrice
    '
    Me.Button_GetPrice.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Button_GetPrice.Location = New System.Drawing.Point(82, 56)
    Me.Button_GetPrice.Name = "Button_GetPrice"
    Me.Button_GetPrice.Size = New System.Drawing.Size(36, 20)
    Me.Button_GetPrice.TabIndex = 3
    Me.Button_GetPrice.TabStop = False
    Me.Button_GetPrice.Text = "Get"
    '
    'Label_CompoundTransaction
    '
    Me.Label_CompoundTransaction.Location = New System.Drawing.Point(122, 668)
    Me.Label_CompoundTransaction.Name = "Label_CompoundTransaction"
    Me.Label_CompoundTransaction.Size = New System.Drawing.Size(464, 20)
    Me.Label_CompoundTransaction.TabIndex = 19
    '
    'Combo_RedemptionID
    '
    Me.Combo_RedemptionID.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_RedemptionID.Enabled = False
    Me.Combo_RedemptionID.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_RedemptionID.Location = New System.Drawing.Point(192, 255)
    Me.Combo_RedemptionID.Name = "Combo_RedemptionID"
    Me.Combo_RedemptionID.Size = New System.Drawing.Size(411, 21)
    Me.Combo_RedemptionID.TabIndex = 4
    '
    'Label_RedemptionID
    '
    Me.Label_RedemptionID.Location = New System.Drawing.Point(8, 259)
    Me.Label_RedemptionID.Name = "Label_RedemptionID"
    Me.Label_RedemptionID.Size = New System.Drawing.Size(172, 16)
    Me.Label_RedemptionID.TabIndex = 139
    Me.Label_RedemptionID.Text = "Redeem vs Specific Subscription"
    '
    'RootMenu
    '
    Me.RootMenu.Location = New System.Drawing.Point(0, 0)
    Me.RootMenu.Name = "RootMenu"
    Me.RootMenu.Size = New System.Drawing.Size(607, 24)
    Me.RootMenu.TabIndex = 25
    Me.RootMenu.Text = " "
    '
    'Form_StatusStrip
    '
    Me.Form_StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Form_ProgressBar, Me.Label_Status, Me.Form_StatusLabel})
    Me.Form_StatusStrip.Location = New System.Drawing.Point(0, 774)
    Me.Form_StatusStrip.Name = "Form_StatusStrip"
    Me.Form_StatusStrip.Size = New System.Drawing.Size(607, 22)
    Me.Form_StatusStrip.TabIndex = 141
    Me.Form_StatusStrip.Text = " "
    '
    'Form_ProgressBar
    '
    Me.Form_ProgressBar.Maximum = 20
    Me.Form_ProgressBar.Name = "Form_ProgressBar"
    Me.Form_ProgressBar.Size = New System.Drawing.Size(150, 17)
    Me.Form_ProgressBar.Step = 1
    Me.Form_ProgressBar.Visible = False
    '
    'Label_Status
    '
    Me.Label_Status.Name = "Label_Status"
    Me.Label_Status.Size = New System.Drawing.Size(10, 17)
    Me.Label_Status.Text = " "
    '
    'Form_StatusLabel
    '
    Me.Form_StatusLabel.Name = "Form_StatusLabel"
    Me.Form_StatusLabel.Size = New System.Drawing.Size(121, 17)
    Me.Form_StatusLabel.Text = "ToolStripStatusLabel1"
    '
    'Check_DontUpdatePrice
    '
    Me.Check_DontUpdatePrice.BackColor = System.Drawing.SystemColors.Control
    Me.Check_DontUpdatePrice.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_DontUpdatePrice.Location = New System.Drawing.Point(201, 43)
    Me.Check_DontUpdatePrice.Name = "Check_DontUpdatePrice"
    Me.Check_DontUpdatePrice.Size = New System.Drawing.Size(251, 37)
    Me.Check_DontUpdatePrice.TabIndex = 21
    Me.Check_DontUpdatePrice.Text = "Exempt this Transaction from Price Auto-update process."
    Me.Check_DontUpdatePrice.UseVisualStyleBackColor = False
    Me.Check_DontUpdatePrice.Visible = False
    '
    'Check_CostsAsPercent
    '
    Me.Check_CostsAsPercent.BackColor = System.Drawing.SystemColors.Control
    Me.Check_CostsAsPercent.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Check_CostsAsPercent.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_CostsAsPercent.Location = New System.Drawing.Point(88, 174)
    Me.Check_CostsAsPercent.Name = "Check_CostsAsPercent"
    Me.Check_CostsAsPercent.Size = New System.Drawing.Size(28, 19)
    Me.Check_CostsAsPercent.TabIndex = 11
    Me.Check_CostsAsPercent.Text = "%"
    Me.Check_CostsAsPercent.UseVisualStyleBackColor = False
    '
    'Button_GetAmount
    '
    Me.Button_GetAmount.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Button_GetAmount.Location = New System.Drawing.Point(82, 32)
    Me.Button_GetAmount.Name = "Button_GetAmount"
    Me.Button_GetAmount.Size = New System.Drawing.Size(36, 20)
    Me.Button_GetAmount.TabIndex = 1
    Me.Button_GetAmount.TabStop = False
    Me.Button_GetAmount.Text = "Get"
    '
    'Check_BlotterRedeemWholeHolding
    '
    Me.Check_BlotterRedeemWholeHolding.BackColor = System.Drawing.SystemColors.Control
    Me.Check_BlotterRedeemWholeHolding.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_BlotterRedeemWholeHolding.Location = New System.Drawing.Point(43, 89)
    Me.Check_BlotterRedeemWholeHolding.Name = "Check_BlotterRedeemWholeHolding"
    Me.Check_BlotterRedeemWholeHolding.Size = New System.Drawing.Size(273, 16)
    Me.Check_BlotterRedeemWholeHolding.TabIndex = 14
    Me.Check_BlotterRedeemWholeHolding.Text = "'Redeem whole holding' on Trade blotter."
    Me.Check_BlotterRedeemWholeHolding.UseVisualStyleBackColor = False
    Me.Check_BlotterRedeemWholeHolding.Visible = False
    '
    'Tab_TransactionDetails
    '
    Me.Tab_TransactionDetails.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Tab_TransactionDetails.Controls.Add(Me.Tab_BasicDetails)
    Me.Tab_TransactionDetails.Controls.Add(Me.Tab_TransferDetails)
    Me.Tab_TransactionDetails.Controls.Add(Me.Tab_Legacy)
    Me.Tab_TransactionDetails.Location = New System.Drawing.Point(5, 354)
    Me.Tab_TransactionDetails.Name = "Tab_TransactionDetails"
    Me.Tab_TransactionDetails.SelectedIndex = 0
    Me.Tab_TransactionDetails.Size = New System.Drawing.Size(600, 251)
    Me.Tab_TransactionDetails.TabIndex = 17
    '
    'Tab_BasicDetails
    '
    Me.Tab_BasicDetails.BackColor = System.Drawing.SystemColors.Control
    Me.Tab_BasicDetails.Controls.Add(Me.Check_IsForwardPayment)
    Me.Tab_BasicDetails.Controls.Add(Me.Radio_BondByPrincipal)
    Me.Tab_BasicDetails.Controls.Add(Me.Radio_BondByPrice)
    Me.Tab_BasicDetails.Controls.Add(Me.Label_BrokerFees)
    Me.Tab_BasicDetails.Controls.Add(Me.edit_BrokerFees)
    Me.Tab_BasicDetails.Controls.Add(Me.Label_AccruedInterest)
    Me.Tab_BasicDetails.Controls.Add(Me.edit_AccruedInterest)
    Me.Tab_BasicDetails.Controls.Add(Me.Label_CleanPrice)
    Me.Tab_BasicDetails.Controls.Add(Me.edit_CleanPrice)
    Me.Tab_BasicDetails.Controls.Add(Me.Button_GetPercent)
    Me.Tab_BasicDetails.Controls.Add(Me.Label_InstrumentCurrency)
    Me.Tab_BasicDetails.Controls.Add(Me.Button_GetFX)
    Me.Tab_BasicDetails.Controls.Add(Me.Check_FXSettlement)
    Me.Tab_BasicDetails.Controls.Add(Me.Label_FXSettlementAmount)
    Me.Tab_BasicDetails.Controls.Add(Me.Label19)
    Me.Tab_BasicDetails.Controls.Add(Me.Label18)
    Me.Tab_BasicDetails.Controls.Add(Me.edit_FXRate)
    Me.Tab_BasicDetails.Controls.Add(Me.Combo_SettlementCurrency)
    Me.Tab_BasicDetails.Controls.Add(Me.Label12)
    Me.Tab_BasicDetails.Controls.Add(Me.Button_GetAmount)
    Me.Tab_BasicDetails.Controls.Add(Me.Combo_ValueOrAmount)
    Me.Tab_BasicDetails.Controls.Add(Me.Check_CostsAsPercent)
    Me.Tab_BasicDetails.Controls.Add(Me.Label_Amount)
    Me.Tab_BasicDetails.Controls.Add(Me.edit_Amount)
    Me.Tab_BasicDetails.Controls.Add(Me.Label_Price)
    Me.Tab_BasicDetails.Controls.Add(Me.edit_Price)
    Me.Tab_BasicDetails.Controls.Add(Me.Label_Costs)
    Me.Tab_BasicDetails.Controls.Add(Me.edit_CostPercentage)
    Me.Tab_BasicDetails.Controls.Add(Me.edit_Costs)
    Me.Tab_BasicDetails.Controls.Add(Me.Button_GetPrice)
    Me.Tab_BasicDetails.Controls.Add(Me.Label_Settlement)
    Me.Tab_BasicDetails.Controls.Add(Me.edit_settlement)
    Me.Tab_BasicDetails.Controls.Add(Me.Label_percent)
    Me.Tab_BasicDetails.Controls.Add(Me.Label_Principal)
    Me.Tab_BasicDetails.Controls.Add(Me.edit_Principal)
    Me.Tab_BasicDetails.Location = New System.Drawing.Point(4, 22)
    Me.Tab_BasicDetails.Name = "Tab_BasicDetails"
    Me.Tab_BasicDetails.Padding = New System.Windows.Forms.Padding(3)
    Me.Tab_BasicDetails.Size = New System.Drawing.Size(592, 225)
    Me.Tab_BasicDetails.TabIndex = 0
    Me.Tab_BasicDetails.Text = "Value and Status"
    '
    'Check_IsForwardPayment
    '
    Me.Check_IsForwardPayment.BackColor = System.Drawing.SystemColors.Control
    Me.Check_IsForwardPayment.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_IsForwardPayment.Location = New System.Drawing.Point(326, 117)
    Me.Check_IsForwardPayment.Name = "Check_IsForwardPayment"
    Me.Check_IsForwardPayment.Size = New System.Drawing.Size(249, 18)
    Me.Check_IsForwardPayment.TabIndex = 19
    Me.Check_IsForwardPayment.Text = "Payment vs Forward FX expiry."
    Me.Check_IsForwardPayment.UseVisualStyleBackColor = False
    Me.Check_IsForwardPayment.Visible = False
    '
    'Radio_BondByPrincipal
    '
    Me.Radio_BondByPrincipal.AutoSize = True
    Me.Radio_BondByPrincipal.Location = New System.Drawing.Point(179, 80)
    Me.Radio_BondByPrincipal.Name = "Radio_BondByPrincipal"
    Me.Radio_BondByPrincipal.Size = New System.Drawing.Size(65, 17)
    Me.Radio_BondByPrincipal.TabIndex = 6
    Me.Radio_BondByPrincipal.Text = "Principal"
    Me.Radio_BondByPrincipal.UseVisualStyleBackColor = True
    '
    'Radio_BondByPrice
    '
    Me.Radio_BondByPrice.AutoSize = True
    Me.Radio_BondByPrice.Checked = True
    Me.Radio_BondByPrice.Location = New System.Drawing.Point(122, 79)
    Me.Radio_BondByPrice.Name = "Radio_BondByPrice"
    Me.Radio_BondByPrice.Size = New System.Drawing.Size(49, 17)
    Me.Radio_BondByPrice.TabIndex = 5
    Me.Radio_BondByPrice.TabStop = True
    Me.Radio_BondByPrice.Text = "Price"
    Me.Radio_BondByPrice.UseVisualStyleBackColor = True
    '
    'Label_BrokerFees
    '
    Me.Label_BrokerFees.Location = New System.Drawing.Point(10, 153)
    Me.Label_BrokerFees.Name = "Label_BrokerFees"
    Me.Label_BrokerFees.Size = New System.Drawing.Size(106, 16)
    Me.Label_BrokerFees.TabIndex = 161
    Me.Label_BrokerFees.Text = "Broker Fees"
    '
    'edit_BrokerFees
    '
    Me.edit_BrokerFees.Location = New System.Drawing.Point(122, 149)
    Me.edit_BrokerFees.Name = "edit_BrokerFees"
    Me.edit_BrokerFees.RenaissanceTag = Nothing
    Me.edit_BrokerFees.SelectTextOnFocus = True
    Me.edit_BrokerFees.Size = New System.Drawing.Size(140, 20)
    Me.edit_BrokerFees.TabIndex = 10
    Me.edit_BrokerFees.Text = "0.0000"
    Me.edit_BrokerFees.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.edit_BrokerFees.TextFormat = "#,##0.0000"
    Me.edit_BrokerFees.Value = 0
    '
    'Label_AccruedInterest
    '
    Me.Label_AccruedInterest.Location = New System.Drawing.Point(10, 130)
    Me.Label_AccruedInterest.Name = "Label_AccruedInterest"
    Me.Label_AccruedInterest.Size = New System.Drawing.Size(108, 16)
    Me.Label_AccruedInterest.TabIndex = 159
    Me.Label_AccruedInterest.Text = "Accrued Interest"
    '
    'edit_AccruedInterest
    '
    Me.edit_AccruedInterest.Location = New System.Drawing.Point(122, 126)
    Me.edit_AccruedInterest.Name = "edit_AccruedInterest"
    Me.edit_AccruedInterest.RenaissanceTag = Nothing
    Me.edit_AccruedInterest.SelectTextOnFocus = True
    Me.edit_AccruedInterest.Size = New System.Drawing.Size(140, 20)
    Me.edit_AccruedInterest.TabIndex = 9
    Me.edit_AccruedInterest.Text = "0.0000"
    Me.edit_AccruedInterest.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.edit_AccruedInterest.TextFormat = "#,##0.0000"
    Me.edit_AccruedInterest.Value = 0
    '
    'Label_CleanPrice
    '
    Me.Label_CleanPrice.Location = New System.Drawing.Point(10, 106)
    Me.Label_CleanPrice.Name = "Label_CleanPrice"
    Me.Label_CleanPrice.Size = New System.Drawing.Size(106, 16)
    Me.Label_CleanPrice.TabIndex = 157
    Me.Label_CleanPrice.Text = "Clean Price"
    '
    'edit_CleanPrice
    '
    Me.edit_CleanPrice.Location = New System.Drawing.Point(122, 102)
    Me.edit_CleanPrice.Name = "edit_CleanPrice"
    Me.edit_CleanPrice.RenaissanceTag = Nothing
    Me.edit_CleanPrice.SelectTextOnFocus = True
    Me.edit_CleanPrice.Size = New System.Drawing.Size(140, 20)
    Me.edit_CleanPrice.TabIndex = 7
    Me.edit_CleanPrice.Text = "0.0000"
    Me.edit_CleanPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.edit_CleanPrice.TextFormat = "#,##0.0000"
    Me.edit_CleanPrice.Value = 0
    '
    'Button_GetPercent
    '
    Me.Button_GetPercent.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Button_GetPercent.Location = New System.Drawing.Point(263, 33)
    Me.Button_GetPercent.Name = "Button_GetPercent"
    Me.Button_GetPercent.Size = New System.Drawing.Size(17, 20)
    Me.Button_GetPercent.TabIndex = 154
    Me.Button_GetPercent.TabStop = False
    Me.Button_GetPercent.Text = "?"
    '
    'Label_InstrumentCurrency
    '
    Me.Label_InstrumentCurrency.Location = New System.Drawing.Point(265, 59)
    Me.Label_InstrumentCurrency.Name = "Label_InstrumentCurrency"
    Me.Label_InstrumentCurrency.Size = New System.Drawing.Size(38, 16)
    Me.Label_InstrumentCurrency.TabIndex = 152
    '
    'Button_GetFX
    '
    Me.Button_GetFX.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Button_GetFX.Location = New System.Drawing.Point(395, 56)
    Me.Button_GetFX.Name = "Button_GetFX"
    Me.Button_GetFX.Size = New System.Drawing.Size(36, 20)
    Me.Button_GetFX.TabIndex = 16
    Me.Button_GetFX.TabStop = False
    Me.Button_GetFX.Text = "Get"
    '
    'Check_FXSettlement
    '
    Me.Check_FXSettlement.BackColor = System.Drawing.SystemColors.Control
    Me.Check_FXSettlement.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Check_FXSettlement.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_FXSettlement.Location = New System.Drawing.Point(326, 33)
    Me.Check_FXSettlement.Name = "Check_FXSettlement"
    Me.Check_FXSettlement.Size = New System.Drawing.Size(103, 19)
    Me.Check_FXSettlement.TabIndex = 14
    Me.Check_FXSettlement.Text = "Settlement CCy"
    Me.Check_FXSettlement.UseVisualStyleBackColor = False
    '
    'Label_FXSettlementAmount
    '
    Me.Label_FXSettlementAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Label_FXSettlementAmount.Location = New System.Drawing.Point(435, 81)
    Me.Label_FXSettlementAmount.Name = "Label_FXSettlementAmount"
    Me.Label_FXSettlementAmount.Size = New System.Drawing.Size(140, 20)
    Me.Label_FXSettlementAmount.TabIndex = 18
    Me.Label_FXSettlementAmount.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'Label19
    '
    Me.Label19.Location = New System.Drawing.Point(323, 85)
    Me.Label19.Name = "Label19"
    Me.Label19.Size = New System.Drawing.Size(108, 16)
    Me.Label19.TabIndex = 148
    Me.Label19.Text = "Settlement"
    '
    'Label18
    '
    Me.Label18.Location = New System.Drawing.Point(323, 61)
    Me.Label18.Name = "Label18"
    Me.Label18.Size = New System.Drawing.Size(64, 16)
    Me.Label18.TabIndex = 147
    Me.Label18.Text = "FX Rate"
    '
    'edit_FXRate
    '
    Me.edit_FXRate.Location = New System.Drawing.Point(435, 57)
    Me.edit_FXRate.Name = "edit_FXRate"
    Me.edit_FXRate.RenaissanceTag = Nothing
    Me.edit_FXRate.SelectTextOnFocus = False
    Me.edit_FXRate.Size = New System.Drawing.Size(140, 20)
    Me.edit_FXRate.TabIndex = 17
    Me.edit_FXRate.Text = "0.0000"
    Me.edit_FXRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.edit_FXRate.TextFormat = "#,##0.0000"
    Me.edit_FXRate.Value = 0
    '
    'Combo_SettlementCurrency
    '
    Me.Combo_SettlementCurrency.Enabled = False
    Me.Combo_SettlementCurrency.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SettlementCurrency.Location = New System.Drawing.Point(435, 32)
    Me.Combo_SettlementCurrency.Name = "Combo_SettlementCurrency"
    Me.Combo_SettlementCurrency.Size = New System.Drawing.Size(140, 21)
    Me.Combo_SettlementCurrency.TabIndex = 15
    '
    'edit_Amount
    '
    Me.edit_Amount.Location = New System.Drawing.Point(122, 32)
    Me.edit_Amount.Name = "edit_Amount"
    Me.edit_Amount.RenaissanceTag = Nothing
    Me.edit_Amount.SelectTextOnFocus = True
    Me.edit_Amount.Size = New System.Drawing.Size(140, 20)
    Me.edit_Amount.TabIndex = 2
    Me.edit_Amount.Text = "0.00"
    Me.edit_Amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.edit_Amount.TextFormat = "#,##0.00"
    Me.edit_Amount.Value = 0
    '
    'edit_Price
    '
    Me.edit_Price.Location = New System.Drawing.Point(122, 56)
    Me.edit_Price.Name = "edit_Price"
    Me.edit_Price.RenaissanceTag = Nothing
    Me.edit_Price.SelectTextOnFocus = True
    Me.edit_Price.Size = New System.Drawing.Size(140, 20)
    Me.edit_Price.TabIndex = 4
    Me.edit_Price.Text = "0.0000"
    Me.edit_Price.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.edit_Price.TextFormat = "#,##0.0000"
    Me.edit_Price.Value = 0
    '
    'edit_CostPercentage
    '
    Me.edit_CostPercentage.Location = New System.Drawing.Point(122, 174)
    Me.edit_CostPercentage.Name = "edit_CostPercentage"
    Me.edit_CostPercentage.RenaissanceTag = Nothing
    Me.edit_CostPercentage.SelectTextOnFocus = True
    Me.edit_CostPercentage.Size = New System.Drawing.Size(140, 20)
    Me.edit_CostPercentage.TabIndex = 12
    Me.edit_CostPercentage.Text = "0.00%"
    Me.edit_CostPercentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.edit_CostPercentage.TextFormat = "#,##0.00%"
    Me.edit_CostPercentage.Value = 0
    Me.edit_CostPercentage.Visible = False
    '
    'edit_Costs
    '
    Me.edit_Costs.Location = New System.Drawing.Point(122, 174)
    Me.edit_Costs.Name = "edit_Costs"
    Me.edit_Costs.RenaissanceTag = Nothing
    Me.edit_Costs.SelectTextOnFocus = False
    Me.edit_Costs.Size = New System.Drawing.Size(140, 20)
    Me.edit_Costs.TabIndex = 7
    Me.edit_Costs.Text = "0.00"
    Me.edit_Costs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.edit_Costs.TextFormat = "#,##0.00"
    Me.edit_Costs.Value = 0
    '
    'edit_settlement
    '
    Me.edit_settlement.Location = New System.Drawing.Point(122, 198)
    Me.edit_settlement.Name = "edit_settlement"
    Me.edit_settlement.RenaissanceTag = Nothing
    Me.edit_settlement.SelectTextOnFocus = True
    Me.edit_settlement.Size = New System.Drawing.Size(140, 20)
    Me.edit_settlement.TabIndex = 13
    Me.edit_settlement.Text = "0.00"
    Me.edit_settlement.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.edit_settlement.TextFormat = "#,##0.00"
    Me.edit_settlement.Value = 0
    '
    'Label_percent
    '
    Me.Label_percent.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label_percent.Location = New System.Drawing.Point(271, 36)
    Me.Label_percent.Name = "Label_percent"
    Me.Label_percent.Size = New System.Drawing.Size(50, 17)
    Me.Label_percent.TabIndex = 153
    Me.Label_percent.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Label_Principal
    '
    Me.Label_Principal.Location = New System.Drawing.Point(10, 105)
    Me.Label_Principal.Name = "Label_Principal"
    Me.Label_Principal.Size = New System.Drawing.Size(108, 16)
    Me.Label_Principal.TabIndex = 163
    Me.Label_Principal.Text = "Principal"
    '
    'edit_Principal
    '
    Me.edit_Principal.Location = New System.Drawing.Point(122, 102)
    Me.edit_Principal.Name = "edit_Principal"
    Me.edit_Principal.RenaissanceTag = Nothing
    Me.edit_Principal.SelectTextOnFocus = True
    Me.edit_Principal.Size = New System.Drawing.Size(140, 20)
    Me.edit_Principal.TabIndex = 8
    Me.edit_Principal.Text = "0.0000"
    Me.edit_Principal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.edit_Principal.TextFormat = "#,##0.0000"
    Me.edit_Principal.Value = 0
    '
    'Tab_TransferDetails
    '
    Me.Tab_TransferDetails.BackColor = System.Drawing.SystemColors.Control
    Me.Tab_TransferDetails.Controls.Add(Me.Label16)
    Me.Tab_TransferDetails.Controls.Add(Me.Label13)
    Me.Tab_TransferDetails.Controls.Add(Me.Label14)
    Me.Tab_TransferDetails.Controls.Add(Me.Label15)
    Me.Tab_TransferDetails.Controls.Add(Me.Check_SpecificInitialEqualisation)
    Me.Tab_TransferDetails.Controls.Add(Me.Check_IsTransfer)
    Me.Tab_TransferDetails.Controls.Add(Me.Date_EffectiveValueDate)
    Me.Tab_TransferDetails.Controls.Add(Me.Label10)
    Me.Tab_TransferDetails.Controls.Add(Me.Label9)
    Me.Tab_TransferDetails.Controls.Add(Me.Label4)
    Me.Tab_TransferDetails.Controls.Add(Me.Label11)
    Me.Tab_TransferDetails.Controls.Add(Me.Edit_SpecificInitialEqualisation)
    Me.Tab_TransferDetails.Controls.Add(Me.Edit_EffectiveWatermark)
    Me.Tab_TransferDetails.Controls.Add(Me.Edit_EffectivePrice)
    Me.Tab_TransferDetails.Location = New System.Drawing.Point(4, 22)
    Me.Tab_TransferDetails.Name = "Tab_TransferDetails"
    Me.Tab_TransferDetails.Padding = New System.Windows.Forms.Padding(3)
    Me.Tab_TransferDetails.Size = New System.Drawing.Size(592, 225)
    Me.Tab_TransferDetails.TabIndex = 1
    Me.Tab_TransferDetails.Text = "Transfer Details"
    '
    'Label16
    '
    Me.Label16.Location = New System.Drawing.Point(347, 136)
    Me.Label16.Name = "Label16"
    Me.Label16.Size = New System.Drawing.Size(220, 16)
    Me.Label16.TabIndex = 13
    Me.Label16.Text = "Use with care."
    '
    'Label13
    '
    Me.Label13.Location = New System.Drawing.Point(347, 83)
    Me.Label13.Name = "Label13"
    Me.Label13.Size = New System.Drawing.Size(220, 39)
    Me.Label13.TabIndex = 12
    Me.Label13.Text = " Zero for normal Watermarks or Specify if different."
    '
    'Label14
    '
    Me.Label14.Location = New System.Drawing.Point(347, 57)
    Me.Label14.Name = "Label14"
    Me.Label14.Size = New System.Drawing.Size(220, 16)
    Me.Label14.TabIndex = 11
    Me.Label14.Text = "(Original Subscription Price)"
    '
    'Label15
    '
    Me.Label15.Location = New System.Drawing.Point(347, 32)
    Me.Label15.Name = "Label15"
    Me.Label15.Size = New System.Drawing.Size(220, 16)
    Me.Label15.TabIndex = 10
    Me.Label15.Text = "(Original Subscription Value Date)"
    '
    'Check_SpecificInitialEqualisation
    '
    Me.Check_SpecificInitialEqualisation.BackColor = System.Drawing.SystemColors.Control
    Me.Check_SpecificInitialEqualisation.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_SpecificInitialEqualisation.Location = New System.Drawing.Point(26, 111)
    Me.Check_SpecificInitialEqualisation.Name = "Check_SpecificInitialEqualisation"
    Me.Check_SpecificInitialEqualisation.Size = New System.Drawing.Size(532, 16)
    Me.Check_SpecificInitialEqualisation.TabIndex = 4
    Me.Check_SpecificInitialEqualisation.Text = "Use a specific Initial Equalisation value :"
    Me.Check_SpecificInitialEqualisation.UseVisualStyleBackColor = False
    '
    'Check_IsTransfer
    '
    Me.Check_IsTransfer.BackColor = System.Drawing.SystemColors.Control
    Me.Check_IsTransfer.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_IsTransfer.Location = New System.Drawing.Point(7, 6)
    Me.Check_IsTransfer.Name = "Check_IsTransfer"
    Me.Check_IsTransfer.Size = New System.Drawing.Size(273, 16)
    Me.Check_IsTransfer.TabIndex = 0
    Me.Check_IsTransfer.Text = "Transaction is a Transfer"
    Me.Check_IsTransfer.UseVisualStyleBackColor = False
    '
    'Date_EffectiveValueDate
    '
    Me.Date_EffectiveValueDate.Location = New System.Drawing.Point(170, 28)
    Me.Date_EffectiveValueDate.Name = "Date_EffectiveValueDate"
    Me.Date_EffectiveValueDate.Size = New System.Drawing.Size(174, 20)
    Me.Date_EffectiveValueDate.TabIndex = 1
    '
    'Label10
    '
    Me.Label10.Location = New System.Drawing.Point(23, 83)
    Me.Label10.Name = "Label10"
    Me.Label10.Size = New System.Drawing.Size(141, 16)
    Me.Label10.TabIndex = 8
    Me.Label10.Text = "Current Watermark"
    '
    'Label9
    '
    Me.Label9.Location = New System.Drawing.Point(23, 57)
    Me.Label9.Name = "Label9"
    Me.Label9.Size = New System.Drawing.Size(148, 16)
    Me.Label9.TabIndex = 7
    Me.Label9.Text = "Effective Price"
    '
    'Label4
    '
    Me.Label4.Location = New System.Drawing.Point(23, 32)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(148, 16)
    Me.Label4.TabIndex = 6
    Me.Label4.Text = "Effective Value Date"
    '
    'Label11
    '
    Me.Label11.Location = New System.Drawing.Point(23, 136)
    Me.Label11.Name = "Label11"
    Me.Label11.Size = New System.Drawing.Size(148, 16)
    Me.Label11.TabIndex = 9
    Me.Label11.Text = "Initial Equalisation Value"
    '
    'Edit_SpecificInitialEqualisation
    '
    Me.Edit_SpecificInitialEqualisation.Location = New System.Drawing.Point(170, 133)
    Me.Edit_SpecificInitialEqualisation.Name = "Edit_SpecificInitialEqualisation"
    Me.Edit_SpecificInitialEqualisation.RenaissanceTag = Nothing
    Me.Edit_SpecificInitialEqualisation.SelectTextOnFocus = False
    Me.Edit_SpecificInitialEqualisation.Size = New System.Drawing.Size(174, 20)
    Me.Edit_SpecificInitialEqualisation.TabIndex = 5
    Me.Edit_SpecificInitialEqualisation.Text = "0.00"
    Me.Edit_SpecificInitialEqualisation.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.Edit_SpecificInitialEqualisation.TextFormat = "#,##0.00"
    Me.Edit_SpecificInitialEqualisation.Value = 0
    '
    'Edit_EffectiveWatermark
    '
    Me.Edit_EffectiveWatermark.Location = New System.Drawing.Point(170, 80)
    Me.Edit_EffectiveWatermark.Name = "Edit_EffectiveWatermark"
    Me.Edit_EffectiveWatermark.RenaissanceTag = Nothing
    Me.Edit_EffectiveWatermark.SelectTextOnFocus = False
    Me.Edit_EffectiveWatermark.Size = New System.Drawing.Size(174, 20)
    Me.Edit_EffectiveWatermark.TabIndex = 3
    Me.Edit_EffectiveWatermark.Text = "0.00"
    Me.Edit_EffectiveWatermark.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.Edit_EffectiveWatermark.TextFormat = "#,##0.00"
    Me.Edit_EffectiveWatermark.Value = 0
    '
    'Edit_EffectivePrice
    '
    Me.Edit_EffectivePrice.Location = New System.Drawing.Point(170, 54)
    Me.Edit_EffectivePrice.Name = "Edit_EffectivePrice"
    Me.Edit_EffectivePrice.RenaissanceTag = Nothing
    Me.Edit_EffectivePrice.SelectTextOnFocus = False
    Me.Edit_EffectivePrice.Size = New System.Drawing.Size(174, 20)
    Me.Edit_EffectivePrice.TabIndex = 2
    Me.Edit_EffectivePrice.Text = "0.00"
    Me.Edit_EffectivePrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.Edit_EffectivePrice.TextFormat = "#,##0.00"
    Me.Edit_EffectivePrice.Value = 0
    '
    'Tab_Legacy
    '
    Me.Tab_Legacy.BackColor = System.Drawing.SystemColors.Control
    Me.Tab_Legacy.Controls.Add(Me.Check_BlotterRedeemWholeHolding)
    Me.Tab_Legacy.Controls.Add(Me.Check_InstructionGiven)
    Me.Tab_Legacy.Controls.Add(Me.Check_BankConfirmation)
    Me.Tab_Legacy.Controls.Add(Me.Check_InitialDataEntry)
    Me.Tab_Legacy.Controls.Add(Me.Check_PriceConfirmation)
    Me.Tab_Legacy.Controls.Add(Me.Check_SettlementConfirmed)
    Me.Tab_Legacy.Controls.Add(Me.Check_FinalDataEntry)
    Me.Tab_Legacy.Controls.Add(Me.Check_DontUpdatePrice)
    Me.Tab_Legacy.Controls.Add(Me.Combo_InvestmentManager)
    Me.Tab_Legacy.Controls.Add(Me.Combo_TransactionManager)
    Me.Tab_Legacy.Controls.Add(Me.Combo_DataEntryManager)
    Me.Tab_Legacy.Controls.Add(Me.Date_ConfirmationDate)
    Me.Tab_Legacy.Controls.Add(Me.Label_DecisionDate)
    Me.Tab_Legacy.Controls.Add(Me.Label_ConfirmationDate)
    Me.Tab_Legacy.Controls.Add(Me.Date_DecisionDate)
    Me.Tab_Legacy.Controls.Add(Me.Check_FinalAmount)
    Me.Tab_Legacy.Controls.Add(Me.Combo_TransactionGroup)
    Me.Tab_Legacy.Controls.Add(Me.Label5)
    Me.Tab_Legacy.Controls.Add(Me.Label17)
    Me.Tab_Legacy.Controls.Add(Me.Check_FinalSettlement)
    Me.Tab_Legacy.Controls.Add(Me.Check_FinalCosts)
    Me.Tab_Legacy.Controls.Add(Me.Check_FinalPrice)
    Me.Tab_Legacy.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Legacy.Name = "Tab_Legacy"
    Me.Tab_Legacy.Size = New System.Drawing.Size(592, 225)
    Me.Tab_Legacy.TabIndex = 2
    Me.Tab_Legacy.Text = "Legacy"
    '
    'Combo_Versusinstrument
    '
    Me.Combo_Versusinstrument.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Versusinstrument.Enabled = False
    Me.Combo_Versusinstrument.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Versusinstrument.Location = New System.Drawing.Point(192, 255)
    Me.Combo_Versusinstrument.Name = "Combo_Versusinstrument"
    Me.Combo_Versusinstrument.Size = New System.Drawing.Size(411, 21)
    Me.Combo_Versusinstrument.TabIndex = 13
    Me.Combo_Versusinstrument.Visible = False
    '
    'Combo_TradeStatus
    '
    Me.Combo_TradeStatus.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_TradeStatus.Enabled = False
    Me.Combo_TradeStatus.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_TradeStatus.Location = New System.Drawing.Point(412, 169)
    Me.Combo_TradeStatus.Name = "Combo_TradeStatus"
    Me.Combo_TradeStatus.Size = New System.Drawing.Size(191, 21)
    Me.Combo_TradeStatus.TabIndex = 11
    '
    'Label6
    '
    Me.Label6.Location = New System.Drawing.Point(300, 173)
    Me.Label6.Name = "Label6"
    Me.Label6.Size = New System.Drawing.Size(102, 16)
    Me.Label6.TabIndex = 143
    Me.Label6.Text = "Trade Status"
    '
    'Combo_VersusFuturesInstrument
    '
    Me.Combo_VersusFuturesInstrument.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_VersusFuturesInstrument.Enabled = False
    Me.Combo_VersusFuturesInstrument.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_VersusFuturesInstrument.Location = New System.Drawing.Point(192, 255)
    Me.Combo_VersusFuturesInstrument.Name = "Combo_VersusFuturesInstrument"
    Me.Combo_VersusFuturesInstrument.Size = New System.Drawing.Size(411, 21)
    Me.Combo_VersusFuturesInstrument.TabIndex = 12
    '
    'Combo_Workflow
    '
    Me.Combo_Workflow.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Workflow.Enabled = False
    Me.Combo_Workflow.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Workflow.Location = New System.Drawing.Point(412, 144)
    Me.Combo_Workflow.Name = "Combo_Workflow"
    Me.Combo_Workflow.Size = New System.Drawing.Size(191, 21)
    Me.Combo_Workflow.TabIndex = 10
    '
    'Label7
    '
    Me.Label7.Location = New System.Drawing.Point(300, 148)
    Me.Label7.Name = "Label7"
    Me.Label7.Size = New System.Drawing.Size(102, 16)
    Me.Label7.TabIndex = 205
    Me.Label7.Text = "Workflow"
    '
    'Check_ByISIN
    '
    Me.Check_ByISIN.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Check_ByISIN.BackColor = System.Drawing.SystemColors.Control
    Me.Check_ByISIN.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_ByISIN.Location = New System.Drawing.Point(517, 120)
    Me.Check_ByISIN.Name = "Check_ByISIN"
    Me.Check_ByISIN.Size = New System.Drawing.Size(43, 19)
    Me.Check_ByISIN.TabIndex = 5
    Me.Check_ByISIN.Text = "ISIN"
    Me.Check_ByISIN.UseVisualStyleBackColor = False
    '
    'Label_TransactionAddOrUpdate
    '
    Me.Label_TransactionAddOrUpdate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label_TransactionAddOrUpdate.Location = New System.Drawing.Point(151, 743)
    Me.Label_TransactionAddOrUpdate.Name = "Label_TransactionAddOrUpdate"
    Me.Label_TransactionAddOrUpdate.Size = New System.Drawing.Size(188, 20)
    Me.Label_TransactionAddOrUpdate.TabIndex = 206
    Me.Label_TransactionAddOrUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'Label_TransactionStatusLabel
    '
    Me.Label_TransactionStatusLabel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label_TransactionStatusLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label_TransactionStatusLabel.Location = New System.Drawing.Point(117, 32)
    Me.Label_TransactionStatusLabel.Name = "Label_TransactionStatusLabel"
    Me.Label_TransactionStatusLabel.Size = New System.Drawing.Size(412, 20)
    Me.Label_TransactionStatusLabel.TabIndex = 207
    Me.Label_TransactionStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'Combo_Broker
    '
    Me.Combo_Broker.Enabled = False
    Me.Combo_Broker.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Broker.Location = New System.Drawing.Point(120, 196)
    Me.Combo_Broker.Name = "Combo_Broker"
    Me.Combo_Broker.Size = New System.Drawing.Size(174, 21)
    Me.Combo_Broker.TabIndex = 8
    '
    'Label8
    '
    Me.Label8.Location = New System.Drawing.Point(8, 200)
    Me.Label8.Name = "Label8"
    Me.Label8.Size = New System.Drawing.Size(104, 16)
    Me.Label8.TabIndex = 209
    Me.Label8.Text = "Broker"
    '
    'Combo_BrokerOrderType
    '
    Me.Combo_BrokerOrderType.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_BrokerOrderType.Enabled = False
    Me.Combo_BrokerOrderType.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_BrokerOrderType.Items.AddRange(New Object() {"", "Market", "Limit", "Manual", "Manual (GTC)"})
    Me.Combo_BrokerOrderType.Location = New System.Drawing.Point(412, 196)
    Me.Combo_BrokerOrderType.Name = "Combo_BrokerOrderType"
    Me.Combo_BrokerOrderType.Size = New System.Drawing.Size(191, 21)
    Me.Combo_BrokerOrderType.TabIndex = 12
    '
    'Label20
    '
    Me.Label20.Location = New System.Drawing.Point(302, 199)
    Me.Label20.Name = "Label20"
    Me.Label20.Size = New System.Drawing.Size(104, 16)
    Me.Label20.TabIndex = 211
    Me.Label20.Text = "Broker order type"
    '
    'Combo_SubFund
    '
    Me.Combo_SubFund.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_SubFund.Enabled = False
    Me.Combo_SubFund.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SubFund.Location = New System.Drawing.Point(120, 92)
    Me.Combo_SubFund.Name = "Combo_SubFund"
    Me.Combo_SubFund.Size = New System.Drawing.Size(483, 21)
    Me.Combo_SubFund.TabIndex = 3
    '
    'Label21
    '
    Me.Label21.Location = New System.Drawing.Point(8, 96)
    Me.Label21.Name = "Label21"
    Me.Label21.Size = New System.Drawing.Size(106, 16)
    Me.Label21.TabIndex = 213
    Me.Label21.Text = "Strategy (Sub Fund)"
    '
    'Label_BrokerInfo
    '
    Me.Label_BrokerInfo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label_BrokerInfo.Location = New System.Drawing.Point(303, 225)
    Me.Label_BrokerInfo.Name = "Label_BrokerInfo"
    Me.Label_BrokerInfo.Size = New System.Drawing.Size(300, 16)
    Me.Label_BrokerInfo.TabIndex = 214
    Me.Label_BrokerInfo.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Combo_OpeningOrClosingTrade
    '
    Me.Combo_OpeningOrClosingTrade.Enabled = False
    Me.Combo_OpeningOrClosingTrade.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_OpeningOrClosingTrade.Items.AddRange(New Object() {"", "Opening", "Closing"})
    Me.Combo_OpeningOrClosingTrade.Location = New System.Drawing.Point(120, 222)
    Me.Combo_OpeningOrClosingTrade.Name = "Combo_OpeningOrClosingTrade"
    Me.Combo_OpeningOrClosingTrade.Size = New System.Drawing.Size(174, 21)
    Me.Combo_OpeningOrClosingTrade.TabIndex = 9
    '
    'Label_OpeningOrClosingTrade
    '
    Me.Label_OpeningOrClosingTrade.Location = New System.Drawing.Point(8, 225)
    Me.Label_OpeningOrClosingTrade.Name = "Label_OpeningOrClosingTrade"
    Me.Label_OpeningOrClosingTrade.Size = New System.Drawing.Size(104, 34)
    Me.Label_OpeningOrClosingTrade.TabIndex = 216
    Me.Label_OpeningOrClosingTrade.Text = "Opening / Closing trade"
    '
    'Combo_SpecificShareClass
    '
    Me.Combo_SpecificShareClass.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_SpecificShareClass.Enabled = False
    Me.Combo_SpecificShareClass.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SpecificShareClass.Location = New System.Drawing.Point(192, 282)
    Me.Combo_SpecificShareClass.Name = "Combo_SpecificShareClass"
    Me.Combo_SpecificShareClass.Size = New System.Drawing.Size(411, 21)
    Me.Combo_SpecificShareClass.TabIndex = 14
    Me.Combo_SpecificShareClass.Visible = False
    '
    'ComboBox2
    '
    Me.ComboBox2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.ComboBox2.Enabled = False
    Me.ComboBox2.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.ComboBox2.Location = New System.Drawing.Point(192, 282)
    Me.ComboBox2.Name = "ComboBox2"
    Me.ComboBox2.Size = New System.Drawing.Size(411, 21)
    Me.ComboBox2.TabIndex = 217
    '
    'Label_SpecificShareClass
    '
    Me.Label_SpecificShareClass.Location = New System.Drawing.Point(8, 286)
    Me.Label_SpecificShareClass.Name = "Label_SpecificShareClass"
    Me.Label_SpecificShareClass.Size = New System.Drawing.Size(228, 16)
    Me.Label_SpecificShareClass.TabIndex = 220
    Me.Label_SpecificShareClass.Text = "Transaction Specific to Share Class :"
    Me.Label_SpecificShareClass.Visible = False
    '
    'ComboBox3
    '
    Me.ComboBox3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.ComboBox3.Enabled = False
    Me.ComboBox3.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.ComboBox3.Location = New System.Drawing.Point(192, 282)
    Me.ComboBox3.Name = "ComboBox3"
    Me.ComboBox3.Size = New System.Drawing.Size(411, 21)
    Me.ComboBox3.TabIndex = 219
    '
    'Check_ShowExpired
    '
    Me.Check_ShowExpired.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Check_ShowExpired.BackColor = System.Drawing.SystemColors.Control
    Me.Check_ShowExpired.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_ShowExpired.Location = New System.Drawing.Point(565, 120)
    Me.Check_ShowExpired.Name = "Check_ShowExpired"
    Me.Check_ShowExpired.Size = New System.Drawing.Size(40, 19)
    Me.Check_ShowExpired.TabIndex = 164
    Me.Check_ShowExpired.Text = "Exp"
    Me.Check_ShowExpired.UseVisualStyleBackColor = False
    '
    'frmTransaction
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(607, 796)
    Me.Controls.Add(Me.Check_ShowExpired)
    Me.Controls.Add(Me.Combo_SpecificShareClass)
    Me.Controls.Add(Me.ComboBox2)
    Me.Controls.Add(Me.ComboBox3)
    Me.Controls.Add(Me.Combo_OpeningOrClosingTrade)
    Me.Controls.Add(Me.Label_BrokerInfo)
    Me.Controls.Add(Me.Combo_SubFund)
    Me.Controls.Add(Me.Label21)
    Me.Controls.Add(Me.Combo_BrokerOrderType)
    Me.Controls.Add(Me.Label20)
    Me.Controls.Add(Me.Combo_Broker)
    Me.Controls.Add(Me.Label8)
    Me.Controls.Add(Me.Label_TransactionStatusLabel)
    Me.Controls.Add(Me.Label_TransactionAddOrUpdate)
    Me.Controls.Add(Me.Check_ByISIN)
    Me.Controls.Add(Me.Combo_Workflow)
    Me.Controls.Add(Me.Label7)
    Me.Controls.Add(Me.Combo_TradeStatus)
    Me.Controls.Add(Me.Label6)
    Me.Controls.Add(Me.Combo_Versusinstrument)
    Me.Controls.Add(Me.Tab_TransactionDetails)
    Me.Controls.Add(Me.Form_StatusStrip)
    Me.Controls.Add(Me.RootMenu)
    Me.Controls.Add(Me.Combo_RedemptionID)
    Me.Controls.Add(Me.Label_CompoundTransaction)
    Me.Controls.Add(Me.GroupBox4)
    Me.Controls.Add(Me.Date_SettlementDate)
    Me.Controls.Add(Me.Label_SettlementDate)
    Me.Controls.Add(Me.Date_ValueDate)
    Me.Controls.Add(Me.Label_TradeDate)
    Me.Controls.Add(Me.GroupBox2)
    Me.Controls.Add(Me.Combo_TransactionCounterparty)
    Me.Controls.Add(Me.Label_Counterparty)
    Me.Controls.Add(Me.Combo_TransactionType)
    Me.Controls.Add(Me.Label3)
    Me.Controls.Add(Me.Combo_TransactionInstrument)
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.Combo_TransactionFund)
    Me.Controls.Add(Me.label_CptyIsFund)
    Me.Controls.Add(Me.btnClose)
    Me.Controls.Add(Me.GroupBox1)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.Panel1)
    Me.Controls.Add(Me.Combo_SelectTransaction)
    Me.Controls.Add(Me.btnSave)
    Me.Controls.Add(Me.editAuditID)
    Me.Controls.Add(Me.editTransactionComment)
    Me.Controls.Add(Me.lblTransaction)
    Me.Controls.Add(Me.btnDelete)
    Me.Controls.Add(Me.btnCancel)
    Me.Controls.Add(Me.Label_RedemptionID)
    Me.Controls.Add(Me.Combo_VersusFuturesInstrument)
    Me.Controls.Add(Me.Label_OpeningOrClosingTrade)
    Me.Controls.Add(Me.Label_SpecificShareClass)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frmTransaction"
    Me.Text = "Add/Edit Transaction"
    Me.Panel1.ResumeLayout(False)
    Me.Form_StatusStrip.ResumeLayout(False)
    Me.Form_StatusStrip.PerformLayout()
    Me.Tab_TransactionDetails.ResumeLayout(False)
    Me.Tab_BasicDetails.ResumeLayout(False)
    Me.Tab_BasicDetails.PerformLayout()
    Me.Tab_TransferDetails.ResumeLayout(False)
    Me.Tab_TransferDetails.PerformLayout()
    Me.Tab_Legacy.ResumeLayout(False)
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "


  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
  Private WithEvents _MainForm As VeniceMain

  ' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
  Private FormTooltip As New ToolTip()

  ' Form Menu


  ' Form Constants, specific to the table being updated.

    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
  Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

  ' Form Locals, initialised on 'New' defining what standard data items to use
    ''' <summary>
  ''' Variable holding the form's data table name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_TABLENAME As String
    ''' <summary>
  ''' Variable holding the form's data adaptor name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_ADAPTORNAME As String
    ''' <summary>
  ''' Variable holding the form's dataset name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_DATASETNAME As String


  ' The standard ChangeID for this form. e.g. tblTransaction
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

    ''' <summary>
  ''' Principal control used for selecting items on this form. 
    ''' </summary>
  Private THIS_FORM_SelectingCombo As ComboBox
    ''' <summary>
  ''' Control to select after proessing the "New" button.
    ''' </summary>
  Private THIS_FORM_NewMoveToControl As Control

  ' Form Specific Order fields
    ''' <summary>
  ''' Field Name to show in the Select combo.
    ''' </summary>
  Private THIS_FORM_SelectBy As String
    ''' <summary>
  ''' Field Name to order the Select combo by.
    ''' </summary>
  Private THIS_FORM_OrderBy As String

    ''' <summary>
  ''' The field name that holds the unique identifier for each entry. e.g. FundID for the tblFund table.
    ''' </summary>
  Private THIS_FORM_ValueMember As String

  ' Form Specific Select Criteria
    ''' <summary>
    ''' The THI s_ FOR m_ select criteria
    ''' </summary>
  Private THIS_FORM_SelectCriteria As String = "True"

  ' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
  Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
  Private THIS_FORM_FormID As VeniceFormID

  ' Data Structures

    ''' <summary>
    ''' My dataset
    ''' </summary>
  Private myDataset As RenaissanceDataClass.DSTransaction   ' Form Specific !!!!
    ''' <summary>
    ''' My table
    ''' </summary>
  Private myTable As RenaissanceDataClass.DSTransaction.tblTransactionDataTable
    ''' <summary>
    ''' My connection
    ''' </summary>
  Private myConnection As SqlConnection
    ''' <summary>
    ''' My adaptor
    ''' </summary>
  Private myAdaptor As SqlDataAdapter

    ''' <summary>
    ''' The this standard dataset
    ''' </summary>
  Private ThisStandardDataset As RenaissanceGlobals.StandardDataset


  ' Active Element.

    ''' <summary>
    ''' The sorted rows
    ''' </summary>
  Private SortedRows() As DataRow
    ''' <summary>
    ''' The select by sorted rows
    ''' </summary>
  Private SelectBySortedRows() As DataRow
    ''' <summary>
    ''' The this data row
    ''' </summary>
  Private thisDataRow As RenaissanceDataClass.DSTransaction.tblTransactionRow  ' Form Specific !!!!
    ''' <summary>
    ''' The this audit ID
    ''' </summary>
  Private thisAuditID As Integer
    ''' <summary>
    ''' The this position
    ''' </summary>
  Private thisPosition As Integer
    ''' <summary>
    ''' The previous position
    ''' </summary>
  Private previousPosition As Integer
    ''' <summary>
    ''' The __ is over cancel button
    ''' </summary>
  Private __IsOverCancelButton As Boolean
    ''' <summary>
    ''' The _ in use
    ''' </summary>
  Private _InUse As Boolean

  ' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
  Private FormIsValid As Boolean
    ''' <summary>
    ''' The form changed
    ''' </summary>
  Private FormChanged As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
  Private _FormOpenFailed As Boolean
    ''' <summary>
    ''' The in paint
    ''' </summary>
  Private InPaint As Boolean
    ''' <summary>
    ''' The add new record
    ''' </summary>
  Private AddNewRecord As Boolean

  ' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
  Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
  Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
  Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
  Private HasDeletePermission As Boolean

  '
    ''' <summary>
    ''' The futures style instrument types
    ''' </summary>
  Private FuturesStyleInstrumentTypes As String = ""

  ' 
    ''' <summary>
    ''' The custom_ workflow
    ''' </summary>
  Private Custom_Workflow As Boolean = False

    ''' <summary>
    ''' The BU y_ background
    ''' </summary>
  Private BUY_Background As System.Drawing.Color = System.Drawing.SystemColors.Window
    ''' <summary>
    ''' The BU y_ foreground
    ''' </summary>
  Private BUY_Foreground As System.Drawing.Color = Color.RoyalBlue
    ''' <summary>
    ''' The SEL l_ background
    ''' </summary>
  Private SELL_Background As System.Drawing.Color = System.Drawing.SystemColors.Window
    ''' <summary>
    ''' The SEL l_ foreground
    ''' </summary>
  Private SELL_Foreground As System.Drawing.Color = Color.Firebrick

    ''' <summary>
    ''' The NE w_ background
    ''' </summary>
  Private NEW_Background As System.Drawing.Color = System.Drawing.SystemColors.Control
    ''' <summary>
    ''' The AMEN d_ background
    ''' </summary>
  Private AMEND_Background As System.Drawing.Color = Color.Gold

  Private InstrumentsShowingExpired As Boolean = False

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
  Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
    ' Public property to return handle to the 'Main' Venice form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
  Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
    ' Public property maintaining a value indicating if the cursor is over the 'Cancel'
    ' Button on this form.
    ' This property is specifically designed for use by the field formating Event functions
    ' In order that they do not impose format restrictions if the user is about to click the 
    ' 'Cancel' button.
    '
    Get
      Return __IsOverCancelButton
    End Get
    Set(ByVal Value As Boolean)
      __IsOverCancelButton = Value
    End Set
  End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
  Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
    Get
      Return InPaint
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
    Get
      Return _InUse
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

    ''' <summary>
    ''' Gets or sets the form select criteria.
    ''' </summary>
    ''' <value>The form select criteria.</value>
  Public Property FormSelectCriteria() As String
    Get
      Return THIS_FORM_SelectCriteria
    End Get
    Set(ByVal Value As String)
      If (THIS_FORM_SelectCriteria <> Value) Then
        THIS_FORM_SelectCriteria = Value
        If (Value.Equals("True")) Then
          Me.Text = "Add/Edit Transaction"
        Else
          Me.Text = "Add/Edit Transaction (Selected View)"
        End If
        Call Form_Load(Me, New System.EventArgs)
      End If
    End Set
  End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmTransaction"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  Public Sub New(ByVal pMainForm As VeniceMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()

    _MainForm = pMainForm

    AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

    _FormOpenFailed = False
    _InUse = True

    ' ******************************************************
    ' Form Specific Settings :
    ' ******************************************************

    THIS_FORM_SelectingCombo = Me.Combo_SelectTransaction
    THIS_FORM_NewMoveToControl = Me.editTransactionComment

    ' Default Select and Order fields.

    THIS_FORM_SelectBy = "TransactionTicket"
    THIS_FORM_OrderBy = "TransactionValueDate"

    THIS_FORM_ValueMember = "TransactionParentID"

    ' Form Permissioning :-

    THIS_FORM_PermissionArea = Me.Name
    THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

    ' 'This' form ID

    THIS_FORM_FormID = VeniceFormID.frmTransaction

    ' This form's dataset type.

    ThisStandardDataset = RenaissanceStandardDatasets.tblTransaction ' This Defines the Form Data !!! 

    ' Format Event Handlers for form controls

    'AddHandler editTransactionComment.LostFocus, AddressOf MainForm.Text_NotNull

    ' Form Control Changed events
    AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler
    'AddHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
    'AddHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

    AddHandler Combo_TransactionFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_SubFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_TransactionInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_TransactionType.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_TradeStatus.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_TransactionCounterparty.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_Broker.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_OpeningOrClosingTrade.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_BrokerOrderType.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_TransactionGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
    AddHandler Combo_InvestmentManager.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_TransactionManager.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_DataEntryManager.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_RedemptionID.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_Versusinstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_SpecificShareClass.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_VersusFuturesInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_Workflow.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_SettlementCurrency.KeyUp, AddressOf MainForm.ComboSelectAsYouType

    AddHandler Combo_TransactionFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_SubFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_TransactionInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_TransactionType.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_TradeStatus.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_TransactionCounterparty.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_Broker.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_OpeningOrClosingTrade.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_BrokerOrderType.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_InvestmentManager.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_TransactionManager.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_DataEntryManager.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_RedemptionID.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_Versusinstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_SpecificShareClass.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_VersusFuturesInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_Workflow.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_SettlementCurrency.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

    AddHandler Combo_TransactionFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_SubFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_TransactionInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_TransactionType.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_TradeStatus.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_TransactionCounterparty.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_Broker.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_OpeningOrClosingTrade.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_BrokerOrderType.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_InvestmentManager.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_TransactionManager.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_DataEntryManager.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_RedemptionID.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_Versusinstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_SpecificShareClass.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_VersusFuturesInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_Workflow.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_SettlementCurrency.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

    AddHandler Combo_TransactionFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_SubFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_TransactionInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_TransactionType.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_TradeStatus.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_TransactionCounterparty.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_Broker.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_OpeningOrClosingTrade.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_BrokerOrderType.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_InvestmentManager.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_TransactionManager.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_DataEntryManager.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_RedemptionID.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_Versusinstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_SpecificShareClass.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_VersusFuturesInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_Workflow.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_SettlementCurrency.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

    AddHandler Combo_TransactionFund.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_SubFund.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_TransactionInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_TransactionType.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_TradeStatus.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_TransactionCounterparty.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Broker.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_OpeningOrClosingTrade.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_BrokerOrderType.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_TransactionGroup.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_TransactionGroup.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_InvestmentManager.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_TransactionManager.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_DataEntryManager.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_RedemptionID.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Versusinstrument.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_SpecificShareClass.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_VersusFuturesInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Workflow.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_SettlementCurrency.SelectedValueChanged, AddressOf Me.FormControlChanged

    AddHandler Date_DecisionDate.ValueChanged, AddressOf Me.FormControlChanged
    AddHandler Date_ValueDate.ValueChanged, AddressOf Me.FormControlChanged
    AddHandler Date_SettlementDate.ValueChanged, AddressOf Me.FormControlChanged
    AddHandler Date_ConfirmationDate.ValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_ValueOrAmount.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler edit_Amount.TextChanged, AddressOf Me.FormControlChanged
    AddHandler edit_Price.TextChanged, AddressOf Me.FormControlChanged
    AddHandler edit_CleanPrice.TextChanged, AddressOf Me.FormControlChanged
    AddHandler edit_Principal.TextChanged, AddressOf Me.FormControlChanged
    AddHandler edit_AccruedInterest.TextChanged, AddressOf Me.FormControlChanged
    AddHandler edit_BrokerFees.TextChanged, AddressOf Me.FormControlChanged
    AddHandler edit_Costs.TextChanged, AddressOf Me.FormControlChanged
    AddHandler edit_settlement.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Check_IsForwardPayment.CheckStateChanged, AddressOf Me.FormControlChanged
    AddHandler Check_FinalAmount.CheckStateChanged, AddressOf Me.FormControlChanged
    AddHandler Check_FinalPrice.CheckStateChanged, AddressOf Me.FormControlChanged
    AddHandler Check_FinalCosts.CheckStateChanged, AddressOf Me.FormControlChanged
    AddHandler Check_FinalSettlement.CheckStateChanged, AddressOf Me.FormControlChanged
    AddHandler Check_InstructionGiven.CheckStateChanged, AddressOf Me.FormControlChanged
    AddHandler Check_BlotterRedeemWholeHolding.CheckStateChanged, AddressOf Me.FormControlChanged
    AddHandler Check_BankConfirmation.CheckStateChanged, AddressOf Me.FormControlChanged
    AddHandler Check_InitialDataEntry.CheckStateChanged, AddressOf Me.FormControlChanged
    AddHandler Check_PriceConfirmation.CheckStateChanged, AddressOf Me.FormControlChanged
    AddHandler Check_SettlementConfirmed.CheckStateChanged, AddressOf Me.FormControlChanged
    AddHandler Check_FinalDataEntry.CheckStateChanged, AddressOf Me.FormControlChanged
    AddHandler Check_CostsAsPercent.CheckStateChanged, AddressOf Me.FormControlChanged
    AddHandler Check_DontUpdatePrice.CheckStateChanged, AddressOf Me.FormControlChanged

    AddHandler Check_IsTransfer.CheckStateChanged, AddressOf Me.FormControlChanged
    AddHandler Date_EffectiveValueDate.ValueChanged, AddressOf Me.FormControlChanged
    AddHandler Edit_EffectivePrice.ValueChanged, AddressOf Me.FormControlChanged
    AddHandler Edit_EffectiveWatermark.ValueChanged, AddressOf Me.FormControlChanged
    AddHandler Check_SpecificInitialEqualisation.CheckStateChanged, AddressOf Me.FormControlChanged
    AddHandler Edit_SpecificInitialEqualisation.ValueChanged, AddressOf Me.FormControlChanged
    AddHandler Check_FXSettlement.CheckStateChanged, AddressOf Me.FormControlChanged
    AddHandler edit_FXRate.ValueChanged, AddressOf Me.FormControlChanged

    AddHandler editTransactionComment.TextChanged, AddressOf Me.FormControlChanged

    ' Set up the ToolTip
    MainForm.SetFormToolTip(Me, FormTooltip)

    ' ******************************************************
    ' End Form Specific.
    ' ******************************************************

    ' Data object names standard to this Form type.

    THIS_TABLENAME = ThisStandardDataset.TableName
    THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
    THIS_DATASETNAME = ThisStandardDataset.DatasetName

    THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

    ' Establish / Retrieve data objects for this form.

    myConnection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)
    myAdaptor = MainForm.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, VENICE_CONNECTION, THIS_TABLENAME)
    myDataset = MainForm.Load_Table(ThisStandardDataset, False)
    myTable = myDataset.Tables(0)

    MainForm.BuildStandardFormMenu(Me.RootMenu, myDataset.Tables(0), AddressOf SelectMenuEvent, AddressOf OrderMenuEvent, AddressOf AuditReportMenuEvent)

    Call SetTransactionReportMenu(Me.RootMenu)

  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
  Public Sub ResetForm() Implements StandardVeniceForm.ResetForm
    ' ********************************************************************************
    '
    '
    ' ********************************************************************************

    THIS_FORM_SelectCriteria = "True"
    Me.Text = "Add/Edit Transaction"

    THIS_FORM_SelectBy = "TransactionTicket"
    THIS_FORM_OrderBy = "TransactionValueDate"

    Call Form_Load(Me, New System.EventArgs)
  End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
  Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
    ' ********************************************************************************
    '
    '
    ' ********************************************************************************

    ALWAYS_CLOSE_THIS_FORM = True
    Me.Close()
  End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    ' ********************************************************************************
    '
    '
    ' ********************************************************************************

    Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
    _FormOpenFailed = False
    _InUse = True

    Try

      ' Initialise Data structures. Connection, Adaptor and Dataset.

      If Not (MainForm Is Nothing) Then
        FormIsValid = True
      Else
        MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        FormIsValid = False
        _FormOpenFailed = True
        Exit Sub
      End If

      If (myConnection Is Nothing) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

        FormIsValid = False
        _FormOpenFailed = True
        Exit Sub
      End If

      If (myAdaptor Is Nothing) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

        FormIsValid = False
        _FormOpenFailed = True
        Exit Sub
      End If

      If (myDataset Is Nothing) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

        FormIsValid = False
        _FormOpenFailed = True
        Exit Sub
      End If

    Catch ex As Exception
    End Try

    Try

      ' Initialse form

      InPaint = True
      IsOverCancelButton = False

      ' Check User permissions
      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

        FormIsValid = False
        _FormOpenFailed = True
        Exit Sub
      End If

      Me.Check_ShowExpired.Checked = False
      InstrumentsShowingExpired = False

      Call SetFundCombo()
      Call SetInstrumentCombo(False)
      Call SetTransactionTypeCombo()
      Call SetTradeStatusCombo()
      Call SetRedemptionIDCombo()
      Call SetCounterpartyCombo()
      Call SetBrokerCombo()
      Call SetTransactionGroupCombo()
      Call SetPersonCombos()
      Call SetWorkflowCombo()
      Call SetCurrencyCombo()

      ' Display initial record.

      thisPosition = -1
      previousPosition = -1
      Me.Combo_TransactionType.BackColor = System.Drawing.SystemColors.Window
      'Combo_TransactionType.BackColor = Color.Aqua

      ' Build Sorted data list from which this form operates
      Call SetSortedRows()

      If myTable.Rows.Count > 0 Then
        'If (THIS_FORM_SelectingCombo.Items.Count > 0) Then THIS_FORM_SelectingCombo.SelectedIndex = 0
        If (thisPosition > 0) Then
          thisDataRow = SortedRows(thisPosition)
          Call GetFormData(thisDataRow)
        Else
          AddNewRecord = True
          thisDataRow = Nothing
          Call GetFormData(Nothing)
        End If
      Else
        'MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
        AddNewRecord = True
        Call GetFormData(Nothing)
      End If

    Catch ex As Exception

      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Initialising Form (Form.Load).", ex.StackTrace, True)

    Finally

      InPaint = False

    End Try

  End Sub

    ''' <summary>
    ''' Handles the Closing event of the frmTransaction control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
  Private Sub frmTransaction_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    ' ********************************************************************************
    '
    '
    ' ********************************************************************************

    Dim HideForm As Boolean

    ' Hide or Close this form ?
    ' All depends on how many of this form type are Open or in Cache...

    _InUse = False

    ' Clear Form

    Try
      Me.GetFormData(Nothing)

    Catch ex As Exception
    End Try

    Try
      If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
        HideForm = False
      Else
        If (FormChanged = True) Then
          Call SetFormData()
        End If

        HideForm = True
        If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
          HideForm = False
        End If
      End If

    Catch ex As Exception
    End Try

    If HideForm = True Then
      MainForm.HideInFormsCollection(Me)
      Me.Hide() ' NPP Fix

      e.Cancel = True
    Else
      Try

        MainForm.RemoveFromFormsCollection(Me)
        RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

        ' Form Control Changed events
        RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler
        'RemoveHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
        'RemoveHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        RemoveHandler Combo_TransactionFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_SubFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_TransactionInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_TransactionType.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_TradeStatus.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_TransactionCounterparty.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_Broker.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_OpeningOrClosingTrade.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_BrokerOrderType.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_TransactionGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
        RemoveHandler Combo_InvestmentManager.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_TransactionManager.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_DataEntryManager.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_RedemptionID.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_Versusinstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_SpecificShareClass.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_VersusFuturesInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_Workflow.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_SettlementCurrency.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        RemoveHandler Combo_TransactionFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_SubFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_TransactionInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_TransactionType.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_TradeStatus.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_TransactionCounterparty.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_Broker.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_OpeningOrClosingTrade.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_BrokerOrderType.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_InvestmentManager.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_TransactionManager.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_DataEntryManager.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_RedemptionID.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_Versusinstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_SpecificShareClass.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_VersusFuturesInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_Workflow.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_SettlementCurrency.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

        RemoveHandler Combo_TransactionFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_SubFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_TransactionInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_TransactionType.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_TradeStatus.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_TransactionCounterparty.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_Broker.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_OpeningOrClosingTrade.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_BrokerOrderType.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_InvestmentManager.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_TransactionManager.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_DataEntryManager.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_RedemptionID.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_Versusinstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_SpecificShareClass.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_VersusFuturesInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_Workflow.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_SettlementCurrency.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

        RemoveHandler Combo_TransactionFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_SubFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_TransactionInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_TransactionType.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_TradeStatus.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_TransactionCounterparty.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_Broker.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_OpeningOrClosingTrade.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_BrokerOrderType.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_InvestmentManager.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_TransactionManager.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_DataEntryManager.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_RedemptionID.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_Versusinstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_SpecificShareClass.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_VersusFuturesInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_Workflow.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_SettlementCurrency.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

        RemoveHandler Combo_TransactionFund.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_SubFund.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_TransactionInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_TransactionType.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_TradeStatus.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_TransactionCounterparty.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_Broker.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_OpeningOrClosingTrade.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_BrokerOrderType.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_TransactionGroup.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_TransactionGroup.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_InvestmentManager.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_TransactionManager.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_DataEntryManager.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_RedemptionID.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_Versusinstrument.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_SpecificShareClass.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_VersusFuturesInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_Workflow.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_SettlementCurrency.SelectedValueChanged, AddressOf Me.FormControlChanged

        RemoveHandler Date_DecisionDate.ValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Date_ValueDate.ValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Date_SettlementDate.ValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Date_ConfirmationDate.ValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_ValueOrAmount.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_Amount.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_Price.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_CleanPrice.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_Principal.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_AccruedInterest.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_BrokerFees.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_Costs.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_settlement.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_IsForwardPayment.CheckStateChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_FinalAmount.CheckStateChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_FinalPrice.CheckStateChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_FinalCosts.CheckStateChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_FinalSettlement.CheckStateChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_InstructionGiven.CheckStateChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_BlotterRedeemWholeHolding.CheckStateChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_BankConfirmation.CheckStateChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_InitialDataEntry.CheckStateChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_PriceConfirmation.CheckStateChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_SettlementConfirmed.CheckStateChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_FinalDataEntry.CheckStateChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_CostsAsPercent.CheckStateChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_DontUpdatePrice.CheckStateChanged, AddressOf Me.FormControlChanged

        RemoveHandler Check_IsTransfer.CheckStateChanged, AddressOf Me.FormControlChanged
        RemoveHandler Date_EffectiveValueDate.ValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Edit_EffectivePrice.ValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Edit_EffectiveWatermark.ValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_SpecificInitialEqualisation.CheckStateChanged, AddressOf Me.FormControlChanged
        RemoveHandler Edit_SpecificInitialEqualisation.ValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_FXSettlement.CheckStateChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_FXRate.ValueChanged, AddressOf Me.FormControlChanged

        RemoveHandler editTransactionComment.TextChanged, AddressOf Me.FormControlChanged

      Catch ex As Exception
      End Try
    End If

  End Sub


#End Region

  ' Routine to handle changes / updates to tables by this and other windows.
  ' If this, or any other, form posts a change to a table, then it will invoke an update event 
  ' detailing what tables have been altered.
  ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
  ' the 'VeniceAutoUpdate' event of the main Venice form.
  ' Each form may them react as appropriate to changes in any table that might impact it.
  '
    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    ' ***********************************************************************************************
    '
    '
    ' ***********************************************************************************************

    Dim OrgInPaint As Boolean
    Dim KnowledgeDateChanged As Boolean
    Dim SetButtonStatus_Flag As Boolean
    Dim RefreshForm As Boolean = False

    If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

    Try

      OrgInPaint = InPaint
      InPaint = True
      KnowledgeDateChanged = False
      SetButtonStatus_Flag = False

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
        KnowledgeDateChanged = True
      End If

      ' ****************************************************************
      ' Check for changes relevant to this form
      ' ****************************************************************

      ' Changes to the tblFund table :-
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFund) = True) Or KnowledgeDateChanged Then
        Try
          ' Re-Set combo.
          Call SetFundCombo()
        Catch ex As Exception
          MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblFund", ex.StackTrace, True)
        End Try
      End If

      ' Changes to the tblInstrument table :-
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrument) = True) Or KnowledgeDateChanged Then
        Try
          ' Re-Set combo.
          Call SetInstrumentCombo(Check_ShowExpired.Checked Or (Not AddNewRecord))
          Call SetSpecificShareClassCombo()

        Catch ex As Exception
          MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblInstrument", ex.StackTrace, True)
        End Try
      End If

      ' Changes to the tblSubFund table :-
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblSubFund) = True) Or KnowledgeDateChanged Then
        Try
          ' Re-Set combo.
          Call SetSubFundCombo()
        Catch ex As Exception
          MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblSubFund", ex.StackTrace, True)
        End Try
      End If

      ' Changes to the tblInstrumentType table :-
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrumentType) = True) Or KnowledgeDateChanged Then
        Try
          FuturesStyleInstrumentTypes = ""

          ' Re-Set combo.
          Call SetInstrumentCombo(Check_ShowExpired.Checked Or (Not AddNewRecord))
        Catch ex As Exception
          MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblInstrumentType", ex.StackTrace, True)
        End Try
      End If

      ' Changes to the tblTransactionType table :-
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblTransactionType) = True) Or KnowledgeDateChanged Then
        Try
          ' Re-Set combo.
          Call SetTransactionTypeCombo()
        Catch ex As Exception
          MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblTransactionType", ex.StackTrace, True)
        End Try

      End If

      ' Changes to the tblTradeStatus table :-
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblTradeStatus) = True) Or KnowledgeDateChanged Then
        Try
          ' Re-Set combo.
          Call SetTradeStatusCombo()
        Catch ex As Exception
          MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblTradeStatus", ex.StackTrace, True)
        End Try
      End If

      ' Changes to the tblWorkflows table :-
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblWorkflows) = True) Or KnowledgeDateChanged Then
        Try
          ' Re-Set combo.
          Call SetWorkflowCombo()
        Catch ex As Exception
          MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblWorkflows", ex.StackTrace, True)
        End Try
      End If

      ' Changes to the tblCurrency table :-
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblCurrency) = True) Or KnowledgeDateChanged Then
        Try
          ' Re-Set combo.
          Call SetCurrencyCombo()
        Catch ex As Exception
          MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblCurrency", ex.StackTrace, True)
        End Try
      End If

      ' Changes to the tblCounterparty table :-
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblCounterparty) = True) Or KnowledgeDateChanged Then
        Try
          ' Re-Set combo.
          Call SetCounterpartyCombo()
        Catch ex As Exception
          MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblCounterparty", ex.StackTrace, True)
        End Try
      End If

      ' Changes to the tblBroker table :-
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblBroker) = True) Or KnowledgeDateChanged Then
        Try
          ' Re-Set combo.
          Call SetBrokerCombo()
        Catch ex As Exception
          MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblBroker", ex.StackTrace, True)
        End Try
      End If

      ' Changes to the tblPerson table :-
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblPerson) = True) Or KnowledgeDateChanged Then
        Try
          ' Re-Set combo.
          Call SetPersonCombos()
        Catch ex As Exception
          MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblPerson", ex.StackTrace, True)
        End Try
      End If

      ' Changes to the tblTransaction table :-
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblTransaction) = True) Or KnowledgeDateChanged Then
        Try
          ' Re-Set combo.
          Call SetTransactionGroupCombo()
          Call SetRedemptionIDCombo()

          RefreshForm = True
        Catch ex As Exception
          MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblTransaction", ex.StackTrace, True)
        End Try
      End If

      ' Changes to the KnowledgeDate :-
      If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
        SetButtonStatus_Flag = True
        RefreshForm = True
      End If

      ' Changes to the tblUserPermissions table :-
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

        ' Check ongoing permissions.

        Call CheckPermissions()
        If (HasReadPermission = False) Then
          Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

          FormIsValid = False
          Me.Close()
          Exit Sub
        End If

        SetButtonStatus_Flag = True

      End If


      ' ****************************************************************
      ' Changes to the Main FORM table :-
      ' ****************************************************************

      If (e.TableChanged(THIS_FORM_ChangeID) = True) Or KnowledgeDateChanged Then

        ' Re-Set Controls etc.
        Call SetSortedRows()

        RefreshForm = True

        ' Move again to the correct item
        thisPosition = Get_Position(thisAuditID)

        ' Validate current position.
        If thisPosition >= 0 And thisPosition < Me.SortedRows.GetLength(0) Then
          thisDataRow = Me.SortedRows(thisPosition)
        Else
          If (Me.SortedRows.GetLength(0) > 0) And (AddNewRecord = False) Then
            thisPosition = 0
            thisDataRow = Me.SortedRows(thisPosition)
            FormChanged = False
          Else
            thisDataRow = Nothing
          End If
        End If

        ' Set SelectingCombo Index.
        'If (Me.THIS_FORM_SelectingCombo.Items.Count <= 0) Then
        '	Try
        '		Me.THIS_FORM_SelectingCombo.SelectedIndex = -1
        '	Catch ex As Exception
        '	End Try
        'ElseIf (thisPosition < 0) Then
        '	Try
        '		MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
        '	Catch ex As Exception
        '	End Try
        'Else
        '	Try
        '		Me.THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
        '	Catch ex As Exception
        '	End Try
        'End If

      End If

    Catch ex As Exception
      Call MainForm.LogError(Me.Name & ", AutoUpdate", LOG_LEVELS.Error, ex.Message, "Error in AutoUpdate", ex.StackTrace, True)
    End Try

    ' ****************************************************************
    ' Repaint if not currently in Edit Mode
    '
    ' ****************************************************************

    Try
      InPaint = OrgInPaint

      ' Retrieve form data if appropriate, i.e. this data item is not already being edited.
      If (RefreshForm = True) AndAlso (FormChanged = False) AndAlso (Me.AddNewRecord = False) Then
        GetFormData(thisDataRow) ' Includes a call to 'SetButtonStatus()'
      Else
        If SetButtonStatus_Flag Then
          Call SetButtonStatus(thisDataRow)
        End If
      End If

    Catch ex As Exception
      Call MainForm.LogError(Me.Name & ", AutoUpdate", LOG_LEVELS.Error, ex.Message, "Error in AutoUpdate", ex.StackTrace, True)
    End Try

  End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_TransactionInstrument control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_TransactionInstrument_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_TransactionInstrument.SelectedIndexChanged
    ' ***********************************************************************************************
    '
    '
    ' ***********************************************************************************************
    Try
      Dim InstrumentType As InstrumentTypes

      If (InPaint = False) Then

        Call Combo_TransactionType_Changed(sender, e) ' To ensure the 'versus Instrument' field is correctly set.

        If (Me.Combo_TransactionInstrument.SelectedIndex >= 0) AndAlso (IsNumeric(Combo_TransactionInstrument.SelectedValue)) AndAlso (CInt(Me.Combo_TransactionInstrument.SelectedValue) > 0) Then
          Dim thisInstrument As RenaissanceDataClass.DSInstrument.tblInstrumentRow

          thisInstrument = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, CInt(Me.Combo_TransactionInstrument.SelectedValue)), RenaissanceDataClass.DSInstrument.tblInstrumentRow)

          Radio_BondByPrice.Visible = False
          Radio_BondByPrincipal.Visible = False
          edit_CleanPrice.Visible = False
          edit_Principal.Visible = False
          Label_Principal.Visible = edit_Principal.Visible
          edit_AccruedInterest.Visible = False
          Combo_ValueOrAmount.Enabled = Combo_TransactionFund.Enabled

          If (thisInstrument IsNot Nothing) Then

            Label_InstrumentCurrency.Text = thisInstrument.InstrumentCurrency '  Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, CInt(Me.Combo_TransactionInstrument.SelectedValue), "InstrumentCurrency"), "")

            InstrumentType = CType(thisInstrument.InstrumentType, InstrumentTypes)

            Select Case InstrumentType

              Case InstrumentTypes.Bond, InstrumentTypes.ConvertibleBond

                Radio_BondByPrice.Visible = True
                Radio_BondByPrincipal.Visible = True
                edit_CleanPrice.Visible = True
                edit_Principal.Visible = True
                Label_Principal.Visible = edit_Principal.Visible
                edit_AccruedInterest.Visible = True

                Combo_ValueOrAmount.Enabled = False
                Combo_ValueOrAmount.SelectedIndex = 1 ' Amount

                Call Radio_BondByPrice_CheckedChanged(Radio_BondByPrice, Nothing)

            End Select

          End If

        Else
          Label_InstrumentCurrency.Text = ""
        End If

      End If

    Catch ex As Exception

    End Try

  End Sub

    ''' <summary>
    ''' Handles the Changed event of the Combo_TransactionType control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_TransactionType_Changed(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_TransactionType.SelectedIndexChanged
    ' ***********************************************************************************************
    '
    '
    ' ***********************************************************************************************
    Try
      If (InPaint = False) Then
        Call Me.SetFormLabels()
      End If

      Dim ThisTransactionType As RenaissanceGlobals.TransactionTypes = 0
      If (Combo_TransactionType.SelectedValue IsNot Nothing) AndAlso (IsNumeric(Combo_TransactionType.SelectedValue)) Then
        ThisTransactionType = CType(Me.Combo_TransactionType.SelectedValue, RenaissanceGlobals.TransactionTypes)
      End If

      If (Me.Combo_TransactionType.SelectedIndex >= 0) AndAlso (IsNumeric(Combo_TransactionType.SelectedValue)) Then
        Try

          If (ThisTransactionType = RenaissanceGlobals.TransactionTypes.Sell) Then
            Me.Check_BlotterRedeemWholeHolding.Enabled = Combo_TransactionType.Enabled
          Else
            Me.Check_BlotterRedeemWholeHolding.Enabled = False
            Me.Check_BlotterRedeemWholeHolding.Checked = False
          End If

          If (ThisTransactionType = TransactionTypes.BuyFX) OrElse (ThisTransactionType = TransactionTypes.SellFX) Then
            Me.Check_IsForwardPayment.Visible = True
          Else
            Me.Check_IsForwardPayment.Visible = False
          End If

          ' Transfer conditional formatting

          If (Check_IsTransfer.Enabled) AndAlso (Check_IsTransfer.Checked) Then

            If (ThisTransactionType = RenaissanceGlobals.TransactionTypes.Subscribe) Then

              Date_EffectiveValueDate.Enabled = True
              Edit_EffectivePrice.Enabled = True
              Edit_EffectiveWatermark.Enabled = True
              Check_SpecificInitialEqualisation.Enabled = True
              If (Check_SpecificInitialEqualisation.Checked) Then
                Edit_SpecificInitialEqualisation.Enabled = True
              Else
                Edit_SpecificInitialEqualisation.Enabled = False
              End If

            Else

              Date_EffectiveValueDate.Enabled = False
              Edit_EffectivePrice.Enabled = False
              Edit_EffectiveWatermark.Enabled = False
              Check_SpecificInitialEqualisation.Enabled = False
              Edit_SpecificInitialEqualisation.Enabled = False

            End If

          End If

        Catch ex As Exception
        End Try
      Else
        Me.Check_BlotterRedeemWholeHolding.Enabled = False
      End If

      ' Combo_SpecificShareClass.Visible = False

      Select Case ThisTransactionType

        Case TransactionTypes.Subscribe, TransactionTypes.Redeem
          Combo_SpecificShareClass.Visible = False
          Label_SpecificShareClass.Visible = False

        Case Else
          Combo_SpecificShareClass.Visible = True
          Label_SpecificShareClass.Visible = True

      End Select

 
      ' Set label / Visible for 'Redeem vs Transaction' or 'Transact vs Instrument' Combos.

      If (Combo_TransactionType.Enabled) Then

        Me.Combo_RedemptionID.Enabled = False
        Me.Combo_Versusinstrument.Enabled = False
        Me.Combo_VersusFuturesInstrument.Enabled = False

        Try
          Me.edit_Price.Enabled = Me.Combo_TransactionInstrument.Enabled

          If (Combo_TransactionType.SelectedValue IsNot Nothing) AndAlso (IsNumeric(Combo_TransactionType.SelectedValue)) Then

            Select Case ThisTransactionType

              Case RenaissanceGlobals.TransactionTypes.Redeem
                Me.Combo_RedemptionID.Enabled = True
                Combo_RedemptionID.Visible = True
                Combo_Versusinstrument.Visible = False
                Combo_VersusFuturesInstrument.Visible = False
                Me.Label_RedemptionID.Text = "Redeem vs Specific Subscription"

              Case RenaissanceGlobals.TransactionTypes.Expense_Increase, RenaissanceGlobals.TransactionTypes.Expense_Reduce

                Combo_Versusinstrument.Enabled = True
                Combo_Versusinstrument.Visible = True
                Combo_RedemptionID.Visible = False
                Combo_VersusFuturesInstrument.Visible = False
                Me.Label_RedemptionID.Text = "Payment vs Expense"

                If (thisPosition < 0) Then ' new Transaction
                  Me.edit_Price.Enabled = False
                  Me.edit_Price.Value = 1.0#
                  Call UpdateUnitsAndSettlement(sender, Nothing)
                End If

              Case RenaissanceGlobals.TransactionTypes.Fee

                Combo_Versusinstrument.Enabled = True
                Combo_Versusinstrument.Visible = True
                Combo_RedemptionID.Visible = False
                Combo_VersusFuturesInstrument.Visible = False
                Me.Label_RedemptionID.Text = "Payment vs Fee"

              Case RenaissanceGlobals.TransactionTypes.Provision_Increase

                Combo_Versusinstrument.Enabled = True
                Combo_Versusinstrument.Visible = True
                Combo_RedemptionID.Visible = False
                Combo_VersusFuturesInstrument.Visible = False
                Me.Label_RedemptionID.Text = "Payment vs Provision"

                If (thisPosition < 0) Then ' new Transaction
                  Me.edit_Price.Enabled = False
                  Me.edit_Price.Value = 0.0#
                  Call UpdateUnitsAndSettlement(sender, Nothing)
                End If

              Case RenaissanceGlobals.TransactionTypes.Provision_Reduce

                Combo_Versusinstrument.Enabled = True
                Combo_Versusinstrument.Visible = True
                Combo_RedemptionID.Visible = False
                Combo_VersusFuturesInstrument.Visible = False
                Me.Label_RedemptionID.Text = "Payment vs Provision"

                If (thisPosition < 0) Then ' new Transaction
                  Me.edit_Price.Enabled = True
                  Call Button_GetPrice_Click(Nothing, Nothing)
                  Call UpdateUnitsAndSettlement(sender, Nothing)
                End If


              Case RenaissanceGlobals.TransactionTypes.Buy, RenaissanceGlobals.TransactionTypes.Sell

                Dim InstrumentType As RenaissanceGlobals.InstrumentTypes

                If (Me.Combo_TransactionInstrument.SelectedIndex >= 0) AndAlso (CInt(Me.Combo_TransactionInstrument.SelectedValue) > 0) Then
                  InstrumentType = LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, CInt(Me.Combo_TransactionInstrument.SelectedValue), "InstrumentType")

                  If InstrumentType = InstrumentTypes.Notional Then

                    Combo_VersusFuturesInstrument.Enabled = True
                    Combo_VersusFuturesInstrument.Visible = True
                    Combo_RedemptionID.Visible = False
                    Combo_Versusinstrument.Visible = False
                    Me.Label_RedemptionID.Text = "Notional vs Instrument"

                  Else

                    Combo_Versusinstrument.Visible = True
                    Combo_RedemptionID.Visible = False
                    Combo_VersusFuturesInstrument.Visible = False
                    Me.Label_RedemptionID.Text = ""

                  End If
                End If

              Case Else

                Combo_Versusinstrument.Visible = True
                Combo_RedemptionID.Visible = False
                Combo_VersusFuturesInstrument.Visible = False
                Me.Label_RedemptionID.Text = ""

            End Select

          Else
            Combo_Versusinstrument.Visible = True
            Combo_RedemptionID.Visible = False
            Combo_VersusFuturesInstrument.Visible = False
            Me.Label_RedemptionID.Text = ""
          End If

        Catch Inner_Ex As Exception
        End Try
      End If

    Catch ex As Exception
    End Try

		Try
			' Set Enabled / Disabled for FX Trade or not.
			If Me.InPaint = False Then
				SetValueFieldEnabled()
			End If
		Catch ex As Exception
		End Try

  End Sub

    ''' <summary>
    ''' Updates the units and settlement.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub UpdateUnitsAndSettlement(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_TransactionType.SelectedIndexChanged, edit_Amount.TextChanged, edit_Price.TextChanged, edit_CleanPrice.TextChanged, edit_Principal.TextChanged, edit_AccruedInterest.TextChanged, edit_BrokerFees.TextChanged, edit_Costs.TextChanged, edit_settlement.TextChanged, edit_CostPercentage.TextChanged, edit_FXRate.TextChanged, Check_FXSettlement.CheckedChanged
    ' ***********************************************************************************************
    ' Update Units / Price / Settlement fields as any of them (or Cost) changes.
    '
    '
    ' ***********************************************************************************************

    'Dim debug As Integer

    'If (sender Is edit_Amount) Then
    '	debug = 1
    'End If

    'If (sender Is edit_Costs) Then
    '	debug = 2
    'End If

    'If (sender Is edit_Price) Then
    '	debug = 3
    'End If

    'If (sender Is edit_settlement) Then
    '	debug = 4
    'End If

    If (InPaint = False) AndAlso ((sender Is Nothing) OrElse (CType(sender, Control).Enabled = True)) Then
      Try
        ' Ignore changes in Price if trade is entered as units & principal
        If (edit_Principal.Visible) AndAlso ((sender Is edit_Price) OrElse (sender Is edit_CleanPrice)) Then
          Exit Sub
        End If

        InPaint = True

        Dim Units As Double = 0.0#
        Dim Principal As Double = 0.0#
        Dim Price As Double = 0.0#
        Dim CleanPrice As Double = 0.0#
        Dim AccruedInterest As Double = 0.0#
        Dim BrokerFees As Double = 0.0#
        Dim Costs As Double = 0.0#
        Dim CostsPercentage As Double = 0.0#
        Dim Settlement As Double = 0.0#
        Dim CashMovement As Double = 1.0#
        Dim ContractSize As Double
        Dim FXRate As Double = 1.0#
        Dim IsAmount As Boolean = True

        If (Me.Combo_ValueOrAmount.Text.ToUpper.StartsWith("V")) Then
          IsAmount = False
        End If

        Try
          If (Me.Combo_TransactionInstrument.SelectedIndex >= 0) AndAlso (IsNumeric(Combo_TransactionInstrument.SelectedValue)) Then
            ContractSize = GetInstrumentContractSize(Me.Name, MainForm, CInt(Combo_TransactionInstrument.SelectedValue))
          Else
            ContractSize = 1.0#
          End If
        Catch ex As Exception
          ContractSize = 1.0#
        End Try

        If (Me.edit_Amount.Focused = False) AndAlso (IsNumeric(Me.edit_Amount.Value)) Then
          Units = CDbl(Me.edit_Amount.Value)
        ElseIf IsNumeric(Me.edit_Amount.Text) Then
          Units = CDbl(Me.edit_Amount.Text)
        End If

        If (Me.edit_Price.Focused = False) AndAlso (IsNumeric(Me.edit_Price.Value)) Then
          Price = CDbl(Me.edit_Price.Value)
        ElseIf IsNumeric(Me.edit_Price.Text) Then
          Price = CDbl(Me.edit_Price.Text)
        End If

        If (edit_Principal.Visible) Then
          If (Me.edit_Principal.Focused = False) AndAlso (IsNumeric(Me.edit_Principal.Value)) Then
            Principal = CDbl(Me.edit_Principal.Value)
          ElseIf IsNumeric(Me.edit_Principal.Text) Then
            Principal = CDbl(Me.edit_Principal.Text)
          End If
        End If

        If (edit_CleanPrice.Visible) Then
          If (Me.edit_CleanPrice.Focused = False) AndAlso (IsNumeric(Me.edit_CleanPrice.Value)) Then
            CleanPrice = CDbl(Me.edit_CleanPrice.Value)
          ElseIf IsNumeric(Me.edit_CleanPrice.Text) Then
            CleanPrice = CDbl(Me.edit_CleanPrice.Text)
          End If
        End If

        If (edit_AccruedInterest.Visible) Then
          If (Me.edit_AccruedInterest.Focused = False) AndAlso (IsNumeric(Me.edit_AccruedInterest.Value)) Then
            AccruedInterest = CDbl(Me.edit_AccruedInterest.Value)
          ElseIf IsNumeric(Me.edit_AccruedInterest.Text) Then
            AccruedInterest = CDbl(Me.edit_AccruedInterest.Text)
          End If
        End If

        If (Me.edit_BrokerFees.Focused = False) AndAlso (IsNumeric(Me.edit_BrokerFees.Value)) Then
          BrokerFees = CDbl(Me.edit_BrokerFees.Value)
        ElseIf IsNumeric(Me.edit_BrokerFees.Text) Then
          BrokerFees = CDbl(Me.edit_BrokerFees.Text)
        End If

        If Me.Check_CostsAsPercent.Checked Then

          If (Me.edit_CostPercentage.Focused = False) AndAlso (IsNumeric(Me.edit_CostPercentage.Value)) Then
            CostsPercentage = CDbl(Me.edit_CostPercentage.Value)
          ElseIf IsNumeric(Me.edit_CostPercentage.Text) Then
            CostsPercentage = CDbl(Me.edit_CostPercentage.Text)
          End If

          Costs = (Units * ContractSize * Price) * CostsPercentage
          edit_Costs.Value = Costs

        Else

          If (Me.edit_Costs.Focused = False) AndAlso (IsNumeric(Me.edit_Costs.Value)) Then
            Costs = CDbl(Me.edit_Costs.Value)
          ElseIf IsNumeric(Me.edit_Costs.Text) Then
            Costs = CDbl(Me.edit_Costs.Text)
          End If

          If ((Units * ContractSize * Price) = 0) Then
            CostsPercentage = 0
          Else
            CostsPercentage = (Costs / (Units * Price))
          End If
          edit_CostPercentage.Value = CostsPercentage

        End If

        If (Check_FXSettlement.Checked) Then
          If (Me.edit_FXRate.Focused = False) AndAlso (IsNumeric(Me.edit_FXRate.Value)) Then
            FXRate = CDbl(Me.edit_FXRate.Value)
          ElseIf IsNumeric(Me.edit_FXRate.Text) Then
            FXRate = CDbl(Me.edit_FXRate.Text)
          End If

          If FXRate = 0.0# Then
            FXRate = 1.0#
          End If

        End If

        If (Me.edit_settlement.Focused = False) AndAlso (IsNumeric(Me.edit_settlement.Value)) Then
          Settlement = CDbl(Me.edit_settlement.Value)
        ElseIf IsNumeric(Me.edit_settlement.Text) Then
          Settlement = CDbl(Me.edit_settlement.Text)
        End If

        If (Me.Combo_TransactionType.SelectedIndex >= 0) Then
          CashMovement = GetCashMove(Me.Name, Me.MainForm, Me.Combo_TransactionType.SelectedValue)
        End If

        If (Units <> 0.0#) Then

          ' Enter as bond principal ?
          If (edit_Principal.Visible) AndAlso (Radio_BondByPrincipal.Checked) Then
            CleanPrice = Principal / (Units * ContractSize)
            Price = (Principal + AccruedInterest) / (Units * ContractSize)

            edit_CleanPrice.Value = CleanPrice
            edit_Price.Value = Price
          Else
            ' Not entered as bond principal
            If (edit_CleanPrice.Visible) AndAlso (Not edit_Price.Focused) AndAlso (Math.Abs(Price - (CleanPrice + (AccruedInterest / Units))) > 0.00000001) Then
              Price = CleanPrice + (AccruedInterest / (Units * ContractSize))
              edit_Price.Value = Price
            ElseIf (Not edit_CleanPrice.Focused) AndAlso (Math.Abs(CleanPrice - (Price - (AccruedInterest / Units))) > 0.0000001) Then
              edit_CleanPrice.Value = Price - (AccruedInterest / (Units * ContractSize))
            End If

            edit_Principal.Value = CleanPrice * (Units * ContractSize)
          End If
        End If

        'If (Me.Combo_ValueOrAmount.Text.ToUpper.StartsWith("V")) Then
        '  Me.edit_Amount.Value = GetTransactionUnitsOrSettlement("Value", Units, Price, ContractSize, BrokerFees, Costs, Settlement, CashMovement)
        'Else
        '  Me.edit_settlement.Value = GetTransactionUnitsOrSettlement("Amount", Units, Price, ContractSize, BrokerFees, Costs, Settlement, CashMovement)
        '  Settlement = Me.edit_settlement.Value
        'End If

        If (IsAmount) Then
          Me.edit_settlement.Value = GetTransactionUnitsOrSettlement("Amount", Units, Price, ContractSize, BrokerFees, Costs, Settlement, CashMovement)
          Settlement = Me.edit_settlement.Value
        Else
          Me.edit_Amount.Value = GetTransactionUnitsOrSettlement("Value", Units, Price, ContractSize, BrokerFees, Costs, Settlement, CashMovement)
        End If

        Label_FXSettlementAmount.Text = CDbl(Settlement * FXRate).ToString(RenaissanceGlobals.Globals.DISPLAYMEMBER_DOUBLEFORMAT)

      Catch ex As Exception
      Finally
        InPaint = False
      End Try
    End If

    ' Enable / Disable RedemptionID combo as appropriate to the transaction type
    Try
      Dim FormLabelTransactionType As RenaissanceGlobals.TransactionTypes = TransactionTypes.Consideration

      If (Combo_TransactionType.Enabled) AndAlso (Me.Combo_TransactionType.SelectedIndex >= 0) AndAlso (IsNumeric(Combo_TransactionType.SelectedValue)) Then
        FormLabelTransactionType = CType(Me.Combo_TransactionType.SelectedValue, RenaissanceGlobals.TransactionTypes)

        If FormLabelTransactionType = RenaissanceGlobals.TransactionTypes.Redeem Then
          Me.Combo_RedemptionID.Enabled = Me.Combo_TransactionType.Enabled ' Allow that the form may be read-only!
        Else
          Me.Combo_RedemptionID.Enabled = False
        End If

        If ((FormLabelTransactionType = RenaissanceGlobals.TransactionTypes.Subscribe) OrElse (FormLabelTransactionType = RenaissanceGlobals.TransactionTypes.Redeem)) Then
          Me.Check_IsTransfer.Enabled = True
        End If

      End If

    Catch ex As Exception
      Call MainForm.LogError(Me.Name & ", UpdateUnitsAndSettlement()", LOG_LEVELS.Error, ex.Message, "Error in UpdateUnitsAndSettlement()", ex.StackTrace, True)
    End Try

  End Sub


    ''' <summary>
    ''' Handles the PercentageValue event of the edit_Amount control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub edit_Amount_PercentageValue(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles edit_Amount.PercentageValue
    ' ****************************************************************
    '  Custom Event called at the end of 'OnLostFocus' if the updated value was a percentage
    ' ****************************************************************
    Dim InstrumentRow As RenaissanceDataClass.DSInstrument.tblInstrumentRow = Nothing
    Dim InstrumentID As Integer = 0
    Dim InstrumentFXID As Integer = 0
    Dim InstrumentMultiplier As Double = 1.0#
    Dim FundFXID As Integer = 0
    Dim FXID As Integer
    Dim PriceDate As Date

    Try
      Dim editControl As RenaissanceControls.NumericTextBox = CType(sender, RenaissanceControls.NumericTextBox)
      Dim percentageValue As Double = Math.Abs(editControl.Value)
      Dim InstFX As Double
      Dim FundNAV_USD As Double
      Dim FundNAV_InstrumentCcy As Double
      Dim InstrumentPrice As Double

      FXID = 0
      PriceDate = Now().Date

      If (IsNumeric(Combo_TransactionInstrument.SelectedValue) = False) OrElse (IsNumeric(Combo_TransactionFund.SelectedValue) = False) OrElse (CInt(Combo_TransactionFund.SelectedValue) <= 0) Then
        Exit Sub
      End If

      Try
        InstrumentRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, Nz(CInt(Combo_TransactionInstrument.SelectedValue), 0))
        InstrumentID = InstrumentRow.InstrumentID
        InstrumentFXID = InstrumentRow.InstrumentCurrencyID
        InstrumentMultiplier = InstrumentRow.InstrumentContractSize * InstrumentRow.InstrumentMultiplier
        FundFXID = CInt(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblFund, CInt(Nz(Combo_TransactionFund.SelectedValue, 0)), "FundBaseCurrency"))
      Catch ex As Exception
      End Try

			InstFX = MainForm.GetBestFX(InstrumentFXID, PriceDate)
      FundNAV_USD = Get_USDFundValue(MainForm, CInt(Nz(Combo_TransactionFund.SelectedValue, 0)), Now.Date, MainForm.Main_Knowledgedate, False, "", 0)
      FundNAV_InstrumentCcy = FundNAV_USD / InstFX
			InstrumentPrice = MainForm.GetBestNAV(InstrumentID, Now.Date)

      ' Units to buy

      edit_Amount.Value = Math.Round((FundNAV_InstrumentCcy * percentageValue) / (InstrumentPrice * InstrumentMultiplier), 0)

    Catch ex As Exception
      Call MainForm.LogError(Me.Name & ", edit_Amount_PercentageValue()", LOG_LEVELS.Error, ex.Message, "Error in edit_Amount_PercentageValue()", ex.StackTrace, True)
    End Try

  End Sub

    ''' <summary>
    ''' Handles the Click event of the Button_GetPrice control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Button_GetPrice_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_GetPrice.Click
    ' ****************************************************************
    ' Function to retrieve an Instrument Price appropriate to the Given 
    ' Instrument and Value Date.
    '
    ' ****************************************************************
    'Dim myConnection As SqlConnection
    'Dim QueryCommand As New SqlCommand

    Dim InstrumentID As Integer
    Dim PriceDate As Date
    'Dim InstrumentPrice As Object

    Try
      InstrumentID = 0
      PriceDate = Now().Date

      'myConnection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)

      If (Me.Combo_TransactionInstrument.SelectedIndex >= 0) Then
        InstrumentID = Me.Combo_TransactionInstrument.SelectedValue
      Else
        Exit Sub
      End If

      If IsDate(Me.Date_ValueDate.Value) Then
        PriceDate = Me.Date_ValueDate.Value
      End If

      'lotfi: get the date of the last price and alerts if it's older then 7 days for Funds ans than 3 days for other

      'edit_Price.Value = MainForm.GetBestNAV(InstrumentID, PriceDate)
      Dim lastPrice As Double
      Dim lastPriceDate As Date
      Dim instrumentType As String = "#NOPRICEFOUND#"


      If Not MainForm.GetBestNAVandDate(InstrumentID, PriceDate, lastPrice, lastPriceDate, instrumentType) Then
        MessageBox.Show("technical Error getting price !", "Instrument price", MessageBoxButtons.OK, MessageBoxIcon.Error)
        'Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error getting an Instrument Price.", ex.StackTrace, True)
        Exit Sub
      End If

      Dim lim As Integer = 3

      If instrumentType = "Fund" Then 'funds can be valutede weekly
        lim = 7
      End If

      If instrumentType = "#NOPRICEFOUND#" Then
        MessageBox.Show("No price is defined for this instrumerent !", "Instrument price alert", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1)
        Exit Sub
      End If

      'if a weekend exists between the two dates we ignore it
      If lastPriceDate < PriceDate And (Weekday(lastPriceDate) <= Weekday(PriceDate)) Then
        lim += 2
      End If

      Dim delay As Long = DateDiff(DateInterval.Day, lastPriceDate.Date, PriceDate.Date)

      If delay > lim Then
        If (MessageBox.Show("This instrument's price (" & lastPrice.ToString("#,##0.0000") & ") wasnt updated since " & lastPriceDate.ToString("dd-MMM-yyyy") & " ! " & vbCrLf & "           Do you accept it ?", "Instrument price update alert", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = vbYes) Then
          edit_Price.Value = lastPrice
        End If
      ElseIf lastPrice = 0 Then
        If (MessageBox.Show("This instrument's price is ZERO since " & lastPriceDate.ToString("dd-MMM-yyyy") & " ! " & vbCrLf & "           Do you accept it ?", "Instrument price update alert", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = vbYes) Then
          edit_Price.Value = lastPrice
        End If
      Else
        edit_Price.Value = MainForm.GetBestNAV(InstrumentID, PriceDate)
      End If

      'QueryCommand.Connection = myConnection
      'QueryCommand.CommandText = "SELECT dbo.fn_SingleInstrumentBestPrice(@InstrumentID, @ValueDate, 0, Null)"
      'QueryCommand.Parameters.Clear()
      'QueryCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@InstrumentID", System.Data.SqlDbType.Int, 4))
      'QueryCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValueDate", System.Data.SqlDbType.DateTime, 8))
      'QueryCommand.Parameters("@InstrumentID").Value = InstrumentID
      'QueryCommand.Parameters("@ValueDate").Value = PriceDate

      'SyncLock QueryCommand.Connection
      '  InstrumentPrice = QueryCommand.ExecuteScalar
      'End SyncLock

      'If IsNumeric(InstrumentPrice) Then
      '  Me.edit_Price.Value = CDbl(InstrumentPrice)
      'End If

    Catch ex As Exception
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error getting an Instrument Price.", ex.StackTrace, True)
    End Try

  End Sub

    ''' <summary>
    ''' Handles the Click event of the Button_GetFX control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Button_GetFX_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_GetFX.Click
    ' ****************************************************************
    ' Function to retrieve an Instrument FX appropriate to the Given 
    ' SettlementCurrency and Value Date.
    '
    ' ****************************************************************

    Dim InstrumentFXID As Integer = 0
    Dim FXID As Integer
    Dim PriceDate As Date

    Dim InstFX As Double
    Dim SettFX As Double

    'Dim InstrumentPrice As Object

    Try
      FXID = 0
      PriceDate = Now().Date

      If (IsNumeric(Combo_TransactionInstrument.SelectedValue) = False) Then
        Exit Sub
      End If

      Try
        InstrumentFXID = CInt(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, Nz(CInt(Combo_TransactionInstrument.SelectedValue), 0), "InstrumentCurrencyID"))
      Catch ex As Exception
      End Try

      If (Me.Combo_SettlementCurrency.SelectedIndex >= 0) Then
        FXID = Me.Combo_SettlementCurrency.SelectedValue
      Else
        Exit Sub
      End If

      If IsDate(Me.Date_ValueDate.Value) Then
        PriceDate = Me.Date_ValueDate.Value
      End If

			InstFX = MainForm.GetBestFX(InstrumentFXID, PriceDate)
			SettFX = MainForm.GetBestFX(FXID, PriceDate)

      If (SettFX = 0.0#) Then
        edit_FXRate.Value = 1.0#
      Else
        edit_FXRate.Value = InstFX / SettFX
      End If

    Catch ex As Exception
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error getting an Instrument Price.", ex.StackTrace, True)
    End Try

  End Sub

    ''' <summary>
    ''' Handles the Click event of the Button_GetAmount control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Button_GetAmount_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_GetAmount.Click
    ' ****************************************************************************
    '
    '
    ' ****************************************************************************

    Dim FundID As Integer
    Dim InstrumentID As Integer
    Dim InstrumentRow As RenaissanceDataClass.DSInstrument.tblInstrumentRow

    Try

      If (Me.Combo_ValueOrAmount.Text.ToUpper.StartsWith("V")) Then
        Exit Sub
      End If

      If (Me.Combo_TransactionFund.SelectedIndex >= 0) Then
        FundID = CInt(Combo_TransactionFund.SelectedValue)
      Else
        Exit Sub
      End If

      If (Me.Combo_TransactionInstrument.SelectedIndex >= 0) Then
        InstrumentID = CInt(Combo_TransactionInstrument.SelectedValue)
        InstrumentRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, InstrumentID)
      Else
        Exit Sub
      End If

      If (FundID <= 0) OrElse (InstrumentID <= 0) Then
        Exit Sub
      End If

      ' OK, Get Position.
      Dim SelectedRows As RenaissanceDataClass.DSTransaction.tblTransactionRow()
      Dim ContraDS As RenaissanceDataClass.DSTransactionContra
      Dim SelectedContraRows() As RenaissanceDataClass.DSTransactionContra.tblTransactionContraRow
      Dim TradeCount As Integer
      Dim TradeTotal As Double = 0
      Dim SelectString As String = "(TransactionFund=" & FundID.ToString & ") AND (TransactionInstrument=" & InstrumentID.ToString & ")"

      SyncLock myTable
        SelectedRows = Me.myTable.Select(SelectString)
        If (SelectedRows IsNot Nothing) AndAlso (SelectedRows.Length > 0) Then
          For TradeCount = 0 To (SelectedRows.Length - 1)
            TradeTotal += CDbl(SelectedRows(TradeCount).TransactionSignedUnits)
          Next
        End If
      End SyncLock

      ' For Cash type instruments, that can be the instrument on leg2, check the Contra transactions table also.

      Select Case CType(InstrumentRow.InstrumentType, InstrumentTypes)

        Case InstrumentTypes.Cash, InstrumentTypes.Notional

          ' Check Contra table also.

          ContraDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblTransactionContra)
          If (ContraDS IsNot Nothing) Then
            SyncLock ContraDS
              SelectedContraRows = ContraDS.tblTransactionContra.Select(SelectString)

              If (SelectedContraRows IsNot Nothing) AndAlso (SelectedContraRows.Length > 0) Then
                For TradeCount = 0 To (SelectedContraRows.Length - 1)
                  TradeTotal += CDbl(SelectedContraRows(TradeCount).TransactionSignedUnits)
                Next
              End If
            End SyncLock

          End If

      End Select

      Me.edit_Amount.Value = Math.Abs(TradeTotal)

    Catch ex As Exception

    End Try


  End Sub

    ''' <summary>
    ''' Handles the Click event of the Button_GetPercent control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Button_GetPercent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_GetPercent.Click
    ' ****************************************************************
    '  Custom Event called at the end of 'OnLostFocus' if the updated value was not a percentage
    ' ****************************************************************
    Dim InstrumentRow As RenaissanceDataClass.DSInstrument.tblInstrumentRow = Nothing
    Dim InstrumentID As Integer = 0
    Dim InstrumentFXID As Integer = 0
    Dim FundFXID As Integer = 0
    Dim Multiplier As Double = 1.0#

    Dim FXID As Integer
    Dim PriceDate As Date
    Label_percent.Text = ""

    Try
      Dim editControl As RenaissanceControls.NumericTextBox = Me.edit_Amount
      Dim percentageValue As Double = Math.Abs(editControl.Value)
      Dim InstFX As Double
      Dim FundNAV_USD As Double
      Dim FundNAV_InstrumentCcy As Double
      Dim InstrumentPrice As Double

      FXID = 0
      PriceDate = Now().Date

      If (IsNumeric(Combo_TransactionInstrument.SelectedValue) = False) OrElse (IsNumeric(Combo_TransactionFund.SelectedValue) = False) OrElse (CInt(Combo_TransactionFund.SelectedValue) <= 0) Then
        Exit Sub
      End If

      Try
        InstrumentRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, Nz(CInt(Combo_TransactionInstrument.SelectedValue), 0))
        InstrumentID = InstrumentRow.InstrumentID
        InstrumentFXID = InstrumentRow.InstrumentCurrencyID
        FundFXID = CInt(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblFund, CInt(Nz(Combo_TransactionFund.SelectedValue, 0)), "FundBaseCurrency"))
        Multiplier = InstrumentRow.InstrumentContractSize * InstrumentRow.InstrumentMultiplier
      Catch ex As Exception
      End Try

			InstFX = MainForm.GetBestFX(InstrumentFXID, PriceDate)
      FundNAV_USD = Get_USDFundValue(MainForm, CInt(Nz(Combo_TransactionFund.SelectedValue, 0)), Now.Date, MainForm.Main_Knowledgedate, False, "", 0)
      FundNAV_InstrumentCcy = FundNAV_USD / InstFX
			InstrumentPrice = MainForm.GetBestNAV(InstrumentID, Now.Date)

      If (InstrumentPrice = 0.0#) Then
        InstrumentPrice = edit_Price.Value
      End If

      ' Fund percentage

      percentageValue = (edit_Amount.Value * InstrumentPrice * Multiplier) / FundNAV_InstrumentCcy

      Label_percent.Text = percentageValue.ToString("#,##0.00%")

    Catch ex As Exception
      Call MainForm.LogError(Me.Name & ", edit_Amount_NotPercentageValue()", LOG_LEVELS.Error, ex.Message, "Error in edit_Amount_NotPercentageValue()", ex.StackTrace, True)
    End Try

  End Sub



    ''' <summary>
    ''' Gets the price exist.
    ''' </summary>
    ''' <param name="pInstrumentID">The p instrument ID.</param>
    ''' <param name="pValueDate">The p value date.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function GetPriceExist(ByVal pInstrumentID As Integer, ByVal pValueDate As Date) As Boolean
    ' ****************************************************************************
    '
    '
    ' ****************************************************************************

    Dim myConnection As SqlConnection = Nothing
    Dim QueryCommand As New SqlCommand
    Dim InstrumentCount As Object

    Try
      myConnection = MainForm.GetVeniceConnection()

      QueryCommand.Connection = myConnection
      'QueryCommand.CommandText = "SELECT COUNT(RN) FROM fn_tblPrice_SelectKD(Null) WHERE (PriceInstrument=@InstrumentID) AND (PriceDate <= @ValueDate)"
      'QueryCommand.CommandText = "SELECT COUNT(RN) FROM dbo.fn_tblPrice_SelectByInstrument(@InstrumentID, @KnowledgeDate) WHERE (PriceDate <= @ValueDate)"
      QueryCommand.CommandText = "SELECT TOP 1 RN FROM tblPrice WHERE (PriceDate <= @ValueDate) AND (PriceDate <= @ValueDate)"
      QueryCommand.Parameters.Clear()
      QueryCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@InstrumentID", System.Data.SqlDbType.Int, 4))
      QueryCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime, 8))
      QueryCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ValueDate", System.Data.SqlDbType.DateTime, 8))
      QueryCommand.Parameters("@InstrumentID").Value = pInstrumentID
      QueryCommand.Parameters("@KnowledgeDate").Value = KNOWLEDGEDATE_NOW
      QueryCommand.Parameters("@ValueDate").Value = pValueDate

      SyncLock QueryCommand.Connection
        InstrumentCount = QueryCommand.ExecuteScalar
      End SyncLock

      If IsNumeric(InstrumentCount) Then
        If CInt(InstrumentCount) > 0 Then
          Return True
        End If
      End If
    Catch ex As Exception
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error getting an Instrument Price.", ex.StackTrace, True)
    Finally
      ' Close connection...

      If (Not (myConnection Is Nothing)) Then
        Try
          myConnection.Close()
        Catch ex As Exception
        End Try
      End If

    End Try

    Return False
  End Function

    ''' <summary>
    ''' Handles the Click event of the Button_ViewCT control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Button_ViewCT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_ViewCT.Click
    ' ****************************************************************
    ' Identify and display the Compound Transaction Record associated
    ' with the currently displayed Transaction.
    ' ****************************************************************

    Dim CTTable As RenaissanceDataClass.DSCompoundTransaction.tblCompoundTransactionDataTable
    Dim CTRows() As RenaissanceDataClass.DSCompoundTransaction.tblCompoundTransactionRow
    Dim TargetAuditID As Integer
    Dim thisFormHandle As RenaissanceGlobals.RenaissanceFormHandle

    ' Exit if this roes not appear to be linked to a CompoundTransaction

    If (thisDataRow.TransactionCompoundRN <= 0) Then Exit Sub

    ' Firstly Resolve the AuditID of the CompoundTransaction To move to.
    ' 
    Try
      CTTable = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblCompoundTransaction, False), RenaissanceDataClass.DSCompoundTransaction).tblCompoundTransaction
      CTRows = CTTable.Select("RN=" & thisDataRow.TransactionCompoundRN.ToString)

      If (CTRows.Length <= 0) Then Exit Sub
      TargetAuditID = CTRows(0)("AuditID")

      thisFormHandle = MainForm.New_VeniceForm(VeniceFormID.frmCompoundTransaction)
      CType(thisFormHandle.Form, frmCompoundTransaction).MoveToAuditID(TargetAuditID)

    Catch ex As Exception
      Call MainForm.LogError(Me.Name & ", ViewCT_Click", LOG_LEVELS.Error, ex.Message, "Error Showing CompoundTransaction Form", ex.StackTrace, True)
    End Try

  End Sub

    ''' <summary>
    ''' Handles the DropDown event of the Combo_RedemptionID control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_RedemptionID_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Combo_RedemptionID.DropDown
    ' ****************************************************************
    '
    ' ****************************************************************

    Call SetRedemptionIDCombo()
  End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the Check_CostsAsPercent control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Check_CostsAsPercent_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_CostsAsPercent.CheckedChanged
    ' ****************************************************************
    '
    ' ****************************************************************

    If Me.Check_CostsAsPercent.Checked Then
      Me.edit_Costs.Visible = False
      Me.edit_CostPercentage.Visible = True
    Else
      Me.edit_Costs.Visible = True
      Me.edit_CostPercentage.Visible = False
    End If
  End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the Check_IsTransfer control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Check_IsTransfer_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_IsTransfer.CheckedChanged
    ' **********************************************************************
    '
    '
    ' **********************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) AndAlso (Not Me.InPaint) Then
        If (Check_IsTransfer.Enabled) Then
          If (Check_IsTransfer.Checked) Then
            Dim FormLabelTransactionType As RenaissanceGlobals.TransactionTypes = TransactionTypes.Consideration

            If (Me.Combo_TransactionType.SelectedIndex >= 0) AndAlso (IsNumeric(Combo_TransactionType.SelectedValue)) Then
              FormLabelTransactionType = CType(Me.Combo_TransactionType.SelectedValue, RenaissanceGlobals.TransactionTypes)
            End If

            If (FormLabelTransactionType = RenaissanceGlobals.TransactionTypes.Subscribe) Then

              Date_EffectiveValueDate.Enabled = True
              Edit_EffectivePrice.Enabled = True
              Edit_EffectiveWatermark.Enabled = True
              Check_SpecificInitialEqualisation.Enabled = True
              If (Check_SpecificInitialEqualisation.Checked) Then
                Edit_SpecificInitialEqualisation.Enabled = True
              Else
                Edit_SpecificInitialEqualisation.Enabled = False
              End If

            Else

              Date_EffectiveValueDate.Enabled = False
              Edit_EffectivePrice.Enabled = False
              Edit_EffectiveWatermark.Enabled = False
              Check_SpecificInitialEqualisation.Enabled = False
              Edit_SpecificInitialEqualisation.Enabled = False

            End If

          Else
            Date_EffectiveValueDate.Enabled = False
            Edit_EffectivePrice.Enabled = False
            Edit_EffectiveWatermark.Enabled = False
            Check_SpecificInitialEqualisation.Enabled = False
            Edit_SpecificInitialEqualisation.Enabled = False
          End If
        End If
      End If

    Catch ex As Exception
    End Try

  End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the Check_SpecificInitialEqualisation control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Check_SpecificInitialEqualisation_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_SpecificInitialEqualisation.CheckedChanged

    ' **********************************************************************
    '
    '
    ' **********************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) AndAlso (Not Me.InPaint) Then
        If (Check_SpecificInitialEqualisation.Enabled) Then
          If (Check_SpecificInitialEqualisation.Checked) Then
            Edit_SpecificInitialEqualisation.Enabled = True
          Else
            Edit_SpecificInitialEqualisation.Enabled = False
          End If
        End If
      End If

    Catch ex As Exception
    End Try

  End Sub

    ''' <summary>
    ''' Handles the CheckStateChanged event of the Check_FXSettlement control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Check_FXSettlement_CheckStateChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_FXSettlement.CheckStateChanged
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Try
      Combo_SettlementCurrency.Visible = Check_FXSettlement.Checked
      edit_FXRate.Visible = Check_FXSettlement.Checked
      Button_GetFX.Visible = Check_FXSettlement.Checked

    Catch ex As Exception
      Call MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Check_FXSettlement_CheckStateChanged()", ex.StackTrace, True)

    End Try
  End Sub

  Private Sub Check_ShowExpired_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_ShowExpired.CheckedChanged
    ' *******************************************************************************
    '
    ' *******************************************************************************

    If (InPaint = False) Then
      Try
        If (Check_ShowExpired.Checked) Then
          If (InstrumentsShowingExpired = False) Then
            Call SetInstrumentCombo(True)
          End If
        Else
          If (InstrumentsShowingExpired) Then
            Call SetInstrumentCombo(False)
          End If
        End If
      Catch ex As Exception
      End Try
    End If

  End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the Check_ByISIN control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Check_ByISIN_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_ByISIN.CheckedChanged
    ' *******************************************************************************
    '
    ' *******************************************************************************

    If (InPaint = False) Then
      Try
        Dim ExistingID As Integer = (-1)

        If (Me.Combo_TransactionInstrument.SelectedIndex >= 0) AndAlso (CInt(Me.Combo_TransactionInstrument.SelectedValue) > 0) Then
          ExistingID = CInt(Me.Combo_TransactionInstrument.SelectedValue)
        ElseIf (thisDataRow IsNot Nothing AndAlso IsNumeric(thisDataRow.TransactionInstrument)) Then
          ExistingID = thisDataRow.TransactionInstrument
        End If

        InPaint = True

        Call SetInstrumentCombo(Check_ShowExpired.Checked Or (Not AddNewRecord))

        If Combo_TransactionInstrument.Items.Count > 0 Then
          If (ExistingID > 0) Then
            Combo_TransactionInstrument.SelectedValue = ExistingID
          Else
            Combo_TransactionInstrument.SelectedIndex = 0
          End If
        End If

      Catch ex As Exception
        Call MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Check_ByISIN_CheckedChanged()", ex.StackTrace, True)
      Finally
        InPaint = False
      End Try
    End If

  End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

  ' Build Sorted list from the Source dataset and update the Select Combo
    ''' <summary>
    ''' Sets the sorted rows.
    ''' </summary>
  Private Sub SetSortedRows()

    Dim OrgInPaint As Boolean

    ' Code is pretty Generic form here on...
    ' Set paint local so that changes to the selection combo do not trigger form updates.

    OrgInPaint = InPaint
    InPaint = True

    Try

      ' Get selected Row sets, or exit if no data is present.
      If myDataset Is Nothing Then
        ReDim SortedRows(0)
        ReDim SelectBySortedRows(0)
      Else
        Try
          SortedRows = myTable.Select(THIS_FORM_SelectCriteria, THIS_FORM_OrderBy)
          SelectBySortedRows = myTable.Select(THIS_FORM_SelectCriteria, THIS_FORM_SelectBy)
        Catch ex As Exception
          ' In the event of an error, try to select all rows.
          Me.MainForm.LogError(Me.Name & ",SetSortedRows()", LOG_LEVELS.Error, ex.Message, "Error Selecting Data Rows.", ex.StackTrace, True)

          THIS_FORM_OrderBy = "RN"
          THIS_FORM_SelectBy = "True"

          Try
            SortedRows = myTable.Select(THIS_FORM_SelectCriteria, THIS_FORM_OrderBy)
            SelectBySortedRows = myTable.Select(THIS_FORM_SelectCriteria, THIS_FORM_SelectBy)
          Catch ex_1 As Exception
            ReDim SortedRows(0)
            ReDim SelectBySortedRows(0)
          End Try
        End Try
      End If

    Catch ex As Exception
    Finally
      InPaint = OrgInPaint
    End Try

  End Sub


  ' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
  Private Sub CheckPermissions()

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

  End Sub

  ' Flag changes to form controls, Event Associations made in 'New' routine.
    ''' <summary>
  ''' Basic event handler, called when a form value is changed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    If InPaint = False Then
      Dim thisFund As RenaissanceDataClass.DSFund.tblFundRow = Nothing
      Dim TransactionType As RenaissanceGlobals.TransactionTypes = Nothing
      Dim thisInstrument As RenaissanceDataClass.DSInstrument.tblInstrumentRow = Nothing
      Dim InstrumentType As RenaissanceGlobals.InstrumentTypes = Nothing
      Dim ThisBrokerAccount As RenaissanceDataClass.DSBrokerAccounts.tblBrokerAccountsRow = Nothing
      Dim ThisBroker As RenaissanceDataClass.DSBroker.tblBrokerRow = Nothing

      If (Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord) Then
        FormChanged = True
        Me.btnSave.Enabled = True
        Me.Label_TransactionAddOrUpdate.Visible = True
        Me.Label_TransactionStatusLabel.Visible = True
        Me.btnCancel.Enabled = True

        If (sender Is Combo_TransactionFund) Then
          Call SetSubFundCombo()
          Call SetSpecificShareClassCombo()
        End If

        'If (sender Is Combo_SubFund) AndAlso (Not AddNewRecord) AndAlso (thisDataRow isnot nothing) andalso (thisDataRow.s Then
        '  Exit Sub
        'End If

        If (AddNewRecord) AndAlso ((sender Is Combo_TransactionInstrument) Or (sender Is Combo_TransactionType) Or (sender Is Date_ValueDate)) Then
          ' Get Default settlement dates.
          If IsNumeric(Combo_TransactionInstrument.SelectedValue) Then

            thisInstrument = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, CInt(Combo_TransactionInstrument.SelectedValue)), RenaissanceDataClass.DSInstrument.tblInstrumentRow)

            If (thisInstrument IsNot Nothing) Then
              TransactionType = CType(Nz(Me.Combo_TransactionType.SelectedValue, TransactionTypes.Buy), RenaissanceGlobals.TransactionTypes)

              ' Set Value and settlement dates to default if the instrument is changed.

              If (sender Is Combo_TransactionInstrument) Then
                If (thisInstrument.InstrumentDealingCutOffTime > 0) AndAlso (thisInstrument.InstrumentDealingCutOffTime < CInt(Now.TimeOfDay.TotalSeconds)) Then
                  ' After Cutoff
                  Date_ValueDate.Value = AddPeriodToDate(DealingPeriod.Daily, Now.Date, 1)
                Else
                  Date_ValueDate.Value = Now.Date
                End If

                If (TransactionType = TransactionTypes.Sell) Then
                  Date_SettlementDate.Value = AddPeriodToDate(thisInstrument.InstrumentDealingPeriod, Date_ValueDate.Value, thisInstrument.InstrumentDefaultSettlementDays_Sell)
                Else
                  Date_SettlementDate.Value = AddPeriodToDate(thisInstrument.InstrumentDealingPeriod, Date_ValueDate.Value, thisInstrument.InstrumentDefaultSettlementDays_Buy)
                End If
              ElseIf (sender Is Date_ValueDate) Then
                If (TransactionType = TransactionTypes.Sell) Then
                  Date_SettlementDate.Value = AddPeriodToDate(thisInstrument.InstrumentDealingPeriod, Date_ValueDate.Value, thisInstrument.InstrumentDefaultSettlementDays_Sell)
                Else
                  Date_SettlementDate.Value = AddPeriodToDate(thisInstrument.InstrumentDealingPeriod, Date_ValueDate.Value, thisInstrument.InstrumentDefaultSettlementDays_Buy)
                End If
              End If

            End If
          End If
        End If

        ' DEFAULT_FX_FOR_INSTRUMENT_TYPES()
        ' Check for default FX if : NewRecord && Check_FXSettlement = false && InstrumentFX != FundFX

        If (AddNewRecord) AndAlso ((sender Is Combo_TransactionFund) Or (sender Is Combo_TransactionInstrument)) Then
          If IsNumeric(Combo_TransactionFund.SelectedValue) AndAlso IsNumeric(Combo_TransactionInstrument.SelectedValue) Then

            thisFund = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, CInt(Combo_TransactionFund.SelectedValue)), RenaissanceDataClass.DSFund.tblFundRow)

            If (thisInstrument Is Nothing) Then
              thisInstrument = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, CInt(Combo_TransactionInstrument.SelectedValue)), RenaissanceDataClass.DSInstrument.tblInstrumentRow)
            End If

            If (thisInstrument IsNot Nothing) AndAlso (thisFund IsNot Nothing) AndAlso (thisFund.FundDefaultTradeFXToFundCurrency) Then
              InstrumentType = CType(thisInstrument.InstrumentType, RenaissanceGlobals.InstrumentTypes)

              ' This Instrument Type eligible for Auto FX
              If DEFAULT_FX_FOR_INSTRUMENT_TYPES.Contains(InstrumentType) Then

                If (thisInstrument.InstrumentCurrencyID <> thisFund.FundBaseCurrency) Then
                  Check_FXSettlement.Checked = True
                  Combo_SettlementCurrency.SelectedValue = thisFund.FundBaseCurrency
                Else
                  Check_FXSettlement.Checked = False
                  Combo_SettlementCurrency.SelectedValue = thisFund.FundBaseCurrency
                  edit_FXRate.Value = 1.0#
                End If

              ElseIf (Combo_TransactionInstrument.Focused) Then
                Check_FXSettlement.Checked = False
                edit_FXRate.Value = 1.0#
              End If
            End If

          End If
        End If

        '
        If (sender Is Combo_Broker) Then
          SetBrokerTradeTypesCombo()
        End If

        ' Show Broker Account
        If (AddNewRecord) AndAlso ((sender Is Combo_TransactionFund) OrElse (sender Is Combo_TransactionInstrument) OrElse (sender Is Combo_TransactionType) OrElse (sender Is Combo_Broker)) Then
          Label_BrokerInfo.Text = ""

          If (IsNumeric(Combo_Broker.SelectedValue) AndAlso (CInt(Combo_Broker.SelectedValue) > 0) AndAlso IsNumeric(Combo_TransactionFund.SelectedValue) AndAlso IsNumeric(Combo_TransactionInstrument.SelectedValue)) Then

            Dim TransactionExchangeID As Integer = 0

            If (thisInstrument Is Nothing) Then
              thisInstrument = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, CInt(Combo_TransactionInstrument.SelectedValue)), RenaissanceDataClass.DSInstrument.tblInstrumentRow)
            End If

            If (thisInstrument IsNot Nothing) Then
              ThisBrokerAccount = GetExpectedBrokerAccount(MainForm, CInt(Combo_TransactionFund.SelectedValue), thisInstrument.InstrumentType, CInt(Combo_Broker.SelectedValue), TransactionExchangeID, thisInstrument.InstrumentCurrencyID, thisInstrument.InstrumentID)

              If (ThisBrokerAccount IsNot Nothing) Then
                ThisBroker = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblBroker, ThisBrokerAccount.BrokerID), RenaissanceDataClass.DSBroker.tblBrokerRow)

                If (ThisBroker Is Nothing) Then
                  Label_BrokerInfo.Text = "Broker Account found but Broker (tblBroker) missing : " & ThisBrokerAccount.AccountDescription & " (" & ThisBrokerAccount.AuditID.ToString & ")"
                Else
                  Label_BrokerInfo.Text = "Execute : " & IIf(ThisBroker.IsEMSX, "EMSX, ", "") & ThisBrokerAccount.AccountDescription & ", " & ThisBrokerAccount.AccountNumber
                End If
              End If
            End If
          End If
        End If

        ' Combo_OpeningOrClosingTrade
        If (sender Is Combo_TransactionInstrument) OrElse (sender Is Combo_Broker) Then
          If IsNumeric(Combo_TransactionInstrument.SelectedValue) AndAlso (IsNumeric(Combo_Broker.SelectedIndex)) Then

            If (CInt(Nz(Me.Combo_Broker.SelectedValue, 0)) > 0) Then
              If (thisInstrument Is Nothing) Then
                thisInstrument = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, CInt(Combo_TransactionInstrument.SelectedValue)), RenaissanceDataClass.DSInstrument.tblInstrumentRow)
              End If

              Select Case CType(thisInstrument.InstrumentType, InstrumentTypes)

                Case InstrumentTypes.Option
                  Me.Combo_OpeningOrClosingTrade.Visible = True
                  Label_OpeningOrClosingTrade.Visible = True

                Case Else
                  Me.Combo_OpeningOrClosingTrade.Visible = False
                  Label_OpeningOrClosingTrade.Visible = False

              End Select

            Else

              Me.Combo_OpeningOrClosingTrade.Visible = False
              Label_OpeningOrClosingTrade.Visible = False

            End If

          Else
            Me.Combo_OpeningOrClosingTrade.Visible = False
            Label_OpeningOrClosingTrade.Visible = False
          End If
        End If



        ' BUY / SELL Formatting

        If (sender Is Combo_TransactionType) Then
          SetFormatting()
        End If

        ' Get Best FX if appropriate

        If (sender Is Combo_SettlementCurrency) Then
          Call Button_GetFX_Click(Button_GetFX, Nothing)
        End If

        ' 

        If (sender Is Me.Combo_Workflow) Then
          Custom_Workflow = True
          Combo_Workflow.BackColor = CUSTOM_BACKGROUND_CHANGED
        End If

        '

        If (sender IsNot Combo_TradeStatus) Then
          Call SetExpectedWorkflow(AddNewRecord, True)
        End If

      End If
    End If

  End Sub

    ''' <summary>
    ''' Selects the menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
  Public Sub SelectMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
    ' Event handler for the Select Menu
    ' Event association is made during the dynamic menu creation

    THIS_FORM_SelectBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

    Call SetSortedRows()

  End Sub

    ''' <summary>
    ''' Orders the menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
  Public Sub OrderMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
    ' Event handler for the Order Menu
    ' Event association is made during the dynamic menu creation

    THIS_FORM_OrderBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

    Call SetSortedRows()

  End Sub


    ''' <summary>
    ''' Audits the report menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
  Public Sub AuditReportMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
    ' Event handler for the Audit Report Menu
    ' Event association is made during the dynamic menu creation

    Try
      Select Case CInt(CType(sender, System.Windows.Forms.ToolStripMenuItem).Tag)
        Case 0 ' This Record
          If (Me.AddNewRecord = False) AndAlso (thisAuditID >= 0) Then
            Run_AuditReport_ThisRecord(MainForm, ThisStandardDataset, thisAuditID, MainForm.VeniceProgressBar)
          End If

        Case 1 ' All Records
          Run_AuditReport_ALLRecords(MainForm, ThisStandardDataset, MainForm.VeniceProgressBar)

      End Select
    Catch ex As Exception
      Call MainForm.LogError(Me.Name & ", AuditReportMenuEvent()", 0, ex.Message, "Error calling Audit report.", ex.StackTrace, True)
    End Try

  End Sub


#End Region

#Region " Set Form Combos (Form Specific Code) "

    ''' <summary>
    ''' Sets the fund combo.
    ''' </summary>
  Private Sub SetFundCombo()
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_TransactionFund, _
    RenaissanceStandardDatasets.tblFund, _
    "FundName", _
    "FundID", _
    "FundClosed=0")   ' 

  End Sub

  ''' <summary>
  ''' Sets the Sub-Fund combo.
  ''' </summary>
  Private Sub SetSubFundCombo()
    ' *******************************************************************************
    '
    ' *******************************************************************************
    Dim FundID As Integer = 0

    Try

      If (IsNumeric(Combo_TransactionFund.SelectedValue)) Then
        FundID = CInt(Combo_TransactionFund.SelectedValue)
      End If

    Catch ex As Exception
    End Try

    Call MainForm.SetTblGenericCombo( _
      Me.Combo_SubFund, _
      RenaissanceStandardDatasets.tblSubFundHeirarchy, _
      "Level|SubFundName", _
      "SubFundID", _
      "TopFundID=" & FundID.ToString, False, False, True, 0, "", "TreeOrder")   ' 

  End Sub

  ''' <summary>
  ''' Sets the Specific Share Class Combo.
  ''' </summary>
  Private Sub SetSpecificShareClassCombo()
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Dim ThisFundID As Integer = CInt(Nz(Combo_TransactionFund.SelectedValue, 0))

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_SpecificShareClass, _
    RenaissanceStandardDatasets.tblInstrument, _
    "InstrumentDescription", _
    "InstrumentID", _
    "InstrumentFundID=" & ThisFundID.ToString, False, True, True, 0, "")   ' 

  End Sub

  ''' <summary>
  ''' Sets the instrument combo.
  ''' </summary>
  Private Sub SetInstrumentCombo(ByVal pShowExpiredInstruments As Boolean)
    ' *******************************************************************************
    '
    ' *******************************************************************************

    If (Check_ByISIN.Checked) Then

      If (pShowExpiredInstruments) Then
        InstrumentsShowingExpired = True

        Call MainForm.SetTblGenericCombo( _
           Me.Combo_TransactionInstrument, _
           RenaissanceStandardDatasets.tblInstrument, _
           "InstrumentISIN|InstrumentDescription", _
           "InstrumentID", _
           "InstrumentISIN<>''")   ' 

      Else
        InstrumentsShowingExpired = False

        Call MainForm.SetTblGenericCombo( _
           Me.Combo_TransactionInstrument, _
           RenaissanceStandardDatasets.tblInstrument, _
           "InstrumentISIN|InstrumentDescription", _
           "InstrumentID", _
           "(InstrumentISIN<>'') AND ((InstrumentLastValidTradeDate>='" & Now.Date.ToString(QUERY_SHORTDATEFORMAT) & "') OR (InstrumentLastValidTradeDate<='1900-01-01'))")   ' 

      End If

    Else

      If (pShowExpiredInstruments) Then
        InstrumentsShowingExpired = True

        Call MainForm.SetTblGenericCombo( _
            Me.Combo_TransactionInstrument, _
            RenaissanceStandardDatasets.tblInstrument, _
            "InstrumentDescription|InstrumentISIN", _
            "InstrumentID", _
            "")   ' 

      Else
        InstrumentsShowingExpired = False

        Call MainForm.SetTblGenericCombo( _
            Me.Combo_TransactionInstrument, _
            RenaissanceStandardDatasets.tblInstrument, _
            "InstrumentDescription|InstrumentISIN", _
            "InstrumentID", _
            "((InstrumentLastValidTradeDate>='" & Now.Date.ToString(QUERY_SHORTDATEFORMAT) & "') OR (InstrumentLastValidTradeDate<='1900-01-01'))")   ' 

      End If


    End If

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_Versusinstrument, _
    RenaissanceStandardDatasets.tblInstrument, _
    "InstrumentDescription", _
    "InstrumentID", _
    "InstrumentType=" & CInt(RenaissanceGlobals.InstrumentTypes.Expense).ToString, False, True, True, 0, "")   ' 

    ' Get Futures style Instrument Types (Once only)

    If (FuturesStyleInstrumentTypes.Length = 0) Then
      Dim thisTable As RenaissanceDataClass.DSInstrumentType.tblInstrumentTypeDataTable
      Dim theseRows() As RenaissanceDataClass.DSInstrumentType.tblInstrumentTypeRow
      Dim thisRow As RenaissanceDataClass.DSInstrumentType.tblInstrumentTypeRow

      Try

        thisTable = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblInstrumentType, False), RenaissanceDataClass.DSInstrumentType).tblInstrumentType
        theseRows = thisTable.Select("InstrumentTypeFuturesStylePricing>0")

        For Each thisRow In theseRows
          If (FuturesStyleInstrumentTypes.Length = 0) Then
            FuturesStyleInstrumentTypes = thisRow.InstrumentTypeID.ToString
          Else
            FuturesStyleInstrumentTypes &= "," & thisRow.InstrumentTypeID.ToString
          End If
        Next

        If (FuturesStyleInstrumentTypes.Length = 0) Then
          FuturesStyleInstrumentTypes = "0"
        End If

      Catch ex As Exception
        FuturesStyleInstrumentTypes = "0"
      End Try

    End If

    Call MainForm.SetTblGenericCombo( _
     Me.Combo_VersusFuturesInstrument, _
     RenaissanceStandardDatasets.tblInstrument, _
     "InstrumentDescription", _
     "InstrumentID", _
     "InstrumentType IN (" & FuturesStyleInstrumentTypes & ")", False, True, True, 0, "")   ' 

  End Sub

  ''' <summary>
  ''' Sets the transaction type combo.
  ''' </summary>
  Private Sub SetTransactionTypeCombo()
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_TransactionType, _
    RenaissanceStandardDatasets.tblTransactionType, _
    "TransactionType", _
    "TransactionTypeID", _
    "")   ' 

  End Sub

  ''' <summary>
  ''' Sets the trade status combo.
  ''' </summary>
  Private Sub SetTradeStatusCombo()
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_TradeStatus, _
    RenaissanceStandardDatasets.tblTradeStatus, _
    "tradeStatusName", _
    "tradeStatusID", _
    "")   ' 

  End Sub

  Private Sub SetBrokerTradeTypesCombo()
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Dim TradeTypeString As String = "|Market|Limit|Manual|Manual (GTC)"
    Dim BrokerID As Integer
    Dim BrokerRow As RenaissanceDataClass.DSBroker.tblBrokerRow
    Dim tradeCounter As Integer
    Dim Trades() As String
    Dim ExistingValue As String = ""
    Dim ExistingValueExists As Boolean = False

    Try
      ExistingValue = Nz(Combo_BrokerOrderType.SelectedItem, "")

      If (IsNumeric(Combo_Broker.SelectedValue)) Then
        BrokerID = CInt(Combo_Broker.SelectedValue)

        If (BrokerID > 0) Then
          BrokerRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblBroker, BrokerID)

          If (BrokerRow IsNot Nothing) AndAlso (Not BrokerRow.IsValidOrderTypesNull) Then
            TradeTypeString = BrokerRow.ValidOrderTypes
          End If
        End If
      End If

      Trades = TradeTypeString.Split(New Char() {"|"})

      Combo_BrokerOrderType.Items.Clear()

      For tradeCounter = 0 To (Trades.Length - 1)
        Combo_BrokerOrderType.Items.Add(Trades(tradeCounter))

        If (ExistingValue = Trades(tradeCounter)) Then
          ExistingValueExists = True
        End If
      Next

      Try
        If (ExistingValueExists) Then
          Combo_BrokerOrderType.SelectedItem = ExistingValue
        Else
          Combo_BrokerOrderType.SelectedItem = ""
        End If
      Catch ex As Exception

      End Try
    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Sets the workflow combo.
  ''' </summary>
  Private Sub SetWorkflowCombo()
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_Workflow, _
    RenaissanceStandardDatasets.tblWorkflows, _
    "WorkflowName", _
    "WorkflowID", _
    "")   ' 

  End Sub

  ''' <summary>
  ''' Sets the currency combo.
  ''' </summary>
  Private Sub SetCurrencyCombo()
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_SettlementCurrency, _
    RenaissanceStandardDatasets.tblCurrency, _
    "CurrencyDescription", _
    "CurrencyID", _
    "")   ' 

  End Sub

  ''' <summary>
  ''' Sets the redemption ID combo.
  ''' </summary>
  ''' <param name="ShowEmptyCombo">if set to <c>true</c> [show empty combo].</param>
  Private Sub SetRedemptionIDCombo(Optional ByVal ShowEmptyCombo As Boolean = False)
    ' *******************************************************************************
    ' Populate the Redemption ID Combo with Subscription transactions relating to the 
    ' currently selected Counterparty, Fund and Instrument.
    '
    ' *******************************************************************************

    Dim FundID As Integer = 0
    Dim InstrumentID As Integer = 0
    Dim CounterpartyID As Integer = 0
    Dim TransactionType As Integer = 0

    Dim SelectString As String = "False"

    If (ShowEmptyCombo = False) Then
      If (Me.Combo_TransactionType.SelectedIndex >= 0) AndAlso (IsNumeric(Combo_TransactionType.SelectedValue) = True) Then
        TransactionType = Combo_TransactionType.SelectedValue
      End If

      If (TransactionType = RenaissanceGlobals.TransactionTypes.Redeem) Then
        If (Me.Combo_TransactionFund.SelectedIndex >= 0) AndAlso (IsNumeric(Combo_TransactionFund.SelectedValue) = True) Then
          FundID = Combo_TransactionFund.SelectedValue
        End If

        If (Me.Combo_TransactionInstrument.SelectedIndex >= 0) AndAlso (IsNumeric(Combo_TransactionInstrument.SelectedValue) = True) Then
          InstrumentID = Combo_TransactionInstrument.SelectedValue
        End If

        If (Me.Combo_TransactionCounterparty.SelectedIndex >= 0) AndAlso (IsNumeric(Combo_TransactionCounterparty.SelectedValue) = True) Then
          CounterpartyID = Combo_TransactionCounterparty.SelectedValue
        End If

        SelectString = "(TransactionCounterparty=" & CounterpartyID.ToString & ") AND (TransactionType=" & CInt(RenaissanceGlobals.TransactionTypes.Subscribe).ToString & ") AND (TransactionFund=" & FundID.ToString & ") AND (TransactionInstrument=" & InstrumentID.ToString & ")"
      End If
    End If

    If (SelectString = "False") AndAlso (Combo_RedemptionID.Items.Count = 1) Then
      Exit Sub
    End If

    Call MainForm.SetTblGenericCombo( _
     Me.Combo_RedemptionID, _
     RenaissanceStandardDatasets.tblTransaction, _
     "TransactionTicket", _
     "TransactionParentID", _
     SelectString, True, True, True, 0)    ' 

  End Sub

  ''' <summary>
  ''' Sets the counterparty combo.
  ''' </summary>
  Private Sub SetCounterpartyCombo()
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_TransactionCounterparty, _
    RenaissanceStandardDatasets.tblCounterparty, _
    "CounterpartyName", _
    "CounterpartyID", _
    "", False, True, True)   ' 

  End Sub
  ''' <summary>
  ''' Sets the Broker combo.
  ''' </summary>
  Private Sub SetBrokerCombo()
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_Broker, _
    RenaissanceStandardDatasets.tblBroker, _
    "BrokerDescription", _
    "BrokerID", _
    "", False, True, True, 0)   ' 

  End Sub

  ''' <summary>
  ''' Sets the transaction group combo.
  ''' </summary>
  Private Sub SetTransactionGroupCombo()
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_TransactionGroup, _
    RenaissanceStandardDatasets.tblTransaction, _
    "TransactionGroup", _
    "TransactionGroup", _
    "", _
    True)   ' 

  End Sub

  ''' <summary>
  ''' Sets the person combos.
  ''' </summary>
  Private Sub SetPersonCombos()
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_InvestmentManager, _
    RenaissanceStandardDatasets.tblPerson, _
    "Person", _
    "Person", _
    "")   ' 

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_TransactionManager, _
    RenaissanceStandardDatasets.tblPerson, _
    "Person", _
    "Person", _
    "")   ' 

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_DataEntryManager, _
    RenaissanceStandardDatasets.tblPerson, _
    "Person", _
    "Person", _
    "")   ' 

  End Sub

#End Region

#Region " Get & Set Form Data / SetButton / ValidateForm / btnCancel Events (Form Specific Code) "

  ''' <summary>
  ''' Gets the form data.
  ''' </summary>
  ''' <param name="ThisRow">The this row.</param>
  Private Sub GetFormData(ByRef ThisRow As RenaissanceDataClass.DSTransaction.tblTransactionRow)
    ' *******************************************************************************
    '
    ' Routine to populate form Controls given a data row.
    ' Form is cleared for invalid Datarows or Invalid Form.
    '
    ' *******************************************************************************

    Dim OrgInpaint As Boolean
    Dim InstrumentCurrency As Integer
    Dim thisInstrument As RenaissanceDataClass.DSInstrument.tblInstrumentRow = Nothing

    ' Set 'Paint' flag to prevent event actions caused by changing the value of form fields.
    OrgInpaint = InPaint
    InPaint = True

    Try
      Tab_TransactionDetails.SelectedIndex = 0
      Custom_Workflow = False
      Combo_Workflow.BackColor = Me.Combo_TransactionFund.BackColor

      If (ThisRow Is Nothing) Or (FormIsValid = False) Or (Me.InUse = False) Then
        ' Bad / New Datarow - Clear Form.
        Try
          Try
            If (Check_ShowExpired.Checked) Then
              If (InstrumentsShowingExpired = False) Then
                Call SetInstrumentCombo(True)
              End If
            Else
              If (InstrumentsShowingExpired) Then
                Call SetInstrumentCombo(False)
              End If
            End If
          Catch ex As Exception
          End Try


          thisAuditID = (-1)

          Me.editAuditID.Text = ""
          Me.Label_TransactionAddOrUpdate.Text = "Adding a NEW trade."
          Me.Label_TransactionStatusLabel.Text = "Adding a NEW trade."
          Me.Form_StatusLabel.Text = ""

          MainForm.ClearComboSelection(Combo_TransactionFund)
          MainForm.ClearComboSelection(Combo_TransactionInstrument)
          MainForm.ClearComboSelection(Combo_TransactionType)
          MainForm.ClearComboSelection(Combo_SettlementCurrency)
          MainForm.ClearComboSelection(Combo_SubFund)

          Call SetSubFundCombo()

          If (Combo_TradeStatus.Items.Count > 0) Then
            Combo_TradeStatus.SelectedValue = 0
          End If

          MainForm.ClearComboSelection(Combo_TransactionCounterparty)
          Me.Combo_TransactionCounterparty.SelectedValue = RenaissanceGlobals.Counterparty.MARKET

          If (Me.Combo_Broker.Items.Count > 0) Then
            Combo_Broker.SelectedIndex = 0
          End If
          If (Me.Combo_BrokerOrderType.Items.Count > 0) Then
            Combo_BrokerOrderType.SelectedIndex = 0
          End If

          MainForm.ClearComboSelection(Combo_TransactionGroup)
          MainForm.ClearComboSelection(Combo_InvestmentManager)
          MainForm.ClearComboSelection(Combo_TransactionManager)
          MainForm.ClearComboSelection(Combo_DataEntryManager)

          Call SetRedemptionIDCombo(True)
          If (Combo_RedemptionID.Items.Count > 0) Then
            Me.Combo_RedemptionID.SelectedIndex = 0
          End If

          If (Combo_Versusinstrument.Items.Count > 0) Then
            Combo_Versusinstrument.SelectedIndex = 0
          End If

          If (Combo_SpecificShareClass.Items.Count > 0) Then
            Combo_SpecificShareClass.SelectedIndex = 0
          End If
          Combo_SpecificShareClass.Visible = False
          Label_SpecificShareClass.Visible = False

          If (Combo_VersusFuturesInstrument.Items.Count > 0) Then
            Combo_VersusFuturesInstrument.SelectedIndex = 0
          End If

          If (Combo_Workflow.Items.Count > 0) Then
            Combo_Workflow.SelectedValue = 0
          End If

          Me.Date_DecisionDate.Value = Now
          Me.Date_ValueDate.Value = Now.Date
          Me.Date_SettlementDate.Text = ""
          Me.Date_ConfirmationDate.Value = Now.Date
          Me.Combo_ValueOrAmount.SelectedIndex = 1 ' Amount
          Me.edit_Amount.Value = 0.0#
          Me.edit_Price.Value = 0.0#

          Me.edit_CleanPrice.Value = 0.0#
          Me.edit_Principal.Value = 0.0#
          Me.edit_AccruedInterest.Value = 0.0#
          Radio_BondByPrice.Visible = False
          Radio_BondByPrincipal.Visible = False
          Me.edit_CleanPrice.Visible = False
          Me.edit_Principal.Visible = False
          Label_Principal.Visible = edit_Principal.Visible
          Me.edit_AccruedInterest.Visible = False
          Me.edit_BrokerFees.Value = 0.0#

          Me.edit_Costs.Value = 0.0#
          Me.edit_CostPercentage.Value = 0.0#
          Me.edit_settlement.Value = 0.0#
          Me.edit_FXRate.Value = 1.0#
          Me.Label_InstrumentCurrency.Text = ""
          Me.Label_BrokerInfo.Text = ""

          Combo_OpeningOrClosingTrade.SelectedIndex = 0
          Combo_OpeningOrClosingTrade.Visible = False
          Label_OpeningOrClosingTrade.Visible = False

          Me.Check_CostsAsPercent.CheckState = CheckState.Unchecked
          Me.Check_DontUpdatePrice.CheckState = CheckState.Checked ' Default to true
          Me.Check_IsForwardPayment.CheckState = CheckState.Unchecked
          Me.Check_FinalAmount.CheckState = CheckState.Unchecked
          Me.Check_FinalPrice.CheckState = CheckState.Unchecked
          Me.Check_FinalCosts.CheckState = CheckState.Unchecked
          Me.Check_FinalSettlement.CheckState = CheckState.Unchecked
          Me.Check_InstructionGiven.CheckState = CheckState.Unchecked
          Me.Check_BlotterRedeemWholeHolding.CheckState = CheckState.Unchecked
          Me.Check_BankConfirmation.CheckState = CheckState.Unchecked
          Me.Check_InitialDataEntry.CheckState = CheckState.Unchecked
          Me.Check_PriceConfirmation.CheckState = CheckState.Unchecked
          Me.Check_SettlementConfirmed.CheckState = CheckState.Unchecked
          Me.Check_FinalDataEntry.CheckState = CheckState.Unchecked
          Me.Check_FXSettlement.CheckState = CheckState.Unchecked

          Me.Check_IsTransfer.Checked = False
          Me.Edit_EffectivePrice.Value = 0
          Me.Date_ValueDate.Text = ""
          Me.Check_SpecificInitialEqualisation.Checked = False
          Me.Edit_SpecificInitialEqualisation.Value = 0
          Me.Edit_EffectiveWatermark.Value = 0

          Me.editTransactionComment.Text = ""
          Me.Label_CompoundTransaction.Text = ""
          Me.Label_FXSettlementAmount.Text = "0"
          Me.Label_percent.Text = ""

          SetExpectedWorkflow(AddNewRecord, True)
          SetFormatting(True)

          Me.Button_ViewCT.Enabled = True

          If AddNewRecord = True Then
            Me.btnCancel.Enabled = True
            'Me.THIS_FORM_SelectingCombo.Enabled = False
          Else
            Me.btnCancel.Enabled = False
            'Me.THIS_FORM_SelectingCombo.Enabled = True
          End If

        Catch ex As Exception
          Call MainForm.LogError(Me.Name & ", GetFormData", 0, ex.Message, "Error showing clear form.", ex.StackTrace, True)
        End Try

      Else
        previousPosition = thisPosition

        ' Populate Form with given data.
        Try
          thisAuditID = ThisRow.AuditID
          Me.Label_TransactionAddOrUpdate.Text = "Amending an EXISTING trade."
          Me.Label_TransactionStatusLabel.Text = "Amending an EXISTING trade."

          Try
            If (InstrumentsShowingExpired = False) Then
              Call SetInstrumentCombo(True)
            End If
          Catch ex As Exception
          End Try

          Try
            InstrumentCurrency = CInt(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, ThisRow.TransactionInstrument, "InstrumentCurrencyID"))
          Catch ex As Exception
            InstrumentCurrency = (-1)
          End Try

          Try
            thisInstrument = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, ThisRow.TransactionInstrument), RenaissanceDataClass.DSInstrument.tblInstrumentRow)
          Catch ex As Exception
          End Try

          Me.editAuditID.Text = ThisRow.AuditID.ToString

          Me.Combo_TransactionFund.SelectedValue = ThisRow.TransactionFund
          Call SetSubFundCombo()

          If (ThisRow.TransactionSubFund = ThisRow.TransactionFund) Then
            Me.Combo_SubFund.SelectedValue = 0
          Else
            Me.Combo_SubFund.SelectedValue = ThisRow.TransactionSubFund
          End If

          Me.Combo_TransactionInstrument.SelectedValue = ThisRow.TransactionInstrument
          If (Combo_TransactionInstrument.SelectedValue <> ThisRow.TransactionInstrument) Then
            MainForm.ClearComboSelection(Combo_TransactionInstrument)
          End If
          Me.Combo_TransactionType.SelectedValue = ThisRow.TransactionType
          Me.Combo_TradeStatus.SelectedValue = ThisRow.TransactionTradeStatusID
          Me.Combo_TransactionCounterparty.SelectedValue = ThisRow.TransactionCounterparty
          Me.Combo_Broker.SelectedValue = ThisRow.TransactionBroker

          SetBrokerTradeTypesCombo()

          If (ThisRow.TransactionBroker > 0) Then
            Select Case CType(thisInstrument.InstrumentType, InstrumentTypes)

              Case InstrumentTypes.Option
                Combo_OpeningOrClosingTrade.Visible = True
                Label_OpeningOrClosingTrade.Visible = True

                If Nz(ThisRow.TransactionClosingTrade, False) Then
                  Combo_OpeningOrClosingTrade.SelectedIndex = 2
                Else
                  Combo_OpeningOrClosingTrade.SelectedIndex = 1
                End If

              Case Else

                Combo_OpeningOrClosingTrade.SelectedIndex = 0
                Combo_OpeningOrClosingTrade.Visible = False
                Label_OpeningOrClosingTrade.Visible = False
            End Select
          Else
            Combo_OpeningOrClosingTrade.SelectedIndex = 0
            Combo_OpeningOrClosingTrade.Visible = False
            Label_OpeningOrClosingTrade.Visible = False
          End If

          Me.Combo_BrokerOrderType.SelectedItem = ThisRow.TransactionBrokerStrategy
          Me.Combo_TransactionGroup.Text = ThisRow.TransactionGroup
          Me.Combo_InvestmentManager.Text = ThisRow.TransactionInvestment
          Me.Combo_TransactionManager.SelectedValue = ThisRow.TransactionExecution
          Me.Combo_DataEntryManager.SelectedValue = ThisRow.TransactionDataEntry
          Me.Date_DecisionDate.Value = ThisRow.TransactionDecisionDate
          'Lotfi:  the decision date is now editable so it's no longer necessarly the first entry date ...
          'Me.Form_StatusLabel.Text = "First entered : " & ThisRow.TransactionDecisionDate.ToString(DISPLAYMEMBER_LONGDATEFORMAT)
          Me.Form_StatusLabel.Text = "Decision date : " & ThisRow.TransactionDecisionDate.ToString(DISPLAYMEMBER_LONGDATEFORMAT)

          Me.Date_ValueDate.Value = ThisRow.TransactionValueDate
          Me.Date_SettlementDate.Value = ThisRow.TransactionSettlementDate
          Me.Date_ConfirmationDate.Value = ThisRow.TransactionConfirmationDate
          Me.Label_InstrumentCurrency.Text = Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, ThisRow.TransactionInstrument, "InstrumentCurrency"), "")
          Me.Label_percent.Text = ""
          Me.Label_BrokerInfo.Text = ""

          Me.Combo_SettlementCurrency.SelectedValue = ThisRow.TransactionSettlementCurrencyID
          Me.edit_FXRate.Value = ThisRow.TransactionFXRate

          Me.Check_IsTransfer.Checked = ThisRow.TransactionIsTransfer
          If (ThisRow.TransactionIsTransfer) Then
            Me.Edit_EffectivePrice.Value = ThisRow.TransactionEffectivePrice
            Me.Date_EffectiveValueDate.Value = ThisRow.TransactionEffectiveValueDate
            Me.Edit_EffectiveWatermark.Value = ThisRow.TransactionEffectiveWatermark
            Me.Check_SpecificInitialEqualisation.Checked = ThisRow.TransactionSpecificInitialEqualisationFlag

            If (ThisRow.TransactionSpecificInitialEqualisationFlag) Then
              Me.Edit_SpecificInitialEqualisation.Value = ThisRow.TransactionSpecificInitialEqualisationValue
            Else
              Me.Edit_SpecificInitialEqualisation.Value = 0
            End If
          Else
            Me.Edit_EffectivePrice.Value = ThisRow.TransactionPrice
            Me.Date_EffectiveValueDate.Value = ThisRow.TransactionValueDate
            Me.Check_SpecificInitialEqualisation.Checked = False
            Me.Edit_SpecificInitialEqualisation.Value = 0
            Me.Edit_EffectiveWatermark.Value = 0
          End If

          If ThisRow.TransactionValueorAmount.ToUpper = "VALUE" Then
            Me.Combo_ValueOrAmount.SelectedIndex = 0
          Else
            Me.Combo_ValueOrAmount.SelectedIndex = 1
          End If

          Me.edit_Amount.Value = ThisRow.TransactionUnits
          Me.edit_Price.Value = ThisRow.TransactionPrice

          Me.edit_CleanPrice.Value = ThisRow.TransactionCleanPrice
          Me.edit_AccruedInterest.Value = ThisRow.TransactionAccruedInterest
          Me.edit_Principal.Value = ThisRow.TransactionCleanPrice * ThisRow.TransactionUnits * thisInstrument.InstrumentMultiplier * thisInstrument.InstrumentContractSize

          Select Case CType(thisInstrument.InstrumentType, InstrumentTypes)

            Case InstrumentTypes.Bond, InstrumentTypes.ConvertibleBond
              Radio_BondByPrice.Visible = True
              Radio_BondByPrincipal.Visible = True
              Me.edit_CleanPrice.Visible = True
              Me.edit_AccruedInterest.Visible = True
              Me.edit_Principal.Visible = True
              Label_Principal.Visible = edit_Principal.Visible

              Call Radio_BondByPrice_CheckedChanged(Radio_BondByPrice, Nothing)

            Case Else
              Radio_BondByPrice.Visible = False
              Radio_BondByPrincipal.Visible = False
              Me.edit_CleanPrice.Visible = False
              Me.edit_AccruedInterest.Visible = False
              Me.edit_Principal.Visible = False
              Label_Principal.Visible = edit_Principal.Visible

          End Select

          Me.edit_BrokerFees.Value = ThisRow.TransactionBrokerFees

          Me.edit_Costs.Value = ThisRow.TransactionCosts
          Me.edit_CostPercentage.Value = ThisRow.TransactionCostPercent

          Me.Check_CostsAsPercent.Checked = ThisRow.TransactionCostIsPercent
          Me.Check_DontUpdatePrice.Checked = ThisRow.TransactionExemptFromUpdate

          Try
            Me.edit_settlement.Value = Math.Round(GetTransactionUnitsOrSettlement("Amount", ThisRow.TransactionUnits, ThisRow.TransactionPrice, GetInstrumentContractSize(Me.Name, MainForm, ThisRow.TransactionInstrument), ThisRow.TransactionBrokerFees, ThisRow.TransactionCosts, 0, GetCashMove(Me.Name, Me.MainForm, ThisRow.TransactionType)), 6)
          Catch ex As Exception
            Me.edit_settlement.Value = ThisRow.TransactionSettlement
          End Try

          If (InstrumentCurrency >= 0) AndAlso (InstrumentCurrency <> ThisRow.TransactionSettlementCurrencyID) Then
            Check_FXSettlement.CheckState = CheckState.Checked
            Me.Label_FXSettlementAmount.Text = CDbl(Me.edit_settlement.Value * Me.edit_FXRate.Value).ToString(DISPLAYMEMBER_DOUBLEFORMAT)
          Else
            Check_FXSettlement.CheckState = CheckState.Unchecked
            Me.Label_FXSettlementAmount.Text = Me.edit_settlement.Value.ToString(DISPLAYMEMBER_DOUBLEFORMAT)
          End If

          'If (Check_FXSettlement.Checked) AndAlso (Me.edit_FXRate.Value <> 0.0#) Then
          '	Me.Label_FXSettlementAmount.Text = CDbl(Me.edit_settlement.Value * Me.edit_FXRate.Value).ToString(DISPLAYMEMBER_DOUBLEFORMAT)
          'Else
          '	Me.Label_FXSettlementAmount.Text = Me.edit_settlement.Value.ToString(DISPLAYMEMBER_DOUBLEFORMAT)
          'End If

          If ThisRow.TransactionIsForwardPayment = False Then
            Me.Check_IsForwardPayment.CheckState = CheckState.Unchecked
          Else
            Me.Check_IsForwardPayment.CheckState = CheckState.Checked
          End If

          If ThisRow.TransactionFinalAmount = 0 Then
            Me.Check_FinalAmount.CheckState = CheckState.Unchecked
          Else
            Me.Check_FinalAmount.CheckState = CheckState.Checked
          End If

          If ThisRow.TransactionFinalPrice = 0 Then
            Me.Check_FinalPrice.CheckState = CheckState.Unchecked
          Else
            Me.Check_FinalPrice.CheckState = CheckState.Checked
          End If

          If ThisRow.TransactionFinalCosts = 0 Then
            Me.Check_FinalCosts.CheckState = CheckState.Unchecked
          Else
            Me.Check_FinalCosts.CheckState = CheckState.Checked
          End If

          If ThisRow.TransactionFinalSettlement = 0 Then
            Me.Check_FinalSettlement.CheckState = CheckState.Unchecked
          Else
            Me.Check_FinalSettlement.CheckState = CheckState.Checked
          End If

          If ThisRow.TransactionInstructionFlag = 0 Then
            Me.Check_InstructionGiven.CheckState = CheckState.Unchecked
          Else
            Me.Check_InstructionGiven.CheckState = CheckState.Checked
          End If

          If ThisRow.TransactionRedeemWholeHoldingFlag = False Then
            Me.Check_BlotterRedeemWholeHolding.CheckState = CheckState.Unchecked
          Else
            Me.Check_BlotterRedeemWholeHolding.CheckState = CheckState.Checked
          End If

          If ThisRow.TransactionBankConfirmation = 0 Then
            Me.Check_BankConfirmation.CheckState = CheckState.Unchecked
          Else
            Me.Check_BankConfirmation.CheckState = CheckState.Checked
          End If

          If ThisRow.TransactionInitialDataEntry = 0 Then
            Me.Check_InitialDataEntry.CheckState = CheckState.Unchecked
          Else
            Me.Check_InitialDataEntry.CheckState = CheckState.Checked
          End If

          If ThisRow.TransactionAmountConfirmed = 0 Then
            Me.Check_PriceConfirmation.CheckState = CheckState.Unchecked
          Else
            Me.Check_PriceConfirmation.CheckState = CheckState.Checked
          End If

          If ThisRow.TransactionSettlementConfirmed = 0 Then
            Me.Check_SettlementConfirmed.CheckState = CheckState.Unchecked
          Else
            Me.Check_SettlementConfirmed.CheckState = CheckState.Checked
          End If

          If ThisRow.TransactionFinalDataEntry = 0 Then
            Me.Check_FinalDataEntry.CheckState = CheckState.Unchecked
          Else
            Me.Check_FinalDataEntry.CheckState = CheckState.Checked
          End If

          Me.editTransactionComment.Text = ThisRow.TransactionComment

          Call SetRedemptionIDCombo()

          If ThisRow.TransactionRedemptionID <= 0 Then
            If (Combo_RedemptionID.Items.Count > 0) AndAlso (Combo_RedemptionID.SelectedIndex > 0) Then
              Me.Combo_RedemptionID.SelectedIndex = 0 ' Leading blank
            End If
          Else
            Me.Combo_RedemptionID.SelectedValue = ThisRow.TransactionRedemptionID
          End If

          ' Combo_Versusinstrument

          If ThisRow.TransactionVersusInstrument <= 0 Then
            ' Not versus an instrument

            If (Combo_Versusinstrument.Items.Count > 0) AndAlso (Combo_Versusinstrument.SelectedIndex > 0) Then
              Me.Combo_Versusinstrument.SelectedIndex = 0 ' Leading blank
            End If
            If (Combo_VersusFuturesInstrument.Items.Count > 0) AndAlso (Combo_VersusFuturesInstrument.SelectedIndex > 0) Then
              Me.Combo_VersusFuturesInstrument.SelectedIndex = 0  ' Leading blank
            End If
          Else
            Try
              ' notional Versus Future, or something else...

              Dim InstrumentType As RenaissanceGlobals.InstrumentTypes
              ' Dim InstrumentPricingType As Integer = 0

              If (ThisRow.TransactionInstrument > 0) Then
                InstrumentType = LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, ThisRow.TransactionInstrument, "InstrumentType")
                ' InstrumentPricingType = LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrumentType, InstrumentType, "InstrumentTypeFuturesStylePricing") ' 

                If InstrumentType = InstrumentTypes.Notional Then
                  Me.Combo_VersusFuturesInstrument.SelectedValue = ThisRow.TransactionVersusInstrument
                Else
                  Me.Combo_Versusinstrument.SelectedValue = ThisRow.TransactionVersusInstrument
                End If

              End If
            Catch ex As Exception
            End Try
          End If

          ' Combo_SpecificShareClass
          Call SetSpecificShareClassCombo()

          Select Case CType(ThisRow.TransactionType, TransactionTypes)

            Case TransactionTypes.Subscribe, TransactionTypes.Redeem

              MainForm.ClearComboSelection(Combo_SpecificShareClass)
              Combo_SpecificShareClass.Visible = False
              Label_SpecificShareClass.Visible = False

            Case Else
              Combo_SpecificShareClass.SelectedValue = ThisRow.TransactionSpecificToFundUnitID
              Combo_SpecificShareClass.Visible = True
              Label_SpecificShareClass.Visible = True

          End Select

          AddNewRecord = False

          ' MainForm.SetComboSelectionLengths(Me)

          SetExpectedWorkflow(False, False)
          SetFormatting()

          Me.btnCancel.Enabled = False
          'Me.THIS_FORM_SelectingCombo.Enabled = True

        Catch ex As Exception

          Call MainForm.LogError(Me.Name & ", GetFormData", 0, ex.Message, "Error Showing Data", ex.StackTrace, True)
          Call GetFormData(Nothing)

        End Try

      End If

      Call SetFormLabels()

    Catch ex As Exception

      Call MainForm.LogError(Me.Name & ", GetFormData", 0, ex.Message, "Error Showing Data", ex.StackTrace, True)

    Finally

      Me.btnSave.Enabled = False
      Me.Label_TransactionAddOrUpdate.Visible = False
      Me.Label_TransactionStatusLabel.Visible = False

      ' As it says on the can.... (Perform before DoEvents() incase ThisRow becomes invalid during DoEvents() i.e. Background updating)
      Call SetButtonStatus(ThisRow)

      ' Allow Field events to trigger before 'InPaint' Is re-set. 
      ' (Should) Prevent Validation errors during Form Draw.
      Application.DoEvents()

      FormChanged = False

      ' Restore 'Paint' flag.
      InPaint = OrgInpaint

    End Try

  End Sub

  ''' <summary>
  ''' Sets the expected workflow.
  ''' </summary>
  ''' <param name="pIsNewTrade">if set to <c>true</c> [p is new trade].</param>
  ''' <param name="pSetStatus">if set to <c>true</c> [p set status].</param>
  Private Sub SetExpectedWorkflow(ByVal pIsNewTrade As Boolean, ByVal pSetStatus As Boolean)
    ' *******************************************************************************
    '
    ' *******************************************************************************
    Dim OrgPaint As Boolean = InPaint

    Try
      If (Custom_Workflow = False) Then
        InPaint = True ' Stop the Combo_Workflow events setting Custom_Workflow = true 

        Dim SuggestedWorkflowRule As RenaissanceDataClass.DSWorkflowRules.tblWorkflowRulesRow = Nothing
        Dim InstrumentID As Integer = 0
        Dim InstrumentRow As RenaissanceDataClass.DSInstrument.tblInstrumentRow = Nothing
        Dim InstrumentType As Integer = 0
        Dim Currency As Integer = 0
        Dim CurrentStatus As Integer = 0

        If (IsNumeric(Combo_TransactionInstrument.SelectedValue)) Then
          InstrumentID = CInt(Combo_TransactionInstrument.SelectedValue)
        End If

        ' Get Instrument Details

        If (InstrumentID <> 0) Then
          InstrumentRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, InstrumentID)
        End If

        If (InstrumentRow IsNot Nothing) Then
          InstrumentType = InstrumentRow.InstrumentType
          Currency = InstrumentRow.InstrumentCurrencyID
        End If

        ' Current Status

        If pIsNewTrade = False Then
          If IsNumeric(Combo_TradeStatus.SelectedValue) Then
            CurrentStatus = CInt(Combo_TradeStatus.SelectedValue)
          End If
        End If

        ' Different Settlement currency

        If (Check_FXSettlement.Checked) AndAlso (IsNumeric(Combo_SettlementCurrency.SelectedValue)) Then
          Currency = CInt(Combo_SettlementCurrency.SelectedValue)
        End If

        '

        If (IsNumeric(Combo_TransactionInstrument.SelectedValue) AndAlso IsNumeric(Combo_TransactionFund.SelectedValue)) Then
          SuggestedWorkflowRule = GetExpectedWorkflowRule(MainForm, Nz(Combo_TransactionFund.SelectedValue, 0), InstrumentType, 0, Currency, Combo_TransactionInstrument.SelectedValue, CurrentStatus, pIsNewTrade, False)
        End If

        ' Set Workflow Combo...

        If (SuggestedWorkflowRule IsNot Nothing) Then
          Combo_Workflow.SelectedValue = SuggestedWorkflowRule.ResultingWorkflow
        Else
          Combo_Workflow.SelectedValue = 0
        End If


      End If ' not Custom Workflow

      Dim ExpectedWorkflow As Integer = 0
      Dim ExpectedStatus As Integer

      Try

        If (pSetStatus) Then

          If (IsNumeric(Combo_Workflow.SelectedValue)) Then
            ExpectedWorkflow = CInt(Combo_Workflow.SelectedValue)
          End If

          If (ExpectedWorkflow <> 0) Then
            ExpectedStatus = CInt(Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblWorkflows, ExpectedWorkflow, "ResultingTradeStatus"), 0))

            If (ExpectedStatus <> 0) Then
              Combo_TradeStatus.SelectedValue = ExpectedStatus
            ElseIf (IsNumeric(Combo_TradeStatus.SelectedValue) = False) OrElse CInt(Combo_TradeStatus.SelectedValue) = 0 Then
              Combo_TradeStatus.SelectedValue = CInt(RenaissanceGlobals.TradeStatus.Reconciled)
            End If
          ElseIf (IsNumeric(Combo_TradeStatus.SelectedValue)) AndAlso CInt(Combo_TradeStatus.SelectedValue) = 0 Then
            Combo_TradeStatus.SelectedValue = CInt(RenaissanceGlobals.TradeStatus.Reconciled)
          End If

        End If

      Catch ex As Exception
        Call MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in SetExpectedWorkflow()", ex.StackTrace, True)
      End Try

    Catch ex As Exception
      Call MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in SetExpectedWorkflow()", ex.StackTrace, True)
    Finally
      InPaint = OrgPaint
    End Try

  End Sub

  ''' <summary>
  ''' Sets the form labels.
  ''' </summary>
  Private Sub SetFormLabels()
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Dim FormLabelsDS As RenaissanceDataClass.DSDataDictionary_TransactionFormLabels.tblDataDictionary_TransactionFormLabelsDataTable
    Dim FormLabelRows() As RenaissanceDataClass.DSDataDictionary_TransactionFormLabels.tblDataDictionary_TransactionFormLabelsRow
    Dim FormLabelRow As RenaissanceDataClass.DSDataDictionary_TransactionFormLabels.tblDataDictionary_TransactionFormLabelsRow
    Dim FormLabelTransactionType As Integer

    Try
      FormLabelTransactionType = 0
      If (Me.Combo_TransactionType.SelectedIndex >= 0) Then
        Try
          FormLabelTransactionType = CType(Me.Combo_TransactionType.SelectedValue, Integer)
        Catch ex As Exception
        End Try
      End If

      FormLabelsDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblDataDictionary_TransactionFormLabels, False).Tables(0)
      FormLabelRows = FormLabelsDS.Select("((TransactionType=0) OR (TransactionType=" & FormLabelTransactionType.ToString & ")) AND (CompoundFlag=0)", "TransactionType DESC")
      If (FormLabelRows.Length) > 0 Then
        FormLabelRow = FormLabelRows(0)
      Else
        FormLabelRow = Nothing
      End If
    Catch ex As Exception
      FormLabelRow = Nothing
    End Try

    If (FormLabelRow Is Nothing) Then
      Me.Label_Counterparty.Text = "Counterparty"

      Me.Label_Price.Text = "Price"
      Me.Label_DecisionDate.Text = "Decision Date"
      Me.Label_TradeDate.Text = "Trade Date"
      Me.Label_SettlementDate.Text = "Settlement Date"
      Me.Label_ConfirmationDate.Text = "Conirmation Date"

      Me.Check_InstructionGiven.Text = "Instruction Given"
      Me.Check_BankConfirmation.Text = "Bank Confirmation"
      Me.Check_InitialDataEntry.Text = "Initial Data Entry"
      Me.Check_PriceConfirmation.Text = "Price Confirmation"
      Me.Check_SettlementConfirmed.Text = "Settlement Confirmed"
      Me.Check_FinalDataEntry.Text = "Final Data Entry"
    Else
      Me.Label_Counterparty.Text = FormLabelRow.CounterpartyText

      Me.Label_Price.Text = FormLabelRow.TransactionPrice
      Me.Label_DecisionDate.Text = FormLabelRow.DecisionDateText
      Me.Label_TradeDate.Text = FormLabelRow.TradeDateText
      Me.Label_SettlementDate.Text = FormLabelRow.SettlementDateText
      Me.Label_ConfirmationDate.Text = FormLabelRow.ConfirmationDateText

      Me.Check_InstructionGiven.Text = FormLabelRow.FieldText1
      Me.Check_BankConfirmation.Text = FormLabelRow.FieldText2
      Me.Check_InitialDataEntry.Text = FormLabelRow.FieldText3
      Me.Check_PriceConfirmation.Text = FormLabelRow.FieldText4
      Me.Check_SettlementConfirmed.Text = FormLabelRow.FieldText5
      Me.Check_FinalDataEntry.Text = FormLabelRow.FieldText6

    End If

  End Sub

  ''' <summary>
  ''' Sets the formatting.
  ''' </summary>
  ''' <param name="Neutral">if set to <c>true</c> [neutral].</param>
  Private Sub SetFormatting(Optional ByVal Neutral As Boolean = False)
    ' *******************************************************************************
    ' Function to set contingent form formatting.
    ' At the moment, this relates only to cosmetic formatting indicating BUY or SELL conditions.
    ' *******************************************************************************

    Try

      Dim thisTransactionType As RenaissanceGlobals.TransactionTypes
      Dim TransactionType As RenaissanceDataClass.DSTransactionType.tblTransactionTypeRow

      If (Neutral) OrElse (Nz(Combo_TransactionType.SelectedValue, 0) <= 0) Then
        Combo_TransactionType.BackColor = System.Drawing.SystemColors.Window
        Combo_TransactionType.ForeColor = System.Drawing.SystemColors.WindowText

        Label_TransactionStatusLabel.BackColor = System.Drawing.SystemColors.Control
        Label_TransactionStatusLabel.ForeColor = System.Drawing.SystemColors.ControlText
      Else
        thisTransactionType = CType(Me.Combo_TransactionType.SelectedValue, RenaissanceGlobals.TransactionTypes)
        TransactionType = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblTransactionType, CInt(thisTransactionType))

        If (TransactionType.TransactionTypeCashMove <= 0) Then ' BUY
          Combo_TransactionType.BackColor = BUY_Background
          Combo_TransactionType.ForeColor = BUY_Foreground

          If (AddNewRecord) Then
            Label_TransactionStatusLabel.BackColor = NEW_Background
            Label_TransactionStatusLabel.ForeColor = BUY_Foreground
          Else
            Label_TransactionStatusLabel.BackColor = AMEND_Background
            Label_TransactionStatusLabel.ForeColor = BUY_Foreground
          End If
        Else ' SELL
          Combo_TransactionType.BackColor = SELL_Background
          Combo_TransactionType.ForeColor = SELL_Foreground

          If (AddNewRecord) Then
            Label_TransactionStatusLabel.BackColor = NEW_Background
            Label_TransactionStatusLabel.ForeColor = SELL_Foreground
          Else
            Label_TransactionStatusLabel.BackColor = AMEND_Background
            Label_TransactionStatusLabel.ForeColor = SELL_Foreground
          End If
        End If

      End If

    Catch ex As Exception

    End Try
  End Sub


  ''' <summary>
  ''' Sets the form data.
  ''' </summary>
  ''' <param name="pConfirm">if set to <c>true</c> [p confirm].</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function SetFormData(Optional ByVal pConfirm As Boolean = True) As Boolean
    ' *************************************************************
    '
    ' *************************************************************
    Dim ErrMessage As String
    Dim ErrFlag As Boolean
    Dim ErrStack As String
    Dim thisInstrumentRow As RenaissanceDataClass.DSInstrument.tblInstrumentRow = Nothing
    Dim thisTransactionType As TransactionTypes = 0

    ErrMessage = ""
    ErrStack = ""


    ' *************************************************************
    ' Appropriate Save permission :-
    ' *************************************************************

    If (Me.HasUpdatePermission = False) And ((Me.HasInsertPermission = False) Or (Me.AddNewRecord = False)) Then
      MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "You do not have permission to save this record.", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Function
    End If

    ' *************************************************************
    ' If Save button is disabled then should not be able to save, exit silently.
    ' *************************************************************
    If Me.btnSave.Enabled = False Then
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Function
    End If

    ' *************************************************************
    ' KnowledgeDate OK :-
    ' *************************************************************

    If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
      MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Changes to Venice are not allowed when the Knowledgedate is not set to `NOW`", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Function
    End If

    ' Confirm Save, if required.

    If (pConfirm = True) Then
      If MessageBox.Show("Save Changes ?", "Save Changes ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
        Return True
        Exit Function
      End If
    End If



    ' *************************************************************
    ' Procedure to Save the current form information.
    ' *************************************************************

    Dim StatusString As String = ""
    Dim LogString As String
    Dim UpdateRows(0) As DataRow
    Dim Position As Integer

    If (FormChanged = False) Or (FormIsValid = False) Then
      Return False
      Exit Function
    End If

    ' Validation
    If ValidateForm(StatusString) = False Then
      MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Validation Error." & vbCrLf & StatusString, "", True)
      Return False
      Exit Function
    End If

    ' Check current position in the table.
    Position = Get_Position(thisAuditID)

    ' Allow for new or missing ID.
    If Position < 0 Then
      If (AddNewRecord = True) OrElse (SortedRows.Length <= 0) Then
        thisDataRow = myTable.NewRow
        LogString = "Add: "
        AddNewRecord = True
      Else
        Return False
        Exit Function
      End If
    Else
      ' Row found OK.
      LogString = "Edit : AuditID = " & thisAuditID.ToString

      thisDataRow = Me.SortedRows(Position)

    End If

    '

    thisInstrumentRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, CInt(Combo_TransactionInstrument.SelectedValue)), RenaissanceDataClass.DSInstrument.tblInstrumentRow)

    ' Check if Instrument has changed on an existing transaction
    If (AddNewRecord = False) Then

      If (thisDataRow.TransactionInstrument <> CInt(Combo_TransactionInstrument.SelectedValue)) Then
        ' Confirm
        Dim InitialName As String
        Dim NewName As String

        InitialName = LookupTableValue(MainForm, RenaissanceGlobals.RenaissanceStandardDatasets.tblInstrument, thisDataRow.TransactionInstrument, "InstrumentDescription")
        NewName = thisInstrumentRow.InstrumentDescription

        If MessageBox.Show("You are changing the instrument on an existing transaction from" & vbCrLf & InitialName & " to " & vbCrLf & NewName & vbCrLf & "Do you wish to continue ?", "Transaction Query", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) <> Windows.Forms.DialogResult.OK Then
          Return False
          Exit Function
        End If

      End If
    End If

    ' Check FX vs Non-Cash.

    If (IsNumeric(Combo_TransactionType.SelectedValue)) Then
      thisTransactionType = CType(Combo_TransactionType.SelectedValue, TransactionTypes)
    End If

    If ((CType(thisInstrumentRow.InstrumentType, InstrumentTypes) <> InstrumentTypes.Cash) AndAlso ((thisTransactionType = TransactionTypes.BuyFX) OrElse (thisTransactionType = TransactionTypes.SellFX) OrElse (thisTransactionType = TransactionTypes.BuyFXForward) OrElse (thisTransactionType = TransactionTypes.SellFXForward))) Then
      If (MessageBox.Show("You are entering an FX Trade for a non-Cash instrument" & vbCrLf & "Do you really want to do this ?" & vbCrLf & "OK to continue, Abort to cancel the save", "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Cancel) Then
        Exit Function
      End If
    End If

    ' Set 'Paint' flag.
    InPaint = True

    ' Check to see if a price already exists for this Instrument On-or-Before the Value Date.
    ' If not, then prompt to add one.

    If GetPriceExist(CInt(Combo_TransactionInstrument.SelectedValue), Date_ValueDate.Value.Date) = False Then
      If MessageBox.Show("There does not appear to be a price saved for this Instrument on or before the Value Date" & vbCrLf & "Do you wish to add one ?", "No Price", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
        Try
          Dim PricesTbl As New RenaissanceDataClass.DSPrice.tblPriceDataTable
          Dim PriceRow As RenaissanceDataClass.DSPrice.tblPriceRow
          Dim PriceAdaptor As SqlDataAdapter

          PriceAdaptor = MainForm.MainDataHandler.Get_Adaptor(RenaissanceStandardDatasets.tblPrice.Adaptorname, VENICE_CONNECTION, RenaissanceStandardDatasets.tblPrice.TableName)

          If PricesTbl IsNot Nothing Then
            PriceRow = PricesTbl.NewtblPriceRow
          Else
            PriceRow = Nothing
          End If

          If (PriceRow IsNot Nothing) AndAlso (PriceAdaptor IsNot Nothing) Then
            PriceRow.PriceInstrument = CInt(Combo_TransactionInstrument.SelectedValue)
            ' Make the Price Date the last day of the previous month.
            ' This should resolve regular P&L report issues for Nick S.
            PriceRow.PriceDate = Me.Date_ValueDate.Value.Date.AddDays(-Date_ValueDate.Value.Date.Day)
            PriceRow.PriceFinal = 0
            PriceRow.PriceIsMilestone = 0
            PriceRow.PriceIsAdministrator = 0
            PriceRow.PriceIsPercent = False
            PriceRow.PricePercent = 0
            PriceRow.PriceLevel = Me.edit_Price.Value
            PriceRow.PriceMultiplier = 1.0#
            PriceRow.PriceNAV = PriceRow.PriceLevel ' Not Used.
            PriceRow.PriceBasicNAV = 0
            PriceRow.PriceGNAV = 0
            PriceRow.PriceGAV = 0
            PriceRow.SetPriceBaseDateNull()
            PriceRow.PriceComment = "Auto Price - Add Transaction"

            SyncLock PricesTbl
              PriceAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
              PriceAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

              PricesTbl.AddtblPriceRow(PriceRow)
              UpdateRows(0) = PriceRow
              MainForm.AdaptorUpdate(Me.Name, PriceAdaptor, UpdateRows)
              UpdateRows(0) = Nothing
            End SyncLock

            ' Reload Table and post update message

            If (PriceRow IsNot Nothing) AndAlso (PriceRow.IsAuditIDNull = False) Then
              MainForm.ReloadTable(RenaissanceGlobals.RenaissanceStandardDatasets.tblPrice.ChangeID, PriceRow.AuditID.ToString, True)
            Else
              MainForm.ReloadTable(RenaissanceGlobals.RenaissanceStandardDatasets.tblPrice.ChangeID, "", True)
            End If

          End If

        Catch ex As Exception
          MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Adding Price.", ex.StackTrace, True)
        End Try
      Else
        MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "There does not appear to be a price saved for Instrument " & Combo_TransactionInstrument.SelectedValue.ToString & "  on or before the Value Date", "", False)
      End If

    End If

    ' *************************************************************
    ' Lock the Data Table, to prevent update conflicts.
    ' *************************************************************


    SyncLock myTable

      ' Initiate Edit,
      thisDataRow.BeginEdit()

      ' Set Data Values
      'thisDataRow.TransactionComment = Me.editTransactionComment.Text
      'LogString &= ", Transaction = " & thisDataRow.Transaction

      'If Me.Radio_Pay.Checked = True Then
      '  thisDataRow.TransactionCashMove = (-1)
      'Else
      '  thisDataRow.TransactionCashMove = 1
      'End If
      'LogString &= ", CashMove = " & thisDataRow.TransactionCashMove.ToString

      thisDataRow.TransactionFund = Me.Combo_TransactionFund.SelectedValue

      Try
        If (IsNumeric(Me.Combo_SubFund.SelectedValue)) Then
          thisDataRow.TransactionSubFund = Nz(Me.Combo_SubFund.SelectedValue, thisDataRow.TransactionFund)
        Else
          thisDataRow.TransactionSubFund = thisDataRow.TransactionFund
        End If

        If (thisDataRow.TransactionSubFund = 0) Then
          thisDataRow.TransactionSubFund = thisDataRow.TransactionFund
        End If
      Catch ex As Exception
      End Try

      thisDataRow.TransactionInstrument = Me.Combo_TransactionInstrument.SelectedValue
      thisDataRow.TransactionType = Me.Combo_TransactionType.SelectedValue
      thisDataRow.TransactionTradeStatusID = Me.Combo_TradeStatus.SelectedValue
      thisDataRow.TransactionCounterparty = CInt(Nz(Me.Combo_TransactionCounterparty.SelectedValue, 0))
      thisDataRow.TransactionBroker = CInt(Nz(Me.Combo_Broker.SelectedValue, 0))
      thisDataRow.TransactionBrokerStrategy = CStr(Nz(Me.Combo_BrokerOrderType.SelectedItem, ""))
      thisDataRow.TransactionGroup = Me.Combo_TransactionGroup.Text
      thisDataRow.TransactionInvestment = Me.Combo_InvestmentManager.Text
      thisDataRow.TransactionExecution = Nz(Me.Combo_TransactionManager.SelectedValue, "")
      thisDataRow.TransactionDataEntry = Nz(Me.Combo_DataEntryManager.SelectedValue, "")

      'lotfi: take the entered decisionDate
      thisDataRow.TransactionDecisionDate = Nz(Me.Date_DecisionDate.Value, Now()) ' Note : Date and Time....
      'If (AddNewRecord) Then
      '  thisDataRow.TransactionDecisionDate = Now()
      'Else
      '  thisDataRow.TransactionDecisionDate = Nz(Me.Date_DecisionDate.Value, "") ' Note : Date and Time....
      'End If

      thisDataRow.TransactionValueDate = Me.Date_ValueDate.Value.Date
      thisDataRow.TransactionSettlementDate = Me.Date_SettlementDate.Value.Date
      thisDataRow.TransactionConfirmationDate = Me.Date_ConfirmationDate.Value.Date

      Select Case CInt(Nz(Combo_OpeningOrClosingTrade.SelectedIndex, 0))

        Case 2
          thisDataRow.TransactionClosingTrade = Combo_OpeningOrClosingTrade.Visible ' (Only set 'True' value if the control is actually visible)

        Case Else
          thisDataRow.TransactionClosingTrade = False

      End Select

      If (Me.Combo_ValueOrAmount.Text.ToUpper.StartsWith("V")) OrElse ((thisInstrumentRow IsNot Nothing) AndAlso (thisInstrumentRow.InstrumentOrderOnlyByValue)) Then
        thisDataRow.TransactionValueorAmount = "Value"
      Else
        thisDataRow.TransactionValueorAmount = "Amount"
      End If

      ' Zero Units and Costs for Cancelled trades.

      If (thisDataRow.TransactionTradeStatusID = TradeStatus.Cancelled) OrElse (thisDataRow.TransactionTradeStatusID = TradeStatus.CancelledFM) Then
        thisDataRow.TransactionUnits = 0.0#
        thisDataRow.TransactionCosts = 0.0#
      Else
        thisDataRow.TransactionUnits = Math.Round(Me.edit_Amount.Value, thisInstrumentRow.InstrumentOrderPrecision)
        thisDataRow.TransactionCosts = Me.edit_Costs.Value
      End If

      ' 

      thisDataRow.TransactionPrice = Me.edit_Price.Value

      thisDataRow.TransactionCleanPrice = Me.edit_CleanPrice.Value
      thisDataRow.TransactionAccruedInterest = Me.edit_AccruedInterest.Value
      thisDataRow.TransactionBrokerFees = Me.edit_BrokerFees.Value

      thisDataRow.TransactionCostPercent = Me.edit_CostPercentage.Value

      ' If you entered an Amount, but were forced into a 'Value' then round the value to the nearest integer.
      If (Not Me.Combo_ValueOrAmount.Text.ToUpper.StartsWith("V")) AndAlso ((thisInstrumentRow IsNot Nothing) AndAlso (thisInstrumentRow.InstrumentOrderOnlyByValue)) Then
        thisDataRow.TransactionSettlement = Math.Round(Me.edit_settlement.Value)
      Else
        thisDataRow.TransactionSettlement = Me.edit_settlement.Value
      End If

      ' Enforce Positive amounts for new Expense and Provision trades
      If (AddNewRecord) Then
        Select Case CType(thisDataRow.TransactionType, TransactionTypes)

          Case TransactionTypes.Expense_Increase, TransactionTypes.Expense_Reduce, TransactionTypes.Provision_Increase, TransactionTypes.Provision_Reduce

            thisDataRow.TransactionUnits = Math.Abs(thisDataRow.TransactionUnits)

        End Select
      End If


      ' FX on Settlement ?

      If (Check_FXSettlement.Checked) AndAlso (IsNumeric(Combo_SettlementCurrency.SelectedValue)) AndAlso (edit_FXRate.Value <> 0.0#) AndAlso (CInt(Combo_SettlementCurrency.SelectedValue) <> CInt(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, thisDataRow.TransactionInstrument, "InstrumentCurrencyID"))) Then
        thisDataRow.TransactionSettlementCurrencyID = CInt(Combo_SettlementCurrency.SelectedValue)
        thisDataRow.TransactionFXRate = edit_FXRate.Value
      Else
        thisDataRow.TransactionSettlementCurrencyID = 0 ' Default to Instrument Currency
        thisDataRow.TransactionFXRate = 1.0#
      End If

      ' Transfer Details.
      ' Note Transfer details are only relevant for Subscription Transactions.
      ' the 'IsTransfer' flag is also relevent for redemptions, though the other details are not.

      If (Check_IsTransfer.Enabled) AndAlso (Check_IsTransfer.Checked) Then
        thisDataRow.TransactionIsTransfer = True

        If (thisDataRow.TransactionType = RenaissanceGlobals.TransactionTypes.Subscribe) Then
          thisDataRow.TransactionEffectivePrice = Edit_EffectivePrice.Value
          thisDataRow.TransactionEffectiveValueDate = Date_EffectiveValueDate.Value

          If (Check_SpecificInitialEqualisation.Checked) Then
            thisDataRow.TransactionSpecificInitialEqualisationFlag = True
            thisDataRow.TransactionSpecificInitialEqualisationValue = Edit_SpecificInitialEqualisation.Value
          Else
            thisDataRow.TransactionSpecificInitialEqualisationFlag = False
            thisDataRow.TransactionSpecificInitialEqualisationValue = 0
          End If
          thisDataRow.TransactionEffectiveWatermark = Edit_EffectiveWatermark.Value
        Else
          thisDataRow.TransactionEffectivePrice = thisDataRow.TransactionPrice
          thisDataRow.TransactionEffectiveValueDate = thisDataRow.TransactionValueDate
          thisDataRow.TransactionSpecificInitialEqualisationFlag = False
          thisDataRow.TransactionSpecificInitialEqualisationValue = 0
          thisDataRow.TransactionEffectiveWatermark = 0
        End If

      Else
        thisDataRow.TransactionIsTransfer = False
        thisDataRow.TransactionEffectivePrice = thisDataRow.TransactionPrice
        thisDataRow.TransactionEffectiveValueDate = thisDataRow.TransactionValueDate
        thisDataRow.TransactionSpecificInitialEqualisationFlag = False
        thisDataRow.TransactionSpecificInitialEqualisationValue = 0
        thisDataRow.TransactionEffectiveWatermark = 0
      End If ' Check_IsTransfer

      If Me.Check_CostsAsPercent.CheckState = CheckState.Unchecked Then
        thisDataRow.TransactionCostIsPercent = False
      Else
        thisDataRow.TransactionCostIsPercent = True
      End If

      If Me.Check_DontUpdatePrice.CheckState = CheckState.Unchecked Then
        thisDataRow.TransactionExemptFromUpdate = False
      Else
        thisDataRow.TransactionExemptFromUpdate = True
      End If

      thisDataRow.TransactionIsForwardPayment = (Check_IsForwardPayment.Checked And Check_IsForwardPayment.Visible)

      If Me.Check_FinalAmount.CheckState = CheckState.Unchecked Then
        thisDataRow.TransactionFinalAmount = 0
      Else
        thisDataRow.TransactionFinalAmount = (-1)
      End If

      If Me.Check_FinalPrice.CheckState = CheckState.Unchecked Then
        thisDataRow.TransactionFinalPrice = 0
      Else
        thisDataRow.TransactionFinalPrice = (-1)
      End If

      If Me.Check_FinalCosts.CheckState = CheckState.Unchecked Then
        thisDataRow.TransactionFinalCosts = 0
      Else
        thisDataRow.TransactionFinalCosts = (-1)
      End If

      If Me.Check_FinalSettlement.CheckState = CheckState.Unchecked Then
        thisDataRow.TransactionFinalSettlement = 0
      Else
        thisDataRow.TransactionFinalSettlement = (-1)
      End If

      If Me.Check_InstructionGiven.CheckState = CheckState.Unchecked Then
        thisDataRow.TransactionInstructionFlag = 0
      Else
        thisDataRow.TransactionInstructionFlag = (-1)
      End If

      If Me.Check_BlotterRedeemWholeHolding.CheckState = CheckState.Unchecked Then
        thisDataRow.TransactionRedeemWholeHoldingFlag = False
      Else
        thisDataRow.TransactionRedeemWholeHoldingFlag = True
      End If

      If Me.Check_BankConfirmation.CheckState = CheckState.Unchecked Then
        thisDataRow.TransactionBankConfirmation = 0
      Else
        thisDataRow.TransactionBankConfirmation = (-1)
      End If

      If Me.Check_InitialDataEntry.CheckState = CheckState.Unchecked Then
        thisDataRow.TransactionInitialDataEntry = 0
      Else
        thisDataRow.TransactionInitialDataEntry = (-1)
      End If

      If Me.Check_PriceConfirmation.CheckState = CheckState.Unchecked Then
        thisDataRow.TransactionAmountConfirmed = 0
      Else
        thisDataRow.TransactionAmountConfirmed = (-1)
      End If

      If Me.Check_SettlementConfirmed.CheckState = CheckState.Unchecked Then
        thisDataRow.TransactionSettlementConfirmed = 0
      Else
        thisDataRow.TransactionSettlementConfirmed = (-1)
      End If

      If Me.Check_FinalDataEntry.CheckState = CheckState.Unchecked Then
        thisDataRow.TransactionFinalDataEntry = 0
      Else
        thisDataRow.TransactionFinalDataEntry = (-1)
      End If

      thisDataRow.TransactionRedemptionID = 0
      If (Me.Combo_RedemptionID.Enabled = True) AndAlso (Me.Combo_RedemptionID.Visible = True) AndAlso (Me.Combo_RedemptionID.SelectedIndex > 0) Then
        If IsNumeric(Combo_RedemptionID.SelectedValue) Then
          thisDataRow.TransactionRedemptionID = Combo_RedemptionID.SelectedValue
        End If
      End If

      thisDataRow.TransactionVersusInstrument = 0
      If (Me.Combo_Versusinstrument.Enabled = True) AndAlso (Me.Combo_Versusinstrument.Visible = True) AndAlso (Me.Combo_Versusinstrument.SelectedIndex > 0) Then
        If IsNumeric(Combo_Versusinstrument.SelectedValue) Then
          thisDataRow.TransactionVersusInstrument = Combo_Versusinstrument.SelectedValue
        End If
      ElseIf (Me.Combo_VersusFuturesInstrument.Enabled = True) AndAlso (Me.Combo_VersusFuturesInstrument.Visible = True) AndAlso (Me.Combo_VersusFuturesInstrument.SelectedIndex > 0) Then
        If IsNumeric(Combo_VersusFuturesInstrument.SelectedValue) Then
          thisDataRow.TransactionVersusInstrument = Combo_VersusFuturesInstrument.SelectedValue
        End If
      End If

      ' Combo_SpecificShareClass

      Select Case CType(thisDataRow.TransactionType, TransactionTypes)

        Case TransactionTypes.Subscribe, TransactionTypes.Redeem
          thisDataRow.TransactionSpecificToFundUnitID = 0

        Case Else
          thisDataRow.TransactionSpecificToFundUnitID = Combo_SpecificShareClass.SelectedValue

      End Select

      thisDataRow.TransactionComment = Me.editTransactionComment.Text

      thisDataRow.TransactionCTFLAGS = 0
      thisDataRow.TransactionCompoundRN = 0

      thisDataRow.EndEdit()
      InPaint = False

      ' Add and Update DataRow. 

      ErrFlag = False

      If AddNewRecord = True Then
        myTable.Rows.Add(thisDataRow)
      End If
      UpdateRows(0) = thisDataRow

      ' Post Additions / Updates to the underlying table :-
      ' 
      ' The Return Value from the Insert / Update Stored procedure should be the
      ' Transaction Parent ID of the just-modified transaction. If Zero is returned
      ' then an erro occurred, in this instance report the error and do not update
      ' the form as if the transaction was saved correctly.
      '
      Dim temp As Integer
      Try
        If (ErrFlag = False) Then
          myAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
          myAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

          temp = MainForm.AdaptorUpdate(Me.Name, myAdaptor, UpdateRows)
        End If

        Dim SQLReturnValue As Integer
        Dim SQLReturnString As String

        If AddNewRecord = True Then
          SQLReturnValue = CInt(myAdaptor.InsertCommand.Parameters("@RETURN_VALUE").Value)
          SQLReturnString = CStr(myAdaptor.InsertCommand.Parameters("@rvStatusString").Value)
        Else
          SQLReturnValue = CInt(myAdaptor.UpdateCommand.Parameters("@RETURN_VALUE").Value)
          SQLReturnString = CStr(myAdaptor.UpdateCommand.Parameters("@rvStatusString").Value)
        End If

        If (SQLReturnValue = 0) Then
          ErrMessage = SQLReturnString
          ErrFlag = True
          ErrStack = ""

          If AddNewRecord = True Then
            myTable.Rows.Remove(thisDataRow)
          End If
        Else
          thisAuditID = thisDataRow.AuditID
        End If

      Catch ex As Exception

        ErrMessage = ex.Message
        ErrFlag = True
        ErrStack = ex.StackTrace

      End Try


      ' De-Restrict this Form Selection if a New Transaction Has been added.
      THIS_FORM_SelectCriteria = "True"

    End SyncLock

    ' *************************************************************
    ' Error report. Not done in the Try-Catch as that would serve to persist the SyncLock.
    ' *************************************************************

    If (ErrFlag = True) Then
      Call MainForm.LogError(Me.Name & ", SetFormData", 0, ErrMessage, "Error Saving Data", ErrStack, True)

      Return False
      Exit Function
    End If

    ' Finish off

    AddNewRecord = False
    FormChanged = False

    'Me.THIS_FORM_SelectingCombo.Enabled = True

    ' Performance improver...
    GetFormData(thisDataRow)

    ' Propogate changes

    Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID, thisAuditID.ToString), True)

  End Function

  ''' <summary>
  ''' Sets the button status.
  ''' </summary>
  ''' <param name="ThisRow">The this row.</param>
  Private Sub SetButtonStatus(ByVal ThisRow As RenaissanceDataClass.DSTransaction.tblTransactionRow)
    ' Sets the status of the form controlls appropriate to the current users 
    'permissions and the 'Changed' or 'New' status of the form.

    ' For the moment, consider Compound Transactions as Un-Editable using this form.
    ' Treat them as Read-Only.

    Dim IsThisACompoundTransaction As Boolean
    Dim thisTransactionType As RenaissanceGlobals.TransactionTypes = TransactionTypes.Consideration
    Dim thisInstrument As RenaissanceDataClass.DSInstrument.tblInstrumentRow = Nothing

    IsThisACompoundTransaction = False

    If (Not (ThisRow Is Nothing)) AndAlso (ThisRow.RowState <> DataRowState.Detached) AndAlso (ThisRow.TransactionCompoundRN > 0) Then
      IsThisACompoundTransaction = True
      Me.Label_CompoundTransaction.Text = "This Transaction is part of a Compound Transaction."
      Me.Button_ViewCT.Enabled = True
    Else
      Me.Label_CompoundTransaction.Text = ""
      Me.Button_ViewCT.Enabled = False
    End If

    ' No Read permission :-

    If Me.HasReadPermission = False Then
      MainForm.LogError(Me.Name & ", SetButtonStatus()", 0, "", "You do not have Read permission for this Form", "", True)
      Me.Close()
      Exit Sub
    End If

    ' Has Insert Permission.
    If Me.HasInsertPermission Then
      Me.btnAdd.Enabled = True
      Me.btnNewUsing.Enabled = True
    Else
      Me.btnAdd.Enabled = False
      Me.btnNewUsing.Enabled = False
    End If

    ' Has Delete permission. 
    If (Me.HasDeletePermission) And (Me.AddNewRecord = False) And (thisPosition >= 0) Then
      Me.btnDelete.Enabled = True
    Else
      Me.btnDelete.Enabled = False
    End If

    If (Me.MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And _
     ((Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord)) And _
     (IsThisACompoundTransaction = False) Then

      If (ThisRow IsNot Nothing) Then
        thisInstrument = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, ThisRow.TransactionInstrument)
      ElseIf (CInt(Nz(Combo_TransactionInstrument.SelectedValue, 0)) > 0) Then
        thisInstrument = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, CInt(Nz(Combo_TransactionInstrument.SelectedValue, 0)))
      End If

      Me.Combo_TransactionFund.Enabled = True
      Me.Combo_SubFund.Enabled = True
      Me.Combo_TransactionInstrument.Enabled = True
      Me.Combo_TransactionType.Enabled = True
      Me.Combo_TradeStatus.Enabled = True
      Me.Combo_TransactionCounterparty.Enabled = True
      Me.Combo_Broker.Enabled = True
      Me.Combo_OpeningOrClosingTrade.Enabled = True
      Me.Combo_BrokerOrderType.Enabled = True
      Me.Combo_TransactionGroup.Enabled = True
      Me.Combo_InvestmentManager.Enabled = True
      Me.Combo_TransactionManager.Enabled = True
      Me.Combo_DataEntryManager.Enabled = True
      Me.Combo_Workflow.Enabled = True
      Me.Date_DecisionDate.Enabled = True
      Me.Date_ValueDate.Enabled = True
      Me.Date_SettlementDate.Enabled = True
      Me.Date_ConfirmationDate.Enabled = True

      Me.edit_Price.Enabled = True
      Me.edit_CleanPrice.Enabled = True
      Me.edit_Principal.Enabled = True
      Me.edit_AccruedInterest.Enabled = True
      Me.edit_BrokerFees.Enabled = True

      If (thisInstrument IsNot Nothing) Then
        Select Case CType(thisInstrument.InstrumentType, InstrumentTypes)

          Case InstrumentTypes.Bond, InstrumentTypes.ConvertibleBond
            Me.Combo_ValueOrAmount.Enabled = False
            Combo_ValueOrAmount.SelectedIndex = 1 ' Amount

            Call Radio_BondByPrice_CheckedChanged(Radio_BondByPrice, Nothing)

          Case Else
            Me.Combo_ValueOrAmount.Enabled = True

        End Select

      Else
        Me.Combo_ValueOrAmount.Enabled = True
      End If

      SetValueFieldEnabled()

      Me.Check_FXSettlement.Enabled = True
      Me.Combo_SettlementCurrency.Visible = Me.Check_FXSettlement.Checked
      Me.Combo_SettlementCurrency.Enabled = True
      Me.edit_FXRate.Visible = Me.Check_FXSettlement.Checked
      Me.Button_GetFX.Visible = Me.Check_FXSettlement.Checked
      Me.edit_FXRate.Enabled = True
      Me.Combo_SpecificShareClass.Enabled = True

      Me.Check_IsForwardPayment.Enabled = True
      Me.Check_CostsAsPercent.Enabled = True
      Me.Check_DontUpdatePrice.Enabled = True
      Me.Check_FinalAmount.Enabled = True
      Me.Check_FinalPrice.Enabled = True
      Me.Check_FinalCosts.Enabled = True
      Me.Check_FinalSettlement.Enabled = True
      Me.Check_InstructionGiven.Enabled = True
      Me.Check_IsTransfer.Enabled = False

      Me.Check_BankConfirmation.Enabled = True
      Me.Check_InitialDataEntry.Enabled = True
      Me.Check_PriceConfirmation.Enabled = True
      Me.Check_SettlementConfirmed.Enabled = True
      Me.Check_FinalDataEntry.Enabled = True
      Me.editTransactionComment.Enabled = True

      ' Conditional formatting based on Transaction Type 

      If (Me.Combo_TransactionType.SelectedIndex >= 0) Then
        Try

          thisTransactionType = CType(Me.Combo_TransactionType.SelectedValue, RenaissanceGlobals.TransactionTypes)

          Select Case thisTransactionType

            Case TransactionTypes.Sell

              Me.Check_BlotterRedeemWholeHolding.Enabled = Combo_TransactionType.Enabled

            Case TransactionTypes.Subscribe, TransactionTypes.Redeem

              Me.Check_IsTransfer.Enabled = True

            Case TransactionTypes.Expense_Increase, TransactionTypes.Expense_Reduce

              If (thisPosition < 0) Then ' new Transaction
                Me.edit_Price.Enabled = False
                Me.edit_Price.Value = 1.0#
                Call UpdateUnitsAndSettlement(Nothing, Nothing)
              End If

            Case TransactionTypes.Provision_Increase
              If (thisPosition < 0) Then ' new Transaction
                Me.edit_Price.Enabled = False
                Me.edit_Price.Value = 0.0#
                Call UpdateUnitsAndSettlement(Nothing, Nothing)
              End If

            Case TransactionTypes.Provision_Reduce

              If (thisPosition < 0) Then ' new Transaction
                Me.edit_Price.Enabled = True
                Call Button_GetPrice_Click(Nothing, Nothing)
                Call UpdateUnitsAndSettlement(Nothing, Nothing)
              End If

            Case Else

              Me.Check_BlotterRedeemWholeHolding.Enabled = False
              Me.Check_BlotterRedeemWholeHolding.Checked = False

          End Select

        Catch ex As Exception
        End Try
      Else
        Me.Check_BlotterRedeemWholeHolding.Enabled = False
      End If

      Me.Combo_RedemptionID.Enabled = False
      Me.Combo_Versusinstrument.Enabled = False
      Me.Combo_VersusFuturesInstrument.Enabled = False

      ' Sort out 'Trade vs Instrument' fields...

      Try

        Select Case thisTransactionType

          Case RenaissanceGlobals.TransactionTypes.Redeem
            Me.Combo_RedemptionID.Enabled = True
            Combo_RedemptionID.Visible = True
            Combo_Versusinstrument.Visible = False
            Combo_VersusFuturesInstrument.Visible = False
            Me.Label_RedemptionID.Text = "Redeem vs Specific Subscription"

          Case RenaissanceGlobals.TransactionTypes.Expense_Increase, RenaissanceGlobals.TransactionTypes.Expense_Reduce, RenaissanceGlobals.TransactionTypes.Fee

            Combo_Versusinstrument.Enabled = True
            Combo_Versusinstrument.Visible = True
            Combo_RedemptionID.Visible = False
            Combo_VersusFuturesInstrument.Visible = False
            Me.Label_RedemptionID.Text = "Payment vs Expense"

          Case RenaissanceGlobals.TransactionTypes.Provision_Increase

            Combo_Versusinstrument.Enabled = True
            Combo_Versusinstrument.Visible = True
            Combo_RedemptionID.Visible = False
            Combo_VersusFuturesInstrument.Visible = False
            Me.Label_RedemptionID.Text = "Payment vs Provision"

          Case RenaissanceGlobals.TransactionTypes.Provision_Reduce

            Combo_Versusinstrument.Enabled = True
            Combo_Versusinstrument.Visible = True
            Combo_RedemptionID.Visible = False
            Combo_VersusFuturesInstrument.Visible = False
            Me.Label_RedemptionID.Text = "Payment vs Provision"

          Case RenaissanceGlobals.TransactionTypes.Buy, RenaissanceGlobals.TransactionTypes.Sell

            Dim InstrumentType As RenaissanceGlobals.InstrumentTypes

            If (Me.Combo_TransactionInstrument.SelectedIndex >= 0) AndAlso (CInt(Me.Combo_TransactionInstrument.SelectedValue) > 0) Then
              InstrumentType = LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, CInt(Me.Combo_TransactionInstrument.SelectedValue), "InstrumentType")

              If InstrumentType = InstrumentTypes.Notional Then

                Combo_VersusFuturesInstrument.Enabled = True
                Combo_VersusFuturesInstrument.Visible = True
                Combo_RedemptionID.Visible = False
                Combo_Versusinstrument.Visible = False
                Me.Label_RedemptionID.Text = "Notional vs Instrument"

              Else

                Combo_Versusinstrument.Visible = True
                Combo_RedemptionID.Visible = False
                Combo_VersusFuturesInstrument.Visible = False
                Me.Label_RedemptionID.Text = ""

              End If
            End If

          Case Else

            Combo_Versusinstrument.Visible = True
            Combo_RedemptionID.Visible = False
            Combo_VersusFuturesInstrument.Visible = False
            Me.Label_RedemptionID.Text = ""

        End Select

      Catch Inner_Ex As Exception
      End Try

      If (Check_IsTransfer.Enabled) AndAlso (Check_IsTransfer.Checked) AndAlso (thisTransactionType = RenaissanceGlobals.TransactionTypes.Subscribe) Then

        Date_EffectiveValueDate.Enabled = True
        Edit_EffectivePrice.Enabled = True
        Edit_EffectiveWatermark.Enabled = True
        Check_SpecificInitialEqualisation.Enabled = True
        If (Check_SpecificInitialEqualisation.Checked) Then
          Edit_SpecificInitialEqualisation.Enabled = True
        Else
          Edit_SpecificInitialEqualisation.Enabled = False
        End If

      Else
        Date_EffectiveValueDate.Enabled = False
        Edit_EffectivePrice.Enabled = False
        Edit_EffectiveWatermark.Enabled = False
        Check_SpecificInitialEqualisation.Enabled = False
        Edit_SpecificInitialEqualisation.Enabled = False
      End If

    Else
      Try
        Me.Combo_TransactionFund.Enabled = False
        Me.Combo_SubFund.Enabled = False
        Me.Combo_TransactionInstrument.Enabled = False
        Me.Combo_TransactionType.Enabled = False
        Me.Combo_TradeStatus.Enabled = False
        Me.Combo_TransactionCounterparty.Enabled = False
        Me.Combo_Broker.Enabled = False
        Me.Combo_OpeningOrClosingTrade.Enabled = False
        Me.Combo_BrokerOrderType.Enabled = False
        Me.Combo_TransactionGroup.Enabled = False
        Me.Combo_InvestmentManager.Enabled = False
        Me.Combo_TransactionManager.Enabled = False
        Me.Combo_DataEntryManager.Enabled = False
        Me.Combo_Workflow.Enabled = False
        Me.Date_DecisionDate.Enabled = False
        Me.Date_ValueDate.Enabled = False
        Me.Date_SettlementDate.Enabled = False
        Me.Date_ConfirmationDate.Enabled = False
        Me.Combo_ValueOrAmount.Enabled = False
        Me.Combo_RedemptionID.Enabled = False
        Me.Combo_Versusinstrument.Enabled = False
        Me.Combo_SpecificShareClass.Enabled = False
        Me.Combo_VersusFuturesInstrument.Enabled = False

        Me.edit_Amount.Enabled = False
        Me.edit_Price.Enabled = False
        Me.edit_CleanPrice.Enabled = False
        Me.edit_Principal.Enabled = False
        Me.edit_AccruedInterest.Enabled = False
        Me.edit_BrokerFees.Enabled = False
        Me.edit_Costs.Enabled = False
        Me.edit_CostPercentage.Enabled = False
        Me.edit_settlement.Enabled = False

        Me.Check_FXSettlement.Enabled = False
        Me.Combo_SettlementCurrency.Visible = Me.Check_FXSettlement.Checked
        Me.Combo_SettlementCurrency.Enabled = False
        Me.edit_FXRate.Visible = Me.Check_FXSettlement.Checked
        Me.Button_GetFX.Visible = Me.Check_FXSettlement.Checked
        Me.edit_FXRate.Enabled = False

        Me.Check_CostsAsPercent.Enabled = False
        Me.Check_DontUpdatePrice.Enabled = False
        Me.Check_IsForwardPayment.Enabled = False
        Me.Check_FinalAmount.Enabled = False
        Me.Check_FinalPrice.Enabled = False
        Me.Check_FinalCosts.Enabled = False
        Me.Check_FinalSettlement.Enabled = False
        Me.Check_InstructionGiven.Enabled = False
        Me.Check_BlotterRedeemWholeHolding.Enabled = False
        Me.Check_BankConfirmation.Enabled = False
        Me.Check_InitialDataEntry.Enabled = False
        Me.Check_PriceConfirmation.Enabled = False
        Me.Check_SettlementConfirmed.Enabled = False
        Me.Check_FinalDataEntry.Enabled = False
        Me.editTransactionComment.Enabled = False
        Me.Check_IsTransfer.Enabled = False
        Me.Date_EffectiveValueDate.Enabled = False
        Me.Edit_EffectivePrice.Enabled = False
        Me.Edit_EffectiveWatermark.Enabled = False
        Me.Check_SpecificInitialEqualisation.Enabled = False
        Me.Edit_SpecificInitialEqualisation.Enabled = False

      Catch ex As Exception
      End Try
    End If

  End Sub

  ''' <summary>
  ''' Validates the form.
  ''' </summary>
  ''' <param name="pReturnString">The p return string.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function ValidateForm(ByRef pReturnString As String) As Boolean
    ' Form Validation code.
    ' 
    ' This code should be the final arbiter of what is allowed. no assumptions regarding 
    ' prior validation should be made.
    ' 
    ' This Code is called by the SetFormData routine before position changes.
    '
    Dim RVal As Boolean

    RVal = True
    pReturnString = ""

    ' Validate 

    If Me.Combo_TransactionFund.SelectedIndex < 0 Then
      pReturnString = "Transaction Fund must not be left blank."
      RVal = False
    End If

    If (RVal) AndAlso (Me.Combo_TransactionInstrument.SelectedIndex < 0) Then
      pReturnString = "Transaction Instrument must not be left blank."
      RVal = False
    End If

    If (RVal) AndAlso (Me.Combo_TransactionType.SelectedIndex < 0) Then
      pReturnString = "Transaction Type must not be left blank."
      RVal = False
    End If

    If (RVal) AndAlso (Me.Combo_TradeStatus.SelectedIndex < 0) Then
      pReturnString = "Trade Status must not be left blank."
      RVal = False
    End If

    If (RVal) AndAlso (Me.Combo_TransactionCounterparty.SelectedIndex < 0) Then
      pReturnString = "Counterparty must not be left blank."
      RVal = False
    End If

    'If (RVal) AndAlso (Me.Combo_InvestmentManager.SelectedIndex < 0) Then
    '	pReturnString = "Investment Manager must not be left blank."
    '	RVal = False
    'End If

    'If (RVal) AndAlso (Me.Combo_DataEntryManager.SelectedIndex < 0) Then
    '	pReturnString = "Data Entry Manager must not be left blank."
    '	RVal = False
    'End If

    If (RVal) AndAlso (Me.Combo_ValueOrAmount.SelectedIndex < 0) Then
      pReturnString = "`Value or Number-of-shares` must not be left blank."
      RVal = False
    End If

    If (RVal) AndAlso ((Me.Date_DecisionDate.Value) <= Renaissance_BaseDate) Then
      pReturnString = "Decision Date must not be left blank."
      RVal = False
    End If

    If (RVal) AndAlso (IsDate(Me.Date_ValueDate.Text) = False) Then
      pReturnString = "Trade Date must not be left blank."
      RVal = False
    End If

    If (RVal) AndAlso (IsDate(Me.Date_SettlementDate.Text) = False) Then
      pReturnString = "Settlement Date must not be left blank."
      RVal = False
    End If

    If (RVal) AndAlso ((Me.Date_ConfirmationDate.Value) <= Renaissance_BaseDate) Then
      pReturnString = "Confirmation Date must not be left blank."
      RVal = False
    End If

    If (RVal) AndAlso (Me.Combo_OpeningOrClosingTrade.Visible) AndAlso (Nz(Me.Combo_OpeningOrClosingTrade.SelectedIndex, 0) <= 0) Then
      pReturnString = "Option orders to be sent to market require the 'Opening / Closing trade' field to be set."
      RVal = False
    End If

    ' Do not allow payments vs Expenses where the instrument currencies do not match.
    If (RVal) Then
      If (Me.Combo_Versusinstrument.Visible) AndAlso (Combo_Versusinstrument.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Versusinstrument.SelectedValue)) Then
        If (Combo_TransactionType.SelectedValue IsNot Nothing) AndAlso (IsNumeric(Combo_TransactionType.SelectedValue)) AndAlso ((CInt(Me.Combo_TransactionType.SelectedValue) = RenaissanceGlobals.TransactionTypes.Expense_Increase) OrElse (CInt(Me.Combo_TransactionType.SelectedValue) = RenaissanceGlobals.TransactionTypes.Expense_Reduce) OrElse (CInt(Me.Combo_TransactionType.SelectedValue) = RenaissanceGlobals.TransactionTypes.Provision_Increase) OrElse (CInt(Me.Combo_TransactionType.SelectedValue) = RenaissanceGlobals.TransactionTypes.Provision_Reduce) OrElse (CInt(Me.Combo_TransactionType.SelectedValue) = RenaissanceGlobals.TransactionTypes.Fee)) Then
          If (CInt(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, CInt(Combo_TransactionInstrument.SelectedValue), "InstrumentCurrencyID")) <> CInt(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, CInt(Combo_Versusinstrument.SelectedValue), "InstrumentCurrencyID"))) Then
            pReturnString = "Expense and Payment Instruments must be in the same currency."
            RVal = False
          End If
        End If
      End If
    End If

    Return RVal

  End Function

  ''' <summary>
  ''' Handles the MouseEnter event of the btnCancel control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnCancel_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseEnter
    ' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
    Me.IsOverCancelButton = True
  End Sub

  ''' <summary>
  ''' Handles the MouseLeave event of the btnCancel control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnCancel_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseLeave
    ' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
    Me.IsOverCancelButton = False
  End Sub

  ''' <summary>
  ''' Handles the SelectedIndexChanged event of the Combo_ValueOrAmount control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_ValueOrAmount_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_ValueOrAmount.SelectedIndexChanged

    Try
      If Me.InPaint = False Then
        SetValueFieldEnabled()
      End If
    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Sets the value field enabled.
  ''' </summary>
  Private Sub SetValueFieldEnabled()

    Try
      Dim TransactionType As RenaissanceGlobals.TransactionTypes = TransactionTypes.Buy

      If (Me.Combo_TransactionType.SelectedIndex >= 0) Then
        TransactionType = CType(Combo_TransactionType.SelectedValue, RenaissanceGlobals.TransactionTypes)
      End If

      If (Me.Combo_ValueOrAmount.Text.ToUpper.StartsWith("V")) Then
        Me.edit_Amount.Enabled = False
        Me.edit_Costs.Enabled = True
        Me.edit_settlement.Enabled = True
        Me.Button_GetAmount.Enabled = False
      Else
        Me.edit_Amount.Enabled = True
        Me.edit_Costs.Enabled = True
        Me.edit_settlement.Enabled = False
        Me.Button_GetAmount.Enabled = True
      End If

      If ((TransactionType = TransactionTypes.BuyFX) OrElse (TransactionType = TransactionTypes.SellFX) OrElse (TransactionType = TransactionTypes.BuyFXForward) OrElse (TransactionType = TransactionTypes.SellFXForward)) And (AddNewRecord) Then
        Me.edit_Price.Enabled = False
        Me.edit_Price.Value = 1.0#
        Me.Check_FXSettlement.Checked = True

        If (Combo_SettlementCurrency.SelectedIndex <= 0) Then
          Dim thisFund As RenaissanceDataClass.DSFund.tblFundRow = Nothing

          If (Combo_TransactionFund.SelectedIndex > 0) Then
            thisFund = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, CInt(Combo_TransactionFund.SelectedValue))
          End If

          If thisFund Is Nothing Then
            Combo_SettlementCurrency.SelectedValue = REFERENCE_REPORT_CURRENCY
          Else
            Combo_SettlementCurrency.SelectedValue = thisFund.FundBaseCurrency
          End If
        End If
      End If

    Catch ex As Exception
    End Try
  End Sub

  'Private Sub Combo_Workflow_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_Workflow.SelectedIndexChanged
  '	' *******************************************************************************
  '	'
  '	' *******************************************************************************

  '	Try
  '		If (InPaint = False) Then
  '			If (sender Is Combo_Workflow) Then
  '				Custom_Workflow = True
  '				Combo_Workflow.BackColor = CUSTOM_BACKGROUND_CHANGED

  '				Call SetExpectedWorkflow(AddNewRecord, True)	' Won't set the Workflow (because Custom_Workflow = True), will set the Trade Status.

  '			End If
  '		End If

  '	Catch ex As Exception
  '		Call MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Check_FXSettlement_CheckStateChanged()", ex.StackTrace, True)
  '	End Try

  'End Sub

#End Region

#Region " Navigation Code / GetPosition() (Generic Code) "

  ''' <summary>
  ''' Moves to audit ID.
  ''' </summary>
  ''' <param name="pAuditID">The p audit ID.</param>
  Public Sub MoveToAuditID(ByVal pAuditID As Integer)
    ' ****************************************************************
    ' Subroutine to move the current form focus to a given AuditID
    ' ****************************************************************

    Dim findDataRow As DataRow
    Dim Counter As Integer

    Try
      If (SortedRows Is Nothing) Then Exit Sub
      If (SortedRows.Length <= 0) Then Exit Sub

      For Counter = 0 To (SortedRows.Length - 1)
        findDataRow = SortedRows(Counter)
        If findDataRow("AuditID").Equals(pAuditID) Then
          'THIS_FORM_SelectingCombo.SelectedIndex = Counter
          thisPosition = Counter
          Combo_SelectComboChanged(THIS_FORM_SelectingCombo, New EventArgs)
          Exit For
        End If
      Next
    Catch ex As Exception
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Moving to given AuditID.", ex.StackTrace, True)
    End Try

  End Sub

  ''' <summary>
  ''' Handles the SelectComboChanged event of the Combo control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_SelectComboChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' Selection Combo. SelectedItem changed.
    '

    ' Don't react to changes made in paint routines etc.
    If InPaint = True Then Exit Sub

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    ' Find the correct data row, then show it...
    'thisPosition = THIS_FORM_SelectingCombo.SelectedIndex

    If (thisPosition >= 0) Then
      thisDataRow = Me.SortedRows(Math.Min(thisPosition, Me.SortedRows.Length - 1))
    Else
      thisDataRow = Nothing
    End If

    Call GetFormData(thisDataRow)

  End Sub


  ''' <summary>
  ''' Handles the Click event of the btnNavPrev control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnNavPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavPrev.Click
    ' 'Previous' Button.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    If thisPosition > 0 Then thisPosition -= 1
    If thisPosition >= myTable.Rows.Count Then
      thisPosition = (myTable.Rows.Count - 1)
    End If

    'THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
    Combo_SelectComboChanged(THIS_FORM_SelectingCombo, New EventArgs)

    Exit Sub

    If myTable.Rows.Count > thisPosition Then
      thisDataRow = myTable.Rows(thisPosition)
      Call GetFormData(thisDataRow)
    Else
      Call GetFormData(Nothing)
    End If

  End Sub

  ''' <summary>
  ''' Handles the Click event of the btnNavNext control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnNavNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavNext.Click
    ' 'Next' Button.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    If thisPosition < (myTable.Rows.Count - 1) Then thisPosition += 1
    'If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
    '	thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
    'End If

    'THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
    Combo_SelectComboChanged(THIS_FORM_SelectingCombo, New EventArgs)

    Exit Sub

    If myTable.Rows.Count > thisPosition Then
      thisDataRow = myTable.Rows(thisPosition)
      Call GetFormData(thisDataRow)
    Else
      Call GetFormData(Nothing)
    End If

  End Sub

  ''' <summary>
  ''' Handles the Click event of the btnNavFirst control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnNavFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavFirst.Click
    ' 'First' Button.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    If (thisPosition <> 0) Then
      thisPosition = 0
      'If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
      '	thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
      'End If

      'If (THIS_FORM_SelectingCombo.SelectedIndex = thisPosition) Then
      Combo_SelectComboChanged(THIS_FORM_SelectingCombo, New EventArgs)
      'Else
      '  THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
      'End If

    End If

  End Sub

  ''' <summary>
  ''' Handles the Click event of the btnLast control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLast.Click
    ' 'Last' Button.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    'thisPosition = THIS_FORM_SelectingCombo.Items.Count - 1
    thisPosition = myTable.Rows.Count - 1

    'THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
    Combo_SelectComboChanged(THIS_FORM_SelectingCombo, New EventArgs)

  End Sub

  ''' <summary>
  ''' Get_s the position.
  ''' </summary>
  ''' <param name="pAuditID">The p audit ID.</param>
  ''' <returns>System.Int32.</returns>
  Private Function Get_Position(ByVal pAuditID As Integer) As Integer
    ' Returns the position in the current 'SortedRows' array of the entry corresponding to the given 
    ' AudidID.



    Dim MinIndex As Integer
    Dim MaxIndex As Integer
    Dim CurrentMin As Integer
    Dim CurrentMax As Integer
    Dim searchDataRow As DataRow

    Try
      ' SortedRows exists ?

      If SortedRows Is Nothing Then
        Return (-1)
        Exit Function
      End If

      ' Return (-1) if there are no rows in the 'SortedRows'

      If (SortedRows.GetLength(0) <= 0) Then
        Return (-1)
        Exit Function
      End If


      ' Use a modified Search moving outwards from the last returned value to locate the required value.
      ' Reflecting the fact that for the most part One looks for closely located records.

      MinIndex = 0
      MaxIndex = SortedRows.GetLength(0) - 1


      ' Check First and Last records (Incase 'First' or 'Last' was pressed).

      searchDataRow = SortedRows(MinIndex)

      If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(searchDataRow("AuditID")) = pAuditID) Then
        Return MinIndex
        Exit Function
      End If

      searchDataRow = SortedRows(MaxIndex)
      If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(searchDataRow("AuditID")) = pAuditID) Then
        Return MaxIndex
        Exit Function
      End If

      ' now search outwards from the last returned value.

      If (Me.thisPosition >= MinIndex) And (Me.thisPosition <= MaxIndex) Then
        CurrentMin = thisPosition
        CurrentMax = CurrentMin + 1
      Else
        CurrentMin = CInt((MinIndex + MaxIndex) / 2)
        CurrentMax = CurrentMin + 1
      End If

      While (CurrentMin >= MinIndex) Or (CurrentMax <= MaxIndex)
        If (CurrentMin >= MinIndex) Then
          searchDataRow = SortedRows(CurrentMin)

          If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(searchDataRow("AuditID")) = pAuditID) Then
            Return CurrentMin
            Exit Function
          End If

          CurrentMin -= 1
        End If

        If (CurrentMax <= MaxIndex) Then
          searchDataRow = SortedRows(CurrentMax)

          If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(searchDataRow("AuditID")) = pAuditID) Then
            Return CurrentMax
            Exit Function
          End If

          CurrentMax += 1
        End If

      End While

      Return (-1)
      Exit Function

    Catch ex As Exception
      MainForm.LogError(Me.Name & ", Get_Position()", LOG_LEVELS.Warning, ex.Message, "", ex.StackTrace, True)
      Return (-1)
      Exit Function
    End Try

  End Function

#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

  ''' <summary>
  ''' Handles the Click event of the btnCancel control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    ' Cancel Changes, redisplay form.

    FormChanged = False
    AddNewRecord = False

    'Me.THIS_FORM_SelectingCombo.Enabled = True

    If (thisPosition >= 0) And (thisPosition < Me.SortedRows.GetLength(0)) Then
      thisDataRow = Me.SortedRows(thisPosition)
      Call GetFormData(thisDataRow)
    ElseIf (previousPosition >= 0) And (previousPosition < Me.SortedRows.GetLength(0)) Then
      thisPosition = previousPosition
      thisDataRow = Me.SortedRows(thisPosition)
      Call GetFormData(thisDataRow)
    ElseIf (previousPosition < 0) Then
      thisPosition = previousPosition
      thisDataRow = Nothing
      Call GetFormData(Nothing)
    Else
      Call btnNavFirst_Click(Me, New System.EventArgs)
    End If

  End Sub

  ''' <summary>
  ''' Handles the Click event of the btnSave control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
    ' Save Changes, if any, without prompting.

    If (FormChanged = True) Then
      Call SetFormData(False)
    End If

  End Sub

  ''' <summary>
  ''' Handles the Click event of the btnDelete control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
    Dim Position As Integer
    Dim NextAuditID As Integer

    If (AddNewRecord = True) Then
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Sub
    End If

    ' No Appropriate Save permission :-

    If (Me.HasDeletePermission = False) Then
      MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "You do not have permission to Delete this record.", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Sub
    End If

    ' *************************************************************
    ' KnowledgeDate OK :-
    ' *************************************************************

    If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
      MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "Changes to Venice are not allowed when the Knowledgedate is not set to `NOW`", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Sub
    End If

    ' *************************************************************
    ' Confirm :-
    ' *************************************************************
    If MsgBox("Are you Sure ?", MsgBoxStyle.OkCancel, "Confirm Delete") = MsgBoxResult.Cancel Then
      Exit Sub
    End If

    ' Check Data position.

    Position = Get_Position(thisAuditID)

    If Position < 0 Then Exit Sub

    ' Check Referential Integrity 
    If CheckReferentialIntegrity(_MainForm, Me.SortedRows(Position)) = False Then
      MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "Deleting this record would voilate referential integrity.", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Sub
    End If

    ' Resolve row to show after deleting this one.

    NextAuditID = (-1)
    If (Position + 1) < Me.SortedRows.GetLength(0) Then
      NextAuditID = CInt(Me.SortedRows(Position + 1)("AuditID"))
    ElseIf (Position - 1) >= 0 Then
      NextAuditID = CInt(Me.SortedRows(Position - 1)("AuditID"))
    Else
      NextAuditID = (-1)
    End If

    ' Delete this row

    Try
      Me.SortedRows(Position).Delete()

      MainForm.AdaptorUpdate(Me.Name, myAdaptor, myTable)

    Catch ex As Exception

      Call MainForm.LogError(Me.Name & ", Delete()", LOG_LEVELS.Error, ex.Message, "Error Deleting Record.", ex.StackTrace, True)
    End Try

    ' Tidy Up.

    FormChanged = False

    thisAuditID = NextAuditID
    Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID), True)

  End Sub

  ''' <summary>
  ''' Handles the Click event of the btnAdd control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
    ' Prepare form to Add a new record.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    thisPosition = -1
    InPaint = True
    'MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
    InPaint = False

    GetFormData(Nothing)
    AddNewRecord = True

    Me.btnCancel.Enabled = True
    'Me.THIS_FORM_SelectingCombo.Enabled = False

    ' Call SetButtonStatus(Nothing) ' Not necessary, done in GetFormData(Nothing)

    THIS_FORM_NewMoveToControl.Focus()

  End Sub

  ''' <summary>
  ''' Handles the Click event of the btnNewUsing control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnNewUsing_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewUsing.Click
    ' *******************************************************************************
    ' Prepare form to Add a new record using the existing details
    ' *******************************************************************************

    Try
      If (FormChanged = True) Then
        Call SetFormData()
      End If
    Catch ex As Exception
      Call MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in btnNewUsing_Click()", ex.StackTrace, True)
    End Try

    Try
      InPaint = True
      'MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
      Me.editAuditID.Text = "0"

      AddNewRecord = True
      FormChanged = True
      thisPosition = -1
      thisAuditID = (-1)
      Me.Label_TransactionAddOrUpdate.Text = "Adding a NEW trade."
      Me.Label_TransactionStatusLabel.Text = "Adding a NEW trade."

      Me.Date_DecisionDate.Value = Now.Date

      If (Me.Combo_TransactionInstrument.SelectedIndex >= 0) AndAlso (CInt(Me.Combo_TransactionInstrument.SelectedValue) > 0) Then
        Dim thisInstrument As RenaissanceDataClass.DSInstrument.tblInstrumentRow
        Dim TransactionType As RenaissanceGlobals.TransactionTypes

        thisInstrument = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, CInt(Me.Combo_TransactionInstrument.SelectedValue)), RenaissanceDataClass.DSInstrument.tblInstrumentRow)

        If (thisInstrument IsNot Nothing) Then
          Label_InstrumentCurrency.Text = thisInstrument.InstrumentCurrency '  Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, CInt(Me.Combo_TransactionInstrument.SelectedValue), "InstrumentCurrency"), "")

          If (thisInstrument.InstrumentDealingCutOffTime > 0) AndAlso (thisInstrument.InstrumentDealingCutOffTime < CInt(Now.TimeOfDay.TotalSeconds)) Then
            ' After Cutoff
            Date_ValueDate.Value = AddPeriodToDate(DealingPeriod.Daily, Now.Date, 1)
          Else
            Date_ValueDate.Value = Now.Date
          End If

          TransactionType = CType(Nz(Me.Combo_TransactionType.SelectedValue, TransactionTypes.Buy), RenaissanceGlobals.TransactionTypes)

          If (TransactionType = TransactionTypes.Sell) Then
            Date_SettlementDate.Value = AddPeriodToDate(thisInstrument.InstrumentDealingPeriod, Date_ValueDate.Value, thisInstrument.InstrumentDefaultSettlementDays_Sell)
          Else
            Date_SettlementDate.Value = AddPeriodToDate(thisInstrument.InstrumentDealingPeriod, Date_ValueDate.Value, thisInstrument.InstrumentDefaultSettlementDays_Buy)
          End If

        End If

      Else
        Label_InstrumentCurrency.Text = ""
      End If

      Combo_TradeStatus.SelectedValue = 0
      Me.Check_DontUpdatePrice.CheckState = CheckState.Checked
      Me.Combo_OpeningOrClosingTrade.SelectedIndex = 0

      Call SetExpectedWorkflow(AddNewRecord, True)
    Catch ex As Exception
      Call MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in btnNewUsing_Click()", ex.StackTrace, True)
    Finally
      InPaint = False

      Call FormControlChanged(Combo_TransactionInstrument, Nothing) ' Use FormControlChanged to Update Settlement Dates  

      If (Check_FXSettlement.Checked) Then
        ' Update FX Rate, if appropriate.
        Call Button_GetFX_Click(Button_GetFX, Nothing)
      End If

    End Try

    Call SetButtonStatus(Nothing)
    Call SetFormatting()

    THIS_FORM_NewMoveToControl.Focus()

  End Sub

  ''' <summary>
  ''' Handles the Click event of the btnClose control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
    ' Close Form

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    Me.Close()

  End Sub

#End Region

#Region " Bug hunting "

  ' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
  ' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
  ' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

  ''' <summary>
  ''' Called when [row updating].
  ''' </summary>
  ''' <param name="Sender">The sender.</param>
  ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  ''' <summary>
  ''' Called when [row updated].
  ''' </summary>
  ''' <param name="Sender">The sender.</param>
  ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  ''' <summary>
  ''' Called when [row fill error].
  ''' </summary>
  ''' <param name="Sender">The sender.</param>
  ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
    Dim a As Integer

    a = 1
  End Sub

#End Region

#Region " Transaction report Menu"

  ''' <summary>
  ''' Sets the transaction report menu.
  ''' </summary>
  ''' <param name="RootMenu">The root menu.</param>
  ''' <returns>MenuStrip.</returns>
  Private Function SetTransactionReportMenu(ByRef RootMenu As MenuStrip) As MenuStrip

    Dim ReportMenu As New ToolStripMenuItem("Transaction &Reports")
    Dim newMenuItem As ToolStripMenuItem

    newMenuItem = ReportMenu.DropDownItems.Add("Transaction &Ticket", Nothing, AddressOf Me.rptTransactionTicket)

    ReportMenu.DropDownItems.Add(New ToolStripSeparator)

    newMenuItem = ReportMenu.DropDownItems.Add("Transaction By &Value Date", Nothing, AddressOf Me.rptTransactionByDate)
    newMenuItem = ReportMenu.DropDownItems.Add("Transaction By &Entry Date", Nothing, AddressOf Me.rptTransactionByEntryDate)
    newMenuItem = ReportMenu.DropDownItems.Add("Transaction By &Group", Nothing, AddressOf Me.rptTransactionByGroup)
    newMenuItem = ReportMenu.DropDownItems.Add("Transaction By &Instrument", Nothing, AddressOf Me.rptTransactionByStock)

    RootMenu.Items.Add(ReportMenu)
    Return RootMenu

  End Function



  ''' <summary>
  ''' RPTs the transaction ticket.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
  Private Sub rptTransactionTicket(ByVal sender As Object, ByVal e As EventArgs)
    Dim rptDataset As RenaissanceDataClass.DSSelectTransaction
    Dim rptDataRow As DataRow
    Dim myDataView As DataView

    rptDataset = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblSelectTransaction, False)

    Try
      If thisPosition >= 0 Then
        rptDataRow = Me.SortedRows(thisPosition)
        myDataView = New DataView(rptDataset.tblSelectTransaction, "RN=" & CStr(rptDataRow("RN")), "", DataViewRowState.CurrentRows)

        MainForm.MainReportHandler.DisplayReport(0, "rptTransactionTicket", myDataView, Form_ProgressBar)
      End If
    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' RPTs the transaction by date.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
  Private Sub rptTransactionByDate(ByVal sender As Object, ByVal e As EventArgs)
    Dim rptDataset As RenaissanceDataClass.DSSelectTransaction
    Dim rptDataRow As DataRow
    Dim myDataView As DataView

    rptDataset = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblSelectTransaction, False)

    Try
      If thisPosition >= 0 Then
        rptDataRow = Me.SortedRows(thisPosition)
        myDataView = New DataView(rptDataset.tblSelectTransaction, "RN=" & CStr(rptDataRow("RN")), "", DataViewRowState.CurrentRows)

        MainForm.MainReportHandler.DisplayReport(0, "rptTransactionByDate", myDataView, Form_ProgressBar)
      End If
    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' RPTs the transaction by entry date.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
  Private Sub rptTransactionByEntryDate(ByVal sender As Object, ByVal e As EventArgs)
    Dim rptDataset As RenaissanceDataClass.DSSelectTransaction
    Dim rptDataRow As DataRow
    Dim myDataView As DataView

    rptDataset = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblSelectTransaction, False)

    Try
      If thisPosition >= 0 Then
        rptDataRow = Me.SortedRows(thisPosition)
        myDataView = New DataView(rptDataset.tblSelectTransaction, "RN=" & CStr(rptDataRow("RN")), "", DataViewRowState.CurrentRows)

        MainForm.MainReportHandler.DisplayReport(0, "rptTransactionByEntryDate", myDataView, Form_ProgressBar)
      End If
    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' RPTs the transaction by group.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
  Private Sub rptTransactionByGroup(ByVal sender As Object, ByVal e As EventArgs)
    Dim rptDataset As RenaissanceDataClass.DSSelectTransaction
    Dim rptDataRow As DataRow
    Dim myDataView As DataView

    rptDataset = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblSelectTransaction, False)

    Try
      If thisPosition >= 0 Then
        rptDataRow = Me.SortedRows(thisPosition)
        myDataView = New DataView(rptDataset.tblSelectTransaction, "RN=" & CStr(rptDataRow("RN")), "", DataViewRowState.CurrentRows)

        MainForm.MainReportHandler.DisplayReport(0, "rptTransactionByGroup", myDataView, Form_ProgressBar)
      End If
    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' RPTs the transaction by stock.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
  Private Sub rptTransactionByStock(ByVal sender As Object, ByVal e As EventArgs)
    Dim rptDataset As RenaissanceDataClass.DSSelectTransaction
    Dim rptDataRow As DataRow
    Dim myDataView As DataView

    rptDataset = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblSelectTransaction, False)

    Try
      If thisPosition >= 0 Then
        rptDataRow = Me.SortedRows(thisPosition)
        myDataView = New DataView(rptDataset.tblSelectTransaction, "RN=" & CStr(rptDataRow("RN")), "", DataViewRowState.CurrentRows)

        MainForm.MainReportHandler.DisplayReport(0, "rptTransactionByStock", myDataView, Form_ProgressBar)
      End If
    Catch ex As Exception
    End Try

  End Sub

#End Region




  Private Sub Radio_BondByPrice_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_BondByPrice.CheckedChanged, Radio_BondByPrincipal.CheckedChanged
    ' *******************************************************************************
    ' *******************************************************************************

    Try

      If Radio_BondByPrice.Visible Then
        If Radio_BondByPrice.Checked Then
          edit_Principal.Visible = False
          Label_Principal.Visible = edit_Principal.Visible

          edit_Price.Enabled = True
          Label_CleanPrice.Visible = True
          edit_CleanPrice.Visible = True
        Else
          edit_Principal.Visible = True
          Label_Principal.Visible = edit_Principal.Visible

          edit_Price.Enabled = False
          Label_CleanPrice.Visible = False
          edit_CleanPrice.Visible = False
        End If
      End If

    Catch ex As Exception
    End Try
  End Sub


End Class
