' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmPrice.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals

Imports RenaissanceUtilities.DatePeriodFunctions

''' <summary>
''' Class frmPrice
''' </summary>
Public Class frmPrice

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmPrice"/> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The LBL price code
    ''' </summary>
  Friend WithEvents lblPriceCode As System.Windows.Forms.Label
    ''' <summary>
    ''' The LBL price description
    ''' </summary>
  Friend WithEvents lblPriceDescription As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit audit ID
    ''' </summary>
  Friend WithEvents editAuditID As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The BTN nav first
    ''' </summary>
  Friend WithEvents btnNavFirst As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN nav prev
    ''' </summary>
  Friend WithEvents btnNavPrev As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN nav next
    ''' </summary>
  Friend WithEvents btnNavNext As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN last
    ''' </summary>
  Friend WithEvents btnLast As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN add
    ''' </summary>
  Friend WithEvents btnAdd As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN delete
    ''' </summary>
  Friend WithEvents btnDelete As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN cancel
    ''' </summary>
  Friend WithEvents btnCancel As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN save
    ''' </summary>
  Friend WithEvents btnSave As System.Windows.Forms.Button
    ''' <summary>
    ''' The panel1
    ''' </summary>
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The label1
    ''' </summary>
  Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The group box1
    ''' </summary>
  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The BTN close
    ''' </summary>
  Friend WithEvents btnClose As System.Windows.Forms.Button
    ''' <summary>
    ''' The label2
    ''' </summary>
  Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label3
    ''' </summary>
  Friend WithEvents Label3 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label4
    ''' </summary>
  Friend WithEvents Label4 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ price instrument
    ''' </summary>
  Friend WithEvents Combo_PriceInstrument As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select price instrument
    ''' </summary>
  Friend WithEvents Combo_SelectPriceInstrument As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select price date
    ''' </summary>
  Friend WithEvents Combo_SelectPriceDate As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The date_ price date
    ''' </summary>
  Friend WithEvents Date_PriceDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The group box2
    ''' </summary>
  Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The check_ percent increase
    ''' </summary>
  Friend WithEvents Check_PercentIncrease As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The label_percent increase
    ''' </summary>
  Friend WithEvents Label_percentIncrease As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ price percent
    ''' </summary>
  Friend WithEvents edit_PricePercent As RenaissanceControls.PercentageTextBox
    ''' <summary>
    ''' The label_pricemocepercent
    ''' </summary>
  Friend WithEvents label_pricemocepercent As System.Windows.Forms.Label
    ''' <summary>
    ''' The label5
    ''' </summary>
  Friend WithEvents Label5 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ base price
    ''' </summary>
  Friend WithEvents edit_BasePrice As C1.Win.C1Input.C1TextBox
    ''' <summary>
    ''' The label6
    ''' </summary>
  Friend WithEvents Label6 As System.Windows.Forms.Label
    ''' <summary>
    ''' The group box3
    ''' </summary>
  Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The check_ is final price
    ''' </summary>
  Friend WithEvents Check_IsFinalPrice As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The label7
    ''' </summary>
  Friend WithEvents Label7 As System.Windows.Forms.Label
    ''' <summary>
    ''' The check_ is administrator price
    ''' </summary>
  Friend WithEvents Check_IsAdministratorPrice As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The label8
    ''' </summary>
  Friend WithEvents Label8 As System.Windows.Forms.Label
    ''' <summary>
    ''' The check_ is milestone price
    ''' </summary>
  Friend WithEvents Check_IsMilestonePrice As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The label9
    ''' </summary>
  Friend WithEvents Label9 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ price NAV
    ''' </summary>
	Friend WithEvents edit_PriceNAV As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The root menu
    ''' </summary>
	Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
    ''' <summary>
    ''' The label10
    ''' </summary>
	Friend WithEvents Label10 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ adjusted price
    ''' </summary>
	Friend WithEvents Label_AdjustedPrice As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ multiplier
    ''' </summary>
	Friend WithEvents edit_Multiplier As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The label11
    ''' </summary>
	Friend WithEvents Label11 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label12
    ''' </summary>
	Friend WithEvents Label12 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ price reference date
    ''' </summary>
	Friend WithEvents Combo_PriceReferenceDate As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Me.lblPriceCode = New System.Windows.Forms.Label
		Me.lblPriceDescription = New System.Windows.Forms.Label
		Me.editAuditID = New System.Windows.Forms.TextBox
		Me.btnNavFirst = New System.Windows.Forms.Button
		Me.btnNavPrev = New System.Windows.Forms.Button
		Me.btnNavNext = New System.Windows.Forms.Button
		Me.btnLast = New System.Windows.Forms.Button
		Me.btnAdd = New System.Windows.Forms.Button
		Me.btnDelete = New System.Windows.Forms.Button
		Me.btnCancel = New System.Windows.Forms.Button
		Me.btnSave = New System.Windows.Forms.Button
		Me.Combo_SelectPriceInstrument = New System.Windows.Forms.ComboBox
		Me.Panel1 = New System.Windows.Forms.Panel
		Me.Label1 = New System.Windows.Forms.Label
		Me.GroupBox1 = New System.Windows.Forms.GroupBox
		Me.btnClose = New System.Windows.Forms.Button
		Me.Combo_SelectPriceDate = New System.Windows.Forms.ComboBox
		Me.Label2 = New System.Windows.Forms.Label
		Me.Label3 = New System.Windows.Forms.Label
		Me.Label4 = New System.Windows.Forms.Label
		Me.Combo_PriceInstrument = New System.Windows.Forms.ComboBox
		Me.Date_PriceDate = New System.Windows.Forms.DateTimePicker
		Me.GroupBox2 = New System.Windows.Forms.GroupBox
		Me.Check_PercentIncrease = New System.Windows.Forms.CheckBox
		Me.Label_percentIncrease = New System.Windows.Forms.Label
		Me.edit_PricePercent = New RenaissanceControls.PercentageTextBox
		Me.label_pricemocepercent = New System.Windows.Forms.Label
		Me.Label5 = New System.Windows.Forms.Label
		Me.edit_BasePrice = New C1.Win.C1Input.C1TextBox
		Me.Label6 = New System.Windows.Forms.Label
		Me.GroupBox3 = New System.Windows.Forms.GroupBox
		Me.Check_IsFinalPrice = New System.Windows.Forms.CheckBox
		Me.Label7 = New System.Windows.Forms.Label
		Me.Check_IsAdministratorPrice = New System.Windows.Forms.CheckBox
		Me.Label8 = New System.Windows.Forms.Label
		Me.Check_IsMilestonePrice = New System.Windows.Forms.CheckBox
		Me.Label9 = New System.Windows.Forms.Label
		Me.edit_PriceNAV = New RenaissanceControls.NumericTextBox
		Me.Combo_PriceReferenceDate = New System.Windows.Forms.ComboBox
		Me.RootMenu = New System.Windows.Forms.MenuStrip
		Me.Label10 = New System.Windows.Forms.Label
		Me.Label_AdjustedPrice = New System.Windows.Forms.Label
		Me.edit_Multiplier = New RenaissanceControls.NumericTextBox
		Me.Label11 = New System.Windows.Forms.Label
		Me.Label12 = New System.Windows.Forms.Label
		Me.Panel1.SuspendLayout()
		CType(Me.edit_BasePrice, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.SuspendLayout()
		'
		'lblPriceCode
		'
		Me.lblPriceCode.Location = New System.Drawing.Point(8, 100)
		Me.lblPriceCode.Name = "lblPriceCode"
		Me.lblPriceCode.Size = New System.Drawing.Size(100, 20)
		Me.lblPriceCode.TabIndex = 24
		Me.lblPriceCode.Text = "Instrument"
		'
		'lblPriceDescription
		'
		Me.lblPriceDescription.Location = New System.Drawing.Point(8, 128)
		Me.lblPriceDescription.Name = "lblPriceDescription"
		Me.lblPriceDescription.Size = New System.Drawing.Size(100, 20)
		Me.lblPriceDescription.TabIndex = 25
		Me.lblPriceDescription.Text = "Price Date"
		'
		'editAuditID
		'
		Me.editAuditID.Enabled = False
		Me.editAuditID.Location = New System.Drawing.Point(324, 32)
		Me.editAuditID.Name = "editAuditID"
		Me.editAuditID.Size = New System.Drawing.Size(48, 20)
		Me.editAuditID.TabIndex = 1
		'
		'btnNavFirst
		'
		Me.btnNavFirst.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnNavFirst.Location = New System.Drawing.Point(8, 8)
		Me.btnNavFirst.Name = "btnNavFirst"
		Me.btnNavFirst.Size = New System.Drawing.Size(40, 28)
		Me.btnNavFirst.TabIndex = 0
		Me.btnNavFirst.Text = "<<"
		'
		'btnNavPrev
		'
		Me.btnNavPrev.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnNavPrev.Location = New System.Drawing.Point(49, 8)
		Me.btnNavPrev.Name = "btnNavPrev"
		Me.btnNavPrev.Size = New System.Drawing.Size(35, 28)
		Me.btnNavPrev.TabIndex = 1
		Me.btnNavPrev.Text = "<"
		'
		'btnNavNext
		'
		Me.btnNavNext.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnNavNext.Location = New System.Drawing.Point(88, 8)
		Me.btnNavNext.Name = "btnNavNext"
		Me.btnNavNext.Size = New System.Drawing.Size(35, 28)
		Me.btnNavNext.TabIndex = 2
		Me.btnNavNext.Text = ">"
		'
		'btnLast
		'
		Me.btnLast.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnLast.Location = New System.Drawing.Point(124, 8)
		Me.btnLast.Name = "btnLast"
		Me.btnLast.Size = New System.Drawing.Size(40, 28)
		Me.btnLast.TabIndex = 3
		Me.btnLast.Text = ">>"
		'
		'btnAdd
		'
		Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnAdd.Location = New System.Drawing.Point(176, 8)
		Me.btnAdd.Name = "btnAdd"
		Me.btnAdd.Size = New System.Drawing.Size(75, 28)
		Me.btnAdd.TabIndex = 4
		Me.btnAdd.Text = "&New"
		'
		'btnDelete
		'
		Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnDelete.Location = New System.Drawing.Point(111, 463)
		Me.btnDelete.Name = "btnDelete"
		Me.btnDelete.Size = New System.Drawing.Size(75, 28)
		Me.btnDelete.TabIndex = 17
		Me.btnDelete.Text = "&Delete"
		'
		'btnCancel
		'
		Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
		Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnCancel.Location = New System.Drawing.Point(195, 463)
		Me.btnCancel.Name = "btnCancel"
		Me.btnCancel.Size = New System.Drawing.Size(75, 28)
		Me.btnCancel.TabIndex = 18
		Me.btnCancel.Text = "&Cancel"
		'
		'btnSave
		'
		Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnSave.Location = New System.Drawing.Point(23, 463)
		Me.btnSave.Name = "btnSave"
		Me.btnSave.Size = New System.Drawing.Size(75, 28)
		Me.btnSave.TabIndex = 16
		Me.btnSave.Text = "&Save"
		'
		'Combo_SelectPriceInstrument
		'
		Me.Combo_SelectPriceInstrument.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
		Me.Combo_SelectPriceInstrument.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_SelectPriceInstrument.Location = New System.Drawing.Point(120, 32)
		Me.Combo_SelectPriceInstrument.Name = "Combo_SelectPriceInstrument"
		Me.Combo_SelectPriceInstrument.Size = New System.Drawing.Size(192, 21)
		Me.Combo_SelectPriceInstrument.TabIndex = 0
		'
		'Panel1
		'
		Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Panel1.Controls.Add(Me.btnNavFirst)
		Me.Panel1.Controls.Add(Me.btnNavPrev)
		Me.Panel1.Controls.Add(Me.btnNavNext)
		Me.Panel1.Controls.Add(Me.btnLast)
		Me.Panel1.Controls.Add(Me.btnAdd)
		Me.Panel1.Location = New System.Drawing.Point(59, 407)
		Me.Panel1.Name = "Panel1"
		Me.Panel1.Size = New System.Drawing.Size(260, 48)
		Me.Panel1.TabIndex = 15
		'
		'Label1
		'
		Me.Label1.Location = New System.Drawing.Point(8, 36)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(40, 16)
		Me.Label1.TabIndex = 20
		Me.Label1.Text = "Select"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		'
		'GroupBox1
		'
		Me.GroupBox1.Location = New System.Drawing.Point(8, 88)
		Me.GroupBox1.Name = "GroupBox1"
		Me.GroupBox1.Size = New System.Drawing.Size(364, 4)
		Me.GroupBox1.TabIndex = 23
		Me.GroupBox1.TabStop = False
		Me.GroupBox1.Text = "GroupBox1"
		'
		'btnClose
		'
		Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnClose.Location = New System.Drawing.Point(279, 463)
		Me.btnClose.Name = "btnClose"
		Me.btnClose.Size = New System.Drawing.Size(75, 28)
		Me.btnClose.TabIndex = 19
		Me.btnClose.Text = "&Close"
		'
		'Combo_SelectPriceDate
		'
		Me.Combo_SelectPriceDate.Location = New System.Drawing.Point(120, 60)
		Me.Combo_SelectPriceDate.Name = "Combo_SelectPriceDate"
		Me.Combo_SelectPriceDate.Size = New System.Drawing.Size(192, 21)
		Me.Combo_SelectPriceDate.TabIndex = 2
		'
		'Label2
		'
		Me.Label2.Location = New System.Drawing.Point(56, 36)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(60, 16)
		Me.Label2.TabIndex = 21
		Me.Label2.Text = "Instrument"
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'Label3
		'
		Me.Label3.Location = New System.Drawing.Point(56, 60)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(60, 16)
		Me.Label3.TabIndex = 22
		Me.Label3.Text = "Date"
		Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'Label4
		'
		Me.Label4.Location = New System.Drawing.Point(8, 328)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(100, 20)
		Me.Label4.TabIndex = 34
		Me.Label4.Text = "Price (NAV)"
		'
		'Combo_PriceInstrument
		'
		Me.Combo_PriceInstrument.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_PriceInstrument.Location = New System.Drawing.Point(120, 96)
		Me.Combo_PriceInstrument.Name = "Combo_PriceInstrument"
		Me.Combo_PriceInstrument.Size = New System.Drawing.Size(252, 21)
		Me.Combo_PriceInstrument.TabIndex = 3
		'
		'Date_PriceDate
		'
		Me.Date_PriceDate.Location = New System.Drawing.Point(120, 124)
		Me.Date_PriceDate.Name = "Date_PriceDate"
		Me.Date_PriceDate.Size = New System.Drawing.Size(252, 20)
		Me.Date_PriceDate.TabIndex = 4
		'
		'GroupBox2
		'
		Me.GroupBox2.Location = New System.Drawing.Point(8, 208)
		Me.GroupBox2.Name = "GroupBox2"
		Me.GroupBox2.Size = New System.Drawing.Size(364, 4)
		Me.GroupBox2.TabIndex = 28
		Me.GroupBox2.TabStop = False
		Me.GroupBox2.Text = "GroupBox2"
		'
		'Check_PercentIncrease
		'
		Me.Check_PercentIncrease.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Check_PercentIncrease.Location = New System.Drawing.Point(120, 216)
		Me.Check_PercentIncrease.Name = "Check_PercentIncrease"
		Me.Check_PercentIncrease.Size = New System.Drawing.Size(16, 24)
		Me.Check_PercentIncrease.TabIndex = 8
		'
		'Label_percentIncrease
		'
		Me.Label_percentIncrease.Location = New System.Drawing.Point(8, 220)
		Me.Label_percentIncrease.Name = "Label_percentIncrease"
		Me.Label_percentIncrease.Size = New System.Drawing.Size(108, 16)
		Me.Label_percentIncrease.TabIndex = 29
		Me.Label_percentIncrease.Text = "Percent Increase"
		'
		'edit_PricePercent
		'
		Me.edit_PricePercent.Location = New System.Drawing.Point(120, 240)
		Me.edit_PricePercent.MaxLength = 100
		Me.edit_PricePercent.Name = "edit_PricePercent"
		Me.edit_PricePercent.RenaissanceTag = Nothing
		Me.edit_PricePercent.Size = New System.Drawing.Size(92, 20)
		Me.edit_PricePercent.TabIndex = 9
		Me.edit_PricePercent.Text = "0.00%"
		Me.edit_PricePercent.TextFormat = "#,##0.00##%"
		Me.edit_PricePercent.Value = 0
		Me.edit_PricePercent.WordWrap = False
		'
		'label_pricemocepercent
		'
		Me.label_pricemocepercent.Location = New System.Drawing.Point(8, 244)
		Me.label_pricemocepercent.Name = "label_pricemocepercent"
		Me.label_pricemocepercent.Size = New System.Drawing.Size(112, 20)
		Me.label_pricemocepercent.TabIndex = 30
		Me.label_pricemocepercent.Text = "Price move (%)"
		'
		'Label5
		'
		Me.Label5.Location = New System.Drawing.Point(8, 268)
		Me.Label5.Name = "Label5"
		Me.Label5.Size = New System.Drawing.Size(100, 20)
		Me.Label5.TabIndex = 31
		Me.Label5.Text = "Reference Date"
		'
		'edit_BasePrice
		'
		Me.edit_BasePrice.AutoSize = False
		Me.edit_BasePrice.DataType = GetType(Double)
		Me.edit_BasePrice.DisableOnNoData = False
		Me.edit_BasePrice.DisplayFormat.CustomFormat = "###,###,##0.0000"
		Me.edit_BasePrice.DisplayFormat.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat
		Me.edit_BasePrice.DisplayFormat.Inherit = CType((((C1.Win.C1Input.FormatInfoInheritFlags.NullText Or C1.Win.C1Input.FormatInfoInheritFlags.EmptyAsNull) _
								Or C1.Win.C1Input.FormatInfoInheritFlags.TrimStart) _
								Or C1.Win.C1Input.FormatInfoInheritFlags.TrimEnd), C1.Win.C1Input.FormatInfoInheritFlags)
		Me.edit_BasePrice.ErrorInfo.ErrorAction = C1.Win.C1Input.ErrorActionEnum.ResetValue
		Me.edit_BasePrice.ErrorInfo.ErrorMessage = "Validation Error"
		Me.edit_BasePrice.ErrorInfo.ShowErrorMessage = False
		Me.edit_BasePrice.FormatType = C1.Win.C1Input.FormatTypeEnum.GeneralNumber
		Me.edit_BasePrice.Location = New System.Drawing.Point(120, 288)
		Me.edit_BasePrice.MaxLength = 100
		Me.edit_BasePrice.Name = "edit_BasePrice"
		Me.edit_BasePrice.ShowContextMenu = False
		Me.edit_BasePrice.Size = New System.Drawing.Size(92, 20)
		Me.edit_BasePrice.TabIndex = 11
		Me.edit_BasePrice.Tag = Nothing
		Me.edit_BasePrice.WordWrap = False
		'
		'Label6
		'
		Me.Label6.Location = New System.Drawing.Point(8, 292)
		Me.Label6.Name = "Label6"
		Me.Label6.Size = New System.Drawing.Size(100, 20)
		Me.Label6.TabIndex = 32
		Me.Label6.Text = "Base Price"
		'
		'GroupBox3
		'
		Me.GroupBox3.Location = New System.Drawing.Point(8, 316)
		Me.GroupBox3.Name = "GroupBox3"
		Me.GroupBox3.Size = New System.Drawing.Size(364, 4)
		Me.GroupBox3.TabIndex = 33
		Me.GroupBox3.TabStop = False
		Me.GroupBox3.Text = "GroupBox3"
		'
		'Check_IsFinalPrice
		'
		Me.Check_IsFinalPrice.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Check_IsFinalPrice.Location = New System.Drawing.Point(120, 152)
		Me.Check_IsFinalPrice.Name = "Check_IsFinalPrice"
		Me.Check_IsFinalPrice.Size = New System.Drawing.Size(16, 24)
		Me.Check_IsFinalPrice.TabIndex = 5
		'
		'Label7
		'
		Me.Label7.Location = New System.Drawing.Point(8, 156)
		Me.Label7.Name = "Label7"
		Me.Label7.Size = New System.Drawing.Size(108, 16)
		Me.Label7.TabIndex = 26
		Me.Label7.Text = "Final Price"
		'
		'Check_IsAdministratorPrice
		'
		Me.Check_IsAdministratorPrice.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Check_IsAdministratorPrice.Location = New System.Drawing.Point(120, 176)
		Me.Check_IsAdministratorPrice.Name = "Check_IsAdministratorPrice"
		Me.Check_IsAdministratorPrice.Size = New System.Drawing.Size(16, 24)
		Me.Check_IsAdministratorPrice.TabIndex = 7
		'
		'Label8
		'
		Me.Label8.Location = New System.Drawing.Point(8, 176)
		Me.Label8.Name = "Label8"
		Me.Label8.Size = New System.Drawing.Size(108, 28)
		Me.Label8.TabIndex = 27
		Me.Label8.Text = "Administrator Sourced Price"
		'
		'Check_IsMilestonePrice
		'
		Me.Check_IsMilestonePrice.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Check_IsMilestonePrice.Location = New System.Drawing.Point(288, 152)
		Me.Check_IsMilestonePrice.Name = "Check_IsMilestonePrice"
		Me.Check_IsMilestonePrice.Size = New System.Drawing.Size(16, 24)
		Me.Check_IsMilestonePrice.TabIndex = 6
		'
		'Label9
		'
		Me.Label9.Location = New System.Drawing.Point(176, 156)
		Me.Label9.Name = "Label9"
		Me.Label9.Size = New System.Drawing.Size(108, 16)
		Me.Label9.TabIndex = 35
		Me.Label9.Text = "Milestone Price"
		'
		'edit_PriceNAV
		'
		Me.edit_PriceNAV.Location = New System.Drawing.Point(120, 324)
		Me.edit_PriceNAV.Name = "edit_PriceNAV"
		Me.edit_PriceNAV.RenaissanceTag = Nothing
		Me.edit_PriceNAV.Size = New System.Drawing.Size(92, 20)
		Me.edit_PriceNAV.TabIndex = 12
		Me.edit_PriceNAV.Text = "0.00"
		Me.edit_PriceNAV.TextFormat = "#,##0.00##"
		Me.edit_PriceNAV.Value = 0
		'
		'Combo_PriceReferenceDate
		'
		Me.Combo_PriceReferenceDate.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_PriceReferenceDate.Location = New System.Drawing.Point(120, 264)
		Me.Combo_PriceReferenceDate.Name = "Combo_PriceReferenceDate"
		Me.Combo_PriceReferenceDate.Size = New System.Drawing.Size(192, 21)
		Me.Combo_PriceReferenceDate.TabIndex = 10
		'
		'RootMenu
		'
		Me.RootMenu.Location = New System.Drawing.Point(0, 0)
		Me.RootMenu.Name = "RootMenu"
		Me.RootMenu.Size = New System.Drawing.Size(378, 24)
		Me.RootMenu.TabIndex = 79
		Me.RootMenu.Text = " "
		'
		'Label10
		'
		Me.Label10.Location = New System.Drawing.Point(8, 380)
		Me.Label10.Name = "Label10"
		Me.Label10.Size = New System.Drawing.Size(100, 20)
		Me.Label10.TabIndex = 81
		Me.Label10.Text = "Adjusted Price"
		'
		'Label_AdjustedPrice
		'
		Me.Label_AdjustedPrice.BackColor = System.Drawing.SystemColors.ControlLightLight
		Me.Label_AdjustedPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Label_AdjustedPrice.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label_AdjustedPrice.Location = New System.Drawing.Point(120, 376)
		Me.Label_AdjustedPrice.Name = "Label_AdjustedPrice"
		Me.Label_AdjustedPrice.Size = New System.Drawing.Size(192, 20)
		Me.Label_AdjustedPrice.TabIndex = 14
		Me.Label_AdjustedPrice.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		'
		'edit_Multiplier
		'
		Me.edit_Multiplier.Location = New System.Drawing.Point(120, 350)
		Me.edit_Multiplier.Name = "edit_Multiplier"
		Me.edit_Multiplier.RenaissanceTag = Nothing
		Me.edit_Multiplier.Size = New System.Drawing.Size(92, 20)
		Me.edit_Multiplier.TabIndex = 13
		Me.edit_Multiplier.Text = "1.00"
		Me.edit_Multiplier.TextFormat = "#,##0.00######"
		Me.edit_Multiplier.Value = 1
		'
		'Label11
		'
		Me.Label11.Location = New System.Drawing.Point(8, 354)
		Me.Label11.Name = "Label11"
		Me.Label11.Size = New System.Drawing.Size(112, 16)
		Me.Label11.TabIndex = 84
		Me.Label11.Text = "Valuation Multiplier"
		'
		'Label12
		'
		Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label12.Location = New System.Drawing.Point(218, 354)
		Me.Label12.Name = "Label12"
		Me.Label12.Size = New System.Drawing.Size(154, 22)
		Me.Label12.TabIndex = 85
		Me.Label12.Text = "Adjust Price for Valuation purposes"
		'
		'frmPrice
		'
		Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
		Me.CancelButton = Me.btnCancel
		Me.ClientSize = New System.Drawing.Size(378, 500)
		Me.Controls.Add(Me.edit_Multiplier)
		Me.Controls.Add(Me.Label11)
		Me.Controls.Add(Me.Label_AdjustedPrice)
		Me.Controls.Add(Me.Label10)
		Me.Controls.Add(Me.RootMenu)
		Me.Controls.Add(Me.Combo_PriceReferenceDate)
		Me.Controls.Add(Me.edit_PriceNAV)
		Me.Controls.Add(Me.Check_IsMilestonePrice)
		Me.Controls.Add(Me.Label9)
		Me.Controls.Add(Me.Check_IsAdministratorPrice)
		Me.Controls.Add(Me.Label8)
		Me.Controls.Add(Me.Check_IsFinalPrice)
		Me.Controls.Add(Me.Label7)
		Me.Controls.Add(Me.GroupBox3)
		Me.Controls.Add(Me.edit_BasePrice)
		Me.Controls.Add(Me.Label6)
		Me.Controls.Add(Me.Label5)
		Me.Controls.Add(Me.edit_PricePercent)
		Me.Controls.Add(Me.label_pricemocepercent)
		Me.Controls.Add(Me.Check_PercentIncrease)
		Me.Controls.Add(Me.Label_percentIncrease)
		Me.Controls.Add(Me.GroupBox2)
		Me.Controls.Add(Me.Date_PriceDate)
		Me.Controls.Add(Me.Combo_PriceInstrument)
		Me.Controls.Add(Me.Label4)
		Me.Controls.Add(Me.Label3)
		Me.Controls.Add(Me.Label2)
		Me.Controls.Add(Me.Combo_SelectPriceDate)
		Me.Controls.Add(Me.btnClose)
		Me.Controls.Add(Me.GroupBox1)
		Me.Controls.Add(Me.Label1)
		Me.Controls.Add(Me.Panel1)
		Me.Controls.Add(Me.Combo_SelectPriceInstrument)
		Me.Controls.Add(Me.btnSave)
		Me.Controls.Add(Me.editAuditID)
		Me.Controls.Add(Me.lblPriceCode)
		Me.Controls.Add(Me.lblPriceDescription)
		Me.Controls.Add(Me.btnDelete)
		Me.Controls.Add(Me.btnCancel)
		Me.Controls.Add(Me.Label12)
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Name = "frmPrice"
		Me.Text = "Add/Edit Price"
		Me.Panel1.ResumeLayout(False)
		CType(Me.edit_BasePrice, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

#End Region

#Region " Form Locals and Constants "


	' Form 'Parent', the Main Venice form.
	' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
	Private WithEvents _MainForm As VeniceMain

	' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
	Private FormTooltip As New ToolTip()

	' Form Menu


	' Form Constants, specific to the table being updated.

    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
	Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

	' Form Locals, initialised on 'New' defining what standard data items to use
    ''' <summary>
  ''' Variable holding the form's data table name. taken from the "ThisStandardDataset" object.
    ''' </summary>
	Private THIS_TABLENAME As String
    ''' <summary>
  ''' Variable holding the form's data adaptor name. taken from the "ThisStandardDataset" object.
    ''' </summary>
	Private THIS_ADAPTORNAME As String
    ''' <summary>
  ''' Variable holding the form's dataset name. taken from the "ThisStandardDataset" object.
    ''' </summary>
	Private THIS_DATASETNAME As String


	' The standard ChangeID for this form. e.g. tblPrice
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
	Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

    ''' <summary>
    ''' The THI s_ FOR m_ first selecting combo
    ''' </summary>
	Private THIS_FORM_FirstSelectingCombo As ComboBox
    ''' <summary>
    ''' The THI s_ FOR m_ second selecting combo
    ''' </summary>
	Private THIS_FORM_SecondSelectingCombo As ComboBox
    ''' <summary>
  ''' Control to select after proessing the "New" button.
    ''' </summary>
	Private THIS_FORM_NewMoveToControl As Control

	' Form Specific Order fields
    ''' <summary>
    ''' The THI s_ FOR m_ first order by
    ''' </summary>
	Private THIS_FORM_FirstOrderBy As String
    ''' <summary>
    ''' The THI s_ FOR m_ second order by
    ''' </summary>
	Private THIS_FORM_SecondOrderBy As String

	' Form Specific Select fields
    ''' <summary>
    ''' The THI s_ FOR m_ first select by
    ''' </summary>
	Private THIS_FORM_FirstSelectBy As String
    ''' <summary>
    ''' The THI s_ FOR m_ second select by
    ''' </summary>
	Private THIS_FORM_SecondSelectBy As String

    ''' <summary>
  ''' The field name that holds the unique identifier for each entry. e.g. FundID for the tblFund table.
    ''' </summary>
	Private THIS_FORM_ValueMember As String

	' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
	Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
	Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

	' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
	Private THIS_FORM_FormID As VeniceFormID

	' Data Structures

    ''' <summary>
    ''' The this standard dataset
    ''' </summary>
	Private ThisStandardDataset As RenaissanceGlobals.StandardDataset

    ''' <summary>
    ''' My dataset
    ''' </summary>
	Private myDataset As RenaissanceDataClass.DSPrice		 ' Form Specific !!!!
    ''' <summary>
    ''' My table
    ''' </summary>
	Private myTable As RenaissanceDataClass.DSPrice.tblPriceDataTable
    ''' <summary>
    ''' My connection
    ''' </summary>
	Private myConnection As SqlConnection
    ''' <summary>
    ''' My adaptor
    ''' </summary>
	Private myAdaptor As SqlDataAdapter


    ''' <summary>
    ''' The highlight live positions
    ''' </summary>
	Private HighlightLivePositions As Boolean

	' Active Element.

    ''' <summary>
    ''' The sorted rows
    ''' </summary>
	Private SortedRows() As DataRow
    ''' <summary>
    ''' The this data row
    ''' </summary>
	Private thisDataRow As RenaissanceDataClass.DSPrice.tblPriceRow		' Form Specific !!!!
    ''' <summary>
    ''' The this audit ID
    ''' </summary>
	Private thisAuditID As Integer
    ''' <summary>
    ''' The this position
    ''' </summary>
	Private thisPosition As Integer
    ''' <summary>
    ''' The __ is over cancel button
    ''' </summary>
	Private __IsOverCancelButton As Boolean
    ''' <summary>
    ''' The _ in use
    ''' </summary>
	Private _InUse As Boolean

	' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
	Private FormIsValid As Boolean
    ''' <summary>
    ''' The form changed
    ''' </summary>
	Private FormChanged As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
	Private _FormOpenFailed As Boolean
    ''' <summary>
    ''' The in paint
    ''' </summary>
	Private InPaint As Boolean
    ''' <summary>
    ''' The add new record
    ''' </summary>
	Private AddNewRecord As Boolean

	' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
	Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
	Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
	Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
	Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
	Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
	Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return __IsOverCancelButton
		End Get
		Set(ByVal Value As Boolean)
			__IsOverCancelButton = Value
		End Set
	End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
	Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
		Get
			Return InPaint
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
		Get
			Return _InUse
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmPrice"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
	Public Sub New(ByVal pMainForm As VeniceMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()

		_MainForm = pMainForm
		AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

		_FormOpenFailed = False

		' ******************************************************
		' Form Specific Settings :
		' ******************************************************

		THIS_FORM_FirstSelectingCombo = Me.Combo_SelectPriceInstrument
		THIS_FORM_SecondSelectingCombo = Me.Combo_SelectPriceDate
		THIS_FORM_NewMoveToControl = Me.Combo_PriceInstrument

		' Default Select and Order fields.

		THIS_FORM_FirstOrderBy = "PriceInstrument"
		THIS_FORM_SecondOrderBy = "PriceDate"
		THIS_FORM_FirstSelectBy = "PriceInstrument"
		THIS_FORM_SecondSelectBy = "PriceDate"

		THIS_FORM_ValueMember = "PriceID"

		' Form Permissioning :-

		THIS_FORM_PermissionArea = Me.Name
		THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

		' 'This' form ID

		THIS_FORM_FormID = VeniceFormID.frmPrice

		' This form's dataset type.

		ThisStandardDataset = RenaissanceStandardDatasets.tblPrice ' This Defines the Form Data !!! 

		' Format Event Handlers for form controls


		' Form Control Changed events
		AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler
		AddHandler THIS_FORM_FirstSelectingCombo.SelectedIndexChanged, AddressOf Combo_FirstSelectComboChanged
		AddHandler THIS_FORM_SecondSelectingCombo.SelectedIndexChanged, AddressOf Combo_SecondSelectComboChanged
		AddHandler THIS_FORM_FirstSelectingCombo.KeyUp, AddressOf Combo_FirstSelectCombo_KeyUp

		AddHandler Combo_PriceInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_PriceInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_PriceInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_PriceInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

		AddHandler Combo_PriceInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Date_PriceDate.ValueChanged, AddressOf Me.FormControlChanged
		AddHandler Check_IsFinalPrice.CheckStateChanged, AddressOf Me.FormControlChanged
		AddHandler Check_IsMilestonePrice.CheckStateChanged, AddressOf Me.FormControlChanged
		AddHandler Check_IsAdministratorPrice.CheckStateChanged, AddressOf Me.FormControlChanged
		AddHandler Check_PercentIncrease.CheckStateChanged, AddressOf Me.FormControlChanged
		AddHandler edit_PricePercent.ValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_PriceReferenceDate.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler edit_BasePrice.ValueChanged, AddressOf Me.FormControlChanged
		AddHandler edit_PriceNAV.ValueChanged, AddressOf Me.FormControlChanged
		AddHandler edit_PriceNAV.TextChanged, AddressOf Me.FormControlChanged
		AddHandler edit_Multiplier.ValueChanged, AddressOf Me.FormControlChanged

		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

		' ******************************************************
		' End Form Specific.
		' ******************************************************

		' Data object names standard to this Form type.

		THIS_TABLENAME = ThisStandardDataset.TableName
		THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
		THIS_DATASETNAME = ThisStandardDataset.DatasetName

		THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

		' Establish / Retrieve data objects for this form.

		myConnection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)

		' myAdaptor = MainForm.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, VENICE_CONNECTION, THIS_TABLENAME)

		myAdaptor = New SqlDataAdapter
		MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION), myAdaptor, "TBLPRICEBYINSTRUMENT")

		myDataset = New RenaissanceDataClass.DSPrice ' MainForm.Load_Table(ThisStandardDataset, False)
		myTable = myDataset.Tables(0)

		MainForm.BuildStandardFormMenu(Me.RootMenu, myDataset.Tables(0), Nothing, AddressOf OrderMenuEvent, AddressOf AuditReportMenuEvent)

		HighlightLivePositions = False

		Call SetHighlighLivePositionsMenu(Me.RootMenu)

	End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

	' Form Initialisation code.
	'
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
	Public Sub ResetForm() Implements StandardVeniceForm.ResetForm
		' ********************************************************************************
		'
		'
		' ********************************************************************************

		THIS_FORM_FirstOrderBy = "PriceInstrument"
		THIS_FORM_SecondOrderBy = "PriceDate"
		THIS_FORM_FirstSelectBy = "PriceInstrument"
		THIS_FORM_SecondSelectBy = "PriceDate"

		Call Form_Load(Me, New System.EventArgs)
	End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
	Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
		' ********************************************************************************
		'
		'
		' ********************************************************************************

		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		' ********************************************************************************
		'
		'
		' ********************************************************************************

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
		_FormOpenFailed = False
		_InUse = True

		Try

			' Initialise Data structures. Connection, Adaptor and Dataset.

			If Not (MainForm Is Nothing) Then
				FormIsValid = True
			Else
				MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
				FormIsValid = False
				_FormOpenFailed = True
				Exit Sub
			End If

			If (myConnection Is Nothing) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

				FormIsValid = False
				_FormOpenFailed = True
				Exit Sub
			End If

			If (myAdaptor Is Nothing) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

				FormIsValid = False
				_FormOpenFailed = True
				Exit Sub
			End If

			If (myDataset Is Nothing) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

				FormIsValid = False
				_FormOpenFailed = True
				Exit Sub
			End If

		Catch ex As Exception
		End Try

		Try

			' Initialse form
			SetPriceInstrumentCombo()

			InPaint = True
			IsOverCancelButton = False

			' Check User permissions
			Call CheckPermissions()
			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

				FormIsValid = False
				_FormOpenFailed = True
				Exit Sub
			End If

			' Build Sorted data list from which this form operates
			Call SetSortedRows()
			Call SetFirstSelectCombo()

			' Display initial record.

			thisPosition = 0
			If THIS_FORM_FirstSelectingCombo.Items.Count > 0 Then

				If (THIS_FORM_FirstSelectingCombo.Items.Count > 0) Then THIS_FORM_FirstSelectingCombo.SelectedIndex = 0

				LoadSelectedPriceData(myTable, THIS_FORM_FirstSelectingCombo.SelectedValue)

				' Re-Set Controls etc.
				Call SetSortedRows()

				If (myTable.Rows.Count > 0) Then

					Call SetSecondSelectCombo()

					If (THIS_FORM_SecondSelectingCombo.Items.Count > 0) Then THIS_FORM_SecondSelectingCombo.SelectedIndex = 0
					thisPosition = Get_Position()

					thisDataRow = SortedRows(thisPosition)
					Call GetFormData(thisDataRow)

				Else

					MainForm.ClearComboSelection(THIS_FORM_FirstSelectingCombo)
					MainForm.ClearComboSelection(THIS_FORM_SecondSelectingCombo)
					Call GetFormData(Nothing)

				End If
			Else
				MainForm.ClearComboSelection(THIS_FORM_FirstSelectingCombo)
				MainForm.ClearComboSelection(THIS_FORM_SecondSelectingCombo)
				Call GetFormData(Nothing)
			End If

		Catch ex As Exception

			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Initialising Form (Form.Load).", ex.StackTrace, True)

		Finally

			InPaint = False

		End Try

	End Sub

    ''' <summary>
    ''' Handles the Closing event of the frmPrice control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
	Private Sub frmPrice_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		' ********************************************************************************
		'
		'
		' ********************************************************************************

		Dim HideForm As Boolean

		_InUse = False

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...

		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else
			If (FormChanged = True) Then
				Call SetFormData()
			End If

			HideForm = True
			If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			e.Cancel = True
		Else
			Try
				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler
				RemoveHandler THIS_FORM_FirstSelectingCombo.SelectedIndexChanged, AddressOf Combo_FirstSelectComboChanged
				RemoveHandler THIS_FORM_SecondSelectingCombo.SelectedIndexChanged, AddressOf Combo_SecondSelectComboChanged
				RemoveHandler THIS_FORM_FirstSelectingCombo.KeyUp, AddressOf Combo_FirstSelectCombo_KeyUp

				RemoveHandler Combo_PriceInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_PriceInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_PriceInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_PriceInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

				RemoveHandler Combo_PriceInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Date_PriceDate.ValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_IsFinalPrice.CheckStateChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_IsMilestonePrice.CheckStateChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_IsAdministratorPrice.CheckStateChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_PercentIncrease.CheckStateChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_PricePercent.ValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_PriceReferenceDate.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_BasePrice.ValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_PriceNAV.ValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_PriceNAV.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_Multiplier.ValueChanged, AddressOf Me.FormControlChanged

			Catch ex As Exception
			End Try
		End If

	End Sub


#End Region

    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		' Routine to handle changes / updates to tables by this and other windows.
		' If this, or any other, form posts a change to a table, then it will invoke an update event 
		' detailing what tables have been altered.
		' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
		' the 'VeniceAutoUpdate' event of the main Venice form.
		' Each form may them react as appropriate to changes in any table that might impact it.
		'
		Dim OrgInPaint As Boolean
		Dim KnowledgeDateChanged As Boolean
		Dim SetButtonStatus_Flag As Boolean
		Dim RefreshForm As Boolean = False

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then
			Exit Sub
		End If

		OrgInPaint = InPaint

		Try
			InPaint = True
			KnowledgeDateChanged = False
			SetButtonStatus_Flag = False

			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
				KnowledgeDateChanged = True
				RefreshForm = True
			End If

			' ****************************************************************
			' Check for changes relevant to this form
			' ****************************************************************

			' Changes to the tblInstrument table :-
			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrument) = True) Or KnowledgeDateChanged Then

				' ReBuild the Instrument derived combos

				Call Me.SetFirstSelectCombo()
				Call Me.SetPriceInstrumentCombo()

				RefreshForm = True
			End If

			' Changes to the KnowledgeDate :-
			If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
				SetButtonStatus_Flag = True
				RefreshForm = True
			End If

			' Changes to the tblUserPermissions table :-
			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

				' Check ongoing permissions.

				Call CheckPermissions()
				If (HasReadPermission = False) Then
					Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

					FormIsValid = False
					Me.Close()
					Exit Sub
				End If

				SetButtonStatus_Flag = True

			End If


			' ****************************************************************
			' Changes to the Main FORM table :-
			' ****************************************************************

			If (e.TableChanged(THIS_FORM_ChangeID) = True) Or KnowledgeDateChanged Then

				RefreshForm = True
				LoadSelectedPriceData(myTable, THIS_FORM_FirstSelectingCombo.SelectedValue)

				' Re-Set Controls etc.
				Call SetSortedRows()

				' Move again to the correct item
				thisPosition = Get_Position(thisAuditID)

				' Validate current position.
				If thisPosition >= 0 And thisPosition < Me.SortedRows.GetLength(0) Then
					thisDataRow = Me.SortedRows(thisPosition)
				Else
					If (Me.SortedRows.GetLength(0) > 0) And (AddNewRecord = False) Then
						thisPosition = 0
						thisDataRow = Me.SortedRows(thisPosition)
						FormChanged = False
					Else
						thisDataRow = Nothing
					End If
				End If

				' Set SelectingCombo Index.

				If (Me.THIS_FORM_SecondSelectingCombo.Items.Count <= 0) Then
					Try
						MainForm.ClearComboSelection(THIS_FORM_FirstSelectingCombo)
						MainForm.ClearComboSelection(THIS_FORM_SecondSelectingCombo)
					Catch ex As Exception
					End Try
				ElseIf (thisPosition < 0) Then
					Try
						MainForm.ClearComboSelection(THIS_FORM_FirstSelectingCombo)
						Call SetSecondSelectCombo()
						MainForm.ClearComboSelection(THIS_FORM_SecondSelectingCombo)
					Catch ex As Exception
					End Try
				Else
					Try
						Me.THIS_FORM_FirstSelectingCombo.SelectedValue = thisDataRow.PriceInstrument
						Call SetSecondSelectCombo()
						Me.THIS_FORM_SecondSelectingCombo.SelectedValue = thisDataRow.PriceDate
					Catch ex As Exception
					End Try
				End If

			End If

		Catch ex As Exception
		Finally
			InPaint = OrgInPaint
		End Try


		' ****************************************************************
		' Repaint if not currently in Edit Mode
		'
		' ****************************************************************

		' Retrieve form data if appropriate, i.e. this data item is not already being edited.
		If (RefreshForm) AndAlso ((FormChanged = False) And (AddNewRecord = False)) Then
			GetFormData(thisDataRow) ' Includes a call to 'SetButtonStatus()'
		Else
			If SetButtonStatus_Flag Then
				Call SetButtonStatus()
			End If
		End If

	End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

	' Build Sorted list from the Source dataset and update the Select Combo
    ''' <summary>
    ''' Sets the sorted rows.
    ''' </summary>
	Private Sub SetSortedRows()

		Dim OrgInPaint As Boolean

		' Form Specific Selection Combo :-
		If (THIS_FORM_FirstSelectingCombo Is Nothing) Then Exit Sub
		If (THIS_FORM_SecondSelectingCombo Is Nothing) Then Exit Sub

		' Code is pretty Generic form here on...

		Try

			' Set paint local so that changes to the selection combo do not trigger form updates.

			OrgInPaint = InPaint
			InPaint = True

			' Get selected Row sets, or exit if no data is present.

			If (myDataset Is Nothing) OrElse (myTable Is Nothing) OrElse (myTable.Rows.Count <= 0) Then
				ReDim SortedRows(-1)
			Else
				SortedRows = myTable.Select("True", THIS_FORM_FirstOrderBy & ", " & THIS_FORM_SecondOrderBy)
			End If

		Catch ex As Exception
		Finally

			InPaint = OrgInPaint

		End Try


	End Sub


	' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
	Private Sub CheckPermissions()

		Dim Permissions As Integer

		Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

		HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
		HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
		HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
		HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

	End Sub

	' Flag changes to form controls, Event Associations made in 'New' routine.
    ''' <summary>
  ''' Basic event handler, called when a form value is changed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

		If InPaint = False Then
			If (Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord) Then
				If (Me.Check_IsMilestonePrice.Checked = False) Then
					FormChanged = True
					Me.btnSave.Enabled = True
					Me.btnCancel.Enabled = True
				End If
			End If
		End If

	End Sub


    ''' <summary>
    ''' Orders the menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Public Sub OrderMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Order Menu
		' Event association is made during the dynamic menu creation

		THIS_FORM_FirstOrderBy = CInt(CType(sender, System.Windows.Forms.ToolStripMenuItem).Tag)

		Select Case THIS_FORM_FirstOrderBy.ToUpper
			Case "PriceDATE"
				THIS_FORM_SecondOrderBy = "PriceInstrument"

			Case Else
				THIS_FORM_SecondOrderBy = "PriceDate"

		End Select

		Call SetSortedRows()

	End Sub


    ''' <summary>
    ''' Audits the report menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Public Sub AuditReportMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Audit Report Menu
		' Event association is made during the dynamic menu creation

		Try
			Select Case CInt(CType(sender, System.Windows.Forms.ToolStripMenuItem).Tag)
				Case 0 ' This Record
					If (Me.AddNewRecord = False) AndAlso (thisAuditID >= 0) Then
						Run_AuditReport_ThisRecord(MainForm, ThisStandardDataset, thisAuditID, MainForm.VeniceProgressBar)
					End If

				Case 1 ' All Records
					Run_AuditReport_ALLRecords(MainForm, ThisStandardDataset, MainForm.VeniceProgressBar)

			End Select
		Catch ex As Exception
			Call MainForm.LogError(Me.Name & ", AuditReportMenuEvent()", 0, ex.Message, "Error calling Audit report.", ex.StackTrace, True)
		End Try

	End Sub

#End Region

#Region " Conrtol Event Code, inc. CalculatePriceLevel()"

    ''' <summary>
    ''' Calculates the price level.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub CalculatePriceLevel(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles edit_PricePercent.TextChanged, edit_BasePrice.TextChanged
		If InPaint = False Then
			Dim BasePrice As Double
			Dim PercentMove As Double

			Try
				If Not (Me.edit_PricePercent.Focused) Then
					PercentMove = Me.edit_PricePercent.Value
				ElseIf IsNumeric(Me.edit_PricePercent.Text) Then
					PercentMove = CDbl(Me.edit_PricePercent.Text)
				ElseIf (Me.edit_PricePercent.Text.Trim.EndsWith("%")) AndAlso (IsNumeric(Me.edit_PricePercent.Text.Trim.Substring(0, (Me.edit_PricePercent.Text.Trim.Length - 1)))) Then
					PercentMove = CDbl(Me.edit_PricePercent.Text.Trim.Substring(0, (Me.edit_PricePercent.Text.Trim.Length - 1))) / 100.0
				Else
					Me.edit_PriceNAV.Value = 0
				End If

				If Not (Me.edit_BasePrice.Focused) AndAlso (IsNumeric(Me.edit_BasePrice.Value)) Then
					BasePrice = Me.edit_BasePrice.Value
				ElseIf IsNumeric(Me.edit_BasePrice.Text) Then
					BasePrice = CDbl(Me.edit_BasePrice.Text)
				Else
					Me.edit_PriceNAV.Value = 0
				End If

				Me.edit_PriceNAV.Value = BasePrice * (1.0 + PercentMove)

			Catch ex As Exception
				Me.edit_PriceNAV.Value = 0
			End Try

		End If
	End Sub


    ''' <summary>
    ''' Handles the DrawItem event of the Combo_SelectPriceInstrument control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.DrawItemEventArgs"/> instance containing the event data.</param>
	Private Sub Combo_SelectPriceInstrument_DrawItem(ByVal sender As Object, ByVal e As System.Windows.Forms.DrawItemEventArgs) Handles Combo_SelectPriceInstrument.DrawItem

		Dim myFont As System.Drawing.Font
		Dim eventCombo As ComboBox

		Try

			eventCombo = CType(sender, ComboBox)

			' Draw the background of the item.
			e.DrawBackground()

			' Draw each string in the array, using a different size, color,
			' and font for each item.

			Dim thisRow As DataRowView
			thisRow = eventCombo.Items(e.Index)

			If CStr(thisRow("DM")).EndsWith("(��)") Then
				myFont = New Font(eventCombo.Font, FontStyle.Bold)
			Else
				myFont = New Font(eventCombo.Font, FontStyle.Regular)
			End If

			e.Graphics.DrawString(CStr(thisRow("DM")), myFont, System.Drawing.Brushes.Black, _
			 New RectangleF(e.Bounds.X + 2, e.Bounds.Y, _
			 e.Bounds.Width, e.Bounds.Height))

			' Draw the focus rectangle if the mouse hovers over an item.
			e.DrawFocusRectangle()

		Catch ex As Exception
		End Try

	End Sub

#End Region

#Region " Set Form Combos (Form Specific Code) "

    ''' <summary>
    ''' Sets the price instrument combo.
    ''' </summary>
	Private Sub SetPriceInstrumentCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_PriceInstrument, _
		RenaissanceStandardDatasets.tblInstrument, _
		"InstrumentDescription", _
		"InstrumentID", _
		"")		' 

	End Sub

    ''' <summary>
    ''' Sets the first select combo.
    ''' </summary>
	Private Sub SetFirstSelectCombo()

		If Me.HighlightLivePositions Then

			Call MainForm.SetTblGenericCombo( _
			THIS_FORM_FirstSelectingCombo, _
			RenaissanceStandardDatasets.tblInstrumentNamesGrossUnits, _
			"InstrumentDescription", _
			"InstrumentID", _
			"")		' 

		Else

			Call MainForm.SetTblGenericCombo( _
			THIS_FORM_FirstSelectingCombo, _
			RenaissanceStandardDatasets.tblInstrument, _
			"InstrumentDescription", _
			"InstrumentID", _
			"")		' 

		End If

	End Sub

    ''' <summary>
    ''' Sets the second select combo.
    ''' </summary>
	Private Sub SetSecondSelectCombo()
		Dim SelectedValue As Object
		Dim orgInPaint As Boolean

		orgInPaint = InPaint
		InPaint = True

		If THIS_FORM_FirstSelectingCombo.SelectedIndex >= 0 Then
			SelectedValue = THIS_FORM_FirstSelectingCombo.SelectedValue
		Else
			SelectedValue = 0
		End If

		' Set 'SELEECT' Combo
		Call MainForm.SetTblGenericCombo( _
		THIS_FORM_SecondSelectingCombo, _
		myTable.Select("True", "PriceDate Desc"), _
		"PriceDate", _
		"PriceDate", _
		"PriceInstrument = " & SelectedValue.ToString, _
		True, _
		False)

		' Set 'ReferenceDate' Combo.
		' Putting this here is a bit of a fudge, but it is convenient and accurate.

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_PriceReferenceDate, _
		myTable.Select("True", "PriceDate Desc"), _
		"PriceDate", _
		"PriceDate", _
		"PriceInstrument = " & SelectedValue.ToString, _
		True, _
		False)


		'' Set 'SELEECT' Combo
		'Call MainForm.SetTblGenericCombo( _
		'THIS_FORM_SecondSelectingCombo, _
		'RenaissanceStandardDatasets.tblPrice, _
		'"PriceDate", _
		'"PriceDate", _
		'"PriceInstrument = " & THIS_FORM_FirstSelectingCombo.SelectedValue.ToString, _
		'True, _
		'False)

		'' Set 'ReferenceDate' Combo.
		'' Putting this here is a bit of a fudge, but it is convenient and accurate.

		'Call MainForm.SetTblGenericCombo( _
		'Me.Combo_PriceReferenceDate, _
		'RenaissanceStandardDatasets.tblPrice, _
		'"PriceDate", _
		'"PriceDate", _
		'"PriceInstrument = " & THIS_FORM_FirstSelectingCombo.SelectedValue.ToString, _
		'True, _
		'False)

		If (THIS_FORM_SecondSelectingCombo.Items.Count > 0) Then
			THIS_FORM_SecondSelectingCombo.SelectedIndex = 0
			Application.DoEvents()
		End If

		InPaint = orgInPaint

	End Sub


#End Region

#Region " Get & Set Form Data / SetButton / ValidateForm / btnCancel Events (Form Specific Code) "

    ''' <summary>
    ''' Gets the form data.
    ''' </summary>
    ''' <param name="ThisRow">The this row.</param>
	Private Sub GetFormData(ByRef ThisRow As RenaissanceDataClass.DSPrice.tblPriceRow)
		' Routine to populate form Controls given a data row.
		' Form is cleared for invalid Datarows or Invalid Form.

		If (Me.IsDisposed) OrElse (Me.Disposing) Then
			Exit Sub
		End If

		Dim OrgInpaint As Boolean

		' Set 'Paint' flag to prevent event actions caused by changing the value of form fields.
		OrgInpaint = InPaint
		InPaint = True


		If (ThisRow Is Nothing) Or (FormIsValid = False) Then
			' Bad / New Datarow - Clear Form.

			thisAuditID = (-1)

			Me.editAuditID.Text = ""

			MainForm.ClearComboSelection(Combo_PriceInstrument)

			Me.Date_PriceDate.Value = Now.Date

      Me.Check_IsFinalPrice.CheckState = CheckState.Checked
			Me.Check_IsMilestonePrice.CheckState = CheckState.Unchecked
			Me.Check_IsAdministratorPrice.CheckState = CheckState.Unchecked
			Me.Check_PercentIncrease.CheckState = CheckState.Unchecked
			Me.edit_PricePercent.Value = 0

			MainForm.ClearComboSelection(Combo_PriceReferenceDate)

			Me.edit_BasePrice.Value = Nothing
			Me.edit_PriceNAV.Value = 1
			Me.edit_Multiplier.Value = 1.0#

			If AddNewRecord = True Then
				Me.btnCancel.Enabled = True
				Me.THIS_FORM_FirstSelectingCombo.Enabled = False
				Me.THIS_FORM_SecondSelectingCombo.Enabled = False
			Else
				Me.btnCancel.Enabled = False
				Me.THIS_FORM_FirstSelectingCombo.Enabled = True
				Me.THIS_FORM_SecondSelectingCombo.Enabled = True
			End If

		Else

			' Populate Form with given data.
			Try
				thisAuditID = thisDataRow.AuditID
				Me.editAuditID.Text = thisAuditID.ToString

				Me.Combo_PriceInstrument.SelectedValue = thisDataRow.PriceInstrument
				Me.Date_PriceDate.Value = thisDataRow.PriceDate

				If thisDataRow.PriceFinal = 0 Then
					Me.Check_IsFinalPrice.CheckState = CheckState.Unchecked
				Else
					Me.Check_IsFinalPrice.CheckState = CheckState.Checked
				End If

				If thisDataRow.PriceIsMilestone = 0 Then
					Me.Check_IsMilestonePrice.CheckState = CheckState.Unchecked
				Else
					Me.Check_IsMilestonePrice.CheckState = CheckState.Checked
				End If

				If thisDataRow.PriceIsAdministrator = 0 Then
					Me.Check_IsAdministratorPrice.CheckState = CheckState.Unchecked
				Else
					Me.Check_IsAdministratorPrice.CheckState = CheckState.Checked
				End If

				If thisDataRow.PriceIsPercent = 0 Then
					Me.Check_PercentIncrease.CheckState = CheckState.Unchecked
				Else
					Me.Check_PercentIncrease.CheckState = CheckState.Checked
				End If

				Me.edit_PricePercent.Value = thisDataRow.PricePercent

				If thisDataRow.IsPriceBaseDateNull Then
					Me.Combo_PriceReferenceDate.SelectedItem = Nothing
					Me.edit_BasePrice.Value = Nothing
				Else
					Me.Combo_PriceReferenceDate.SelectedValue = thisDataRow.PriceBaseDate

					' Base Price
					Me.edit_BasePrice.Value = GetBaseNAV(thisDataRow.PriceInstrument, thisDataRow.PriceBaseDate)
					If (IsNumeric(Me.edit_BasePrice.Value) = False) Then
						Me.edit_BasePrice.Value = 0
					End If
				End If

				' Price Level
				Me.edit_PriceNAV.Value = thisDataRow.PriceNAV
				Me.edit_Multiplier.Value = thisDataRow.PriceMultiplier

				AddNewRecord = False
				' MainForm.SetComboSelectionLengths(Me)

				Me.btnCancel.Enabled = False
				Me.THIS_FORM_FirstSelectingCombo.Enabled = True
				Me.THIS_FORM_SecondSelectingCombo.Enabled = True

			Catch ex As Exception

        Call MainForm.LogError(Me.Name & ", GetFormData", LOG_LEVELS.Error, ex.Message, "Error Showing Data", ex.StackTrace, True)
				Call GetFormData(Nothing)

			End Try

		End If

		' Allow Field events to trigger before 'InPaint' Is re-set. 
		' (Should) Prevent Validation errors during Form Draw.
		Application.DoEvents()

		' Restore 'Paint' flag.

		InPaint = OrgInpaint
		FormChanged = False
		Me.btnSave.Enabled = False

		' As it says on the can....
		Call SetButtonStatus()

	End Sub

    ''' <summary>
    ''' Sets the form data.
    ''' </summary>
    ''' <param name="pConfirm">if set to <c>true</c> [p confirm].</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	Private Function SetFormData(Optional ByVal pConfirm As Boolean = True) As Boolean
		' *************************************************************
		' Note, when making changes, please consider changes necessary in the 
		' frmPriceGrid form.
		'
		'
		' *************************************************************

		Dim ErrMessage As String
		Dim ErrFlag As Boolean
		Dim ErrStack As String
		Dim ProtectedItem As Boolean = False
		Dim thisInstrumentID As Integer = 0
		Dim thisPriceDate As Date = KNOWLEDGEDATE_NOW
		Dim ChildInstrumentIDs As ArrayList = Nothing
		Dim ChildCount As Integer

		ErrMessage = ""
		ErrStack = ""

		' *************************************************************
		' Appropriate Save permission :-
		' *************************************************************

		If (Me.HasUpdatePermission = False) And ((Me.HasInsertPermission = False) Or (Me.AddNewRecord = False)) Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "You do not have permission to save this record.", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' *************************************************************
		' If Save button is disabled then should not be able to save, exit silently.
		' *************************************************************
		If Me.btnSave.Enabled = False Then
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' *************************************************************
		' KnowledgeDate OK :-
		' *************************************************************

		If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Changes to Venice are not allowed when the Knowledgedate is not set to `NOW`", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' Confirm Save, if required.

		If (pConfirm = True) Then
			If MessageBox.Show("Save Changes ?", "Save Changes ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
				Return True
				Exit Function
			End If
		End If

		' *************************************************************
		' Procedure to Save the current form information.
		' *************************************************************

		Dim StatusString As String = ""
		Dim LogString As String
		Dim UpdateRows(0) As DataRow
		Dim Position As Integer

		If (FormChanged = False) Or (FormIsValid = False) Then
			Return False
			Exit Function
		End If

		' Validation
		If ValidateForm(StatusString) = False Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Validation Error." & vbCrLf & StatusString, "", True)
			Return False
			Exit Function
		End If

		' Check current position in the table.
		Position = Get_Position(thisAuditID)


		' Allow for new or missing ID.
		If Position < 0 Then
			If (AddNewRecord = True) OrElse (SortedRows.Length <= 0) Then
				thisDataRow = myTable.NewRow
				LogString = "Add: "
				AddNewRecord = True
			Else
				Return False
				Exit Function
			End If
		Else
			' Row found OK.
			LogString = "Edit : AuditID = " & thisAuditID.ToString

			thisDataRow = Me.SortedRows(Position)

			' Check for replacing a Milestone Price
			If (thisDataRow.PriceIsMilestone = True) Then
				If MsgBox("Are you sure that you want to replace a milestone price ?" & vbCrLf & "The new price will not be saved as a Milestone price.", MsgBoxStyle.YesNo, "Edit Price Rate") = MsgBoxResult.No Then
					Return False
					Exit Function
				End If
			End If

			' Check for Changed CcyCode or Date
			If (thisDataRow.PriceInstrument <> CInt(Me.Combo_PriceInstrument.SelectedValue)) Then
				If MsgBox("Are you sure that you want to change the Instrument for this Price ?" & vbCrLf & "You are editing an existing Price Rate rather than adding a new one!", MsgBoxStyle.YesNo, "Edit Price Rate") = MsgBoxResult.No Then
					Return False
					Exit Function
				End If
			End If
			If (thisDataRow.PriceDate <> Me.Date_PriceDate.Value) Then
				If MsgBox("Are you sure that you want to change the Date for this Price ?" & vbCrLf & "You are changing the date for an existing Price Rate rather than adding a new one!", MsgBoxStyle.YesNo, "Edit Price Rate") = MsgBoxResult.No Then
					Return False
					Exit Function
				End If
			End If
		End If

		' Set 'Paint' flag.
		InPaint = True

		If Me.Check_PercentIncrease.CheckState = CheckState.Checked Then
			' Do this check outside of the SyncLock so that the usual Error message can be used.

			ChildInstrumentIDs = GetPriceChildInstrumentIDs(MainForm, Combo_PriceInstrument.SelectedValue)
		End If

		' *************************************************************
		' Lock the Data Table, to prevent update conflicts.
		' *************************************************************

		SyncLock myTable

			' Initiate Edit,
			thisDataRow.BeginEdit()

			' Set Data Values

			thisDataRow.PriceInstrument = Me.Combo_PriceInstrument.SelectedValue
			thisDataRow.PriceDate = Me.Date_PriceDate.Value

			thisInstrumentID = thisDataRow.PriceInstrument
			thisPriceDate = thisDataRow.PriceDate

			If Me.Check_IsFinalPrice.CheckState = CheckState.Checked Then
				thisDataRow.PriceFinal = (-1)
			Else
				thisDataRow.PriceFinal = 0
			End If

			thisDataRow.PriceIsMilestone = False

			If Me.Check_IsAdministratorPrice.CheckState = CheckState.Checked Then
				thisDataRow.PriceIsAdministrator = (-1)
			Else
				thisDataRow.PriceIsAdministrator = 0
			End If

			If Me.Check_IsMilestonePrice.CheckState = CheckState.Checked Then
				thisDataRow.PriceIsMilestone = (-1)
			Else
				thisDataRow.PriceIsMilestone = 0
			End If

			If Me.Check_PercentIncrease.CheckState = CheckState.Checked Then
				thisDataRow.PriceIsPercent = (-1)
				thisDataRow.PricePercent = Me.edit_PricePercent.Value
				thisDataRow.PriceBaseDate = Me.Combo_PriceReferenceDate.SelectedValue
				Call CalculatePriceLevel(Me, New EventArgs)
			Else
				thisDataRow.PriceIsPercent = 0
				thisDataRow.PricePercent = 0
				thisDataRow.SetPriceBaseDateNull()
			End If

			thisDataRow.PriceNAV = Me.edit_PriceNAV.Value
			thisDataRow.PriceMultiplier = Me.edit_Multiplier.Value
			thisDataRow.PriceLevel = Math.Round((thisDataRow.PriceNAV * thisDataRow.PriceMultiplier), 8)
			thisDataRow.PriceBasicNAV = 0
			thisDataRow.PriceGNAV = 0
			thisDataRow.PriceGAV = 0
			thisDataRow.PriceComment = ""

			LogString &= ", PriceInstrument = " & thisDataRow.PriceInstrument
			LogString &= ", PriceDate = " & thisDataRow.PriceDate.ToString(DISPLAYMEMBER_DATEFORMAT)
			LogString &= ", PriceLevel = " & thisDataRow.PriceLevel

			thisDataRow.EndEdit()
			InPaint = False

			' Add and Update DataRow. 

			ErrFlag = False

			If AddNewRecord = True Then
				Try
					myTable.Rows.Add(thisDataRow)
				Catch ex As Exception
					ErrFlag = True
					ErrMessage = ex.Message
					ErrStack = ex.StackTrace
				End Try
			End If

			UpdateRows(0) = thisDataRow

			' Post Additions / Updates to the underlying table :-

			Dim temp As Integer
			Try
				If (ErrFlag = False) Then

					myAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
					myAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

					temp = MainForm.AdaptorUpdate(Me.Name, myAdaptor, UpdateRows)

					' Propagate to child Instruments ?
					If (thisDataRow.PriceIsPercent <> 0) Then
						Try
							If (Not (ChildInstrumentIDs Is Nothing)) AndAlso (ChildInstrumentIDs.Count > 0) Then

								' OK, Propagate Price Changes.
								Dim ChildPriceTable As New RenaissanceDataClass.DSPrice.tblPriceDataTable
								Dim ChildDataRow As RenaissanceDataClass.DSPrice.tblPriceRow

								For ChildCount = 0 To (ChildInstrumentIDs.Count - 1)
									' If a price exists for the Base Date, then propogate the Child price.

									If PriceExists(MainForm, CInt(ChildInstrumentIDs(ChildCount)), thisDataRow.PriceBaseDate) Then
										ChildDataRow = ChildPriceTable.NewRow

										ChildDataRow.PriceInstrument = CInt(ChildInstrumentIDs(ChildCount))
										ChildDataRow.PriceDate = thisDataRow.PriceDate
										ChildDataRow.PricePercent = thisDataRow.PricePercent
										ChildDataRow.PriceLevel = 0
										ChildDataRow.PriceNAV = 0
										ChildDataRow.PriceMultiplier = Me.edit_Multiplier.Value
										ChildDataRow.PriceBasicNAV = 0
										ChildDataRow.PriceGNAV = 0
										ChildDataRow.PriceGAV = 0
										ChildDataRow.PriceBaseDate = thisDataRow.PriceBaseDate
										ChildDataRow.PriceFinal = thisDataRow.PriceFinal
										ChildDataRow.PriceIsPercent = thisDataRow.PriceIsPercent
										ChildDataRow.PriceIsAdministrator = thisDataRow.PriceIsAdministrator
										ChildDataRow.PriceIsMilestone = False
										ChildDataRow.PriceComment = "Child % Price Cascade"

										ChildPriceTable.Rows.Add(ChildDataRow)
										UpdateRows(0) = ChildDataRow
										temp = MainForm.AdaptorUpdate(Me.Name, myAdaptor, UpdateRows)
									End If

								Next

							End If
						Catch ex As Exception
							ErrMessage = ex.Message
							ErrFlag = True
							ErrStack = ex.StackTrace
						End Try
					End If

					' Always Re-Load the Prices table since a price cascade may have altered other prices, 
					' Or Child Price updates may have occurred.

					If AddNewRecord = True Then
						thisAuditID = thisDataRow.AuditID
					End If

					thisDataRow = Nothing

					'LoadSelectedPriceData(myTable, THIS_FORM_FirstSelectingCombo.SelectedValue)
					LoadSelectedPriceData(myTable, Combo_PriceInstrument.SelectedValue)

					' Re-Set Controls etc.
					Call SetSortedRows()

          MainForm.ReloadTable(ThisStandardDataset.ChangeID, "", False)

					' myDataset = MainForm.Load_Table(ThisStandardDataset, True, False)
					' myTable = myDataset.Tables(0)

				End If

			Catch ex As Exception

				ErrMessage = ex.Message
				ErrFlag = True
				ErrStack = ex.StackTrace

			End Try

			If Not (thisDataRow Is Nothing) Then
				thisAuditID = thisDataRow.AuditID
			End If

		End SyncLock

		' *************************************************************
		' Error report. Not done in the Try-Catch as that would serve to persist the SyncLock.
		' *************************************************************

		If (ErrFlag = True) Then
			Call MainForm.LogError(Me.Name & ", SetFormData", 0, ErrMessage, "Error Saving Data", ErrStack, True)
		End If

		' Finish off

		AddNewRecord = False
		FormChanged = False

		Me.THIS_FORM_FirstSelectingCombo.Enabled = True
		Me.THIS_FORM_SecondSelectingCombo.Enabled = True

		THIS_FORM_FirstSelectingCombo.SelectedValue = Combo_PriceInstrument.SelectedValue

		' Update dependent transaction, and
		' Propagate changes.

		Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID), True)
		Application.DoEvents()

		' Rough check for transactions to update...
		Dim RoughTransactionCount As Integer = 0
		Try
			Dim TransactionDataset As RenaissanceDataClass.DSTransaction
			Dim TransactionTable As RenaissanceDataClass.DSTransaction.tblTransactionDataTable
			Dim DSInstruments As RenaissanceDataClass.DSInstrument = Nothing
			Dim TblInstruments As RenaissanceDataClass.DSInstrument.tblInstrumentDataTable = Nothing
			Dim SelectedInstruments() As RenaissanceDataClass.DSInstrument.tblInstrumentRow = Nothing
			Dim InstrumentCounter As Integer

			' Get handle to Transactions Dataset

			TransactionDataset = MainForm.Load_Table(RenaissanceStandardDatasets.tblTransaction, False)

			' Get Related Instrument Details.

			DSInstruments = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblInstrument, False)
			If DSInstruments IsNot Nothing Then
				TblInstruments = DSInstruments.tblInstrument
				SelectedInstruments = TblInstruments.Select("(InstrumentParent=" & thisInstrumentID.ToString & ")")
			End If

			' Check for related transactions on or after this price date.

			If (TransactionDataset IsNot Nothing) Then
				TransactionTable = TransactionDataset.tblTransaction

				If (TransactionTable IsNot Nothing) AndAlso (TransactionTable.Rows.Count > 0) Then

					Dim SelectedTransactions() As RenaissanceDataClass.DSTransaction.tblTransactionRow
					Dim SelectString As String

					SelectString = "(TransactionInstrument IN (" & thisInstrumentID.ToString
					If (SelectedInstruments IsNot Nothing) AndAlso (SelectedInstruments.Length > 0) Then
						For InstrumentCounter = 0 To (SelectedInstruments.Length - 1)
							If (SelectedInstruments(InstrumentCounter).InstrumentID <> thisInstrumentID) Then
								SelectString &= "," & SelectedInstruments(InstrumentCounter).InstrumentID.ToString
							End If
						Next
					End If

					SelectString &= ")) AND (TransactionValueDate>='" & thisPriceDate.ToString(QUERY_SHORTDATEFORMAT) & "') AND (TransactionExemptFromUpdate=0)"
					SelectedTransactions = TransactionTable.Select(SelectString)

					If (SelectedTransactions IsNot Nothing) Then
						RoughTransactionCount = SelectedTransactions.Length
					End If
				End If
			End If

			' Clear DataSet & Table locals.

			SelectedInstruments = Nothing
			TblInstruments = Nothing
			DSInstruments = Nothing

		Catch ex As Exception
			RoughTransactionCount = 1	' Try to Update, in case of problems.
		End Try

		If (RoughTransactionCount > 0) Then
			If MessageBox.Show("Do you want to update Transactions" & vbCrLf & "that have been affected by this price change ?", "Update Transactions ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
				Dim RaiseEventFlag As Boolean
				RaiseEventFlag = False

				' Update transactions for Main Instrument

				If Update_Dependent_Transactions_NonMilestone(Me.MainForm, Me.Name, thisInstrumentID, New Date() {thisPriceDate}) = False Then
					RaiseEventFlag = True
					'Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblTransaction))
					Application.DoEvents()
				End If

				' Update Transactions for child instruments.

				If (Not (ChildInstrumentIDs Is Nothing)) AndAlso (ChildInstrumentIDs.Count > 0) Then
					For ChildCount = 0 To (ChildInstrumentIDs.Count - 1)
						If Update_Dependent_Transactions_NonMilestone(Me.MainForm, Me.Name, CInt(ChildInstrumentIDs(ChildCount)), New Date() {thisPriceDate}) = False Then
							RaiseEventFlag = True
							' Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblTransaction))
							Application.DoEvents()
						End If
					Next
				End If

				If (RaiseEventFlag) Then
					Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblTransaction), True)
					Application.DoEvents()
				End If
			End If
		End If
	End Function


    ''' <summary>
    ''' Sets the button status.
    ''' </summary>
	Private Sub SetButtonStatus()
		' Sets the status of the form controlls appropriate to the current users 
		'permissions and the 'Changed' or 'New' status of the form.

		' No Read permission :-

		If Me.HasReadPermission = False Then
			MainForm.LogError(Me.Name & ", SetButtonStatus()", 0, "", "You do not have Read permission for this Form", "", True)
			Me.Close()
			Exit Sub
		End If

		' Has Insert Permission.
		If Me.HasInsertPermission Then
			Me.btnAdd.Enabled = True
		Else
			Me.btnAdd.Enabled = False
		End If

		' Has Delete permission. 
		If (Me.HasDeletePermission) And (Me.AddNewRecord = False) And (thisPosition >= 0) Then
			Me.btnDelete.Enabled = True
		Else
			Me.btnDelete.Enabled = False
		End If

		If (Me.MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And _
		 ((Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord)) And _
		 (Check_IsMilestonePrice.Checked = False) Then

			Me.Combo_PriceInstrument.Enabled = True
			Me.Date_PriceDate.Enabled = True

			Me.Check_IsFinalPrice.Enabled = True
			Me.Check_IsMilestonePrice.Enabled = False	' Always False !!!
			Me.Check_IsAdministratorPrice.Enabled = True

			If Me.Check_PercentIncrease.CheckState = CheckState.Checked Then
				Me.Check_PercentIncrease.Enabled = True
				Me.edit_PricePercent.Enabled = True
				Me.Combo_PriceReferenceDate.Enabled = True
				Me.edit_BasePrice.Enabled = False

				Me.edit_PriceNAV.Enabled = False
			Else
				Me.Check_PercentIncrease.Enabled = True
				Me.edit_PricePercent.Enabled = False
				Me.Combo_PriceReferenceDate.Enabled = False
				Me.edit_BasePrice.Enabled = False

				Me.edit_PriceNAV.Enabled = True
			End If

		Else

			Me.Combo_PriceInstrument.Enabled = False
			Me.Date_PriceDate.Enabled = False
			Me.Check_IsFinalPrice.Enabled = False
			Me.Check_IsMilestonePrice.Enabled = False
			Me.Check_IsAdministratorPrice.Enabled = False
			Me.Check_PercentIncrease.Enabled = False
			Me.edit_PricePercent.Enabled = False
			Me.Combo_PriceReferenceDate.Enabled = False
			Me.edit_BasePrice.Enabled = False
			Me.edit_PriceNAV.Enabled = False

		End If

	End Sub

    ''' <summary>
    ''' Validates the form.
    ''' </summary>
    ''' <param name="pReturnString">The p return string.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	Private Function ValidateForm(ByRef pReturnString As String) As Boolean
		' Form Validation code.
		' 
		' This code should be the final arbiter of what is allowed. no assumptions regarding 
		' prior validation should be made.
		' 
		' This Code is called by the SetFormData routine before position changes.
		'
		Dim RVal As Boolean

		RVal = True
		pReturnString = ""

		' Validate 

		If Me.Combo_PriceInstrument.SelectedIndex < 0 Then
			pReturnString = "Price Instrument must be selected."
			RVal = False
		End If

		If (RVal = True) And (IsDate(Me.Date_PriceDate.Value) = False) Then
			pReturnString = "Price Date must be valid."
			RVal = False
		End If

		If (RVal = True) And (Me.Check_PercentIncrease.CheckState = CheckState.Checked) Then
			If (RVal = True) And (Me.Check_IsFinalPrice.Checked) Then
				pReturnString = "Percentage change prices may not be entered as Final prices."
				RVal = False
			End If

			If (RVal = True) And (IsNumeric(Me.edit_PricePercent.Value) = False) Then
				pReturnString = "Price Percent move must be a valid number."
				RVal = False
			End If

			If (RVal = True) And (IsDate(Me.Combo_PriceReferenceDate.SelectedValue) = False) Then
				pReturnString = "Price Reference Date must be valid."
				RVal = False
			End If

			If (RVal = True) And (IsNumeric(Me.edit_BasePrice.Value) = False) Then
				pReturnString = "Base (Reference) Price is not valid."
				RVal = False
			End If

			If (CType(Me.Combo_PriceReferenceDate.SelectedValue, Date) >= Me.Date_PriceDate.Value) Then
				pReturnString = "Base (Reference) Price must be before the Price Date."
				RVal = False
			End If
		End If

		If (RVal = True) And (IsNumeric(Me.edit_PriceNAV.Value) = False) Then
			pReturnString = "Price Instrument must be selected."
			RVal = False
		End If

		Return RVal

	End Function

    ''' <summary>
    ''' Handles the MouseEnter event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnCancel_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseEnter
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = True
	End Sub

    ''' <summary>
    ''' Handles the MouseLeave event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnCancel_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseLeave
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = False
	End Sub


#End Region

#Region " Form Specific Functions and Event code "

    ''' <summary>
    ''' Gets the base NAV.
    ''' </summary>
    ''' <param name="pPriceInstrument">The p price instrument.</param>
    ''' <param name="pPriceBaseDate">The p price base date.</param>
    ''' <returns>System.Double.</returns>
	Private Function GetBaseNAV(ByVal pPriceInstrument As Integer, ByVal pPriceBaseDate As Date) As Double
		' ***************************************************************
		' Function to return the price for a given Instrument on a Given Date
		' 
		' Price is returned from the 'myTable' table. The assumption is that this is the 
		' frmPrice Form !
		' ***************************************************************
		Dim SelectedRows() As DataRow
		Dim SelectString As String

		If (pPriceInstrument <= 0) Then
			Return 0
		End If

    SelectString = "(PriceInstrument=" & pPriceInstrument.ToString & ") AND (PriceDate='" & pPriceBaseDate.ToString(QUERY_SHORTDATEFORMAT) & "')"
		SelectedRows = myTable.Select(SelectString)
		If (SelectedRows.Length > 0) Then
			Return SelectedRows(0)("PriceNAV")
		Else
			Return (0)
		End If


	End Function


    ''' <summary>
    ''' Handles the CheckedChanged event of the Check_PercentIncrease control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Check_PercentIncrease_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_PercentIncrease.CheckedChanged
		' *************************************************************************************
		' 
		' *************************************************************************************

		If Me.InPaint = False Then
			Call SetButtonStatus()

			If Me.Check_PercentIncrease.Checked = True Then

				' If the 'Percent Incerease' box is checked and no Reference Date is selected then
				' attempt to select the last date in the prior month.

				If (Me.Combo_PriceReferenceDate.SelectedIndex < 0) AndAlso (Combo_PriceReferenceDate.Items.Count > 0) Then
					Dim Counter As Integer
					Dim thisDate As Date
					Dim PriorMonth As Date

					' Establish desired reference date.
					PriorMonth = AddPeriodToDate(DealingPeriod.Monthly, Me.Date_PriceDate.Value.Date, -1)

					' Cycle through the Combo looking for the desired reference date
					For Counter = 0 To (Combo_PriceReferenceDate.Items.Count - 1)
						Try
							thisDate = CDate(Combo_PriceReferenceDate.Items(Counter)(Combo_PriceReferenceDate.ValueMember))
							If (thisDate.CompareTo(PriorMonth) <= 0) Then
								Combo_PriceReferenceDate.SelectedIndex = Counter
								Exit For
							End If
						Catch ex As Exception
						End Try
					Next

					If (Combo_PriceReferenceDate.SelectedIndex < 0) Then
						Try
							Combo_PriceReferenceDate.SelectedIndex = 0
						Catch ex As Exception
						End Try
					End If
				End If

				Call CalculatePriceLevel(Me, New EventArgs)
			End If
		End If
	End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_PriceReferenceDate control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_PriceReferenceDate_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_PriceReferenceDate.SelectedIndexChanged
		' *********************************************************************************************
		'
		'
		' *********************************************************************************************

		Try
			If (Me.IsDisposed) Then
				Exit Sub
			End If

			If InPaint = False Then
				If (Combo_PriceInstrument.SelectedIndex >= 0) AndAlso (Combo_PriceReferenceDate.SelectedIndex >= 0) Then
					Try
						Me.edit_BasePrice.Value = GetBaseNAV(Combo_PriceInstrument.SelectedValue, Combo_PriceReferenceDate.SelectedValue)
					Catch ex As Exception
					End Try
				Else
					Me.edit_BasePrice.Value = 0
				End If
			End If

		Catch ex As Exception
		End Try

	End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_PriceInstrument control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_PriceInstrument_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_PriceInstrument.SelectedIndexChanged
		' *********************************************************************************************
		' If the user changes the Instrument for this price, e.g. if this is a new price, then
		' Update the Reference Date combo and the Base Price field.
		' *********************************************************************************************

		Try
			If (Me.IsDisposed) Then
				Exit Sub
			End If

			If (Me.InPaint = False) AndAlso (Combo_PriceInstrument.SelectedIndex >= 0) AndAlso (IsNumeric(Combo_PriceInstrument.SelectedValue)) AndAlso (CInt(Combo_PriceInstrument.SelectedValue) > 0) Then

				If (CInt(THIS_FORM_FirstSelectingCombo.SelectedValue) = CInt(Combo_PriceInstrument.SelectedValue)) Then
					' If This Instrument = Form Instrument, then use the form data set.

					Call MainForm.SetTblGenericCombo( _
					 Me.Combo_PriceReferenceDate, _
					 myTable.Select("True", "PriceDate DESC"), _
					 "PriceDate", _
					 "PriceDate", _
					 "PriceInstrument = " & Combo_PriceInstrument.SelectedValue.ToString, _
					 True, _
					 False)

				Else

					Dim PriceTable As New RenaissanceDataClass.DSPrice.tblPriceDataTable

					LoadSelectedPriceData(PriceTable, CInt(Combo_PriceInstrument.SelectedValue))

					Call MainForm.SetTblGenericCombo( _
					 Me.Combo_PriceReferenceDate, _
					 PriceTable.Select("True", "PriceDate DESC"), _
					 "PriceDate", _
					 "PriceDate", _
					 "PriceInstrument = " & Combo_PriceInstrument.SelectedValue.ToString, _
					 True, _
					 False)

					PriceTable.Clear()
					PriceTable = Nothing
				End If

				Call Combo_PriceReferenceDate_SelectedIndexChanged(Me, New EventArgs)
			End If
		Catch ex As Exception
		End Try

	End Sub

    ''' <summary>
    ''' Handles the ValueChanged event of the edit_PriceNAV control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub edit_PriceNAV_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles edit_PriceNAV.ValueChanged
		' *********************************************************************************
		'
		' *********************************************************************************

		Try

			Label_AdjustedPrice.Text = (edit_PriceNAV.Value * edit_Multiplier.Value).ToString("#,##0.00######")

		Catch ex As Exception
		End Try

	End Sub

    ''' <summary>
    ''' Handles the ValueChanged event of the edit_Multiplier control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub edit_Multiplier_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles edit_Multiplier.ValueChanged
		' *********************************************************************************
		'
		' *********************************************************************************

		Try

			Label_AdjustedPrice.Text = (edit_PriceNAV.Value * edit_Multiplier.Value).ToString("#,##0.00######")

		Catch ex As Exception
		End Try

	End Sub

    ''' <summary>
    ''' Handles the TextChanged event of the edit_Multiplier control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub edit_Multiplier_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles edit_Multiplier.TextChanged
		' *********************************************************************************
		'
		' *********************************************************************************

		Try

			If (IsNumeric(edit_Multiplier.Text)) Then

				Label_AdjustedPrice.Text = (edit_PriceNAV.Value * CDbl(edit_Multiplier.Text)).ToString("#,##0.00######")

			End If

		Catch ex As Exception
		End Try

	End Sub

#End Region

#Region " Navigation Code / GetPosition() (Generic Code) "

    ''' <summary>
    ''' Loads the selected price data.
    ''' </summary>
    ''' <param name="pPriceTable">The p price table.</param>
    ''' <param name="pInstrumentID">The p instrument ID.</param>
	Private Sub LoadSelectedPriceData(ByRef pPriceTable As RenaissanceDataClass.DSPrice.tblPriceDataTable, ByVal pInstrumentID As Object)
		' ************************************************************************************
		'
		'
		' ************************************************************************************

		Try

			If (pPriceTable Is Nothing) Then
				pPriceTable = New RenaissanceDataClass.DSPrice.tblPriceDataTable
			Else
				pPriceTable.Clear()
			End If

			If IsNumeric(pInstrumentID) AndAlso (CInt(pInstrumentID) > 0) Then
				myAdaptor.SelectCommand.Parameters("@InstrumentID").Value = CInt(pInstrumentID)
			Else
				myAdaptor.SelectCommand.Parameters("@InstrumentID").Value = (-1)
			End If

			myAdaptor.SelectCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

			SyncLock myAdaptor.SelectCommand.Connection
        MainForm.LoadTable_Custom(pPriceTable, myAdaptor.SelectCommand)
        'pPriceTable.Load(myAdaptor.SelectCommand.ExecuteReader)
      End SyncLock

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error loading Price Data (LoadSelectedPriceData)", ex.Message, ex.StackTrace, True)
      pPriceTable.Clear()
    End Try

  End Sub

  ''' <summary>
  ''' Handles the FirstSelectComboChanged event of the Combo control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_FirstSelectComboChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' ************************************************************************************
    ' Selection Combo. SelectedItem changed.
    '
    ' ************************************************************************************

    Dim OrgInPaint As Boolean

    If InPaint = True Then Exit Sub

    Try

      ' Don't react to changes made in paint routines etc.

      OrgInPaint = InPaint
      InPaint = True

      Try
        If (FormChanged = True) Then
          Call SetFormData()
        End If

        ' Get Instrument Price Data

        LoadSelectedPriceData(myTable, THIS_FORM_FirstSelectingCombo.SelectedValue)

        ' Re-Set Controls etc.
        Call SetSortedRows()

        'myTable.Clear()

        'If THIS_FORM_FirstSelectingCombo.SelectedIndex >= 0 Then
        '	myAdaptor.SelectCommand.Parameters("@InstrumentID").Value = CInt(THIS_FORM_FirstSelectingCombo.SelectedValue)
        'Else
        '	myAdaptor.SelectCommand.Parameters("@InstrumentID").Value = (-1)
        'End If

        'myAdaptor.SelectCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
        'myTable.Load(myAdaptor.SelectCommand.ExecuteReader)

        ' 

        Call SetSecondSelectCombo()
        If THIS_FORM_SecondSelectingCombo.SelectedIndex < 0 Then
          If THIS_FORM_SecondSelectingCombo.Items.Count > 0 Then
            THIS_FORM_SecondSelectingCombo.SelectedIndex = 0
          End If
        End If

        thisPosition = Me.Get_Position()

        If (thisPosition >= 0) Then
          thisDataRow = Me.SortedRows(thisPosition)
        Else
          thisDataRow = Nothing
        End If

        Call GetFormData(thisDataRow)

      Catch ex As Exception
      End Try


    Catch ex As Exception

      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error in Combo_FirstSelectComboChanged()", ex.Message, ex.StackTrace, True)

    Finally

      InPaint = OrgInPaint

    End Try


  End Sub

    ''' <summary>
    ''' Handles the SecondSelectComboChanged event of the Combo control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_SecondSelectComboChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
		' ************************************************************************************
		' Selection Combo. SelectedItem changed.
		'
		' ************************************************************************************

		' Don't react to changes made in paint routines etc.
		If InPaint = True Then Exit Sub

		Try

			If (FormChanged = True) Then
				Call SetFormData()
			End If

			thisPosition = Me.Get_Position()

			If (thisPosition >= 0) Then
				thisDataRow = Me.SortedRows(thisPosition)
			Else
				thisDataRow = Nothing
			End If

			Call GetFormData(thisDataRow)

		Catch ex As Exception

			MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error in Combo_SecondSelectComboChanged()", ex.Message, ex.StackTrace, True)

		End Try
	

	End Sub


    ''' <summary>
    ''' Handles the KeyUp event of the Combo_FirstSelectCombo control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
	Private Sub Combo_FirstSelectCombo_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
		' Key Up Event.
		'
		' Managed the item selection process where the user types the name.
		'
		Dim SelectString As String
		Dim selectRowView As DataRowView
		Dim selectRow As DataRow
		Dim IndexCounter As Integer

		Try

			' Check for <Enter>
			If e.KeyCode = System.Windows.Forms.Keys.Enter Then
				THIS_FORM_FirstSelectingCombo.SelectionStart = 0
				THIS_FORM_FirstSelectingCombo.SelectionLength = THIS_FORM_FirstSelectingCombo.Text.Length
				Exit Sub
			End If

			SelectString = THIS_FORM_FirstSelectingCombo.Text

			If e.KeyCode = System.Windows.Forms.Keys.Back Then
				If SelectString.Length > 0 Then
					SelectString = SelectString.Substring(0, SelectString.Length - 1)
				End If
			End If

			If e.KeyCode = System.Windows.Forms.Keys.Down Then
				Exit Sub
			End If

			If e.KeyCode = System.Windows.Forms.Keys.Down Then
				Exit Sub
			End If

startSearch:

			If SelectString.Length <= 0 Then
				If THIS_FORM_FirstSelectingCombo.Items.Count > 0 Then
					THIS_FORM_FirstSelectingCombo.SelectedIndex = 0
					THIS_FORM_FirstSelectingCombo.SelectionStart = 0
					THIS_FORM_FirstSelectingCombo.SelectionLength = THIS_FORM_FirstSelectingCombo.Text.Length
				Else
					THIS_FORM_FirstSelectingCombo.SelectedIndex = -1
				End If

				Exit Sub
			End If

			For IndexCounter = 0 To (THIS_FORM_FirstSelectingCombo.Items.Count - 1)
				selectRowView = THIS_FORM_FirstSelectingCombo.Items(IndexCounter)
				selectRow = selectRowView.Row
				If selectRow("DM").ToString.ToUpper.StartsWith(SelectString.ToUpper) Then
					If (THIS_FORM_FirstSelectingCombo.SelectedIndex <> IndexCounter) Then
						THIS_FORM_FirstSelectingCombo.SelectedIndex = IndexCounter
						THIS_FORM_FirstSelectingCombo.SelectionStart = SelectString.Length
						THIS_FORM_FirstSelectingCombo.SelectionLength = selectRow("DM").ToString.Length - SelectString.Length
					End If
					Exit Sub
				End If
			Next

			' String not found
			SelectString = SelectString.Substring(0, SelectString.Length - 1)
			GoTo startSearch

		Catch ex As Exception
			Exit Sub
		End Try


	End Sub


    ''' <summary>
    ''' Handles the Click event of the btnNavPrev control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnNavPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavPrev.Click
		' 'Previous' Button.

		Try

			If (FormChanged = True) Then
				Call SetFormData()
			End If

			If thisPosition > 0 Then
				thisPosition -= 1
				thisDataRow = Me.SortedRows(thisPosition)
				InPaint = True

				If CompareValue(THIS_FORM_FirstSelectingCombo.SelectedValue, thisDataRow(THIS_FORM_FirstSelectBy)) <> 0 Then
					THIS_FORM_FirstSelectingCombo.SelectedValue = thisDataRow(THIS_FORM_FirstSelectBy)
					SetSecondSelectCombo()
				End If

				THIS_FORM_SecondSelectingCombo.SelectedValue = thisDataRow(THIS_FORM_SecondSelectBy)

				Application.DoEvents()
				InPaint = False

				Call GetFormData(thisDataRow)

			End If

		Catch ex As Exception

			MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error in btnNavPrev_Click()", ex.Message, ex.StackTrace, True)

		End Try

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnNavNext control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnNavNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavNext.Click
		' 'Next' Button.

		Try

			If (FormChanged = True) Then
				Call SetFormData()
			End If

			If thisPosition < (SortedRows.Length - 1) Then
				thisPosition += 1
				thisDataRow = Me.SortedRows(thisPosition)
				InPaint = True

				If CompareValue(THIS_FORM_FirstSelectingCombo.SelectedValue, thisDataRow(THIS_FORM_FirstSelectBy)) <> 0 Then
					THIS_FORM_FirstSelectingCombo.SelectedValue = thisDataRow(THIS_FORM_FirstSelectBy)
					SetSecondSelectCombo()
				End If

				THIS_FORM_SecondSelectingCombo.SelectedValue = thisDataRow(THIS_FORM_SecondSelectBy)

				Application.DoEvents()
				InPaint = False

				Call GetFormData(thisDataRow)

			End If

		Catch ex As Exception

			MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error in btnNavNext_Click()", ex.Message, ex.StackTrace, True)

		End Try

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnNavFirst control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnNavFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavFirst.Click
		' 'First' Button.

		Try

			If (FormChanged = True) Then
				Call SetFormData()
			End If

			If (thisPosition <> 0) Then
				thisPosition = 0

				InPaint = True

				If (SortedRows.Length > thisPosition) Then
					thisDataRow = Me.SortedRows(thisPosition)

					If CompareValue(THIS_FORM_FirstSelectingCombo.SelectedValue, thisDataRow(THIS_FORM_FirstSelectBy)) <> 0 Then
						THIS_FORM_FirstSelectingCombo.SelectedValue = thisDataRow(THIS_FORM_FirstSelectBy)
						SetSecondSelectCombo()
					End If

					THIS_FORM_SecondSelectingCombo.SelectedValue = thisDataRow(THIS_FORM_SecondSelectBy)

				Else
					thisDataRow = Nothing
				End If

				Application.DoEvents()

			End If

		Catch ex As Exception

			MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error in btnNavFirst_Click()", ex.Message, ex.StackTrace, True)

		Finally

			InPaint = False

		End Try

		Call GetFormData(thisDataRow)

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnLast control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLast.Click
		' 'Last' Button.

		Try

			If (FormChanged = True) Then
				Call SetFormData()
			End If

			If thisPosition < (SortedRows.Length - 1) Then
				thisPosition = (SortedRows.Length - 1)
				thisDataRow = Me.SortedRows(thisPosition)
				InPaint = True

				If CompareValue(THIS_FORM_FirstSelectingCombo.SelectedValue, thisDataRow(THIS_FORM_FirstSelectBy)) <> 0 Then
					THIS_FORM_FirstSelectingCombo.SelectedValue = thisDataRow(THIS_FORM_FirstSelectBy)
					SetSecondSelectCombo()
				End If

				THIS_FORM_SecondSelectingCombo.SelectedValue = thisDataRow(THIS_FORM_SecondSelectBy)

				Application.DoEvents()
				InPaint = False

				Call GetFormData(thisDataRow)

			End If

		Catch ex As Exception

			MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error in btnLast_Click()", ex.Message, ex.StackTrace, True)

		End Try

	End Sub

    ''' <summary>
    ''' Get_s the position.
    ''' </summary>
    ''' <returns>System.Int32.</returns>
	Private Function Get_Position() As Integer
		' ********************************************************************************
		'
		'
		' ********************************************************************************

		Dim SelectedRows() As DataRow
		Dim SelectString As String

		Try

			If SortedRows Is Nothing Then
				Return (-1)
				Exit Function
			End If

			' Return (-1) if there are no rows in the 'SortedRows'

			If (SortedRows.GetLength(0) <= 0) Then
				Return (-1)
				Exit Function
			End If

			' Validate Combos Selected.
			If (THIS_FORM_FirstSelectingCombo.SelectedIndex < 0) Or (THIS_FORM_SecondSelectingCombo.SelectedIndex < 0) Then
				Return (-1)
				Exit Function
			End If

			SelectString = "(" & THIS_FORM_FirstSelectBy & "=" & THIS_FORM_FirstSelectingCombo.SelectedValue.ToString & ") AND (" & THIS_FORM_SecondSelectBy & "='" & CDate(THIS_FORM_SecondSelectingCombo.SelectedValue).ToString(QUERY_SHORTDATEFORMAT) & "')"
			SelectedRows = myTable.Select(SelectString)
			If (SelectedRows.Length > 0) Then
				Return Get_Position(CInt(SelectedRows(0)("AuditID")))
			Else
				Return (-1)
			End If

		Catch ex As Exception

			MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error in Get_Position()", ex.Message, ex.StackTrace, True)

			Return (-1)

		End Try

	End Function

    ''' <summary>
    ''' Get_s the position.
    ''' </summary>
    ''' <param name="pAuditID">The p audit ID.</param>
    ''' <returns>System.Int32.</returns>
	Private Function Get_Position(ByVal pAuditID As Integer) As Integer
		' Returns the position in the current 'SortedRows' array of the entry corresponding to the given 
		' AudidID.

		Dim MinIndex As Integer
		Dim MaxIndex As Integer
		Dim CurrentMin As Integer
		Dim CurrentMax As Integer

		Dim searchDataRow As DataRow

		Try

			' SortedRows exists ?

			If SortedRows Is Nothing Then
				Return (-1)
				Exit Function
			End If

			' Return (-1) if there are no rows in the 'SortedRows'

			If (SortedRows.GetLength(0) <= 0) Then
				Return (-1)
				Exit Function
			End If


			' Use a modified Search moving outwards from the last returned value to locate the required value.
			' Reflecting the fact that for the most part One looks for closely located records.

			MinIndex = 0
			MaxIndex = SortedRows.GetLength(0) - 1


			' Check First and Last records (Incase 'First' or 'Last' was pressed).

			searchDataRow = SortedRows(MinIndex)
			If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(searchDataRow("AuditID")) = pAuditID) Then
				Return MinIndex
				Exit Function
			End If

			searchDataRow = SortedRows(MaxIndex)
			If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(searchDataRow("AuditID")) = pAuditID) Then
				Return MaxIndex
				Exit Function
			End If

			' now search outwards from the last returned value.

			If (Me.thisPosition >= MinIndex) And (Me.thisPosition <= MaxIndex) Then
				CurrentMin = thisPosition
				CurrentMax = CurrentMin + 1
			Else
				CurrentMin = CInt((MinIndex + MaxIndex) / 2)
				CurrentMax = CurrentMin + 1
			End If

			While (CurrentMin >= MinIndex) Or (CurrentMax <= MaxIndex)
				If (CurrentMin >= MinIndex) Then
					searchDataRow = SortedRows(CurrentMin)

					If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(searchDataRow("AuditID")) = pAuditID) Then
						Return CurrentMin
						Exit Function
					End If

					CurrentMin -= 1
				End If

				If (CurrentMax <= MaxIndex) Then
					searchDataRow = SortedRows(CurrentMax)

					If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(searchDataRow("AuditID")) = pAuditID) Then
						Return CurrentMax
						Exit Function
					End If

					CurrentMax += 1
				End If

			End While

			Return (-1)
			Exit Function

		Catch ex As Exception
			MainForm.LogError(Me.Name & ", Get_Position()", LOG_LEVELS.Warning, ex.Message, "", ex.StackTrace, True)
			Return (-1)
			Exit Function
		End Try

	End Function

#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

    ''' <summary>
    ''' Handles the Click event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
		' Cancel Changes, redisplay form.

		FormChanged = False
		AddNewRecord = False

		Me.THIS_FORM_FirstSelectingCombo.Enabled = True
		Me.THIS_FORM_SecondSelectingCombo.Enabled = True

		If (Me.Combo_PriceInstrument.SelectedIndex >= 0) AndAlso IsNumeric(Me.Combo_PriceInstrument.SelectedValue) Then
			THIS_FORM_FirstSelectingCombo.SelectedValue = Combo_PriceInstrument.SelectedValue

			If (thisPosition >= 0) And (thisPosition < Me.SortedRows.GetLength(0)) Then
				thisDataRow = Me.SortedRows(thisPosition)
				Call GetFormData(thisDataRow)
			Else
				Call btnNavFirst_Click(Me, New System.EventArgs)
			End If

		ElseIf (Combo_PriceInstrument.Items.Count > 0) Then
			Combo_PriceInstrument.SelectedIndex = 0

			Call btnNavFirst_Click(Me, New System.EventArgs)
		End If

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnSave control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
		' ********************************************************************
		' Save Changes, if any, without prompting.
		' ********************************************************************

		If (FormChanged) Then

			Try

				Call SetFormData(False)

			Catch ex As Exception

				MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error Saving Instrument Price", ex.Message, ex.StackTrace, True)

			End Try

		End If

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnDelete control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
		Dim Position As Integer
		Dim NextAuditID As Integer

		If (AddNewRecord = True) Then
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Sub
		End If

		' No Appropriate Save permission :-

		If (Me.HasDeletePermission = False) Then
			MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "You do not have permission to Delete this record.", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Sub
		End If

		' *************************************************************
		' KnowledgeDate OK :-
		' *************************************************************

		If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
			MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "Changes to Venice are not allowed when the Knowledgedate is not set to `NOW`", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Sub
		End If

		' *************************************************************
		' Confirm :-
		' *************************************************************
		If MsgBox("Are you Sure ?", MsgBoxStyle.OkCancel, "Confirm Delete") = MsgBoxResult.Cancel Then
			Exit Sub
		End If

		' Check Data position.

		Position = Get_Position(thisAuditID)

		If Position < 0 Then Exit Sub

		' Check Referential Integrity 
		If CheckReferentialIntegrity(_MainForm, Me.SortedRows(Position)) = False Then
			MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "Deleting this record would voilate referential integrity.", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Sub
		End If

		' Resolve row to show after deleting this one.

		NextAuditID = (-1)
		If (Position - 1) >= 0 Then
			NextAuditID = CInt(Me.SortedRows(Position - 1)("AuditID"))
		ElseIf (Position + 1) < Me.SortedRows.GetLength(0) Then
			NextAuditID = CInt(Me.SortedRows(Position + 1)("AuditID"))
		Else
			NextAuditID = (-1)
		End If

		' Delete this row

		Try
			Me.SortedRows(Position).Delete()

			MainForm.AdaptorUpdate(Me.Name, myAdaptor, myTable)

		Catch ex As Exception

			Call MainForm.LogError(Me.Name & ", Delete()", LOG_LEVELS.Error, ex.Message, "Error Deleting Record.", ex.StackTrace, True)
		End Try

		' Tidy Up.

		FormChanged = False

		thisAuditID = NextAuditID
		Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID), True)

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnAdd control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
		' Prepare form to Add a new record.
		Dim CurrentInstrumentID As Integer

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		thisPosition = -1
		InPaint = True

		If Combo_SelectPriceInstrument.SelectedIndex >= 0 Then
			CurrentInstrumentID = Combo_SelectPriceInstrument.SelectedValue
		Else
			CurrentInstrumentID = (-1)
		End If

		MainForm.ClearComboSelection(THIS_FORM_FirstSelectingCombo)
		MainForm.ClearComboSelection(THIS_FORM_SecondSelectingCombo)

		InPaint = False

		GetFormData(Nothing)
		AddNewRecord = True

		If (CurrentInstrumentID > 0) Then
			Combo_PriceInstrument.SelectedValue = CurrentInstrumentID
		End If

		Me.btnCancel.Enabled = True
		Me.THIS_FORM_FirstSelectingCombo.Enabled = False
		Me.THIS_FORM_SecondSelectingCombo.Enabled = False

		Call SetButtonStatus()

		THIS_FORM_NewMoveToControl.Focus()

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnClose control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
		' Close Form

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		Me.Close()

	End Sub

#End Region

#Region " Bug hunting "

	' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
	' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
	' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

    ''' <summary>
    ''' Called when [row updating].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
	Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
		Dim a As Integer

		If Not e.Command Is Nothing Then
			a = 1
		End If

		If Not e.Errors Is Nothing Then
			a = 2
		End If
	End Sub

    ''' <summary>
    ''' Called when [row updated].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
	Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
		Dim a As Integer

		If Not e.Command Is Nothing Then
			a = 1
		End If

		If Not e.Errors Is Nothing Then
			a = 2
		End If
	End Sub

    ''' <summary>
    ''' Called when [row fill error].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
	Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
		Dim a As Integer

		a = 1
	End Sub

#End Region

#Region " Highlight Live positions Menu"

    ''' <summary>
    ''' Sets the highligh live positions menu.
    ''' </summary>
    ''' <param name="RootMenu">The root menu.</param>
    ''' <returns>MenuStrip.</returns>
	Private Function SetHighlighLivePositionsMenu(ByRef RootMenu As MenuStrip) As MenuStrip

		Try
			Dim ReportMenu As New ToolStripMenuItem("&Highlight")
			Dim newMenuItem As ToolStripMenuItem

			newMenuItem = ReportMenu.DropDownItems.Add("Highlight 'Live' Instruments.", Nothing, AddressOf Me.HighlightInstruments)

			RootMenu.Items.Add(ReportMenu)
		Catch ex As Exception
		End Try

		Return RootMenu

	End Function

    ''' <summary>
    ''' Highlights the instruments.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Private Sub HighlightInstruments(ByVal sender As Object, ByVal e As EventArgs)
		Dim ReportMenu As ToolStripMenuItem

		Try
			ReportMenu = CType(sender, ToolStripMenuItem)
			ReportMenu.Checked = Not ReportMenu.Checked

			HighlightLivePositions = ReportMenu.Checked
			Call SetFirstSelectCombo()

			Call Combo_FirstSelectComboChanged(Me, New EventArgs)
		Catch ex As Exception
		End Try

	End Sub



#End Region


End Class
