' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmFundType.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals


''' <summary>
''' Class frmFundType
''' </summary>
Public Class frmFundType

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmFundType"/> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The LBL fund type description
    ''' </summary>
  Friend WithEvents lblFundTypeDescription As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit audit ID
    ''' </summary>
  Friend WithEvents editAuditID As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The edit fund type description
    ''' </summary>
  Friend WithEvents editFundTypeDescription As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The BTN nav first
    ''' </summary>
  Friend WithEvents btnNavFirst As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN nav prev
    ''' </summary>
  Friend WithEvents btnNavPrev As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN nav next
    ''' </summary>
  Friend WithEvents btnNavNext As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN last
    ''' </summary>
  Friend WithEvents btnLast As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN add
    ''' </summary>
  Friend WithEvents btnAdd As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN delete
    ''' </summary>
  Friend WithEvents btnDelete As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN cancel
    ''' </summary>
  Friend WithEvents btnCancel As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN save
    ''' </summary>
  Friend WithEvents btnSave As System.Windows.Forms.Button
    ''' <summary>
    ''' The combo_ select fund type description
    ''' </summary>
  Friend WithEvents Combo_SelectFundTypeDescription As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The panel1
    ''' </summary>
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The label1
    ''' </summary>
  Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The group box1
    ''' </summary>
  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The BTN close
    ''' </summary>
  Friend WithEvents btnClose As System.Windows.Forms.Button
    ''' <summary>
    ''' The combo_ group
    ''' </summary>
  Friend WithEvents Combo_Group As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ pertrac index
    ''' </summary>
	Friend WithEvents Combo_PertracIndex As FCP_TelerikControls.FCP_RadComboBox
    ''' <summary>
    ''' The edit_ short volatility
    ''' </summary>
  Friend WithEvents edit_ShortVolatility As C1.Win.C1Input.C1TextBox
    ''' <summary>
    ''' The numeric_ sort order
    ''' </summary>
  Friend WithEvents Numeric_SortOrder As System.Windows.Forms.NumericUpDown
    ''' <summary>
    ''' The LBL group name
    ''' </summary>
  Friend WithEvents lblGroupName As System.Windows.Forms.Label
    ''' <summary>
    ''' The LBL pertrac index
    ''' </summary>
  Friend WithEvents lblPertracIndex As System.Windows.Forms.Label
    ''' <summary>
    ''' The LBL volatility
    ''' </summary>
  Friend WithEvents lblVolatility As System.Windows.Forms.Label
    ''' <summary>
    ''' The LBL sort order
    ''' </summary>
  Friend WithEvents lblSortOrder As System.Windows.Forms.Label
    ''' <summary>
    ''' The check_ administrative fund type
    ''' </summary>
  Friend WithEvents Check_AdministrativeFundType As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The combo_ benchmark index
    ''' </summary>
  Friend WithEvents Combo_BenchmarkIndex As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label19
    ''' </summary>
  Friend WithEvents Label19 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label20
    ''' </summary>
  Friend WithEvents Label20 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ instrument index sector
    ''' </summary>
  Friend WithEvents Combo_InstrumentIndexSector As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The root menu
    ''' </summary>
  Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
    ''' <summary>
    ''' The combo_ attribution group
    ''' </summary>
  Friend WithEvents Combo_AttributionGroup As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label2
    ''' </summary>
	Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The group box2
    ''' </summary>
	Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The check_ show in reporter
    ''' </summary>
	Friend WithEvents Check_ShowInReporter As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The label3
    ''' </summary>
	Friend WithEvents Label3 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ reporter sector
    ''' </summary>
	Friend WithEvents Combo_ReporterSector As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label4
    ''' </summary>
	Friend WithEvents Label4 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label5
    ''' </summary>
	Friend WithEvents Label5 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ adminfund type
    ''' </summary>
	Friend WithEvents Label_AdminfundType As System.Windows.Forms.Label
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Me.lblFundTypeDescription = New System.Windows.Forms.Label
		Me.lblGroupName = New System.Windows.Forms.Label
		Me.lblPertracIndex = New System.Windows.Forms.Label
		Me.editAuditID = New System.Windows.Forms.TextBox
		Me.editFundTypeDescription = New System.Windows.Forms.TextBox
		Me.lblVolatility = New System.Windows.Forms.Label
		Me.lblSortOrder = New System.Windows.Forms.Label
		Me.btnNavFirst = New System.Windows.Forms.Button
		Me.btnNavPrev = New System.Windows.Forms.Button
		Me.btnNavNext = New System.Windows.Forms.Button
		Me.btnLast = New System.Windows.Forms.Button
		Me.btnAdd = New System.Windows.Forms.Button
		Me.btnDelete = New System.Windows.Forms.Button
		Me.btnCancel = New System.Windows.Forms.Button
		Me.btnSave = New System.Windows.Forms.Button
		Me.Combo_SelectFundTypeDescription = New System.Windows.Forms.ComboBox
		Me.Panel1 = New System.Windows.Forms.Panel
		Me.Label1 = New System.Windows.Forms.Label
		Me.GroupBox1 = New System.Windows.Forms.GroupBox
		Me.btnClose = New System.Windows.Forms.Button
		Me.Combo_Group = New System.Windows.Forms.ComboBox
		Me.Combo_PertracIndex = New FCP_TelerikControls.FCP_RadComboBox
		Me.edit_ShortVolatility = New C1.Win.C1Input.C1TextBox
		Me.Numeric_SortOrder = New System.Windows.Forms.NumericUpDown
		Me.Check_AdministrativeFundType = New System.Windows.Forms.CheckBox
		Me.Label_AdminfundType = New System.Windows.Forms.Label
		Me.Combo_BenchmarkIndex = New System.Windows.Forms.ComboBox
		Me.Label19 = New System.Windows.Forms.Label
		Me.Label20 = New System.Windows.Forms.Label
		Me.Combo_InstrumentIndexSector = New System.Windows.Forms.ComboBox
		Me.RootMenu = New System.Windows.Forms.MenuStrip
		Me.Combo_AttributionGroup = New System.Windows.Forms.ComboBox
		Me.Label2 = New System.Windows.Forms.Label
		Me.GroupBox2 = New System.Windows.Forms.GroupBox
		Me.Check_ShowInReporter = New System.Windows.Forms.CheckBox
		Me.Label3 = New System.Windows.Forms.Label
		Me.Combo_ReporterSector = New System.Windows.Forms.ComboBox
		Me.Label4 = New System.Windows.Forms.Label
		Me.Label5 = New System.Windows.Forms.Label
		Me.Panel1.SuspendLayout()
		CType(Me.edit_ShortVolatility, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.Numeric_SortOrder, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.SuspendLayout()
		'
		'lblFundTypeDescription
		'
		Me.lblFundTypeDescription.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.lblFundTypeDescription.Location = New System.Drawing.Point(8, 79)
		Me.lblFundTypeDescription.Name = "lblFundTypeDescription"
		Me.lblFundTypeDescription.Size = New System.Drawing.Size(100, 20)
		Me.lblFundTypeDescription.TabIndex = 39
		Me.lblFundTypeDescription.Text = "Fund Type"
		'
		'lblGroupName
		'
		Me.lblGroupName.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.lblGroupName.Location = New System.Drawing.Point(8, 105)
		Me.lblGroupName.Name = "lblGroupName"
		Me.lblGroupName.Size = New System.Drawing.Size(100, 20)
		Me.lblGroupName.TabIndex = 40
		Me.lblGroupName.Text = "Group Name"
		'
		'lblPertracIndex
		'
		Me.lblPertracIndex.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.lblPertracIndex.Location = New System.Drawing.Point(7, 133)
		Me.lblPertracIndex.Name = "lblPertracIndex"
		Me.lblPertracIndex.Size = New System.Drawing.Size(100, 20)
		Me.lblPertracIndex.TabIndex = 41
		Me.lblPertracIndex.Text = "Pertrac Index"
		'
		'editAuditID
		'
		Me.editAuditID.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.editAuditID.Enabled = False
		Me.editAuditID.Location = New System.Drawing.Point(312, 32)
		Me.editAuditID.Name = "editAuditID"
		Me.editAuditID.Size = New System.Drawing.Size(64, 20)
		Me.editAuditID.TabIndex = 1
		'
		'editFundTypeDescription
		'
		Me.editFundTypeDescription.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.editFundTypeDescription.Location = New System.Drawing.Point(120, 76)
		Me.editFundTypeDescription.Name = "editFundTypeDescription"
		Me.editFundTypeDescription.Size = New System.Drawing.Size(256, 20)
		Me.editFundTypeDescription.TabIndex = 2
		'
		'lblVolatility
		'
		Me.lblVolatility.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.lblVolatility.Location = New System.Drawing.Point(7, 241)
		Me.lblVolatility.Name = "lblVolatility"
		Me.lblVolatility.Size = New System.Drawing.Size(106, 20)
		Me.lblVolatility.TabIndex = 48
		Me.lblVolatility.Text = "Short Term Volatilty"
		'
		'lblSortOrder
		'
		Me.lblSortOrder.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.lblSortOrder.Location = New System.Drawing.Point(7, 266)
		Me.lblSortOrder.Name = "lblSortOrder"
		Me.lblSortOrder.Size = New System.Drawing.Size(106, 16)
		Me.lblSortOrder.TabIndex = 49
		Me.lblSortOrder.Text = "Sort Order"
		'
		'btnNavFirst
		'
		Me.btnNavFirst.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnNavFirst.Location = New System.Drawing.Point(8, 8)
		Me.btnNavFirst.Name = "btnNavFirst"
		Me.btnNavFirst.Size = New System.Drawing.Size(40, 28)
		Me.btnNavFirst.TabIndex = 0
		Me.btnNavFirst.Text = "<<"
		'
		'btnNavPrev
		'
		Me.btnNavPrev.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnNavPrev.Location = New System.Drawing.Point(50, 8)
		Me.btnNavPrev.Name = "btnNavPrev"
		Me.btnNavPrev.Size = New System.Drawing.Size(35, 28)
		Me.btnNavPrev.TabIndex = 1
		Me.btnNavPrev.Text = "<"
		'
		'btnNavNext
		'
		Me.btnNavNext.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnNavNext.Location = New System.Drawing.Point(88, 8)
		Me.btnNavNext.Name = "btnNavNext"
		Me.btnNavNext.Size = New System.Drawing.Size(35, 28)
		Me.btnNavNext.TabIndex = 2
		Me.btnNavNext.Text = ">"
		'
		'btnLast
		'
		Me.btnLast.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnLast.Location = New System.Drawing.Point(125, 8)
		Me.btnLast.Name = "btnLast"
		Me.btnLast.Size = New System.Drawing.Size(40, 28)
		Me.btnLast.TabIndex = 3
		Me.btnLast.Text = ">>"
		'
		'btnAdd
		'
		Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnAdd.Location = New System.Drawing.Point(176, 8)
		Me.btnAdd.Name = "btnAdd"
		Me.btnAdd.Size = New System.Drawing.Size(75, 28)
		Me.btnAdd.TabIndex = 4
		Me.btnAdd.Text = "&New"
		'
		'btnDelete
		'
		Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnDelete.Location = New System.Drawing.Point(114, 478)
		Me.btnDelete.Name = "btnDelete"
		Me.btnDelete.Size = New System.Drawing.Size(75, 28)
		Me.btnDelete.TabIndex = 15
		Me.btnDelete.Text = "&Delete"
		'
		'btnCancel
		'
		Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
		Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnCancel.Location = New System.Drawing.Point(198, 478)
		Me.btnCancel.Name = "btnCancel"
		Me.btnCancel.Size = New System.Drawing.Size(75, 28)
		Me.btnCancel.TabIndex = 16
		Me.btnCancel.Text = "&Cancel"
		'
		'btnSave
		'
		Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnSave.Location = New System.Drawing.Point(26, 478)
		Me.btnSave.Name = "btnSave"
		Me.btnSave.Size = New System.Drawing.Size(75, 28)
		Me.btnSave.TabIndex = 14
		Me.btnSave.Text = "&Save"
		'
		'Combo_SelectFundTypeDescription
		'
		Me.Combo_SelectFundTypeDescription.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_SelectFundTypeDescription.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_SelectFundTypeDescription.Location = New System.Drawing.Point(120, 32)
		Me.Combo_SelectFundTypeDescription.Name = "Combo_SelectFundTypeDescription"
		Me.Combo_SelectFundTypeDescription.Size = New System.Drawing.Size(168, 21)
		Me.Combo_SelectFundTypeDescription.TabIndex = 0
		'
		'Panel1
		'
		Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Panel1.Controls.Add(Me.btnNavFirst)
		Me.Panel1.Controls.Add(Me.btnNavPrev)
		Me.Panel1.Controls.Add(Me.btnNavNext)
		Me.Panel1.Controls.Add(Me.btnLast)
		Me.Panel1.Controls.Add(Me.btnAdd)
		Me.Panel1.Location = New System.Drawing.Point(62, 422)
		Me.Panel1.Name = "Panel1"
		Me.Panel1.Size = New System.Drawing.Size(260, 48)
		Me.Panel1.TabIndex = 13
		'
		'Label1
		'
		Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label1.Location = New System.Drawing.Point(8, 32)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(100, 23)
		Me.Label1.TabIndex = 30
		Me.Label1.Text = "Select"
		'
		'GroupBox1
		'
		Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.GroupBox1.Location = New System.Drawing.Point(8, 64)
		Me.GroupBox1.Name = "GroupBox1"
		Me.GroupBox1.Size = New System.Drawing.Size(368, 4)
		Me.GroupBox1.TabIndex = 77
		Me.GroupBox1.TabStop = False
		Me.GroupBox1.Text = "GroupBox1"
		'
		'btnClose
		'
		Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnClose.Location = New System.Drawing.Point(282, 478)
		Me.btnClose.Name = "btnClose"
		Me.btnClose.Size = New System.Drawing.Size(75, 28)
		Me.btnClose.TabIndex = 17
		Me.btnClose.Text = "&Close"
		'
		'Combo_Group
		'
		Me.Combo_Group.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_Group.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_Group.Location = New System.Drawing.Point(120, 102)
		Me.Combo_Group.Name = "Combo_Group"
		Me.Combo_Group.Size = New System.Drawing.Size(256, 21)
		Me.Combo_Group.TabIndex = 3
		'
		'Combo_PertracIndex
		'
		Me.Combo_PertracIndex.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_PertracIndex.Location = New System.Drawing.Point(120, 130)
		Me.Combo_PertracIndex.Name = "Combo_PertracIndex"
		'
		'
		'
		Me.Combo_PertracIndex.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
		Me.Combo_PertracIndex.SelectNoMatch = False
		Me.Combo_PertracIndex.Size = New System.Drawing.Size(256, 21)
		Me.Combo_PertracIndex.TabIndex = 4
		Me.Combo_PertracIndex.ThemeName = "ControlDefault"
		'
		'edit_ShortVolatility
		'
		Me.edit_ShortVolatility.AutoSize = False
		Me.edit_ShortVolatility.DataType = GetType(Double)
		Me.edit_ShortVolatility.DisableOnNoData = False
		Me.edit_ShortVolatility.EditFormat.CustomFormat = "#####0.0#####"
		Me.edit_ShortVolatility.EditFormat.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat
		Me.edit_ShortVolatility.EditFormat.Inherit = CType((((C1.Win.C1Input.FormatInfoInheritFlags.NullText Or C1.Win.C1Input.FormatInfoInheritFlags.EmptyAsNull) _
								Or C1.Win.C1Input.FormatInfoInheritFlags.TrimStart) _
								Or C1.Win.C1Input.FormatInfoInheritFlags.TrimEnd), C1.Win.C1Input.FormatInfoInheritFlags)
		Me.edit_ShortVolatility.ErrorInfo.ErrorAction = C1.Win.C1Input.ErrorActionEnum.ResetValue
		Me.edit_ShortVolatility.ErrorInfo.ErrorMessage = "Validation Error"
		Me.edit_ShortVolatility.ErrorInfo.ShowErrorMessage = False
		Me.edit_ShortVolatility.FormatType = C1.Win.C1Input.FormatTypeEnum.Percent
		Me.edit_ShortVolatility.Location = New System.Drawing.Point(120, 238)
		Me.edit_ShortVolatility.MaxLength = 100
		Me.edit_ShortVolatility.Name = "edit_ShortVolatility"
		Me.edit_ShortVolatility.ShowContextMenu = False
		Me.edit_ShortVolatility.Size = New System.Drawing.Size(104, 20)
		Me.edit_ShortVolatility.TabIndex = 8
		Me.edit_ShortVolatility.Tag = Nothing
		Me.edit_ShortVolatility.WordWrap = False
		'
		'Numeric_SortOrder
		'
		Me.Numeric_SortOrder.Location = New System.Drawing.Point(120, 264)
		Me.Numeric_SortOrder.Name = "Numeric_SortOrder"
		Me.Numeric_SortOrder.Size = New System.Drawing.Size(104, 20)
		Me.Numeric_SortOrder.TabIndex = 9
		'
		'Check_AdministrativeFundType
		'
		Me.Check_AdministrativeFundType.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Check_AdministrativeFundType.Location = New System.Drawing.Point(120, 291)
		Me.Check_AdministrativeFundType.Name = "Check_AdministrativeFundType"
		Me.Check_AdministrativeFundType.Size = New System.Drawing.Size(16, 24)
		Me.Check_AdministrativeFundType.TabIndex = 10
		'
		'Label_AdminfundType
		'
		Me.Label_AdminfundType.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label_AdminfundType.Location = New System.Drawing.Point(8, 291)
		Me.Label_AdminfundType.Name = "Label_AdminfundType"
		Me.Label_AdminfundType.Size = New System.Drawing.Size(108, 32)
		Me.Label_AdminfundType.TabIndex = 88
		Me.Label_AdminfundType.Text = "Administrative Fund Type"
		'
		'Combo_BenchmarkIndex
		'
		Me.Combo_BenchmarkIndex.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_BenchmarkIndex.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_BenchmarkIndex.Location = New System.Drawing.Point(120, 157)
		Me.Combo_BenchmarkIndex.Name = "Combo_BenchmarkIndex"
		Me.Combo_BenchmarkIndex.Size = New System.Drawing.Size(256, 21)
		Me.Combo_BenchmarkIndex.TabIndex = 5
		'
		'Label19
		'
		Me.Label19.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label19.Location = New System.Drawing.Point(8, 160)
		Me.Label19.Name = "Label19"
		Me.Label19.Size = New System.Drawing.Size(100, 18)
		Me.Label19.TabIndex = 120
		Me.Label19.Text = "BenchMark Index"
		'
		'Label20
		'
		Me.Label20.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label20.Location = New System.Drawing.Point(8, 187)
		Me.Label20.Name = "Label20"
		Me.Label20.Size = New System.Drawing.Size(91, 18)
		Me.Label20.TabIndex = 122
		Me.Label20.Text = "Index Sector"
		'
		'Combo_InstrumentIndexSector
		'
		Me.Combo_InstrumentIndexSector.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_InstrumentIndexSector.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_InstrumentIndexSector.Location = New System.Drawing.Point(120, 184)
		Me.Combo_InstrumentIndexSector.Name = "Combo_InstrumentIndexSector"
		Me.Combo_InstrumentIndexSector.Size = New System.Drawing.Size(256, 21)
		Me.Combo_InstrumentIndexSector.TabIndex = 6
		'
		'RootMenu
		'
		Me.RootMenu.Location = New System.Drawing.Point(0, 0)
		Me.RootMenu.Name = "RootMenu"
		Me.RootMenu.Size = New System.Drawing.Size(384, 24)
		Me.RootMenu.TabIndex = 18
		Me.RootMenu.Text = " "
		'
		'Combo_AttributionGroup
		'
		Me.Combo_AttributionGroup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_AttributionGroup.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_AttributionGroup.Location = New System.Drawing.Point(120, 211)
		Me.Combo_AttributionGroup.Name = "Combo_AttributionGroup"
		Me.Combo_AttributionGroup.Size = New System.Drawing.Size(256, 21)
		Me.Combo_AttributionGroup.TabIndex = 7
		'
		'Label2
		'
		Me.Label2.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label2.Location = New System.Drawing.Point(8, 214)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(108, 18)
		Me.Label2.TabIndex = 125
		Me.Label2.Text = "Attribution Group"
		'
		'GroupBox2
		'
		Me.GroupBox2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.GroupBox2.Location = New System.Drawing.Point(8, 324)
		Me.GroupBox2.Name = "GroupBox2"
		Me.GroupBox2.Size = New System.Drawing.Size(368, 4)
		Me.GroupBox2.TabIndex = 126
		Me.GroupBox2.TabStop = False
		Me.GroupBox2.Text = "GroupBox2"
		'
		'Check_ShowInReporter
		'
		Me.Check_ShowInReporter.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Check_ShowInReporter.Location = New System.Drawing.Point(119, 378)
		Me.Check_ShowInReporter.Name = "Check_ShowInReporter"
		Me.Check_ShowInReporter.Size = New System.Drawing.Size(16, 17)
		Me.Check_ShowInReporter.TabIndex = 12
		'
		'Label3
		'
		Me.Label3.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label3.Location = New System.Drawing.Point(7, 378)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(108, 17)
		Me.Label3.TabIndex = 128
		Me.Label3.Text = "Show in Reporter"
		'
		'Combo_ReporterSector
		'
		Me.Combo_ReporterSector.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_ReporterSector.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_ReporterSector.Location = New System.Drawing.Point(120, 351)
		Me.Combo_ReporterSector.Name = "Combo_ReporterSector"
		Me.Combo_ReporterSector.Size = New System.Drawing.Size(256, 21)
		Me.Combo_ReporterSector.TabIndex = 11
		'
		'Label4
		'
		Me.Label4.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label4.Location = New System.Drawing.Point(8, 354)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(108, 18)
		Me.Label4.TabIndex = 130
		Me.Label4.Text = "Attribution Sector"
		'
		'Label5
		'
		Me.Label5.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label5.Location = New System.Drawing.Point(8, 331)
		Me.Label5.Name = "Label5"
		Me.Label5.Size = New System.Drawing.Size(368, 18)
		Me.Label5.TabIndex = 131
		Me.Label5.Text = "Monthly Reporter Application parameters :"
		'
		'frmFundType
		'
		Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
		Me.CancelButton = Me.btnCancel
		Me.ClientSize = New System.Drawing.Size(384, 514)
		Me.Controls.Add(Me.Label5)
		Me.Controls.Add(Me.Combo_ReporterSector)
		Me.Controls.Add(Me.Label4)
		Me.Controls.Add(Me.Check_ShowInReporter)
		Me.Controls.Add(Me.Label3)
		Me.Controls.Add(Me.GroupBox2)
		Me.Controls.Add(Me.Combo_AttributionGroup)
		Me.Controls.Add(Me.Label2)
		Me.Controls.Add(Me.RootMenu)
		Me.Controls.Add(Me.Label20)
		Me.Controls.Add(Me.Combo_InstrumentIndexSector)
		Me.Controls.Add(Me.Combo_BenchmarkIndex)
		Me.Controls.Add(Me.Label19)
		Me.Controls.Add(Me.Check_AdministrativeFundType)
		Me.Controls.Add(Me.Label_AdminfundType)
		Me.Controls.Add(Me.Numeric_SortOrder)
		Me.Controls.Add(Me.edit_ShortVolatility)
		Me.Controls.Add(Me.Combo_PertracIndex)
		Me.Controls.Add(Me.Combo_Group)
		Me.Controls.Add(Me.btnClose)
		Me.Controls.Add(Me.GroupBox1)
		Me.Controls.Add(Me.Label1)
		Me.Controls.Add(Me.Panel1)
		Me.Controls.Add(Me.Combo_SelectFundTypeDescription)
		Me.Controls.Add(Me.btnSave)
		Me.Controls.Add(Me.editAuditID)
		Me.Controls.Add(Me.editFundTypeDescription)
		Me.Controls.Add(Me.lblFundTypeDescription)
		Me.Controls.Add(Me.lblGroupName)
		Me.Controls.Add(Me.lblPertracIndex)
		Me.Controls.Add(Me.lblVolatility)
		Me.Controls.Add(Me.lblSortOrder)
		Me.Controls.Add(Me.btnDelete)
		Me.Controls.Add(Me.btnCancel)
		Me.Name = "frmFundType"
		Me.Text = "Add/Edit Fund Type"
		Me.Panel1.ResumeLayout(False)
		CType(Me.edit_ShortVolatility, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.Numeric_SortOrder, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

#End Region

#Region " Form Locals and Constants "


  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
  Private WithEvents _MainForm As VeniceMain

  ' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
  Private FormTooltip As New ToolTip()

  ' Form Menu


  ' Form Constants, specific to the table being updated.

    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
  Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

  ' Form Locals, initialised on 'New' defining what standard data items to use
    ''' <summary>
  ''' Variable holding the form's data table name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_TABLENAME As String
    ''' <summary>
  ''' Variable holding the form's data adaptor name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_ADAPTORNAME As String
    ''' <summary>
  ''' Variable holding the form's dataset name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_DATASETNAME As String


  ' The standard ChangeID for this form. e.g. tblFundType
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

    ''' <summary>
  ''' Principal control used for selecting items on this form. 
    ''' </summary>
  Private THIS_FORM_SelectingCombo As ComboBox
    ''' <summary>
  ''' Control to select after proessing the "New" button.
    ''' </summary>
  Private THIS_FORM_NewMoveToControl As Control

  ' Form Specific Order fields
    ''' <summary>
  ''' Field Name to show in the Select combo.
    ''' </summary>
  Private THIS_FORM_SelectBy As String
    ''' <summary>
  ''' Field Name to order the Select combo by.
    ''' </summary>
  Private THIS_FORM_OrderBy As String

    ''' <summary>
  ''' The field name that holds the unique identifier for each entry. e.g. FundID for the tblFund table.
    ''' </summary>
  Private THIS_FORM_ValueMember As String

  ' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
  Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
  Private THIS_FORM_FormID As VeniceFormID

  ' Data Structures

    ''' <summary>
    ''' My dataset
    ''' </summary>
	Private myDataset As RenaissanceDataClass.DSFundType		' Form Specific !!!!
    ''' <summary>
    ''' My table
    ''' </summary>
	Private myTable As RenaissanceDataClass.DSFundType.tblFundTypeDataTable
    ''' <summary>
    ''' My connection
    ''' </summary>
  Private myConnection As SqlConnection
    ''' <summary>
    ''' My adaptor
    ''' </summary>
  Private myAdaptor As SqlDataAdapter

    ''' <summary>
    ''' The this standard dataset
    ''' </summary>
  Private ThisStandardDataset As RenaissanceGlobals.StandardDataset

    ''' <summary>
    ''' The index mapping dataset
    ''' </summary>
	Private IndexMappingDataset As RenaissanceDataClass.DSIndexMapping		' Form Specific !!!!
    ''' <summary>
    ''' The index mapping table
    ''' </summary>
	Private IndexMappingTable As RenaissanceDataClass.DSIndexMapping.tblIndexMappingDataTable
    ''' <summary>
    ''' The index mapping adaptor
    ''' </summary>
  Private IndexMappingAdaptor As SqlDataAdapter

  ' Active Element.

    ''' <summary>
    ''' The sorted rows
    ''' </summary>
  Private SortedRows() As DataRow
    ''' <summary>
    ''' The select by sorted rows
    ''' </summary>
  Private SelectBySortedRows() As DataRow
    ''' <summary>
    ''' The this data row
    ''' </summary>
	Private thisDataRow As RenaissanceDataClass.DSFundType.tblFundTypeRow		' Form Specific !!!!
    ''' <summary>
    ''' The this audit ID
    ''' </summary>
	Private thisAuditID As Integer
    ''' <summary>
    ''' The this position
    ''' </summary>
	Private thisPosition As Integer
    ''' <summary>
    ''' The __ is over cancel button
    ''' </summary>
	Private __IsOverCancelButton As Boolean
    ''' <summary>
    ''' The _ in use
    ''' </summary>
	Private _InUse As Boolean

	' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
	Private FormIsValid As Boolean
    ''' <summary>
    ''' The form changed
    ''' </summary>
	Private FormChanged As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
	Private _FormOpenFailed As Boolean
    ''' <summary>
    ''' The in paint
    ''' </summary>
	Private InPaint As Boolean
    ''' <summary>
    ''' The add new record
    ''' </summary>
	Private AddNewRecord As Boolean

	' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
	Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
	Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
	Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
	Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
	Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
	Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return __IsOverCancelButton
		End Get
		Set(ByVal Value As Boolean)
			__IsOverCancelButton = Value
		End Set
	End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
	Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
		Get
			Return InPaint
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
		Get
			Return _InUse
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmFundType"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
	Public Sub New(ByVal pMainForm As VeniceMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()

		_MainForm = pMainForm
		AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

		_FormOpenFailed = False
		_InUse = True

		' ******************************************************
		' Form Specific Settings :
		' ******************************************************

		THIS_FORM_SelectingCombo = Me.Combo_SelectFundTypeDescription
		THIS_FORM_NewMoveToControl = Me.editFundTypeDescription

		' Default Select and Order fields.

		THIS_FORM_SelectBy = "FundTypeDescription"
		THIS_FORM_OrderBy = "FundTypeDescription"

		THIS_FORM_ValueMember = "FundTypeID"

		' Form Permissioning :-

		THIS_FORM_PermissionArea = Me.Name
		THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

		' 'This' form ID

		THIS_FORM_FormID = VeniceFormID.frmFundType

		' This form's dataset type.

		ThisStandardDataset = RenaissanceStandardDatasets.tblFundType	' This Defines the Form Data !!! 

		' Format Event Handlers for form controls

		AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler
		AddHandler editFundTypeDescription.LostFocus, AddressOf MainForm.Text_NotNull

		' Form Control Changed events
		AddHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
		AddHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		AddHandler Combo_Group.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
		'AddHandler Combo_PertracIndex.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_InstrumentIndexSector.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_AttributionGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
		AddHandler Combo_ReporterSector.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

		AddHandler Combo_InstrumentIndexSector.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_InstrumentIndexSector.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_InstrumentIndexSector.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

		'AddHandler Combo_PertracIndex.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_PertracIndex.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		'AddHandler Combo_PertracIndex.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

		AddHandler editFundTypeDescription.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_Group.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_PertracIndex.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler edit_ShortVolatility.ValueChanged, AddressOf Me.FormControlChanged
		AddHandler Numeric_SortOrder.ValueChanged, AddressOf Me.FormControlChanged
		AddHandler Check_AdministrativeFundType.CheckStateChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_InstrumentIndexSector.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_AttributionGroup.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_AttributionGroup.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_ReporterSector.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_ReporterSector.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Check_ShowInReporter.CheckStateChanged, AddressOf Me.FormControlChanged

		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

		' ******************************************************
		' End Form Specific.
		' ******************************************************

		' Data object names standard to this Form type.

		THIS_TABLENAME = ThisStandardDataset.TableName
		THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
		THIS_DATASETNAME = ThisStandardDataset.DatasetName

		THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

		' Establish / Retrieve data objects for this form.

		myConnection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)
		myAdaptor = MainForm.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, VENICE_CONNECTION, THIS_TABLENAME)
		myDataset = MainForm.Load_Table(ThisStandardDataset, False)
		myTable = myDataset.Tables(0)

		IndexMappingDataset = MainForm.Load_Table(RenaissanceStandardDatasets.tblIndexMapping, False)
		If (Not (IndexMappingDataset Is Nothing)) Then
			IndexMappingTable = IndexMappingDataset.tblIndexMapping
			IndexMappingAdaptor = MainForm.MainDataHandler.Get_Adaptor(RenaissanceStandardDatasets.tblIndexMapping.Adaptorname)
		End If

		MainForm.BuildStandardFormMenu(Me.RootMenu, myDataset.Tables(0), AddressOf SelectMenuEvent, AddressOf OrderMenuEvent, AddressOf AuditReportMenuEvent)

	End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

	' Form Initialisation code.
	'
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
	Public Sub ResetForm() Implements StandardVeniceForm.ResetForm
		' ********************************************************************************
		'
		'
		' ********************************************************************************

		THIS_FORM_SelectBy = "FundTypeDescription"
		THIS_FORM_OrderBy = "FundTypeDescription"

		Call Form_Load(Me, New System.EventArgs)
	End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
	Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
		' ********************************************************************************
		'
		'
		' ********************************************************************************

		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		' ********************************************************************************
		'
		'
		' ********************************************************************************

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
		_FormOpenFailed = False
		_InUse = True

		Try

			' Initialise Data structures. Connection, Adaptor and Dataset.

			If Not (MainForm Is Nothing) Then
				FormIsValid = True
			Else
				MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
				FormIsValid = False
				_FormOpenFailed = True
				Exit Sub
			End If

			If (myConnection Is Nothing) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

				FormIsValid = False
				_FormOpenFailed = True
				Exit Sub
			End If

			If (myAdaptor Is Nothing) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

				FormIsValid = False
				_FormOpenFailed = True
				Exit Sub
			End If

			If (myDataset Is Nothing) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

				FormIsValid = False
				_FormOpenFailed = True
				Exit Sub
			End If

		Catch ex As Exception
		End Try

		Try

			' Initialse form

			InPaint = True
			IsOverCancelButton = False

			' Check User permissions
			Call CheckPermissions()
			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

				FormIsValid = False
				_FormOpenFailed = True
				Exit Sub
			End If

			' Build Sorted data list from which this form operates
			Call SetSortedRows()

			Call SetGroupCombo()
			Call SetPertracCombo()
			Call SetBenchmarkIndexCombo()
			Call SetIndexSectorCombo()
			Call SetAttributionGroupCombo()
			Call SetReporterGroupCombo()

			' Display initial record.

			thisPosition = 0
			If THIS_FORM_SelectingCombo.Items.Count > 0 Then
				Me.THIS_FORM_SelectingCombo.SelectedIndex = 0
				thisDataRow = SortedRows(thisPosition)
				Call GetFormData(thisDataRow)
			Else
				Me.THIS_FORM_SelectingCombo.SelectedIndex = (-1)
				Call GetFormData(Nothing)
			End If

		Catch ex As Exception

			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Initialising Form (Form.Load).", ex.StackTrace, True)

		Finally

			InPaint = False

		End Try


	End Sub

    ''' <summary>
    ''' Handles the Closing event of the frmFundType control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
	Private Sub frmFundType_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		' ********************************************************************************
		'
		'
		' ********************************************************************************

		Dim HideForm As Boolean

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...

		_InUse = False

		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else
			If (FormChanged = True) Then
				Call SetFormData()
			End If

			HideForm = True
			If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			e.Cancel = True
		Else
			Try
				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler
				RemoveHandler editFundTypeDescription.LostFocus, AddressOf MainForm.Text_NotNull

				RemoveHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
				RemoveHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				RemoveHandler Combo_Group.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
				'RemoveHandler Combo_PertracIndex.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_InstrumentIndexSector.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_AttributionGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
				RemoveHandler Combo_ReporterSector.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

				RemoveHandler Combo_InstrumentIndexSector.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_InstrumentIndexSector.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_InstrumentIndexSector.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

				'RemoveHandler Combo_PertracIndex.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_PertracIndex.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				'RemoveHandler Combo_PertracIndex.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

				RemoveHandler editFundTypeDescription.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Group.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_PertracIndex.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler edit_ShortVolatility.ValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Numeric_SortOrder.ValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_AdministrativeFundType.CheckStateChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_InstrumentIndexSector.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_AttributionGroup.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_AttributionGroup.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_ReporterSector.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_ReporterSector.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_ShowInReporter.CheckStateChanged, AddressOf Me.FormControlChanged

			Catch ex As Exception
			End Try
		End If

	End Sub


#End Region

	' Routine to handle changes / updates to tables by this and other windows.
	' If this, or any other, form posts a change to a table, then it will invoke an update event 
	' detailing what tables have been altered.
	' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
	' the 'VeniceAutoUpdate' event of the main Venice form.
	' Each form may them react as appropriate to changes in any table that might impact it.
	'
    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		Dim OrgInPaint As Boolean
		Dim KnowledgeDateChanged As Boolean
		Dim SetButtonStatus_Flag As Boolean
		Dim RefreshForm As Boolean = False

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		OrgInPaint = InPaint
		InPaint = True
		KnowledgeDateChanged = False
		SetButtonStatus_Flag = False

		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
			KnowledgeDateChanged = True
			RefreshForm = True
		End If

		' ****************************************************************
		' Check for changes relevant to this form
		' ****************************************************************

		' Changes to the tblFundType table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFundType) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			Call SetGroupCombo()
			Call SetAttributionGroupCombo()
			Call SetReporterGroupCombo()
		End If

		' Changes to the tblInstrument table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrument) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			Call SetIndexSectorCombo()
		End If

		' Changes to the tblPortfolioIndex table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblPortfolioIndex) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			Call Me.SetBenchmarkIndexCombo()
		End If

		' Changes to the Mastername table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Mastername) = True) Then

			' ReBuild the Pertrac-Mastername derived combo

			Call SetPertracCombo()

		End If

		' Changes to the KnowledgeDate :-
		If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
			SetButtonStatus_Flag = True
		End If

		' Changes to the tblUserPermissions table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

			' Check ongoing permissions.

			Call CheckPermissions()
			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

				FormIsValid = False
				Me.Close()
				Exit Sub
			End If

			SetButtonStatus_Flag = True

		End If


		' ****************************************************************
		' Changes to the Main FORM table :-
		' ****************************************************************

		If (e.TableChanged(THIS_FORM_ChangeID) = True) Or KnowledgeDateChanged Then

			RefreshForm = True

			' Re-Set Controls etc.
			Call SetSortedRows()

			' Move again to the correct item
			thisPosition = Get_Position(thisAuditID)

			' Validate current position.
			If thisPosition >= 0 And thisPosition < Me.SortedRows.GetLength(0) Then
				thisDataRow = Me.SortedRows(thisPosition)
			Else
				If (Me.SortedRows.GetLength(0) > 0) And (AddNewRecord = False) Then
					thisPosition = 0
					thisDataRow = Me.SortedRows(thisPosition)
					FormChanged = False
				Else
					thisDataRow = Nothing
				End If
			End If

			' Set SelectingCombo Index.
			If (Me.THIS_FORM_SelectingCombo.Items.Count <= 0) Then
				Try
					Me.THIS_FORM_SelectingCombo.SelectedIndex = -1
				Catch ex As Exception
				End Try
			ElseIf (thisPosition < 0) Then
				Try
					MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
				Catch ex As Exception
				End Try
			Else
				Try
					Me.THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
				Catch ex As Exception
				End Try
			End If

		End If

		' ****************************************************************
		' Repaint if not currently in Edit Mode
		'
		' ****************************************************************

		InPaint = OrgInPaint

		' Retrieve form data if appropriate, i.e. this data item is not already being edited.
		If (RefreshForm = True) AndAlso (FormChanged = False) AndAlso (AddNewRecord = False) Then
			GetFormData(thisDataRow) ' Includes a call to 'SetButtonStatus()'
		Else
			If SetButtonStatus_Flag Then
				Call SetButtonStatus()
			End If
		End If

	End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

	' Build Sorted list from the Source dataset and update the Select Combo
    ''' <summary>
    ''' Sets the sorted rows.
    ''' </summary>
	Private Sub SetSortedRows()

		Dim OrgInPaint As Boolean

		Dim thisrow As DataRow
		Dim thisDrowDownWidth As Integer
		Dim SizingBitmap As Bitmap
		Dim SizingGraphics As Graphics

		' Form Specific Selection Combo :-
		If (THIS_FORM_SelectingCombo Is Nothing) Then Exit Sub

		' Code is pretty Generic form here on...

		' Set paint local so that changes to the selection combo do not trigger form updates.

		OrgInPaint = InPaint
		InPaint = True

		' Get selected Row sets, or exit if no data is present.
		If myDataset Is Nothing Then
			ReDim SortedRows(0)
			ReDim SelectBySortedRows(0)
		Else
			SortedRows = myTable.Select("RN >= 0", THIS_FORM_OrderBy)
			SelectBySortedRows = myTable.Select("RN >= 0", THIS_FORM_SelectBy)
		End If

		' Set Combo data source
		THIS_FORM_SelectingCombo.DataSource = SortedRows
		THIS_FORM_SelectingCombo.DisplayMember = THIS_FORM_SelectBy
		THIS_FORM_SelectingCombo.ValueMember = THIS_FORM_ValueMember

		' Ensure that the Selection Combo's DrowDownWidth is wide enough to take the required strings

		thisDrowDownWidth = THIS_FORM_SelectingCombo.Width
		For Each thisrow In SortedRows

			' Compute the string dimensions in the given font
			SizingBitmap = New Bitmap(1, 1, System.Drawing.Imaging.PixelFormat.Format32bppArgb)
			SizingGraphics = Graphics.FromImage(SizingBitmap)
			Dim stringSize As SizeF = SizingGraphics.MeasureString(thisrow(THIS_FORM_SelectBy).ToString, THIS_FORM_SelectingCombo.Font)
			If (stringSize.Width) > thisDrowDownWidth Then
				thisDrowDownWidth = CInt(stringSize.Width)
			End If
		Next

		THIS_FORM_SelectingCombo.DropDownWidth = thisDrowDownWidth

		InPaint = OrgInPaint
	End Sub


	' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
	Private Sub CheckPermissions()

		Dim Permissions As Integer

		Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

		HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
		HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
		HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
		HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

	End Sub

	' Flag changes to form controls, Event Associations made in 'New' routine.
    ''' <summary>
  ''' Basic event handler, called when a form value is changed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

		If InPaint = False Then
			If (Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord) Then
				FormChanged = True
				Me.btnSave.Enabled = True
				Me.btnCancel.Enabled = True
			End If
		End If

	End Sub

    ''' <summary>
    ''' Selects the menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Public Sub SelectMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Select Menu
		' Event association is made during the dynamic menu creation

		THIS_FORM_SelectBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

		Call SetSortedRows()

	End Sub

    ''' <summary>
    ''' Orders the menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Public Sub OrderMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Order Menu
		' Event association is made during the dynamic menu creation

		THIS_FORM_OrderBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

		Call SetSortedRows()

	End Sub


    ''' <summary>
    ''' Audits the report menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Public Sub AuditReportMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Audit Report Menu
		' Event association is made during the dynamic menu creation

		Try
			Select Case CInt(CType(sender, System.Windows.Forms.ToolStripMenuItem).Tag)
				Case 0 ' This Record
					If (Me.AddNewRecord = False) AndAlso (thisAuditID >= 0) Then
						Run_AuditReport_ThisRecord(MainForm, ThisStandardDataset, thisAuditID, MainForm.VeniceProgressBar)
					End If

				Case 1 ' All Records
					Run_AuditReport_ALLRecords(MainForm, ThisStandardDataset, MainForm.VeniceProgressBar)

			End Select
		Catch ex As Exception
			Call MainForm.LogError(Me.Name & ", AuditReportMenuEvent()", 0, ex.Message, "Error calling Audit report.", ex.StackTrace, True)
		End Try

	End Sub


#End Region

#Region " Set Form Combos (Form Specific Code) "

    ''' <summary>
    ''' Sets the group combo.
    ''' </summary>
	Private Sub SetGroupCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_Group, _
		RenaissanceStandardDatasets.tblFundType, _
		"FundTypeGroup", _
		"FundTypeGroup", _
		"", _
		True)

	End Sub

    ''' <summary>
    ''' Sets the attribution group combo.
    ''' </summary>
	Private Sub SetAttributionGroupCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_AttributionGroup, _
		RenaissanceStandardDatasets.tblFundType, _
		"FundTypeAttributionGrouping", _
		"FundTypeAttributionGrouping", _
		"", _
		True)

	End Sub

    ''' <summary>
    ''' Sets the reporter group combo.
    ''' </summary>
	Private Sub SetReporterGroupCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_ReporterSector, _
		RenaissanceStandardDatasets.tblFundType, _
		"FundTypeReporterGrouping", _
		"FundTypeReporterGrouping", _
		"", _
		True)

	End Sub

    ''' <summary>
    ''' Sets the pertrac combo.
    ''' </summary>
	Private Sub SetPertracCombo()

		Combo_PertracIndex.MasternameCollection = MainForm.MasternameDictionary
		Combo_PertracIndex.SelectNoMatch = False

	End Sub

    ''' <summary>
    ''' Sets the benchmark index combo.
    ''' </summary>
	Private Sub SetBenchmarkIndexCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_BenchmarkIndex, _
		RenaissanceStandardDatasets.tblPortfolioIndex, _
		"PortfolioDescription", _
		"PortfolioTicker", _
		"PortfolioType=" & CInt(PortfolioType.IndexWeighting).ToString, True, True, True, "")

	End Sub

    ''' <summary>
    ''' Sets the index sector combo.
    ''' </summary>
	Private Sub SetIndexSectorCombo()
		Dim SelectString As String = "False"

		Try
			If (Me.Combo_BenchmarkIndex.SelectedIndex > 0) Then
				SelectString = "InstrumentType=" & CInt(InstrumentTypes.Index).ToString
			End If
		Catch ex As Exception
		End Try

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_InstrumentIndexSector, _
		RenaissanceStandardDatasets.tblInstrument, _
		"InstrumentDescription", _
		"InstrumentID", _
		SelectString, False, True, True, 0)

	End Sub

#End Region

#Region " Get & Set Form Data / SetButton / ValidateForm / btnCancel Events (Form Specific Code) "

    ''' <summary>
    ''' Gets the form data.
    ''' </summary>
    ''' <param name="ThisRow">The this row.</param>
	Private Sub GetFormData(ByRef ThisRow As RenaissanceDataClass.DSFundType.tblFundTypeRow)
		' Routine to populate form Controls given a data row.
		' Form is cleared for invalid Datarows or Invalid Form.

		Dim OrgInpaint As Boolean

		' Set 'Paint' flag to prevent event actions caused by changing the value of form fields.
		OrgInpaint = InPaint
		InPaint = True


		If (ThisRow Is Nothing) Or (FormIsValid = False) Or (Me.InUse = False) Then
			' Bad / New Datarow - Clear Form.

			thisAuditID = (-1)

			Me.editAuditID.Text = ""

			Me.editFundTypeDescription.Text = ""

			MainForm.ClearComboSelection(Combo_Group)
			Me.Combo_Group.Text = ""
			Me.Combo_AttributionGroup.Text = ""
			Combo_PertracIndex.SelectedValue = Nothing '	MainForm.ClearComboSelection(Combo_PertracIndex)
			Me.Combo_ReporterSector.Text = ""
			MainForm.ClearComboSelection(Combo_ReporterSector)

			Me.edit_ShortVolatility.Value = 0
			Me.Numeric_SortOrder.Value = 0
			Me.Check_AdministrativeFundType.CheckState = CheckState.Unchecked
			Me.Check_ShowInReporter.CheckState = CheckState.Checked

			If Me.Combo_InstrumentIndexSector.Items.Count > 0 Then
				Me.Combo_InstrumentIndexSector.SelectedIndex = 0 ' Leading value is blank (I hope).
			End If
			If Me.Combo_BenchmarkIndex.Items.Count > 0 Then
				Me.Combo_BenchmarkIndex.SelectedIndex = 0	' Leading value is blank (I hope).
			End If

			If AddNewRecord = True Then
				Me.btnCancel.Enabled = True
				Me.THIS_FORM_SelectingCombo.Enabled = False
			Else
				Me.btnCancel.Enabled = False
				Me.THIS_FORM_SelectingCombo.Enabled = True
			End If
		Else
			' Populate Form with given data.
			Try
				thisAuditID = thisDataRow.AuditID

				Me.editAuditID.Text = thisDataRow.AuditID.ToString

				Me.editFundTypeDescription.Text = thisDataRow.FundTypeDescription
				Me.Combo_Group.Text = thisDataRow.FundTypeGroup
				Me.Combo_AttributionGroup.Text = thisDataRow.FundTypeAttributionGrouping
				Me.Combo_ReporterSector.Text = thisDataRow.FundTypeReporterGrouping

				' Combo_FundBaseCurrency
				If (thisDataRow.FundTypePertracIndex > 0) Then
					Me.Combo_PertracIndex.SelectedValue = thisDataRow.FundTypePertracIndex
					Me.Combo_PertracIndex.Text = Me.Combo_PertracIndex.Text
				Else
					MainForm.ClearComboSelection(Combo_PertracIndex)
				End If

				Me.edit_ShortVolatility.Value = thisDataRow.FundTypeShortTrackRecordVolatility
				Me.Numeric_SortOrder.Value = thisDataRow.FundTypeSortOrder

				If thisDataRow.FundTypeIsAdministrativeOnly = 0 Then
					Check_AdministrativeFundType.CheckState = CheckState.Unchecked
				Else
					Check_AdministrativeFundType.CheckState = CheckState.Checked
				End If

				Check_ShowInReporter.Checked = thisDataRow.FundTypeShowInReporter

				Try
					Dim IndexMappingRows As RenaissanceDataClass.DSIndexMapping.tblIndexMappingRow()

					IndexMappingRows = Me.IndexMappingTable.Select("MappedFundTypeID=" & thisDataRow.FundTypeID, "RN Desc")

					If IndexMappingRows.Length > 0 Then
						Me.Combo_BenchmarkIndex.SelectedValue = IndexMappingRows(0).IndexTicker
						Call Me.SetIndexSectorCombo()
						Me.Combo_InstrumentIndexSector.SelectedValue = IndexMappingRows(0).IndexInstrumentID
					Else
						If (Combo_BenchmarkIndex.Items.Count > 0) Then Combo_BenchmarkIndex.SelectedIndex = 0
						If (Combo_InstrumentIndexSector.Items.Count > 0) Then Combo_InstrumentIndexSector.SelectedIndex = 0
					End If
				Catch Inner_Ex As Exception
				End Try

				AddNewRecord = False
				' MainForm.SetComboSelectionLengths(Me)

				Me.btnCancel.Enabled = False
				Me.THIS_FORM_SelectingCombo.Enabled = True

			Catch ex As Exception

				Call MainForm.LogError(Me.Name & ", GetFormData", 0, ex.Message, "Error Showing Data", ex.StackTrace, True)
				Call GetFormData(Nothing)

			End Try

		End If

		' Allow Field events to trigger before 'InPaint' Is re-set. 
		' (Should) Prevent Validation errors during Form Draw.
		Application.DoEvents()

		' Restore 'Paint' flag.
		InPaint = OrgInpaint
		FormChanged = False
		Me.btnSave.Enabled = False

		' As it says on the can....
		Call SetButtonStatus()

	End Sub

    ''' <summary>
    ''' Sets the form data.
    ''' </summary>
    ''' <param name="pConfirm">if set to <c>true</c> [p confirm].</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	Private Function SetFormData(Optional ByVal pConfirm As Boolean = True) As Boolean
		' *************************************************************
		'
		' *************************************************************
		Dim ErrMessage As String
		Dim ErrFlag As Boolean
		Dim ErrStack As String
		Dim ProtectedItem As Boolean = False

		ErrMessage = ""
		ErrStack = ""

		' *************************************************************
		' Appropriate Save permission :-
		' *************************************************************

		If (Me.HasUpdatePermission = False) And ((Me.HasInsertPermission = False) Or (Me.AddNewRecord = False)) Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "You do not have permission to save this record.", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' *************************************************************
		' If Save button is disabled then should not be able to save, exit silently.
		' *************************************************************
		If Me.btnSave.Enabled = False Then
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' *************************************************************
		' KnowledgeDate OK :-
		' *************************************************************

		If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Changes to Venice are not allowed when the Knowledgedate is not set to `NOW`", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' Confirm Save, if required.

		If (pConfirm = True) Then
			If MessageBox.Show("Save Changes ?", "Save Changes ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
				Return True
				Exit Function
			End If
		End If

		' *************************************************************
		' Procedure to Save the current form information.
		' *************************************************************

		Dim StatusString As String
		Dim LogString As String
		Dim UpdateRows(0) As DataRow
		Dim Position As Integer

		If (FormChanged = False) Or (FormIsValid = False) Then
			Return False
			Exit Function
		End If

		' Validation
		StatusString = ""
		If ValidateForm(StatusString) = False Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Validation Error." & vbCrLf & StatusString, "", True)
			Return False
			Exit Function
		End If

		' Check current position in the table.
		Position = Get_Position(thisAuditID)

		' Allow for new or missing ID.
		If Position < 0 Then
			If (AddNewRecord = True) OrElse (SortedRows.Length <= 0) Then
				thisDataRow = myTable.NewRow
				LogString = "Add: "
				AddNewRecord = True
			Else
				Return False
				Exit Function
			End If
		Else
			' Row found OK.
			LogString = "Edit : AuditID = " & thisAuditID.ToString

			thisDataRow = Me.SortedRows(Position)

			' Check for Protected FundType
			If Not thisDataRow.IsFundTypeIDNull Then
				If (thisDataRow.FundTypeID > 0) AndAlso (System.Enum.IsDefined(GetType(RenaissanceGlobals.FundType), thisDataRow.FundTypeID) = True) Then

					ProtectedItem = True

				End If
			End If
		End If

		' Set 'Paint' flag.
		InPaint = True

		' *************************************************************
		' Lock the Data Table, to prevent update conflicts.
		' *************************************************************
		SyncLock myTable

			' Initiate Edit,
			thisDataRow.BeginEdit()

			' Set Data Values
			If (ProtectedItem = True) AndAlso (AddNewRecord = False) AndAlso (thisDataRow.FundTypeDescription <> Me.editFundTypeDescription.Text) Then
				MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "This Fund Type is protected, Fund Type Description may not be changed.", "", True)
			Else
				thisDataRow.FundTypeDescription = Me.editFundTypeDescription.Text
			End If

			LogString &= ", FundTypeDescription = " & thisDataRow.FundTypeDescription

			thisDataRow.FundTypeGroup = Me.Combo_Group.Text
			LogString &= ", FundName = " & thisDataRow.FundTypeGroup

			thisDataRow.FundTypeAttributionGrouping = Me.Combo_AttributionGroup.Text
			LogString &= ", AttribGrouping = " & thisDataRow.FundTypeAttributionGrouping

			thisDataRow.FundTypeReporterGrouping = Me.Combo_ReporterSector.Text
			LogString &= ", ReporterGrouping = " & thisDataRow.FundTypeReporterGrouping

			' Combo_PertracIndex
			If Me.Combo_PertracIndex.SelectedIndex > 0 Then
				thisDataRow.FundTypePertracIndex = Me.Combo_PertracIndex.SelectedValue
			Else
				thisDataRow.FundTypePertracIndex = 0
			End If

			thisDataRow.FundTypeShortTrackRecordVolatility = Me.edit_ShortVolatility.Value
			thisDataRow.FundTypeSortOrder = Me.Numeric_SortOrder.Value

			If Check_AdministrativeFundType.CheckState = CheckState.Checked Then
				thisDataRow.FundTypeIsAdministrativeOnly = (-1)
			Else
				thisDataRow.FundTypeIsAdministrativeOnly = 0
			End If

			thisDataRow.FundTypeShowInReporter = Check_ShowInReporter.Checked

			thisDataRow.EndEdit()
			InPaint = False

			' Add and Update DataRow. 

			ErrFlag = False

			If AddNewRecord = True Then
				Try
					myTable.Rows.Add(thisDataRow)
				Catch ex As Exception
					ErrFlag = True
					ErrMessage = ex.Message
					ErrStack = ex.StackTrace
				End Try
			End If

			UpdateRows(0) = thisDataRow

			' Post Additions / Updates to the underlying table :-
			Dim temp As Integer
			Try
				If (ErrFlag = False) Then
					myAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
					myAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

					temp = MainForm.AdaptorUpdate(Me.Name, myAdaptor, UpdateRows)
				End If

			Catch ex As Exception

				ErrMessage = ex.Message
				ErrFlag = True
				ErrStack = ex.StackTrace

			End Try

			thisAuditID = thisDataRow.AuditID


		End SyncLock

		Try
			Dim IndexMappingRows As RenaissanceDataClass.DSIndexMapping.tblIndexMappingRow()
			Dim thisIndexMappingRow As RenaissanceDataClass.DSIndexMapping.tblIndexMappingRow

			If (Combo_BenchmarkIndex.Items.Count > 0) AndAlso (Combo_BenchmarkIndex.SelectedIndex > 0) Then
				If (Combo_InstrumentIndexSector.Enabled = True) AndAlso (Me.Combo_InstrumentIndexSector.Items.Count > 0) AndAlso (Combo_InstrumentIndexSector.SelectedIndex > 0) Then

					IndexMappingRows = Me.IndexMappingTable.Select("(MappedFundTypeID=" & thisDataRow.FundTypeID.ToString & ") AND (IndexTicker='" & Combo_BenchmarkIndex.SelectedValue.ToString & "')")

					If (IndexMappingRows.Length <= 0) Then
						thisIndexMappingRow = IndexMappingTable.NewtblIndexMappingRow

						thisIndexMappingRow.IndexTicker = Combo_BenchmarkIndex.SelectedValue.ToString
						thisIndexMappingRow.MappedFundTypeID = thisDataRow.FundTypeID
						thisIndexMappingRow.IndexInstrumentID = CInt(Combo_InstrumentIndexSector.SelectedValue)

						IndexMappingTable.Rows.Add(thisIndexMappingRow)
					Else
						thisIndexMappingRow = IndexMappingRows(0)

						thisIndexMappingRow.IndexTicker = Combo_BenchmarkIndex.SelectedValue.ToString
						thisIndexMappingRow.MappedFundTypeID = thisDataRow.FundTypeID
						thisIndexMappingRow.IndexInstrumentID = CInt(Combo_InstrumentIndexSector.SelectedValue)
					End If

					MainForm.AdaptorUpdate(Me.Name, Me.IndexMappingAdaptor, New DataRow() {thisIndexMappingRow})

				End If
			End If
		Catch Inner_Ex As Exception
		End Try

		' *************************************************************
		' Error report. Not done in the Try-Catch as that would serve to persist the SyncLock.
		' *************************************************************

		If (ErrFlag = True) Then
			Call MainForm.LogError(Me.Name & ", SetFormData", LOG_LEVELS.Error, ErrMessage, "Error Saving Data", ErrStack, True)
		End If

		' Finish off

		AddNewRecord = False
		FormChanged = False

		Me.THIS_FORM_SelectingCombo.Enabled = True

		' Propogate changes

    ' Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID), True)

    If (thisDataRow IsNot Nothing) AndAlso (thisDataRow.IsAuditIDNull = False) AndAlso (thisDataRow.AuditID > 0) Then
      Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID, thisDataRow.AuditID.ToString), True)
    Else
      Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID), True)
    End If

    Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblIndexMapping), True)

	End Function

    ''' <summary>
    ''' Sets the button status.
    ''' </summary>
	Private Sub SetButtonStatus()
		' Sets the status of the form controlls appropriate to the current users 
		' permissions and the 'Changed' or 'New' status of the form.

		' No Read permission :-

		If Me.HasReadPermission = False Then
			MainForm.LogError(Me.Name & ", SetButtonStatus()", 0, "", "You do not have Read permission for this Form", "", True)
			Me.Close()
			Exit Sub
		End If

		' Has Insert Permission.
		If Me.HasInsertPermission Then
			Me.btnAdd.Enabled = True
		Else
			Me.btnAdd.Enabled = False
		End If

		' Has Delete permission. 
		If (Me.HasDeletePermission) And (Me.AddNewRecord = False) And (thisPosition >= 0) Then
			Me.btnDelete.Enabled = True
		Else
			Me.btnDelete.Enabled = False
		End If

		If (Me.MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And _
		 ((Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord)) Then

			Me.editFundTypeDescription.Enabled = True
			Me.Combo_Group.Enabled = True
			Me.Combo_PertracIndex.Enabled = True
			Me.Combo_BenchmarkIndex.Enabled = True
			Me.Combo_InstrumentIndexSector.Enabled = True
			Me.Combo_AttributionGroup.Enabled = True
			Me.edit_ShortVolatility.Enabled = True
			Me.Numeric_SortOrder.Enabled = True
			Me.Check_AdministrativeFundType.Enabled = True
			Me.Combo_ReporterSector.Enabled = True
			Me.Check_ShowInReporter.Enabled = True

		Else

			Me.editFundTypeDescription.Enabled = False
			Me.Combo_Group.Enabled = False
			Me.Combo_PertracIndex.Enabled = False
			Me.Combo_BenchmarkIndex.Enabled = False
			Me.Combo_InstrumentIndexSector.Enabled = False
			Me.Combo_AttributionGroup.Enabled = False
			Me.edit_ShortVolatility.Enabled = False
			Me.Numeric_SortOrder.Enabled = False
			Me.Check_AdministrativeFundType.Enabled = False
			Me.Combo_ReporterSector.Enabled = False
			Me.Check_ShowInReporter.Enabled = False

		End If

	End Sub

    ''' <summary>
    ''' Validates the form.
    ''' </summary>
    ''' <param name="pReturnString">The p return string.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	Private Function ValidateForm(ByRef pReturnString As String) As Boolean
		' Form Validation code.
		' 
		' This code should be the final arbiter of what is allowed. no assumptions regarding 
		' prior validation should be made.
		' 
		' This Code is called by the SetFormData routine before position changes.
		'
		Dim RVal As Boolean

		RVal = True
		pReturnString = ""

		' Validate 

		If Me.editFundTypeDescription.Text.Length <= 0 Then
			pReturnString = "Fund Type Description must not be left blank."
			RVal = False
		End If

		Return RVal

	End Function

    ''' <summary>
    ''' Handles the MouseEnter event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnCancel_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseEnter
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = True
	End Sub

    ''' <summary>
    ''' Handles the MouseLeave event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnCancel_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseLeave
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = False
	End Sub


#End Region

#Region " Navigation Code / GetPosition() (Generic Code) "

    ''' <summary>
    ''' Handles the SelectComboChanged event of the Combo control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_SelectComboChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
		' Selection Combo. SelectedItem changed.
		'

		' Don't react to changes made in paint routines etc.
		If InPaint = True Then Exit Sub

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		' Find the correct data row, then show it...
		thisPosition = THIS_FORM_SelectingCombo.SelectedIndex

		If (thisPosition >= 0) Then
			thisDataRow = Me.SortedRows(thisPosition)
		Else
			thisDataRow = Nothing
		End If

		Call GetFormData(thisDataRow)

	End Sub


    ''' <summary>
    ''' Handles the Click event of the btnNavPrev control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnNavPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavPrev.Click
		' 'Previous' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		If thisPosition > 0 Then thisPosition -= 1
		If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
			thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
		End If

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

		Exit Sub

		If myTable.Rows.Count > thisPosition Then
			thisDataRow = myTable.Rows(thisPosition)
			Call GetFormData(thisDataRow)
		Else
			Call GetFormData(Nothing)
		End If

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnNavNext control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnNavNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavNext.Click
		' 'Next' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		If thisPosition < (myTable.Rows.Count - 1) Then thisPosition += 1
		If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
			thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
		End If

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

		Exit Sub

		If myTable.Rows.Count > thisPosition Then
			thisDataRow = myTable.Rows(thisPosition)
			Call GetFormData(thisDataRow)
		Else
			Call GetFormData(Nothing)
		End If

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnNavFirst control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnNavFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavFirst.Click
		' 'First' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		thisPosition = 0
		If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
			thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
		End If

    If (THIS_FORM_SelectingCombo.SelectedIndex = thisPosition) Then
      Combo_SelectComboChanged(THIS_FORM_SelectingCombo, New EventArgs)
    Else
      THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
    End If

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnLast control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLast.Click
		' 'Last' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		thisPosition = THIS_FORM_SelectingCombo.Items.Count - 1

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

	End Sub

    ''' <summary>
    ''' Get_s the position.
    ''' </summary>
    ''' <param name="pAuditID">The p audit ID.</param>
    ''' <returns>System.Int32.</returns>
	Private Function Get_Position(ByVal pAuditID As Integer) As Integer
		' Returns the position in the current 'SortedRows' array of the entry corresponding to the given 
		' AudidID.

		Dim MinIndex As Integer
		Dim MaxIndex As Integer
		Dim CurrentMin As Integer
		Dim CurrentMax As Integer
		Dim searchDataRow As DataRow

		Try
			' SortedRows exists ?

			If SortedRows Is Nothing Then
				Return (-1)
				Exit Function
			End If

			' Return (-1) if there are no rows in the 'SortedRows'

			If (SortedRows.GetLength(0) <= 0) Then
				Return (-1)
				Exit Function
			End If


			' Use a modified Search moving outwards from the last returned value to locate the required value.
			' Reflecting the fact that for the most part One looks for closely located records.

			MinIndex = 0
			MaxIndex = SortedRows.GetLength(0) - 1


			' Check First and Last records (Incase 'First' or 'Last' was pressed).

			searchDataRow = SortedRows(MinIndex)

			If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(searchDataRow("AuditID")) = pAuditID) Then
				Return MinIndex
				Exit Function
			End If

			searchDataRow = SortedRows(MaxIndex)
			If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(searchDataRow("AuditID")) = pAuditID) Then
				Return MaxIndex
				Exit Function
			End If

			' now search outwards from the last returned value.

			If (Me.thisPosition >= MinIndex) And (Me.thisPosition <= MaxIndex) Then
				CurrentMin = thisPosition
				CurrentMax = CurrentMin + 1
			Else
				CurrentMin = CInt((MinIndex + MaxIndex) / 2)
				CurrentMax = CurrentMin + 1
			End If

			While (CurrentMin >= MinIndex) Or (CurrentMax <= MaxIndex)
				If (CurrentMin >= MinIndex) Then
					searchDataRow = SortedRows(CurrentMin)

					If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(searchDataRow("AuditID")) = pAuditID) Then
						Return CurrentMin
						Exit Function
					End If

					CurrentMin -= 1
				End If

				If (CurrentMax <= MaxIndex) Then
					searchDataRow = SortedRows(CurrentMax)

					If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(searchDataRow("AuditID")) = pAuditID) Then
						Return CurrentMax
						Exit Function
					End If

					CurrentMax += 1
				End If

			End While

			Return (-1)
			Exit Function

		Catch ex As Exception
			MainForm.LogError(Me.Name & ", Get_Position()", LOG_LEVELS.Warning, ex.Message, "", ex.StackTrace, True)
			Return (-1)
			Exit Function
		End Try

	End Function

#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

    ''' <summary>
    ''' Handles the Click event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
		' Cancel Changes, redisplay form.

		FormChanged = False
		AddNewRecord = False

		Me.THIS_FORM_SelectingCombo.Enabled = True

		If (thisPosition >= 0) And (thisPosition < Me.SortedRows.GetLength(0)) Then
			thisDataRow = Me.SortedRows(thisPosition)
			Call GetFormData(thisDataRow)
		Else
			Call btnNavFirst_Click(Me, New System.EventArgs)
		End If

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnSave control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
		' Save Changes, if any, without prompting.

		If (FormChanged = True) Then
			Call SetFormData(False)
		End If

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnDelete control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
		Dim Position As Integer
		Dim NextAuditID As Integer

		If (AddNewRecord = True) Then
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Sub
		End If

		' No Appropriate Save permission :-

		If (Me.HasDeletePermission = False) Then
			MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "You do not have permission to Delete this record.", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Sub
		End If

		' *************************************************************
		' KnowledgeDate OK :-
		' *************************************************************

		If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
			MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "Changes to Venice are not allowed when the Knowledgedate is not set to `NOW`", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Sub
		End If

		' *************************************************************
		' Confirm :-
		' *************************************************************
		If MsgBox("Are you Sure ?", MsgBoxStyle.OKCancel, "Confirm Delete") = MsgBoxResult.Cancel Then
			Exit Sub
		End If

		' Check Data position.

		Position = Get_Position(thisAuditID)

		If Position < 0 Then Exit Sub

		' Check Referential Integrity 
		If CheckReferentialIntegrity(_MainForm, Me.SortedRows(Position)) = False Then
			MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "Deleting this record would voilate referential integrity.", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Sub
		End If

		' Resolve row to show after deleting this one.

		NextAuditID = (-1)
		If (Position + 1) < Me.SortedRows.GetLength(0) Then
			NextAuditID = CInt(Me.SortedRows(Position + 1)("AuditID"))
		ElseIf (Position - 1) >= 0 Then
			NextAuditID = CInt(Me.SortedRows(Position - 1)("AuditID"))
		Else
			NextAuditID = (-1)
		End If

		' Delete this row

		Try
			Me.SortedRows(Position).Delete()

			MainForm.AdaptorUpdate(Me.Name, myAdaptor, myTable)

		Catch ex As Exception

			Call MainForm.LogError(Me.Name & ", Delete()", LOG_LEVELS.Error, ex.Message, "Error Deleting Record.", ex.StackTrace, True)
		End Try

		' Tidy Up.

		FormChanged = False

		thisAuditID = NextAuditID
		Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID), True)

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnAdd control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
		' Prepare form to Add a new record.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		thisPosition = -1
		InPaint = True
		MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
		InPaint = False

		GetFormData(Nothing)
		AddNewRecord = True

		Me.btnCancel.Enabled = True
		Me.THIS_FORM_SelectingCombo.Enabled = False

		Call SetButtonStatus()

		THIS_FORM_NewMoveToControl.Focus()

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnClose control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
		' Close Form

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		Me.Close()

	End Sub

#End Region

#Region " Form Control Event Code "

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_BenchmarkIndex control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_BenchmarkIndex_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_BenchmarkIndex.SelectedIndexChanged
		Call Me.SetIndexSectorCombo()

		If Me.InPaint = False Then

			Try
				Dim IndexMappingRows As RenaissanceDataClass.DSIndexMapping.tblIndexMappingRow()
				Dim thisFundTypeID As Integer = 0
				Dim thisIndexTicker As String = ""
				Dim Position As Integer

				' Check current position in the table.
				Position = Get_Position(thisAuditID)

				If (Position < 0) Then Exit Sub

				thisFundTypeID = CInt(SortedRows(Position)("FundTypeID"))

				If (Combo_BenchmarkIndex.SelectedIndex > 0) Then
					thisIndexTicker = Combo_BenchmarkIndex.SelectedValue.ToString
				End If

				IndexMappingRows = Me.IndexMappingTable.Select("(MappedFundTypeID=" & thisFundTypeID & ") AND (IndexTicker='" & thisIndexTicker & "')", "RN Desc")

				If IndexMappingRows.Length > 0 Then
					Me.Combo_InstrumentIndexSector.SelectedValue = IndexMappingRows(0).IndexInstrumentID
				Else
					If (Combo_InstrumentIndexSector.Items.Count > 0) Then Combo_InstrumentIndexSector.SelectedIndex = 0
				End If
			Catch Inner_Ex As Exception
			End Try

		End If
	End Sub


#End Region

#Region " Bug hunting "

  ' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
  ' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
  ' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

    ''' <summary>
    ''' Called when [row updating].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

    ''' <summary>
    ''' Called when [row updated].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

    ''' <summary>
    ''' Called when [row fill error].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
    Dim a As Integer

    a = 1
  End Sub

#End Region



End Class
