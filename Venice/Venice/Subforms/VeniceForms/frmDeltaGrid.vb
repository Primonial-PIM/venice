﻿' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 01-17-2013
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmDeltaGrid.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************

Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals
Imports RenaissanceDataClass
Imports RenaissanceUtilities.DatePeriodFunctions

''' <summary>
''' Class frmDeltaGrid
''' </summary>
Public Class frmDeltaGrid

	Inherits System.Windows.Forms.Form
	Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmDeltaGrid"/> class from being created.
    ''' </summary>
	Private Sub New()
		MyBase.New()

		'This call is required by the Windows Form Designer.
		InitializeComponent()

		'Add any initialization after the InitializeComponent() call

	End Sub

#End Region

#Region " Form Locals and Constants "


	' Form 'Parent', the Main Venice form.
	' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
	Private WithEvents _MainForm As VeniceMain

	' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
	Private FormTooltip As New ToolTip()

	' Form Constants, specific to the table being updated.

    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
	Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

	' Form Locals, initialised on 'New' defining what standard data items to use
    ''' <summary>
  ''' Variable holding the form's data table name. taken from the "ThisStandardDataset" object.
    ''' </summary>
	Private THIS_TABLENAME As String
    ''' <summary>
  ''' Variable holding the form's data adaptor name. taken from the "ThisStandardDataset" object.
    ''' </summary>
	Private THIS_ADAPTORNAME As String
    ''' <summary>
  ''' Variable holding the form's dataset name. taken from the "ThisStandardDataset" object.
    ''' </summary>
	Private THIS_DATASETNAME As String

    ''' <summary>
    ''' The generic update object
    ''' </summary>
	Private GenericUpdateObject As RenaissanceTimerUpdateClass

	' The standard ChangeID for this form. e.g. tblGroupList
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
	Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

	' Form Specific Order fields
    ''' <summary>
  ''' Field Name to show in the Select combo.
    ''' </summary>
	Private THIS_FORM_SelectBy As String
    ''' <summary>
  ''' Field Name to order the Select combo by.
    ''' </summary>
	Private THIS_FORM_OrderBy As String

    ''' <summary>
  ''' The field name that holds the unique identifier for each entry. e.g. FundID for the tblFund table.
    ''' </summary>
	Private THIS_FORM_ValueMember As String

	' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
	Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
	Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

	' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
	Private THIS_FORM_FormID As VeniceFormID

	' Data Structures

    ''' <summary>
    ''' My dataset
    ''' </summary>
	Private myDataset As RenaissanceDataClass.DSGreeks		 ' Form Specific !!!!
    ''' <summary>
    ''' My table
    ''' </summary>
	Private myTable As RenaissanceDataClass.DSGreeks.tblGreeksDataTable
    ''' <summary>
    ''' My connection
    ''' </summary>
	Private myConnection As SqlConnection
    ''' <summary>
    ''' The by instrument adaptor
    ''' </summary>
	Private byInstrumentAdaptor As SqlDataAdapter
    ''' <summary>
    ''' The by date adaptor
    ''' </summary>
	Private byDateAdaptor As SqlDataAdapter
    ''' <summary>
    ''' My read adaptor
    ''' </summary>
	Private myReadAdaptor As SqlDataAdapter

    ''' <summary>
    ''' The selected instrument list_ all
    ''' </summary>
	Private SelectedInstrumentList_All As SortedList = Nothing
    ''' <summary>
    ''' The selected instrument list_ active
    ''' </summary>
	Private SelectedInstrumentList_Active As SortedList = Nothing

	'Private PerformanceDataset As RenaissanceDataClass.DSPerformance			' Form Specific !!!!
	'Private PerformanceTable As RenaissanceDataClass.DSPerformance.tblPerformanceDataTable
	'Private PerformanceAdaptor As SqlDataAdapter

    ''' <summary>
    ''' The this standard dataset
    ''' </summary>
	Private ThisStandardDataset As RenaissanceGlobals.StandardDataset


	' Active Element.

    ''' <summary>
    ''' The sorted rows
    ''' </summary>
	Private SortedRows() As DataRow
	'Private SelectBySortedRows() As DataRow
	'Private thisDataRow As RenaissanceDataClass.DSGreeks.tblGreeksRow		' Form Specific !!!!
	'Private thisAuditID As Integer
	'Private thisPosition As Integer
    ''' <summary>
    ''' The _ is over cancel button
    ''' </summary>
	Private _IsOverCancelButton As Boolean
    ''' <summary>
    ''' The _ in use
    ''' </summary>
	Private _InUse As Boolean

	' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
	Private FormIsValid As Boolean
    ''' <summary>
    ''' The form changed
    ''' </summary>
	Private FormChanged As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
	Private _FormOpenFailed As Boolean
    ''' <summary>
    ''' The in paint
    ''' </summary>
	Private InPaint As Boolean
    ''' <summary>
    ''' The in get form data_ tick
    ''' </summary>
	Private InGetFormData_Tick As Boolean
    ''' <summary>
    ''' The add new record
    ''' </summary>
	Private AddNewRecord As Boolean

	' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
	Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
	Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
	Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
	Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
	Public ReadOnly Property MainForm() As VeniceMain Implements StandardVeniceForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [form changed flag].
    ''' </summary>
    ''' <value><c>true</c> if [form changed flag]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property FormChangedFlag() As Boolean
		Get
			Return FormChanged
		End Get
	End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
	Public Property IsOverCancelButton() As Boolean Implements StandardVeniceForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return _IsOverCancelButton
		End Get
		Set(ByVal Value As Boolean)
			_IsOverCancelButton = Value
		End Set
	End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
	Public ReadOnly Property IsInPaint() As Boolean Implements StandardVeniceForm.IsInPaint
		Get
			Return InPaint
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property InUse() As Boolean Implements StandardVeniceForm.InUse
		Get
			Return _InUse
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property FormOpenFailed() As Boolean Implements StandardVeniceForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

#End Region

#Region " Local Classes"


#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmDeltaGrid"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
	Public Sub New(ByVal pMainForm As VeniceMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()

		_MainForm = pMainForm
		AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

		_FormOpenFailed = False
		_InUse = True

		' ******************************************************
		' Form Specific Settings :
		' ******************************************************

		THIS_FORM_ChangeID = RenaissanceChangeID.tblGreeks

		' Default Select and Order fields.

		THIS_FORM_SelectBy = "GreekDate"
		THIS_FORM_OrderBy = "InstrumentID, GreekDate"

		THIS_FORM_ValueMember = "ID"

		' Form Permissioning :-

		THIS_FORM_PermissionArea = Me.Name
		THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

		' 'This' form ID

		THIS_FORM_FormID = VeniceFormID.frmDeltaGrid

		' This form's dataset type.

		ThisStandardDataset = RenaissanceStandardDatasets.tblGreeks	' This Defines the Form Data !!! 

		' Format Event Handlers for form controls

		AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

		' Form Control Changed events

		AddHandler Combo_Select.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
		AddHandler Combo_Select.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler DateTime_SelectedDate.ValueChanged, AddressOf Combo_SelectComboChanged

		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

		' ******************************************************
		' End Form Specific.
		' ******************************************************

		' Data object names standard to this Form type.

		THIS_TABLENAME = ThisStandardDataset.TableName
		THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
		THIS_DATASETNAME = ThisStandardDataset.DatasetName

		' Establish / Retrieve data objects for this form.

		myConnection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)

		byDateAdaptor = New SqlDataAdapter
		MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION), byDateAdaptor, "TBLGREEKS_SELECTBYDATE")

		byInstrumentAdaptor = New SqlDataAdapter
		MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION), byInstrumentAdaptor, "TBLGREEKS_SELECTBYINSTRUMENT")

		myDataset = New RenaissanceDataClass.DSGreeks
		myTable = myDataset.Tables(0)

		'PerformanceAdaptor = New SqlDataAdapter
		'MainForm.MainAdaptorHandler.Set_AdaptorCommands(myConnection, PerformanceAdaptor, RenaissanceStandardDatasets.Performance.TableName)
		'PerformanceDataset = New DSPerformance
		'PerformanceTable = PerformanceDataset.tblPerformance
		'If (PerformanceTable.Columns.Contains("ID")) Then
		'	PerformanceTable.Columns("ID").AllowDBNull = True
		'End If

		MainForm.BuildStandardFormMenu(Me.RootMenu, myDataset.Tables(0), AddressOf SelectMenuEvent, AddressOf OrderMenuEvent, AddressOf AuditReportMenuEvent)
		Me.RootMenu.PerformLayout()

	End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

	' Form Initialisation code.
	'
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
	Public Sub ResetForm() Implements StandardVeniceForm.ResetForm
		THIS_FORM_SelectBy = "GreekDate"
		THIS_FORM_OrderBy = "GreekDate"

		Call Form_Load(Me, New System.EventArgs)
	End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
	Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
		_FormOpenFailed = False
		_InUse = True

		' Initialise Data structures. Connection, Adaptor and Dataset.

		If Not (MainForm Is Nothing) Then
			FormIsValid = True
		Else
			MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myConnection Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (byDateAdaptor Is Nothing) OrElse (byInstrumentAdaptor Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myDataset Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		' Initialse form

		InPaint = True
		IsOverCancelButton = False

		' Check User permissions
		Call CheckPermissions()
		If (HasReadPermission = False) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		Call SetSortedRows()

		' Initialise Grid

		Grid_Greeks.DataSource = Nothing
		Grid_Greeks.Columns.Clear()

		Try
			Grid_Greeks.AutoGenerateColumns = True
			Grid_Greeks.DataSource = myTable
		Catch ex As Exception
		Finally
			Grid_Greeks.AutoGenerateColumns = False
			Grid_Greeks.DataSource = Nothing
		End Try

		Grid_Greeks.Columns("RN").Visible = False
		Grid_Greeks.Columns("AuditID").Visible = False
		Grid_Greeks.Columns("GreekID").Visible = False
		Grid_Greeks.Columns("DateEntered").Visible = False
		Grid_Greeks.Columns("DateDeleted").Visible = False
		Grid_Greeks.Columns("UserEntered").Visible = False

		Grid_Greeks.Columns("InstrumentID").Visible = True
		Grid_Greeks.Columns("InstrumentID").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
		Grid_Greeks.Columns("InstrumentID").DefaultCellStyle.Format = "########0"
		Grid_Greeks.Columns("InstrumentID").HeaderText = "InstrumentID"
		Grid_Greeks.Columns("InstrumentID").ReadOnly = True
		Grid_Greeks.Columns("InstrumentID").Resizable = DataGridViewTriState.False
		Grid_Greeks.Columns("InstrumentID").Width = 80

		Grid_Greeks.Columns("GreekDate").DefaultCellStyle.Format = "dd MMM yyyy"
		Grid_Greeks.Columns("GreekDate").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
		Grid_Greeks.Columns("GreekDate").HeaderText = "    Date    "
		'Grid_Greeks.Columns("GreekDate").ReadOnly = True

		Grid_Greeks.Columns("Delta").DefaultCellStyle.Format = "#,##0.0000;-#,##0.0000"
		Grid_Greeks.Columns("Delta").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
		Grid_Greeks.Columns("Delta").HeaderText = " Delta "
		Grid_Greeks.Columns("Delta").Width = 60

		Grid_Greeks.Columns("Gamma").DefaultCellStyle.Format = "#,##0.0000;-#,##0.0000"
		Grid_Greeks.Columns("Gamma").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
		Grid_Greeks.Columns("Gamma").HeaderText = " Gamma "
		Grid_Greeks.Columns("Gamma").Width = 60

		Grid_Greeks.Columns("Rho").DefaultCellStyle.Format = "#,##0.0000;-#,##0.0000"
		Grid_Greeks.Columns("Rho").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
		Grid_Greeks.Columns("Rho").HeaderText = " Rho "
		Grid_Greeks.Columns("Rho").Width = 60

		Grid_Greeks.Columns("Vega").DefaultCellStyle.Format = "#,##0.0000;-#,##0.0000"
		Grid_Greeks.Columns("Vega").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
		Grid_Greeks.Columns("Vega").HeaderText = " Vega "
		Grid_Greeks.Columns("Vega").Width = 60

		Grid_Greeks.Columns("Theta").DefaultCellStyle.Format = "#,##0.0000;-#,##0.0000"
		Grid_Greeks.Columns("Theta").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
		Grid_Greeks.Columns("Theta").HeaderText = " Theta "
		Grid_Greeks.Columns("Theta").Width = 60

		Grid_Greeks.Columns("Volatility").DefaultCellStyle.Format = "#,##0.00%;-#,##0.00%"
		Grid_Greeks.Columns("Volatility").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
		Grid_Greeks.Columns("Volatility").HeaderText = " Volatility "
		Grid_Greeks.Columns("Volatility").Width = 60

		Dim ModifiedColumn As DataGridViewCheckBoxColumn = New DataGridViewCheckBoxColumn(False)
		ModifiedColumn.Name = "Modified"
		ModifiedColumn.HeaderText = ""
		ModifiedColumn.ReadOnly = True
		ModifiedColumn.Visible = True	' False
		Grid_Greeks.Columns.Add(ModifiedColumn)
		ModifiedColumn.DisplayIndex = 0
		ModifiedColumn.Resizable = DataGridViewTriState.False
		ModifiedColumn.Frozen = True
		ModifiedColumn.Width = 30

		Grid_Greeks.Columns("InstrumentID").DisplayIndex = 1
		Grid_Greeks.Columns("InstrumentID").Frozen = True

		Dim InstrumentColumn As DataGridViewTextBoxColumn = New DataGridViewTextBoxColumn()
		InstrumentColumn.Name = "InstrumentName"
		InstrumentColumn.HeaderText = "         Instrument         "
		InstrumentColumn.ReadOnly = True
		InstrumentColumn.Visible = True
		Grid_Greeks.Columns.Add(InstrumentColumn)
		InstrumentColumn.DisplayIndex = 2
		InstrumentColumn.Resizable = DataGridViewTriState.True
		InstrumentColumn.Frozen = True
		InstrumentColumn.Width = 300

		Dim DeleteColumn As DataGridViewCheckBoxColumn = New DataGridViewCheckBoxColumn(False)
		DeleteColumn.Name = "Delete"
		DeleteColumn.HeaderText = "Delete"
		Grid_Greeks.Columns.Add(DeleteColumn)
		Grid_Greeks.Columns("Delete").Visible = True
		Grid_Greeks.Columns("Delete").Width = 80


		Grid_Greeks.AllowUserToAddRows = True
		Grid_Greeks.AllowUserToDeleteRows = False
		Grid_Greeks.Sort(Grid_Greeks.Columns("GreekDate"), System.ComponentModel.ListSortDirection.Ascending)
		Grid_Greeks.MultiSelect = False

		Grid_Greeks.Columns("Modified").DisplayIndex = 0
		Grid_Greeks.Columns("InstrumentID").DisplayIndex = 1
		Grid_Greeks.Columns("InstrumentName").DisplayIndex = 2
		Grid_Greeks.Columns("GreekDate").DisplayIndex = 3
		Grid_Greeks.Columns("Delta").DisplayIndex = 4
		Grid_Greeks.Columns("Gamma").DisplayIndex = 5
		Grid_Greeks.Columns("Rho").DisplayIndex = 6
		Grid_Greeks.Columns("Vega").DisplayIndex = 7
		Grid_Greeks.Columns("Theta").DisplayIndex = 8
		Grid_Greeks.Columns("Volatility").DisplayIndex = 9
		Grid_Greeks.Columns("Delete").DisplayIndex = 10

		' Initialise Timer

		GenericUpdateObject = MainForm.AddFormUpdate(Me, AddressOf GetFormData_Tick)

		' Display initial record.

		Call SetSelectCombo()

		Me.Radio_ByDate.Checked = False
		Me.Radio_ByInstrument.Checked = True
		Me.Check_DisplayActiveInstruments.Checked = True

		If Me.Combo_Select.Items.Count > 0 Then
			Me.Combo_Select.SelectedIndex = 0

			LoadSelectedGreeksData(myTable)

			' Re-Set Controls etc.

			Call SetSortedRows()

			Call GetFormData()

		Else

			Me.Combo_Select.SelectedIndex = (-1)
			Call GetFormData()
		End If

		Application.DoEvents()

		InPaint = False

	End Sub

    ''' <summary>
    ''' Handles the Closing event of the frmEntity control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
	Private Sub frmEntity_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		Dim HideForm As Boolean

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...

		_InUse = False
		MainForm.RemoveFormUpdate(Me)

		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else
			If (FormChanged = True) Then
				Call SetFormData()
			End If

			HideForm = True
			If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			e.Cancel = True
		Else
			Try
				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

				' Form Control Changed events

				RemoveHandler Combo_Select.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
				RemoveHandler Combo_Select.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler DateTime_SelectedDate.ValueChanged, AddressOf Combo_SelectComboChanged

			Catch ex As Exception
			End Try
		End If

	End Sub


#End Region

	' Routine to handle changes / updates to tables by this and other windows.
	' If this, or any other, form posts a change to a table, then it will invoke an update event 
	' detailing what tables have been altered.
	' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
	' the 'VeniceAutoUpdate' event of the main Venice form.
	' Each form may them react as appropriate to changes in any table that might impact it.
	'
    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		Dim OrgInPaint As Boolean
		Dim KnowledgeDateChanged As Boolean
		Dim SetButtonStatus_Flag As Boolean
		Dim RefreshForm As Boolean = False

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		OrgInPaint = InPaint
		InPaint = True
		KnowledgeDateChanged = False
		SetButtonStatus_Flag = False

		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
			KnowledgeDateChanged = True
			RefreshForm = True
		End If

		' ****************************************************************
		' Check for changes relevant to this form
		' ****************************************************************

		' Changes to the tblInstrument table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrument) = True) Or KnowledgeDateChanged Then

			' ReBuild the Instrument derived combos

			Call Me.SetSelectCombo()

			If (SelectedInstrumentList_All IsNot Nothing) Then
				SelectedInstrumentList_All.Clear()
				SelectedInstrumentList_All = Nothing
			End If

			RefreshForm = True
		End If

		' Changes to the tblInstrumentType table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrumentType) = True) Or KnowledgeDateChanged Then

			If (SelectedInstrumentList_All IsNot Nothing) Then
				SelectedInstrumentList_All.Clear()
				SelectedInstrumentList_All = Nothing
			End If

			RefreshForm = True
		End If

		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblTransaction) = True) Or KnowledgeDateChanged Then

			If (SelectedInstrumentList_Active IsNot Nothing) Then
				SelectedInstrumentList_Active.Clear()
				SelectedInstrumentList_Active = Nothing
			End If

			If (Radio_ByDate.Checked) And (Check_DisplayActiveInstruments.Checked) Then
				RefreshForm = True
			End If
		End If

		' Changes to the KnowledgeDate :-
		If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
			SetButtonStatus_Flag = True
		End If

		' Changes to the tblUserPermissions table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

			' Check ongoing permissions.

			Call CheckPermissions()
			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

				FormIsValid = False
				Me.Close()
				Exit Sub
			End If

			SetButtonStatus_Flag = True

		End If

		' ****************************************************************
		' Changes to the Main FORM table :-
		' ****************************************************************

		If (e.TableChanged(THIS_FORM_ChangeID) = True) Or KnowledgeDateChanged Then
			RefreshForm = True

			LoadSelectedGreeksData(myTable)

			' Re-Set Controls etc.
			Call SetSortedRows()

			If (Not FormChanged) Then
				RefreshForm = True
			End If

		End If

		' ****************************************************************
		' Repaint if not currently in Edit Mode
		'
		' ****************************************************************

		InPaint = OrgInPaint

		' Retrieve form data if appropriate, i.e. this data item is not already being edited.
		If (RefreshForm = True) AndAlso (FormChanged = False) AndAlso (AddNewRecord = False) Then
			GetFormData()	' Includes a call to 'SetButtonStatus()'
		Else
			If SetButtonStatus_Flag Then
				Call SetButtonStatus()
			End If
		End If

	End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

	' Build Sorted list from the Source dataset and update the Select Combo
    ''' <summary>
    ''' Sets the sorted rows.
    ''' </summary>
	Private Sub SetSortedRows()

		Dim OrgInPaint As Boolean

		'Dim thisrow As DataRow
		'Dim thisDrowDownWidth As Integer
		'Dim SizingBitmap As Bitmap
		'Dim SizingGraphics As Graphics
		Dim SelectString As String = "True"

		' Code is pretty Generic from here on...

		Try

			' Set paint local so that changes to the selection combo do not trigger form updates.

			OrgInPaint = InPaint
			InPaint = True

			' Get selected Row sets, or exit if no data is present.
			If myDataset Is Nothing Then
				ReDim SortedRows(0)
				'ReDim SelectBySortedRows(0)
			Else
				SortedRows = myTable.Select(SelectString, THIS_FORM_OrderBy)
				'SelectBySortedRows = myTable.Select(SelectString, THIS_FORM_SelectBy)
			End If

		Catch ex As Exception
		Finally

			InPaint = OrgInPaint

		End Try

	End Sub

    ''' <summary>
    ''' Sets the active instruments list.
    ''' </summary>
    ''' <param name="pForceRefresh">if set to <c>true</c> [p force refresh].</param>
	Private Sub SetActiveInstrumentsList(Optional ByVal pForceRefresh As Boolean = False)

		Try
			If (Check_DisplayActiveInstruments.Checked) Then
				If (SelectedInstrumentList_Active IsNot Nothing) Then
					If (pForceRefresh = False) Then
						Exit Sub
					End If

					SelectedInstrumentList_Active.Clear()
				End If
			Else
				If (SelectedInstrumentList_All IsNot Nothing) Then
					If (pForceRefresh = False) Then
						Exit Sub
					End If

					SelectedInstrumentList_All.Clear()
				End If
			End If


		Catch ex As Exception
		End Try

		Try

			Dim QueryCommand As New SqlCommand
			Dim ResultsTable As New DataTable
			Dim thisRow As DataRow

			Try
				QueryCommand.CommandType = CommandType.Text
				QueryCommand.CommandText = "SELECT Instrument from [dbo].[fn_ActiveDeltaInstruments](@AllDeltaInstruments, @FundID, @KnowledgeDate)"

				If (Me.Check_DisplayActiveInstruments.Checked) Then
					QueryCommand.Parameters.Add("@AllDeltaInstruments", SqlDbType.Int).Value = 0
				Else
					QueryCommand.Parameters.Add("@AllDeltaInstruments", SqlDbType.Int).Value = 1
				End If
				QueryCommand.Parameters.Add("@FundID", SqlDbType.Int).Value = 0
				QueryCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime).Value = MainForm.Main_Knowledgedate
				QueryCommand.Connection = MainForm.GetVeniceConnection

				SyncLock QueryCommand.Connection
          MainForm.LoadTable_Custom(ResultsTable, QueryCommand)
          'ResultsTable.Load(QueryCommand.ExecuteReader)
        End SyncLock

        If ResultsTable.Rows.Count > 0 Then
          If (Check_DisplayActiveInstruments.Checked) Then
            SelectedInstrumentList_Active = New SortedList(ResultsTable.Rows.Count)

            For Each thisRow In ResultsTable.Rows
              SelectedInstrumentList_Active.Add(CInt(thisRow(0)), True)
            Next
          Else
            SelectedInstrumentList_All = New SortedList(ResultsTable.Rows.Count)

            For Each thisRow In ResultsTable.Rows
              SelectedInstrumentList_All.Add(CInt(thisRow(0)), True)
            Next
          End If

        End If

      Catch ex As Exception
        MainForm.LogError("SetActiveInstrumentsList()", LOG_LEVELS.Error, ex.Message, "Error getting Active Delta positions.", ex.StackTrace)
      Finally
        If (QueryCommand.Connection IsNot Nothing) Then
          QueryCommand.Connection.Close()
        End If
      End Try

    Catch ex As Exception

    End Try

  End Sub

  ' Check User permissions
  ''' <summary>
  ''' Checks the permissions.
  ''' </summary>
  Private Sub CheckPermissions()

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

  End Sub

  ' Flag changes to form controls, Event Associations made in 'New' routine.
  ''' <summary>
  ''' Basic event handler, called when a form value is changed.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    If InPaint = False Then
      If (Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord) Then
        FormChanged = True
        Me.btnSave.Enabled = True
        Me.btnCancel.Enabled = True
        Me.Check_ShowRecent.Enabled = False
      End If
    End If

  End Sub

  ''' <summary>
  ''' Handles the CellChanged event of the Grid control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="C1.Win.C1FlexGrid.RowColEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_CellChanged(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs)
    Call FormControlChanged(Grid_Greeks, New System.EventArgs)
  End Sub

  ''' <summary>
  ''' Selects the menu event.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
  Public Sub SelectMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
    ' Event handler for the Select Menu
    ' Event association is made during the dynamic menu creation

    THIS_FORM_SelectBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

    Call SetSortedRows()

  End Sub

  ''' <summary>
  ''' Orders the menu event.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
  Public Sub OrderMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
    ' Event handler for the Order Menu
    ' Event association is made during the dynamic menu creation

    THIS_FORM_OrderBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

    Call SetSortedRows()

  End Sub


  ''' <summary>
  ''' Audits the report menu event.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
  Public Sub AuditReportMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
    ' Event handler for the Audit Report Menu
    ' Event association is made during the dynamic menu creation

    Select Case CInt(CType(sender, System.Windows.Forms.ToolStripMenuItem).Tag)
      Case 0 ' This Record
        If (Me.AddNewRecord = False) Then
          If Grid_Greeks.SelectedRows.Count > 0 Then
            Run_AuditReport_ThisRecord(MainForm, ThisStandardDataset, CInt(Grid_Greeks.SelectedRows(0).Cells("AuditID").Value), Nothing)
          End If

        End If

      Case 1 ' All Records
        ' Run_AuditReport_ALLRecords(MainForm, ThisStandardDataset, Nothing)

    End Select

  End Sub


#End Region

#Region " Set Form Combos (Form Specific Code) "



#End Region

#Region " Get & Set Form Data / SetButton / ValidateForm / btnCancel Events (Form Specific Code) "

  ''' <summary>
  ''' Gets the form data_ tick.
  ''' </summary>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function GetFormData_Tick() As Boolean
    ' *******************************************************************************
    '
    ' Callback process for the Generic Form Update Process.
    '
    ' Function MUST return True / False on Update.
    ' True will clear the update request. False will not.
    '
    ' *******************************************************************************
    Dim RVal As Boolean = False

    If (InGetFormData_Tick) OrElse (Not _InUse) Then
      Return False
      Exit Function
    End If

    Try
      InGetFormData_Tick = True

      ' Find the correct data row, then show it...

      GetFormData()

      RVal = True

      ' Grid_Greeks.Sort(Grid_Greeks.Columns("PerformanceDate"), System.ComponentModel.ListSortDirection.Ascending)

    Catch ex As Exception
      RVal = False
    Finally
      InGetFormData_Tick = False
    End Try

    Return RVal

  End Function


  ''' <summary>
  ''' Gets the form data.
  ''' </summary>
  Private Sub GetFormData()
    ' ******************************************************************************
    '
    ' Routine to populate form Controls given a data row.
    ' Form is cleared for invalid Datarows or Invalid Form.
    '
    ' ******************************************************************************

    Dim OrgInpaint As Boolean

    ' Set 'Paint' flag to prevent event actions caused by changing the value of form fields.
    OrgInpaint = InPaint
    InPaint = True

    Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

    If (FormIsValid = False) Or (Me.InUse = False) Then
      ' Bad / New Datarow - Clear Form.

      Me.editAuditID.Text = ""

      Grid_Greeks.DataSource = Nothing
      Grid_Greeks.Rows.Clear()

      If AddNewRecord = True Then
        Me.btnCancel.Enabled = True
        Me.GroupBox_SelectByDate.Enabled = False
        Me.GroupBox_SelectByInstrument.Enabled = False
      Else
        Me.btnCancel.Enabled = False
        Me.GroupBox_SelectByDate.Enabled = Me.Radio_ByDate.Checked
        Me.GroupBox_SelectByDate.Visible = Me.Radio_ByDate.Checked
        Me.GroupBox_SelectByInstrument.Enabled = Me.Radio_ByInstrument.Checked
        Me.GroupBox_SelectByInstrument.Visible = Me.Radio_ByInstrument.Checked
      End If

    Else

      ' Populate Form with given data.
      Try

        'PerformanceTable.Rows.Clear()
        'If (Grid_Greeks.DataSource Is Nothing) Then
        '	Grid_Greeks.DataSource = PerformanceTable
        'End If

        ' Populate Grid.
        ' Check_ShowRecent

        Grid_Greeks.Rows.Clear()
        Dim NewGridRow As DataGridViewRow
        Dim ThisTableRow As DSGreeks.tblGreeksRow
        Dim RowCounter As Integer
        Dim StartCounter As Integer = 0

        If (Me.Radio_ByInstrument.Checked) And (Check_ShowRecent.Checked) Then
          StartCounter = Math.Max(0, myTable.Rows.Count - 10)
        End If

        If (Radio_ByDate.Checked) Then

          SetActiveInstrumentsList()

          Dim ThisInstrumentID As Integer
          Dim thisInstrumentList As SortedList

          If (Check_DisplayActiveInstruments.Checked) Then
            thisInstrumentList = SelectedInstrumentList_Active
          Else
            thisInstrumentList = SelectedInstrumentList_All
          End If

          For Each ThisInstrumentID In thisInstrumentList.GetKeyList

            NewGridRow = Grid_Greeks.Rows(Grid_Greeks.Rows.Add())
            NewGridRow.ReadOnly = False

            NewGridRow.Cells("RN").Value = 0
            NewGridRow.Cells("AuditID").Value = 0
            NewGridRow.Cells("GreekID").Value = 0
            NewGridRow.Cells("InstrumentName").Value = CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, ThisInstrumentID, "InstrumentDescription"))
            NewGridRow.Cells("InstrumentID").Value = ThisInstrumentID
            NewGridRow.Cells("GreekDate").Value = Me.DateTime_SelectedDate.Value
            NewGridRow.Cells("Delta").Value = 0
            NewGridRow.Cells("Gamma").Value = 0
            NewGridRow.Cells("Vega").Value = 0
            NewGridRow.Cells("Rho").Value = 0
            NewGridRow.Cells("Theta").Value = 0
            NewGridRow.Cells("Volatility").Value = 0
            NewGridRow.Cells("Modified").Value = False
            NewGridRow.Cells("Delete").Value = False


            For RowCounter = 0 To (myTable.Rows.Count - 1)

              ThisTableRow = myTable.Rows(RowCounter)

              If (ThisTableRow.InstrumentID = ThisInstrumentID) AndAlso (ThisTableRow.GreekDate.Date = DateTime_SelectedDate.Value.Date) Then

                NewGridRow.Cells("RN").Value = ThisTableRow.RN
                NewGridRow.Cells("AuditID").Value = ThisTableRow.AuditID
                NewGridRow.Cells("GreekID").Value = ThisTableRow.GreekID
                NewGridRow.Cells("GreekDate").Value = ThisTableRow.GreekDate
                NewGridRow.Cells("Delta").Value = ThisTableRow.Delta
                NewGridRow.Cells("Gamma").Value = ThisTableRow.Gamma
                NewGridRow.Cells("Vega").Value = ThisTableRow.Vega
                NewGridRow.Cells("Rho").Value = ThisTableRow.Rho
                NewGridRow.Cells("Theta").Value = ThisTableRow.Theta
                NewGridRow.Cells("Volatility").Value = ThisTableRow.Volatility

              End If

            Next RowCounter

          Next

          Grid_Greeks.AllowUserToAddRows = False

          If (Grid_Greeks.SortedColumn.Name = "GreekDate") Then
            Grid_Greeks.Sort(Grid_Greeks.Columns("InstrumentName"), System.ComponentModel.ListSortDirection.Ascending)
          End If

          Grid_Greeks.Columns("InstrumentName").Visible = True
          Grid_Greeks.Columns("InstrumentID").Visible = True

        ElseIf (Radio_ByInstrument.Checked) Then

          For RowCounter = StartCounter To (myTable.Rows.Count - 1)

            ThisTableRow = myTable.Rows(RowCounter)

            NewGridRow = Grid_Greeks.Rows(Grid_Greeks.Rows.Add())
            NewGridRow.ReadOnly = False

            NewGridRow.Cells("RN").Value = ThisTableRow.RN
            NewGridRow.Cells("AuditID").Value = ThisTableRow.AuditID
            NewGridRow.Cells("GreekID").Value = ThisTableRow.GreekID
            NewGridRow.Cells("InstrumentName").Value = ""
            NewGridRow.Cells("InstrumentID").Value = ThisTableRow.InstrumentID
            NewGridRow.Cells("GreekDate").Value = ThisTableRow.GreekDate
            NewGridRow.Cells("Delta").Value = ThisTableRow.Delta
            NewGridRow.Cells("Gamma").Value = ThisTableRow.Gamma
            NewGridRow.Cells("Vega").Value = ThisTableRow.Vega
            NewGridRow.Cells("Rho").Value = ThisTableRow.Rho
            NewGridRow.Cells("Theta").Value = ThisTableRow.Theta
            NewGridRow.Cells("Volatility").Value = ThisTableRow.Volatility
            NewGridRow.Cells("Modified").Value = False
            NewGridRow.Cells("Delete").Value = False

          Next

          Grid_Greeks.AllowUserToAddRows = True

          If (Grid_Greeks.SortedColumn.Name = "InstrumentName") Then
            Grid_Greeks.Sort(Grid_Greeks.Columns("GreekDate"), System.ComponentModel.ListSortDirection.Ascending)
          End If

          Grid_Greeks.Columns("InstrumentName").Visible = False
          Grid_Greeks.Columns("InstrumentID").Visible = False

        End If

        ' Format Grid.

        If (Grid_Greeks.SortOrder = Windows.Forms.SortOrder.Descending) Then
          Grid_Greeks.Sort(Grid_Greeks.SortedColumn, System.ComponentModel.ListSortDirection.Descending)
        Else
          Grid_Greeks.Sort(Grid_Greeks.SortedColumn, System.ComponentModel.ListSortDirection.Ascending)
        End If

        AddNewRecord = False

        Me.btnCancel.Enabled = False
        Me.GroupBox_SelectByDate.Enabled = Me.Radio_ByDate.Checked
        Me.GroupBox_SelectByDate.Visible = Me.Radio_ByDate.Checked
        Me.GroupBox_SelectByInstrument.Enabled = Me.Radio_ByInstrument.Checked
        Me.GroupBox_SelectByInstrument.Visible = Me.Radio_ByInstrument.Checked

      Catch ex As Exception

        Call MainForm.LogError(Me.Name & ", GetFormData", 0, ex.Message, "Error Showing Data", ex.StackTrace, True)
        Grid_Greeks.Rows.Clear()

      End Try

    End If

    ' Allow Field events to trigger before 'InPaint' Is re-set. 
    ' (Should) Prevent Validation errors during Form Draw.
    Application.DoEvents()

    ' Restore 'Paint' flag.
    InPaint = OrgInpaint
    FormChanged = False
    Me.btnSave.Enabled = False
    Me.Cursor = System.Windows.Forms.Cursors.Default

    ' As it says on the can....
    Call SetButtonStatus()

  End Sub

  ''' <summary>
  ''' Sets the form data.
  ''' </summary>
  ''' <param name="pConfirm">if set to <c>true</c> [p confirm].</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function SetFormData(Optional ByVal pConfirm As Boolean = True) As Boolean
    ' *************************************************************
    '
    ' Note that GroupList Entries are also added from the FundSearch Form. Changes
    ' to the GroupList table should be reflected there also.
    '
    ' *************************************************************
    Dim ErrMessage As String
    Dim ErrStack As String
    Dim ThisInstrumentID As Integer
    Dim ProtectedItem As Boolean = False
    Dim ChildInstrumentIDs As ArrayList = Nothing

    ErrMessage = ""
    ErrStack = ""

    ' *************************************************************
    ' Appropriate Save permission :-
    ' *************************************************************

    If (Me.HasUpdatePermission = False) And ((Me.HasInsertPermission = False) Or (Me.AddNewRecord = False)) Then
      MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "You do not have permission to save this record.", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Function
    End If

    ' *************************************************************
    ' If Save button is disabled then should not be able to save, exit silently.
    ' *************************************************************
    If Me.btnSave.Enabled = False Then
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Function
    End If

    ' *************************************************************
    ' KnowledgeDate OK :-
    ' *************************************************************

    If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
      MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Changes to Venice are not allowed when the Knowledgedate is not set to `NOW`", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Function
    End If

    ' Confirm Save, if required.

    If (pConfirm = True) Then
      If MessageBox.Show("Save Changes ?", "Save Changes ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
        Return True
        Exit Function
      End If
    End If

    ' *************************************************************
    ' Procedure to Save the current form information.
    ' *************************************************************

    Dim StatusString As String

    If (FormChanged = False) Or (FormIsValid = False) Then
      Return False
      Exit Function
    End If

    ' Instrument ID

    If (Radio_ByInstrument.Checked) Then
      If (Combo_Select.SelectedIndex >= 0) AndAlso (IsNumeric(Combo_Select.SelectedValue)) AndAlso (CInt(Combo_Select.SelectedValue) > 0) Then
        ThisInstrumentID = CInt(Combo_Select.SelectedValue)
      Else
        MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Instrument Combo : Instrument appears not to be selected. Cant get Instrument ID.", "", True)
        Return False
        Exit Function
      End If
    End If


    ' Validation
    StatusString = ""
    If ValidateForm(StatusString) = False Then
      MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Validation Error." & vbCrLf & StatusString, "", True)
      Return False
      Exit Function
    End If


    ' *************************************************************
    ' Collect Greeks to Change
    ' *************************************************************

    Dim GreeksUpdateTable As New DSGreeks.tblGreeksDataTable
    Dim NewGreeksRow As DSGreeks.tblGreeksRow
    Dim SortedGreeksArray() As DSGreeks.tblGreeksRow = Nothing
    Dim ThisGridRow As DataGridViewRow = Nothing
    Dim UpdateString As String = ""
    Dim Counter As Integer

    Try

      For Each ThisGridRow In Grid_Greeks.Rows
        If (Not ThisGridRow.IsNewRow) AndAlso (ThisGridRow.Cells("Modified").Value) Then
          NewGreeksRow = GreeksUpdateTable.NewtblGreeksRow

          NewGreeksRow.GreekID = CInt(ThisGridRow.Cells("GreekID").Value)
          NewGreeksRow.InstrumentID = CInt(ThisGridRow.Cells("InstrumentID").Value)
          NewGreeksRow.GreekDate = CDate(ThisGridRow.Cells("GreekDate").Value)
          NewGreeksRow.Delta = CDbl(ThisGridRow.Cells("Delta").Value)
          NewGreeksRow.Gamma = CDbl(ThisGridRow.Cells("Gamma").Value)
          NewGreeksRow.Vega = CDbl(ThisGridRow.Cells("Vega").Value)
          NewGreeksRow.Rho = CDbl(ThisGridRow.Cells("Rho").Value)
          NewGreeksRow.Theta = CDbl(ThisGridRow.Cells("Theta").Value)
          NewGreeksRow.Volatility = CDbl(ThisGridRow.Cells("Volatility").Value)

          ' Add New Row to the Update Table.

          GreeksUpdateTable.Rows.Add(NewGreeksRow)
          NewGreeksRow = Nothing

        End If

      Next

    Catch ex As Exception

      MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Error, "Error Getting changed Greeks from Grid.", ex.Message, ex.StackTrace, True)
      Return False
      Exit Function

    End Try

    Try

      SortedGreeksArray = GreeksUpdateTable.Select("True", "GreekDate")

    Catch ex As Exception
    End Try

    Try

      If (SortedGreeksArray IsNot Nothing) AndAlso (SortedGreeksArray.Length > 0) Then

        MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblGreeks, SortedGreeksArray)

        For Counter = 0 To (SortedGreeksArray.Length - 1)
          NewGreeksRow = SortedGreeksArray(Counter)

          If (NewGreeksRow.IsAuditIDNull = False) Then
            If (Counter = 0) Then
              UpdateString = NewGreeksRow.AuditID.ToString()
            Else
              UpdateString &= "," & NewGreeksRow.AuditID.ToString()
            End If
          End If
        Next
      End If

    Catch ex As Exception

      MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Error, "Error Saving Greeks.", ex.Message, ex.StackTrace, True)

    End Try

    Try

      ' Reload table / Update Event.

      FormChanged = False

      ' Do not make this a background task.
      MainForm.ReloadTable_Background(RenaissanceChangeID.tblGreeks, UpdateString, True)

      'Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID))

      Application.DoEvents()

    Catch ex As Exception

      MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Error, "Error Reloading / Updating after Greeks Updates.", ex.Message, ex.StackTrace, True)

    End Try


  End Function

  ''' <summary>
  ''' Sets the button status.
  ''' </summary>
  Private Sub SetButtonStatus()
    ' Sets the status of the form controlls appropriate to the current users 
    'permissions and the 'Changed' or 'New' status of the form.

    ' No Read permission :-

    If Me.HasReadPermission = False Then
      MainForm.LogError(Me.Name & ", SetButtonStatus()", 0, "", "You do not have Read permission for this Form", "", True)
      Me.Close()
      Exit Sub
    End If

    If (Me.MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And _
     ((Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord)) Then

      Me.Grid_Greeks.Enabled = True

    Else

      Me.Grid_Greeks.Enabled = False

    End If

  End Sub

  ''' <summary>
  ''' Validates the form.
  ''' </summary>
  ''' <param name="pReturnString">The p return string.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function ValidateForm(ByRef pReturnString As String) As Boolean
    ' Form Validation code.
    ' 
    ' This code should be the final arbiter of what is allowed. no assumptions regarding 
    ' prior validation should be made.
    ' 
    ' This Code is called by the SetFormData routine before position changes.
    '
    Dim RVal As Boolean

    RVal = True
    pReturnString = ""

    ' Validate 

    Return RVal

  End Function

  ''' <summary>
  ''' Handles the MouseEnter event of the btnCancel control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnCancel_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseEnter
    ' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
    Me.IsOverCancelButton = True
  End Sub

  ''' <summary>
  ''' Handles the MouseLeave event of the btnCancel control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnCancel_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseLeave
    ' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
    Me.IsOverCancelButton = False
  End Sub

#End Region

#Region " Navigation Code / GetPosition() (Generic Code) "

  ''' <summary>
  ''' Handles the SelectComboChanged event of the Combo control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_SelectComboChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' ***************************************************************
    ' Selection Combo. SelectedItem changed.
    '
    ' ***************************************************************

    ' Don't react to changes made in paint routines etc.

    If InPaint Then Exit Sub

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    ' Get Instrument Greeks Data

    LoadSelectedGreeksData(myTable)

    ' Re-Set Controls etc.
    Call SetSortedRows()

    If (GenericUpdateObject IsNot Nothing) Then
      Try
        GenericUpdateObject.FormToUpdate = True  '	GetFormDataToUpdate = True
      Catch ex As Exception
      End Try
    Else
      Call GetFormData()
    End If

  End Sub


  ''' <summary>
  ''' Handles the Click event of the btnNavPrev control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnNavPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavPrev.Click
    ' 'Previous' Button.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    If (Me.Radio_ByDate.Checked) Then
      Me.DateTime_SelectedDate.Value = FitDateToPeriod(DEFAULT_DATA_PERIOD, AddPeriodToDate(DEFAULT_DATA_PERIOD, DateTime_SelectedDate.Value, -1), True)
    ElseIf (Me.Radio_ByInstrument.Checked) Then
      If (Combo_Select.Items.Count > 0) AndAlso (Combo_Select.SelectedIndex > 0) Then
        Combo_Select.SelectedIndex = Math.Max(Combo_Select.SelectedIndex - 1, 0)
      End If
    End If

  End Sub

  ''' <summary>
  ''' Handles the Click event of the btnNavNext control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnNavNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavNext.Click
    ' 'Next' Button.


    If (Me.Radio_ByDate.Checked) Then
      Me.DateTime_SelectedDate.Value = FitDateToPeriod(DEFAULT_DATA_PERIOD, AddPeriodToDate(DEFAULT_DATA_PERIOD, DateTime_SelectedDate.Value, 1), True)
    ElseIf (Me.Radio_ByInstrument.Checked) Then
      If (Combo_Select.Items.Count > 0) AndAlso (Combo_Select.SelectedIndex < Combo_Select.Items.Count) Then
        Combo_Select.SelectedIndex = Math.Min(Combo_Select.SelectedIndex + 1, Combo_Select.Items.Count - 1)
      End If
    End If

  End Sub

  ''' <summary>
  ''' Handles the Click event of the btnNavFirst control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnNavFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavFirst.Click
    ' 'First' Button.

    If (FormChanged = True) Then
      Call SetFormData()
    End If


    If (Me.Radio_ByDate.Checked) Then
      Me.DateTime_SelectedDate.Value = FitDateToPeriod(DealingPeriod.Weekly, FitDateToPeriod(DealingPeriod.Annually, DateTime_SelectedDate.Value, False), False)
    ElseIf (Me.Radio_ByInstrument.Checked) Then
      If (Combo_Select.Items.Count > 0) Then
        Combo_Select.SelectedIndex = 0
      End If
    End If
  End Sub

  ''' <summary>
  ''' Handles the Click event of the btnLast control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLast.Click
    ' 'Last' Button.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    If (Me.Radio_ByDate.Checked) Then
      Me.DateTime_SelectedDate.Value = FitDateToPeriod(DealingPeriod.Weekly, FitDateToPeriod(DealingPeriod.Annually, DateTime_SelectedDate.Value, True), True)
    ElseIf (Me.Radio_ByInstrument.Checked) Then
      If (Combo_Select.Items.Count > 0) Then
        Combo_Select.SelectedIndex = Combo_Select.Items.Count - 1
      End If
    End If
  End Sub


#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

  ''' <summary>
  ''' Handles the Click event of the btnCancel control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    ' Cancel Changes, redisplay form.

    FormChanged = False
    AddNewRecord = False

    Call GetFormData()

  End Sub

  ''' <summary>
  ''' Handles the Click event of the btnSave control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
    ' Save Changes, if any, without prompting.

    If (FormChanged = True) Then
      Call SetFormData(False)
    End If

  End Sub

  ''' <summary>
  ''' Handles the Click event of the btnClose control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
    ' *************************************************************
    ' Close Form
    ' *************************************************************

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    Me.Close()

  End Sub

#End Region

#Region " Form Control Event Code"

  ''' <summary>
  ''' Handles the CheckedChanged event of the Radio_ByInstrument control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Radio_ByInstrument_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_ByInstrument.CheckedChanged
    ' ************************************************************************************
    '
    '
    ' ************************************************************************************

    Try
      If (Radio_ByInstrument.Checked) Then

        If (FormChangedFlag) Then
          Call SetFormData(True)
        End If

        Radio_ByDate.Checked = False
        GroupBox_SelectByInstrument.Enabled = True
        GroupBox_SelectByInstrument.Visible = True
        GroupBox_SelectByDate.Enabled = False
        GroupBox_SelectByDate.Visible = False

        LoadSelectedGreeksData(myTable)

        If (Not InPaint) And (Radio_ByInstrument.Checked) Then
          GetFormData()
        End If
      End If

    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Handles the CheckedChanged event of the Radio_ByDate control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Radio_ByDate_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_ByDate.CheckedChanged
    ' ************************************************************************************
    '
    '
    ' ************************************************************************************

    Try
      If (Radio_ByDate.Checked) Then

        If (FormChangedFlag) Then
          Call SetFormData(True)
        End If

        Radio_ByInstrument.Checked = False
        GroupBox_SelectByDate.Enabled = True
        GroupBox_SelectByDate.Visible = True
        GroupBox_SelectByInstrument.Enabled = False
        GroupBox_SelectByInstrument.Visible = False

        LoadSelectedGreeksData(myTable)

        If (Not InPaint) And (Radio_ByDate.Checked) Then
          GetFormData()
        End If
      End If


    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Handles the CheckedChanged event of the Check_ShowRecent control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Check_ShowRecent_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_ShowRecent.CheckedChanged
    ' ************************************************************************************
    '
    '
    ' ************************************************************************************

    Try
      If (Not InPaint) AndAlso (Not FormChangedFlag) Then
        GetFormData()
      End If
    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Handles the CheckedChanged event of the Check_DisplayActiveInstruments control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Check_DisplayActiveInstruments_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_DisplayActiveInstruments.CheckedChanged
    ' ************************************************************************************
    '
    '
    ' ************************************************************************************

    Try
      If (Not InPaint) AndAlso (Not FormChangedFlag) Then
        GetFormData()
      End If
    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Handles the CellEnter event of the Grid_Greeks control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.Windows.Forms.DataGridViewCellEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Greeks_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Grid_Greeks.CellEnter
    ' ****************************************************************************************
    '
    '
    ' ****************************************************************************************

    Try
      If (Me.IsDisposed) OrElse (Not Me.Created) Then
        Exit Sub
      End If

      If (Grid_Greeks.Rows(e.RowIndex).IsNewRow) Then
        If (Not Grid_Greeks.Focused) Then
          Exit Sub
        End If

        If IsNumeric(Grid_Greeks.Item(Grid_Greeks.Columns("GreekID").Index, e.RowIndex).Value) = False Then
          Grid_Greeks.Item(Grid_Greeks.Columns("RN").Index, e.RowIndex).Value = 0
          Grid_Greeks.Item(Grid_Greeks.Columns("AuditID").Index, e.RowIndex).Value = 0
          Grid_Greeks.Item(Grid_Greeks.Columns("GreekID").Index, e.RowIndex).Value = 0
          Grid_Greeks.Item(Grid_Greeks.Columns("InstrumentID").Index, e.RowIndex).Value = CInt(Me.Combo_Select.SelectedValue)
          Grid_Greeks.Item(Grid_Greeks.Columns("GreekDate").Index, e.RowIndex).Value = Now.Date
          Grid_Greeks.Item(Grid_Greeks.Columns("Delta").Index, e.RowIndex).Value = 0
          Grid_Greeks.Item(Grid_Greeks.Columns("Gamma").Index, e.RowIndex).Value = 0
          Grid_Greeks.Item(Grid_Greeks.Columns("Vega").Index, e.RowIndex).Value = 0
          Grid_Greeks.Item(Grid_Greeks.Columns("Theta").Index, e.RowIndex).Value = 0
          Grid_Greeks.Item(Grid_Greeks.Columns("Rho").Index, e.RowIndex).Value = 0
          Grid_Greeks.Item(Grid_Greeks.Columns("Volatility").Index, e.RowIndex).Value = 0
          Grid_Greeks.Item(Grid_Greeks.Columns("Modified").Index, e.RowIndex).Value = False
          Grid_Greeks.Item(Grid_Greeks.Columns("Delete").Index, e.RowIndex).Value = False

          If (Radio_ByInstrument.Checked) Then
            If (e.RowIndex > 0) AndAlso IsDate(Grid_Greeks.Item(Grid_Greeks.Columns("GreekDate").Index, e.RowIndex - 1).Value) Then
              Grid_Greeks.Item(Grid_Greeks.Columns("GreekDate").Index, e.RowIndex).Value = FitDateToPeriod(DEFAULT_DATA_PERIOD, AddPeriodToDate(DEFAULT_DATA_PERIOD, CDate(Grid_Greeks.Item(Grid_Greeks.Columns("GreekDate").Index, e.RowIndex - 1).Value), 1), True)
            Else
              Grid_Greeks.Item(Grid_Greeks.Columns("GreekDate").Index, e.RowIndex).Value = FitDateToPeriod(DEFAULT_DATA_PERIOD, Now.Date, True)
            End If
          End If

          Grid_Greeks.BeginEdit(True)

        End If

      End If

      If (e.RowIndex >= 0) Then
        If Grid_Greeks.Columns(e.ColumnIndex).Visible AndAlso (Grid_Greeks.Columns(e.ColumnIndex).ValueType Is GetType(Double)) Then
          Dim ThisGridRow As DataGridViewRow = Grid_Greeks.Rows(e.RowIndex)

          Select Case e.ColumnIndex

            Case Grid_Greeks.Columns("Volatility").Index

              ThisGridRow.Cells(e.ColumnIndex).Style.Format = "#,##0.00######%;-#,##0.00######%"

            Case Else

              ThisGridRow.Cells(e.ColumnIndex).Style.Format = "#,##0.00######;-#,##0.00######"

          End Select

        End If
      End If
    Catch ex As Exception

    End Try
  End Sub

  ''' <summary>
  ''' Handles the CellLeave event of the Grid_Greeks control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.Windows.Forms.DataGridViewCellEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Greeks_CellLeave(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Grid_Greeks.CellLeave
    ' ****************************************************************************************
    '
    '
    ' ****************************************************************************************

    Try
      If (Me.IsDisposed) OrElse (Not Me.Created) Then
        Exit Sub
      End If

      ' If the row being left is the 'NewRow' row, then nothing has been entered on it and it should be cleared.

      If (Grid_Greeks.Rows(e.RowIndex).IsNewRow) Then
        'Grid_Greeks.Item(Grid_Greeks.Columns("RN").Index, e.RowIndex).Value = 0
        'Grid_Greeks.Item(Grid_Greeks.Columns("AuditID").Index, e.RowIndex).Value = 0
        'Grid_Greeks.Item(Grid_Greeks.Columns("GreekID").Index, e.RowIndex).Value = 0
        'Grid_Greeks.Item(Grid_Greeks.Columns("InstrumentID").Index, e.RowIndex).Value = 0
        'Grid_Greeks.Item(Grid_Greeks.Columns("GreekDate").Index, e.RowIndex).Value = Now.Date
        'Grid_Greeks.Item(Grid_Greeks.Columns("Delta").Index, e.RowIndex).Value = 0
        'Grid_Greeks.Item(Grid_Greeks.Columns("Gamma").Index, e.RowIndex).Value = 0
        'Grid_Greeks.Item(Grid_Greeks.Columns("Vega").Index, e.RowIndex).Value = 0
        'Grid_Greeks.Item(Grid_Greeks.Columns("Rho").Index, e.RowIndex).Value = 0
        'Grid_Greeks.Item(Grid_Greeks.Columns("Theta").Index, e.RowIndex).Value = 0
        'Grid_Greeks.Item(Grid_Greeks.Columns("Volatility").Index, e.RowIndex).Value = 0
        'Grid_Greeks.Item(Grid_Greeks.Columns("Modified").Index, e.RowIndex).Value = False
        'Grid_Greeks.Item(Grid_Greeks.Columns("Delete").Index, e.RowIndex).Value = False

      End If

      If (e.RowIndex >= 0) Then
        If Grid_Greeks.Columns(e.ColumnIndex).Visible AndAlso (Grid_Greeks.Columns(e.ColumnIndex).ValueType Is GetType(Double)) Then
          Dim ThisGridRow As DataGridViewRow = Grid_Greeks.Rows(e.RowIndex)

          ThisGridRow.Cells(e.ColumnIndex).Style.Format = Grid_Greeks.Columns(e.ColumnIndex).DefaultCellStyle.Format

        End If
      End If

    Catch ex As Exception

    End Try

  End Sub

  ''' <summary>
  ''' Handles the CellValueChanged event of the Grid_Greeks control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.Windows.Forms.DataGridViewCellEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Greeks_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Grid_Greeks.CellValueChanged
    ' ****************************************************************************************
    '
    '
    ' ****************************************************************************************

    Try
      If (Grid_Greeks.Columns.Contains("Modified")) Then
        If (e.ColumnIndex = Grid_Greeks.Columns("Modified").Index) Then
          Exit Sub
        End If
      End If

      Dim ThisGridRow As DataGridViewRow = Nothing

      If (Not InPaint) Then
        Call FormControlChanged(Grid_Greeks, Nothing)
        Grid_Greeks.Item(Grid_Greeks.Columns("Modified").Index, e.RowIndex).Value = True
      End If

      ' Format Cell

      If (e.RowIndex >= 0) Then
        ThisGridRow = Grid_Greeks.Rows(e.RowIndex)

        Select Case e.ColumnIndex

          Case Grid_Greeks.Columns("Delta").Index, Grid_Greeks.Columns("Gamma").Index, Grid_Greeks.Columns("Vega").Index, Grid_Greeks.Columns("Rho").Index, Grid_Greeks.Columns("Theta").Index, Grid_Greeks.Columns("Volatility").Index

            If IsNumeric(Grid_Greeks.Item(e.ColumnIndex, e.RowIndex).Value) Then
              If CDbl(Grid_Greeks.Item(e.ColumnIndex, e.RowIndex).Value) < 0 Then
                Grid_Greeks.Item(e.ColumnIndex, e.RowIndex).Style.ForeColor = Color.Red
              Else
                If Grid_Greeks.Item(e.ColumnIndex, e.RowIndex).Style.ForeColor <> Grid_Greeks.Columns(e.ColumnIndex).DefaultCellStyle.ForeColor Then
                  Grid_Greeks.Item(e.ColumnIndex, e.RowIndex).Style.ForeColor = Grid_Greeks.Columns(e.ColumnIndex).DefaultCellStyle.ForeColor
                End If
              End If
            End If

        End Select
      End If

    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Handles the CellContentClick event of the Grid_Greeks control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.Windows.Forms.DataGridViewCellEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Greeks_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Grid_Greeks.CellContentClick
    ' ****************************************************************************************
    '
    '
    ' ****************************************************************************************

    'Try

    '	If (Not InPaint) AndAlso (e.RowIndex >= 0) Then
    '		Call FormControlChanged(Grid_Greeks, Nothing)
    '		Grid_Greeks.Item(Grid_Greeks.Columns("Modified").Index, e.RowIndex).Value = True
    '	End If

    'Catch ex As Exception
    'End Try

  End Sub

  ''' <summary>
  ''' Handles the CellParsing event of the Grid_Greeks control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.Windows.Forms.DataGridViewCellParsingEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Greeks_CellParsing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellParsingEventArgs) Handles Grid_Greeks.CellParsing
    ' ****************************************************************************************
    '
    '
    ' ****************************************************************************************

    Try
      e.Value = ConvertValue(e.Value, e.DesiredType)
      e.ParsingApplied = True
    Catch ex As Exception
      e.ParsingApplied = False
    End Try
  End Sub

  ''' <summary>
  ''' Handles the RowsRemoved event of the Grid_Greeks control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.Windows.Forms.DataGridViewRowsRemovedEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Greeks_RowsRemoved(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowsRemovedEventArgs) Handles Grid_Greeks.RowsRemoved
    ' ****************************************************************************************
    '
    '
    ' ****************************************************************************************


    Try
      If (Not InPaint) Then
        If (Not Grid_Greeks.Rows(e.RowIndex).IsNewRow) Then
          Try
            ' Reflect deletion in data table

            'Dim ThisID As Integer
            'Dim ThisDate As Date

            'ThisID = Grid_Greeks.Rows(e.RowIndex).Cells("ID").Value
            'ThisDate = Grid_Greeks.Rows(e.RowIndex).Cells("PerformanceDate").Value

            'For Each ThisTableRow As DSPerformance.tblPerformanceRow In PerformanceTable.Select("(ID=" & ThisID.ToString & ") AND (PerformanceDate='" & ThisDate.ToString(QUERY_SHORTDATEFORMAT) & "')")
            '	ThisTableRow.Delete()
            'Next

          Catch ex As Exception
          End Try
        End If

        Call FormControlChanged(Grid_Greeks, Nothing)
      End If
    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Handles the DataError event of the Grid_Greeks control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.Windows.Forms.DataGridViewDataErrorEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Greeks_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles Grid_Greeks.DataError

  End Sub

  ''' <summary>
  ''' The _valid data
  ''' </summary>
  Private _validData As Boolean

  ''' <summary>
  ''' Handles the DragEnter event of the Grid_Greeks control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.Windows.Forms.DragEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Greeks_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles Grid_Greeks.DragEnter
    ' ***********************************************************************************
    '
    '
    ' ***********************************************************************************

    Try
      ' Get the data
      Dim DragString As String = CType(e.Data.GetData(GetType(String)), String)

      ' No empty data
      If DragString Is Nothing OrElse DragString.Length = 0 Then
        _validData = False
        Return
      End If

      If (DragString IsNot Nothing) Then
        If DragString.Split(Chr(13)).Length > 1 Then
          _validData = True
          Return
        End If
      End If

      _validData = False
    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Handles the DragLeave event of the Grid_Greeks control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Greeks_DragLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid_Greeks.DragLeave
    ' ***********************************************************************************
    '
    '
    ' ***********************************************************************************

    Try
      _validData = False
    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Handles the DragOver event of the Grid_Greeks control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.Windows.Forms.DragEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Greeks_DragOver(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles Grid_Greeks.DragOver
    ' ***********************************************************************************
    '
    '
    ' ***********************************************************************************

    Try
      If _validData Then
        e.Effect = DragDropEffects.Copy
      Else
        e.Effect = DragDropEffects.None
      End If
    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Handles the DragDrop event of the Grid_Greeks control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.Windows.Forms.DragEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Greeks_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles Grid_Greeks.DragDrop
    ' ***********************************************************************************
    '
    '
    ' ***********************************************************************************

    Try
      'get the data
      Dim DragString As String = CType(e.Data.GetData(GetType(String)), String)

      Grid_Greeks_ParseString(DragString)

    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Grid_s the greeks_ parse string.
  ''' </summary>
  ''' <param name="ReturnsString">The returns string.</param>
  Private Sub Grid_Greeks_ParseString(ByVal ReturnsString As String)
    ' ***********************************************************************************
    '
    '
    ' ***********************************************************************************

    'Try
    '	' No data
    '	If (ReturnsString Is Nothing) OrElse (ReturnsString.Length <= 0) Then
    '		Exit Sub
    '	End If

    '	'get the data
    '	Dim DragString As String = ReturnsString ' CType(e.Data.GetData(GetType(String)), String)

    '	Dim DroppedRows() As String
    '	Dim DroppedCols() As String
    '	Dim RowCount As Integer
    '	Dim ColCount As Integer
    '	Dim IsInRows As Boolean = False
    '	Dim IsInCols As Boolean = False
    '	Dim DateValues(-1) As Date
    '	Dim ReturnValue(-1) As Double
    '	Dim ReturnIndex As Integer
    '	Dim SourceIndex As Integer

    '	DroppedRows = DragString.Split(New String() {CStr(Chr(13)) & CStr(Chr(10))}, StringSplitOptions.RemoveEmptyEntries)

    '	If (DroppedRows Is Nothing) OrElse (DroppedRows.Length <= 0) Then
    '		Exit Sub
    '	End If
    '	RowCount = DroppedRows.Length

    '	DroppedCols = DroppedRows(0).Split(Chr(9))
    '	If (DroppedCols Is Nothing) OrElse (DroppedCols.Length <= 0) Then
    '		Exit Sub
    '	End If
    '	ColCount = DroppedCols.Length

    '	If (RowCount <= 1) And (ColCount <= 1) Then
    '		Exit Sub
    '	End If

    '	' OK, Now try to figure out what format the Data is in.

    '	If (RowCount = 1) Then
    '		' Must be <Date>, <Value> [,<Date>, <Value>] .... on a single line to be valid.

    '		ReturnIndex = 0

    '		For SourceIndex = 0 To (CInt(ColCount / 2) - 1)
    '			If (ConvertIsNumeric(DroppedCols((SourceIndex * 2) + 1))) Then
    '				If IsDate(DroppedCols(SourceIndex * 2)) Then
    '					ReturnIndex += 1
    '				ElseIf IsNumeric(DroppedCols(SourceIndex * 2)) AndAlso (CDbl(DroppedCols(SourceIndex * 2)) > 10000) Then
    '					ReturnIndex += 1
    '				End If
    '			End If
    '		Next

    '		If (ReturnIndex > 0) Then
    '			ReDim DateValues(ReturnIndex - 1)
    '			ReDim ReturnValue(ReturnIndex - 1)

    '			ReturnIndex = 0

    '			For SourceIndex = 0 To (CInt(ColCount / 2) - 1)
    '				If (ConvertIsNumeric(DroppedCols((SourceIndex * 2) + 1))) Then
    '					If IsDate(DroppedCols(SourceIndex * 2)) Then
    '						DateValues(ReturnIndex) = Date.Parse(DroppedCols(SourceIndex * 2))
    '						ReturnValue(ReturnIndex) = ConvertValue(DroppedCols((SourceIndex * 2) + 1), GetType(Double))

    '						ReturnIndex += 1
    '					ElseIf IsNumeric(DroppedCols(SourceIndex * 2)) AndAlso (CDbl(DroppedCols(SourceIndex * 2)) > 10000) Then
    '						DateValues(ReturnIndex) = Date.FromOADate(CDbl(DroppedCols(SourceIndex * 2)))
    '						ReturnValue(ReturnIndex) = ConvertValue(DroppedCols((SourceIndex * 2) + 1), GetType(Double))

    '						ReturnIndex += 1
    '					End If
    '				End If

    '				If (ReturnIndex >= DateValues.Length) Then
    '					Exit For
    '				End If
    '			Next
    '		End If

    '	ElseIf (ColCount = 1) And (RowCount = 2) Then
    '		' Must be in <Date>, <Value> to be valid.

    '		If (ConvertIsNumeric(DroppedRows(1))) Then
    '			If IsDate(DroppedRows(0)) Then
    '				ReDim DateValues(0)
    '				ReDim ReturnValue(0)

    '				DateValues(0) = Date.Parse(DroppedRows(0))
    '				ReturnValue(0) = ConvertValue(DroppedRows(1), GetType(Double))
    '			ElseIf IsNumeric(DroppedRows(0)) AndAlso (CDbl(IsNumeric(DroppedRows(0))) > 10000) Then
    '				ReDim DateValues(0)
    '				ReDim ReturnValue(0)

    '				DateValues(0) = Date.FromOADate(CDbl(DroppedRows(0)))
    '				ReturnValue(0) = ConvertValue(DroppedRows(1), GetType(Double))
    '			End If

    '		End If

    '	ElseIf (ColCount > 1) And (RowCount > 1) Then

    '		If (ColCount = 2) And (RowCount = 2) Then
    '			' In Rows or Cols >

    '			If IsDate(DroppedCols(1)) Then
    '				IsInRows = True
    '			ElseIf IsNumeric(DroppedCols(1)) AndAlso (CDbl(DroppedCols(1)) > 10000) Then
    '				IsInRows = True
    '			Else
    '				IsInCols = True
    '			End If

    '		ElseIf (ColCount = 2) Then
    '			IsInCols = True

    '		ElseIf (RowCount = 2) Then
    '			IsInRows = True
    '		End If

    '		If (IsInRows) Then
    '			Dim DroppedDates() As String = DroppedRows(0).Split(Chr(9))
    '			Dim DroppedReturns() As String = DroppedRows(1).Split(Chr(9))

    '			ReturnIndex = 0

    '			If (DroppedDates IsNot Nothing) AndAlso (DroppedReturns IsNot Nothing) AndAlso (DroppedDates.Length = DroppedReturns.Length) Then
    '				For SourceIndex = 0 To (ColCount - 1)
    '					If (ConvertIsNumeric(DroppedReturns(SourceIndex))) Then
    '						If (IsDate(DroppedDates(SourceIndex))) Then
    '							ReturnIndex += 1
    '						ElseIf (IsNumeric(DroppedDates(SourceIndex))) AndAlso (CDbl(DroppedDates(SourceIndex)) > 10000) Then
    '							ReturnIndex += 1
    '						End If
    '					End If
    '				Next

    '				If (ReturnIndex > 0) Then
    '					ReDim DateValues(ReturnIndex - 1)
    '					ReDim ReturnValue(ReturnIndex - 1)

    '					ReturnIndex = 0

    '					For SourceIndex = 0 To (ColCount - 1)
    '						If (ConvertIsNumeric(DroppedReturns(SourceIndex))) Then
    '							If (IsDate(DroppedDates(SourceIndex))) Then
    '								DateValues(ReturnIndex) = Date.Parse(DroppedDates(SourceIndex))
    '								ReturnValue(ReturnIndex) = ConvertValue(DroppedReturns(SourceIndex), GetType(Double))

    '								ReturnIndex += 1
    '							ElseIf (IsNumeric(DroppedDates(SourceIndex))) AndAlso (CDbl(DroppedDates(SourceIndex)) > 10000) Then
    '								DateValues(ReturnIndex) = Date.FromOADate(CDbl(DroppedDates(SourceIndex)))
    '								ReturnValue(ReturnIndex) = ConvertValue(DroppedReturns(SourceIndex), GetType(Double))

    '								ReturnIndex += 1
    '							End If
    '						End If

    '						If (ReturnIndex >= DateValues.Length) Then
    '							Exit For
    '						End If
    '					Next

    '				End If
    '			End If

    '		ElseIf (IsInCols) Then
    '			ReturnIndex = 0

    '			For SourceIndex = 0 To (RowCount - 1)
    '				DroppedCols = DroppedRows(SourceIndex).Split(Chr(9))

    '				If (DroppedCols.Length = 2) AndAlso (ConvertIsNumeric(DroppedCols(1))) Then
    '					If IsDate(DroppedCols(0)) Then
    '						ReturnIndex += 1
    '					ElseIf IsNumeric(DroppedCols(0)) AndAlso (CDbl(DroppedCols(0)) > 10000) Then
    '						ReturnIndex += 1
    '					End If
    '				End If
    '			Next

    '			If (ReturnIndex > 0) Then
    '				ReDim DateValues(ReturnIndex - 1)
    '				ReDim ReturnValue(ReturnIndex - 1)

    '				ReturnIndex = 0

    '				For SourceIndex = 0 To (RowCount - 1)
    '					DroppedCols = DroppedRows(SourceIndex).Split(Chr(9))

    '					If (DroppedCols.Length = 2) AndAlso (ConvertIsNumeric(DroppedCols(1))) Then
    '						If IsDate(DroppedCols(0)) Then

    '							DateValues(ReturnIndex) = Date.Parse(DroppedCols(0))
    '							ReturnValue(ReturnIndex) = ConvertValue(DroppedCols(1), GetType(Double))

    '							ReturnIndex += 1
    '						ElseIf IsNumeric(DroppedCols(0)) AndAlso (CDbl(DroppedCols(0)) > 10000) Then
    '							DateValues(ReturnIndex) = Date.FromOADate(CDbl(DroppedCols(0)))
    '							ReturnValue(ReturnIndex) = ConvertValue(DroppedCols(1), GetType(Double))

    '							ReturnIndex += 1
    '						End If
    '					End If

    '					If (ReturnIndex >= DateValues.Length) Then
    '						Exit For
    '					End If
    '				Next

    '			End If
    '		End If
    '	End If

    '	' Merge in Data....
    '	' Ahhhh, at last....

    '	If (DateValues Is Nothing) OrElse (DateValues.Length <= 0) Then
    '		Exit Sub
    '	End If
    '	If (ReturnValue Is Nothing) OrElse (ReturnValue.Length <= 0) Then
    '		Exit Sub
    '	End If

    '	Dim ThisDate As Date
    '	Dim ThisReturn As Double
    '	Dim GridCounter As Integer
    '	Dim NewGridRow As DataGridViewRow
    '	Dim GridDateOrdinal As Integer = Grid_Greeks.Columns("GreekDate").Index
    '	Dim GridPriceOrdinal As Integer = Grid_Greeks.Columns("PriceLevel").Index
    '	Dim FoundIt As Boolean

    '	For ReturnIndex = 0 To (DateValues.Length - 1)
    '		ThisDate = DateValues(ReturnIndex)
    '		ThisReturn = ReturnValue(ReturnIndex)
    '		FoundIt = False

    '		For GridCounter = 0 To (Grid_Greeks.Rows.Count - 1)
    '			Try
    '				If (Grid_Greeks.Item(GridDateOrdinal, GridCounter).Value = ThisDate) Then

    '					Grid_Greeks.Item(GridPriceOrdinal, GridCounter).Value = ThisReturn
    '					Grid_Greeks.UpdateCellValue(GridPriceOrdinal, GridCounter)
    '					FoundIt = True
    '					Exit For
    '				End If
    '			Catch ex As Exception
    '			End Try
    '		Next

    '		If (Not FoundIt) Then
    '			NewGridRow = Grid_Greeks.Rows(Grid_Greeks.Rows.Add())

    '			NewGridRow.Cells("AuditID").Value = 0
    '			NewGridRow.Cells("GreekID").Value = 0
    '			NewGridRow.Cells("PriceInstrument").Value = thisAuditID
    '			NewGridRow.Cells("GreekDate").Value = ThisDate
    '			NewGridRow.Cells("PriceLevel").Value = ThisReturn
    '			NewGridRow.Cells("PricePercent").Value = 0
    '			NewGridRow.Cells("PriceMultiplier").Value = 1
    '			NewGridRow.Cells("PriceNAV").Value = ThisReturn
    '			NewGridRow.Cells("PriceBasicNAV").Value = 0
    '			NewGridRow.Cells("PriceGNAV").Value = 0
    '			NewGridRow.Cells("PriceGAV").Value = 0
    '			NewGridRow.Cells("PriceFinal").Value = 0
    '			NewGridRow.Cells("PriceIsPercent").Value = False
    '			NewGridRow.Cells("PriceIsAdministrator").Value = False
    '			NewGridRow.Cells("PriceIsMilestone").Value = 0
    '			NewGridRow.Cells("PriceComment").Value = ""
    '			NewGridRow.Cells("Modified").Value = True
    '		End If
    '	Next

    '	' Format Grid.

    '	If (Grid_Greeks.SortOrder = Windows.Forms.SortOrder.Descending) Then
    '		Grid_Greeks.Sort(Grid_Greeks.SortedColumn, System.ComponentModel.ListSortDirection.Descending)
    '	Else
    '		Grid_Greeks.Sort(Grid_Greeks.SortedColumn, System.ComponentModel.ListSortDirection.Ascending)
    '	End If
    '	FormatNumericGridColumn(Grid_Greeks.Columns("PricePercent").Index)

    'Catch ex As Exception
    'End Try
  End Sub

  ''' <summary>
  ''' Handles the KeyDown event of the Grid_Greeks control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Greeks_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Grid_Greeks.KeyDown
    ' ***********************************************************************************
    '
    '
    ' ***********************************************************************************

    Try
      If e.Control And (e.KeyCode = Keys.C) Then
        Dim HeaderString As String = "ID" & Chr(9) & "Date" & Chr(9) & "Return" & Chr(9) & "FundsManaged" & Chr(9) & "NAV" & Chr(9) & "Estimate" & Chr(13) & Chr(10)
        Dim GridRow As Integer

        For GridRow = 0 To (Grid_Greeks.Rows.Count - 1)
          Try
            If (GridRow <> Grid_Greeks.NewRowIndex) Then
              HeaderString &= Grid_Greeks.Item(Grid_Greeks.Columns("ID").Index, GridRow).Value.ToString & Chr(9) & CDate(Grid_Greeks.Item(Grid_Greeks.Columns("PerformanceDate").Index, GridRow).Value).ToString(DISPLAYMEMBER_DATEFORMAT) & Chr(9) & Grid_Greeks.Item(Grid_Greeks.Columns("PerformanceReturn").Index, GridRow).Value.ToString & Chr(9) & Grid_Greeks.Item(Grid_Greeks.Columns("FundsManaged").Index, GridRow).Value.ToString() & Chr(9) & Grid_Greeks.Item(Grid_Greeks.Columns("NAV").Index, GridRow).Value.ToString & Chr(9) & Grid_Greeks.Item(Grid_Greeks.Columns("Estimate").Index, GridRow).Value.ToString & Chr(13) & Chr(10)
            End If
          Catch ex As Exception
          End Try
        Next

        Clipboard.Clear()
        Clipboard.SetData(System.Windows.Forms.DataFormats.Text, (HeaderString))

        e.Handled = True
      ElseIf e.Control And (e.KeyCode = Keys.V) Then
        Dim ClipboardData As String

        ClipboardData = Clipboard.GetData(System.Windows.Forms.DataFormats.Text)

        Grid_Greeks_ParseString(ClipboardData)

        e.Handled = True
      End If
    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Formats the numeric grid column.
  ''' </summary>
  ''' <param name="pColumnID">The p column ID.</param>
  Private Sub FormatNumericGridColumn(ByVal pColumnID As Integer)
    ' ****************************************************************************************
    '
    '
    ' ****************************************************************************************
    Dim RowCount As Integer

    Try
      For RowCount = 0 To (Grid_Greeks.Rows.Count - 1)
        If (Not Grid_Greeks.Rows(RowCount).IsNewRow) Then
          If IsNumeric(Grid_Greeks.Item(pColumnID, RowCount).Value) Then
            If CDbl(Grid_Greeks.Item(pColumnID, RowCount).Value) < 0 Then
              Grid_Greeks.Item(pColumnID, RowCount).Style.ForeColor = Color.Red
            Else
              If Grid_Greeks.Item(pColumnID, RowCount).Style.ForeColor <> Grid_Greeks.Columns(pColumnID).DefaultCellStyle.ForeColor Then
                Grid_Greeks.Item(pColumnID, RowCount).Style.ForeColor = Grid_Greeks.Columns(pColumnID).DefaultCellStyle.ForeColor
              End If
            End If

          End If
        End If
      Next
    Catch ex As Exception
    End Try

  End Sub

#End Region

#Region " Bug hunting "

  ' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
  ' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
  ' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

  ''' <summary>
  ''' Called when [row updating].
  ''' </summary>
  ''' <param name="Sender">The sender.</param>
  ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  ''' <summary>
  ''' Called when [row updated].
  ''' </summary>
  ''' <param name="Sender">The sender.</param>
  ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  ''' <summary>
  ''' Called when [row fill error].
  ''' </summary>
  ''' <param name="Sender">The sender.</param>
  ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
    Dim a As Integer

    a = 1
  End Sub

#End Region

  ''' <summary>
  ''' Sets the select combo.
  ''' </summary>
  Private Sub SetSelectCombo()

    Try

      ' Get Delta Instrument Types

      Dim DSTypes As RenaissanceDataClass.DSInstrumentType
      Dim tblTypes As RenaissanceDataClass.DSInstrumentType.tblInstrumentTypeDataTable
      Dim DeltaRows() As RenaissanceDataClass.DSInstrumentType.tblInstrumentTypeRow
      Dim thisRow As RenaissanceDataClass.DSInstrumentType.tblInstrumentTypeRow
      Dim SelectString As String = ""

      DSTypes = MainForm.Load_Table(RenaissanceStandardDatasets.tblInstrumentType, False)
      tblTypes = DSTypes.tblInstrumentType
      DeltaRows = tblTypes.Select("InstrumentTypeHasDelta<>0")

      For Each thisRow In DeltaRows
        If (SelectString.Length > 0) Then
          SelectString &= "," & thisRow.InstrumentTypeID.ToString
        Else
          SelectString = thisRow.InstrumentTypeID.ToString
        End If
      Next

      Call MainForm.SetTblGenericCombo( _
      Combo_Select, _
      RenaissanceStandardDatasets.tblInstrument, _
      "InstrumentDescription", _
      "InstrumentID", _
      "InstrumentType IN (" & SelectString & ")")   ' 

    Catch ex As Exception

    End Try


  End Sub

  ''' <summary>
  ''' Loads the selected greeks data.
  ''' </summary>
  ''' <param name="pGreeksTable">The p greeks table.</param>
  Private Sub LoadSelectedGreeksData(ByRef pGreeksTable As RenaissanceDataClass.DSGreeks.tblGreeksDataTable)
    ' ************************************************************************************
    '
    '
    ' ************************************************************************************

    Try

      If (pGreeksTable Is Nothing) Then
        pGreeksTable = New RenaissanceDataClass.DSGreeks.tblGreeksDataTable
      Else
        pGreeksTable.Clear()
      End If

      If (Radio_ByDate.Checked) Then

        If IsDate(Me.DateTime_SelectedDate.Value) Then
          byDateAdaptor.SelectCommand.Parameters("@GreekDate").Value = CDate(Me.DateTime_SelectedDate.Value).Date
        Else
          byDateAdaptor.SelectCommand.Parameters("@GreekDate").Value = (Now())
        End If

        byDateAdaptor.SelectCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

        SyncLock byDateAdaptor.SelectCommand.Connection
          MainForm.LoadTable_Custom(pGreeksTable, byDateAdaptor.SelectCommand)
          'pGreeksTable.Load(byDateAdaptor.SelectCommand.ExecuteReader)
        End SyncLock

      ElseIf (Radio_ByInstrument.Checked) Then

        If IsNumeric(Combo_Select.SelectedValue) AndAlso (CInt(Combo_Select.SelectedValue) > 0) Then
          byInstrumentAdaptor.SelectCommand.Parameters("@InstrumentID").Value = CInt(Combo_Select.SelectedValue)
        Else
          byInstrumentAdaptor.SelectCommand.Parameters("@InstrumentID").Value = (-1)
        End If

        byInstrumentAdaptor.SelectCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

        SyncLock byInstrumentAdaptor.SelectCommand.Connection
          MainForm.LoadTable_Custom(pGreeksTable, byInstrumentAdaptor.SelectCommand)
          'pGreeksTable.Load(byInstrumentAdaptor.SelectCommand.ExecuteReader)
        End SyncLock

      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error loading Greeks Data (LoadSelectedGreeksData)", ex.Message, ex.StackTrace, True)
      pGreeksTable.Clear()
    End Try

  End Sub


End Class
