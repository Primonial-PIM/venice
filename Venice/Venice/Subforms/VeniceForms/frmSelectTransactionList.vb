' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmSelectTransactionList.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals

Imports RenaissanceDataClass
Imports C1.Win.C1FlexGrid

''' <summary>
''' Class frmSelectTransactionList
''' </summary>
Public Class frmSelectTransactionList

	Inherits System.Windows.Forms.Form
	Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmSelectTransactionList"/> class from being created.
    ''' </summary>
	Private Sub New()
		MyBase.New()

		'This call is required by the Windows Form Designer.
		InitializeComponent()

		'Add any initialization after the InitializeComponent() call

	End Sub

	'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
    ''' <summary>
    ''' The combo_ transaction fund
    ''' </summary>
	Friend WithEvents Combo_TransactionFund As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label_ cpty is fund
    ''' </summary>
	Friend WithEvents label_CptyIsFund As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ transaction instrument
    ''' </summary>
	Friend WithEvents Combo_TransactionInstrument As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label2
    ''' </summary>
	Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label1
    ''' </summary>
	Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label3
    ''' </summary>
	Friend WithEvents Label3 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ ticket
    ''' </summary>
	Friend WithEvents Combo_Ticket As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The date_ value date
    ''' </summary>
	Friend WithEvents Date_ValueDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The combo_ ticket operator
    ''' </summary>
	Friend WithEvents Combo_TicketOperator As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ value date operator
    ''' </summary>
	Friend WithEvents Combo_ValueDateOperator As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ field select1
    ''' </summary>
	Friend WithEvents Combo_FieldSelect1 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select1_ operator
    ''' </summary>
	Friend WithEvents Combo_Select1_Operator As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select1_ value
    ''' </summary>
	Friend WithEvents Combo_Select1_Value As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select2_ value
    ''' </summary>
	Friend WithEvents Combo_Select2_Value As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ field select2
    ''' </summary>
	Friend WithEvents Combo_FieldSelect2 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select2_ operator
    ''' </summary>
	Friend WithEvents Combo_Select2_Operator As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select3_ value
    ''' </summary>
	Friend WithEvents Combo_Select3_Value As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ field select3
    ''' </summary>
	Friend WithEvents Combo_FieldSelect3 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select3_ operator
    ''' </summary>
	Friend WithEvents Combo_Select3_Operator As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ and or_1
    ''' </summary>
	Friend WithEvents Combo_AndOr_1 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ and or_2
    ''' </summary>
	Friend WithEvents Combo_AndOr_2 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The check_ incomplete transactions
    ''' </summary>
	Friend WithEvents Check_IncompleteTransactions As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The grid_ transactions
    ''' </summary>
	Friend WithEvents Grid_Transactions As C1.Win.C1FlexGrid.C1FlexGrid
    ''' <summary>
    ''' The check_ aggregate to parent
    ''' </summary>
	Friend WithEvents Check_AggregateToParent As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The root menu
    ''' </summary>
	Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
    ''' <summary>
    ''' The form_ status strip
    ''' </summary>
	Friend WithEvents Form_StatusStrip As System.Windows.Forms.StatusStrip
    ''' <summary>
    ''' The form_ progress bar
    ''' </summary>
	Friend WithEvents Form_ProgressBar As System.Windows.Forms.ToolStripProgressBar
    ''' <summary>
    ''' The label_ status
    ''' </summary>
  Friend WithEvents Label_Status As System.Windows.Forms.ToolStripStatusLabel
  Friend WithEvents Check_ByISIN As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ omit zero balances
    ''' </summary>
	Friend WithEvents Check_OmitZeroBalances As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSelectTransactionList))
    Me.Combo_TransactionFund = New System.Windows.Forms.ComboBox
    Me.label_CptyIsFund = New System.Windows.Forms.Label
    Me.Combo_TransactionInstrument = New System.Windows.Forms.ComboBox
    Me.Label2 = New System.Windows.Forms.Label
    Me.Combo_Ticket = New System.Windows.Forms.ComboBox
    Me.Label1 = New System.Windows.Forms.Label
    Me.Label3 = New System.Windows.Forms.Label
    Me.Date_ValueDate = New System.Windows.Forms.DateTimePicker
    Me.Combo_TicketOperator = New System.Windows.Forms.ComboBox
    Me.Combo_ValueDateOperator = New System.Windows.Forms.ComboBox
    Me.Combo_Select1_Operator = New System.Windows.Forms.ComboBox
    Me.Combo_FieldSelect1 = New System.Windows.Forms.ComboBox
    Me.Combo_Select1_Value = New System.Windows.Forms.ComboBox
    Me.Combo_Select2_Value = New System.Windows.Forms.ComboBox
    Me.Combo_FieldSelect2 = New System.Windows.Forms.ComboBox
    Me.Combo_Select2_Operator = New System.Windows.Forms.ComboBox
    Me.Combo_Select3_Value = New System.Windows.Forms.ComboBox
    Me.Combo_FieldSelect3 = New System.Windows.Forms.ComboBox
    Me.Combo_Select3_Operator = New System.Windows.Forms.ComboBox
    Me.Combo_AndOr_1 = New System.Windows.Forms.ComboBox
    Me.Combo_AndOr_2 = New System.Windows.Forms.ComboBox
    Me.Check_IncompleteTransactions = New System.Windows.Forms.CheckBox
    Me.Grid_Transactions = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.Check_AggregateToParent = New System.Windows.Forms.CheckBox
    Me.Check_OmitZeroBalances = New System.Windows.Forms.CheckBox
    Me.RootMenu = New System.Windows.Forms.MenuStrip
    Me.Form_StatusStrip = New System.Windows.Forms.StatusStrip
    Me.Form_ProgressBar = New System.Windows.Forms.ToolStripProgressBar
    Me.Label_Status = New System.Windows.Forms.ToolStripStatusLabel
    Me.Check_ByISIN = New System.Windows.Forms.CheckBox
    CType(Me.Grid_Transactions, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Form_StatusStrip.SuspendLayout()
    Me.SuspendLayout()
    '
    'Combo_TransactionFund
    '
    Me.Combo_TransactionFund.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_TransactionFund.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_TransactionFund.Location = New System.Drawing.Point(128, 32)
    Me.Combo_TransactionFund.Name = "Combo_TransactionFund"
    Me.Combo_TransactionFund.Size = New System.Drawing.Size(808, 21)
    Me.Combo_TransactionFund.TabIndex = 0
    '
    'label_CptyIsFund
    '
    Me.label_CptyIsFund.Location = New System.Drawing.Point(16, 36)
    Me.label_CptyIsFund.Name = "label_CptyIsFund"
    Me.label_CptyIsFund.Size = New System.Drawing.Size(104, 16)
    Me.label_CptyIsFund.TabIndex = 82
    Me.label_CptyIsFund.Text = "Fund"
    '
    'Combo_TransactionInstrument
    '
    Me.Combo_TransactionInstrument.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_TransactionInstrument.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_TransactionInstrument.Location = New System.Drawing.Point(128, 60)
    Me.Combo_TransactionInstrument.Name = "Combo_TransactionInstrument"
    Me.Combo_TransactionInstrument.Size = New System.Drawing.Size(756, 21)
    Me.Combo_TransactionInstrument.TabIndex = 1
    '
    'Label2
    '
    Me.Label2.Location = New System.Drawing.Point(16, 64)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(104, 16)
    Me.Label2.TabIndex = 84
    Me.Label2.Text = "Instrument"
    '
    'Combo_Ticket
    '
    Me.Combo_Ticket.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Ticket.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Ticket.Location = New System.Drawing.Point(760, 88)
    Me.Combo_Ticket.Name = "Combo_Ticket"
    Me.Combo_Ticket.Size = New System.Drawing.Size(107, 21)
    Me.Combo_Ticket.TabIndex = 3
    Me.Combo_Ticket.Visible = False
    '
    'Label1
    '
    Me.Label1.Location = New System.Drawing.Point(548, 92)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(104, 16)
    Me.Label1.TabIndex = 86
    Me.Label1.Text = "Ticket"
    Me.Label1.Visible = False
    '
    'Label3
    '
    Me.Label3.Location = New System.Drawing.Point(16, 92)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(104, 16)
    Me.Label3.TabIndex = 88
    Me.Label3.Text = "ValueDate"
    '
    'Date_ValueDate
    '
    Me.Date_ValueDate.Location = New System.Drawing.Point(228, 88)
    Me.Date_ValueDate.Name = "Date_ValueDate"
    Me.Date_ValueDate.Size = New System.Drawing.Size(174, 20)
    Me.Date_ValueDate.TabIndex = 5
    '
    'Combo_TicketOperator
    '
    Me.Combo_TicketOperator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_TicketOperator.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_TicketOperator.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like"})
    Me.Combo_TicketOperator.Location = New System.Drawing.Point(660, 88)
    Me.Combo_TicketOperator.Name = "Combo_TicketOperator"
    Me.Combo_TicketOperator.Size = New System.Drawing.Size(96, 21)
    Me.Combo_TicketOperator.TabIndex = 2
    Me.Combo_TicketOperator.Visible = False
    '
    'Combo_ValueDateOperator
    '
    Me.Combo_ValueDateOperator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_ValueDateOperator.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_ValueDateOperator.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", ""})
    Me.Combo_ValueDateOperator.Location = New System.Drawing.Point(128, 88)
    Me.Combo_ValueDateOperator.Name = "Combo_ValueDateOperator"
    Me.Combo_ValueDateOperator.Size = New System.Drawing.Size(96, 21)
    Me.Combo_ValueDateOperator.TabIndex = 4
    '
    'Combo_Select1_Operator
    '
    Me.Combo_Select1_Operator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_Select1_Operator.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Select1_Operator.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like"})
    Me.Combo_Select1_Operator.Location = New System.Drawing.Point(272, 116)
    Me.Combo_Select1_Operator.Name = "Combo_Select1_Operator"
    Me.Combo_Select1_Operator.Size = New System.Drawing.Size(96, 21)
    Me.Combo_Select1_Operator.TabIndex = 7
    '
    'Combo_FieldSelect1
    '
    Me.Combo_FieldSelect1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_FieldSelect1.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_FieldSelect1.Location = New System.Drawing.Point(128, 116)
    Me.Combo_FieldSelect1.Name = "Combo_FieldSelect1"
    Me.Combo_FieldSelect1.Size = New System.Drawing.Size(136, 21)
    Me.Combo_FieldSelect1.Sorted = True
    Me.Combo_FieldSelect1.TabIndex = 6
    '
    'Combo_Select1_Value
    '
    Me.Combo_Select1_Value.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Select1_Value.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Select1_Value.Location = New System.Drawing.Point(376, 116)
    Me.Combo_Select1_Value.Name = "Combo_Select1_Value"
    Me.Combo_Select1_Value.Size = New System.Drawing.Size(272, 21)
    Me.Combo_Select1_Value.TabIndex = 8
    '
    'Combo_Select2_Value
    '
    Me.Combo_Select2_Value.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Select2_Value.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Select2_Value.Location = New System.Drawing.Point(376, 148)
    Me.Combo_Select2_Value.Name = "Combo_Select2_Value"
    Me.Combo_Select2_Value.Size = New System.Drawing.Size(272, 21)
    Me.Combo_Select2_Value.TabIndex = 12
    '
    'Combo_FieldSelect2
    '
    Me.Combo_FieldSelect2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_FieldSelect2.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_FieldSelect2.Location = New System.Drawing.Point(128, 148)
    Me.Combo_FieldSelect2.Name = "Combo_FieldSelect2"
    Me.Combo_FieldSelect2.Size = New System.Drawing.Size(136, 21)
    Me.Combo_FieldSelect2.Sorted = True
    Me.Combo_FieldSelect2.TabIndex = 10
    '
    'Combo_Select2_Operator
    '
    Me.Combo_Select2_Operator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_Select2_Operator.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Select2_Operator.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like"})
    Me.Combo_Select2_Operator.Location = New System.Drawing.Point(272, 148)
    Me.Combo_Select2_Operator.Name = "Combo_Select2_Operator"
    Me.Combo_Select2_Operator.Size = New System.Drawing.Size(96, 21)
    Me.Combo_Select2_Operator.TabIndex = 11
    '
    'Combo_Select3_Value
    '
    Me.Combo_Select3_Value.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Select3_Value.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Select3_Value.Location = New System.Drawing.Point(376, 180)
    Me.Combo_Select3_Value.Name = "Combo_Select3_Value"
    Me.Combo_Select3_Value.Size = New System.Drawing.Size(272, 21)
    Me.Combo_Select3_Value.TabIndex = 16
    '
    'Combo_FieldSelect3
    '
    Me.Combo_FieldSelect3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_FieldSelect3.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_FieldSelect3.Location = New System.Drawing.Point(128, 180)
    Me.Combo_FieldSelect3.Name = "Combo_FieldSelect3"
    Me.Combo_FieldSelect3.Size = New System.Drawing.Size(136, 21)
    Me.Combo_FieldSelect3.Sorted = True
    Me.Combo_FieldSelect3.TabIndex = 14
    '
    'Combo_Select3_Operator
    '
    Me.Combo_Select3_Operator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_Select3_Operator.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Select3_Operator.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like"})
    Me.Combo_Select3_Operator.Location = New System.Drawing.Point(272, 180)
    Me.Combo_Select3_Operator.Name = "Combo_Select3_Operator"
    Me.Combo_Select3_Operator.Size = New System.Drawing.Size(96, 21)
    Me.Combo_Select3_Operator.TabIndex = 15
    '
    'Combo_AndOr_1
    '
    Me.Combo_AndOr_1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_AndOr_1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_AndOr_1.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_AndOr_1.Items.AddRange(New Object() {"AND", "OR"})
    Me.Combo_AndOr_1.Location = New System.Drawing.Point(660, 116)
    Me.Combo_AndOr_1.Name = "Combo_AndOr_1"
    Me.Combo_AndOr_1.Size = New System.Drawing.Size(96, 21)
    Me.Combo_AndOr_1.TabIndex = 9
    '
    'Combo_AndOr_2
    '
    Me.Combo_AndOr_2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_AndOr_2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_AndOr_2.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_AndOr_2.Items.AddRange(New Object() {"AND", "OR"})
    Me.Combo_AndOr_2.Location = New System.Drawing.Point(660, 148)
    Me.Combo_AndOr_2.Name = "Combo_AndOr_2"
    Me.Combo_AndOr_2.Size = New System.Drawing.Size(96, 21)
    Me.Combo_AndOr_2.TabIndex = 13
    '
    'Check_IncompleteTransactions
    '
    Me.Check_IncompleteTransactions.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_IncompleteTransactions.Location = New System.Drawing.Point(128, 209)
    Me.Check_IncompleteTransactions.Name = "Check_IncompleteTransactions"
    Me.Check_IncompleteTransactions.Size = New System.Drawing.Size(236, 16)
    Me.Check_IncompleteTransactions.TabIndex = 17
    Me.Check_IncompleteTransactions.Text = "Select Only Incomplete Transactions"
    '
    'Grid_Transactions
    '
    Me.Grid_Transactions.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_Transactions.ColumnInfo = resources.GetString("Grid_Transactions.ColumnInfo")
    Me.Grid_Transactions.Cursor = System.Windows.Forms.Cursors.Default
    Me.Grid_Transactions.Location = New System.Drawing.Point(4, 237)
    Me.Grid_Transactions.Name = "Grid_Transactions"
    Me.Grid_Transactions.Rows.DefaultSize = 17
    Me.Grid_Transactions.Size = New System.Drawing.Size(936, 466)
    Me.Grid_Transactions.TabIndex = 104
    Me.Grid_Transactions.Tree.Column = 0
    '
    'Check_AggregateToParent
    '
    Me.Check_AggregateToParent.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_AggregateToParent.Location = New System.Drawing.Point(376, 209)
    Me.Check_AggregateToParent.Name = "Check_AggregateToParent"
    Me.Check_AggregateToParent.Size = New System.Drawing.Size(236, 16)
    Me.Check_AggregateToParent.TabIndex = 105
    Me.Check_AggregateToParent.Text = "Aggregate To Parent Instrument"
    '
    'Check_OmitZeroBalances
    '
    Me.Check_OmitZeroBalances.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_OmitZeroBalances.Location = New System.Drawing.Point(618, 209)
    Me.Check_OmitZeroBalances.Name = "Check_OmitZeroBalances"
    Me.Check_OmitZeroBalances.Size = New System.Drawing.Size(284, 16)
    Me.Check_OmitZeroBalances.TabIndex = 106
    Me.Check_OmitZeroBalances.Text = "Omit Instruments with Zero Aggregate Balances"
    '
    'RootMenu
    '
    Me.RootMenu.Location = New System.Drawing.Point(0, 0)
    Me.RootMenu.Name = "RootMenu"
    Me.RootMenu.Size = New System.Drawing.Size(944, 24)
    Me.RootMenu.TabIndex = 107
    Me.RootMenu.Text = " "
    '
    'Form_StatusStrip
    '
    Me.Form_StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Form_ProgressBar, Me.Label_Status})
    Me.Form_StatusStrip.Location = New System.Drawing.Point(0, 704)
    Me.Form_StatusStrip.Name = "Form_StatusStrip"
    Me.Form_StatusStrip.Size = New System.Drawing.Size(944, 22)
    Me.Form_StatusStrip.TabIndex = 108
    Me.Form_StatusStrip.Text = " "
    '
    'Form_ProgressBar
    '
    Me.Form_ProgressBar.Maximum = 20
    Me.Form_ProgressBar.Name = "Form_ProgressBar"
    Me.Form_ProgressBar.Size = New System.Drawing.Size(150, 17)
    Me.Form_ProgressBar.Step = 1
    Me.Form_ProgressBar.Visible = False
    '
    'Label_Status
    '
    Me.Label_Status.Name = "Label_Status"
    Me.Label_Status.Size = New System.Drawing.Size(10, 17)
    Me.Label_Status.Text = " "
    '
    'Check_ByISIN
    '
    Me.Check_ByISIN.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Check_ByISIN.BackColor = System.Drawing.SystemColors.Control
    Me.Check_ByISIN.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_ByISIN.Location = New System.Drawing.Point(890, 62)
    Me.Check_ByISIN.Name = "Check_ByISIN"
    Me.Check_ByISIN.Size = New System.Drawing.Size(46, 19)
    Me.Check_ByISIN.TabIndex = 154
    Me.Check_ByISIN.Text = "ISIN"
    Me.Check_ByISIN.UseVisualStyleBackColor = False
    '
    'frmSelectTransactionList
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(944, 726)
    Me.Controls.Add(Me.Check_ByISIN)
    Me.Controls.Add(Me.Form_StatusStrip)
    Me.Controls.Add(Me.RootMenu)
    Me.Controls.Add(Me.Check_OmitZeroBalances)
    Me.Controls.Add(Me.Check_AggregateToParent)
    Me.Controls.Add(Me.Grid_Transactions)
    Me.Controls.Add(Me.Check_IncompleteTransactions)
    Me.Controls.Add(Me.Combo_AndOr_2)
    Me.Controls.Add(Me.Combo_AndOr_1)
    Me.Controls.Add(Me.Combo_Select3_Value)
    Me.Controls.Add(Me.Combo_FieldSelect3)
    Me.Controls.Add(Me.Combo_Select3_Operator)
    Me.Controls.Add(Me.Combo_Select2_Value)
    Me.Controls.Add(Me.Combo_FieldSelect2)
    Me.Controls.Add(Me.Combo_Select2_Operator)
    Me.Controls.Add(Me.Combo_Select1_Value)
    Me.Controls.Add(Me.Combo_FieldSelect1)
    Me.Controls.Add(Me.Combo_Select1_Operator)
    Me.Controls.Add(Me.Combo_ValueDateOperator)
    Me.Controls.Add(Me.Combo_TicketOperator)
    Me.Controls.Add(Me.Date_ValueDate)
    Me.Controls.Add(Me.Label3)
    Me.Controls.Add(Me.Combo_Ticket)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.Combo_TransactionInstrument)
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.Combo_TransactionFund)
    Me.Controls.Add(Me.label_CptyIsFund)
    Me.Name = "frmSelectTransactionList"
    Me.Text = "Select Transaction (List)"
    CType(Me.Grid_Transactions, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Form_StatusStrip.ResumeLayout(False)
    Me.Form_StatusStrip.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "

	' Form 'Parent', the Main Venice form.
	' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
	Private WithEvents _MainForm As VeniceMain

	' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
	Private FormTooltip As New ToolTip()

	' Form Menu


	' Form Constants, specific to the table being updated.

    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
	Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

	' Form Locals, initialised on 'New' defining what standard data items to use
    ''' <summary>
  ''' Variable holding the form's data table name. taken from the "ThisStandardDataset" object.
    ''' </summary>
	Private THIS_TABLENAME As String
    ''' <summary>
  ''' Variable holding the form's data adaptor name. taken from the "ThisStandardDataset" object.
    ''' </summary>
	Private THIS_ADAPTORNAME As String
    ''' <summary>
  ''' Variable holding the form's dataset name. taken from the "ThisStandardDataset" object.
    ''' </summary>
	Private THIS_DATASETNAME As String


	' The standard ChangeID for this form. e.g. tblFund
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
	Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

	' Form Specific Order fields
    ''' <summary>
  ''' Field Name to order the Select combo by.
    ''' </summary>
	Private THIS_FORM_OrderBy As String

	' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
	Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
	Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

	' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
	Private THIS_FORM_FormID As VeniceFormID

	' Data Structures

    ''' <summary>
    ''' My dataset
    ''' </summary>
	Private myDataset As DataSet
    ''' <summary>
    ''' My table
    ''' </summary>
	Private myTable As DataTable
    ''' <summary>
    ''' My connection
    ''' </summary>
	Private myConnection As SqlConnection
    ''' <summary>
    ''' My adaptor
    ''' </summary>
	Private myAdaptor As SqlDataAdapter
    ''' <summary>
    ''' My data view
    ''' </summary>
	Private myDataView As DataView

    ''' <summary>
    ''' The this standard dataset
    ''' </summary>
	Private ThisStandardDataset As RenaissanceGlobals.StandardDataset

    ''' <summary>
    ''' The instruments DS
    ''' </summary>
	Private InstrumentsDS As RenaissanceDataClass.DSInstrument
    ''' <summary>
    ''' The best prices table
    ''' </summary>
	Private BestPricesTable As DataTable
    ''' <summary>
    ''' The sorted prices
    ''' </summary>
	Private SortedPrices As DataRow()

	' Active Element.

    ''' <summary>
    ''' The __ is over cancel button
    ''' </summary>
	Private __IsOverCancelButton As Boolean
    ''' <summary>
    ''' The _ in use
    ''' </summary>
	Private _InUse As Boolean

	' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
	Private FormIsValid As Boolean
    ''' <summary>
    ''' The in paint
    ''' </summary>
	Private InPaint As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
	Private _FormOpenFailed As Boolean

	' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
	Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
	Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
	Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
	Private HasDeletePermission As Boolean

    ''' <summary>
    ''' Class GridColumns
    ''' </summary>
	Private Class GridColumns
        ''' <summary>
        ''' The first col
        ''' </summary>
		Public Shared FirstCol As Integer = 0
        ''' <summary>
        ''' The value
        ''' </summary>
		Public Shared Value As Integer = 1
        ''' <summary>
        ''' The last price
        ''' </summary>
		Public Shared LastPrice As Integer = 2
        ''' <summary>
        ''' The units
        ''' </summary>
		Public Shared Units As Integer = 3
        ''' <summary>
        ''' The type
        ''' </summary>
		Public Shared Type As Integer = 4
        ''' <summary>
        ''' The value date
        ''' </summary>
		Public Shared ValueDate As Integer = 5
        ''' <summary>
        ''' The price
        ''' </summary>
		Public Shared Price As Integer = 6
        ''' <summary>
        ''' The settlement
        ''' </summary>
		Public Shared Settlement As Integer = 7
        ''' <summary>
        ''' The parent ID
        ''' </summary>
		Public Shared ParentID As Integer = 8
        ''' <summary>
        ''' The fund ID
        ''' </summary>
		Public Shared FundID As Integer = 9
        ''' <summary>
        ''' The instrument ID
        ''' </summary>
		Public Shared InstrumentID As Integer = 10
        ''' <summary>
        ''' The counter
        ''' </summary>
		Public Shared Counter As Integer = 11
	End Class

    ''' <summary>
    ''' Init_s the grid columns class.
    ''' </summary>
	Private Sub Init_GridColumnsClass()
		GridColumns.FirstCol = Me.Grid_Transactions.Cols("FirstCol").SafeIndex
		GridColumns.Value = Me.Grid_Transactions.Cols("Value").SafeIndex
		GridColumns.LastPrice = Me.Grid_Transactions.Cols("LastPrice").SafeIndex
		GridColumns.Units = Me.Grid_Transactions.Cols("Units").SafeIndex
		GridColumns.Type = Me.Grid_Transactions.Cols("Type").SafeIndex
		GridColumns.ValueDate = Me.Grid_Transactions.Cols("ValueDate").SafeIndex
		GridColumns.Price = Me.Grid_Transactions.Cols("Price").SafeIndex
		GridColumns.Settlement = Me.Grid_Transactions.Cols("Settlement").SafeIndex
		GridColumns.ParentID = Me.Grid_Transactions.Cols("ParentID").SafeIndex
		GridColumns.FundID = Me.Grid_Transactions.Cols("FundID").SafeIndex
		GridColumns.InstrumentID = Me.Grid_Transactions.Cols("InstrumentID").SafeIndex
		GridColumns.Counter = Me.Grid_Transactions.Cols("Counter").SafeIndex

	End Sub
#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
	Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
	Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return __IsOverCancelButton
		End Get
		Set(ByVal Value As Boolean)
			__IsOverCancelButton = Value
		End Set
	End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
	Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
		Get
			Return InPaint
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
		Get
			Return _InUse
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmSelectTransactionList"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
	Public Sub New(ByVal pMainForm As VeniceMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()


		_MainForm = pMainForm
		AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

		_FormOpenFailed = False
		_InUse = True

		' Default Select and Order fields.

		THIS_FORM_OrderBy = "TransactionFund, TransactionInstrument"

		' Form Permissioning :-

		THIS_FORM_PermissionArea = "frmSelectTransaction"
		THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

		' 'This' form ID

		THIS_FORM_FormID = VeniceFormID.frmSelectTransactionList

		' This form's dataset type.

		ThisStandardDataset = RenaissanceStandardDatasets.tblSelectTransaction ' This Defines the Form Data !!! 


		' Data object names standard to this Form type.

		THIS_TABLENAME = ThisStandardDataset.TableName
		THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
		THIS_DATASETNAME = ThisStandardDataset.DatasetName

		THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

		' Establish / Retrieve data objects for this form.

		myConnection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)
		myAdaptor = MainForm.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, VENICE_CONNECTION, THIS_TABLENAME)
		myDataset = MainForm.Load_Table(ThisStandardDataset, False)
		myTable = myDataset.Tables(0)

		' Report
		SetTransactionReportMenu(RootMenu)

		' Grid :- Grid_Transactions

		Grid_Transactions.Rows.Count = 1
		Grid_Transactions.Cols.Fixed = 0
		'    Grid_Transactions.ExtendLastCol = True

		'styles
		Dim cs As CellStyle = Grid_Transactions.Styles.Normal
		cs.Border.Direction = BorderDirEnum.Vertical
		cs.WordWrap = False

		cs = Grid_Transactions.Styles.Add("Data")
		cs.BackColor = SystemColors.Info
		cs.ForeColor = Color.Black

		cs = Grid_Transactions.Styles.Add("DataNegative")
		cs.BackColor = SystemColors.Info
		cs.ForeColor = Color.Red

		cs = Grid_Transactions.Styles.Add("SourceNode")
		cs.BackColor = Color.Yellow
		cs.Font = New Font(Grid_Transactions.Font, FontStyle.Bold)

		cs = Grid_Transactions.Styles.Add("FundPositionPositive")
		cs.ForeColor = Color.Black
		cs.Font = New Font(Grid_Transactions.Font, FontStyle.Bold)

		cs = Grid_Transactions.Styles.Add("FundPositionNegative")
		cs.ForeColor = Color.Red
		cs.Font = New Font(Grid_Transactions.Font, FontStyle.Bold)

		'outline tree
		Grid_Transactions.Tree.Column = 0
    Grid_Transactions.AllowMerging = AllowMergingEnum.Nodes

		'other
		Grid_Transactions.AllowResizing = AllowResizingEnum.Columns
		Grid_Transactions.SelectionMode = SelectionModeEnum.Cell

		Call Init_GridColumnsClass()

		' Establish initial DataView and Initialise Transactions Grig.

		Try
			myDataView = New DataView(myTable, "True", "", DataViewRowState.CurrentRows)
			myDataView.Sort = "FundName, InstrumentDescription, TransactionValueDate, RN"
		Catch ex As Exception

		End Try

		' Initialise Field select area

		Call BuildFieldSelectCombos()
		Me.Combo_AndOr_1.SelectedIndex = 0
		Me.Combo_AndOr_2.SelectedIndex = 0

		' Form Control Changed events
		AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

		AddHandler Combo_TransactionFund.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_TransactionInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_TicketOperator.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_Ticket.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_Ticket.SelectedIndexChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_ValueDateOperator.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Date_ValueDate.ValueChanged, AddressOf Me.FormControlChanged

		AddHandler Combo_Select1_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_Select1_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_Select1_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp
		AddHandler Combo_Select2_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_Select2_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_Select2_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp
		AddHandler Combo_Select3_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_Select3_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_Select3_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp

		AddHandler Combo_AndOr_1.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_AndOr_2.SelectedValueChanged, AddressOf Me.FormControlChanged

		AddHandler Check_IncompleteTransactions.CheckedChanged, AddressOf Me.FormControlChanged

		AddHandler Combo_TransactionFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_TransactionInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_Ticket.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_TicketOperator.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_ValueDateOperator.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		AddHandler Combo_TransactionFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_TransactionInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_Ticket.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_TicketOperator.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_ValueDateOperator.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

		AddHandler Combo_TransactionFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_TransactionInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_Ticket.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_TicketOperator.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_ValueDateOperator.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

		AddHandler Combo_TransactionFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_TransactionInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_Ticket.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_TicketOperator.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_ValueDateOperator.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

		InstrumentsDS = Nothing
		BestPricesTable = Nothing

	End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

	' Form Initialisation code.
	'
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
	Public Sub ResetForm() Implements StandardVeniceForm.ResetForm
		THIS_FORM_OrderBy = "TransactionFund, TransactionInstrument"

		Call Form_Load(Me, New System.EventArgs)
	End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
	Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
		_FormOpenFailed = False
		_InUse = True

		' Initialise Data structures. Connection, Adaptor and Dataset.

		If Not (MainForm Is Nothing) Then
			FormIsValid = True
		Else
			MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myConnection Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myAdaptor Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myDataset Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If


		' Check User permissions
		Try
			Call CheckPermissions()
			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

				FormIsValid = False
				_FormOpenFailed = True

				Exit Sub
			End If
		Catch ex As Exception
			FormIsValid = False
			_FormOpenFailed = True

			Exit Sub
		End Try

		' Initialse form

		InPaint = True
		IsOverCancelButton = False

		' Initialise main select controls
		' Build Sorted data list from which this form operates

		Try
			Call SetFundCombo()
      Call SetTicketCombo()

			Me.Combo_TransactionFund.SelectedIndex = -1

      Call SetInstrumentCombo()

      Me.Combo_TransactionInstrument.SelectedIndex = -1
			Me.Combo_TicketOperator.SelectedIndex = -1
			Me.Combo_Ticket.SelectedIndex = -1
			Me.Combo_ValueDateOperator.SelectedIndex = 0
			Me.Date_ValueDate.Value = Now.Date()

			Me.Check_IncompleteTransactions.Checked = False

			Me.Combo_FieldSelect1.SelectedIndex = 0
			Me.Combo_FieldSelect2.SelectedIndex = 0
			Me.Combo_FieldSelect3.SelectedIndex = 0

			Call SetSortedRows()

			Call MainForm.SetComboSelectionLengths(Me)
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error initialising Select Transaction Form.", ex.StackTrace, True)
		End Try

		InPaint = False

	End Sub

    ''' <summary>
    ''' Handles the Closing event of the frm control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
	Private Sub frm_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		Dim HideForm As Boolean

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...

		_InUse = False

		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else
			HideForm = True
			If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			e.Cancel = True
		Else
			Try
				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

				RemoveHandler Combo_TransactionFund.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_TransactionInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_TicketOperator.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Ticket.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Ticket.SelectedIndexChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_ValueDateOperator.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Date_ValueDate.ValueChanged, AddressOf Me.FormControlChanged

				RemoveHandler Combo_Select1_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Select1_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Select1_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp
				RemoveHandler Combo_Select2_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Select2_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Select2_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp
				RemoveHandler Combo_Select3_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Select3_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Select3_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp

				RemoveHandler Combo_AndOr_1.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_AndOr_2.SelectedValueChanged, AddressOf Me.FormControlChanged

				RemoveHandler Check_IncompleteTransactions.CheckedChanged, AddressOf Me.FormControlChanged

				RemoveHandler Combo_TransactionFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_TransactionInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_Ticket.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_TicketOperator.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_ValueDateOperator.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				RemoveHandler Combo_TransactionFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_TransactionInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_Ticket.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_TicketOperator.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_ValueDateOperator.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

				RemoveHandler Combo_TransactionFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_TransactionInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_Ticket.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_TicketOperator.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_ValueDateOperator.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

				RemoveHandler Combo_TransactionFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_TransactionInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_Ticket.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_TicketOperator.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_ValueDateOperator.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

			Catch ex As Exception
			End Try
		End If

	End Sub


#End Region

    ''' <summary>
    ''' Handles the CheckedChanged event of the Check_AggregateToParent control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Check_AggregateToParent_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_AggregateToParent.CheckedChanged
		If InPaint = False Then
			Call SetSortedRows(True)
		End If
	End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the Check_OmitZeroBalances control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Check_OmitZeroBalances_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_OmitZeroBalances.CheckedChanged
		If InPaint = False Then
			Call SetSortedRows(True)
		End If
	End Sub

	' Routine to handle changes / updates to tables by this and other windows.
	' If this, or any other, form posts a change to a table, then it will invoke an update event 
	' detailing what tables have been altered.
	' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
	' the 'VeniceAutoUpdate' event of the main Venice form.
	' Each form may them react as appropriate to changes in any table that might impact it.
	'
    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		Dim OrgInPaint As Boolean
		Dim KnowledgeDateChanged As Boolean
		Dim RefreshGrid As Boolean

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		OrgInPaint = InPaint

		Try
			InPaint = True
			KnowledgeDateChanged = False
			RefreshGrid = False

			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
				KnowledgeDateChanged = True
			End If
		Catch ex As Exception
		End Try

		' ****************************************************************
		' Check for changes relevant to this form
		' ****************************************************************

		' Changes to the tblPrice table :-
		' Clear 'Best Prices' cache.

		Try
			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblPrice) = True) Or KnowledgeDateChanged Then
				Call GetInstrumentPrice(-1)	' Flush Static Valiables

				If (BestPricesTable IsNot Nothing) Then
					BestPricesTable.Clear()
				End If
				BestPricesTable = Nothing

			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblPrice", ex.StackTrace, True)
		End Try

		' Changes to the tblFund table :-

		Try
			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFund) = True) Or KnowledgeDateChanged Then
				Call SetFundCombo()
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblFund", ex.StackTrace, True)
		End Try

		' Changes to the tblInstrument table :-
		Try
			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrument) = True) Or KnowledgeDateChanged Then
				Call SetInstrumentCombo()
				InstrumentsDS = Nothing
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblInstrument", ex.StackTrace, True)
		End Try

		' Changes to the tblTransaction table :-
		Try
			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblTransaction) = True) Or KnowledgeDateChanged Then
				Call SetTicketCombo()
				RefreshGrid = True
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblTransaction", ex.StackTrace, True)
		End Try

		' Changes to the KnowledgeDate :-
		Try
			If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
				RefreshGrid = True
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: KnowledgeDate", ex.StackTrace, True)
		End Try

		' Check TableUpdate against first Select Field
		Try
			If Me.Combo_FieldSelect1.SelectedIndex >= 0 Then
				Try

					' If the First FieldSelect combo has selected a Transaction Field which is related to
					' another table, as defined in tblReferentialIntegrity (e.g. TransactionCounterparty)
					' Then if that related table is updated, the FieldSelect-Values combo must be updated.

					' If ChangedID is ChangeID of related table then ,..

					If (e.TableChanged(GetFieldChangeID(Combo_FieldSelect1.SelectedItem)) = True) Or KnowledgeDateChanged Then
						Dim SelectedValue As Object
						Dim TextValue As String

						' Save current Value Combo Value.

						SelectedValue = Nothing
						TextValue = ""

						If Me.Combo_Select1_Value.SelectedIndex >= 0 Then
							SelectedValue = Me.Combo_Select1_Value.SelectedValue
						ElseIf (SelectedValue Is Nothing) Then
							TextValue = Me.Combo_Select1_Value.Text
						End If

						' Update FieldSelect-Values Combo.

						Call UpdateSelectedValueCombo(Me.Combo_FieldSelect1.SelectedItem, Me.Combo_Select1_Value)

						' Restore Saved Value.

						If (Not (SelectedValue Is Nothing)) Then
							Me.Combo_Select1_Value.SelectedValue = SelectedValue
						ElseIf TextValue.Length > 0 Then
							Me.Combo_Select1_Value.Text = TextValue
						End If
					End If
				Catch ex As Exception
				End Try
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: First SelectField", ex.StackTrace, True)
		End Try

		' Check TableUpdate against second Select Field
		Try
			If Me.Combo_FieldSelect2.SelectedIndex >= 0 Then
				Try
					' GetFieldChangeID
					If (e.TableChanged(GetFieldChangeID(Combo_FieldSelect2.SelectedItem)) = True) Or KnowledgeDateChanged Then
						Dim SelectedValue As Object
						Dim TextValue As String

						SelectedValue = Nothing
						TextValue = ""

						If Me.Combo_Select2_Value.SelectedIndex >= 0 Then
							SelectedValue = Me.Combo_Select2_Value.SelectedValue
						ElseIf (SelectedValue Is Nothing) Then
							TextValue = Me.Combo_Select2_Value.Text
						End If

						Call UpdateSelectedValueCombo(Me.Combo_FieldSelect2.SelectedItem, Me.Combo_Select2_Value)

						If (Not (SelectedValue Is Nothing)) Then
							Me.Combo_Select2_Value.SelectedValue = SelectedValue
						ElseIf TextValue.Length > 0 Then
							Me.Combo_Select2_Value.Text = TextValue
						End If
					End If
				Catch ex As Exception
				End Try
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: Second SelectField", ex.StackTrace, True)
		End Try

		' Check TableUpdate against third Select Field
		Try
			If Me.Combo_FieldSelect3.SelectedIndex >= 0 Then
				Try
					' GetFieldChangeID
					If (e.TableChanged(GetFieldChangeID(Combo_FieldSelect3.SelectedItem)) = True) Or KnowledgeDateChanged Then
						Dim SelectedValue As Object
						Dim TextValue As String

						SelectedValue = Nothing
						TextValue = ""

						If Me.Combo_Select3_Value.SelectedIndex >= 0 Then
							SelectedValue = Me.Combo_Select3_Value.SelectedValue
						ElseIf (SelectedValue Is Nothing) Then
							TextValue = Me.Combo_Select3_Value.Text
						End If

						Call UpdateSelectedValueCombo(Me.Combo_FieldSelect3.SelectedItem, Me.Combo_Select3_Value)

						If (Not (SelectedValue Is Nothing)) Then
							Me.Combo_Select3_Value.SelectedValue = SelectedValue
						ElseIf TextValue.Length > 0 Then
							Me.Combo_Select3_Value.Text = TextValue
						End If
					End If
				Catch ex As Exception
				End Try
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: Third SelectField", ex.StackTrace, True)
		End Try


		' Changes to the tblUserPermissions table :-

		Try
			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

				' Check ongoing permissions.

				Call CheckPermissions()
				If (HasReadPermission = False) Then
					Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

					FormIsValid = False
					InPaint = OrgInPaint
					Me.Close()
					Exit Sub
				End If

				RefreshGrid = True

			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: tblUserPermissions", ex.StackTrace, True)
		End Try


		' ****************************************************************
		' Changes to the Main FORM table :-
		' (or the tblTransactions table)
		' ****************************************************************

		Try
			If (e.TableChanged(THIS_FORM_ChangeID) = True) Or _
				 (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblTransaction) = True) Or _
				 (RefreshGrid = True) Or _
				 KnowledgeDateChanged Then

				' Re-Set Controls etc.
				Call SetSortedRows(True)

			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: Main Form table", ex.StackTrace, True)
		End Try

		InPaint = OrgInPaint

	End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

    ''' <summary>
    ''' Gets the transaction select string.
    ''' </summary>
    ''' <param name="OnlyUsertblTransactionFields">if set to <c>true</c> [only usertbl transaction fields].</param>
    ''' <returns>System.String.</returns>
	Private Function GetTransactionSelectString(Optional ByVal OnlyUsertblTransactionFields As Boolean = False) As String
		' *******************************************************************************
		' Build a Select String appropriate to the form status.
		' *******************************************************************************

		Dim SelectString As String
		Dim FieldSelectString As String

		SelectString = ""
		FieldSelectString = ""
		MainForm.SetToolStripText(Label_Status, "")

		If (myDataset Is Nothing) Then
			Return "True"
			Exit Function
		End If

		Try

			' Select on Fund...
			If (Me.Combo_TransactionFund.SelectedIndex > 0) Then
				If IsFieldTransactionField("TransactionFund") = True Then
					SelectString = "(TransactionFund=" & Combo_TransactionFund.SelectedValue.ToString & ")"
				End If
			End If

			' Select on Instrument

			If (Me.Combo_TransactionInstrument.SelectedIndex > 0) Then
				If IsFieldTransactionField("TransactionInstrument") = True Then
					If (SelectString.Length > 0) Then
						SelectString &= " AND "
					End If
					SelectString &= "(TransactionInstrument=" & Combo_TransactionInstrument.SelectedValue.ToString & ")"
				End If
			End If

			' Select on Transaction Ticket...

			If (Me.Combo_TicketOperator.SelectedIndex > 0) Then
				If IsFieldTransactionField("TransactionTicket") = True Then
					If (SelectString.Length > 0) Then
						SelectString &= " AND "
					End If

					SelectString &= "(TransactionTicket " & Combo_TicketOperator.SelectedItem.ToString & " '" & Combo_Ticket.Text & "')"
				End If
			End If


			' Select on Value Date

			If (Me.Combo_ValueDateOperator.SelectedIndex > 0) Then
				If (SelectString.Length > 0) Then
					SelectString &= " AND "
				End If

				SelectString &= "(TransactionValueDate " & Combo_ValueDateOperator.SelectedItem.ToString & " #" & Me.Date_ValueDate.Value.ToString(QUERY_SHORTDATEFORMAT) & "#)"
			End If

			' Field Select ...

			FieldSelectString = "("
      'Dim TransactionDataset As New RenaissanceDataClass.DSTransaction
      Dim TransactionTable As DataTable
      Dim FieldName As String

      'TransactionDataset = MainForm.Load_Table(RenaissanceStandardDatasets.tblTransaction, False)
			TransactionTable = myDataset.Tables(0)

			' Select Field One.

			Try
				If (Me.Combo_FieldSelect1.SelectedIndex > 0) AndAlso (Me.Combo_Select1_Operator.SelectedIndex > 0) And (Me.Combo_Select1_Value.Text.Length > 0) Then
					' Get Field Name, Add 'Transaction' backon if necessary.

					FieldName = Combo_FieldSelect1.SelectedItem

					If (FieldName.EndsWith(".")) Then
						FieldName = "Transaction" & FieldName.Substring(0, FieldName.Length - 1)
					End If

					If (OnlyUsertblTransactionFields = False) OrElse (IsFieldTransactionField(FieldName) = True) Then
						' If the Selected Field Name is a String Field ...

						If (TransactionTable.Columns(FieldName).DataType Is GetType(System.String)) Then

							' If there is a value Selected, Use it - else use the Combo Text.

							If (Not (Combo_Select1_Value.SelectedValue Is Nothing)) Then
								FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.Text & " '" & Combo_Select1_Value.SelectedValue.ToString & "')"
							Else
								FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.Text & " '" & Combo_Select1_Value.Text.ToString & "')"
							End If

						ElseIf (TransactionTable.Columns(FieldName).DataType Is GetType(System.DateTime)) Then

							' For Dates, If there is no 'Selected' value use the Combo text if it is a Valid date, else
							' Use the 'Selected' Value if it is a date.

							If (Me.Combo_Select1_Value.SelectedValue Is Nothing) AndAlso (IsDate(Me.Combo_Select1_Value.Text)) Then
								FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select1_Value.Text).ToString(QUERY_SHORTDATEFORMAT) & "#)"
							ElseIf (Not (Me.Combo_Select1_Value.SelectedValue Is Nothing)) And IsDate(Combo_Select1_Value.SelectedValue) Then
								FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select1_Value.SelectedValue).ToString(QUERY_SHORTDATEFORMAT) & "#)"
							Else
								FieldSelectString &= "(True)"
							End If

						Else
							' Now Deemed to be numeric, or at least not in need of quotes / delimeters...
							' If there is no 'Selected' value use the Combo text if it is a Valid number, else
							' Use the 'Selected' Value if it is a valid numeric.

							If (Not (Combo_Select1_Value.SelectedValue Is Nothing)) AndAlso (IsNumeric(Combo_Select1_Value.SelectedValue)) Then
								FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.SelectedItem.ToString & " " & Combo_Select1_Value.SelectedValue.ToString & ")"
							ElseIf IsNumeric(Combo_Select1_Value.Text) Then
								FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.SelectedItem.ToString & " " & Combo_Select1_Value.Text & ")"
							Else
								FieldSelectString &= "(True)"
							End If
						End If
					End If
				End If

			Catch ex As Exception
			End Try

			' Select Field Two.

			Try
				If (Me.Combo_FieldSelect2.SelectedIndex > 0) AndAlso (Me.Combo_Select2_Operator.SelectedIndex > 0) And (Me.Combo_Select2_Value.Text.Length > 0) Then
					FieldName = Combo_FieldSelect2.SelectedItem
					If (FieldName.EndsWith(".")) Then
						FieldName = "Transaction" & FieldName.Substring(0, FieldName.Length - 1)
					End If

					If (OnlyUsertblTransactionFields = False) OrElse (IsFieldTransactionField(FieldName) = True) Then
						If (FieldSelectString.Length > 1) Then
							FieldSelectString &= " " & Me.Combo_AndOr_1.SelectedItem.ToString & " "
						End If

						If (TransactionTable.Columns(FieldName).DataType Is GetType(System.String)) Then

							If (Not (Combo_Select2_Value.SelectedValue Is Nothing)) Then
								FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.Text & " '" & Combo_Select2_Value.SelectedValue.ToString & "')"
							Else
								FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.Text & " '" & Combo_Select2_Value.Text.ToString & "')"
							End If

						ElseIf (TransactionTable.Columns(FieldName).DataType Is GetType(System.DateTime)) Then

							If (Me.Combo_Select2_Value.SelectedValue Is Nothing) AndAlso (IsDate(Me.Combo_Select2_Value.Text)) Then
								FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select2_Value.Text).ToString(QUERY_SHORTDATEFORMAT) & "#)"
							ElseIf (Not (Me.Combo_Select2_Value.SelectedValue Is Nothing)) And IsDate(Combo_Select2_Value.SelectedValue) Then
								FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select2_Value.SelectedValue).ToString(QUERY_SHORTDATEFORMAT) & "#)"
							Else
								FieldSelectString &= "(True)"
							End If

						Else
							If (Not (Combo_Select2_Value.SelectedValue Is Nothing)) AndAlso (IsNumeric(Combo_Select2_Value.SelectedValue)) Then
								FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.SelectedItem.ToString & " " & Combo_Select2_Value.SelectedValue.ToString & ")"
							ElseIf IsNumeric(Combo_Select2_Value.Text) Then
								FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.SelectedItem.ToString & " " & Combo_Select2_Value.Text & ")"
							Else
								FieldSelectString &= "(True)"
							End If
						End If
					End If
				End If

			Catch ex As Exception
			End Try


			' Select Field Three.

			Try
				If (Me.Combo_FieldSelect3.SelectedIndex > 0) AndAlso (Me.Combo_Select3_Operator.SelectedIndex > 0) And (Me.Combo_Select3_Value.Text.Length > 0) Then
					FieldName = Combo_FieldSelect3.SelectedItem
					If (FieldName.EndsWith(".")) Then
						FieldName = "Transaction" & FieldName.Substring(0, FieldName.Length - 1)
					End If

					If (OnlyUsertblTransactionFields = False) OrElse (IsFieldTransactionField(FieldName) = True) Then

						If (FieldSelectString.Length > 1) Then
							FieldSelectString &= " " & Me.Combo_AndOr_2.SelectedItem.ToString & " "
						End If

						If (TransactionTable.Columns(FieldName).DataType Is GetType(System.String)) Then

							If (Not (Combo_Select3_Value.SelectedValue Is Nothing)) Then
								FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.Text & " '" & Combo_Select3_Value.SelectedValue.ToString & "')"
							Else
								FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.Text & " '" & Combo_Select3_Value.Text.ToString & "')"
							End If

						ElseIf (TransactionTable.Columns(FieldName).DataType Is GetType(System.DateTime)) Then

							If (Me.Combo_Select3_Value.SelectedValue Is Nothing) AndAlso (IsDate(Me.Combo_Select3_Value.Text)) Then
								FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select3_Value.Text).ToString(QUERY_SHORTDATEFORMAT) & "#)"
							ElseIf (Not (Me.Combo_Select3_Value.SelectedValue Is Nothing)) And IsDate(Combo_Select3_Value.SelectedValue) Then
								FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select3_Value.SelectedValue).ToString(QUERY_SHORTDATEFORMAT) & "#)"
							Else
								FieldSelectString &= "(True)"
							End If

						Else
							If (Not (Combo_Select3_Value.SelectedValue Is Nothing)) AndAlso (IsNumeric(Combo_Select3_Value.SelectedValue)) Then
								FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.SelectedItem.ToString & " " & Combo_Select3_Value.SelectedValue.ToString & ")"
							ElseIf IsNumeric(Combo_Select3_Value.Text) Then
								FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.SelectedItem.ToString & " " & Combo_Select3_Value.Text & ")"
							Else
								FieldSelectString &= "(True)"
							End If
						End If
					End If
				End If

			Catch ex As Exception
			End Try

			FieldSelectString &= ")"

			' If the FieldSelect string is used, then tag it on to the main select string.

			If FieldSelectString.Length > 2 Then
				If (SelectString.Length > 0) Then
					SelectString &= " AND "
				End If
				SelectString &= FieldSelectString
			End If

			' Check for the 'Incomplete Transactions' option.

			If Me.Check_IncompleteTransactions.Checked = True Then
				If SelectString.Length <= 0 Then
					SelectString = INCOMPLETE_TRANSACTION_QUERY
				Else
					SelectString = "(" & SelectString & ") AND " & INCOMPLETE_TRANSACTION_QUERY
				End If
			End If

			If SelectString.Length <= 0 Then
				SelectString = "true"
			End If

		Catch ex As Exception
			SelectString = "true"
		End Try

		Return SelectString

	End Function

    ''' <summary>
    ''' Determines whether [is field transaction field] [the specified field name].
    ''' </summary>
    ''' <param name="FieldName">Name of the field.</param>
    ''' <returns><c>true</c> if [is field transaction field] [the specified field name]; otherwise, <c>false</c>.</returns>
	Private Function IsFieldTransactionField(ByVal FieldName As String) As Boolean
		' *******************************************************************************
		' Simple function to return a boolean value indicating whether or not a column name is
		' part of the tblTransactions table.
		' *******************************************************************************

    Try
      Dim TransactionTable As New RenaissanceDataClass.DSTransaction.tblTransactionDataTable

      If (TransactionTable Is Nothing) Then
        Return False
      Else
        Return TransactionTable.Columns.Contains(FieldName)
      End If
    Catch ex As Exception
      Return (False)
    End Try

		Return (False)

  End Function

    ''' <summary>
    ''' Sets the sorted rows.
    ''' </summary>
    ''' <param name="ForceRefresh">if set to <c>true</c> [force refresh].</param>
	Private Sub SetSortedRows(Optional ByVal ForceRefresh As Boolean = False)
		' *******************************************************************************
		' Build a Select String appropriate to the form status and apply it to the DataView object.
		' *******************************************************************************

		Dim SelectString As String

		SelectString = GetTransactionSelectString()

		If (myDataView Is Nothing) Then
			Exit Sub
		End If

		Try
			If (Not (myDataView.RowFilter = SelectString)) Or (ForceRefresh = True) Then
				If ForceRefresh = True Then
					myDataView.RowFilter = "False"
				End If

				myDataView.RowFilter = SelectString
				PaintTransactionGrid()

			End If
		Catch ex As Exception
      Try
        SelectString = "true"
        myDataView.RowFilter = SelectString
      Catch ex_inner As Exception
      End Try
		End Try

		Me.Label_Status.Text = "(" & myDataView.Count.ToString & " Records) " & SelectString

	End Sub


	' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
	Private Sub CheckPermissions()
		' *******************************************************************************
		'
		' *******************************************************************************

		Dim Permissions As Integer

		Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

		HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
		HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
		HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
		HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

	End Sub

    ''' <summary>
  ''' Basic event handler, called when a form value is changed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
		' *******************************************************************************
		' In the event of one of the Form Controls changing, refresh the data grid.
		' *******************************************************************************

		If InPaint = False Then

      If sender Is Combo_TransactionFund Then
        Call SetInstrumentCombo()
      End If

			Call SetSortedRows()

		End If
	End Sub

    ''' <summary>
    ''' Handles the KeyUp event of the Combo_FieldSelect control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
	Private Sub Combo_FieldSelect_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Combo_Select1_Value.KeyUp
		' *******************************************************************************
		' In the event of one of the Form Controls changing, refresh the data grid.
		' *******************************************************************************

		If InPaint = False Then

			Call SetSortedRows()

		End If
	End Sub


    ''' <summary>
    ''' Builds the field select combos.
    ''' </summary>
	Private Sub BuildFieldSelectCombos()
		' *********************************************************************************
		' Build the FieldSelect combo boxes.
		'
		' Field Names starting with 'Transaction' are modified to start with '.'
		' This is done in order to make the name more readable.
		'
		' *********************************************************************************

		Dim ColumnName As String
		Dim thisColumn As DataColumn

		Combo_FieldSelect1.Items.Clear()
		Combo_Select1_Value.Items.Clear()
		Combo_FieldSelect1.Items.Add("")

		Combo_FieldSelect2.Items.Clear()
		Combo_Select2_Value.Items.Clear()
		Combo_FieldSelect2.Items.Add("")

		Combo_FieldSelect3.Items.Clear()
		Combo_Select3_Value.Items.Clear()
		Combo_FieldSelect3.Items.Add("")

		For Each thisColumn In myTable.Columns
			Try
        ColumnName = thisColumn.ColumnName
				If (ColumnName.StartsWith("Transaction")) Then
					ColumnName = ColumnName.Substring(11) & "."
				End If

				Combo_FieldSelect1.Items.Add(ColumnName)
				Combo_FieldSelect2.Items.Add(ColumnName)
				Combo_FieldSelect3.Items.Add(ColumnName)

			Catch ex As Exception
			End Try
		Next

	End Sub

    ''' <summary>
    ''' Updates the selected value combo.
    ''' </summary>
    ''' <param name="pFieldName">Name of the p field.</param>
    ''' <param name="pValueCombo">The p value combo.</param>
	Private Sub UpdateSelectedValueCombo(ByVal pFieldName As String, ByRef pValueCombo As ComboBox)
		' *******************************************************************************
		' Build a Value Combo Box appropriate to the chosen DataTable Select Field.
		'
		' By default the combo will contain all existing values for the chosen field, however
    ' If a relationship is defined for this table and field, then a Combo will be built
		' which reflects this relationship.
		' e.g. the tblReferentialIntegrity table defined a relationship between the
		' TransactionCounterparty field and the tblCounterparty.Counterparty field, thus if
		' the chosen Select field ti 'Counterparty.' then the Value combo will be built using
		' the CounterpartyID & CounterpartyName details from the tblCounterparty table.
		'
		' *******************************************************************************

		Dim tblRefIntegrity As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityDataTable
		Dim IntegrityRows() As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityRow
    'Dim TransactionDataset As DataSet
    Dim SelectTransactionsTable As DataTable
    Dim SortOrder As Boolean

    Dim Org_InPaint As Boolean = InPaint

    Try
      InPaint = True

      SortOrder = True

      ' Clear the Value Combo.

      Try
        pValueCombo.DataSource = Nothing
        pValueCombo.DisplayMember = ""
        pValueCombo.ValueMember = ""
        pValueCombo.Items.Clear()
      Catch ex As Exception
      End Try

      ' By default, make the Combo a Standard Dropdown, so that non-list items can be entered.

      pValueCombo.DropDownStyle = ComboBoxStyle.DropDown

      ' Exit if no FiledName is given (having cleared the combo).

      If (pFieldName.Length <= 0) Then Exit Sub

      ' Adjust the Field name if appropriate.

      If (pFieldName.EndsWith(".")) Then
        pFieldName = "Transaction" & pFieldName.Substring(0, pFieldName.Length - 1)
      End If

      ' Get a handle to the Standard Transactions Table.
      ' This is so that the field types can be determined and used.

      Try
        'TransactionDataset = MainForm.Load_Table(ThisStandardDataset, False)
        SelectTransactionsTable = myDataset.Tables(0)
      Catch ex As Exception
        Exit Sub
      End Try

      ' If the selected field is a Date then sort the selection combo in reverse order.

      If (SelectTransactionsTable.Columns(pFieldName).DataType Is GetType(System.DateTime)) Then
        SortOrder = False
      End If

      ' Get a handle to the Referential Integrity table and the row matching this table and field.
      ' this table defines a relationship between Transaction Table fields and other tables.
      ' This information can be used to build more meaningfull Value Combos.

      Try
        tblRefIntegrity = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblReferentialIntegrity, False), RenaissanceDataClass.DSReferentialIntegrity).tblReferentialIntegrity
        IntegrityRows = tblRefIntegrity.Select("(FeedsTable LIKE 'tblSelectTransaction') AND (FeedsField = '" & pFieldName & "')", "RN")
        If (IntegrityRows.Length <= 0) Then
          GoTo StandardExit
        End If

      Catch ex As Exception
        Exit Sub
      End Try

      If (IntegrityRows(0).IsDescriptionFieldNull) Then
        ' No Linked Description Field

        GoTo StandardExit
      End If

      ' OK, a referential record exists.
      ' Determine the Table and Field Names to use to build the Value Combo.

      Dim TableName As String
      Dim TableField As String
      Dim DescriptionField As String
      Dim stdDS As StandardDataset
      Dim thisChangeID As RenaissanceChangeID

      Try

        TableName = IntegrityRows(0).TableName
        TableField = IntegrityRows(0).TableField
        DescriptionField = IntegrityRows(0).DescriptionField

        thisChangeID = CType(System.Enum.Parse(GetType(RenaissanceChangeID), TableName), RenaissanceChangeID)

        stdDS = RenaissanceStandardDatasets.GetStandardDataset(thisChangeID)

        ' Build the appropriate Values Combo.

        Call MainForm.SetTblGenericCombo( _
        pValueCombo, _
        stdDS, _
        DescriptionField, _
        TableField, _
        "", False, SortOrder, True)    ' 

        ' For Referential Integrity generated combo's, make the Combo a 
        ' DropDownList

        pValueCombo.DropDownStyle = ComboBoxStyle.DropDownList

      Catch ex As Exception
        GoTo StandardExit

      End Try

      Exit Sub

StandardExit:

      ' Build the standard Combo, just use the values from the given field.
      ' NPP 17 Apr 2013, Change 'RenaissanceStandardDatasets.tblTransaction' to 'ThisStandardDataset'

      Call MainForm.SetTblGenericCombo( _
      pValueCombo, _
      ThisStandardDataset, _
      pFieldName, _
      pFieldName, _
      "", True, SortOrder)   ' 

    Catch ex As Exception
    Finally
      InPaint = Org_InPaint
    End Try

	End Sub

    ''' <summary>
    ''' Gets the field change ID.
    ''' </summary>
    ''' <param name="pFieldName">Name of the p field.</param>
    ''' <returns>RenaissanceChangeID.</returns>
	Private Function GetFieldChangeID(ByVal pFieldName As String) As RenaissanceChangeID
		Dim tblRefIntegrity As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityDataTable
		Dim IntegrityRows() As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityRow

		If (pFieldName.EndsWith(".")) Then
			pFieldName = "Transaction" & pFieldName.Substring(0, pFieldName.Length - 1)
		End If

		Try
			tblRefIntegrity = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblReferentialIntegrity, False), RenaissanceDataClass.DSReferentialIntegrity).tblReferentialIntegrity
      IntegrityRows = tblRefIntegrity.Select("(FeedsTable LIKE '%tblSelectTransaction') AND (FeedsField = '" & pFieldName & "')", "RN")
			If (IntegrityRows.Length <= 0) Then
				Return RenaissanceChangeID.None
				Exit Function
			End If

		Catch ex As Exception
			Return RenaissanceChangeID.None
			Exit Function
		End Try


		If (IntegrityRows(0).IsDescriptionFieldNull) Then
			Return RenaissanceChangeID.None
			Exit Function
		End If

		' OK, a referential record exists.
		' Determine the Table and Field Names to use to build the Value Combo.

		Dim TableName As String
		Dim TableField As String
		Dim DescriptionField As String
		Dim thisChangeID As RenaissanceChangeID

		Try

			TableName = IntegrityRows(0).TableName
			TableField = IntegrityRows(0).TableField
			DescriptionField = IntegrityRows(0).DescriptionField

			thisChangeID = CType(System.Enum.Parse(GetType(RenaissanceChangeID), TableName), RenaissanceChangeID)

			Return thisChangeID
			Exit Function
		Catch ex As Exception
		End Try

		Return RenaissanceChangeID.None
		Exit Function

	End Function


    ''' <summary>
    ''' Handles the DoubleClick event of the Grid_Transactions control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Grid_Transactions_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Grid_Transactions.DoubleClick

		' ***************************************************************************************
		' Spawn an Add/Edit Transactions form reflecting the chosen selection of transactions
		' and pointing to the transaction which was double clicked.
		' ***************************************************************************************

		Dim ParentID As Integer
		Dim thisFormHandle As RenaissanceGlobals.RenaissanceFormHandle

		If (Grid_Transactions.RowSel > 0) AndAlso (Grid_Transactions.Rows(Grid_Transactions.RowSel).IsNode) Then
			Grid_Transactions.Rows(Grid_Transactions.RowSel).Node.Expanded = (Not Grid_Transactions.Rows(Grid_Transactions.RowSel).Node.Expanded)
			Exit Sub
		End If

		Try
			ParentID = Me.Grid_Transactions.Rows(Grid_Transactions.RowSel)("ParentID")

			If (ParentID > 0) Then
				Grid_Transactions.Cursor = Cursors.WaitCursor

				thisFormHandle = MainForm.New_VeniceForm(VeniceFormID.frmTransaction)
				CType(thisFormHandle.Form, frmTransaction).FormSelectCriteria = GetTransactionSelectString(True)
				CType(thisFormHandle.Form, frmTransaction).MoveToAuditID(ParentID)
			End If

		Catch ex As Exception
		Finally
			Grid_Transactions.Cursor = Cursors.Default
		End Try

	End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_FieldSelect1 control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_FieldSelect1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_FieldSelect1.SelectedIndexChanged
		' ***************************************************************************************
		' Field Select Combo Chosen, Update the associated Values Combo.
		'
		' ***************************************************************************************

		Call UpdateSelectedValueCombo(Me.Combo_FieldSelect1.SelectedItem, Me.Combo_Select1_Value)
		Me.Combo_Select1_Operator.SelectedIndex = 1
		If (Not InPaint) Then
			Me.SetSortedRows(True)
		End If

	End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_FieldSelect2 control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_FieldSelect2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_FieldSelect2.SelectedIndexChanged
		' ***************************************************************************************
		' Field Select Combo Chosen, Update the associated Values Combo.
		'
		' ***************************************************************************************
		Call UpdateSelectedValueCombo(Me.Combo_FieldSelect2.SelectedItem, Me.Combo_Select2_Value)
		Me.Combo_Select2_Operator.SelectedIndex = 1
		If (Not InPaint) Then
			Me.SetSortedRows(True)
		End If

	End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_FieldSelect3 control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_FieldSelect3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_FieldSelect3.SelectedIndexChanged
		' ***************************************************************************************
		' Field Select Combo Chosen, Update the associated Values Combo.
		'
		' ***************************************************************************************
		Call UpdateSelectedValueCombo(Me.Combo_FieldSelect3.SelectedItem, Me.Combo_Select3_Value)
		Me.Combo_Select3_Operator.SelectedIndex = 1
		If (Not InPaint) Then
			Me.SetSortedRows(True)
		End If

	End Sub

  ''' <summary>
  ''' Handles the CheckedChanged event of the Check_ByISIN control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Check_ByISIN_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_ByISIN.CheckedChanged
    ' *******************************************************************************
    '
    ' *******************************************************************************

    If (InPaint = False) Then
      Try
        Dim ExistingID As Integer = (-1)

        If (Me.Combo_TransactionInstrument.SelectedIndex >= 0) AndAlso (CInt(Me.Combo_TransactionInstrument.SelectedValue) > 0) Then
          ExistingID = CInt(Me.Combo_TransactionInstrument.SelectedValue)
        End If

        InPaint = True

        Call SetInstrumentCombo()

        If Combo_TransactionInstrument.Items.Count > 0 Then
          If (ExistingID > 0) Then
            Combo_TransactionInstrument.SelectedValue = ExistingID
          Else
            Combo_TransactionInstrument.SelectedIndex = 0
          End If
        End If

      Catch ex As Exception
        Call MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Check_ByISIN_CheckedChanged()", ex.StackTrace, True)
      Finally
        InPaint = False
      End Try
    End If

  End Sub

#End Region

#Region " Set Form Combos (Form Specific Code) "

    ''' <summary>
    ''' Sets the fund combo.
    ''' </summary>
	Private Sub SetFundCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_TransactionFund, _
    RenaissanceStandardDatasets.tblFund, _
    "FundName", _
    "FundID", _
    "FundClosed=0", False, True, True)   ' 

	End Sub

    ''' <summary>
  ''' Populate the Instrument combo.
    ''' </summary>
	Private Sub SetInstrumentCombo()

    Try

      Dim FundID As Integer = 0
      If (Combo_TransactionFund.SelectedIndex >= 0) AndAlso (CInt(Nz(Combo_TransactionFund.SelectedValue, 0)) > 0) Then

        FundID = CInt(Combo_TransactionFund.SelectedValue)

        If (Check_ByISIN.Checked) Then

          Call MainForm.SetTblGenericCombo( _
            Me.Combo_TransactionInstrument, _
            RenaissanceStandardDatasets.tblSelectTransaction, _
            "InstrumentISIN|InstrumentDescription", _
            "TransactionInstrument", _
            "TransactionFund=" & FundID.ToString, True, True, True, 0)    ' 

        Else

          Call MainForm.SetTblGenericCombo( _
            Me.Combo_TransactionInstrument, _
            RenaissanceStandardDatasets.tblSelectTransaction, _
            "InstrumentDescription|InstrumentISIN", _
            "TransactionInstrument", _
            "TransactionFund=" & FundID.ToString, True, True, True, 0)    ' 

        End If

      Else

        If (Check_ByISIN.Checked) Then

          Call MainForm.SetTblGenericCombo( _
            Me.Combo_TransactionInstrument, _
            RenaissanceStandardDatasets.tblSelectTransaction, _
            "InstrumentISIN|InstrumentDescription", _
            "TransactionInstrument", _
            "", True, True, True, 0)    ' 

        Else

          Call MainForm.SetTblGenericCombo( _
            Me.Combo_TransactionInstrument, _
            RenaissanceStandardDatasets.tblSelectTransaction, _
            "InstrumentDescription|InstrumentISIN", _
            "TransactionInstrument", _
            "", True, True, True, 0)    ' 

        End If

      End If

    Catch ex As Exception
    End Try

	End Sub

    ''' <summary>
    ''' Sets the ticket combo.
    ''' </summary>
	Private Sub SetTicketCombo()

		'Call MainForm.SetTblGenericCombo( _
		'Me.Combo_Ticket, _
		'RenaissanceStandardDatasets.tblTransaction, _
		'"TransactionTicket", _
		'"TransactionTicket", _
		'"", True, True, True)			' 

	End Sub


#End Region

#Region " Transaction report Menu"

    ''' <summary>
    ''' Sets the transaction report menu.
    ''' </summary>
    ''' <param name="RootMenu">The root menu.</param>
    ''' <returns>MenuStrip.</returns>
	Private Function SetTransactionReportMenu(ByRef RootMenu As MenuStrip) As MenuStrip

		Dim ReportMenu As New ToolStripMenuItem("Transaction &Reports")
		Dim newMenuItem As ToolStripMenuItem

		newMenuItem = ReportMenu.DropDownItems.Add("Transaction &Ticket", Nothing, AddressOf Me.rptTransactionTicket)

		ReportMenu.DropDownItems.Add(New ToolStripSeparator)

		newMenuItem = ReportMenu.DropDownItems.Add("Transaction By &Value Date", Nothing, AddressOf Me.rptTransactionByDate)
		newMenuItem = ReportMenu.DropDownItems.Add("Transaction By &Entry Date", Nothing, AddressOf Me.rptTransactionByEntryDate)
		newMenuItem = ReportMenu.DropDownItems.Add("Transaction By &Group", Nothing, AddressOf Me.rptTransactionByGroup)
		newMenuItem = ReportMenu.DropDownItems.Add("Transaction By &Instrument", Nothing, AddressOf Me.rptTransactionByStock)
		newMenuItem = ReportMenu.DropDownItems.Add("&Incomplete Transaction Report", Nothing, AddressOf Me.rptIncompleteTransactions)
		newMenuItem = ReportMenu.DropDownItems.Add("Transaction &Price Differences", Nothing, AddressOf Me.rptTransactionPriceDifference)
    newMenuItem = ReportMenu.DropDownItems.Add("&Instrument Position Report", Nothing, AddressOf Me.rptInstrumentPositionsReport)

		ReportMenu.DropDownItems.Add(New ToolStripSeparator)

		newMenuItem = ReportMenu.DropDownItems.Add("Trade &Blotter", Nothing, AddressOf Me.rptTradeBlotter)
		newMenuItem = ReportMenu.DropDownItems.Add("&Final Trade Blotter", Nothing, AddressOf Me.rptFinalTradeBlotter)
		newMenuItem = ReportMenu.DropDownItems.Add("Trade &Revision Blotter", Nothing, AddressOf Me.rptTradeRevisionBlotter)
		newMenuItem = ReportMenu.DropDownItems.Add("Final Trade &Revision Blotter", Nothing, AddressOf Me.rptFinalTradeRevisionBlotter)

		ReportMenu.DropDownItems.Add(New ToolStripSeparator)

		newMenuItem = ReportMenu.DropDownItems.Add("F&X Blotter", Nothing, AddressOf Me.rptFXBlotter)
		newMenuItem = ReportMenu.DropDownItems.Add("Final FX Blotter", Nothing, AddressOf Me.rptFinalFXBlotter)

		RootMenu.Items.Add(ReportMenu)
		Return RootMenu

	End Function

    ''' <summary>
    ''' RPTs the transaction ticket.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Private Sub rptTransactionTicket(ByVal sender As Object, ByVal e As EventArgs)
		Dim ThisDataset As StandardDataset

		ThisDataset = RenaissanceStandardDatasets.GetStandardDataset(RenaissanceChangeID.tblSelectTransaction)
		If (ThisDataset.NotedIDsHaveChanged) Then
			MainForm.Load_Table(ThisDataset, True)
			Call SetSortedRows()
		End If

		MainForm.MainReportHandler.DisplayReport(0, "rptTransactionTicket", myDataView, Form_ProgressBar)
	End Sub

    ''' <summary>
    ''' RPTs the transaction by date.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Private Sub rptTransactionByDate(ByVal sender As Object, ByVal e As EventArgs)
		Dim ThisDataset As StandardDataset

		ThisDataset = RenaissanceStandardDatasets.GetStandardDataset(RenaissanceChangeID.tblSelectTransaction)
		If (ThisDataset.NotedIDsHaveChanged) Then
			MainForm.Load_Table(ThisDataset, True)
			Call SetSortedRows()
		End If

		MainForm.MainReportHandler.DisplayReport(0, "rptTransactionByDate", myDataView, Form_ProgressBar)
	End Sub

  ' rptInstrumentPositionsReport
    ''' <summary>
    ''' RPTs the instrument positions report.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
  Private Sub rptInstrumentPositionsReport(ByVal sender As Object, ByVal e As EventArgs)
    Dim ThisDataset As StandardDataset

    ThisDataset = RenaissanceStandardDatasets.GetStandardDataset(RenaissanceChangeID.tblSelectTransaction)
    If (ThisDataset.NotedIDsHaveChanged) Then
			MainForm.Load_Table(ThisDataset, True)
      Call SetSortedRows()
    End If

    MainForm.MainReportHandler.DisplayReport(0, "rptInstrumentPositionsReport", myDataView, Form_ProgressBar)
  End Sub

    ''' <summary>
    ''' RPTs the transaction by entry date.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Private Sub rptTransactionByEntryDate(ByVal sender As Object, ByVal e As EventArgs)
		Dim ThisDataset As StandardDataset

		ThisDataset = RenaissanceStandardDatasets.GetStandardDataset(RenaissanceChangeID.tblSelectTransaction)
		If (ThisDataset.NotedIDsHaveChanged) Then
			MainForm.Load_Table(ThisDataset, True)
			Call SetSortedRows()
		End If

		MainForm.MainReportHandler.DisplayReport(0, "rptTransactionByEntryDate", myDataView, Form_ProgressBar)
	End Sub

    ''' <summary>
    ''' RPTs the transaction by group.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Private Sub rptTransactionByGroup(ByVal sender As Object, ByVal e As EventArgs)
		Dim ThisDataset As StandardDataset

		ThisDataset = RenaissanceStandardDatasets.GetStandardDataset(RenaissanceChangeID.tblSelectTransaction)
		If (ThisDataset.NotedIDsHaveChanged) Then
			MainForm.Load_Table(ThisDataset, True)
			Call SetSortedRows()
		End If

		MainForm.MainReportHandler.DisplayReport(0, "rptTransactionByGroup", myDataView, Form_ProgressBar)
	End Sub

    ''' <summary>
    ''' RPTs the transaction by stock.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Private Sub rptTransactionByStock(ByVal sender As Object, ByVal e As EventArgs)
		Dim ThisDataset As StandardDataset

		ThisDataset = RenaissanceStandardDatasets.GetStandardDataset(RenaissanceChangeID.tblSelectTransaction)
		If (ThisDataset.NotedIDsHaveChanged) Then
			MainForm.Load_Table(ThisDataset, True)
			Call SetSortedRows()
		End If

		MainForm.MainReportHandler.DisplayReport(0, "rptTransactionByStock", myDataView, Form_ProgressBar)
	End Sub


    ''' <summary>
    ''' RPTs the incomplete transactions.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Private Sub rptIncompleteTransactions(ByVal sender As Object, ByVal e As EventArgs)
		Dim ThisDataset As StandardDataset

		Try
			ThisDataset = RenaissanceStandardDatasets.GetStandardDataset(RenaissanceChangeID.tblSelectTransaction)
			If (ThisDataset.NotedIDsHaveChanged) Then
				MainForm.Load_Table(ThisDataset, True)
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error, Failed to Load Select Transactions table", ex.StackTrace, True)
			Exit Sub
		End Try

		Dim SelectString As String
		Dim rptDataView As DataView

		SelectString = GetTransactionSelectString()

		Try
			If (Not SelectString.EndsWith("AND " & INCOMPLETE_TRANSACTION_QUERY)) Then
				SelectString = "(" & SelectString & ") AND " & INCOMPLETE_TRANSACTION_QUERY
			End If

			rptDataView = New DataView(myTable, SelectString, "", DataViewRowState.CurrentRows)
		Catch ex As Exception
			SelectString = "true"
			rptDataView = New DataView(myTable, SelectString, "", DataViewRowState.CurrentRows)
		End Try

		MainForm.MainReportHandler.DisplayReport(0, "rptIncompleteTransactions", rptDataView, Form_ProgressBar)
	End Sub


    ''' <summary>
    ''' RPTs the transaction price difference.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Private Sub rptTransactionPriceDifference(ByVal sender As Object, ByVal e As EventArgs)
		Dim rptDataTable As DataTable
		Dim FundID As Integer

		Try
			FundID = 0
			If Me.Combo_TransactionFund.SelectedIndex >= 0 Then
				If IsNumeric(Combo_TransactionFund.SelectedValue) Then
					FundID = CInt(Combo_TransactionFund.SelectedValue)
				End If
			End If
		Catch ex As Exception
			FundID = 0
		End Try

		rptDataTable = MainForm.MainReportHandler.GetData_rptTransactionPriceDifference(FundID)


		Dim SelectString As String
		Dim rptDataView As DataView

		SelectString = GetTransactionSelectString()

		Try
			rptDataView = New DataView(rptDataTable, SelectString, "", DataViewRowState.CurrentRows)
		Catch ex As Exception
			SelectString = "true"
			rptDataView = New DataView(rptDataTable, SelectString, "", DataViewRowState.CurrentRows)
		End Try

		' dbo.fn_BestSinglePrice(Transactions.TransactionInstrument, Transactions.TransactionValueDate , @OnlyFinalPrices, @KnowledgeDate) as PriceLevel,
		Dim thisDataRow As DataRow
		Dim GetPriceCommand As New SqlCommand
		Dim Counter As Integer

		Try
			GetPriceCommand.Connection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)
			GetPriceCommand.CommandType = CommandType.Text
			GetPriceCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

			rptDataView.AllowEdit = True
			For Counter = 0 To (rptDataView.Count - 1)
				Try
					thisDataRow = rptDataView.Item(Counter).Row
					GetPriceCommand.CommandText = "SELECT dbo.fn_BestSinglePrice(" & thisDataRow("TransactionInstrument").ToString & ", '" & CDate(thisDataRow("TransactionValueDate")).ToString(QUERY_SHORTDATEFORMAT) & "', 0, '" & MainForm.Main_Knowledgedate.ToString(QUERY_LONGDATEFORMAT) & "')"
					SyncLock GetPriceCommand.Connection
            thisDataRow("PriceLevel") = CDbl(MainForm.ExecuteScalar(GetPriceCommand))
            ' thisDataRow("PriceLevel") = CDbl(GetPriceCommand.ExecuteScalar)
          End SyncLock
				Catch ex As Exception
				End Try
			Next

		Catch ex As Exception
		Finally
			If (GetPriceCommand IsNot Nothing) Then
				GetPriceCommand.Connection = Nothing
			End If
		End Try

		MainForm.MainReportHandler.DisplayReport(0, "rptTransactionPriceDifference", rptDataView, Form_ProgressBar)
	End Sub

    ''' <summary>
    ''' RPTs the trade revision blotter.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Private Sub rptTradeRevisionBlotter(ByVal sender As Object, ByVal e As EventArgs)
		' ***************************************************************************************
		'
		'
		' ***************************************************************************************

		Call ShowTradeRevisionBlotter(False)
	End Sub

    ''' <summary>
    ''' RPTs the final trade revision blotter.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Private Sub rptFinalTradeRevisionBlotter(ByVal sender As Object, ByVal e As EventArgs)
		' ***************************************************************************************
		'
		'
		' ***************************************************************************************

		Call ShowTradeRevisionBlotter(True)
	End Sub

    ''' <summary>
    ''' RPTs the trade blotter.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Private Sub rptTradeBlotter(ByVal sender As Object, ByVal e As EventArgs)
		' ***************************************************************************************
		'
		'
		' ***************************************************************************************

		Call ShowTradeBlotter(False)
	End Sub

    ''' <summary>
    ''' RPTs the final trade blotter.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Private Sub rptFinalTradeBlotter(ByVal sender As Object, ByVal e As EventArgs)
		' ***************************************************************************************
		'
		'
		' ***************************************************************************************

		Call ShowTradeBlotter(True)
	End Sub

    ''' <summary>
    ''' Shows the trade blotter.
    ''' </summary>
    ''' <param name="pFinal">if set to <c>true</c> [p final].</param>
	Private Sub ShowTradeBlotter(Optional ByVal pFinal As Boolean = False)
		' ***************************************************************************************
		'
		'
		' ***************************************************************************************

		Dim TradesDataset As StandardDataset

		Dim UpFrontFeeFunds As String
		Dim TradeBlotterTicketNumber As String
		Dim Counter As Integer

		' Refresh 'SelectTransaction' Table ?

		Try
			TradesDataset = RenaissanceStandardDatasets.GetStandardDataset(RenaissanceChangeID.tblSelectTransaction)
			If (TradesDataset.NotedIDsHaveChanged) Then
				MainForm.Load_Table(TradesDataset, True)
				Call SetSortedRows()
			End If
		Catch ex As Exception
			MainForm.LogError("ShowTradeBlotter()", LOG_LEVELS.Error, ex.Message, "Error loading tblSelectTransaction", ex.StackTrace, True)
			Exit Sub
		End Try

		' Resolve Trade Blotter number.

		Try
			TradeBlotterTicketNumber = MainForm.Get_SystemString("TradeBlotterTicketNumber")
			If IsNumeric(TradeBlotterTicketNumber) = False Then
				TradeBlotterTicketNumber = "000001"
				MainForm.Set_SystemString("TradeBlotterTicketNumber", TradeBlotterTicketNumber)
			ElseIf pFinal Then
				If MessageBox.Show("Are you sure that you want to run a FINAL blotter ?" & vbCrLf & "This will increment the Ticker number.", "Are you sure ?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
					TradeBlotterTicketNumber = (CInt(TradeBlotterTicketNumber) + 1).ToString("000000")
					MainForm.Set_SystemString("TradeBlotterTicketNumber", TradeBlotterTicketNumber)
				Else
					Exit Sub
				End If
			End If

			' Get Fund names that we know have UpFront fees.

			UpFrontFeeFunds = ""

			For Counter = 0 To (myDataView.Count - 1)
				If (CDbl(myDataView(Counter)("InstrumentPenalty")) > 0) Then
					If (UpFrontFeeFunds.Length > 0) Then
						UpFrontFeeFunds &= ", "
					End If
					UpFrontFeeFunds &= CStr(myDataView(Counter)("InstrumentDescription"))
				End If
			Next

			If (UpFrontFeeFunds.Length > 0) Then
				If UpFrontFeeFunds.LastIndexOf(",") > 0 Then
					UpFrontFeeFunds = UpFrontFeeFunds.Substring(0, UpFrontFeeFunds.LastIndexOf(",")) & " and" & UpFrontFeeFunds.Substring(UpFrontFeeFunds.LastIndexOf(",") + 1)
				End If

				UpFrontFeeFunds = " other than for " & UpFrontFeeFunds
			End If

		Catch ex As Exception
			MainForm.LogError("ShowTradeBlotter()", LOG_LEVELS.Error, ex.Message, "Error getting header information.", ex.StackTrace, True)
			Exit Sub
		End Try

		' Produce report.

		Call MainForm.MainReportHandler.rptTradeBlotterReport(UpFrontFeeFunds, TradeBlotterTicketNumber, myDataView, pFinal, Form_ProgressBar)

	End Sub

    ''' <summary>
    ''' Shows the trade revision blotter.
    ''' </summary>
    ''' <param name="pFinal">if set to <c>true</c> [p final].</param>
	Private Sub ShowTradeRevisionBlotter(Optional ByVal pFinal As Boolean = False)
		' ***************************************************************************************
		'
		'
		' ***************************************************************************************

		Dim TradesDataset As StandardDataset

		Try
			TradesDataset = RenaissanceStandardDatasets.GetStandardDataset(RenaissanceChangeID.tblSelectTransaction)
			If (TradesDataset.NotedIDsHaveChanged) Then
				MainForm.Load_Table(TradesDataset, True)
				Call SetSortedRows()
			End If
		Catch ex As Exception
			MainForm.LogError("ShowTradeRevisionBlotter()", LOG_LEVELS.Error, ex.Message, "Error loading tblSelectTransaction", ex.StackTrace, True)
			Exit Sub
		End Try

		Try
			If (pFinal) Then
				If MessageBox.Show("Are you sure that you want to run a FINAL blotter ?" & vbCrLf & "This will increment the Ticker number.", "Are you sure ?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) <> Windows.Forms.DialogResult.Yes Then
					Exit Sub
				End If
			End If

			Call MainForm.MainReportHandler.rptTradeRevisionReport(myDataView, pFinal, Form_ProgressBar)

		Catch ex As Exception
			MainForm.LogError("ShowTradeBlotter()", LOG_LEVELS.Error, ex.Message, "Error getting header information.", ex.StackTrace, True)
			Exit Sub
		End Try


	End Sub

    ''' <summary>
    ''' RPTs the FX blotter.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Private Sub rptFXBlotter(ByVal sender As Object, ByVal e As EventArgs)
		Call ShowFXBlotter(False)
	End Sub

    ''' <summary>
    ''' RPTs the final FX blotter.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Private Sub rptFinalFXBlotter(ByVal sender As Object, ByVal e As EventArgs)
		Call ShowFXBlotter(True)
	End Sub

    ''' <summary>
    ''' Shows the FX blotter.
    ''' </summary>
    ''' <param name="pFinal">if set to <c>true</c> [p final].</param>
	Private Sub ShowFXBlotter(Optional ByVal pFinal As Boolean = False)
		Dim TradesDataset As StandardDataset
		Dim rptTradesDS As New RenaissanceDataClass.DSSelectTransaction
		Dim rptTradesTbl As RenaissanceDataClass.DSSelectTransaction.tblSelectTransactionDataTable
		Dim TradesRow As RenaissanceDataClass.DSSelectTransaction.tblSelectTransactionRow
		Dim thisDataRow As DataRow
		Dim reportDataView As DataView


		Dim TradeBlotterTicketNumber As String
		Dim ColumnCounter As Integer
		Dim RowCounter As Integer
		Dim Counter As Integer
		Dim ParentIDList As String

		rptTradesTbl = rptTradesDS.tblSelectTransaction

		' Build Source Table

		Try
			TradesDataset = RenaissanceStandardDatasets.GetStandardDataset(RenaissanceChangeID.tblSelectTransaction)
			If (TradesDataset.NotedIDsHaveChanged) Then
				MainForm.Load_Table(TradesDataset, True)
				Call SetSortedRows()
			End If
		Catch ex As Exception
			MainForm.LogError("ShowFXTradeBlotter()", LOG_LEVELS.Error, ex.Message, "Error loading tblSelectTransaction", ex.StackTrace, True)
			Exit Sub
		End Try

		' Derive a DataView from the reference table, further refining the selection to exclude non-FX
		' Transactions.

    reportDataView = New DataView(myTable, "((TransactionType=" & TransactionTypes.BuyFX & ") OR (TransactionType=" & TransactionTypes.SellFX & ") OR (TransactionType=" & TransactionTypes.BuyFXForward & ") OR (TransactionType=" & TransactionTypes.SellFXForward & ")) AND(" & GetTransactionSelectString() & ")", "", DataViewRowState.CurrentRows)

		ParentIDList = ""
		If (reportDataView.Count <= 0) Then
			MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "No FX Transactions Selected.", "", True)
			Exit Sub
		End If

		' Build a list of selected transaction Parent IDs and copy the DataRecords to a new Table.
		' A new table is used so as not to screw up the original table when we add the USD_Settle column.

		For Counter = 0 To (reportDataView.Count - 1)
			ParentIDList &= reportDataView(Counter)("TransactionParentID").ToString
			If Counter < (reportDataView.Count - 1) Then
				ParentIDList &= ","
			End If

			' Copy SourceDataView to rptTradesTbl
			TradesRow = rptTradesTbl.NewtblSelectTransactionRow
			For ColumnCounter = 0 To (rptTradesTbl.Columns.Count - 1)
				TradesRow(ColumnCounter) = reportDataView(Counter)(ColumnCounter)
			Next
			rptTradesTbl.Rows.Add(TradesRow)
		Next

		' Get SecondLeg Settlement values
		' 

		Dim SecondLegTable As New DataTable

		Dim tempAdaptor As New SqlDataAdapter
		Dim tempCommand As New SqlCommand
		tempCommand.Connection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)
		tempCommand.CommandType = CommandType.Text
		tempCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

		tempCommand.CommandText = "SELECT RN, TransactionParentID, TransactionSettlement FROM fn_tblTransaction_SelectKD(@KnowledgeDate) WHERE (TransactionLeg = 2) AND (TransactionParentID IN (" & ParentIDList & "))"
		tempCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))
		tempCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
		tempAdaptor.SelectCommand = tempCommand
		SyncLock tempCommand.Connection
			tempAdaptor.Fill(SecondLegTable)
		End SyncLock

		' Add USD_Settle Column
		' and populate it with the Signed Settlment from the Second Transaction Leg.

		rptTradesTbl.Columns.Add(New DataColumn("USD_Settle", GetType(Double)))

		For Counter = 0 To (rptTradesTbl.Rows.Count - 1)
			thisDataRow = rptTradesTbl.Rows(Counter)
			thisDataRow("USD_Settle") = 0

			For RowCounter = 0 To (SecondLegTable.Rows.Count - 1)
				If (CInt(thisDataRow("TransactionParentID")) = CInt(SecondLegTable.Rows(RowCounter)("TransactionParentID"))) Then
					thisDataRow("USD_Settle") = SecondLegTable.Rows(RowCounter)("TransactionSettlement")
					Exit For
				End If
			Next
		Next

		' Get Ticket Number

		Try
			TradeBlotterTicketNumber = MainForm.Get_SystemString("FXBlotterTicketNumber")

			If IsNumeric(TradeBlotterTicketNumber) = False Then
				TradeBlotterTicketNumber = "000001"
				MainForm.Set_SystemString("FXBlotterTicketNumber", TradeBlotterTicketNumber)
			ElseIf pFinal Then
				If MessageBox.Show("Are you sure that you want to run a FINAL blotter ?" & vbCrLf & "This will increment the Ticker number.", "Are you sure ?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
					TradeBlotterTicketNumber = (CInt(TradeBlotterTicketNumber) + 1).ToString("000000")
					MainForm.Set_SystemString("FXBlotterTicketNumber", TradeBlotterTicketNumber)
				Else
					Exit Sub
				End If
			End If

		Catch ex As Exception
			MainForm.LogError("ShowFXTradeBlotter()", LOG_LEVELS.Error, ex.Message, "Error getting header information.", ex.StackTrace, True)
			Exit Sub
		End Try

		' Display Report

		Call MainForm.MainReportHandler.rptFXTradeBlotterReport(TradeBlotterTicketNumber, rptTradesTbl, pFinal, Form_ProgressBar)

	End Sub

#End Region

#Region " Bug hunting "

  ' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
  ' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
  ' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

  ''' <summary>
  ''' Called when [row updating].
  ''' </summary>
  ''' <param name="Sender">The sender.</param>
  ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  ''' <summary>
  ''' Called when [row updated].
  ''' </summary>
  ''' <param name="Sender">The sender.</param>
  ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  ''' <summary>
  ''' Called when [row fill error].
  ''' </summary>
  ''' <param name="Sender">The sender.</param>
  ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
    Dim a As Integer

    a = 1
  End Sub

#End Region

  ''' <summary>
  ''' Class TransactionSummaryClass
  ''' </summary>

  Private Class TransactionSummaryClass
    ''' <summary>
    ''' The fund ID
    ''' </summary>
    Public FundID As Integer
    ''' <summary>
    ''' The fund name
    ''' </summary>
    Public FundName As String
    ''' <summary>
    ''' The instrument ID
    ''' </summary>
    Public InstrumentID As Integer
    ''' <summary>
    ''' The instrument description
    ''' </summary>
    Public InstrumentDescription As String
    ''' <summary>
    ''' The parent ID
    ''' </summary>
    Public ParentID As Integer
    ''' <summary>
    ''' The counter
    ''' </summary>
    Public Counter As Integer
    ''' <summary>
    ''' The instrument position
    ''' </summary>
    Public InstrumentPosition As Double
    ''' <summary>
    ''' The instrument value
    ''' </summary>
    Public InstrumentValue As Double

    ''' <summary>
    ''' Initializes a new instance of the <see cref="TransactionSummaryClass"/> class.
    ''' </summary>
    Public Sub New()
      FundID = 0
      FundName = ""
      InstrumentID = 0
      InstrumentDescription = ""
      ParentID = 0
      Counter = 0
      InstrumentPosition = 0
      InstrumentValue = 0
    End Sub

  End Class

    ''' <summary>
    ''' Paints the transaction grid.
    ''' </summary>

  Private Sub PaintTransactionGrid()
    ' **********************************************************************************
    ' Routine to Paint (In Outline) the Transactions Grid.
    '
    ' This Routine populates the grid with the appropriate heirarchy of Funds and Instruments.
    ' To improve performance, the Transaction lines are not added, they are in-filled as 
    ' Instrument lines are expanded.
    '
    ' **********************************************************************************

    Dim Counter As Integer
    Dim thisRowView As DataRowView
    Dim thisRow As RenaissanceDataClass.DSSelectTransaction.tblSelectTransactionRow

    Dim AggregateToParent As Boolean = False

    Dim LastFund As Integer
    Dim LastInstrument As Integer
    Dim thisInstrumentID As Integer
    Dim thisPrice As Double

    ' Dim LastInstrumentLine As Integer

    Dim InstrumentPosition As Double
    Dim InstrumentValue As Double

    Try
      Grid_Transactions.Redraw = False

      Try
        ' Clear Grid, to start
        Grid_Transactions.Rows.Count = 1

        ' Add Header Line.
        Grid_Transactions.Rows.Add()
        Grid_Transactions(Grid_Transactions.Rows.Count - 1, GridColumns.FirstCol) = "Primonial"
        Grid_Transactions.Rows(Grid_Transactions.Rows.Count - 1).IsNode = True
        Grid_Transactions.Rows(Grid_Transactions.Rows.Count - 1).Node.Level = 0

      Catch ex As Exception
      End Try

      InPaint = True

      ' Initialise tracking variables.
      LastFund = (-1)
      LastInstrument = (-1)
      ' LastInstrumentLine = (-1)
      InstrumentPosition = 0
      InstrumentValue = 0

      AggregateToParent = Me.Check_AggregateToParent.Checked

      ' NPP - 29 Nov 2007
      ' Rewrote this section of code to fix a couple of bugs.
      ' Now build an array of Instruments and add them to the array in one pass rather than conditionally deleting
      ' previously added lines which seems to have had some odd effects.

      Dim InstrumentList As New ArrayList
      Dim thisInstrument As TransactionSummaryClass = Nothing
      Dim NewGridRow As C1.Win.C1FlexGrid.Row

      Try

        ' Loop through the selected data (DataView) adding Instruments to the ArrayList.

        For Counter = 0 To (myDataView.Count - 1)

          ' Resolve Transaction Row.
          thisRowView = myDataView.Item(Counter)
          thisRow = thisRowView.Row

          If AggregateToParent Then
            thisInstrumentID = thisRow.InstrumentParent
          Else
            thisInstrumentID = thisRow.TransactionInstrument
          End If

          ' Add a New Fund Row if this transaction represents the start of a new Fund or Instrument
          If (thisRow.TransactionFund <> LastFund) OrElse (thisInstrumentID <> LastInstrument) Then
            thisInstrument = New TransactionSummaryClass

            thisInstrument.FundID = thisRow.TransactionFund
            thisInstrument.FundName = thisRow.FundName
            thisInstrument.InstrumentID = thisInstrumentID
            thisInstrument.InstrumentDescription = thisRow.InstrumentDescription
            thisInstrument.ParentID = thisRow.InstrumentParent
            thisInstrument.Counter = Counter
            thisInstrument.InstrumentPosition = 0
            thisInstrument.InstrumentValue = 0

            InstrumentList.Add(thisInstrument)

          End If

          thisInstrument.InstrumentPosition += (thisRow.TransactionSignedUnits * thisRow.InstrumentParentEquivalentRatio)

          thisPrice = GetInstrumentPrice(thisRow.TransactionInstrument)
          thisInstrument.InstrumentValue += (thisRow.TransactionSignedUnits * thisRow.InstrumentContractSize * thisPrice)

          LastFund = thisRow.TransactionFund
          LastInstrument = thisInstrumentID

        Next

        ' Now add the Instruments to the Grid.

        LastFund = (-1)
        LastInstrument = (-1)

        For Counter = 0 To (InstrumentList.Count - 1)
          thisInstrument = InstrumentList(Counter)  '  TransactionSummaryClass

          ' Add a New Fund Row if this transaction represents the start of a new Fund.

          If (LastFund <> thisInstrument.FundID) Then

            NewGridRow = Grid_Transactions.Rows.Add()
            NewGridRow(GridColumns.FirstCol) = thisInstrument.FundName
            NewGridRow.IsNode = True
            NewGridRow.Node.Level = 1

            NewGridRow(GridColumns.ParentID) = (-1)
            NewGridRow(GridColumns.FundID) = thisInstrument.FundID
            NewGridRow(GridColumns.Counter) = (-1)  ' Null Counter, Dont Add lines when the 'Fund' line is expanded.

          End If

          ' Add Instrument Line

          If (Check_OmitZeroBalances.Checked = False) OrElse (Math.Abs(thisInstrument.InstrumentPosition) > 0.0001) Then

            NewGridRow = Grid_Transactions.Rows.Add()

            If AggregateToParent Then
              NewGridRow(GridColumns.FirstCol) = GetInstrumentDescription(thisInstrument.InstrumentID)
            Else
              NewGridRow(GridColumns.FirstCol) = thisInstrument.InstrumentDescription
            End If

            ' Fill in other Instrument details

            NewGridRow.IsNode = True
            NewGridRow.Node.Level = 2

            NewGridRow(GridColumns.ParentID) = 0   ' thisRow.TransactionParentID
            NewGridRow(GridColumns.FundID) = thisInstrument.FundID   ' FundID
            NewGridRow(GridColumns.InstrumentID) = thisInstrument.InstrumentID
            NewGridRow(GridColumns.Counter) = thisInstrument.Counter
            NewGridRow(GridColumns.Units) = thisInstrument.InstrumentPosition
            NewGridRow(GridColumns.Value) = thisInstrument.InstrumentValue

            If thisInstrument.InstrumentPosition < 0 Then
              Grid_Transactions.SetCellStyle(NewGridRow.SafeIndex, GridColumns.Units, Grid_Transactions.Styles("FundPositionNegative"))
            Else
              Grid_Transactions.SetCellStyle(NewGridRow.SafeIndex, GridColumns.Units, Grid_Transactions.Styles("FundPositionPositive"))
            End If

            If thisInstrument.InstrumentValue < 0 Then
              Grid_Transactions.SetCellStyle(NewGridRow.SafeIndex, GridColumns.Value, Grid_Transactions.Styles("FundPositionNegative"))
            Else
              Grid_Transactions.SetCellStyle(NewGridRow.SafeIndex, GridColumns.Value, Grid_Transactions.Styles("FundPositionPositive"))
            End If

            ' Add Dummy Row - Facilitates in-fill later.

            NewGridRow = Grid_Transactions.Rows.Add()
            NewGridRow(GridColumns.FirstCol) = ""
            NewGridRow.IsNode = False

          End If

          LastFund = thisInstrument.FundID
          LastInstrument = thisInstrument.InstrumentID

        Next

        ' Now Colapse and expand rows as necessary.

        For Counter = (Grid_Transactions.Rows.Count - 1) To 1 Step -1
          Try
            If (Grid_Transactions.Rows(Counter).IsNode) AndAlso (Grid_Transactions.Rows(Counter).Node.Level >= 1) Then
              Grid_Transactions.Rows(Counter).Node.Expanded = False
            End If
          Catch Inner_Ex As Exception
          End Try
        Next

        For Counter = (Grid_Transactions.Rows.Count - 1) To 1 Step -1
          Try
            If (Grid_Transactions.Rows(Counter).IsNode) AndAlso (Grid_Transactions.Rows(Counter).Node.Level = 1) Then
              If (CInt(Grid_Transactions(Counter, GridColumns.FundID)) = LastExpandedFund) AndAlso (Grid_Transactions.Rows(Counter).Node.Level = 1) Then
                Grid_Transactions.Rows(Counter).Node.Expanded = True
              End If
            End If
          Catch Inner_Ex As Exception
          End Try
        Next

        For Counter = (Grid_Transactions.Rows.Count - 1) To 1 Step -1
          Try
            If (Grid_Transactions.Rows(Counter).IsNode) AndAlso (Grid_Transactions.Rows(Counter).Node.Level = 2) Then
              If (CInt(Grid_Transactions(Counter, GridColumns.FundID)) = LastExpandedFund) AndAlso (CInt(Grid_Transactions(Counter, GridColumns.InstrumentID)) = LastExpandedInstrument) Then
                Grid_Transactions.Rows(Counter).Node.Expanded = True
              End If
            End If
          Catch Inner_Ex As Exception
          End Try
        Next

      Catch ex As Exception
      Finally
        InstrumentList.Clear()
      End Try


      'Try

      '	' Loop through the selected data (DataView) adding Fund and Instrument rows as appropriate.
      '	For Counter = 0 To (myDataView.Count - 1)

      '		Try

      '			' Resolve Transaction Row.
      '			thisRowView = myDataView.Item(Counter)
      '			thisRow = thisRowView.Row

      '			If AggregateToParent Then
      '				thisInstrumentID = thisRow.InstrumentParent
      '			Else
      '				thisInstrumentID = thisRow.TransactionInstrument
      '			End If

      '			' Add a New Fund Row if this transaction represents the start of a new Fund.
      '			If thisRow.TransactionFund <> LastFund Then

      '				Grid_Transactions.Rows.Add()
      '				Grid_Transactions(Grid_Transactions.Rows.Count - 1, GridColumns.FirstCol) = thisRow.FundName.ToString
      '				Grid_Transactions.Rows(Grid_Transactions.Rows.Count - 1).IsNode = True
      '				Grid_Transactions.Rows(Grid_Transactions.Rows.Count - 1).Node.Level = 1
      '				Grid_Transactions.Rows(Grid_Transactions.Rows.Count - 1).Node.Collapsed = True

      '				Grid_Transactions(Grid_Transactions.Rows.Count - 1, GridColumns.ParentID) = (-1)
      '				Grid_Transactions(Grid_Transactions.Rows.Count - 1, GridColumns.FundID) = thisRow.TransactionFund	 ' FundID
      '				Grid_Transactions(Grid_Transactions.Rows.Count - 1, GridColumns.Counter) = (-1)	 ' Null Counter, Dont Add lines when the 'Fund' line is expanded.

      '				LastInstrument = (-1)
      '			End If

      '			' Add a New Instrument Row if this transaction represents the start of a new Instrument.
      '			If thisInstrumentID <> LastInstrument Then
      '				' BackFill Position totals on Instrument Lines
      '				' Style FundPosition
      '				If (LastInstrumentLine >= 0) Then
      '					If (Me.Check_OmitZeroBalances.Checked) AndAlso (Math.Abs(InstrumentPosition) < 0.0001) Then
      '						' Remove rows relating to the Instrument with Zero Aggregate Balance

      '						Grid_Transactions.Rows.RemoveRange(LastInstrumentLine, 2)

      '						' Grid_Transactions.Rows.Count = Grid_Transactions.Rows.Count - 2
      '					Else
      '						' Backfill Summary Values
      '						Grid_Transactions(LastInstrumentLine, GridColumns.Units) = InstrumentPosition
      '						Grid_Transactions(LastInstrumentLine, GridColumns.Value) = InstrumentValue

      '						If InstrumentPosition < 0 Then
      '							Grid_Transactions.SetCellStyle(LastInstrumentLine, GridColumns.Units, Grid_Transactions.Styles("FundPositionNegative"))
      '						Else
      '							Grid_Transactions.SetCellStyle(LastInstrumentLine, GridColumns.Units, Grid_Transactions.Styles("FundPositionPositive"))
      '						End If

      '						If InstrumentValue < 0 Then
      '							Grid_Transactions.SetCellStyle(LastInstrumentLine, GridColumns.Value, Grid_Transactions.Styles("FundPositionNegative"))
      '						Else
      '							Grid_Transactions.SetCellStyle(LastInstrumentLine, GridColumns.Value, Grid_Transactions.Styles("FundPositionPositive"))
      '						End If
      '					End If

      '				End If

      '				' Add New Instrument line

      '				Grid_Transactions.Rows.Add()

      '				' Get Instrument description.
      '				' If not Aggregating then use the supplied description, If Aggregating, then get the Parent Instrument description.

      '				If AggregateToParent Then
      '					Grid_Transactions(Grid_Transactions.Rows.Count - 1, GridColumns.FirstCol) = GetInstrumentDescription(thisInstrumentID)
      '				Else
      '					Grid_Transactions(Grid_Transactions.Rows.Count - 1, GridColumns.FirstCol) = thisRow.InstrumentDescription.ToString
      '				End If

      '				' Fill in other Instrument details

      '				Grid_Transactions.Rows(Grid_Transactions.Rows.Count - 1).IsNode = True
      '				Grid_Transactions.Rows(Grid_Transactions.Rows.Count - 1).Node.Level = 2

      '				Grid_Transactions(Grid_Transactions.Rows.Count - 1, GridColumns.ParentID) = 0	 ' thisRow.TransactionParentID
      '				Grid_Transactions(Grid_Transactions.Rows.Count - 1, GridColumns.FundID) = thisRow.TransactionFund	 ' FundID
      '				Grid_Transactions(Grid_Transactions.Rows.Count - 1, GridColumns.InstrumentID) = thisInstrumentID
      '				Grid_Transactions(Grid_Transactions.Rows.Count - 1, GridColumns.Counter) = Counter

      '				InstrumentPosition = 0
      '				InstrumentValue = 0
      '				LastInstrumentLine = Grid_Transactions.Rows.Count - 1

      '				' Add Dummy Row - Facilitates in-fill later.

      '				Grid_Transactions.Rows.Add()
      '				Grid_Transactions(Grid_Transactions.Rows.Count - 1, GridColumns.FirstCol) = ""
      '				Grid_Transactions.Rows(Grid_Transactions.Rows.Count - 1).IsNode = False

      '			End If

      '			InstrumentPosition += (thisRow.TransactionSignedUnits * thisRow.InstrumentParentEquivalentRatio)

      '			thisPrice = GetInstrumentPrice(thisRow.TransactionInstrument)
      '			InstrumentValue += (thisRow.TransactionSignedUnits * thisPrice)

      '			LastFund = thisRow.TransactionFund
      '			LastInstrument = thisInstrumentID

      '		Catch Inner_Ex As Exception

      '		End Try
      '	Next Counter

      '	' Update Final Instrument Position.

      '	If (LastInstrumentLine >= 0) Then
      '		If (Me.Check_OmitZeroBalances.Checked) AndAlso (Math.Abs(InstrumentPosition) < 0.0001) Then
      '			' Remove rows relating to the Instrument with Zero Aggregate Balance

      '			Grid_Transactions.Rows.RemoveRange(LastInstrumentLine, 2)

      '			' Grid_Transactions.Rows.Count = Grid_Transactions.Rows.Count - 2
      '		Else
      '			' Backfill Summary Values
      '			Grid_Transactions(LastInstrumentLine, GridColumns.Units) = InstrumentPosition
      '			Grid_Transactions(LastInstrumentLine, GridColumns.Value) = InstrumentValue

      '			If InstrumentPosition < 0 Then
      '				Grid_Transactions.SetCellStyle(LastInstrumentLine, GridColumns.Units, Grid_Transactions.Styles("FundPositionNegative"))
      '			Else
      '				Grid_Transactions.SetCellStyle(LastInstrumentLine, GridColumns.Units, Grid_Transactions.Styles("FundPositionPositive"))
      '			End If

      '			If InstrumentValue < 0 Then
      '				Grid_Transactions.SetCellStyle(LastInstrumentLine, GridColumns.Value, Grid_Transactions.Styles("FundPositionNegative"))
      '			Else
      '				Grid_Transactions.SetCellStyle(LastInstrumentLine, GridColumns.Value, Grid_Transactions.Styles("FundPositionPositive"))
      '			End If
      '		End If
      '	End If

      '	' Collapse rows.
      '	' Leave uncolapsed the section last opened by the user.

      '	For Counter = (Grid_Transactions.Rows.Count - 1) To 1 Step -1
      '		Try
      '			If Grid_Transactions.Rows(Counter).IsNode Then
      '				If Grid_Transactions.Rows(Counter).Node.Level > 0 Then
      '					If (CInt(Grid_Transactions(Counter, GridColumns.FundID)) = LastExpandedFund) AndAlso (Grid_Transactions.Rows(Counter).Node.Level = 1) Then
      '						Grid_Transactions.Rows(Counter).Node.Expanded = True
      '					ElseIf (CInt(Grid_Transactions(Counter, GridColumns.FundID)) = LastExpandedFund) AndAlso (CInt(Grid_Transactions(Counter, GridColumns.InstrumentID)) = LastExpandedInstrument) AndAlso (Grid_Transactions.Rows(Counter).Node.Level = 2) Then
      '						Grid_Transactions.Rows(Counter).Node.Expanded = True

      '						m_ExpandingNode = True
      '						ExpandRow(Counter)
      '						m_ExpandingNode = False
      '					Else
      '						Grid_Transactions.Rows(Counter).Node.Expanded = False
      '					End If

      '				End If
      '			End If
      '		Catch Inner_Ex As Exception
      '		End Try
      '	Next

      'Catch ex As Exception
      'End Try

    Catch ex As Exception
    Finally
      Grid_Transactions.Redraw = True
      Grid_Transactions.Refresh()
      InPaint = False
    End Try

  End Sub

    ''' <summary>
    ''' The m_ expanding node
    ''' </summary>
	Private m_ExpandingNode As Boolean = False
    ''' <summary>
    ''' The last expanded fund
    ''' </summary>
	Private LastExpandedFund As Integer = 0
    ''' <summary>
    ''' The last expanded instrument
    ''' </summary>
	Private LastExpandedInstrument As Integer = 0

    ''' <summary>
    ''' Handles the BeforeCollapse event of the Grid_Transactions control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="C1.Win.C1FlexGrid.RowColEventArgs"/> instance containing the event data.</param>
	Private Sub Grid_Transactions_BeforeCollapse(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles Grid_Transactions.BeforeCollapse
		' **********************************************************************************
		' Handle the expanding of Instrument sections.
		'
		'
		' **********************************************************************************

		' if we're working, ignore this
		If m_ExpandingNode Then
			Return
		End If

		Try

			' don't allow Collapsing/expanding groups of rows (with shift-click)
			If e.Row < 0 Then
				e.Cancel = True
				Return
			End If

			' if we're already collapsed, populate node
			' before expanding
			If (Grid_Transactions.Rows(e.Row).IsNode) AndAlso (Grid_Transactions.Rows(e.Row).Node.Collapsed) Then
				Try
					m_ExpandingNode = True
					ExpandRow(e.Row)
				Catch ex As Exception
				Finally
					m_ExpandingNode = False
				End Try
			Else
				If InPaint = False Then
					LastExpandedFund = 0
					LastExpandedInstrument = 0
				End If
			End If

		Catch ex As Exception
		End Try

	End Sub

    ''' <summary>
    ''' Handles the AfterDragColumn event of the Grid_Transactions control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="C1.Win.C1FlexGrid.DragRowColEventArgs"/> instance containing the event data.</param>
	Private Sub Grid_Transactions_AfterDragColumn(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.DragRowColEventArgs) Handles Grid_Transactions.AfterDragColumn
		' **********************************************************************************
		' Handle the Moving of Grid columns
		'
		' Simply re-set the values in the GridColumns class
		'
		' **********************************************************************************
		Call Init_GridColumnsClass()
	End Sub

    ''' <summary>
    ''' Expands the row.
    ''' </summary>
    ''' <param name="RowNumber">The row number.</param>
	Private Sub ExpandRow(ByVal RowNumber As Integer)
		' **********************************************************************************
		' Process the expanding of Instrument sections.
		'
		' Inserts the appropriate transaction lines under an Instrument group.
		' Use the 'Counter' column to determine whether in-fill has already occurred.
		' 'Counter' value < 0 indicates in-fill has occurred.
		'
		' The 'Counter' column will contain the Index reference in the DataView at which transactions
		' relating to the selected Instrument & Fund are located
		' The List and Counter values are updated each time the Dataview is updated in order to keep
		' these value in-sync.
		'
		' **********************************************************************************
		Dim Counter As Integer
		Dim thisRowView As DataRowView
		Dim thisRow As RenaissanceDataClass.DSSelectTransaction.tblSelectTransactionRow

		Dim FundID As Integer
		Dim InstrumentID As Integer
		Dim StartCounter As Integer

		Dim thisInstrumentID As Integer
		Dim thisPrice As Double
		Dim InstrumentValue As Double

		Dim FirstLine As Boolean = True
		Dim UpdateRow As Integer = RowNumber + 1

		Dim AggregateToParent As Boolean

		AggregateToParent = Me.Check_AggregateToParent.Checked

		Try
			' Get Fund and InstrumentIDs

			FundID = CInt(Grid_Transactions.Item(RowNumber, GridColumns.FundID))
			InstrumentID = CInt(Grid_Transactions.Item(RowNumber, GridColumns.InstrumentID))
			StartCounter = CInt(Grid_Transactions.Item(RowNumber, GridColumns.Counter))

			LastExpandedFund = FundID
			LastExpandedInstrument = InstrumentID

			' Are the IDs OK, has In-Fill already occured ?

			If (StartCounter >= 0) AndAlso (FundID > 0) AndAlso (InstrumentID > 0) Then

				' Mark Instrument Group Line as having been filled : Set Counter to Negative
				Grid_Transactions(RowNumber, GridColumns.Counter) = (-1)

				' Validate StartCounter
				If (StartCounter > (myDataView.Count - 1)) Then
					Exit Sub
				End If

				' Add Lines.
				For Counter = StartCounter To (myDataView.Count - 1)

					' Get Transaction Row.
					thisRowView = myDataView.Item(Counter)
					thisRow = thisRowView.Row

					' Select appropriate InstrumentID to group by. (Parent ID for Aggregated)
					If AggregateToParent Then
						thisInstrumentID = thisRow.InstrumentParent
					Else
						thisInstrumentID = thisRow.TransactionInstrument
					End If

					' If this Transaction is appropriate to the selected group....

					If (thisRow.TransactionFund = FundID) AndAlso (thisInstrumentID = InstrumentID) Then

						' If this is the first transaction, then overwrite the 'Dummy' grid line, else insert
						' a new grid line.

						If (FirstLine = False) Then
							' Add Line
							UpdateRow += 1

							Grid_Transactions.Rows.Insert(UpdateRow)
							Grid_Transactions.Rows(UpdateRow).IsNode = False
						End If

						' Set Values / Styles

						If (AggregateToParent = False) Then
							Grid_Transactions(UpdateRow, GridColumns.FirstCol) = thisRow.TransactionTicket.ToString
						Else
							Grid_Transactions(UpdateRow, GridColumns.FirstCol) = thisRow.InstrumentDescription.ToString
						End If

						Grid_Transactions(UpdateRow, GridColumns.ValueDate) = thisRow.TransactionValueDate
						Grid_Transactions.SetCellStyle(UpdateRow, GridColumns.ValueDate, Grid_Transactions.Styles("Data"))
						Grid_Transactions(UpdateRow, GridColumns.Type) = thisRow.TransactionTypeName.ToString
						Grid_Transactions.SetCellStyle(UpdateRow, GridColumns.Type, Grid_Transactions.Styles("Data"))

						Grid_Transactions(UpdateRow, GridColumns.Units) = thisRow.TransactionUnits
						If (thisRow.TransactionSignedUnits < 0) Then
							Grid_Transactions.SetCellStyle(UpdateRow, GridColumns.Units, Grid_Transactions.Styles("DataNegative"))
						Else
							Grid_Transactions.SetCellStyle(UpdateRow, GridColumns.Units, Grid_Transactions.Styles("Data"))
						End If

						Grid_Transactions(UpdateRow, GridColumns.Price) = thisRow.TransactionPrice
						Grid_Transactions.SetCellStyle(UpdateRow, GridColumns.Price, Grid_Transactions.Styles("Data"))
						Grid_Transactions(UpdateRow, GridColumns.Settlement) = thisRow.TransactionSettlement
						Grid_Transactions.SetCellStyle(UpdateRow, GridColumns.Settlement, Grid_Transactions.Styles("Data"))
						Grid_Transactions(UpdateRow, GridColumns.ParentID) = thisRow.TransactionParentID
						Grid_Transactions.SetCellStyle(UpdateRow, GridColumns.ParentID, Grid_Transactions.Styles("Data"))
						Grid_Transactions(UpdateRow, GridColumns.FundID) = thisRow.TransactionFund
						Grid_Transactions(UpdateRow, GridColumns.InstrumentID) = thisRow.TransactionInstrument
						Grid_Transactions(UpdateRow, GridColumns.Counter) = 0

						thisPrice = GetInstrumentPrice(thisRow.TransactionInstrument)
						InstrumentValue = (thisRow.TransactionSignedUnits * thisRow.InstrumentContractSize * thisPrice)

						Grid_Transactions(UpdateRow, GridColumns.LastPrice) = thisPrice
						Grid_Transactions.SetCellStyle(UpdateRow, GridColumns.LastPrice, Grid_Transactions.Styles("Data"))
						Grid_Transactions(UpdateRow, GridColumns.Value) = InstrumentValue
						If (InstrumentValue < 0) Then
							Grid_Transactions.SetCellStyle(UpdateRow, GridColumns.Value, Grid_Transactions.Styles("DataNegative"))
						Else
							Grid_Transactions.SetCellStyle(UpdateRow, GridColumns.Value, Grid_Transactions.Styles("Data"))
						End If

						FirstLine = False
					Else
						' If the current transaction does not match, and we have added some lines already, then
						' assume that we have come to the end of the section and exit.
						' This logic works on the assumption that the DataView is ordered correctly by FundID and (Parent)InstrumentID. 

						If (FirstLine = False) Then
							' If we have worked through the required Fund / Instrument, then exit.
							Exit Sub
						End If

					End If

				Next Counter
			End If

		Catch ex As Exception

		End Try

	End Sub

    ''' <summary>
    ''' Gets the instrument description.
    ''' </summary>
    ''' <param name="InstrumentID">The instrument ID.</param>
    ''' <returns>System.String.</returns>
	Private Function GetInstrumentDescription(ByVal InstrumentID As Integer) As String

		Static LastInstrumentID As Integer = 0
		Static LastInstrumentDescription As String = ""

		Try
			If (InstrumentID = LastInstrumentID) OrElse (InstrumentID <= 0) Then
				If (InstrumentID <= 0) Then	' Facilitate the explicit clearing of Static variables
					LastInstrumentID = 0
					LastInstrumentDescription = ""
				End If

				Return LastInstrumentDescription
			End If
		Catch ex As Exception
			Return ""
		End Try

		Try
			If (InstrumentsDS Is Nothing) Then
				InstrumentsDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblInstrument, False)
			End If

			If (InstrumentsDS Is Nothing) Then
				LastInstrumentID = 0
				LastInstrumentDescription = ""
				Return ""
				Exit Function
			End If
		Catch ex As Exception
			LastInstrumentID = 0
			LastInstrumentDescription = ""
			Return ""
			Exit Function
		End Try

		Try
			If (InstrumentsDS Is Nothing) Then
				LastInstrumentID = 0
				LastInstrumentDescription = ""
				Return ""
				Exit Function
			Else

				Dim SelectedRows As RenaissanceDataClass.DSInstrument.tblInstrumentRow()
				SelectedRows = InstrumentsDS.tblInstrument.Select("InstrumentID=" & InstrumentID.ToString)

				If (SelectedRows.Length > 0) Then
					LastInstrumentID = InstrumentID
					LastInstrumentDescription = SelectedRows(0).InstrumentDescription
					Return LastInstrumentDescription
				End If
			End If
		Catch ex As Exception
		End Try

		LastInstrumentID = 0
		LastInstrumentDescription = ""
		Return ""
		Exit Function

	End Function

    ''' <summary>
    ''' Gets the instrument price.
    ''' </summary>
    ''' <param name="InstrumentID">The instrument ID.</param>
    ''' <returns>System.Double.</returns>
	Private Function GetInstrumentPrice(ByVal InstrumentID As Integer) As Double
		' ******************************************************************************************************
		'
		' Return the Latest Price for a given Instrument from the sorted array of
		' Instrument prices.
		'
		' ******************************************************************************************************

		Static LastInstrumentID As Integer = 0
		Static LastInstrumentPrice As Double = 0

		Try
			If (InstrumentID = LastInstrumentID) OrElse (InstrumentID <= 0) Then
				If (InstrumentID <= 0) Then	' Facilitate the explicit clearing of Static variables
					LastInstrumentID = 0
					LastInstrumentPrice = 0
				End If

				Return LastInstrumentPrice
			End If
		Catch ex As Exception
		End Try

		Try

			' Check the Prices table is loaded and the sorted Prices array is established

			If (BestPricesTable Is Nothing) Then
				BestPricesTable = New DataTable
				Dim thisCommand As New SqlCommand

				Try

					thisCommand.CommandType = CommandType.Text
					thisCommand.CommandText = "SELECT InstrumentID, PriceDate, PriceLevel FROM dbo.fn_BestPrice(@ValueDate, @OnlyFinalPrices, @KnowledgeDate)"
					thisCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

					thisCommand.Parameters.Add(New SqlParameter("@ValueDate", SqlDbType.DateTime)).Value = RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW
					thisCommand.Parameters.Add(New SqlParameter("@OnlyFinalPrices", SqlDbType.Int)).Value = 0
					thisCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", SqlDbType.DateTime)).Value = MainForm.Main_Knowledgedate

					thisCommand.Connection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)

					If (thisCommand IsNot Nothing) AndAlso (thisCommand.Connection IsNot Nothing) Then
						SyncLock thisCommand.Connection
              MainForm.LoadTable_Custom(BestPricesTable, thisCommand)
              'BestPricesTable.Load(thisCommand.ExecuteReader)
						End SyncLock
					End If

					SortedPrices = BestPricesTable.Select("True", "InstrumentID, PriceDate")

				Catch ex As Exception

					MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error Getting Best Prices.", ex.Message, ex.StackTrace, True)

					If (BestPricesTable IsNot Nothing) Then
						BestPricesTable.Clear()
					End If

					BestPricesTable = Nothing
					SortedPrices = Nothing

				Finally

					If (thisCommand IsNot Nothing) Then
						thisCommand.Connection = Nothing
					End If
					thisCommand = Nothing

				End Try

			End If

			If (BestPricesTable Is Nothing) OrElse (SortedPrices.Length <= 0) Then
				LastInstrumentID = 0
				LastInstrumentPrice = 0
				Return 0
				Exit Function
			End If

		Catch ex As Exception
			LastInstrumentID = 0
			LastInstrumentPrice = 0
			Return 0
			Exit Function
		End Try

		' Find Latest Price

		' Code changed to use the fn_BestPrice function , thus there is only One Price per instrument.

		Dim FirstIndex As Integer
		Dim MidIndex As Integer
		Dim LastIndex As Integer
		Dim thisPriceRow As DataRow

		Dim InstrumentIdOrdinal As Integer = BestPricesTable.Columns.IndexOf("InstrumentID")
		Dim PriceLevelOrdinal As Integer = BestPricesTable.Columns.IndexOf("PriceLevel")

		Try
			FirstIndex = 0
			LastIndex = SortedPrices.Length - 1

			' Check the First and last Items

			thisPriceRow = SortedPrices(FirstIndex)
			If CInt(thisPriceRow(InstrumentIdOrdinal)) = InstrumentID Then
				LastInstrumentID = InstrumentID
				LastInstrumentPrice = CDbl(thisPriceRow(PriceLevelOrdinal))
				Return LastInstrumentPrice
				Exit Function
			End If

			thisPriceRow = SortedPrices(LastIndex)
			If CInt(thisPriceRow(InstrumentIdOrdinal)) = InstrumentID Then
				LastInstrumentID = InstrumentID
				LastInstrumentPrice = CDbl(thisPriceRow(PriceLevelOrdinal))
				Return LastInstrumentPrice
				Exit Function
			End If

			' Chop the array to find the Instrument.

			While (LastIndex > (FirstIndex + 1))

				MidIndex = CInt((LastIndex + FirstIndex) / 2)
				thisPriceRow = SortedPrices(MidIndex)

				If CInt(thisPriceRow(InstrumentIdOrdinal)) = InstrumentID Then
					LastInstrumentID = InstrumentID
					LastInstrumentPrice = CDbl(thisPriceRow(PriceLevelOrdinal))
					Return LastInstrumentPrice
					Exit Function
				ElseIf CInt(thisPriceRow(InstrumentIdOrdinal)) < InstrumentID Then
					FirstIndex = MidIndex
				Else
					LastIndex = MidIndex
				End If

			End While
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error returning Best Price.", ex.Message, ex.StackTrace, True)
		End Try

		LastInstrumentID = 0
		LastInstrumentPrice = 0
		Return 0
		Exit Function

	
	End Function


End Class
