﻿'Imports System.Data.SqlClient
'Imports RenaissanceGlobals
'Imports RenaissanceGlobals.Globals

'Imports RenaissanceDataClass
'Imports C1.Win.C1FlexGrid

'Public Class frmRiskBrowser

'	Inherits System.Windows.Forms.Form
'	Implements StandardVeniceForm

'#Region " Windows Form Designer generated code "

'	Private Sub New()
'		MyBase.New()

'		'This call is required by the Windows Form Designer.
'		InitializeComponent()

'		'Add any initialization after the InitializeComponent() call

'	End Sub

'	'NOTE: The following procedure is required by the Windows Form Designer
'	'It can be modified using the Windows Form Designer.  
'	'Do not modify it using the code editor.
'	Friend WithEvents label_Category As System.Windows.Forms.Label
'	Friend WithEvents Label2 As System.Windows.Forms.Label
'	Friend WithEvents Grid_RiskBrowser As C1.Win.C1FlexGrid.C1FlexGrid
'	Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
'	Friend WithEvents Form_StatusStrip As System.Windows.Forms.StatusStrip
'	Friend WithEvents Form_ProgressBar As System.Windows.Forms.ToolStripProgressBar
'	Friend WithEvents ContextMenu_GridRiskBrowser As System.Windows.Forms.ContextMenuStrip
'	Friend WithEvents Context_AddNewCategory As System.Windows.Forms.ToolStripMenuItem
'	Friend WithEvents Context_AddNewCharacteristic As System.Windows.Forms.ToolStripMenuItem
'	Friend WithEvents Context_SeparatorSave As System.Windows.Forms.ToolStripSeparator
'	Friend WithEvents Context_SaveChanges As System.Windows.Forms.ToolStripMenuItem
'	Friend WithEvents Context_CancelChanges As System.Windows.Forms.ToolStripMenuItem
'	Friend WithEvents Context_RenameSeparator As System.Windows.Forms.ToolStripSeparator
'	Friend WithEvents Context_RenameLabel As System.Windows.Forms.ToolStripMenuItem
'	Friend WithEvents Context_RenameTextBox As System.Windows.Forms.ToolStripTextBox
'	Friend WithEvents Context_SeparatorDelete As System.Windows.Forms.ToolStripSeparator
'	Friend WithEvents Context_Delete As System.Windows.Forms.ToolStripMenuItem
'	Friend WithEvents Context_CharacteristicValueSeparator As System.Windows.Forms.ToolStripSeparator
'	Friend WithEvents Context_SetCharacteristicDefaultValue As System.Windows.Forms.ToolStripMenuItem
'	Friend WithEvents Context_SetCharacteristicFixedValue As System.Windows.Forms.ToolStripMenuItem
'	Friend WithEvents Context_ExpandInstruments As System.Windows.Forms.ToolStripMenuItem
'	Friend WithEvents Context_ColapseInstruments As System.Windows.Forms.ToolStripMenuItem
'	Friend WithEvents Context_ExpandCategories As System.Windows.Forms.ToolStripMenuItem
'	Friend WithEvents Context_ColapseCategories As System.Windows.Forms.ToolStripMenuItem
'	Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
'	Friend WithEvents StatusLabel_Characteristics As System.Windows.Forms.ToolStripStatusLabel
'	Friend WithEvents Label_Status As System.Windows.Forms.ToolStripStatusLabel

'#End Region

'#Region " Form Locals and Constants "

'	' Form 'Parent', the Main Venice form.
'	' Generally only accessed through the 'MainForm' property.
'	Private WithEvents _MainForm As VeniceMain

'	' Form ToolTip
'	Private FormTooltip As New ToolTip()

'	' Form Menu


'	' Form Constants, specific to the table being updated.

'	Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

'	' Form Locals, initialised on 'New' defining what standard data items to use
'	Private THIS_TABLENAME As String
'	Private THIS_ADAPTORNAME As String
'	Private THIS_DATASETNAME As String


'	' The standard ChangeID for this form. e.g. tblFund
'	Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

'	' Form Specific Order fields
'	Private THIS_FORM_OrderBy As String

'	' Form specific Permissioning variables
'	Private THIS_FORM_PermissionArea As String
'	Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

'	' Form specific Form type 
'	Private THIS_FORM_FormID As VeniceFormID

'	' Data Structures

'	Private myDataset As DataSet
'	Private myTable As DataTable
'	Private myConnection As SqlConnection
'	Private myAdaptor As SqlDataAdapter
'	Private myDataView As DataView

'	Private ThisStandardDataset As RenaissanceGlobals.StandardDataset

'	Private InstrumentsDS As RenaissanceDataClass.DSInstrument

'	Private UpdatedCategoryEntries As New ArrayList
'	Private ExpandedNodes As New Dictionary(Of ULong, Boolean)
'	Private LastGridScroll As System.Drawing.Point = New System.Drawing.Point(0, 0)
'	Private LastEnteredGridRow As Integer = 0
'	Private LastEnteredGridCol As Integer = 0
'	Private GridRenameFocus As Boolean = False
'	Private GridRenameItem As String = ""
'	Private GridDeleteItem As String = ""

'	' List of Instruments and Categories. built in the paint function.

'	Private CategoryList As New ArrayList

'	' Active Element.

'	Private __IsOverCancelButton As Boolean
'	Private _InUse As Boolean

'	' Form Status Flags

'	Private FormIsValid As Boolean
'	Private InPaint As Boolean
'	Private _FormOpenFailed As Boolean

'	' User Permission Flags

'	Private HasReadPermission As Boolean
'	Private HasUpdatePermission As Boolean
'	Private HasInsertPermission As Boolean
'	Private HasDeletePermission As Boolean

'	Private Class GridColumns
'		Public Shared FirstCol As Integer = 0
'		Public Shared DataWeighting As Integer = 1
'		Public Shared LastUpdated As Integer = 2
'		Public Shared InstrumentID As Integer = 3
'		Public Shared CategoryID As Integer = 4
'		Public Shared CharacteristicID As Integer = 5
'		Public Shared Counter As Integer = 6
'		Public Shared RiskDataID As Integer = 7
'		Public Shared Expanded As Integer = 8
'	End Class

'	Private Sub Init_GridColumnsClass()
'		GridColumns.FirstCol = Me.Grid_RiskBrowser.Cols("FirstCol").SafeIndex
'		GridColumns.DataWeighting = Me.Grid_RiskBrowser.Cols("DataWeighting").SafeIndex
'		GridColumns.LastUpdated = Me.Grid_RiskBrowser.Cols("LastUpdated").SafeIndex
'	End Sub

'	Private Class CharacteristicSummaryClass
'		Public RiskDataID As Integer
'		Public DataCategoryID As Integer
'		Public DataCharacteristicID As Integer
'		Public InstrumentID As Integer
'		Public InstrumentDescription As String
'		Public DataCategory As String
'		Public DataCharacteristic As String
'		Public DataViewCounter As Integer
'		Public CharacteristicsCount As Integer
'		Public DataWeighting As Double
'		Public DateEntered As Date

'		Public Sub New()
'			RiskDataID = 0
'			DataCategoryID = 0
'			DataCharacteristicID = 0
'			InstrumentID = 0
'			DataCategory = ""
'			DataCharacteristic = ""
'			InstrumentDescription = ""
'			DataViewCounter = 0
'			CharacteristicsCount = 0
'			DataWeighting = 0.0
'			DateEntered = Renaissance_BaseDate
'		End Sub

'		Public Sub New(ByVal pRiskDataID As Integer, ByVal pInstrumentID As Integer, ByVal pDataCategoryID As Integer, ByVal pDataCharacteristicID As Integer, ByVal pDataWeighting As Double)
'			Me.New()

'			RiskDataID = pRiskDataID
'			DataCategoryID = pDataCategoryID
'			DataCharacteristicID = pDataCharacteristicID
'			InstrumentID = pInstrumentID
'			DataWeighting = pDataWeighting
'		End Sub

'		Public Sub New(ByVal pRiskDataID As Integer, ByVal pInstrumentID As Integer, ByVal pInstrumentName As String, ByVal pDataCategoryID As Integer, ByVal pCategoryName As String, ByVal pDataCharacteristicID As Integer, ByVal pCharacteristicName As String, ByVal pDataWeighting As Double, ByVal pDateEntered As Date)
'			Me.New()

'			RiskDataID = pRiskDataID
'			DataCategoryID = pDataCategoryID
'			DataCategory = pCategoryName
'			DataCharacteristicID = pDataCharacteristicID
'			DataCharacteristic = pCharacteristicName
'			InstrumentID = pInstrumentID
'			InstrumentDescription = pInstrumentName
'			DataWeighting = pDataWeighting
'			DateEntered = pDateEntered
'		End Sub
'	End Class

'#End Region

'#Region " Form 'Properties' "

'	Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
'		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
'		' data structures and many common utilities.
'		Get
'			Return _MainForm
'		End Get
'	End Property

'	Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
'		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
'		' Button on this form.
'		' This property is specifically designed for use by the field formating Event functions
'		' In order that they do not impose format restrictions if the user is about to click the 
'		' 'Cancel' button.
'		'
'		Get
'			Return __IsOverCancelButton
'		End Get
'		Set(ByVal Value As Boolean)
'			__IsOverCancelButton = Value
'		End Set
'	End Property

'	Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
'		Get
'			Return InPaint
'		End Get
'	End Property

'	Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
'		Get
'			Return _InUse
'		End Get
'	End Property

'	Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
'		Get
'			Return _FormOpenFailed
'		End Get
'	End Property

'#End Region

'	Public Sub New(ByVal pMainForm As VeniceMain)
'		' *************************************************************
'		' Custom 'New'. 
'		' Passes in the reference to the parent form.
'		' 
'		' Establishes form specific variables.
'		' Establishes Form specific Data connection / data structures.
'		'
'		' *************************************************************

'		Me.New()


'		_MainForm = pMainForm
'		AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

'		_FormOpenFailed = False
'		_InUse = True

'		' Default Select and Order fields.

'		THIS_FORM_OrderBy = "InstrumentDescription"

'		' Form Permissioning :-

'		THIS_FORM_PermissionArea = "frmRiskBrowser"
'		THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

'		' 'This' form ID

'		THIS_FORM_FormID = VeniceFormID.frmRiskBrowser

'		' This form's dataset type.

'		ThisStandardDataset = RenaissanceStandardDatasets.tblSelectCharacteristics ' This Defines the Form Data !!! 


'		' Data object names standard to this Form type.

'		THIS_TABLENAME = ThisStandardDataset.TableName
'		THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
'		THIS_DATASETNAME = ThisStandardDataset.DatasetName

'		THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

'		' Establish / Retrieve data objects for this form.

'		myConnection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)
'		myAdaptor = MainForm.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, VENICE_CONNECTION, THIS_TABLENAME)
'		myDataset = MainForm.Load_Table(ThisStandardDataset, False)
'		myTable = myDataset.Tables(0)

'		' Report
'		SetTransactionReportMenu(RootMenu)

'		' Grid :- Grid_Characteristics

'		Grid_RiskBrowser.Rows.Count = 1
'		Grid_RiskBrowser.Cols.Fixed = 0
'		'    Grid_Characteristics.ExtendLastCol = True

'		'styles
'		Dim cs As CellStyle = Grid_RiskBrowser.Styles.Normal
'		cs.Border.Direction = BorderDirEnum.Vertical
'		cs.WordWrap = False

'		cs = Grid_RiskBrowser.Styles.Add("Data")
'		cs.BackColor = Color.FromArgb(233, 233, 245)
'		cs.ForeColor = Color.Black

'		cs = Grid_RiskBrowser.Styles.Add("DataNegative")
'		cs.BackColor = Color.FromArgb(245, 233, 233)
'		cs.ForeColor = Color.Red

'		cs = Grid_RiskBrowser.Styles.Add("Data_Default")
'		cs.BackColor = Color.FromArgb(210, 210, 245)
'		cs.ForeColor = Color.Black

'		cs = Grid_RiskBrowser.Styles.Add("DataNegative")
'		cs.BackColor = Color.FromArgb(245, 210, 210)
'		cs.ForeColor = Color.Red

'		cs = Grid_RiskBrowser.Styles.Add("SourceNode")
'		cs.BackColor = Color.Yellow
'		cs.Font = New Font(Grid_RiskBrowser.Font, FontStyle.Bold)

'		cs = Grid_RiskBrowser.Styles.Add("UpdatedWeightPositive")
'		cs.ForeColor = Color.Black
'		cs.Font = New Font(Grid_RiskBrowser.Font, FontStyle.Bold)

'		cs = Grid_RiskBrowser.Styles.Add("UpdatedWeightNegative")
'		cs.ForeColor = Color.Red
'		cs.Font = New Font(Grid_RiskBrowser.Font, FontStyle.Bold)

'		'outline tree
'		Grid_RiskBrowser.Tree.Column = 0
'		Grid_RiskBrowser.Tree.Style = TreeStyleFlags.Simple
'		Grid_RiskBrowser.AllowMerging = AllowMergingEnum.Nodes

'		'other
'		Grid_RiskBrowser.AllowResizing = AllowResizingEnum.Columns
'		Grid_RiskBrowser.SelectionMode = SelectionModeEnum.Cell

'		Call Init_GridColumnsClass()

'		' Establish initial DataView and Initialise Characteristics Grid.

'		Try
'			myDataView = New DataView(myTable, "True", "", DataViewRowState.CurrentRows)
'			myDataView.Sort = "InstrumentID, DataCategoryId, DataCharacteristicId"
'		Catch ex As Exception

'		End Try

'		' Form Control Changed events
'		AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

'		AddHandler Combo_Category.SelectedValueChanged, AddressOf Me.FormControlChanged
'		AddHandler Combo_Characteristic.SelectedValueChanged, AddressOf Me.FormControlChanged
'		AddHandler Combo_Instrument.SelectedValueChanged, AddressOf Me.FormControlChanged
'		AddHandler Combo_UpdatedDateOperator.SelectedValueChanged, AddressOf Me.FormControlChanged
'		AddHandler Date_UpdatedDate.ValueChanged, AddressOf Me.FormControlChanged

'		AddHandler Check_EnteredCharacteristics.CheckedChanged, AddressOf Me.FormControlChanged

'		AddHandler Combo_Category.KeyUp, AddressOf MainForm.ComboSelectAsYouType
'		AddHandler Combo_Characteristic.KeyUp, AddressOf MainForm.ComboSelectAsYouType
'		AddHandler Combo_Instrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
'		AddHandler Combo_UpdatedDateOperator.KeyUp, AddressOf MainForm.ComboSelectAsYouType

'		AddHandler Combo_Category.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
'		AddHandler Combo_Characteristic.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
'		AddHandler Combo_Instrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
'		AddHandler Combo_UpdatedDateOperator.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

'		AddHandler Combo_Category.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
'		AddHandler Combo_Characteristic.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
'		AddHandler Combo_Instrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
'		AddHandler Combo_UpdatedDateOperator.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

'		AddHandler Combo_Category.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
'		AddHandler Combo_Characteristic.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
'		AddHandler Combo_Instrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
'		AddHandler Combo_UpdatedDateOperator.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

'		' Set up the ToolTip
'		MainForm.SetFormToolTip(Me, FormTooltip)

'		InstrumentsDS = Nothing

'	End Sub

'#Region " This Form Event handlers : FormLoad / FormClose "

'	' Form Initialisation code.
'	'
'	Public Sub ResetForm() Implements StandardVeniceForm.ResetForm
'		THIS_FORM_OrderBy = "TransactionFund, TransactionInstrument"

'		Call Form_Load(Me, New System.EventArgs)
'	End Sub

'	Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
'		ALWAYS_CLOSE_THIS_FORM = True
'		Me.Close()
'	End Sub

'	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
'		' *************************************************************
'		'
'		'
'		' *************************************************************

'		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
'		_FormOpenFailed = False
'		_InUse = True

'		' Initialise Data structures. Connection, Adaptor and Dataset.

'		If Not (MainForm Is Nothing) Then
'			FormIsValid = True
'		Else
'			MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
'			FormIsValid = False
'			_FormOpenFailed = True
'			Exit Sub
'		End If

'		If (myConnection Is Nothing) Then
'			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

'			FormIsValid = False
'			_FormOpenFailed = True
'			Exit Sub
'		End If

'		If (myAdaptor Is Nothing) Then
'			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

'			FormIsValid = False
'			_FormOpenFailed = True
'			Exit Sub
'		End If

'		If (myDataset Is Nothing) Then
'			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

'			FormIsValid = False
'			_FormOpenFailed = True
'			Exit Sub
'		End If


'		' Check User permissions
'		Try
'			Call CheckPermissions()
'			If (HasReadPermission = False) Then
'				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

'				FormIsValid = False
'				_FormOpenFailed = True

'				Exit Sub
'			End If
'		Catch ex As Exception
'			FormIsValid = False
'			_FormOpenFailed = True

'			Exit Sub
'		End Try

'		' Initialse form

'		InPaint = True
'		IsOverCancelButton = False

'		' Initialise main select controls
'		' Build Sorted data list from which this form operates

'		Try
'			UpdatedCategoryEntries.Clear()
'			ExpandedNodes.Clear()
'			LastGridScroll = New System.Drawing.Point(0, 0)

'			Call SetInstrumentCombo()
'			Call SetCategoryCombo()
'			Call SetCharacteristicCombo()

'			Me.Combo_Category.SelectedIndex = -1
'			Me.Combo_Instrument.SelectedIndex = -1
'			Me.Combo_UpdatedDateOperator.SelectedIndex = 0
'			Me.Date_UpdatedDate.Value = Renaissance_BaseDate

'			Me.Check_EnteredCharacteristics.Checked = False

'			Dim NewStyle As C1.Win.C1FlexGrid.CellStyle
'			NewStyle = Grid_RiskBrowser.Styles.Add("Weight_IntegerFormatLeft", Grid_RiskBrowser.GetCellStyle(0, GridColumns.DataWeighting))
'			NewStyle.Format = "#,##0"
'			NewStyle.Font = New Font(NewStyle.Font, FontStyle.Regular)
'			NewStyle.ForeColor = Color.Navy
'			NewStyle.TextAlign = TextAlignEnum.LeftCenter

'			NewStyle = Grid_RiskBrowser.Styles.Add("Weight_IntegerFormatMiddle", Grid_RiskBrowser.GetCellStyle(0, GridColumns.DataWeighting))
'			NewStyle.Format = "#,##0"
'			NewStyle.ForeColor = Color.CornflowerBlue
'			NewStyle.TextAlign = TextAlignEnum.LeftCenter
'			Call SetSortedRows()

'			Call MainForm.SetComboSelectionLengths(Me)
'		Catch ex As Exception
'			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error initialising Set Characteristics Form.", ex.StackTrace, True)
'		End Try

'		InPaint = False

'	End Sub

'	Private Sub frm_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
'		Dim HideForm As Boolean

'		' Hide or Close this form ?
'		' All depends on how many of this form type are Open or in Cache...

'		_InUse = False

'		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
'			HideForm = False
'		Else
'			HideForm = True
'			If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
'				HideForm = False
'			End If
'		End If

'		If HideForm = True Then
'			MainForm.HideInFormsCollection(Me)
'			Me.Hide()	' NPP Fix

'			e.Cancel = True
'		Else
'			Try
'				MainForm.RemoveFromFormsCollection(Me)
'				RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

'				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

'				RemoveHandler Combo_Category.SelectedValueChanged, AddressOf Me.FormControlChanged
'				RemoveHandler Combo_Instrument.SelectedValueChanged, AddressOf Me.FormControlChanged
'				RemoveHandler Combo_UpdatedDateOperator.SelectedValueChanged, AddressOf Me.FormControlChanged
'				RemoveHandler Date_UpdatedDate.ValueChanged, AddressOf Me.FormControlChanged

'				RemoveHandler Check_EnteredCharacteristics.CheckedChanged, AddressOf Me.FormControlChanged

'				RemoveHandler Combo_Category.KeyUp, AddressOf MainForm.ComboSelectAsYouType
'				RemoveHandler Combo_Instrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
'				RemoveHandler Combo_UpdatedDateOperator.KeyUp, AddressOf MainForm.ComboSelectAsYouType

'				RemoveHandler Combo_Category.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
'				RemoveHandler Combo_Instrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
'				RemoveHandler Combo_UpdatedDateOperator.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

'				RemoveHandler Combo_Category.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
'				RemoveHandler Combo_Instrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
'				RemoveHandler Combo_UpdatedDateOperator.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

'				RemoveHandler Combo_Category.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
'				RemoveHandler Combo_Instrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
'				RemoveHandler Combo_UpdatedDateOperator.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

'			Catch ex As Exception
'			End Try
'		End If

'	End Sub

'	Private Sub Check_EnteredCharacteristics_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

'		Call SetSortedRows(True)

'	End Sub


'#End Region



'	' Routine to handle changes / updates to tables by this and other windows.
'	' If this, or any other, form posts a change to a table, then it will invoke an update event 
'	' detailing what tables have been altered.
'	' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
'	' the 'VeniceAutoUpdate' event of the main Venice form.
'	' Each form may them react as appropriate to changes in any table that might impact it.
'	'
'	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
'		Dim OrgInPaint As Boolean
'		Dim KnowledgeDateChanged As Boolean
'		Dim RefreshGrid As Boolean

'		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

'		OrgInPaint = InPaint

'		Try
'			InPaint = True
'			KnowledgeDateChanged = False
'			RefreshGrid = False

'			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
'				KnowledgeDateChanged = True
'			End If
'		Catch ex As Exception
'		End Try

'		' ****************************************************************
'		' Check for changes relevant to this form
'		' ****************************************************************

'		' Changes to the tblRiskDataCharacteristic table :-

'		Try
'			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblRiskDataCharacteristic) = True) Or KnowledgeDateChanged Then
'				Call SetCharacteristicCombo()
'			End If
'		Catch ex As Exception
'			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblRiskDataCharacteristic", ex.StackTrace, True)
'		End Try

'		' Changes to the tblRiskDataCategory table :-

'		Try
'			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblRiskDataCategory) = True) Or KnowledgeDateChanged Then
'				Call SetCategoryCombo()
'			End If
'		Catch ex As Exception
'			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblRiskDataCategory", ex.StackTrace, True)
'		End Try

'		' Changes to the tblInstrument table :-
'		Try
'			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrument) = True) Or KnowledgeDateChanged Then
'				Call SetInstrumentCombo()
'				InstrumentsDS = Nothing
'			End If
'		Catch ex As Exception
'			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblInstrument", ex.StackTrace, True)
'		End Try

'		' Changes to the KnowledgeDate :-

'		Try
'			If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
'				RefreshGrid = True
'			End If
'		Catch ex As Exception
'			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: KnowledgeDate", ex.StackTrace, True)
'		End Try

'		' Changes to the tblUserPermissions table :-

'		Try
'			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

'				' Check ongoing permissions.

'				Call CheckPermissions()
'				If (HasReadPermission = False) Then
'					Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

'					FormIsValid = False
'					InPaint = OrgInPaint
'					Me.Close()
'					Exit Sub
'				End If

'				RefreshGrid = True

'			End If
'		Catch ex As Exception
'			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: tblUserPermissions", ex.StackTrace, True)
'		End Try


'		' ****************************************************************
'		' Changes to the Main FORM table :-
'		' 
'		' ****************************************************************

'		Try
'			If (e.TableChanged(THIS_FORM_ChangeID) = True) Or _
'			 (RefreshGrid = True) Or _
'			 KnowledgeDateChanged Then

'				' Re-Set Controls etc.
'				Call SetSortedRows(True)

'			End If
'		Catch ex As Exception
'			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: Main Form table", ex.StackTrace, True)
'		End Try

'		InPaint = OrgInPaint

'	End Sub

'#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

'	Private Function GetCharacteristicsSelectString() As String
'		' *******************************************************************************
'		' Build a Select String appropriate to the form status.
'		' *******************************************************************************

'		Dim SelectString As String
'		Dim FieldSelectString As String

'		SelectString = ""
'		FieldSelectString = ""
'		MainForm.SetToolStripText(Label_Status, "")

'		If (myDataset Is Nothing) Then
'			Return "True"
'			Exit Function
'		End If

'		Try

'			' Select on Fund...
'			If (Me.Combo_Category.SelectedIndex > 0) Then
'				SelectString = "(DataCategoryId=" & Combo_Category.SelectedValue.ToString & ")"
'			End If

'			' Select on Instrument

'			If (Me.Combo_Instrument.SelectedIndex > 0) Then
'				If (SelectString.Length > 0) Then
'					SelectString &= " AND "
'				End If
'				SelectString &= "(InstrumentID=" & Combo_Instrument.SelectedValue.ToString & ")"
'			End If

'			' Select on Entered Date

'			If (Me.Combo_UpdatedDateOperator.SelectedIndex > 0) Then
'				If (SelectString.Length > 0) Then
'					SelectString &= " AND "
'				End If

'				SelectString &= "(DateEntered " & Combo_UpdatedDateOperator.SelectedItem.ToString & " #" & Me.Date_UpdatedDate.Value.ToString(QUERY_SHORTDATEFORMAT) & "#)"
'			End If

'			' Check for the 'Incomplete Transactions' option.

'			If Me.Check_EnteredCharacteristics.Checked = True Then
'				If SelectString.Length <= 0 Then
'					SelectString = ENTERED_CHARACTERISTICS_QUERY
'				Else
'					SelectString = "(" & SelectString & ") AND (" & ENTERED_CHARACTERISTICS_QUERY & ")"
'				End If
'			End If

'			If SelectString.Length <= 0 Then
'				SelectString = "true"
'			End If

'		Catch ex As Exception
'			SelectString = "true"
'		End Try

'		Return SelectString

'	End Function

'	Private Sub SetSortedRows(Optional ByVal ForceRefresh As Boolean = False)
'		' *******************************************************************************
'		' Build a Select String appropriate to the form status and apply it to the DataView object.
'		' *******************************************************************************

'		Dim SelectString As String

'		SelectString = GetCharacteristicsSelectString()

'		If (myDataView Is Nothing) Then
'			Exit Sub
'		End If

'		Try
'			If (Not (myDataView.RowFilter = SelectString)) Or (ForceRefresh = True) Then
'				If ForceRefresh = True Then
'					myDataView.RowFilter = "False"
'				End If

'				myDataView.RowFilter = SelectString
'				PaintCharacteristicsGrid()

'			End If
'		Catch ex As Exception
'			SelectString = "true"
'			myDataView.RowFilter = SelectString
'		End Try

'		Me.Label_Status.Text = "(" & myDataView.Count.ToString & " Records) " & SelectString

'	End Sub


'	' Check User permissions
'	Private Sub CheckPermissions()
'		' *******************************************************************************
'		'
'		' *******************************************************************************

'		Dim Permissions As Integer

'		Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

'		HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
'		HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
'		HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
'		HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

'	End Sub

'	Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
'		' *******************************************************************************
'		' In the event of one of the Form Controls changing, refresh the data grid.
'		' *******************************************************************************

'		If InPaint = False Then
'			LastGridScroll = New System.Drawing.Point(0, 0)

'			Call SetSortedRows()

'		End If
'	End Sub

'	Private Sub Combo_FieldSelect_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
'		' *******************************************************************************
'		' In the event of one of the Form Controls changing, refresh the data grid.
'		' *******************************************************************************

'		If InPaint = False Then

'			Call SetSortedRows()

'		End If
'	End Sub


'	Private Sub UpdateSelectedValueCombo(ByVal pFieldName As String, ByRef pValueCombo As ComboBox)
'		' *******************************************************************************
'		' Build a Value Combo Box appropriate to the chosen DataTable Select Field.
'		'
'		' By default the combo will contain all existing values for the chosen field, however
'		' If a relationship is defined for thi table and field, then a Combo will be built
'		' which reflects this relationship.
'		' e.g. the tblReferentialIntegrity table defined a relationship between the
'		' TransactionCounterparty field and the tblCounterparty.Counterparty field, thus if
'		' the chosen Select field ti 'Counterparty.' then the Value combo will be built using
'		' the CounterpartyID & CounterpartyName details from the tblCounterparty table.
'		'
'		' *******************************************************************************

'		Dim tblRefIntegrity As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityDataTable
'		Dim IntegrityRows() As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityRow
'		Dim TransactionDataset As DataSet
'		Dim TransactionTable As DataTable
'		Dim SortOrder As Boolean

'		Dim Org_InPaint As Boolean = InPaint

'		Try
'			InPaint = True

'			SortOrder = True

'			' Clear the Value Combo.

'			Try
'				pValueCombo.DataSource = Nothing
'				pValueCombo.DisplayMember = ""
'				pValueCombo.ValueMember = ""
'				pValueCombo.Items.Clear()
'			Catch ex As Exception
'			End Try

'			' By default, make the Combo a Standard Dropdown, so that non-list items can be entered.

'			pValueCombo.DropDownStyle = ComboBoxStyle.DropDown

'			' Exit if no FiledName is given (having cleared the combo).

'			If (pFieldName.Length <= 0) Then Exit Sub

'			' Adjust the Field name if appropriate.

'			If (pFieldName.EndsWith(".")) Then
'				pFieldName = "Transaction" & pFieldName.Substring(0, pFieldName.Length - 1)
'			End If

'			' Get a handle to the Standard Characteristics Table.
'			' This is so that the field types can be determined and used.

'			Try
'				TransactionDataset = MainForm.Load_Table(ThisStandardDataset, False)
'				TransactionTable = myDataset.Tables(0)
'			Catch ex As Exception
'				Exit Sub
'			End Try

'			' If the selected field is a Date then sort the selection combo in reverse order.

'			If (TransactionTable.Columns(pFieldName).DataType Is GetType(System.DateTime)) Then
'				SortOrder = False
'			End If

'			' Get a handle to the Referential Integrity table and the row matching this table and field.
'			' this table defines a relationship between Transaction Table fields and other tables.
'			' This information can be used to build more meaningfull Value Combos.

'			Try
'				tblRefIntegrity = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblReferentialIntegrity, False), RenaissanceDataClass.DSReferentialIntegrity).tblReferentialIntegrity
'				IntegrityRows = tblRefIntegrity.Select("(FeedsTable LIKE 'tblSelectCharacteristics') AND (FeedsField = '" & pFieldName & "')", "RN")
'				If (IntegrityRows.Length <= 0) Then
'					GoTo StandardExit
'				End If

'			Catch ex As Exception
'				Exit Sub
'			End Try

'			If (IntegrityRows(0).IsDescriptionFieldNull) Then
'				' No Linked Description Field

'				GoTo StandardExit
'			End If

'			' OK, a referential record exists.
'			' Determine the Table and Field Names to use to build the Value Combo.

'			Dim TableName As String
'			Dim TableField As String
'			Dim DescriptionField As String
'			Dim stdDS As StandardDataset
'			Dim thisChangeID As RenaissanceChangeID

'			Try

'				TableName = IntegrityRows(0).TableName
'				TableField = IntegrityRows(0).TableField
'				DescriptionField = IntegrityRows(0).DescriptionField

'				thisChangeID = CType(System.Enum.Parse(GetType(RenaissanceChangeID), TableName), RenaissanceChangeID)

'				stdDS = RenaissanceStandardDatasets.GetStandardDataset(thisChangeID)

'				' Build the appropriate Values Combo.

'				Call MainForm.SetTblGenericCombo( _
'				pValueCombo, _
'				stdDS, _
'				DescriptionField, _
'				TableField, _
'				"", False, SortOrder, True)		 ' 

'				' For Referential Integrity generated combo's, make the Combo a 
'				' DropDownList

'				pValueCombo.DropDownStyle = ComboBoxStyle.DropDownList

'			Catch ex As Exception
'				GoTo StandardExit

'			End Try

'			Exit Sub

'StandardExit:

'			' Build the standard Combo, just use the values from the given field.

'			Call MainForm.SetTblGenericCombo( _
'			pValueCombo, _
'			RenaissanceStandardDatasets.tblTransaction, _
'			pFieldName, _
'			pFieldName, _
'			"", True, SortOrder)	 ' 

'		Catch ex As Exception
'		Finally
'			InPaint = Org_InPaint
'		End Try

'	End Sub

'	Private Function GetFieldChangeID(ByVal pFieldName As String) As RenaissanceChangeID
'		Dim tblRefIntegrity As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityDataTable
'		Dim IntegrityRows() As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityRow

'		If (pFieldName.EndsWith(".")) Then
'			pFieldName = "Transaction" & pFieldName.Substring(0, pFieldName.Length - 1)
'		End If

'		Try
'			tblRefIntegrity = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblReferentialIntegrity, False), RenaissanceDataClass.DSReferentialIntegrity).tblReferentialIntegrity
'			IntegrityRows = tblRefIntegrity.Select("(FeedsTable LIKE '%tblTransaction') AND (FeedsField = '" & pFieldName & "')", "RN")
'			If (IntegrityRows.Length <= 0) Then
'				Return RenaissanceChangeID.None
'				Exit Function
'			End If

'		Catch ex As Exception
'			Return RenaissanceChangeID.None
'			Exit Function
'		End Try


'		If (IntegrityRows(0).IsDescriptionFieldNull) Then
'			Return RenaissanceChangeID.None
'			Exit Function
'		End If

'		' OK, a referential record exists.
'		' Determine the Table and Field Names to use to build the Value Combo.

'		Dim TableName As String
'		Dim TableField As String
'		Dim DescriptionField As String
'		Dim thisChangeID As RenaissanceChangeID

'		Try

'			TableName = IntegrityRows(0).TableName
'			TableField = IntegrityRows(0).TableField
'			DescriptionField = IntegrityRows(0).DescriptionField

'			thisChangeID = CType(System.Enum.Parse(GetType(RenaissanceChangeID), TableName), RenaissanceChangeID)

'			Return thisChangeID
'			Exit Function
'		Catch ex As Exception
'		End Try

'		Return RenaissanceChangeID.None
'		Exit Function

'	End Function

'	Private Sub Grid_Characteristics_AfterEdit(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles Grid_RiskBrowser.AfterEdit
'		' ***************************************************************************************
'		'
'		'
'		' ***************************************************************************************
'		Me.btnSave.Visible = True
'		Me.btnCancel.Visible = True

'		Me.Context_SaveChanges.Visible = True
'		Me.Context_CancelChanges.Visible = True
'		Me.Context_SeparatorSave.Visible = True

'		Try
'			Dim Counter As Integer
'			Dim thisCategory As CharacteristicSummaryClass = Nothing

'			Dim thisGridRow As C1.Win.C1FlexGrid.Row

'			thisGridRow = Grid_RiskBrowser.Rows(e.Row)

'			Dim thisRiskDataID As Integer = CInt(thisGridRow(GridColumns.RiskDataID))
'			Dim thisInstrumentID As Integer = CInt(thisGridRow(GridColumns.InstrumentID))
'			Dim thisCatagoryID As Integer = CInt(thisGridRow(GridColumns.CategoryID))
'			Dim thisCharacteristicID As Integer = CInt(thisGridRow(GridColumns.CharacteristicID))
'			Dim thisDataWeighting As Double = CDbl(thisGridRow(GridColumns.DataWeighting))

'			If (thisDataWeighting >= 0) Then
'				Grid_RiskBrowser.SetCellStyle(e.Row, GridColumns.DataWeighting, Grid_RiskBrowser.Styles("UpdatedWeightPositive"))
'			Else
'				Grid_RiskBrowser.SetCellStyle(e.Row, GridColumns.DataWeighting, Grid_RiskBrowser.Styles("UpdatedWeightNegative"))
'			End If

'			' Has this Characteristic been changed before ?

'			If UpdatedCategoryEntries.Count > 0 Then

'				For Counter = 0 To (UpdatedCategoryEntries.Count - 1)
'					thisCategory = UpdatedCategoryEntries(Counter)	'  CharacteristicSummaryClass

'					If (thisCategory.InstrumentID = thisInstrumentID) And (thisCategory.DataCategoryID = thisCatagoryID) And (thisCategory.DataCharacteristicID = thisCharacteristicID) Then
'						thisCategory.DataWeighting = thisDataWeighting
'						UpdatedCategoryEntries(Counter) = thisCategory

'						Exit Sub
'					End If

'				Next Counter

'			End If

'			' Add new Characteristic

'			UpdatedCategoryEntries.Add(New CharacteristicSummaryClass(thisRiskDataID, thisInstrumentID, thisCatagoryID, thisCharacteristicID, thisDataWeighting))

'		Catch ex As Exception
'		End Try

'	End Sub


'	Private Sub Grid_Characteristics_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Grid_RiskBrowser.DoubleClick

'		' ***************************************************************************************
'		' Expand if DoubleClick on a Node
'		'
'		' What to do if double click on a characteristic ?
'		' ***************************************************************************************

'		Try
'			If (Grid_RiskBrowser.RowSel > 0) AndAlso (Grid_RiskBrowser.Rows(Grid_RiskBrowser.RowSel).IsNode) Then
'				Grid_RiskBrowser.Rows(Grid_RiskBrowser.RowSel).Node.Expanded = (Not Grid_RiskBrowser.Rows(Grid_RiskBrowser.RowSel).Node.Expanded)
'				Exit Sub
'			End If
'		Catch ex As Exception
'		End Try

'		'Try
'		'  Dim ParentID As Integer
'		'  Dim thisFormHandle As RenaissanceGlobals.RenaissanceFormHandle
'		'
'		'  ParentID = Me.Grid_Characteristics.Rows(Grid_Characteristics.RowSel)("ParentID")

'		'  If (ParentID > 0) Then
'		'    Grid_Characteristics.Cursor = Cursors.WaitCursor

'		'    thisFormHandle = MainForm.New_VeniceForm(VeniceFormID.frmTransaction)
'		'    CType(thisFormHandle.Form, frmTransaction).FormSelectCriteria = GetTransactionSelectString(True)
'		'    CType(thisFormHandle.Form, frmTransaction).MoveToAuditID(ParentID)
'		'  End If

'		'Catch ex As Exception
'		'Finally
'		'  Grid_Characteristics.Cursor = Cursors.Default
'		'End Try

'	End Sub

'#End Region

'#Region " Set Form Combos (Form Specific Code) "

'	Private Sub SetInstrumentCombo()

'		Call MainForm.SetTblGenericCombo( _
'		Me.Combo_Instrument, _
'		RenaissanceStandardDatasets.tblInstrument, _
'		"InstrumentDescription", _
'		"InstrumentID", _
'		"", False, True, True)		' 

'	End Sub


'	Private Sub SetCategoryCombo()

'		Call MainForm.SetTblGenericCombo( _
'		Me.Combo_Category, _
'		RenaissanceStandardDatasets.tblRiskDataCategory, _
'		"DataCategory", _
'		"DataCategoryId", _
'		"", False, True, True)		' 

'	End Sub


'	Private Sub SetCharacteristicCombo()

'		Call MainForm.SetTblGenericCombo( _
'		Me.Combo_Characteristic, _
'		RenaissanceStandardDatasets.tblRiskDataCharacteristic, _
'		"DataCharacteristic", _
'		"DataCharacteristicId", _
'		"", False, True, True)		' 

'	End Sub

'#End Region

'#Region " Transaction report Menu"

'	Private Function SetTransactionReportMenu(ByRef RootMenu As MenuStrip) As MenuStrip

'		Dim ReportMenu As New ToolStripMenuItem("&Reports")
'		'Dim newMenuItem As ToolStripMenuItem

'		'newMenuItem = ReportMenu.DropDownItems.Add("Transaction &Ticket", Nothing, AddressOf Me.rptTransactionTicket)

'		ReportMenu.DropDownItems.Add(New ToolStripSeparator)

'		'newMenuItem = ReportMenu.DropDownItems.Add("Transaction By &Value Date", Nothing, AddressOf Me.rptTransactionByDate)
'		'newMenuItem = ReportMenu.DropDownItems.Add("Transaction By &Entry Date", Nothing, AddressOf Me.rptTransactionByEntryDate)
'		'newMenuItem = ReportMenu.DropDownItems.Add("Transaction By &Group", Nothing, AddressOf Me.rptTransactionByGroup)
'		'newMenuItem = ReportMenu.DropDownItems.Add("Transaction By &Instrument", Nothing, AddressOf Me.rptTransactionByStock)
'		'newMenuItem = ReportMenu.DropDownItems.Add("&Incomplete Transaction Report", Nothing, AddressOf Me.rptIncompleteTransactions)
'		'newMenuItem = ReportMenu.DropDownItems.Add("Transaction &Price Differences", Nothing, AddressOf Me.rptTransactionPriceDifference)
'		'newMenuItem = ReportMenu.DropDownItems.Add("&Instrument Position Report", Nothing, AddressOf Me.rptInstrumentPositionsReport)

'		'ReportMenu.DropDownItems.Add(New ToolStripSeparator)

'		'newMenuItem = ReportMenu.DropDownItems.Add("Trade &Blotter", Nothing, AddressOf Me.rptTradeBlotter)
'		'newMenuItem = ReportMenu.DropDownItems.Add("&Final Trade Blotter", Nothing, AddressOf Me.rptFinalTradeBlotter)
'		'newMenuItem = ReportMenu.DropDownItems.Add("Trade &Revision Blotter", Nothing, AddressOf Me.rptTradeRevisionBlotter)
'		'newMenuItem = ReportMenu.DropDownItems.Add("Final Trade &Revision Blotter", Nothing, AddressOf Me.rptFinalTradeRevisionBlotter)

'		ReportMenu.DropDownItems.Add(New ToolStripSeparator)


'		RootMenu.Items.Add(ReportMenu)
'		Return RootMenu

'	End Function


'#End Region

'#Region " Bug hunting "

'	' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
'	' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
'	' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

'	Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
'		Dim a As Integer

'		If Not e.Command Is Nothing Then
'			a = 1
'		End If

'		If Not e.Errors Is Nothing Then
'			a = 2
'		End If
'	End Sub

'	Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
'		Dim a As Integer

'		If Not e.Command Is Nothing Then
'			a = 1
'		End If

'		If Not e.Errors Is Nothing Then
'			a = 2
'		End If
'	End Sub

'	Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
'		Dim a As Integer

'		a = 1
'	End Sub

'#End Region

'	Private Class CategoryListComparerClass
'		Implements IComparer

'		Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare
'			' ************************************************************************
'			' Implement class to compare two CategoryList items.
'			' Nothing is deemed to be less than something.
'			'
'			' ************************************************************************

'			Try

'				If (TypeOf x Is CharacteristicSummaryClass) AndAlso (TypeOf y Is CharacteristicSummaryClass) Then
'					Dim xCategory As CharacteristicSummaryClass = x
'					Dim yCategory As CharacteristicSummaryClass = y

'					If (x Is Nothing) AndAlso (y Is Nothing) Then
'						Return 0

'					ElseIf (x Is Nothing) Then
'						Return (-1)

'					ElseIf (y Is Nothing) Then
'						Return (1)

'					Else
'						' Compare
'						Dim RVal As Integer = 0

'						RVal = CType(xCategory.InstrumentDescription, IComparable).CompareTo(yCategory.InstrumentDescription)

'						If RVal <> 0 Then
'							Return RVal
'						End If

'						RVal = CType(xCategory.InstrumentID, IComparable).CompareTo(yCategory.InstrumentID)

'						If RVal <> 0 Then
'							Return RVal
'						End If

'						RVal = CType(xCategory.DataCategory, IComparable).CompareTo(yCategory.DataCategory)

'						If RVal <> 0 Then
'							Return RVal
'						End If

'						RVal = CType(xCategory.DataCategoryID, IComparable).CompareTo(yCategory.DataCategoryID)

'						If RVal <> 0 Then
'							Return RVal
'						End If

'						RVal = CType(xCategory.DataCharacteristic, IComparable).CompareTo(yCategory.DataCharacteristic)

'						If RVal <> 0 Then
'							Return RVal
'						End If

'						RVal = CType(xCategory.DataCharacteristicID, IComparable).CompareTo(yCategory.DataCharacteristicID)

'						Return RVal

'					End If

'				Else

'					Return 0

'				End If

'			Catch ex As Exception
'			End Try

'			Return 0

'		End Function

'	End Class

'	Private Sub PaintCharacteristicsGrid()
'		' **********************************************************************************
'		' Routine to Paint (In Outline) the Characteristics Grid.
'		'
'		' This Routine populates the grid with the appropriate heirarchy of Funds and Instruments.
'		' To improve performance, the Transaction lines are not added, they are in-filled as 
'		' Instrument lines are expanded.
'		'
'		' **********************************************************************************

'		Dim Counter As Integer
'		Dim thisRowView As DataRowView
'		Dim thisRow As RenaissanceDataClass.DSSelectCharacteristics.tblSelectCharacteristicsRow
'		Dim NewGridRow As C1.Win.C1FlexGrid.Row = Nothing
'		Dim InstrumentGridRow As C1.Win.C1FlexGrid.Row = Nothing
'		Dim thisGridRow As C1.Win.C1FlexGrid.Row

'		Dim LastInstrument As Integer
'		Dim LastCategory As Integer
'		Dim thisInstrumentID As ULong
'		Dim thisCategoryID As ULong
'		Dim thisKey As ULong
'		Dim thisValue As Boolean
'		Dim KeyVal As ULong

'		If (UpdatedCategoryEntries.Count > 0) Then
'			Me.btnSave.Visible = True
'			Me.btnCancel.Visible = True

'			Me.Context_SaveChanges.Visible = True
'			Me.Context_CancelChanges.Visible = True
'			Me.Context_SeparatorSave.Visible = True
'		Else
'			Me.btnSave.Visible = False
'			Me.btnCancel.Visible = False

'			Me.Context_SaveChanges.Visible = False
'			Me.Context_CancelChanges.Visible = False
'			Me.Context_SeparatorSave.Visible = False
'		End If

'		Try
'			Grid_RiskBrowser.Redraw = False

'			Try
'				' Clear Grid, to start
'				Grid_RiskBrowser.Rows.Count = 1	' (The Header Row)

'				' Add Header Line.
'				NewGridRow = Grid_RiskBrowser.Rows.Add()
'				NewGridRow(GridColumns.FirstCol) = "Primonial"
'				NewGridRow.IsNode = True
'				NewGridRow.Node.Level = 0

'			Catch ex As Exception
'			End Try

'			InPaint = True

'			' Initialise tracking variables.
'			LastCategory = (-1)
'			LastInstrument = (-1)

'			' NPP - 29 Nov 2007
'			' Rewrote this section of code to fix a couple of bugs.
'			' Now build an array of Instruments and add them to the array in one pass rather than conditionally deleting
'			' previously added lines which seems to have had some odd effects.

'			CategoryList.Clear()
'			Dim thisCategory As CharacteristicSummaryClass = Nothing

'			Try

'				' Loop through the selected data (DataView) adding Instruments to the ArrayList.

'				For Counter = 0 To (myDataView.Count - 1)

'					' Resolve Transaction Row.
'					thisRowView = myDataView.Item(Counter)
'					thisRow = thisRowView.Row

'					' Add a New Category Row if this transaction represents the start of a new Category or Instrument
'					If (thisRow.InstrumentID <> LastInstrument) OrElse (thisRow.DataCategoryId <> LastCategory) Then

'						thisCategory = New CharacteristicSummaryClass

'						thisCategory.InstrumentID = thisRow.InstrumentID
'						thisCategory.InstrumentDescription = CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, CInt(thisRow.InstrumentID), "InstrumentDescription"))	' thisRow.InstrumentDescription
'						thisCategory.DataCategoryID = thisRow.DataCategoryId
'						thisCategory.DataCategory = CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblRiskDataCategory, CInt(thisRow.DataCategoryId), "DataCategory"))	' thisRow.DataCategoryId
'						thisCategory.DataCharacteristicID = thisRow.DataCharacteristicId
'						thisCategory.DataCharacteristic = ""
'						thisCategory.DataViewCounter = Counter
'						thisCategory.CharacteristicsCount = 0
'						CategoryList.Add(thisCategory)

'					End If

'					If (thisRow.RiskDataID) Or (thisRow.DataWeighting) Then
'						thisCategory.CharacteristicsCount += 1
'					End If

'					LastInstrument = thisRow.InstrumentID
'					LastCategory = thisRow.DataCategoryId

'				Next

'				' Sort

'				CategoryList.Sort(New CategoryListComparerClass)

'				' Now add the Categories to the Grid.

'				LastCategory = (-1)
'				LastInstrument = (-1)

'				For Counter = 0 To (CategoryList.Count - 1)
'					thisCategory = CategoryList(Counter)	'  CharacteristicSummaryClass

'					' Add a New Row if this Characteristic represents the start of a new Instrument.

'					If (LastInstrument <> thisCategory.InstrumentID) Then

'						NewGridRow = Grid_RiskBrowser.Rows.Add()
'						NewGridRow(GridColumns.FirstCol) = thisCategory.InstrumentDescription
'						NewGridRow(GridColumns.InstrumentID) = thisCategory.InstrumentID
'						NewGridRow(GridColumns.CategoryID) = 0
'						NewGridRow(GridColumns.Counter) = Counter	' thisCategory.Counter
'						NewGridRow(GridColumns.Expanded) = False
'						NewGridRow(GridColumns.DataWeighting) = 0
'						NewGridRow.IsNode = True
'						NewGridRow.Node.Level = 1
'						Grid_RiskBrowser.SetCellStyle(NewGridRow.Index, GridColumns.DataWeighting, Grid_RiskBrowser.Styles("Weight_IntegerFormatLeft"))
'						InstrumentGridRow = NewGridRow

'						LastCategory = -1

'						If (thisCategory.DataCategoryID > 0) Then
'							NewGridRow = Grid_RiskBrowser.Rows.Add()
'							NewGridRow(GridColumns.FirstCol) = ""
'							NewGridRow.IsNode = False
'						Else
'							' Ensure that it will not be expanded later
'							KeyVal = (CULng(thisCategory.InstrumentID) << 32)
'							ExpandedNodes(KeyVal) = False

'						End If

'					End If

'					If (InstrumentGridRow IsNot Nothing) Then
'						InstrumentGridRow(GridColumns.DataWeighting) = CInt(InstrumentGridRow(GridColumns.DataWeighting)) + thisCategory.CharacteristicsCount
'					End If

'					LastCategory = thisCategory.DataCategoryID
'					LastInstrument = thisCategory.InstrumentID

'				Next

'				' Now Colapse and expand rows as necessary.

'				For Counter = (Grid_RiskBrowser.Rows.Count - 1) To 1 Step -1
'					Try
'						thisGridRow = Grid_RiskBrowser.Rows(Counter)

'						If (thisGridRow.IsNode) AndAlso (thisGridRow.Node.Level >= 1) Then
'							Grid_RiskBrowser.Rows(Counter).Node.Expanded = False
'						End If
'					Catch Inner_Ex As Exception
'					End Try
'				Next

'				For Counter = (Grid_RiskBrowser.Rows.Count - 1) To 1 Step -1
'					Try
'						thisGridRow = Grid_RiskBrowser.Rows(Counter)

'						If (thisGridRow.IsNode) AndAlso (thisGridRow.Node.Level = 1) Then
'							thisInstrumentID = CULng(thisGridRow(GridColumns.InstrumentID))
'							thisCategoryID = CULng(thisGridRow(GridColumns.CategoryID))
'							thisKey = ((thisInstrumentID << 32) + thisCategoryID)

'							If (ExpandedNodes.TryGetValue(thisKey, thisValue)) Then
'								If (thisValue) Then
'									ExpandRow(Counter)

'									thisGridRow.Node.Expanded = True
'								Else
'									thisGridRow.Node.Expanded = False
'								End If
'							Else
'								thisGridRow.Node.Expanded = False
'							End If

'						End If
'					Catch Inner_Ex As Exception
'					End Try
'				Next

'				For Counter = (Grid_RiskBrowser.Rows.Count - 1) To 1 Step -1
'					Try
'						thisGridRow = Grid_RiskBrowser.Rows(Counter)

'						If (thisGridRow.IsNode) AndAlso (thisGridRow.Node.Level = 2) Then
'							thisInstrumentID = CULng(thisGridRow(GridColumns.InstrumentID))
'							thisCategoryID = CULng(thisGridRow(GridColumns.CategoryID))
'							thisKey = ((thisInstrumentID << 32) + thisCategoryID)

'							If (ExpandedNodes.TryGetValue(thisKey, thisValue)) Then
'								If (thisValue) Then
'									ExpandRow(Counter)

'									thisGridRow.Node.Expanded = True
'								Else
'									thisGridRow.Node.Expanded = False
'								End If
'							Else
'								thisGridRow.Node.Expanded = False
'							End If

'						End If
'					Catch Inner_Ex As Exception
'					End Try
'				Next

'			Catch ex As Exception


'			End Try

'		Catch ex As Exception
'		Finally
'			Grid_RiskBrowser.Redraw = True
'			Grid_RiskBrowser.Refresh()
'			InPaint = False
'			Grid_RiskBrowser.ScrollPosition = LastGridScroll
'		End Try

'	End Sub

'	Private m_ExpandingNode As Boolean = False

'	Private Sub Grid_Characteristics_BeforeCollapse(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles Grid_RiskBrowser.BeforeCollapse
'		' **********************************************************************************
'		' Handle the expanding of Instrument sections.
'		'
'		'
'		' **********************************************************************************
'		Dim KeyVal As ULong
'		Dim thisInstrumentID As ULong
'		Dim thisCategoryID As ULong
'		Dim thisRow As C1.Win.C1FlexGrid.Row

'		' if we're working, ignore this
'		If m_ExpandingNode Then
'			Return
'		End If

'		Try

'			' don't allow Collapsing/expanding groups of rows (with shift-click)
'			If e.Row < 0 Then
'				e.Cancel = True
'				Return
'			End If

'			' if we're already collapsed, populate node
'			' before expanding
'			thisRow = Grid_RiskBrowser.Rows(e.Row)
'			thisInstrumentID = CULng(thisRow(GridColumns.InstrumentID))
'			thisCategoryID = CULng(thisRow(GridColumns.CategoryID))
'			KeyVal = ((thisInstrumentID << 32) + thisCategoryID)

'			If (thisRow.IsNode) AndAlso (thisRow.Node.Collapsed) Then
'				If InPaint = False Then
'					ExpandedNodes(KeyVal) = True
'				End If

'				Try
'					m_ExpandingNode = True
'					ExpandRow(e.Row)
'				Catch ex As Exception
'				Finally
'					m_ExpandingNode = False
'				End Try
'			Else
'				If InPaint = False Then
'					If (thisRow.IsNode) Then
'						If (ExpandedNodes.ContainsKey(KeyVal)) Then
'							ExpandedNodes(KeyVal) = False
'						End If
'					End If

'				End If
'			End If

'		Catch ex As Exception
'		End Try

'	End Sub

'	Private Sub Grid_Characteristics_AfterDragColumn(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.DragRowColEventArgs) Handles Grid_RiskBrowser.AfterDragColumn
'		' **********************************************************************************
'		' Handle the Moving of Grid columns
'		'
'		' Simply re-set the values in the GridColumns class
'		'
'		' **********************************************************************************
'		Call Init_GridColumnsClass()
'	End Sub

'	Private Sub ExpandRow(ByVal RowNumber As Integer)
'		' **********************************************************************************
'		' Process the expanding of Category sections.
'		'
'		' Inserts the appropriate Characterisric lines under a Category group.
'		' Use the 'Counter' column to determine whether in-fill has already occurred.
'		' 'Counter' value < 0 indicates in-fill has occurred.
'		'
'		' The 'Counter' column will contain the Index reference in the DataView at which Characterisrics
'		' relating to the selected Category & Instrument are located
'		' The List and Counter values are updated each time the Dataview is updated in order to keep
'		' these value in-sync.
'		'
'		' **********************************************************************************
'		Dim DataViewCounter As Integer
'		Dim thisRowView As DataRowView
'		Dim thisRow As RenaissanceDataClass.DSSelectCharacteristics.tblSelectCharacteristicsRow
'		Dim thisCategory As CharacteristicSummaryClass

'		Dim thisGridRow As C1.Win.C1FlexGrid.Row
'		Dim NewGridRow As C1.Win.C1FlexGrid.Row = Nothing
'		Dim CategoryGridRow As C1.Win.C1FlexGrid.Row = Nothing
'		Dim CategoryID As Integer
'		Dim InstrumentID As Integer
'		Dim CategoryListCounter As Integer
'		Dim thisCategoryListItem As CharacteristicSummaryClass
'		Dim thisCharcteristicListItem As CharacteristicSummaryClass
'		Dim StartCounter As Integer
'		Dim Expanded As Boolean
'		Dim ListCounter As Integer

'		Dim thisCategoryID As Integer

'		Dim FirstLine As Boolean = True
'		Dim UpdateRow As Integer = RowNumber + 1
'		Dim KeyVal As ULong

'		Dim OnlyEnteredCharacteristics As Boolean = Check_EnteredCharacteristics.Checked

'		' CategoryList

'		Try
'			' Get Instrument and CategoryIDs
'			thisGridRow = Grid_RiskBrowser.Rows(RowNumber)

'			CategoryID = CInt(thisGridRow(GridColumns.CategoryID))
'			InstrumentID = CInt(thisGridRow(GridColumns.InstrumentID))
'			Expanded = CBool(thisGridRow(GridColumns.Expanded))

'			' Are the IDs OK, has In-Fill already occured ?

'			If (Expanded = False) And (thisGridRow.IsNode) Then

'				If (thisGridRow.Node.Level = 1) Then

'					CategoryListCounter = CInt(thisGridRow(GridColumns.Counter))
'					thisCategoryListItem = CategoryList(CategoryListCounter)
'					StartCounter = thisCategoryListItem.DataViewCounter

'					If (StartCounter >= 0) AndAlso (InstrumentID > 0) Then

'						' Mark Instrument Line as having been filled : Set Expanded to True
'						thisGridRow(GridColumns.Expanded) = True

'						'' Validate StartCounter
'						'      If (StartCounter > (myDataView.Count - 1)) Then
'						'        Exit Sub
'						'      End If

'						' Add Lines.
'						For ListCounter = CategoryListCounter To (CategoryList.Count - 1)

'							' Get Category Row.
'							thisCategoryListItem = CategoryList(ListCounter)

'							If (thisCategoryListItem.InstrumentID = InstrumentID) And (CInt(thisCategoryListItem.DataCategoryID) > 0) Then

'								If (FirstLine) Or (thisCategoryListItem.DataCategoryID <> thisCategoryID) Then
'									' New Category

'									If (FirstLine = False) Then
'										' Add Line
'										UpdateRow += 1

'										Grid_RiskBrowser.Rows.Insert(UpdateRow)
'									End If

'									NewGridRow = Grid_RiskBrowser.Rows(UpdateRow)

'									' Fill in other Category details

'									NewGridRow.IsNode = True
'									NewGridRow.Node.Level = 2
'									NewGridRow.Node.Expanded = False

'									NewGridRow(GridColumns.FirstCol) = CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblRiskDataCategory, CInt(thisCategoryListItem.DataCategoryID), "DataCategory"))	' thisRow.DataCategory
'									NewGridRow(GridColumns.InstrumentID) = thisCategoryListItem.InstrumentID
'									NewGridRow(GridColumns.CategoryID) = thisCategoryListItem.DataCategoryID
'									NewGridRow(GridColumns.CharacteristicID) = 0
'									NewGridRow(GridColumns.DataWeighting) = 0
'									NewGridRow(GridColumns.RiskDataID) = 0
'									NewGridRow(GridColumns.Counter) = ListCounter
'									NewGridRow(GridColumns.Expanded) = False
'									Grid_RiskBrowser.SetCellStyle(NewGridRow.Index, GridColumns.DataWeighting, Grid_RiskBrowser.Styles("Weight_IntegerFormatMiddle"))
'									CategoryGridRow = NewGridRow

'									If (thisCategoryListItem.DataCharacteristicID > 0) Then
'										' Add Dummy Row - Facilitates in-fill later.
'										UpdateRow += 1
'										NewGridRow = Grid_RiskBrowser.Rows.Insert(UpdateRow)
'										NewGridRow(GridColumns.FirstCol) = ""
'										NewGridRow.IsNode = False
'									Else
'										' Ensure that it will not be expanded later
'										KeyVal = ((CULng(thisCategoryListItem.InstrumentID) << 32) + CULng(thisCategoryListItem.DataCategoryID))
'										ExpandedNodes(KeyVal) = False
'									End If

'									Grid_RiskBrowser.Rows(UpdateRow - 1).Node.Expanded = False

'								End If

'								If (CategoryGridRow IsNot Nothing) Then
'									CategoryGridRow(GridColumns.DataWeighting) = CInt(CategoryGridRow(GridColumns.DataWeighting)) + thisCategoryListItem.CharacteristicsCount
'								End If

'								FirstLine = False
'								thisCategoryID = thisCategoryListItem.DataCategoryID

'							Else
'								' Exit if we move on to a different Instrument
'								Exit Sub
'							End If

'						Next

'					End If

'				ElseIf (thisGridRow.Node.Level = 2) Then

'					CategoryListCounter = CInt(thisGridRow(GridColumns.Counter))
'					thisCategoryListItem = CategoryList(CategoryListCounter)
'					StartCounter = thisCategoryListItem.DataViewCounter

'					If (StartCounter >= 0) AndAlso (InstrumentID > 0) AndAlso (CategoryID > 0) Then

'						' Mark Risk Category Line as having been filled : Set Expanded to True
'						thisGridRow(GridColumns.Expanded) = True

'						' Validate StartCounter
'						If (StartCounter > (myDataView.Count - 1)) Then
'							Exit Sub
'						End If

'						' Add Lines.
'						Dim CharacteristicList As New ArrayList

'						For DataViewCounter = StartCounter To (myDataView.Count - 1)
'							thisRowView = myDataView.Item(DataViewCounter)
'							thisRow = thisRowView.Row

'							If (OnlyEnteredCharacteristics = False) Or (thisRow.RiskDataID > 0) Or (thisRow.DataWeighting <> 0.0) Then

'								thisCategoryID = thisRow.DataCategoryId

'								' If this Transaction is appropriate to the selected group....

'								If (thisRow.InstrumentID = InstrumentID) AndAlso (thisCategoryID = CategoryID) And (CInt(thisRow.DataCharacteristicId) > 0) Then
'									CharacteristicList.Add(New CharacteristicSummaryClass(thisRow.RiskDataID, thisRow.InstrumentID, "", thisRow.DataCategoryId, "", thisRow.DataCharacteristicId, CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblRiskDataCharacteristic, CInt(thisRow.DataCharacteristicId), "DataCharacteristic")), thisRow.DataWeighting, thisRow.DateEntered))
'								End If
'							End If

'						Next

'						CharacteristicList.Sort(New CategoryListComparerClass)

'						For ListCounter = 0 To (CharacteristicList.Count - 1)
'							thisCharcteristicListItem = CharacteristicList(ListCounter)

'							thisCategoryID = thisCharcteristicListItem.DataCategoryID

'							' If this Transaction is appropriate to the selected group....

'							If (thisCharcteristicListItem.InstrumentID = InstrumentID) AndAlso (thisCategoryID = CategoryID) And (CInt(thisCharcteristicListItem.DataCharacteristicID) > 0) Then

'								' If this is the first transaction, then overwrite the 'Dummy' grid line, else insert
'								' a new grid line.

'								If (FirstLine = False) Then
'									' Add Line
'									UpdateRow += 1

'									Grid_RiskBrowser.Rows.Insert(UpdateRow)
'									Grid_RiskBrowser.Rows(UpdateRow).IsNode = False
'								End If

'								' Set Values / Styles
'								thisGridRow = Grid_RiskBrowser.Rows(UpdateRow)

'								thisGridRow(GridColumns.FirstCol) = thisCharcteristicListItem.DataCharacteristic
'								thisGridRow(GridColumns.InstrumentID) = thisCharcteristicListItem.InstrumentID
'								thisGridRow(GridColumns.CategoryID) = thisCharcteristicListItem.DataCategoryID
'								thisGridRow(GridColumns.CharacteristicID) = thisCharcteristicListItem.DataCharacteristicID
'								thisGridRow(GridColumns.RiskDataID) = thisCharcteristicListItem.RiskDataID
'								thisGridRow(GridColumns.DataWeighting) = thisCharcteristicListItem.DataWeighting

'								If (thisCharcteristicListItem.RiskDataID) Then
'									If (thisCharcteristicListItem.DataWeighting >= 0) Then
'										Grid_RiskBrowser.SetCellStyle(UpdateRow, GridColumns.DataWeighting, Grid_RiskBrowser.Styles("Data"))
'									Else
'										Grid_RiskBrowser.SetCellStyle(UpdateRow, GridColumns.DataWeighting, Grid_RiskBrowser.Styles("DataNegative"))
'									End If
'								ElseIf (thisCharcteristicListItem.DataWeighting) Then
'									If (thisCharcteristicListItem.DataWeighting >= 0) Then
'										Grid_RiskBrowser.SetCellStyle(UpdateRow, GridColumns.DataWeighting, Grid_RiskBrowser.Styles("Data_Default"))
'									Else
'										Grid_RiskBrowser.SetCellStyle(UpdateRow, GridColumns.DataWeighting, Grid_RiskBrowser.Styles("DataNegative_Default"))
'									End If
'								End If

'								If (thisCharcteristicListItem.DateEntered <> Renaissance_BaseDate) Then
'									thisGridRow(GridColumns.LastUpdated) = thisCharcteristicListItem.DateEntered
'								End If

'								' Row Edited ?

'								If UpdatedCategoryEntries.Count > 0 Then
'									Dim UpdateCounter As Integer

'									For UpdateCounter = 0 To (UpdatedCategoryEntries.Count - 1)
'										thisCategory = UpdatedCategoryEntries(UpdateCounter)	'  CharacteristicSummaryClass

'										If (thisCategory.InstrumentID = thisCharcteristicListItem.InstrumentID) And (thisCategory.DataCategoryID = thisCharcteristicListItem.DataCategoryID) And (thisCategory.DataCharacteristicID = thisCharcteristicListItem.DataCharacteristicID) Then
'											thisGridRow(GridColumns.DataWeighting) = thisCategory.DataWeighting

'											If (thisCategory.DataWeighting >= 0) Then
'												Grid_RiskBrowser.SetCellStyle(UpdateRow, GridColumns.DataWeighting, Grid_RiskBrowser.Styles("UpdatedWeightPositive"))
'											Else
'												Grid_RiskBrowser.SetCellStyle(UpdateRow, GridColumns.DataWeighting, Grid_RiskBrowser.Styles("UpdatedWeightNegative"))
'											End If

'											Exit For
'										End If

'									Next UpdateCounter
'								End If

'								FirstLine = False
'							Else
'								' If the current transaction does not match, and we have added some lines already, then
'								' assume that we have come to the end of the section and exit.
'								' This logic works on the assumption that the DataView is ordered correctly by InstrumentID and CategoryID. 

'								If (FirstLine = False) Then
'									' If we have worked through the required Instrument / Category, then exit.
'									Exit Sub
'								End If

'							End If

'						Next ListCounter
'					End If

'				End If ' (thisGridRow.Node.Level = 2) Then

'			End If ' If (Expanded = False) And (thisGridRow.IsNode) Then

'		Catch ex As Exception

'		End Try

'	End Sub


'	Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
'		' **********************************************************************************
'		'
'		'
'		' **********************************************************************************

'		Try
'			' Hmmm, How are we goint to do this?
'			' Plan A is to take the Characteristic entries in 'UpdatedCategoryEntries', build a new tblRiskData and then update it.
'			' So here we go...


'			Dim UpdateCounter As Integer
'			Dim thisCategory As CharacteristicSummaryClass

'			Dim newTblRiskData As New RenaissanceDataClass.DSRiskData.tblRiskDataDataTable
'			Dim newTblRiskDataRow As RenaissanceDataClass.DSRiskData.tblRiskDataRow

'			For UpdateCounter = 0 To (UpdatedCategoryEntries.Count - 1)
'				thisCategory = UpdatedCategoryEntries(UpdateCounter)	'  CharacteristicSummaryClass

'				newTblRiskDataRow = newTblRiskData.NewtblRiskDataRow

'				newTblRiskDataRow.RiskDataID = thisCategory.RiskDataID
'				newTblRiskDataRow.InstrumentID = thisCategory.InstrumentID
'				newTblRiskDataRow.DataCharacteristicId = thisCategory.DataCharacteristicID
'				newTblRiskDataRow.DataWeighting = thisCategory.DataWeighting

'				newTblRiskData.AddtblRiskDataRow(newTblRiskDataRow)
'			Next UpdateCounter

'			UpdatedCategoryEntries.Clear()

'			Try
'				If (newTblRiskData.Rows.Count > 0) Then

'					MainForm.AdaptorUpdate("frmRiskBrowser, SaveCharacteristicChanges()", RenaissanceStandardDatasets.tblRiskData, newTblRiskData.Select())

'				End If
'			Catch ex As Exception
'				MainForm.LogError("frmRiskBrowser, SaveCharacteristicChanges()", LOG_LEVELS.Error, ex.Message, "Error saving Characteristic Changes.", ex.StackTrace, True)
'			End Try

'			Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblRiskData))

'		Catch ex As Exception
'		End Try
'	End Sub

'	Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
'		' **********************************************************************************
'		'
'		'
'		' **********************************************************************************

'		Try
'			UpdatedCategoryEntries.Clear()

'			PaintCharacteristicsGrid()

'		Catch ex As Exception
'		End Try

'	End Sub

'	Private Sub Context_AddNewCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Context_AddNewCategory.Click
'		' **********************************************************************************
'		' Add new Category.
'		'
'		' **********************************************************************************

'		Try
'			Dim NewCategoryName As String

'			' Get New Category name

'			NewCategoryName = InputBox("New Category Name", "New Category", "")

'			' If Name has length, i.e. 'OK' was pressed...

'			If (NewCategoryName.Length > 0) Then

'				' Create Category table and add new row.

'				Dim TblRiskCategory As New RenaissanceDataClass.DSRiskDataCategory.tblRiskDataCategoryDataTable
'				Dim newTblRiskCategoryRow As RenaissanceDataClass.DSRiskDataCategory.tblRiskDataCategoryRow

'				newTblRiskCategoryRow = TblRiskCategory.NewtblRiskDataCategoryRow

'				newTblRiskCategoryRow.DataCategoryId = 0
'				newTblRiskCategoryRow.DataCategory = NewCategoryName

'				TblRiskCategory.AddtblRiskDataCategoryRow(newTblRiskCategoryRow)

'				' Update table.

'				Try

'					MainForm.AdaptorUpdate("frmRiskBrowser, SaveNewCategory()", RenaissanceStandardDatasets.tblRiskDataCategory, TblRiskCategory.Select())

'				Catch ex As Exception
'					MainForm.LogError("frmRiskBrowser, SaveNewCategory()", LOG_LEVELS.Error, ex.Message, "Error saving New Category.", ex.StackTrace, True)
'				End Try

'				' Reload main Category table and cause update event.

'				Call MainForm.Load_Table(RenaissanceStandardDatasets.tblRiskDataCategory, True, True)

'			End If

'		Catch ex As Exception

'		End Try
'	End Sub

'	Private Sub Context_AddNewCharacteristic_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Context_AddNewCharacteristic.Click
'		' **********************************************************************************
'		' Add New Risk Data Characteristic from the Grid Context menu.
'		'
'		' **********************************************************************************
'		Try
'			If (LastEnteredGridRow > 0) And (LastEnteredGridRow < Grid_RiskBrowser.Rows.Count) Then

'				Dim thisRow As C1.Win.C1FlexGrid.Row = Grid_RiskBrowser.Rows(LastEnteredGridRow)

'				Dim NewCharacteristicName As String

'				NewCharacteristicName = InputBox("Enter the new Characteristic Name" & vbCrLf & "for category `" & CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblRiskDataCategory, CInt(thisRow(GridColumns.CategoryID)), "DataCategory")) & "`", "New Characteristic", "")

'				If (NewCharacteristicName.Length > 0) Then

'					Dim TblRiskCharacteristic As New RenaissanceDataClass.DSRiskDataCharacteristic.tblRiskDataCharacteristicDataTable
'					Dim newTblRiskCharacteristicRow As RenaissanceDataClass.DSRiskDataCharacteristic.tblRiskDataCharacteristicRow

'					newTblRiskCharacteristicRow = TblRiskCharacteristic.NewtblRiskDataCharacteristicRow

'					newTblRiskCharacteristicRow.DataCharacteristicId = 0
'					newTblRiskCharacteristicRow.DataCategoryId = thisRow(GridColumns.CategoryID)
'					newTblRiskCharacteristicRow.DataCharacteristic = NewCharacteristicName

'					TblRiskCharacteristic.AddtblRiskDataCharacteristicRow(newTblRiskCharacteristicRow)

'					Try

'						MainForm.AdaptorUpdate("frmRiskBrowser, SaveNewCharacteristic()", RenaissanceStandardDatasets.tblRiskDataCharacteristic, TblRiskCharacteristic.Select())

'					Catch ex As Exception
'						MainForm.LogError("frmRiskBrowser, SaveNewCharacteristic()", LOG_LEVELS.Error, ex.Message, "Error saving New Characteristic.", ex.StackTrace, True)
'					End Try

'					' Reload Characteristic table and cause update event.
'					Call MainForm.Load_Table(RenaissanceStandardDatasets.tblRiskDataCharacteristic, True, True)

'				End If

'			End If

'		Catch ex As Exception

'		End Try
'	End Sub

'	Private Sub Context_SetCharacteristicDefaultValue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Context_SetCharacteristicDefaultValue.Click
'		' **********************************************************************************
'		'
'		'
'		' **********************************************************************************

'		Try
'			If (LastEnteredGridRow > 0) And (LastEnteredGridRow < Grid_RiskBrowser.Rows.Count) Then
'				Dim thisRow As C1.Win.C1FlexGrid.Row = Grid_RiskBrowser.Rows(LastEnteredGridRow)
'				Dim thisCharacteristicRow As DSRiskDataCharacteristic.tblRiskDataCharacteristicRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblRiskDataCharacteristic, CInt(thisRow(GridColumns.CharacteristicID)))

'				Dim NewCharacteristicDefault As String
'				NewCharacteristicDefault = InputBox("Set Default Value :" & vbCrLf & "for Characteristic `" & CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblRiskDataCharacteristic, CInt(thisRow(GridColumns.CharacteristicID)), "DataCharacteristic")) & "`." & vbCrLf & "Type 'null' to clear an existing value.", "New Characteristic Default", "")

'				If (NewCharacteristicDefault.Length > 0) Then

'					If (NewCharacteristicDefault.ToLower = "null") Or (NewCharacteristicDefault.ToLower = "'null'") Then

'						thisCharacteristicRow.SetDefaultValueNull()

'					ElseIf ConvertIsNumeric(NewCharacteristicDefault) Then

'						thisCharacteristicRow.DefaultValue = CDbl(ConvertValue(NewCharacteristicDefault, GetType(Double)))

'					Else

'						Call MessageBox.Show("Sorry...", "Sorry, did not recognise that value as a number.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
'						Exit Sub

'					End If

'					MainForm.AdaptorUpdate("frmRiskBrowser", RenaissanceStandardDatasets.tblRiskDataCharacteristic, New System.Data.DataRow() {thisCharacteristicRow})

'					Call MainForm.Load_Table(RenaissanceStandardDatasets.tblRiskDataCharacteristic, True, True)

'				End If


'			End If

'		Catch ex As Exception
'			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error saving default characteristic value.", ex.StackTrace, True)
'		End Try
'	End Sub

'	Private Sub Context_SetCharacteristicFixedValue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Context_SetCharacteristicFixedValue.Click
'		' **********************************************************************************
'		'
'		'
'		' **********************************************************************************

'		Try
'			If (LastEnteredGridRow > 0) And (LastEnteredGridRow < Grid_RiskBrowser.Rows.Count) Then
'				Dim thisRow As C1.Win.C1FlexGrid.Row = Grid_RiskBrowser.Rows(LastEnteredGridRow)
'				Dim thisCharacteristicRow As DSRiskDataCharacteristic.tblRiskDataCharacteristicRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblRiskDataCharacteristic, CInt(thisRow(GridColumns.CharacteristicID)))

'				Dim NewCharacteristicDefault As String
'				NewCharacteristicDefault = InputBox("Set Fixed Value :" & vbCrLf & "for Characteristic `" & CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblRiskDataCharacteristic, CInt(thisRow(GridColumns.CharacteristicID)), "DataCharacteristic")) & "`." & vbCrLf & "Type 'null' to clear an existing value.", "New Characteristic Default", "")

'				If (NewCharacteristicDefault.Length > 0) Then

'					If (NewCharacteristicDefault.ToLower = "null") Or (NewCharacteristicDefault.ToLower = "'null'") Then

'						thisCharacteristicRow.SetFixedValueNull()

'					ElseIf ConvertIsNumeric(NewCharacteristicDefault) Then

'						thisCharacteristicRow.FixedValue = CDbl(ConvertValue(NewCharacteristicDefault, GetType(Double)))

'					Else

'						Call MessageBox.Show("Sorry...", "Sorry, did not recognise that value as a number.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
'						Exit Sub

'					End If

'					MainForm.AdaptorUpdate("frmRiskBrowser", RenaissanceStandardDatasets.tblRiskDataCharacteristic, New System.Data.DataRow() {thisCharacteristicRow})

'					Call MainForm.Load_Table(RenaissanceStandardDatasets.tblRiskDataCharacteristic, True, True)

'				End If


'			End If

'		Catch ex As Exception
'			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error saving fixed characteristic value.", ex.StackTrace, True)
'		End Try

'	End Sub


'	Private Sub Context_ExpandInstruments_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Context_ExpandInstruments.Click
'		' **********************************************************************************
'		' 
'		'
'		' **********************************************************************************

'		Try
'			Dim Counter As Integer
'			Dim thisGridRow As C1.Win.C1FlexGrid.Row
'			Grid_RiskBrowser.Visible = False

'			Counter = 1
'			While Counter < Grid_RiskBrowser.Rows.Count

'				Try
'					thisGridRow = Grid_RiskBrowser.Rows(Counter)

'					If (thisGridRow.IsNode) AndAlso (thisGridRow.Node.Level = 1) And (Not thisGridRow.Node.Expanded) Then
'						thisGridRow.Node.Expanded = True
'					End If
'				Catch Inner_Ex As Exception
'				End Try

'				If (Counter Mod 100) = 0 Then
'					StatusLabel_Characteristics.Text = "Expanding Instruments, " & CInt(Grid_RiskBrowser.Rows.Count - Counter).ToString & " to go..."
'					Application.DoEvents()
'				End If

'				Counter += 1
'			End While

'		Catch ex As Exception
'		Finally
'			StatusLabel_Characteristics.Text = ""
'			Grid_RiskBrowser.Visible = True
'		End Try
'	End Sub

'	Private Sub Context_ColapseInstruments_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Context_ColapseInstruments.Click
'		' **********************************************************************************
'		' 
'		'
'		' **********************************************************************************

'		Try
'			Dim Counter As Integer
'			Dim thisGridRow As C1.Win.C1FlexGrid.Row

'			For Counter = (Grid_RiskBrowser.Rows.Count - 1) To 1 Step -1
'				StatusLabel_Characteristics.Text = "Collapsing, " & Counter.ToString & " to go..."

'				Try
'					thisGridRow = Grid_RiskBrowser.Rows(Counter)

'					If (thisGridRow.IsNode) AndAlso (thisGridRow.Node.Level >= 1) And (thisGridRow.Node.Expanded) Then
'						thisGridRow.Node.Expanded = False
'					End If
'				Catch Inner_Ex As Exception
'				End Try

'				If (Counter Mod 100) = 0 Then
'					Application.DoEvents()
'				End If
'			Next
'		Catch ex As Exception
'		End Try
'	End Sub

'	Private Sub Context_ExpandCategories_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Context_ExpandCategories.Click
'		' **********************************************************************************
'		' 
'		'
'		' **********************************************************************************

'		Try
'			If MessageBox.Show("Confirm", "Are you sure? This may take a while...", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.OK Then
'				Context_ExpandInstruments_Click(sender, e)

'				Dim Counter As Integer
'				Dim thisGridRow As C1.Win.C1FlexGrid.Row

'				For Counter = (Grid_RiskBrowser.Rows.Count - 1) To 1 Step -1
'					StatusLabel_Characteristics.Text = "Expanding Categories, " & Counter.ToString & " to go..."

'					Try
'						thisGridRow = Grid_RiskBrowser.Rows(Counter)

'						If (thisGridRow.IsNode) AndAlso (thisGridRow.Node.Level >= 2) And (Not thisGridRow.Node.Expanded) Then
'							thisGridRow.Node.Expanded = True
'						End If
'					Catch Inner_Ex As Exception
'					End Try

'					If (Counter Mod 100) = 0 Then
'						Application.DoEvents()
'					End If
'				Next
'			End If

'		Catch ex As Exception
'		End Try
'	End Sub

'	Private Sub Context_ColapseCategories_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Context_ColapseCategories.Click
'		' **********************************************************************************
'		' 
'		'
'		' **********************************************************************************

'		Try
'			Dim Counter As Integer
'			Dim thisGridRow As C1.Win.C1FlexGrid.Row

'			For Counter = (Grid_RiskBrowser.Rows.Count - 1) To 1 Step -1
'				StatusLabel_Characteristics.Text = "Collapsing, " & Counter.ToString & " to go..."

'				Try
'					thisGridRow = Grid_RiskBrowser.Rows(Counter)

'					If (thisGridRow.IsNode) AndAlso (thisGridRow.Node.Level >= 2) And (thisGridRow.Node.Expanded) Then
'						thisGridRow.Node.Expanded = False
'					End If
'				Catch Inner_Ex As Exception
'				End Try

'				If (Counter Mod 100) = 0 Then
'					Application.DoEvents()
'				End If
'			Next
'		Catch ex As Exception
'		End Try

'	End Sub

'	Private Sub Context_SaveChanges_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Context_SaveChanges.Click
'		' **********************************************************************************
'		' Save SGrid changes
'		'
'		' **********************************************************************************

'		btnSave_Click(sender, e)
'	End Sub

'	Private Sub Context_CancelChanges_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Context_CancelChanges.Click
'		' **********************************************************************************
'		' Cancel grid changes
'		'
'		' **********************************************************************************

'		btnCancel_Click(sender, e)
'	End Sub

'	Private Sub Grid_Characteristics_MouseEnterCell(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles Grid_RiskBrowser.MouseEnterCell
'		' **********************************************************************************
'		' Record cell just entered and re-arrange the context menu according to the type
'		' of Grid Row.
'		' **********************************************************************************

'		Try
'			LastEnteredGridRow = e.Row
'			LastEnteredGridCol = e.Col

'			Dim enableAddCharacteristic As Boolean = False
'			Dim enableSetCharacteristicValues As Boolean = False
'			Dim enableRename As Boolean = False
'			Dim enableDelete As Boolean = False

'			If (e.Row <= 0) Then
'				' Header row, Primonial Row
'				Grid_RiskBrowser.ContextMenuStrip = Nothing
'			Else
'				Dim thisRow As C1.Win.C1FlexGrid.Row = Grid_RiskBrowser.Rows(LastEnteredGridRow)

'				If (Grid_RiskBrowser.ContextMenuStrip Is Nothing) Then
'					Grid_RiskBrowser.ContextMenuStrip = Me.ContextMenu_GridRiskBrowser
'				End If

'				If (thisRow.IsNode) Then
'					If (thisRow.Node.Level <= 1) Then
'						' Node Level = 1 : Instrument

'					Else
'						' Node Level = 2 : Category

'						enableAddCharacteristic = True
'						enableRename = True
'						enableDelete = True

'						Context_RenameLabel.Text = "Rename Category" '  `" & CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblRiskDataCategory, CInt(thisRow(GridColumns.CategoryID)), "DataCategory")) & "`"
'						Context_RenameTextBox.Text = CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblRiskDataCategory, CInt(thisRow(GridColumns.CategoryID)), "DataCategory"))
'						Context_Delete.Text = "Delete `" & CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblRiskDataCategory, CInt(thisRow(GridColumns.CategoryID)), "DataCategory")) & "`"
'						GridRenameItem = "CATEGORY"
'						GridDeleteItem = "CATEGORY"

'					End If
'				Else
'					' Not a Node : Characteristic

'					enableAddCharacteristic = True
'					enableSetCharacteristicValues = True
'					enableRename = True
'					enableDelete = True

'					Context_RenameLabel.Text = "Rename Characteristic" '  `" & CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblRiskDataCharacteristic, CInt(thisRow(GridColumns.CharacteristicID)), "DataCharacteristic")) & "`"
'					Context_RenameTextBox.Text = CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblRiskDataCharacteristic, CInt(thisRow(GridColumns.CharacteristicID)), "DataCharacteristic"))
'					Context_Delete.Text = "Delete `" & CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblRiskDataCharacteristic, CInt(thisRow(GridColumns.CharacteristicID)), "DataCharacteristic")) & "`"
'					GridRenameItem = "CHARACTERISTIC"
'					GridDeleteItem = "CHARACTERISTIC"

'				End If
'			End If

'			' Show / Hide menu options.

'			Context_AddNewCharacteristic.Visible = enableAddCharacteristic
'			Context_RenameSeparator.Visible = enableRename
'			Context_RenameLabel.Visible = enableRename
'			Context_RenameTextBox.Visible = enableRename
'			Context_SeparatorDelete.Visible = enableDelete
'			Context_Delete.Visible = enableDelete

'			Context_CharacteristicValueSeparator.Visible = enableSetCharacteristicValues
'			Context_SetCharacteristicDefaultValue.Visible = enableSetCharacteristicValues
'			Context_SetCharacteristicFixedValue.Visible = enableSetCharacteristicValues

'			GridRenameFocus = False

'		Catch ex As Exception
'		End Try
'	End Sub

'	Private Sub Grid_Characteristics_AfterScroll(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RangeEventArgs) Handles Grid_RiskBrowser.AfterScroll
'		Try
'			If InPaint = False Then
'				LastGridScroll = Grid_RiskBrowser.ScrollPosition
'			End If
'		Catch ex As Exception

'		End Try
'	End Sub

'	Private Sub Context_RenameTextBox_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Context_RenameTextBox.GotFocus

'	End Sub

'	Private Sub Context_RenameTextBox_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Context_RenameTextBox.KeyDown
'		Try
'			If GridRenameFocus Then
'				If e.KeyCode = Keys.Enter Then
'					Grid_Rename_Item(sender)
'				End If
'			End If
'		Catch ex As Exception

'		End Try
'	End Sub


'	Private Sub Context_RenameTextBox_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Context_RenameTextBox.LostFocus
'		Try
'			If GridRenameFocus Then
'				Grid_Rename_Item(sender)
'			End If
'		Catch ex As Exception

'		End Try
'	End Sub

'	Private Sub Context_RenameTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Context_RenameTextBox.TextChanged
'		Try
'			GridRenameFocus = True
'		Catch ex As Exception
'		End Try
'	End Sub

'	Private Sub ContextMenu_GridCharacteristics_Opened(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContextMenu_GridRiskBrowser.Opened

'		Try
'			GridRenameFocus = False
'		Catch ex As Exception
'		End Try
'	End Sub

'	Private Function Grid_Rename_Item(ByVal sender As Object) As Boolean
'		' **********************************************************************************
'		'
'		'
'		' **********************************************************************************

'		Try
'			If GridRenameFocus And (LastEnteredGridRow > 0) And (LastEnteredGridRow < Grid_RiskBrowser.Rows.Count) Then
'				GridRenameFocus = False

'				Dim thisRow As C1.Win.C1FlexGrid.Row = Grid_RiskBrowser.Rows(LastEnteredGridRow)

'				If GridRenameItem = "CATEGORY" Then

'					If MessageBox.Show("Rename Category" & vbCrLf & "`" & CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblRiskDataCategory, CInt(thisRow(GridColumns.CategoryID)), "DataCategory")) & "`" & vbCrLf & "to" & vbCrLf & "`" & Context_RenameTextBox.Text & "`", "Rename this Category ?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button3) = Windows.Forms.DialogResult.Yes Then

'						Dim thisCategoryRow As DSRiskDataCategory.tblRiskDataCategoryRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblRiskDataCategory, CInt(thisRow(GridColumns.CategoryID)))

'						If (thisCategoryRow IsNot Nothing) Then
'							thisCategoryRow.DataCategory = Context_RenameTextBox.Text

'							MainForm.AdaptorUpdate("frmRiskBrowser", RenaissanceStandardDatasets.tblRiskDataCategory, New System.Data.DataRow() {thisCategoryRow})

'							Call MainForm.Load_Table(RenaissanceStandardDatasets.tblRiskDataCategory, True, True)

'						End If

'					End If

'				ElseIf GridRenameItem = "CHARACTERISTIC" Then

'					If MessageBox.Show("Rename Characteristic" & vbCrLf & "`" & CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblRiskDataCharacteristic, CInt(thisRow(GridColumns.CharacteristicID)), "DataCharacteristic")) & "`" & vbCrLf & "to" & vbCrLf & "`" & Context_RenameTextBox.Text & "`", "Rename this Characteristic ?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button3) = Windows.Forms.DialogResult.Yes Then

'						Dim thisCharacteristicRow As DSRiskDataCharacteristic.tblRiskDataCharacteristicRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblRiskDataCharacteristic, CInt(thisRow(GridColumns.CharacteristicID)))

'						If (thisCharacteristicRow IsNot Nothing) Then
'							thisCharacteristicRow.DataCharacteristic = Context_RenameTextBox.Text

'							MainForm.AdaptorUpdate("frmRiskBrowser", RenaissanceStandardDatasets.tblRiskDataCharacteristic, New System.Data.DataRow() {thisCharacteristicRow})

'							Call MainForm.Load_Table(RenaissanceStandardDatasets.tblRiskDataCharacteristic, True, True)

'						End If

'					End If

'				End If

'			End If

'		Catch ex As Exception
'		Finally
'			ContextMenu_GridRiskBrowser.Hide()
'		End Try

'		Return True
'	End Function

'	Private Sub Context_Delete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Context_Delete.Click
'		' **********************************************************************************
'		'
'		'
'		' **********************************************************************************

'		Try
'			If (LastEnteredGridRow > 0) And (LastEnteredGridRow < Grid_RiskBrowser.Rows.Count) Then
'				GridRenameFocus = False

'				Dim thisRow As C1.Win.C1FlexGrid.Row = Grid_RiskBrowser.Rows(LastEnteredGridRow)

'				If GridRenameItem = "CATEGORY" Then

'					If MessageBox.Show("Delete Category" & vbCrLf & "`" & CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblRiskDataCategory, CInt(thisRow(GridColumns.CategoryID)), "DataCategory")) & "` ?", "Delete Category ?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button3) = Windows.Forms.DialogResult.Yes Then

'						Dim thisCategoryRow As DSRiskDataCategory.tblRiskDataCategoryRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblRiskDataCategory, thisRow(GridColumns.CategoryID))

'						If (thisCategoryRow IsNot Nothing) Then
'							thisCategoryRow.Delete()

'							MainForm.AdaptorUpdate("frmRiskBrowser", RenaissanceStandardDatasets.tblRiskDataCategory, New System.Data.DataRow() {thisCategoryRow})

'							Call MainForm.Load_Table(RenaissanceStandardDatasets.tblRiskDataCategory, True, True)

'						End If

'					End If

'				ElseIf GridRenameItem = "CHARACTERISTIC" Then

'					If MessageBox.Show("Delete Characteristic" & vbCrLf & "`" & CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblRiskDataCharacteristic, CInt(thisRow(GridColumns.CharacteristicID)), "DataCharacteristic")) & "` ?", "Delete Characteristic ?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button3) = Windows.Forms.DialogResult.Yes Then

'						Dim thisCharacteristicRow As DSRiskDataCharacteristic.tblRiskDataCharacteristicRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblRiskDataCharacteristic, thisRow(GridColumns.CharacteristicID))

'						If (thisCharacteristicRow IsNot Nothing) Then
'							thisCharacteristicRow.Delete()

'							MainForm.AdaptorUpdate("frmRiskBrowser", RenaissanceStandardDatasets.tblRiskDataCharacteristic, New System.Data.DataRow() {thisCharacteristicRow})

'							Call MainForm.Load_Table(RenaissanceStandardDatasets.tblRiskDataCharacteristic, True, True)

'						End If

'					End If

'				End If

'			End If

'		Catch ex As Exception
'		Finally
'			ContextMenu_GridRiskBrowser.Hide()
'		End Try

'	End Sub




'End Class
