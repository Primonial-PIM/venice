' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmSelectCompoundTransaction.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals

Imports RenaissanceDataClass
Imports C1.Win.C1FlexGrid

''' <summary>
''' Class frmSelectCompoundTransaction
''' </summary>
Public Class frmSelectCompoundTransaction

	Inherits System.Windows.Forms.Form
	Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmSelectCompoundTransaction"/> class from being created.
    ''' </summary>
	Private Sub New()
		MyBase.New()

		'This call is required by the Windows Form Designer.
		InitializeComponent()

		'Add any initialization after the InitializeComponent() call

	End Sub

	'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
    ''' <summary>
    ''' The combo_ transaction fund
    ''' </summary>
	Friend WithEvents Combo_TransactionFund As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label_ cpty is fund
    ''' </summary>
	Friend WithEvents label_CptyIsFund As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ transaction instrument
    ''' </summary>
	Friend WithEvents Combo_TransactionInstrument As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label2
    ''' </summary>
	Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label1
    ''' </summary>
	Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label3
    ''' </summary>
	Friend WithEvents Label3 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ ticket
    ''' </summary>
	Friend WithEvents Combo_Ticket As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The date_ value date
    ''' </summary>
	Friend WithEvents Date_ValueDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The combo_ ticket operator
    ''' </summary>
	Friend WithEvents Combo_TicketOperator As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ value date operator
    ''' </summary>
	Friend WithEvents Combo_ValueDateOperator As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The grid_ transactions
    ''' </summary>
	Friend WithEvents Grid_Transactions As C1.Win.C1FlexGrid.C1FlexGrid
    ''' <summary>
    ''' The combo_ field select1
    ''' </summary>
	Friend WithEvents Combo_FieldSelect1 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select1_ operator
    ''' </summary>
	Friend WithEvents Combo_Select1_Operator As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select1_ value
    ''' </summary>
	Friend WithEvents Combo_Select1_Value As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select2_ value
    ''' </summary>
	Friend WithEvents Combo_Select2_Value As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ field select2
    ''' </summary>
	Friend WithEvents Combo_FieldSelect2 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select2_ operator
    ''' </summary>
	Friend WithEvents Combo_Select2_Operator As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select3_ value
    ''' </summary>
	Friend WithEvents Combo_Select3_Value As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ field select3
    ''' </summary>
	Friend WithEvents Combo_FieldSelect3 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select3_ operator
    ''' </summary>
	Friend WithEvents Combo_Select3_Operator As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ and or_1
    ''' </summary>
	Friend WithEvents Combo_AndOr_1 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ and or_2
    ''' </summary>
	Friend WithEvents Combo_AndOr_2 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The status bar_ select compound transactions
    ''' </summary>
	Friend WithEvents StatusBar_SelectCompoundTransactions As System.Windows.Forms.StatusBar
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Me.Grid_Transactions = New C1.Win.C1FlexGrid.C1FlexGrid
		Me.Combo_TransactionFund = New System.Windows.Forms.ComboBox
		Me.label_CptyIsFund = New System.Windows.Forms.Label
		Me.Combo_TransactionInstrument = New System.Windows.Forms.ComboBox
		Me.Label2 = New System.Windows.Forms.Label
		Me.Combo_Ticket = New System.Windows.Forms.ComboBox
		Me.Label1 = New System.Windows.Forms.Label
		Me.Label3 = New System.Windows.Forms.Label
		Me.Date_ValueDate = New System.Windows.Forms.DateTimePicker
		Me.Combo_TicketOperator = New System.Windows.Forms.ComboBox
		Me.Combo_ValueDateOperator = New System.Windows.Forms.ComboBox
		Me.Combo_Select1_Operator = New System.Windows.Forms.ComboBox
		Me.Combo_FieldSelect1 = New System.Windows.Forms.ComboBox
		Me.Combo_Select1_Value = New System.Windows.Forms.ComboBox
		Me.Combo_Select2_Value = New System.Windows.Forms.ComboBox
		Me.Combo_FieldSelect2 = New System.Windows.Forms.ComboBox
		Me.Combo_Select2_Operator = New System.Windows.Forms.ComboBox
		Me.Combo_Select3_Value = New System.Windows.Forms.ComboBox
		Me.Combo_FieldSelect3 = New System.Windows.Forms.ComboBox
		Me.Combo_Select3_Operator = New System.Windows.Forms.ComboBox
		Me.Combo_AndOr_1 = New System.Windows.Forms.ComboBox
		Me.Combo_AndOr_2 = New System.Windows.Forms.ComboBox
		Me.StatusBar_SelectCompoundTransactions = New System.Windows.Forms.StatusBar
		CType(Me.Grid_Transactions, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.SuspendLayout()
		'
		'Grid_Transactions
		'
		Me.Grid_Transactions.AllowEditing = False
		Me.Grid_Transactions.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
								Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Grid_Transactions.AutoClipboard = True
		Me.Grid_Transactions.CausesValidation = False
		Me.Grid_Transactions.ColumnInfo = "5,1,0,0,0,85,Columns:"
		Me.Grid_Transactions.EditOptions = C1.Win.C1FlexGrid.EditFlags.None
		Me.Grid_Transactions.KeyActionTab = C1.Win.C1FlexGrid.KeyActionEnum.MoveAcross
		Me.Grid_Transactions.Location = New System.Drawing.Point(4, 224)
		Me.Grid_Transactions.Name = "Grid_Transactions"
		Me.Grid_Transactions.Rows.DefaultSize = 17
		Me.Grid_Transactions.Size = New System.Drawing.Size(880, 412)
		Me.Grid_Transactions.TabIndex = 17
		'
		'Combo_TransactionFund
		'
		Me.Combo_TransactionFund.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_TransactionFund.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_TransactionFund.Location = New System.Drawing.Point(128, 12)
		Me.Combo_TransactionFund.Name = "Combo_TransactionFund"
		Me.Combo_TransactionFund.Size = New System.Drawing.Size(752, 21)
		Me.Combo_TransactionFund.TabIndex = 0
		'
		'label_CptyIsFund
		'
		Me.label_CptyIsFund.Location = New System.Drawing.Point(16, 16)
		Me.label_CptyIsFund.Name = "label_CptyIsFund"
		Me.label_CptyIsFund.Size = New System.Drawing.Size(104, 16)
		Me.label_CptyIsFund.TabIndex = 82
		Me.label_CptyIsFund.Text = "Fund"
		'
		'Combo_TransactionInstrument
		'
		Me.Combo_TransactionInstrument.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_TransactionInstrument.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_TransactionInstrument.Location = New System.Drawing.Point(128, 40)
		Me.Combo_TransactionInstrument.Name = "Combo_TransactionInstrument"
		Me.Combo_TransactionInstrument.Size = New System.Drawing.Size(752, 21)
		Me.Combo_TransactionInstrument.TabIndex = 1
		'
		'Label2
		'
		Me.Label2.Location = New System.Drawing.Point(16, 44)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(104, 16)
		Me.Label2.TabIndex = 84
		Me.Label2.Text = "Instrument"
		'
		'Combo_Ticket
		'
		Me.Combo_Ticket.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_Ticket.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_Ticket.Location = New System.Drawing.Point(228, 68)
		Me.Combo_Ticket.Name = "Combo_Ticket"
		Me.Combo_Ticket.Size = New System.Drawing.Size(652, 21)
		Me.Combo_Ticket.TabIndex = 3
		'
		'Label1
		'
		Me.Label1.Location = New System.Drawing.Point(16, 72)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(104, 16)
		Me.Label1.TabIndex = 86
		Me.Label1.Text = "Ticket"
		'
		'Label3
		'
		Me.Label3.Location = New System.Drawing.Point(16, 100)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(104, 16)
		Me.Label3.TabIndex = 88
		Me.Label3.Text = "ValueDate"
		'
		'Date_ValueDate
		'
		Me.Date_ValueDate.Location = New System.Drawing.Point(228, 96)
		Me.Date_ValueDate.Name = "Date_ValueDate"
		Me.Date_ValueDate.Size = New System.Drawing.Size(174, 20)
		Me.Date_ValueDate.TabIndex = 5
		'
		'Combo_TicketOperator
		'
		Me.Combo_TicketOperator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.Combo_TicketOperator.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_TicketOperator.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like"})
		Me.Combo_TicketOperator.Location = New System.Drawing.Point(128, 68)
		Me.Combo_TicketOperator.Name = "Combo_TicketOperator"
		Me.Combo_TicketOperator.Size = New System.Drawing.Size(96, 21)
		Me.Combo_TicketOperator.TabIndex = 2
		'
		'Combo_ValueDateOperator
		'
		Me.Combo_ValueDateOperator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.Combo_ValueDateOperator.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_ValueDateOperator.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", ""})
		Me.Combo_ValueDateOperator.Location = New System.Drawing.Point(128, 96)
		Me.Combo_ValueDateOperator.Name = "Combo_ValueDateOperator"
		Me.Combo_ValueDateOperator.Size = New System.Drawing.Size(96, 21)
		Me.Combo_ValueDateOperator.TabIndex = 4
		'
		'Combo_Select1_Operator
		'
		Me.Combo_Select1_Operator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.Combo_Select1_Operator.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_Select1_Operator.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like"})
		Me.Combo_Select1_Operator.Location = New System.Drawing.Point(272, 124)
		Me.Combo_Select1_Operator.Name = "Combo_Select1_Operator"
		Me.Combo_Select1_Operator.Size = New System.Drawing.Size(96, 21)
		Me.Combo_Select1_Operator.TabIndex = 7
		'
		'Combo_FieldSelect1
		'
		Me.Combo_FieldSelect1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.Combo_FieldSelect1.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_FieldSelect1.Location = New System.Drawing.Point(128, 124)
		Me.Combo_FieldSelect1.Name = "Combo_FieldSelect1"
		Me.Combo_FieldSelect1.Size = New System.Drawing.Size(136, 21)
		Me.Combo_FieldSelect1.Sorted = True
		Me.Combo_FieldSelect1.TabIndex = 6
		'
		'Combo_Select1_Value
		'
		Me.Combo_Select1_Value.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_Select1_Value.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_Select1_Value.Location = New System.Drawing.Point(376, 124)
		Me.Combo_Select1_Value.Name = "Combo_Select1_Value"
		Me.Combo_Select1_Value.Size = New System.Drawing.Size(216, 21)
		Me.Combo_Select1_Value.TabIndex = 8
		'
		'Combo_Select2_Value
		'
		Me.Combo_Select2_Value.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_Select2_Value.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_Select2_Value.Location = New System.Drawing.Point(376, 156)
		Me.Combo_Select2_Value.Name = "Combo_Select2_Value"
		Me.Combo_Select2_Value.Size = New System.Drawing.Size(216, 21)
		Me.Combo_Select2_Value.TabIndex = 12
		'
		'Combo_FieldSelect2
		'
		Me.Combo_FieldSelect2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.Combo_FieldSelect2.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_FieldSelect2.Location = New System.Drawing.Point(128, 156)
		Me.Combo_FieldSelect2.Name = "Combo_FieldSelect2"
		Me.Combo_FieldSelect2.Size = New System.Drawing.Size(136, 21)
		Me.Combo_FieldSelect2.Sorted = True
		Me.Combo_FieldSelect2.TabIndex = 10
		'
		'Combo_Select2_Operator
		'
		Me.Combo_Select2_Operator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.Combo_Select2_Operator.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_Select2_Operator.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like"})
		Me.Combo_Select2_Operator.Location = New System.Drawing.Point(272, 156)
		Me.Combo_Select2_Operator.Name = "Combo_Select2_Operator"
		Me.Combo_Select2_Operator.Size = New System.Drawing.Size(96, 21)
		Me.Combo_Select2_Operator.TabIndex = 11
		'
		'Combo_Select3_Value
		'
		Me.Combo_Select3_Value.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_Select3_Value.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_Select3_Value.Location = New System.Drawing.Point(376, 188)
		Me.Combo_Select3_Value.Name = "Combo_Select3_Value"
		Me.Combo_Select3_Value.Size = New System.Drawing.Size(216, 21)
		Me.Combo_Select3_Value.TabIndex = 16
		'
		'Combo_FieldSelect3
		'
		Me.Combo_FieldSelect3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.Combo_FieldSelect3.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_FieldSelect3.Location = New System.Drawing.Point(128, 188)
		Me.Combo_FieldSelect3.Name = "Combo_FieldSelect3"
		Me.Combo_FieldSelect3.Size = New System.Drawing.Size(136, 21)
		Me.Combo_FieldSelect3.Sorted = True
		Me.Combo_FieldSelect3.TabIndex = 14
		'
		'Combo_Select3_Operator
		'
		Me.Combo_Select3_Operator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.Combo_Select3_Operator.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_Select3_Operator.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like"})
		Me.Combo_Select3_Operator.Location = New System.Drawing.Point(272, 188)
		Me.Combo_Select3_Operator.Name = "Combo_Select3_Operator"
		Me.Combo_Select3_Operator.Size = New System.Drawing.Size(96, 21)
		Me.Combo_Select3_Operator.TabIndex = 15
		'
		'Combo_AndOr_1
		'
		Me.Combo_AndOr_1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_AndOr_1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.Combo_AndOr_1.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_AndOr_1.Items.AddRange(New Object() {"AND", "OR"})
		Me.Combo_AndOr_1.Location = New System.Drawing.Point(604, 124)
		Me.Combo_AndOr_1.Name = "Combo_AndOr_1"
		Me.Combo_AndOr_1.Size = New System.Drawing.Size(96, 21)
		Me.Combo_AndOr_1.TabIndex = 9
		'
		'Combo_AndOr_2
		'
		Me.Combo_AndOr_2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_AndOr_2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.Combo_AndOr_2.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_AndOr_2.Items.AddRange(New Object() {"AND", "OR"})
		Me.Combo_AndOr_2.Location = New System.Drawing.Point(604, 156)
		Me.Combo_AndOr_2.Name = "Combo_AndOr_2"
		Me.Combo_AndOr_2.Size = New System.Drawing.Size(96, 21)
		Me.Combo_AndOr_2.TabIndex = 13
		'
		'StatusBar_SelectCompoundTransactions
		'
		Me.StatusBar_SelectCompoundTransactions.Location = New System.Drawing.Point(0, 642)
		Me.StatusBar_SelectCompoundTransactions.Name = "StatusBar_SelectCompoundTransactions"
		Me.StatusBar_SelectCompoundTransactions.Size = New System.Drawing.Size(888, 16)
		Me.StatusBar_SelectCompoundTransactions.TabIndex = 103
		Me.StatusBar_SelectCompoundTransactions.Text = "StatusBar1"
		'
		'frmSelectCompoundTransaction
		'
		Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
		Me.ClientSize = New System.Drawing.Size(888, 658)
		Me.Controls.Add(Me.StatusBar_SelectCompoundTransactions)
		Me.Controls.Add(Me.Combo_AndOr_2)
		Me.Controls.Add(Me.Combo_AndOr_1)
		Me.Controls.Add(Me.Combo_Select3_Value)
		Me.Controls.Add(Me.Combo_FieldSelect3)
		Me.Controls.Add(Me.Combo_Select3_Operator)
		Me.Controls.Add(Me.Combo_Select2_Value)
		Me.Controls.Add(Me.Combo_FieldSelect2)
		Me.Controls.Add(Me.Combo_Select2_Operator)
		Me.Controls.Add(Me.Combo_Select1_Value)
		Me.Controls.Add(Me.Combo_FieldSelect1)
		Me.Controls.Add(Me.Combo_Select1_Operator)
		Me.Controls.Add(Me.Combo_ValueDateOperator)
		Me.Controls.Add(Me.Combo_TicketOperator)
		Me.Controls.Add(Me.Date_ValueDate)
		Me.Controls.Add(Me.Label3)
		Me.Controls.Add(Me.Combo_Ticket)
		Me.Controls.Add(Me.Label1)
		Me.Controls.Add(Me.Combo_TransactionInstrument)
		Me.Controls.Add(Me.Label2)
		Me.Controls.Add(Me.Combo_TransactionFund)
		Me.Controls.Add(Me.label_CptyIsFund)
		Me.Controls.Add(Me.Grid_Transactions)
		Me.Name = "frmSelectCompoundTransaction"
		Me.Text = "Select Compound Transactions"
		CType(Me.Grid_Transactions, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)

	End Sub

#End Region

#Region " Form Locals and Constants "

	' Form 'Parent', the Main Venice form.
	' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
	Private WithEvents _MainForm As VeniceMain

	' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
	Private FormTooltip As New ToolTip()

	' Form Constants, specific to the table being updated.

    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
	Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

	' Form Locals, initialised on 'New' defining what standard data items to use
    ''' <summary>
  ''' Variable holding the form's data table name. taken from the "ThisStandardDataset" object.
    ''' </summary>
	Private THIS_TABLENAME As String
    ''' <summary>
  ''' Variable holding the form's data adaptor name. taken from the "ThisStandardDataset" object.
    ''' </summary>
	Private THIS_ADAPTORNAME As String
    ''' <summary>
  ''' Variable holding the form's dataset name. taken from the "ThisStandardDataset" object.
    ''' </summary>
	Private THIS_DATASETNAME As String


	' The standard ChangeID for this form. e.g. tblFund
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
	Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

	' Form Specific Order fields
    ''' <summary>
  ''' Field Name to order the Select combo by.
    ''' </summary>
	Private THIS_FORM_OrderBy As String

	' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
	Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
	Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

	' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
	Private THIS_FORM_FormID As VeniceFormID

	' Data Structures

    ''' <summary>
    ''' My dataset
    ''' </summary>
	Private myDataset As DataSet
    ''' <summary>
    ''' My table
    ''' </summary>
	Private myTable As DataTable
    ''' <summary>
    ''' My connection
    ''' </summary>
	Private myConnection As SqlConnection
    ''' <summary>
    ''' My adaptor
    ''' </summary>
	Private myAdaptor As SqlDataAdapter
    ''' <summary>
    ''' My data view
    ''' </summary>
	Private myDataView As DataView

    ''' <summary>
    ''' The this standard dataset
    ''' </summary>
	Private ThisStandardDataset As RenaissanceGlobals.StandardDataset

	' Active Element.

    ''' <summary>
    ''' The __ is over cancel button
    ''' </summary>
	Private __IsOverCancelButton As Boolean
    ''' <summary>
    ''' The _ in use
    ''' </summary>
	Private _InUse As Boolean

	' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
	Private FormIsValid As Boolean
    ''' <summary>
    ''' The in paint
    ''' </summary>
	Private InPaint As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
	Private _FormOpenFailed As Boolean

	' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
	Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
	Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
	Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
	Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
	Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
	Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return __IsOverCancelButton
		End Get
		Set(ByVal Value As Boolean)
			__IsOverCancelButton = Value
		End Set
	End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
	Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
		Get
			Return InPaint
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
		Get
			Return _InUse
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmSelectCompoundTransaction"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
	Public Sub New(ByVal pMainForm As VeniceMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()



		_MainForm = pMainForm
		AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

		_FormOpenFailed = False
		_InUse = True

		' Default Select and Order fields.

		THIS_FORM_OrderBy = "TransactionFund, TransactionInstrument"

		' Form Permissioning :-

		THIS_FORM_PermissionArea = Me.Name
		THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

		' 'This' form ID

		THIS_FORM_FormID = VeniceFormID.frmSelectCompoundTransaction

		' This form's dataset type.

		ThisStandardDataset = RenaissanceStandardDatasets.tblSelectCompoundTransaction ' This Defines the Form Data !!! 


		' Data object names standard to this Form type.

		THIS_TABLENAME = ThisStandardDataset.TableName
		THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
		THIS_DATASETNAME = ThisStandardDataset.DatasetName

		THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

		' Establish / Retrieve data objects for this form.

		myConnection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)
		myAdaptor = MainForm.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, VENICE_CONNECTION, THIS_TABLENAME)
		myDataset = MainForm.Load_Table(ThisStandardDataset, False)
		myTable = myDataset.Tables(0)

		' Establish initial DataView and Initialise Transactions Grig.

		Dim thisCol As C1.Win.C1FlexGrid.Column
		Dim Counter As Integer

		myDataView = New DataView(myTable, "True", "", DataViewRowState.CurrentRows)

		Grid_Transactions.DataSource = myDataView

		' Format Transactions Grid, hide unwanted columns.

		Grid_Transactions.Cols("TransactionTicket").Move(1)
		Grid_Transactions.Cols("FundName").Move(2)
		Grid_Transactions.Cols("InstrumentDescription").Move(3)
		Grid_Transactions.Cols("TransactionTypeName").Move(4)
		Grid_Transactions.Cols("TransactionUnits").Move(5)
		Grid_Transactions.Cols("TransactionPrice").Move(6)
		Grid_Transactions.Cols("TransactionValueDate").Move(7)

		For Each thisCol In Grid_Transactions.Cols
			Select Case thisCol.Name
				Case "TransactionTicket"
				Case "FundName"
				Case "InstrumentDescription"
				Case "TransactionTypeName"
				Case "TransactionUnits"
					thisCol.Format = "#,##0.0000"
				Case "TransactionPrice"
					thisCol.Format = "#,##0.0000"
				Case "TransactionValueDate"
					thisCol.Format = DISPLAYMEMBER_DATEFORMAT

				Case Else
					thisCol.Visible = False
			End Select

			If (thisCol.Caption.ToUpper.StartsWith("TRANSACTION")) Then
				thisCol.Caption = thisCol.Caption.Substring(11)
			End If
		Next

		For Counter = 0 To (Grid_Transactions.Cols.Count - 1)
			If (Grid_Transactions.Cols(Counter).Caption.ToUpper.StartsWith("TRANSACTION")) Then
				Grid_Transactions.Cols(Counter).Caption = Grid_Transactions.Cols(Counter).Caption.Substring(11)
			End If
		Next

		' Initialise Field select area

		Call BuildFieldSelectCombos()
		Me.Combo_AndOr_1.SelectedIndex = 0
		Me.Combo_AndOr_2.SelectedIndex = 0

		' Form Control Changed events
		AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

		AddHandler Combo_TransactionFund.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_TransactionInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_TicketOperator.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_Ticket.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_Ticket.SelectedIndexChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_ValueDateOperator.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Date_ValueDate.ValueChanged, AddressOf Me.FormControlChanged

		AddHandler Combo_Select1_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_Select1_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_Select1_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp
		AddHandler Combo_Select2_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_Select2_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_Select2_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp
		AddHandler Combo_Select3_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_Select3_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_Select3_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp

		AddHandler Combo_AndOr_1.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_AndOr_2.SelectedValueChanged, AddressOf Me.FormControlChanged

		AddHandler Combo_TransactionFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_TransactionInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_Ticket.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_TicketOperator.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_ValueDateOperator.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		AddHandler Combo_TransactionFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_TransactionInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_Ticket.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_TicketOperator.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_ValueDateOperator.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

		AddHandler Combo_TransactionFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_TransactionInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_Ticket.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_TicketOperator.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_ValueDateOperator.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

		AddHandler Combo_TransactionFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_TransactionInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_Ticket.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_TicketOperator.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_ValueDateOperator.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

	End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

	' Form Initialisation code.
	'
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
	Public Sub ResetForm() Implements StandardVeniceForm.ResetForm
		THIS_FORM_OrderBy = "TransactionFund, TransactionInstrument"

		Call Form_Load(Me, New System.EventArgs)
	End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
	Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
		_FormOpenFailed = False
		_InUse = True

		' Initialise Data structures. Connection, Adaptor and Dataset.

		If Not (MainForm Is Nothing) Then
			FormIsValid = True
		Else
			MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myConnection Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myAdaptor Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myDataset Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		' Initialse form

		InPaint = True
		IsOverCancelButton = False

		' Check User permissions
		Call CheckPermissions()
		If (HasReadPermission = False) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		' Initialise main select controls
		' Build Sorted data list from which this form operates

		Call SetFundCombo()
		Call SetInstrumentCombo()
		Call SetTicketCombo()

		Me.Combo_TransactionFund.SelectedIndex = -1
		Me.Combo_TransactionInstrument.SelectedIndex = -1
		Me.Combo_TicketOperator.SelectedIndex = -1
		Me.Combo_Ticket.SelectedIndex = -1
		Me.Combo_ValueDateOperator.SelectedIndex = 0
		Me.Date_ValueDate.Value = Renaissance_BaseDate

		Me.Combo_FieldSelect1.SelectedIndex = 0
		Me.Combo_FieldSelect2.SelectedIndex = 0
		Me.Combo_FieldSelect3.SelectedIndex = 0

		Call SetSortedRows()

		MainForm.SetComboSelectionLengths(Me)

		InPaint = False

	End Sub

    ''' <summary>
    ''' Handles the Closing event of the frm control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
	Private Sub frm_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		Dim HideForm As Boolean

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...

		_InUse = False

		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else
			HideForm = True
			If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			e.Cancel = True
		Else
			Try
				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

				RemoveHandler Combo_TransactionFund.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_TransactionInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_TicketOperator.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Ticket.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Ticket.SelectedIndexChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_ValueDateOperator.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Date_ValueDate.ValueChanged, AddressOf Me.FormControlChanged

				RemoveHandler Combo_Select1_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Select1_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Select1_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp
				RemoveHandler Combo_Select2_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Select2_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Select2_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp
				RemoveHandler Combo_Select3_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Select3_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Select3_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp

				RemoveHandler Combo_AndOr_1.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_AndOr_2.SelectedValueChanged, AddressOf Me.FormControlChanged

				RemoveHandler Combo_TransactionFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_TransactionInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_Ticket.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_TicketOperator.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_ValueDateOperator.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				RemoveHandler Combo_TransactionFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_TransactionInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_Ticket.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_TicketOperator.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_ValueDateOperator.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

				RemoveHandler Combo_TransactionFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_TransactionInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_Ticket.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_TicketOperator.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_ValueDateOperator.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

				RemoveHandler Combo_TransactionFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_TransactionInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_Ticket.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_TicketOperator.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_ValueDateOperator.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

			Catch ex As Exception
			End Try
		End If

	End Sub


#End Region

	' Routine to handle changes / updates to tables by this and other windows.
	' If this, or any other, form posts a change to a table, then it will invoke an update event 
	' detailing what tables have been altered.
	' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
	' the 'VeniceAutoUpdate' event of the main Venice form.
	' Each form may them react as appropriate to changes in any table that might impact it.
	'
    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		Dim OrgInPaint As Boolean
		Dim KnowledgeDateChanged As Boolean
		Dim RefreshGrid As Boolean

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		OrgInPaint = InPaint
		InPaint = True
		KnowledgeDateChanged = False
		RefreshGrid = False

		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
			KnowledgeDateChanged = True
		End If

		' ****************************************************************
		' Check for changes relevant to this form
		' ****************************************************************

		' Changes to the tblFund table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFund) = True) Or KnowledgeDateChanged Then
			Call SetFundCombo()
		End If

		' Changes to the tblInstrument table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrument) = True) Or KnowledgeDateChanged Then
			Call SetInstrumentCombo()
		End If

		' Changes to the tblCompoundTransaction table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblCompoundTransaction) = True) Or KnowledgeDateChanged Then
			Call SetTicketCombo()
			RefreshGrid = True
		End If

		' Changes to the KnowledgeDate :-
		If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
			RefreshGrid = True
		End If

		' Check TableUpdate against first Select Field
		If Me.Combo_FieldSelect1.SelectedIndex >= 0 Then
			Try

				' If the First FieldSelect combo has selected a Transaction Field which is related to
				' another table, as defined in tblReferentialIntegrity (e.g. TransactionCounterparty)
				' Then if that related table is updated, the FieldSelect-Values combo must be updated.

				' If ChangedID is ChangeID of related table then ,..

				If (e.TableChanged(GetFieldChangeID(CStr(Combo_FieldSelect1.SelectedItem))) = True) Or KnowledgeDateChanged Then
					Dim SelectedValue As Object
					Dim TextValue As String

					' Save current Value Combo Value.

					SelectedValue = Nothing
					TextValue = ""

					If Me.Combo_Select1_Value.SelectedIndex >= 0 Then
						SelectedValue = Me.Combo_Select1_Value.SelectedValue
					ElseIf (SelectedValue Is Nothing) Then
						TextValue = Me.Combo_Select1_Value.Text
					End If

					' Update FieldSelect-Values Combo.

					Call UpdateSelectedValueCombo(CStr(Me.Combo_FieldSelect1.SelectedItem), Me.Combo_Select1_Value)

					' Restore Saved Value.

					If (Not (SelectedValue Is Nothing)) Then
						Me.Combo_Select1_Value.SelectedValue = SelectedValue
					ElseIf TextValue.Length > 0 Then
						Me.Combo_Select1_Value.Text = TextValue
					End If
				End If
			Catch ex As Exception
			End Try
		End If

		' Check TableUpdate against second Select Field
		If Me.Combo_FieldSelect2.SelectedIndex >= 0 Then
			Try
				' GetFieldChangeID
				If (e.TableChanged(GetFieldChangeID(CStr(Combo_FieldSelect2.SelectedItem))) = True) Or KnowledgeDateChanged Then
					Dim SelectedValue As Object
					Dim TextValue As String

					SelectedValue = Nothing
					TextValue = ""

					If Me.Combo_Select2_Value.SelectedIndex >= 0 Then
						SelectedValue = Me.Combo_Select2_Value.SelectedValue
					ElseIf (SelectedValue Is Nothing) Then
						TextValue = Me.Combo_Select2_Value.Text
					End If

					Call UpdateSelectedValueCombo(CStr(Me.Combo_FieldSelect2.SelectedItem), Me.Combo_Select2_Value)

					If (Not (SelectedValue Is Nothing)) Then
						Me.Combo_Select2_Value.SelectedValue = SelectedValue
					ElseIf TextValue.Length > 0 Then
						Me.Combo_Select2_Value.Text = TextValue
					End If
				End If
			Catch ex As Exception
			End Try
		End If

		' Check TableUpdate against third Select Field
		If Me.Combo_FieldSelect3.SelectedIndex >= 0 Then
			Try
				' GetFieldChangeID
				If (e.TableChanged(GetFieldChangeID(CStr(Combo_FieldSelect3.SelectedItem))) = True) Or KnowledgeDateChanged Then
					Dim SelectedValue As Object
					Dim TextValue As String

					SelectedValue = Nothing
					TextValue = ""

					If Me.Combo_Select3_Value.SelectedIndex >= 0 Then
						SelectedValue = Me.Combo_Select3_Value.SelectedValue
					ElseIf (SelectedValue Is Nothing) Then
						TextValue = Me.Combo_Select3_Value.Text
					End If

					Call UpdateSelectedValueCombo(Me.Combo_FieldSelect3.SelectedItem.ToString, Me.Combo_Select3_Value)

					If (Not (SelectedValue Is Nothing)) Then
						Me.Combo_Select3_Value.SelectedValue = SelectedValue
					ElseIf TextValue.Length > 0 Then
						Me.Combo_Select3_Value.Text = TextValue
					End If
				End If
			Catch ex As Exception
			End Try
		End If


		' Changes to the tblUserPermissions table :-

		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

			' Check ongoing permissions.

			Call CheckPermissions()
			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

				FormIsValid = False
				Me.Close()
				Exit Sub
			End If

			RefreshGrid = True

		End If


		' ****************************************************************
		' Changes to the Main FORM table :-
		' (or the tblCompoundTransactions table)
		' ****************************************************************

		If (e.TableChanged(THIS_FORM_ChangeID) = True) Or _
			(e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblCompoundTransaction) = True) Or _
			(RefreshGrid = True) Or _
			KnowledgeDateChanged Then

			' Re-Set Controls etc.
			Call SetSortedRows()

		End If

		InPaint = OrgInPaint

	End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

    ''' <summary>
    ''' Sets the sorted rows.
    ''' </summary>
	Private Sub SetSortedRows()
		' *******************************************************************************
		' Build a Select String appropriate to the form status and apply it to the DataView object.
		' *******************************************************************************

		Dim SelectString As String
		Dim FieldSelectString As String

		SelectString = ""
		FieldSelectString = ""
		Me.StatusBar_SelectCompoundTransactions.Text = ""

		Try

			' Select on Fund...

			If (Me.Combo_TransactionFund.SelectedIndex > 0) Then
				SelectString = "(TransactionFund=" & Combo_TransactionFund.SelectedValue.ToString & ")"
			End If

			' Select on Instrument

			If (Me.Combo_TransactionInstrument.SelectedIndex > 0) Then
				If (SelectString.Length > 0) Then
					SelectString &= " AND "
				End If
				SelectString &= "(TransactionInstrument=" & Combo_TransactionInstrument.SelectedValue.ToString & ")"
			End If

			' Select on Transaction Ticket...

			If (Me.Combo_TicketOperator.SelectedIndex > 0) Then
				If (SelectString.Length > 0) Then
					SelectString &= " AND "
				End If

				SelectString &= "(TransactionTicket " & Combo_TicketOperator.SelectedItem.ToString & " '" & Combo_Ticket.Text & "')"
			End If

			' Select on Value Date

			If (Me.Combo_ValueDateOperator.SelectedIndex > 0) Then
				If (SelectString.Length > 0) Then
					SelectString &= " AND "
				End If

				SelectString &= "(TransactionValueDate " & Combo_ValueDateOperator.SelectedItem.ToString & " #" & Me.Date_ValueDate.Value.ToString(QUERY_SHORTDATEFORMAT) & "#)"
			End If

			' Field Select ...

			FieldSelectString = "("
			Dim TransactionDataset As DataSet
			Dim TransactionTable As DataTable
			Dim FieldName As String

			TransactionDataset = MainForm.Load_Table(RenaissanceStandardDatasets.tblCompoundTransaction, False)
			TransactionTable = myDataset.Tables(0)

			' Select Field One.

			If (Me.Combo_FieldSelect1.SelectedIndex >= 0) AndAlso (Me.Combo_Select1_Operator.SelectedIndex >= 0) And (Me.Combo_Select1_Value.Text.Length > 0) Then
				' Get Field Name, Add 'Transaction' backon if necessary.

				FieldName = Combo_FieldSelect1.SelectedItem.ToString
				If (FieldName.EndsWith(".")) Then
					FieldName = "Transaction" & FieldName.Substring(0, FieldName.Length - 1)
				End If

				' If the Selected Field Name is a String Field ...

				If (TransactionTable.Columns(FieldName).DataType Is GetType(System.String)) Then

					' If there is a value Selected, Use it - else use the Combo Text.

					If (Not (Combo_Select1_Value.SelectedValue Is Nothing)) Then
						FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.Text & " '" & Combo_Select1_Value.SelectedValue.ToString & "')"
					Else
						FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.Text & " '" & Combo_Select1_Value.Text.ToString & "')"
					End If

				ElseIf (TransactionTable.Columns(FieldName).DataType Is GetType(System.DateTime)) Then

					' For Dates, If there is no 'Selected' value use the Combo text if it is a Valid date, else
					' Use the 'Selected' Value if it is a date.

					If (Me.Combo_Select1_Value.SelectedValue Is Nothing) AndAlso (IsDate(Me.Combo_Select1_Value.Text)) Then
						FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select1_Value.Text).ToString(QUERY_SHORTDATEFORMAT) & "#)"
					ElseIf (Not (Me.Combo_Select1_Value.SelectedValue Is Nothing)) And IsDate(Combo_Select1_Value.SelectedValue) Then
						FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select1_Value.SelectedValue).ToString(QUERY_SHORTDATEFORMAT) & "#)"
					Else
						FieldSelectString &= "(true)"
					End If

				Else
					' Now Deemed to be numeric, or at least not in need of quotes / delimeters...
					' If there is no 'Selected' value use the Combo text if it is a Valid number, else
					' Use the 'Selected' Value if it is a valid numeric.

					If (Not (Combo_Select1_Value.SelectedValue Is Nothing)) AndAlso (IsNumeric(Combo_Select1_Value.SelectedValue)) Then
						FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.SelectedItem.ToString & " " & Combo_Select1_Value.SelectedValue.ToString & ")"
					ElseIf IsNumeric(Combo_Select1_Value.Text) Then
						FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.SelectedItem.ToString & " " & Combo_Select1_Value.Text & ")"
					Else
						FieldSelectString &= "(true)"
					End If
				End If
			End If

			' Select Field Two.

			If (Me.Combo_FieldSelect2.SelectedIndex >= 0) AndAlso (Me.Combo_Select2_Operator.SelectedIndex >= 0) And (Me.Combo_Select2_Value.Text.Length > 0) Then
				FieldName = Combo_FieldSelect2.SelectedItem.ToString
				If (FieldName.EndsWith(".")) Then
					FieldName = "Transaction" & FieldName.Substring(0, FieldName.Length - 1)
				End If

				If (FieldSelectString.Length > 1) Then
					FieldSelectString &= " " & Me.Combo_AndOr_1.SelectedItem.ToString & " "
				End If

				If (TransactionTable.Columns(FieldName).DataType Is GetType(System.String)) Then

					If (Not (Combo_Select2_Value.SelectedValue Is Nothing)) Then
						FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.Text & " '" & Combo_Select2_Value.SelectedValue.ToString & "')"
					Else
						FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.Text & " '" & Combo_Select2_Value.Text.ToString & "')"
					End If

				ElseIf (TransactionTable.Columns(FieldName).DataType Is GetType(System.DateTime)) Then

					If (Me.Combo_Select2_Value.SelectedValue Is Nothing) AndAlso (IsDate(Me.Combo_Select2_Value.Text)) Then
						FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select2_Value.Text).ToString(QUERY_SHORTDATEFORMAT) & "#)"
					ElseIf (Not (Me.Combo_Select2_Value.SelectedValue Is Nothing)) And IsDate(Combo_Select2_Value.SelectedValue) Then
						FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select2_Value.SelectedValue).ToString(QUERY_SHORTDATEFORMAT) & "#)"
					Else
						FieldSelectString &= "(true)"
					End If

				Else
					If (Not (Combo_Select2_Value.SelectedValue Is Nothing)) AndAlso (IsNumeric(Combo_Select2_Value.SelectedValue)) Then
						FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.SelectedItem.ToString & " " & Combo_Select2_Value.SelectedValue.ToString & ")"
					ElseIf IsNumeric(Combo_Select2_Value.Text) Then
						FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.SelectedItem.ToString & " " & Combo_Select2_Value.Text & ")"
					Else
						FieldSelectString &= "(true)"
					End If
				End If
			End If


			' Select Field Three.

			If (Me.Combo_FieldSelect3.SelectedIndex >= 0) AndAlso (Me.Combo_Select3_Operator.SelectedIndex >= 0) And (Me.Combo_Select3_Value.Text.Length > 0) Then
				FieldName = Combo_FieldSelect3.SelectedItem.ToString
				If (FieldName.EndsWith(".")) Then
					FieldName = "Transaction" & FieldName.Substring(0, FieldName.Length - 1)
				End If

				If (FieldSelectString.Length > 1) Then
					FieldSelectString &= " " & Me.Combo_AndOr_2.SelectedItem.ToString & " "
				End If

				If (TransactionTable.Columns(FieldName).DataType Is GetType(System.String)) Then

					If (Not (Combo_Select3_Value.SelectedValue Is Nothing)) Then
						FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.Text & " '" & Combo_Select3_Value.SelectedValue.ToString & "')"
					Else
						FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.Text & " '" & Combo_Select3_Value.Text.ToString & "')"
					End If

				ElseIf (TransactionTable.Columns(FieldName).DataType Is GetType(System.DateTime)) Then

					If (Me.Combo_Select3_Value.SelectedValue Is Nothing) AndAlso (IsDate(Me.Combo_Select3_Value.Text)) Then
						FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select3_Value.Text).ToString(QUERY_SHORTDATEFORMAT) & "#)"
					ElseIf (Not (Me.Combo_Select3_Value.SelectedValue Is Nothing)) And IsDate(Combo_Select3_Value.SelectedValue) Then
						FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select3_Value.SelectedValue).ToString(QUERY_SHORTDATEFORMAT) & "#)"
					Else
						FieldSelectString &= "(true)"
					End If

				Else
					If (Not (Combo_Select3_Value.SelectedValue Is Nothing)) AndAlso (IsNumeric(Combo_Select3_Value.SelectedValue)) Then
						FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.SelectedItem.ToString & " " & Combo_Select3_Value.SelectedValue.ToString & ")"
					ElseIf IsNumeric(Combo_Select3_Value.Text) Then
						FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.SelectedItem.ToString & " " & Combo_Select3_Value.Text & ")"
					Else
						FieldSelectString &= "(true)"
					End If
				End If
			End If

			FieldSelectString &= ")"

			' If the FieldSelect string is used, then tag it on to the main select string.

			If FieldSelectString.Length > 2 Then
				If (SelectString.Length > 0) Then
					SelectString &= " AND "
				End If
				SelectString &= FieldSelectString
			End If

			If SelectString.Length <= 0 Then
				SelectString = "true"
			End If

		Catch ex As Exception
			SelectString = "true"
		End Try

		' Now apply the Select String to the DataView object (in order to update the DataLinked Grid).

		Try
			If (Not (myDataView.RowFilter = SelectString)) Then
				myDataView.RowFilter = SelectString

			End If
		Catch ex As Exception
			SelectString = "true"
			myDataView.RowFilter = SelectString
		End Try

		Me.StatusBar_SelectCompoundTransactions.Text = "(" & myDataView.Count.ToString & " Records) " & SelectString

	End Sub


	' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
	Private Sub CheckPermissions()
		' *******************************************************************************
		'
		' *******************************************************************************

		Dim Permissions As Integer

		Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

		HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
		HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
		HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
		HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

	End Sub

    ''' <summary>
  ''' Basic event handler, called when a form value is changed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
		' *******************************************************************************
		' In the event of one of the Form Controls changing, refresh the data grid.
		' *******************************************************************************

		If InPaint = False Then

			Call SetSortedRows()

		End If
	End Sub

    ''' <summary>
    ''' Handles the KeyUp event of the Combo_FieldSelect control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
	Private Sub Combo_FieldSelect_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Combo_Select1_Value.KeyUp
		' *******************************************************************************
		' In the event of one of the Form Controls changing, refresh the data grid.
		' *******************************************************************************

		If InPaint = False Then
			Try

				Call SetSortedRows()

			Catch ex As Exception
			End Try

		End If
	End Sub


    ''' <summary>
    ''' Builds the field select combos.
    ''' </summary>
	Private Sub BuildFieldSelectCombos()
		' *********************************************************************************
		' Build the FieldSelect combo boxes.
		'
		' Field Names starting with 'Transaction' are modified to start with '.'
		' This is done in order to make the name more readable.
		'
		' *********************************************************************************

		Dim TransactionDataset As DataSet
		Dim TransactionTable As DataTable

		Dim ColumnName As String
		Dim thisColumn As DataColumn

		' Clear Field Select Combos

		Combo_FieldSelect1.Items.Clear()
		Combo_Select1_Value.Items.Clear()
		Combo_FieldSelect1.Items.Add("")

		Combo_FieldSelect2.Items.Clear()
		Combo_Select2_Value.Items.Clear()
		Combo_FieldSelect2.Items.Add("")

		Combo_FieldSelect3.Items.Clear()
		Combo_Select3_Value.Items.Clear()
		Combo_FieldSelect3.Items.Add("")

		' Get a handle to the Transactions Table.

		TransactionDataset = MainForm.Load_Table(RenaissanceStandardDatasets.tblCompoundTransaction, False)
		TransactionTable = TransactionDataset.Tables(0)

		' Add to the Field Combo those fields which are common to the 

		For Each thisColumn In myTable.Columns
			Try
				If (TransactionTable.Columns.Contains(thisColumn.ColumnName)) Then

					ColumnName = TransactionTable.Columns(thisColumn.ColumnName).ColumnName
					If (ColumnName.StartsWith("Transaction")) Then
						ColumnName = ColumnName.Substring(11) & "."
					End If

					Combo_FieldSelect1.Items.Add(ColumnName)
					Combo_FieldSelect2.Items.Add(ColumnName)
					Combo_FieldSelect3.Items.Add(ColumnName)

				End If

			Catch ex As Exception
			End Try
		Next

	End Sub

    ''' <summary>
    ''' Updates the selected value combo.
    ''' </summary>
    ''' <param name="pFieldName">Name of the p field.</param>
    ''' <param name="pValueCombo">The p value combo.</param>
	Private Sub UpdateSelectedValueCombo(ByVal pFieldName As String, ByRef pValueCombo As ComboBox)
		' *******************************************************************************
		' Build a Value Combo Box appropriate to the chosen DataTable Select Field.
		'
		' By default the combo will contain all existing values for the chosen field, however
		' If a relationship is defined for thi table and field, then a Combo will be built
		' which reflects this relationship.
		' e.g. the tblReferentialIntegrity table defined a relationship between the
		' TransactionCounterparty field and the tblCounterparty.Counterparty field, thus if
		' the chosen Select field ti 'Counterparty.' then the Value combo will be built using
		' the CounterpartyID & CounterpartyName details from the tblCounterparty table.
		'
		' *******************************************************************************

		Dim tblRefIntegrity As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityDataTable
		Dim IntegrityRows() As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityRow
		Dim TransactionDataset As DataSet
		Dim TransactionTable As DataTable
		Dim SortOrder As Boolean

		SortOrder = True

		' Clear the Value Combo.

		Try
			pValueCombo.DataSource = Nothing
			pValueCombo.DisplayMember = ""
			pValueCombo.ValueMember = ""
			pValueCombo.Items.Clear()
		Catch ex As Exception
		End Try

		' By default, make the Combo a Standard Dropdown, so that non-list items can be entered.

		pValueCombo.DropDownStyle = ComboBoxStyle.DropDown

		' Exit if no FiledName is given (having cleared the combo).

		If (pFieldName.Length <= 0) Then Exit Sub

		' Adjust the Field name if appropriate.

		If (pFieldName.EndsWith(".")) Then
			pFieldName = "Transaction" & pFieldName.Substring(0, pFieldName.Length - 1)
		End If

		' Get a handle to the Standard Transactions Table.
		' This is so that the field types can be determined and used.

		Try
			TransactionDataset = MainForm.Load_Table(RenaissanceStandardDatasets.tblCompoundTransaction, False)
			TransactionTable = myDataset.Tables(0)
		Catch ex As Exception
			Exit Sub
		End Try

		' If the selected field is a Date then sort the selection combo in reverse order.

		If (TransactionTable.Columns(pFieldName).DataType Is GetType(System.DateTime)) Then
			SortOrder = False
		End If

		' Get a handle to the Referential Integrity table and the row matching this table and field.
		' this table defines a relationship between Transaction Table fields and other tables.
		' This information can be used to build more meaningfull Value Combos.

		Try
			tblRefIntegrity = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblReferentialIntegrity, False), RenaissanceDataClass.DSReferentialIntegrity).tblReferentialIntegrity
			IntegrityRows = tblRefIntegrity.Select("(FeedsTable LIKE '%tblCompoundTransaction') AND (FeedsField = '" & pFieldName & "')", "RN")
			If (IntegrityRows.Length <= 0) Then
				GoTo StandardExit
			End If

		Catch ex As Exception
			Exit Sub
		End Try


		If (IntegrityRows(0).IsDescriptionFieldNull) Then
			' No Linked Description Field

			GoTo StandardExit
		End If

		' OK, a referential record exists.
		' Determine the Table and Field Names to use to build the Value Combo.

		Dim TableName As String
		Dim TableField As String
		Dim DescriptionField As String
		Dim stdDS As StandardDataset
		Dim thisChangeID As RenaissanceChangeID

		Try

			TableName = IntegrityRows(0).TableName
			TableField = IntegrityRows(0).TableField
			DescriptionField = IntegrityRows(0).DescriptionField

			thisChangeID = CType(System.Enum.Parse(GetType(RenaissanceChangeID), TableName), RenaissanceChangeID)

			stdDS = RenaissanceStandardDatasets.GetStandardDataset(thisChangeID)

			' Build the appropriate Values Combo.

			Call MainForm.SetTblGenericCombo( _
			pValueCombo, _
			stdDS, _
			DescriptionField, _
			TableField, _
			"", False, SortOrder, True)		 ' 

			' For Referential Integrity generated combo's, make the Combo a 
			' DropDownList

			pValueCombo.DropDownStyle = ComboBoxStyle.DropDownList

		Catch ex As Exception
			GoTo StandardExit

		End Try

		Exit Sub

StandardExit:

		' Build the standard Combo, just use the values from the given field.

		Call MainForm.SetTblGenericCombo( _
		pValueCombo, _
		RenaissanceStandardDatasets.tblSelectCompoundTransaction, _
		pFieldName, _
		pFieldName, _
		"", True, SortOrder)	 ' 

	End Sub

    ''' <summary>
    ''' Gets the field change ID.
    ''' </summary>
    ''' <param name="pFieldName">Name of the p field.</param>
    ''' <returns>RenaissanceChangeID.</returns>
	Private Function GetFieldChangeID(ByVal pFieldName As String) As RenaissanceChangeID
		Dim tblRefIntegrity As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityDataTable
		Dim IntegrityRows() As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityRow

		If (pFieldName.EndsWith(".")) Then
			pFieldName = "Transaction" & pFieldName.Substring(0, pFieldName.Length - 1)
		End If

		Try
			tblRefIntegrity = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblReferentialIntegrity, False), RenaissanceDataClass.DSReferentialIntegrity).tblReferentialIntegrity
			IntegrityRows = tblRefIntegrity.Select("(FeedsTable LIKE '%tblCompoundTransaction') AND (FeedsField = '" & pFieldName & "')", "RN")
			If (IntegrityRows.Length <= 0) Then
				Return RenaissanceChangeID.None
				Exit Function
			End If

		Catch ex As Exception
			Return RenaissanceChangeID.None
			Exit Function
		End Try


		If (IntegrityRows(0).IsDescriptionFieldNull) Then
			Return RenaissanceChangeID.None
			Exit Function
		End If

		' OK, a referential record exists.
		' Determine the Table and Field Names to use to build the Value Combo.

		Dim TableName As String
		Dim TableField As String
		Dim DescriptionField As String
		Dim thisChangeID As RenaissanceChangeID

		Try

			TableName = IntegrityRows(0).TableName
			TableField = IntegrityRows(0).TableField
			DescriptionField = IntegrityRows(0).DescriptionField

			thisChangeID = CType(System.Enum.Parse(GetType(RenaissanceChangeID), TableName), RenaissanceChangeID)

			Return thisChangeID
			Exit Function
		Catch ex As Exception
		End Try

		Return RenaissanceChangeID.None
		Exit Function

	End Function


    ''' <summary>
    ''' Handles the DoubleClick event of the Grid_Transactions control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Grid_Transactions_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid_Transactions.DoubleClick
		' ***************************************************************************************
		' Spawn an Add/Edit Transactions form reflecting the chosen selection of transactions
		' and pointing to the transaction which was double clicked.
		' ***************************************************************************************

		Dim CTTable As RenaissanceDataClass.DSCompoundTransaction.tblCompoundTransactionDataTable
		Dim CTRows() As RenaissanceDataClass.DSCompoundTransaction.tblCompoundTransactionRow
		Dim TargetAuditID As Integer
		Dim thisFormHandle As RenaissanceGlobals.RenaissanceFormHandle
		Dim CompoundID As Integer

		' Firstly Resolve the AuditID of the CompoundTransaction To move to.
		' 

		Try
			CompoundID = CInt(Me.Grid_Transactions.Rows(Grid_Transactions.RowSel).DataSource("TransactionCompoundID"))
			CTTable = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblCompoundTransaction, False), RenaissanceDataClass.DSCompoundTransaction).tblCompoundTransaction
			CTRows = CTTable.Select("TransactionCompoundID=" & CompoundID.ToString)

			If (CTRows.Length <= 0) Then Exit Sub
			TargetAuditID = CInt(CTRows(0)("AuditID"))

			thisFormHandle = MainForm.New_VeniceForm(VeniceFormID.frmCompoundTransaction)
			CType(thisFormHandle.Form, frmCompoundTransaction).FormSelectCriteria = "AuditID=" & TargetAuditID.ToString
			CType(thisFormHandle.Form, frmCompoundTransaction).MoveToAuditID(TargetAuditID)

		Catch ex As Exception
			Call MainForm.LogError(Me.Name & ", TransactionsGrid DblClick", LOG_LEVELS.Error, ex.Message, "Error Showing Compound Transaction Form", ex.StackTrace, True)
		End Try

	End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_FieldSelect1 control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_FieldSelect1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_FieldSelect1.SelectedIndexChanged
		' ***************************************************************************************
		' Field Select Combo Chosen, Update the associated Values Combo.
		'
		' ***************************************************************************************

		Call UpdateSelectedValueCombo(Me.Combo_FieldSelect1.SelectedItem.ToString, Me.Combo_Select1_Value)
		Me.Combo_Select1_Operator.SelectedIndex = 1

	End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_FieldSelect2 control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_FieldSelect2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_FieldSelect2.SelectedIndexChanged
		' ***************************************************************************************
		' Field Select Combo Chosen, Update the associated Values Combo.
		'
		' ***************************************************************************************
		Call UpdateSelectedValueCombo(Me.Combo_FieldSelect2.SelectedItem.ToString, Me.Combo_Select2_Value)
		Me.Combo_Select2_Operator.SelectedIndex = 1
	End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_FieldSelect3 control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_FieldSelect3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_FieldSelect3.SelectedIndexChanged
		' ***************************************************************************************
		' Field Select Combo Chosen, Update the associated Values Combo.
		'
		' ***************************************************************************************
		Call UpdateSelectedValueCombo(Me.Combo_FieldSelect3.SelectedItem.ToString, Me.Combo_Select3_Value)
		Me.Combo_Select3_Operator.SelectedIndex = 1
	End Sub

#End Region

#Region " Set Form Combos (Form Specific Code) "

    ''' <summary>
    ''' Sets the fund combo.
    ''' </summary>
	Private Sub SetFundCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_TransactionFund, _
    RenaissanceStandardDatasets.tblFund, _
    "FundName", _
    "FundID", _
    "FundClosed=0", False, True, True)   ' 

	End Sub

    ''' <summary>
    ''' Sets the instrument combo.
    ''' </summary>
	Private Sub SetInstrumentCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_TransactionInstrument, _
		RenaissanceStandardDatasets.tblInstrument, _
		"InstrumentDescription", _
		"InstrumentID", _
		"", False, True, True)		' 

	End Sub

    ''' <summary>
    ''' Sets the ticket combo.
    ''' </summary>
	Private Sub SetTicketCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_Ticket, _
		RenaissanceStandardDatasets.tblCompoundTransaction, _
		"TransactionTicket", _
		"TransactionTicket", _
		"", True, True, True)			' 

	End Sub


#End Region

#Region " Bug hunting "

	' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
	' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
	' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

    ''' <summary>
    ''' Called when [row updating].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
	Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
		Dim a As Integer

		If Not e.Command Is Nothing Then
			a = 1
		End If

		If Not e.Errors Is Nothing Then
			a = 2
		End If
	End Sub

    ''' <summary>
    ''' Called when [row updated].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
	Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
		Dim a As Integer

		If Not e.Command Is Nothing Then
			a = 1
		End If

		If Not e.Errors Is Nothing Then
			a = 2
		End If
	End Sub

    ''' <summary>
    ''' Called when [row fill error].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
	Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
		Dim a As Integer

		a = 1
	End Sub

#End Region




    ''' <summary>
    ''' Handles the Click event of the Grid_Transactions control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Grid_Transactions_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Grid_Transactions.Click

	End Sub
End Class
