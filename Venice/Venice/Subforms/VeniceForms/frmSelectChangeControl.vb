' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmSelectChangeControl.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals

Imports RenaissanceDataClass
Imports C1.Win.C1FlexGrid

''' <summary>
''' Class frmSelectChangeControl
''' </summary>
Public Class frmSelectChangeControl

	Inherits System.Windows.Forms.Form
	Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmSelectChangeControl"/> class from being created.
    ''' </summary>
	Private Sub New()
		MyBase.New()

		'This call is required by the Windows Form Designer.
		InitializeComponent()

		'Add any initialization after the InitializeComponent() call

	End Sub

	'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
    ''' <summary>
    ''' The combo_ product
    ''' </summary>
	Friend WithEvents Combo_Product As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label_ cpty is fund
    ''' </summary>
	Friend WithEvents label_CptyIsFund As System.Windows.Forms.Label
    ''' <summary>
    ''' The label1
    ''' </summary>
	Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label3
    ''' </summary>
	Friend WithEvents Label3 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ reference
    ''' </summary>
	Friend WithEvents Combo_Reference As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The date_ raised
    ''' </summary>
	Friend WithEvents Date_Raised As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The combo_ reference operator
    ''' </summary>
	Friend WithEvents Combo_ReferenceOperator As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ date_ raised operator
    ''' </summary>
	Friend WithEvents Combo_Date_RaisedOperator As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The grid_ change controls
    ''' </summary>
	Friend WithEvents Grid_ChangeControls As C1.Win.C1FlexGrid.C1FlexGrid
    ''' <summary>
    ''' The combo_ field select1
    ''' </summary>
	Friend WithEvents Combo_FieldSelect1 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select1_ operator
    ''' </summary>
	Friend WithEvents Combo_Select1_Operator As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select1_ value
    ''' </summary>
	Friend WithEvents Combo_Select1_Value As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select2_ value
    ''' </summary>
	Friend WithEvents Combo_Select2_Value As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ field select2
    ''' </summary>
	Friend WithEvents Combo_FieldSelect2 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select2_ operator
    ''' </summary>
	Friend WithEvents Combo_Select2_Operator As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select3_ value
    ''' </summary>
	Friend WithEvents Combo_Select3_Value As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ field select3
    ''' </summary>
	Friend WithEvents Combo_FieldSelect3 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select3_ operator
    ''' </summary>
	Friend WithEvents Combo_Select3_Operator As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ and or_1
    ''' </summary>
	Friend WithEvents Combo_AndOr_1 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ and or_2
    ''' </summary>
	Friend WithEvents Combo_AndOr_2 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The root menu
    ''' </summary>
	Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
    ''' <summary>
    ''' The form_ status strip
    ''' </summary>
	Friend WithEvents Form_StatusStrip As System.Windows.Forms.StatusStrip
    ''' <summary>
    ''' The form_ progress bar
    ''' </summary>
	Friend WithEvents Form_ProgressBar As System.Windows.Forms.ToolStripProgressBar
    ''' <summary>
    ''' The label_ status
    ''' </summary>
	Friend WithEvents Label_Status As System.Windows.Forms.ToolStripStatusLabel
    ''' <summary>
    ''' The check_ view authorise rejected
    ''' </summary>
	Friend WithEvents Check_ViewAuthoriseRejected As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ view only authorised
    ''' </summary>
	Friend WithEvents Check_ViewOnlyAuthorised As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ view non authorised
    ''' </summary>
	Friend WithEvents Check_ViewNonAuthorised As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ view review rejected
    ''' </summary>
	Friend WithEvents Check_ViewReviewRejected As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ view only reviewed
    ''' </summary>
	Friend WithEvents Check_ViewOnlyReviewed As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ view non reviewed
    ''' </summary>
	Friend WithEvents Check_ViewNonReviewed As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The radio_ or
    ''' </summary>
	Friend WithEvents Radio_Or As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The radio_ and
    ''' </summary>
	Friend WithEvents Radio_And As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The label2
    ''' </summary>
	Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The check_ view only accepted
    ''' </summary>
	Friend WithEvents Check_ViewOnlyAccepted As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ view non accepted
    ''' </summary>
	Friend WithEvents Check_ViewNonAccepted As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The panel1
    ''' </summary>
	Friend WithEvents Panel1 As System.Windows.Forms.Panel
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSelectChangeControl))
		Me.Grid_ChangeControls = New C1.Win.C1FlexGrid.C1FlexGrid
		Me.Combo_Product = New System.Windows.Forms.ComboBox
		Me.label_CptyIsFund = New System.Windows.Forms.Label
		Me.Combo_Reference = New System.Windows.Forms.ComboBox
		Me.Label1 = New System.Windows.Forms.Label
		Me.Label3 = New System.Windows.Forms.Label
		Me.Date_Raised = New System.Windows.Forms.DateTimePicker
		Me.Combo_ReferenceOperator = New System.Windows.Forms.ComboBox
		Me.Combo_Date_RaisedOperator = New System.Windows.Forms.ComboBox
		Me.Combo_Select1_Operator = New System.Windows.Forms.ComboBox
		Me.Combo_FieldSelect1 = New System.Windows.Forms.ComboBox
		Me.Combo_Select1_Value = New System.Windows.Forms.ComboBox
		Me.Combo_Select2_Value = New System.Windows.Forms.ComboBox
		Me.Combo_FieldSelect2 = New System.Windows.Forms.ComboBox
		Me.Combo_Select2_Operator = New System.Windows.Forms.ComboBox
		Me.Combo_Select3_Value = New System.Windows.Forms.ComboBox
		Me.Combo_FieldSelect3 = New System.Windows.Forms.ComboBox
		Me.Combo_Select3_Operator = New System.Windows.Forms.ComboBox
		Me.Combo_AndOr_1 = New System.Windows.Forms.ComboBox
		Me.Combo_AndOr_2 = New System.Windows.Forms.ComboBox
		Me.RootMenu = New System.Windows.Forms.MenuStrip
		Me.Form_StatusStrip = New System.Windows.Forms.StatusStrip
		Me.Form_ProgressBar = New System.Windows.Forms.ToolStripProgressBar
		Me.Label_Status = New System.Windows.Forms.ToolStripStatusLabel
		Me.Panel1 = New System.Windows.Forms.Panel
		Me.Label2 = New System.Windows.Forms.Label
		Me.Radio_And = New System.Windows.Forms.RadioButton
		Me.Radio_Or = New System.Windows.Forms.RadioButton
		Me.Check_ViewNonReviewed = New System.Windows.Forms.CheckBox
		Me.Check_ViewOnlyReviewed = New System.Windows.Forms.CheckBox
		Me.Check_ViewReviewRejected = New System.Windows.Forms.CheckBox
		Me.Check_ViewAuthoriseRejected = New System.Windows.Forms.CheckBox
		Me.Check_ViewOnlyAuthorised = New System.Windows.Forms.CheckBox
		Me.Check_ViewNonAuthorised = New System.Windows.Forms.CheckBox
		Me.Check_ViewOnlyAccepted = New System.Windows.Forms.CheckBox
		Me.Check_ViewNonAccepted = New System.Windows.Forms.CheckBox
		CType(Me.Grid_ChangeControls, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.Form_StatusStrip.SuspendLayout()
		Me.Panel1.SuspendLayout()
		Me.SuspendLayout()
		'
		'Grid_ChangeControls
		'
		Me.Grid_ChangeControls.AllowEditing = False
		Me.Grid_ChangeControls.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
								Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Grid_ChangeControls.AutoClipboard = True
		Me.Grid_ChangeControls.AutoGenerateColumns = False
		Me.Grid_ChangeControls.CausesValidation = False
		Me.Grid_ChangeControls.ColumnInfo = resources.GetString("Grid_ChangeControls.ColumnInfo")
		Me.Grid_ChangeControls.Cursor = System.Windows.Forms.Cursors.Default
		Me.Grid_ChangeControls.EditOptions = C1.Win.C1FlexGrid.EditFlags.None
		Me.Grid_ChangeControls.KeyActionTab = C1.Win.C1FlexGrid.KeyActionEnum.MoveAcross
		Me.Grid_ChangeControls.Location = New System.Drawing.Point(4, 250)
		Me.Grid_ChangeControls.Name = "Grid_ChangeControls"
		Me.Grid_ChangeControls.Rows.DefaultSize = 17
		Me.Grid_ChangeControls.Size = New System.Drawing.Size(808, 261)
		Me.Grid_ChangeControls.TabIndex = 18
		'
		'Combo_Product
		'
		Me.Combo_Product.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_Product.Location = New System.Drawing.Point(128, 30)
		Me.Combo_Product.Name = "Combo_Product"
		Me.Combo_Product.Size = New System.Drawing.Size(274, 21)
		Me.Combo_Product.TabIndex = 0
		'
		'label_CptyIsFund
		'
		Me.label_CptyIsFund.Location = New System.Drawing.Point(16, 34)
		Me.label_CptyIsFund.Name = "label_CptyIsFund"
		Me.label_CptyIsFund.Size = New System.Drawing.Size(104, 16)
		Me.label_CptyIsFund.TabIndex = 82
		Me.label_CptyIsFund.Text = "Product"
		'
		'Combo_Reference
		'
		Me.Combo_Reference.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_Reference.Location = New System.Drawing.Point(228, 57)
		Me.Combo_Reference.Name = "Combo_Reference"
		Me.Combo_Reference.Size = New System.Drawing.Size(174, 21)
		Me.Combo_Reference.TabIndex = 2
		'
		'Label1
		'
		Me.Label1.Location = New System.Drawing.Point(16, 61)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(104, 16)
		Me.Label1.TabIndex = 86
		Me.Label1.Text = "C/C Reference"
		'
		'Label3
		'
		Me.Label3.Location = New System.Drawing.Point(420, 61)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(104, 16)
		Me.Label3.TabIndex = 88
		Me.Label3.Text = "Date Raised"
		Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopRight
		'
		'Date_Raised
		'
		Me.Date_Raised.Location = New System.Drawing.Point(632, 57)
		Me.Date_Raised.Name = "Date_Raised"
		Me.Date_Raised.Size = New System.Drawing.Size(174, 20)
		Me.Date_Raised.TabIndex = 4
		'
		'Combo_ReferenceOperator
		'
		Me.Combo_ReferenceOperator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.Combo_ReferenceOperator.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_ReferenceOperator.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like"})
		Me.Combo_ReferenceOperator.Location = New System.Drawing.Point(128, 57)
		Me.Combo_ReferenceOperator.Name = "Combo_ReferenceOperator"
		Me.Combo_ReferenceOperator.Size = New System.Drawing.Size(96, 21)
		Me.Combo_ReferenceOperator.TabIndex = 1
		'
		'Combo_Date_RaisedOperator
		'
		Me.Combo_Date_RaisedOperator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.Combo_Date_RaisedOperator.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_Date_RaisedOperator.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", ""})
		Me.Combo_Date_RaisedOperator.Location = New System.Drawing.Point(532, 57)
		Me.Combo_Date_RaisedOperator.Name = "Combo_Date_RaisedOperator"
		Me.Combo_Date_RaisedOperator.Size = New System.Drawing.Size(96, 21)
		Me.Combo_Date_RaisedOperator.TabIndex = 3
		'
		'Combo_Select1_Operator
		'
		Me.Combo_Select1_Operator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.Combo_Select1_Operator.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_Select1_Operator.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like"})
		Me.Combo_Select1_Operator.Location = New System.Drawing.Point(272, 84)
		Me.Combo_Select1_Operator.Name = "Combo_Select1_Operator"
		Me.Combo_Select1_Operator.Size = New System.Drawing.Size(96, 21)
		Me.Combo_Select1_Operator.TabIndex = 6
		'
		'Combo_FieldSelect1
		'
		Me.Combo_FieldSelect1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.Combo_FieldSelect1.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_FieldSelect1.Location = New System.Drawing.Point(128, 84)
		Me.Combo_FieldSelect1.Name = "Combo_FieldSelect1"
		Me.Combo_FieldSelect1.Size = New System.Drawing.Size(136, 21)
		Me.Combo_FieldSelect1.Sorted = True
		Me.Combo_FieldSelect1.TabIndex = 5
		'
		'Combo_Select1_Value
		'
		Me.Combo_Select1_Value.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_Select1_Value.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_Select1_Value.Location = New System.Drawing.Point(376, 84)
		Me.Combo_Select1_Value.Name = "Combo_Select1_Value"
		Me.Combo_Select1_Value.Size = New System.Drawing.Size(144, 21)
		Me.Combo_Select1_Value.TabIndex = 7
		'
		'Combo_Select2_Value
		'
		Me.Combo_Select2_Value.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_Select2_Value.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_Select2_Value.Location = New System.Drawing.Point(376, 111)
		Me.Combo_Select2_Value.Name = "Combo_Select2_Value"
		Me.Combo_Select2_Value.Size = New System.Drawing.Size(144, 21)
		Me.Combo_Select2_Value.TabIndex = 11
		'
		'Combo_FieldSelect2
		'
		Me.Combo_FieldSelect2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.Combo_FieldSelect2.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_FieldSelect2.Location = New System.Drawing.Point(128, 111)
		Me.Combo_FieldSelect2.Name = "Combo_FieldSelect2"
		Me.Combo_FieldSelect2.Size = New System.Drawing.Size(136, 21)
		Me.Combo_FieldSelect2.Sorted = True
		Me.Combo_FieldSelect2.TabIndex = 9
		'
		'Combo_Select2_Operator
		'
		Me.Combo_Select2_Operator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.Combo_Select2_Operator.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_Select2_Operator.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like"})
		Me.Combo_Select2_Operator.Location = New System.Drawing.Point(272, 111)
		Me.Combo_Select2_Operator.Name = "Combo_Select2_Operator"
		Me.Combo_Select2_Operator.Size = New System.Drawing.Size(96, 21)
		Me.Combo_Select2_Operator.TabIndex = 10
		'
		'Combo_Select3_Value
		'
		Me.Combo_Select3_Value.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_Select3_Value.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_Select3_Value.Location = New System.Drawing.Point(376, 138)
		Me.Combo_Select3_Value.Name = "Combo_Select3_Value"
		Me.Combo_Select3_Value.Size = New System.Drawing.Size(144, 21)
		Me.Combo_Select3_Value.TabIndex = 15
		'
		'Combo_FieldSelect3
		'
		Me.Combo_FieldSelect3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.Combo_FieldSelect3.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_FieldSelect3.Location = New System.Drawing.Point(128, 138)
		Me.Combo_FieldSelect3.Name = "Combo_FieldSelect3"
		Me.Combo_FieldSelect3.Size = New System.Drawing.Size(136, 21)
		Me.Combo_FieldSelect3.Sorted = True
		Me.Combo_FieldSelect3.TabIndex = 13
		'
		'Combo_Select3_Operator
		'
		Me.Combo_Select3_Operator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.Combo_Select3_Operator.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_Select3_Operator.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like"})
		Me.Combo_Select3_Operator.Location = New System.Drawing.Point(272, 138)
		Me.Combo_Select3_Operator.Name = "Combo_Select3_Operator"
		Me.Combo_Select3_Operator.Size = New System.Drawing.Size(96, 21)
		Me.Combo_Select3_Operator.TabIndex = 14
		'
		'Combo_AndOr_1
		'
		Me.Combo_AndOr_1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_AndOr_1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.Combo_AndOr_1.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_AndOr_1.Items.AddRange(New Object() {"AND", "OR"})
		Me.Combo_AndOr_1.Location = New System.Drawing.Point(532, 84)
		Me.Combo_AndOr_1.Name = "Combo_AndOr_1"
		Me.Combo_AndOr_1.Size = New System.Drawing.Size(96, 21)
		Me.Combo_AndOr_1.TabIndex = 8
		'
		'Combo_AndOr_2
		'
		Me.Combo_AndOr_2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_AndOr_2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.Combo_AndOr_2.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_AndOr_2.Items.AddRange(New Object() {"AND", "OR"})
		Me.Combo_AndOr_2.Location = New System.Drawing.Point(532, 111)
		Me.Combo_AndOr_2.Name = "Combo_AndOr_2"
		Me.Combo_AndOr_2.Size = New System.Drawing.Size(96, 21)
		Me.Combo_AndOr_2.TabIndex = 12
		'
		'RootMenu
		'
		Me.RootMenu.Location = New System.Drawing.Point(0, 0)
		Me.RootMenu.Name = "RootMenu"
		Me.RootMenu.Size = New System.Drawing.Size(816, 24)
		Me.RootMenu.TabIndex = 19
		Me.RootMenu.Text = " "
		'
		'Form_StatusStrip
		'
		Me.Form_StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Form_ProgressBar, Me.Label_Status})
		Me.Form_StatusStrip.Location = New System.Drawing.Point(0, 516)
		Me.Form_StatusStrip.Name = "Form_StatusStrip"
		Me.Form_StatusStrip.Size = New System.Drawing.Size(816, 22)
		Me.Form_StatusStrip.TabIndex = 105
		Me.Form_StatusStrip.Text = " "
		'
		'Form_ProgressBar
		'
		Me.Form_ProgressBar.Maximum = 20
		Me.Form_ProgressBar.Name = "Form_ProgressBar"
		Me.Form_ProgressBar.Size = New System.Drawing.Size(150, 17)
		Me.Form_ProgressBar.Step = 1
		Me.Form_ProgressBar.Visible = False
		'
		'Label_Status
		'
		Me.Label_Status.Name = "Label_Status"
		Me.Label_Status.Size = New System.Drawing.Size(10, 17)
		Me.Label_Status.Text = " "
		'
		'Panel1
		'
		Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Panel1.Controls.Add(Me.Check_ViewOnlyAccepted)
		Me.Panel1.Controls.Add(Me.Check_ViewNonAccepted)
		Me.Panel1.Controls.Add(Me.Check_ViewAuthoriseRejected)
		Me.Panel1.Controls.Add(Me.Check_ViewOnlyAuthorised)
		Me.Panel1.Controls.Add(Me.Check_ViewNonAuthorised)
		Me.Panel1.Controls.Add(Me.Check_ViewReviewRejected)
		Me.Panel1.Controls.Add(Me.Check_ViewOnlyReviewed)
		Me.Panel1.Controls.Add(Me.Check_ViewNonReviewed)
		Me.Panel1.Controls.Add(Me.Radio_Or)
		Me.Panel1.Controls.Add(Me.Radio_And)
		Me.Panel1.Controls.Add(Me.Label2)
		Me.Panel1.Location = New System.Drawing.Point(19, 165)
		Me.Panel1.Name = "Panel1"
		Me.Panel1.Size = New System.Drawing.Size(791, 75)
		Me.Panel1.TabIndex = 17
		'
		'Label2
		'
		Me.Label2.AutoSize = True
		Me.Label2.Location = New System.Drawing.Point(3, 4)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(66, 13)
		Me.Label2.TabIndex = 0
		Me.Label2.Text = "Select Logic"
		'
		'Radio_And
		'
		Me.Radio_And.AutoSize = True
		Me.Radio_And.Location = New System.Drawing.Point(6, 24)
		Me.Radio_And.Name = "Radio_And"
		Me.Radio_And.Size = New System.Drawing.Size(48, 17)
		Me.Radio_And.TabIndex = 1
		Me.Radio_And.TabStop = True
		Me.Radio_And.Text = "AND"
		Me.Radio_And.UseVisualStyleBackColor = True
		'
		'Radio_Or
		'
		Me.Radio_Or.AutoSize = True
		Me.Radio_Or.Location = New System.Drawing.Point(6, 47)
		Me.Radio_Or.Name = "Radio_Or"
		Me.Radio_Or.Size = New System.Drawing.Size(41, 17)
		Me.Radio_Or.TabIndex = 2
		Me.Radio_Or.TabStop = True
		Me.Radio_Or.Text = "OR"
		Me.Radio_Or.UseVisualStyleBackColor = True
		'
		'Check_ViewNonReviewed
		'
		Me.Check_ViewNonReviewed.AutoSize = True
		Me.Check_ViewNonReviewed.Location = New System.Drawing.Point(108, 7)
		Me.Check_ViewNonReviewed.Name = "Check_ViewNonReviewed"
		Me.Check_ViewNonReviewed.Size = New System.Drawing.Size(145, 17)
		Me.Check_ViewNonReviewed.TabIndex = 3
		Me.Check_ViewNonReviewed.Text = "View Non-Reviewed C/C"
		Me.Check_ViewNonReviewed.UseVisualStyleBackColor = True
		'
		'Check_ViewOnlyReviewed
		'
		Me.Check_ViewOnlyReviewed.AutoSize = True
		Me.Check_ViewOnlyReviewed.Location = New System.Drawing.Point(108, 30)
		Me.Check_ViewOnlyReviewed.Name = "Check_ViewOnlyReviewed"
		Me.Check_ViewOnlyReviewed.Size = New System.Drawing.Size(146, 17)
		Me.Check_ViewOnlyReviewed.TabIndex = 4
		Me.Check_ViewOnlyReviewed.Text = "View Only-Reviewed C/C"
		Me.Check_ViewOnlyReviewed.UseVisualStyleBackColor = True
		'
		'Check_ViewReviewRejected
		'
		Me.Check_ViewReviewRejected.AutoSize = True
		Me.Check_ViewReviewRejected.Location = New System.Drawing.Point(108, 53)
		Me.Check_ViewReviewRejected.Name = "Check_ViewReviewRejected"
		Me.Check_ViewReviewRejected.Size = New System.Drawing.Size(156, 17)
		Me.Check_ViewReviewRejected.TabIndex = 5
		Me.Check_ViewReviewRejected.Text = "View Review Rejected C/C"
		Me.Check_ViewReviewRejected.UseVisualStyleBackColor = True
		'
		'Check_ViewAuthoriseRejected
		'
		Me.Check_ViewAuthoriseRejected.AutoSize = True
		Me.Check_ViewAuthoriseRejected.Location = New System.Drawing.Point(323, 53)
		Me.Check_ViewAuthoriseRejected.Name = "Check_ViewAuthoriseRejected"
		Me.Check_ViewAuthoriseRejected.Size = New System.Drawing.Size(164, 17)
		Me.Check_ViewAuthoriseRejected.TabIndex = 8
		Me.Check_ViewAuthoriseRejected.Text = "View Authorise Rejected C/C"
		Me.Check_ViewAuthoriseRejected.UseVisualStyleBackColor = True
		'
		'Check_ViewOnlyAuthorised
		'
		Me.Check_ViewOnlyAuthorised.AutoSize = True
		Me.Check_ViewOnlyAuthorised.Location = New System.Drawing.Point(323, 30)
		Me.Check_ViewOnlyAuthorised.Name = "Check_ViewOnlyAuthorised"
		Me.Check_ViewOnlyAuthorised.Size = New System.Drawing.Size(148, 17)
		Me.Check_ViewOnlyAuthorised.TabIndex = 7
		Me.Check_ViewOnlyAuthorised.Text = "View Only Authorised C/C"
		Me.Check_ViewOnlyAuthorised.UseVisualStyleBackColor = True
		'
		'Check_ViewNonAuthorised
		'
		Me.Check_ViewNonAuthorised.AutoSize = True
		Me.Check_ViewNonAuthorised.Location = New System.Drawing.Point(323, 7)
		Me.Check_ViewNonAuthorised.Name = "Check_ViewNonAuthorised"
		Me.Check_ViewNonAuthorised.Size = New System.Drawing.Size(147, 17)
		Me.Check_ViewNonAuthorised.TabIndex = 6
		Me.Check_ViewNonAuthorised.Text = "View Non-Authorised C/C"
		Me.Check_ViewNonAuthorised.UseVisualStyleBackColor = True
		'
		'Check_ViewOnlyAccepted
		'
		Me.Check_ViewOnlyAccepted.AutoSize = True
		Me.Check_ViewOnlyAccepted.Location = New System.Drawing.Point(531, 30)
		Me.Check_ViewOnlyAccepted.Name = "Check_ViewOnlyAccepted"
		Me.Check_ViewOnlyAccepted.Size = New System.Drawing.Size(144, 17)
		Me.Check_ViewOnlyAccepted.TabIndex = 10
		Me.Check_ViewOnlyAccepted.Text = "View Only-Accepted C/C"
		Me.Check_ViewOnlyAccepted.UseVisualStyleBackColor = True
		'
		'Check_ViewNonAccepted
		'
		Me.Check_ViewNonAccepted.AutoSize = True
		Me.Check_ViewNonAccepted.Location = New System.Drawing.Point(531, 7)
		Me.Check_ViewNonAccepted.Name = "Check_ViewNonAccepted"
		Me.Check_ViewNonAccepted.Size = New System.Drawing.Size(143, 17)
		Me.Check_ViewNonAccepted.TabIndex = 9
		Me.Check_ViewNonAccepted.Text = "View Non-Accepted C/C"
		Me.Check_ViewNonAccepted.UseVisualStyleBackColor = True
		'
		'frmSelectChangeControl
		'
		Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
		Me.ClientSize = New System.Drawing.Size(816, 538)
		Me.Controls.Add(Me.Panel1)
		Me.Controls.Add(Me.Form_StatusStrip)
		Me.Controls.Add(Me.RootMenu)
		Me.Controls.Add(Me.Combo_AndOr_2)
		Me.Controls.Add(Me.Combo_AndOr_1)
		Me.Controls.Add(Me.Combo_Select3_Value)
		Me.Controls.Add(Me.Combo_FieldSelect3)
		Me.Controls.Add(Me.Combo_Select3_Operator)
		Me.Controls.Add(Me.Combo_Select2_Value)
		Me.Controls.Add(Me.Combo_FieldSelect2)
		Me.Controls.Add(Me.Combo_Select2_Operator)
		Me.Controls.Add(Me.Combo_Select1_Value)
		Me.Controls.Add(Me.Combo_FieldSelect1)
		Me.Controls.Add(Me.Combo_Select1_Operator)
		Me.Controls.Add(Me.Combo_Date_RaisedOperator)
		Me.Controls.Add(Me.Combo_ReferenceOperator)
		Me.Controls.Add(Me.Date_Raised)
		Me.Controls.Add(Me.Label3)
		Me.Controls.Add(Me.Combo_Reference)
		Me.Controls.Add(Me.Label1)
		Me.Controls.Add(Me.Combo_Product)
		Me.Controls.Add(Me.label_CptyIsFund)
		Me.Controls.Add(Me.Grid_ChangeControls)
		Me.MinimumSize = New System.Drawing.Size(824, 400)
		Me.Name = "frmSelectChangeControl"
		Me.Text = "Select Change Controls"
		CType(Me.Grid_ChangeControls, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Form_StatusStrip.ResumeLayout(False)
		Me.Form_StatusStrip.PerformLayout()
		Me.Panel1.ResumeLayout(False)
		Me.Panel1.PerformLayout()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

#End Region

#Region " Form Locals and Constants "

	' Form 'Parent', the Main Venice form.
	' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
	Private WithEvents _MainForm As VeniceMain

	' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
	Private FormTooltip As New ToolTip()

	' Form Menu


	' Form Constants, specific to the table being updated.

    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
	Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

	' Form Locals, initialised on 'New' defining what standard data items to use
    ''' <summary>
  ''' Variable holding the form's data table name. taken from the "ThisStandardDataset" object.
    ''' </summary>
	Private THIS_TABLENAME As String
    ''' <summary>
  ''' Variable holding the form's data adaptor name. taken from the "ThisStandardDataset" object.
    ''' </summary>
	Private THIS_ADAPTORNAME As String
    ''' <summary>
  ''' Variable holding the form's dataset name. taken from the "ThisStandardDataset" object.
    ''' </summary>
	Private THIS_DATASETNAME As String


	' The standard ChangeID for this form. e.g. tblFund
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
	Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

	' Form Specific Order fields
    ''' <summary>
  ''' Field Name to order the Select combo by.
    ''' </summary>
	Private THIS_FORM_OrderBy As String

	' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
	Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
	Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

	' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
	Private THIS_FORM_FormID As VeniceFormID

	' Data Structures

    ''' <summary>
    ''' My dataset
    ''' </summary>
	Private myDataset As DataSet
    ''' <summary>
    ''' My table
    ''' </summary>
	Private myTable As DataTable
    ''' <summary>
    ''' My connection
    ''' </summary>
	Private myConnection As SqlConnection
    ''' <summary>
    ''' My adaptor
    ''' </summary>
	Private myAdaptor As SqlDataAdapter
    ''' <summary>
    ''' My data view
    ''' </summary>
	Private myDataView As DataView

    ''' <summary>
    ''' The this standard dataset
    ''' </summary>
	Private ThisStandardDataset As RenaissanceGlobals.StandardDataset

	' Active Element.

    ''' <summary>
    ''' The __ is over cancel button
    ''' </summary>
	Private __IsOverCancelButton As Boolean
    ''' <summary>
    ''' The _ in use
    ''' </summary>
	Private _InUse As Boolean

	' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
	Private FormIsValid As Boolean
    ''' <summary>
    ''' The in paint
    ''' </summary>
	Private InPaint As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
	Private _FormOpenFailed As Boolean

	' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
	Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
	Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
	Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
	Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
	Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
	Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return __IsOverCancelButton
		End Get
		Set(ByVal Value As Boolean)
			__IsOverCancelButton = Value
		End Set
	End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
	Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
		Get
			Return InPaint
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
		Get
			Return _InUse
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmSelectChangeControl"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
	Public Sub New(ByVal pMainForm As VeniceMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()


		_MainForm = pMainForm
		AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

		_FormOpenFailed = False
		_InUse = True

		' Default Select and Order fields.

		THIS_FORM_OrderBy = "Change_ReferenceNumber"

		' Form Permissioning :-

		THIS_FORM_PermissionArea = Me.Name
		THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

		' 'This' form ID

		THIS_FORM_FormID = VeniceFormID.frmSelectChangeControl

		' This form's dataset type.

		ThisStandardDataset = RenaissanceStandardDatasets.tblChangeControl	' This Defines the Form Data !!! 


		' Data object names standard to this Form type.

		THIS_TABLENAME = ThisStandardDataset.TableName
		THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
		THIS_DATASETNAME = ThisStandardDataset.DatasetName

		THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

		' Establish / Retrieve data objects for this form.

		myConnection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)
		myAdaptor = MainForm.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, VENICE_CONNECTION, THIS_TABLENAME)
		myDataset = MainForm.Load_Table(ThisStandardDataset, False)
		myTable = myDataset.Tables(0)

		' Report
		SetChangeControlReportMenu(RootMenu)

		' Establish initial DataView and Initialise ChangeControls Grid.

		Dim thisCol As C1.Win.C1FlexGrid.Column

		myDataView = New DataView(myTable, "True", "", DataViewRowState.CurrentRows)

		Grid_ChangeControls.DataSource = myDataView

		' Format ChangeControls Grid, hide unwanted columns.

		Try
			Grid_ChangeControls.Cols("Change_ReferenceNumber").Move(0)
			Grid_ChangeControls.Cols("Product").Move(1)
			Grid_ChangeControls.Cols("Change_Category").Move(2)
			Grid_ChangeControls.Cols("Change_Priority").Move(3)
			Grid_ChangeControls.Cols("Date_Raised").Move(4)
			Grid_ChangeControls.Cols("Status").Move(5)
			Grid_ChangeControls.Cols("Text_Change").Move(6)
			Grid_ChangeControls.Cols("ChangeID").Move(7)
		Catch ex As Exception
		End Try

		For Each thisCol In Grid_ChangeControls.Cols
			Select Case thisCol.Name
				Case "Change_ReferenceNumber"
				Case "Product"
				Case "Change_Category"
				Case "Status"
				Case "Text_Change"
				Case "Change_Priority"
					thisCol.Format = "#,##0"
				Case "Date_Raised"
					thisCol.Format = DISPLAYMEMBER_DATEFORMAT

				Case Else
					thisCol.Visible = False
			End Select
		Next

		' Initialise Field select area

		Call BuildFieldSelectCombos()
		Me.Combo_AndOr_1.SelectedIndex = 0
		Me.Combo_AndOr_2.SelectedIndex = 0

		' Form Control Changed events
		AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

		AddHandler Combo_Product.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_ReferenceOperator.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_Reference.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_Reference.SelectedIndexChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_Date_RaisedOperator.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Date_Raised.ValueChanged, AddressOf Me.FormControlChanged

		AddHandler Combo_Select1_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_Select1_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_Select1_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp
		AddHandler Combo_Select2_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_Select2_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_Select2_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp
		AddHandler Combo_Select3_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_Select3_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_Select3_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp

		AddHandler Combo_AndOr_1.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_AndOr_2.SelectedValueChanged, AddressOf Me.FormControlChanged

		AddHandler Radio_And.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Radio_Or.CheckedChanged, AddressOf Me.FormControlChanged

		AddHandler Check_ViewNonReviewed.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Check_ViewOnlyReviewed.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Check_ViewReviewRejected.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Check_ViewNonAuthorised.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Check_ViewOnlyAuthorised.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Check_ViewAuthoriseRejected.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Check_ViewNonAccepted.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Check_ViewOnlyAccepted.CheckedChanged, AddressOf Me.FormControlChanged

		AddHandler Combo_Product.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_Reference.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_ReferenceOperator.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_Date_RaisedOperator.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		AddHandler Combo_Product.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_Reference.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_ReferenceOperator.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_Date_RaisedOperator.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

		AddHandler Combo_Product.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_Reference.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_ReferenceOperator.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_Date_RaisedOperator.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

		AddHandler Combo_Product.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_Reference.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_ReferenceOperator.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_Date_RaisedOperator.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

	End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

	' Form Initialisation code.
	'
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
	Public Sub ResetForm() Implements StandardVeniceForm.ResetForm
		THIS_FORM_OrderBy = "Change_ReferenceNumber"

		Call Form_Load(Me, New System.EventArgs)
	End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
	Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
		_FormOpenFailed = False
		_InUse = True

		' Initialise Data structures. Connection, Adaptor and Dataset.

		If Not (MainForm Is Nothing) Then
			FormIsValid = True
		Else
			MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myConnection Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myAdaptor Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myDataset Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If


		' Check User permissions
		Try
			Call CheckPermissions()
			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

				FormIsValid = False
				_FormOpenFailed = True

				Exit Sub
			End If
		Catch ex As Exception
			FormIsValid = False
			_FormOpenFailed = True

			Exit Sub
		End Try

		' Initialse form

		InPaint = True
		IsOverCancelButton = False

		' Initialise main select controls
		' Build Sorted data list from which this form operates

		Try
			Call SetProductCombo()
			Call SetTicketCombo()

			Me.Combo_Product.SelectedIndex = -1
			Me.Combo_ReferenceOperator.SelectedIndex = -1
			Me.Combo_Reference.SelectedIndex = -1
			Me.Combo_Date_RaisedOperator.SelectedIndex = 0
			Me.Date_Raised.Value = Renaissance_BaseDate

			Me.Check_ViewNonReviewed.Checked = False
			Me.Check_ViewOnlyReviewed.Checked = False
			Me.Check_ViewReviewRejected.Checked = False
			Me.Check_ViewNonAuthorised.Checked = False
			Me.Check_ViewOnlyAuthorised.Checked = False
			Me.Check_ViewAuthoriseRejected.Checked = False
			Me.Check_ViewNonAccepted.Checked = False
			Me.Check_ViewOnlyAccepted.Checked = False

			Me.Radio_Or.Checked = False
			Me.Radio_And.Checked = True

			Me.Combo_FieldSelect1.SelectedIndex = 0
			Me.Combo_FieldSelect2.SelectedIndex = 0
			Me.Combo_FieldSelect3.SelectedIndex = 0

			Call SetSortedRows()

			Call MainForm.SetComboSelectionLengths(Me)
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error initialising Select ChangeControl Form.", ex.StackTrace, True)
		End Try

		InPaint = False

	End Sub

    ''' <summary>
    ''' Handles the Closing event of the frm control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
	Private Sub frm_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		Dim HideForm As Boolean

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...

		_InUse = False

		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else
			HideForm = True
			If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			e.Cancel = True
		Else
			Try
				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

				RemoveHandler Combo_Product.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_ReferenceOperator.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Reference.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Reference.SelectedIndexChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Date_RaisedOperator.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Date_Raised.ValueChanged, AddressOf Me.FormControlChanged

				RemoveHandler Combo_Select1_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Select1_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Select1_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp
				RemoveHandler Combo_Select2_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Select2_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Select2_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp
				RemoveHandler Combo_Select3_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Select3_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Select3_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp

				RemoveHandler Combo_AndOr_1.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_AndOr_2.SelectedValueChanged, AddressOf Me.FormControlChanged

				RemoveHandler Radio_And.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Radio_Or.CheckedChanged, AddressOf Me.FormControlChanged

				RemoveHandler Check_ViewNonReviewed.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_ViewOnlyReviewed.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_ViewReviewRejected.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_ViewNonAuthorised.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_ViewOnlyAuthorised.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_ViewAuthoriseRejected.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_ViewNonAccepted.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_ViewOnlyAccepted.CheckedChanged, AddressOf Me.FormControlChanged

				RemoveHandler Combo_Product.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_Reference.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_ReferenceOperator.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_Date_RaisedOperator.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				RemoveHandler Combo_Product.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_Reference.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_ReferenceOperator.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_Date_RaisedOperator.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

				RemoveHandler Combo_Product.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_Reference.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_ReferenceOperator.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_Date_RaisedOperator.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

				RemoveHandler Combo_Product.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_Reference.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_ReferenceOperator.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_Date_RaisedOperator.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

			Catch ex As Exception
			End Try
		End If

	End Sub


#End Region

	' Routine to handle changes / updates to tables by this and other windows.
	' If this, or any other, form posts a change to a table, then it will invoke an update event 
	' detailing what tables have been altered.
	' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
	' the 'VeniceAutoUpdate' event of the main Venice form.
	' Each form may them react as appropriate to changes in any table that might impact it.
	'
    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		Dim OrgInPaint As Boolean
		Dim KnowledgeDateChanged As Boolean
		Dim RefreshGrid As Boolean

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		OrgInPaint = InPaint

		Try
			InPaint = True
			KnowledgeDateChanged = False
			RefreshGrid = False

			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
				KnowledgeDateChanged = True
			End If
		Catch ex As Exception
		End Try

		' ****************************************************************
		' Check for changes relevant to this form
		' ****************************************************************


		' Changes to the tblChangeControl table :-
		Try
			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblChangeControl) = True) Or KnowledgeDateChanged Then
				Call SetProductCombo()
				Call SetTicketCombo()

				RefreshGrid = True
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblChangeControl", ex.StackTrace, True)
		End Try

		' Changes to the KnowledgeDate :-
		Try
			If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
				RefreshGrid = True
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: KnowledgeDate", ex.StackTrace, True)
		End Try

		' Check TableUpdate against first Select Field
		Try
			If Me.Combo_FieldSelect1.SelectedIndex >= 0 Then
				Try

					' If the First FieldSelect combo has selected a Change Control Field which is related to
					' another table, as defined in tblReferentialIntegrity,
					' then if that related table is updated, the FieldSelect-Values combo must be updated.

					' If ChangedID is ChangeID of related table then ,..

					If (e.TableChanged(GetFieldChangeID(Combo_FieldSelect1.SelectedItem)) = True) Or KnowledgeDateChanged Then
						Dim SelectedValue As Object
						Dim TextValue As String

						' Save current Value Combo Value.

						SelectedValue = Nothing
						TextValue = ""

						If Me.Combo_Select1_Value.SelectedIndex >= 0 Then
							SelectedValue = Me.Combo_Select1_Value.SelectedValue
						ElseIf (SelectedValue Is Nothing) Then
							TextValue = Me.Combo_Select1_Value.Text
						End If

						' Update FieldSelect-Values Combo.

						Call UpdateSelectedValueCombo(Me.Combo_FieldSelect1.SelectedItem, Me.Combo_Select1_Value)

						' Restore Saved Value.

						If (Not (SelectedValue Is Nothing)) Then
							Me.Combo_Select1_Value.SelectedValue = SelectedValue
						ElseIf TextValue.Length > 0 Then
							Me.Combo_Select1_Value.Text = TextValue
						End If
					End If
				Catch ex As Exception
				End Try
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: First SelectField", ex.StackTrace, True)
		End Try

		' Check TableUpdate against second Select Field
		Try
			If Me.Combo_FieldSelect2.SelectedIndex >= 0 Then
				Try
					' GetFieldChangeID
					If (e.TableChanged(GetFieldChangeID(Combo_FieldSelect2.SelectedItem)) = True) Or KnowledgeDateChanged Then
						Dim SelectedValue As Object
						Dim TextValue As String

						SelectedValue = Nothing
						TextValue = ""

						If Me.Combo_Select2_Value.SelectedIndex >= 0 Then
							SelectedValue = Me.Combo_Select2_Value.SelectedValue
						ElseIf (SelectedValue Is Nothing) Then
							TextValue = Me.Combo_Select2_Value.Text
						End If

						Call UpdateSelectedValueCombo(Me.Combo_FieldSelect2.SelectedItem, Me.Combo_Select2_Value)

						If (Not (SelectedValue Is Nothing)) Then
							Me.Combo_Select2_Value.SelectedValue = SelectedValue
						ElseIf TextValue.Length > 0 Then
							Me.Combo_Select2_Value.Text = TextValue
						End If
					End If
				Catch ex As Exception
				End Try
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: Second SelectField", ex.StackTrace, True)
		End Try

		' Check TableUpdate against third Select Field
		Try
			If Me.Combo_FieldSelect3.SelectedIndex >= 0 Then
				Try
					' GetFieldChangeID
					If (e.TableChanged(GetFieldChangeID(Combo_FieldSelect3.SelectedItem)) = True) Or KnowledgeDateChanged Then
						Dim SelectedValue As Object
						Dim TextValue As String

						SelectedValue = Nothing
						TextValue = ""

						If Me.Combo_Select3_Value.SelectedIndex >= 0 Then
							SelectedValue = Me.Combo_Select3_Value.SelectedValue
						ElseIf (SelectedValue Is Nothing) Then
							TextValue = Me.Combo_Select3_Value.Text
						End If

						Call UpdateSelectedValueCombo(Me.Combo_FieldSelect3.SelectedItem, Me.Combo_Select3_Value)

						If (Not (SelectedValue Is Nothing)) Then
							Me.Combo_Select3_Value.SelectedValue = SelectedValue
						ElseIf TextValue.Length > 0 Then
							Me.Combo_Select3_Value.Text = TextValue
						End If
					End If
				Catch ex As Exception
				End Try
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: Third SelectField", ex.StackTrace, True)
		End Try


		' Changes to the tblUserPermissions table :-

		Try
			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

				' Check ongoing permissions.

				Call CheckPermissions()
				If (HasReadPermission = False) Then
					Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

					FormIsValid = False
					InPaint = OrgInPaint
					Me.Close()
					Exit Sub
				End If

				RefreshGrid = True

			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: tblUserPermissions", ex.StackTrace, True)
		End Try


		' ****************************************************************
		' Changes to the Main FORM table :-
		' (or the tblChangeControls table)
		' ****************************************************************

		Try
			If (e.TableChanged(THIS_FORM_ChangeID) = True) Or _
				 (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblChangeControl) = True) Or _
				 (RefreshGrid = True) Or _
				 KnowledgeDateChanged Then

				' Re-Set Controls etc.
				Call SetSortedRows()

			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: Main Form table", ex.StackTrace, True)
		End Try

		InPaint = OrgInPaint

	End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

    ''' <summary>
    ''' Gets the change control select string.
    ''' </summary>
    ''' <param name="OnlyUsertblChangeControlFields">if set to <c>true</c> [only usertbl change control fields].</param>
    ''' <returns>System.String.</returns>
	Private Function GetChangeControlSelectString(Optional ByVal OnlyUsertblChangeControlFields As Boolean = False) As String
		' *******************************************************************************
		' Build a Select String appropriate to the form status.
		' *******************************************************************************

		Dim SelectString As String
		Dim FieldSelectString As String

		SelectString = ""
		FieldSelectString = ""
		Me.MainForm.SetToolStripText(Label_Status, "")

		Try

			' Select on Fund...

			If (Me.Combo_Product.SelectedIndex > 0) Then
				If IsFieldChangeControlField("Product") = True Then
					SelectString = "(Product='" & Combo_Product.SelectedValue.ToString & "')"
				End If
			End If

			' Select on Change Reference Number...

			If (Me.Combo_ReferenceOperator.SelectedIndex > 0) Then
				If IsFieldChangeControlField("Change_ReferenceNumber") = True Then
					If (SelectString.Length > 0) Then
						SelectString &= " AND "
					End If

					SelectString &= "(Change_ReferenceNumber " & Combo_ReferenceOperator.SelectedItem.ToString & " '" & Combo_Reference.Text & "')"
				End If
			End If

			' Select on Date Raised

			If (Me.Combo_Date_RaisedOperator.SelectedIndex > 0) Then
				If (SelectString.Length > 0) Then
					SelectString &= " AND "
				End If

				SelectString &= "(Date_Raised " & Combo_Date_RaisedOperator.SelectedItem.ToString & " #" & Me.Date_Raised.Value.ToString(QUERY_SHORTDATEFORMAT) & "#)"
			End If

			' Field Select ...

			FieldSelectString = "("
			Dim ChangeControlDataset As DataSet
			Dim ChangeControlTable As DataTable
			Dim FieldName As String

			ChangeControlDataset = MainForm.Load_Table(RenaissanceStandardDatasets.tblChangeControl, False)
			ChangeControlTable = myDataset.Tables(0)

			' Select Field One.

			Try
				If (Me.Combo_FieldSelect1.SelectedIndex > 0) AndAlso (Me.Combo_Select1_Operator.SelectedIndex > 0) And (Me.Combo_Select1_Value.Text.Length > 0) Then
					' Get Field Name, Add 'ChangeControl' backon if necessary.

					FieldName = Combo_FieldSelect1.SelectedItem

					If (OnlyUsertblChangeControlFields = False) OrElse (IsFieldChangeControlField(FieldName) = True) Then
						' If the Selected Field Name is a String Field ...

						If (ChangeControlTable.Columns(FieldName).DataType Is GetType(System.String)) Then

							' If there is a value Selected, Use it - else use the Combo Text.

							If (Not (Combo_Select1_Value.SelectedValue Is Nothing)) Then
								FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.Text & " '" & Combo_Select1_Value.SelectedValue.ToString & "')"
							Else
								FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.Text & " '" & Combo_Select1_Value.Text.ToString & "')"
							End If

						ElseIf (ChangeControlTable.Columns(FieldName).DataType Is GetType(System.DateTime)) Then

							' For Dates, If there is no 'Selected' value use the Combo text if it is a Valid date, else
							' Use the 'Selected' Value if it is a date.

							If (Me.Combo_Select1_Value.SelectedValue Is Nothing) AndAlso (IsDate(Me.Combo_Select1_Value.Text)) Then
								FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select1_Value.Text).ToString(QUERY_SHORTDATEFORMAT) & "#)"
							ElseIf (Not (Me.Combo_Select1_Value.SelectedValue Is Nothing)) And IsDate(Combo_Select1_Value.SelectedValue) Then
								FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select1_Value.SelectedValue).ToString(QUERY_SHORTDATEFORMAT) & "#)"
							Else
								FieldSelectString &= "(true)"
							End If

						Else
							' Now Deemed to be numeric, or at least not in need of quotes / delimeters...
							' If there is no 'Selected' value use the Combo text if it is a Valid number, else
							' Use the 'Selected' Value if it is a valid numeric.

							If (Not (Combo_Select1_Value.SelectedValue Is Nothing)) AndAlso (IsNumeric(Combo_Select1_Value.SelectedValue)) Then
								FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.SelectedItem.ToString & " " & Combo_Select1_Value.SelectedValue.ToString & ")"
							ElseIf IsNumeric(Combo_Select1_Value.Text) Then
								FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.SelectedItem.ToString & " " & Combo_Select1_Value.Text & ")"
							Else
								FieldSelectString &= "(true)"
							End If
						End If
					End If
				End If

			Catch ex As Exception
			End Try

			' Select Field Two.

			Try
				If (Me.Combo_FieldSelect2.SelectedIndex > 0) AndAlso (Me.Combo_Select2_Operator.SelectedIndex > 0) And (Me.Combo_Select2_Value.Text.Length > 0) Then
					FieldName = Combo_FieldSelect2.SelectedItem

					If (OnlyUsertblChangeControlFields = False) OrElse (IsFieldChangeControlField(FieldName) = True) Then

						If (FieldSelectString.Length > 1) Then
							FieldSelectString &= " " & Me.Combo_AndOr_1.SelectedItem.ToString & " "
						End If

						If (ChangeControlTable.Columns(FieldName).DataType Is GetType(System.String)) Then

							If (Not (Combo_Select2_Value.SelectedValue Is Nothing)) Then
								FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.Text & " '" & Combo_Select2_Value.SelectedValue.ToString & "')"
							Else
								FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.Text & " '" & Combo_Select2_Value.Text.ToString & "')"
							End If

						ElseIf (ChangeControlTable.Columns(FieldName).DataType Is GetType(System.DateTime)) Then

							If (Me.Combo_Select2_Value.SelectedValue Is Nothing) AndAlso (IsDate(Me.Combo_Select2_Value.Text)) Then
								FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select2_Value.Text).ToString(QUERY_SHORTDATEFORMAT) & "#)"
							ElseIf (Not (Me.Combo_Select2_Value.SelectedValue Is Nothing)) And IsDate(Combo_Select2_Value.SelectedValue) Then
								FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select2_Value.SelectedValue).ToString(QUERY_SHORTDATEFORMAT) & "#)"
							Else
								FieldSelectString &= "(true)"
							End If

						Else
							If (Not (Combo_Select2_Value.SelectedValue Is Nothing)) AndAlso (IsNumeric(Combo_Select2_Value.SelectedValue)) Then
								FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.SelectedItem.ToString & " " & Combo_Select2_Value.SelectedValue.ToString & ")"
							ElseIf IsNumeric(Combo_Select2_Value.Text) Then
								FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.SelectedItem.ToString & " " & Combo_Select2_Value.Text & ")"
							Else
								FieldSelectString &= "(true)"
							End If
						End If
					End If
				End If
			Catch ex As Exception
			End Try


			' Select Field Three.

			Try
				If (Me.Combo_FieldSelect3.SelectedIndex > 0) AndAlso (Me.Combo_Select3_Operator.SelectedIndex > 0) And (Me.Combo_Select3_Value.Text.Length > 0) Then
					FieldName = Combo_FieldSelect3.SelectedItem

					If (OnlyUsertblChangeControlFields = False) OrElse (IsFieldChangeControlField(FieldName) = True) Then

						If (FieldSelectString.Length > 1) Then
							FieldSelectString &= " " & Me.Combo_AndOr_2.SelectedItem.ToString & " "
						End If

						If (ChangeControlTable.Columns(FieldName).DataType Is GetType(System.String)) Then

							If (Not (Combo_Select3_Value.SelectedValue Is Nothing)) Then
								FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.Text & " '" & Combo_Select3_Value.SelectedValue.ToString & "')"
							Else
								FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.Text & " '" & Combo_Select3_Value.Text.ToString & "')"
							End If

						ElseIf (ChangeControlTable.Columns(FieldName).DataType Is GetType(System.DateTime)) Then

							If (Me.Combo_Select3_Value.SelectedValue Is Nothing) AndAlso (IsDate(Me.Combo_Select3_Value.Text)) Then
								FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select3_Value.Text).ToString(QUERY_SHORTDATEFORMAT) & "#)"
							ElseIf (Not (Me.Combo_Select3_Value.SelectedValue Is Nothing)) And IsDate(Combo_Select3_Value.SelectedValue) Then
								FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select3_Value.SelectedValue).ToString(QUERY_SHORTDATEFORMAT) & "#)"
							Else
								FieldSelectString &= "(true)"
							End If

						Else
							If (Not (Combo_Select3_Value.SelectedValue Is Nothing)) AndAlso (IsNumeric(Combo_Select3_Value.SelectedValue)) Then
								FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.SelectedItem.ToString & " " & Combo_Select3_Value.SelectedValue.ToString & ")"
							ElseIf IsNumeric(Combo_Select3_Value.Text) Then
								FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.SelectedItem.ToString & " " & Combo_Select3_Value.Text & ")"
							Else
								FieldSelectString &= "(true)"
							End If
						End If
					End If
				End If
			Catch ex As Exception
			End Try

			FieldSelectString &= ")"

			' If the FieldSelect string is used, then tag it on to the main select string.

			If FieldSelectString.Length > 2 Then
				If (SelectString.Length > 0) Then
					SelectString &= " AND "
				End If
				SelectString &= FieldSelectString
			End If

			' Check for the various Check options

			If Me.Check_ViewNonReviewed.Checked Then
				If SelectString.Length <= 0 Then
					SelectString = "(Flag_ReviewedIT = False)"
				Else
					If (Me.Radio_And.Checked) Then
						SelectString &= " AND "
					Else
						SelectString &= " OR "
					End If

					SelectString &= "(Flag_ReviewedIT = False)"
				End If
			End If

			If Me.Check_ViewOnlyReviewed.Checked Then
				If SelectString.Length <= 0 Then
					SelectString = "(Flag_ReviewedIT = True)"
				Else
					If (Me.Radio_And.Checked) Then
						SelectString &= " AND "
					Else
						SelectString &= " OR "
					End If

					SelectString &= "(Flag_ReviewedIT = True)"
				End If
			End If

			If Me.Check_ViewReviewRejected.Checked Then
				If SelectString.Length <= 0 Then
					SelectString = "(Flag_ReviewRejected = True)"
				Else
					If (Me.Radio_And.Checked) Then
						SelectString &= " AND "
					Else
						SelectString &= " OR "
					End If

					SelectString &= "(Flag_ReviewRejected = True)"
				End If
			End If

			If Me.Check_ViewNonAuthorised.Checked Then
				If SelectString.Length <= 0 Then
					SelectString = "((Flag_AuthorisedIT = False) OR (Flag_AuthorisedOwner = False) OR (Flag_AuthorisedBusiness = False))"
				Else
					If (Me.Radio_And.Checked) Then
						SelectString &= " AND "
					Else
						SelectString &= " OR "
					End If

					SelectString &= "((Flag_AuthorisedIT = False) OR (Flag_AuthorisedOwner = False) OR (Flag_AuthorisedBusiness = False))"
				End If
			End If

			If Me.Check_ViewOnlyAuthorised.Checked Then
				If SelectString.Length <= 0 Then
					SelectString = "((Flag_AuthorisedIT = True) AND (Flag_AuthorisedOwner = True) AND (Flag_AuthorisedBusiness = True))"
				Else
					If (Me.Radio_And.Checked) Then
						SelectString &= " AND "
					Else
						SelectString &= " OR "
					End If

					SelectString &= "((Flag_AuthorisedIT = True) AND (Flag_AuthorisedOwner = True) AND (Flag_AuthorisedBusiness = True))"
				End If
			End If

			If Me.Check_ViewAuthoriseRejected.Checked Then
				If SelectString.Length <= 0 Then
					SelectString = "(Flag_AuthoriseRejected = True)"
				Else
					If (Me.Radio_And.Checked) Then
						SelectString &= " AND "
					Else
						SelectString &= " OR "
					End If

					SelectString &= "(Flag_AuthoriseRejected = True)"
				End If
			End If

			If Me.Check_ViewNonAccepted.Checked Then
				If SelectString.Length <= 0 Then
					SelectString = "((Flag_AcceptedIT = False) OR (Flag_AcceptedOwner = False) OR (Flag_AcceptedBusiness = False))"
				Else
					If (Me.Radio_And.Checked) Then
						SelectString &= " AND "
					Else
						SelectString &= " OR "
					End If

					SelectString &= "((Flag_AcceptedIT = False) OR (Flag_AcceptedOwner = False) OR (Flag_AcceptedBusiness = False))"
				End If
			End If

			If Me.Check_ViewOnlyAccepted.Checked Then
				If SelectString.Length <= 0 Then
					SelectString = "((Flag_AcceptedIT = True) AND (Flag_AcceptedOwner = True) AND (Flag_AcceptedBusiness = True))"
				Else
					If (Me.Radio_And.Checked) Then
						SelectString &= " AND "
					Else
						SelectString &= " OR "
					End If

					SelectString &= "((Flag_AcceptedIT = True) AND (Flag_AcceptedOwner = True) AND (Flag_AcceptedBusiness = True))"
				End If
			End If
			If SelectString.Length <= 0 Then
				SelectString = "true"
			End If

		Catch ex As Exception
			SelectString = "true"
		End Try

		Return SelectString

	End Function

    ''' <summary>
    ''' Determines whether [is field change control field] [the specified field name].
    ''' </summary>
    ''' <param name="FieldName">Name of the field.</param>
    ''' <returns><c>true</c> if [is field change control field] [the specified field name]; otherwise, <c>false</c>.</returns>
	Private Function IsFieldChangeControlField(ByVal FieldName As String) As Boolean
		' *******************************************************************************
		' Simple function to return a boolean value indicating whether or not a column name is
		' part of the tblChangeControls table.
		' *******************************************************************************

		Dim ChangeControlDS As RenaissanceDataClass.DSChangeControl

		Try
			ChangeControlDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblChangeControl, False)

			If (ChangeControlDS Is Nothing) Then
				Return False
			Else
				Return ChangeControlDS.tblChangeControl.Columns.Contains(FieldName)
			End If
		Catch ex As Exception
			Return (False)
		End Try

		Return (False)
	End Function

    ''' <summary>
    ''' Sets the sorted rows.
    ''' </summary>
	Private Sub SetSortedRows()
		' *******************************************************************************
		' Build a Select String appropriate to the form status and apply it to the DataView object.
		' *******************************************************************************

		Dim SelectString As String

		SelectString = GetChangeControlSelectString()

		Try
			If (Not (myDataView.RowFilter = SelectString)) Then
				myDataView.RowFilter = SelectString

			End If
		Catch ex As Exception
			SelectString = "true"
			myDataView.RowFilter = SelectString
		End Try

		Me.Label_Status.Text = "(" & myDataView.Count.ToString & " Records) " & SelectString

	End Sub


	' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
	Private Sub CheckPermissions()
		' *******************************************************************************
		'
		' *******************************************************************************

		Dim Permissions As Integer

		Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

		HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
		HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
		HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
		HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

	End Sub

    ''' <summary>
  ''' Basic event handler, called when a form value is changed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
		' *******************************************************************************
		' In the event of one of the Form Controls changing, refresh the data grid.
		' *******************************************************************************

		If InPaint = False Then

			Call SetSortedRows()

		End If
	End Sub

    ''' <summary>
    ''' Handles the KeyUp event of the Combo_FieldSelect control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
	Private Sub Combo_FieldSelect_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Combo_Select1_Value.KeyUp
		' *******************************************************************************
		' In the event of one of the Form Controls changing, refresh the data grid.
		' *******************************************************************************

		If InPaint = False Then

			Call SetSortedRows()

		End If
	End Sub


    ''' <summary>
    ''' Builds the field select combos.
    ''' </summary>
	Private Sub BuildFieldSelectCombos()
		' *********************************************************************************
		' Build the FieldSelect combo boxes.
		'
		' *********************************************************************************

		Dim ChangeControlDataset As DataSet
		Dim ChangeControlTable As DataTable

		Dim ColumnName As String
		Dim thisColumn As DataColumn

		ChangeControlDataset = MainForm.Load_Table(RenaissanceStandardDatasets.tblChangeControl, False)
		ChangeControlTable = ChangeControlDataset.Tables(0)

		Combo_FieldSelect1.Items.Clear()
		Combo_Select1_Value.Items.Clear()
		Combo_FieldSelect1.Items.Add("")

		Combo_FieldSelect2.Items.Clear()
		Combo_Select2_Value.Items.Clear()
		Combo_FieldSelect2.Items.Add("")

		Combo_FieldSelect3.Items.Clear()
		Combo_Select3_Value.Items.Clear()
		Combo_FieldSelect3.Items.Add("")

		For Each thisColumn In myTable.Columns
			Try
				ColumnName = ChangeControlTable.Columns(thisColumn.ColumnName).ColumnName

				Combo_FieldSelect1.Items.Add(ColumnName)
				Combo_FieldSelect2.Items.Add(ColumnName)
				Combo_FieldSelect3.Items.Add(ColumnName)

			Catch ex As Exception
			End Try
		Next

	End Sub

    ''' <summary>
    ''' Updates the selected value combo.
    ''' </summary>
    ''' <param name="pFieldName">Name of the p field.</param>
    ''' <param name="pValueCombo">The p value combo.</param>
	Private Sub UpdateSelectedValueCombo(ByVal pFieldName As String, ByRef pValueCombo As ComboBox)
		' *******************************************************************************
		' Build a Value Combo Box appropriate to the chosen DataTable Select Field.
		'
		' By default the combo will contain all existing values for the chosen field, however
		' If a relationship is defined for thi table and field, then a Combo will be built
		' which reflects this relationship.
		' e.g. the tblReferentialIntegrity table defined a relationship between the
		' TransactionCounterparty field and the tblCounterparty.Counterparty field, thus if
		' the chosen Select field is 'Counterparty.' then the Value combo will be built using
		' the CounterpartyID & CounterpartyName details from the tblCounterparty table.
		'
		' *******************************************************************************

		Dim tblRefIntegrity As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityDataTable
		Dim IntegrityRows() As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityRow
		Dim ChangeControlDataset As DataSet
		Dim ChangeControlTable As DataTable
		Dim SortOrder As Boolean

		SortOrder = True

		' Clear the Value Combo.

		Try
			pValueCombo.DataSource = Nothing
			pValueCombo.DisplayMember = ""
			pValueCombo.ValueMember = ""
			pValueCombo.Items.Clear()
		Catch ex As Exception
		End Try

		' By default, make the Combo a Standard Dropdown, so that non-list items can be entered.

		pValueCombo.DropDownStyle = ComboBoxStyle.DropDown

		' Exit if no FiledName is given (having cleared the combo).

		If (pFieldName.Length <= 0) Then Exit Sub

		' Get a handle to the Standard ChangeControls Table.
		' This is so that the field types can be determined and used.

		Try
			ChangeControlDataset = MainForm.Load_Table(RenaissanceStandardDatasets.tblChangeControl, False)
			ChangeControlTable = myDataset.Tables(0)
		Catch ex As Exception
			Exit Sub
		End Try

		' If the selected field is a Date then sort the selection combo in reverse order.

		If (ChangeControlTable.Columns(pFieldName).DataType Is GetType(System.DateTime)) Then
			SortOrder = False
		End If

		' Get a handle to the Referential Integrity table and the row matching this table and field.
		' this table defines a relationship between ChangeControl Table fields and other tables.
		' This information can be used to build more meaningfull Value Combos.

		Try
			tblRefIntegrity = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblReferentialIntegrity, False), RenaissanceDataClass.DSReferentialIntegrity).tblReferentialIntegrity
			IntegrityRows = tblRefIntegrity.Select("(FeedsTable LIKE 'tblChangeControl') AND (FeedsField = '" & pFieldName & "')", "RN")
			If (IntegrityRows.Length <= 0) Then
				GoTo StandardExit
			End If

		Catch ex As Exception
			Exit Sub
		End Try

		If (IntegrityRows(0).IsDescriptionFieldNull) Then
			' No Linked Description Field

			GoTo StandardExit
		End If

		' OK, a referential record exists.
		' Determine the Table and Field Names to use to build the Value Combo.

		Dim TableName As String
		Dim TableField As String
		Dim DescriptionField As String
		Dim stdDS As StandardDataset
		Dim thisChangeID As RenaissanceChangeID

		Try

			TableName = IntegrityRows(0).TableName
			TableField = IntegrityRows(0).TableField
			DescriptionField = IntegrityRows(0).DescriptionField

			thisChangeID = CType(System.Enum.Parse(GetType(RenaissanceChangeID), TableName), RenaissanceChangeID)

			stdDS = RenaissanceStandardDatasets.GetStandardDataset(thisChangeID)

			' Build the appropriate Values Combo.

			Call MainForm.SetTblGenericCombo( _
			pValueCombo, _
			stdDS, _
			DescriptionField, _
			TableField, _
			"", False, SortOrder, True)		 ' 

			' For Referential Integrity generated combo's, make the Combo a 
			' DropDownList

			pValueCombo.DropDownStyle = ComboBoxStyle.DropDownList

		Catch ex As Exception
			GoTo StandardExit

		End Try

		Exit Sub

StandardExit:

		' Build the standard Combo, just use the values from the given field.

		Call MainForm.SetTblGenericCombo( _
		pValueCombo, _
		RenaissanceStandardDatasets.tblChangeControl, _
		pFieldName, _
		pFieldName, _
		"", True, SortOrder)	 ' 

	End Sub

    ''' <summary>
    ''' Gets the field change ID.
    ''' </summary>
    ''' <param name="pFieldName">Name of the p field.</param>
    ''' <returns>RenaissanceChangeID.</returns>
	Private Function GetFieldChangeID(ByVal pFieldName As String) As RenaissanceChangeID
		Dim tblRefIntegrity As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityDataTable
		Dim IntegrityRows() As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityRow

		Try
			tblRefIntegrity = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblReferentialIntegrity, False), RenaissanceDataClass.DSReferentialIntegrity).tblReferentialIntegrity
			IntegrityRows = tblRefIntegrity.Select("(FeedsTable LIKE '%tblChangeControl') AND (FeedsField = '" & pFieldName & "')", "RN")
			If (IntegrityRows.Length <= 0) Then
				Return RenaissanceChangeID.None
				Exit Function
			End If

		Catch ex As Exception
			Return RenaissanceChangeID.None
			Exit Function
		End Try


		If (IntegrityRows(0).IsDescriptionFieldNull) Then
			Return RenaissanceChangeID.None
			Exit Function
		End If

		' OK, a referential record exists.
		' Determine the Table and Field Names to use to build the Value Combo.

		Dim TableName As String
		Dim TableField As String
		Dim DescriptionField As String
		Dim thisChangeID As RenaissanceChangeID

		Try

			TableName = IntegrityRows(0).TableName
			TableField = IntegrityRows(0).TableField
			DescriptionField = IntegrityRows(0).DescriptionField

			thisChangeID = CType(System.Enum.Parse(GetType(RenaissanceChangeID), TableName), RenaissanceChangeID)

			Return thisChangeID
			Exit Function
		Catch ex As Exception
		End Try

		Return RenaissanceChangeID.None
		Exit Function

	End Function


    ''' <summary>
    ''' Handles the DoubleClick event of the Grid_ChangeControls control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Grid_ChangeControls_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid_ChangeControls.DoubleClick
		' ***************************************************************************************
		' Spawn an Add/Edit ChangeControls form reflecting the chosen selection of ChangeControls
		' and pointing to the ChangeControl which was double clicked.
		' ***************************************************************************************

		Dim ChangeID As Integer
		Dim thisFormHandle As RenaissanceGlobals.RenaissanceFormHandle

		ChangeID = Me.Grid_ChangeControls.Rows(Grid_ChangeControls.RowSel).DataSource("ChangeID")

		thisFormHandle = MainForm.New_VeniceForm(VeniceFormID.frmChangeControlReview)
		CType(thisFormHandle.Form, frmChangeControlReview).FormSelectCriteria = GetChangeControlSelectString(True)
		CType(thisFormHandle.Form, frmChangeControlReview).MoveToAuditID(ChangeID)

	End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_FieldSelect1 control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_FieldSelect1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_FieldSelect1.SelectedIndexChanged
		' ***************************************************************************************
		' Field Select Combo Chosen, Update the associated Values Combo.
		'
		' ***************************************************************************************

		Call UpdateSelectedValueCombo(Me.Combo_FieldSelect1.SelectedItem, Me.Combo_Select1_Value)
		Me.Combo_Select1_Operator.SelectedIndex = 1

	End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_FieldSelect2 control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_FieldSelect2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_FieldSelect2.SelectedIndexChanged
		' ***************************************************************************************
		' Field Select Combo Chosen, Update the associated Values Combo.
		'
		' ***************************************************************************************
		Call UpdateSelectedValueCombo(Me.Combo_FieldSelect2.SelectedItem, Me.Combo_Select2_Value)
		Me.Combo_Select2_Operator.SelectedIndex = 1
	End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_FieldSelect3 control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_FieldSelect3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_FieldSelect3.SelectedIndexChanged
		' ***************************************************************************************
		' Field Select Combo Chosen, Update the associated Values Combo.
		'
		' ***************************************************************************************
		Call UpdateSelectedValueCombo(Me.Combo_FieldSelect3.SelectedItem, Me.Combo_Select3_Value)
		Me.Combo_Select3_Operator.SelectedIndex = 1
	End Sub

#End Region

#Region " Set Form Combos (Form Specific Code) "

    ''' <summary>
    ''' Sets the product combo.
    ''' </summary>
	Private Sub SetProductCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_Product, _
		RenaissanceStandardDatasets.tblChangeControl, _
		"Product", _
		"Product", _
		"", True, True, True, "")		' 

	End Sub


    ''' <summary>
    ''' Sets the ticket combo.
    ''' </summary>
	Private Sub SetTicketCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_Reference, _
		RenaissanceStandardDatasets.tblChangeControl, _
		"Change_ReferenceNumber", _
		"Change_ReferenceNumber", _
		"", True, True, True)			' 

	End Sub


#End Region

#Region " Change Control report Menu"

    ''' <summary>
    ''' Sets the change control report menu.
    ''' </summary>
    ''' <param name="RootMenu">The root menu.</param>
    ''' <returns>MenuStrip.</returns>
	Private Function SetChangeControlReportMenu(ByRef RootMenu As MenuStrip) As MenuStrip

		Dim ReportMenu As New ToolStripMenuItem("Change Control &Reports")
		Dim newMenuItem As ToolStripMenuItem

		newMenuItem = ReportMenu.DropDownItems.Add("Change Control &Form", Nothing, AddressOf Me.rpt_ChangeControlForm)
		newMenuItem = ReportMenu.DropDownItems.Add("Change Control &Summary", Nothing, AddressOf Me.rpt_ChangeControlSummary)

		RootMenu.Items.Add(ReportMenu)
		Return RootMenu

	End Function

    ''' <summary>
    ''' Handles the ChangeControlForm event of the rpt control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Private Sub rpt_ChangeControlForm(ByVal sender As Object, ByVal e As EventArgs)
		Dim ThisDataset As StandardDataset

		ThisDataset = RenaissanceStandardDatasets.GetStandardDataset(RenaissanceChangeID.tblChangeControl)
		If (ThisDataset.NotedIDsHaveChanged) Then
			MainForm.Load_Table(ThisDataset, True)
			Call SetSortedRows()
		End If

		MainForm.MainReportHandler.DisplayReport(0, "rptCCForm", myDataView, Form_ProgressBar)
	End Sub

    ''' <summary>
    ''' Handles the ChangeControlSummary event of the rpt control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Private Sub rpt_ChangeControlSummary(ByVal sender As Object, ByVal e As EventArgs)
		Dim ThisDataset As StandardDataset

		ThisDataset = RenaissanceStandardDatasets.GetStandardDataset(RenaissanceChangeID.tblChangeControl)
		If (ThisDataset.NotedIDsHaveChanged) Then
			MainForm.Load_Table(ThisDataset, True)
			Call SetSortedRows()
		End If

		MainForm.MainReportHandler.DisplayReport(0, "rptCCSummary", myDataView, Form_ProgressBar)
	End Sub

#End Region

#Region " Bug hunting "

	' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
	' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
	' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

    ''' <summary>
    ''' Called when [row updating].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
	Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
		Dim a As Integer

		If Not e.Command Is Nothing Then
			a = 1
		End If

		If Not e.Errors Is Nothing Then
			a = 2
		End If
	End Sub

    ''' <summary>
    ''' Called when [row updated].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
	Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
		Dim a As Integer

		If Not e.Command Is Nothing Then
			a = 1
		End If

		If Not e.Errors Is Nothing Then
			a = 2
		End If
	End Sub

    ''' <summary>
    ''' Called when [row fill error].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
	Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
		Dim a As Integer

		a = 1
	End Sub

#End Region




End Class
