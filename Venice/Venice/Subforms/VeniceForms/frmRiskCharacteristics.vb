' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 01-17-2013
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmRiskCharacteristics.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals


''' <summary>
''' Class frmRiskCharacteristics
''' </summary>
Public Class frmRiskCharacteristics

	Inherits System.Windows.Forms.Form
	Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmRiskCharacteristics"/> class from being created.
    ''' </summary>
	Private Sub New()
		MyBase.New()

		'This call is required by the Windows Form Designer.
		InitializeComponent()

		'Add any initialization after the InitializeComponent() call

	End Sub

	'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
    ''' <summary>
    ''' The LBL characteristic name
    ''' </summary>
	Friend WithEvents lblCharacteristicName As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit audit ID
    ''' </summary>
	Friend WithEvents editAuditID As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The edit characteristic name
    ''' </summary>
	Friend WithEvents editCharacteristicName As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The BTN nav first
    ''' </summary>
	Friend WithEvents btnNavFirst As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN nav prev
    ''' </summary>
	Friend WithEvents btnNavPrev As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN nav next
    ''' </summary>
	Friend WithEvents btnNavNext As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN last
    ''' </summary>
	Friend WithEvents btnLast As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN add
    ''' </summary>
	Friend WithEvents btnAdd As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN delete
    ''' </summary>
	Friend WithEvents btnDelete As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN cancel
    ''' </summary>
	Friend WithEvents btnCancel As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN save
    ''' </summary>
	Friend WithEvents btnSave As System.Windows.Forms.Button
    ''' <summary>
    ''' The combo_ select characteristic code
    ''' </summary>
	Friend WithEvents Combo_SelectCharacteristicCode As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The panel1
    ''' </summary>
	Friend WithEvents Panel1 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The label1
    ''' </summary>
	Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The group box1
    ''' </summary>
	Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The root menu
    ''' </summary>
	Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
    ''' <summary>
    ''' The combo_ category
    ''' </summary>
	Friend WithEvents Combo_Category As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label4
    ''' </summary>
	Friend WithEvents Label4 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ characteristic type
    ''' </summary>
	Friend WithEvents Combo_CharacteristicType As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The LBL characteristic type
    ''' </summary>
	Friend WithEvents lblCharacteristicType As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ fixed value
    ''' </summary>
	Friend WithEvents edit_FixedValue As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The label2
    ''' </summary>
	Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The check_ fixed value
    ''' </summary>
	Friend WithEvents Check_FixedValue As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ default value
    ''' </summary>
	Friend WithEvents Check_DefaultValue As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The edit_ default value
    ''' </summary>
	Friend WithEvents edit_DefaultValue As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The label3
    ''' </summary>
	Friend WithEvents Label3 As System.Windows.Forms.Label
    ''' <summary>
    ''' The check_ boolean value
    ''' </summary>
	Friend WithEvents Check_BooleanValue As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The edit_ boolean value
    ''' </summary>
	Friend WithEvents edit_BooleanValue As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The label5
    ''' </summary>
	Friend WithEvents Label5 As System.Windows.Forms.Label
    ''' <summary>
    ''' The compound group
    ''' </summary>
	Friend WithEvents CompoundGroup As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The combo_ add characteristic
    ''' </summary>
	Friend WithEvents Combo_AddCharacteristic As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label6
    ''' </summary>
	Friend WithEvents Label6 As System.Windows.Forms.Label
    ''' <summary>
    ''' The list_ children
    ''' </summary>
	Friend WithEvents List_Children As System.Windows.Forms.ListBox
    ''' <summary>
    ''' The button_ delete child
    ''' </summary>
	Friend WithEvents Button_DeleteChild As System.Windows.Forms.Button
    ''' <summary>
    ''' The label7
    ''' </summary>
	Friend WithEvents Label7 As System.Windows.Forms.Label
    ''' <summary>
    ''' The button_ add child
    ''' </summary>
	Friend WithEvents Button_AddChild As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN close
    ''' </summary>
	Friend WithEvents btnClose As System.Windows.Forms.Button
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Me.lblCharacteristicName = New System.Windows.Forms.Label
		Me.editAuditID = New System.Windows.Forms.TextBox
		Me.editCharacteristicName = New System.Windows.Forms.TextBox
		Me.btnNavFirst = New System.Windows.Forms.Button
		Me.btnNavPrev = New System.Windows.Forms.Button
		Me.btnNavNext = New System.Windows.Forms.Button
		Me.btnLast = New System.Windows.Forms.Button
		Me.btnAdd = New System.Windows.Forms.Button
		Me.btnDelete = New System.Windows.Forms.Button
		Me.btnCancel = New System.Windows.Forms.Button
		Me.btnSave = New System.Windows.Forms.Button
		Me.Combo_SelectCharacteristicCode = New System.Windows.Forms.ComboBox
		Me.Panel1 = New System.Windows.Forms.Panel
		Me.Label1 = New System.Windows.Forms.Label
		Me.GroupBox1 = New System.Windows.Forms.GroupBox
		Me.btnClose = New System.Windows.Forms.Button
		Me.RootMenu = New System.Windows.Forms.MenuStrip
		Me.Combo_Category = New System.Windows.Forms.ComboBox
		Me.Label4 = New System.Windows.Forms.Label
		Me.Combo_CharacteristicType = New System.Windows.Forms.ComboBox
		Me.lblCharacteristicType = New System.Windows.Forms.Label
		Me.edit_FixedValue = New RenaissanceControls.NumericTextBox
		Me.Label2 = New System.Windows.Forms.Label
		Me.Check_FixedValue = New System.Windows.Forms.CheckBox
		Me.Check_DefaultValue = New System.Windows.Forms.CheckBox
		Me.edit_DefaultValue = New RenaissanceControls.NumericTextBox
		Me.Label3 = New System.Windows.Forms.Label
		Me.Check_BooleanValue = New System.Windows.Forms.CheckBox
		Me.edit_BooleanValue = New RenaissanceControls.NumericTextBox
		Me.Label5 = New System.Windows.Forms.Label
		Me.CompoundGroup = New System.Windows.Forms.GroupBox
		Me.List_Children = New System.Windows.Forms.ListBox
		Me.Button_DeleteChild = New System.Windows.Forms.Button
		Me.Label7 = New System.Windows.Forms.Label
		Me.Button_AddChild = New System.Windows.Forms.Button
		Me.Combo_AddCharacteristic = New System.Windows.Forms.ComboBox
		Me.Label6 = New System.Windows.Forms.Label
		Me.Panel1.SuspendLayout()
		Me.CompoundGroup.SuspendLayout()
		Me.SuspendLayout()
		'
		'lblCharacteristicName
		'
		Me.lblCharacteristicName.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.lblCharacteristicName.Location = New System.Drawing.Point(8, 74)
		Me.lblCharacteristicName.Name = "lblCharacteristicName"
		Me.lblCharacteristicName.Size = New System.Drawing.Size(100, 23)
		Me.lblCharacteristicName.TabIndex = 39
		Me.lblCharacteristicName.Text = "Characteristic Name"
		'
		'editAuditID
		'
		Me.editAuditID.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.editAuditID.Enabled = False
		Me.editAuditID.Location = New System.Drawing.Point(451, 30)
		Me.editAuditID.Name = "editAuditID"
		Me.editAuditID.Size = New System.Drawing.Size(53, 20)
		Me.editAuditID.TabIndex = 1
		'
		'editCharacteristicName
		'
		Me.editCharacteristicName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.editCharacteristicName.Location = New System.Drawing.Point(120, 74)
		Me.editCharacteristicName.Name = "editCharacteristicName"
		Me.editCharacteristicName.Size = New System.Drawing.Size(384, 20)
		Me.editCharacteristicName.TabIndex = 2
		'
		'btnNavFirst
		'
		Me.btnNavFirst.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnNavFirst.Location = New System.Drawing.Point(8, 8)
		Me.btnNavFirst.Name = "btnNavFirst"
		Me.btnNavFirst.Size = New System.Drawing.Size(40, 28)
		Me.btnNavFirst.TabIndex = 0
		Me.btnNavFirst.Text = "<<"
		'
		'btnNavPrev
		'
		Me.btnNavPrev.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnNavPrev.Location = New System.Drawing.Point(49, 8)
		Me.btnNavPrev.Name = "btnNavPrev"
		Me.btnNavPrev.Size = New System.Drawing.Size(35, 28)
		Me.btnNavPrev.TabIndex = 1
		Me.btnNavPrev.Text = "<"
		'
		'btnNavNext
		'
		Me.btnNavNext.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnNavNext.Location = New System.Drawing.Point(88, 8)
		Me.btnNavNext.Name = "btnNavNext"
		Me.btnNavNext.Size = New System.Drawing.Size(35, 28)
		Me.btnNavNext.TabIndex = 2
		Me.btnNavNext.Text = ">"
		'
		'btnLast
		'
		Me.btnLast.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnLast.Location = New System.Drawing.Point(124, 8)
		Me.btnLast.Name = "btnLast"
		Me.btnLast.Size = New System.Drawing.Size(40, 28)
		Me.btnLast.TabIndex = 3
		Me.btnLast.Text = ">>"
		'
		'btnAdd
		'
		Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnAdd.Location = New System.Drawing.Point(176, 8)
		Me.btnAdd.Name = "btnAdd"
		Me.btnAdd.Size = New System.Drawing.Size(75, 28)
		Me.btnAdd.TabIndex = 4
		Me.btnAdd.Text = "&New"
		'
		'btnDelete
		'
		Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnDelete.Location = New System.Drawing.Point(177, 479)
		Me.btnDelete.Name = "btnDelete"
		Me.btnDelete.Size = New System.Drawing.Size(75, 28)
		Me.btnDelete.TabIndex = 8
		Me.btnDelete.Text = "&Delete"
		'
		'btnCancel
		'
		Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
		Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnCancel.Location = New System.Drawing.Point(261, 479)
		Me.btnCancel.Name = "btnCancel"
		Me.btnCancel.Size = New System.Drawing.Size(75, 28)
		Me.btnCancel.TabIndex = 9
		Me.btnCancel.Text = "&Cancel"
		'
		'btnSave
		'
		Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnSave.Location = New System.Drawing.Point(89, 479)
		Me.btnSave.Name = "btnSave"
		Me.btnSave.Size = New System.Drawing.Size(75, 28)
		Me.btnSave.TabIndex = 7
		Me.btnSave.Text = "&Save"
		'
		'Combo_SelectCharacteristicCode
		'
		Me.Combo_SelectCharacteristicCode.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_SelectCharacteristicCode.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_SelectCharacteristicCode.Location = New System.Drawing.Point(120, 30)
		Me.Combo_SelectCharacteristicCode.Name = "Combo_SelectCharacteristicCode"
		Me.Combo_SelectCharacteristicCode.Size = New System.Drawing.Size(325, 21)
		Me.Combo_SelectCharacteristicCode.TabIndex = 0
		'
		'Panel1
		'
		Me.Panel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Panel1.Controls.Add(Me.btnNavFirst)
		Me.Panel1.Controls.Add(Me.btnNavPrev)
		Me.Panel1.Controls.Add(Me.btnNavNext)
		Me.Panel1.Controls.Add(Me.btnLast)
		Me.Panel1.Controls.Add(Me.btnAdd)
		Me.Panel1.Location = New System.Drawing.Point(125, 423)
		Me.Panel1.Name = "Panel1"
		Me.Panel1.Size = New System.Drawing.Size(260, 48)
		Me.Panel1.TabIndex = 6
		'
		'Label1
		'
		Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label1.Location = New System.Drawing.Point(8, 30)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(100, 23)
		Me.Label1.TabIndex = 17
		Me.Label1.Text = "Select"
		'
		'GroupBox1
		'
		Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.GroupBox1.Location = New System.Drawing.Point(8, 62)
		Me.GroupBox1.Name = "GroupBox1"
		Me.GroupBox1.Size = New System.Drawing.Size(507, 4)
		Me.GroupBox1.TabIndex = 77
		Me.GroupBox1.TabStop = False
		Me.GroupBox1.Text = "GroupBox1"
		'
		'btnClose
		'
		Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnClose.Location = New System.Drawing.Point(345, 479)
		Me.btnClose.Name = "btnClose"
		Me.btnClose.Size = New System.Drawing.Size(75, 28)
		Me.btnClose.TabIndex = 10
		Me.btnClose.Text = "&Close"
		'
		'RootMenu
		'
		Me.RootMenu.Location = New System.Drawing.Point(0, 0)
		Me.RootMenu.Name = "RootMenu"
		Me.RootMenu.Size = New System.Drawing.Size(511, 24)
		Me.RootMenu.TabIndex = 11
		Me.RootMenu.Text = " "
		'
		'Combo_Category
		'
		Me.Combo_Category.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_Category.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_Category.Location = New System.Drawing.Point(120, 104)
		Me.Combo_Category.Name = "Combo_Category"
		Me.Combo_Category.Size = New System.Drawing.Size(383, 21)
		Me.Combo_Category.TabIndex = 4
		'
		'Label4
		'
		Me.Label4.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label4.Location = New System.Drawing.Point(8, 107)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(100, 20)
		Me.Label4.TabIndex = 87
		Me.Label4.Text = "Category"
		'
		'Combo_CharacteristicType
		'
		Me.Combo_CharacteristicType.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_CharacteristicType.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_CharacteristicType.Location = New System.Drawing.Point(120, 131)
		Me.Combo_CharacteristicType.Name = "Combo_CharacteristicType"
		Me.Combo_CharacteristicType.Size = New System.Drawing.Size(383, 21)
		Me.Combo_CharacteristicType.TabIndex = 90
		'
		'lblCharacteristicType
		'
		Me.lblCharacteristicType.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.lblCharacteristicType.Location = New System.Drawing.Point(8, 134)
		Me.lblCharacteristicType.Name = "lblCharacteristicType"
		Me.lblCharacteristicType.Size = New System.Drawing.Size(100, 20)
		Me.lblCharacteristicType.TabIndex = 91
		Me.lblCharacteristicType.Text = "Characteristic Type"
		'
		'edit_FixedValue
		'
		Me.edit_FixedValue.Enabled = False
		Me.edit_FixedValue.Location = New System.Drawing.Point(144, 158)
		Me.edit_FixedValue.Name = "edit_FixedValue"
		Me.edit_FixedValue.RenaissanceTag = Nothing
		Me.edit_FixedValue.Size = New System.Drawing.Size(92, 20)
		Me.edit_FixedValue.TabIndex = 92
		Me.edit_FixedValue.Text = "0.00"
		Me.edit_FixedValue.TextFormat = "#,##0.00##"
		Me.edit_FixedValue.Value = 0
		'
		'Label2
		'
		Me.Label2.Location = New System.Drawing.Point(5, 161)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(100, 20)
		Me.Label2.TabIndex = 93
		Me.Label2.Text = "Fixed Value"
		'
		'Check_FixedValue
		'
		Me.Check_FixedValue.AutoSize = True
		Me.Check_FixedValue.Location = New System.Drawing.Point(120, 161)
		Me.Check_FixedValue.Name = "Check_FixedValue"
		Me.Check_FixedValue.Size = New System.Drawing.Size(15, 14)
		Me.Check_FixedValue.TabIndex = 94
		Me.Check_FixedValue.UseVisualStyleBackColor = True
		'
		'Check_DefaultValue
		'
		Me.Check_DefaultValue.AutoSize = True
		Me.Check_DefaultValue.Location = New System.Drawing.Point(120, 187)
		Me.Check_DefaultValue.Name = "Check_DefaultValue"
		Me.Check_DefaultValue.Size = New System.Drawing.Size(15, 14)
		Me.Check_DefaultValue.TabIndex = 97
		Me.Check_DefaultValue.UseVisualStyleBackColor = True
		'
		'edit_DefaultValue
		'
		Me.edit_DefaultValue.Enabled = False
		Me.edit_DefaultValue.Location = New System.Drawing.Point(144, 184)
		Me.edit_DefaultValue.Name = "edit_DefaultValue"
		Me.edit_DefaultValue.RenaissanceTag = Nothing
		Me.edit_DefaultValue.Size = New System.Drawing.Size(92, 20)
		Me.edit_DefaultValue.TabIndex = 95
		Me.edit_DefaultValue.Text = "0.00"
		Me.edit_DefaultValue.TextFormat = "#,##0.00##"
		Me.edit_DefaultValue.Value = 0
		'
		'Label3
		'
		Me.Label3.Location = New System.Drawing.Point(5, 187)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(100, 20)
		Me.Label3.TabIndex = 96
		Me.Label3.Text = "Default Value"
		'
		'Check_BooleanValue
		'
		Me.Check_BooleanValue.AutoSize = True
		Me.Check_BooleanValue.Location = New System.Drawing.Point(120, 213)
		Me.Check_BooleanValue.Name = "Check_BooleanValue"
		Me.Check_BooleanValue.Size = New System.Drawing.Size(15, 14)
		Me.Check_BooleanValue.TabIndex = 100
		Me.Check_BooleanValue.UseVisualStyleBackColor = True
		'
		'edit_BooleanValue
		'
		Me.edit_BooleanValue.Enabled = False
		Me.edit_BooleanValue.Location = New System.Drawing.Point(144, 210)
		Me.edit_BooleanValue.Name = "edit_BooleanValue"
		Me.edit_BooleanValue.RenaissanceTag = Nothing
		Me.edit_BooleanValue.Size = New System.Drawing.Size(92, 20)
		Me.edit_BooleanValue.TabIndex = 98
		Me.edit_BooleanValue.Text = "0.00"
		Me.edit_BooleanValue.TextFormat = "#,##0.00##"
		Me.edit_BooleanValue.Value = 0
		'
		'Label5
		'
		Me.Label5.Location = New System.Drawing.Point(5, 213)
		Me.Label5.Name = "Label5"
		Me.Label5.Size = New System.Drawing.Size(100, 20)
		Me.Label5.TabIndex = 99
		Me.Label5.Text = "Boolean Value"
		'
		'CompoundGroup
		'
		Me.CompoundGroup.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
								Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.CompoundGroup.Controls.Add(Me.List_Children)
		Me.CompoundGroup.Controls.Add(Me.Button_DeleteChild)
		Me.CompoundGroup.Controls.Add(Me.Label7)
		Me.CompoundGroup.Controls.Add(Me.Button_AddChild)
		Me.CompoundGroup.Controls.Add(Me.Combo_AddCharacteristic)
		Me.CompoundGroup.Controls.Add(Me.Label6)
		Me.CompoundGroup.Enabled = False
		Me.CompoundGroup.Location = New System.Drawing.Point(7, 237)
		Me.CompoundGroup.Name = "CompoundGroup"
		Me.CompoundGroup.Size = New System.Drawing.Size(495, 175)
		Me.CompoundGroup.TabIndex = 101
		Me.CompoundGroup.TabStop = False
		Me.CompoundGroup.Text = "Compound Characteristic : Children"
		'
		'List_Children
		'
		Me.List_Children.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
								Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.List_Children.FormattingEnabled = True
		Me.List_Children.Location = New System.Drawing.Point(118, 19)
		Me.List_Children.Name = "List_Children"
		Me.List_Children.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
		Me.List_Children.Size = New System.Drawing.Size(308, 121)
		Me.List_Children.TabIndex = 94
		'
		'Button_DeleteChild
		'
		Me.Button_DeleteChild.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Button_DeleteChild.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Button_DeleteChild.Location = New System.Drawing.Point(432, 19)
		Me.Button_DeleteChild.Name = "Button_DeleteChild"
		Me.Button_DeleteChild.Size = New System.Drawing.Size(57, 20)
		Me.Button_DeleteChild.TabIndex = 93
		Me.Button_DeleteChild.Text = "Delete"
		'
		'Label7
		'
		Me.Label7.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label7.Location = New System.Drawing.Point(6, 19)
		Me.Label7.Name = "Label7"
		Me.Label7.Size = New System.Drawing.Size(106, 20)
		Me.Label7.TabIndex = 92
		Me.Label7.Text = "Children"
		'
		'Button_AddChild
		'
		Me.Button_AddChild.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Button_AddChild.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Button_AddChild.Location = New System.Drawing.Point(432, 148)
		Me.Button_AddChild.Name = "Button_AddChild"
		Me.Button_AddChild.Size = New System.Drawing.Size(57, 20)
		Me.Button_AddChild.TabIndex = 90
		Me.Button_AddChild.Text = "Add"
		'
		'Combo_AddCharacteristic
		'
		Me.Combo_AddCharacteristic.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_AddCharacteristic.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_AddCharacteristic.Location = New System.Drawing.Point(118, 149)
		Me.Combo_AddCharacteristic.Name = "Combo_AddCharacteristic"
		Me.Combo_AddCharacteristic.Size = New System.Drawing.Size(308, 21)
		Me.Combo_AddCharacteristic.TabIndex = 88
		'
		'Label6
		'
		Me.Label6.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label6.Location = New System.Drawing.Point(6, 152)
		Me.Label6.Name = "Label6"
		Me.Label6.Size = New System.Drawing.Size(106, 20)
		Me.Label6.TabIndex = 89
		Me.Label6.Text = "Characteristic"
		'
		'frmRiskCharacteristics
		'
		Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
		Me.CancelButton = Me.btnCancel
		Me.ClientSize = New System.Drawing.Size(511, 515)
		Me.Controls.Add(Me.CompoundGroup)
		Me.Controls.Add(Me.Check_BooleanValue)
		Me.Controls.Add(Me.edit_BooleanValue)
		Me.Controls.Add(Me.Label5)
		Me.Controls.Add(Me.Check_DefaultValue)
		Me.Controls.Add(Me.edit_DefaultValue)
		Me.Controls.Add(Me.Label3)
		Me.Controls.Add(Me.Check_FixedValue)
		Me.Controls.Add(Me.edit_FixedValue)
		Me.Controls.Add(Me.Label2)
		Me.Controls.Add(Me.Combo_CharacteristicType)
		Me.Controls.Add(Me.lblCharacteristicType)
		Me.Controls.Add(Me.Combo_Category)
		Me.Controls.Add(Me.Label4)
		Me.Controls.Add(Me.RootMenu)
		Me.Controls.Add(Me.btnClose)
		Me.Controls.Add(Me.GroupBox1)
		Me.Controls.Add(Me.Label1)
		Me.Controls.Add(Me.Panel1)
		Me.Controls.Add(Me.Combo_SelectCharacteristicCode)
		Me.Controls.Add(Me.btnSave)
		Me.Controls.Add(Me.editAuditID)
		Me.Controls.Add(Me.editCharacteristicName)
		Me.Controls.Add(Me.lblCharacteristicName)
		Me.Controls.Add(Me.btnDelete)
		Me.Controls.Add(Me.btnCancel)
		Me.Name = "frmRiskCharacteristics"
		Me.Text = "Add/Edit Risk Characteristic"
		Me.Panel1.ResumeLayout(False)
		Me.CompoundGroup.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

#End Region

#Region " Form Locals and Constants "


	' Form 'Parent', the Main Venice form.
	' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
	Private WithEvents _MainForm As VeniceMain

	' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
	Private FormTooltip As New ToolTip()

	' Form Menu


	' Form Constants, specific to the table being updated.

    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
	Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

	' Form Locals, initialised on 'New' defining what standard data items to use
    ''' <summary>
  ''' Variable holding the form's data table name. taken from the "ThisStandardDataset" object.
    ''' </summary>
	Private THIS_TABLENAME As String
    ''' <summary>
  ''' Variable holding the form's data adaptor name. taken from the "ThisStandardDataset" object.
    ''' </summary>
	Private THIS_ADAPTORNAME As String
    ''' <summary>
  ''' Variable holding the form's dataset name. taken from the "ThisStandardDataset" object.
    ''' </summary>
	Private THIS_DATASETNAME As String


	' The standard ChangeID for this form. e.g. tblCurrency
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
	Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

    ''' <summary>
  ''' Principal control used for selecting items on this form. 
    ''' </summary>
	Private THIS_FORM_SelectingCombo As ComboBox
    ''' <summary>
  ''' Control to select after proessing the "New" button.
    ''' </summary>
	Private THIS_FORM_NewMoveToControl As Control

	' Form Specific Order fields
    ''' <summary>
  ''' Field Name to show in the Select combo.
    ''' </summary>
	Private THIS_FORM_SelectBy As String
    ''' <summary>
  ''' Field Name to order the Select combo by.
    ''' </summary>
	Private THIS_FORM_OrderBy As String

    ''' <summary>
  ''' The field name that holds the unique identifier for each entry. e.g. FundID for the tblFund table.
    ''' </summary>
	Private THIS_FORM_ValueMember As String

	' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
	Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
	Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

	' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
	Private THIS_FORM_FormID As VeniceFormID

	' Data Structures

    ''' <summary>
    ''' My dataset
    ''' </summary>
	Private myDataset As RenaissanceDataClass.DSRiskDataCharacteristic		' Form Specific !!!!
    ''' <summary>
    ''' My table
    ''' </summary>
	Private myTable As RenaissanceDataClass.DSRiskDataCharacteristic.tblRiskDataCharacteristicDataTable
    ''' <summary>
    ''' My connection
    ''' </summary>
	Private myConnection As SqlConnection
    ''' <summary>
    ''' My adaptor
    ''' </summary>
	Private myAdaptor As SqlDataAdapter

    ''' <summary>
    ''' The this standard dataset
    ''' </summary>
	Private ThisStandardDataset As RenaissanceGlobals.StandardDataset


	' Active Element.

    ''' <summary>
    ''' The sorted rows
    ''' </summary>
	Private SortedRows() As DataRow
    ''' <summary>
    ''' The select by sorted rows
    ''' </summary>
	Private SelectBySortedRows() As DataRow
    ''' <summary>
    ''' The this data row
    ''' </summary>
	Private thisDataRow As RenaissanceDataClass.DSRiskDataCharacteristic.tblRiskDataCharacteristicRow		' Form Specific !!!!
    ''' <summary>
    ''' The this audit ID
    ''' </summary>
	Private thisAuditID As Integer
    ''' <summary>
    ''' The this position
    ''' </summary>
	Private thisPosition As Integer
    ''' <summary>
    ''' The _ is over cancel button
    ''' </summary>
	Private _IsOverCancelButton As Boolean
    ''' <summary>
    ''' The _ in use
    ''' </summary>
	Private _InUse As Boolean

	' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
	Private FormIsValid As Boolean
    ''' <summary>
    ''' The form changed
    ''' </summary>
	Private FormChanged As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
	Private _FormOpenFailed As Boolean
    ''' <summary>
    ''' The in paint
    ''' </summary>
	Private InPaint As Boolean
    ''' <summary>
    ''' The add new record
    ''' </summary>
	Private AddNewRecord As Boolean

	' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
	Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
	Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
	Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
	Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
	Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
	Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return _IsOverCancelButton
		End Get
		Set(ByVal Value As Boolean)
			_IsOverCancelButton = Value
		End Set
	End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
	Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
		Get
			Return InPaint
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
		Get
			Return _InUse
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmRiskCharacteristics"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
	Public Sub New(ByVal pMainForm As VeniceMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()

		_MainForm = pMainForm
		AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

		_FormOpenFailed = False
		_InUse = True

		' ******************************************************
		' Form Specific Settings :
		' ******************************************************

		THIS_FORM_SelectingCombo = Me.Combo_SelectCharacteristicCode
		THIS_FORM_NewMoveToControl = Me.editCharacteristicName

		' Default Select and Order fields.

		THIS_FORM_SelectBy = "DataCharacteristic"
		THIS_FORM_OrderBy = "DataCharacteristic"

		THIS_FORM_ValueMember = "DataCharacteristicId"

		' Form Permissioning :-

		THIS_FORM_PermissionArea = Me.Name
		THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

		' 'This' form ID

		THIS_FORM_FormID = VeniceFormID.frmRiskCharacteristics

		' This form's dataset type.

		ThisStandardDataset = RenaissanceStandardDatasets.tblRiskDataCharacteristic	' This Defines the Form Data !!! 

		' Format Event Handlers for form controls

		AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler
		AddHandler editCharacteristicName.LostFocus, AddressOf MainForm.Text_NotNull

		' Form Control Changed events
		AddHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
		AddHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		AddHandler editCharacteristicName.TextChanged, AddressOf Me.FormControlChanged

		AddHandler Combo_Category.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_Category.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_Category.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_Category.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_Category.SelectedValueChanged, AddressOf Me.FormControlChanged

		AddHandler Combo_CharacteristicType.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_CharacteristicType.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_CharacteristicType.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_CharacteristicType.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_CharacteristicType.SelectedValueChanged, AddressOf Me.FormControlChanged

		AddHandler Combo_AddCharacteristic.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_AddCharacteristic.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_AddCharacteristic.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_AddCharacteristic.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_AddCharacteristic.SelectedValueChanged, AddressOf Me.FormControlChanged

		AddHandler Check_FixedValue.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Check_DefaultValue.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Check_BooleanValue.CheckedChanged, AddressOf Me.FormControlChanged

		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

		' ******************************************************
		' End Form Specific.
		' ******************************************************

		' Data object names standard to this Form type.

		THIS_TABLENAME = ThisStandardDataset.TableName
		THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
		THIS_DATASETNAME = ThisStandardDataset.DatasetName

		THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

		' Establish / Retrieve data objects for this form.

		myConnection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)
		myAdaptor = MainForm.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, VENICE_CONNECTION, THIS_TABLENAME)
		myDataset = MainForm.Load_Table(ThisStandardDataset, False)
		myTable = myDataset.Tables(0)

		MainForm.BuildStandardFormMenu(Me.RootMenu, myDataset.Tables(0), AddressOf SelectMenuEvent, AddressOf OrderMenuEvent, AddressOf AuditReportMenuEvent)

		' Initialise List_GroupList
		Dim CharacteristicTable As New RenaissanceDataClass.DSRiskDataCharacteristic.tblRiskDataCharacteristicDataTable
		Me.List_Children.DataSource = CharacteristicTable
		List_Children.DisplayMember = "DataCharacteristic"
		List_Children.ValueMember = "DataCharacteristicId"

	End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

	' Form Initialisation code.
	'
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
	Public Sub ResetForm() Implements StandardVeniceForm.ResetForm
		' ********************************************************************************
		'
		'
		' ********************************************************************************

		THIS_FORM_SelectBy = "DataCharacteristic"
		THIS_FORM_OrderBy = "DataCharacteristic"

		Call Form_Load(Me, New System.EventArgs)
	End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
	Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
		' ********************************************************************************
		'
		'
		' ********************************************************************************

		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		' ********************************************************************************
		'
		'
		' ********************************************************************************

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
		_FormOpenFailed = False
		_InUse = True

		Try

			' Initialise Data structures. Connection, Adaptor and Dataset.

			If Not (MainForm Is Nothing) Then
				FormIsValid = True
			Else
				MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
				FormIsValid = False
				_FormOpenFailed = True
				Exit Sub
			End If

			If (myConnection Is Nothing) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

				FormIsValid = False
				_FormOpenFailed = True
				Exit Sub
			End If

			If (myAdaptor Is Nothing) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

				FormIsValid = False
				_FormOpenFailed = True
				Exit Sub
			End If

			If (myDataset Is Nothing) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

				FormIsValid = False
				_FormOpenFailed = True
				Exit Sub
			End If

		Catch ex As Exception
		End Try

		Try

			' Initialse form

			InPaint = True
			IsOverCancelButton = False

			' Check User permissions
			Call CheckPermissions()
			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

				FormIsValid = False
				_FormOpenFailed = True
				Exit Sub
			End If

			' Build Sorted data list from which this form operates

			Call SetSortedRows()

			Call SetCategoryCombo()
			Call SetCharacteristicCombo()
			Call SetCharacteristicTypeCombo()

			' Display initial record.

			thisPosition = 0
			If THIS_FORM_SelectingCombo.Items.Count > 0 Then
				Me.THIS_FORM_SelectingCombo.SelectedIndex = 0
				thisDataRow = SortedRows(thisPosition)
				Call GetFormData(thisDataRow)
			Else
				Me.THIS_FORM_SelectingCombo.SelectedIndex = (-1)
				Call GetFormData(Nothing)
			End If

		Catch ex As Exception

			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Initialising Form (Form.Load).", ex.StackTrace, True)

		Finally

			InPaint = False

		End Try

	End Sub

    ''' <summary>
    ''' Handles the Closing event of the frmRiskCharacteristics control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
	Private Sub frmRiskCharacteristics_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		' ********************************************************************************
		'
		'
		' ********************************************************************************

		Dim HideForm As Boolean

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...

		_InUse = False

		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else
			If (FormChanged = True) Then
				Call SetFormData()
			End If

			HideForm = True
			If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			e.Cancel = True
		Else
			Try
				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler
				RemoveHandler editCharacteristicName.LostFocus, AddressOf MainForm.Text_NotNull

				' Form Control Changed events
				RemoveHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
				RemoveHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				RemoveHandler editCharacteristicName.TextChanged, AddressOf Me.FormControlChanged

				RemoveHandler Combo_Category.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_Category.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_Category.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_Category.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_Category.SelectedValueChanged, AddressOf Me.FormControlChanged

				RemoveHandler Combo_CharacteristicType.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_CharacteristicType.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_CharacteristicType.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_CharacteristicType.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_CharacteristicType.SelectedValueChanged, AddressOf Me.FormControlChanged

				RemoveHandler Combo_AddCharacteristic.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_AddCharacteristic.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_AddCharacteristic.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_AddCharacteristic.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_AddCharacteristic.SelectedValueChanged, AddressOf Me.FormControlChanged

				RemoveHandler Check_FixedValue.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_DefaultValue.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_BooleanValue.CheckedChanged, AddressOf Me.FormControlChanged

			Catch ex As Exception
			End Try
		End If

	End Sub


#End Region

	' Routine to handle changes / updates to tables by this and other windows.
	' If this, or any other, form posts a change to a table, then it will invoke an update event 
	' detailing what tables have been altered.
	' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
	' the 'VeniceAutoUpdate' event of the main Venice form.
	' Each form may them react as appropriate to changes in any table that might impact it.
	'
    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		Dim OrgInPaint As Boolean
		Dim KnowledgeDateChanged As Boolean
		Dim SetButtonStatus_Flag As Boolean
		Dim RefreshForm As Boolean = False

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		OrgInPaint = InPaint
		InPaint = True
		KnowledgeDateChanged = False
		SetButtonStatus_Flag = False

		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
			KnowledgeDateChanged = True
			RefreshForm = True
		End If

		' ****************************************************************
		' Check for changes relevant to this form
		' ****************************************************************

		' Changes to the tblRiskDataCategory table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblRiskDataCategory) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			Call SetCategoryCombo()
		End If

		' Changes to the tblRiskDataCharacteristic table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblRiskDataCharacteristic) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			Call SetCharacteristicCombo()

		End If

		' Changes to the KnowledgeDate :-
		If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
			SetButtonStatus_Flag = True
		End If

		' Changes to the tblUserPermissions table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

			' Check ongoing permissions.

			Call CheckPermissions()
			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

				FormIsValid = False
				Me.Close()
				Exit Sub
			End If

			SetButtonStatus_Flag = True

		End If


		' ****************************************************************
		' Changes to the Main FORM table :-
		' ****************************************************************

		If (e.TableChanged(THIS_FORM_ChangeID) = True) Or KnowledgeDateChanged Then
			RefreshForm = True

			' Re-Set Controls etc.
			Call SetSortedRows()

			' Move again to the correct item
			thisPosition = Get_Position(thisAuditID)

			' Validate current position.
			If thisPosition >= 0 And thisPosition < Me.SortedRows.GetLength(0) Then
				thisDataRow = Me.SortedRows(thisPosition)
			Else
				If (Me.SortedRows.GetLength(0) > 0) And (AddNewRecord = False) Then
					thisPosition = 0
					thisDataRow = Me.SortedRows(thisPosition)
					FormChanged = False
				Else
					thisDataRow = Nothing
				End If
			End If

			' Set SelectingCombo Index.
			If (Me.THIS_FORM_SelectingCombo.Items.Count <= 0) Then
				Try
					Me.THIS_FORM_SelectingCombo.SelectedIndex = -1
				Catch ex As Exception
				End Try
			ElseIf (thisPosition < 0) Then
				Try
					MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
				Catch ex As Exception
				End Try
			Else
				Try
					Me.THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
				Catch ex As Exception
				End Try
			End If

		End If

		' ****************************************************************
		' Repaint if not currently in Edit Mode
		'
		' ****************************************************************

		InPaint = OrgInPaint

		' Retrieve form data if appropriate, i.e. this data item is not already being edited.
		If (RefreshForm = True) AndAlso (FormChanged = False) AndAlso (AddNewRecord = False) Then
			GetFormData(thisDataRow) ' Includes a call to 'SetButtonStatus()'
		Else
			If SetButtonStatus_Flag Then
				Call SetButtonStatus()
			End If
		End If

	End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

    ''' <summary>
    ''' Handles the SelectedValueChanged event of the Combo_CharacteristicType control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_CharacteristicType_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Combo_CharacteristicType.SelectedValueChanged
		' ****************************************************************
		'
		'
		' ****************************************************************

		Try
			If (Combo_CharacteristicType.Enabled = True) Then
				Me.Check_BooleanValue.Enabled = False

				If (Me.Combo_CharacteristicType.SelectedValue = RenaissanceGlobals.RiskCharacteristicType.Normal) Then
					Me.CompoundGroup.Enabled = False
				Else
					Me.CompoundGroup.Enabled = True

					If (Me.Combo_CharacteristicType.SelectedValue = RenaissanceGlobals.RiskCharacteristicType.Compound_Boolean) Then
						Me.Check_BooleanValue.Enabled = True
					End If
				End If
			End If

		Catch ex As Exception
		End Try

	End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_CharacteristicType control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_CharacteristicType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_CharacteristicType.SelectedIndexChanged
		' ****************************************************************
		'
		'
		' ****************************************************************

	End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the Check_FixedValue control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Check_FixedValue_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Check_FixedValue.CheckedChanged
		' ****************************************************************
		'
		'
		' ****************************************************************

		Try
			If Me.Check_FixedValue.Enabled Then
				Me.edit_FixedValue.Enabled = Me.Check_FixedValue.Checked
			End If

		Catch ex As Exception
		End Try

	End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the Check_DefaultValue control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Check_DefaultValue_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_DefaultValue.CheckedChanged
		' ****************************************************************
		'
		'
		' ****************************************************************

		Try
			If Me.Check_DefaultValue.Enabled Then
				Me.edit_DefaultValue.Enabled = Me.Check_DefaultValue.Checked
			End If

		Catch ex As Exception
		End Try

	End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the Check_BooleanValue control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Check_BooleanValue_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_BooleanValue.CheckedChanged
		' ****************************************************************
		'
		'
		' ****************************************************************

		Try
			If Me.Check_BooleanValue.Enabled Then
				Me.edit_BooleanValue.Enabled = Me.Check_BooleanValue.Checked
			End If

		Catch ex As Exception
		End Try

	End Sub

    ''' <summary>
    ''' Handles the Click event of the Button_DeleteChild control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Button_DeleteChild_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_DeleteChild.Click
		' ***********************************************************************************
		' Remove Selected Items from the Characteristics List
		'
		' A temporary array of Selected Rows must be created because of the way collections work.
		' ***********************************************************************************

		Try
			If Me.List_Children.SelectedItems.Count > 0 Then
				Dim ListTable As RenaissanceDataClass.DSRiskDataCharacteristic.tblRiskDataCharacteristicDataTable
				Dim SelectedRow As DataRowView
				Dim SelectedRows(List_Children.SelectedItems.Count - 1) As DataRow
				Dim RowCount As Integer = 0

				ListTable = CType(List_Children.DataSource, RenaissanceDataClass.DSRiskDataCharacteristic.tblRiskDataCharacteristicDataTable)

				For Each SelectedRow In List_Children.SelectedItems
					SelectedRows(RowCount) = (SelectedRow.Row)
					RowCount += 1
				Next

				For RowCount = 0 To (SelectedRows.Length - 1)
					ListTable.Rows.Remove(SelectedRows(RowCount))
				Next

				List_Children.SelectedIndex = (-1)

				Call FormControlChanged(Nothing, Nothing)

			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error removing Item(s) from Group Items.", ex.StackTrace, True)
		End Try

	End Sub

    ''' <summary>
    ''' Handles the Click event of the Button_AddChild control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Button_AddChild_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_AddChild.Click
		' ****************************************************************
		'
		'
		' ****************************************************************

		' ***********************************************************************************
		' Add Selected Item  to the List_Children List
		'
		' Do not Duplicate Items.
		' ***********************************************************************************

		Try
			If Me.Combo_AddCharacteristic.SelectedValue > 0 Then
				Me.FormChanged = True

				Dim ListTable As RenaissanceDataClass.DSRiskDataCharacteristic.tblRiskDataCharacteristicDataTable

				' Get pointer to the List_Children List Table

				ListTable = CType(Me.List_Children.DataSource, RenaissanceDataClass.DSRiskDataCharacteristic.tblRiskDataCharacteristicDataTable)

				' Add selected Characteristic (if it does not already exist) to the List_Children List

				If GroupContainsID(CInt(Combo_AddCharacteristic.SelectedValue)) = False Then

					Dim CharacteristicRow As RenaissanceDataClass.DSRiskDataCharacteristic.tblRiskDataCharacteristicRow
					Dim ThisRow As RenaissanceDataClass.DSRiskDataCharacteristic.tblRiskDataCharacteristicRow

					CharacteristicRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblRiskDataCharacteristic, CInt(Combo_AddCharacteristic.SelectedValue))

					' Construct a new Item Row.

					ThisRow = ListTable.NewRow
					ThisRow.DataCategoryId = CharacteristicRow.DataCategoryId
					ThisRow.DataCharacteristicId = CharacteristicRow.DataCharacteristicId
					ThisRow.DataCharacteristic = CharacteristicRow.DataCharacteristic

					ListTable.Rows.Add(ThisRow)

					Call FormControlChanged(Nothing, Nothing)
				End If

				List_Children.SelectedIndex = (-1)

				SetButtonStatus()
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Adding to Group Items List.", ex.StackTrace, True)
		End Try

	End Sub

    ''' <summary>
    ''' Groups the contains ID.
    ''' </summary>
    ''' <param name="pCharacteristicId">The p characteristic id.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	Private Function GroupContainsID(ByVal pCharacteristicId As Integer) As Boolean
		' ***********************************************************************************
		' Function to check if the List_Children List contains a given Characteristic.
		'
		'
		' ***********************************************************************************

		Dim thisRow As RenaissanceDataClass.DSRiskDataCharacteristic.tblRiskDataCharacteristicRow
		Dim ListTable As New RenaissanceDataClass.DSRiskDataCharacteristic.tblRiskDataCharacteristicDataTable

		Try
			If (List_Children.DataSource Is Nothing) Then
				Return False
			End If

			ListTable = CType(List_Children.DataSource, RenaissanceDataClass.DSRiskDataCharacteristic.tblRiskDataCharacteristicDataTable)

			For Each thisRow In ListTable
				If thisRow.DataCharacteristicId = pCharacteristicId Then
					Return True
				End If
			Next

		Catch ex As Exception

		End Try

		Return False
	End Function

	' Build Sorted list from the Source dataset and update the Select Combo

    ''' <summary>
    ''' Sets the sorted rows.
    ''' </summary>
	Private Sub SetSortedRows()

		Dim OrgInPaint As Boolean

		Dim thisrow As DataRow
		Dim thisDrowDownWidth As Integer
		Dim SizingBitmap As Bitmap
		Dim SizingGraphics As Graphics

		' Form Specific Selection Combo :-
		If (THIS_FORM_SelectingCombo Is Nothing) Then Exit Sub

		' Code is pretty Generic form here on...

		' Set paint local so that changes to the selection combo do not trigger form updates.

		OrgInPaint = InPaint
		InPaint = True

		' Get selected Row sets, or exit if no data is present.
		If myDataset Is Nothing Then
			ReDim SortedRows(0)
			ReDim SelectBySortedRows(0)
		Else
			SortedRows = myTable.Select("RN >= 0", THIS_FORM_OrderBy)
			SelectBySortedRows = myTable.Select("RN >= 0", THIS_FORM_SelectBy)
		End If

		' Set Combo data source
		THIS_FORM_SelectingCombo.DataSource = SortedRows
		THIS_FORM_SelectingCombo.DisplayMember = THIS_FORM_SelectBy
		THIS_FORM_SelectingCombo.ValueMember = THIS_FORM_ValueMember

		' Ensure that the Selection Combo's DrowDownWidth is wide enough to take the required strings

		thisDrowDownWidth = THIS_FORM_SelectingCombo.Width
		For Each thisrow In SortedRows

			' Compute the string dimensions in the given font
			SizingBitmap = New Bitmap(1, 1, System.Drawing.Imaging.PixelFormat.Format32bppArgb)
			SizingGraphics = Graphics.FromImage(SizingBitmap)
			Dim stringSize As SizeF = SizingGraphics.MeasureString(thisrow(THIS_FORM_SelectBy).ToString, THIS_FORM_SelectingCombo.Font)
			If (stringSize.Width) > thisDrowDownWidth Then
				thisDrowDownWidth = CInt(stringSize.Width)
			End If
		Next

		THIS_FORM_SelectingCombo.DropDownWidth = thisDrowDownWidth

		InPaint = OrgInPaint
	End Sub


	' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
	Private Sub CheckPermissions()

		Dim Permissions As Integer

		Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

		HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
		HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
		HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
		HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

	End Sub

	' Flag changes to form controls, Event Associations made in 'New' routine.
    ''' <summary>
  ''' Basic event handler, called when a form value is changed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

		If InPaint = False Then
			If (Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord) Then
				FormChanged = True
				Me.btnSave.Enabled = True
				Me.btnCancel.Enabled = True
			End If
		End If

	End Sub

    ''' <summary>
    ''' Selects the menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Public Sub SelectMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Select Menu
		' Event association is made during the dynamic menu creation

		THIS_FORM_SelectBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

		Call SetSortedRows()

	End Sub

    ''' <summary>
    ''' Orders the menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Public Sub OrderMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Order Menu
		' Event association is made during the dynamic menu creation

		THIS_FORM_OrderBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

		Call SetSortedRows()

	End Sub


    ''' <summary>
    ''' Audits the report menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Public Sub AuditReportMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Audit Report Menu
		' Event association is made during the dynamic menu creation

		Try
			Select Case CInt(CType(sender, System.Windows.Forms.ToolStripMenuItem).Tag)
				Case 0 ' This Record
					If (Me.AddNewRecord = False) AndAlso (thisAuditID >= 0) Then
						Run_AuditReport_ThisRecord(MainForm, ThisStandardDataset, thisAuditID, MainForm.VeniceProgressBar)
					End If

				Case 1 ' All Records
					Run_AuditReport_ALLRecords(MainForm, ThisStandardDataset, MainForm.VeniceProgressBar)

			End Select
		Catch ex As Exception
			Call MainForm.LogError(Me.Name & ", AuditReportMenuEvent()", 0, ex.Message, "Error calling Audit report.", ex.StackTrace, True)
		End Try

	End Sub


#End Region

#Region " Set Form Combos (Form Specific Code) "

    ''' <summary>
    ''' Sets the category combo.
    ''' </summary>
	Private Sub SetCategoryCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_Category, _
		RenaissanceStandardDatasets.tblRiskDataCategory, _
		"DataCategory", _
		"DataCategoryId", _
		"", False, True, False)	' 

	End Sub

    ''' <summary>
    ''' Sets the characteristic combo.
    ''' </summary>
	Private Sub SetCharacteristicCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_AddCharacteristic, _
		RenaissanceStandardDatasets.tblRiskDataCharacteristic, _
		"DataCharacteristic", _
		"DataCharacteristicId", _
		"", False, True, True, 0)	' 

	End Sub

    ''' <summary>
    ''' Sets the characteristic type combo.
    ''' </summary>
	Private Sub SetCharacteristicTypeCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_CharacteristicType, _
		GetType(RenaissanceGlobals.RiskCharacteristicType), False)	' 

	End Sub



#End Region

#Region " Get & Set Form Data / SetButton / ValidateForm / btnCancel Events (Form Specific Code) "

    ''' <summary>
    ''' Gets the form data.
    ''' </summary>
    ''' <param name="ThisRow">The this row.</param>
	Private Sub GetFormData(ByRef ThisRow As RenaissanceDataClass.DSRiskDataCharacteristic.tblRiskDataCharacteristicRow)
		' Routine to populate form Controls given a data row.
		' Form is cleared for invalid Datarows or Invalid Form.

		Dim OrgInpaint As Boolean

		' Set 'Paint' flag to prevent event actions caused by changing the value of form fields.
		OrgInpaint = InPaint
		InPaint = True


		If (ThisRow Is Nothing) Or (FormIsValid = False) Or (Me.InUse = False) Then
			' Bad / New Datarow - Clear Form.

			thisAuditID = (-1)

			Me.editAuditID.Text = ""

			Me.editCharacteristicName.Text = ""
			MainForm.ClearComboSelection(Me.Combo_Category)
			Me.Combo_CharacteristicType.SelectedValue = RenaissanceGlobals.RiskCharacteristicType.Normal
			Me.Check_FixedValue.Checked = False
			Me.edit_FixedValue.Value = 0.0#
			Me.Check_DefaultValue.Checked = False
			Me.edit_DefaultValue.Value = 0.0#
			Me.Check_BooleanValue.Checked = False
			Me.edit_BooleanValue.Value = 0.0#

			CType(List_Children.DataSource, RenaissanceDataClass.DSRiskDataCharacteristic.tblRiskDataCharacteristicDataTable).Rows.Clear()

			Me.Combo_AddCharacteristic.SelectedValue = 0

			If AddNewRecord = True Then
				Me.btnCancel.Enabled = True
				Me.THIS_FORM_SelectingCombo.Enabled = False
			Else
				Me.btnCancel.Enabled = False
				Me.THIS_FORM_SelectingCombo.Enabled = True
			End If

		Else

			' Populate Form with given data.
			Try
				thisAuditID = thisDataRow.AuditID

				Me.editAuditID.Text = thisDataRow.AuditID.ToString

				Me.editCharacteristicName.Text = thisDataRow.DataCharacteristic
				Me.Combo_Category.SelectedValue = thisDataRow.DataCategoryId
				Me.Combo_CharacteristicType.SelectedValue = thisDataRow.DataCharacteristicTypeID

				If (thisDataRow.IsFixedValueNull) Then
					Me.Check_FixedValue.Checked = False
					Me.edit_FixedValue.Value = 0.0#
				Else
					Me.Check_FixedValue.Checked = True
					Me.edit_FixedValue.Value = thisDataRow.FixedValue
				End If

				If (thisDataRow.IsDefaultValueNull) Then
					Me.Check_DefaultValue.Checked = False
					Me.edit_DefaultValue.Value = 0.0#
				Else
					Me.Check_DefaultValue.Checked = True
					Me.edit_DefaultValue.Value = thisDataRow.DefaultValue
				End If

				If (thisDataRow.IsBooleanValueNull) Then
					Me.Check_BooleanValue.Checked = False
					Me.edit_BooleanValue.Value = 0.0#
				Else
					Me.Check_BooleanValue.Checked = True
					Me.edit_BooleanValue.Value = thisDataRow.BooleanValue
				End If

				' Set Child Characteristics List

				If (thisDataRow.DataCharacteristicTypeID = RenaissanceGlobals.RiskCharacteristicType.Normal) Then
					CType(List_Children.DataSource, RenaissanceDataClass.DSRiskDataCharacteristic.tblRiskDataCharacteristicDataTable).Rows.Clear()
				Else

					' Populate Form with given data.
					Dim tmpCommand As New SqlCommand
					Dim ListTable As RenaissanceDataClass.DSRiskDataCharacteristic.tblRiskDataCharacteristicDataTable

					Try

						tmpCommand.CommandType = CommandType.StoredProcedure
						tmpCommand.CommandText = "adp_tblRiskDataCompoundCharacteristics_SelectGroup"
						tmpCommand.Connection = MainForm.GetVeniceConnection
						tmpCommand.Parameters.Add("@CharacteristicID", SqlDbType.Int).Value = thisDataRow.DataCharacteristicId
						tmpCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime).Value = MainForm.Main_Knowledgedate
						tmpCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

						List_Children.SuspendLayout()
						ListTable = CType(List_Children.DataSource, RenaissanceDataClass.DSRiskDataCharacteristic.tblRiskDataCharacteristicDataTable)
						ListTable.Rows.Clear()
            MainForm.LoadTable_Custom(ListTable, tmpCommand)
            'ListTable.Load(tmpCommand.ExecuteReader)
						List_Children.ResumeLayout()

						' MainForm.SetComboSelectionLengths(Me, THIS_FORM_SelectingCombo)

						Me.btnCancel.Enabled = False
						Me.THIS_FORM_SelectingCombo.Enabled = True

					Catch ex As Exception

						Call MainForm.LogError(Me.Name & ", GetFormData", 0, ex.Message, "Error Showing Data", ex.StackTrace, True)
						Call GetFormData(Nothing)

					Finally
						Try
							If (tmpCommand IsNot Nothing) AndAlso (tmpCommand.Connection IsNot Nothing) Then
								tmpCommand.Connection.Close()
								tmpCommand.Connection = Nothing
							End If
						Catch ex As Exception
						End Try
					End Try

				End If

				AddNewRecord = False
				' MainForm.SetComboSelectionLengths(Me)

				Me.btnCancel.Enabled = False
				Me.THIS_FORM_SelectingCombo.Enabled = True

			Catch ex As Exception

				Call MainForm.LogError(Me.Name & ", GetFormData", 0, ex.Message, "Error Showing Data", ex.StackTrace, True)
				Call GetFormData(Nothing)

			End Try

		End If

		' Allow Field events to trigger before 'InPaint' Is re-set. 
		' (Should) Prevent Validation errors during Form Draw.
		Application.DoEvents()

		' Restore 'Paint' flag.
		InPaint = OrgInpaint
		FormChanged = False
		Me.btnSave.Enabled = False

		' As it says on the can....
		Call SetButtonStatus()

	End Sub

    ''' <summary>
    ''' Sets the form data.
    ''' </summary>
    ''' <param name="pConfirm">if set to <c>true</c> [p confirm].</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	Private Function SetFormData(Optional ByVal pConfirm As Boolean = True) As Boolean
		' *************************************************************
		'
		' *************************************************************
		Dim ErrMessage As String
		Dim ErrFlag As Boolean
		Dim ErrStack As String
		Dim CompoundItemsUpdated As Boolean = False

		ErrMessage = ""
		ErrStack = ""

		' *************************************************************
		' Appropriate Save permission :-
		' *************************************************************

		If (Me.HasUpdatePermission = False) And ((Me.HasInsertPermission = False) Or (Me.AddNewRecord = False)) Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "You do not have permission to save this record.", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' *************************************************************
		' If Save button is disabled then should not be able to save, exit silently.
		' *************************************************************
		If Me.btnSave.Enabled = False Then
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' *************************************************************
		' KnowledgeDate OK :-
		' *************************************************************

		If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Changes to Venice are not allowed when the Knowledgedate is not set to `NOW`", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' Confirm Save, if required.

		If (pConfirm = True) Then
			If MessageBox.Show("Save Changes ?", "Save Changes ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
				Return True
				Exit Function
			End If
		End If

		' *************************************************************
		' Procedure to Save the current form information.
		' *************************************************************

		Dim StatusString As String
		Dim LogString As String
		Dim UpdateRows(0) As DataRow
		Dim Position As Integer
		Dim CharacteristicType As RenaissanceGlobals.RiskCharacteristicType = RiskCharacteristicType.Normal

		If (FormChanged = False) Or (FormIsValid = False) Then
			Return False
			Exit Function
		End If

		' Validation
		StatusString = ""
		If ValidateForm(StatusString) = False Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Validation Error." & vbCrLf & StatusString, "", True)
			Return False
			Exit Function
		End If

		' Check current position in the table.
		Position = Get_Position(thisAuditID)

		' Allow for new or missing ID.
		If Position < 0 Then
			If (AddNewRecord = True) OrElse (SortedRows.Length <= 0) Then
				thisDataRow = myTable.NewRow
				LogString = "Add: "
				AddNewRecord = True
			Else
				Return False
				Exit Function
			End If
		Else
			' Row found OK.
			LogString = "Edit : AuditID = " & thisAuditID.ToString

			thisDataRow = Me.SortedRows(Position)

		End If

		' Set 'Paint' flag.
		InPaint = True

		' *************************************************************
		' Lock the Data Table, to prevent update conflicts.
		' *************************************************************
		SyncLock myTable

			' Initiate Edit,
			thisDataRow.BeginEdit()

			thisDataRow.DataCharacteristic = editCharacteristicName.Text

			If (Combo_Category.SelectedIndex >= 0) Then
				thisDataRow.DataCategoryId = CInt(Me.Combo_Category.SelectedValue)
			Else
				Try
					If (thisDataRow.RowState <> System.Data.DataRowState.Detached) Then
						thisDataRow.RejectChanges()
					End If
				Catch ex As Exception
				End Try

				Me.MainForm.LogError(Me.Name & ", SetFormData", LOG_LEVELS.Warning, "Add/Edit Record", "Category must be set.", "", True)
				Return False
				Exit Function
			End If

			If (Combo_CharacteristicType.SelectedIndex >= 0) Then
				thisDataRow.DataCharacteristicTypeID = CInt(Me.Combo_CharacteristicType.SelectedValue)
				CharacteristicType = CType(thisDataRow.DataCharacteristicTypeID, RenaissanceGlobals.RiskCharacteristicType)
			Else
				thisDataRow.RejectChanges()
				Me.MainForm.LogError(Me.Name & ", SetFormData", LOG_LEVELS.Warning, "Add/Edit Record", "Characteristic Type must be set.", "", True)
				Return False
				Exit Function
			End If

			If (Check_FixedValue.Checked) Then
				thisDataRow.FixedValue = edit_FixedValue.Value
			Else
				thisDataRow.SetFixedValueNull()
			End If

			If (Check_DefaultValue.Checked) Then
				thisDataRow.DefaultValue = edit_DefaultValue.Value
			Else
				thisDataRow.SetDefaultValueNull()
			End If

			If (Check_BooleanValue.Checked) AndAlso (thisDataRow.DataCharacteristicTypeID = RenaissanceGlobals.RiskCharacteristicType.Compound_Boolean) Then
				thisDataRow.BooleanValue = edit_BooleanValue.Value
			Else
				thisDataRow.SetBooleanValueNull()
			End If

			thisDataRow.EndEdit()
			InPaint = False

			' Add and Update DataRow. 

			ErrFlag = False

			If AddNewRecord = True Then
				Try
					myTable.Rows.Add(thisDataRow)
				Catch ex As Exception
					ErrFlag = True
					ErrMessage = ex.Message
					ErrStack = ex.StackTrace
				End Try
			End If

			UpdateRows(0) = thisDataRow

			' Post Additions / Updates to the underlying table :-
			Dim temp As Integer

			Try
				If (ErrFlag = False) Then
					myAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
					myAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

					temp = MainForm.AdaptorUpdate(Me.Name, myAdaptor, UpdateRows)
				End If

			Catch ex As Exception

				ErrMessage = ex.Message
				ErrFlag = True
				ErrStack = ex.StackTrace

			End Try

			' Save Compound Characteristics if necessary...

			If (CharacteristicType <> RiskCharacteristicType.Normal) Then

				Dim ListTable As RenaissanceDataClass.DSRiskDataCharacteristic.tblRiskDataCharacteristicDataTable
				ListTable = CType(List_Children.DataSource, RenaissanceDataClass.DSRiskDataCharacteristic.tblRiskDataCharacteristicDataTable)
				Dim tblCharacteristicRow As RenaissanceDataClass.DSRiskDataCharacteristic.tblRiskDataCharacteristicRow

				Dim DSCompoundItems As RenaissanceDataClass.DSRiskDataCompoundCharacteristic
				Dim tblCompoundItems As RenaissanceDataClass.DSRiskDataCompoundCharacteristic.tblRiskDataCompoundCharacteristicDataTable
				Dim SelectedRows() As RenaissanceDataClass.DSRiskDataCompoundCharacteristic.tblRiskDataCompoundCharacteristicRow
				Dim tblCompoundRow As RenaissanceDataClass.DSRiskDataCompoundCharacteristic.tblRiskDataCompoundCharacteristicRow
				Dim ItemCount As Integer
				Dim ParentCharacteristicID As Integer = thisDataRow.DataCharacteristicId

				DSCompoundItems = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblRiskDataCompoundCharacteristic, False), RenaissanceDataClass.DSRiskDataCompoundCharacteristic)
				tblCompoundItems = DSCompoundItems.tblRiskDataCompoundCharacteristic

				SelectedRows = tblCompoundItems.Select("ParentCharacteristicID=" & ParentCharacteristicID.ToString, "ChildCharacteristicID")

				SyncLock tblCompoundItems

					Try

						' Delete any rows that are no longer in the Group

						If (SelectedRows IsNot Nothing) AndAlso (SelectedRows.Length > 0) Then
							For ItemCount = 0 To (SelectedRows.Length - 1)
								tblCompoundRow = SelectedRows(ItemCount)

								If (GroupContainsID(tblCompoundRow.ChildCharacteristicID) = False) Then
									tblCompoundRow.Delete()
									CompoundItemsUpdated = True
								End If
							Next
						End If

						' Add / Update Group Items to the table

						Dim FoundFlag As Boolean = False

						For Each tblCharacteristicRow In ListTable.Rows
							FoundFlag = False
							tblCompoundRow = Nothing

							If (SelectedRows.Length > 0) Then
								For ItemCount = 0 To (SelectedRows.Length - 1)
									tblCompoundRow = SelectedRows(ItemCount)

									If tblCompoundRow.RowState <> DataRowState.Deleted Then
										If (tblCompoundRow.ChildCharacteristicID = tblCharacteristicRow.DataCharacteristicId) Then
											FoundFlag = True

											Exit For
										End If
									End If
								Next
							End If

							If (FoundFlag = False) Then

								' Add New Row

								tblCompoundRow = tblCompoundItems.NewRow

								tblCompoundRow.CompoundID = 0
								tblCompoundRow.ParentCharacteristicID = ParentCharacteristicID
								tblCompoundRow.ChildCharacteristicID = tblCharacteristicRow.DataCharacteristicId

								tblCompoundItems.Rows.Add(tblCompoundRow)
								CompoundItemsUpdated = True

							End If
						Next

						' Post Additions / Updates to the underlying table :-

						If (ErrFlag = False) Then
							temp = MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblRiskDataCompoundCharacteristic, tblCompoundItems.ToArray())
						End If

					Catch ex As Exception

						ErrMessage = ex.Message
						ErrFlag = True
						ErrStack = ex.StackTrace

					End Try

				End SyncLock

			End If

			thisAuditID = thisDataRow.AuditID

		End SyncLock

		' *************************************************************
		' Error report. Not done in the Try-Catch as that would serve to persist the SyncLock.
		' *************************************************************

		If (ErrFlag = True) Then
			Call MainForm.LogError(Me.Name & ", SetFormData", 0, ErrMessage, "Error Saving Data", ErrStack, True)
		End If

		' Finish off

		AddNewRecord = False
		FormChanged = False

		Me.THIS_FORM_SelectingCombo.Enabled = True

		' Propogate changes

		Dim UpdateEventArg As RenaissanceGlobals.RenaissanceUpdateEventArgs

		UpdateEventArg = New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID)

		If (CompoundItemsUpdated) Then
			UpdateEventArg.TableChanged(RenaissanceChangeID.tblRiskDataCompoundCharacteristic) = True
		End If

		Call MainForm.Main_RaiseEvent_Background(UpdateEventArg, True)

	End Function

    ''' <summary>
    ''' Sets the button status.
    ''' </summary>
	Private Sub SetButtonStatus()
		' Sets the status of the form controlls appropriate to the current users 
		'permissions and the 'Changed' or 'New' status of the form.

		' No Read permission :-

		If Me.HasReadPermission = False Then
			MainForm.LogError(Me.Name & ", SetButtonStatus()", 0, "", "You do not have Read permission for this Form", "", True)
			Me.Close()
			Exit Sub
		End If

		' Has Insert Permission.
		If Me.HasInsertPermission Then
			Me.btnAdd.Enabled = True
		Else
			Me.btnAdd.Enabled = False
		End If

		' Has Delete permission. 
		If (Me.HasDeletePermission) And (Me.AddNewRecord = False) And (thisPosition >= 0) Then
			Me.btnDelete.Enabled = True
		Else
			Me.btnDelete.Enabled = False
		End If

		If (Me.MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And _
		 ((Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord)) Then

			Me.editCharacteristicName.Enabled = True
			Me.Combo_Category.Enabled = True
			Me.Combo_CharacteristicType.Enabled = True
			Me.Check_FixedValue.Enabled = True
			Me.edit_FixedValue.Enabled = Me.Check_FixedValue.Checked
			Me.Check_DefaultValue.Enabled = True
			Me.edit_DefaultValue.Enabled = Me.Check_DefaultValue.Checked
			Me.Check_BooleanValue.Enabled = True
			Me.edit_BooleanValue.Enabled = Me.Check_BooleanValue.Checked

			Call Combo_CharacteristicType_SelectedValueChanged(Nothing, Nothing)

		Else

			Me.editCharacteristicName.Enabled = False
			Me.Combo_Category.Enabled = False
			Me.Combo_CharacteristicType.Enabled = False
			Me.Check_FixedValue.Enabled = False
			Me.edit_FixedValue.Enabled = False
			Me.Check_DefaultValue.Enabled = False
			Me.edit_DefaultValue.Enabled = False
			Me.Check_BooleanValue.Enabled = False
			Me.edit_BooleanValue.Enabled = False
			Me.CompoundGroup.Enabled = False

		End If

	End Sub

    ''' <summary>
    ''' Validates the form.
    ''' </summary>
    ''' <param name="pReturnString">The p return string.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	Private Function ValidateForm(ByRef pReturnString As String) As Boolean
		' Form Validation code.
		' 
		' This code should be the final arbiter of what is allowed. no assumptions regarding 
		' prior validation should be made.
		' 
		' This Code is called by the SetFormData routine before position changes.
		'
		Dim RVal As Boolean

		RVal = True
		pReturnString = ""

		' Validate 

		If Me.editCharacteristicName.Text.Length <= 0 Then
			pReturnString = "Characteristic Name must not be left blank."
			RVal = False
		End If

		Return RVal

	End Function

    ''' <summary>
    ''' Handles the MouseEnter event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnCancel_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseEnter
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = True
	End Sub

    ''' <summary>
    ''' Handles the MouseLeave event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnCancel_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseLeave
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = False
	End Sub


#End Region

#Region " Navigation Code / GetPosition() (Generic Code) "

    ''' <summary>
    ''' Handles the SelectComboChanged event of the Combo control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_SelectComboChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
		' Selection Combo. SelectedItem changed.
		'

		' Don't react to changes made in paint routines etc.
		If InPaint = True Then Exit Sub

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		' Find the correct data row, then show it...
		thisPosition = THIS_FORM_SelectingCombo.SelectedIndex

		If (thisPosition >= 0) Then
			thisDataRow = Me.SortedRows(thisPosition)
		Else
			thisDataRow = Nothing
		End If

		Call GetFormData(thisDataRow)

	End Sub


    ''' <summary>
    ''' Handles the Click event of the btnNavPrev control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnNavPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavPrev.Click
		' 'Previous' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		If thisPosition > 0 Then thisPosition -= 1
		If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
			thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
		End If

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

		Exit Sub

		If myTable.Rows.Count > thisPosition Then
			thisDataRow = myTable.Rows(thisPosition)
			Call GetFormData(thisDataRow)
		Else
			Call GetFormData(Nothing)
		End If

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnNavNext control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnNavNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavNext.Click
		' 'Next' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		If thisPosition < (myTable.Rows.Count - 1) Then thisPosition += 1
		If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
			thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
		End If

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

		Exit Sub

		If myTable.Rows.Count > thisPosition Then
			thisDataRow = myTable.Rows(thisPosition)
			Call GetFormData(thisDataRow)
		Else
			Call GetFormData(Nothing)
		End If

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnNavFirst control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnNavFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavFirst.Click
		' 'First' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		thisPosition = 0
		If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
			thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
		End If

		If (THIS_FORM_SelectingCombo.SelectedIndex = thisPosition) Then
			Combo_SelectComboChanged(THIS_FORM_SelectingCombo, New EventArgs)
		Else
			THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
		End If

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnLast control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLast.Click
		' 'Last' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		thisPosition = THIS_FORM_SelectingCombo.Items.Count - 1

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

	End Sub

    ''' <summary>
    ''' Get_s the position.
    ''' </summary>
    ''' <param name="pAuditID">The p audit ID.</param>
    ''' <returns>System.Int32.</returns>
	Private Function Get_Position(ByVal pAuditID As Integer) As Integer
		' Returns the position in the current 'SortedRows' array of the entry corresponding to the given 
		' AudidID.

		Dim MinIndex As Integer
		Dim MaxIndex As Integer
		Dim CurrentMin As Integer
		Dim CurrentMax As Integer
		Dim searchDataRow As DataRow

		Try
			' SortedRows exists ?

			If SortedRows Is Nothing Then
				Return (-1)
				Exit Function
			End If

			' Return (-1) if there are no rows in the 'SortedRows'

			If (SortedRows.GetLength(0) <= 0) Then
				Return (-1)
				Exit Function
			End If


			' Use a modified Search moving outwards from the last returned value to locate the required value.
			' Reflecting the fact that for the most part One looks for closely located records.

			MinIndex = 0
			MaxIndex = SortedRows.GetLength(0) - 1


			' Check First and Last records (Incase 'First' or 'Last' was pressed).

			searchDataRow = SortedRows(MinIndex)

			If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
				Return MinIndex
				Exit Function
			End If

			searchDataRow = SortedRows(MaxIndex)
			If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
				Return MaxIndex
				Exit Function
			End If

			' now search outwards from the last returned value.

			If (Me.thisPosition >= MinIndex) And (Me.thisPosition <= MaxIndex) Then
				CurrentMin = thisPosition
				CurrentMax = CurrentMin + 1
			Else
				CurrentMin = CInt((MinIndex + MaxIndex) / 2)
				CurrentMax = CurrentMin + 1
			End If

			While (CurrentMin >= MinIndex) Or (CurrentMax <= MaxIndex)
				If (CurrentMin >= MinIndex) Then
					searchDataRow = SortedRows(CurrentMin)

					If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
						Return CurrentMin
						Exit Function
					End If

					CurrentMin -= 1
				End If

				If (CurrentMax <= MaxIndex) Then
					searchDataRow = SortedRows(CurrentMax)

					If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
						Return CurrentMax
						Exit Function
					End If

					CurrentMax += 1
				End If

			End While

			Return (-1)
			Exit Function

		Catch ex As Exception
			MainForm.LogError(Me.Name & ", Get_Position()", LOG_LEVELS.Warning, ex.Message, "", ex.StackTrace, True)
			Return (-1)
			Exit Function
		End Try

	End Function

#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

    ''' <summary>
    ''' Handles the Click event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
		' Cancel Changes, redisplay form.

		FormChanged = False
		AddNewRecord = False

		Me.THIS_FORM_SelectingCombo.Enabled = True

		If (thisPosition >= 0) And (thisPosition < Me.SortedRows.GetLength(0)) Then
			thisDataRow = Me.SortedRows(thisPosition)
			Call GetFormData(thisDataRow)
		Else
			Call btnNavFirst_Click(Me, New System.EventArgs)
		End If

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnSave control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
		' Save Changes, if any, without prompting.

		If (FormChanged = True) Then
			Call SetFormData(False)
		End If

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnDelete control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
		Dim Position As Integer
		Dim NextAuditID As Integer

		If (AddNewRecord = True) Then
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Sub
		End If

		' No Appropriate Save permission :-

		If (Me.HasDeletePermission = False) Then
			MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "You do not have permission to Delete this record.", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Sub
		End If

		' *************************************************************
		' KnowledgeDate OK :-
		' *************************************************************

		If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
			MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "Changes to Venice are not allowed when the Knowledgedate is not set to `NOW`", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Sub
		End If

		' *************************************************************
		' Confirm :-
		' *************************************************************
		If MsgBox("Are you Sure ?", MsgBoxStyle.OkCancel, "Confirm Delete") = MsgBoxResult.Cancel Then
			Exit Sub
		End If

		' Check Data position.

		Position = Get_Position(thisAuditID)

		If Position < 0 Then Exit Sub

		' Check Referential Integrity 
		If CheckReferentialIntegrity(_MainForm, Me.SortedRows(Position)) = False Then
			MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "Deleting this record would voilate referential integrity.", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Sub
		End If

		' Resolve row to show after deleting this one.

		NextAuditID = (-1)
		If (Position + 1) < Me.SortedRows.GetLength(0) Then
			NextAuditID = CInt(Me.SortedRows(Position + 1)("AuditID"))
		ElseIf (Position - 1) >= 0 Then
			NextAuditID = CInt(Me.SortedRows(Position - 1)("AuditID"))
		Else
			NextAuditID = (-1)
		End If

		' Delete this row

		Try
			Me.SortedRows(Position).Delete()

			MainForm.AdaptorUpdate(Me.Name, myAdaptor, myTable)

		Catch ex As Exception

			Call MainForm.LogError(Me.Name & ", Delete()", LOG_LEVELS.Error, ex.Message, "Error Deleting Record.", ex.StackTrace, True)
		End Try

		' Tidy Up.

		FormChanged = False

		thisAuditID = NextAuditID
		Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID), True)

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnAdd control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
		' Prepare form to Add a new record.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		thisPosition = -1
		InPaint = True
		MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
		InPaint = False

		GetFormData(Nothing)
		AddNewRecord = True
		Me.btnCancel.Enabled = True
		Me.THIS_FORM_SelectingCombo.Enabled = False

		Call SetButtonStatus()

		THIS_FORM_NewMoveToControl.Focus()

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnClose control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
		' Close Form

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		Me.Close()

	End Sub

#End Region

#Region " Bug hunting "

	' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
	' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
	' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

    ''' <summary>
    ''' Called when [row updating].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
	Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
		Dim a As Integer

		If Not e.Command Is Nothing Then
			a = 1
		End If

		If Not e.Errors Is Nothing Then
			a = 2
		End If
	End Sub

    ''' <summary>
    ''' Called when [row updated].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
	Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
		Dim a As Integer

		If Not e.Command Is Nothing Then
			a = 1
		End If

		If Not e.Errors Is Nothing Then
			a = 2
		End If
	End Sub

    ''' <summary>
    ''' Called when [row fill error].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
	Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
		Dim a As Integer

		a = 1
	End Sub

#End Region







End Class
