﻿' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 08-15-2013
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmSubFundParent.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals


''' <summary>
''' Class frmSubFundParent
''' </summary>
Public Class frmSubFundParent

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmSubFundParent"/> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The BTN cancel
    ''' </summary>
  Friend WithEvents btnCancel As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN save
    ''' </summary>
  Friend WithEvents btnSave As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN close
    ''' </summary>
  Friend WithEvents btnClose As System.Windows.Forms.Button
    ''' <summary>
    ''' The tree view_ sub funds
    ''' </summary>
  Friend WithEvents TreeView_SubFunds As System.Windows.Forms.TreeView
    ''' <summary>
    ''' The context menu_ sub funds
    ''' </summary>
  Friend WithEvents ContextMenu_SubFunds As System.Windows.Forms.ContextMenuStrip
    ''' <summary>
    ''' The menu_ add sub fund
    ''' </summary>
  Friend WithEvents Menu_AddSubFund As System.Windows.Forms.ToolStripMenuItem
    ''' <summary>
    ''' The tool strip separator_ rename
    ''' </summary>
  Friend WithEvents ToolStripSeparator_Rename As System.Windows.Forms.ToolStripSeparator
    ''' <summary>
    ''' The tool strip label_ rename
    ''' </summary>
  Friend WithEvents ToolStripLabel_Rename As System.Windows.Forms.ToolStripMenuItem
    ''' <summary>
    ''' The tool strip text_ rename
    ''' </summary>
  Friend WithEvents ToolStripText_Rename As System.Windows.Forms.ToolStripTextBox
    ''' <summary>
    ''' The tool strip separator_ basket
    ''' </summary>
  Friend WithEvents ToolStripSeparator_Basket As System.Windows.Forms.ToolStripSeparator
    ''' <summary>
    ''' The tool strip label_ basket
    ''' </summary>
  Friend WithEvents ToolStripLabel_Basket As System.Windows.Forms.ToolStripMenuItem
    ''' <summary>
    ''' The tool strip combo_ reference basket
    ''' </summary>
  Friend WithEvents ToolStripCombo_ReferenceBasket As System.Windows.Forms.ToolStripComboBox
    ''' <summary>
    ''' The group box1
    ''' </summary>
  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The label1
    ''' </summary>
  Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The button_ move up
    ''' </summary>
  Friend WithEvents Button_MoveUp As System.Windows.Forms.Button
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container
    Me.btnCancel = New System.Windows.Forms.Button
    Me.btnSave = New System.Windows.Forms.Button
    Me.btnClose = New System.Windows.Forms.Button
    Me.TreeView_SubFunds = New System.Windows.Forms.TreeView
    Me.ContextMenu_SubFunds = New System.Windows.Forms.ContextMenuStrip(Me.components)
    Me.Menu_AddSubFund = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripSeparator_Rename = New System.Windows.Forms.ToolStripSeparator
    Me.ToolStripLabel_Rename = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripText_Rename = New System.Windows.Forms.ToolStripTextBox
    Me.ToolStripSeparator_Basket = New System.Windows.Forms.ToolStripSeparator
    Me.ToolStripLabel_Basket = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripCombo_ReferenceBasket = New System.Windows.Forms.ToolStripComboBox
    Me.Button_MoveUp = New System.Windows.Forms.Button
    Me.GroupBox1 = New System.Windows.Forms.GroupBox
    Me.Label1 = New System.Windows.Forms.Label
    Me.ContextMenu_SubFunds.SuspendLayout()
    Me.GroupBox1.SuspendLayout()
    Me.SuspendLayout()
    '
    'btnCancel
    '
    Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnCancel.Location = New System.Drawing.Point(12, 19)
    Me.btnCancel.Name = "btnCancel"
    Me.btnCancel.Size = New System.Drawing.Size(75, 28)
    Me.btnCancel.TabIndex = 1
    Me.btnCancel.Text = "&Cancel"
    '
    'btnSave
    '
    Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnSave.Location = New System.Drawing.Point(220, 19)
    Me.btnSave.Name = "btnSave"
    Me.btnSave.Size = New System.Drawing.Size(75, 28)
    Me.btnSave.TabIndex = 4
    Me.btnSave.Text = "&Save"
    '
    'btnClose
    '
    Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnClose.Location = New System.Drawing.Point(334, 481)
    Me.btnClose.Name = "btnClose"
    Me.btnClose.Size = New System.Drawing.Size(75, 28)
    Me.btnClose.TabIndex = 2
    Me.btnClose.Text = "&Close"
    '
    'TreeView_SubFunds
    '
    Me.TreeView_SubFunds.AllowDrop = True
    Me.TreeView_SubFunds.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.TreeView_SubFunds.ContextMenuStrip = Me.ContextMenu_SubFunds
    Me.TreeView_SubFunds.Location = New System.Drawing.Point(4, 4)
    Me.TreeView_SubFunds.Name = "TreeView_SubFunds"
    Me.TreeView_SubFunds.Size = New System.Drawing.Size(413, 427)
    Me.TreeView_SubFunds.Sorted = True
    Me.TreeView_SubFunds.TabIndex = 0
    '
    'ContextMenu_SubFunds
    '
    Me.ContextMenu_SubFunds.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_AddSubFund, Me.ToolStripSeparator_Rename, Me.ToolStripLabel_Rename, Me.ToolStripText_Rename, Me.ToolStripSeparator_Basket, Me.ToolStripLabel_Basket, Me.ToolStripCombo_ReferenceBasket})
    Me.ContextMenu_SubFunds.Name = "ContextMenu_SubFunds"
    Me.ContextMenu_SubFunds.Size = New System.Drawing.Size(261, 156)
    '
    'Menu_AddSubFund
    '
    Me.Menu_AddSubFund.Name = "Menu_AddSubFund"
    Me.Menu_AddSubFund.Size = New System.Drawing.Size(260, 22)
    Me.Menu_AddSubFund.Text = "Add Sub Fund"
    '
    'ToolStripSeparator_Rename
    '
    Me.ToolStripSeparator_Rename.Name = "ToolStripSeparator_Rename"
    Me.ToolStripSeparator_Rename.Size = New System.Drawing.Size(257, 6)
    '
    'ToolStripLabel_Rename
    '
    Me.ToolStripLabel_Rename.Enabled = False
    Me.ToolStripLabel_Rename.Name = "ToolStripLabel_Rename"
    Me.ToolStripLabel_Rename.Size = New System.Drawing.Size(260, 22)
    Me.ToolStripLabel_Rename.Text = "Rename Sub Fund"
    '
    'ToolStripText_Rename
    '
    Me.ToolStripText_Rename.Name = "ToolStripText_Rename"
    Me.ToolStripText_Rename.Size = New System.Drawing.Size(200, 23)
    '
    'ToolStripSeparator_Basket
    '
    Me.ToolStripSeparator_Basket.Name = "ToolStripSeparator_Basket"
    Me.ToolStripSeparator_Basket.Size = New System.Drawing.Size(257, 6)
    '
    'ToolStripLabel_Basket
    '
    Me.ToolStripLabel_Basket.Enabled = False
    Me.ToolStripLabel_Basket.Name = "ToolStripLabel_Basket"
    Me.ToolStripLabel_Basket.Size = New System.Drawing.Size(260, 22)
    Me.ToolStripLabel_Basket.Text = "Reference Basket"
    '
    'ToolStripCombo_ReferenceBasket
    '
    Me.ToolStripCombo_ReferenceBasket.Name = "ToolStripCombo_ReferenceBasket"
    Me.ToolStripCombo_ReferenceBasket.Size = New System.Drawing.Size(200, 23)
    '
    'Button_MoveUp
    '
    Me.Button_MoveUp.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Button_MoveUp.Location = New System.Drawing.Point(115, 19)
    Me.Button_MoveUp.Name = "Button_MoveUp"
    Me.Button_MoveUp.Size = New System.Drawing.Size(85, 28)
    Me.Button_MoveUp.TabIndex = 3
    Me.Button_MoveUp.Text = "< 1 Level"
    '
    'GroupBox1
    '
    Me.GroupBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.GroupBox1.Controls.Add(Me.btnCancel)
    Me.GroupBox1.Controls.Add(Me.btnSave)
    Me.GroupBox1.Controls.Add(Me.Button_MoveUp)
    Me.GroupBox1.Location = New System.Drawing.Point(4, 459)
    Me.GroupBox1.Name = "GroupBox1"
    Me.GroupBox1.Size = New System.Drawing.Size(315, 60)
    Me.GroupBox1.TabIndex = 5
    Me.GroupBox1.TabStop = False
    Me.GroupBox1.Text = "Heirarchy"
    '
    'Label1
    '
    Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label1.AutoSize = True
    Me.Label1.Location = New System.Drawing.Point(7, 434)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(261, 13)
    Me.Label1.TabIndex = 6
    Me.Label1.Text = "Right-Click to Add / Rename / Set Reference basket."
    '
    'frmSubFundParent
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.CancelButton = Me.btnCancel
    Me.ClientSize = New System.Drawing.Size(421, 521)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.GroupBox1)
    Me.Controls.Add(Me.TreeView_SubFunds)
    Me.Controls.Add(Me.btnClose)
    Me.Name = "frmSubFundParent"
    Me.Text = "Set Sub-Fund Heirarchy"
    Me.ContextMenu_SubFunds.ResumeLayout(False)
    Me.ContextMenu_SubFunds.PerformLayout()
    Me.GroupBox1.ResumeLayout(False)
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "


  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
  Private WithEvents _MainForm As VeniceMain

  ' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
  Private FormTooltip As New ToolTip()

  ' Form Menu


  ' Form Constants, specific to the table being updated.

    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
  Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

  ' Form Locals, initialised on 'New' defining what standard data items to use
    ''' <summary>
  ''' Variable holding the form's data table name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_TABLENAME As String
    ''' <summary>
  ''' Variable holding the form's data adaptor name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_ADAPTORNAME As String
    ''' <summary>
  ''' Variable holding the form's dataset name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_DATASETNAME As String


  ' The standard ChangeID for this form. e.g. tblSubFund
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

  ' Form Specific Order fields
    ''' <summary>
  ''' Field Name to show in the Select combo.
    ''' </summary>
  Private THIS_FORM_SelectBy As String
    ''' <summary>
  ''' Field Name to order the Select combo by.
    ''' </summary>
  Private THIS_FORM_OrderBy As String

    ''' <summary>
  ''' The field name that holds the unique identifier for each entry. e.g. FundID for the tblFund table.
    ''' </summary>
  Private THIS_FORM_ValueMember As String

  ' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
  Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
  Private THIS_FORM_FormID As VeniceFormID

  ' Data Structures

    ''' <summary>
    ''' My dataset
    ''' </summary>
  Private myDataset As RenaissanceDataClass.DSSubFund  ' Form Specific !!!!
    ''' <summary>
    ''' My table
    ''' </summary>
  Private myTable As RenaissanceDataClass.DSSubFund.tblSubFundDataTable
    ''' <summary>
    ''' My connection
    ''' </summary>
  Private myConnection As SqlConnection
    ''' <summary>
    ''' My adaptor
    ''' </summary>
  Private myAdaptor As SqlDataAdapter

    ''' <summary>
    ''' The this standard dataset
    ''' </summary>
  Private ThisStandardDataset As RenaissanceGlobals.StandardDataset


  ' Active Element.

    ''' <summary>
    ''' The sorted rows
    ''' </summary>
  Private SortedRows() As DataRow
    ''' <summary>
    ''' The select by sorted rows
    ''' </summary>
  Private SelectBySortedRows() As DataRow
    ''' <summary>
    ''' The this audit ID
    ''' </summary>
  Private thisAuditID As Integer
    ''' <summary>
    ''' The this position
    ''' </summary>
  Private thisPosition As Integer
    ''' <summary>
    ''' The __ is over cancel button
    ''' </summary>
  Private __IsOverCancelButton As Boolean
    ''' <summary>
    ''' The _ in use
    ''' </summary>
  Private _InUse As Boolean

  ' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
  Private FormIsValid As Boolean
    ''' <summary>
    ''' The form changed
    ''' </summary>
  Private FormChanged As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
  Private _FormOpenFailed As Boolean
    ''' <summary>
    ''' The in paint
    ''' </summary>
  Private InPaint As Boolean
    ''' <summary>
    ''' The add new record
    ''' </summary>
  Private AddNewRecord As Boolean

  ' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
  Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
  Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
  Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
  Private HasDeletePermission As Boolean

    ''' <summary>
    ''' The current node
    ''' </summary>
  Private CurrentNode As TreeNode

    ''' <summary>
    ''' The sub fund rename_ focus
    ''' </summary>
  Private SubFundRename_Focus As Boolean = False
    ''' <summary>
    ''' The sub fund basket_ focus
    ''' </summary>
  Private SubFundBasket_Focus As Boolean = False

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
  Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
    ' Public property to return handle to the 'Main' Venice form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
  Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
    ' Public property maintaining a value indicating if the cursor is over the 'Cancel'
    ' Button on this form.
    ' This property is specifically designed for use by the field formating Event functions
    ' In order that they do not impose format restrictions if the user is about to click the 
    ' 'Cancel' button.
    '
    Get
      Return __IsOverCancelButton
    End Get
    Set(ByVal Value As Boolean)
      __IsOverCancelButton = Value
    End Set
  End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
  Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
    Get
      Return InPaint
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
    Get
      Return _InUse
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

#End Region

    ''' <summary>
    ''' Class ID_Struct
    ''' </summary>
  Private Class ID_Struct
        ''' <summary>
        ''' The audit ID
        ''' </summary>
    Public AuditID As Integer
        ''' <summary>
        ''' The sub fund ID
        ''' </summary>
    Public SubFundID As Integer
        ''' <summary>
        ''' The parent ID
        ''' </summary>
    Public ParentID As Integer
        ''' <summary>
        ''' The reference basket ID
        ''' </summary>
    Public ReferenceBasketID As Integer
        ''' <summary>
        ''' The moved
        ''' </summary>
    Public Moved As Boolean

        ''' <summary>
        ''' Initializes a new instance of the <see cref="ID_Struct"/> class.
        ''' </summary>
        ''' <param name="pAuditID">The p audit ID.</param>
        ''' <param name="pSubFundID">The p sub fund ID.</param>
        ''' <param name="pParentID">The p parent ID.</param>
        ''' <param name="pReferenceBasketID">The p reference basket ID.</param>
        ''' <param name="pMoved">if set to <c>true</c> [p moved].</param>
    Public Sub New(ByVal pAuditID As Integer, ByVal pSubFundID As Integer, ByVal pParentID As Integer, ByVal pReferenceBasketID As Integer, ByVal pMoved As Boolean)
      Me.AuditID = pAuditID
      Me.SubFundID = pSubFundID
      Me.ParentID = pParentID
      Me.ReferenceBasketID = pReferenceBasketID
      Me.Moved = pMoved
    End Sub
  End Class

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmSubFundParent"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  Public Sub New(ByVal pMainForm As VeniceMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()



    _MainForm = pMainForm
    AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

    _FormOpenFailed = False
    _InUse = True

    ' ******************************************************
    ' Form Specific Settings :
    ' ******************************************************


    ' Default Select and Order fields.

    THIS_FORM_SelectBy = "SubFundName"
    THIS_FORM_OrderBy = "SubFundName"

    THIS_FORM_ValueMember = "SubFundID"

    ' Form Permissioning :-

    THIS_FORM_PermissionArea = "frmSubFundParent"
    THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

    ' 'This' form ID

    THIS_FORM_FormID = VeniceFormID.frmSubFundParent

    ' This form's dataset type.

    ThisStandardDataset = RenaissanceStandardDatasets.tblSubFund ' This Defines the Form Data !!! 



    ' Form Control Changed events

    ' Set up the ToolTip
    MainForm.SetFormToolTip(Me, FormTooltip)

    ' ******************************************************
    ' End Form Specific.
    ' ******************************************************

    ' Data object names standard to this Form type.

    THIS_TABLENAME = ThisStandardDataset.TableName
    THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
    THIS_DATASETNAME = ThisStandardDataset.DatasetName

    THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

    ' Establish / Retrieve data objects for this form.

    myConnection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)
    myAdaptor = MainForm.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, VENICE_CONNECTION, THIS_TABLENAME)
    myDataset = MainForm.Load_Table(ThisStandardDataset, False)
    myTable = myDataset.Tables(0)

  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
  Public Sub ResetForm() Implements StandardVeniceForm.ResetForm
    THIS_FORM_SelectBy = "SubFundName"
    THIS_FORM_OrderBy = "SubFundName"

    Call Form_Load(Me, New System.EventArgs)
  End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
  Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
    ALWAYS_CLOSE_THIS_FORM = True
    Me.Close()
  End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
    _FormOpenFailed = False
    _InUse = True

    ' Initialise Data structures. Connection, Adaptor and Dataset.

    If Not (MainForm Is Nothing) Then
      FormIsValid = True
    Else
      MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (myConnection Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (myAdaptor Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (myDataset Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    ' Initialse form

    InPaint = True
    IsOverCancelButton = False

    ' Check User permissions
    Call CheckPermissions()
    If (HasReadPermission = False) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

      FormIsValid = False
      _FormOpenFailed = True

      Exit Sub
    End If

    Call SetGroupListCombo()

    ' Build Sorted data list from which this form operates
    Call SetSortedRows()

    SubFundRename_Focus = False

    ' Display initial record.
    Call GetInitialData()

    InPaint = False


  End Sub

    ''' <summary>
    ''' Handles the Closing event of the frmSubFund control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
  Private Sub frmSubFund_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    Dim HideForm As Boolean

    ' Hide or Close this form ?
    ' All depends on how many of this form type are Open or in Cache...

    _InUse = False

    If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
      HideForm = False
    Else
      If (FormChanged = True) Then
        Call SetFormData()
      End If

      HideForm = True
      If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
        HideForm = False
      End If
    End If

    If HideForm = True Then
      MainForm.HideInFormsCollection(Me)
      Me.Hide() ' NPP Fix

      e.Cancel = True
    Else
      Try
        MainForm.RemoveFromFormsCollection(Me)
        RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate
      Catch ex As Exception
      End Try
    End If

  End Sub


#End Region

  ' Routine to handle changes / updates to tables by this and other windows.
  ' If this, or any other, form posts a change to a table, then it will invoke an update event 
  ' detailing what tables have been altered.
  ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
  ' the 'VeniceAutoUpdate' event of the main Venice form.
  ' Each form may them react as appropriate to changes in any table that might impact it.
  '
    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    Dim OrgInPaint As Boolean
    Dim KnowledgeDateChanged As Boolean
    Dim SetButtonStatus_Flag As Boolean

    If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

    OrgInPaint = InPaint
    InPaint = True
    KnowledgeDateChanged = False
    SetButtonStatus_Flag = False

    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
      KnowledgeDateChanged = True
    End If

    ' ****************************************************************
    ' Check for changes relevant to this form
    ' ****************************************************************

    ' Changes to the tblGroupList table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGroupList) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Call SetGroupListCombo()
    End If

    ' Changes to the KnowledgeDate :-
    If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
      SetButtonStatus_Flag = True
    End If

    ' Changes to the tblUserPermissions table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

      ' Check ongoing permissions.

      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

        FormIsValid = False
        Me.Close()
        Exit Sub
      End If

      SetButtonStatus_Flag = True

    End If


    ' ****************************************************************
    ' Changes to the Main FORM table :-
    ' ****************************************************************

    If (e.TableChanged(THIS_FORM_ChangeID) = True) Or KnowledgeDateChanged Then

      ' Re-Set Controls etc.
      Call SetSortedRows()

      Dim NodeArray() As TreeNode
      Dim thisDataRow As RenaissanceDataClass.DSSubFund.tblSubFundRow
      Dim MissingNodes As Integer
      Dim ParentIDsChanged As Boolean
      Dim NodeCounter As Integer

      MissingNodes = 0
      ParentIDsChanged = False

      ' Check some SubFunds still remain

      If SortedRows Is Nothing Then
        GetInitialData()
        GoTo finish
      End If

      If SortedRows.Length <= 0 Then
        GetInitialData()
        GoTo finish
      End If

      ' I have chosen not to reflect changes by other users, It was causing problems when the current user made changes too.
      If (False) Then
        ' Check each SubFund in the table against it's respective node (if it exists)
        NodeArray = GetNodeArray(Me.TreeView_SubFunds)
        For Each thisDataRow In SortedRows
          MissingNodes += 1

          For NodeCounter = 0 To (NodeArray.GetLength(0) - 1)
            If Not (NodeArray(NodeCounter) Is Nothing) Then
              If CInt(thisDataRow("AuditID")) = CType(NodeArray(NodeCounter).Tag, ID_Struct).AuditID Then
                ' OK, this SubFund exists as a node...
                MissingNodes -= 1

                ' Has it changed ?
                If (CInt(thisDataRow.ParentFundID) <> CType(NodeArray(NodeCounter).Tag, ID_Struct).ParentID) Then
                  ' Parent has changed

                  MessageBox.Show("The SubFund Parent heirarchy has been changed by another user." & vbCrLf & "Parent structure will be re-loaded, any unsaved changed will be lost.", "Warning : External SubFund Change", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                  GetInitialData()
                  GoTo finish
                End If

                Exit For
              End If
            End If
          Next
        Next

        If (MissingNodes > 0) Then
          MessageBox.Show("The new SubFunds have been added by another user." & vbCrLf & "Parent structure will be re-loaded, any unsaved changed will be lost.", "Warning : External SubFund Change", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
          GetInitialData()
        End If
      End If ' False

    End If

    ' ****************************************************************
    ' Repaint if not currently in Edit Mode
    '
    ' ****************************************************************
finish:

    InPaint = OrgInPaint
    If SetButtonStatus_Flag Then
      Call SetButtonStatus()
    End If

  End Sub

    ''' <summary>
    ''' Gets the node array.
    ''' </summary>
    ''' <param name="pTreeView">The p tree view.</param>
    ''' <returns>TreeNode[][].</returns>
  Private Function GetNodeArray(ByRef pTreeView As TreeView) As TreeNode()
    ' ****************************************************************
    ' Header function to retrieve an array of ALL TreeNodes from the 
    ' Given TreeView.
    ' ****************************************************************
    Dim ReturnArray() As TreeNode
    Dim NodeCount As Integer

    Try
      Dim ThisNode As TreeNode
      NodeCount = 0
      For Each ThisNode In pTreeView.Nodes
        NodeCount += CountNodeArray(ThisNode)
      Next

      ReDim ReturnArray(NodeCount)

      NodeCount = 0
      For Each ThisNode In pTreeView.Nodes
        PopulateNodeArray(ThisNode, ReturnArray, NodeCount)
      Next

    Catch ex As Exception
      Erase ReturnArray
    End Try

    Return ReturnArray

  End Function

    ''' <summary>
    ''' Counts the node array.
    ''' </summary>
    ''' <param name="pTreeNode">The p tree node.</param>
    ''' <returns>System.Int32.</returns>
  Private Function CountNodeArray(ByRef pTreeNode As TreeNode) As Integer
    ' ****************************************************************
    ' Recursive function used to count the number of TreeNodes in a TreeView.
    ' ****************************************************************
    Dim Rval As Integer

    Dim ThisNode As TreeNode
    Rval = 1
    For Each ThisNode In pTreeNode.Nodes
      Rval += CountNodeArray(ThisNode)
    Next

    Return Rval
  End Function

    ''' <summary>
    ''' Populates the node array.
    ''' </summary>
    ''' <param name="pTreeNode">The p tree node.</param>
    ''' <param name="ReturnArray">The return array.</param>
    ''' <param name="RVal">The R val.</param>
  Private Sub PopulateNodeArray(ByRef pTreeNode As TreeNode, ByRef ReturnArray() As TreeNode, ByRef RVal As Integer)
    ' ****************************************************************
    ' Recursive function to retrieve an array of ALL TreeNodes from the 
    ' Given TreeView.
    ' ****************************************************************

    Dim ThisNode As TreeNode
    ReturnArray(RVal) = pTreeNode
    RVal += 1
    For Each ThisNode In pTreeNode.Nodes
      PopulateNodeArray(ThisNode, ReturnArray, RVal)
    Next

  End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

  ' Build Sorted list from the Source dataset and update the Select Combo
    ''' <summary>
    ''' Sets the sorted rows.
    ''' </summary>
  Private Sub SetSortedRows()

    Dim OrgInPaint As Boolean

    ' Code is pretty Generic form here on...

    ' Set paint local so that changes to the selection combo do not trigger form updates.

    OrgInPaint = InPaint
    InPaint = True

    ' Get selected Row sets, or exit if no data is present.
    If myDataset Is Nothing Then
      ReDim SortedRows(0)
      ReDim SelectBySortedRows(0)
    Else
      SortedRows = myTable.Select("True", THIS_FORM_OrderBy)
      SelectBySortedRows = myTable.Select("True", THIS_FORM_SelectBy)
    End If



    InPaint = OrgInPaint
  End Sub


  ' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
  Private Sub CheckPermissions()

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

  End Sub

  ' Flag changes to form controls, Event Associations made in 'New' routine.
    ''' <summary>
  ''' Basic event handler, called when a form value is changed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    If InPaint = False Then
      If (Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord) Then
        FormChanged = True
        Me.btnSave.Enabled = True
        Me.btnCancel.Enabled = True
      End If
    End If

  End Sub

    ''' <summary>
    ''' Selects the menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
  Public Sub SelectMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
    ' Event handler for the Select Menu
    ' Event association is made during the dynamic menu creation

    THIS_FORM_SelectBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

    Call SetSortedRows()

  End Sub

    ''' <summary>
    ''' Orders the menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
  Public Sub OrderMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
    ' Event handler for the Order Menu
    ' Event association is made during the dynamic menu creation

    THIS_FORM_OrderBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

    Call SetSortedRows()

  End Sub


#End Region

#Region " Get & Set Form Data / SetButton / ValidateForm / btnCancel Events (Form Specific Code) "

    ''' <summary>
    ''' Gets the initial data.
    ''' </summary>
  Private Sub GetInitialData()
    Dim DSFund As RenaissanceDataClass.DSFund
    Dim GetRows() As RenaissanceDataClass.DSFund.tblFundRow
    Dim thisRow As RenaissanceDataClass.DSFund.tblFundRow

    Dim SubFundsDS As RenaissanceDataClass.DSSubFund = MainForm.Load_Table(RenaissanceStandardDatasets.tblSubFund)
    Dim RootSubFunds() As RenaissanceDataClass.DSSubFund.tblSubFundRow = SubFundsDS.tblSubFund.Select("(SubFundID=ParentFundID) OR (ParentFundID=0)", "SubFundName")
    Dim SubFundRow As RenaissanceDataClass.DSSubFund.tblSubFundRow

    DSFund = MainForm.Load_Table(RenaissanceStandardDatasets.tblFund)

    GetRows = CType(DSFund.tblFund.Select("FundClosed=0"), RenaissanceDataClass.DSFund.tblFundRow())

    While (Me.TreeView_SubFunds.Nodes.Count > 0)
      Me.TreeView_SubFunds.Nodes.RemoveAt(0)
    End While
    Me.TreeView_SubFunds.Nodes.Clear()

    Try
      Me.TreeView_SubFunds.BeginUpdate()

      Dim RootNode As New TreeNode(">")
      RootNode.Tag = New ID_Struct((-1), (-1), (-1), 0, False)
      Me.TreeView_SubFunds.Nodes.Add(RootNode)

      For Each thisRow In GetRows
        Dim NewNode As New TreeNode(thisRow.FundName)
        NewNode.Tag = New ID_Struct(thisRow.AuditID, thisRow.FundID, thisRow.FundID, 0, False)
        FillNode(NewNode)
        RootNode.Nodes.Add(NewNode)
      Next

      For Each SubFundRow In RootSubFunds
        Dim NewNode As New TreeNode(SubFundRow.SubFundName)
        NewNode.Tag = New ID_Struct(SubFundRow.AuditID, SubFundRow.SubFundID, SubFundRow.ParentFundID, SubFundRow.ReferenceBasket, False)
        FillNode(NewNode)
        RootNode.Nodes.Add(NewNode)
      Next

      Me.TreeView_SubFunds.EndUpdate()
      RootNode.Expand()

    Catch ex As Exception
    End Try

    Me.btnCancel.Enabled = False
    Me.btnSave.Enabled = False

  End Sub

    ''' <summary>
    ''' Fills the node.
    ''' </summary>
    ''' <param name="SubfundNode">The subfund node.</param>
  Private Sub FillNode(ByVal SubfundNode As TreeNode)
    Dim SubFundID As Integer

    Try
      SubFundID = CType(SubfundNode.Tag, ID_Struct).SubFundID
    Catch ex As Exception
      Exit Sub
    End Try

    Dim GetRows() As DataRow
    Dim thisRow As RenaissanceDataClass.DSSubFund.tblSubFundRow

    GetRows = myTable.Select("(ParentFundID = " & SubFundID & ") AND (SubFundID <> " & SubFundID & ")")

    SubfundNode.Nodes.Clear()

    Try
      Me.TreeView_SubFunds.BeginUpdate()
      For Each thisRow In GetRows
        Dim NewNode As New TreeNode(thisRow.SubFundName)
        NewNode.Tag = New ID_Struct(thisRow.AuditID, thisRow.SubFundID, thisRow.ParentFundID, thisRow.ReferenceBasket, False)
        FillNode(NewNode)
        SubfundNode.Nodes.Add(NewNode)
      Next
      Me.TreeView_SubFunds.EndUpdate()

    Catch ex As Exception
      Exit Sub
    End Try


  End Sub

    ''' <summary>
    ''' Handles the BeforeExpand event of the TreeView_SubFunds control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.TreeViewCancelEventArgs"/> instance containing the event data.</param>
  Private Sub TreeView_SubFunds_BeforeExpand(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewCancelEventArgs) Handles TreeView_SubFunds.BeforeExpand
    'FillNode(e.Node)
  End Sub

    ''' <summary>
    ''' Handles the ItemDrag event of the TreeView_SubFunds control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.ItemDragEventArgs"/> instance containing the event data.</param>
  Private Sub TreeView_SubFunds_ItemDrag(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemDragEventArgs) Handles TreeView_SubFunds.ItemDrag
    'Set the drag node and initiate the DragDrop 
    DoDragDrop(e.Item, DragDropEffects.Move)
  End Sub

    ''' <summary>
    ''' Handles the DragEnter event of the TreeView_SubFunds control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.DragEventArgs"/> instance containing the event data.</param>
  Private Sub TreeView_SubFunds_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles TreeView_SubFunds.DragEnter
    'See if there is a TreeNode being dragged
    If e.Data.GetDataPresent("System.Windows.Forms.TreeNode", _
        True) Then
      'TreeNode found allow move effect
      e.Effect = DragDropEffects.Move
    Else
      'No TreeNode found, prevent move
      e.Effect = DragDropEffects.None
    End If
  End Sub

    ''' <summary>
    ''' Handles the DragLeave event of the TreeView_SubFunds control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub TreeView_SubFunds_DragLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles TreeView_SubFunds.DragLeave

  End Sub

    ''' <summary>
    ''' Handles the DragOver event of the TreeView_SubFunds control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.DragEventArgs"/> instance containing the event data.</param>
  Private Sub TreeView_SubFunds_DragOver(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles TreeView_SubFunds.DragOver
    'Check that there is a TreeNode being dragged 
    If e.Data.GetDataPresent("System.Windows.Forms.TreeNode", _
           True) = False Then Exit Sub

    'Get the TreeView raising the event (incase multiple on form)

    Dim selectedTreeview As TreeView = CType(sender, TreeView)

    'As the mouse moves over nodes, provide feedback to the user by highlighting the node that is the 
    'current drop target

    Dim pt As Point = _
        CType(sender, TreeView).PointToClient(New Point(e.X, e.Y))
    Dim targetNode As TreeNode = selectedTreeview.GetNodeAt(pt)

    'See if the targetNode is currently selected, if so no need to validate again

    If Not (selectedTreeview.SelectedNode Is targetNode) Then
      'Select the    node currently under the cursor
      selectedTreeview.SelectedNode = targetNode

      'Check that the selected node is not the dropNode and
      'also that it is not a child of the dropNode and 
      'therefore an invalid target
      Dim dropNode As TreeNode = _
          CType(e.Data.GetData("System.Windows.Forms.TreeNode"),  _
          TreeNode)

      Do Until targetNode Is Nothing
        If targetNode Is dropNode Then
          e.Effect = DragDropEffects.None
          Exit Sub
        End If
        targetNode = targetNode.Parent
      Loop

      'Currently selected node is a suitable target
      e.Effect = DragDropEffects.Move
    End If
  End Sub

    ''' <summary>
    ''' Handles the DragDrop event of the TreeView_SubFunds control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.DragEventArgs"/> instance containing the event data.</param>
  Private Sub TreeView_SubFunds_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles TreeView_SubFunds.DragDrop
    'Check that there is a TreeNode being dragged
    If e.Data.GetDataPresent("System.Windows.Forms.TreeNode", _
          True) = False Then Exit Sub

    'Get the TreeView raising the event (incase multiple on form)
    Dim selectedTreeview As TreeView = CType(sender, TreeView)

    'Get the TreeNode being dragged
    Dim dropNode As TreeNode = _
          CType(e.Data.GetData("System.Windows.Forms.TreeNode"),  _
          TreeNode)

    'The target node should be selected from the DragOver event
    Dim targetNode As TreeNode = selectedTreeview.SelectedNode

    If (CType(targetNode.Tag, ID_Struct).SubFundID = CType(dropNode.Tag, ID_Struct).ParentID) Or ((CType(targetNode.Tag, ID_Struct).SubFundID <= 0) And (CType(dropNode.Tag, ID_Struct).SubFundID = CType(dropNode.Tag, ID_Struct).ParentID)) Then
      dropNode.ForeColor = Color.Black
      CType(dropNode.Tag, ID_Struct).Moved = False
    Else
      dropNode.ForeColor = Color.Red
      CType(dropNode.Tag, ID_Struct).Moved = True
      Call FormControlChanged(Me, New System.EventArgs)
    End If

    'Remove the drop node from its current location
    dropNode.Remove()

    'If there is no targetNode add dropNode to the bottom of
    'the TreeView root nodes, otherwise add it to the end of
    'the dropNode child nodes
    If targetNode Is Nothing Then
      selectedTreeview.Nodes.Add(dropNode)
    Else
      targetNode.Nodes.Add(dropNode)
    End If

    'Ensure the newley created node is visible to
    'the user and select it
    dropNode.EnsureVisible()
    selectedTreeview.SelectedNode = dropNode

  End Sub

    ''' <summary>
    ''' Handles the Click event of the Button_MoveUp control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Button_MoveUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_MoveUp.Click
    Dim dropnode As TreeNode
    Dim targetNode As TreeNode
    Dim selectedTreeview As TreeView = TreeView_SubFunds

    ' Exit if no node is selected
    If Me.TreeView_SubFunds.SelectedNode Is Nothing Then
      Exit Sub
    End If
    dropnode = Me.TreeView_SubFunds.SelectedNode

    ' Exit if this is the root node
    If (dropnode.Parent Is Nothing) Then
      Exit Sub
    End If
    targetNode = dropnode.Parent
    ' Exit if This node's parent is the root node
    If (CType(targetNode.Tag, ID_Struct).SubFundID <= 0) Then
      Exit Sub
    End If
    If (targetNode.Parent Is Nothing) Then
      Exit Sub
    End If
    targetNode = targetNode.Parent

    If (CType(targetNode.Tag, ID_Struct).SubFundID = CType(dropnode.Tag, ID_Struct).ParentID) Or ((CType(targetNode.Tag, ID_Struct).SubFundID <= 0) And (CType(dropnode.Tag, ID_Struct).SubFundID = CType(dropnode.Tag, ID_Struct).ParentID)) Then
      dropnode.ForeColor = Color.Black
      CType(dropnode.Tag, ID_Struct).Moved = False
    Else
      dropnode.ForeColor = Color.Red
      CType(dropnode.Tag, ID_Struct).Moved = True
      Call FormControlChanged(Me, New System.EventArgs)
    End If

    'Remove the drop node from its current location
    dropnode.Remove()

    'If there is no targetNode add dropNode to the bottom of
    'the TreeView root nodes, otherwise add it to the end of
    'the dropNode child nodes
    If targetNode Is Nothing Then
      selectedTreeview.Nodes.Add(dropnode)
    Else
      targetNode.Nodes.Add(dropnode)
    End If

    'Ensure the newley created node is visible to
    'the user and select it
    dropnode.EnsureVisible()
    selectedTreeview.SelectedNode = dropnode


  End Sub

    ''' <summary>
    ''' Sets the form data.
    ''' </summary>
    ''' <param name="pConfirm">if set to <c>true</c> [p confirm].</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function SetFormData(Optional ByVal pConfirm As Boolean = True) As Boolean
    ' *************************************************************
    '
    ' *************************************************************
    Dim ProtectedItem As Boolean = False

    ' *************************************************************
    ' Appropriate Save permission :-
    ' *************************************************************

    If (Me.HasUpdatePermission = False) And ((Me.HasInsertPermission = False) Or (Me.AddNewRecord = False)) Then
      MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "You do not have permission to save this record.", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Function
    End If

    ' *************************************************************
    ' If Save button is disabled then should not be able to save, exit silently.
    ' *************************************************************
    If Me.btnSave.Enabled = False Then
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Function
    End If

    ' *************************************************************
    ' KnowledgeDate OK :-
    ' *************************************************************

    If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
      MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Changes to Venice are not allowed when the Knowledgedate is not set to `NOW`", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Function
    End If

    ' Confirm Save, if required.

    If (pConfirm = True) Then
      If MessageBox.Show("Save Changes ?", "Save Changes ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
        Return True
        Exit Function
      End If
    End If

    ' *************************************************************
    ' Procedure to Save the current form information.
    ' *************************************************************

    Dim UpdateRows(0) As DataRow

    If (FormChanged = False) Or (FormIsValid = False) Then
      Return False
      Exit Function
    End If


    Try
      ' Set 'Paint' flag.
      InPaint = True

      ' *************************************************************
      ' Lock the Data Table, to prevent update conflicts.
      ' *************************************************************
      SyncLock myTable

        ' Ensure SortedRows is up to date.
        SetSortedRows()

        ' Initiate Edit,
        Call SaveChangedNodes(Me.TreeView_SubFunds.TopNode)

      End SyncLock

    Catch ex As Exception
    Finally
      InPaint = False
    End Try

    ' Finish off

    AddNewRecord = False
    FormChanged = False
    Me.btnCancel.Enabled = False
    Me.btnSave.Enabled = False

    ' Propogate changes

    Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID), True)

  End Function

    ''' <summary>
    ''' Saves the changed nodes.
    ''' </summary>
    ''' <param name="pParentNode">The p parent node.</param>
  Private Sub SaveChangedNodes(ByRef pParentNode As TreeNode)
    Dim ChildNode As TreeNode
    Dim AuditID As Integer
    Dim SubFundID As Integer
    Dim CurrentParentID As Integer
    Dim NewParentID As Integer
    Dim UpdateRows(0) As DataRow
    Dim ErrMessage As String
    Dim ErrFlag As Boolean
    Dim ErrStack As String
    Dim thisDataRow As RenaissanceDataClass.DSSubFund.tblSubFundRow

    For Each ChildNode In pParentNode.Nodes
      Call SaveChangedNodes(ChildNode)

      Try
        ' Get Information on this node and it's parent

        AuditID = CType(ChildNode.Tag, ID_Struct).AuditID
        SubFundID = CType(ChildNode.Tag, ID_Struct).SubFundID
        CurrentParentID = CType(ChildNode.Tag, ID_Struct).ParentID
        NewParentID = CType(pParentNode.Tag, ID_Struct).SubFundID

        thisDataRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblSubFund, AuditID), RenaissanceDataClass.DSSubFund.tblSubFundRow)

        ' Resolve if it has changed, If not then don't save it - (AuditID = (-1))

        If (CType(ChildNode.Tag, ID_Struct).Moved) Then
          If ((NewParentID <= 0) And (SubFundID = CurrentParentID)) Or _
              (NewParentID = CurrentParentID) Then
            AuditID = (-1)
            thisDataRow = Nothing
          ElseIf (NewParentID <= 0) Then
            NewParentID = SubFundID
          End If
        Else
          thisDataRow = Nothing
        End If

      Catch ex As Exception
        AuditID = (-1)
        thisDataRow = Nothing
      End Try

      ' If this SubFund has changed, then save changes.
      If (AuditID > 0) And (thisDataRow IsNot Nothing) Then

        thisDataRow.BeginEdit()

        thisDataRow.ParentFundID = NewParentID
        thisDataRow.EndEdit()
        InPaint = False

        ' Add and Update DataRow. 

        UpdateRows(0) = thisDataRow

        ' Post Additions / Updates to the underlying table :-
        Dim temp As Integer
        Try
          ErrFlag = False

          myAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
          myAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

          temp = MainForm.AdaptorUpdate(Me.Name, myAdaptor, UpdateRows)
          CType(ChildNode.Tag, ID_Struct).ParentID = NewParentID
          ChildNode.ForeColor = Color.Black
          CType(ChildNode.Tag, ID_Struct).Moved = False
        Catch ex As Exception

          ErrMessage = ex.Message
          ErrFlag = True
          ErrStack = ex.StackTrace

          ' *************************************************************
          ' Error report. Not done in the Try-Catch as that would serve to persist the SyncLock.
          ' *************************************************************

          If (ErrFlag = True) Then
            Call MainForm.LogError(Me.Name & ", SetFormData", 0, ErrMessage, "Error Saving Data", ErrStack, True)
          End If

        End Try

      Else
        ChildNode.ForeColor = Color.Black
        CType(ChildNode.Tag, ID_Struct).Moved = False
      End If

    Next


  End Sub


    ''' <summary>
    ''' Sets the button status.
    ''' </summary>
  Private Sub SetButtonStatus()
    ' Sets the status of the form controlls appropriate to the current users 
    'permissions and the 'Changed' or 'New' status of the form.

    ' No Read permission :-

    If Me.HasReadPermission = False Then
      MainForm.LogError(Me.Name & ", SetButtonStatus()", 0, "", "You do not have Read permission for this Form", "", True)
      Me.Close()
      Exit Sub
    End If

    If (Me.MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And _
      ((Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord)) Then

      Me.TreeView_SubFunds.Enabled = True

    Else

      Me.TreeView_SubFunds.Enabled = False

    End If

  End Sub

    ''' <summary>
    ''' Validates the form.
    ''' </summary>
    ''' <param name="pReturnString">The p return string.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function ValidateForm(ByRef pReturnString As String) As Boolean
    ' Form Validation code.
    ' 
    ' This code should be the final arbiter of what is allowed. no assumptions regarding 
    ' prior validation should be made.
    ' 
    ' This Code is called by the SetFormData routine before position changes.
    '
    Dim RVal As Boolean

    RVal = True
    pReturnString = ""

    ' Validate 

    Return RVal

  End Function

    ''' <summary>
    ''' Handles the MouseEnter event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnCancel_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseEnter
    ' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
    Me.IsOverCancelButton = True
  End Sub

    ''' <summary>
    ''' Handles the MouseLeave event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnCancel_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseLeave
    ' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
    Me.IsOverCancelButton = False
  End Sub

#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

    ''' <summary>
    ''' Handles the Click event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    ' Cancel Changes, redisplay form.

    FormChanged = False
    AddNewRecord = False

    GetInitialData()

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnSave control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
    ' Save Changes, if any, without prompting.

    If (FormChanged = True) Then
      Call SetFormData(False)
    End If

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnClose control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
    ' Close Form

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    Me.Close()

  End Sub

#End Region

#Region " Bug hunting "

  ' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
  ' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
  ' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

    ''' <summary>
    ''' Called when [row updating].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

    ''' <summary>
    ''' Called when [row updated].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

    ''' <summary>
    ''' Called when [row fill error].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
    Dim a As Integer

    a = 1
  End Sub

#End Region


    ''' <summary>
    ''' Class ComboBoxItemClass
    ''' </summary>
  Public Class ComboBoxItemClass
        ''' <summary>
        ''' The _ DM
        ''' </summary>
    Private _DM As String
        ''' <summary>
        ''' The _ VM
        ''' </summary>
    Private _VM As Integer

        ''' <summary>
        ''' Prevents a default instance of the <see cref="ComboBoxItemClass"/> class from being created.
        ''' </summary>
    Private Sub New()
    End Sub

        ''' <summary>
        ''' Initializes a new instance of the <see cref="ComboBoxItemClass"/> class.
        ''' </summary>
        ''' <param name="Display">The display.</param>
        ''' <param name="Value">The value.</param>
    Public Sub New(ByVal Display As String, ByVal Value As Integer)
      _DM = Display
      _VM = Value
    End Sub

        ''' <summary>
        ''' Gets or sets the DM.
        ''' </summary>
        ''' <value>The DM.</value>
    Public Property DM() As String
      Get
        Return _DM
      End Get
      Set(ByVal value As String)
        _DM = value
      End Set
    End Property

        ''' <summary>
        ''' Gets or sets the VM.
        ''' </summary>
        ''' <value>The VM.</value>
    Public Property VM() As Integer
      Get
        Return _VM
      End Get
      Set(ByVal value As Integer)
        _VM = value
      End Set
    End Property
  End Class

    ''' <summary>
    ''' Sets the group list combo.
    ''' </summary>
  Private Sub SetGroupListCombo()
    ' **********************************************************************************
    '
    '
    ' **********************************************************************************

    Try

      ToolStripCombo_ReferenceBasket.Items.Clear()

      Dim GroupListDS As RenaissanceDataClass.DSGroupList = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblGroupList), RenaissanceDataClass.DSGroupList)
      Dim thisGroupListRow As RenaissanceDataClass.DSGroupList.tblGroupListRow

      If (GroupListDS IsNot Nothing) Then

        ToolStripCombo_ReferenceBasket.Items.Add(New ComboBoxItemClass("", 0))

        For Each thisGroupListRow In GroupListDS.tblGroupList.Select("true", "GroupListName")
          ToolStripCombo_ReferenceBasket.Items.Add(New ComboBoxItemClass(thisGroupListRow.GroupListName, thisGroupListRow.GroupListID))
        Next

        ToolStripCombo_ReferenceBasket.ComboBox.DisplayMember = "DM"

      End If

    Catch ex As Exception

    End Try

  End Sub

    ''' <summary>
    ''' Handles the MouseMove event of the TreeView_SubFunds control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.MouseEventArgs"/> instance containing the event data.</param>
  Private Sub TreeView_SubFunds_MouseMove(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TreeView_SubFunds.MouseMove
    ' **********************************************************************************
    '
    '
    ' **********************************************************************************

    Try

      CurrentNode = TreeView_SubFunds.GetNodeAt(e.Location)

      If (CurrentNode Is Nothing) OrElse (CurrentNode.Level <= 1) Then
        ToolStripSeparator_Rename.Visible = False
        ToolStripLabel_Rename.Visible = False
        ToolStripText_Rename.Visible = False
        ToolStripSeparator_Basket.Visible = False
        ToolStripLabel_Basket.Visible = False
        ToolStripCombo_ReferenceBasket.Visible = False

        If (CurrentNode Is Nothing) OrElse (CurrentNode.Level <= 0) Then
          Menu_AddSubFund.Visible = False
        Else
          Menu_AddSubFund.Visible = True
        End If
      Else
        Menu_AddSubFund.Visible = True
        ToolStripSeparator_Rename.Visible = True
        ToolStripLabel_Rename.Visible = True
        ToolStripText_Rename.Visible = True
        ToolStripSeparator_Basket.Visible = True
        ToolStripLabel_Basket.Visible = True
        ToolStripCombo_ReferenceBasket.Visible = True

        ToolStripText_Rename.Text = CurrentNode.Text

        Dim thisComboBoxItem As ComboBoxItemClass
        Dim thisID_Struct As ID_Struct = CType(CurrentNode.Tag, ID_Struct)

        ToolStripCombo_ReferenceBasket.SelectedIndex = 0
        If (thisID_Struct IsNot Nothing) AndAlso (thisID_Struct.ReferenceBasketID > 0) Then
          For Each thisComboBoxItem In ToolStripCombo_ReferenceBasket.Items
            If (thisComboBoxItem.VM = thisID_Struct.ReferenceBasketID) Then
              ToolStripCombo_ReferenceBasket.SelectedItem = thisComboBoxItem
              Exit For
            End If
          Next
        End If

      End If

    Catch ex As Exception

    End Try

  End Sub

    ''' <summary>
    ''' Handles the Click event of the Menu_AddSubFund control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Menu_AddSubFund_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_AddSubFund.Click
    ' **********************************************************************************
    ' Add new Sub Fund.
    '
    ' **********************************************************************************

    Try

      Dim NewSubfundName As String

      ' Get New Subfund name

      NewSubfundName = InputBox("New Subfund Name", "New Subfund", "")

      ' If Name has length, i.e. 'OK' was pressed...

      If (NewSubfundName.Length > 0) Then

        ' Get Subfund table and add new row.

        Dim newTblSubfundRow As RenaissanceDataClass.DSSubFund.tblSubFundRow

        newTblSubfundRow = myTable.NewtblSubFundRow

        newTblSubfundRow.SubFundID = 0
        newTblSubfundRow.SubFundName = NewSubfundName
        newTblSubfundRow.ParentFundID = CType(CurrentNode.Tag, ID_Struct).SubFundID

        ' Update table.

        Try
          SyncLock myTable
            myTable.AddtblSubFundRow(newTblSubfundRow)

            myAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
            myAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

            MainForm.AdaptorUpdate(Me.Name, myAdaptor, New DataRow() {newTblSubfundRow})
          End SyncLock
        Catch ex As Exception
          MainForm.LogError("frmSubFundParent, SaveNewSubfund()", LOG_LEVELS.Error, ex.Message, "Error saving New Subfund.", ex.StackTrace, True)
        End Try

        GetInitialData()

        ' Reload main Subfund table and cause update event.

        If (newTblSubfundRow IsNot Nothing) AndAlso (newTblSubfundRow.IsAuditIDNull = False) AndAlso (newTblSubfundRow.AuditID > 0) Then
          Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID, newTblSubfundRow.AuditID.ToString), True)
        Else
          Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID), True)
        End If
      End If


    Catch ex As Exception

    End Try
  End Sub

    ''' <summary>
    ''' Handles the KeyDown event of the ToolStripText_Rename control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
  Private Sub ToolStripText_Rename_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ToolStripText_Rename.KeyDown
    Try
      If SubFundRename_Focus Then
        If e.KeyCode = Keys.Enter Then
          Rename_SubFund(sender)
        End If
      End If
    Catch ex As Exception
    End Try
  End Sub


    ''' <summary>
    ''' Handles the LostFocus event of the ToolStripText_Rename control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub ToolStripText_Rename_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles ToolStripText_Rename.LostFocus
    Try
      If SubFundRename_Focus Then
        Rename_SubFund(sender)
      End If
    Catch ex As Exception

    End Try
  End Sub

    ''' <summary>
    ''' Handles the TextChanged event of the ToolStripText_Rename control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub ToolStripText_Rename_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ToolStripText_Rename.TextChanged
    Try
      SubFundRename_Focus = True
    Catch ex As Exception
    End Try
  End Sub

    ''' <summary>
    ''' Handles the LostFocus event of the ToolStripCombo_ReferenceBasket control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub ToolStripCombo_ReferenceBasket_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles ToolStripCombo_ReferenceBasket.LostFocus
    Try
      If SubFundBasket_Focus Then
        SetBasket_SubFund(sender)
      End If
    Catch ex As Exception
    End Try

  End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the ToolStripCombo_ReferenceBasket control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub ToolStripCombo_ReferenceBasket_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripCombo_ReferenceBasket.SelectedIndexChanged
    Try
      SubFundBasket_Focus = True
    Catch ex As Exception
    End Try
  End Sub

    ''' <summary>
    ''' Rename_s the sub fund.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function Rename_SubFund(ByVal sender As Object) As Boolean
    ' **********************************************************************************
    '
    '
    ' **********************************************************************************

    Try
      If (SubFundRename_Focus) And (CurrentNode IsNot Nothing) Then
        SubFundRename_Focus = False

        If MessageBox.Show("Rename SubFund" & vbCrLf & "`" & CurrentNode.Text & "`" & vbCrLf & "to" & vbCrLf & "`" & ToolStripText_Rename.Text & "`", "Rename this SubFund ?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button3) = Windows.Forms.DialogResult.Yes Then

          Dim SubFundID As Integer = CType(CurrentNode.Tag, ID_Struct).SubFundID
          Dim thisDataRow As RenaissanceDataClass.DSSubFund.tblSubFundRow

          thisDataRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblSubFund, SubFundID)

          If (thisDataRow IsNot Nothing) Then
            CurrentNode.Text = ToolStripText_Rename.Text

            Try
              SyncLock myDataset

                thisDataRow.SubFundName = ToolStripText_Rename.Text

                myAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
                myAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

                MainForm.AdaptorUpdate(Me.Name, myAdaptor, New DataRow() {thisDataRow})

              End SyncLock

            Catch ex As Exception
              MainForm.LogError("frmSubFundParent, SaveNewSubfund()", LOG_LEVELS.Error, ex.Message, "Error saving New Subfund.", ex.StackTrace, True)
            End Try

            If (thisDataRow IsNot Nothing) AndAlso (thisDataRow.IsAuditIDNull = False) AndAlso (thisDataRow.AuditID > 0) Then
              Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID, thisDataRow.AuditID.ToString), True)
            Else
              Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID), True)
            End If

          End If

        End If

      End If

    Catch ex As Exception
      MainForm.LogError("frmSubFundParent, SaveNewSubfund()", LOG_LEVELS.Error, ex.Message, "Error saving New Subfund.", ex.StackTrace, True)
    End Try

  End Function

    ''' <summary>
    ''' Sets the basket_ sub fund.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function SetBasket_SubFund(ByVal sender As Object) As Boolean
    ' **********************************************************************************
    '
    '
    ' **********************************************************************************

    Try
      If (SubFundRename_Focus) And (CurrentNode IsNot Nothing) Then
        SubFundRename_Focus = False

        If MessageBox.Show("Set SubFund Reference basket" & vbCrLf & "to" & vbCrLf & "`" & ToolStripCombo_ReferenceBasket.Text & "`", "Set SubFund Reference Basket ?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button3) = Windows.Forms.DialogResult.Yes Then

          Dim SubFundID As Integer = CType(CurrentNode.Tag, ID_Struct).SubFundID
          Dim thisDataRow As RenaissanceDataClass.DSSubFund.tblSubFundRow

          thisDataRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblSubFund, SubFundID)

          If (thisDataRow IsNot Nothing) Then

            Try
              SyncLock myDataset
                CType(CurrentNode.Tag, ID_Struct).ReferenceBasketID = CType(ToolStripCombo_ReferenceBasket.SelectedItem, ComboBoxItemClass).VM

                thisDataRow.ReferenceBasket = CType(ToolStripCombo_ReferenceBasket.SelectedItem, ComboBoxItemClass).VM

                myAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
                myAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

                MainForm.AdaptorUpdate(Me.Name, myAdaptor, New DataRow() {thisDataRow})

              End SyncLock

            Catch ex As Exception
              MainForm.LogError("frmSubFundParent, SaveNewSubfund()", LOG_LEVELS.Error, ex.Message, "Error saving New Subfund.", ex.StackTrace, True)
            End Try

            If (thisDataRow IsNot Nothing) AndAlso (thisDataRow.IsAuditIDNull = False) AndAlso (thisDataRow.AuditID > 0) Then
              Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID, thisDataRow.AuditID.ToString), True)
            Else
              Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID), True)
            End If

          End If

        End If

      End If

    Catch ex As Exception
      MainForm.LogError("frmSubFundParent, SaveNewSubfund()", LOG_LEVELS.Error, ex.Message, "Error saving New Subfund.", ex.StackTrace, True)
    End Try

  End Function

    ''' <summary>
    ''' Handles the MouseClick event of the TreeView_SubFunds control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.MouseEventArgs"/> instance containing the event data.</param>
  Private Sub TreeView_SubFunds_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TreeView_SubFunds.MouseClick

    Call TreeView_SubFunds_MouseMove(sender, e)

  End Sub
End Class
