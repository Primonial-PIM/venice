﻿' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmLoadPacVL.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals
Imports C1.C1Excel
Imports C1.Win.C1FlexGrid


''' <summary>
''' Class frmLoadPacVL
''' </summary>
Public Class frmLoadPacVL

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm


#Region " Windows Form Designer generated code "

  ''' <summary>
  ''' Prevents a default instance of the <see cref="frmLoadPacVL"/> class from being created.
  ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  ''' <summary>
  ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
  ''' </summary>
  ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  ''' <summary>
  ''' The components
  ''' </summary>
  ''' 

  Private components As System.ComponentModel.IContainer
  Friend WithEvents _XL_Book As C1.C1Excel.C1XLBook
  Private WithEvents _btnLoad As System.Windows.Forms.Button
  Friend WithEvents _VL_TabControl As System.Windows.Forms.TabControl
  Friend WithEvents Combo_TransactionFund As System.Windows.Forms.ComboBox
  Friend WithEvents label_CptyIsFund As System.Windows.Forms.Label
  Private WithEvents Button_SaveFundData As System.Windows.Forms.Button
  Friend WithEvents Date_NAVDate As System.Windows.Forms.DateTimePicker
  Friend WithEvents Label1 As System.Windows.Forms.Label

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.

  ''' <summary>
  ''' The root menu
  ''' </summary>
  Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
  ''' <summary>
  ''' Initializes the component.
  ''' </summary>
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.RootMenu = New System.Windows.Forms.MenuStrip
    Me._XL_Book = New C1.C1Excel.C1XLBook
    Me._btnLoad = New System.Windows.Forms.Button
    Me._VL_TabControl = New System.Windows.Forms.TabControl
    Me.Combo_TransactionFund = New System.Windows.Forms.ComboBox
    Me.label_CptyIsFund = New System.Windows.Forms.Label
    Me.Button_SaveFundData = New System.Windows.Forms.Button
    Me.Date_NAVDate = New System.Windows.Forms.DateTimePicker
    Me.Label1 = New System.Windows.Forms.Label
    Me.SuspendLayout()
    '
    'RootMenu
    '
    Me.RootMenu.Location = New System.Drawing.Point(0, 0)
    Me.RootMenu.Name = "RootMenu"
    Me.RootMenu.Size = New System.Drawing.Size(998, 24)
    Me.RootMenu.TabIndex = 14
    Me.RootMenu.Text = " "
    '
    '_XL_Book
    '
    Me._XL_Book.CompatibilityMode = C1.C1Excel.CompatibilityMode.NoLimits
    '
    '_btnLoad
    '
    Me._btnLoad.Location = New System.Drawing.Point(12, 27)
    Me._btnLoad.Name = "_btnLoad"
    Me._btnLoad.Size = New System.Drawing.Size(147, 24)
    Me._btnLoad.TabIndex = 15
    Me._btnLoad.Text = "Open VL File..."
    '
    '_VL_TabControl
    '
    Me._VL_TabControl.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me._VL_TabControl.Location = New System.Drawing.Point(366, 27)
    Me._VL_TabControl.Name = "_VL_TabControl"
    Me._VL_TabControl.SelectedIndex = 0
    Me._VL_TabControl.Size = New System.Drawing.Size(632, 627)
    Me._VL_TabControl.TabIndex = 16
    '
    'Combo_TransactionFund
    '
    Me.Combo_TransactionFund.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_TransactionFund.Location = New System.Drawing.Point(60, 66)
    Me.Combo_TransactionFund.Name = "Combo_TransactionFund"
    Me.Combo_TransactionFund.Size = New System.Drawing.Size(291, 21)
    Me.Combo_TransactionFund.TabIndex = 81
    '
    'label_CptyIsFund
    '
    Me.label_CptyIsFund.Location = New System.Drawing.Point(9, 71)
    Me.label_CptyIsFund.Name = "label_CptyIsFund"
    Me.label_CptyIsFund.Size = New System.Drawing.Size(45, 16)
    Me.label_CptyIsFund.TabIndex = 82
    Me.label_CptyIsFund.Text = "Fund"
    '
    'Button_SaveFundData
    '
    Me.Button_SaveFundData.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Button_SaveFundData.Location = New System.Drawing.Point(12, 599)
    Me.Button_SaveFundData.Name = "Button_SaveFundData"
    Me.Button_SaveFundData.Size = New System.Drawing.Size(230, 47)
    Me.Button_SaveFundData.TabIndex = 83
    Me.Button_SaveFundData.Text = "Save Fund Data ..."
    '
    'Date_NAVDate
    '
    Me.Date_NAVDate.Location = New System.Drawing.Point(60, 93)
    Me.Date_NAVDate.Name = "Date_NAVDate"
    Me.Date_NAVDate.Size = New System.Drawing.Size(291, 20)
    Me.Date_NAVDate.TabIndex = 84
    '
    'Label1
    '
    Me.Label1.Location = New System.Drawing.Point(9, 99)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(45, 16)
    Me.Label1.TabIndex = 85
    Me.Label1.Text = "Date"
    '
    'frmLoadPacVL
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(998, 658)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.Date_NAVDate)
    Me.Controls.Add(Me.Button_SaveFundData)
    Me.Controls.Add(Me.Combo_TransactionFund)
    Me.Controls.Add(Me.label_CptyIsFund)
    Me.Controls.Add(Me._VL_TabControl)
    Me.Controls.Add(Me._btnLoad)
    Me.Controls.Add(Me.RootMenu)
    Me.Name = "frmLoadPacVL"
    Me.Text = "Load Pac VL"
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "

  Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
    Get

    End Get
    Set(ByVal value As Boolean)

    End Set
  End Property

  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
  ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
  ''' </summary>
  Private WithEvents _MainForm As VeniceMain

  ' Form ToolTip
  ''' <summary>
  ''' Control to provide tooltip functionality.
  ''' </summary>
  Private FormTooltip As New ToolTip()

  ' Form Menu


  ' Form Constants, specific to the table being updated.

  ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
  ''' </summary>
  Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

  ' The standard ChangeID for this form. e.g. tblCurrency
  ''' <summary>
  ''' The standard ChangeID for this form.
  ''' </summary>
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

  ' Form Specific Order fields
  ''' <summary>
  ''' The THI s_ FOR m_ select by
  ''' </summary>
  Private THIS_FORM_SelectBy As String
  ''' <summary>
  ''' The THI s_ FOR m_ order by
  ''' </summary>
  Private THIS_FORM_OrderBy As String

  ''' <summary>
  ''' The THI s_ FOR m_ value member
  ''' </summary>
  Private THIS_FORM_ValueMember As String

  ' Form specific Permissioning variables
  ''' <summary>
  ''' The THI s_ FOR m_ permission area
  ''' </summary>
  Private THIS_FORM_PermissionArea As String
  ''' <summary>
  ''' The THI s_ FOR m_ permission type
  ''' </summary>
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType = PermissionFeatureType.TypeForm

  ' Form specific Form type 
  ''' <summary>
  ''' The THI s_ FOR m_ form ID
  ''' </summary>
  Private THIS_FORM_FormID As VeniceFormID


  ' Form Status Flags

  ''' <summary>
  ''' The form is valid
  ''' </summary>
  Private FormIsValid As Boolean
  ''' <summary>
  ''' The _ form open failed
  ''' </summary>
  Private _FormOpenFailed As Boolean
  ''' <summary>
  ''' The in paint
  ''' </summary>
  Private InPaint As Boolean
  ''' <summary>
  ''' The _ in use
  ''' </summary>
  Private _InUse As Boolean

  ' User Permission Flags

  ''' <summary>
  ''' The has read permission
  ''' </summary>
  Private HasReadPermission As Boolean
  ''' <summary>
  ''' The has update permission
  ''' </summary>
  Private HasUpdatePermission As Boolean
  ''' <summary>
  ''' The has insert permission
  ''' </summary>
  Private HasInsertPermission As Boolean
  ''' <summary>
  ''' The has delete permission
  ''' </summary>
  Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

  ''' <summary>
  ''' Gets the main form.
  ''' </summary>
  ''' <value>The main form.</value>
  Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
    ' Public property to return handle to the 'Main' Venice form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

  ''' <summary>
  ''' Gets a value indicating whether this instance is in paint.
  ''' </summary>
  ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
  Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
    Get
      Return InPaint
    End Get
  End Property

  ''' <summary>
  ''' Gets a value indicating whether [in use].
  ''' </summary>
  ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
    Get
      Return _InUse
    End Get
  End Property

  ''' <summary>
  ''' Gets a value indicating whether [form open failed].
  ''' </summary>
  ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

#End Region

  ''' <summary>
  ''' Initializes a new instance of the <see cref="frmLoadPacVL"/> class.
  ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  Public Sub New(ByVal pMainForm As VeniceMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()

    _MainForm = pMainForm
    AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

    _FormOpenFailed = False
    _InUse = True

    ' ******************************************************
    ' Form Specific Settings :
    ' ******************************************************


    'MainForm.BuildStandardFormMenu(Me.RootMenu, myDataset.Tables(0), AddressOf SelectMenuEvent, AddressOf OrderMenuEvent, AddressOf AuditReportMenuEvent)

  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
  ''' <summary>
  ''' Resets the form.
  ''' </summary>
  Public Sub ResetForm() Implements StandardVeniceForm.ResetForm
    ' ********************************************************************************
    '
    '
    ' ********************************************************************************

    THIS_FORM_SelectBy = "CurrencyCode"
    THIS_FORM_OrderBy = "CurrencyCode"

    Call Form_Load(Me, New System.EventArgs)
  End Sub

  ''' <summary>
  ''' Closes the form.
  ''' </summary>
  Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
    ' ********************************************************************************
    '
    '
    ' ********************************************************************************

    ALWAYS_CLOSE_THIS_FORM = True
    Me.Close()
  End Sub

  ''' <summary>
  ''' Handles the Load event of the Form control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    ' ********************************************************************************
    '
    '
    ' ********************************************************************************

    Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
    _FormOpenFailed = False
    _InUse = True

    Try

      ' Initialise Data structures. Connection, Adaptor and Dataset.

      If Not (MainForm Is Nothing) Then
        FormIsValid = True
      Else
        MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        FormIsValid = False
        _FormOpenFailed = True
        Exit Sub
      End If

    Catch ex As Exception
    End Try

    Try

      ' Initialse form

      InPaint = True

      THIS_FORM_PermissionArea = Me.Name

      ' Check User permissions
      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

        FormIsValid = False
        _FormOpenFailed = True
        Exit Sub
      End If

      Call SetFundCombo()










    Catch ex As Exception

      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Initialising Form (Form.Load).", ex.StackTrace, True)

    Finally

      InPaint = False

    End Try

  End Sub

  ''' <summary>
  ''' Handles the Closing event of the frmLoadPacVL control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
  Private Sub frmLoadPacVL_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    ' ********************************************************************************
    '
    '
    ' ********************************************************************************

    Dim HideForm As Boolean

    ' Hide or Close this form ?
    ' All depends on how many of this form type are Open or in Cache...

    _InUse = False

    If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
      HideForm = False
    Else
      HideForm = True
      If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
        HideForm = False
      End If
    End If

    If HideForm = True Then
      MainForm.HideInFormsCollection(Me)
      Me.Hide() ' NPP Fix

      e.Cancel = True
    Else
      Try
        MainForm.RemoveFromFormsCollection(Me)
        RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

        RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

      Catch ex As Exception
      End Try
    End If

  End Sub


#End Region

  ' Routine to handle changes / updates to tables by this and other windows.
  ' If this, or any other, form posts a change to a table, then it will invoke an update event 
  ' detailing what tables have been altered.
  ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
  ' the 'VeniceAutoUpdate' event of the main Venice form.
  ' Each form may them react as appropriate to changes in any table that might impact it.
  '
  ''' <summary>
  ''' Autoes the update.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    Dim OrgInPaint As Boolean
    Dim KnowledgeDateChanged As Boolean
    Dim RefreshForm As Boolean = False

    If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

    OrgInPaint = InPaint
    InPaint = True
    KnowledgeDateChanged = False

    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
      KnowledgeDateChanged = True
      RefreshForm = True
    End If

    ' ****************************************************************
    ' Check for changes relevant to this form
    ' ****************************************************************

    ' Changes to the tblFund table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFund) = True) Or KnowledgeDateChanged Then
      Try
        ' Re-Set combo.
        Call SetFundCombo()
      Catch ex As Exception
        MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblFund", ex.StackTrace, True)
      End Try
    End If

    ' Changes to the KnowledgeDate :-
    If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then

    End If

    ' Changes to the tblUserPermissions table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

      ' Check ongoing permissions.

      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

        FormIsValid = False
        Me.Close()
        Exit Sub
      End If

    End If


    ' ****************************************************************
    ' Changes to the Main FORM table :-
    ' ****************************************************************

    If (e.TableChanged(THIS_FORM_ChangeID) = True) Or KnowledgeDateChanged Then
      RefreshForm = True

    End If

    ' ****************************************************************
    ' Repaint if not currently in Edit Mode
    '
    ' ****************************************************************

    InPaint = OrgInPaint

    ' Retrieve form data if appropriate, i.e. this data item is not already being edited.
    If (RefreshForm) Then

    Else

    End If

  End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

  ' Check User permissions
  ''' <summary>
  ''' Checks the permissions.
  ''' </summary>
  Private Sub CheckPermissions()

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

  End Sub

#End Region


  Private Sub _btnLoad_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _btnLoad.Click

    Try

      ' choose file
      Dim dlg As New OpenFileDialog()
      dlg.DefaultExt = "xls"
      dlg.FileName = "*.xls"
      dlg.InitialDirectory = "\\srbprodapp12\SR\Maj_SR\process"

      If dlg.ShowDialog() <> DialogResult.OK Then
        Return
      End If

      ' clear everything
      _XL_Book.Clear()
      _VL_TabControl.TabPages.Clear()

      ' load book
      _XL_Book.Load(dlg.FileName)


      ' create one grid per sheet and add them to listbox
      Dim sheet As XLSheet
      For Each sheet In _XL_Book.Sheets

        ' create a new grid for this sheet
        Dim flex As New C1FlexGrid()

        flex.BorderStyle = Util.BaseControls.BorderStyleEnum.None
        flex.AllowMerging = AllowMergingEnum.Spill
        flex.AllowResizing = AllowResizingEnum.Both
        flex.AutoClipboard = True
        flex.Dock = DockStyle.Fill

        ' load sheet into new grid
        LoadSheet(flex, sheet, False)

        ' add new grid to the list
        Dim pg As New TabPage()
        pg.Text = sheet.Name
        flex.Name = sheet.Name
        pg.Controls.Add(flex)
        _VL_TabControl.TabPages.Add(pg)

      Next

      ' select current sheet
      _VL_TabControl.SelectedIndex = _XL_Book.Sheets.SelectedIndex

      ' Now try to lift useful information.

      '1) Get Fund ID

      If (_XL_Book.Sheets.Contains("Portefeuille")) Then
        sheet = _XL_Book.Sheets("Portefeuille")
      Else
        sheet = Nothing
      End If

      ' Paris or Lux Format ?

      Dim CellValue As C1.C1Excel.XLCell
      Dim CellContents As String
      Dim AdminFundID As String = "<Missing>"
      Dim FundID As Integer

      CellValue = sheet.GetCell(2, 0)

      CellContents = CellValue.Value.ToString

      If ((CellContents.IndexOf("-"c) >= 0) AndAlso IsNumeric(Trim(CellContents.Substring(0, CellContents.IndexOf("-"c) - 1)))) Then
        ' Paris Format

        AdminFundID = Trim(CellContents.Substring(0, CellContents.IndexOf("-"c) - 1))

      ElseIf CellContents.Trim.StartsWith("Fonds :") AndAlso (CellContents.IndexOf("("c) > 0) AndAlso (CellContents.IndexOf(")"c) > CellContents.IndexOf("("c)) Then

        AdminFundID = Trim(CellContents.Substring(CellContents.IndexOf("("c) + 1, (CellContents.IndexOf(")"c) - CellContents.IndexOf("("c)) - 1))

      End If

      FundID = CInt(Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblFund, 0, "FundID", "RN", "FundAdministratorCode='" & AdminFundID & "'"), 0))

      If FundID > 0 Then
        Combo_TransactionFund.SelectedValue = FundID
      Else
        Combo_TransactionFund.SelectedIndex = 0
      End If

      ' 

      CellValue = sheet.GetCell(3, 1)

      Date_NAVDate.Value = CDate(CellValue.Value)

    Catch ex As Exception

    End Try
  End Sub

  Dim _styles As Hashtable

  Private Sub LoadSheet(ByVal flex As C1FlexGrid, ByVal sheet As XLSheet, ByVal fixedCells As Boolean)

    ' account for fixed cells
    Dim frows As Integer = flex.Rows.Fixed
    Dim fcols As Integer = flex.Cols.Fixed

    ' copy dimensions
    flex.Rows.Count = sheet.Rows.Count + frows
    flex.Cols.Count = sheet.Columns.Count + fcols

    ' initialize fixed cells
    Dim r As Integer, c As Integer
    If fixedCells AndAlso frows > 0 AndAlso fcols > 0 Then

      flex.Styles.Fixed.TextAlign = TextAlignEnum.CenterCenter
      For r = 1 To flex.Rows.Count - 1
        flex(r, 0) = r
      Next
      For c = 1 To flex.Cols.Count - 1
        flex(0, c) = String.Format("{0}", Chr(Asc("A") + c - 1))
      Next
    End If

    ' set default properties
    flex.Font = sheet.Book.DefaultFont
    flex.Rows.DefaultSize = C1XLBook.TwipsToPixels(sheet.DefaultRowHeight)
    flex.Cols.DefaultSize = C1XLBook.TwipsToPixels(sheet.DefaultColumnWidth)

    ' prepare to convert styles
    _styles = New Hashtable()

    ' set row/column properties
    For r = 0 To sheet.Rows.Count - 1

      ' size/visibility
      Dim fr As Row = flex.Rows(r + frows)
      Dim xr As XLRow = sheet.Rows(r)
      If (xr.Height >= 0) Then
        fr.Height = C1XLBook.TwipsToPixels(xr.Height) * 1.2#
        fr.Visible = xr.Visible

        ' style
        Dim cs As CellStyle = StyleFromExcel(flex, xr.Style)
        If Not (cs Is Nothing) Then
          fr.Style = cs
        End If
      End If
    Next
    For c = 0 To sheet.Columns.Count - 1

      ' size/visibility
      Dim fc As Column = flex.Cols(c + fcols)
      Dim xc As XLColumn = sheet.Columns(c)
      If (xc.Width >= 0) Then
        fc.Width = C1XLBook.TwipsToPixels(xc.Width)
        fc.Visible = xc.Visible

        ' style
        Dim cs As CellStyle = StyleFromExcel(flex, xc.Style)
        If Not (cs Is Nothing) Then
          fc.Style = cs
        End If
      End If
    Next

    ' load cells
    For r = 0 To sheet.Rows.Count - 1
      For c = 0 To sheet.Columns.Count - 1

        ' get cell
        Dim cell As XLCell = sheet.GetCell(r, c)
        If (cell Is Nothing) Then
          Continue For
        End If

        ' apply content
        flex(r + frows, c + fcols) = cell.Value

        ' apply style
        Dim cs As CellStyle = StyleFromExcel(flex, cell.Style)
        If Not (cs Is Nothing) Then
          flex.SetCellStyle(r + frows, c + fcols, cs)
        End If
      Next
    Next
  End Sub

  Private Function StyleFromExcel(ByVal flex As C1FlexGrid, ByVal style As XLStyle) As CellStyle

    ' sanity
    If (style Is Nothing) Then
      Return Nothing
    End If

    ' look it up on list
    If (_styles.Contains(style)) Then
      Return _styles(style)
    End If

    ' create new flex style
    Dim cs As CellStyle = flex.Styles.Add(_styles.Count.ToString())

    ' set up new style
    If Not (style.Font Is Nothing) Then cs.Font = style.Font
    If (style.ForeColor <> Color.Transparent) Then cs.ForeColor = style.ForeColor
    If (style.BackColor <> Color.Transparent) Then cs.BackColor = style.BackColor
    If (style.Rotation = 90) Then cs.TextDirection = TextDirectionEnum.Up
    If (style.Rotation = 180) Then cs.TextDirection = TextDirectionEnum.Down

    If Not (style.Format Is Nothing OrElse style.Format.Length = 0) Then

      cs.Format = XLStyle.FormatXLToDotNet(style.Format)
      Select Case (style.AlignHorz)

        Case XLAlignHorzEnum.Center
          cs.WordWrap = style.WordWrap
          Select Case (style.AlignVert)
            Case XLAlignVertEnum.Top
              cs.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterTop
            Case XLAlignVertEnum.Center
              cs.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterCenter
            Case Else
              cs.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.CenterBottom
          End Select

        Case XLAlignHorzEnum.Right
          cs.WordWrap = style.WordWrap
          Select Case (style.AlignVert)

            Case XLAlignVertEnum.Top
              cs.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.RightTop
            Case XLAlignVertEnum.Center
              cs.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.RightCenter
            Case Else
              cs.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.RightBottom
          End Select

        Case XLAlignHorzEnum.Left
          cs.WordWrap = style.WordWrap
          Select Case (style.AlignVert)
            Case XLAlignVertEnum.Top
              cs.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftTop
            Case XLAlignVertEnum.Center
              cs.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftCenter
            Case Else
              cs.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftBottom
          End Select

        Case Else
          cs.WordWrap = style.WordWrap
          Select Case (style.AlignVert)
            Case XLAlignVertEnum.Top
              cs.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.GeneralTop
            Case XLAlignVertEnum.Center
              cs.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.GeneralCenter
            Case Else
              cs.TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.GeneralBottom
          End Select
      End Select
    End If

    ' save it
    _styles.Add(style, cs)

    ' return it
    Return cs
  End Function

  Private Sub Button_SaveFundData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_SaveFundData.Click
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Try
      Dim CaptureID As Integer = 9999 'GetCaptureID()
      Dim FundCode As String = CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblFund, Combo_TransactionFund.SelectedValue, "FundAdministratorCode"))

      Button_SaveFundData.Enabled = False
      _btnLoad.Enabled = False

      ' OK, One by one....

      If Not (HasInsertPermission Or HasUpdatePermission) Then
        Exit Sub
      End If

      If (_XL_Book Is Nothing) Then Exit Sub

      ' First get a new Capture ID

      Try
        Dim InstrumentCommand As New SqlCommand()

        Try
          InstrumentCommand.CommandText = "[neocapture].[dbo].[adp_capture_InsertCommand]"
          InstrumentCommand.CommandType = CommandType.StoredProcedure
          InstrumentCommand.Connection = MainForm.GetVeniceConnection()

          InstrumentCommand.Parameters.Add("@SetID", SqlDbType.Int).Value = (-1)
          InstrumentCommand.Parameters.Add("@Result", SqlDbType.NVarChar, 255).Value = FundCode
          InstrumentCommand.Parameters.Add("@neo_DateEntered", SqlDbType.NVarChar, 50).Value = Now.ToString(QUERY_LONGDATEFORMAT)
          InstrumentCommand.Parameters.Add("@priority", SqlDbType.Int).Value = 0
          InstrumentCommand.Parameters.Add("@filename", SqlDbType.NVarChar, 50).Value = "Venice - Load pac VL"

          CaptureID = InstrumentCommand.ExecuteScalar()
        Catch ex As Exception
        Finally
          If (InstrumentCommand IsNot Nothing) AndAlso (InstrumentCommand.Connection IsNot Nothing) Then
            InstrumentCommand.Connection.Close()
            InstrumentCommand.Connection = Nothing
          End If
        End Try

      Catch ex As Exception
      End Try

      ' Lets start with the portfolio....

      Dim sheet As XLSheet
      Dim CellValue As C1.C1Excel.XLCell
      Dim RowCounter As Integer
      Dim ColCounter As Integer
      Dim FundCurrencyID As Integer = CInt(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblFund, Combo_TransactionFund.SelectedValue, "FundBaseCurrency"))
      Dim FundCurrency As String = CStr(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblCurrency, FundCurrencyID, "CurrencyCode"))


      If (FundCurrencyID <= 0) OrElse (FundCode.Length = 0) Then
        Exit Sub
      End If

      ' Portfolio :

      Try
        Dim Row_Portfolio As Integer = (-1)
        Dim Row_Portfolio_Headings As Integer = (-1)

        Dim col_InstrumentType As Integer = (-1)
        Dim col_ISIN As Integer = (-1)
        Dim col_ISIN_Futures As Integer = (-1)
        Dim col_Name As Integer = (-1)
        Dim col_Country As Integer = (-1)
        Dim col_Currency As Integer = (-1)
        Dim col_Weight As Integer = (-1)
        Dim col_Quantity As Integer = (-1)
        Dim col_Price As Integer = (-1)
        Dim col_PriceDate As Integer = (-1)
        Dim col_Value_Fund As Integer = (-1)
        Dim col_MaturityDate As Integer = (-1)
        Dim col_AccruedInterest As Integer = (-1)
        Dim col_UnrealisedProfit As Integer = (-1)

        Dim ThisPrice As Double
        Dim ThisQuantity As Double

        If (_XL_Book.Sheets.Contains("Portefeuille")) Then
          sheet = _XL_Book.Sheets("Portefeuille")

          ' Find Portfolio Section : 

          For RowCounter = 0 To (sheet.Rows.Count - 1)
            If (sheet.GetCell(RowCounter, 0) IsNot Nothing) AndAlso (sheet.GetCell(RowCounter, 0).Value.ToString = "PORTEFEUILLE") Then
              Row_Portfolio = RowCounter
              Row_Portfolio_Headings = RowCounter + 1
              Exit For
            End If
          Next

          ' Get Column Indices
          If (Row_Portfolio_Headings > 0) Then

            For ColCounter = 0 To (sheet.Columns.Count - 1)

              CellValue = sheet.GetCell(Row_Portfolio_Headings, ColCounter)

              If (CellValue Is Nothing) OrElse (CellValue.Value Is Nothing) Then
                Continue For
              End If

              Select Case Trim(CellValue.Value.ToString).ToUpper

                Case ""

                Case "INSTRUMENT"
                  col_InstrumentType = ColCounter

                Case "CODE VALEUR SJ"
                  col_ISIN = ColCounter

                Case "CODE VALEUR"
                  col_ISIN_Futures = ColCounter

                Case "LIBELLÉ VALEUR"
                  col_Name = ColCounter

                Case "PAYS"
                  col_Country = ColCounter

                Case "DATE TERME"
                  col_MaturityDate = ColCounter

                Case "DEV"
                  col_Currency = ColCounter

                Case "QUANTITÉ"
                  col_Quantity = ColCounter

                Case "VALEUR BOURSIÈRE"
                  col_Value_Fund = ColCounter

                Case "COURS"
                  col_Price = ColCounter

                Case "DATE COURS", "DATE CRS"
                  col_PriceDate = ColCounter

                Case "% AN"
                  col_Weight = ColCounter

                Case "COUPON COURU" ' Accrued Interest
                  col_AccruedInterest = ColCounter

                Case "PMV LATENTES" ' Unrealised P&L
                  col_UnrealisedProfit = ColCounter


              End Select
            Next
          End If

          ' Found Cols ?
          If col_ISIN >= 0 Then

            Dim InstrumentCommand As New SqlCommand()
            ThisPrice = 0.0#
            ThisQuantity = 0.0#

            InstrumentCommand.CommandText = "[neocapture].[dbo].[adp_rec_inventory_InsertCommand]"
            InstrumentCommand.CommandType = CommandType.StoredProcedure
            InstrumentCommand.Connection = MainForm.GetVeniceConnection()
            InstrumentCommand.Parameters.Add("@IDENTITY", SqlDbType.Int).Direction = ParameterDirection.Output
            InstrumentCommand.Parameters.Add("@captureID", SqlDbType.Int).Value = CaptureID
            InstrumentCommand.Parameters.Add("@NAVDate", SqlDbType.NVarChar, 50).Value = Date_NAVDate.Value.Date.ToString("dd/MM/yyyy")
            InstrumentCommand.Parameters.Add("@fundcode", SqlDbType.NVarChar, 50).Value = FundCode
            InstrumentCommand.Parameters.Add("@neo_checkall", SqlDbType.NVarChar, 50).Value = ""
            InstrumentCommand.Parameters.Add("@neo_instrumenttype", SqlDbType.NVarChar, 50).Value = ""
            InstrumentCommand.Parameters.Add("@neo_sessionstatus", SqlDbType.NVarChar, 50).Value = ""
            InstrumentCommand.Parameters.Add("@neo_isin", SqlDbType.NVarChar, 50).Value = ""
            InstrumentCommand.Parameters.Add("@neo_description", SqlDbType.NVarChar, 255).Value = ""
            InstrumentCommand.Parameters.Add("@neo_quantity", SqlDbType.NVarChar, 50).Value = "0"
            InstrumentCommand.Parameters.Add("@neo_type", SqlDbType.NVarChar, 50).Value = ""
            InstrumentCommand.Parameters.Add("@neo_pricedate", SqlDbType.NVarChar, 50).Value = "01/01/1970"
            InstrumentCommand.Parameters.Add("@neo_price", SqlDbType.NVarChar, 50).Value = "0"
            InstrumentCommand.Parameters.Add("@neo_priceflag", SqlDbType.NVarChar, 50).Value = ""
            InstrumentCommand.Parameters.Add("@neo_quotecurrency", SqlDbType.NVarChar, 5).Value = ""
            InstrumentCommand.Parameters.Add("@neo_marketvalue_qc", SqlDbType.NVarChar, 50).Value = "0"
            InstrumentCommand.Parameters.Add("@neo_unitprice_qc", SqlDbType.NVarChar, 50).Value = "0"
            InstrumentCommand.Parameters.Add("@neo_exchangerate", SqlDbType.NVarChar, 50).Value = ""
            InstrumentCommand.Parameters.Add("@neo_referencecurrency", SqlDbType.NVarChar, 5).Value = FundCurrency
            InstrumentCommand.Parameters.Add("@neo_marketvalue_rc", SqlDbType.NVarChar, 50).Value = "0"
            InstrumentCommand.Parameters.Add("@neo_accruedinterest_rc", SqlDbType.NVarChar, 50).Value = ""
            InstrumentCommand.Parameters.Add("@neo_costprice_rc", SqlDbType.NVarChar, 50).Value = ""
            InstrumentCommand.Parameters.Add("@neo_nonrealisedgainloss_rc", SqlDbType.NVarChar, 50).Value = ""
            InstrumentCommand.Parameters.Add("@neo_percentnetassets", SqlDbType.NVarChar, 50).Value = "0"
            InstrumentCommand.Parameters.Add("@neo_realisedgainloss_qc", SqlDbType.NVarChar, 50).Value = ""
            InstrumentCommand.Parameters.Add("@neo_instrumentweight", SqlDbType.NVarChar, 50).Value = ""
            InstrumentCommand.Parameters.Add("@neo_assettype", SqlDbType.NVarChar, 50).Value = ""
            InstrumentCommand.Parameters.Add("@neo_subassettype", SqlDbType.NVarChar, 50).Value = ""
            InstrumentCommand.Parameters.Add("@neo_subassettypedesc", SqlDbType.NVarChar, 50).Value = ""
            InstrumentCommand.Parameters.Add("@neo_issuercode", SqlDbType.NVarChar, 50).Value = ""
            InstrumentCommand.Parameters.Add("@neo_quotationplace", SqlDbType.NVarChar, 50).Value = ""
            InstrumentCommand.Parameters.Add("@neo_quotationplacedesc", SqlDbType.NVarChar, 50).Value = ""
            InstrumentCommand.Parameters.Add("@neo_cotationcountry", SqlDbType.NVarChar, 50).Value = ""
            InstrumentCommand.Parameters.Add("@neo_maturitydate", SqlDbType.NVarChar, 50).Value = ""
            InstrumentCommand.Parameters.Add("@neo_nominal", SqlDbType.NVarChar, 50).Value = ""
            InstrumentCommand.Parameters.Add("@neo_underlyingassettype", SqlDbType.NVarChar, 50).Value = ""
            InstrumentCommand.Parameters.Add("@neo_underlyingassettypedesc", SqlDbType.NVarChar, 50).Value = ""
            InstrumentCommand.Parameters.Add("@neo_quotite", SqlDbType.NVarChar, 50).Value = ""
            InstrumentCommand.Parameters.Add("@neo_strike", SqlDbType.NVarChar, 50).Value = ""
            InstrumentCommand.Parameters.Add("@neo_DateEntered", SqlDbType.NVarChar, 50).Value = Now.ToString("yyyy-MM-dd HH:mm:ss")

            Try

              For RowCounter = (Row_Portfolio_Headings + 1) To (sheet.Rows.Count - 1)

                ' Process Line if ISIN is not empty.
                If (sheet(RowCounter, col_ISIN).Value IsNot Nothing) AndAlso (sheet(RowCounter, col_ISIN).Value.ToString.Trim.Length > 0) Then

                  InstrumentCommand.Parameters("@neo_instrumenttype").Value = ""
                  InstrumentCommand.Parameters("@neo_isin").Value = ""
                  InstrumentCommand.Parameters("@neo_description").Value = ""
                  InstrumentCommand.Parameters("@neo_quantity").Value = "0"
                  InstrumentCommand.Parameters("@neo_pricedate").Value = "01/01/1970"
                  InstrumentCommand.Parameters("@neo_price").Value = "0"
                  InstrumentCommand.Parameters("@neo_quotecurrency").Value = ""
                  InstrumentCommand.Parameters("@neo_marketvalue_qc").Value = "0"
                  InstrumentCommand.Parameters("@neo_unitprice_qc").Value = "0"
                  InstrumentCommand.Parameters("@neo_marketvalue_rc").Value = "0"
                  InstrumentCommand.Parameters("@neo_percentnetassets").Value = "0"
                  InstrumentCommand.Parameters("@neo_cotationcountry").Value = ""
                  InstrumentCommand.Parameters("@neo_maturitydate").Value = ""
                  InstrumentCommand.Parameters("@neo_accruedinterest_rc").Value = ""
                  InstrumentCommand.Parameters("@neo_nonrealisedgainloss_rc").Value = ""
                  InstrumentCommand.Parameters("@neo_pricedate").Value = "01/01/1970"
                  InstrumentCommand.Parameters("@neo_subassettype").Value = ""

                  If (col_InstrumentType >= 0) Then
                    CellValue = sheet(RowCounter, col_InstrumentType)
                    If (CellValue IsNot Nothing) AndAlso (CellValue.Value IsNot Nothing) Then
                      InstrumentCommand.Parameters("@neo_instrumenttype").Value = IIf(CellValue.Value.ToString.Trim.Length = 0, "", CellValue.Value.ToString.Trim)

                      If (CellValue.Value.ToString.ToUpper = "FOREIGN EXCHANGE FORWARD") Then
                        InstrumentCommand.Parameters("@neo_subassettype").Value = "ZFX"
                      End If
                    End If
                  End If

                  If (col_ISIN >= 0) Then
                    CellValue = sheet(RowCounter, col_ISIN)
                    If (CellValue IsNot Nothing) AndAlso (CellValue.Value IsNot Nothing) Then
                      InstrumentCommand.Parameters("@neo_isin").Value = IIf(CellValue.Value.ToString.Trim.Length = 0, "", CellValue.Value.ToString.Trim)
                    End If
                  End If

                  If (col_Name >= 0) Then
                    CellValue = sheet(RowCounter, col_Name)
                    If (CellValue IsNot Nothing) AndAlso (CellValue.Value IsNot Nothing) Then
                      InstrumentCommand.Parameters("@neo_description").Value = IIf(CellValue.Value.ToString.Trim.Length = 0, "", CellValue.Value.ToString.Trim)
                    End If
                  End If

                  If (col_Quantity >= 0) Then
                    CellValue = sheet.GetCell(RowCounter, col_Quantity)
                    If CellValue IsNot Nothing Then
                      ThisQuantity = IIf(CellValue.Value.ToString.Trim.Length = 0, 0.0#, CDbl(CellValue.Value))
                      InstrumentCommand.Parameters("@neo_quantity").Value = ThisQuantity.ToString("###0.00000000")
                    End If
                  End If

                  If (col_PriceDate >= 0) Then
                    CellValue = sheet(RowCounter, col_PriceDate)
                    If (CellValue IsNot Nothing) AndAlso (CellValue.Value IsNot Nothing) Then
                      InstrumentCommand.Parameters("@neo_pricedate").Value = IIf(CellValue.Value.ToString.Trim.Length = 0, "", CDate(CellValue.Value).ToString("dd/MM/yyyy"))
                    End If
                  End If

                  If (col_Price >= 0) Then
                    CellValue = sheet.GetCell(RowCounter, col_Price)
                    If CellValue IsNot Nothing Then
                      ThisPrice = IIf(CellValue.Value.ToString.Trim.Length = 0, 0.0#, CDbl(CellValue.Value))
                      InstrumentCommand.Parameters("@neo_price").Value = ThisPrice.ToString("###0.00000000")
                      InstrumentCommand.Parameters("@neo_unitprice_qc").Value = ThisPrice.ToString("###0.00000000")
                    End If
                  End If

                  InstrumentCommand.Parameters("@neo_marketvalue_qc").Value = CDbl(ThisPrice * ThisQuantity).ToString("###0.00")

                  If (col_Currency >= 0) Then
                    CellValue = sheet(RowCounter, col_Currency)
                    If (CellValue IsNot Nothing) AndAlso (CellValue.Value IsNot Nothing) Then
                      InstrumentCommand.Parameters("@neo_quotecurrency").Value = IIf(CellValue.Value.ToString.Trim.Length = 0, "", CellValue.Value.ToString.Trim)
                    End If
                  End If

                  If (col_Value_Fund >= 0) Then
                    CellValue = sheet(RowCounter, col_Value_Fund)
                    If (CellValue IsNot Nothing) AndAlso (CellValue.Value IsNot Nothing) Then
                      InstrumentCommand.Parameters("@neo_marketvalue_rc").Value = IIf(CellValue.Value.ToString.Trim.Length = 0, "", CDbl(CellValue.Value).ToString("###0.00"))
                    End If
                  End If

                  If (col_Weight >= 0) Then
                    CellValue = sheet(RowCounter, col_Weight)
                    If (CellValue IsNot Nothing) AndAlso (CellValue.Value IsNot Nothing) Then
                      If (CellValue.Style.Format.Trim.EndsWith("%")) OrElse (CellValue.Style.Format.Trim.Length = 0) Then
                        InstrumentCommand.Parameters("@neo_percentnetassets").Value = IIf(CellValue.Value.ToString.Trim.Length = 0, "", CDbl(CellValue.Value * 100.0#).ToString("###0.0000"))
                      Else
                        InstrumentCommand.Parameters("@neo_percentnetassets").Value = IIf(CellValue.Value.ToString.Trim.Length = 0, "", CDbl(CellValue.Value).ToString("###0.0000"))
                      End If
                    End If
                  End If

                  If (col_Country >= 0) Then
                    CellValue = sheet(RowCounter, col_Country)
                    If (CellValue IsNot Nothing) AndAlso (CellValue.Value IsNot Nothing) Then
                      InstrumentCommand.Parameters("@neo_cotationcountry").Value = IIf(CellValue.Value.ToString.Trim.Length = 0, "", CellValue.Value.ToString.Trim)
                    End If
                  End If

                  If (col_MaturityDate >= 0) Then
                    CellValue = sheet(RowCounter, col_MaturityDate)
                    If (CellValue IsNot Nothing) AndAlso (CellValue.Value IsNot Nothing) Then
                      InstrumentCommand.Parameters("@neo_maturitydate").Value = IIf(CellValue.Value.ToString.Trim.Length = 0, "", CDate(CellValue.Value).ToString("dd/MM/yyyy"))

                      ' Alternative ISIN for these instruments

                      If (col_ISIN_Futures >= 0) Then
                        CellValue = sheet.GetCell(RowCounter, col_ISIN_Futures)
                        If (CellValue IsNot Nothing) AndAlso (CellValue.Value IsNot Nothing) AndAlso (IsNumeric(CellValue.Value) = False) Then
                          InstrumentCommand.Parameters("@neo_isin").Value = IIf(CellValue.Value.ToString.Trim.Length = 0, "", CellValue.Value.ToString.Trim)
                        End If
                      End If

                    End If
                  End If

                  If (col_AccruedInterest >= 0) Then
                    CellValue = sheet(RowCounter, col_AccruedInterest)
                    If (CellValue IsNot Nothing) AndAlso (CellValue.Value IsNot Nothing) Then
                      InstrumentCommand.Parameters("@neo_accruedinterest_rc").Value = IIf(CellValue.Value.ToString.Trim.Length = 0, "", CDbl(CellValue.Value).ToString("###0.00"))
                    End If
                  End If

                  If (col_UnrealisedProfit >= 0) Then
                    CellValue = sheet(RowCounter, col_UnrealisedProfit)
                    If (CellValue IsNot Nothing) AndAlso (CellValue.Value IsNot Nothing) Then
                      InstrumentCommand.Parameters("@neo_nonrealisedgainloss_rc").Value = IIf(CellValue.Value.ToString.Trim.Length = 0, "", CDbl(CellValue.Value).ToString("###0.00"))
                    End If
                  End If

                End If

                '

                InstrumentCommand.ExecuteNonQuery()


              Next

            Catch ex As Exception
            Finally
              If (InstrumentCommand IsNot Nothing) AndAlso (InstrumentCommand.Connection IsNot Nothing) Then
                InstrumentCommand.Connection.Close()
                InstrumentCommand.Connection = Nothing
              End If
            End Try

          End If

          sheet = Nothing

        End If

      Catch ex As Exception
      End Try ' Portfolio :

      ' Availabilities

      Try

        If (_XL_Book.Sheets.Contains("Disponibilités")) OrElse (_XL_Book.Sheets.Contains("Disponibilites")) Then

          Dim Row_Cash As Integer = (-1)
          Dim Row_Cash_Headings As Integer = (-1)

          Dim Col_Description As Integer = (-1)
          Dim Col_Currency As Integer = (-1)
          Dim Col_Balance As Integer = (-1)
          Dim Col_Balance_Current As Integer = (-1)
          Dim Col_Previous As Integer = (-1)
          Dim Col_Previous_Current As Integer = (-1)
          Dim Col_Balance_Variation As Integer = (-1)
          Dim Col_Balance_Variation_Percent As Integer = (-1)
          Dim Col_Percent_Assets As Integer = (-1)
          Dim Col_Percent_Cash As Integer = (-1)
          Dim thisBalance As Double = 0.0#

          If (_XL_Book.Sheets.Contains("Disponibilités")) Then
            sheet = _XL_Book.Sheets("Disponibilités")
          Else
            sheet = _XL_Book.Sheets("Disponibilites")
          End If

          ' Find Cash Section : 

          For RowCounter = 0 To (sheet.Rows.Count - 1)
            If (sheet.GetCell(RowCounter, 0) IsNot Nothing) AndAlso (sheet.GetCell(RowCounter, 0).Value.ToString = "DISPONIBILITES") Then
              Row_Cash = RowCounter
              Row_Cash_Headings = RowCounter + 1
              Exit For
            End If
          Next

          ' Get Column Indices
          If (Row_Cash_Headings > 0) Then

            For ColCounter = 0 To (sheet.Columns.Count - 1)

              CellValue = sheet(Row_Cash_Headings, ColCounter)

              If (CellValue Is Nothing) OrElse (CellValue.Value Is Nothing) Then
                Continue For
              End If

              Select Case Trim(CellValue.Value.ToString).ToUpper

                Case "RUBRIQUE", "SOUS RUBRIQUE" ' Description
                  Col_Description = ColCounter

                Case "DEVISE" ' Currency
                  Col_Currency = ColCounter

                Case "SOLDE JOUR" ' Balance
                  Col_Balance = ColCounter

                Case "CONTREVALEUR JR" ' Current Balance
                  Col_Balance_Current = ColCounter

                Case "SOLDE PRÉC" ' Previous Balance
                  Col_Previous = ColCounter

                Case "CONTREVALEUR PR" ' Previous Current
                  Col_Previous_Current = ColCounter

                Case "VAR SOLDE" ' Balance Variation
                  Col_Balance_Variation = ColCounter

                Case "% VAR" ' Balance Variation %
                  Col_Balance_Variation_Percent = ColCounter

                Case "% AN" ' % Assets
                  Col_Percent_Assets = ColCounter

                Case "% RÉPART" ' % Availabilities
                  Col_Percent_Cash = ColCounter

              End Select

            Next ' ColCounter

          End If ' Row_Cash_Headings

          ' Found Cols ?
          If Col_Description >= 0 Then

            Dim InstrumentCommand As New SqlCommand()

            InstrumentCommand.CommandText = "[neocapture].[dbo].[adp_rec_Availabilitites_InsertCommand]"
            InstrumentCommand.CommandType = CommandType.StoredProcedure
            InstrumentCommand.Connection = MainForm.GetVeniceConnection()
            InstrumentCommand.Parameters.Add("@IDENTITY", SqlDbType.Int).Direction = ParameterDirection.Output
            InstrumentCommand.Parameters.Add("@captureID", SqlDbType.Int).Value = CaptureID
            InstrumentCommand.Parameters.Add("@NAVDate", SqlDbType.NVarChar, 50).Value = Date_NAVDate.Value.Date.ToString("dd/MM/yyyy")
            InstrumentCommand.Parameters.Add("@FundCode", SqlDbType.NVarChar, 50).Value = FundCode
            InstrumentCommand.Parameters.Add("@neo_description", SqlDbType.NVarChar, 255).Value = ""
            InstrumentCommand.Parameters.Add("@neo_percentassets", SqlDbType.NVarChar, 50).Value = ""
            InstrumentCommand.Parameters.Add("@neo_percentavailabilities", SqlDbType.NVarChar, 50).Value = ""
            InstrumentCommand.Parameters.Add("@neo_currency", SqlDbType.NVarChar, 5).Value = ""
            InstrumentCommand.Parameters.Add("@neo_balance", SqlDbType.NVarChar, 50).Value = ""
            InstrumentCommand.Parameters.Add("@neo_currentbalance", SqlDbType.NVarChar, 50).Value = ""
            InstrumentCommand.Parameters.Add("@neo_previousbalance", SqlDbType.NVarChar, 50).Value = ""
            InstrumentCommand.Parameters.Add("@neo_previouscurrentbalance", SqlDbType.NVarChar, 50).Value = ""
            InstrumentCommand.Parameters.Add("@neo_balancevariation", SqlDbType.NVarChar, 50).Value = ""
            InstrumentCommand.Parameters.Add("@neo_percentvariation", SqlDbType.NVarChar, 50).Value = ""
            InstrumentCommand.Parameters.Add("@neo_DateEntered", SqlDbType.NVarChar, 50).Value = Now.ToString("yyyy-MM-dd HH:mm:ss")

            Try

              For RowCounter = (Row_Cash_Headings + 1) To (sheet.Rows.Count - 1)

                ' Process Line if Currency is not empty.
                If (sheet(RowCounter, Col_Currency).Value IsNot Nothing) AndAlso (sheet(RowCounter, Col_Currency).Value.ToString.Trim.Length > 0) Then

                  InstrumentCommand.Parameters("@neo_description").Value = ""
                  InstrumentCommand.Parameters("@neo_percentassets").Value = ""
                  InstrumentCommand.Parameters("@neo_percentavailabilities").Value = ""
                  InstrumentCommand.Parameters("@neo_currency").Value = ""
                  InstrumentCommand.Parameters("@neo_balance").Value = ""
                  InstrumentCommand.Parameters("@neo_currentbalance").Value = ""
                  InstrumentCommand.Parameters("@neo_previousbalance").Value = ""
                  InstrumentCommand.Parameters("@neo_previouscurrentbalance").Value = ""
                  InstrumentCommand.Parameters("@neo_balancevariation").Value = ""
                  InstrumentCommand.Parameters("@neo_percentvariation").Value = ""
                  thisBalance = 0.0#

                  If (Col_Description >= 0) Then
                    CellValue = sheet(RowCounter, Col_Description)
                    If (CellValue IsNot Nothing) AndAlso (CellValue.Value IsNot Nothing) Then
                      InstrumentCommand.Parameters("@neo_description").Value = CellValue.Value.ToString.Trim
                    End If
                  End If

                  If (Col_Percent_Assets >= 0) Then
                    CellValue = sheet(RowCounter, Col_Percent_Assets)
                    If (CellValue IsNot Nothing) AndAlso (CellValue.Value IsNot Nothing) Then
                      If (CellValue.Style.Format.Trim.EndsWith("%")) OrElse (CellValue.Style.Format.Trim.Length = 0) Then
                        InstrumentCommand.Parameters("@neo_percentassets").Value = IIf(CellValue.Value.ToString.Trim.Length = 0, "", CDbl(CellValue.Value * 100.0#).ToString("###0.0000"))
                      Else
                        InstrumentCommand.Parameters("@neo_percentassets").Value = IIf(CellValue.Value.ToString.Trim.Length = 0, "", CDbl(CellValue.Value).ToString("###0.0000"))
                      End If
                    End If
                  End If

                  If (Col_Percent_Cash >= 0) Then
                    CellValue = sheet(RowCounter, Col_Percent_Cash)
                    If (CellValue IsNot Nothing) AndAlso (CellValue.Value IsNot Nothing) Then
                      If (CellValue.Style.Format.Trim.EndsWith("%")) OrElse (CellValue.Style.Format.Trim.Length = 0) Then
                        InstrumentCommand.Parameters("@neo_percentavailabilities").Value = IIf(CellValue.Value.ToString.Trim.Length = 0, "", CDbl(CellValue.Value * 100.0#).ToString("###0.0000"))
                      Else
                        InstrumentCommand.Parameters("@neo_percentavailabilities").Value = IIf(CellValue.Value.ToString.Trim.Length = 0, "", CDbl(CellValue.Value).ToString("###0.0000"))
                      End If
                    End If
                  End If

                  If (Col_Currency >= 0) Then
                    CellValue = sheet(RowCounter, Col_Currency)
                    If (CellValue IsNot Nothing) AndAlso (CellValue.Value IsNot Nothing) Then
                      InstrumentCommand.Parameters("@neo_currency").Value = CellValue.Value.ToString.Trim
                    End If
                  End If

                  If (Col_Balance >= 0) Then
                    CellValue = sheet(RowCounter, Col_Balance)
                    If (CellValue IsNot Nothing) AndAlso (CellValue.Value IsNot Nothing) Then
                      If IsNumeric(CellValue.Value) Then
                        thisBalance = CDbl(CellValue.Value)
                      Else
                        thisBalance = 0.0#
                      End If
                      InstrumentCommand.Parameters("@neo_balance").Value = thisBalance.ToString("###0.00")
                      InstrumentCommand.Parameters("@neo_currentbalance").Value = thisBalance.ToString("###0.00")
                    End If
                  End If

                  If (Col_Balance_Current >= 0) Then
                    CellValue = sheet(RowCounter, Col_Balance_Current)
                    If (CellValue IsNot Nothing) AndAlso (CellValue.Value IsNot Nothing) Then
                      If IsNumeric(CellValue.Value) Then
                        InstrumentCommand.Parameters("@neo_currentbalance").Value = CDbl(CellValue.Value).ToString("###0.00")
                      End If
                    End If
                  End If

                  If (Col_Previous >= 0) Then
                    CellValue = sheet(RowCounter, Col_Previous)
                    If (CellValue IsNot Nothing) AndAlso (CellValue.Value IsNot Nothing) Then
                      If IsNumeric(CellValue.Value) Then
                        InstrumentCommand.Parameters("@neo_previousbalance").Value = CDbl(CellValue.Value).ToString("###0.00")
                        InstrumentCommand.Parameters("@neo_previouscurrentbalance").Value = CDbl(CellValue.Value).ToString("###0.00")
                      End If
                    End If
                  End If

                  If (Col_Previous_Current >= 0) Then
                    CellValue = sheet(RowCounter, Col_Previous_Current)
                    If (CellValue IsNot Nothing) AndAlso (CellValue.Value IsNot Nothing) Then
                      If IsNumeric(CellValue.Value) Then
                        InstrumentCommand.Parameters("@neo_previouscurrentbalance").Value = CDbl(CellValue.Value).ToString("###0.00")
                      End If
                    End If
                  End If

                  If (Col_Balance_Variation >= 0) Then
                    CellValue = sheet(RowCounter, Col_Balance_Variation)
                    If (CellValue IsNot Nothing) AndAlso (CellValue.Value IsNot Nothing) Then
                      If IsNumeric(CellValue.Value) Then
                        InstrumentCommand.Parameters("@neo_balancevariation").Value = CDbl(CellValue.Value).ToString("###0.00")
                      End If
                    End If
                  End If

                  If (Col_Balance_Variation_Percent >= 0) Then
                    CellValue = sheet(RowCounter, Col_Balance_Variation_Percent)
                    If (CellValue IsNot Nothing) AndAlso (CellValue.Value IsNot Nothing) Then
                      If (CellValue.Style.Format.Trim.EndsWith("%")) OrElse (CellValue.Style.Format.Trim.Length = 0) Then
                        InstrumentCommand.Parameters("@neo_percentvariation").Value = IIf(CellValue.Value.ToString.Trim.Length = 0, "", CDbl(CellValue.Value * 100.0#).ToString("###0.0000"))
                      Else
                        InstrumentCommand.Parameters("@neo_percentvariation").Value = IIf(CellValue.Value.ToString.Trim.Length = 0, "", CDbl(CellValue.Value).ToString("###0.0000"))
                      End If
                    End If
                  End If

                  '

                  InstrumentCommand.ExecuteNonQuery()

                End If


              Next

            Catch ex As Exception
            Finally
              If (InstrumentCommand IsNot Nothing) AndAlso (InstrumentCommand.Connection IsNot Nothing) Then
                InstrumentCommand.Connection.Close()
                InstrumentCommand.Connection = Nothing
              End If
            End Try

          End If ' Found Cols ?

        End If ' Disponibilités


      Catch ex As Exception

      End Try ' Availabilities

      Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.Administrator_Holdings, CInt(Combo_TransactionFund.SelectedValue).ToString), True)
      Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.Administrator_Disponsbilities, CInt(Combo_TransactionFund.SelectedValue).ToString), True)

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error saving Valuation details.", ex.Message, ex.StackTrace, True)

    Finally
      Button_SaveFundData.Enabled = False
      _btnLoad.Enabled = False
    End Try
  End Sub

#Region " Set Form Combos (Form Specific Code) "

  Private Sub SetFundCombo()
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_TransactionFund, _
    RenaissanceStandardDatasets.tblFund, _
    "FundName", _
    "FundID", _
    "FundClosed=0")   ' 

  End Sub

#End Region



End Class
