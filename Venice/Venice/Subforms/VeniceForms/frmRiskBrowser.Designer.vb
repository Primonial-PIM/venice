﻿'<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
'Partial Class frmRiskBrowser
'    Inherits System.Windows.Forms.Form

'    'Form overrides dispose to clean up the component list.
'    <System.Diagnostics.DebuggerNonUserCode()> _
'    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
'        Try
'            If disposing AndAlso components IsNot Nothing Then
'                components.Dispose()
'            End If
'        Finally
'            MyBase.Dispose(disposing)
'        End Try
'    End Sub

'    'Required by the Windows Form Designer
'    Private components As System.ComponentModel.IContainer

'    'NOTE: The following procedure is required by the Windows Form Designer
'    'It can be modified using the Windows Form Designer.  
'    'Do not modify it using the code editor.
'	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
'		Me.components = New System.ComponentModel.Container
'		Me.label_Category = New System.Windows.Forms.Label
'		Me.Label2 = New System.Windows.Forms.Label
'		Me.Grid_RiskBrowser = New C1.Win.C1FlexGrid.C1FlexGrid
'		Me.ContextMenu_GridRiskBrowser = New System.Windows.Forms.ContextMenuStrip(Me.components)
'		Me.Context_ExpandInstruments = New System.Windows.Forms.ToolStripMenuItem
'		Me.Context_ColapseInstruments = New System.Windows.Forms.ToolStripMenuItem
'		Me.Context_ExpandCategories = New System.Windows.Forms.ToolStripMenuItem
'		Me.Context_ColapseCategories = New System.Windows.Forms.ToolStripMenuItem
'		Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
'		Me.Context_AddNewCategory = New System.Windows.Forms.ToolStripMenuItem
'		Me.Context_AddNewCharacteristic = New System.Windows.Forms.ToolStripMenuItem
'		Me.Context_RenameSeparator = New System.Windows.Forms.ToolStripSeparator
'		Me.Context_RenameLabel = New System.Windows.Forms.ToolStripMenuItem
'		Me.Context_RenameTextBox = New System.Windows.Forms.ToolStripTextBox
'		Me.Context_CharacteristicValueSeparator = New System.Windows.Forms.ToolStripSeparator
'		Me.Context_SetCharacteristicDefaultValue = New System.Windows.Forms.ToolStripMenuItem
'		Me.Context_SetCharacteristicFixedValue = New System.Windows.Forms.ToolStripMenuItem
'		Me.Context_SeparatorDelete = New System.Windows.Forms.ToolStripSeparator
'		Me.Context_Delete = New System.Windows.Forms.ToolStripMenuItem
'		Me.Context_SeparatorSave = New System.Windows.Forms.ToolStripSeparator
'		Me.Context_SaveChanges = New System.Windows.Forms.ToolStripMenuItem
'		Me.Context_CancelChanges = New System.Windows.Forms.ToolStripMenuItem
'		Me.RootMenu = New System.Windows.Forms.MenuStrip
'		Me.Form_StatusStrip = New System.Windows.Forms.StatusStrip
'		Me.Form_ProgressBar = New System.Windows.Forms.ToolStripProgressBar
'		Me.Label_Status = New System.Windows.Forms.ToolStripStatusLabel
'		Me.StatusLabel_Characteristics = New System.Windows.Forms.ToolStripStatusLabel
'		Me.Combo_Fund = New System.Windows.Forms.ComboBox
'		Me.Combo_MinimumUsage = New System.Windows.Forms.ComboBox
'		Me.Label7 = New System.Windows.Forms.Label
'		Me.Combo_Instrument = New System.Windows.Forms.ComboBox
'		Me.Label6 = New System.Windows.Forms.Label
'		Me.Combo_LimitCategory = New System.Windows.Forms.ComboBox
'		Me.Label5 = New System.Windows.Forms.Label
'		Me.Combo_RiskSets = New System.Windows.Forms.ComboBox
'		CType(Me.Grid_RiskBrowser, System.ComponentModel.ISupportInitialize).BeginInit()
'		Me.ContextMenu_GridRiskBrowser.SuspendLayout()
'		Me.Form_StatusStrip.SuspendLayout()
'		Me.SuspendLayout()
'		'
'		'label_Category
'		'
'		Me.label_Category.Location = New System.Drawing.Point(16, 67)
'		Me.label_Category.Name = "label_Category"
'		Me.label_Category.Size = New System.Drawing.Size(104, 16)
'		Me.label_Category.TabIndex = 82
'		Me.label_Category.Text = "Available Risk Sets"
'		'
'		'Label2
'		'
'		Me.Label2.Location = New System.Drawing.Point(16, 39)
'		Me.Label2.Name = "Label2"
'		Me.Label2.Size = New System.Drawing.Size(104, 16)
'		Me.Label2.TabIndex = 84
'		Me.Label2.Text = "Fund"
'		'
'		'Grid_RiskBrowser
'		'
'		Me.Grid_RiskBrowser.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None
'		Me.Grid_RiskBrowser.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None
'		Me.Grid_RiskBrowser.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
'								Or System.Windows.Forms.AnchorStyles.Left) _
'								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
'		Me.Grid_RiskBrowser.AutoGenerateColumns = False
'		Me.Grid_RiskBrowser.ColumnInfo = "10,1,0,0,0,85,Columns:"
'		Me.Grid_RiskBrowser.ContextMenuStrip = Me.ContextMenu_GridRiskBrowser
'		Me.Grid_RiskBrowser.Cursor = System.Windows.Forms.Cursors.Default
'		Me.Grid_RiskBrowser.Location = New System.Drawing.Point(4, 183)
'		Me.Grid_RiskBrowser.Name = "Grid_RiskBrowser"
'		Me.Grid_RiskBrowser.Rows.DefaultSize = 17
'		Me.Grid_RiskBrowser.Size = New System.Drawing.Size(936, 520)
'		Me.Grid_RiskBrowser.TabIndex = 104
'		Me.Grid_RiskBrowser.Tree.Column = 0
'		'
'		'ContextMenu_GridRiskBrowser
'		'
'		Me.ContextMenu_GridRiskBrowser.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Context_ExpandInstruments, Me.Context_ColapseInstruments, Me.Context_ExpandCategories, Me.Context_ColapseCategories, Me.ToolStripSeparator1, Me.Context_AddNewCategory, Me.Context_AddNewCharacteristic, Me.Context_RenameSeparator, Me.Context_RenameLabel, Me.Context_RenameTextBox, Me.Context_CharacteristicValueSeparator, Me.Context_SetCharacteristicDefaultValue, Me.Context_SetCharacteristicFixedValue, Me.Context_SeparatorDelete, Me.Context_Delete, Me.Context_SeparatorSave, Me.Context_SaveChanges, Me.Context_CancelChanges})
'		Me.ContextMenu_GridRiskBrowser.Name = "ContextMenu_GridCharacteristics"
'		Me.ContextMenu_GridRiskBrowser.Size = New System.Drawing.Size(261, 323)
'		'
'		'Context_ExpandInstruments
'		'
'		Me.Context_ExpandInstruments.Name = "Context_ExpandInstruments"
'		Me.Context_ExpandInstruments.Size = New System.Drawing.Size(260, 22)
'		Me.Context_ExpandInstruments.Text = "Expand all Instruments"
'		'
'		'Context_ColapseInstruments
'		'
'		Me.Context_ColapseInstruments.Name = "Context_ColapseInstruments"
'		Me.Context_ColapseInstruments.Size = New System.Drawing.Size(260, 22)
'		Me.Context_ColapseInstruments.Text = "Colapse all Instruments"
'		'
'		'Context_ExpandCategories
'		'
'		Me.Context_ExpandCategories.Name = "Context_ExpandCategories"
'		Me.Context_ExpandCategories.Size = New System.Drawing.Size(260, 22)
'		Me.Context_ExpandCategories.Text = "Expand all Categories"
'		'
'		'Context_ColapseCategories
'		'
'		Me.Context_ColapseCategories.Name = "Context_ColapseCategories"
'		Me.Context_ColapseCategories.Size = New System.Drawing.Size(260, 22)
'		Me.Context_ColapseCategories.Text = "Colapse all Categories"
'		'
'		'ToolStripSeparator1
'		'
'		Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
'		Me.ToolStripSeparator1.Size = New System.Drawing.Size(257, 6)
'		'
'		'Context_AddNewCategory
'		'
'		Me.Context_AddNewCategory.Name = "Context_AddNewCategory"
'		Me.Context_AddNewCategory.Size = New System.Drawing.Size(260, 22)
'		Me.Context_AddNewCategory.Text = "Add New Category"
'		'
'		'Context_AddNewCharacteristic
'		'
'		Me.Context_AddNewCharacteristic.Name = "Context_AddNewCharacteristic"
'		Me.Context_AddNewCharacteristic.Size = New System.Drawing.Size(260, 22)
'		Me.Context_AddNewCharacteristic.Text = "Add New Characteristic"
'		Me.Context_AddNewCharacteristic.Visible = False
'		'
'		'Context_RenameSeparator
'		'
'		Me.Context_RenameSeparator.Name = "Context_RenameSeparator"
'		Me.Context_RenameSeparator.Size = New System.Drawing.Size(257, 6)
'		Me.Context_RenameSeparator.Visible = False
'		'
'		'Context_RenameLabel
'		'
'		Me.Context_RenameLabel.Enabled = False
'		Me.Context_RenameLabel.Name = "Context_RenameLabel"
'		Me.Context_RenameLabel.Size = New System.Drawing.Size(260, 22)
'		Me.Context_RenameLabel.Text = "Rename ..."
'		Me.Context_RenameLabel.Visible = False
'		'
'		'Context_RenameTextBox
'		'
'		Me.Context_RenameTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
'		Me.Context_RenameTextBox.Name = "Context_RenameTextBox"
'		Me.Context_RenameTextBox.Size = New System.Drawing.Size(200, 23)
'		Me.Context_RenameTextBox.Visible = False
'		'
'		'Context_CharacteristicValueSeparator
'		'
'		Me.Context_CharacteristicValueSeparator.Name = "Context_CharacteristicValueSeparator"
'		Me.Context_CharacteristicValueSeparator.Size = New System.Drawing.Size(257, 6)
'		Me.Context_CharacteristicValueSeparator.Visible = False
'		'
'		'Context_SetCharacteristicDefaultValue
'		'
'		Me.Context_SetCharacteristicDefaultValue.Name = "Context_SetCharacteristicDefaultValue"
'		Me.Context_SetCharacteristicDefaultValue.Size = New System.Drawing.Size(260, 22)
'		Me.Context_SetCharacteristicDefaultValue.Text = "Set Characteristic Default Value"
'		Me.Context_SetCharacteristicDefaultValue.Visible = False
'		'
'		'Context_SetCharacteristicFixedValue
'		'
'		Me.Context_SetCharacteristicFixedValue.Name = "Context_SetCharacteristicFixedValue"
'		Me.Context_SetCharacteristicFixedValue.Size = New System.Drawing.Size(260, 22)
'		Me.Context_SetCharacteristicFixedValue.Text = "Set Characteristic Fixed Value"
'		Me.Context_SetCharacteristicFixedValue.Visible = False
'		'
'		'Context_SeparatorDelete
'		'
'		Me.Context_SeparatorDelete.Name = "Context_SeparatorDelete"
'		Me.Context_SeparatorDelete.Size = New System.Drawing.Size(257, 6)
'		Me.Context_SeparatorDelete.Visible = False
'		'
'		'Context_Delete
'		'
'		Me.Context_Delete.Name = "Context_Delete"
'		Me.Context_Delete.Size = New System.Drawing.Size(260, 22)
'		Me.Context_Delete.Text = "Delete ..."
'		Me.Context_Delete.Visible = False
'		'
'		'Context_SeparatorSave
'		'
'		Me.Context_SeparatorSave.Name = "Context_SeparatorSave"
'		Me.Context_SeparatorSave.Size = New System.Drawing.Size(257, 6)
'		Me.Context_SeparatorSave.Visible = False
'		'
'		'Context_SaveChanges
'		'
'		Me.Context_SaveChanges.Name = "Context_SaveChanges"
'		Me.Context_SaveChanges.Size = New System.Drawing.Size(260, 22)
'		Me.Context_SaveChanges.Text = "Save Changes"
'		Me.Context_SaveChanges.Visible = False
'		'
'		'Context_CancelChanges
'		'
'		Me.Context_CancelChanges.Name = "Context_CancelChanges"
'		Me.Context_CancelChanges.Size = New System.Drawing.Size(260, 22)
'		Me.Context_CancelChanges.Text = "Cancel Changes"
'		Me.Context_CancelChanges.Visible = False
'		'
'		'RootMenu
'		'
'		Me.RootMenu.Location = New System.Drawing.Point(0, 0)
'		Me.RootMenu.Name = "RootMenu"
'		Me.RootMenu.Size = New System.Drawing.Size(944, 24)
'		Me.RootMenu.TabIndex = 107
'		Me.RootMenu.Text = " "
'		'
'		'Form_StatusStrip
'		'
'		Me.Form_StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Form_ProgressBar, Me.Label_Status, Me.StatusLabel_Characteristics})
'		Me.Form_StatusStrip.Location = New System.Drawing.Point(0, 704)
'		Me.Form_StatusStrip.Name = "Form_StatusStrip"
'		Me.Form_StatusStrip.Size = New System.Drawing.Size(944, 22)
'		Me.Form_StatusStrip.TabIndex = 108
'		Me.Form_StatusStrip.Text = " "
'		'
'		'Form_ProgressBar
'		'
'		Me.Form_ProgressBar.Maximum = 20
'		Me.Form_ProgressBar.Name = "Form_ProgressBar"
'		Me.Form_ProgressBar.Size = New System.Drawing.Size(150, 17)
'		Me.Form_ProgressBar.Step = 1
'		Me.Form_ProgressBar.Visible = False
'		'
'		'Label_Status
'		'
'		Me.Label_Status.Name = "Label_Status"
'		Me.Label_Status.Size = New System.Drawing.Size(10, 17)
'		Me.Label_Status.Text = " "
'		'
'		'StatusLabel_Characteristics
'		'
'		Me.StatusLabel_Characteristics.Name = "StatusLabel_Characteristics"
'		Me.StatusLabel_Characteristics.Size = New System.Drawing.Size(10, 17)
'		Me.StatusLabel_Characteristics.Text = " "
'		'
'		'Combo_Fund
'		'
'		Me.Combo_Fund.FlatStyle = System.Windows.Forms.FlatStyle.System
'		Me.Combo_Fund.Location = New System.Drawing.Point(129, 34)
'		Me.Combo_Fund.Name = "Combo_Fund"
'		Me.Combo_Fund.Size = New System.Drawing.Size(256, 21)
'		Me.Combo_Fund.TabIndex = 115
'		'
'		'Combo_MinimumUsage
'		'
'		Me.Combo_MinimumUsage.FlatStyle = System.Windows.Forms.FlatStyle.System
'		Me.Combo_MinimumUsage.Items.AddRange(New Object() {"0%", "5%", "10%", "20%", "25%", "50%", "75%", "100%"})
'		Me.Combo_MinimumUsage.Location = New System.Drawing.Point(129, 146)
'		Me.Combo_MinimumUsage.Name = "Combo_MinimumUsage"
'		Me.Combo_MinimumUsage.Size = New System.Drawing.Size(256, 21)
'		Me.Combo_MinimumUsage.TabIndex = 128
'		'
'		'Label7
'		'
'		Me.Label7.FlatStyle = System.Windows.Forms.FlatStyle.System
'		Me.Label7.Location = New System.Drawing.Point(18, 149)
'		Me.Label7.Name = "Label7"
'		Me.Label7.Size = New System.Drawing.Size(105, 20)
'		Me.Label7.TabIndex = 131
'		Me.Label7.Text = "Minimum Usage"
'		'
'		'Combo_Instrument
'		'
'		Me.Combo_Instrument.FlatStyle = System.Windows.Forms.FlatStyle.System
'		Me.Combo_Instrument.Location = New System.Drawing.Point(129, 118)
'		Me.Combo_Instrument.Name = "Combo_Instrument"
'		Me.Combo_Instrument.Size = New System.Drawing.Size(256, 21)
'		Me.Combo_Instrument.TabIndex = 127
'		'
'		'Label6
'		'
'		Me.Label6.FlatStyle = System.Windows.Forms.FlatStyle.System
'		Me.Label6.Location = New System.Drawing.Point(17, 121)
'		Me.Label6.Name = "Label6"
'		Me.Label6.Size = New System.Drawing.Size(105, 20)
'		Me.Label6.TabIndex = 130
'		Me.Label6.Text = "Limit Instrument"
'		'
'		'Combo_LimitCategory
'		'
'		Me.Combo_LimitCategory.FlatStyle = System.Windows.Forms.FlatStyle.System
'		Me.Combo_LimitCategory.Location = New System.Drawing.Point(129, 90)
'		Me.Combo_LimitCategory.Name = "Combo_LimitCategory"
'		Me.Combo_LimitCategory.Size = New System.Drawing.Size(256, 21)
'		Me.Combo_LimitCategory.TabIndex = 126
'		'
'		'Label5
'		'
'		Me.Label5.FlatStyle = System.Windows.Forms.FlatStyle.System
'		Me.Label5.Location = New System.Drawing.Point(17, 93)
'		Me.Label5.Name = "Label5"
'		Me.Label5.Size = New System.Drawing.Size(105, 20)
'		Me.Label5.TabIndex = 129
'		Me.Label5.Text = "Limit Category"
'		'
'		'Combo_RiskSets
'		'
'		Me.Combo_RiskSets.FlatStyle = System.Windows.Forms.FlatStyle.System
'		Me.Combo_RiskSets.Location = New System.Drawing.Point(129, 62)
'		Me.Combo_RiskSets.Name = "Combo_RiskSets"
'		Me.Combo_RiskSets.Size = New System.Drawing.Size(256, 21)
'		Me.Combo_RiskSets.TabIndex = 132
'		'
'		'frmRiskBrowser
'		'
'		Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
'		Me.ClientSize = New System.Drawing.Size(944, 726)
'		Me.Controls.Add(Me.Combo_RiskSets)
'		Me.Controls.Add(Me.Combo_MinimumUsage)
'		Me.Controls.Add(Me.Label7)
'		Me.Controls.Add(Me.Combo_Instrument)
'		Me.Controls.Add(Me.Label6)
'		Me.Controls.Add(Me.Combo_LimitCategory)
'		Me.Controls.Add(Me.Label5)
'		Me.Controls.Add(Me.Combo_Fund)
'		Me.Controls.Add(Me.Form_StatusStrip)
'		Me.Controls.Add(Me.RootMenu)
'		Me.Controls.Add(Me.Grid_RiskBrowser)
'		Me.Controls.Add(Me.Label2)
'		Me.Controls.Add(Me.label_Category)
'		Me.Name = "frmRiskBrowser"
'		Me.Text = "Add / Edit Characteristics"
'		CType(Me.Grid_RiskBrowser, System.ComponentModel.ISupportInitialize).EndInit()
'		Me.ContextMenu_GridRiskBrowser.ResumeLayout(False)
'		Me.ContextMenu_GridRiskBrowser.PerformLayout()
'		Me.Form_StatusStrip.ResumeLayout(False)
'		Me.Form_StatusStrip.PerformLayout()
'		Me.ResumeLayout(False)
'		Me.PerformLayout()

'	End Sub
'	Friend WithEvents Combo_Fund As System.Windows.Forms.ComboBox
'	Friend WithEvents Combo_MinimumUsage As System.Windows.Forms.ComboBox
'	Friend WithEvents Label7 As System.Windows.Forms.Label
'	Friend WithEvents Combo_Instrument As System.Windows.Forms.ComboBox
'	Friend WithEvents Label6 As System.Windows.Forms.Label
'	Friend WithEvents Combo_LimitCategory As System.Windows.Forms.ComboBox
'	Friend WithEvents Label5 As System.Windows.Forms.Label
'	Friend WithEvents Combo_RiskSets As System.Windows.Forms.ComboBox

'End Class
