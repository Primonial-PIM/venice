' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 11-05-2012
' ***********************************************************************
' <copyright file="frmSetKnowledgedate.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals


''' <summary>
''' Class frmSetKnowledgedate
''' </summary>
Public Class frmSetKnowledgedate

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmSetKnowledgedate"/> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The BTN cancel
    ''' </summary>
  Friend WithEvents btnCancel As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN set knowledge date
    ''' </summary>
  Friend WithEvents btnSetKnowledgeDate As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN close
    ''' </summary>
  Friend WithEvents btnClose As System.Windows.Forms.Button
    ''' <summary>
    ''' The LBL person user name
    ''' </summary>
  Friend WithEvents lblPersonUserName As System.Windows.Forms.Label
    ''' <summary>
    ''' The label1
    ''' </summary>
  Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The radio_ whole day
    ''' </summary>
  Friend WithEvents Radio_WholeDay As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The date_ knowledge date
    ''' </summary>
  Friend WithEvents Date_KnowledgeDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The radio_ precise time
    ''' </summary>
  Friend WithEvents Radio_PreciseTime As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The radio_ historic KD
    ''' </summary>
  Friend WithEvents Radio_HistoricKD As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The radio_ KD live
    ''' </summary>
  Friend WithEvents Radio_KDLive As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The panel1
    ''' </summary>
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The label2
    ''' </summary>
  Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label3
    ''' </summary>
  Friend WithEvents Label3 As System.Windows.Forms.Label
    ''' <summary>
    ''' The panel2
    ''' </summary>
  Friend WithEvents Panel2 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The panel3
    ''' </summary>
  Friend WithEvents Panel3 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The panel4
    ''' </summary>
  Friend WithEvents Panel4 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The panel5
    ''' </summary>
  Friend WithEvents Panel5 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The combo_ milestone fund
    ''' </summary>
  Friend WithEvents Combo_MilestoneFund As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ milestone date
    ''' </summary>
  Friend WithEvents Combo_MilestoneDate As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ bookmark
    ''' </summary>
  Friend WithEvents Combo_Bookmark As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label4
    ''' </summary>
  Friend WithEvents Label4 As System.Windows.Forms.Label
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.btnCancel = New System.Windows.Forms.Button
    Me.btnSetKnowledgeDate = New System.Windows.Forms.Button
    Me.btnClose = New System.Windows.Forms.Button
    Me.Date_KnowledgeDate = New System.Windows.Forms.DateTimePicker
    Me.lblPersonUserName = New System.Windows.Forms.Label
    Me.Label1 = New System.Windows.Forms.Label
    Me.Radio_WholeDay = New System.Windows.Forms.RadioButton
    Me.Radio_PreciseTime = New System.Windows.Forms.RadioButton
    Me.Radio_HistoricKD = New System.Windows.Forms.RadioButton
    Me.Radio_KDLive = New System.Windows.Forms.RadioButton
    Me.Panel1 = New System.Windows.Forms.Panel
    Me.Panel2 = New System.Windows.Forms.Panel
    Me.Label2 = New System.Windows.Forms.Label
    Me.Label3 = New System.Windows.Forms.Label
    Me.Panel3 = New System.Windows.Forms.Panel
    Me.Panel4 = New System.Windows.Forms.Panel
    Me.Panel5 = New System.Windows.Forms.Panel
    Me.Combo_MilestoneFund = New System.Windows.Forms.ComboBox
    Me.Combo_MilestoneDate = New System.Windows.Forms.ComboBox
    Me.Combo_Bookmark = New System.Windows.Forms.ComboBox
    Me.Label4 = New System.Windows.Forms.Label
    Me.Panel1.SuspendLayout()
    Me.Panel3.SuspendLayout()
    Me.Panel5.SuspendLayout()
    Me.SuspendLayout()
    '
    'btnCancel
    '
    Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnCancel.Location = New System.Drawing.Point(232, 292)
    Me.btnCancel.Name = "btnCancel"
    Me.btnCancel.Size = New System.Drawing.Size(75, 28)
    Me.btnCancel.TabIndex = 7
    Me.btnCancel.Text = "&Cancel"
    '
    'btnSetKnowledgeDate
    '
    Me.btnSetKnowledgeDate.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnSetKnowledgeDate.Location = New System.Drawing.Point(60, 292)
    Me.btnSetKnowledgeDate.Name = "btnSetKnowledgeDate"
    Me.btnSetKnowledgeDate.Size = New System.Drawing.Size(148, 28)
    Me.btnSetKnowledgeDate.TabIndex = 5
    Me.btnSetKnowledgeDate.Text = "Set KnowledgeDate"
    '
    'btnClose
    '
    Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnClose.Location = New System.Drawing.Point(316, 292)
    Me.btnClose.Name = "btnClose"
    Me.btnClose.Size = New System.Drawing.Size(75, 28)
    Me.btnClose.TabIndex = 8
    Me.btnClose.Text = "&Close"
    '
    'Date_KnowledgeDate
    '
    Me.Date_KnowledgeDate.CustomFormat = "dd MMM yyyy"
    Me.Date_KnowledgeDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
    Me.Date_KnowledgeDate.Location = New System.Drawing.Point(180, 80)
    Me.Date_KnowledgeDate.Name = "Date_KnowledgeDate"
    Me.Date_KnowledgeDate.Size = New System.Drawing.Size(252, 20)
    Me.Date_KnowledgeDate.TabIndex = 9
    '
    'lblPersonUserName
    '
    Me.lblPersonUserName.Location = New System.Drawing.Point(8, 148)
    Me.lblPersonUserName.Name = "lblPersonUserName"
    Me.lblPersonUserName.Size = New System.Drawing.Size(152, 20)
    Me.lblPersonUserName.TabIndex = 41
    Me.lblPersonUserName.Text = "Get MilestoneDate"
    '
    'Label1
    '
    Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label1.Location = New System.Drawing.Point(8, 12)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(284, 24)
    Me.Label1.TabIndex = 42
    Me.Label1.Text = "Set Knowledgedate"
    '
    'Radio_WholeDay
    '
    Me.Radio_WholeDay.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Radio_WholeDay.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Radio_WholeDay.Location = New System.Drawing.Point(4, 4)
    Me.Radio_WholeDay.Name = "Radio_WholeDay"
    Me.Radio_WholeDay.Size = New System.Drawing.Size(104, 16)
    Me.Radio_WholeDay.TabIndex = 43
    Me.Radio_WholeDay.Text = "WholeDay"
    '
    'Radio_PreciseTime
    '
    Me.Radio_PreciseTime.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Radio_PreciseTime.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Radio_PreciseTime.Location = New System.Drawing.Point(128, 4)
    Me.Radio_PreciseTime.Name = "Radio_PreciseTime"
    Me.Radio_PreciseTime.Size = New System.Drawing.Size(104, 16)
    Me.Radio_PreciseTime.TabIndex = 44
    Me.Radio_PreciseTime.Text = "Precise Time"
    '
    'Radio_HistoricKD
    '
    Me.Radio_HistoricKD.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Radio_HistoricKD.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Radio_HistoricKD.Location = New System.Drawing.Point(8, 80)
    Me.Radio_HistoricKD.Name = "Radio_HistoricKD"
    Me.Radio_HistoricKD.Size = New System.Drawing.Size(148, 16)
    Me.Radio_HistoricKD.TabIndex = 45
    Me.Radio_HistoricKD.Text = "Historic KnowledgeDate"
    '
    'Radio_KDLive
    '
    Me.Radio_KDLive.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Radio_KDLive.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Radio_KDLive.Location = New System.Drawing.Point(8, 44)
    Me.Radio_KDLive.Name = "Radio_KDLive"
    Me.Radio_KDLive.Size = New System.Drawing.Size(148, 16)
    Me.Radio_KDLive.TabIndex = 46
    Me.Radio_KDLive.Text = "'LIVE' KnowledgeDate"
    '
    'Panel1
    '
    Me.Panel1.Controls.Add(Me.Panel2)
    Me.Panel1.Location = New System.Drawing.Point(8, 132)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(428, 2)
    Me.Panel1.TabIndex = 47
    '
    'Panel2
    '
    Me.Panel2.Location = New System.Drawing.Point(0, 116)
    Me.Panel2.Name = "Panel2"
    Me.Panel2.Size = New System.Drawing.Size(428, 2)
    Me.Panel2.TabIndex = 48
    '
    'Label2
    '
    Me.Label2.Location = New System.Drawing.Point(28, 176)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(120, 20)
    Me.Label2.TabIndex = 48
    Me.Label2.Text = "Fund Name"
    '
    'Label3
    '
    Me.Label3.Location = New System.Drawing.Point(28, 204)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(120, 20)
    Me.Label3.TabIndex = 49
    Me.Label3.Text = "Milestone Date"
    '
    'Panel3
    '
    Me.Panel3.Controls.Add(Me.Panel4)
    Me.Panel3.Location = New System.Drawing.Point(8, 232)
    Me.Panel3.Name = "Panel3"
    Me.Panel3.Size = New System.Drawing.Size(428, 2)
    Me.Panel3.TabIndex = 50
    '
    'Panel4
    '
    Me.Panel4.Location = New System.Drawing.Point(0, 116)
    Me.Panel4.Name = "Panel4"
    Me.Panel4.Size = New System.Drawing.Size(428, 2)
    Me.Panel4.TabIndex = 48
    '
    'Panel5
    '
    Me.Panel5.Controls.Add(Me.Radio_PreciseTime)
    Me.Panel5.Controls.Add(Me.Radio_WholeDay)
    Me.Panel5.Location = New System.Drawing.Point(180, 104)
    Me.Panel5.Name = "Panel5"
    Me.Panel5.Size = New System.Drawing.Size(248, 24)
    Me.Panel5.TabIndex = 51
    '
    'Combo_MilestoneFund
    '
    Me.Combo_MilestoneFund.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_MilestoneFund.Location = New System.Drawing.Point(180, 172)
    Me.Combo_MilestoneFund.Name = "Combo_MilestoneFund"
    Me.Combo_MilestoneFund.Size = New System.Drawing.Size(252, 21)
    Me.Combo_MilestoneFund.TabIndex = 52
    '
    'Combo_MilestoneDate
    '
    Me.Combo_MilestoneDate.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_MilestoneDate.Location = New System.Drawing.Point(180, 200)
    Me.Combo_MilestoneDate.Name = "Combo_MilestoneDate"
    Me.Combo_MilestoneDate.Size = New System.Drawing.Size(252, 21)
    Me.Combo_MilestoneDate.TabIndex = 53
    '
    'Combo_Bookmark
    '
    Me.Combo_Bookmark.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Bookmark.Location = New System.Drawing.Point(180, 244)
    Me.Combo_Bookmark.Name = "Combo_Bookmark"
    Me.Combo_Bookmark.Size = New System.Drawing.Size(252, 21)
    Me.Combo_Bookmark.TabIndex = 55
    '
    'Label4
    '
    Me.Label4.Location = New System.Drawing.Point(8, 248)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(140, 20)
    Me.Label4.TabIndex = 54
    Me.Label4.Text = "Bookmark"
    '
    'frmSetKnowledgedate
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.CancelButton = Me.btnCancel
    Me.ClientSize = New System.Drawing.Size(444, 332)
    Me.Controls.Add(Me.Combo_Bookmark)
    Me.Controls.Add(Me.Label4)
    Me.Controls.Add(Me.Combo_MilestoneDate)
    Me.Controls.Add(Me.Combo_MilestoneFund)
    Me.Controls.Add(Me.Panel5)
    Me.Controls.Add(Me.Panel3)
    Me.Controls.Add(Me.Label3)
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.Panel1)
    Me.Controls.Add(Me.Radio_KDLive)
    Me.Controls.Add(Me.Radio_HistoricKD)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.lblPersonUserName)
    Me.Controls.Add(Me.Date_KnowledgeDate)
    Me.Controls.Add(Me.btnClose)
    Me.Controls.Add(Me.btnSetKnowledgeDate)
    Me.Controls.Add(Me.btnCancel)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frmSetKnowledgedate"
    Me.Text = "Set Knowledgedate"
    Me.Panel1.ResumeLayout(False)
    Me.Panel3.ResumeLayout(False)
    Me.Panel5.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Form Locals and Constants "


  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
  Private WithEvents _MainForm As VeniceMain

  ' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
  Private FormTooltip As New ToolTip()

  ' Form Constants, specific to the table being updated.

    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
  Private ALWAYS_CLOSE_THIS_FORM As Boolean = True

  ' The standard ChangeID for this form. e.g. tblPerson
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

  ' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
  Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
  Private THIS_FORM_FormID As VeniceFormID

  ' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
  Private FormIsValid As Boolean
    ''' <summary>
    ''' The form changed
    ''' </summary>
  Private FormChanged As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
  Private _FormOpenFailed As Boolean

  ' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
  Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
  Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
  Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
  Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
  Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
    ' Public property to return handle to the 'Main' Venice form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
  Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
    ' Public property maintaining a value indicating if the cursor is over the 'Cancel'
    ' Button on this form.
    ' This property is specifically designed for use by the field formating Event functions
    ' In order that they do not impose format restrictions if the user is about to click the 
    ' 'Cancel' button.
    '
    Get
      Return False
    End Get
    Set(ByVal Value As Boolean)
    End Set
  End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
  Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
    Get
      Return False
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
    Get
      Return True
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmSetKnowledgedate"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  Public Sub New(ByVal pMainForm As VeniceMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()

    _MainForm = pMainForm
    AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

    _FormOpenFailed = False

    ' ******************************************************
    ' Form Specific Settings :
    ' ******************************************************

    ' Form Permissioning :-

    THIS_FORM_PermissionArea = Me.Name
    THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

    ' 'This' form ID

    THIS_FORM_FormID = VeniceFormID.frmSetKnowledgeDate

    ' Format Event Handlers for form controls

    AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

    AddHandler Combo_MilestoneFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_MilestoneDate.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_Bookmark.KeyUp, AddressOf MainForm.ComboSelectAsYouType

    AddHandler Combo_MilestoneFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_MilestoneDate.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_Bookmark.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

    AddHandler Combo_MilestoneFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_MilestoneDate.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_Bookmark.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

    AddHandler Combo_MilestoneFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_MilestoneDate.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_Bookmark.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

    ' Form Control Changed events

    AddHandler Radio_KDLive.CheckedChanged, AddressOf Me.FormControlChanged
    AddHandler Radio_PreciseTime.CheckedChanged, AddressOf Me.FormControlChanged
    AddHandler Radio_WholeDay.CheckedChanged, AddressOf Me.FormControlChanged
    AddHandler Radio_PreciseTime.CheckedChanged, AddressOf Me.FormControlChanged
    AddHandler Date_KnowledgeDate.ValueChanged, AddressOf Me.FormControlChanged

    ' Set up the ToolTip
    MainForm.SetFormToolTip(Me, FormTooltip)

    ' ******************************************************
    ' End Form Specific.
    ' ******************************************************

    THIS_FORM_ChangeID = RenaissanceChangeID.KnowledgeDate

  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
  Public Sub ResetForm() Implements StandardVeniceForm.ResetForm
		' ********************************************************************************
		'
		'
		' ********************************************************************************

    Call Form_Load(Me, New System.EventArgs)
  End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
  Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
		' ********************************************************************************
		'
		'
		' ********************************************************************************

		ALWAYS_CLOSE_THIS_FORM = True
    Me.Close()
  End Sub

    ''' <summary>
    ''' Sets the knowledge date.
    ''' </summary>
    ''' <param name="pKnowledgeDate">The p knowledge date.</param>
  Public Sub SetKnowledgeDate(ByVal pKnowledgeDate As Date)
		' ********************************************************************************
		'
		'
		' ********************************************************************************

    Call Form_Load(Me, New System.EventArgs)

    If pKnowledgeDate <= KNOWLEDGEDATE_NOW Then
      Me.Radio_KDLive.Checked = True
    Else
      Me.Radio_HistoricKD.Checked = True

      If (pKnowledgeDate.TimeOfDay.TotalSeconds >= LAST_SECOND) Then
        Me.Radio_WholeDay.Checked = True
        Date_KnowledgeDate.Value = pKnowledgeDate.Date
      Else
        Me.Radio_PreciseTime.Checked = True
        Date_KnowledgeDate.Value = pKnowledgeDate
      End If
    End If

    Application.DoEvents()

    Call btnSetKnowledgeDate_Click(Me, New EventArgs)

  End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		' ********************************************************************************
		'
		'
		' ********************************************************************************

		Try

			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
			_FormOpenFailed = False

			If Not (MainForm Is Nothing) Then
				FormIsValid = True
			Else
				MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
				FormIsValid = False
				_FormOpenFailed = True
				Exit Sub
			End If

		Catch ex As Exception
		End Try

		Try

			' Check User permissions
			Call CheckPermissions()
			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

				FormIsValid = False
				_FormOpenFailed = True
				Exit Sub
			End If

			' Build Combos

			Call Me.SetFundCombo()
			Call Me.SetMilestoneDateCombo()
			Call Me.SetBookmarkCombo()

			' Initialise controls

			Call GetFormData()

			MainForm.SetComboSelectionLengths(Me)

		Catch ex As Exception

			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Initialising Form (Form.Load).", ex.StackTrace, True)

		End Try



  End Sub

    ''' <summary>
    ''' Handles the Closing event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
  Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		' ********************************************************************************
		'
		'
		' ********************************************************************************

		Dim HideForm As Boolean

    ' Hide or Close this form ?
    ' All depends on how many of this form type are Open or in Cache...


    If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
      HideForm = False
    Else
      HideForm = True
      If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.NoCache_CacheCount Then
        HideForm = False
      End If
    End If

    If HideForm = True Then
      MainForm.HideInFormsCollection(Me)
      Me.Hide() ' NPP Fix

      e.Cancel = True
    Else
      Try
        MainForm.RemoveFromFormsCollection(Me)
        RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

        RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

        RemoveHandler Combo_MilestoneFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_MilestoneDate.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_Bookmark.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        RemoveHandler Combo_MilestoneFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_MilestoneDate.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_Bookmark.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

        RemoveHandler Combo_MilestoneFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_MilestoneDate.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_Bookmark.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

        RemoveHandler Combo_MilestoneFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_MilestoneDate.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_Bookmark.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

        ' Form Control Changed events

        RemoveHandler Radio_KDLive.CheckedChanged, AddressOf Me.FormControlChanged
        RemoveHandler Radio_PreciseTime.CheckedChanged, AddressOf Me.FormControlChanged
        RemoveHandler Radio_WholeDay.CheckedChanged, AddressOf Me.FormControlChanged
        RemoveHandler Radio_PreciseTime.CheckedChanged, AddressOf Me.FormControlChanged
        RemoveHandler Date_KnowledgeDate.ValueChanged, AddressOf Me.FormControlChanged

      Catch ex As Exception
      End Try
    End If

  End Sub


#End Region

  ' Routine to handle changes / updates to tables by this and other windows.
  ' If this, or any other, form posts a change to a table, then it will invoke an update event 
  ' detailing what tables have been altered.
  ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
  ' the 'VeniceAutoUpdate' event of the main Venice form.
  ' Each form may them react as appropriate to changes in any table that might impact it.
  '
    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    Dim KnowledgeDateChanged As Boolean
    Dim SetButtonStatus_Flag As Boolean

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

    KnowledgeDateChanged = False
    SetButtonStatus_Flag = False

    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
      KnowledgeDateChanged = True
    End If

    ' ****************************************************************
    ' Check for changes relevant to this form
    ' ****************************************************************

    ' Changes to the KnowledgeDate :-
    If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
      SetButtonStatus_Flag = True
    End If

    ' Changes to the tblFund table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFund) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Call SetFundCombo()
    End If

    ' Changes to the tblUserPermissions table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

      ' Check ongoing permissions.

      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

        FormIsValid = False
        Me.Close()
        Exit Sub
      End If

      SetButtonStatus_Flag = True

    End If

    ' ****************************************************************
    '
    ' ****************************************************************

    If SetButtonStatus_Flag Then
      Call SetButtonStatus()
    End If

  End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

  ' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
  Private Sub CheckPermissions()
    ' *****************************************************************************
    '
    ' *****************************************************************************

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

  End Sub

  ' Flag changes to form controls, Event Associations made in 'New' routine.
    ''' <summary>
  ''' Basic event handler, called when a form value is changed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' *****************************************************************************
    ' Update form buttons to reflect form changes.
    ' *****************************************************************************

    If (Me.HasUpdatePermission) Then
      FormChanged = True
      Call SetButtonStatus()
    End If

  End Sub


#End Region

#Region " Set Form Combos (Form Specific Code) "

    ''' <summary>
    ''' Sets the fund combo.
    ''' </summary>
  Private Sub SetFundCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_MilestoneFund, _
    RenaissanceStandardDatasets.tblFund, _
    "FundName", _
    "FundID", _
    "", False, True, True)    ' 

  End Sub

    ''' <summary>
    ''' Sets the milestone date combo.
    ''' </summary>
  Private Sub SetMilestoneDateCombo()
    Dim SelectString As String

    SelectString = False
    If (Combo_MilestoneFund.SelectedIndex > 0) AndAlso (IsNumeric(Combo_MilestoneFund.SelectedValue)) Then
      SelectString = "MilestoneFundID=" & CInt(Combo_MilestoneFund.SelectedValue).ToString
    End If

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_MilestoneDate, _
    RenaissanceStandardDatasets.tblFundMilestones, _
    "MilestoneDate", _
    "MilestoneKnowledgeDate", _
    SelectString, False, False, True)    ' 

  End Sub

    ''' <summary>
    ''' Sets the bookmark combo.
    ''' </summary>
  Private Sub SetBookmarkCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_Bookmark, _
    RenaissanceStandardDatasets.tblBookmark, _
    "BookMarkDescription", _
    "BookmarkDate", _
    "", False, True, True)    ' 

  End Sub

#End Region

#Region " SetButton / Control Events (Form Specific Code) "

    ''' <summary>
    ''' Sets the button status.
    ''' </summary>
  Private Sub SetButtonStatus()
    ' Sets the status of the form controlls appropriate to the current users 
    'permissions and the 'Changed' or 'New' status of the form.

    ' No Read permission :-

    If Me.HasReadPermission = False Then
      MainForm.LogError(Me.Name & ", SetButtonStatus()", 0, "", "You do not have Read permission for this Form", "", True)
      Me.Close()
      Exit Sub
    End If

    If (Me.HasUpdatePermission) And (FormChanged = True) Then
      Me.btnSetKnowledgeDate.Enabled = True
      Me.btnCancel.Enabled = True
    Else
      Me.btnSetKnowledgeDate.Enabled = False
      Me.btnCancel.Enabled = False
    End If

  End Sub

    ''' <summary>
    ''' Gets the form data.
    ''' </summary>
  Private Sub GetFormData()
    ' *****************************************************************************
    ' Display the current Knowledgedate on the KD form.
    '
    ' *****************************************************************************

    If MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW Then
      ' If KD is Live, then ...

      Me.Radio_HistoricKD.Checked = False
      Me.Radio_KDLive.Checked = True

      Me.Date_KnowledgeDate.Value = Now.Date

      Me.Radio_WholeDay.Checked = True
    Else
      ' KD is not Live, set buttons appropriate to the given time.
      ' Assume that a time of 23:59:59 or later represents the Whole Day.

      Me.Radio_KDLive.Checked = False
      Me.Radio_HistoricKD.Checked = True

      Me.Date_KnowledgeDate.Value = MainForm.Main_Knowledgedate

      If MainForm.Main_Knowledgedate.TimeOfDay.TotalSeconds >= LAST_SECOND Then
        Me.Radio_WholeDay.Checked = True
        Me.Radio_PreciseTime.Checked = False
      Else
        Me.Radio_PreciseTime.Checked = True
        Me.Radio_WholeDay.Checked = False
      End If
    End If

    Call SetButtonStatus()
  End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the Radio_KDType control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Radio_KDType_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_KDLive.CheckedChanged, Radio_HistoricKD.CheckedChanged
    ' *****************************************************************************
    ' Update the form to reflect changes in the Live / Historic KD Radio Buttons.
    ' *****************************************************************************

    If Me.Radio_KDLive.Checked = True Then
      Me.Date_KnowledgeDate.Enabled = False
      Me.Radio_WholeDay.Enabled = False
      Me.Radio_PreciseTime.Enabled = False
    Else
      Me.Date_KnowledgeDate.Enabled = True
      Me.Radio_WholeDay.Enabled = True
      Me.Radio_PreciseTime.Enabled = True
    End If
  End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the Radio_WholeDay control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Radio_WholeDay_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_WholeDay.CheckedChanged, Radio_PreciseTime.CheckedChanged
    ' *****************************************************************************
    ' Update the form to reflect changes in the WholeDaye  / Precise-Time KD Radio Buttons.
    ' *****************************************************************************

    If Me.Radio_WholeDay.Checked = True Then
      Me.Date_KnowledgeDate.CustomFormat = DISPLAYMEMBER_DATEFORMAT
    Else
      Me.Date_KnowledgeDate.CustomFormat = DISPLAYMEMBER_LONGDATEFORMAT
    End If
  End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_MilestoneFund control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_MilestoneFund_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_MilestoneFund.SelectedIndexChanged
    ' *****************************************************************************
    ' If a Milestone Fund is selected, Update the Milestone Date Combo.
    ' *****************************************************************************

    If (Combo_MilestoneFund.SelectedIndex > 0) Then
      Call SetMilestoneDateCombo()
    End If
  End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_MilestoneDate control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_MilestoneDate_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_MilestoneDate.SelectedIndexChanged
    ' *****************************************************************************
    ' If a Milestone Date is selected, update the Displayed Knowledgedate.
    ' *****************************************************************************

    If Combo_MilestoneDate.SelectedIndex > 0 Then
      If IsDate(Combo_MilestoneDate.SelectedValue) Then
        Me.Radio_HistoricKD.Checked = True
        Me.Radio_PreciseTime.Checked = True
        Me.Date_KnowledgeDate.Value = CDate(Combo_MilestoneDate.SelectedValue)
      End If
      Combo_MilestoneDate.SelectedIndex = 0
    End If
  End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_Bookmark control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_Bookmark_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_Bookmark.SelectedIndexChanged
    ' *****************************************************************************
    ' If a Bookmark Date is selected, update the Displayed Knowledgedate.
    ' *****************************************************************************

    If Combo_Bookmark.SelectedIndex > 0 Then
      If IsDate(Combo_Bookmark.SelectedValue) Then
        If CDate(Combo_Bookmark.SelectedValue).TimeOfDay.TotalSeconds >= LAST_SECOND Then
          Me.Radio_HistoricKD.Checked = True
          Me.Radio_PreciseTime.Checked = False
          Me.Radio_WholeDay.Checked = True
          Me.Date_KnowledgeDate.Value = CDate(Combo_Bookmark.SelectedValue)
        Else
          Me.Radio_HistoricKD.Checked = True
          Me.Radio_PreciseTime.Checked = True
          Me.Date_KnowledgeDate.Value = CDate(Combo_Bookmark.SelectedValue)
        End If
      End If
      Combo_Bookmark.SelectedIndex = 0
    End If

  End Sub

#End Region


#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

    ''' <summary>
    ''' Handles the Click event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    ' *****************************************************************************
    ' Cancel Changes, redisplay form.
    ' *****************************************************************************

    FormChanged = False
    Call GetFormData()

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnSetKnowledgeDate control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnSetKnowledgeDate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSetKnowledgeDate.Click
    ' *****************************************************************************
    ' Save Changes, if any, without prompting.
    ' *****************************************************************************

    If (FormChanged = True) Then

      If (Me.Radio_KDLive.Checked = True) Then
        ' LIVE Knowledgedate - By Radio Button

        MainForm.Main_Knowledgedate = KNOWLEDGEDATE_NOW

      Else

        If IsDate(Me.Date_KnowledgeDate.Value) Then
          ' Valid Date field

          If CDate(Me.Date_KnowledgeDate.Value) <= KNOWLEDGEDATE_NOW Then
            ' Date is <= KNOWLEDGEDATE_NOW (1 Jan 1900), Use 'LIVE'

            Me.Radio_KDLive.Checked = True
            MainForm.Main_Knowledgedate = KNOWLEDGEDATE_NOW
          Else
            ' Use given date

            If Me.Radio_WholeDay.Checked = True Then
              ' Set Knowledgedate to the last second of the day.

              MainForm.Main_Knowledgedate = CDate(CDate(Me.Date_KnowledgeDate.Value).ToString(DISPLAYMEMBER_DATEFORMAT) & " 23:59:59")
            Else
              ' Set Knowledgedate to the precise time given.

              MainForm.Main_Knowledgedate = CDate(Me.Date_KnowledgeDate.Value)
            End If
          End If

        Else
          ' Invalid Date Field, use 'LIVE'

          Me.Radio_KDLive.Checked = True
          MainForm.Main_Knowledgedate = KNOWLEDGEDATE_NOW
        End If
      End If

      FormChanged = False
      Call SetButtonStatus()

      Dim MessageString As String
      If MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW Then
        MessageString = "LIVE"
      Else
        If MainForm.Main_Knowledgedate.TimeOfDay.TotalSeconds >= LAST_SECOND Then
          MessageString = MainForm.Main_Knowledgedate.ToString(DISPLAYMEMBER_DATEFORMAT)
        Else
          MessageString = MainForm.Main_Knowledgedate.ToString(DISPLAYMEMBER_LONGDATEFORMAT)
        End If
      End If
      MessageBox.Show("KnowledgeDate changed to " & MessageString, "Changed KnowledgeDate", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)

    End If

  End Sub


    ''' <summary>
    ''' Handles the Click event of the btnClose control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
    ' *****************************************************************************
    ' Close Form
    ' *****************************************************************************

    Me.Close()

  End Sub

#End Region


End Class
