' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmSelectTransaction.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals

Imports RenaissanceDataClass
Imports C1.Win.C1FlexGrid

''' <summary>
''' Class frmSelectTransaction
''' </summary>
Public Class frmSelectTransaction

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmSelectTransaction"/> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The combo_ transaction fund
    ''' </summary>
  Friend WithEvents Combo_TransactionFund As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label_ cpty is fund
    ''' </summary>
  Friend WithEvents label_CptyIsFund As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ transaction instrument
    ''' </summary>
  Friend WithEvents Combo_TransactionInstrument As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label2
    ''' </summary>
  Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label3
    ''' </summary>
  Friend WithEvents Label3 As System.Windows.Forms.Label
    ''' <summary>
    ''' The date_ value date
    ''' </summary>
  Friend WithEvents Date_ValueDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The combo_ value date operator
    ''' </summary>
  Friend WithEvents Combo_ValueDateOperator As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The grid_ transactions
    ''' </summary>
  Friend WithEvents Grid_Transactions As C1.Win.C1FlexGrid.C1FlexGrid
    ''' <summary>
    ''' The combo_ field select1
    ''' </summary>
  Friend WithEvents Combo_FieldSelect1 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select1_ operator
    ''' </summary>
  Friend WithEvents Combo_Select1_Operator As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select1_ value
    ''' </summary>
  Friend WithEvents Combo_Select1_Value As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select2_ value
    ''' </summary>
  Friend WithEvents Combo_Select2_Value As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ field select2
    ''' </summary>
  Friend WithEvents Combo_FieldSelect2 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select2_ operator
    ''' </summary>
  Friend WithEvents Combo_Select2_Operator As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select3_ value
    ''' </summary>
  Friend WithEvents Combo_Select3_Value As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ field select3
    ''' </summary>
  Friend WithEvents Combo_FieldSelect3 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select3_ operator
    ''' </summary>
  Friend WithEvents Combo_Select3_Operator As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ and or_1
    ''' </summary>
  Friend WithEvents Combo_AndOr_1 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ and or_2
    ''' </summary>
  Friend WithEvents Combo_AndOr_2 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The root menu
    ''' </summary>
  Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
    ''' <summary>
    ''' The form_ status strip
    ''' </summary>
  Friend WithEvents Form_StatusStrip As System.Windows.Forms.StatusStrip
    ''' <summary>
    ''' The form_ progress bar
    ''' </summary>
  Friend WithEvents Form_ProgressBar As System.Windows.Forms.ToolStripProgressBar
    ''' <summary>
    ''' The label_ status
    ''' </summary>
  Friend WithEvents Label_Status As System.Windows.Forms.ToolStripStatusLabel
    ''' <summary>
    ''' The combo_ value date operator2
    ''' </summary>
  Friend WithEvents Combo_ValueDateOperator2 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The date_ value date2
    ''' </summary>
  Friend WithEvents Date_ValueDate2 As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The combo_ trade status group
    ''' </summary>
  Friend WithEvents Combo_TradeStatusGroup As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label1
    ''' </summary>
  Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ trade status operator
    ''' </summary>
  Friend WithEvents Combo_TradeStatusOperator As System.Windows.Forms.ComboBox
  Friend WithEvents Check_ByISIN As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ incomplete transactions
    ''' </summary>
  Friend WithEvents Check_IncompleteTransactions As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.Grid_Transactions = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.Combo_TransactionFund = New System.Windows.Forms.ComboBox
    Me.label_CptyIsFund = New System.Windows.Forms.Label
    Me.Combo_TransactionInstrument = New System.Windows.Forms.ComboBox
    Me.Label2 = New System.Windows.Forms.Label
    Me.Label3 = New System.Windows.Forms.Label
    Me.Date_ValueDate = New System.Windows.Forms.DateTimePicker
    Me.Combo_ValueDateOperator = New System.Windows.Forms.ComboBox
    Me.Combo_Select1_Operator = New System.Windows.Forms.ComboBox
    Me.Combo_FieldSelect1 = New System.Windows.Forms.ComboBox
    Me.Combo_Select1_Value = New System.Windows.Forms.ComboBox
    Me.Combo_Select2_Value = New System.Windows.Forms.ComboBox
    Me.Combo_FieldSelect2 = New System.Windows.Forms.ComboBox
    Me.Combo_Select2_Operator = New System.Windows.Forms.ComboBox
    Me.Combo_Select3_Value = New System.Windows.Forms.ComboBox
    Me.Combo_FieldSelect3 = New System.Windows.Forms.ComboBox
    Me.Combo_Select3_Operator = New System.Windows.Forms.ComboBox
    Me.Combo_AndOr_1 = New System.Windows.Forms.ComboBox
    Me.Combo_AndOr_2 = New System.Windows.Forms.ComboBox
    Me.Check_IncompleteTransactions = New System.Windows.Forms.CheckBox
    Me.RootMenu = New System.Windows.Forms.MenuStrip
    Me.Form_StatusStrip = New System.Windows.Forms.StatusStrip
    Me.Form_ProgressBar = New System.Windows.Forms.ToolStripProgressBar
    Me.Label_Status = New System.Windows.Forms.ToolStripStatusLabel
    Me.Combo_ValueDateOperator2 = New System.Windows.Forms.ComboBox
    Me.Date_ValueDate2 = New System.Windows.Forms.DateTimePicker
    Me.Combo_TradeStatusGroup = New System.Windows.Forms.ComboBox
    Me.Label1 = New System.Windows.Forms.Label
    Me.Combo_TradeStatusOperator = New System.Windows.Forms.ComboBox
    Me.Check_ByISIN = New System.Windows.Forms.CheckBox
    CType(Me.Grid_Transactions, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Form_StatusStrip.SuspendLayout()
    Me.SuspendLayout()
    '
    'Grid_Transactions
    '
    Me.Grid_Transactions.AllowEditing = False
    Me.Grid_Transactions.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_Transactions.AutoClipboard = True
    Me.Grid_Transactions.CausesValidation = False
    Me.Grid_Transactions.ColumnInfo = "5,1,0,0,0,85,Columns:"
    Me.Grid_Transactions.EditOptions = C1.Win.C1FlexGrid.EditFlags.None
    Me.Grid_Transactions.KeyActionTab = C1.Win.C1FlexGrid.KeyActionEnum.MoveAcross
    Me.Grid_Transactions.Location = New System.Drawing.Point(4, 242)
    Me.Grid_Transactions.Name = "Grid_Transactions"
    Me.Grid_Transactions.Rows.DefaultSize = 17
    Me.Grid_Transactions.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.ListBox
    Me.Grid_Transactions.Size = New System.Drawing.Size(880, 353)
    Me.Grid_Transactions.TabIndex = 18
    '
    'Combo_TransactionFund
    '
    Me.Combo_TransactionFund.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_TransactionFund.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_TransactionFund.Location = New System.Drawing.Point(128, 32)
    Me.Combo_TransactionFund.Name = "Combo_TransactionFund"
    Me.Combo_TransactionFund.Size = New System.Drawing.Size(752, 21)
    Me.Combo_TransactionFund.TabIndex = 0
    '
    'label_CptyIsFund
    '
    Me.label_CptyIsFund.Location = New System.Drawing.Point(16, 36)
    Me.label_CptyIsFund.Name = "label_CptyIsFund"
    Me.label_CptyIsFund.Size = New System.Drawing.Size(104, 16)
    Me.label_CptyIsFund.TabIndex = 82
    Me.label_CptyIsFund.Text = "Fund"
    '
    'Combo_TransactionInstrument
    '
    Me.Combo_TransactionInstrument.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_TransactionInstrument.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_TransactionInstrument.Location = New System.Drawing.Point(128, 60)
    Me.Combo_TransactionInstrument.Name = "Combo_TransactionInstrument"
    Me.Combo_TransactionInstrument.Size = New System.Drawing.Size(700, 21)
    Me.Combo_TransactionInstrument.TabIndex = 1
    '
    'Label2
    '
    Me.Label2.Location = New System.Drawing.Point(16, 64)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(104, 16)
    Me.Label2.TabIndex = 84
    Me.Label2.Text = "Instrument"
    '
    'Label3
    '
    Me.Label3.Location = New System.Drawing.Point(16, 93)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(104, 16)
    Me.Label3.TabIndex = 88
    Me.Label3.Text = "NAV Date"
    '
    'Date_ValueDate
    '
    Me.Date_ValueDate.CustomFormat = "dd MMM yyyy"
    Me.Date_ValueDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
    Me.Date_ValueDate.Location = New System.Drawing.Point(228, 89)
    Me.Date_ValueDate.Name = "Date_ValueDate"
    Me.Date_ValueDate.Size = New System.Drawing.Size(174, 20)
    Me.Date_ValueDate.TabIndex = 5
    '
    'Combo_ValueDateOperator
    '
    Me.Combo_ValueDateOperator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_ValueDateOperator.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_ValueDateOperator.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", ""})
    Me.Combo_ValueDateOperator.Location = New System.Drawing.Point(128, 89)
    Me.Combo_ValueDateOperator.Name = "Combo_ValueDateOperator"
    Me.Combo_ValueDateOperator.Size = New System.Drawing.Size(96, 21)
    Me.Combo_ValueDateOperator.TabIndex = 4
    '
    'Combo_Select1_Operator
    '
    Me.Combo_Select1_Operator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_Select1_Operator.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Select1_Operator.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like"})
    Me.Combo_Select1_Operator.Location = New System.Drawing.Point(272, 148)
    Me.Combo_Select1_Operator.Name = "Combo_Select1_Operator"
    Me.Combo_Select1_Operator.Size = New System.Drawing.Size(96, 21)
    Me.Combo_Select1_Operator.TabIndex = 7
    '
    'Combo_FieldSelect1
    '
    Me.Combo_FieldSelect1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_FieldSelect1.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_FieldSelect1.Location = New System.Drawing.Point(128, 148)
    Me.Combo_FieldSelect1.Name = "Combo_FieldSelect1"
    Me.Combo_FieldSelect1.Size = New System.Drawing.Size(136, 21)
    Me.Combo_FieldSelect1.Sorted = True
    Me.Combo_FieldSelect1.TabIndex = 6
    '
    'Combo_Select1_Value
    '
    Me.Combo_Select1_Value.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Select1_Value.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Select1_Value.Location = New System.Drawing.Point(376, 148)
    Me.Combo_Select1_Value.Name = "Combo_Select1_Value"
    Me.Combo_Select1_Value.Size = New System.Drawing.Size(216, 21)
    Me.Combo_Select1_Value.TabIndex = 8
    '
    'Combo_Select2_Value
    '
    Me.Combo_Select2_Value.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Select2_Value.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Select2_Value.Location = New System.Drawing.Point(376, 180)
    Me.Combo_Select2_Value.Name = "Combo_Select2_Value"
    Me.Combo_Select2_Value.Size = New System.Drawing.Size(216, 21)
    Me.Combo_Select2_Value.TabIndex = 12
    '
    'Combo_FieldSelect2
    '
    Me.Combo_FieldSelect2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_FieldSelect2.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_FieldSelect2.Location = New System.Drawing.Point(128, 180)
    Me.Combo_FieldSelect2.Name = "Combo_FieldSelect2"
    Me.Combo_FieldSelect2.Size = New System.Drawing.Size(136, 21)
    Me.Combo_FieldSelect2.Sorted = True
    Me.Combo_FieldSelect2.TabIndex = 10
    '
    'Combo_Select2_Operator
    '
    Me.Combo_Select2_Operator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_Select2_Operator.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Select2_Operator.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like"})
    Me.Combo_Select2_Operator.Location = New System.Drawing.Point(272, 180)
    Me.Combo_Select2_Operator.Name = "Combo_Select2_Operator"
    Me.Combo_Select2_Operator.Size = New System.Drawing.Size(96, 21)
    Me.Combo_Select2_Operator.TabIndex = 11
    '
    'Combo_Select3_Value
    '
    Me.Combo_Select3_Value.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Select3_Value.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Select3_Value.Location = New System.Drawing.Point(376, 212)
    Me.Combo_Select3_Value.Name = "Combo_Select3_Value"
    Me.Combo_Select3_Value.Size = New System.Drawing.Size(216, 21)
    Me.Combo_Select3_Value.TabIndex = 16
    '
    'Combo_FieldSelect3
    '
    Me.Combo_FieldSelect3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_FieldSelect3.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_FieldSelect3.Location = New System.Drawing.Point(128, 212)
    Me.Combo_FieldSelect3.Name = "Combo_FieldSelect3"
    Me.Combo_FieldSelect3.Size = New System.Drawing.Size(136, 21)
    Me.Combo_FieldSelect3.Sorted = True
    Me.Combo_FieldSelect3.TabIndex = 14
    '
    'Combo_Select3_Operator
    '
    Me.Combo_Select3_Operator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_Select3_Operator.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Select3_Operator.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like"})
    Me.Combo_Select3_Operator.Location = New System.Drawing.Point(272, 212)
    Me.Combo_Select3_Operator.Name = "Combo_Select3_Operator"
    Me.Combo_Select3_Operator.Size = New System.Drawing.Size(96, 21)
    Me.Combo_Select3_Operator.TabIndex = 15
    '
    'Combo_AndOr_1
    '
    Me.Combo_AndOr_1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_AndOr_1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_AndOr_1.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_AndOr_1.Items.AddRange(New Object() {"AND", "OR"})
    Me.Combo_AndOr_1.Location = New System.Drawing.Point(604, 148)
    Me.Combo_AndOr_1.Name = "Combo_AndOr_1"
    Me.Combo_AndOr_1.Size = New System.Drawing.Size(96, 21)
    Me.Combo_AndOr_1.TabIndex = 9
    '
    'Combo_AndOr_2
    '
    Me.Combo_AndOr_2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_AndOr_2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_AndOr_2.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_AndOr_2.Items.AddRange(New Object() {"AND", "OR"})
    Me.Combo_AndOr_2.Location = New System.Drawing.Point(604, 180)
    Me.Combo_AndOr_2.Name = "Combo_AndOr_2"
    Me.Combo_AndOr_2.Size = New System.Drawing.Size(96, 21)
    Me.Combo_AndOr_2.TabIndex = 13
    '
    'Check_IncompleteTransactions
    '
    Me.Check_IncompleteTransactions.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_IncompleteTransactions.Location = New System.Drawing.Point(604, 214)
    Me.Check_IncompleteTransactions.Name = "Check_IncompleteTransactions"
    Me.Check_IncompleteTransactions.Size = New System.Drawing.Size(136, 16)
    Me.Check_IncompleteTransactions.TabIndex = 17
    Me.Check_IncompleteTransactions.Text = "Select Only Incomplete Transactions"
    Me.Check_IncompleteTransactions.Visible = False
    '
    'RootMenu
    '
    Me.RootMenu.Location = New System.Drawing.Point(0, 0)
    Me.RootMenu.Name = "RootMenu"
    Me.RootMenu.Size = New System.Drawing.Size(888, 24)
    Me.RootMenu.TabIndex = 104
    Me.RootMenu.Text = " "
    '
    'Form_StatusStrip
    '
    Me.Form_StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Form_ProgressBar, Me.Label_Status})
    Me.Form_StatusStrip.Location = New System.Drawing.Point(0, 600)
    Me.Form_StatusStrip.Name = "Form_StatusStrip"
    Me.Form_StatusStrip.Size = New System.Drawing.Size(888, 22)
    Me.Form_StatusStrip.TabIndex = 105
    Me.Form_StatusStrip.Text = " "
    '
    'Form_ProgressBar
    '
    Me.Form_ProgressBar.Maximum = 20
    Me.Form_ProgressBar.Name = "Form_ProgressBar"
    Me.Form_ProgressBar.Size = New System.Drawing.Size(150, 17)
    Me.Form_ProgressBar.Step = 1
    Me.Form_ProgressBar.Visible = False
    '
    'Label_Status
    '
    Me.Label_Status.Name = "Label_Status"
    Me.Label_Status.Size = New System.Drawing.Size(10, 17)
    Me.Label_Status.Text = " "
    '
    'Combo_ValueDateOperator2
    '
    Me.Combo_ValueDateOperator2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_ValueDateOperator2.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_ValueDateOperator2.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", ""})
    Me.Combo_ValueDateOperator2.Location = New System.Drawing.Point(426, 89)
    Me.Combo_ValueDateOperator2.Name = "Combo_ValueDateOperator2"
    Me.Combo_ValueDateOperator2.Size = New System.Drawing.Size(96, 21)
    Me.Combo_ValueDateOperator2.TabIndex = 106
    '
    'Date_ValueDate2
    '
    Me.Date_ValueDate2.CustomFormat = "dd MMM yyyy"
    Me.Date_ValueDate2.Format = System.Windows.Forms.DateTimePickerFormat.Custom
    Me.Date_ValueDate2.Location = New System.Drawing.Point(526, 89)
    Me.Date_ValueDate2.Name = "Date_ValueDate2"
    Me.Date_ValueDate2.Size = New System.Drawing.Size(174, 20)
    Me.Date_ValueDate2.TabIndex = 107
    '
    'Combo_TradeStatusGroup
    '
    Me.Combo_TradeStatusGroup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_TradeStatusGroup.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_TradeStatusGroup.Location = New System.Drawing.Point(228, 119)
    Me.Combo_TradeStatusGroup.Name = "Combo_TradeStatusGroup"
    Me.Combo_TradeStatusGroup.Size = New System.Drawing.Size(652, 21)
    Me.Combo_TradeStatusGroup.TabIndex = 108
    '
    'Label1
    '
    Me.Label1.Location = New System.Drawing.Point(16, 123)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(104, 16)
    Me.Label1.TabIndex = 109
    Me.Label1.Text = "Trade Status Group"
    '
    'Combo_TradeStatusOperator
    '
    Me.Combo_TradeStatusOperator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_TradeStatusOperator.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_TradeStatusOperator.Items.AddRange(New Object() {"", "IN", "NOT IN"})
    Me.Combo_TradeStatusOperator.Location = New System.Drawing.Point(128, 118)
    Me.Combo_TradeStatusOperator.Name = "Combo_TradeStatusOperator"
    Me.Combo_TradeStatusOperator.Size = New System.Drawing.Size(96, 21)
    Me.Combo_TradeStatusOperator.TabIndex = 110
    '
    'Check_ByISIN
    '
    Me.Check_ByISIN.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Check_ByISIN.BackColor = System.Drawing.SystemColors.Control
    Me.Check_ByISIN.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_ByISIN.Location = New System.Drawing.Point(834, 62)
    Me.Check_ByISIN.Name = "Check_ByISIN"
    Me.Check_ByISIN.Size = New System.Drawing.Size(46, 19)
    Me.Check_ByISIN.TabIndex = 154
    Me.Check_ByISIN.Text = "ISIN"
    Me.Check_ByISIN.UseVisualStyleBackColor = False
    '
    'frmSelectTransaction
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(888, 622)
    Me.Controls.Add(Me.Check_ByISIN)
    Me.Controls.Add(Me.Combo_TradeStatusOperator)
    Me.Controls.Add(Me.Combo_TradeStatusGroup)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.Combo_ValueDateOperator2)
    Me.Controls.Add(Me.Date_ValueDate2)
    Me.Controls.Add(Me.Form_StatusStrip)
    Me.Controls.Add(Me.RootMenu)
    Me.Controls.Add(Me.Check_IncompleteTransactions)
    Me.Controls.Add(Me.Combo_AndOr_2)
    Me.Controls.Add(Me.Combo_AndOr_1)
    Me.Controls.Add(Me.Combo_Select3_Value)
    Me.Controls.Add(Me.Combo_FieldSelect3)
    Me.Controls.Add(Me.Combo_Select3_Operator)
    Me.Controls.Add(Me.Combo_Select2_Value)
    Me.Controls.Add(Me.Combo_FieldSelect2)
    Me.Controls.Add(Me.Combo_Select2_Operator)
    Me.Controls.Add(Me.Combo_Select1_Value)
    Me.Controls.Add(Me.Combo_FieldSelect1)
    Me.Controls.Add(Me.Combo_Select1_Operator)
    Me.Controls.Add(Me.Combo_ValueDateOperator)
    Me.Controls.Add(Me.Date_ValueDate)
    Me.Controls.Add(Me.Label3)
    Me.Controls.Add(Me.Combo_TransactionInstrument)
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.Combo_TransactionFund)
    Me.Controls.Add(Me.label_CptyIsFund)
    Me.Controls.Add(Me.Grid_Transactions)
    Me.Name = "frmSelectTransaction"
    Me.Text = "Select Transactions"
    CType(Me.Grid_Transactions, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Form_StatusStrip.ResumeLayout(False)
    Me.Form_StatusStrip.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "

  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
  Private WithEvents _MainForm As VeniceMain

  ' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
  Private FormTooltip As New ToolTip()

  ' Form Menu


  ' Form Constants, specific to the table being updated.

    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
  Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

  ' Form Locals, initialised on 'New' defining what standard data items to use
    ''' <summary>
  ''' Variable holding the form's data table name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_TABLENAME As String
    ''' <summary>
  ''' Variable holding the form's data adaptor name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_ADAPTORNAME As String
    ''' <summary>
  ''' Variable holding the form's dataset name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_DATASETNAME As String


  ' The standard ChangeID for this form. e.g. tblFund
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

  ' Form Specific Order fields
    ''' <summary>
  ''' Field Name to order the Select combo by.
    ''' </summary>
  Private THIS_FORM_OrderBy As String

  ' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
  Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
  Private THIS_FORM_FormID As VeniceFormID

  ' Data Structures

    ''' <summary>
    ''' My dataset
    ''' </summary>
  Private myDataset As DataSet
    ''' <summary>
    ''' My table
    ''' </summary>
  Private myTable As DataTable
    ''' <summary>
    ''' My connection
    ''' </summary>
  Private myConnection As SqlConnection
    ''' <summary>
    ''' My adaptor
    ''' </summary>
  Private myAdaptor As SqlDataAdapter
    ''' <summary>
    ''' My data view
    ''' </summary>
  Private myDataView As DataView

    ''' <summary>
    ''' The this standard dataset
    ''' </summary>
  Private ThisStandardDataset As RenaissanceGlobals.StandardDataset

  ' Active Element.

    ''' <summary>
    ''' The __ is over cancel button
    ''' </summary>
  Private __IsOverCancelButton As Boolean
    ''' <summary>
    ''' The _ in use
    ''' </summary>
  Private _InUse As Boolean

  ' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
  Private FormIsValid As Boolean
    ''' <summary>
    ''' The in paint
    ''' </summary>
  Private InPaint As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
  Private _FormOpenFailed As Boolean

  ' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
  Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
  Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
  Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
  Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
  Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
    ' Public property to return handle to the 'Main' Venice form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
  Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
    ' Public property maintaining a value indicating if the cursor is over the 'Cancel'
    ' Button on this form.
    ' This property is specifically designed for use by the field formating Event functions
    ' In order that they do not impose format restrictions if the user is about to click the 
    ' 'Cancel' button.
    '
    Get
      Return __IsOverCancelButton
    End Get
    Set(ByVal Value As Boolean)
      __IsOverCancelButton = Value
    End Set
  End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
  Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
    Get
      Return InPaint
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
    Get
      Return _InUse
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmSelectTransaction"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  Public Sub New(ByVal pMainForm As VeniceMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()


    _MainForm = pMainForm
    AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

    _FormOpenFailed = False
    _InUse = True

    ' Default Select and Order fields.

    THIS_FORM_OrderBy = "TransactionFund, TransactionInstrument"

    ' Form Permissioning :-

    THIS_FORM_PermissionArea = Me.Name
    THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

    ' 'This' form ID

    THIS_FORM_FormID = VeniceFormID.frmSelectTransaction

    ' This form's dataset type.

    ThisStandardDataset = RenaissanceStandardDatasets.tblSelectTransaction ' This Defines the Form Data !!! 


    ' Data object names standard to this Form type.

    THIS_TABLENAME = ThisStandardDataset.TableName
    THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
    THIS_DATASETNAME = ThisStandardDataset.DatasetName

    THIS_FORM_ChangeID = ThisStandardDataset.ChangeID


    ' Establish / Retrieve data objects for this form.

    myConnection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)
    myAdaptor = MainForm.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, VENICE_CONNECTION, THIS_TABLENAME)
    myDataset = MainForm.Load_Table(ThisStandardDataset, False)
    myTable = myDataset.Tables(0)

    ' Report
    SetTransactionReportMenu(RootMenu)

    ' Grid Fields
    Dim FieldsMenu As ToolStripMenuItem = SetFieldSelectMenu(RootMenu)

    ' Establish initial DataView and Initialise Transactions Grig.

    Dim thisCol As C1.Win.C1FlexGrid.Column
    Dim Counter As Integer

    Try
      myDataView = New DataView(myTable, "False", "", DataViewRowState.CurrentRows)

      Grid_Transactions.DataSource = myDataView

      ' Format Transactions Grid, hide unwanted columns.

      Grid_Transactions.Cols("TransactionTicket").Move(1)
      Grid_Transactions.Cols("FundName").Move(2)
      Grid_Transactions.Cols("InstrumentDescription").Move(3)
      Grid_Transactions.Cols("TransactionTypeName").Move(4)
      Grid_Transactions.Cols("TransactionUnits").Move(5)
      Grid_Transactions.Cols("TransactionPrice").Move(6)
      Grid_Transactions.Cols("TransactionSettlement").Move(7)
      Grid_Transactions.Cols("TransactionValueDate").Move(8)
      Grid_Transactions.Cols("TradeStatusName").Move(9)
      Grid_Transactions.Cols("SpecificFundUnitName").Move(10)
    Catch ex As Exception
    End Try

    For Each thisCol In Grid_Transactions.Cols
      Try
        Select Case thisCol.Name
          Case "TransactionTicket"
          Case "FundName"
          Case "InstrumentDescription"
          Case "TransactionTypeName"
          Case "TransactionUnits"
            thisCol.Format = "#,##0.0000"
          Case "TransactionPrice"
            thisCol.Format = "#,##0.0000"
          Case "TransactionSettlement"
            thisCol.Format = "#,##0.0000"
          Case "TransactionValueDate"
            thisCol.Format = DISPLAYMEMBER_DATEFORMAT
          Case "TradeStatusName"

          Case "SpecificFundUnitName"

          Case Else
            thisCol.Visible = False

            Try
              If thisCol.DataType Is GetType(Date) Then
                thisCol.Format = DISPLAYMEMBER_DATEFORMAT
              ElseIf thisCol.DataType Is GetType(Integer) Then
                thisCol.Format = "#,##0"
              ElseIf thisCol.DataType Is GetType(Double) Then
                thisCol.Format = "#,##0.0000"
              End If
            Catch ex As Exception
            End Try
        End Select

        If (thisCol.Caption.ToUpper.StartsWith("TRANSACTION")) Then
          thisCol.Caption = thisCol.Caption.Substring(11)
        End If

        If (thisCol.Visible) Then
          Try
            CType(FieldsMenu.DropDownItems("Menu_GridField_" & thisCol.Name), ToolStripMenuItem).Checked = True
          Catch ex As Exception
          End Try
        End If
      Catch ex As Exception
      End Try
    Next

    Try
      For Counter = 0 To (Grid_Transactions.Cols.Count - 1)
        If (Grid_Transactions.Cols(Counter).Caption.ToUpper.StartsWith("TRANSACTION")) Then
          Grid_Transactions.Cols(Counter).Caption = Grid_Transactions.Cols(Counter).Caption.Substring(11)
        End If
      Next
    Catch ex As Exception
    End Try

    ' Initialise Field select area

    Try
      Call BuildFieldSelectCombos()
      Me.Combo_AndOr_1.SelectedIndex = 0
      Me.Combo_AndOr_2.SelectedIndex = 0
    Catch ex As Exception
    End Try

    ' Form Control Changed events
    AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

    AddHandler Combo_TransactionFund.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_TransactionInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_TradeStatusGroup.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_ValueDateOperator.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_ValueDateOperator2.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_TradeStatusOperator.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Date_ValueDate.ValueChanged, AddressOf Me.FormControlChanged
    AddHandler Date_ValueDate2.ValueChanged, AddressOf Me.FormControlChanged

    AddHandler Combo_Select1_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Select1_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Select1_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp
    AddHandler Combo_Select2_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Select2_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Select2_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp
    AddHandler Combo_Select3_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Select3_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Select3_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp

    AddHandler Combo_AndOr_1.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_AndOr_2.SelectedValueChanged, AddressOf Me.FormControlChanged

    AddHandler Check_IncompleteTransactions.CheckedChanged, AddressOf Me.FormControlChanged

    AddHandler Combo_TransactionFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_TransactionInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_TradeStatusGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_ValueDateOperator.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_ValueDateOperator2.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_TradeStatusOperator.KeyUp, AddressOf MainForm.ComboSelectAsYouType

    AddHandler Combo_TransactionFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_TransactionInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_TradeStatusGroup.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_ValueDateOperator.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_ValueDateOperator2.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_TradeStatusOperator.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

    AddHandler Combo_TransactionFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_TransactionInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_TradeStatusGroup.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_ValueDateOperator.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_ValueDateOperator2.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_TradeStatusOperator.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

    AddHandler Combo_TransactionFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_TransactionInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_TradeStatusGroup.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_ValueDateOperator.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_ValueDateOperator2.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_TradeStatusOperator.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

    ' Set up the ToolTip
    MainForm.SetFormToolTip(Me, FormTooltip)

  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
  Public Sub ResetForm() Implements StandardVeniceForm.ResetForm
    THIS_FORM_OrderBy = "TransactionFund, TransactionInstrument"

    Call Form_Load(Me, New System.EventArgs)
  End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
  Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
    ALWAYS_CLOSE_THIS_FORM = True
    Me.Close()
  End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
    _FormOpenFailed = False
    _InUse = True

    ' Initialise Data structures. Connection, Adaptor and Dataset.

    If Not (MainForm Is Nothing) Then
      FormIsValid = True
    Else
      MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (myConnection Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (myAdaptor Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (myDataset Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If


    ' Check User permissions
    Try
      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

        FormIsValid = False
        _FormOpenFailed = True

        Exit Sub
      End If
    Catch ex As Exception
      FormIsValid = False
      _FormOpenFailed = True

      Exit Sub
    End Try

    ' Initialse form

    InPaint = True
    IsOverCancelButton = False

    ' Initialise main select controls
    ' Build Sorted data list from which this form operates

    Try
      Call SetFundCombo()
      Call SetTradeStatusGroupCombo()

      If (Combo_TransactionFund.Items.Count > 1) Then
        Me.Combo_TransactionFund.SelectedIndex = 1
      Else
        Me.Combo_TransactionFund.SelectedIndex = -1
      End If

      Call SetInstrumentCombo()

      Me.Combo_TransactionInstrument.SelectedIndex = -1
      Me.Combo_ValueDateOperator.SelectedIndex = 6 ' >=
      Me.Date_ValueDate.Value = RenaissanceUtilities.DatePeriodFunctions.FitDateToPeriod(DealingPeriod.Weekly, RenaissanceUtilities.DatePeriodFunctions.AddPeriodToDate(DealingPeriod.Weekly, Now.Date(), -1), False)
      Me.Combo_ValueDateOperator2.SelectedIndex = 4 ' <=
      Me.Date_ValueDate2.Value = RenaissanceUtilities.DatePeriodFunctions.FitDateToPeriod(DealingPeriod.Weekly, Now.Date())
      Me.Combo_TradeStatusOperator.SelectedIndex = 0
      Me.Combo_TradeStatusGroup.SelectedIndex = -1

      Me.Check_IncompleteTransactions.Checked = False

      Me.Combo_FieldSelect1.SelectedIndex = 0
      Me.Combo_FieldSelect2.SelectedIndex = 0
      Me.Combo_FieldSelect3.SelectedIndex = 0

      ' Default Grid Sort : By Value Date and ParentID (Effectively Entry Order)
      For Each thisCol As C1.Win.C1FlexGrid.Column In Grid_Transactions.Cols
        thisCol.Sort = SortFlags.None
      Next
      Grid_Transactions.Cols("TransactionValueDate").Sort = SortFlags.Ascending
      Grid_Transactions.Cols("TransactionParentID").Sort = SortFlags.Ascending
      Grid_Transactions.Sort(SortFlags.UseColSort, Grid_Transactions.Cols("TransactionValueDate").Index, Grid_Transactions.Cols("TransactionParentID").Index)

      Call SetSortedRows()

      Call MainForm.SetComboSelectionLengths(Me)
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error initialising Select Transaction Form.", ex.StackTrace, True)
    End Try

    InPaint = False

  End Sub

    ''' <summary>
    ''' Handles the Closing event of the frm control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
  Private Sub frm_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    Dim HideForm As Boolean

    ' Hide or Close this form ?
    ' All depends on how many of this form type are Open or in Cache...

    _InUse = False

    If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
      HideForm = False
    Else
      HideForm = True
      If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
        HideForm = False
      End If
    End If

    If HideForm = True Then
      MainForm.HideInFormsCollection(Me)
      Me.Hide() ' NPP Fix

      e.Cancel = True
    Else
      Try
        MainForm.RemoveFromFormsCollection(Me)
        RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

        RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

        RemoveHandler Combo_TransactionFund.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_TransactionInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_TradeStatusGroup.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_ValueDateOperator.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Date_ValueDate.ValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Date_ValueDate2.ValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_TradeStatusOperator.SelectedValueChanged, AddressOf Me.FormControlChanged

        RemoveHandler Combo_Select1_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_Select1_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_Select1_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp
        RemoveHandler Combo_Select2_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_Select2_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_Select2_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp
        RemoveHandler Combo_Select3_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_Select3_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_Select3_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp

        RemoveHandler Combo_AndOr_1.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_AndOr_2.SelectedValueChanged, AddressOf Me.FormControlChanged

        RemoveHandler Check_IncompleteTransactions.CheckedChanged, AddressOf Me.FormControlChanged

        RemoveHandler Combo_TransactionFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_TransactionInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_TradeStatusGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_ValueDateOperator.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_ValueDateOperator2.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_TradeStatusOperator.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        RemoveHandler Combo_TransactionFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_TransactionInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_TradeStatusGroup.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_ValueDateOperator.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_ValueDateOperator2.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_TradeStatusOperator.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

        RemoveHandler Combo_TransactionFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_TransactionInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_TradeStatusGroup.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_ValueDateOperator.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_ValueDateOperator2.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_TradeStatusOperator.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

        RemoveHandler Combo_TransactionFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_TransactionInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_TradeStatusGroup.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_ValueDateOperator.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_ValueDateOperator2.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_TradeStatusOperator.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

      Catch ex As Exception
      End Try
    End If

  End Sub

  ''' <summary>
  ''' Handles the CheckedChanged event of the Check_ByISIN control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Check_ByISIN_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_ByISIN.CheckedChanged
    ' *******************************************************************************
    '
    ' *******************************************************************************

    If (InPaint = False) Then
      Try
        Dim ExistingID As Integer = (-1)

        If (Me.Combo_TransactionInstrument.SelectedIndex >= 0) AndAlso (CInt(Me.Combo_TransactionInstrument.SelectedValue) > 0) Then
          ExistingID = CInt(Me.Combo_TransactionInstrument.SelectedValue)
        End If

        InPaint = True

        Call SetInstrumentCombo()

        If Combo_TransactionInstrument.Items.Count > 0 Then
          If (ExistingID > 0) Then
            Combo_TransactionInstrument.SelectedValue = ExistingID
          Else
            Combo_TransactionInstrument.SelectedIndex = 0
          End If
        End If

      Catch ex As Exception
        Call MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Check_ByISIN_CheckedChanged()", ex.StackTrace, True)
      Finally
        InPaint = False
      End Try
    End If

  End Sub

#End Region

  ' Routine to handle changes / updates to tables by this and other windows.
  ' If this, or any other, form posts a change to a table, then it will invoke an update event 
  ' detailing what tables have been altered.
  ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
  ' the 'VeniceAutoUpdate' event of the main Venice form.
  ' Each form may them react as appropriate to changes in any table that might impact it.
  '
    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    Dim OrgInPaint As Boolean
    Dim KnowledgeDateChanged As Boolean
    Dim RefreshGrid As Boolean

    If (Not Me.Created) OrElse (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then
      Exit Sub
    End If

    OrgInPaint = InPaint

    Try
      InPaint = True
      KnowledgeDateChanged = False
      RefreshGrid = False

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
        KnowledgeDateChanged = True
      End If
    Catch ex As Exception
    End Try

    ' ****************************************************************
    ' Check for changes relevant to this form
    ' ****************************************************************


    ' Changes to the tblFund table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFund) = True) Or KnowledgeDateChanged Then
        Call SetFundCombo()
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblFund", ex.StackTrace, True)
    End Try

    ' Changes to the tblInstrument table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrument) = True) Or KnowledgeDateChanged Then
        Call SetInstrumentCombo()
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblInstrument", ex.StackTrace, True)
    End Try

    ' Changes to the tblTradeStatusGroup table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblTradeStatusGroup) = True) Or KnowledgeDateChanged Then
        Call SetTradeStatusGroupCombo()
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblTradeStatusGroup", ex.StackTrace, True)
    End Try

    ' Changes to the tblTransaction table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblTransaction) = True) Or KnowledgeDateChanged Then
        ' RefreshGrid = True ' Grid will be refreshed by subsequent tblSelectTransactions Update.
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblTransaction", ex.StackTrace, True)
    End Try

    ' Changes to the KnowledgeDate :-
    Try
      If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
        RefreshGrid = True
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: KnowledgeDate", ex.StackTrace, True)
    End Try

    ' Check TableUpdate against first Select Field
    Try
      If Me.Combo_FieldSelect1.SelectedIndex >= 0 Then
        Try

          ' If the First FieldSelect combo has selected a Transaction Field which is related to
          ' another table, as defined in tblReferentialIntegrity (e.g. TransactionCounterparty)
          ' Then if that related table is updated, the FieldSelect-Values combo must be updated.

          ' If ChangedID is ChangeID of related table then ,..

          If (e.TableChanged(GetFieldChangeID(Combo_FieldSelect1.SelectedItem)) = True) Or KnowledgeDateChanged Then
            Dim SelectedValue As Object
            Dim TextValue As String

            ' Save current Value Combo Value.

            SelectedValue = Nothing
            TextValue = ""

            If Me.Combo_Select1_Value.SelectedIndex >= 0 Then
              SelectedValue = Me.Combo_Select1_Value.SelectedValue
            ElseIf (SelectedValue Is Nothing) Then
              TextValue = Me.Combo_Select1_Value.Text
            End If

            ' Update FieldSelect-Values Combo.

            Call UpdateSelectedValueCombo(Me.Combo_FieldSelect1.SelectedItem, Me.Combo_Select1_Value)

            ' Restore Saved Value.

            If (Not (SelectedValue Is Nothing)) Then
              Me.Combo_Select1_Value.SelectedValue = SelectedValue
            ElseIf TextValue.Length > 0 Then
              Me.Combo_Select1_Value.Text = TextValue
            End If
          End If
        Catch ex As Exception
        End Try
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: First SelectField", ex.StackTrace, True)
    End Try

    ' Check TableUpdate against second Select Field
    Try
      If Me.Combo_FieldSelect2.SelectedIndex >= 0 Then
        Try
          ' GetFieldChangeID
          If (e.TableChanged(GetFieldChangeID(Combo_FieldSelect2.SelectedItem)) = True) Or KnowledgeDateChanged Then
            Dim SelectedValue As Object
            Dim TextValue As String

            SelectedValue = Nothing
            TextValue = ""

            If Me.Combo_Select2_Value.SelectedIndex >= 0 Then
              SelectedValue = Me.Combo_Select2_Value.SelectedValue
            ElseIf (SelectedValue Is Nothing) Then
              TextValue = Me.Combo_Select2_Value.Text
            End If

            Call UpdateSelectedValueCombo(Me.Combo_FieldSelect2.SelectedItem, Me.Combo_Select2_Value)

            If (Not (SelectedValue Is Nothing)) Then
              Me.Combo_Select2_Value.SelectedValue = SelectedValue
            ElseIf TextValue.Length > 0 Then
              Me.Combo_Select2_Value.Text = TextValue
            End If
          End If
        Catch ex As Exception
        End Try
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: Second SelectField", ex.StackTrace, True)
    End Try

    ' Check TableUpdate against third Select Field
    Try
      If Me.Combo_FieldSelect3.SelectedIndex >= 0 Then
        Try
          ' GetFieldChangeID
          If (e.TableChanged(GetFieldChangeID(Combo_FieldSelect3.SelectedItem)) = True) Or KnowledgeDateChanged Then
            Dim SelectedValue As Object
            Dim TextValue As String

            SelectedValue = Nothing
            TextValue = ""

            If Me.Combo_Select3_Value.SelectedIndex >= 0 Then
              SelectedValue = Me.Combo_Select3_Value.SelectedValue
            ElseIf (SelectedValue Is Nothing) Then
              TextValue = Me.Combo_Select3_Value.Text
            End If

            Call UpdateSelectedValueCombo(Me.Combo_FieldSelect3.SelectedItem, Me.Combo_Select3_Value)

            If (Not (SelectedValue Is Nothing)) Then
              Me.Combo_Select3_Value.SelectedValue = SelectedValue
            ElseIf TextValue.Length > 0 Then
              Me.Combo_Select3_Value.Text = TextValue
            End If
          End If
        Catch ex As Exception
        End Try
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: Third SelectField", ex.StackTrace, True)
    End Try


    ' Changes to the tblUserPermissions table :-

    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

        ' Check ongoing permissions.

        Call CheckPermissions()
        If (HasReadPermission = False) Then
          Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

          FormIsValid = False
          InPaint = OrgInPaint
          Me.Close()
          Exit Sub
        End If

        RefreshGrid = True

      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: tblUserPermissions", ex.StackTrace, True)
    End Try


    ' ****************************************************************
    ' Changes to the Main FORM table :-
    ' (or the tblTransactions table)
    ' ****************************************************************

    Try
      If (e.TableChanged(THIS_FORM_ChangeID) = True) Or _
         (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblTransaction) = True) Or _
         (RefreshGrid = True) Or _
         KnowledgeDateChanged Then

        ' Re-Set Controls etc.
				Call SetSortedRows(True)

      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: Main Form table", ex.StackTrace, True)
    End Try

    InPaint = OrgInPaint

  End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

    ''' <summary>
    ''' Gets the transaction select string.
    ''' </summary>
    ''' <param name="OnlyUsertblTransactionFields">if set to <c>true</c> [only usertbl transaction fields].</param>
    ''' <returns>System.String.</returns>
  Private Function GetTransactionSelectString(Optional ByVal OnlyUsertblTransactionFields As Boolean = False) As String
    ' *******************************************************************************
    ' Build a Select String appropriate to the form status.
    ' *******************************************************************************

    Dim SelectString As String
    Dim FieldSelectString As String

    SelectString = ""
    FieldSelectString = ""
    Me.MainForm.SetToolStripText(Label_Status, "")

    Try

      ' Select on Fund...

      If (Me.Combo_TransactionFund.SelectedIndex > 0) Then
        If IsFieldTransactionField("TransactionFund") = True Then
          SelectString = "(TransactionFund=" & Combo_TransactionFund.SelectedValue.ToString & ")"
        End If
      End If

      ' Select on Instrument

      If (Me.Combo_TransactionInstrument.SelectedIndex > 0) Then
        If IsFieldTransactionField("TransactionInstrument") = True Then
          If (SelectString.Length > 0) Then
            SelectString &= " AND "
          End If
          SelectString &= "(TransactionInstrument=" & Combo_TransactionInstrument.SelectedValue.ToString & ")"
        End If
      End If

      ' Select on Value Date

      If (Me.Combo_ValueDateOperator.SelectedIndex > 0) AndAlso (Combo_ValueDateOperator.SelectedItem.ToString.Length > 0) Then
        If (SelectString.Length > 0) Then
          SelectString &= " AND "
        End If

        SelectString &= "(TransactionValueDate " & Combo_ValueDateOperator.SelectedItem.ToString & " #" & Me.Date_ValueDate.Value.ToString(QUERY_SHORTDATEFORMAT) & "#)"
      End If

      If (Me.Combo_ValueDateOperator2.SelectedIndex > 0) AndAlso (Combo_ValueDateOperator2.SelectedItem.ToString.Length > 0) Then
        If (SelectString.Length > 0) Then
          SelectString &= " AND "
        End If

        SelectString &= "(TransactionValueDate " & Combo_ValueDateOperator2.SelectedItem.ToString & " #" & Me.Date_ValueDate2.Value.ToString(QUERY_SHORTDATEFORMAT) & "#)"
      End If

      ' Select on Trade Status Group
      ' Combo_TradeStatusOperator
      ' Combo_TradeStatusGroup

      If (Combo_TradeStatusOperator.SelectedIndex > 0) AndAlso (Combo_TradeStatusGroup.SelectedIndex > 0) Then
        Dim GroupID As Integer = CInt(Combo_TradeStatusGroup.SelectedValue)
        Dim GroupItemsDS As RenaissanceDataClass.DSTradeStatusGroupItems
        Dim SelectedItems() As RenaissanceDataClass.DSTradeStatusGroupItems.tblTradeStatusGroupItemsRow
        Dim ItemIndex As Integer

        GroupItemsDS = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblTradeStatusGroupItems), RenaissanceDataClass.DSTradeStatusGroupItems)

        If (GroupItemsDS IsNot Nothing) Then
          SelectedItems = GroupItemsDS.tblTradeStatusGroupItems.Select("GroupID=" & GroupID.ToString())

          If (SelectString.Length > 0) Then
            SelectString &= " AND "
          End If

          If (SelectedItems.Count > 0) Then

            If (Combo_TradeStatusOperator.SelectedIndex = 2) Then ' NOT IN
              SelectString &= " (TransactionTradeStatusID NOT IN ("
            Else
              SelectString &= " (TransactionTradeStatusID IN ("
            End If

            Try
              For ItemIndex = 0 To (SelectedItems.Count - 1)
                If (ItemIndex > 0) Then
                  SelectString &= ","
                End If

                SelectString &= SelectedItems(ItemIndex).StatusID
              Next

            Catch ex As Exception
            Finally
              SelectString &= ")) "
            End Try

          Else
            SelectString &= " (TransactionTradeStatusID < 0) "
          End If
        End If

      End If


      ' Field Select ...

      FieldSelectString = "("
      Dim TransactionDataset As RenaissanceDataClass.DSSelectTransaction
      Dim TransactionTable As RenaissanceDataClass.DSSelectTransaction.tblSelectTransactionDataTable = CType(myTable, RenaissanceDataClass.DSSelectTransaction.tblSelectTransactionDataTable) 'TransactionDataset.tblSelectTransaction
      Dim FieldName As String

      If (TransactionTable Is Nothing) Then
        TransactionDataset = New RenaissanceDataClass.DSSelectTransaction
        TransactionTable = TransactionDataset.tblSelectTransaction
      End If

      ' Don't need the data, just the column defs.
      'TransactionDataset = MainForm.Load_Table(RenaissanceStandardDatasets.tblTransaction, False)
      'TransactionTable = myDataset.Tables(0)

      ' Select Field One.

      Try
        If (Me.Combo_FieldSelect1.SelectedIndex > 0) AndAlso (Me.Combo_Select1_Operator.SelectedIndex > 0) And (Me.Combo_Select1_Value.Text.Length > 0) Then
          ' Get Field Name, Add 'Transaction' back on if necessary.

          FieldName = Combo_FieldSelect1.SelectedItem
          If (FieldName.EndsWith(".")) Then
            FieldName = "Transaction" & FieldName.Substring(0, FieldName.Length - 1)
          End If

          If (OnlyUsertblTransactionFields = False) OrElse (IsFieldTransactionField(FieldName) = True) Then
            ' If the Selected Field Name is a String Field ...

            If TransactionTable.Columns.Contains(FieldName) Then
              If (TransactionTable.Columns(FieldName).DataType Is GetType(System.String)) Then

                ' If there is a value Selected, Use it - else use the Combo Text.

                If (Not (Combo_Select1_Value.SelectedValue Is Nothing)) Then
                  FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.Text & " '" & Combo_Select1_Value.SelectedValue.ToString & "')"
                Else
                  FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.Text & " '" & Combo_Select1_Value.Text.ToString & "')"
                End If

              ElseIf (TransactionTable.Columns(FieldName).DataType Is GetType(System.DateTime)) Then

                ' For Dates, If there is no 'Selected' value use the Combo text if it is a Valid date, else
                ' Use the 'Selected' Value if it is a date.

                If (Me.Combo_Select1_Value.SelectedValue Is Nothing) AndAlso (IsDate(Me.Combo_Select1_Value.Text)) Then
                  FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select1_Value.Text).ToString(QUERY_SHORTDATEFORMAT) & "#)"
                ElseIf (Not (Me.Combo_Select1_Value.SelectedValue Is Nothing)) And IsDate(Combo_Select1_Value.SelectedValue) Then
                  FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select1_Value.SelectedValue).ToString(QUERY_SHORTDATEFORMAT) & "#)"
                Else
                  FieldSelectString &= "(true)"
                End If

              Else
                ' Now Deemed to be numeric, or at least not in need of quotes / delimeters...
                ' If there is no 'Selected' value use the Combo text if it is a Valid number, else
                ' Use the 'Selected' Value if it is a valid numeric.

                If (Not (Combo_Select1_Value.SelectedValue Is Nothing)) AndAlso (IsNumeric(Combo_Select1_Value.SelectedValue)) Then
                  FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.SelectedItem.ToString & " " & Combo_Select1_Value.SelectedValue.ToString & ")"
                ElseIf IsNumeric(Combo_Select1_Value.Text) Then
                  FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.SelectedItem.ToString & " " & Combo_Select1_Value.Text & ")"
                Else
                  FieldSelectString &= "(true)"
                End If
              End If
            End If

          End If
        End If

      Catch ex As Exception
      End Try

      ' Select Field Two.

      Try
        If (Me.Combo_FieldSelect2.SelectedIndex > 0) AndAlso (Me.Combo_Select2_Operator.SelectedIndex > 0) And (Me.Combo_Select2_Value.Text.Length > 0) Then
          FieldName = Combo_FieldSelect2.SelectedItem
          If (FieldName.EndsWith(".")) Then
            FieldName = "Transaction" & FieldName.Substring(0, FieldName.Length - 1)
          End If

          If (OnlyUsertblTransactionFields = False) OrElse (IsFieldTransactionField(FieldName) = True) Then

            If (FieldSelectString.Length > 1) Then
              FieldSelectString &= " " & Me.Combo_AndOr_1.SelectedItem.ToString & " "
            End If

            If (TransactionTable.Columns(FieldName).DataType Is GetType(System.String)) Then

              If (Not (Combo_Select2_Value.SelectedValue Is Nothing)) Then
                FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.Text & " '" & Combo_Select2_Value.SelectedValue.ToString & "')"
              Else
                FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.Text & " '" & Combo_Select2_Value.Text.ToString & "')"
              End If

            ElseIf (TransactionTable.Columns(FieldName).DataType Is GetType(System.DateTime)) Then

              If (Me.Combo_Select2_Value.SelectedValue Is Nothing) AndAlso (IsDate(Me.Combo_Select2_Value.Text)) Then
                FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select2_Value.Text).ToString(QUERY_SHORTDATEFORMAT) & "#)"
              ElseIf (Not (Me.Combo_Select2_Value.SelectedValue Is Nothing)) And IsDate(Combo_Select2_Value.SelectedValue) Then
                FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select2_Value.SelectedValue).ToString(QUERY_SHORTDATEFORMAT) & "#)"
              Else
                FieldSelectString &= "(true)"
              End If

            Else
              If (Not (Combo_Select2_Value.SelectedValue Is Nothing)) AndAlso (IsNumeric(Combo_Select2_Value.SelectedValue)) Then
                FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.SelectedItem.ToString & " " & Combo_Select2_Value.SelectedValue.ToString & ")"
              ElseIf IsNumeric(Combo_Select2_Value.Text) Then
                FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.SelectedItem.ToString & " " & Combo_Select2_Value.Text & ")"
              Else
                FieldSelectString &= "(true)"
              End If
            End If
          End If
        End If
      Catch ex As Exception
      End Try


      ' Select Field Three.

      Try
        If (Me.Combo_FieldSelect3.SelectedIndex > 0) AndAlso (Me.Combo_Select3_Operator.SelectedIndex > 0) And (Me.Combo_Select3_Value.Text.Length > 0) Then
          FieldName = Combo_FieldSelect3.SelectedItem
          If (FieldName.EndsWith(".")) Then
            FieldName = "Transaction" & FieldName.Substring(0, FieldName.Length - 1)
          End If

          If (OnlyUsertblTransactionFields = False) OrElse (IsFieldTransactionField(FieldName) = True) Then

            If (FieldSelectString.Length > 1) Then
              FieldSelectString &= " " & Me.Combo_AndOr_2.SelectedItem.ToString & " "
            End If

            If (TransactionTable.Columns(FieldName).DataType Is GetType(System.String)) Then

              If (Not (Combo_Select3_Value.SelectedValue Is Nothing)) Then
                FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.Text & " '" & Combo_Select3_Value.SelectedValue.ToString & "')"
              Else
                FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.Text & " '" & Combo_Select3_Value.Text.ToString & "')"
              End If

            ElseIf (TransactionTable.Columns(FieldName).DataType Is GetType(System.DateTime)) Then

              If (Me.Combo_Select3_Value.SelectedValue Is Nothing) AndAlso (IsDate(Me.Combo_Select3_Value.Text)) Then
                FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select3_Value.Text).ToString(QUERY_SHORTDATEFORMAT) & "#)"
              ElseIf (Not (Me.Combo_Select3_Value.SelectedValue Is Nothing)) And IsDate(Combo_Select3_Value.SelectedValue) Then
                FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select3_Value.SelectedValue).ToString(QUERY_SHORTDATEFORMAT) & "#)"
              Else
                FieldSelectString &= "(true)"
              End If

            Else
              If (Not (Combo_Select3_Value.SelectedValue Is Nothing)) AndAlso (IsNumeric(Combo_Select3_Value.SelectedValue)) Then
                FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.SelectedItem.ToString & " " & Combo_Select3_Value.SelectedValue.ToString & ")"
              ElseIf IsNumeric(Combo_Select3_Value.Text) Then
                FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.SelectedItem.ToString & " " & Combo_Select3_Value.Text & ")"
              Else
                FieldSelectString &= "(true)"
              End If
            End If
          End If
        End If
      Catch ex As Exception
      End Try

      FieldSelectString &= ")"

      ' If the FieldSelect string is used, then tag it on to the main select string.

      If FieldSelectString.Length > 2 Then
        If (SelectString.Length > 0) Then
          SelectString &= " AND "
        End If
        SelectString &= FieldSelectString
      End If

      ' Check for the 'Incomplete Transactions' option.

      If Me.Check_IncompleteTransactions.Checked = True Then
        If SelectString.Length <= 0 Then
          SelectString = INCOMPLETE_TRANSACTION_QUERY
        Else
          SelectString = "(" & SelectString & ") AND " & INCOMPLETE_TRANSACTION_QUERY
        End If
      End If

      If SelectString.Length <= 0 Then
        SelectString = "true"
      End If

    Catch ex As Exception
      SelectString = "true"
    End Try

    Return SelectString

  End Function

    ''' <summary>
    ''' Determines whether [is field transaction field] [the specified field name].
    ''' </summary>
    ''' <param name="FieldName">Name of the field.</param>
    ''' <returns><c>true</c> if [is field transaction field] [the specified field name]; otherwise, <c>false</c>.</returns>
  Private Function IsFieldTransactionField(ByVal FieldName As String) As Boolean
    ' *******************************************************************************
    ' Simple function to return a boolean value indicating whether or not a column name is
    ' part of the tblTransactions table.
    ' *******************************************************************************

    Try
      Dim TransactionTable As New RenaissanceDataClass.DSTransaction.tblTransactionDataTable

      If (TransactionTable Is Nothing) Then
        Return False
      Else
        Return TransactionTable.Columns.Contains(FieldName)
      End If
    Catch ex As Exception
      Return (False)
    End Try

    Return (False)

  End Function

    ''' <summary>
    ''' Sets the sorted rows.
    ''' </summary>
    ''' <param name="ForceRefresh">if set to <c>true</c> [force refresh].</param>
	Private Sub SetSortedRows(Optional ByVal ForceRefresh As Boolean = False)
		' *******************************************************************************
		' Build a Select String appropriate to the form status and apply it to the DataView object.
		' *******************************************************************************

		Dim SelectString As String

		If (Me.Created) And (Not Me.IsDisposed) Then

			SelectString = GetTransactionSelectString()

			Try

				If ForceRefresh Then
					myDataView.RowFilter = "False"
				End If
				If (myDataView IsNot Nothing) AndAlso (Not (myDataView.RowFilter = SelectString)) Then
          myDataView.RowFilter = SelectString
				End If

			Catch ex As Exception
				Try
					SelectString = "true"
					myDataView.RowFilter = SelectString
				Catch ex_inner As Exception
				End Try

			End Try

			Me.Label_Status.Text = "(" & myDataView.Count.ToString & " Records) " & SelectString

		End If

	End Sub


  ' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
  Private Sub CheckPermissions()
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

  End Sub

    ''' <summary>
  ''' Basic event handler, called when a form value is changed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' *******************************************************************************
    ' In the event of one of the Form Controls changing, refresh the data grid.
    ' *******************************************************************************

    If InPaint = False Then

      If sender Is Combo_TransactionFund Then
        Call SetInstrumentCombo()
      End If

      Call SetSortedRows()

    End If
  End Sub

    ''' <summary>
    ''' Sets the form select criteria.
    ''' </summary>
    ''' <param name="FundID">The fund ID.</param>
    ''' <param name="InstrumentID">The instrument ID.</param>
  Public Sub SetFormSelectCriteria(ByVal FundID As Integer, ByVal InstrumentID As Integer)
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Try

      If (FundID > 0) Then
        Combo_TransactionFund.SelectedValue = FundID
      Else
        Combo_TransactionFund.SelectedIndex = 0
      End If

      If (InstrumentID > 0) Then
        Combo_TransactionInstrument.SelectedValue = InstrumentID
      Else
        Combo_TransactionInstrument.SelectedIndex = 0
      End If

    Catch ex As Exception

    End Try

  End Sub

    ''' <summary>
    ''' Sets the form select criteria.
    ''' </summary>
    ''' <param name="FundID">The fund ID.</param>
    ''' <param name="InstrumentID">The instrument ID.</param>
    ''' <param name="StartDate">The start date.</param>
  Public Sub SetFormSelectCriteria(ByVal FundID As Integer, ByVal InstrumentID As Integer, ByVal StartDate As Date)
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Try

      SetFormSelectCriteria(FundID, InstrumentID)

      Me.Date_ValueDate.Value = StartDate

    Catch ex As Exception

    End Try

  End Sub

    ''' <summary>
    ''' Handles the KeyUp event of the Combo_FieldSelect control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
  Private Sub Combo_FieldSelect_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Combo_Select1_Value.KeyUp
    ' *******************************************************************************
    ' In the event of one of the Form Controls changing, refresh the data grid.
    ' *******************************************************************************

    If InPaint = False Then

      Call SetSortedRows()

    End If
  End Sub


    ''' <summary>
    ''' Builds the field select combos.
    ''' </summary>
  Private Sub BuildFieldSelectCombos()
    ' *********************************************************************************
    ' Build the FieldSelect combo boxes.
    '
    ' Field Names starting with 'Transaction' are modified to start with '.'
    ' This is done in order to make the name more readable.
    '
    ' *********************************************************************************

    Dim ColumnName As String
    Dim thisColumn As DataColumn

    Combo_FieldSelect1.Items.Clear()
    Combo_Select1_Value.Items.Clear()
    Combo_FieldSelect1.Items.Add("")

    Combo_FieldSelect2.Items.Clear()
    Combo_Select2_Value.Items.Clear()
    Combo_FieldSelect2.Items.Add("")

    Combo_FieldSelect3.Items.Clear()
    Combo_Select3_Value.Items.Clear()
    Combo_FieldSelect3.Items.Add("")

    For Each thisColumn In myTable.Columns
      Try
        ColumnName = thisColumn.ColumnName
        If (ColumnName.StartsWith("Transaction")) Then
          ColumnName = ColumnName.Substring(11) & "."
        End If

        Combo_FieldSelect1.Items.Add(ColumnName)
        Combo_FieldSelect2.Items.Add(ColumnName)
        Combo_FieldSelect3.Items.Add(ColumnName)

      Catch ex As Exception
      End Try
    Next

  End Sub

    ''' <summary>
    ''' Updates the selected value combo.
    ''' </summary>
    ''' <param name="pFieldName">Name of the p field.</param>
    ''' <param name="pValueCombo">The p value combo.</param>
  Private Sub UpdateSelectedValueCombo(ByVal pFieldName As String, ByRef pValueCombo As ComboBox)
    ' *******************************************************************************
    ' Build a Value Combo Box appropriate to the chosen DataTable Select Field.
    '
    ' By default the combo will contain all existing values for the chosen field, however
    ' If a relationship is defined for thi table and field, then a Combo will be built
    ' which reflects this relationship.
    ' e.g. the tblReferentialIntegrity table defined a relationship between the
    ' TransactionCounterparty field and the tblCounterparty.Counterparty field, thus if
    ' the chosen Select field ti 'Counterparty.' then the Value combo will be built using
    ' the CounterpartyID & CounterpartyName details from the tblCounterparty table.
    '
    ' *******************************************************************************

    Dim tblRefIntegrity As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityDataTable
    Dim IntegrityRows() As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityRow
    Dim TransactionDataset As DataSet
    Dim TransactionTable As DataTable
    Dim SortOrder As Boolean

    SortOrder = True

    ' Clear the Value Combo.

    Try
      pValueCombo.DataSource = Nothing
      pValueCombo.DisplayMember = ""
      pValueCombo.ValueMember = ""
      pValueCombo.Items.Clear()
    Catch ex As Exception
    End Try

    ' By default, make the Combo a Standard Dropdown, so that non-list items can be entered.

    pValueCombo.DropDownStyle = ComboBoxStyle.DropDown

    ' Exit if no FieldName is given (having cleared the combo).

    If (pFieldName.Length <= 0) Then Exit Sub

    ' Adjust the Field name if appropriate.

    If (pFieldName.EndsWith(".")) Then
      pFieldName = "Transaction" & pFieldName.Substring(0, pFieldName.Length - 1)
    End If

    ' Get a handle to the Standard Transactions Table.
    ' This is so that the field types can be determined and used.

    Try
      TransactionDataset = MainForm.Load_Table(RenaissanceStandardDatasets.tblSelectTransaction, False)
      TransactionTable = myDataset.Tables(0)
    Catch ex As Exception
      Exit Sub
    End Try

    ' If the selected field is a Date then sort the selection combo in reverse order.

    If (TransactionTable.Columns(pFieldName).DataType Is GetType(System.DateTime)) Then
      SortOrder = False
    End If

    ' Get a handle to the Referential Integrity table and the row matching this table and field.
    ' this table defines a relationship between Transaction Table fields and other tables.
    ' This information can be used to build more meaningfull Value Combos.

    Try
      tblRefIntegrity = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblReferentialIntegrity, False), RenaissanceDataClass.DSReferentialIntegrity).tblReferentialIntegrity
      IntegrityRows = tblRefIntegrity.Select("(FeedsTable LIKE '%tblSelectTransaction') AND (FeedsField = '" & pFieldName & "')", "RN")
      If (IntegrityRows.Length <= 0) Then
        GoTo StandardExit
      End If

    Catch ex As Exception
      Exit Sub
    End Try

    If (IntegrityRows(0).IsDescriptionFieldNull) Then
      ' No Linked Description Field

      GoTo StandardExit
    End If

    ' OK, a referential record exists.
    ' Determine the Table and Field Names to use to build the Value Combo.

    Dim TableName As String
    Dim TableField As String
    Dim DescriptionField As String
    Dim stdDS As StandardDataset
    Dim thisChangeID As RenaissanceChangeID

    Try

      TableName = IntegrityRows(0).TableName
      TableField = IntegrityRows(0).TableField
      DescriptionField = IntegrityRows(0).DescriptionField

      thisChangeID = CType(System.Enum.Parse(GetType(RenaissanceChangeID), TableName), RenaissanceChangeID)

      stdDS = RenaissanceStandardDatasets.GetStandardDataset(thisChangeID)

      ' Build the appropriate Values Combo.

      Call MainForm.SetTblGenericCombo( _
      pValueCombo, _
      stdDS, _
      DescriptionField, _
      TableField, _
      "", False, SortOrder, True)    ' 

      ' For Referential Integrity generated combo's, make the Combo a 
      ' DropDownList

      pValueCombo.DropDownStyle = ComboBoxStyle.DropDownList

    Catch ex As Exception
      GoTo StandardExit

    End Try

    Exit Sub

StandardExit:

    ' Build the standard Combo, just use the values from the given field.

    Try

      Call MainForm.SetTblGenericCombo( _
       pValueCombo, _
       RenaissanceStandardDatasets.tblSelectTransaction, _
       pFieldName, _
       pFieldName, _
       "", True, SortOrder, False)    ' 

      MainForm.ClearComboSelection(pValueCombo)

    Catch ex As Exception
    End Try

  End Sub

    ''' <summary>
    ''' Gets the field change ID.
    ''' </summary>
    ''' <param name="pFieldName">Name of the p field.</param>
    ''' <returns>RenaissanceChangeID.</returns>
  Private Function GetFieldChangeID(ByVal pFieldName As String) As RenaissanceChangeID
    ' ***************************************************************************************
    '
    '
    ' ***************************************************************************************

    Dim tblRefIntegrity As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityDataTable
    Dim IntegrityRows() As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityRow

    If (pFieldName.EndsWith(".")) Then
      pFieldName = "Transaction" & pFieldName.Substring(0, pFieldName.Length - 1)
    End If

    Try
      tblRefIntegrity = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblReferentialIntegrity, False), RenaissanceDataClass.DSReferentialIntegrity).tblReferentialIntegrity
      IntegrityRows = tblRefIntegrity.Select("(FeedsTable LIKE '%tblSelectTransaction') AND (FeedsField = '" & pFieldName & "')", "RN")
      If (IntegrityRows.Length <= 0) Then
        Return RenaissanceChangeID.None
        Exit Function
      End If

    Catch ex As Exception
      Return RenaissanceChangeID.None
      Exit Function
    End Try


    If (IntegrityRows(0).IsDescriptionFieldNull) Then
      Return RenaissanceChangeID.None
      Exit Function
    End If

    ' OK, a referential record exists.
    ' Determine the Table and Field Names to use to build the Value Combo.

    Dim TableName As String
    Dim TableField As String
    Dim DescriptionField As String
    Dim thisChangeID As RenaissanceChangeID

    Try

      TableName = IntegrityRows(0).TableName
      TableField = IntegrityRows(0).TableField
      DescriptionField = IntegrityRows(0).DescriptionField

      thisChangeID = CType(System.Enum.Parse(GetType(RenaissanceChangeID), TableName), RenaissanceChangeID)

      Return thisChangeID
      Exit Function
    Catch ex As Exception
    End Try

    Return RenaissanceChangeID.None
    Exit Function

  End Function


    ''' <summary>
    ''' Handles the DoubleClick event of the Grid_Transactions control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Transactions_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid_Transactions.DoubleClick
    ' ***************************************************************************************
    ' Spawn an Add/Edit Transactions form reflecting the chosen selection of transactions
    ' and pointing to the transaction which was double clicked.
    ' ***************************************************************************************

    Try

      Dim ParentID As Integer
      Dim thisFormHandle As RenaissanceGlobals.RenaissanceFormHandle

      ParentID = Me.Grid_Transactions.Rows(Grid_Transactions.RowSel).DataSource("TransactionParentID")

      thisFormHandle = MainForm.New_VeniceForm(VeniceFormID.frmTransaction)
      CType(thisFormHandle.Form, frmTransaction).FormSelectCriteria = GetTransactionSelectString(True)
      CType(thisFormHandle.Form, frmTransaction).MoveToAuditID(ParentID)

    Catch ex As Exception
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error opening Transactions Form.", ex.StackTrace, True)
    End Try

  End Sub


    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_FieldSelect1 control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_FieldSelect1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_FieldSelect1.SelectedIndexChanged
    ' ***************************************************************************************
    ' Field Select Combo Chosen, Update the associated Values Combo.
    '
    ' ***************************************************************************************

    Try

      Call UpdateSelectedValueCombo(Me.Combo_FieldSelect1.SelectedItem, Me.Combo_Select1_Value)
      Me.Combo_Select1_Operator.SelectedIndex = 1

    Catch ex As Exception
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Combo_FieldSelect1_SelectedIndexChanged()", ex.StackTrace, True)
    End Try

  End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_FieldSelect2 control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_FieldSelect2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_FieldSelect2.SelectedIndexChanged
    ' ***************************************************************************************
    ' Field Select Combo Chosen, Update the associated Values Combo.
    '
    ' ***************************************************************************************
    Try

      Call UpdateSelectedValueCombo(Me.Combo_FieldSelect2.SelectedItem, Me.Combo_Select2_Value)
      Me.Combo_Select2_Operator.SelectedIndex = 1

    Catch ex As Exception
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Combo_FieldSelect1_SelectedIndexChanged()", ex.StackTrace, True)
    End Try

  End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_FieldSelect3 control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_FieldSelect3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_FieldSelect3.SelectedIndexChanged
    ' ***************************************************************************************
    ' Field Select Combo Chosen, Update the associated Values Combo.
    '
    ' ***************************************************************************************

    Try

      Call UpdateSelectedValueCombo(Me.Combo_FieldSelect3.SelectedItem, Me.Combo_Select3_Value)
      Me.Combo_Select3_Operator.SelectedIndex = 1

    Catch ex As Exception
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Combo_FieldSelect1_SelectedIndexChanged()", ex.StackTrace, True)
    End Try

  End Sub

#End Region

#Region " Set Form Combos (Form Specific Code) "

    ''' <summary>
    ''' Sets the fund combo.
    ''' </summary>
	Private Sub SetFundCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_TransactionFund, _
    RenaissanceStandardDatasets.tblFund, _
    "FundName", _
    "FundID", _
    "FundClosed=0", False, True, True)   ' 

	End Sub

    ''' <summary>
    ''' Sets the instrument combo.
    ''' </summary>
	Private Sub SetInstrumentCombo()

    Try

      Dim FundID As Integer = 0
      If (Combo_TransactionFund.SelectedIndex >= 0) AndAlso (CInt(Nz(Combo_TransactionFund.SelectedValue, 0)) > 0) Then

        FundID = CInt(Combo_TransactionFund.SelectedValue)

        If (Check_ByISIN.Checked) Then

          Call MainForm.SetTblGenericCombo( _
            Me.Combo_TransactionInstrument, _
            RenaissanceStandardDatasets.tblSelectTransaction, _
            "InstrumentISIN|InstrumentDescription", _
            "TransactionInstrument", _
            "TransactionFund=" & FundID.ToString, True, True, True, 0)    ' 

        Else

          Call MainForm.SetTblGenericCombo( _
            Me.Combo_TransactionInstrument, _
            RenaissanceStandardDatasets.tblSelectTransaction, _
            "InstrumentDescription|InstrumentISIN", _
            "TransactionInstrument", _
            "TransactionFund=" & FundID.ToString, True, True, True, 0)    ' 

        End If

      Else

        If (Check_ByISIN.Checked) Then

          Call MainForm.SetTblGenericCombo( _
            Me.Combo_TransactionInstrument, _
            RenaissanceStandardDatasets.tblSelectTransaction, _
            "InstrumentISIN|InstrumentDescription", _
            "TransactionInstrument", _
            "", True, True, True, 0)    ' 

        Else

          Call MainForm.SetTblGenericCombo( _
            Me.Combo_TransactionInstrument, _
            RenaissanceStandardDatasets.tblSelectTransaction, _
            "InstrumentDescription|InstrumentISIN", _
            "TransactionInstrument", _
            "", True, True, True, 0)    ' 

        End If

      End If

    Catch ex As Exception
    End Try

	End Sub

    ''' <summary>
    ''' Sets the trade status group combo.
    ''' </summary>
  Private Sub SetTradeStatusGroupCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_TradeStatusGroup, _
    RenaissanceStandardDatasets.tblTradeStatusGroup, _
    "TradeStatusGroupName", _
    "TradeStatusGroupID", _
    "", False, True, True)    ' 

  End Sub


#End Region

#Region " Transaction report Menu"

    ''' <summary>
    ''' Sets the transaction report menu.
    ''' </summary>
    ''' <param name="RootMenu">The root menu.</param>
    ''' <returns>MenuStrip.</returns>
	Private Function SetTransactionReportMenu(ByRef RootMenu As MenuStrip) As MenuStrip
		' ***************************************************************************************
		' Setup the Transaction Reports menu with appropriate items and callbacks
		'
		' ***************************************************************************************

		Dim ReportMenu As New ToolStripMenuItem("Transaction &Reports")
		Dim newMenuItem As ToolStripMenuItem

		newMenuItem = ReportMenu.DropDownItems.Add("Transaction &Ticket", Nothing, AddressOf Me.rptTransactionTicket)

		ReportMenu.DropDownItems.Add(New ToolStripSeparator)

		newMenuItem = ReportMenu.DropDownItems.Add("Transaction By &Value Date", Nothing, AddressOf Me.rptTransactionByDate)
		newMenuItem = ReportMenu.DropDownItems.Add("Transaction By &Entry Date", Nothing, AddressOf Me.rptTransactionByEntryDate)
		newMenuItem = ReportMenu.DropDownItems.Add("Transaction By &Group", Nothing, AddressOf Me.rptTransactionByGroup)
		newMenuItem = ReportMenu.DropDownItems.Add("Transaction By &Instrument", Nothing, AddressOf Me.rptTransactionByStock)
		newMenuItem = ReportMenu.DropDownItems.Add("&Incomplete Transaction Report", Nothing, AddressOf Me.rptIncompleteTransactions)
		newMenuItem = ReportMenu.DropDownItems.Add("Transaction &Price Differences", Nothing, AddressOf Me.rptTransactionPriceDifference)
    newMenuItem = ReportMenu.DropDownItems.Add("&Instrument Position Report", Nothing, AddressOf Me.rptInstrumentPositionsReport)

		ReportMenu.DropDownItems.Add(New ToolStripSeparator)

		newMenuItem = ReportMenu.DropDownItems.Add("Trade &Blotter", Nothing, AddressOf Me.rptTradeBlotter)
		newMenuItem = ReportMenu.DropDownItems.Add("&Final Trade Blotter", Nothing, AddressOf Me.rptFinalTradeBlotter)
		newMenuItem = ReportMenu.DropDownItems.Add("Trade &Revision Blotter", Nothing, AddressOf Me.rptTradeRevisionBlotter)
		newMenuItem = ReportMenu.DropDownItems.Add("Final Trade &Revision Blotter", Nothing, AddressOf Me.rptFinalTradeRevisionBlotter)

		ReportMenu.DropDownItems.Add(New ToolStripSeparator)

		newMenuItem = ReportMenu.DropDownItems.Add("F&X Blotter", Nothing, AddressOf Me.rptFXBlotter)
		newMenuItem = ReportMenu.DropDownItems.Add("Final FX Blotter", Nothing, AddressOf Me.rptFinalFXBlotter)

		RootMenu.Items.Add(ReportMenu)
		Return RootMenu

	End Function

    ''' <summary>
    ''' Sets the field select menu.
    ''' </summary>
    ''' <param name="RootMenu">The root menu.</param>
    ''' <returns>ToolStripMenuItem.</returns>
	Private Function SetFieldSelectMenu(ByRef RootMenu As MenuStrip) As ToolStripMenuItem
		' ***************************************************************************************
		' Build the FieldSelect Menu.
		'
		' Add an item for each field in the SelectTransactions table, when selected the item
		' will show or hide the associated field on the transactions grid.
		' ***************************************************************************************

		Dim ColumnNames(-1) As String
		Dim ColumnCount As Integer

		Dim FieldsMenu As New ToolStripMenuItem("Grid &Fields")
		FieldsMenu.Name = "Menu_GridField"

		Dim newMenuItem As ToolStripMenuItem

		ReDim ColumnNames(myTable.Columns.Count - 1)
		For ColumnCount = 0 To (myTable.Columns.Count - 1)
			ColumnNames(ColumnCount) = myTable.Columns(ColumnCount).ColumnName
			If (ColumnNames(ColumnCount).StartsWith("Transaction")) Then
				ColumnNames(ColumnCount) = ColumnNames(ColumnCount).Substring(11) & "."
			End If
		Next
		Array.Sort(ColumnNames)

		For ColumnCount = 0 To (ColumnNames.Length - 1)
			newMenuItem = FieldsMenu.DropDownItems.Add(ColumnNames(ColumnCount), Nothing, AddressOf Me.ShowGridField)

			If (ColumnNames(ColumnCount).EndsWith(".")) Then
				newMenuItem.Name = "Menu_GridField_Transaction" & ColumnNames(ColumnCount).Substring(0, ColumnNames(ColumnCount).Length - 1)
			Else
				newMenuItem.Name = "Menu_GridField_" & ColumnNames(ColumnCount)
			End If
		Next

		RootMenu.Items.Add(FieldsMenu)
		Return FieldsMenu

	End Function

    ''' <summary>
    ''' Shows the grid field.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Private Sub ShowGridField(ByVal sender As Object, ByVal e As EventArgs)
		' ***************************************************************************************
		' Callback function for the Grid Fields Menu items.
		'
		' ***************************************************************************************

		Try
			If TypeOf (sender) Is ToolStripMenuItem Then
				Dim thisMenuItem As ToolStripMenuItem
				Dim FieldName As String

				thisMenuItem = CType(sender, ToolStripMenuItem)

				FieldName = thisMenuItem.Name
				If (FieldName.StartsWith("Menu_GridField_")) Then
					FieldName = FieldName.Substring(15)
				End If

				thisMenuItem.Checked = Not thisMenuItem.Checked

				If (thisMenuItem.Checked) Then
					Grid_Transactions.Cols(FieldName).Visible = True
				Else
					Grid_Transactions.Cols(FieldName).Visible = False
				End If
			End If
		Catch ex As Exception

		End Try
	End Sub

    ''' <summary>
    ''' RPTs the transaction ticket.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Private Sub rptTransactionTicket(ByVal sender As Object, ByVal e As EventArgs)
		' ***************************************************************************************
		'
		'
		' ***************************************************************************************

		Dim ThisDataset As StandardDataset

		ThisDataset = RenaissanceStandardDatasets.GetStandardDataset(RenaissanceChangeID.tblSelectTransaction)
		If (ThisDataset.NotedIDsHaveChanged) Then
			MainForm.Load_Table(ThisDataset, True)
			Call SetSortedRows()
		End If

		MainForm.MainReportHandler.DisplayReport(0, "rptTransactionTicket", myDataView, Form_ProgressBar)
	End Sub

    ''' <summary>
    ''' RPTs the transaction by date.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Private Sub rptTransactionByDate(ByVal sender As Object, ByVal e As EventArgs)
		' ***************************************************************************************
		'
		'
		' ***************************************************************************************

		Dim ThisDataset As StandardDataset

		ThisDataset = RenaissanceStandardDatasets.GetStandardDataset(RenaissanceChangeID.tblSelectTransaction)
		If (ThisDataset.NotedIDsHaveChanged) Then
			MainForm.Load_Table(ThisDataset, True)
			Call SetSortedRows()
		End If

		MainForm.MainReportHandler.DisplayReport(0, "rptTransactionByDate", myDataView, Form_ProgressBar)
	End Sub

    ''' <summary>
    ''' RPTs the transaction by entry date.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Private Sub rptTransactionByEntryDate(ByVal sender As Object, ByVal e As EventArgs)
		' ***************************************************************************************
		'
		'
		' ***************************************************************************************

		Dim ThisDataset As StandardDataset

		ThisDataset = RenaissanceStandardDatasets.GetStandardDataset(RenaissanceChangeID.tblSelectTransaction)
		If (ThisDataset.NotedIDsHaveChanged) Then
			MainForm.Load_Table(ThisDataset, True)
			Call SetSortedRows()
		End If

		MainForm.MainReportHandler.DisplayReport(0, "rptTransactionByEntryDate", myDataView, Form_ProgressBar)
	End Sub

    ''' <summary>
    ''' RPTs the transaction by group.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Private Sub rptTransactionByGroup(ByVal sender As Object, ByVal e As EventArgs)
		' ***************************************************************************************
		'
		'
		' ***************************************************************************************

		Dim ThisDataset As StandardDataset

		ThisDataset = RenaissanceStandardDatasets.GetStandardDataset(RenaissanceChangeID.tblSelectTransaction)
		If (ThisDataset.NotedIDsHaveChanged) Then
			MainForm.Load_Table(ThisDataset, True)
			Call SetSortedRows()
		End If

		MainForm.MainReportHandler.DisplayReport(0, "rptTransactionByGroup", myDataView, Form_ProgressBar)
	End Sub

    ''' <summary>
    ''' RPTs the transaction by stock.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Private Sub rptTransactionByStock(ByVal sender As Object, ByVal e As EventArgs)
		' ***************************************************************************************
		'
		'
		' ***************************************************************************************

		Dim ThisDataset As StandardDataset

		ThisDataset = RenaissanceStandardDatasets.GetStandardDataset(RenaissanceChangeID.tblSelectTransaction)
		If (ThisDataset.NotedIDsHaveChanged) Then
			MainForm.Load_Table(ThisDataset, True)
			Call SetSortedRows()
		End If

		MainForm.MainReportHandler.DisplayReport(0, "rptTransactionByStock", myDataView, Form_ProgressBar)
	End Sub

    ''' <summary>
    ''' RPTs the instrument positions report.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
  Private Sub rptInstrumentPositionsReport(ByVal sender As Object, ByVal e As EventArgs)
    Dim ThisDataset As StandardDataset

    ThisDataset = RenaissanceStandardDatasets.GetStandardDataset(RenaissanceChangeID.tblSelectTransaction)
    If (ThisDataset.NotedIDsHaveChanged) Then
			MainForm.Load_Table(ThisDataset, True)
      Call SetSortedRows()
    End If

    MainForm.MainReportHandler.DisplayReport(0, "rptInstrumentPositionsReport", myDataView, Form_ProgressBar)
  End Sub

    ''' <summary>
    ''' RPTs the incomplete transactions.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Private Sub rptIncompleteTransactions(ByVal sender As Object, ByVal e As EventArgs)
		' ***************************************************************************************
		'
		'
		' ***************************************************************************************

		Dim ThisDataset As StandardDataset

		Try
			ThisDataset = RenaissanceStandardDatasets.GetStandardDataset(RenaissanceChangeID.tblSelectTransaction)
			If (ThisDataset.NotedIDsHaveChanged) Then
				MainForm.Load_Table(ThisDataset, True)
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error, Failed to Load Select Transactions table", ex.StackTrace, True)
			Exit Sub
		End Try

		Dim SelectString As String
		Dim rptDataView As DataView

		SelectString = GetTransactionSelectString()

		Try
			If (Not SelectString.EndsWith("AND " & INCOMPLETE_TRANSACTION_QUERY)) Then
				SelectString = "(" & SelectString & ") AND " & INCOMPLETE_TRANSACTION_QUERY
			End If

			rptDataView = New DataView(myTable, SelectString, "", DataViewRowState.CurrentRows)
		Catch ex As Exception
			SelectString = "true"
			rptDataView = New DataView(myTable, SelectString, "", DataViewRowState.CurrentRows)
		End Try

		MainForm.MainReportHandler.DisplayReport(0, "rptIncompleteTransactions", rptDataView, Form_ProgressBar)
	End Sub


    ''' <summary>
    ''' RPTs the transaction price difference.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Private Sub rptTransactionPriceDifference(ByVal sender As Object, ByVal e As EventArgs)
		' ***************************************************************************************
		'
		'
		' ***************************************************************************************

		Dim rptDataTable As DataTable
		Dim FundID As Integer

		Try
			FundID = 0
			If Me.Combo_TransactionFund.SelectedIndex >= 0 Then
				If IsNumeric(Combo_TransactionFund.SelectedValue) Then
					FundID = CInt(Combo_TransactionFund.SelectedValue)
				End If
			End If
		Catch ex As Exception
			FundID = 0
		End Try

		rptDataTable = MainForm.MainReportHandler.GetData_rptTransactionPriceDifference(FundID)


		Dim SelectString As String
		Dim rptDataView As DataView

		SelectString = GetTransactionSelectString()

		Try
			rptDataView = New DataView(rptDataTable, SelectString, "", DataViewRowState.CurrentRows)
		Catch ex As Exception
			SelectString = "true"
			rptDataView = New DataView(rptDataTable, SelectString, "", DataViewRowState.CurrentRows)
		End Try

		' dbo.fn_BestSinglePrice(Transactions.TransactionInstrument, Transactions.TransactionValueDate , @OnlyFinalPrices, @KnowledgeDate) as PriceLevel,
		Dim thisDataRow As DataRow
		Dim GetPriceCommand As New SqlCommand
		Dim Counter As Integer

		GetPriceCommand.Connection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)
		GetPriceCommand.CommandType = CommandType.Text
		GetPriceCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

		rptDataView.AllowEdit = True
		For Counter = 0 To (rptDataView.Count - 1)
			Try
				thisDataRow = rptDataView.Item(Counter).Row
				GetPriceCommand.CommandText = "SELECT dbo.fn_BestSinglePrice(" & thisDataRow("TransactionInstrument").ToString & ", '" & CDate(thisDataRow("TransactionValueDate")).ToString(QUERY_SHORTDATEFORMAT) & "', 0, '" & MainForm.Main_Knowledgedate.ToString(QUERY_LONGDATEFORMAT) & "')"
				SyncLock GetPriceCommand.Connection
          thisDataRow("PriceLevel") = CDbl(MainForm.ExecuteScalar(GetPriceCommand))
          'thisDataRow("PriceLevel") = CDbl(GetPriceCommand.ExecuteScalar)
        End SyncLock
			Catch ex As Exception
			End Try
		Next

		MainForm.MainReportHandler.DisplayReport(0, "rptTransactionPriceDifference", rptDataView, Form_ProgressBar)
	End Sub


    ''' <summary>
    ''' RPTs the trade blotter.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Private Sub rptTradeBlotter(ByVal sender As Object, ByVal e As EventArgs)
		' ***************************************************************************************
		'
		'
		' ***************************************************************************************

		Call ShowTradeBlotter(False)
	End Sub

    ''' <summary>
    ''' RPTs the final trade blotter.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Private Sub rptFinalTradeBlotter(ByVal sender As Object, ByVal e As EventArgs)
		' ***************************************************************************************
		'
		'
		' ***************************************************************************************

		Call ShowTradeBlotter(True)
	End Sub

    ''' <summary>
    ''' RPTs the trade revision blotter.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Private Sub rptTradeRevisionBlotter(ByVal sender As Object, ByVal e As EventArgs)
		' ***************************************************************************************
		'
		'
		' ***************************************************************************************

		Call ShowTradeRevisionBlotter(False)
	End Sub

    ''' <summary>
    ''' RPTs the final trade revision blotter.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Private Sub rptFinalTradeRevisionBlotter(ByVal sender As Object, ByVal e As EventArgs)
		' ***************************************************************************************
		'
		'
		' ***************************************************************************************

		Call ShowTradeRevisionBlotter(True)
	End Sub

    ''' <summary>
    ''' Shows the trade blotter.
    ''' </summary>
    ''' <param name="pFinal">if set to <c>true</c> [p final].</param>
	Private Sub ShowTradeBlotter(Optional ByVal pFinal As Boolean = False)
		' ***************************************************************************************
		'
		'
		' ***************************************************************************************

		Dim TradesDataset As StandardDataset

		Dim UpFrontFeeFunds As String
		Dim TradeBlotterTicketNumber As String
		Dim Counter As Integer

		Try
			TradesDataset = RenaissanceStandardDatasets.GetStandardDataset(RenaissanceChangeID.tblSelectTransaction)
			If (TradesDataset.NotedIDsHaveChanged) Then
				MainForm.Load_Table(TradesDataset, True)
				Call SetSortedRows()
			End If
		Catch ex As Exception
			MainForm.LogError("ShowTradeBlotter()", LOG_LEVELS.Error, ex.Message, "Error loading tblSelectTransaction", ex.StackTrace, True)
			Exit Sub
		End Try

		Try
			TradeBlotterTicketNumber = MainForm.Get_SystemString("TradeBlotterTicketNumber")
			If IsNumeric(TradeBlotterTicketNumber) = False Then
				TradeBlotterTicketNumber = "000001"
				MainForm.Set_SystemString("TradeBlotterTicketNumber", TradeBlotterTicketNumber)
			ElseIf pFinal Then
				If MessageBox.Show("Are you sure that you want to run a FINAL blotter ?" & vbCrLf & "This will increment the Ticker number.", "Are you sure ?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
					TradeBlotterTicketNumber = (CInt(TradeBlotterTicketNumber) + 1).ToString("000000")
					MainForm.Set_SystemString("TradeBlotterTicketNumber", TradeBlotterTicketNumber)
				Else
					Exit Sub
				End If
			End If

			UpFrontFeeFunds = ""

			For Counter = 0 To (myDataView.Count - 1)
				If (CDbl(myDataView(Counter)("InstrumentUpfrontFees")) > 0) Then
					If (UpFrontFeeFunds.Length > 0) Then
						UpFrontFeeFunds &= ", "
					End If
					UpFrontFeeFunds &= CStr(myDataView(Counter)("InstrumentDescription"))
				End If
			Next

			If (UpFrontFeeFunds.Length > 0) Then
				If UpFrontFeeFunds.LastIndexOf(",") > 0 Then
					UpFrontFeeFunds = UpFrontFeeFunds.Substring(0, UpFrontFeeFunds.LastIndexOf(",")) & " and" & UpFrontFeeFunds.Substring(UpFrontFeeFunds.LastIndexOf(",") + 1)
				End If

				UpFrontFeeFunds = " other than for " & UpFrontFeeFunds
			End If

		Catch ex As Exception
			MainForm.LogError("ShowTradeBlotter()", LOG_LEVELS.Error, ex.Message, "Error getting header information.", ex.StackTrace, True)
			Exit Sub
		End Try

		Call MainForm.MainReportHandler.rptTradeBlotterReport(UpFrontFeeFunds, TradeBlotterTicketNumber, myDataView, pFinal, Form_ProgressBar)

	End Sub

    ''' <summary>
    ''' Shows the trade revision blotter.
    ''' </summary>
    ''' <param name="pFinal">if set to <c>true</c> [p final].</param>
	Private Sub ShowTradeRevisionBlotter(Optional ByVal pFinal As Boolean = False)
		' ***************************************************************************************
		'
		'
		' ***************************************************************************************

		Dim TradesDataset As StandardDataset

		Try
			TradesDataset = RenaissanceStandardDatasets.GetStandardDataset(RenaissanceChangeID.tblSelectTransaction)
			If (TradesDataset.NotedIDsHaveChanged) Then
				MainForm.Load_Table(TradesDataset, True)
				Call SetSortedRows()
			End If
		Catch ex As Exception
			MainForm.LogError("ShowTradeRevisionBlotter()", LOG_LEVELS.Error, ex.Message, "Error loading tblSelectTransaction", ex.StackTrace, True)
			Exit Sub
		End Try

		Try
			If (pFinal) Then
				If MessageBox.Show("Are you sure that you want to run a FINAL blotter ?" & vbCrLf & "This will increment the Ticker number.", "Are you sure ?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) <> Windows.Forms.DialogResult.Yes Then
					Exit Sub
				End If
			End If

			Call MainForm.MainReportHandler.rptTradeRevisionReport(myDataView, pFinal, Form_ProgressBar)

		Catch ex As Exception
			MainForm.LogError("ShowTradeBlotter()", LOG_LEVELS.Error, ex.Message, "Error getting header information.", ex.StackTrace, True)
			Exit Sub
		End Try


	End Sub

    ''' <summary>
    ''' RPTs the FX blotter.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Private Sub rptFXBlotter(ByVal sender As Object, ByVal e As EventArgs)
		' ***************************************************************************************
		'
		'
		' ***************************************************************************************

		Call ShowFXBlotter(False)
	End Sub

    ''' <summary>
    ''' RPTs the final FX blotter.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Private Sub rptFinalFXBlotter(ByVal sender As Object, ByVal e As EventArgs)
		' ***************************************************************************************
		'
		'
		' ***************************************************************************************

		Call ShowFXBlotter(True)
	End Sub

    ''' <summary>
    ''' Shows the FX blotter.
    ''' </summary>
    ''' <param name="pFinal">if set to <c>true</c> [p final].</param>
	Private Sub ShowFXBlotter(Optional ByVal pFinal As Boolean = False)
		' ***************************************************************************************
		'
		'
		' ***************************************************************************************

		Dim TradesDataset As StandardDataset
		Dim rptTradesDS As New RenaissanceDataClass.DSSelectTransaction
		Dim rptTradesTbl As RenaissanceDataClass.DSSelectTransaction.tblSelectTransactionDataTable
		Dim TradesRow As RenaissanceDataClass.DSSelectTransaction.tblSelectTransactionRow
		Dim thisDataRow As DataRow
		Dim reportDataView As DataView

		Dim TradeBlotterTicketNumber As String
		Dim ColumnCounter As Integer
		Dim RowCounter As Integer
		Dim Counter As Integer
		Dim ParentIDList As String

		rptTradesTbl = rptTradesDS.tblSelectTransaction

		' Build Source Table

		Try
			TradesDataset = RenaissanceStandardDatasets.GetStandardDataset(RenaissanceChangeID.tblSelectTransaction)
			If (TradesDataset.NotedIDsHaveChanged) Then
				MainForm.Load_Table(TradesDataset, True)
				Call SetSortedRows()
			End If
		Catch ex As Exception
			MainForm.LogError("ShowFXTradeBlotter()", LOG_LEVELS.Error, ex.Message, "Error loading tblSelectTransaction", ex.StackTrace, True)
			Exit Sub
		End Try

		' Derive a DataView from the reference table, further refining the selection to exclude non-FX
		' Transactions.

    reportDataView = New DataView(myTable, "((TransactionType=" & TransactionTypes.BuyFX & ") OR (TransactionType=" & TransactionTypes.SellFX & ") OR (TransactionType=" & TransactionTypes.BuyFXForward & ") OR (TransactionType=" & TransactionTypes.SellFXForward & ")) AND(" & GetTransactionSelectString() & ")", "", DataViewRowState.CurrentRows)

		ParentIDList = ""
		If (reportDataView.Count <= 0) Then
			MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "No FX Transactions Selected.", "", True)
			Exit Sub
		End If

		' Build a list of selected transaction Parent IDs and copy the DataRecords to a new Table.
		' A new table is used so as not to screw up the original table when we add the USD_Settle column.

		For Counter = 0 To (reportDataView.Count - 1)
			ParentIDList &= reportDataView(Counter)("TransactionParentID").ToString
			If Counter < (reportDataView.Count - 1) Then
				ParentIDList &= ","
			End If

			' Copy SourceDataView to rptTradesTbl
			TradesRow = rptTradesTbl.NewtblSelectTransactionRow
			For ColumnCounter = 0 To (rptTradesTbl.Columns.Count - 1)
				TradesRow(ColumnCounter) = reportDataView(Counter)(ColumnCounter)
			Next
			rptTradesTbl.Rows.Add(TradesRow)
		Next

		' Get SecondLeg Settlement values
		' 

		Dim SecondLegTable As New DataTable

		Dim tempAdaptor As New SqlDataAdapter
		Dim tempCommand As New SqlCommand
		tempCommand.Connection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)
		tempCommand.CommandType = CommandType.Text
		tempCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

		tempCommand.CommandText = "SELECT RN, TransactionParentID, TransactionSettlement FROM fn_tblTransaction_SelectKD(@KnowledgeDate) WHERE (TransactionLeg = 2) AND (TransactionParentID IN (" & ParentIDList & "))"
		tempCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))
		tempCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
		tempAdaptor.SelectCommand = tempCommand
		SyncLock tempCommand.Connection
			tempAdaptor.Fill(SecondLegTable)
		End SyncLock

		' Add USD_Settle Column
		' and populate it with the Signed Settlment from the Second Transaction Leg.

		rptTradesTbl.Columns.Add(New DataColumn("USD_Settle", GetType(Double)))

		For Counter = 0 To (rptTradesTbl.Rows.Count - 1)
			thisDataRow = rptTradesTbl.Rows(Counter)
			thisDataRow("USD_Settle") = 0

			For RowCounter = 0 To (SecondLegTable.Rows.Count - 1)
				If (CInt(thisDataRow("TransactionParentID")) = CInt(SecondLegTable.Rows(RowCounter)("TransactionParentID"))) Then
					thisDataRow("USD_Settle") = SecondLegTable.Rows(RowCounter)("TransactionSettlement")
					Exit For
				End If
			Next
		Next

		' Get Ticket Number

		Try
			TradeBlotterTicketNumber = MainForm.Get_SystemString("FXBlotterTicketNumber")

			If IsNumeric(TradeBlotterTicketNumber) = False Then
				TradeBlotterTicketNumber = "000001"
				MainForm.Set_SystemString("FXBlotterTicketNumber", TradeBlotterTicketNumber)
			ElseIf pFinal Then
				If MessageBox.Show("Are you sure that you want to run a FINAL blotter ?" & vbCrLf & "This will increment the Ticker number.", "Are you sure ?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
					TradeBlotterTicketNumber = (CInt(TradeBlotterTicketNumber) + 1).ToString("000000")
					MainForm.Set_SystemString("FXBlotterTicketNumber", TradeBlotterTicketNumber)
				Else
					Exit Sub
				End If
			End If

		Catch ex As Exception
			MainForm.LogError("ShowFXTradeBlotter()", LOG_LEVELS.Error, ex.Message, "Error getting header information.", ex.StackTrace, True)
			Exit Sub
		End Try

		' Display Report

		Call MainForm.MainReportHandler.rptFXTradeBlotterReport(TradeBlotterTicketNumber, rptTradesTbl, pFinal, Form_ProgressBar)

	End Sub

#End Region

#Region " Bug hunting "

  ' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
  ' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
  ' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

    ''' <summary>
    ''' Called when [row updating].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

    ''' <summary>
    ''' Called when [row updated].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

    ''' <summary>
    ''' Called when [row fill error].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
    Dim a As Integer

    a = 1
  End Sub

#End Region






End Class
