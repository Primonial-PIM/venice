' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmChangeControlReview.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals


''' <summary>
''' Class frmChangeControlReview
''' </summary>
Public Class frmChangeControlReview

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmChangeControlReview"/> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The LBL currency code
    ''' </summary>
  Friend WithEvents lblCurrencyCode As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit audit ID
    ''' </summary>
  Friend WithEvents editAuditID As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The text_ change request
    ''' </summary>
  Friend WithEvents Text_ChangeRequest As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The BTN nav first
    ''' </summary>
  Friend WithEvents btnNavFirst As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN nav prev
    ''' </summary>
  Friend WithEvents btnNavPrev As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN nav next
    ''' </summary>
  Friend WithEvents btnNavNext As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN last
    ''' </summary>
  Friend WithEvents btnLast As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN add
    ''' </summary>
  Friend WithEvents btnAdd As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN delete
    ''' </summary>
  Friend WithEvents btnDelete As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN cancel
    ''' </summary>
  Friend WithEvents btnCancel As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN save
    ''' </summary>
  Friend WithEvents btnSave As System.Windows.Forms.Button
    ''' <summary>
    ''' The combo_ select change ID
    ''' </summary>
  Friend WithEvents Combo_SelectChangeID As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The panel1
    ''' </summary>
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The label1
    ''' </summary>
  Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The group box1
    ''' </summary>
  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The root menu
    ''' </summary>
  Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
    ''' <summary>
    ''' The label2
    ''' </summary>
  Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ product
    ''' </summary>
  Friend WithEvents Label_Product As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ date raised
    ''' </summary>
  Friend WithEvents Label_DateRaised As System.Windows.Forms.Label
    ''' <summary>
    ''' The label5
    ''' </summary>
  Friend WithEvents Label5 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ raised by
    ''' </summary>
  Friend WithEvents Label_RaisedBy As System.Windows.Forms.Label
    ''' <summary>
    ''' The label4
    ''' </summary>
  Friend WithEvents Label4 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label6
    ''' </summary>
  Friend WithEvents Label6 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ priority
    ''' </summary>
  Friend WithEvents Combo_Priority As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label8
    ''' </summary>
  Friend WithEvents Label8 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ change category
    ''' </summary>
  Friend WithEvents Combo_ChangeCategory As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The text_ detailed description
    ''' </summary>
  Friend WithEvents Text_DetailedDescription As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The label9
    ''' </summary>
  Friend WithEvents Label9 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label10
    ''' </summary>
  Friend WithEvents Label10 As System.Windows.Forms.Label
    ''' <summary>
    ''' The group box2
    ''' </summary>
  Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The tab control_ units
    ''' </summary>
  Friend WithEvents TabControl_Units As System.Windows.Forms.TabControl
    ''' <summary>
    ''' The tab page_ scope
    ''' </summary>
  Friend WithEvents TabPage_Scope As System.Windows.Forms.TabPage
    ''' <summary>
    ''' The tab page_ dependencies
    ''' </summary>
  Friend WithEvents TabPage_Dependencies As System.Windows.Forms.TabPage
    ''' <summary>
    ''' The text box_ scope
    ''' </summary>
  Friend WithEvents TextBox_Scope As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The tab page_ restrictions
    ''' </summary>
  Friend WithEvents TabPage_Restrictions As System.Windows.Forms.TabPage
    ''' <summary>
    ''' The tab page_ implementation
    ''' </summary>
  Friend WithEvents TabPage_Implementation As System.Windows.Forms.TabPage
    ''' <summary>
    ''' The tab page_ test plan
    ''' </summary>
  Friend WithEvents TabPage_TestPlan As System.Windows.Forms.TabPage
    ''' <summary>
    ''' The text box_ dependencies
    ''' </summary>
  Friend WithEvents TextBox_Dependencies As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The text box_ restrictions
    ''' </summary>
  Friend WithEvents TextBox_Restrictions As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The text box_ implementation
    ''' </summary>
  Friend WithEvents TextBox_Implementation As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The text box_ test plan
    ''' </summary>
  Friend WithEvents TextBox_TestPlan As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The group box3
    ''' </summary>
  Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The group box4
    ''' </summary>
  Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The group box5
    ''' </summary>
  Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The group box6
    ''' </summary>
  Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The check_ review_ rejected
    ''' </summary>
  Friend WithEvents Check_Review_Rejected As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ IT reviewed
    ''' </summary>
  Friend WithEvents Check_ITReviewed As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ authorised rejected
    ''' </summary>
  Friend WithEvents Check_AuthorisedRejected As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ authorised business
    ''' </summary>
  Friend WithEvents Check_AuthorisedBusiness As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ authorised owner
    ''' </summary>
  Friend WithEvents Check_AuthorisedOwner As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ authorised IT
    ''' </summary>
  Friend WithEvents Check_AuthorisedIT As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ accepted business
    ''' </summary>
  Friend WithEvents Check_AcceptedBusiness As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ accepted owner
    ''' </summary>
  Friend WithEvents Check_AcceptedOwner As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ accepted IT
    ''' </summary>
  Friend WithEvents Check_AcceptedIT As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The label3
    ''' </summary>
  Friend WithEvents Label3 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ reference number
    ''' </summary>
  Friend WithEvents Label_ReferenceNumber As System.Windows.Forms.Label
    ''' <summary>
    ''' The BTN close
    ''' </summary>
  Friend WithEvents btnClose As System.Windows.Forms.Button
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.lblCurrencyCode = New System.Windows.Forms.Label
    Me.editAuditID = New System.Windows.Forms.TextBox
    Me.Text_ChangeRequest = New System.Windows.Forms.TextBox
    Me.btnNavFirst = New System.Windows.Forms.Button
    Me.btnNavPrev = New System.Windows.Forms.Button
    Me.btnNavNext = New System.Windows.Forms.Button
    Me.btnLast = New System.Windows.Forms.Button
    Me.btnAdd = New System.Windows.Forms.Button
    Me.btnDelete = New System.Windows.Forms.Button
    Me.btnCancel = New System.Windows.Forms.Button
    Me.btnSave = New System.Windows.Forms.Button
    Me.Combo_SelectChangeID = New System.Windows.Forms.ComboBox
    Me.Panel1 = New System.Windows.Forms.Panel
    Me.Label1 = New System.Windows.Forms.Label
    Me.GroupBox1 = New System.Windows.Forms.GroupBox
    Me.btnClose = New System.Windows.Forms.Button
    Me.RootMenu = New System.Windows.Forms.MenuStrip
    Me.Label2 = New System.Windows.Forms.Label
    Me.Label_Product = New System.Windows.Forms.Label
    Me.Label_DateRaised = New System.Windows.Forms.Label
    Me.Label5 = New System.Windows.Forms.Label
    Me.Label_RaisedBy = New System.Windows.Forms.Label
    Me.Label4 = New System.Windows.Forms.Label
    Me.Label6 = New System.Windows.Forms.Label
    Me.Combo_Priority = New System.Windows.Forms.ComboBox
    Me.Label8 = New System.Windows.Forms.Label
    Me.Combo_ChangeCategory = New System.Windows.Forms.ComboBox
    Me.Text_DetailedDescription = New System.Windows.Forms.TextBox
    Me.Label9 = New System.Windows.Forms.Label
    Me.Label10 = New System.Windows.Forms.Label
    Me.GroupBox2 = New System.Windows.Forms.GroupBox
    Me.TabControl_Units = New System.Windows.Forms.TabControl
    Me.TabPage_Scope = New System.Windows.Forms.TabPage
    Me.TextBox_Scope = New System.Windows.Forms.TextBox
    Me.TabPage_Dependencies = New System.Windows.Forms.TabPage
    Me.TextBox_Dependencies = New System.Windows.Forms.TextBox
    Me.TabPage_Restrictions = New System.Windows.Forms.TabPage
    Me.TextBox_Restrictions = New System.Windows.Forms.TextBox
    Me.TabPage_Implementation = New System.Windows.Forms.TabPage
    Me.TextBox_Implementation = New System.Windows.Forms.TextBox
    Me.TabPage_TestPlan = New System.Windows.Forms.TabPage
    Me.TextBox_TestPlan = New System.Windows.Forms.TextBox
    Me.GroupBox3 = New System.Windows.Forms.GroupBox
    Me.GroupBox4 = New System.Windows.Forms.GroupBox
    Me.Check_Review_Rejected = New System.Windows.Forms.CheckBox
    Me.Check_ITReviewed = New System.Windows.Forms.CheckBox
    Me.GroupBox5 = New System.Windows.Forms.GroupBox
    Me.Check_AuthorisedRejected = New System.Windows.Forms.CheckBox
    Me.Check_AuthorisedBusiness = New System.Windows.Forms.CheckBox
    Me.Check_AuthorisedOwner = New System.Windows.Forms.CheckBox
    Me.Check_AuthorisedIT = New System.Windows.Forms.CheckBox
    Me.GroupBox6 = New System.Windows.Forms.GroupBox
    Me.Check_AcceptedBusiness = New System.Windows.Forms.CheckBox
    Me.Check_AcceptedOwner = New System.Windows.Forms.CheckBox
    Me.Check_AcceptedIT = New System.Windows.Forms.CheckBox
    Me.Label3 = New System.Windows.Forms.Label
    Me.Label_ReferenceNumber = New System.Windows.Forms.Label
    Me.Panel1.SuspendLayout()
    Me.TabControl_Units.SuspendLayout()
    Me.TabPage_Scope.SuspendLayout()
    Me.TabPage_Dependencies.SuspendLayout()
    Me.TabPage_Restrictions.SuspendLayout()
    Me.TabPage_Implementation.SuspendLayout()
    Me.TabPage_TestPlan.SuspendLayout()
    Me.GroupBox4.SuspendLayout()
    Me.GroupBox5.SuspendLayout()
    Me.GroupBox6.SuspendLayout()
    Me.SuspendLayout()
    '
    'lblCurrencyCode
    '
    Me.lblCurrencyCode.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.lblCurrencyCode.Location = New System.Drawing.Point(8, 126)
    Me.lblCurrencyCode.Name = "lblCurrencyCode"
    Me.lblCurrencyCode.Size = New System.Drawing.Size(100, 23)
    Me.lblCurrencyCode.TabIndex = 39
    Me.lblCurrencyCode.Text = "Change Request"
    '
    'editAuditID
    '
    Me.editAuditID.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.editAuditID.Enabled = False
    Me.editAuditID.Location = New System.Drawing.Point(677, 27)
    Me.editAuditID.Name = "editAuditID"
    Me.editAuditID.Size = New System.Drawing.Size(64, 20)
    Me.editAuditID.TabIndex = 1
    Me.editAuditID.TabStop = False
    '
    'Text_ChangeRequest
    '
    Me.Text_ChangeRequest.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Text_ChangeRequest.Location = New System.Drawing.Point(120, 126)
    Me.Text_ChangeRequest.Multiline = True
    Me.Text_ChangeRequest.Name = "Text_ChangeRequest"
    Me.Text_ChangeRequest.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.Text_ChangeRequest.Size = New System.Drawing.Size(621, 92)
    Me.Text_ChangeRequest.TabIndex = 8
    '
    'btnNavFirst
    '
    Me.btnNavFirst.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNavFirst.Location = New System.Drawing.Point(8, 8)
    Me.btnNavFirst.Name = "btnNavFirst"
    Me.btnNavFirst.Size = New System.Drawing.Size(40, 28)
    Me.btnNavFirst.TabIndex = 0
    Me.btnNavFirst.Text = "<<"
    '
    'btnNavPrev
    '
    Me.btnNavPrev.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNavPrev.Location = New System.Drawing.Point(49, 8)
    Me.btnNavPrev.Name = "btnNavPrev"
    Me.btnNavPrev.Size = New System.Drawing.Size(35, 28)
    Me.btnNavPrev.TabIndex = 1
    Me.btnNavPrev.Text = "<"
    '
    'btnNavNext
    '
    Me.btnNavNext.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNavNext.Location = New System.Drawing.Point(88, 8)
    Me.btnNavNext.Name = "btnNavNext"
    Me.btnNavNext.Size = New System.Drawing.Size(35, 28)
    Me.btnNavNext.TabIndex = 2
    Me.btnNavNext.Text = ">"
    '
    'btnLast
    '
    Me.btnLast.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnLast.Location = New System.Drawing.Point(124, 8)
    Me.btnLast.Name = "btnLast"
    Me.btnLast.Size = New System.Drawing.Size(40, 28)
    Me.btnLast.TabIndex = 3
    Me.btnLast.Text = ">>"
    '
    'btnAdd
    '
    Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnAdd.Location = New System.Drawing.Point(176, 8)
    Me.btnAdd.Name = "btnAdd"
    Me.btnAdd.Size = New System.Drawing.Size(75, 28)
    Me.btnAdd.TabIndex = 4
    Me.btnAdd.Text = "&New"
    '
    'btnDelete
    '
    Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnDelete.Location = New System.Drawing.Point(443, 604)
    Me.btnDelete.Name = "btnDelete"
    Me.btnDelete.Size = New System.Drawing.Size(75, 28)
    Me.btnDelete.TabIndex = 16
    Me.btnDelete.Text = "&Delete"
    '
    'btnCancel
    '
    Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnCancel.Location = New System.Drawing.Point(527, 604)
    Me.btnCancel.Name = "btnCancel"
    Me.btnCancel.Size = New System.Drawing.Size(75, 28)
    Me.btnCancel.TabIndex = 17
    Me.btnCancel.Text = "&Cancel"
    '
    'btnSave
    '
    Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnSave.Location = New System.Drawing.Point(355, 604)
    Me.btnSave.Name = "btnSave"
    Me.btnSave.Size = New System.Drawing.Size(75, 28)
    Me.btnSave.TabIndex = 15
    Me.btnSave.Text = "&Save"
    '
    'Combo_SelectChangeID
    '
    Me.Combo_SelectChangeID.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_SelectChangeID.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectChangeID.Location = New System.Drawing.Point(120, 27)
    Me.Combo_SelectChangeID.Name = "Combo_SelectChangeID"
    Me.Combo_SelectChangeID.Size = New System.Drawing.Size(162, 21)
    Me.Combo_SelectChangeID.TabIndex = 0
    '
    'Panel1
    '
    Me.Panel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Panel1.Controls.Add(Me.btnNavFirst)
    Me.Panel1.Controls.Add(Me.btnNavPrev)
    Me.Panel1.Controls.Add(Me.btnNavNext)
    Me.Panel1.Controls.Add(Me.btnLast)
    Me.Panel1.Controls.Add(Me.btnAdd)
    Me.Panel1.Location = New System.Drawing.Point(79, 595)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(260, 48)
    Me.Panel1.TabIndex = 14
    '
    'Label1
    '
    Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label1.Location = New System.Drawing.Point(8, 30)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(100, 17)
    Me.Label1.TabIndex = 20
    Me.Label1.Text = "Select"
    '
    'GroupBox1
    '
    Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.GroupBox1.Location = New System.Drawing.Point(3, 54)
    Me.GroupBox1.Name = "GroupBox1"
    Me.GroupBox1.Size = New System.Drawing.Size(738, 4)
    Me.GroupBox1.TabIndex = 77
    Me.GroupBox1.TabStop = False
    Me.GroupBox1.Text = "GroupBox1"
    '
    'btnClose
    '
    Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnClose.Location = New System.Drawing.Point(611, 604)
    Me.btnClose.Name = "btnClose"
    Me.btnClose.Size = New System.Drawing.Size(75, 28)
    Me.btnClose.TabIndex = 18
    Me.btnClose.Text = "&Close"
    '
    'RootMenu
    '
    Me.RootMenu.Location = New System.Drawing.Point(0, 0)
    Me.RootMenu.Name = "RootMenu"
    Me.RootMenu.Size = New System.Drawing.Size(747, 24)
    Me.RootMenu.TabIndex = 19
    Me.RootMenu.Text = " "
    '
    'Label2
    '
    Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label2.AutoSize = True
    Me.Label2.Location = New System.Drawing.Point(321, 69)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(44, 13)
    Me.Label2.TabIndex = 81
    Me.Label2.Text = "Product"
    Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Label_Product
    '
    Me.Label_Product.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label_Product.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_Product.Location = New System.Drawing.Point(371, 67)
    Me.Label_Product.Name = "Label_Product"
    Me.Label_Product.Size = New System.Drawing.Size(142, 20)
    Me.Label_Product.TabIndex = 3
    Me.Label_Product.Text = "Product"
    '
    'Label_DateRaised
    '
    Me.Label_DateRaised.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label_DateRaised.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_DateRaised.Location = New System.Drawing.Point(599, 67)
    Me.Label_DateRaised.Name = "Label_DateRaised"
    Me.Label_DateRaised.Size = New System.Drawing.Size(142, 20)
    Me.Label_DateRaised.TabIndex = 4
    Me.Label_DateRaised.Text = "Product"
    '
    'Label5
    '
    Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label5.AutoSize = True
    Me.Label5.Location = New System.Drawing.Point(527, 69)
    Me.Label5.Name = "Label5"
    Me.Label5.Size = New System.Drawing.Size(66, 13)
    Me.Label5.TabIndex = 83
    Me.Label5.Text = "Date Raised"
    Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Label_RaisedBy
    '
    Me.Label_RaisedBy.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label_RaisedBy.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_RaisedBy.Location = New System.Drawing.Point(371, 97)
    Me.Label_RaisedBy.Name = "Label_RaisedBy"
    Me.Label_RaisedBy.Size = New System.Drawing.Size(142, 20)
    Me.Label_RaisedBy.TabIndex = 6
    Me.Label_RaisedBy.Text = "Product"
    '
    'Label4
    '
    Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label4.AutoSize = True
    Me.Label4.Location = New System.Drawing.Point(310, 99)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(55, 13)
    Me.Label4.TabIndex = 85
    Me.Label4.Text = "Raised By"
    Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Label6
    '
    Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label6.AutoSize = True
    Me.Label6.Location = New System.Drawing.Point(538, 99)
    Me.Label6.Name = "Label6"
    Me.Label6.Size = New System.Drawing.Size(38, 13)
    Me.Label6.TabIndex = 87
    Me.Label6.Text = "Priority"
    Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Combo_Priority
    '
    Me.Combo_Priority.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Priority.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Priority.Items.AddRange(New Object() {"1 (High)", "2", "3", "4", "5", "6", "7", "8", "9", "10 (Low)"})
    Me.Combo_Priority.Location = New System.Drawing.Point(599, 96)
    Me.Combo_Priority.Name = "Combo_Priority"
    Me.Combo_Priority.Size = New System.Drawing.Size(142, 21)
    Me.Combo_Priority.TabIndex = 7
    '
    'Label8
    '
    Me.Label8.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label8.Location = New System.Drawing.Point(8, 99)
    Me.Label8.Name = "Label8"
    Me.Label8.Size = New System.Drawing.Size(100, 17)
    Me.Label8.TabIndex = 92
    Me.Label8.Text = "Change Category"
    '
    'Combo_ChangeCategory
    '
    Me.Combo_ChangeCategory.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_ChangeCategory.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_ChangeCategory.Items.AddRange(New Object() {"Bug Fix", "New Feature", "Data Change"})
    Me.Combo_ChangeCategory.Location = New System.Drawing.Point(120, 96)
    Me.Combo_ChangeCategory.Name = "Combo_ChangeCategory"
    Me.Combo_ChangeCategory.Size = New System.Drawing.Size(162, 21)
    Me.Combo_ChangeCategory.TabIndex = 5
    '
    'Text_DetailedDescription
    '
    Me.Text_DetailedDescription.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Text_DetailedDescription.Location = New System.Drawing.Point(120, 224)
    Me.Text_DetailedDescription.Multiline = True
    Me.Text_DetailedDescription.Name = "Text_DetailedDescription"
    Me.Text_DetailedDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.Text_DetailedDescription.Size = New System.Drawing.Size(621, 88)
    Me.Text_DetailedDescription.TabIndex = 9
    '
    'Label9
    '
    Me.Label9.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label9.Location = New System.Drawing.Point(8, 224)
    Me.Label9.Name = "Label9"
    Me.Label9.Size = New System.Drawing.Size(100, 23)
    Me.Label9.TabIndex = 94
    Me.Label9.Text = "Detailed Description"
    '
    'Label10
    '
    Me.Label10.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label10.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label10.Location = New System.Drawing.Point(7, 330)
    Me.Label10.Name = "Label10"
    Me.Label10.Size = New System.Drawing.Size(106, 20)
    Me.Label10.TabIndex = 96
    Me.Label10.Text = "Change Control units"
    '
    'GroupBox2
    '
    Me.GroupBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.GroupBox2.Location = New System.Drawing.Point(3, 318)
    Me.GroupBox2.Name = "GroupBox2"
    Me.GroupBox2.Size = New System.Drawing.Size(740, 4)
    Me.GroupBox2.TabIndex = 97
    Me.GroupBox2.TabStop = False
    Me.GroupBox2.Text = "GroupBox2"
    '
    'TabControl_Units
    '
    Me.TabControl_Units.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.TabControl_Units.Controls.Add(Me.TabPage_Scope)
    Me.TabControl_Units.Controls.Add(Me.TabPage_Dependencies)
    Me.TabControl_Units.Controls.Add(Me.TabPage_Restrictions)
    Me.TabControl_Units.Controls.Add(Me.TabPage_Implementation)
    Me.TabControl_Units.Controls.Add(Me.TabPage_TestPlan)
    Me.TabControl_Units.Location = New System.Drawing.Point(120, 328)
    Me.TabControl_Units.Name = "TabControl_Units"
    Me.TabControl_Units.SelectedIndex = 0
    Me.TabControl_Units.Size = New System.Drawing.Size(621, 140)
    Me.TabControl_Units.TabIndex = 10
    '
    'TabPage_Scope
    '
    Me.TabPage_Scope.Controls.Add(Me.TextBox_Scope)
    Me.TabPage_Scope.Location = New System.Drawing.Point(4, 22)
    Me.TabPage_Scope.Name = "TabPage_Scope"
    Me.TabPage_Scope.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage_Scope.Size = New System.Drawing.Size(613, 114)
    Me.TabPage_Scope.TabIndex = 0
    Me.TabPage_Scope.Text = "Scope"
    Me.TabPage_Scope.UseVisualStyleBackColor = True
    '
    'TextBox_Scope
    '
    Me.TextBox_Scope.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.TextBox_Scope.Location = New System.Drawing.Point(0, 0)
    Me.TextBox_Scope.Multiline = True
    Me.TextBox_Scope.Name = "TextBox_Scope"
    Me.TextBox_Scope.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.TextBox_Scope.Size = New System.Drawing.Size(613, 114)
    Me.TextBox_Scope.TabIndex = 0
    '
    'TabPage_Dependencies
    '
    Me.TabPage_Dependencies.Controls.Add(Me.TextBox_Dependencies)
    Me.TabPage_Dependencies.Location = New System.Drawing.Point(4, 22)
    Me.TabPage_Dependencies.Name = "TabPage_Dependencies"
    Me.TabPage_Dependencies.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage_Dependencies.Size = New System.Drawing.Size(613, 114)
    Me.TabPage_Dependencies.TabIndex = 1
    Me.TabPage_Dependencies.Text = "Dependencies"
    Me.TabPage_Dependencies.UseVisualStyleBackColor = True
    '
    'TextBox_Dependencies
    '
    Me.TextBox_Dependencies.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.TextBox_Dependencies.Location = New System.Drawing.Point(0, 0)
    Me.TextBox_Dependencies.Multiline = True
    Me.TextBox_Dependencies.Name = "TextBox_Dependencies"
    Me.TextBox_Dependencies.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.TextBox_Dependencies.Size = New System.Drawing.Size(613, 114)
    Me.TextBox_Dependencies.TabIndex = 100
    '
    'TabPage_Restrictions
    '
    Me.TabPage_Restrictions.Controls.Add(Me.TextBox_Restrictions)
    Me.TabPage_Restrictions.Location = New System.Drawing.Point(4, 22)
    Me.TabPage_Restrictions.Name = "TabPage_Restrictions"
    Me.TabPage_Restrictions.Size = New System.Drawing.Size(613, 114)
    Me.TabPage_Restrictions.TabIndex = 2
    Me.TabPage_Restrictions.Text = "Restrictions"
    Me.TabPage_Restrictions.UseVisualStyleBackColor = True
    '
    'TextBox_Restrictions
    '
    Me.TextBox_Restrictions.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.TextBox_Restrictions.Location = New System.Drawing.Point(0, 0)
    Me.TextBox_Restrictions.Multiline = True
    Me.TextBox_Restrictions.Name = "TextBox_Restrictions"
    Me.TextBox_Restrictions.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.TextBox_Restrictions.Size = New System.Drawing.Size(613, 114)
    Me.TextBox_Restrictions.TabIndex = 100
    '
    'TabPage_Implementation
    '
    Me.TabPage_Implementation.Controls.Add(Me.TextBox_Implementation)
    Me.TabPage_Implementation.Location = New System.Drawing.Point(4, 22)
    Me.TabPage_Implementation.Name = "TabPage_Implementation"
    Me.TabPage_Implementation.Size = New System.Drawing.Size(613, 114)
    Me.TabPage_Implementation.TabIndex = 3
    Me.TabPage_Implementation.Text = "Implementation"
    Me.TabPage_Implementation.UseVisualStyleBackColor = True
    '
    'TextBox_Implementation
    '
    Me.TextBox_Implementation.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.TextBox_Implementation.Location = New System.Drawing.Point(0, 0)
    Me.TextBox_Implementation.Multiline = True
    Me.TextBox_Implementation.Name = "TextBox_Implementation"
    Me.TextBox_Implementation.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.TextBox_Implementation.Size = New System.Drawing.Size(613, 114)
    Me.TextBox_Implementation.TabIndex = 100
    '
    'TabPage_TestPlan
    '
    Me.TabPage_TestPlan.Controls.Add(Me.TextBox_TestPlan)
    Me.TabPage_TestPlan.Location = New System.Drawing.Point(4, 22)
    Me.TabPage_TestPlan.Name = "TabPage_TestPlan"
    Me.TabPage_TestPlan.Size = New System.Drawing.Size(613, 114)
    Me.TabPage_TestPlan.TabIndex = 4
    Me.TabPage_TestPlan.Text = "Test Plan"
    Me.TabPage_TestPlan.UseVisualStyleBackColor = True
    '
    'TextBox_TestPlan
    '
    Me.TextBox_TestPlan.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.TextBox_TestPlan.Location = New System.Drawing.Point(0, 0)
    Me.TextBox_TestPlan.Multiline = True
    Me.TextBox_TestPlan.Name = "TextBox_TestPlan"
    Me.TextBox_TestPlan.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.TextBox_TestPlan.Size = New System.Drawing.Size(613, 114)
    Me.TextBox_TestPlan.TabIndex = 100
    '
    'GroupBox3
    '
    Me.GroupBox3.Anchor = System.Windows.Forms.AnchorStyles.Bottom
    Me.GroupBox3.Location = New System.Drawing.Point(3, 471)
    Me.GroupBox3.Name = "GroupBox3"
    Me.GroupBox3.Size = New System.Drawing.Size(740, 4)
    Me.GroupBox3.TabIndex = 99
    Me.GroupBox3.TabStop = False
    Me.GroupBox3.Text = "GroupBox3"
    '
    'GroupBox4
    '
    Me.GroupBox4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.GroupBox4.Controls.Add(Me.Check_Review_Rejected)
    Me.GroupBox4.Controls.Add(Me.Check_ITReviewed)
    Me.GroupBox4.Location = New System.Drawing.Point(23, 481)
    Me.GroupBox4.Name = "GroupBox4"
    Me.GroupBox4.Size = New System.Drawing.Size(228, 105)
    Me.GroupBox4.TabIndex = 11
    Me.GroupBox4.TabStop = False
    Me.GroupBox4.Text = "Review"
    '
    'Check_Review_Rejected
    '
    Me.Check_Review_Rejected.AutoSize = True
    Me.Check_Review_Rejected.Location = New System.Drawing.Point(10, 39)
    Me.Check_Review_Rejected.Name = "Check_Review_Rejected"
    Me.Check_Review_Rejected.Size = New System.Drawing.Size(145, 17)
    Me.Check_Review_Rejected.TabIndex = 1
    Me.Check_Review_Rejected.Text = "Change Control Rejected"
    Me.Check_Review_Rejected.UseVisualStyleBackColor = True
    '
    'Check_ITReviewed
    '
    Me.Check_ITReviewed.AutoSize = True
    Me.Check_ITReviewed.Location = New System.Drawing.Point(10, 19)
    Me.Check_ITReviewed.Name = "Check_ITReviewed"
    Me.Check_ITReviewed.Size = New System.Drawing.Size(87, 17)
    Me.Check_ITReviewed.TabIndex = 0
    Me.Check_ITReviewed.Text = "IT Reviewed"
    Me.Check_ITReviewed.UseVisualStyleBackColor = True
    '
    'GroupBox5
    '
    Me.GroupBox5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.GroupBox5.Controls.Add(Me.Check_AuthorisedRejected)
    Me.GroupBox5.Controls.Add(Me.Check_AuthorisedBusiness)
    Me.GroupBox5.Controls.Add(Me.Check_AuthorisedOwner)
    Me.GroupBox5.Controls.Add(Me.Check_AuthorisedIT)
    Me.GroupBox5.Location = New System.Drawing.Point(257, 481)
    Me.GroupBox5.Name = "GroupBox5"
    Me.GroupBox5.Size = New System.Drawing.Size(228, 105)
    Me.GroupBox5.TabIndex = 12
    Me.GroupBox5.TabStop = False
    Me.GroupBox5.Text = "Authorisation"
    '
    'Check_AuthorisedRejected
    '
    Me.Check_AuthorisedRejected.AutoSize = True
    Me.Check_AuthorisedRejected.Location = New System.Drawing.Point(6, 79)
    Me.Check_AuthorisedRejected.Name = "Check_AuthorisedRejected"
    Me.Check_AuthorisedRejected.Size = New System.Drawing.Size(145, 17)
    Me.Check_AuthorisedRejected.TabIndex = 3
    Me.Check_AuthorisedRejected.Text = "Change Control Rejected"
    Me.Check_AuthorisedRejected.UseVisualStyleBackColor = True
    '
    'Check_AuthorisedBusiness
    '
    Me.Check_AuthorisedBusiness.AutoSize = True
    Me.Check_AuthorisedBusiness.Location = New System.Drawing.Point(6, 59)
    Me.Check_AuthorisedBusiness.Name = "Check_AuthorisedBusiness"
    Me.Check_AuthorisedBusiness.Size = New System.Drawing.Size(135, 17)
    Me.Check_AuthorisedBusiness.TabIndex = 2
    Me.Check_AuthorisedBusiness.Text = "Authorised by Business"
    Me.Check_AuthorisedBusiness.UseVisualStyleBackColor = True
    '
    'Check_AuthorisedOwner
    '
    Me.Check_AuthorisedOwner.AutoSize = True
    Me.Check_AuthorisedOwner.Location = New System.Drawing.Point(6, 39)
    Me.Check_AuthorisedOwner.Name = "Check_AuthorisedOwner"
    Me.Check_AuthorisedOwner.Size = New System.Drawing.Size(124, 17)
    Me.Check_AuthorisedOwner.TabIndex = 1
    Me.Check_AuthorisedOwner.Text = "Authorised by Owner"
    Me.Check_AuthorisedOwner.UseVisualStyleBackColor = True
    '
    'Check_AuthorisedIT
    '
    Me.Check_AuthorisedIT.AutoSize = True
    Me.Check_AuthorisedIT.Location = New System.Drawing.Point(6, 19)
    Me.Check_AuthorisedIT.Name = "Check_AuthorisedIT"
    Me.Check_AuthorisedIT.Size = New System.Drawing.Size(103, 17)
    Me.Check_AuthorisedIT.TabIndex = 0
    Me.Check_AuthorisedIT.Text = "Authorised by IT"
    Me.Check_AuthorisedIT.UseVisualStyleBackColor = True
    '
    'GroupBox6
    '
    Me.GroupBox6.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.GroupBox6.Controls.Add(Me.Check_AcceptedBusiness)
    Me.GroupBox6.Controls.Add(Me.Check_AcceptedOwner)
    Me.GroupBox6.Controls.Add(Me.Check_AcceptedIT)
    Me.GroupBox6.Location = New System.Drawing.Point(491, 481)
    Me.GroupBox6.Name = "GroupBox6"
    Me.GroupBox6.Size = New System.Drawing.Size(228, 105)
    Me.GroupBox6.TabIndex = 13
    Me.GroupBox6.TabStop = False
    Me.GroupBox6.Text = "Acceptance"
    '
    'Check_AcceptedBusiness
    '
    Me.Check_AcceptedBusiness.AutoSize = True
    Me.Check_AcceptedBusiness.Location = New System.Drawing.Point(6, 59)
    Me.Check_AcceptedBusiness.Name = "Check_AcceptedBusiness"
    Me.Check_AcceptedBusiness.Size = New System.Drawing.Size(131, 17)
    Me.Check_AcceptedBusiness.TabIndex = 2
    Me.Check_AcceptedBusiness.Text = "Accepted by Business"
    Me.Check_AcceptedBusiness.UseVisualStyleBackColor = True
    '
    'Check_AcceptedOwner
    '
    Me.Check_AcceptedOwner.AutoSize = True
    Me.Check_AcceptedOwner.Location = New System.Drawing.Point(6, 39)
    Me.Check_AcceptedOwner.Name = "Check_AcceptedOwner"
    Me.Check_AcceptedOwner.Size = New System.Drawing.Size(120, 17)
    Me.Check_AcceptedOwner.TabIndex = 1
    Me.Check_AcceptedOwner.Text = "Accepted by Owner"
    Me.Check_AcceptedOwner.UseVisualStyleBackColor = True
    '
    'Check_AcceptedIT
    '
    Me.Check_AcceptedIT.AutoSize = True
    Me.Check_AcceptedIT.Location = New System.Drawing.Point(6, 19)
    Me.Check_AcceptedIT.Name = "Check_AcceptedIT"
    Me.Check_AcceptedIT.Size = New System.Drawing.Size(99, 17)
    Me.Check_AcceptedIT.TabIndex = 0
    Me.Check_AcceptedIT.Text = "Accepted by IT"
    Me.Check_AcceptedIT.UseVisualStyleBackColor = True
    '
    'Label3
    '
    Me.Label3.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label3.Location = New System.Drawing.Point(8, 69)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(100, 17)
    Me.Label3.TabIndex = 101
    Me.Label3.Text = "Reference Number"
    '
    'Label_ReferenceNumber
    '
    Me.Label_ReferenceNumber.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label_ReferenceNumber.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_ReferenceNumber.Location = New System.Drawing.Point(120, 67)
    Me.Label_ReferenceNumber.Name = "Label_ReferenceNumber"
    Me.Label_ReferenceNumber.Size = New System.Drawing.Size(162, 20)
    Me.Label_ReferenceNumber.TabIndex = 2
    Me.Label_ReferenceNumber.Text = "Reference"
    '
    'frmChangeControlReview
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.CancelButton = Me.btnCancel
    Me.ClientSize = New System.Drawing.Size(747, 650)
    Me.Controls.Add(Me.Label_ReferenceNumber)
    Me.Controls.Add(Me.Label3)
    Me.Controls.Add(Me.GroupBox6)
    Me.Controls.Add(Me.GroupBox5)
    Me.Controls.Add(Me.GroupBox4)
    Me.Controls.Add(Me.GroupBox3)
    Me.Controls.Add(Me.TabControl_Units)
    Me.Controls.Add(Me.GroupBox2)
    Me.Controls.Add(Me.Label10)
    Me.Controls.Add(Me.Text_DetailedDescription)
    Me.Controls.Add(Me.Label9)
    Me.Controls.Add(Me.Label8)
    Me.Controls.Add(Me.Combo_ChangeCategory)
    Me.Controls.Add(Me.Combo_Priority)
    Me.Controls.Add(Me.Label6)
    Me.Controls.Add(Me.Label_RaisedBy)
    Me.Controls.Add(Me.Label4)
    Me.Controls.Add(Me.Label_DateRaised)
    Me.Controls.Add(Me.Label5)
    Me.Controls.Add(Me.Label_Product)
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.RootMenu)
    Me.Controls.Add(Me.btnClose)
    Me.Controls.Add(Me.GroupBox1)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.Panel1)
    Me.Controls.Add(Me.Combo_SelectChangeID)
    Me.Controls.Add(Me.btnSave)
    Me.Controls.Add(Me.editAuditID)
    Me.Controls.Add(Me.Text_ChangeRequest)
    Me.Controls.Add(Me.lblCurrencyCode)
    Me.Controls.Add(Me.btnDelete)
    Me.Controls.Add(Me.btnCancel)
    Me.MinimumSize = New System.Drawing.Size(500, 675)
    Me.Name = "frmChangeControlReview"
    Me.Text = "Add/Edit Change Control"
    Me.Panel1.ResumeLayout(False)
    Me.TabControl_Units.ResumeLayout(False)
    Me.TabPage_Scope.ResumeLayout(False)
    Me.TabPage_Scope.PerformLayout()
    Me.TabPage_Dependencies.ResumeLayout(False)
    Me.TabPage_Dependencies.PerformLayout()
    Me.TabPage_Restrictions.ResumeLayout(False)
    Me.TabPage_Restrictions.PerformLayout()
    Me.TabPage_Implementation.ResumeLayout(False)
    Me.TabPage_Implementation.PerformLayout()
    Me.TabPage_TestPlan.ResumeLayout(False)
    Me.TabPage_TestPlan.PerformLayout()
    Me.GroupBox4.ResumeLayout(False)
    Me.GroupBox4.PerformLayout()
    Me.GroupBox5.ResumeLayout(False)
    Me.GroupBox5.PerformLayout()
    Me.GroupBox6.ResumeLayout(False)
    Me.GroupBox6.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "


  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
  Private WithEvents _MainForm As VeniceMain

  ' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
  Private FormTooltip As New ToolTip()

  ' Form Menu


  ' Form Constants, specific to the table being updated.

    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
  Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

  ' Form Locals, initialised on 'New' defining what standard data items to use
    ''' <summary>
  ''' Variable holding the form's data table name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_TABLENAME As String
    ''' <summary>
  ''' Variable holding the form's data adaptor name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_ADAPTORNAME As String
    ''' <summary>
  ''' Variable holding the form's dataset name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_DATASETNAME As String


  ' The standard ChangeID for this form. e.g. tblCurrency
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

    ''' <summary>
  ''' Principal control used for selecting items on this form. 
    ''' </summary>
  Private THIS_FORM_SelectingCombo As ComboBox
    ''' <summary>
  ''' Control to select after proessing the "New" button.
    ''' </summary>
  Private THIS_FORM_NewMoveToControl As Control

  ' Form Specific Order fields
    ''' <summary>
  ''' Field Name to show in the Select combo.
    ''' </summary>
  Private THIS_FORM_SelectBy As String
    ''' <summary>
  ''' Field Name to order the Select combo by.
    ''' </summary>
  Private THIS_FORM_OrderBy As String

    ''' <summary>
  ''' The field name that holds the unique identifier for each entry. e.g. FundID for the tblFund table.
    ''' </summary>
  Private THIS_FORM_ValueMember As String

  ' Form Specific Select Criteria
    ''' <summary>
    ''' The THI s_ FOR m_ select criteria
    ''' </summary>
  Private THIS_FORM_SelectCriteria As String = "True"

  ' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
  Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
  Private THIS_FORM_FormID As VeniceFormID

  ' Data Structures

    ''' <summary>
    ''' My dataset
    ''' </summary>
	Private myDataset As RenaissanceDataClass.DSChangeControl		 ' Form Specific !!!!
    ''' <summary>
    ''' My table
    ''' </summary>
	Private myTable As RenaissanceDataClass.DSChangeControl.tblChangeControlDataTable
    ''' <summary>
    ''' My connection
    ''' </summary>
  Private myConnection As SqlConnection
    ''' <summary>
    ''' My adaptor
    ''' </summary>
  Private myAdaptor As SqlDataAdapter

    ''' <summary>
    ''' The this standard dataset
    ''' </summary>
  Private ThisStandardDataset As RenaissanceGlobals.StandardDataset


  ' Active Element.

    ''' <summary>
    ''' The sorted rows
    ''' </summary>
  Private SortedRows() As DataRow
    ''' <summary>
    ''' The select by sorted rows
    ''' </summary>
  Private SelectBySortedRows() As DataRow
    ''' <summary>
    ''' The this data row
    ''' </summary>
	Private thisDataRow As RenaissanceDataClass.DSChangeControl.tblChangeControlRow		 ' Form Specific !!!!
    ''' <summary>
    ''' The this audit ID
    ''' </summary>
	Private thisAuditID As Integer
    ''' <summary>
    ''' The this position
    ''' </summary>
	Private thisPosition As Integer
    ''' <summary>
    ''' The _ is over cancel button
    ''' </summary>
	Private _IsOverCancelButton As Boolean
    ''' <summary>
    ''' The _ in use
    ''' </summary>
	Private _InUse As Boolean

	' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
	Private FormIsValid As Boolean
    ''' <summary>
    ''' The form changed
    ''' </summary>
	Private FormChanged As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
	Private _FormOpenFailed As Boolean
    ''' <summary>
    ''' The in paint
    ''' </summary>
	Private InPaint As Boolean
    ''' <summary>
    ''' The add new record
    ''' </summary>
	Private AddNewRecord As Boolean

	' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
	Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
	Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
	Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
	Private HasDeletePermission As Boolean

    ''' <summary>
    ''' The change CTRL IT review permission
    ''' </summary>
	Private ChangeCtrlITReviewPermission As Boolean
    ''' <summary>
    ''' The change CTRL authorise IT permission
    ''' </summary>
	Private ChangeCtrlAuthoriseITPermission As Boolean
    ''' <summary>
    ''' The change CTRL authorise owner permission
    ''' </summary>
	Private ChangeCtrlAuthoriseOwnerPermission As Boolean
    ''' <summary>
    ''' The change CTRL authorise business permission
    ''' </summary>
	Private ChangeCtrlAuthoriseBusinessPermission As Boolean
    ''' <summary>
    ''' The change CTRL accept IT permission
    ''' </summary>
	Private ChangeCtrlAcceptITPermission As Boolean
    ''' <summary>
    ''' The change CTRL accept owner permission
    ''' </summary>
	Private ChangeCtrlAcceptOwnerPermission As Boolean
    ''' <summary>
    ''' The change CTRL accept business permission
    ''' </summary>
	Private ChangeCtrlAcceptBusinessPermission As Boolean

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
	Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
	Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return _IsOverCancelButton
		End Get
		Set(ByVal Value As Boolean)
			_IsOverCancelButton = Value
		End Set
	End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
	Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
		Get
			Return InPaint
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
		Get
			Return _InUse
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

    ''' <summary>
    ''' Gets or sets the form select criteria.
    ''' </summary>
    ''' <value>The form select criteria.</value>
	Public Property FormSelectCriteria() As String
		Get
			Return THIS_FORM_SelectCriteria
		End Get
		Set(ByVal Value As String)
			If (THIS_FORM_SelectCriteria <> Value) Then
				THIS_FORM_SelectCriteria = Value
				If (Value.Equals("True")) Then
					Me.Text = "Add/Edit Change Control"
				Else
					Me.Text = "Add/Edit Change Control (Selected View)"
				End If
				Call Form_Load(Me, New System.EventArgs)
			End If
		End Set
	End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmChangeControlReview"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
	Public Sub New(ByVal pMainForm As VeniceMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()



		_MainForm = pMainForm
		AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

		_FormOpenFailed = False
		_InUse = True

		' ******************************************************
		' Form Specific Settings :
		' ******************************************************

		THIS_FORM_SelectingCombo = Me.Combo_SelectChangeID
		THIS_FORM_NewMoveToControl = Me.Text_ChangeRequest

		' Default Select and Order fields.

		THIS_FORM_SelectBy = "Change_ReferenceNumber"
		THIS_FORM_OrderBy = "Change_ReferenceNumber"

		THIS_FORM_ValueMember = "ChangeID"

		' Form Permissioning :-

		THIS_FORM_PermissionArea = Me.Name
		THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

		' 'This' form ID

		THIS_FORM_FormID = VeniceFormID.frmChangeControlReview

		' This form's dataset type.

		ThisStandardDataset = RenaissanceStandardDatasets.tblChangeControl	' This Defines the Form Data !!! 

		' Format Event Handlers for form controls

		AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

		' Form Control Changed events
		AddHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
		AddHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		AddHandler Combo_ChangeCategory.SelectedIndexChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_Priority.SelectedIndexChanged, AddressOf Me.FormControlChanged
		AddHandler Text_ChangeRequest.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Text_DetailedDescription.TextChanged, AddressOf Me.FormControlChanged
		AddHandler TextBox_Scope.TextChanged, AddressOf Me.FormControlChanged
		AddHandler TextBox_Dependencies.TextChanged, AddressOf Me.FormControlChanged
		AddHandler TextBox_Restrictions.TextChanged, AddressOf Me.FormControlChanged
		AddHandler TextBox_Implementation.TextChanged, AddressOf Me.FormControlChanged
		AddHandler TextBox_TestPlan.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Check_ITReviewed.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Check_Review_Rejected.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Check_AuthorisedIT.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Check_AuthorisedOwner.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Check_AuthorisedBusiness.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Check_AuthorisedRejected.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Check_AcceptedIT.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Check_AcceptedOwner.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Check_AcceptedBusiness.CheckedChanged, AddressOf Me.FormControlChanged

		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

		' ******************************************************
		' End Form Specific.
		' ******************************************************

		' Data object names standard to this Form type.

		THIS_TABLENAME = ThisStandardDataset.TableName
		THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
		THIS_DATASETNAME = ThisStandardDataset.DatasetName

		THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

		' Establish / Retrieve data objects for this form.

		myConnection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)
		myAdaptor = MainForm.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, VENICE_CONNECTION, THIS_TABLENAME)
		myDataset = MainForm.Load_Table(ThisStandardDataset, False)
		myTable = myDataset.Tables(0)

		MainForm.BuildStandardFormMenu(Me.RootMenu, myDataset.Tables(0), AddressOf SelectMenuEvent, AddressOf OrderMenuEvent, AddressOf AuditReportMenuEvent)

	End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

	' Form Initialisation code.
	'
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
	Public Sub ResetForm() Implements StandardVeniceForm.ResetForm
		THIS_FORM_SelectCriteria = "True"
		Me.Text = "Add/Edit Change Control"

		THIS_FORM_SelectBy = "Change_ReferenceNumber"
		THIS_FORM_OrderBy = "Change_ReferenceNumber"

		Call Form_Load(Me, New System.EventArgs)
	End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
	Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub


    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
		_FormOpenFailed = False
		_InUse = True

		' Initialise Data structures. Connection, Adaptor and Dataset.

		If Not (MainForm Is Nothing) Then
			FormIsValid = True
		Else
			MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myConnection Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myAdaptor Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myDataset Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		' Initialse form

		InPaint = True
		IsOverCancelButton = False

		' Check User permissions
		Call CheckPermissions()
		If (HasReadPermission = False) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		' Build Sorted data list from which this form operates
		Call SetSortedRows()

		' Display initial record.

		thisPosition = 0
		If THIS_FORM_SelectingCombo.Items.Count > 0 Then
			Me.THIS_FORM_SelectingCombo.SelectedIndex = 0
			thisDataRow = SortedRows(thisPosition)
			Call GetFormData(thisDataRow)
		Else
			Me.THIS_FORM_SelectingCombo.SelectedIndex = (-1)
			Call GetFormData(Nothing)
		End If

		InPaint = False


	End Sub

    ''' <summary>
    ''' Handles the Closing event of the frmChangeControlReview control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
	Private Sub frmChangeControlReview_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		Dim HideForm As Boolean

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...

		_InUse = False

		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else
			If (FormChanged = True) Then
				Call SetFormData()
			End If

			HideForm = True
			If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			e.Cancel = True
		Else
			Try
				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler
				RemoveHandler Text_ChangeRequest.LostFocus, AddressOf MainForm.Text_NotNull

				RemoveHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
				RemoveHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				RemoveHandler Combo_ChangeCategory.SelectedIndexChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Priority.SelectedIndexChanged, AddressOf Me.FormControlChanged
				RemoveHandler Text_ChangeRequest.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Text_DetailedDescription.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler TextBox_Scope.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler TextBox_Dependencies.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler TextBox_Restrictions.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler TextBox_Implementation.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler TextBox_TestPlan.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_ITReviewed.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_Review_Rejected.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_AuthorisedIT.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_AuthorisedOwner.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_AuthorisedBusiness.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_AuthorisedRejected.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_AcceptedIT.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_AcceptedOwner.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_AcceptedBusiness.CheckedChanged, AddressOf Me.FormControlChanged


			Catch ex As Exception
			End Try
		End If

	End Sub


#End Region

	' Routine to handle changes / updates to tables by this and other windows.
	' If this, or any other, form posts a change to a table, then it will invoke an update event 
	' detailing what tables have been altered.
	' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
	' the 'VeniceAutoUpdate' event of the main Venice form.
	' Each form may them react as appropriate to changes in any table that might impact it.
	'
    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		Dim OrgInPaint As Boolean
		Dim KnowledgeDateChanged As Boolean
		Dim SetButtonStatus_Flag As Boolean
		Dim RefreshForm As Boolean = False

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		OrgInPaint = InPaint
		InPaint = True
		KnowledgeDateChanged = False
		SetButtonStatus_Flag = False

		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
			KnowledgeDateChanged = True
			RefreshForm = True
		End If

		' ****************************************************************
		' Check for changes relevant to this form
		' ****************************************************************

		' Changes to the KnowledgeDate :-
		If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
			SetButtonStatus_Flag = True
		End If

		' Changes to the tblUserPermissions table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

			' Check ongoing permissions.

			Call CheckPermissions()
			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

				FormIsValid = False
				Me.Close()
				Exit Sub
			End If

			SetButtonStatus_Flag = True

		End If


		' ****************************************************************
		' Changes to the Main FORM table :-
		' ****************************************************************

		If (e.TableChanged(THIS_FORM_ChangeID) = True) Or KnowledgeDateChanged Then
			RefreshForm = True

			' Re-Set Controls etc.
			Call SetSortedRows()

			' Move again to the correct item
			thisPosition = Get_Position(thisAuditID)

			' Validate current position.
			If thisPosition >= 0 And thisPosition < Me.SortedRows.GetLength(0) Then
				thisDataRow = Me.SortedRows(thisPosition)
			Else
				If (Me.SortedRows.GetLength(0) > 0) And (AddNewRecord = False) Then
					thisPosition = 0
					thisDataRow = Me.SortedRows(thisPosition)
					FormChanged = False
				Else
					thisDataRow = Nothing
				End If
			End If

			' Set SelectingCombo Index.
			If (Me.THIS_FORM_SelectingCombo.Items.Count <= 0) Then
				Try
					Me.THIS_FORM_SelectingCombo.SelectedIndex = -1
				Catch ex As Exception
				End Try
			ElseIf (thisPosition < 0) Then
				Try
					MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
				Catch ex As Exception
				End Try
			Else
				Try
					Me.THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
				Catch ex As Exception
				End Try
			End If

		End If

		' ****************************************************************
		' Repaint if not currently in Edit Mode
		'
		' ****************************************************************

		InPaint = OrgInPaint

		' Retrieve form data if appropriate, i.e. this data item is not already being edited.
		If (RefreshForm = True) AndAlso (FormChanged = False) AndAlso (AddNewRecord = False) Then
			GetFormData(thisDataRow) ' Includes a call to 'SetButtonStatus()'
		Else
			If SetButtonStatus_Flag Then
				Call SetButtonStatus()
			End If
		End If

	End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

	' Build Sorted list from the Source dataset and update the Select Combo
    ''' <summary>
    ''' Sets the sorted rows.
    ''' </summary>
	Private Sub SetSortedRows()

		Dim OrgInPaint As Boolean

		Dim thisrow As DataRow
		Dim thisDrowDownWidth As Integer
		Dim SizingBitmap As Bitmap
		Dim SizingGraphics As Graphics

		' Form Specific Selection Combo :-
		If (THIS_FORM_SelectingCombo Is Nothing) Then Exit Sub

		' Code is pretty Generic form here on...

		' Set paint local so that changes to the selection combo do not trigger form updates.

		OrgInPaint = InPaint
		InPaint = True

		' Get selected Row sets, or exit if no data is present.
		If myDataset Is Nothing Then
			ReDim SortedRows(0)
			ReDim SelectBySortedRows(0)
		Else
			Try
				SortedRows = myTable.Select(THIS_FORM_SelectCriteria, THIS_FORM_OrderBy)
				SelectBySortedRows = myTable.Select(THIS_FORM_SelectCriteria, THIS_FORM_SelectBy)
			Catch ex As Exception
				' In the event of an error, try to select all rows.
				Me.MainForm.LogError(Me.Name & ",SetSortedRows()", LOG_LEVELS.Error, ex.Message, "Error Selecting Data Rows.", ex.StackTrace, True)

				THIS_FORM_OrderBy = "ChangeID"
				THIS_FORM_SelectBy = "True"

				Try
					SortedRows = myTable.Select(THIS_FORM_SelectCriteria, THIS_FORM_OrderBy)
					SelectBySortedRows = myTable.Select(THIS_FORM_SelectCriteria, THIS_FORM_SelectBy)
				Catch ex_1 As Exception
					ReDim SortedRows(0)
					ReDim SelectBySortedRows(0)
				End Try
			End Try
		End If

		' Set Combo data source
		THIS_FORM_SelectingCombo.DataSource = SortedRows
		THIS_FORM_SelectingCombo.DisplayMember = THIS_FORM_SelectBy
		THIS_FORM_SelectingCombo.ValueMember = THIS_FORM_ValueMember

		' Ensure that the Selection Combo's DrowDownWidth is wide enough to take the required strings

		thisDrowDownWidth = THIS_FORM_SelectingCombo.Width
		For Each thisrow In SortedRows

			' Compute the string dimensions in the given font
			SizingBitmap = New Bitmap(1, 1, System.Drawing.Imaging.PixelFormat.Format32bppArgb)
			SizingGraphics = Graphics.FromImage(SizingBitmap)
			Dim stringSize As SizeF = SizingGraphics.MeasureString(thisrow(THIS_FORM_SelectBy).ToString, THIS_FORM_SelectingCombo.Font)
			If (stringSize.Width) > thisDrowDownWidth Then
				thisDrowDownWidth = CInt(stringSize.Width)
			End If
		Next

		THIS_FORM_SelectingCombo.DropDownWidth = thisDrowDownWidth

		InPaint = OrgInPaint
	End Sub


	' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
	Private Sub CheckPermissions()

		Dim Permissions As Integer

		Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

		HasReadPermission = ((Permissions And PermissionBitmap.PermRead) > 0)
		HasUpdatePermission = ((Permissions And PermissionBitmap.PermUpdate) > 0)
		HasInsertPermission = ((Permissions And PermissionBitmap.PermInsert) > 0)
		HasDeletePermission = ((Permissions And PermissionBitmap.PermDelete) > 0)

		ChangeCtrlITReviewPermission = CBool(MainForm.CheckPermissions("ChangeCtrlITReview", THIS_FORM_PermissionType) And (PermissionBitmap.PermUpdate Or PermissionBitmap.PermInsert))
		ChangeCtrlAuthoriseITPermission = CBool(MainForm.CheckPermissions("ChangeCtrlAuthoriseIT", THIS_FORM_PermissionType) And (PermissionBitmap.PermUpdate Or PermissionBitmap.PermInsert))
		ChangeCtrlAuthoriseOwnerPermission = CBool(MainForm.CheckPermissions("ChangeCtrlAuthoriseOwner", THIS_FORM_PermissionType) And (PermissionBitmap.PermUpdate Or PermissionBitmap.PermInsert))
		ChangeCtrlAuthoriseBusinessPermission = CBool(MainForm.CheckPermissions("ChangeCtrlAuthoriseBusiness", THIS_FORM_PermissionType) And (PermissionBitmap.PermUpdate Or PermissionBitmap.PermInsert))
		ChangeCtrlAcceptITPermission = CBool(MainForm.CheckPermissions("ChangeCtrlAcceptIT", THIS_FORM_PermissionType) And (PermissionBitmap.PermUpdate Or PermissionBitmap.PermInsert))
		ChangeCtrlAcceptOwnerPermission = CBool(MainForm.CheckPermissions("ChangeCtrlAcceptOwner", THIS_FORM_PermissionType) And (PermissionBitmap.PermUpdate Or PermissionBitmap.PermInsert))
		ChangeCtrlAcceptBusinessPermission = CBool(MainForm.CheckPermissions("ChangeCtrlAcceptBusiness", THIS_FORM_PermissionType) And (PermissionBitmap.PermUpdate Or PermissionBitmap.PermInsert))


	End Sub

	' Flag changes to form controls, Event Associations made in 'New' routine.
    ''' <summary>
  ''' Basic event handler, called when a form value is changed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

		If InPaint = False Then

			SetButtonStatus()

			' If (Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord) Then
			FormChanged = True
			Me.btnSave.Enabled = True
			Me.btnCancel.Enabled = True
			' End If

		End If

	End Sub

    ''' <summary>
    ''' Selects the menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Public Sub SelectMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Select Menu
		' Event association is made during the dynamic menu creation

		THIS_FORM_SelectBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

		Call SetSortedRows()

	End Sub

    ''' <summary>
    ''' Orders the menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Public Sub OrderMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Order Menu
		' Event association is made during the dynamic menu creation

		THIS_FORM_OrderBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

		Call SetSortedRows()

	End Sub


    ''' <summary>
    ''' Audits the report menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Public Sub AuditReportMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Audit Report Menu
		' Event association is made during the dynamic menu creation

		Try
			Select Case CInt(CType(sender, System.Windows.Forms.ToolStripMenuItem).Tag)
				Case 0 ' This Record
					If (Me.AddNewRecord = False) AndAlso (thisAuditID >= 0) Then
						Run_AuditReport_ThisRecord(MainForm, ThisStandardDataset, thisAuditID, MainForm.VeniceProgressBar)
					End If

				Case 1 ' All Records
					Run_AuditReport_ALLRecords(MainForm, ThisStandardDataset, MainForm.VeniceProgressBar)

			End Select
		Catch ex As Exception
			Call MainForm.LogError(Me.Name & ", AuditReportMenuEvent()", 0, ex.Message, "Error calling Audit report.", ex.StackTrace, True)
		End Try

	End Sub




#End Region

#Region " Set Form Combos (Form Specific Code) "

#End Region

#Region " Get & Set Form Data / SetButton / ValidateForm / btnCancel Events (Form Specific Code) "

    ''' <summary>
    ''' Gets the form data.
    ''' </summary>
    ''' <param name="ThisRow">The this row.</param>
	Private Sub GetFormData(ByRef ThisRow As RenaissanceDataClass.DSChangeControl.tblChangeControlRow)
		' Routine to populate form Controls given a data row.
		' Form is cleared for invalid Datarows or Invalid Form.

		Dim OrgInpaint As Boolean

		' Set 'Paint' flag to prevent event actions caused by changing the value of form fields.
		OrgInpaint = InPaint
		InPaint = True


		If (ThisRow Is Nothing) Or (FormIsValid = False) Or (Me.InUse = False) Then
			' Bad / New Datarow - Clear Form.

			thisAuditID = (-1)

			Me.editAuditID.Text = ""
			Me.Label_ReferenceNumber.Text = ""

			MainForm.ClearComboSelection(Me.Combo_SelectChangeID)
			If (Combo_ChangeCategory.Items.Count > 0) Then
				Combo_ChangeCategory.SelectedIndex = 0
			End If
			If (Combo_Priority.Items.Count > 0) Then
				Combo_Priority.SelectedIndex = 0
			End If

			Me.Label_DateRaised.Text = Now.ToString(DISPLAYMEMBER_DATEFORMAT)
			Me.Label_Product.Text = Application.ProductName
			Me.Label_RaisedBy.Text = GetUserName()

			Text_ChangeRequest.Text = ""
			Text_DetailedDescription.Text = ""
			TextBox_Scope.Text = ""
			TextBox_Dependencies.Text = ""
			TextBox_Restrictions.Text = ""
			TextBox_Implementation.Text = ""
			TextBox_TestPlan.Text = ""

			TabControl_Units.SelectedIndex = 0

			Check_ITReviewed.Checked = False
			Check_Review_Rejected.Checked = False
			Check_AuthorisedIT.Checked = False
			Check_AuthorisedOwner.Checked = False
			Check_AuthorisedBusiness.Checked = False
			Check_AuthorisedRejected.Checked = False
			Check_AcceptedIT.Checked = False
			Check_AcceptedOwner.Checked = False
			Check_AcceptedBusiness.Checked = False

			If AddNewRecord = True Then
				Me.btnCancel.Enabled = True
				Me.THIS_FORM_SelectingCombo.Enabled = False
			Else
				Me.btnCancel.Enabled = False
				Me.THIS_FORM_SelectingCombo.Enabled = True
			End If

		Else

			' Populate Form with given data.
			Try
				thisAuditID = thisDataRow.AuditID

				Me.editAuditID.Text = thisDataRow.AuditID.ToString

				Label_ReferenceNumber.Text = thisDataRow.Change_ReferenceNumber
				Combo_ChangeCategory.SelectedValue = thisDataRow.Change_Category
				Combo_Priority.SelectedIndex = (thisDataRow.Change_Priority - 1)

				Label_DateRaised.Text = thisDataRow.Date_Raised.ToString(DISPLAYMEMBER_DATEFORMAT)
				Label_Product.Text = thisDataRow.Product
				Label_RaisedBy.Text = thisDataRow.User_Raised

				Text_ChangeRequest.Text = thisDataRow.Text_Change
				Text_DetailedDescription.Text = thisDataRow.Text_Detail
				TextBox_Scope.Text = thisDataRow.Text_ScopeOfChange
				TextBox_Dependencies.Text = thisDataRow.Text_Dependencies
				TextBox_Restrictions.Text = thisDataRow.Text_Restrictions
				TextBox_Implementation.Text = thisDataRow.Text_ImplementationPlan
				TextBox_TestPlan.Text = thisDataRow.Text_TestPlan

				TabControl_Units.SelectedIndex = 0

				Check_ITReviewed.Checked = thisDataRow.Flag_ReviewedIT
				Check_Review_Rejected.Checked = thisDataRow.Flag_ReviewRejected
				Check_AuthorisedIT.Checked = thisDataRow.Flag_AuthorisedIT
				Check_AuthorisedOwner.Checked = thisDataRow.Flag_AuthorisedOwner
				Check_AuthorisedBusiness.Checked = thisDataRow.Flag_AuthorisedBusiness
				Check_AuthorisedRejected.Checked = thisDataRow.Flag_AuthoriseRejected
				Check_AcceptedIT.Checked = thisDataRow.Flag_AcceptedIT
				Check_AcceptedOwner.Checked = thisDataRow.Flag_AcceptedOwner
				Check_AcceptedBusiness.Checked = thisDataRow.Flag_AcceptedBusiness

				AddNewRecord = False
				' MainForm.SetComboSelectionLengths(Me)

				Me.btnCancel.Enabled = False
				Me.THIS_FORM_SelectingCombo.Enabled = True

			Catch ex As Exception

				Call MainForm.LogError(Me.Name & ", GetFormData", 0, ex.Message, "Error Showing Data", ex.StackTrace, True)
				Call GetFormData(Nothing)

			End Try

		End If

		' Allow Field events to trigger before 'InPaint' Is re-set. 
		' (Should) Prevent Validation errors during Form Draw.
		Application.DoEvents()

		' Restore 'Paint' flag.
		InPaint = OrgInpaint
		FormChanged = False
		Me.btnSave.Enabled = False

		' As it says on the can....
		Call SetButtonStatus()

	End Sub

    ''' <summary>
    ''' Sets the form data.
    ''' </summary>
    ''' <param name="pConfirm">if set to <c>true</c> [p confirm].</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	Private Function SetFormData(Optional ByVal pConfirm As Boolean = True) As Boolean
		' *************************************************************
		'
		' *************************************************************
		Dim ErrMessage As String
		Dim ErrFlag As Boolean
		Dim ErrStack As String
		Dim ProtectedItem As Boolean = False

		ErrMessage = ""
		ErrStack = ""

		' *************************************************************
		' Appropriate Save permission :-
		' *************************************************************

		If (Me.HasUpdatePermission = False) And ((Me.HasInsertPermission = False) Or (Me.AddNewRecord = False)) Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "You do not have permission to save this record.", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' *************************************************************
		' If Save button is disabled then should not be able to save, exit silently.
		' *************************************************************
		If Me.btnSave.Enabled = False Then
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' *************************************************************
		' KnowledgeDate OK :-
		' *************************************************************

		If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Changes to Venice are not allowed when the Knowledgedate is not set to `NOW`", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' Confirm Save, if required.

		If (pConfirm = True) Then
			If MessageBox.Show("Save Changes ?", "Save Changes ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
				Return True
				Exit Function
			End If
		End If

		' *************************************************************
		' Procedure to Save the current form information.
		' *************************************************************

		Dim StatusString As String
		Dim LogString As String
		Dim UpdateRows(0) As DataRow
		Dim Position As Integer

		If (FormChanged = False) Or (FormIsValid = False) Then
			Return False
			Exit Function
		End If

		' Validation
		StatusString = ""
		If ValidateForm(StatusString) = False Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Validation Error." & vbCrLf & StatusString, "", True)
			Return False
			Exit Function
		End If

		' Check current position in the table.
		Position = Get_Position(thisAuditID)

		' Allow for new or missing ID.
		If Position < 0 Then
			If (AddNewRecord = True) OrElse (SortedRows.Length <= 0) Then
				thisDataRow = myTable.NewRow
				LogString = "Add: "
				AddNewRecord = True
			Else
				Return False
				Exit Function
			End If
		Else
			' Row found OK.
			LogString = "Edit : AuditID = " & thisAuditID.ToString

			thisDataRow = Me.SortedRows(Position)

		End If

		' Set 'Paint' flag.
		InPaint = True

		' *************************************************************
		' Lock the Data Table, to prevent update conflicts.
		' *************************************************************
		Try
			SyncLock myTable

				' Initiate Edit,
				thisDataRow.BeginEdit()

				If thisDataRow.IsChange_ReferenceNumberNull Then
					thisDataRow.Change_ReferenceNumber = ""
				End If
				thisDataRow.Product = Me.Label_Product.Text

				If AddNewRecord = True Then
					thisDataRow.Date_Raised = Now()
					thisDataRow.User_Raised = GetUserName()
				End If

				thisDataRow.Change_Category = Me.Combo_ChangeCategory.Text
				thisDataRow.Change_Priority = Me.Combo_Priority.SelectedIndex + 1
				thisDataRow.Text_Change = Me.Text_ChangeRequest.Text
				thisDataRow.Text_Detail = Me.Text_DetailedDescription.Text
				thisDataRow.Text_ScopeOfChange = Me.TextBox_Scope.Text
				thisDataRow.Text_Dependencies = Me.TextBox_Dependencies.Text
				thisDataRow.Text_Restrictions = Me.TextBox_Restrictions.Text
				thisDataRow.Text_ImplementationPlan = Me.TextBox_Implementation.Text
				thisDataRow.Text_TestPlan = Me.TextBox_TestPlan.Text

				If AddNewRecord = True Then
					thisDataRow.Flag_ReviewedIT = False
					thisDataRow.User_ReviewedIT = ""
					thisDataRow.SetDate_ReviewedITNull()

					thisDataRow.Flag_ReviewRejected = False
					thisDataRow.User_ReviewRejected = ""
					thisDataRow.SetDate_ReviewRejectedNull()

					thisDataRow.Flag_AuthorisedIT = False
					thisDataRow.User_AuthorisedIT = ""
					thisDataRow.SetDate_AuthorisedITNull()

					thisDataRow.Flag_AuthorisedOwner = False
					thisDataRow.User_AuthorisedOwner = ""
					thisDataRow.SetDate_AuthorisedOwnerNull()

					thisDataRow.Flag_AuthorisedBusiness = False
					thisDataRow.User_AuthorisedBusiness = ""
					thisDataRow.SetDate_AuthorisedBusinessNull()

					thisDataRow.Flag_AuthoriseRejected = False
					thisDataRow.User_AuthoriseRejected = ""
					thisDataRow.SetDate_AuthoriseRejectedNull()

					thisDataRow.Flag_AcceptedIT = False
					thisDataRow.User_AcceptedIT = ""
					thisDataRow.SetDate_AcceptedITNull()

					thisDataRow.Flag_AcceptedOwner = False
					thisDataRow.User_AcceptedOwner = ""
					thisDataRow.SetDate_AcceptedOwnerNull()

					thisDataRow.Flag_AcceptedBusiness = False
					thisDataRow.User_AcceptedBusiness = ""
					thisDataRow.SetDate_AcceptedBusinessNull()
				End If

				' Reviewed

				If (ChangeCtrlITReviewPermission) AndAlso (thisDataRow.Flag_ReviewedIT <> Check_ITReviewed.Checked) Then
					thisDataRow.Flag_ReviewedIT = Check_ITReviewed.Checked

					If (Check_ITReviewed.Checked) Then
						thisDataRow.User_ReviewedIT = GetUserName()
						thisDataRow.Date_ReviewedIT = Now()
					Else
						thisDataRow.User_ReviewedIT = ""
						thisDataRow.SetDate_ReviewedITNull()
					End If
				End If

				If (ChangeCtrlITReviewPermission) AndAlso (thisDataRow.Flag_ReviewRejected <> Check_Review_Rejected.Checked) Then
					thisDataRow.Flag_ReviewRejected = Check_Review_Rejected.Checked

					If (Check_Review_Rejected.Checked) Then
						thisDataRow.User_ReviewRejected = GetUserName()
						thisDataRow.Date_ReviewRejected = Now()
					Else
						thisDataRow.User_ReviewRejected = ""
						thisDataRow.SetDate_ReviewRejectedNull()
					End If
				End If

				' Authorised

				If (thisDataRow.Flag_AuthoriseRejected <> Check_AuthorisedRejected.Checked) Then
					thisDataRow.Flag_AuthoriseRejected = Check_AuthorisedRejected.Checked

					If (Check_AuthorisedRejected.Checked) Then
						thisDataRow.User_AuthoriseRejected = GetUserName()
						thisDataRow.Date_AuthoriseRejected = Now()
					Else
						thisDataRow.User_AuthoriseRejected = ""
						thisDataRow.SetDate_AuthoriseRejectedNull()
					End If
				End If

				If (thisDataRow.Flag_ReviewedIT = True) AndAlso (thisDataRow.Flag_ReviewRejected = False) AndAlso (thisDataRow.Flag_AuthoriseRejected = False) Then
					If (ChangeCtrlAuthoriseITPermission) AndAlso (thisDataRow.Flag_AuthorisedIT <> Check_AuthorisedIT.Checked) Then
						thisDataRow.Flag_AuthorisedIT = Check_AuthorisedIT.Checked

						If (Check_AuthorisedIT.Checked) Then
							thisDataRow.User_AuthorisedIT = GetUserName()
							thisDataRow.Date_AuthorisedIT = Now()
						Else
							thisDataRow.User_AuthorisedIT = ""
							thisDataRow.SetDate_AuthorisedITNull()
						End If
					End If

					If (ChangeCtrlAuthoriseOwnerPermission) AndAlso (thisDataRow.Flag_AuthorisedOwner <> Check_AuthorisedOwner.Checked) Then
						thisDataRow.Flag_AuthorisedOwner = Check_AuthorisedOwner.Checked

						If (Check_AuthorisedOwner.Checked) Then
							thisDataRow.User_AuthorisedOwner = GetUserName()
							thisDataRow.Date_AuthorisedOwner = Now()
						Else
							thisDataRow.User_AuthorisedOwner = ""
							thisDataRow.SetDate_AuthorisedOwnerNull()
						End If
					End If

					If (ChangeCtrlAuthoriseBusinessPermission) AndAlso (thisDataRow.Flag_AuthorisedBusiness <> Check_AuthorisedBusiness.Checked) Then
						thisDataRow.Flag_AuthorisedBusiness = Check_AuthorisedBusiness.Checked

						If (Check_AuthorisedBusiness.Checked) Then
							thisDataRow.User_AuthorisedBusiness = GetUserName()
							thisDataRow.Date_AuthorisedBusiness = Now()
						Else
							thisDataRow.User_AuthorisedBusiness = ""
							thisDataRow.SetDate_AuthorisedBusinessNull()
						End If
					End If
				Else
					thisDataRow.Flag_AuthorisedIT = False
					thisDataRow.User_AuthorisedIT = ""
					thisDataRow.SetDate_AuthorisedITNull()

					thisDataRow.Flag_AuthorisedOwner = False
					thisDataRow.User_AuthorisedOwner = ""
					thisDataRow.SetDate_AuthorisedOwnerNull()

					thisDataRow.Flag_AuthorisedBusiness = False
					thisDataRow.User_AuthorisedBusiness = ""
					thisDataRow.SetDate_AuthorisedBusinessNull()

					thisDataRow.Flag_AcceptedIT = False
					thisDataRow.User_AcceptedIT = ""
					thisDataRow.SetDate_AcceptedITNull()

					thisDataRow.Flag_AcceptedOwner = False
					thisDataRow.User_AcceptedOwner = ""
					thisDataRow.SetDate_AcceptedOwnerNull()

					thisDataRow.Flag_AcceptedBusiness = False
					thisDataRow.User_AcceptedBusiness = ""
					thisDataRow.SetDate_AcceptedBusinessNull()
				End If

				' Accepted
				If (thisDataRow.Flag_AuthorisedIT = True) AndAlso (thisDataRow.Flag_AuthorisedIT = True) AndAlso (thisDataRow.Flag_AuthorisedBusiness = True) AndAlso (thisDataRow.Flag_AuthorisedOwner = True) AndAlso (thisDataRow.Flag_ReviewRejected = False) AndAlso (thisDataRow.Flag_AuthoriseRejected = False) Then
					If (ChangeCtrlAcceptITPermission) AndAlso (thisDataRow.Flag_AcceptedIT <> Check_AcceptedIT.Checked) Then
						thisDataRow.Flag_AcceptedIT = Check_AcceptedIT.Checked

						If (Check_AcceptedIT.Checked) Then
							thisDataRow.User_AcceptedIT = GetUserName()
							thisDataRow.Date_AcceptedIT = Now()
						Else
							thisDataRow.User_AcceptedIT = ""
							thisDataRow.SetDate_AcceptedITNull()
						End If
					End If

					If (ChangeCtrlAcceptOwnerPermission) AndAlso (thisDataRow.Flag_AcceptedOwner <> Check_AcceptedOwner.Checked) Then
						thisDataRow.Flag_AcceptedOwner = Check_AcceptedOwner.Checked

						If (Check_AcceptedOwner.Checked) Then
							thisDataRow.User_AcceptedOwner = GetUserName()
							thisDataRow.Date_AcceptedOwner = Now()
						Else
							thisDataRow.User_AcceptedOwner = ""
							thisDataRow.SetDate_AcceptedOwnerNull()
						End If

					End If

					If (ChangeCtrlAcceptBusinessPermission) AndAlso (thisDataRow.Flag_AcceptedBusiness <> Check_AcceptedBusiness.Checked) Then
						thisDataRow.Flag_AcceptedBusiness = Check_AcceptedBusiness.Checked

						If (Check_AcceptedBusiness.Checked) Then
							thisDataRow.User_AcceptedBusiness = GetUserName()
							thisDataRow.Date_AcceptedBusiness = Now()
						Else
							thisDataRow.User_AcceptedBusiness = ""
							thisDataRow.SetDate_AcceptedBusinessNull()
						End If
					End If
				Else
					thisDataRow.Flag_AcceptedIT = False
					thisDataRow.User_AcceptedIT = ""
					thisDataRow.SetDate_AcceptedITNull()

					thisDataRow.Flag_AcceptedOwner = False
					thisDataRow.User_AcceptedOwner = ""
					thisDataRow.SetDate_AcceptedOwnerNull()

					thisDataRow.Flag_AcceptedBusiness = False
					thisDataRow.User_AcceptedBusiness = ""
					thisDataRow.SetDate_AcceptedBusinessNull()
				End If

				thisDataRow.EndEdit()
				InPaint = False

				' Add and Update DataRow. 

				ErrFlag = False

				If AddNewRecord = True Then
					Try
						myTable.Rows.Add(thisDataRow)
					Catch ex As Exception
						ErrFlag = True
						ErrMessage = ex.Message
						ErrStack = ex.StackTrace
					End Try
				End If

				UpdateRows(0) = thisDataRow

				' Post Additions / Updates to the underlying table :-
				Dim temp As Integer
				Try
					If (ErrFlag = False) Then
						myAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
						myAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

						temp = MainForm.AdaptorUpdate(Me.Name, myAdaptor, UpdateRows)
					End If

				Catch ex As Exception

					ErrMessage = ex.Message
					ErrFlag = True
					ErrStack = ex.StackTrace

				End Try

				thisAuditID = thisDataRow.AuditID

				' De-Restrict this Form Selection if a New Transaction Has been added.
				THIS_FORM_SelectCriteria = "True"

			End SyncLock
		Catch ex As Exception
			Call MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Error, ex.Message, "Error in SetFormData().", ex.StackTrace, True)
		End Try

		' *************************************************************
		' Error report. Not done in the Try-Catch as that would serve to persist the SyncLock.
		' *************************************************************

		If (ErrFlag = True) Then
			Call MainForm.LogError(Me.Name & ", SetFormData", 0, ErrMessage, "Error Saving Data", ErrStack, True)
		End If

		' Finish off

		AddNewRecord = False
		FormChanged = False

		Me.THIS_FORM_SelectingCombo.Enabled = True

		' Propogate changes

		Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID), True)

	End Function

    ''' <summary>
    ''' Sets the button status.
    ''' </summary>
	Private Sub SetButtonStatus()
		' Sets the status of the form controlls appropriate to the current users 
		'permissions and the 'Changed' or 'New' status of the form.

		' No Read permission :-

		If Me.HasReadPermission = False Then
			MainForm.LogError(Me.Name & ", SetButtonStatus()", 0, "", "You do not have Read permission for this Form", "", True)
			Me.Close()
			Exit Sub
		End If

		' Has Insert Permission.
		If Me.HasInsertPermission Then
			Me.btnAdd.Enabled = True
		Else
			Me.btnAdd.Enabled = False
		End If

		' Has Delete permission. 
		If (Me.HasDeletePermission) And (Me.AddNewRecord = False) And (thisPosition >= 0) Then
			Me.btnDelete.Enabled = True
		Else
			Me.btnDelete.Enabled = False
		End If

		If (Me.MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And _
			((Me.HasUpdatePermission) Or (Me.Label_RaisedBy.Text.ToUpper = GetUserName.ToUpper) Or (Me.HasInsertPermission And Me.AddNewRecord)) Then

			If (Me.Check_AcceptedIT.Checked Or Me.Check_AcceptedOwner.Checked Or Me.Check_AuthorisedBusiness.Checked) Then
				Me.Combo_ChangeCategory.Enabled = False
				Me.Combo_Priority.Enabled = False
				Me.Text_ChangeRequest.Enabled = False
				Me.Text_DetailedDescription.Enabled = False
			Else
				Me.Combo_ChangeCategory.Enabled = True
				Me.Combo_Priority.Enabled = True
				Me.Text_ChangeRequest.Enabled = True
				Me.Text_DetailedDescription.Enabled = True
			End If

		Else

			Me.Combo_ChangeCategory.Enabled = False
			Me.Combo_Priority.Enabled = False
			Me.Text_ChangeRequest.Enabled = False
			Me.Text_DetailedDescription.Enabled = False

		End If

		If (Me.MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And (Me.ChangeCtrlITReviewPermission) Then

			If (Me.Check_AcceptedIT.Checked Or Me.Check_AcceptedOwner.Checked Or Me.Check_AuthorisedBusiness.Checked) Then
				Me.Text_ChangeRequest.Enabled = False
				Me.Text_DetailedDescription.Enabled = False
			Else
				Me.Text_ChangeRequest.Enabled = True
				Me.Text_DetailedDescription.Enabled = True
			End If

			Me.TextBox_Scope.Enabled = True
			Me.TextBox_Dependencies.Enabled = True
			Me.TextBox_Restrictions.Enabled = True
			Me.TextBox_Implementation.Enabled = True
			Me.TextBox_TestPlan.Enabled = True

			If (Me.Check_AuthorisedIT.Checked Or Me.Check_AuthorisedOwner.Checked Or Me.Check_AuthorisedBusiness.Checked) Then
				Me.Check_ITReviewed.Enabled = False
				Me.Check_Review_Rejected.Enabled = False
			Else
				Me.Check_ITReviewed.Enabled = True
				Me.Check_Review_Rejected.Enabled = True
			End If
		Else
			Me.TextBox_Scope.Enabled = False
			Me.TextBox_Dependencies.Enabled = False
			Me.TextBox_Restrictions.Enabled = False
			Me.TextBox_Implementation.Enabled = False
			Me.TextBox_TestPlan.Enabled = False

			Me.Check_ITReviewed.Enabled = False
			Me.Check_Review_Rejected.Enabled = False
		End If

		Me.Check_AuthorisedRejected.Enabled = False

		If (Me.MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And (Me.ChangeCtrlAuthoriseITPermission) And (Check_Review_Rejected.Checked = False) And (Check_ITReviewed.Checked = True) Then
			If (Me.Check_AcceptedIT.Checked Or Me.Check_AcceptedOwner.Checked Or Me.Check_AuthorisedBusiness.Checked) Then
				Me.Check_AuthorisedIT.Enabled = False
				Me.Check_AuthorisedRejected.Enabled = False
			Else
				Me.Check_AuthorisedIT.Enabled = True
				Me.Check_AuthorisedRejected.Enabled = True
			End If
		Else
			Me.Check_AuthorisedIT.Enabled = False
		End If

		If (Me.MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And (Me.ChangeCtrlAuthoriseOwnerPermission) And (Check_Review_Rejected.Checked = False) And (Check_ITReviewed.Checked = True) Then
			If (Me.Check_AcceptedIT.Checked Or Me.Check_AcceptedOwner.Checked Or Me.Check_AuthorisedBusiness.Checked) Then
				Me.Check_AuthorisedOwner.Enabled = False
				Me.Check_AuthorisedRejected.Enabled = False
			Else
				Me.Check_AuthorisedOwner.Enabled = True
				Me.Check_AuthorisedRejected.Enabled = True
			End If
		Else
			Me.Check_AuthorisedOwner.Enabled = False
		End If

		If (Me.MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And (Me.ChangeCtrlAuthoriseBusinessPermission) And (Check_Review_Rejected.Checked = False) And (Check_ITReviewed.Checked = True) Then
			If (Me.Check_AcceptedIT.Checked Or Me.Check_AcceptedOwner.Checked Or Me.Check_AuthorisedBusiness.Checked) Then
				Me.Check_AuthorisedBusiness.Enabled = False
				Me.Check_AuthorisedRejected.Enabled = False
			Else
				Me.Check_AuthorisedBusiness.Enabled = True
				Me.Check_AuthorisedRejected.Enabled = True
			End If
		Else
			Me.Check_AuthorisedBusiness.Enabled = False
		End If

		If (Check_Review_Rejected.Checked = False) And (Check_AuthorisedIT.Checked) And (Check_AuthorisedOwner.Checked) And (Check_AuthorisedBusiness.Checked) Then
			If (Me.MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And (Me.ChangeCtrlAcceptITPermission) Then
				Me.Check_AcceptedIT.Enabled = True
			Else
				Me.Check_AcceptedIT.Enabled = False
			End If

			If (Me.MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And (Me.ChangeCtrlAcceptOwnerPermission) Then
				Me.Check_AcceptedOwner.Enabled = True
			Else
				Me.Check_AcceptedOwner.Enabled = False
			End If

			If (Me.MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And (Me.ChangeCtrlAcceptBusinessPermission) Then
				Me.Check_AcceptedBusiness.Enabled = True
			Else
				Me.Check_AcceptedBusiness.Enabled = False
			End If
		Else
			Me.Check_AcceptedIT.Enabled = False
			Me.Check_AcceptedOwner.Enabled = False
			Me.Check_AcceptedBusiness.Enabled = False
		End If

	End Sub

    ''' <summary>
    ''' Validates the form.
    ''' </summary>
    ''' <param name="pReturnString">The p return string.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	Private Function ValidateForm(ByRef pReturnString As String) As Boolean
		' Form Validation code.
		' 
		' This code should be the final arbiter of what is allowed. no assumptions regarding 
		' prior validation should be made.
		' 
		' This Code is called by the SetFormData routine before position changes.
		'
		Dim RVal As Boolean

		RVal = True
		pReturnString = ""

		' Validate 

		If Me.Text_ChangeRequest.Text.Length <= 0 Then
			pReturnString = "Change Request must not be left blank."
			RVal = False
		End If

		Return RVal

	End Function

    ''' <summary>
    ''' Handles the MouseEnter event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnCancel_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseEnter
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = True
	End Sub

    ''' <summary>
    ''' Handles the MouseLeave event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnCancel_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseLeave
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = False
	End Sub


#End Region

#Region " Navigation Code / GetPosition() (Generic Code) "

    ''' <summary>
    ''' Moves to audit ID.
    ''' </summary>
    ''' <param name="pAuditID">The p audit ID.</param>
  Public Sub MoveToAuditID(ByVal pAuditID As Integer)
    ' ****************************************************************
    ' Subroutine to move the current form focus to a given AuditID
    ' ****************************************************************

    Dim findDataRow As DataRow
    Dim Counter As Integer

    Try
      If (SortedRows Is Nothing) Then Exit Sub
      If (SortedRows.Length <= 0) Then Exit Sub

      For Counter = 0 To (SortedRows.Length - 1)
        findDataRow = SortedRows(Counter)
        If findDataRow("AuditID").Equals(pAuditID) Then
          THIS_FORM_SelectingCombo.SelectedIndex = Counter
        End If
      Next
    Catch ex As Exception
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Moving to given AuditID.", ex.StackTrace, True)
    End Try

  End Sub

    ''' <summary>
    ''' Handles the SelectComboChanged event of the Combo control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_SelectComboChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' Selection Combo. SelectedItem changed.
    '

    ' Don't react to changes made in paint routines etc.
    If InPaint = True Then Exit Sub

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    ' Find the correct data row, then show it...
    thisPosition = THIS_FORM_SelectingCombo.SelectedIndex

    If (thisPosition >= 0) Then
      thisDataRow = Me.SortedRows(thisPosition)
    Else
      thisDataRow = Nothing
    End If

    Call GetFormData(thisDataRow)

  End Sub


    ''' <summary>
    ''' Handles the Click event of the btnNavPrev control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnNavPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavPrev.Click
    ' 'Previous' Button.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    If thisPosition > 0 Then thisPosition -= 1
    If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
      thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
    End If

    THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

    Exit Sub

    If myTable.Rows.Count > thisPosition Then
      thisDataRow = myTable.Rows(thisPosition)
      Call GetFormData(thisDataRow)
    Else
      Call GetFormData(Nothing)
    End If

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnNavNext control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnNavNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavNext.Click
    ' 'Next' Button.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    If thisPosition < (myTable.Rows.Count - 1) Then thisPosition += 1
    If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
      thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
    End If

    THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

    Exit Sub

    If myTable.Rows.Count > thisPosition Then
      thisDataRow = myTable.Rows(thisPosition)
      Call GetFormData(thisDataRow)
    Else
      Call GetFormData(Nothing)
    End If

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnNavFirst control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnNavFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavFirst.Click
    ' 'First' Button.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    thisPosition = 0
    If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
      thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
    End If

    If (THIS_FORM_SelectingCombo.SelectedIndex = thisPosition) Then
      Combo_SelectComboChanged(THIS_FORM_SelectingCombo, New EventArgs)
    Else
      THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
    End If

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnLast control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLast.Click
    ' 'Last' Button.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    thisPosition = THIS_FORM_SelectingCombo.Items.Count - 1

    THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

  End Sub

    ''' <summary>
    ''' Get_s the position.
    ''' </summary>
    ''' <param name="pAuditID">The p audit ID.</param>
    ''' <returns>System.Int32.</returns>
  Private Function Get_Position(ByVal pAuditID As Integer) As Integer
    ' Returns the position in the current 'SortedRows' array of the entry corresponding to the given 
    ' AudidID.

    Dim MinIndex As Integer
    Dim MaxIndex As Integer
    Dim CurrentMin As Integer
    Dim CurrentMax As Integer
    Dim searchDataRow As DataRow

    Try
      ' SortedRows exists ?

      If SortedRows Is Nothing Then
        Return (-1)
        Exit Function
      End If

      ' Return (-1) if there are no rows in the 'SortedRows'

      If (SortedRows.GetLength(0) <= 0) Then
        Return (-1)
        Exit Function
      End If


      ' Use a modified Search moving outwards from the last returned value to locate the required value.
      ' Reflecting the fact that for the most part One looks for closely located records.

      MinIndex = 0
      MaxIndex = SortedRows.GetLength(0) - 1


      ' Check First and Last records (Incase 'First' or 'Last' was pressed).

      searchDataRow = SortedRows(MinIndex)

      If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
        Return MinIndex
        Exit Function
      End If

      searchDataRow = SortedRows(MaxIndex)
      If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
        Return MaxIndex
        Exit Function
      End If

      ' now search outwards from the last returned value.

      If (Me.thisPosition >= MinIndex) And (Me.thisPosition <= MaxIndex) Then
        CurrentMin = thisPosition
        CurrentMax = CurrentMin + 1
      Else
        CurrentMin = CInt((MinIndex + MaxIndex) / 2)
        CurrentMax = CurrentMin + 1
      End If

      While (CurrentMin >= MinIndex) Or (CurrentMax <= MaxIndex)
        If (CurrentMin >= MinIndex) Then
          searchDataRow = SortedRows(CurrentMin)

          If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
            Return CurrentMin
            Exit Function
          End If

          CurrentMin -= 1
        End If

        If (CurrentMax <= MaxIndex) Then
          searchDataRow = SortedRows(CurrentMax)

          If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
            Return CurrentMax
            Exit Function
          End If

          CurrentMax += 1
        End If

      End While

      Return (-1)
      Exit Function

    Catch ex As Exception
      MainForm.LogError(Me.Name & ", Get_Position()", LOG_LEVELS.Warning, ex.Message, "", ex.StackTrace, True)
      Return (-1)
      Exit Function
    End Try

  End Function

#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

    ''' <summary>
    ''' Handles the Click event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    ' Cancel Changes, redisplay form.

    Try
      FormChanged = False
      AddNewRecord = False

      Me.THIS_FORM_SelectingCombo.Enabled = True

      If (thisPosition >= 0) And (thisPosition < Me.SortedRows.GetLength(0)) Then
        thisDataRow = Me.SortedRows(thisPosition)
        Call GetFormData(thisDataRow)
      Else
        Call btnNavFirst_Click(Me, New System.EventArgs)
      End If
    Catch ex As Exception
      Call MainForm.LogError(Me.Name & ", btnCancel_Click()", LOG_LEVELS.Error, ex.Message, "Error in btnCancel_Click().", ex.StackTrace, True)
    End Try

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnSave control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
    ' Save Changes, if any, without prompting.

    If (FormChanged = True) Then
      Call SetFormData(False)
    End If

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnDelete control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
    Dim Position As Integer
    Dim NextAuditID As Integer

    Try
      If (AddNewRecord = True) Then
        Call btnCancel_Click(Me, New System.EventArgs)
        Exit Sub
      End If
    Catch ex As Exception
    End Try

    ' No Appropriate Save permission :-

    Try
      If (Me.HasDeletePermission = False) Then
        MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "You do not have permission to Delete this record.", "", True)
        Call btnCancel_Click(Me, New System.EventArgs)
        Exit Sub
      End If
    Catch ex As Exception
    End Try

    ' *************************************************************
    ' KnowledgeDate OK :-
    ' *************************************************************

    Try
      If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
        MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "Changes to Venice are not allowed when the Knowledgedate is not set to `NOW`", "", True)
        Call btnCancel_Click(Me, New System.EventArgs)
        Exit Sub
      End If
    Catch ex As Exception
    End Try

    ' *************************************************************
    ' Confirm :-
    ' *************************************************************
    If MsgBox("Are you Sure ?", MsgBoxStyle.OkCancel, "Confirm Delete") = MsgBoxResult.Cancel Then
      Exit Sub
    End If

    ' Check Data position.

    Try
      Position = Get_Position(thisAuditID)

      If Position < 0 Then Exit Sub
    Catch ex As Exception
      Call MainForm.LogError(Me.Name & ", Delete()", LOG_LEVELS.Error, ex.Message, "Error while getting Record position.", ex.StackTrace, True)
    End Try

    ' Check Referential Integrity 
    Try
      If CheckReferentialIntegrity(_MainForm, Me.SortedRows(Position)) = False Then
        MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "Deleting this record would voilate referential integrity.", "", True)
        Call btnCancel_Click(Me, New System.EventArgs)
        Exit Sub
      End If
    Catch ex As Exception
      Call MainForm.LogError(Me.Name & ", Delete()", LOG_LEVELS.Error, ex.Message, "Error while checking Referential Integrity.", ex.StackTrace, True)
    End Try

    ' Resolve row to show after deleting this one.

    Try
      NextAuditID = (-1)
      If (Position + 1) < Me.SortedRows.GetLength(0) Then
        NextAuditID = CInt(Me.SortedRows(Position + 1)("AuditID"))
      ElseIf (Position - 1) >= 0 Then
        NextAuditID = CInt(Me.SortedRows(Position - 1)("AuditID"))
      Else
        NextAuditID = (-1)
      End If
    Catch ex As Exception
      Call MainForm.LogError(Me.Name & ", Delete()", LOG_LEVELS.Error, ex.Message, "Error while Deleting Record.", ex.StackTrace, True)
    End Try

    ' Delete this row

    Try
      Me.SortedRows(Position).Delete()

      MainForm.AdaptorUpdate(Me.Name, myAdaptor, myTable)

    Catch ex As Exception
      Call MainForm.LogError(Me.Name & ", Delete()", LOG_LEVELS.Error, ex.Message, "Error Deleting Record.", ex.StackTrace, True)
    End Try

    ' Tidy Up.

    FormChanged = False

    thisAuditID = NextAuditID
		Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID), True)

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnAdd control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Friend Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
    ' Prepare form to Add a new record.

    Try
      If (FormChanged = True) Then
        Call SetFormData()
      End If

      thisPosition = -1
      InPaint = True
      MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
      InPaint = False

      GetFormData(Nothing)
      AddNewRecord = True
      Me.btnCancel.Enabled = True
      Me.THIS_FORM_SelectingCombo.Enabled = False

      Call SetButtonStatus()

      THIS_FORM_NewMoveToControl.Focus()
    Catch ex As Exception
      Call MainForm.LogError(Me.Name & ", btnAdd_Click()", LOG_LEVELS.Error, ex.Message, "Error in btnAdd_Click().", ex.StackTrace, True)
    End Try

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnClose control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
    ' Close Form

    Try
      If (FormChanged = True) Then
        Call SetFormData()
      End If

      Me.Close()
    Catch ex As Exception
      Call MainForm.LogError(Me.Name & ", btnClose_Click()", LOG_LEVELS.Error, ex.Message, "Error in btnClose_Click().", ex.StackTrace, True)
    End Try

  End Sub

#End Region

#Region " Bug hunting "

  ' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
  ' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
  ' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

    ''' <summary>
    ''' Called when [row updating].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

    ''' <summary>
    ''' Called when [row updated].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

    ''' <summary>
    ''' Called when [row fill error].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
    Dim a As Integer

    a = 1
  End Sub

#End Region


  'Private Sub Check_ITReviewed_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_ITReviewed.CheckedChanged
  '  If InPaint = False Then

  '  End If
  'End Sub

  'Private Sub Check_Review_Rejected_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_Review_Rejected.CheckedChanged
  '  If InPaint = False Then

  '  End If
  'End Sub

  'Private Sub Check_AuthorisedIT_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_AuthorisedIT.CheckedChanged
  '  If InPaint = False Then

  '  End If
  'End Sub

  'Private Sub Check_AuthorisedOwner_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_AuthorisedOwner.CheckedChanged
  '  If InPaint = False Then

  '  End If
  'End Sub

  'Private Sub Check_AuthorisedBusiness_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_AuthorisedBusiness.CheckedChanged
  '  If InPaint = False Then

  '  End If
  'End Sub

  'Private Sub Check_AuthorisedRejected_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_AuthorisedRejected.CheckedChanged
  '  If InPaint = False Then

  '  End If
  'End Sub

  'Private Sub Check_AcceptedIT_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_AcceptedIT.CheckedChanged
  '  If InPaint = False Then

  '  End If
  'End Sub

  'Private Sub Check_AcceptedOwner_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_AcceptedOwner.CheckedChanged
  '  If InPaint = False Then

  '  End If
  'End Sub

  'Private Sub Check_AcceptedBusiness_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_AcceptedBusiness.CheckedChanged
  '  If InPaint = False Then

  '  End If
  'End Sub
End Class
