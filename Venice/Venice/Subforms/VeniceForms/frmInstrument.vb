' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmInstrument.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals


''' <summary>
''' Class frmInstrument
''' </summary>
Public Class frmInstrument

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmInstrument"/> class from being created.
    ''' </summary>
  Private Sub New()
	MyBase.New()

	'This call is required by the Windows Form Designer.
	InitializeComponent()

	'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
	If disposing Then
	  If Not (components Is Nothing) Then
		components.Dispose()
	  End If
	End If
	MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The LBL instrument description
    ''' </summary>
  Friend WithEvents lblInstrumentDescription As System.Windows.Forms.Label
    ''' <summary>
    ''' The LBL instrument code
    ''' </summary>
  Friend WithEvents lblInstrumentCode As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit audit ID
    ''' </summary>
  Friend WithEvents editAuditID As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The edit_ instrument description
    ''' </summary>
  Friend WithEvents edit_InstrumentDescription As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The edit_ instrument code
    ''' </summary>
  Friend WithEvents edit_InstrumentCode As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The BTN nav first
    ''' </summary>
  Friend WithEvents btnNavFirst As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN nav prev
    ''' </summary>
  Friend WithEvents btnNavPrev As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN nav next
    ''' </summary>
  Friend WithEvents btnNavNext As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN last
    ''' </summary>
  Friend WithEvents btnLast As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN add
    ''' </summary>
  Friend WithEvents btnAdd As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN delete
    ''' </summary>
  Friend WithEvents btnDelete As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN cancel
    ''' </summary>
  Friend WithEvents btnCancel As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN save
    ''' </summary>
  Friend WithEvents btnSave As System.Windows.Forms.Button
    ''' <summary>
    ''' The combo_ select instrument description
    ''' </summary>
  Friend WithEvents Combo_SelectInstrumentDescription As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The panel1
    ''' </summary>
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The label1
    ''' </summary>
  Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The group box1
    ''' </summary>
  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The BTN close
    ''' </summary>
  Friend WithEvents btnClose As System.Windows.Forms.Button
    ''' <summary>
    ''' The label2
    ''' </summary>
  Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ PFPCID
    ''' </summary>
  Friend WithEvents edit_PFPCID As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The combo_ pertrac index
    ''' </summary>
	Friend WithEvents Combo_PertracIndex As FCP_TelerikControls.FCP_RadComboBox
    ''' <summary>
    ''' The LBL pertrac index
    ''' </summary>
  Friend WithEvents lblPertracIndex As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ instrument type
    ''' </summary>
  Friend WithEvents Combo_InstrumentType As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label3
    ''' </summary>
  Friend WithEvents Label3 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ instrument parent
    ''' </summary>
  Friend WithEvents Combo_InstrumentParent As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label4
    ''' </summary>
  Friend WithEvents Label4 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ instrument class
    ''' </summary>
  Friend WithEvents edit_InstrumentClass As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The label5
    ''' </summary>
  Friend WithEvents Label5 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ instrument fund type
    ''' </summary>
  Friend WithEvents Combo_InstrumentFundType As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label6
    ''' </summary>
  Friend WithEvents Label6 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ instrument currency
    ''' </summary>
  Friend WithEvents Combo_InstrumentCurrency As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label7
    ''' </summary>
  Friend WithEvents Label7 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ expected return
    ''' </summary>
  Friend WithEvents edit_ExpectedReturn As C1.Win.C1Input.C1TextBox
    ''' <summary>
    ''' The LBL volatility
    ''' </summary>
  Friend WithEvents lblVolatility As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ worst case fall
    ''' </summary>
  Friend WithEvents edit_WorstCaseFall As C1.Win.C1Input.C1TextBox
    ''' <summary>
    ''' The label8
    ''' </summary>
  Friend WithEvents Label8 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ dealing notice days
    ''' </summary>
  Friend WithEvents edit_DealingNoticeDays As C1.Win.C1Input.C1TextBox
    ''' <summary>
    ''' The label9
    ''' </summary>
  Friend WithEvents Label9 As System.Windows.Forms.Label
    ''' <summary>
    ''' The check_ limit display
    ''' </summary>
  Friend WithEvents Check_LimitDisplay As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The label_ adminfund type
    ''' </summary>
  Friend WithEvents Label_AdminfundType As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ dealing period
    ''' </summary>
  Friend WithEvents Combo_DealingPeriod As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label10
    ''' </summary>
  Friend WithEvents Label10 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ dealing base date
    ''' </summary>
  Friend WithEvents edit_DealingBaseDate As C1.Win.C1Input.C1TextBox
    ''' <summary>
    ''' The label11
    ''' </summary>
  Friend WithEvents Label11 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ lockup period
    ''' </summary>
  Friend WithEvents edit_LockupPeriod As C1.Win.C1Input.C1TextBox
    ''' <summary>
    ''' The label12
    ''' </summary>
  Friend WithEvents Label12 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ penalty
    ''' </summary>
  Friend WithEvents edit_Penalty As C1.Win.C1Input.C1TextBox
    ''' <summary>
    ''' The label13
    ''' </summary>
  Friend WithEvents Label13 As System.Windows.Forms.Label
    ''' <summary>
    ''' The check_ upfront fees
    ''' </summary>
  Friend WithEvents Check_UpfrontFees As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The label14
    ''' </summary>
  Friend WithEvents Label14 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ liquidity comment
    ''' </summary>
  Friend WithEvents edit_LiquidityComment As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The label15
    ''' </summary>
  Friend WithEvents Label15 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ latest trade date
    ''' </summary>
  Friend WithEvents edit_LatestTradeDate As C1.Win.C1Input.C1TextBox
    ''' <summary>
    ''' The label16
    ''' </summary>
  Friend WithEvents Label16 As System.Windows.Forms.Label
    ''' <summary>
    ''' The check_ exclude from GAV
    ''' </summary>
  Friend WithEvents Check_ExcludeFromGAV As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ is management fee
    ''' </summary>
  Friend WithEvents Check_IsManagementFee As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ is leverage instrument
    ''' </summary>
  Friend WithEvents Check_IsLeverageInstrument As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ is incentive fee
    ''' </summary>
  Friend WithEvents Check_IsIncentiveFee As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The combo_ leverage counterparty
    ''' </summary>
  Friend WithEvents Combo_LeverageCounterparty As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label17
    ''' </summary>
  Friend WithEvents Label17 As System.Windows.Forms.Label
    ''' <summary>
    ''' The button_ show parent heirarchy
    ''' </summary>
  Friend WithEvents Button_ShowParentHeirarchy As System.Windows.Forms.Button
    ''' <summary>
    ''' The check_ is equalisation
    ''' </summary>
  Friend WithEvents Check_IsEqualisation As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The edit_ portfolio ticker
    ''' </summary>
  Friend WithEvents edit_PortfolioTicker As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The edit_ exchange code
    ''' </summary>
  Friend WithEvents edit_ExchangeCode As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The label19
    ''' </summary>
  Friend WithEvents Label19 As System.Windows.Forms.Label
    ''' <summary>
    ''' The root menu
    ''' </summary>
	Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
    ''' <summary>
    ''' The BTN add copy
    ''' </summary>
	Friend WithEvents btnAddCopy As System.Windows.Forms.Button
    ''' <summary>
    ''' The check_ exclude from AUM
    ''' </summary>
	Friend WithEvents Check_ExcludeFromAUM As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The edit_ SEDOL
    ''' </summary>
	Friend WithEvents edit_SEDOL As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The label20
    ''' </summary>
	Friend WithEvents Label20 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ ISIN
    ''' </summary>
	Friend WithEvents edit_ISIN As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The label21
    ''' </summary>
	Friend WithEvents Label21 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ review group
    ''' </summary>
	Friend WithEvents Combo_ReviewGroup As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label22
    ''' </summary>
	Friend WithEvents Label22 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ settlement
    ''' </summary>
	Friend WithEvents Label_Settlement As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ instrument multiplier
    ''' </summary>
	Friend WithEvents edit_InstrumentMultiplier As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The label23
    ''' </summary>
	Friend WithEvents Label23 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ contract size
    ''' </summary>
	Friend WithEvents edit_ContractSize As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The edit_ expiry date
    ''' </summary>
	Friend WithEvents edit_ExpiryDate As C1.Win.C1Input.C1TextBox
  ''' <summary>
  ''' The combo_ instrument underlying
  ''' </summary>
  Friend WithEvents Combo_InstrumentUnderlying As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The tab_ instrument details
  ''' </summary>
  Friend WithEvents Tab_InstrumentDetails As System.Windows.Forms.TabControl
  ''' <summary>
  ''' The tab_ basic details
  ''' </summary>
  Friend WithEvents Tab_BasicDetails As System.Windows.Forms.TabPage
  ''' <summary>
  ''' The tab_ advanced
  ''' </summary>
  Friend WithEvents Tab_Advanced As System.Windows.Forms.TabPage
  ''' <summary>
  ''' The tab_ legacy
  ''' </summary>
  Friend WithEvents Tab_Legacy As System.Windows.Forms.TabPage
  ''' <summary>
  ''' The group_ linked entities
  ''' </summary>
  Friend WithEvents Group_LinkedEntities As System.Windows.Forms.GroupBox
  ''' <summary>
  ''' The list_ entity
  ''' </summary>
  Friend WithEvents List_Entity As System.Windows.Forms.ListBox
  ''' <summary>
  ''' The button_ delete entity
  ''' </summary>
  Friend WithEvents Button_DeleteEntity As System.Windows.Forms.Button
  ''' <summary>
  ''' The label26
  ''' </summary>
  Friend WithEvents Label26 As System.Windows.Forms.Label
  ''' <summary>
  ''' The button_ add entity
  ''' </summary>
  Friend WithEvents Button_AddEntity As System.Windows.Forms.Button
  ''' <summary>
  ''' The combo_ add entity
  ''' </summary>
  Friend WithEvents Combo_AddEntity As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The label27
  ''' </summary>
  Friend WithEvents Label27 As System.Windows.Forms.Label
  ''' <summary>
  ''' The edit_ morningstar
  ''' </summary>
  Friend WithEvents edit_Morningstar As System.Windows.Forms.TextBox
  ''' <summary>
  ''' The label28
  ''' </summary>
  Friend WithEvents Label28 As System.Windows.Forms.Label
  ''' <summary>
  ''' The edit_ settlemet_ buy
  ''' </summary>
  Friend WithEvents edit_Settlemet_Buy As C1.Win.C1Input.C1TextBox
  ''' <summary>
  ''' The label29
  ''' </summary>
  Friend WithEvents Label29 As System.Windows.Forms.Label
  ''' <summary>
  ''' The edit_ settlemet_ sell
  ''' </summary>
  Friend WithEvents edit_Settlemet_Sell As C1.Win.C1Input.C1TextBox
  ''' <summary>
  ''' The label30
  ''' </summary>
  Friend WithEvents Label30 As System.Windows.Forms.Label
  ''' <summary>
  ''' The check_ price capture
  ''' </summary>
  Friend WithEvents Check_PriceCapture As System.Windows.Forms.CheckBox
  ''' <summary>
  ''' The label31
  ''' </summary>
  Friend WithEvents Label31 As System.Windows.Forms.Label
  ''' <summary>
  ''' The combo_ benckmark
  ''' </summary>
  Friend WithEvents Combo_Benckmark As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The edit_ max decimal places
  ''' </summary>
  Friend WithEvents edit_MaxDecimalPlaces As C1.Win.C1Input.C1TextBox
  ''' <summary>
  ''' The label32
  ''' </summary>
  Friend WithEvents Label32 As System.Windows.Forms.Label
  ''' <summary>
  ''' The label33
  ''' </summary>
  Friend WithEvents Label33 As System.Windows.Forms.Label
  ''' <summary>
  ''' The date_ cutoff
  ''' </summary>
  Friend WithEvents Date_Cutoff As System.Windows.Forms.DateTimePicker
  ''' <summary>
  ''' The tab_ derivatives
  ''' </summary>
  Friend WithEvents Tab_Derivatives As System.Windows.Forms.TabPage
  ''' <summary>
  ''' The tab_ performance
  ''' </summary>
  Friend WithEvents Tab_Performance As System.Windows.Forms.TabPage
  ''' <summary>
  ''' The check_ trade as value
  ''' </summary>
  Friend WithEvents Check_TradeAsValue As System.Windows.Forms.CheckBox
  Friend WithEvents Tab_Bonds As System.Windows.Forms.TabPage
  Friend WithEvents Label34 As System.Windows.Forms.Label
  Friend WithEvents Tab_Convertibles As System.Windows.Forms.TabPage
  Friend WithEvents Label39 As System.Windows.Forms.Label
  Friend WithEvents Label38 As System.Windows.Forms.Label
  Friend WithEvents Label37 As System.Windows.Forms.Label
  Friend WithEvents Label36 As System.Windows.Forms.Label
  Friend WithEvents Label35 As System.Windows.Forms.Label
  Friend WithEvents Text_BondGoverningLaw As System.Windows.Forms.TextBox
  Friend WithEvents Text_BondRanking As System.Windows.Forms.TextBox
  Friend WithEvents Text_BondIssuer As System.Windows.Forms.TextBox
  Friend WithEvents Panel_Coupons As System.Windows.Forms.Panel
  Friend WithEvents Date_BondMaturityDate As System.Windows.Forms.DateTimePicker
  Friend WithEvents edit_BondInterestPercentage As RenaissanceControls.PercentageTextBox
  Friend WithEvents Combo_CB_Underlying As System.Windows.Forms.ComboBox
  Friend WithEvents Label46 As System.Windows.Forms.Label
  Friend WithEvents edit_CB_InterestRate As RenaissanceControls.PercentageTextBox
  Friend WithEvents Date_CB_MaturityDate As System.Windows.Forms.DateTimePicker
  Friend WithEvents Text_CB_GoverningLaw As System.Windows.Forms.TextBox
  Friend WithEvents Text_CB_Ranking As System.Windows.Forms.TextBox
  Friend WithEvents Text_CB_Issuer As System.Windows.Forms.TextBox
  Friend WithEvents Panel_CB_CouponDates As System.Windows.Forms.Panel
  Friend WithEvents Label40 As System.Windows.Forms.Label
  Friend WithEvents Label41 As System.Windows.Forms.Label
  Friend WithEvents Label42 As System.Windows.Forms.Label
  Friend WithEvents Label43 As System.Windows.Forms.Label
  Friend WithEvents Label44 As System.Windows.Forms.Label
  Friend WithEvents Label45 As System.Windows.Forms.Label
  Friend WithEvents Label48 As System.Windows.Forms.Label
  Friend WithEvents Combo_AttributionSector As System.Windows.Forms.ComboBox
  Friend WithEvents Label47 As System.Windows.Forms.Label
  Friend WithEvents Combo_AttributionMarket As System.Windows.Forms.ComboBox
  Friend WithEvents Label49 As System.Windows.Forms.Label
  Friend WithEvents edit_BondMultiplier As RenaissanceControls.NumericTextBox
  Friend WithEvents Label_FundID As System.Windows.Forms.Label
  Friend WithEvents Combo_FundID As System.Windows.Forms.ComboBox
  Friend WithEvents Tab_Unit As System.Windows.Forms.TabPage
  Friend WithEvents Label50 As System.Windows.Forms.Label
  Friend WithEvents Combo_PerformanceVsBenchmark As System.Windows.Forms.ComboBox
  Friend WithEvents Label51 As System.Windows.Forms.Label
  Friend WithEvents edit_FundManagementFees As RenaissanceControls.PercentageTextBox
  Friend WithEvents Label52 As System.Windows.Forms.Label
  Friend WithEvents edit_FundPerformanceFees As RenaissanceControls.PercentageTextBox
  Friend WithEvents Label53 As System.Windows.Forms.Label
  Friend WithEvents edit_PerformanceHurdle As RenaissanceControls.PercentageTextBox
  Friend WithEvents TabControl_HiddenTabs As System.Windows.Forms.TabControl
  Friend WithEvents Label54 As System.Windows.Forms.Label
  Friend WithEvents Check_IsCrystalisedIncentiveFee As System.Windows.Forms.CheckBox
  Friend WithEvents Label55 As System.Windows.Forms.Label
  Friend WithEvents edit_CB_ContractSize As RenaissanceControls.NumericTextBox
  Friend WithEvents Label57 As System.Windows.Forms.Label
  Friend WithEvents edit_ISO2_CountryCode As System.Windows.Forms.TextBox
  Friend WithEvents Label56 As System.Windows.Forms.Label
  Friend WithEvents Label60 As System.Windows.Forms.Label
  Friend WithEvents Label59 As System.Windows.Forms.Label
  Friend WithEvents edit_SharesPerOption As RenaissanceControls.NumericTextBox
  Friend WithEvents Label_SharesPerOption As System.Windows.Forms.Label
  Friend WithEvents Label_SharesPerOptionComment As System.Windows.Forms.Label
  Friend WithEvents Label_IsFxFuture As System.Windows.Forms.Label
  Friend WithEvents Check_IsFXFuture As System.Windows.Forms.CheckBox
  Friend WithEvents Label_FixedCurrencyNote As System.Windows.Forms.Label
  Friend WithEvents Combo_FXFixedCurrency As System.Windows.Forms.ComboBox
  Friend WithEvents Label_FixedCurrency As System.Windows.Forms.Label
  Friend WithEvents Label25 As System.Windows.Forms.Label
  Friend WithEvents Label24 As System.Windows.Forms.Label
  Friend WithEvents Check_IsMovementCommission As System.Windows.Forms.CheckBox
  Friend WithEvents Label58 As System.Windows.Forms.Label
  Friend WithEvents Text_ProspectusNotes As System.Windows.Forms.TextBox
  Friend WithEvents Tab_ManagerNotes As System.Windows.Forms.TabPage
  Friend WithEvents Label61 As System.Windows.Forms.Label
  Friend WithEvents Text_ManagerNotes As System.Windows.Forms.TextBox
  ''' <summary>
  ''' The label18
  ''' </summary>
  Friend WithEvents Label18 As System.Windows.Forms.Label
  ''' <summary>
  ''' Initializes the component.
  ''' </summary>
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.lblInstrumentDescription = New System.Windows.Forms.Label
    Me.lblInstrumentCode = New System.Windows.Forms.Label
    Me.editAuditID = New System.Windows.Forms.TextBox
    Me.edit_InstrumentDescription = New System.Windows.Forms.TextBox
    Me.edit_InstrumentCode = New System.Windows.Forms.TextBox
    Me.btnNavFirst = New System.Windows.Forms.Button
    Me.btnNavPrev = New System.Windows.Forms.Button
    Me.btnNavNext = New System.Windows.Forms.Button
    Me.btnLast = New System.Windows.Forms.Button
    Me.btnAdd = New System.Windows.Forms.Button
    Me.btnDelete = New System.Windows.Forms.Button
    Me.btnCancel = New System.Windows.Forms.Button
    Me.btnSave = New System.Windows.Forms.Button
    Me.Combo_SelectInstrumentDescription = New System.Windows.Forms.ComboBox
    Me.Panel1 = New System.Windows.Forms.Panel
    Me.btnAddCopy = New System.Windows.Forms.Button
    Me.Label1 = New System.Windows.Forms.Label
    Me.GroupBox1 = New System.Windows.Forms.GroupBox
    Me.btnClose = New System.Windows.Forms.Button
    Me.edit_PFPCID = New System.Windows.Forms.TextBox
    Me.Label2 = New System.Windows.Forms.Label
    Me.lblPertracIndex = New System.Windows.Forms.Label
    Me.Combo_InstrumentType = New System.Windows.Forms.ComboBox
    Me.Label3 = New System.Windows.Forms.Label
    Me.Combo_InstrumentParent = New System.Windows.Forms.ComboBox
    Me.Label4 = New System.Windows.Forms.Label
    Me.edit_InstrumentClass = New System.Windows.Forms.TextBox
    Me.Label5 = New System.Windows.Forms.Label
    Me.Combo_InstrumentFundType = New System.Windows.Forms.ComboBox
    Me.Label6 = New System.Windows.Forms.Label
    Me.Combo_InstrumentCurrency = New System.Windows.Forms.ComboBox
    Me.Label7 = New System.Windows.Forms.Label
    Me.edit_ExpectedReturn = New C1.Win.C1Input.C1TextBox
    Me.lblVolatility = New System.Windows.Forms.Label
    Me.edit_WorstCaseFall = New C1.Win.C1Input.C1TextBox
    Me.Label8 = New System.Windows.Forms.Label
    Me.edit_DealingNoticeDays = New C1.Win.C1Input.C1TextBox
    Me.Label9 = New System.Windows.Forms.Label
    Me.Check_LimitDisplay = New System.Windows.Forms.CheckBox
    Me.Label_AdminfundType = New System.Windows.Forms.Label
    Me.Combo_DealingPeriod = New System.Windows.Forms.ComboBox
    Me.Label10 = New System.Windows.Forms.Label
    Me.edit_DealingBaseDate = New C1.Win.C1Input.C1TextBox
    Me.Label11 = New System.Windows.Forms.Label
    Me.edit_LockupPeriod = New C1.Win.C1Input.C1TextBox
    Me.Label12 = New System.Windows.Forms.Label
    Me.edit_Penalty = New C1.Win.C1Input.C1TextBox
    Me.Label13 = New System.Windows.Forms.Label
    Me.Check_UpfrontFees = New System.Windows.Forms.CheckBox
    Me.Label14 = New System.Windows.Forms.Label
    Me.edit_LiquidityComment = New System.Windows.Forms.TextBox
    Me.Label15 = New System.Windows.Forms.Label
    Me.edit_LatestTradeDate = New C1.Win.C1Input.C1TextBox
    Me.Label16 = New System.Windows.Forms.Label
    Me.Check_ExcludeFromGAV = New System.Windows.Forms.CheckBox
    Me.Check_IsManagementFee = New System.Windows.Forms.CheckBox
    Me.Check_IsLeverageInstrument = New System.Windows.Forms.CheckBox
    Me.Check_IsIncentiveFee = New System.Windows.Forms.CheckBox
    Me.Combo_LeverageCounterparty = New System.Windows.Forms.ComboBox
    Me.Label17 = New System.Windows.Forms.Label
    Me.Button_ShowParentHeirarchy = New System.Windows.Forms.Button
    Me.Check_IsEqualisation = New System.Windows.Forms.CheckBox
    Me.edit_PortfolioTicker = New System.Windows.Forms.TextBox
    Me.Label18 = New System.Windows.Forms.Label
    Me.edit_ExchangeCode = New System.Windows.Forms.TextBox
    Me.Label19 = New System.Windows.Forms.Label
    Me.RootMenu = New System.Windows.Forms.MenuStrip
    Me.Check_ExcludeFromAUM = New System.Windows.Forms.CheckBox
    Me.edit_SEDOL = New System.Windows.Forms.TextBox
    Me.Label20 = New System.Windows.Forms.Label
    Me.edit_ISIN = New System.Windows.Forms.TextBox
    Me.Label21 = New System.Windows.Forms.Label
    Me.Combo_ReviewGroup = New System.Windows.Forms.ComboBox
    Me.Label22 = New System.Windows.Forms.Label
    Me.Label_Settlement = New System.Windows.Forms.Label
    Me.edit_InstrumentMultiplier = New RenaissanceControls.NumericTextBox
    Me.Label23 = New System.Windows.Forms.Label
    Me.edit_ContractSize = New RenaissanceControls.NumericTextBox
    Me.edit_ExpiryDate = New C1.Win.C1Input.C1TextBox
    Me.Combo_InstrumentUnderlying = New System.Windows.Forms.ComboBox
    Me.Tab_InstrumentDetails = New System.Windows.Forms.TabControl
    Me.Tab_BasicDetails = New System.Windows.Forms.TabPage
    Me.Check_IsMovementCommission = New System.Windows.Forms.CheckBox
    Me.Label57 = New System.Windows.Forms.Label
    Me.edit_ISO2_CountryCode = New System.Windows.Forms.TextBox
    Me.Label56 = New System.Windows.Forms.Label
    Me.Check_IsCrystalisedIncentiveFee = New System.Windows.Forms.CheckBox
    Me.Label48 = New System.Windows.Forms.Label
    Me.Combo_AttributionSector = New System.Windows.Forms.ComboBox
    Me.Label47 = New System.Windows.Forms.Label
    Me.Combo_AttributionMarket = New System.Windows.Forms.ComboBox
    Me.Check_TradeAsValue = New System.Windows.Forms.CheckBox
    Me.Label33 = New System.Windows.Forms.Label
    Me.Date_Cutoff = New System.Windows.Forms.DateTimePicker
    Me.edit_MaxDecimalPlaces = New C1.Win.C1Input.C1TextBox
    Me.Label32 = New System.Windows.Forms.Label
    Me.Check_PriceCapture = New System.Windows.Forms.CheckBox
    Me.edit_Settlemet_Sell = New C1.Win.C1Input.C1TextBox
    Me.Label30 = New System.Windows.Forms.Label
    Me.edit_Settlemet_Buy = New C1.Win.C1Input.C1TextBox
    Me.Label29 = New System.Windows.Forms.Label
    Me.edit_Morningstar = New System.Windows.Forms.TextBox
    Me.Label28 = New System.Windows.Forms.Label
    Me.Tab_ManagerNotes = New System.Windows.Forms.TabPage
    Me.Label61 = New System.Windows.Forms.Label
    Me.Text_ManagerNotes = New System.Windows.Forms.TextBox
    Me.Tab_Unit = New System.Windows.Forms.TabPage
    Me.Label58 = New System.Windows.Forms.Label
    Me.Text_ProspectusNotes = New System.Windows.Forms.TextBox
    Me.Label50 = New System.Windows.Forms.Label
    Me.Combo_PerformanceVsBenchmark = New System.Windows.Forms.ComboBox
    Me.Label51 = New System.Windows.Forms.Label
    Me.edit_FundManagementFees = New RenaissanceControls.PercentageTextBox
    Me.Label52 = New System.Windows.Forms.Label
    Me.edit_FundPerformanceFees = New RenaissanceControls.PercentageTextBox
    Me.Label53 = New System.Windows.Forms.Label
    Me.edit_PerformanceHurdle = New RenaissanceControls.PercentageTextBox
    Me.Label_FundID = New System.Windows.Forms.Label
    Me.Combo_FundID = New System.Windows.Forms.ComboBox
    Me.Tab_Derivatives = New System.Windows.Forms.TabPage
    Me.Label24 = New System.Windows.Forms.Label
    Me.Label25 = New System.Windows.Forms.Label
    Me.Label_FixedCurrencyNote = New System.Windows.Forms.Label
    Me.Combo_FXFixedCurrency = New System.Windows.Forms.ComboBox
    Me.Label_IsFxFuture = New System.Windows.Forms.Label
    Me.Check_IsFXFuture = New System.Windows.Forms.CheckBox
    Me.Label_SharesPerOptionComment = New System.Windows.Forms.Label
    Me.Label60 = New System.Windows.Forms.Label
    Me.Label59 = New System.Windows.Forms.Label
    Me.edit_SharesPerOption = New RenaissanceControls.NumericTextBox
    Me.Label_FixedCurrency = New System.Windows.Forms.Label
    Me.Label_SharesPerOption = New System.Windows.Forms.Label
    Me.Tab_Bonds = New System.Windows.Forms.TabPage
    Me.Label49 = New System.Windows.Forms.Label
    Me.edit_BondMultiplier = New RenaissanceControls.NumericTextBox
    Me.edit_BondInterestPercentage = New RenaissanceControls.PercentageTextBox
    Me.Date_BondMaturityDate = New System.Windows.Forms.DateTimePicker
    Me.Text_BondGoverningLaw = New System.Windows.Forms.TextBox
    Me.Text_BondRanking = New System.Windows.Forms.TextBox
    Me.Text_BondIssuer = New System.Windows.Forms.TextBox
    Me.Panel_Coupons = New System.Windows.Forms.Panel
    Me.Label39 = New System.Windows.Forms.Label
    Me.Label38 = New System.Windows.Forms.Label
    Me.Label37 = New System.Windows.Forms.Label
    Me.Label36 = New System.Windows.Forms.Label
    Me.Label35 = New System.Windows.Forms.Label
    Me.Label34 = New System.Windows.Forms.Label
    Me.Tab_Convertibles = New System.Windows.Forms.TabPage
    Me.Label55 = New System.Windows.Forms.Label
    Me.edit_CB_ContractSize = New RenaissanceControls.NumericTextBox
    Me.Combo_CB_Underlying = New System.Windows.Forms.ComboBox
    Me.Label46 = New System.Windows.Forms.Label
    Me.edit_CB_InterestRate = New RenaissanceControls.PercentageTextBox
    Me.Date_CB_MaturityDate = New System.Windows.Forms.DateTimePicker
    Me.Text_CB_GoverningLaw = New System.Windows.Forms.TextBox
    Me.Text_CB_Ranking = New System.Windows.Forms.TextBox
    Me.Text_CB_Issuer = New System.Windows.Forms.TextBox
    Me.Panel_CB_CouponDates = New System.Windows.Forms.Panel
    Me.Label40 = New System.Windows.Forms.Label
    Me.Label41 = New System.Windows.Forms.Label
    Me.Label42 = New System.Windows.Forms.Label
    Me.Label43 = New System.Windows.Forms.Label
    Me.Label44 = New System.Windows.Forms.Label
    Me.Label45 = New System.Windows.Forms.Label
    Me.Tab_Advanced = New System.Windows.Forms.TabPage
    Me.Tab_Performance = New System.Windows.Forms.TabPage
    Me.Group_LinkedEntities = New System.Windows.Forms.GroupBox
    Me.List_Entity = New System.Windows.Forms.ListBox
    Me.Button_DeleteEntity = New System.Windows.Forms.Button
    Me.Label26 = New System.Windows.Forms.Label
    Me.Button_AddEntity = New System.Windows.Forms.Button
    Me.Combo_AddEntity = New System.Windows.Forms.ComboBox
    Me.Label27 = New System.Windows.Forms.Label
    Me.Label31 = New System.Windows.Forms.Label
    Me.Combo_Benckmark = New System.Windows.Forms.ComboBox
    Me.Tab_Legacy = New System.Windows.Forms.TabPage
    Me.Combo_PertracIndex = New FCP_TelerikControls.FCP_RadComboBox
    Me.TabControl_HiddenTabs = New System.Windows.Forms.TabControl
    Me.Label54 = New System.Windows.Forms.Label
    Me.Panel1.SuspendLayout()
    CType(Me.edit_ExpectedReturn, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.edit_WorstCaseFall, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.edit_DealingNoticeDays, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.edit_DealingBaseDate, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.edit_LockupPeriod, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.edit_Penalty, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.edit_LatestTradeDate, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.edit_ExpiryDate, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_InstrumentDetails.SuspendLayout()
    Me.Tab_BasicDetails.SuspendLayout()
    CType(Me.edit_MaxDecimalPlaces, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.edit_Settlemet_Sell, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.edit_Settlemet_Buy, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_ManagerNotes.SuspendLayout()
    Me.Tab_Unit.SuspendLayout()
    Me.Tab_Derivatives.SuspendLayout()
    Me.Tab_Bonds.SuspendLayout()
    Me.Tab_Convertibles.SuspendLayout()
    Me.Tab_Advanced.SuspendLayout()
    Me.Tab_Performance.SuspendLayout()
    Me.Group_LinkedEntities.SuspendLayout()
    Me.Tab_Legacy.SuspendLayout()
    CType(Me.Combo_PertracIndex, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'lblInstrumentDescription
    '
    Me.lblInstrumentDescription.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.lblInstrumentDescription.Location = New System.Drawing.Point(8, 70)
    Me.lblInstrumentDescription.Name = "lblInstrumentDescription"
    Me.lblInstrumentDescription.Size = New System.Drawing.Size(100, 16)
    Me.lblInstrumentDescription.TabIndex = 47
    Me.lblInstrumentDescription.Text = "Description"
    '
    'lblInstrumentCode
    '
    Me.lblInstrumentCode.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.lblInstrumentCode.Location = New System.Drawing.Point(8, 118)
    Me.lblInstrumentCode.Name = "lblInstrumentCode"
    Me.lblInstrumentCode.Size = New System.Drawing.Size(100, 16)
    Me.lblInstrumentCode.TabIndex = 47
    Me.lblInstrumentCode.Text = "Instrument Code"
    '
    'editAuditID
    '
    Me.editAuditID.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.editAuditID.Enabled = False
    Me.editAuditID.Location = New System.Drawing.Point(518, 31)
    Me.editAuditID.Name = "editAuditID"
    Me.editAuditID.Size = New System.Drawing.Size(66, 20)
    Me.editAuditID.TabIndex = 1
    '
    'edit_InstrumentDescription
    '
    Me.edit_InstrumentDescription.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.edit_InstrumentDescription.Location = New System.Drawing.Point(120, 67)
    Me.edit_InstrumentDescription.Name = "edit_InstrumentDescription"
    Me.edit_InstrumentDescription.Size = New System.Drawing.Size(464, 20)
    Me.edit_InstrumentDescription.TabIndex = 2
    '
    'edit_InstrumentCode
    '
    Me.edit_InstrumentCode.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.edit_InstrumentCode.Location = New System.Drawing.Point(120, 115)
    Me.edit_InstrumentCode.Name = "edit_InstrumentCode"
    Me.edit_InstrumentCode.Size = New System.Drawing.Size(464, 20)
    Me.edit_InstrumentCode.TabIndex = 4
    '
    'btnNavFirst
    '
    Me.btnNavFirst.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNavFirst.Location = New System.Drawing.Point(8, 5)
    Me.btnNavFirst.Name = "btnNavFirst"
    Me.btnNavFirst.Size = New System.Drawing.Size(40, 28)
    Me.btnNavFirst.TabIndex = 0
    Me.btnNavFirst.Text = "<<"
    '
    'btnNavPrev
    '
    Me.btnNavPrev.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNavPrev.Location = New System.Drawing.Point(49, 5)
    Me.btnNavPrev.Name = "btnNavPrev"
    Me.btnNavPrev.Size = New System.Drawing.Size(35, 28)
    Me.btnNavPrev.TabIndex = 1
    Me.btnNavPrev.Text = "<"
    '
    'btnNavNext
    '
    Me.btnNavNext.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNavNext.Location = New System.Drawing.Point(88, 5)
    Me.btnNavNext.Name = "btnNavNext"
    Me.btnNavNext.Size = New System.Drawing.Size(35, 28)
    Me.btnNavNext.TabIndex = 2
    Me.btnNavNext.Text = ">"
    '
    'btnLast
    '
    Me.btnLast.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnLast.Location = New System.Drawing.Point(124, 5)
    Me.btnLast.Name = "btnLast"
    Me.btnLast.Size = New System.Drawing.Size(40, 28)
    Me.btnLast.TabIndex = 3
    Me.btnLast.Text = ">>"
    '
    'btnAdd
    '
    Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnAdd.Location = New System.Drawing.Point(176, 5)
    Me.btnAdd.Name = "btnAdd"
    Me.btnAdd.Size = New System.Drawing.Size(75, 28)
    Me.btnAdd.TabIndex = 4
    Me.btnAdd.Text = "&New"
    '
    'btnDelete
    '
    Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnDelete.Location = New System.Drawing.Point(220, 633)
    Me.btnDelete.Name = "btnDelete"
    Me.btnDelete.Size = New System.Drawing.Size(75, 28)
    Me.btnDelete.TabIndex = 8
    Me.btnDelete.Text = "&Delete"
    '
    'btnCancel
    '
    Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnCancel.Location = New System.Drawing.Point(304, 633)
    Me.btnCancel.Name = "btnCancel"
    Me.btnCancel.Size = New System.Drawing.Size(75, 28)
    Me.btnCancel.TabIndex = 9
    Me.btnCancel.Text = "&Cancel"
    '
    'btnSave
    '
    Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnSave.Location = New System.Drawing.Point(132, 633)
    Me.btnSave.Name = "btnSave"
    Me.btnSave.Size = New System.Drawing.Size(75, 28)
    Me.btnSave.TabIndex = 7
    Me.btnSave.Text = "&Save"
    '
    'Combo_SelectInstrumentDescription
    '
    Me.Combo_SelectInstrumentDescription.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_SelectInstrumentDescription.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectInstrumentDescription.Location = New System.Drawing.Point(120, 31)
    Me.Combo_SelectInstrumentDescription.Name = "Combo_SelectInstrumentDescription"
    Me.Combo_SelectInstrumentDescription.Size = New System.Drawing.Size(392, 21)
    Me.Combo_SelectInstrumentDescription.TabIndex = 0
    '
    'Panel1
    '
    Me.Panel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Panel1.Controls.Add(Me.btnAddCopy)
    Me.Panel1.Controls.Add(Me.btnNavFirst)
    Me.Panel1.Controls.Add(Me.btnNavPrev)
    Me.Panel1.Controls.Add(Me.btnNavNext)
    Me.Panel1.Controls.Add(Me.btnLast)
    Me.Panel1.Controls.Add(Me.btnAdd)
    Me.Panel1.Location = New System.Drawing.Point(120, 583)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(351, 40)
    Me.Panel1.TabIndex = 6
    '
    'btnAddCopy
    '
    Me.btnAddCopy.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnAddCopy.Location = New System.Drawing.Point(266, 5)
    Me.btnAddCopy.Name = "btnAddCopy"
    Me.btnAddCopy.Size = New System.Drawing.Size(75, 28)
    Me.btnAddCopy.TabIndex = 5
    Me.btnAddCopy.Text = "&New (Copy)"
    '
    'Label1
    '
    Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label1.Location = New System.Drawing.Point(8, 34)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(100, 17)
    Me.Label1.TabIndex = 46
    Me.Label1.Text = "Select"
    '
    'GroupBox1
    '
    Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.GroupBox1.Location = New System.Drawing.Point(8, 59)
    Me.GroupBox1.Name = "GroupBox1"
    Me.GroupBox1.Size = New System.Drawing.Size(576, 4)
    Me.GroupBox1.TabIndex = 77
    Me.GroupBox1.TabStop = False
    Me.GroupBox1.Text = "GroupBox1"
    '
    'btnClose
    '
    Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnClose.Location = New System.Drawing.Point(388, 633)
    Me.btnClose.Name = "btnClose"
    Me.btnClose.Size = New System.Drawing.Size(75, 28)
    Me.btnClose.TabIndex = 10
    Me.btnClose.Text = "&Close"
    '
    'edit_PFPCID
    '
    Me.edit_PFPCID.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.edit_PFPCID.Location = New System.Drawing.Point(114, 6)
    Me.edit_PFPCID.Name = "edit_PFPCID"
    Me.edit_PFPCID.Size = New System.Drawing.Size(172, 20)
    Me.edit_PFPCID.TabIndex = 0
    '
    'Label2
    '
    Me.Label2.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label2.Location = New System.Drawing.Point(6, 9)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(100, 16)
    Me.Label2.TabIndex = 79
    Me.Label2.Text = "Administrator ID"
    '
    'lblPertracIndex
    '
    Me.lblPertracIndex.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.lblPertracIndex.Location = New System.Drawing.Point(5, 55)
    Me.lblPertracIndex.Name = "lblPertracIndex"
    Me.lblPertracIndex.Size = New System.Drawing.Size(94, 20)
    Me.lblPertracIndex.TabIndex = 81
    Me.lblPertracIndex.Text = "Pertrac Fund"
    '
    'Combo_InstrumentType
    '
    Me.Combo_InstrumentType.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_InstrumentType.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_InstrumentType.Location = New System.Drawing.Point(114, 32)
    Me.Combo_InstrumentType.Name = "Combo_InstrumentType"
    Me.Combo_InstrumentType.Size = New System.Drawing.Size(172, 21)
    Me.Combo_InstrumentType.TabIndex = 2
    '
    'Label3
    '
    Me.Label3.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label3.Location = New System.Drawing.Point(6, 35)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(100, 20)
    Me.Label3.TabIndex = 83
    Me.Label3.Text = "Instrument Type"
    '
    'Combo_InstrumentParent
    '
    Me.Combo_InstrumentParent.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_InstrumentParent.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_InstrumentParent.Location = New System.Drawing.Point(112, 8)
    Me.Combo_InstrumentParent.Name = "Combo_InstrumentParent"
    Me.Combo_InstrumentParent.Size = New System.Drawing.Size(423, 21)
    Me.Combo_InstrumentParent.TabIndex = 0
    '
    'Label4
    '
    Me.Label4.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label4.Location = New System.Drawing.Point(6, 11)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(100, 20)
    Me.Label4.TabIndex = 85
    Me.Label4.Text = "Instrument Parent"
    '
    'edit_InstrumentClass
    '
    Me.edit_InstrumentClass.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.edit_InstrumentClass.Location = New System.Drawing.Point(120, 91)
    Me.edit_InstrumentClass.Name = "edit_InstrumentClass"
    Me.edit_InstrumentClass.Size = New System.Drawing.Size(282, 20)
    Me.edit_InstrumentClass.TabIndex = 0
    '
    'Label5
    '
    Me.Label5.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label5.Location = New System.Drawing.Point(8, 94)
    Me.Label5.Name = "Label5"
    Me.Label5.Size = New System.Drawing.Size(100, 16)
    Me.Label5.TabIndex = 87
    Me.Label5.Text = "Instrument Class"
    '
    'Combo_InstrumentFundType
    '
    Me.Combo_InstrumentFundType.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_InstrumentFundType.Location = New System.Drawing.Point(394, 33)
    Me.Combo_InstrumentFundType.Name = "Combo_InstrumentFundType"
    Me.Combo_InstrumentFundType.Size = New System.Drawing.Size(169, 21)
    Me.Combo_InstrumentFundType.TabIndex = 3
    '
    'Label6
    '
    Me.Label6.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label6.Location = New System.Drawing.Point(290, 35)
    Me.Label6.Name = "Label6"
    Me.Label6.Size = New System.Drawing.Size(91, 20)
    Me.Label6.TabIndex = 89
    Me.Label6.Text = "Fund Type"
    '
    'Combo_InstrumentCurrency
    '
    Me.Combo_InstrumentCurrency.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_InstrumentCurrency.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_InstrumentCurrency.Location = New System.Drawing.Point(394, 7)
    Me.Combo_InstrumentCurrency.Name = "Combo_InstrumentCurrency"
    Me.Combo_InstrumentCurrency.Size = New System.Drawing.Size(169, 21)
    Me.Combo_InstrumentCurrency.TabIndex = 1
    '
    'Label7
    '
    Me.Label7.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label7.Location = New System.Drawing.Point(290, 9)
    Me.Label7.Name = "Label7"
    Me.Label7.Size = New System.Drawing.Size(95, 20)
    Me.Label7.TabIndex = 91
    Me.Label7.Text = "Instrument Currency"
    Me.Label7.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'edit_ExpectedReturn
    '
    Me.edit_ExpectedReturn.AutoSize = False
    Me.edit_ExpectedReturn.DataType = GetType(Double)
    Me.edit_ExpectedReturn.DisableOnNoData = False
    Me.edit_ExpectedReturn.EditFormat.CustomFormat = "#####0.0#######"
    Me.edit_ExpectedReturn.EditFormat.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat
    Me.edit_ExpectedReturn.EditFormat.Inherit = CType((((C1.Win.C1Input.FormatInfoInheritFlags.NullText Or C1.Win.C1Input.FormatInfoInheritFlags.EmptyAsNull) _
                Or C1.Win.C1Input.FormatInfoInheritFlags.TrimStart) _
                Or C1.Win.C1Input.FormatInfoInheritFlags.TrimEnd), C1.Win.C1Input.FormatInfoInheritFlags)
    Me.edit_ExpectedReturn.ErrorInfo.ErrorAction = C1.Win.C1Input.ErrorActionEnum.ResetValue
    Me.edit_ExpectedReturn.ErrorInfo.ErrorMessage = "Validation Error"
    Me.edit_ExpectedReturn.ErrorInfo.ShowErrorMessage = False
    Me.edit_ExpectedReturn.FormatType = C1.Win.C1Input.FormatTypeEnum.Percent
    Me.edit_ExpectedReturn.Location = New System.Drawing.Point(117, 40)
    Me.edit_ExpectedReturn.MaxLength = 100
    Me.edit_ExpectedReturn.Name = "edit_ExpectedReturn"
    Me.edit_ExpectedReturn.ShowContextMenu = False
    Me.edit_ExpectedReturn.Size = New System.Drawing.Size(72, 20)
    Me.edit_ExpectedReturn.TabIndex = 1
    Me.edit_ExpectedReturn.Tag = Nothing
    Me.edit_ExpectedReturn.WordWrap = False
    '
    'lblVolatility
    '
    Me.lblVolatility.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.lblVolatility.Location = New System.Drawing.Point(9, 43)
    Me.lblVolatility.Name = "lblVolatility"
    Me.lblVolatility.Size = New System.Drawing.Size(106, 20)
    Me.lblVolatility.TabIndex = 93
    Me.lblVolatility.Text = "Expected Return"
    '
    'edit_WorstCaseFall
    '
    Me.edit_WorstCaseFall.AutoSize = False
    Me.edit_WorstCaseFall.DataType = GetType(Double)
    Me.edit_WorstCaseFall.DisableOnNoData = False
    Me.edit_WorstCaseFall.EditFormat.CustomFormat = "######0.0#######"
    Me.edit_WorstCaseFall.EditFormat.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat
    Me.edit_WorstCaseFall.EditFormat.Inherit = CType((((C1.Win.C1Input.FormatInfoInheritFlags.NullText Or C1.Win.C1Input.FormatInfoInheritFlags.EmptyAsNull) _
                Or C1.Win.C1Input.FormatInfoInheritFlags.TrimStart) _
                Or C1.Win.C1Input.FormatInfoInheritFlags.TrimEnd), C1.Win.C1Input.FormatInfoInheritFlags)
    Me.edit_WorstCaseFall.ErrorInfo.ErrorAction = C1.Win.C1Input.ErrorActionEnum.ResetValue
    Me.edit_WorstCaseFall.ErrorInfo.ErrorMessage = "Validation Error"
    Me.edit_WorstCaseFall.ErrorInfo.ShowErrorMessage = False
    Me.edit_WorstCaseFall.FormatType = C1.Win.C1Input.FormatTypeEnum.Percent
    Me.edit_WorstCaseFall.Location = New System.Drawing.Point(302, 40)
    Me.edit_WorstCaseFall.MaxLength = 100
    Me.edit_WorstCaseFall.Name = "edit_WorstCaseFall"
    Me.edit_WorstCaseFall.ShowContextMenu = False
    Me.edit_WorstCaseFall.Size = New System.Drawing.Size(72, 20)
    Me.edit_WorstCaseFall.TabIndex = 2
    Me.edit_WorstCaseFall.Tag = Nothing
    Me.edit_WorstCaseFall.WordWrap = False
    '
    'Label8
    '
    Me.Label8.Location = New System.Drawing.Point(201, 43)
    Me.Label8.Name = "Label8"
    Me.Label8.Size = New System.Drawing.Size(95, 20)
    Me.Label8.TabIndex = 95
    Me.Label8.Text = "Worst case fall"
    '
    'edit_DealingNoticeDays
    '
    Me.edit_DealingNoticeDays.AutoSize = False
    Me.edit_DealingNoticeDays.DataType = GetType(Integer)
    Me.edit_DealingNoticeDays.DisableOnNoData = False
    Me.edit_DealingNoticeDays.ErrorInfo.ErrorAction = C1.Win.C1Input.ErrorActionEnum.ResetValue
    Me.edit_DealingNoticeDays.ErrorInfo.ErrorMessage = "Validation Error"
    Me.edit_DealingNoticeDays.ErrorInfo.ShowErrorMessage = False
    Me.edit_DealingNoticeDays.FormatType = C1.Win.C1Input.FormatTypeEnum.[Integer]
    Me.edit_DealingNoticeDays.Location = New System.Drawing.Point(112, 37)
    Me.edit_DealingNoticeDays.MaxLength = 100
    Me.edit_DealingNoticeDays.Name = "edit_DealingNoticeDays"
    Me.edit_DealingNoticeDays.ShowContextMenu = False
    Me.edit_DealingNoticeDays.Size = New System.Drawing.Size(72, 20)
    Me.edit_DealingNoticeDays.TabIndex = 2
    Me.edit_DealingNoticeDays.Tag = Nothing
    Me.edit_DealingNoticeDays.WordWrap = False
    '
    'Label9
    '
    Me.Label9.Location = New System.Drawing.Point(2, 40)
    Me.Label9.Name = "Label9"
    Me.Label9.Size = New System.Drawing.Size(108, 20)
    Me.Label9.TabIndex = 97
    Me.Label9.Text = "Dealing Notice days"
    '
    'Check_LimitDisplay
    '
    Me.Check_LimitDisplay.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_LimitDisplay.Location = New System.Drawing.Point(127, 331)
    Me.Check_LimitDisplay.Name = "Check_LimitDisplay"
    Me.Check_LimitDisplay.Size = New System.Drawing.Size(16, 24)
    Me.Check_LimitDisplay.TabIndex = 3
    '
    'Label_AdminfundType
    '
    Me.Label_AdminfundType.Location = New System.Drawing.Point(15, 335)
    Me.Label_AdminfundType.Name = "Label_AdminfundType"
    Me.Label_AdminfundType.Size = New System.Drawing.Size(108, 16)
    Me.Label_AdminfundType.TabIndex = 14
    Me.Label_AdminfundType.Text = "Limit Display"
    '
    'Combo_DealingPeriod
    '
    Me.Combo_DealingPeriod.CausesValidation = False
    Me.Combo_DealingPeriod.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_DealingPeriod.Location = New System.Drawing.Point(112, 64)
    Me.Combo_DealingPeriod.Name = "Combo_DealingPeriod"
    Me.Combo_DealingPeriod.Size = New System.Drawing.Size(172, 21)
    Me.Combo_DealingPeriod.TabIndex = 3
    '
    'Label10
    '
    Me.Label10.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label10.Location = New System.Drawing.Point(4, 67)
    Me.Label10.Name = "Label10"
    Me.Label10.Size = New System.Drawing.Size(106, 20)
    Me.Label10.TabIndex = 101
    Me.Label10.Text = "Dealing Period"
    '
    'edit_DealingBaseDate
    '
    Me.edit_DealingBaseDate.AutoSize = False
    Me.edit_DealingBaseDate.DataType = GetType(Date)
    Me.edit_DealingBaseDate.DisableOnNoData = False
    Me.edit_DealingBaseDate.ErrorInfo.ErrorAction = C1.Win.C1Input.ErrorActionEnum.ResetValue
    Me.edit_DealingBaseDate.ErrorInfo.ErrorMessage = "Validation Error"
    Me.edit_DealingBaseDate.ErrorInfo.ShowErrorMessage = False
    Me.edit_DealingBaseDate.FormatType = C1.Win.C1Input.FormatTypeEnum.LongDate
    Me.edit_DealingBaseDate.Location = New System.Drawing.Point(392, 64)
    Me.edit_DealingBaseDate.MaxLength = 100
    Me.edit_DealingBaseDate.Name = "edit_DealingBaseDate"
    Me.edit_DealingBaseDate.ShowContextMenu = False
    Me.edit_DealingBaseDate.Size = New System.Drawing.Size(169, 20)
    Me.edit_DealingBaseDate.TabIndex = 4
    Me.edit_DealingBaseDate.Tag = Nothing
    Me.edit_DealingBaseDate.WordWrap = False
    '
    'Label11
    '
    Me.Label11.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label11.Location = New System.Drawing.Point(288, 67)
    Me.Label11.Name = "Label11"
    Me.Label11.Size = New System.Drawing.Size(95, 17)
    Me.Label11.TabIndex = 103
    Me.Label11.Text = "Dealing Base Date"
    '
    'edit_LockupPeriod
    '
    Me.edit_LockupPeriod.AutoSize = False
    Me.edit_LockupPeriod.DataType = GetType(Integer)
    Me.edit_LockupPeriod.DisableOnNoData = False
    Me.edit_LockupPeriod.ErrorInfo.ErrorAction = C1.Win.C1Input.ErrorActionEnum.ResetValue
    Me.edit_LockupPeriod.ErrorInfo.ErrorMessage = "Validation Error"
    Me.edit_LockupPeriod.ErrorInfo.ShowErrorMessage = False
    Me.edit_LockupPeriod.FormatType = C1.Win.C1Input.FormatTypeEnum.[Integer]
    Me.edit_LockupPeriod.Location = New System.Drawing.Point(112, 91)
    Me.edit_LockupPeriod.MaxLength = 100
    Me.edit_LockupPeriod.Name = "edit_LockupPeriod"
    Me.edit_LockupPeriod.ShowContextMenu = False
    Me.edit_LockupPeriod.Size = New System.Drawing.Size(72, 20)
    Me.edit_LockupPeriod.TabIndex = 5
    Me.edit_LockupPeriod.Tag = Nothing
    Me.edit_LockupPeriod.WordWrap = False
    '
    'Label12
    '
    Me.Label12.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label12.Location = New System.Drawing.Point(4, 94)
    Me.Label12.Name = "Label12"
    Me.Label12.Size = New System.Drawing.Size(102, 20)
    Me.Label12.TabIndex = 105
    Me.Label12.Text = "Lockup Period"
    '
    'edit_Penalty
    '
    Me.edit_Penalty.AutoSize = False
    Me.edit_Penalty.DataType = GetType(Double)
    Me.edit_Penalty.DisableOnNoData = False
    Me.edit_Penalty.EditFormat.CustomFormat = "######0.0#######"
    Me.edit_Penalty.EditFormat.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat
    Me.edit_Penalty.EditFormat.Inherit = CType((((C1.Win.C1Input.FormatInfoInheritFlags.NullText Or C1.Win.C1Input.FormatInfoInheritFlags.EmptyAsNull) _
                Or C1.Win.C1Input.FormatInfoInheritFlags.TrimStart) _
                Or C1.Win.C1Input.FormatInfoInheritFlags.TrimEnd), C1.Win.C1Input.FormatInfoInheritFlags)
    Me.edit_Penalty.ErrorInfo.ErrorAction = C1.Win.C1Input.ErrorActionEnum.ResetValue
    Me.edit_Penalty.ErrorInfo.ErrorMessage = "Validation Error"
    Me.edit_Penalty.ErrorInfo.ShowErrorMessage = False
    Me.edit_Penalty.FormatType = C1.Win.C1Input.FormatTypeEnum.Percent
    Me.edit_Penalty.Location = New System.Drawing.Point(111, 117)
    Me.edit_Penalty.MaxLength = 100
    Me.edit_Penalty.Name = "edit_Penalty"
    Me.edit_Penalty.ShowContextMenu = False
    Me.edit_Penalty.Size = New System.Drawing.Size(72, 20)
    Me.edit_Penalty.TabIndex = 8
    Me.edit_Penalty.Tag = Nothing
    Me.edit_Penalty.WordWrap = False
    '
    'Label13
    '
    Me.Label13.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label13.Location = New System.Drawing.Point(6, 120)
    Me.Label13.Name = "Label13"
    Me.Label13.Size = New System.Drawing.Size(106, 20)
    Me.Label13.TabIndex = 107
    Me.Label13.Text = "Penalty"
    '
    'Check_UpfrontFees
    '
    Me.Check_UpfrontFees.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_UpfrontFees.Location = New System.Drawing.Point(372, 91)
    Me.Check_UpfrontFees.Name = "Check_UpfrontFees"
    Me.Check_UpfrontFees.Size = New System.Drawing.Size(16, 24)
    Me.Check_UpfrontFees.TabIndex = 7
    '
    'Label14
    '
    Me.Label14.Location = New System.Drawing.Point(200, 95)
    Me.Label14.Name = "Label14"
    Me.Label14.Size = New System.Drawing.Size(168, 20)
    Me.Label14.TabIndex = 6
    Me.Label14.Text = "Has upfront Fees (Funds Only)"
    '
    'edit_LiquidityComment
    '
    Me.edit_LiquidityComment.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.edit_LiquidityComment.Location = New System.Drawing.Point(111, 147)
    Me.edit_LiquidityComment.Name = "edit_LiquidityComment"
    Me.edit_LiquidityComment.Size = New System.Drawing.Size(450, 20)
    Me.edit_LiquidityComment.TabIndex = 9
    '
    'Label15
    '
    Me.Label15.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label15.Location = New System.Drawing.Point(6, 150)
    Me.Label15.Name = "Label15"
    Me.Label15.Size = New System.Drawing.Size(100, 16)
    Me.Label15.TabIndex = 111
    Me.Label15.Text = "Liquidity Comment"
    '
    'edit_LatestTradeDate
    '
    Me.edit_LatestTradeDate.AutoSize = False
    Me.edit_LatestTradeDate.DataType = GetType(Date)
    Me.edit_LatestTradeDate.DisableOnNoData = False
    Me.edit_LatestTradeDate.ErrorInfo.ErrorAction = C1.Win.C1Input.ErrorActionEnum.ResetValue
    Me.edit_LatestTradeDate.ErrorInfo.ErrorMessage = "Validation Error"
    Me.edit_LatestTradeDate.ErrorInfo.ShowErrorMessage = False
    Me.edit_LatestTradeDate.FormatType = C1.Win.C1Input.FormatTypeEnum.LongDate
    Me.edit_LatestTradeDate.Location = New System.Drawing.Point(112, 66)
    Me.edit_LatestTradeDate.MaxLength = 100
    Me.edit_LatestTradeDate.Name = "edit_LatestTradeDate"
    Me.edit_LatestTradeDate.ShowContextMenu = False
    Me.edit_LatestTradeDate.Size = New System.Drawing.Size(168, 20)
    Me.edit_LatestTradeDate.TabIndex = 3
    Me.edit_LatestTradeDate.Tag = Nothing
    Me.edit_LatestTradeDate.WordWrap = False
    '
    'Label16
    '
    Me.Label16.Location = New System.Drawing.Point(4, 69)
    Me.Label16.Name = "Label16"
    Me.Label16.Size = New System.Drawing.Size(96, 20)
    Me.Label16.TabIndex = 113
    Me.Label16.Text = "Latest Trade Date"
    '
    'Check_ExcludeFromGAV
    '
    Me.Check_ExcludeFromGAV.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_ExcludeFromGAV.Location = New System.Drawing.Point(14, 258)
    Me.Check_ExcludeFromGAV.Name = "Check_ExcludeFromGAV"
    Me.Check_ExcludeFromGAV.Size = New System.Drawing.Size(248, 16)
    Me.Check_ExcludeFromGAV.TabIndex = 17
    Me.Check_ExcludeFromGAV.Text = "Exclude from GAV (Expense)"
    '
    'Check_IsManagementFee
    '
    Me.Check_IsManagementFee.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_IsManagementFee.Location = New System.Drawing.Point(14, 278)
    Me.Check_IsManagementFee.Name = "Check_IsManagementFee"
    Me.Check_IsManagementFee.Size = New System.Drawing.Size(248, 16)
    Me.Check_IsManagementFee.TabIndex = 18
    Me.Check_IsManagementFee.Text = "Is a Management Fee (ex GAV, inc GNAV)"
    '
    'Check_IsLeverageInstrument
    '
    Me.Check_IsLeverageInstrument.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_IsLeverageInstrument.Location = New System.Drawing.Point(3, 6)
    Me.Check_IsLeverageInstrument.Name = "Check_IsLeverageInstrument"
    Me.Check_IsLeverageInstrument.Size = New System.Drawing.Size(248, 16)
    Me.Check_IsLeverageInstrument.TabIndex = 35
    Me.Check_IsLeverageInstrument.Text = "Is a Leverage Instrument"
    '
    'Check_IsIncentiveFee
    '
    Me.Check_IsIncentiveFee.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_IsIncentiveFee.Location = New System.Drawing.Point(14, 298)
    Me.Check_IsIncentiveFee.Name = "Check_IsIncentiveFee"
    Me.Check_IsIncentiveFee.Size = New System.Drawing.Size(248, 16)
    Me.Check_IsIncentiveFee.TabIndex = 19
    Me.Check_IsIncentiveFee.Text = "Is an Incentive Fee (ex GNAV)"
    '
    'Combo_LeverageCounterparty
    '
    Me.Combo_LeverageCounterparty.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_LeverageCounterparty.Enabled = False
    Me.Combo_LeverageCounterparty.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_LeverageCounterparty.Location = New System.Drawing.Point(107, 28)
    Me.Combo_LeverageCounterparty.Name = "Combo_LeverageCounterparty"
    Me.Combo_LeverageCounterparty.Size = New System.Drawing.Size(456, 21)
    Me.Combo_LeverageCounterparty.TabIndex = 0
    '
    'Label17
    '
    Me.Label17.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label17.Location = New System.Drawing.Point(5, 28)
    Me.Label17.Name = "Label17"
    Me.Label17.Size = New System.Drawing.Size(94, 20)
    Me.Label17.TabIndex = 37
    Me.Label17.Text = "Leverage Cpty"
    '
    'Button_ShowParentHeirarchy
    '
    Me.Button_ShowParentHeirarchy.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Button_ShowParentHeirarchy.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Button_ShowParentHeirarchy.Location = New System.Drawing.Point(540, 7)
    Me.Button_ShowParentHeirarchy.Name = "Button_ShowParentHeirarchy"
    Me.Button_ShowParentHeirarchy.Size = New System.Drawing.Size(20, 21)
    Me.Button_ShowParentHeirarchy.TabIndex = 1
    Me.Button_ShowParentHeirarchy.Text = "..."
    '
    'Check_IsEqualisation
    '
    Me.Check_IsEqualisation.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_IsEqualisation.Location = New System.Drawing.Point(274, 278)
    Me.Check_IsEqualisation.Name = "Check_IsEqualisation"
    Me.Check_IsEqualisation.Size = New System.Drawing.Size(248, 16)
    Me.Check_IsEqualisation.TabIndex = 24
    Me.Check_IsEqualisation.Text = "Is an Equalisation Instrument"
    Me.Check_IsEqualisation.Visible = False
    '
    'edit_PortfolioTicker
    '
    Me.edit_PortfolioTicker.Location = New System.Drawing.Point(107, 81)
    Me.edit_PortfolioTicker.Name = "edit_PortfolioTicker"
    Me.edit_PortfolioTicker.Size = New System.Drawing.Size(172, 20)
    Me.edit_PortfolioTicker.TabIndex = 2
    '
    'Label18
    '
    Me.Label18.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label18.Location = New System.Drawing.Point(5, 84)
    Me.Label18.Name = "Label18"
    Me.Label18.Size = New System.Drawing.Size(94, 16)
    Me.Label18.TabIndex = 116
    Me.Label18.Text = "Portfolio Ticker"
    '
    'edit_ExchangeCode
    '
    Me.edit_ExchangeCode.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.edit_ExchangeCode.Location = New System.Drawing.Point(114, 144)
    Me.edit_ExchangeCode.Name = "edit_ExchangeCode"
    Me.edit_ExchangeCode.Size = New System.Drawing.Size(172, 20)
    Me.edit_ExchangeCode.TabIndex = 10
    '
    'Label19
    '
    Me.Label19.Location = New System.Drawing.Point(3, 148)
    Me.Label19.Name = "Label19"
    Me.Label19.Size = New System.Drawing.Size(100, 16)
    Me.Label19.TabIndex = 118
    Me.Label19.Text = "Bloomberg Code"
    '
    'RootMenu
    '
    Me.RootMenu.Location = New System.Drawing.Point(0, 0)
    Me.RootMenu.Name = "RootMenu"
    Me.RootMenu.Size = New System.Drawing.Size(592, 24)
    Me.RootMenu.TabIndex = 45
    Me.RootMenu.Text = " "
    '
    'Check_ExcludeFromAUM
    '
    Me.Check_ExcludeFromAUM.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_ExcludeFromAUM.Location = New System.Drawing.Point(274, 258)
    Me.Check_ExcludeFromAUM.Name = "Check_ExcludeFromAUM"
    Me.Check_ExcludeFromAUM.Size = New System.Drawing.Size(248, 16)
    Me.Check_ExcludeFromAUM.TabIndex = 23
    Me.Check_ExcludeFromAUM.Text = "Exclude from AUM"
    '
    'edit_SEDOL
    '
    Me.edit_SEDOL.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.edit_SEDOL.Location = New System.Drawing.Point(395, 169)
    Me.edit_SEDOL.Name = "edit_SEDOL"
    Me.edit_SEDOL.Size = New System.Drawing.Size(168, 20)
    Me.edit_SEDOL.TabIndex = 13
    '
    'Label20
    '
    Me.Label20.Location = New System.Drawing.Point(290, 173)
    Me.Label20.Name = "Label20"
    Me.Label20.Size = New System.Drawing.Size(100, 16)
    Me.Label20.TabIndex = 123
    Me.Label20.Text = "SEDOL"
    '
    'edit_ISIN
    '
    Me.edit_ISIN.Location = New System.Drawing.Point(114, 170)
    Me.edit_ISIN.Name = "edit_ISIN"
    Me.edit_ISIN.Size = New System.Drawing.Size(172, 20)
    Me.edit_ISIN.TabIndex = 12
    '
    'Label21
    '
    Me.Label21.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label21.Location = New System.Drawing.Point(6, 173)
    Me.Label21.Name = "Label21"
    Me.Label21.Size = New System.Drawing.Size(100, 16)
    Me.Label21.TabIndex = 122
    Me.Label21.Text = "ISIN"
    '
    'Combo_ReviewGroup
    '
    Me.Combo_ReviewGroup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_ReviewGroup.Enabled = False
    Me.Combo_ReviewGroup.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_ReviewGroup.Location = New System.Drawing.Point(111, 173)
    Me.Combo_ReviewGroup.Name = "Combo_ReviewGroup"
    Me.Combo_ReviewGroup.Size = New System.Drawing.Size(449, 21)
    Me.Combo_ReviewGroup.TabIndex = 10
    '
    'Label22
    '
    Me.Label22.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label22.Location = New System.Drawing.Point(3, 176)
    Me.Label22.Name = "Label22"
    Me.Label22.Size = New System.Drawing.Size(100, 20)
    Me.Label22.TabIndex = 124
    Me.Label22.Text = "Review Group"
    '
    'Label_Settlement
    '
    Me.Label_Settlement.Location = New System.Drawing.Point(4, 126)
    Me.Label_Settlement.Name = "Label_Settlement"
    Me.Label_Settlement.Size = New System.Drawing.Size(84, 16)
    Me.Label_Settlement.TabIndex = 126
    Me.Label_Settlement.Text = "Multiplier"
    '
    'edit_InstrumentMultiplier
    '
    Me.edit_InstrumentMultiplier.Location = New System.Drawing.Point(112, 123)
    Me.edit_InstrumentMultiplier.Name = "edit_InstrumentMultiplier"
    Me.edit_InstrumentMultiplier.RenaissanceTag = Nothing
    Me.edit_InstrumentMultiplier.SelectTextOnFocus = False
    Me.edit_InstrumentMultiplier.Size = New System.Drawing.Size(172, 20)
    Me.edit_InstrumentMultiplier.TabIndex = 2
    Me.edit_InstrumentMultiplier.Text = "0.0000"
    Me.edit_InstrumentMultiplier.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.edit_InstrumentMultiplier.TextFormat = "#,##0.0000####"
    Me.edit_InstrumentMultiplier.Value = 0
    '
    'Label23
    '
    Me.Label23.Location = New System.Drawing.Point(4, 99)
    Me.Label23.Name = "Label23"
    Me.Label23.Size = New System.Drawing.Size(98, 16)
    Me.Label23.TabIndex = 128
    Me.Label23.Text = "Contract Size"
    '
    'edit_ContractSize
    '
    Me.edit_ContractSize.Location = New System.Drawing.Point(112, 97)
    Me.edit_ContractSize.Name = "edit_ContractSize"
    Me.edit_ContractSize.RenaissanceTag = Nothing
    Me.edit_ContractSize.SelectTextOnFocus = False
    Me.edit_ContractSize.Size = New System.Drawing.Size(172, 20)
    Me.edit_ContractSize.TabIndex = 1
    Me.edit_ContractSize.Text = "0.0000"
    Me.edit_ContractSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.edit_ContractSize.TextFormat = "#,##0.0000####"
    Me.edit_ContractSize.Value = 0
    '
    'edit_ExpiryDate
    '
    Me.edit_ExpiryDate.AutoSize = False
    Me.edit_ExpiryDate.DataType = GetType(Date)
    Me.edit_ExpiryDate.DisableOnNoData = False
    Me.edit_ExpiryDate.ErrorInfo.ErrorAction = C1.Win.C1Input.ErrorActionEnum.ResetValue
    Me.edit_ExpiryDate.ErrorInfo.ErrorMessage = "Validation Error"
    Me.edit_ExpiryDate.ErrorInfo.ShowErrorMessage = False
    Me.edit_ExpiryDate.FormatType = C1.Win.C1Input.FormatTypeEnum.LongDate
    Me.edit_ExpiryDate.Location = New System.Drawing.Point(112, 40)
    Me.edit_ExpiryDate.MaxLength = 100
    Me.edit_ExpiryDate.Name = "edit_ExpiryDate"
    Me.edit_ExpiryDate.ShowContextMenu = False
    Me.edit_ExpiryDate.Size = New System.Drawing.Size(168, 20)
    Me.edit_ExpiryDate.TabIndex = 4
    Me.edit_ExpiryDate.Tag = Nothing
    Me.edit_ExpiryDate.WordWrap = False
    '
    'Combo_InstrumentUnderlying
    '
    Me.Combo_InstrumentUnderlying.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_InstrumentUnderlying.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_InstrumentUnderlying.Location = New System.Drawing.Point(112, 8)
    Me.Combo_InstrumentUnderlying.Name = "Combo_InstrumentUnderlying"
    Me.Combo_InstrumentUnderlying.Size = New System.Drawing.Size(450, 21)
    Me.Combo_InstrumentUnderlying.TabIndex = 0
    '
    'Tab_InstrumentDetails
    '
    Me.Tab_InstrumentDetails.Controls.Add(Me.Tab_BasicDetails)
    Me.Tab_InstrumentDetails.Controls.Add(Me.Tab_ManagerNotes)
    Me.Tab_InstrumentDetails.Controls.Add(Me.Tab_Unit)
    Me.Tab_InstrumentDetails.Controls.Add(Me.Tab_Derivatives)
    Me.Tab_InstrumentDetails.Controls.Add(Me.Tab_Bonds)
    Me.Tab_InstrumentDetails.Controls.Add(Me.Tab_Convertibles)
    Me.Tab_InstrumentDetails.Controls.Add(Me.Tab_Advanced)
    Me.Tab_InstrumentDetails.Controls.Add(Me.Tab_Performance)
    Me.Tab_InstrumentDetails.Controls.Add(Me.Tab_Legacy)
    Me.Tab_InstrumentDetails.Location = New System.Drawing.Point(8, 150)
    Me.Tab_InstrumentDetails.Name = "Tab_InstrumentDetails"
    Me.Tab_InstrumentDetails.SelectedIndex = 0
    Me.Tab_InstrumentDetails.Size = New System.Drawing.Size(577, 421)
    Me.Tab_InstrumentDetails.TabIndex = 5
    '
    'Tab_BasicDetails
    '
    Me.Tab_BasicDetails.BackColor = System.Drawing.Color.Transparent
    Me.Tab_BasicDetails.Controls.Add(Me.Check_IsMovementCommission)
    Me.Tab_BasicDetails.Controls.Add(Me.Label57)
    Me.Tab_BasicDetails.Controls.Add(Me.edit_ISO2_CountryCode)
    Me.Tab_BasicDetails.Controls.Add(Me.Label56)
    Me.Tab_BasicDetails.Controls.Add(Me.Check_IsCrystalisedIncentiveFee)
    Me.Tab_BasicDetails.Controls.Add(Me.Label48)
    Me.Tab_BasicDetails.Controls.Add(Me.Combo_AttributionSector)
    Me.Tab_BasicDetails.Controls.Add(Me.Label47)
    Me.Tab_BasicDetails.Controls.Add(Me.Combo_AttributionMarket)
    Me.Tab_BasicDetails.Controls.Add(Me.Check_TradeAsValue)
    Me.Tab_BasicDetails.Controls.Add(Me.Label33)
    Me.Tab_BasicDetails.Controls.Add(Me.Date_Cutoff)
    Me.Tab_BasicDetails.Controls.Add(Me.edit_MaxDecimalPlaces)
    Me.Tab_BasicDetails.Controls.Add(Me.Label32)
    Me.Tab_BasicDetails.Controls.Add(Me.Check_PriceCapture)
    Me.Tab_BasicDetails.Controls.Add(Me.edit_Settlemet_Sell)
    Me.Tab_BasicDetails.Controls.Add(Me.Label30)
    Me.Tab_BasicDetails.Controls.Add(Me.edit_Settlemet_Buy)
    Me.Tab_BasicDetails.Controls.Add(Me.Label29)
    Me.Tab_BasicDetails.Controls.Add(Me.edit_Morningstar)
    Me.Tab_BasicDetails.Controls.Add(Me.Label28)
    Me.Tab_BasicDetails.Controls.Add(Me.edit_PFPCID)
    Me.Tab_BasicDetails.Controls.Add(Me.Label2)
    Me.Tab_BasicDetails.Controls.Add(Me.Label3)
    Me.Tab_BasicDetails.Controls.Add(Me.edit_SEDOL)
    Me.Tab_BasicDetails.Controls.Add(Me.Combo_InstrumentType)
    Me.Tab_BasicDetails.Controls.Add(Me.Label20)
    Me.Tab_BasicDetails.Controls.Add(Me.edit_ISIN)
    Me.Tab_BasicDetails.Controls.Add(Me.Label21)
    Me.Tab_BasicDetails.Controls.Add(Me.Label6)
    Me.Tab_BasicDetails.Controls.Add(Me.Check_ExcludeFromAUM)
    Me.Tab_BasicDetails.Controls.Add(Me.Combo_InstrumentFundType)
    Me.Tab_BasicDetails.Controls.Add(Me.Label7)
    Me.Tab_BasicDetails.Controls.Add(Me.edit_ExchangeCode)
    Me.Tab_BasicDetails.Controls.Add(Me.Combo_InstrumentCurrency)
    Me.Tab_BasicDetails.Controls.Add(Me.Label19)
    Me.Tab_BasicDetails.Controls.Add(Me.Check_IsEqualisation)
    Me.Tab_BasicDetails.Controls.Add(Me.Check_IsIncentiveFee)
    Me.Tab_BasicDetails.Controls.Add(Me.Check_IsManagementFee)
    Me.Tab_BasicDetails.Controls.Add(Me.Check_ExcludeFromGAV)
    Me.Tab_BasicDetails.Location = New System.Drawing.Point(4, 22)
    Me.Tab_BasicDetails.Name = "Tab_BasicDetails"
    Me.Tab_BasicDetails.Padding = New System.Windows.Forms.Padding(3)
    Me.Tab_BasicDetails.Size = New System.Drawing.Size(569, 395)
    Me.Tab_BasicDetails.TabIndex = 0
    Me.Tab_BasicDetails.Text = "Standard Details"
    Me.Tab_BasicDetails.UseVisualStyleBackColor = True
    '
    'Check_IsMovementCommission
    '
    Me.Check_IsMovementCommission.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_IsMovementCommission.Location = New System.Drawing.Point(14, 338)
    Me.Check_IsMovementCommission.Name = "Check_IsMovementCommission"
    Me.Check_IsMovementCommission.Size = New System.Drawing.Size(248, 16)
    Me.Check_IsMovementCommission.TabIndex = 21
    Me.Check_IsMovementCommission.Text = "Is a Movement Commission"
    '
    'Label57
    '
    Me.Label57.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label57.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label57.Location = New System.Drawing.Point(176, 226)
    Me.Label57.Name = "Label57"
    Me.Label57.Size = New System.Drawing.Size(110, 16)
    Me.Label57.TabIndex = 144
    Me.Label57.Text = "For Administrator files"
    '
    'edit_ISO2_CountryCode
    '
    Me.edit_ISO2_CountryCode.Location = New System.Drawing.Point(114, 223)
    Me.edit_ISO2_CountryCode.Name = "edit_ISO2_CountryCode"
    Me.edit_ISO2_CountryCode.Size = New System.Drawing.Size(56, 20)
    Me.edit_ISO2_CountryCode.TabIndex = 16
    '
    'Label56
    '
    Me.Label56.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label56.Location = New System.Drawing.Point(6, 226)
    Me.Label56.Name = "Label56"
    Me.Label56.Size = New System.Drawing.Size(100, 16)
    Me.Label56.TabIndex = 143
    Me.Label56.Text = "ISO2 Country Code"
    '
    'Check_IsCrystalisedIncentiveFee
    '
    Me.Check_IsCrystalisedIncentiveFee.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_IsCrystalisedIncentiveFee.Location = New System.Drawing.Point(14, 318)
    Me.Check_IsCrystalisedIncentiveFee.Name = "Check_IsCrystalisedIncentiveFee"
    Me.Check_IsCrystalisedIncentiveFee.Size = New System.Drawing.Size(248, 16)
    Me.Check_IsCrystalisedIncentiveFee.TabIndex = 20
    Me.Check_IsCrystalisedIncentiveFee.Text = "Is a Crystalised Incentive Fee (ex GNAV)"
    '
    'Label48
    '
    Me.Label48.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label48.Location = New System.Drawing.Point(290, 198)
    Me.Label48.Name = "Label48"
    Me.Label48.Size = New System.Drawing.Size(100, 20)
    Me.Label48.TabIndex = 140
    Me.Label48.Text = "Attribution Sector"
    '
    'Combo_AttributionSector
    '
    Me.Combo_AttributionSector.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_AttributionSector.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_AttributionSector.Location = New System.Drawing.Point(394, 195)
    Me.Combo_AttributionSector.Name = "Combo_AttributionSector"
    Me.Combo_AttributionSector.Size = New System.Drawing.Size(169, 21)
    Me.Combo_AttributionSector.TabIndex = 15
    '
    'Label47
    '
    Me.Label47.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label47.Location = New System.Drawing.Point(6, 199)
    Me.Label47.Name = "Label47"
    Me.Label47.Size = New System.Drawing.Size(100, 20)
    Me.Label47.TabIndex = 138
    Me.Label47.Text = "Attribution Market"
    '
    'Combo_AttributionMarket
    '
    Me.Combo_AttributionMarket.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_AttributionMarket.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_AttributionMarket.Location = New System.Drawing.Point(114, 196)
    Me.Combo_AttributionMarket.Name = "Combo_AttributionMarket"
    Me.Combo_AttributionMarket.Size = New System.Drawing.Size(172, 21)
    Me.Combo_AttributionMarket.TabIndex = 14
    '
    'Check_TradeAsValue
    '
    Me.Check_TradeAsValue.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_TradeAsValue.Location = New System.Drawing.Point(114, 118)
    Me.Check_TradeAsValue.Name = "Check_TradeAsValue"
    Me.Check_TradeAsValue.Size = New System.Drawing.Size(248, 16)
    Me.Check_TradeAsValue.TabIndex = 9
    Me.Check_TradeAsValue.Text = "Trade only as 'Value'"
    '
    'Label33
    '
    Me.Label33.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label33.Location = New System.Drawing.Point(192, 95)
    Me.Label33.Name = "Label33"
    Me.Label33.Size = New System.Drawing.Size(106, 17)
    Me.Label33.TabIndex = 136
    Me.Label33.Text = "Trade cutoff time"
    '
    'Date_Cutoff
    '
    Me.Date_Cutoff.CausesValidation = False
    Me.Date_Cutoff.CustomFormat = ""
    Me.Date_Cutoff.Format = System.Windows.Forms.DateTimePickerFormat.Time
    Me.Date_Cutoff.Location = New System.Drawing.Point(299, 92)
    Me.Date_Cutoff.MaxDate = New Date(1900, 1, 1, 23, 59, 59, 0)
    Me.Date_Cutoff.MinDate = New Date(1900, 1, 1, 0, 0, 0, 0)
    Me.Date_Cutoff.Name = "Date_Cutoff"
    Me.Date_Cutoff.Size = New System.Drawing.Size(91, 20)
    Me.Date_Cutoff.TabIndex = 8
    Me.Date_Cutoff.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
    '
    'edit_MaxDecimalPlaces
    '
    Me.edit_MaxDecimalPlaces.AutoSize = False
    Me.edit_MaxDecimalPlaces.DataType = GetType(Double)
    Me.edit_MaxDecimalPlaces.DisableOnNoData = False
    Me.edit_MaxDecimalPlaces.DisplayFormat.CustomFormat = "#######0"
    Me.edit_MaxDecimalPlaces.DisplayFormat.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat
    Me.edit_MaxDecimalPlaces.DisplayFormat.Inherit = CType(((C1.Win.C1Input.FormatInfoInheritFlags.EmptyAsNull Or C1.Win.C1Input.FormatInfoInheritFlags.TrimStart) _
                Or C1.Win.C1Input.FormatInfoInheritFlags.TrimEnd), C1.Win.C1Input.FormatInfoInheritFlags)
    Me.edit_MaxDecimalPlaces.DisplayFormat.NullText = "0"
    Me.edit_MaxDecimalPlaces.EditFormat.FormatType = C1.Win.C1Input.FormatTypeEnum.[Integer]
    Me.edit_MaxDecimalPlaces.EditFormat.Inherit = CType((((C1.Win.C1Input.FormatInfoInheritFlags.NullText Or C1.Win.C1Input.FormatInfoInheritFlags.EmptyAsNull) _
                Or C1.Win.C1Input.FormatInfoInheritFlags.TrimStart) _
                Or C1.Win.C1Input.FormatInfoInheritFlags.TrimEnd), C1.Win.C1Input.FormatInfoInheritFlags)
    Me.edit_MaxDecimalPlaces.ErrorInfo.ErrorAction = C1.Win.C1Input.ErrorActionEnum.ResetValue
    Me.edit_MaxDecimalPlaces.ErrorInfo.ErrorMessage = "Validation Error"
    Me.edit_MaxDecimalPlaces.ErrorInfo.ShowErrorMessage = False
    Me.edit_MaxDecimalPlaces.FormatType = C1.Win.C1Input.FormatTypeEnum.Percent
    Me.edit_MaxDecimalPlaces.Location = New System.Drawing.Point(114, 92)
    Me.edit_MaxDecimalPlaces.MaxLength = 100
    Me.edit_MaxDecimalPlaces.Name = "edit_MaxDecimalPlaces"
    Me.edit_MaxDecimalPlaces.ShowContextMenu = False
    Me.edit_MaxDecimalPlaces.Size = New System.Drawing.Size(72, 20)
    Me.edit_MaxDecimalPlaces.TabIndex = 7
    Me.edit_MaxDecimalPlaces.Tag = Nothing
    Me.edit_MaxDecimalPlaces.WordWrap = False
    '
    'Label32
    '
    Me.Label32.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label32.Location = New System.Drawing.Point(6, 95)
    Me.Label32.Name = "Label32"
    Me.Label32.Size = New System.Drawing.Size(106, 17)
    Me.Label32.TabIndex = 135
    Me.Label32.Text = "Max Order decimals"
    '
    'Check_PriceCapture
    '
    Me.Check_PriceCapture.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_PriceCapture.Location = New System.Drawing.Point(14, 362)
    Me.Check_PriceCapture.Name = "Check_PriceCapture"
    Me.Check_PriceCapture.Size = New System.Drawing.Size(248, 16)
    Me.Check_PriceCapture.TabIndex = 22
    Me.Check_PriceCapture.Text = "Include in automatic price capture process."
    '
    'edit_Settlemet_Sell
    '
    Me.edit_Settlemet_Sell.AutoSize = False
    Me.edit_Settlemet_Sell.DataType = GetType(Double)
    Me.edit_Settlemet_Sell.DisableOnNoData = False
    Me.edit_Settlemet_Sell.DisplayFormat.CustomFormat = "#######0"
    Me.edit_Settlemet_Sell.DisplayFormat.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat
    Me.edit_Settlemet_Sell.DisplayFormat.Inherit = CType(((C1.Win.C1Input.FormatInfoInheritFlags.EmptyAsNull Or C1.Win.C1Input.FormatInfoInheritFlags.TrimStart) _
                Or C1.Win.C1Input.FormatInfoInheritFlags.TrimEnd), C1.Win.C1Input.FormatInfoInheritFlags)
    Me.edit_Settlemet_Sell.DisplayFormat.NullText = "0"
    Me.edit_Settlemet_Sell.EditFormat.FormatType = C1.Win.C1Input.FormatTypeEnum.[Integer]
    Me.edit_Settlemet_Sell.EditFormat.Inherit = CType((((C1.Win.C1Input.FormatInfoInheritFlags.NullText Or C1.Win.C1Input.FormatInfoInheritFlags.EmptyAsNull) _
                Or C1.Win.C1Input.FormatInfoInheritFlags.TrimStart) _
                Or C1.Win.C1Input.FormatInfoInheritFlags.TrimEnd), C1.Win.C1Input.FormatInfoInheritFlags)
    Me.edit_Settlemet_Sell.ErrorInfo.ErrorAction = C1.Win.C1Input.ErrorActionEnum.ResetValue
    Me.edit_Settlemet_Sell.ErrorInfo.ErrorMessage = "Validation Error"
    Me.edit_Settlemet_Sell.ErrorInfo.ShowErrorMessage = False
    Me.edit_Settlemet_Sell.FormatType = C1.Win.C1Input.FormatTypeEnum.Percent
    Me.edit_Settlemet_Sell.Location = New System.Drawing.Point(299, 66)
    Me.edit_Settlemet_Sell.MaxLength = 100
    Me.edit_Settlemet_Sell.Name = "edit_Settlemet_Sell"
    Me.edit_Settlemet_Sell.ShowContextMenu = False
    Me.edit_Settlemet_Sell.Size = New System.Drawing.Size(72, 20)
    Me.edit_Settlemet_Sell.TabIndex = 6
    Me.edit_Settlemet_Sell.Tag = Nothing
    Me.edit_Settlemet_Sell.WordWrap = False
    '
    'Label30
    '
    Me.Label30.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label30.Location = New System.Drawing.Point(191, 69)
    Me.Label30.Name = "Label30"
    Me.Label30.Size = New System.Drawing.Size(106, 17)
    Me.Label30.TabIndex = 130
    Me.Label30.Text = "Settlement (Sell)"
    '
    'edit_Settlemet_Buy
    '
    Me.edit_Settlemet_Buy.AutoSize = False
    Me.edit_Settlemet_Buy.DataType = GetType(Double)
    Me.edit_Settlemet_Buy.DisableOnNoData = False
    Me.edit_Settlemet_Buy.DisplayFormat.CustomFormat = "#######0"
    Me.edit_Settlemet_Buy.DisplayFormat.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat
    Me.edit_Settlemet_Buy.DisplayFormat.Inherit = CType(((C1.Win.C1Input.FormatInfoInheritFlags.EmptyAsNull Or C1.Win.C1Input.FormatInfoInheritFlags.TrimStart) _
                Or C1.Win.C1Input.FormatInfoInheritFlags.TrimEnd), C1.Win.C1Input.FormatInfoInheritFlags)
    Me.edit_Settlemet_Buy.DisplayFormat.NullText = "0"
    Me.edit_Settlemet_Buy.EditFormat.FormatType = C1.Win.C1Input.FormatTypeEnum.[Integer]
    Me.edit_Settlemet_Buy.EditFormat.Inherit = CType((((C1.Win.C1Input.FormatInfoInheritFlags.NullText Or C1.Win.C1Input.FormatInfoInheritFlags.EmptyAsNull) _
                Or C1.Win.C1Input.FormatInfoInheritFlags.TrimStart) _
                Or C1.Win.C1Input.FormatInfoInheritFlags.TrimEnd), C1.Win.C1Input.FormatInfoInheritFlags)
    Me.edit_Settlemet_Buy.ErrorInfo.ErrorAction = C1.Win.C1Input.ErrorActionEnum.ResetValue
    Me.edit_Settlemet_Buy.ErrorInfo.ErrorMessage = "Validation Error"
    Me.edit_Settlemet_Buy.ErrorInfo.ShowErrorMessage = False
    Me.edit_Settlemet_Buy.FormatType = C1.Win.C1Input.FormatTypeEnum.Percent
    Me.edit_Settlemet_Buy.Location = New System.Drawing.Point(114, 66)
    Me.edit_Settlemet_Buy.MaxLength = 100
    Me.edit_Settlemet_Buy.Name = "edit_Settlemet_Buy"
    Me.edit_Settlemet_Buy.ShowContextMenu = False
    Me.edit_Settlemet_Buy.Size = New System.Drawing.Size(72, 20)
    Me.edit_Settlemet_Buy.TabIndex = 5
    Me.edit_Settlemet_Buy.Tag = Nothing
    Me.edit_Settlemet_Buy.WordWrap = False
    '
    'Label29
    '
    Me.Label29.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label29.Location = New System.Drawing.Point(6, 69)
    Me.Label29.Name = "Label29"
    Me.Label29.Size = New System.Drawing.Size(106, 17)
    Me.Label29.TabIndex = 128
    Me.Label29.Text = "Settlement (Buy)"
    '
    'edit_Morningstar
    '
    Me.edit_Morningstar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.edit_Morningstar.Location = New System.Drawing.Point(395, 143)
    Me.edit_Morningstar.Name = "edit_Morningstar"
    Me.edit_Morningstar.Size = New System.Drawing.Size(168, 20)
    Me.edit_Morningstar.TabIndex = 11
    '
    'Label28
    '
    Me.Label28.Location = New System.Drawing.Point(290, 147)
    Me.Label28.Name = "Label28"
    Me.Label28.Size = New System.Drawing.Size(100, 16)
    Me.Label28.TabIndex = 126
    Me.Label28.Text = "Morningstar"
    '
    'Tab_ManagerNotes
    '
    Me.Tab_ManagerNotes.Controls.Add(Me.Label61)
    Me.Tab_ManagerNotes.Controls.Add(Me.Text_ManagerNotes)
    Me.Tab_ManagerNotes.Location = New System.Drawing.Point(4, 22)
    Me.Tab_ManagerNotes.Name = "Tab_ManagerNotes"
    Me.Tab_ManagerNotes.Size = New System.Drawing.Size(569, 395)
    Me.Tab_ManagerNotes.TabIndex = 8
    Me.Tab_ManagerNotes.Text = "Notes"
    Me.Tab_ManagerNotes.UseVisualStyleBackColor = True
    '
    'Label61
    '
    Me.Label61.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label61.Location = New System.Drawing.Point(9, 12)
    Me.Label61.Name = "Label61"
    Me.Label61.Size = New System.Drawing.Size(101, 17)
    Me.Label61.TabIndex = 154
    Me.Label61.Text = "Manager Notes"
    '
    'Text_ManagerNotes
    '
    Me.Text_ManagerNotes.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Text_ManagerNotes.Location = New System.Drawing.Point(118, 8)
    Me.Text_ManagerNotes.Multiline = True
    Me.Text_ManagerNotes.Name = "Text_ManagerNotes"
    Me.Text_ManagerNotes.Size = New System.Drawing.Size(446, 384)
    Me.Text_ManagerNotes.TabIndex = 153
    '
    'Tab_Unit
    '
    Me.Tab_Unit.Controls.Add(Me.Label58)
    Me.Tab_Unit.Controls.Add(Me.Text_ProspectusNotes)
    Me.Tab_Unit.Controls.Add(Me.Label50)
    Me.Tab_Unit.Controls.Add(Me.Combo_PerformanceVsBenchmark)
    Me.Tab_Unit.Controls.Add(Me.Label51)
    Me.Tab_Unit.Controls.Add(Me.edit_FundManagementFees)
    Me.Tab_Unit.Controls.Add(Me.Label52)
    Me.Tab_Unit.Controls.Add(Me.edit_FundPerformanceFees)
    Me.Tab_Unit.Controls.Add(Me.Label53)
    Me.Tab_Unit.Controls.Add(Me.edit_PerformanceHurdle)
    Me.Tab_Unit.Controls.Add(Me.Label_FundID)
    Me.Tab_Unit.Controls.Add(Me.Combo_FundID)
    Me.Tab_Unit.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Unit.Name = "Tab_Unit"
    Me.Tab_Unit.Size = New System.Drawing.Size(569, 395)
    Me.Tab_Unit.TabIndex = 7
    Me.Tab_Unit.Text = "Fund Unit"
    Me.Tab_Unit.UseVisualStyleBackColor = True
    '
    'Label58
    '
    Me.Label58.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label58.Location = New System.Drawing.Point(6, 170)
    Me.Label58.Name = "Label58"
    Me.Label58.Size = New System.Drawing.Size(101, 17)
    Me.Label58.TabIndex = 152
    Me.Label58.Text = "Prospectus Notes"
    '
    'Text_ProspectusNotes
    '
    Me.Text_ProspectusNotes.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Text_ProspectusNotes.Location = New System.Drawing.Point(115, 166)
    Me.Text_ProspectusNotes.Multiline = True
    Me.Text_ProspectusNotes.Name = "Text_ProspectusNotes"
    Me.Text_ProspectusNotes.Size = New System.Drawing.Size(446, 222)
    Me.Text_ProspectusNotes.TabIndex = 5
    '
    'Label50
    '
    Me.Label50.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label50.Location = New System.Drawing.Point(6, 116)
    Me.Label50.Name = "Label50"
    Me.Label50.Size = New System.Drawing.Size(101, 17)
    Me.Label50.TabIndex = 150
    Me.Label50.Text = "vs Benchmark"
    '
    'Combo_PerformanceVsBenchmark
    '
    Me.Combo_PerformanceVsBenchmark.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_PerformanceVsBenchmark.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_PerformanceVsBenchmark.Location = New System.Drawing.Point(113, 113)
    Me.Combo_PerformanceVsBenchmark.Name = "Combo_PerformanceVsBenchmark"
    Me.Combo_PerformanceVsBenchmark.Size = New System.Drawing.Size(449, 21)
    Me.Combo_PerformanceVsBenchmark.TabIndex = 3
    '
    'Label51
    '
    Me.Label51.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label51.Location = New System.Drawing.Point(6, 55)
    Me.Label51.Name = "Label51"
    Me.Label51.Size = New System.Drawing.Size(101, 17)
    Me.Label51.TabIndex = 149
    Me.Label51.Text = "Management Fees"
    '
    'edit_FundManagementFees
    '
    Me.edit_FundManagementFees.Location = New System.Drawing.Point(113, 52)
    Me.edit_FundManagementFees.Name = "edit_FundManagementFees"
    Me.edit_FundManagementFees.RenaissanceTag = Nothing
    Me.edit_FundManagementFees.SelectTextOnFocus = False
    Me.edit_FundManagementFees.Size = New System.Drawing.Size(84, 20)
    Me.edit_FundManagementFees.TabIndex = 1
    Me.edit_FundManagementFees.Text = "0.00%"
    Me.edit_FundManagementFees.TextFormat = "#,##0.00%"
    Me.edit_FundManagementFees.Value = 0
    '
    'Label52
    '
    Me.Label52.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label52.Location = New System.Drawing.Point(6, 90)
    Me.Label52.Name = "Label52"
    Me.Label52.Size = New System.Drawing.Size(101, 17)
    Me.Label52.TabIndex = 147
    Me.Label52.Text = "Performance Fees"
    '
    'edit_FundPerformanceFees
    '
    Me.edit_FundPerformanceFees.Location = New System.Drawing.Point(113, 87)
    Me.edit_FundPerformanceFees.Name = "edit_FundPerformanceFees"
    Me.edit_FundPerformanceFees.RenaissanceTag = Nothing
    Me.edit_FundPerformanceFees.SelectTextOnFocus = False
    Me.edit_FundPerformanceFees.Size = New System.Drawing.Size(84, 20)
    Me.edit_FundPerformanceFees.TabIndex = 2
    Me.edit_FundPerformanceFees.Text = "0.00%"
    Me.edit_FundPerformanceFees.TextFormat = "#,##0.00%"
    Me.edit_FundPerformanceFees.Value = 0
    '
    'Label53
    '
    Me.Label53.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label53.Location = New System.Drawing.Point(6, 143)
    Me.Label53.Name = "Label53"
    Me.Label53.Size = New System.Drawing.Size(101, 17)
    Me.Label53.TabIndex = 148
    Me.Label53.Text = "Performance Hurdle"
    '
    'edit_PerformanceHurdle
    '
    Me.edit_PerformanceHurdle.Location = New System.Drawing.Point(113, 140)
    Me.edit_PerformanceHurdle.Name = "edit_PerformanceHurdle"
    Me.edit_PerformanceHurdle.RenaissanceTag = Nothing
    Me.edit_PerformanceHurdle.SelectTextOnFocus = False
    Me.edit_PerformanceHurdle.Size = New System.Drawing.Size(84, 20)
    Me.edit_PerformanceHurdle.TabIndex = 4
    Me.edit_PerformanceHurdle.Text = "0.00%"
    Me.edit_PerformanceHurdle.TextFormat = "#,##0.00%"
    Me.edit_PerformanceHurdle.Value = 0
    '
    'Label_FundID
    '
    Me.Label_FundID.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label_FundID.Location = New System.Drawing.Point(6, 19)
    Me.Label_FundID.Name = "Label_FundID"
    Me.Label_FundID.Size = New System.Drawing.Size(95, 20)
    Me.Label_FundID.TabIndex = 142
    Me.Label_FundID.Text = "Unit for Fund :"
    '
    'Combo_FundID
    '
    Me.Combo_FundID.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_FundID.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_FundID.Location = New System.Drawing.Point(113, 16)
    Me.Combo_FundID.Name = "Combo_FundID"
    Me.Combo_FundID.Size = New System.Drawing.Size(449, 21)
    Me.Combo_FundID.TabIndex = 0
    '
    'Tab_Derivatives
    '
    Me.Tab_Derivatives.Controls.Add(Me.Label24)
    Me.Tab_Derivatives.Controls.Add(Me.Label25)
    Me.Tab_Derivatives.Controls.Add(Me.Label_FixedCurrencyNote)
    Me.Tab_Derivatives.Controls.Add(Me.Combo_FXFixedCurrency)
    Me.Tab_Derivatives.Controls.Add(Me.Label_IsFxFuture)
    Me.Tab_Derivatives.Controls.Add(Me.Check_IsFXFuture)
    Me.Tab_Derivatives.Controls.Add(Me.Label_SharesPerOptionComment)
    Me.Tab_Derivatives.Controls.Add(Me.Label60)
    Me.Tab_Derivatives.Controls.Add(Me.Label59)
    Me.Tab_Derivatives.Controls.Add(Me.edit_SharesPerOption)
    Me.Tab_Derivatives.Controls.Add(Me.Label_FixedCurrency)
    Me.Tab_Derivatives.Controls.Add(Me.Label_SharesPerOption)
    Me.Tab_Derivatives.Controls.Add(Me.Combo_InstrumentUnderlying)
    Me.Tab_Derivatives.Controls.Add(Me.edit_InstrumentMultiplier)
    Me.Tab_Derivatives.Controls.Add(Me.Label_Settlement)
    Me.Tab_Derivatives.Controls.Add(Me.Label16)
    Me.Tab_Derivatives.Controls.Add(Me.edit_ContractSize)
    Me.Tab_Derivatives.Controls.Add(Me.Label23)
    Me.Tab_Derivatives.Controls.Add(Me.edit_ExpiryDate)
    Me.Tab_Derivatives.Controls.Add(Me.edit_LatestTradeDate)
    Me.Tab_Derivatives.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Derivatives.Name = "Tab_Derivatives"
    Me.Tab_Derivatives.Size = New System.Drawing.Size(569, 395)
    Me.Tab_Derivatives.TabIndex = 3
    Me.Tab_Derivatives.Text = "Derivative"
    Me.Tab_Derivatives.UseVisualStyleBackColor = True
    '
    'Label24
    '
    Me.Label24.Location = New System.Drawing.Point(4, 43)
    Me.Label24.Name = "Label24"
    Me.Label24.Size = New System.Drawing.Size(98, 16)
    Me.Label24.TabIndex = 143
    Me.Label24.Text = "Expiry Date"
    '
    'Label25
    '
    Me.Label25.Location = New System.Drawing.Point(4, 11)
    Me.Label25.Name = "Label25"
    Me.Label25.Size = New System.Drawing.Size(98, 16)
    Me.Label25.TabIndex = 142
    Me.Label25.Text = "Underlying"
    '
    'Label_FixedCurrencyNote
    '
    Me.Label_FixedCurrencyNote.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label_FixedCurrencyNote.Location = New System.Drawing.Point(289, 244)
    Me.Label_FixedCurrencyNote.Name = "Label_FixedCurrencyNote"
    Me.Label_FixedCurrencyNote.Size = New System.Drawing.Size(273, 38)
    Me.Label_FixedCurrencyNote.TabIndex = 141
    Me.Label_FixedCurrencyNote.Text = "Fixed currency for FX Futures. (Not used at present)"
    '
    'Combo_FXFixedCurrency
    '
    Me.Combo_FXFixedCurrency.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_FXFixedCurrency.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_FXFixedCurrency.Location = New System.Drawing.Point(112, 241)
    Me.Combo_FXFixedCurrency.Name = "Combo_FXFixedCurrency"
    Me.Combo_FXFixedCurrency.Size = New System.Drawing.Size(169, 21)
    Me.Combo_FXFixedCurrency.TabIndex = 140
    '
    'Label_IsFxFuture
    '
    Me.Label_IsFxFuture.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label_IsFxFuture.Location = New System.Drawing.Point(142, 202)
    Me.Label_IsFxFuture.Name = "Label_IsFxFuture"
    Me.Label_IsFxFuture.Size = New System.Drawing.Size(420, 36)
    Me.Label_IsFxFuture.TabIndex = 139
    Me.Label_IsFxFuture.Text = "For FX Futures, the risk exposure is reversed.  i.e. For a long position, one is " & _
        "actually short of the floating currency."
    '
    'Check_IsFXFuture
    '
    Me.Check_IsFXFuture.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Check_IsFXFuture.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_IsFXFuture.Location = New System.Drawing.Point(7, 202)
    Me.Check_IsFXFuture.Name = "Check_IsFXFuture"
    Me.Check_IsFXFuture.Size = New System.Drawing.Size(118, 16)
    Me.Check_IsFXFuture.TabIndex = 138
    Me.Check_IsFXFuture.Text = "Is a FX Future"
    '
    'Label_SharesPerOptionComment
    '
    Me.Label_SharesPerOptionComment.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label_SharesPerOptionComment.Location = New System.Drawing.Point(289, 151)
    Me.Label_SharesPerOptionComment.Name = "Label_SharesPerOptionComment"
    Me.Label_SharesPerOptionComment.Size = New System.Drawing.Size(273, 36)
    Me.Label_SharesPerOptionComment.TabIndex = 137
    Me.Label_SharesPerOptionComment.Text = "As for Rights or warrants, for Risk calculations. " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Don't use both Shs per Option" & _
        " AND an adjusted Delta!" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
    '
    'Label60
    '
    Me.Label60.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label60.Location = New System.Drawing.Point(289, 126)
    Me.Label60.Name = "Label60"
    Me.Label60.Size = New System.Drawing.Size(273, 16)
    Me.Label60.TabIndex = 136
    Me.Label60.Text = "Same as 'Contract Size'."
    '
    'Label59
    '
    Me.Label59.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label59.Location = New System.Drawing.Point(289, 100)
    Me.Label59.Name = "Label59"
    Me.Label59.Size = New System.Drawing.Size(273, 16)
    Me.Label59.TabIndex = 135
    Me.Label59.Text = "As for listed futures and options contracts"
    '
    'edit_SharesPerOption
    '
    Me.edit_SharesPerOption.Location = New System.Drawing.Point(112, 149)
    Me.edit_SharesPerOption.Name = "edit_SharesPerOption"
    Me.edit_SharesPerOption.RenaissanceTag = Nothing
    Me.edit_SharesPerOption.SelectTextOnFocus = False
    Me.edit_SharesPerOption.Size = New System.Drawing.Size(172, 20)
    Me.edit_SharesPerOption.TabIndex = 133
    Me.edit_SharesPerOption.Text = "0.0000"
    Me.edit_SharesPerOption.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.edit_SharesPerOption.TextFormat = "#,##0.0000####"
    Me.edit_SharesPerOption.Value = 0
    '
    'Label_FixedCurrency
    '
    Me.Label_FixedCurrency.Location = New System.Drawing.Point(4, 244)
    Me.Label_FixedCurrency.Name = "Label_FixedCurrency"
    Me.Label_FixedCurrency.Size = New System.Drawing.Size(98, 16)
    Me.Label_FixedCurrency.TabIndex = 134
    Me.Label_FixedCurrency.Text = "Fixed Currency"
    '
    'Label_SharesPerOption
    '
    Me.Label_SharesPerOption.Location = New System.Drawing.Point(4, 151)
    Me.Label_SharesPerOption.Name = "Label_SharesPerOption"
    Me.Label_SharesPerOption.Size = New System.Drawing.Size(98, 16)
    Me.Label_SharesPerOption.TabIndex = 134
    Me.Label_SharesPerOption.Text = "Shares per Option"
    '
    'Tab_Bonds
    '
    Me.Tab_Bonds.Controls.Add(Me.Label49)
    Me.Tab_Bonds.Controls.Add(Me.edit_BondMultiplier)
    Me.Tab_Bonds.Controls.Add(Me.edit_BondInterestPercentage)
    Me.Tab_Bonds.Controls.Add(Me.Date_BondMaturityDate)
    Me.Tab_Bonds.Controls.Add(Me.Text_BondGoverningLaw)
    Me.Tab_Bonds.Controls.Add(Me.Text_BondRanking)
    Me.Tab_Bonds.Controls.Add(Me.Text_BondIssuer)
    Me.Tab_Bonds.Controls.Add(Me.Panel_Coupons)
    Me.Tab_Bonds.Controls.Add(Me.Label39)
    Me.Tab_Bonds.Controls.Add(Me.Label38)
    Me.Tab_Bonds.Controls.Add(Me.Label37)
    Me.Tab_Bonds.Controls.Add(Me.Label36)
    Me.Tab_Bonds.Controls.Add(Me.Label35)
    Me.Tab_Bonds.Controls.Add(Me.Label34)
    Me.Tab_Bonds.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Bonds.Name = "Tab_Bonds"
    Me.Tab_Bonds.Size = New System.Drawing.Size(569, 395)
    Me.Tab_Bonds.TabIndex = 5
    Me.Tab_Bonds.Text = "Bonds"
    Me.Tab_Bonds.UseVisualStyleBackColor = True
    '
    'Label49
    '
    Me.Label49.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label49.Location = New System.Drawing.Point(10, 116)
    Me.Label49.Name = "Label49"
    Me.Label49.Size = New System.Drawing.Size(100, 17)
    Me.Label49.TabIndex = 146
    Me.Label49.Text = "Multiplier"
    '
    'edit_BondMultiplier
    '
    Me.edit_BondMultiplier.Location = New System.Drawing.Point(115, 113)
    Me.edit_BondMultiplier.Name = "edit_BondMultiplier"
    Me.edit_BondMultiplier.RenaissanceTag = Nothing
    Me.edit_BondMultiplier.SelectTextOnFocus = False
    Me.edit_BondMultiplier.Size = New System.Drawing.Size(140, 20)
    Me.edit_BondMultiplier.TabIndex = 145
    Me.edit_BondMultiplier.Text = "0.0000"
    Me.edit_BondMultiplier.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.edit_BondMultiplier.TextFormat = "#,##0.0000####"
    Me.edit_BondMultiplier.Value = 0
    '
    'edit_BondInterestPercentage
    '
    Me.edit_BondInterestPercentage.Location = New System.Drawing.Point(115, 141)
    Me.edit_BondInterestPercentage.Name = "edit_BondInterestPercentage"
    Me.edit_BondInterestPercentage.RenaissanceTag = Nothing
    Me.edit_BondInterestPercentage.SelectTextOnFocus = False
    Me.edit_BondInterestPercentage.Size = New System.Drawing.Size(140, 20)
    Me.edit_BondInterestPercentage.TabIndex = 144
    Me.edit_BondInterestPercentage.Text = "0.00%"
    Me.edit_BondInterestPercentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.edit_BondInterestPercentage.TextFormat = "#,##0.00%"
    Me.edit_BondInterestPercentage.Value = 0
    Me.edit_BondInterestPercentage.Visible = False
    '
    'Date_BondMaturityDate
    '
    Me.Date_BondMaturityDate.CustomFormat = "dd MMM yyyy"
    Me.Date_BondMaturityDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
    Me.Date_BondMaturityDate.Location = New System.Drawing.Point(116, 87)
    Me.Date_BondMaturityDate.Name = "Date_BondMaturityDate"
    Me.Date_BondMaturityDate.Size = New System.Drawing.Size(200, 20)
    Me.Date_BondMaturityDate.TabIndex = 143
    '
    'Text_BondGoverningLaw
    '
    Me.Text_BondGoverningLaw.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Text_BondGoverningLaw.Location = New System.Drawing.Point(116, 61)
    Me.Text_BondGoverningLaw.Name = "Text_BondGoverningLaw"
    Me.Text_BondGoverningLaw.Size = New System.Drawing.Size(448, 20)
    Me.Text_BondGoverningLaw.TabIndex = 142
    '
    'Text_BondRanking
    '
    Me.Text_BondRanking.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Text_BondRanking.Location = New System.Drawing.Point(116, 34)
    Me.Text_BondRanking.Name = "Text_BondRanking"
    Me.Text_BondRanking.Size = New System.Drawing.Size(448, 20)
    Me.Text_BondRanking.TabIndex = 141
    '
    'Text_BondIssuer
    '
    Me.Text_BondIssuer.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Text_BondIssuer.Location = New System.Drawing.Point(116, 7)
    Me.Text_BondIssuer.Name = "Text_BondIssuer"
    Me.Text_BondIssuer.Size = New System.Drawing.Size(448, 20)
    Me.Text_BondIssuer.TabIndex = 140
    '
    'Panel_Coupons
    '
    Me.Panel_Coupons.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Panel_Coupons.Location = New System.Drawing.Point(115, 172)
    Me.Panel_Coupons.Name = "Panel_Coupons"
    Me.Panel_Coupons.Size = New System.Drawing.Size(449, 166)
    Me.Panel_Coupons.TabIndex = 139
    '
    'Label39
    '
    Me.Label39.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label39.Location = New System.Drawing.Point(10, 172)
    Me.Label39.Name = "Label39"
    Me.Label39.Size = New System.Drawing.Size(100, 17)
    Me.Label39.TabIndex = 138
    Me.Label39.Text = "Coupon Dates"
    '
    'Label38
    '
    Me.Label38.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label38.Location = New System.Drawing.Point(10, 64)
    Me.Label38.Name = "Label38"
    Me.Label38.Size = New System.Drawing.Size(100, 17)
    Me.Label38.TabIndex = 137
    Me.Label38.Text = "Governing Law"
    '
    'Label37
    '
    Me.Label37.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label37.Location = New System.Drawing.Point(10, 144)
    Me.Label37.Name = "Label37"
    Me.Label37.Size = New System.Drawing.Size(100, 17)
    Me.Label37.TabIndex = 136
    Me.Label37.Text = "Interest Rate"
    '
    'Label36
    '
    Me.Label36.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label36.Location = New System.Drawing.Point(10, 90)
    Me.Label36.Name = "Label36"
    Me.Label36.Size = New System.Drawing.Size(100, 17)
    Me.Label36.TabIndex = 135
    Me.Label36.Text = "Maturity Date"
    '
    'Label35
    '
    Me.Label35.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label35.Location = New System.Drawing.Point(10, 37)
    Me.Label35.Name = "Label35"
    Me.Label35.Size = New System.Drawing.Size(100, 17)
    Me.Label35.TabIndex = 134
    Me.Label35.Text = "Ranking"
    '
    'Label34
    '
    Me.Label34.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label34.Location = New System.Drawing.Point(10, 10)
    Me.Label34.Name = "Label34"
    Me.Label34.Size = New System.Drawing.Size(100, 17)
    Me.Label34.TabIndex = 133
    Me.Label34.Text = "Issuer"
    '
    'Tab_Convertibles
    '
    Me.Tab_Convertibles.Controls.Add(Me.Label55)
    Me.Tab_Convertibles.Controls.Add(Me.edit_CB_ContractSize)
    Me.Tab_Convertibles.Controls.Add(Me.Combo_CB_Underlying)
    Me.Tab_Convertibles.Controls.Add(Me.Label46)
    Me.Tab_Convertibles.Controls.Add(Me.edit_CB_InterestRate)
    Me.Tab_Convertibles.Controls.Add(Me.Date_CB_MaturityDate)
    Me.Tab_Convertibles.Controls.Add(Me.Text_CB_GoverningLaw)
    Me.Tab_Convertibles.Controls.Add(Me.Text_CB_Ranking)
    Me.Tab_Convertibles.Controls.Add(Me.Text_CB_Issuer)
    Me.Tab_Convertibles.Controls.Add(Me.Panel_CB_CouponDates)
    Me.Tab_Convertibles.Controls.Add(Me.Label40)
    Me.Tab_Convertibles.Controls.Add(Me.Label41)
    Me.Tab_Convertibles.Controls.Add(Me.Label42)
    Me.Tab_Convertibles.Controls.Add(Me.Label43)
    Me.Tab_Convertibles.Controls.Add(Me.Label44)
    Me.Tab_Convertibles.Controls.Add(Me.Label45)
    Me.Tab_Convertibles.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Convertibles.Name = "Tab_Convertibles"
    Me.Tab_Convertibles.Size = New System.Drawing.Size(569, 395)
    Me.Tab_Convertibles.TabIndex = 6
    Me.Tab_Convertibles.Text = "Convertibles"
    Me.Tab_Convertibles.UseVisualStyleBackColor = True
    '
    'Label55
    '
    Me.Label55.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label55.Location = New System.Drawing.Point(10, 147)
    Me.Label55.Name = "Label55"
    Me.Label55.Size = New System.Drawing.Size(100, 17)
    Me.Label55.TabIndex = 160
    Me.Label55.Text = "Multiplier"
    '
    'edit_CB_ContractSize
    '
    Me.edit_CB_ContractSize.Location = New System.Drawing.Point(115, 144)
    Me.edit_CB_ContractSize.Name = "edit_CB_ContractSize"
    Me.edit_CB_ContractSize.RenaissanceTag = Nothing
    Me.edit_CB_ContractSize.SelectTextOnFocus = False
    Me.edit_CB_ContractSize.Size = New System.Drawing.Size(140, 20)
    Me.edit_CB_ContractSize.TabIndex = 159
    Me.edit_CB_ContractSize.Text = "0.0000"
    Me.edit_CB_ContractSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.edit_CB_ContractSize.TextFormat = "#,##0.0000####"
    Me.edit_CB_ContractSize.Value = 0
    '
    'Combo_CB_Underlying
    '
    Me.Combo_CB_Underlying.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_CB_Underlying.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_CB_Underlying.Location = New System.Drawing.Point(116, 33)
    Me.Combo_CB_Underlying.Name = "Combo_CB_Underlying"
    Me.Combo_CB_Underlying.Size = New System.Drawing.Size(450, 21)
    Me.Combo_CB_Underlying.TabIndex = 157
    '
    'Label46
    '
    Me.Label46.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label46.Location = New System.Drawing.Point(11, 36)
    Me.Label46.Name = "Label46"
    Me.Label46.Size = New System.Drawing.Size(100, 20)
    Me.Label46.TabIndex = 158
    Me.Label46.Text = "Underlying"
    '
    'edit_CB_InterestRate
    '
    Me.edit_CB_InterestRate.Location = New System.Drawing.Point(115, 172)
    Me.edit_CB_InterestRate.Name = "edit_CB_InterestRate"
    Me.edit_CB_InterestRate.RenaissanceTag = Nothing
    Me.edit_CB_InterestRate.SelectTextOnFocus = False
    Me.edit_CB_InterestRate.Size = New System.Drawing.Size(140, 20)
    Me.edit_CB_InterestRate.TabIndex = 156
    Me.edit_CB_InterestRate.Text = "0.00%"
    Me.edit_CB_InterestRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.edit_CB_InterestRate.TextFormat = "#,##0.00%"
    Me.edit_CB_InterestRate.Value = 0
    Me.edit_CB_InterestRate.Visible = False
    '
    'Date_CB_MaturityDate
    '
    Me.Date_CB_MaturityDate.CustomFormat = "dd MMM yyyy"
    Me.Date_CB_MaturityDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
    Me.Date_CB_MaturityDate.Location = New System.Drawing.Point(116, 116)
    Me.Date_CB_MaturityDate.Name = "Date_CB_MaturityDate"
    Me.Date_CB_MaturityDate.Size = New System.Drawing.Size(200, 20)
    Me.Date_CB_MaturityDate.TabIndex = 155
    '
    'Text_CB_GoverningLaw
    '
    Me.Text_CB_GoverningLaw.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Text_CB_GoverningLaw.Location = New System.Drawing.Point(116, 89)
    Me.Text_CB_GoverningLaw.Name = "Text_CB_GoverningLaw"
    Me.Text_CB_GoverningLaw.Size = New System.Drawing.Size(448, 20)
    Me.Text_CB_GoverningLaw.TabIndex = 154
    '
    'Text_CB_Ranking
    '
    Me.Text_CB_Ranking.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Text_CB_Ranking.Location = New System.Drawing.Point(116, 62)
    Me.Text_CB_Ranking.Name = "Text_CB_Ranking"
    Me.Text_CB_Ranking.Size = New System.Drawing.Size(448, 20)
    Me.Text_CB_Ranking.TabIndex = 153
    '
    'Text_CB_Issuer
    '
    Me.Text_CB_Issuer.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Text_CB_Issuer.Location = New System.Drawing.Point(116, 7)
    Me.Text_CB_Issuer.Name = "Text_CB_Issuer"
    Me.Text_CB_Issuer.Size = New System.Drawing.Size(448, 20)
    Me.Text_CB_Issuer.TabIndex = 152
    '
    'Panel_CB_CouponDates
    '
    Me.Panel_CB_CouponDates.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Panel_CB_CouponDates.Location = New System.Drawing.Point(115, 202)
    Me.Panel_CB_CouponDates.Name = "Panel_CB_CouponDates"
    Me.Panel_CB_CouponDates.Size = New System.Drawing.Size(449, 123)
    Me.Panel_CB_CouponDates.TabIndex = 151
    '
    'Label40
    '
    Me.Label40.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label40.Location = New System.Drawing.Point(10, 202)
    Me.Label40.Name = "Label40"
    Me.Label40.Size = New System.Drawing.Size(100, 17)
    Me.Label40.TabIndex = 150
    Me.Label40.Text = "Coupon Dates"
    '
    'Label41
    '
    Me.Label41.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label41.Location = New System.Drawing.Point(10, 92)
    Me.Label41.Name = "Label41"
    Me.Label41.Size = New System.Drawing.Size(100, 17)
    Me.Label41.TabIndex = 149
    Me.Label41.Text = "Governing Law"
    '
    'Label42
    '
    Me.Label42.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label42.Location = New System.Drawing.Point(10, 175)
    Me.Label42.Name = "Label42"
    Me.Label42.Size = New System.Drawing.Size(100, 17)
    Me.Label42.TabIndex = 148
    Me.Label42.Text = "Interest Rate"
    '
    'Label43
    '
    Me.Label43.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label43.Location = New System.Drawing.Point(10, 119)
    Me.Label43.Name = "Label43"
    Me.Label43.Size = New System.Drawing.Size(100, 17)
    Me.Label43.TabIndex = 147
    Me.Label43.Text = "Maturity Date"
    '
    'Label44
    '
    Me.Label44.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label44.Location = New System.Drawing.Point(10, 65)
    Me.Label44.Name = "Label44"
    Me.Label44.Size = New System.Drawing.Size(100, 17)
    Me.Label44.TabIndex = 146
    Me.Label44.Text = "Ranking"
    '
    'Label45
    '
    Me.Label45.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label45.Location = New System.Drawing.Point(10, 10)
    Me.Label45.Name = "Label45"
    Me.Label45.Size = New System.Drawing.Size(100, 17)
    Me.Label45.TabIndex = 145
    Me.Label45.Text = "Issuer"
    '
    'Tab_Advanced
    '
    Me.Tab_Advanced.BackColor = System.Drawing.Color.Transparent
    Me.Tab_Advanced.Controls.Add(Me.edit_Penalty)
    Me.Tab_Advanced.Controls.Add(Me.Label13)
    Me.Tab_Advanced.Controls.Add(Me.edit_LiquidityComment)
    Me.Tab_Advanced.Controls.Add(Me.Label15)
    Me.Tab_Advanced.Controls.Add(Me.Combo_InstrumentParent)
    Me.Tab_Advanced.Controls.Add(Me.Button_ShowParentHeirarchy)
    Me.Tab_Advanced.Controls.Add(Me.Label4)
    Me.Tab_Advanced.Controls.Add(Me.Label11)
    Me.Tab_Advanced.Controls.Add(Me.Label9)
    Me.Tab_Advanced.Controls.Add(Me.edit_DealingNoticeDays)
    Me.Tab_Advanced.Controls.Add(Me.Label10)
    Me.Tab_Advanced.Controls.Add(Me.Check_UpfrontFees)
    Me.Tab_Advanced.Controls.Add(Me.Label14)
    Me.Tab_Advanced.Controls.Add(Me.edit_DealingBaseDate)
    Me.Tab_Advanced.Controls.Add(Me.Label12)
    Me.Tab_Advanced.Controls.Add(Me.Combo_ReviewGroup)
    Me.Tab_Advanced.Controls.Add(Me.edit_LockupPeriod)
    Me.Tab_Advanced.Controls.Add(Me.Label22)
    Me.Tab_Advanced.Controls.Add(Me.Combo_DealingPeriod)
    Me.Tab_Advanced.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Advanced.Name = "Tab_Advanced"
    Me.Tab_Advanced.Padding = New System.Windows.Forms.Padding(3)
    Me.Tab_Advanced.Size = New System.Drawing.Size(569, 395)
    Me.Tab_Advanced.TabIndex = 1
    Me.Tab_Advanced.Text = "Investment Terms"
    Me.Tab_Advanced.UseVisualStyleBackColor = True
    '
    'Tab_Performance
    '
    Me.Tab_Performance.Controls.Add(Me.Group_LinkedEntities)
    Me.Tab_Performance.Controls.Add(Me.lblVolatility)
    Me.Tab_Performance.Controls.Add(Me.Label8)
    Me.Tab_Performance.Controls.Add(Me.edit_WorstCaseFall)
    Me.Tab_Performance.Controls.Add(Me.edit_ExpectedReturn)
    Me.Tab_Performance.Controls.Add(Me.Label31)
    Me.Tab_Performance.Controls.Add(Me.Check_LimitDisplay)
    Me.Tab_Performance.Controls.Add(Me.Label_AdminfundType)
    Me.Tab_Performance.Controls.Add(Me.Combo_Benckmark)
    Me.Tab_Performance.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Performance.Name = "Tab_Performance"
    Me.Tab_Performance.Size = New System.Drawing.Size(569, 395)
    Me.Tab_Performance.TabIndex = 4
    Me.Tab_Performance.Text = "Risk / Return"
    Me.Tab_Performance.UseVisualStyleBackColor = True
    '
    'Group_LinkedEntities
    '
    Me.Group_LinkedEntities.Controls.Add(Me.List_Entity)
    Me.Group_LinkedEntities.Controls.Add(Me.Button_DeleteEntity)
    Me.Group_LinkedEntities.Controls.Add(Me.Label26)
    Me.Group_LinkedEntities.Controls.Add(Me.Button_AddEntity)
    Me.Group_LinkedEntities.Controls.Add(Me.Combo_AddEntity)
    Me.Group_LinkedEntities.Controls.Add(Me.Label27)
    Me.Group_LinkedEntities.Location = New System.Drawing.Point(6, 74)
    Me.Group_LinkedEntities.Name = "Group_LinkedEntities"
    Me.Group_LinkedEntities.Size = New System.Drawing.Size(560, 251)
    Me.Group_LinkedEntities.TabIndex = 3
    Me.Group_LinkedEntities.TabStop = False
    Me.Group_LinkedEntities.Text = "Due Dilligence Entity"
    '
    'List_Entity
    '
    Me.List_Entity.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.List_Entity.FormattingEnabled = True
    Me.List_Entity.Location = New System.Drawing.Point(125, 19)
    Me.List_Entity.Name = "List_Entity"
    Me.List_Entity.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
    Me.List_Entity.Size = New System.Drawing.Size(356, 186)
    Me.List_Entity.TabIndex = 0
    '
    'Button_DeleteEntity
    '
    Me.Button_DeleteEntity.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Button_DeleteEntity.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Button_DeleteEntity.Location = New System.Drawing.Point(488, 19)
    Me.Button_DeleteEntity.Name = "Button_DeleteEntity"
    Me.Button_DeleteEntity.Size = New System.Drawing.Size(57, 20)
    Me.Button_DeleteEntity.TabIndex = 1
    Me.Button_DeleteEntity.Text = "Delete"
    '
    'Label26
    '
    Me.Label26.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label26.Location = New System.Drawing.Point(9, 19)
    Me.Label26.Name = "Label26"
    Me.Label26.Size = New System.Drawing.Size(106, 20)
    Me.Label26.TabIndex = 98
    Me.Label26.Text = "Linked Entities"
    '
    'Button_AddEntity
    '
    Me.Button_AddEntity.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Button_AddEntity.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Button_AddEntity.Location = New System.Drawing.Point(488, 219)
    Me.Button_AddEntity.Name = "Button_AddEntity"
    Me.Button_AddEntity.Size = New System.Drawing.Size(57, 20)
    Me.Button_AddEntity.TabIndex = 3
    Me.Button_AddEntity.Text = "Add"
    '
    'Combo_AddEntity
    '
    Me.Combo_AddEntity.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_AddEntity.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_AddEntity.Location = New System.Drawing.Point(121, 220)
    Me.Combo_AddEntity.Name = "Combo_AddEntity"
    Me.Combo_AddEntity.Size = New System.Drawing.Size(361, 21)
    Me.Combo_AddEntity.TabIndex = 2
    '
    'Label27
    '
    Me.Label27.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label27.Location = New System.Drawing.Point(9, 221)
    Me.Label27.Name = "Label27"
    Me.Label27.Size = New System.Drawing.Size(106, 20)
    Me.Label27.TabIndex = 96
    Me.Label27.Text = "Entity"
    '
    'Label31
    '
    Me.Label31.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label31.Location = New System.Drawing.Point(9, 12)
    Me.Label31.Name = "Label31"
    Me.Label31.Size = New System.Drawing.Size(100, 20)
    Me.Label31.TabIndex = 133
    Me.Label31.Text = "Benchmark"
    '
    'Combo_Benckmark
    '
    Me.Combo_Benckmark.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Benckmark.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Benckmark.Location = New System.Drawing.Point(117, 9)
    Me.Combo_Benckmark.Name = "Combo_Benckmark"
    Me.Combo_Benckmark.Size = New System.Drawing.Size(449, 21)
    Me.Combo_Benckmark.TabIndex = 0
    '
    'Tab_Legacy
    '
    Me.Tab_Legacy.BackColor = System.Drawing.Color.Transparent
    Me.Tab_Legacy.Controls.Add(Me.Combo_LeverageCounterparty)
    Me.Tab_Legacy.Controls.Add(Me.Check_IsLeverageInstrument)
    Me.Tab_Legacy.Controls.Add(Me.Label17)
    Me.Tab_Legacy.Controls.Add(Me.lblPertracIndex)
    Me.Tab_Legacy.Controls.Add(Me.edit_PortfolioTicker)
    Me.Tab_Legacy.Controls.Add(Me.Label18)
    Me.Tab_Legacy.Controls.Add(Me.Combo_PertracIndex)
    Me.Tab_Legacy.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Legacy.Name = "Tab_Legacy"
    Me.Tab_Legacy.Size = New System.Drawing.Size(569, 395)
    Me.Tab_Legacy.TabIndex = 2
    Me.Tab_Legacy.Text = "Legacy"
    Me.Tab_Legacy.UseVisualStyleBackColor = True
    '
    'Combo_PertracIndex
    '
    Me.Combo_PertracIndex.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_PertracIndex.Location = New System.Drawing.Point(107, 55)
    Me.Combo_PertracIndex.MasternameCollection = Nothing
    Me.Combo_PertracIndex.Name = "Combo_PertracIndex"
    '
    '
    '
    Me.Combo_PertracIndex.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
    Me.Combo_PertracIndex.SelectNoMatch = False
    Me.Combo_PertracIndex.Size = New System.Drawing.Size(456, 20)
    Me.Combo_PertracIndex.TabIndex = 1
    Me.Combo_PertracIndex.ThemeName = "ControlDefault"
    '
    'TabControl_HiddenTabs
    '
    Me.TabControl_HiddenTabs.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.TabControl_HiddenTabs.Location = New System.Drawing.Point(8, 638)
    Me.TabControl_HiddenTabs.Name = "TabControl_HiddenTabs"
    Me.TabControl_HiddenTabs.SelectedIndex = 0
    Me.TabControl_HiddenTabs.Size = New System.Drawing.Size(20, 23)
    Me.TabControl_HiddenTabs.TabIndex = 88
    Me.TabControl_HiddenTabs.Visible = False
    '
    'Label54
    '
    Me.Label54.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label54.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label54.Location = New System.Drawing.Point(406, 94)
    Me.Label54.Name = "Label54"
    Me.Label54.Size = New System.Drawing.Size(181, 16)
    Me.Label54.TabIndex = 89
    Me.Label54.Text = "(Match to Administrator Class name)"
    '
    'frmInstrument
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.CancelButton = Me.btnCancel
    Me.ClientSize = New System.Drawing.Size(592, 670)
    Me.Controls.Add(Me.Label54)
    Me.Controls.Add(Me.TabControl_HiddenTabs)
    Me.Controls.Add(Me.Tab_InstrumentDetails)
    Me.Controls.Add(Me.RootMenu)
    Me.Controls.Add(Me.edit_InstrumentClass)
    Me.Controls.Add(Me.editAuditID)
    Me.Controls.Add(Me.edit_InstrumentDescription)
    Me.Controls.Add(Me.Panel1)
    Me.Controls.Add(Me.edit_InstrumentCode)
    Me.Controls.Add(Me.Label5)
    Me.Controls.Add(Me.btnClose)
    Me.Controls.Add(Me.GroupBox1)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.Combo_SelectInstrumentDescription)
    Me.Controls.Add(Me.btnSave)
    Me.Controls.Add(Me.lblInstrumentDescription)
    Me.Controls.Add(Me.lblInstrumentCode)
    Me.Controls.Add(Me.btnDelete)
    Me.Controls.Add(Me.btnCancel)
    Me.MinimumSize = New System.Drawing.Size(592, 696)
    Me.Name = "frmInstrument"
    Me.Text = "Add/Edit Instrument"
    Me.Panel1.ResumeLayout(False)
    CType(Me.edit_ExpectedReturn, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.edit_WorstCaseFall, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.edit_DealingNoticeDays, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.edit_DealingBaseDate, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.edit_LockupPeriod, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.edit_Penalty, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.edit_LatestTradeDate, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.edit_ExpiryDate, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_InstrumentDetails.ResumeLayout(False)
    Me.Tab_BasicDetails.ResumeLayout(False)
    Me.Tab_BasicDetails.PerformLayout()
    CType(Me.edit_MaxDecimalPlaces, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.edit_Settlemet_Sell, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.edit_Settlemet_Buy, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_ManagerNotes.ResumeLayout(False)
    Me.Tab_ManagerNotes.PerformLayout()
    Me.Tab_Unit.ResumeLayout(False)
    Me.Tab_Unit.PerformLayout()
    Me.Tab_Derivatives.ResumeLayout(False)
    Me.Tab_Derivatives.PerformLayout()
    Me.Tab_Bonds.ResumeLayout(False)
    Me.Tab_Bonds.PerformLayout()
    Me.Tab_Convertibles.ResumeLayout(False)
    Me.Tab_Convertibles.PerformLayout()
    Me.Tab_Advanced.ResumeLayout(False)
    Me.Tab_Advanced.PerformLayout()
    Me.Tab_Performance.ResumeLayout(False)
    Me.Group_LinkedEntities.ResumeLayout(False)
    Me.Tab_Legacy.ResumeLayout(False)
    Me.Tab_Legacy.PerformLayout()
    CType(Me.Combo_PertracIndex, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "


  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
  ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
  ''' </summary>
  Private WithEvents _MainForm As VeniceMain

  ' Form Menu


  ' Form ToolTip
  ''' <summary>
  ''' Control to provide tooltip functionality.
  ''' </summary>
  Private FormTooltip As New ToolTip()

  ' Form Constants, specific to the table being updated.

  ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
  ''' </summary>
  Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

  ' Form Locals, initialised on 'New' defining what standard data items to use
  ''' <summary>
  ''' Variable holding the form's data table name. taken from the "ThisStandardDataset" object.
  ''' </summary>
  Private THIS_TABLENAME As String
  ''' <summary>
  ''' Variable holding the form's data adaptor name. taken from the "ThisStandardDataset" object.
  ''' </summary>
  Private THIS_ADAPTORNAME As String
  ''' <summary>
  ''' Variable holding the form's dataset name. taken from the "ThisStandardDataset" object.
  ''' </summary>
  Private THIS_DATASETNAME As String


  ' The standard ChangeID for this form. e.g. tblInstrument
  ''' <summary>
  ''' The standard ChangeID for this form.
  ''' </summary>
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

  ''' <summary>
  ''' Principal control used for selecting items on this form. 
  ''' </summary>
  Private THIS_FORM_SelectingCombo As ComboBox
  ''' <summary>
  ''' Control to select after proessing the "New" button.
  ''' </summary>
  Private THIS_FORM_NewMoveToControl As Control

  ' Form Specific Order fields
  ''' <summary>
  ''' Field Name to show in the Select combo.
  ''' </summary>
  Private THIS_FORM_SelectBy As String
  ''' <summary>
  ''' Field Name to order the Select combo by.
  ''' </summary>
  Private THIS_FORM_OrderBy As String

  ''' <summary>
  ''' The field name that holds the unique identifier for each entry. e.g. FundID for the tblFund table.
  ''' </summary>
  Private THIS_FORM_ValueMember As String

  ' Form specific Permissioning variables
  ''' <summary>
  ''' The THI s_ FOR m_ permission area
  ''' </summary>
  Private THIS_FORM_PermissionArea As String
  ''' <summary>
  ''' The THI s_ FOR m_ permission type
  ''' </summary>
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
  ''' <summary>
  ''' The THI s_ FOR m_ form ID
  ''' </summary>
  Private THIS_FORM_FormID As VeniceFormID

  ' Data Structures

  ''' <summary>
  ''' My dataset
  ''' </summary>
  Private myDataset As RenaissanceDataClass.DSInstrument ' Form Specific !!!!
  ''' <summary>
  ''' My table
  ''' </summary>
  Private myTable As RenaissanceDataClass.DSInstrument.tblInstrumentDataTable
  ''' <summary>
  ''' My connection
  ''' </summary>
  Private myConnection As SqlConnection
  ''' <summary>
  ''' My adaptor
  ''' </summary>
  Private myAdaptor As SqlDataAdapter

  Private tempNotesAdaptor As SqlDataAdapter = Nothing

  ''' <summary>
  ''' The this standard dataset
  ''' </summary>
  Private ThisStandardDataset As RenaissanceGlobals.StandardDataset


  ' Active Element.

  ''' <summary>
  ''' The sorted rows
  ''' </summary>
  Private SortedRows() As DataRow
  ''' <summary>
  ''' The select by sorted rows
  ''' </summary>
  Private SelectBySortedRows() As DataRow
  ''' <summary>
  ''' The this data row
  ''' </summary>
  Private thisDataRow As RenaissanceDataClass.DSInstrument.tblInstrumentRow   ' Form Specific !!!!
  ''' <summary>
  ''' The this audit ID
  ''' </summary>
  Private thisAuditID As Integer
  ''' <summary>
  ''' The this position
  ''' </summary>
  Private thisPosition As Integer
  ''' <summary>
  ''' The __ is over cancel button
  ''' </summary>
  Private __IsOverCancelButton As Boolean
  ''' <summary>
  ''' The _ in use
  ''' </summary>
  Private _InUse As Boolean

  ' Form Status Flags

  ''' <summary>
  ''' The form is valid
  ''' </summary>
  Private FormIsValid As Boolean
  ''' <summary>
  ''' The form changed
  ''' </summary>
  Private FormChanged As Boolean
  Private Notes_ManagerNoteChanged As Boolean
  Private Notes_ProspectusNoteChanged As Boolean

  ''' <summary>
  ''' The _ form open failed
  ''' </summary>
  Private _FormOpenFailed As Boolean
  ''' <summary>
  ''' The in paint
  ''' </summary>
  Private InPaint As Boolean
  ''' <summary>
  ''' The add new record
  ''' </summary>
  Private AddNewRecord As Boolean

  ' User Permission Flags

  ''' <summary>
  ''' The has read permission
  ''' </summary>
  Private HasReadPermission As Boolean
  ''' <summary>
  ''' The has update permission
  ''' </summary>
  Private HasUpdatePermission As Boolean
  ''' <summary>
  ''' The has insert permission
  ''' </summary>
  Private HasInsertPermission As Boolean
  ''' <summary>
  ''' The has delete permission
  ''' </summary>
  Private HasDeletePermission As Boolean

  ''' <summary>
  ''' Dictionary of TabControls, to facilitate Hiding and Showing then
  ''' </summary>
  Private InstrumentTabs As New Dictionary(Of String, TabPage)

#End Region

#Region " Form 'Properties' "

  ''' <summary>
  ''' Gets the main form.
  ''' </summary>
  ''' <value>The main form.</value>
  Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
    ' Public property to return handle to the 'Main' Venice form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

  ''' <summary>
  ''' Gets or sets a value indicating whether this instance is over cancel button.
  ''' </summary>
  ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
  Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
    ' Public property maintaining a value indicating if the cursor is over the 'Cancel'
    ' Button on this form.
    ' This property is specifically designed for use by the field formating Event functions
    ' In order that they do not impose format restrictions if the user is about to click the 
    ' 'Cancel' button.
    '
    Get
      Return __IsOverCancelButton
    End Get
    Set(ByVal Value As Boolean)
      __IsOverCancelButton = Value
    End Set
  End Property

  ''' <summary>
  ''' Gets a value indicating whether this instance is in paint.
  ''' </summary>
  ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
  Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
    Get
      Return InPaint
    End Get
  End Property

  ''' <summary>
  ''' Gets a value indicating whether [in use].
  ''' </summary>
  ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
    Get
      Return _InUse
    End Get
  End Property

  ''' <summary>
  ''' Gets a value indicating whether [form open failed].
  ''' </summary>
  ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

#End Region

  ''' <summary>
  ''' Initializes a new instance of the <see cref="frmInstrument"/> class.
  ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  Public Sub New(ByVal pMainForm As VeniceMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()

    _MainForm = pMainForm
    AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

    _FormOpenFailed = False
    _InUse = True

    ' ******************************************************
    ' Form Specific Settings :
    ' ******************************************************

    THIS_FORM_SelectingCombo = Me.Combo_SelectInstrumentDescription
    THIS_FORM_NewMoveToControl = Me.edit_InstrumentDescription

    ' Default Select and Order fields.

    THIS_FORM_SelectBy = "InstrumentDescription"
    THIS_FORM_OrderBy = "InstrumentDescription"

    THIS_FORM_ValueMember = "InstrumentID"

    ' Form Permissioning :-

    THIS_FORM_PermissionArea = Me.Name
    THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

    ' 'This' form ID

    THIS_FORM_FormID = VeniceFormID.frmInstrument

    ' This form's dataset type.

    ThisStandardDataset = RenaissanceStandardDatasets.tblInstrument ' This Defines the Form Data !!! 

    ' Format Event Handlers for form controls
    AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler
    AddHandler edit_InstrumentDescription.LostFocus, AddressOf MainForm.Text_NotNull


    ' Form Control Changed events
    AddHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
    AddHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

    'AddHandler Combo_PertracIndex.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_InstrumentType.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_InstrumentParent.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_Benckmark.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_InstrumentUnderlying.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_InstrumentFundType.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_FundID.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_PerformanceVsBenchmark.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_InstrumentCurrency.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_DealingPeriod.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_LeverageCounterparty.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_ReviewGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_CB_Underlying.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_AttributionMarket.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
    AddHandler Combo_AttributionSector.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
    AddHandler Combo_FXFixedCurrency.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

    'AddHandler Combo_PertracIndex.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_InstrumentType.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_InstrumentParent.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_Benckmark.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_InstrumentUnderlying.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_InstrumentFundType.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_FundID.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_PerformanceVsBenchmark.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_InstrumentCurrency.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_DealingPeriod.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_LeverageCounterparty.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_ReviewGroup.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_CB_Underlying.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_AttributionMarket.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_AttributionSector.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_FXFixedCurrency.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

    AddHandler Combo_PertracIndex.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_InstrumentType.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_InstrumentParent.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_Benckmark.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_InstrumentUnderlying.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_InstrumentFundType.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_FundID.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_PerformanceVsBenchmark.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_InstrumentCurrency.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_DealingPeriod.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_LeverageCounterparty.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_ReviewGroup.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_CB_Underlying.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_AttributionMarket.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_AttributionSector.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_FXFixedCurrency.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

    AddHandler Combo_AttributionMarket.SelectedValueChanged, AddressOf FormControlChanged
    AddHandler Combo_AttributionSector.SelectedValueChanged, AddressOf FormControlChanged

    AddHandler Combo_AttributionMarket.TextChanged, AddressOf FormControlChanged
    AddHandler Combo_AttributionSector.TextChanged, AddressOf FormControlChanged

    'AddHandler Combo_PertracIndex.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_InstrumentType.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_InstrumentParent.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_Benckmark.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_InstrumentUnderlying.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_InstrumentFundType.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_FundID.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_PerformanceVsBenchmark.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_InstrumentCurrency.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_DealingPeriod.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_LeverageCounterparty.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_ReviewGroup.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_CB_Underlying.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_AttributionMarket.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_AttributionSector.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_FXFixedCurrency.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

    AddHandler Combo_AddEntity.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_AddEntity.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_AddEntity.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_AddEntity.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_AddEntity.SelectedValueChanged, AddressOf Me.FormControlChanged

    AddHandler edit_InstrumentDescription.TextChanged, AddressOf Me.FormControlChanged
    AddHandler edit_InstrumentCode.TextChanged, AddressOf Me.FormControlChanged
    AddHandler edit_PFPCID.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_PertracIndex.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_InstrumentType.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_InstrumentParent.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Benckmark.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_InstrumentUnderlying.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler edit_InstrumentClass.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_InstrumentFundType.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_FundID.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_PerformanceVsBenchmark.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_InstrumentCurrency.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler edit_ExpectedReturn.TextChanged, AddressOf Me.FormControlChanged
    AddHandler edit_WorstCaseFall.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Check_LimitDisplay.CheckStateChanged, AddressOf Me.FormControlChanged
    AddHandler edit_DealingNoticeDays.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_DealingPeriod.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler edit_DealingBaseDate.TextChanged, AddressOf Me.FormControlChanged
    AddHandler edit_LockupPeriod.TextChanged, AddressOf Me.FormControlChanged
    AddHandler edit_Settlemet_Buy.TextChanged, AddressOf Me.FormControlChanged
    AddHandler edit_FundManagementFees.ValueChanged, AddressOf Me.FormControlChanged
    AddHandler edit_FundPerformanceFees.ValueChanged, AddressOf Me.FormControlChanged
    AddHandler edit_PerformanceHurdle.ValueChanged, AddressOf Me.FormControlChanged
    AddHandler edit_MaxDecimalPlaces.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Date_Cutoff.ValueChanged, AddressOf Me.FormControlChanged
    AddHandler edit_Settlemet_Sell.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Check_PriceCapture.CheckStateChanged, AddressOf Me.FormControlChanged
    AddHandler Check_TradeAsValue.CheckStateChanged, AddressOf Me.FormControlChanged
    AddHandler Check_UpfrontFees.CheckStateChanged, AddressOf Me.FormControlChanged
    AddHandler edit_Penalty.TextChanged, AddressOf Me.FormControlChanged
    AddHandler edit_LiquidityComment.TextChanged, AddressOf Me.FormControlChanged
    AddHandler edit_LatestTradeDate.TextChanged, AddressOf Me.FormControlChanged
    AddHandler edit_ExpiryDate.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Check_ExcludeFromGAV.CheckStateChanged, AddressOf Me.FormControlChanged
    AddHandler Check_ExcludeFromAUM.CheckStateChanged, AddressOf Me.FormControlChanged
    AddHandler Check_IsManagementFee.CheckStateChanged, AddressOf Me.FormControlChanged
    AddHandler Check_IsIncentiveFee.CheckStateChanged, AddressOf Me.FormControlChanged
    AddHandler Check_IsCrystalisedIncentiveFee.CheckStateChanged, AddressOf Me.FormControlChanged
    AddHandler Check_IsMovementCommission.CheckStateChanged, AddressOf Me.FormControlChanged
    AddHandler Check_IsEqualisation.CheckStateChanged, AddressOf Me.FormControlChanged
    AddHandler Check_IsLeverageInstrument.CheckStateChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_LeverageCounterparty.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_ReviewGroup.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler edit_PortfolioTicker.TextChanged, AddressOf Me.FormControlChanged
    AddHandler edit_ISIN.TextChanged, AddressOf Me.FormControlChanged
    AddHandler edit_ISO2_CountryCode.TextChanged, AddressOf Me.FormControlChanged
    AddHandler edit_SEDOL.TextChanged, AddressOf Me.FormControlChanged
    AddHandler edit_Morningstar.TextChanged, AddressOf Me.FormControlChanged
    AddHandler edit_ExchangeCode.TextChanged, AddressOf Me.FormControlChanged
    AddHandler edit_InstrumentMultiplier.ValueChanged, AddressOf Me.FormControlChanged
    AddHandler edit_BondMultiplier.ValueChanged, AddressOf Me.FormControlChanged
    AddHandler edit_CB_ContractSize.ValueChanged, AddressOf Me.FormControlChanged
    AddHandler edit_ContractSize.ValueChanged, AddressOf Me.FormControlChanged

    AddHandler Check_IsFXFuture.CheckStateChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_FXFixedCurrency.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler edit_SharesPerOption.ValueChanged, AddressOf Me.FormControlChanged

    AddHandler Text_ManagerNotes.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Text_ProspectusNotes.TextChanged, AddressOf Me.FormControlChanged

    AddHandler Text_BondIssuer.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Text_BondRanking.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Text_BondGoverningLaw.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Date_BondMaturityDate.ValueChanged, AddressOf Me.FormControlChanged
    AddHandler edit_BondInterestPercentage.ValueChanged, AddressOf Me.FormControlChanged

    AddHandler Text_CB_Issuer.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Text_CB_Ranking.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Text_CB_GoverningLaw.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Date_CB_MaturityDate.ValueChanged, AddressOf Me.FormControlChanged
    AddHandler edit_CB_InterestRate.ValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_CB_Underlying.SelectedValueChanged, AddressOf Me.FormControlChanged

    ' Set up the delays for the ToolTip.
    FormTooltip.AutoPopDelay = 5000
    FormTooltip.InitialDelay = 1000
    FormTooltip.ReshowDelay = 500
    FormTooltip.ShowAlways = False

    ' Set up the ToolTip
    MainForm.SetFormToolTip(Me, FormTooltip)

    ' ******************************************************
    ' End Form Specific.
    ' ******************************************************

    ' Data object names standard to this Form type.

    THIS_TABLENAME = ThisStandardDataset.TableName
    THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
    THIS_DATASETNAME = ThisStandardDataset.DatasetName

    THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

    ' Establish / Retrieve data objects for this form.

    myConnection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)
    myAdaptor = MainForm.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, VENICE_CONNECTION, THIS_TABLENAME)
    myDataset = MainForm.Load_Table(ThisStandardDataset, False)
    myTable = myDataset.Tables(0)

    MainForm.BuildStandardFormMenu(Me.RootMenu, myDataset.Tables(0), AddressOf SelectMenuEvent, AddressOf OrderMenuEvent, AddressOf AuditReportMenuEvent)

    ' Initialise List_Entity
    Dim CharacteristicTable As New RenaissanceDataClass.DSFlorenceEntity.tblFlorenceEntityDataTable
    Me.List_Entity.DataSource = CharacteristicTable
    List_Entity.DisplayMember = "EntityName"
    List_Entity.ValueMember = "EntityID"

  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
  ''' <summary>
  ''' Resets the form.
  ''' </summary>
  Public Sub ResetForm() Implements StandardVeniceForm.ResetForm
    ' *************************************************************
    '
    '
    ' *************************************************************

    THIS_FORM_SelectBy = "InstrumentDescription"
    THIS_FORM_OrderBy = "InstrumentDescription"

    Call Form_Load(Me, New System.EventArgs)
  End Sub

  ''' <summary>
  ''' Closes the form.
  ''' </summary>
  Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
    ' *************************************************************
    '
    '
    ' *************************************************************

    ALWAYS_CLOSE_THIS_FORM = True
    Me.Close()
  End Sub

  ''' <summary>
  ''' Handles the Load event of the Form control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    ' *************************************************************
    '
    '
    ' *************************************************************

    Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
    _FormOpenFailed = False
    _InUse = True

    Try

      ' Initialise Data structures. Connection, Adaptor and Dataset.

      If Not (MainForm Is Nothing) Then
        FormIsValid = True
      Else
        MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        FormIsValid = False
        _FormOpenFailed = True
        Exit Sub
      End If

      If (myConnection Is Nothing) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

        FormIsValid = False
        _FormOpenFailed = True
        Exit Sub
      End If

      If (myAdaptor Is Nothing) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

        FormIsValid = False
        _FormOpenFailed = True
        Exit Sub
      End If

      If (myDataset Is Nothing) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

        FormIsValid = False
        _FormOpenFailed = True
        Exit Sub
      End If

    Catch ex As Exception
    End Try

    Try

      ' Initialse form

      InPaint = True
      IsOverCancelButton = False

      ' Check User permissions
      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

        FormIsValid = False
        _FormOpenFailed = True
        Exit Sub
      End If

      ' 
      InstrumentTabs.Add(Tab_Derivatives.Name, Tab_Derivatives)
      InstrumentTabs.Add(Tab_Bonds.Name, Tab_Bonds)
      InstrumentTabs.Add(Tab_Convertibles.Name, Tab_Convertibles)
      InstrumentTabs.Add(Tab_Unit.Name, Tab_Unit)

      ' Build Sorted data list from which this form operates
      Call SetSortedRows()

      Call SetPertracCombo()
      Call SetInstrumentTypeCombo()
      Call SetInstrumentParentCombo()
      Call SetInstrumentBenchmarkCombo()
      Call SetInstrumentUnderlyingCombo()
      Call SetFundTypeCombo()
      Call SetFundIDCombo()
      Call SetBenchmarkCombo()
      Call SetInstrumentCurrencyCombo()
      Call SetDealingPeriodCombo()
      Call SetEntityCombo()
      Call SetCounterpartyCombo()
      Call SetReviewGroupCombo()
      Call SetAttributionCombos()

      ' Display initial record.

      thisPosition = 0
      If THIS_FORM_SelectingCombo.Items.Count > 0 Then
        Me.THIS_FORM_SelectingCombo.SelectedIndex = 0
        thisDataRow = SortedRows(thisPosition)
        Call GetFormData(thisDataRow)
      Else
        Me.THIS_FORM_SelectingCombo.SelectedIndex = (-1)
        Call GetFormData(Nothing)
      End If

    Catch ex As Exception

      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Initialising Form (Form.Load).", ex.StackTrace, True)

    Finally

      InPaint = False

    End Try

  End Sub

  ''' <summary>
  ''' Handles the Closing event of the frmInstrument control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
  Private Sub frmInstrument_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    Dim HideForm As Boolean

    ' Hide or Close this form ?
    ' All depends on how many of this form type are Open or in Cache...

    _InUse = False

    If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
      HideForm = False
    Else
      If (FormChanged = True) Then
        Call SetFormData()
      End If

      HideForm = True
      If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
        HideForm = False
      End If
    End If

    If HideForm = True Then
      MainForm.HideInFormsCollection(Me)
      Me.Hide() ' NPP Fix

      e.Cancel = True
    Else
      Try
        MainForm.RemoveFromFormsCollection(Me)
        RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

        RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler
        RemoveHandler edit_InstrumentDescription.LostFocus, AddressOf MainForm.Text_NotNull

        ' Form Control Changed events
        RemoveHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
        RemoveHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        'RemoveHandler Combo_PertracIndex.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_InstrumentType.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_InstrumentParent.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_Benckmark.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_InstrumentUnderlying.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_InstrumentFundType.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_FundID.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_PerformanceVsBenchmark.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_InstrumentCurrency.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_DealingPeriod.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_LeverageCounterparty.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_ReviewGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_CB_Underlying.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_AttributionMarket.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
        RemoveHandler Combo_AttributionSector.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
        RemoveHandler Combo_FXFixedCurrency.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

        'RemoveHandler Combo_PertracIndex.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_InstrumentType.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_InstrumentParent.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_Benckmark.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_InstrumentUnderlying.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_InstrumentFundType.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_FundID.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_PerformanceVsBenchmark.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_InstrumentCurrency.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_DealingPeriod.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_LeverageCounterparty.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_ReviewGroup.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_CB_Underlying.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_AttributionMarket.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_AttributionSector.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_FXFixedCurrency.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

        RemoveHandler Combo_PertracIndex.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_InstrumentType.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_InstrumentParent.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_Benckmark.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_InstrumentUnderlying.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_InstrumentFundType.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_FundID.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_PerformanceVsBenchmark.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_InstrumentCurrency.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_DealingPeriod.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_LeverageCounterparty.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_ReviewGroup.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_CB_Underlying.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_AttributionMarket.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_AttributionSector.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_FXFixedCurrency.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

        RemoveHandler Combo_AttributionMarket.SelectedValueChanged, AddressOf FormControlChanged
        RemoveHandler Combo_AttributionSector.SelectedValueChanged, AddressOf FormControlChanged

        RemoveHandler Combo_AttributionMarket.TextChanged, AddressOf FormControlChanged
        RemoveHandler Combo_AttributionSector.TextChanged, AddressOf FormControlChanged

        'RemoveHandler Combo_PertracIndex.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_InstrumentType.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_InstrumentParent.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_Benckmark.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_InstrumentUnderlying.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_InstrumentFundType.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_FundID.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_PerformanceVsBenchmark.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_InstrumentCurrency.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_DealingPeriod.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_LeverageCounterparty.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_ReviewGroup.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_CB_Underlying.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_AttributionMarket.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_AttributionSector.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_FXFixedCurrency.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

        RemoveHandler Combo_AddEntity.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_AddEntity.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_AddEntity.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_AddEntity.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_AddEntity.SelectedValueChanged, AddressOf Me.FormControlChanged

        RemoveHandler edit_InstrumentDescription.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_InstrumentCode.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_PFPCID.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_PertracIndex.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_InstrumentType.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_InstrumentParent.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_Benckmark.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_InstrumentUnderlying.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_InstrumentClass.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_InstrumentFundType.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_FundID.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_PerformanceVsBenchmark.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_InstrumentCurrency.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_ExpectedReturn.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_WorstCaseFall.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_LimitDisplay.CheckStateChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_DealingNoticeDays.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_DealingPeriod.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_DealingBaseDate.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_LockupPeriod.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_Settlemet_Buy.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_FundManagementFees.ValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_FundPerformanceFees.ValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_PerformanceHurdle.ValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_MaxDecimalPlaces.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Date_Cutoff.ValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_Settlemet_Sell.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_PriceCapture.CheckStateChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_TradeAsValue.CheckStateChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_UpfrontFees.CheckStateChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_Penalty.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_LiquidityComment.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_LatestTradeDate.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_ExpiryDate.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_ExcludeFromGAV.CheckStateChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_ExcludeFromAUM.CheckStateChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_IsManagementFee.CheckStateChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_IsIncentiveFee.CheckStateChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_IsCrystalisedIncentiveFee.CheckStateChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_IsMovementCommission.CheckStateChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_IsEqualisation.CheckStateChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_IsLeverageInstrument.CheckStateChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_LeverageCounterparty.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_ReviewGroup.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_PortfolioTicker.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_ISIN.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_ISO2_CountryCode.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_SEDOL.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_Morningstar.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_ExchangeCode.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_InstrumentMultiplier.ValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_BondMultiplier.ValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_CB_ContractSize.ValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_ContractSize.ValueChanged, AddressOf Me.FormControlChanged

        RemoveHandler Check_IsFXFuture.CheckStateChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_FXFixedCurrency.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_SharesPerOption.ValueChanged, AddressOf Me.FormControlChanged

        RemoveHandler Text_ManagerNotes.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Text_ProspectusNotes.TextChanged, AddressOf Me.FormControlChanged

        RemoveHandler Text_BondIssuer.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Text_BondRanking.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Text_BondGoverningLaw.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Date_BondMaturityDate.ValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_BondInterestPercentage.ValueChanged, AddressOf Me.FormControlChanged

        RemoveHandler Text_CB_Issuer.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Text_CB_Ranking.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Text_CB_GoverningLaw.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Date_CB_MaturityDate.ValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler edit_CB_InterestRate.ValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_CB_Underlying.SelectedValueChanged, AddressOf Me.FormControlChanged


      Catch ex As Exception
      End Try
    End If

  End Sub


#End Region

  ' Routine to handle changes / updates to tables by this and other windows.
  ' If this, or any other, form posts a change to a table, then it will invoke an update event 
  ' detailing what tables have been altered.
  ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
  ' the 'VeniceAutoUpdate' event of the main Venice form.
  ' Each form may them react as appropriate to changes in any table that might impact it.
  '
  ''' <summary>
  ''' Autoes the update.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    Dim OrgInPaint As Boolean
    Dim KnowledgeDateChanged As Boolean
    Dim SetButtonStatus_Flag As Boolean

    If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

    OrgInPaint = InPaint
    InPaint = True
    KnowledgeDateChanged = False
    SetButtonStatus_Flag = False

    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
      KnowledgeDateChanged = True
    End If

    ' ****************************************************************
    ' Check for changes relevant to this form
    ' ****************************************************************

    ' Changes to the tblCounterparty table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblCounterparty) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Call SetCounterpartyCombo()
    End If

    ' Changes to the tblCurrency table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblCurrency) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Call SetInstrumentCurrencyCombo()
    End If

    ' Changes to the tblDealingPeriod table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblDealingPeriod) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Call SetDealingPeriodCombo()
    End If

    ' Changes to the tblFlorenceEntity table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFlorenceEntity) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Call SetEntityCombo()
    End If

    ' Changes to the tblFundType table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFundType) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Call SetFundTypeCombo()
    End If

    ' Changes to the tblFund table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFund) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Call SetFundIDCombo()
    End If

    ' Changes to the tblBenchmarkIndex table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblBenchmarkIndex) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Call SetBenchmarkCombo()
    End If

    ' Changes to the tblInstrument table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrument) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Call SetInstrumentParentCombo()
      Call SetInstrumentUnderlyingCombo()
      Call SetAttributionCombos()
    End If

    ' Changes to the tblInstrument table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrumentNotes)) AndAlso (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrument) = False) Then

      ' Re-Set combo.
      Call GetNotesData(thisAuditID)
    End If

    ' Changes to the tblInstrumentType table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrumentType) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Call SetInstrumentTypeCombo()
    End If

    ' Changes to the tblGroupList table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGroupList) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Call SetReviewGroupCombo()
    End If

    ' Changes to the Mastername table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Mastername) = True) Then

      ' ReBuild the Pertrac-Mastername derived combo

      Call SetPertracCombo()
      Call SetInstrumentBenchmarkCombo()

    End If

    ' Changes to the KnowledgeDate :-
    If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
      SetButtonStatus_Flag = True
    End If

    ' Changes to the tblUserPermissions table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

      ' Check ongoing permissions.

      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

        FormIsValid = False
        Me.Close()
        Exit Sub
      End If

      SetButtonStatus_Flag = True

    End If


    ' ****************************************************************
    ' Changes to the Main FORM table :-
    ' ****************************************************************

    If (e.TableChanged(THIS_FORM_ChangeID) = True) Or KnowledgeDateChanged Then

      ' Re-Set Controls etc.
      Call SetSortedRows()

      ' Move again to the correct item
      thisPosition = Get_Position(thisAuditID)

      ' Validate current position.
      If thisPosition >= 0 And thisPosition < Me.SortedRows.GetLength(0) Then
        thisDataRow = Me.SortedRows(thisPosition)
      Else
        If (Me.SortedRows.GetLength(0) > 0) And (AddNewRecord = False) Then
          thisPosition = 0
          thisDataRow = Me.SortedRows(thisPosition)
          FormChanged = False
        Else
          thisDataRow = Nothing
        End If
      End If

      ' Set SelectingCombo Index.
      If (Me.THIS_FORM_SelectingCombo.Items.Count <= 0) Then
        Try
          Me.THIS_FORM_SelectingCombo.SelectedIndex = -1
        Catch ex As Exception
        End Try
      ElseIf (thisPosition < 0) Then
        Try
          MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
        Catch ex As Exception
        End Try
      Else
        Try
          Me.THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
        Catch ex As Exception
        End Try
      End If

      InPaint = OrgInPaint

      If (FormChanged = False) And (AddNewRecord = False) Then
        GetFormData(thisDataRow) ' Includes a call to 'SetButtonStatus()'
        SetButtonStatus_Flag = False
      End If
    End If

    ' ****************************************************************
    ' Repaint if not currently in Edit Mode
    '
    ' ****************************************************************

    'InPaint = OrgInPaint

    '' Retrieve form data if appropriate, i.e. this data item is not already being edited.
    'If (FormChanged = False) And (AddNewRecord = False) Then
    '  GetFormData(thisDataRow) ' Includes a call to 'SetButtonStatus()'
    'Else
    '  If SetButtonStatus_Flag Then
    '    Call SetButtonStatus()
    '  End If
    'End If

    InPaint = OrgInPaint

    ' Retrieve form data if appropriate, i.e. this data item is not already being edited.
    If SetButtonStatus_Flag Then
      Call SetButtonStatus()
    End If

  End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

  ''' <summary>
  ''' Handles the Click event of the Button_DeleteChild control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Button_DeleteChild_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_DeleteEntity.Click
    ' ***********************************************************************************
    ' Remove Selected Items from the Characteristics List
    '
    ' A temporary array of Selected Rows must be created because of the way collections work.
    ' ***********************************************************************************

    Try
      If Me.List_Entity.SelectedItems.Count > 0 Then
        Dim ListTable As RenaissanceDataClass.DSFlorenceEntity.tblFlorenceEntityDataTable
        Dim SelectedRow As DataRowView
        Dim SelectedRows(List_Entity.SelectedItems.Count - 1) As DataRow
        Dim RowCount As Integer = 0

        ListTable = CType(List_Entity.DataSource, RenaissanceDataClass.DSFlorenceEntity.tblFlorenceEntityDataTable)

        For Each SelectedRow In List_Entity.SelectedItems
          SelectedRows(RowCount) = (SelectedRow.Row)
          RowCount += 1
        Next

        For RowCount = 0 To (SelectedRows.Length - 1)
          ListTable.Rows.Remove(SelectedRows(RowCount))
        Next

        List_Entity.SelectedIndex = (-1)

        Call FormControlChanged(Nothing, Nothing)

      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error removing Item(s) from Entity Items.", ex.StackTrace, True)
    End Try

  End Sub

  ''' <summary>
  ''' Handles the Click event of the Button_AddChild control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Button_AddChild_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_AddEntity.Click
    ' ****************************************************************
    '
    '
    ' ****************************************************************

    ' ***********************************************************************************
    ' Add Selected Item  to the List_Entity List
    '
    ' Do not Duplicate Items.
    ' ***********************************************************************************

    Try
      If Me.Combo_AddEntity.SelectedValue > 0 Then
        Me.FormChanged = True

        Dim ListTable As RenaissanceDataClass.DSFlorenceEntity.tblFlorenceEntityDataTable

        ' Get pointer to the List_Entity List Table

        ListTable = CType(Me.List_Entity.DataSource, RenaissanceDataClass.DSFlorenceEntity.tblFlorenceEntityDataTable)

        ' Add selected Characteristic (if it does not already exist) to the List_Entity List

        If GroupContainsID(CInt(Combo_AddEntity.SelectedValue)) = False Then

          Dim EntityRow As RenaissanceDataClass.DSFlorenceEntity.tblFlorenceEntityRow
          Dim ThisRow As RenaissanceDataClass.DSFlorenceEntity.tblFlorenceEntityRow

          EntityRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFlorenceEntity, CInt(Combo_AddEntity.SelectedValue))

          ' Construct a new Item Row.

          ThisRow = ListTable.NewRow
          ThisRow.EntityID = EntityRow.EntityID
          ThisRow.EntityName = EntityRow.EntityName
          ThisRow.ItemGroupID = 0
          ThisRow.EntityIsInactive = False
          ThisRow.AdminContact = ""
          ThisRow.AdminEMail = ""
          ThisRow.AdminEmailSalutation = ""
          ThisRow.AdminFax = ""
          ThisRow.AdminPhone = ""

          ListTable.Rows.Add(ThisRow)

          Call FormControlChanged(Nothing, Nothing)
        End If

        List_Entity.SelectedIndex = (-1)

        SetButtonStatus()
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Adding to Group Items List.", ex.StackTrace, True)
    End Try

  End Sub

  ''' <summary>
  ''' Groups the contains ID.
  ''' </summary>
  ''' <param name="pEntityId">The p entity id.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function GroupContainsID(ByVal pEntityId As Integer) As Boolean
    ' ***********************************************************************************
    ' Function to check if the List_Entity List contains a given Entity.
    '
    '
    ' ***********************************************************************************

    Dim thisRow As RenaissanceDataClass.DSFlorenceEntity.tblFlorenceEntityRow
    Dim ListTable As New RenaissanceDataClass.DSFlorenceEntity.tblFlorenceEntityDataTable

    Try
      If (List_Entity.DataSource Is Nothing) Then
        Return False
      End If

      ListTable = CType(List_Entity.DataSource, RenaissanceDataClass.DSFlorenceEntity.tblFlorenceEntityDataTable)

      For Each thisRow In ListTable
        If thisRow.EntityID = pEntityId Then
          Return True
        End If
      Next

    Catch ex As Exception

    End Try

    Return False
  End Function

  ' Build Sorted list from the Source dataset and update the Select Combo
  ''' <summary>
  ''' Sets the sorted rows.
  ''' </summary>
  Private Sub SetSortedRows()

    Dim OrgInPaint As Boolean

    ' Form Specific Selection Combo :-
    If (THIS_FORM_SelectingCombo Is Nothing) Then Exit Sub

    ' Code is pretty Generic form here on...

    ' Set paint local so that changes to the selection combo do not trigger form updates.

    OrgInPaint = InPaint
    InPaint = True

    ' Get selected Row sets, or exit if no data is present.
    If myDataset Is Nothing Then
      ReDim SortedRows(0)
      ReDim SelectBySortedRows(0)
    Else
      SortedRows = myTable.Select("RN >= 0", THIS_FORM_OrderBy)
      SelectBySortedRows = myTable.Select("RN >= 0", THIS_FORM_SelectBy)
    End If

    ' Set Combo data source
    THIS_FORM_SelectingCombo.DataSource = SortedRows
    THIS_FORM_SelectingCombo.DisplayMember = THIS_FORM_SelectBy
    THIS_FORM_SelectingCombo.ValueMember = THIS_FORM_ValueMember

    ' Ensure that the Selection Combo's DrowDownWidth is wide enough to take the required strings

    'thisDrowDownWidth = THIS_FORM_SelectingCombo.DropDownWidth
    'SizingBitmap = New Bitmap(1, 1, System.Drawing.Imaging.PixelFormat.Format32bppArgb)
    'SizingGraphics = Graphics.FromImage(SizingBitmap)

    'For Each thisrow In SortedRows

    '  ' Compute the string dimensions in the given font
    '  Dim stringSize As SizeF = SizingGraphics.MeasureString(thisrow(THIS_FORM_SelectBy).ToString, THIS_FORM_SelectingCombo.Font)
    '  If (stringSize.Width) > thisDrowDownWidth Then
    '    thisDrowDownWidth = Cint(stringSize.Width)
    '  End If
    'Next

    'THIS_FORM_SelectingCombo.DropDownWidth = thisDrowDownWidth

    MainForm.SetComboDropDownWidth(THIS_FORM_SelectingCombo)

    InPaint = OrgInPaint
  End Sub


  ' Check User permissions
  ''' <summary>
  ''' Checks the permissions.
  ''' </summary>
  Private Sub CheckPermissions()

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

  End Sub

  ' Flag changes to form controls, Event Associations made in 'New' routine.
  ''' <summary>
  ''' Basic event handler, called when a form value is changed.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    Try
      If InPaint = False Then
        If (Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord) Then
          FormChanged = True

          Me.btnSave.Enabled = True
          Me.btnCancel.Enabled = True
        End If

        'If (sender Is Combo_InstrumentType) Then
        '  If CType(Nz(Combo_InstrumentType.SelectedValue, 0), InstrumentTypes) = InstrumentTypes.Unit Then
        '    Combo_FundID.Visible = True
        '    Label_FundID.Visible = True
        '  Else
        '    Combo_FundID.Visible = False
        '    Label_FundID.Visible = False
        '  End If
        'End If

        If (sender Is Text_ManagerNotes) Then
          Notes_ManagerNoteChanged = True
        End If

        If (sender Is Text_ProspectusNotes) Then
          Notes_ProspectusNoteChanged = True
        End If

      End If

      ' If the control that has changed is the Instrument Type control, then ....
      If (sender Is Combo_InstrumentType) Then
        Call SetInstrumentTabVisibilities()
      End If
    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Selects the menu event.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
  Public Sub SelectMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
    ' Event handler for the Select Menu
    ' Event association is made during the dynamic menu creation

    THIS_FORM_SelectBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

    Call SetSortedRows()

  End Sub

  ''' <summary>
  ''' Orders the menu event.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
  Public Sub OrderMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
    ' Event handler for the Order Menu
    ' Event association is made during the dynamic menu creation

    THIS_FORM_OrderBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

    Call SetSortedRows()

  End Sub


  ''' <summary>
  ''' Audits the report menu event.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
  Public Sub AuditReportMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
    ' Event handler for the Audit Report Menu
    ' Event association is made during the dynamic menu creation

    Try
      Select Case CInt(CType(sender, System.Windows.Forms.ToolStripMenuItem).Tag)
        Case 0 ' This Record
          If (Me.AddNewRecord = False) AndAlso (thisAuditID >= 0) Then
            Run_AuditReport_ThisRecord(MainForm, ThisStandardDataset, thisAuditID, MainForm.VeniceProgressBar)
          End If

        Case 1 ' All Records
          Run_AuditReport_ALLRecords(MainForm, ThisStandardDataset, MainForm.VeniceProgressBar)

      End Select
    Catch ex As Exception
      Call MainForm.LogError(Me.Name & ", AuditReportMenuEvent()", 0, ex.Message, "Error calling Audit report.", ex.StackTrace, True)
    End Try

  End Sub




#End Region

#Region " Set Form Combos (Form Specific Code) "

  ''' <summary>
  ''' Sets the pertrac combo.
  ''' </summary>
  Private Sub SetPertracCombo()

    'Combo_PertracIndex.MasternameCollection = MainForm.MasternameDictionary
    'Combo_PertracIndex.SelectNoMatch = False

  End Sub

  ''' <summary>
  ''' Sets the instrument type combo.
  ''' </summary>
  Private Sub SetInstrumentTypeCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_InstrumentType, _
    RenaissanceStandardDatasets.tblInstrumentType, _
    "InstrumentTypeDescription", _
    "InstrumentTypeID", _
    "")

  End Sub

  ''' <summary>
  ''' Sets the instrument parent combo.
  ''' </summary>
  Private Sub SetInstrumentParentCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_InstrumentParent, _
    RenaissanceStandardDatasets.tblInstrument, _
    "InstrumentDescription", _
    "InstrumentID", _
    "", False, True, True)  ' 

  End Sub

  ''' <summary>
  ''' Sets the instrument benchmark combo.
  ''' </summary>
  Private Sub SetInstrumentBenchmarkCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_Benckmark, _
    RenaissanceStandardDatasets.Mastername, _
    "Mastername", _
    "ID", _
    "", False, True, True, 0)  ' 

  End Sub

  ''' <summary>
  ''' Sets the instrument underlying combo.
  ''' </summary>
  Private Sub SetInstrumentUnderlyingCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_InstrumentUnderlying, _
    RenaissanceStandardDatasets.tblInstrument, _
    "InstrumentDescription", _
    "InstrumentID", _
    "", False, True, True)  ' 

  End Sub

  ''' <summary>
  ''' Sets the fund type combo.
  ''' </summary>
  Private Sub SetFundTypeCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_InstrumentFundType, _
    RenaissanceStandardDatasets.tblFundType, _
    "FundTypeDescription", _
    "FundTypeID", _
    "")

  End Sub


  ''' <summary>
  ''' Sets the fund ID combo.
  ''' </summary>
  Private Sub SetFundIDCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_FundID, _
    RenaissanceStandardDatasets.tblFund, _
    "FundName", _
    "FundID", _
    "", False, True, True, 0)

  End Sub

  ''' <summary>
  ''' Sets the benchmark combo.
  ''' </summary>
  Private Sub SetBenchmarkCombo()
    ' Combo_PerformanceVsBenchmark

    Call MainForm.SetTblGenericCombo( _
     Me.Combo_PerformanceVsBenchmark, _
     RenaissanceStandardDatasets.tblBenchmarkIndex, _
     "BenchmarkDescription", _
     "BenchmarkID", _
     "", False, True, True, 0)
  End Sub

  ''' <summary>
  ''' Sets the instrument currency combo.
  ''' </summary>
  Private Sub SetInstrumentCurrencyCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_InstrumentCurrency, _
    RenaissanceStandardDatasets.tblCurrency, _
    "CurrencyCode", _
    "CurrencyID", _
    "")   ' 

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_FXFixedCurrency, _
    RenaissanceStandardDatasets.tblCurrency, _
    "CurrencyCode", _
    "CurrencyID", _
    "", False, True, True, 0)   ' 

  End Sub


  ''' <summary>
  ''' Sets the dealing period combo.
  ''' </summary>
  Private Sub SetDealingPeriodCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_DealingPeriod, _
    RenaissanceStandardDatasets.tblDealingPeriod, _
    "DealingPeriodDescription", _
    "DealingPeriodID", _
    "")

  End Sub

  ''' <summary>
  ''' Sets the entity combo.
  ''' </summary>
  Private Sub SetEntityCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_AddEntity, _
    RenaissanceStandardDatasets.tblFlorenceEntity, _
    "EntityName", _
    "EntityID", _
    "", False, True, False, 0)  ' 

  End Sub

  ''' <summary>
  ''' Sets the counterparty combo.
  ''' </summary>
  Private Sub SetCounterpartyCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_LeverageCounterparty, _
    RenaissanceStandardDatasets.tblCounterparty, _
    "CounterpartyName", _
    "CounterpartyID", _
    "")   ' 

  End Sub

  ''' <summary>
  ''' Sets the review group combo.
  ''' </summary>
  Private Sub SetReviewGroupCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_ReviewGroup, _
    RenaissanceStandardDatasets.tblGroupList, _
    "GroupListName", _
    "GroupListID", _
    "true", False, True, True, 0, "") ' 

  End Sub

  Private Sub SetAttributionCombos()


    Call MainForm.SetTblGenericCombo( _
    Me.Combo_AttributionMarket, _
    RenaissanceStandardDatasets.tblInstrument, _
    "AttributionMarket", _
    "AttributionMarket", _
    "", True, True, False)

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_AttributionSector, _
    RenaissanceStandardDatasets.tblInstrument, _
    "AttributionSector", _
    "AttributionSector", _
    "", True, True, False)

  End Sub

#End Region

#Region " Get & Set Form Data / SetButton / ValidateForm / btnCancel Events (Form Specific Code) "

  ''' <summary>
  ''' Gets the form data.
  ''' </summary>
  ''' <param name="ThisRow">The this row.</param>
  Private Sub GetFormData(ByRef ThisRow As RenaissanceDataClass.DSInstrument.tblInstrumentRow)
    ' Routine to populate form Controls given a data row.
    ' Form is cleared for invalid Datarows or Invalid Form.

    Dim OrgInpaint As Boolean

    ' Set 'Paint' flag to prevent event actions caused by changing the value of form fields.
    OrgInpaint = InPaint
    InPaint = True


    If (ThisRow Is Nothing) OrElse ((ThisRow.RowState And DataRowState.Detached) = DataRowState.Detached) OrElse (FormIsValid = False) OrElse (Me.InUse = False) Then
      ' Bad / New Datarow - Clear Form.

      thisAuditID = (-1)

      Me.editAuditID.Text = ""

      Me.edit_InstrumentDescription.Text = ""
      Me.edit_InstrumentCode.Text = ""

      Me.edit_PFPCID.Text = ""
      Me.edit_PortfolioTicker.Text = ""
      Me.edit_SEDOL.Text = ""
      Me.edit_Morningstar.Text = ""
      Me.edit_ISIN.Text = ""
      Me.edit_ISO2_CountryCode.Text = ""
      Me.edit_ExchangeCode.Text = ""

      MainForm.ClearComboSelection(Combo_PertracIndex)
      MainForm.ClearComboSelection(Combo_InstrumentType)
      MainForm.ClearComboSelection(Combo_InstrumentParent)
      MainForm.ClearComboSelection(Combo_Benckmark)
      MainForm.ClearComboSelection(Combo_InstrumentUnderlying)
      MainForm.ClearComboSelection(Combo_AttributionMarket)
      MainForm.ClearComboSelection(Combo_AttributionSector)

      Me.edit_InstrumentClass.Text = ""

      MainForm.ClearComboSelection(Combo_InstrumentFundType)
      MainForm.ClearComboSelection(Combo_FundID)
      MainForm.ClearComboSelection(Combo_PerformanceVsBenchmark)
      MainForm.ClearComboSelection(Combo_InstrumentCurrency)

      Call SetInstrumentTabVisibilities()

      'Combo_FundID.Visible = False
      'Label_FundID.Visible = False

      Me.edit_ExpectedReturn.Value = 0
      Me.edit_WorstCaseFall.Value = 0
      Me.Check_LimitDisplay.CheckState = CheckState.Unchecked
      Me.edit_DealingNoticeDays.Value = 0

      Try
        Combo_DealingPeriod.SelectedValue = DealingPeriod.Daily
      Catch ex As Exception
      End Try

      Me.edit_InstrumentMultiplier.Value = 1.0#
      Me.edit_BondMultiplier.Value = 1.0#
      Me.edit_CB_ContractSize.Value = 1.0#
      Me.edit_ContractSize.Value = 1.0#
      Me.edit_DealingBaseDate.Value = System.DBNull.Value
      Me.edit_LockupPeriod.Value = 0

      Check_IsFXFuture.Checked = False
      Combo_FXFixedCurrency.SelectedValue = 0
      edit_SharesPerOption.Value = 1.0#

      edit_Settlemet_Buy.Value = 3
      edit_MaxDecimalPlaces.Value = 4
      edit_Settlemet_Sell.Value = 3
      Check_PriceCapture.Checked = False
      Check_TradeAsValue.Checked = False
      Date_Cutoff.Value = Renaissance_BaseDate.AddHours(12)

      edit_FundManagementFees.Value = 0.0#
      edit_FundPerformanceFees.Value = 0.0#
      edit_PerformanceHurdle.Value = 0.0#

      Me.Check_UpfrontFees.CheckState = CheckState.Unchecked
      Me.edit_Penalty.Value = 0
      Me.edit_LiquidityComment.Text = ""
      Me.edit_LatestTradeDate.Value = System.DBNull.Value
      Me.edit_ExpiryDate.Value = System.DBNull.Value
      Me.Check_ExcludeFromGAV.CheckState = CheckState.Unchecked
      Me.Check_ExcludeFromAUM.CheckState = CheckState.Unchecked
      Me.Check_IsManagementFee.CheckState = CheckState.Unchecked
      Me.Check_IsIncentiveFee.CheckState = CheckState.Unchecked
      Me.Check_IsCrystalisedIncentiveFee.CheckState = CheckState.Unchecked
      Me.Check_IsMovementCommission.CheckState = CheckState.Unchecked
      Me.Check_IsEqualisation.CheckState = CheckState.Unchecked
      Me.Check_IsLeverageInstrument.CheckState = CheckState.Unchecked

      Text_BondIssuer.Text = ""
      Text_BondRanking.Text = ""
      Text_BondGoverningLaw.Text = ""
      Date_BondMaturityDate.Value = Now.Date
      edit_BondInterestPercentage.Value = 0.0#

      Text_CB_Issuer.Text = ""
      Text_CB_Ranking.Text = ""
      Text_CB_GoverningLaw.Text = ""
      Date_CB_MaturityDate.Value = Now.Date
      edit_CB_InterestRate.Value = 0.0#

      MainForm.ClearComboSelection(Combo_LeverageCounterparty)
      MainForm.ClearComboSelection(Combo_ReviewGroup)
      MainForm.ClearComboSelection(Combo_CB_Underlying)

      CType(List_Entity.DataSource, RenaissanceDataClass.DSFlorenceEntity.tblFlorenceEntityDataTable).Rows.Clear()

      If AddNewRecord = True Then
        Me.btnCancel.Enabled = True
        Me.THIS_FORM_SelectingCombo.Enabled = False
      Else
        Me.btnCancel.Enabled = False
        Me.THIS_FORM_SelectingCombo.Enabled = True
      End If

    Else

      ' Populate Form with given data.
      Try
        Dim ThisInstrumentType As InstrumentTypes

        thisAuditID = thisDataRow.AuditID

        Me.editAuditID.Text = thisDataRow.AuditID.ToString

        Me.edit_InstrumentDescription.Text = thisDataRow.InstrumentDescription
        Me.edit_PortfolioTicker.Text = thisDataRow.InstrumentPortfolioTicker
        Me.edit_SEDOL.Text = thisDataRow.InstrumentSEDOL
        Me.edit_Morningstar.Text = Nz(thisDataRow.InstrumentMorningstar, "")
        Me.edit_ISIN.Text = thisDataRow.InstrumentISIN
        Me.edit_ISO2_CountryCode.Text = thisDataRow.InstrumentCountryISO2
        Me.edit_ExchangeCode.Text = thisDataRow.InstrumentExchangeCode

        If (thisDataRow.IsInstrumentCodeNull) Then
          Me.edit_InstrumentCode.Text = ""
        Else
          Me.edit_InstrumentCode.Text = thisDataRow.InstrumentCode
        End If

        Me.edit_PFPCID.Text = thisDataRow.InstrumentPFPCID

        If (thisDataRow.IsInstrumentPertracCodeNull = False) AndAlso (thisDataRow.InstrumentPertracCode > 0) Then
          Me.Combo_PertracIndex.SelectedValue = thisDataRow.InstrumentPertracCode
        Else
          Combo_PertracIndex.SelectedValue = Nothing ' MainForm.ClearComboSelection(Combo_PertracIndex)
        End If

        Me.Combo_InstrumentType.SelectedValue = thisDataRow.InstrumentType

        Call SetInstrumentTabVisibilities()

        ThisInstrumentType = CType(thisDataRow.InstrumentType, InstrumentTypes)

        Me.edit_InstrumentMultiplier.Value = thisDataRow.InstrumentMultiplier
        Me.edit_BondMultiplier.Value = thisDataRow.InstrumentContractSize
        Me.edit_CB_ContractSize.Value = thisDataRow.InstrumentContractSize
        Me.edit_ContractSize.Value = thisDataRow.InstrumentContractSize

        Check_IsFXFuture.Checked = thisDataRow.InstrumentIsFXFuture
        Combo_FXFixedCurrency.SelectedValue = thisDataRow.InstrumentFXFutureConstantCurrency
        edit_SharesPerOption.Value = thisDataRow.InstrumentSharesPerOption

        If (thisDataRow.IsInstrumentParentNull = False) AndAlso (thisDataRow.InstrumentParent > 0) AndAlso (thisDataRow.InstrumentParent <> thisDataRow.InstrumentID) Then
          Me.Combo_InstrumentParent.SelectedValue = thisDataRow.InstrumentParent
        Else
          MainForm.ClearComboSelection(Combo_InstrumentParent)
        End If

        If (thisDataRow.IsInstrumentUnderlyingNull = False) AndAlso (thisDataRow.InstrumentUnderlying > 0) AndAlso (thisDataRow.InstrumentUnderlying <> thisDataRow.InstrumentID) Then
          Me.Combo_InstrumentUnderlying.SelectedValue = thisDataRow.InstrumentUnderlying
          Combo_CB_Underlying.SelectedValue = thisDataRow.InstrumentUnderlying
        Else
          MainForm.ClearComboSelection(Combo_InstrumentUnderlying)
          MainForm.ClearComboSelection(Combo_CB_Underlying)
        End If

        If (thisDataRow.IsInstrumentBenchmarkNull = False) AndAlso (thisDataRow.InstrumentBenchmark > 0) Then
          Me.Combo_Benckmark.SelectedValue = thisDataRow.InstrumentBenchmark
        Else
          MainForm.ClearComboSelection(Combo_Benckmark)
        End If

        If (thisDataRow.IsInstrumentClassNull) Then
          Me.edit_InstrumentClass.Text = ""
        Else
          Me.edit_InstrumentClass.Text = thisDataRow.InstrumentClass
        End If

        Me.Combo_InstrumentFundType.SelectedValue = thisDataRow.InstrumentFundType
        Me.Combo_FundID.SelectedValue = thisDataRow.InstrumentFundID
        Me.Combo_PerformanceVsBenchmark.SelectedValue = thisDataRow.InstrumentUnitPerformanceBenchmark
        Me.Combo_InstrumentCurrency.SelectedValue = thisDataRow.InstrumentCurrencyID
        Me.edit_ExpectedReturn.Value = thisDataRow.InstrumentExpectedReturn
        Me.edit_WorstCaseFall.Value = thisDataRow.InstrumentWorstCase

        edit_FundManagementFees.Value = thisDataRow.InstrumentUnitManagementFees
        edit_FundPerformanceFees.Value = thisDataRow.InstrumentUnitPerformanceFees
        edit_PerformanceHurdle.Value = thisDataRow.InstrumentUnitPerformanceHurdle

        'If CType(thisDataRow.InstrumentType, InstrumentTypes) = InstrumentTypes.Unit Then
        '  Combo_FundID.Visible = True
        '  Label_FundID.Visible = True
        'Else
        '  Combo_FundID.Visible = False
        '  Label_FundID.Visible = False
        'End If

        If (thisDataRow.InstrumentLimitDisplay = True) Then
          Me.Check_LimitDisplay.CheckState = CheckState.Checked
        Else
          Me.Check_LimitDisplay.CheckState = CheckState.Unchecked
        End If

        Me.edit_DealingNoticeDays.Value = thisDataRow.InstrumentNoticeDays

        Me.Combo_DealingPeriod.SelectedValue = thisDataRow.InstrumentDealingPeriod

        If (thisDataRow.IsInstrumentDealingBaseDateNull) OrElse (thisDataRow.InstrumentDealingBaseDate <= KNOWLEDGEDATE_NOW) Then
          Me.edit_DealingBaseDate.Value = System.DBNull.Value
        Else
          Me.edit_DealingBaseDate.Value = thisDataRow.InstrumentDealingBaseDate
        End If

        Me.edit_LockupPeriod.Value = thisDataRow.InstrumentLockupPeriod
        edit_Settlemet_Buy.Value = Nz(thisDataRow.InstrumentDefaultSettlementDays_Buy, 0)
        edit_MaxDecimalPlaces.Value = Math.Round(Nz(thisDataRow.InstrumentOrderPrecision, 4))
        edit_Settlemet_Sell.Value = Nz(thisDataRow.InstrumentDefaultSettlementDays_Sell, 0)
        Check_PriceCapture.Checked = thisDataRow.InstrumentCapturePrice
        Check_TradeAsValue.Checked = thisDataRow.InstrumentOrderOnlyByValue
        Date_Cutoff.Value = Renaissance_BaseDate.AddSeconds(thisDataRow.InstrumentDealingCutOffTime)

        If thisDataRow.InstrumentUpfrontfees = 0 Then
          Me.Check_UpfrontFees.CheckState = CheckState.Unchecked
        Else
          Me.Check_UpfrontFees.CheckState = CheckState.Checked
        End If

        Me.edit_Penalty.Value = thisDataRow.InstrumentPenalty

        If (thisDataRow.IsInstrumentLiquidityCommentNull) Then
          Me.edit_LiquidityComment.Text = ""
        Else
          Me.edit_LiquidityComment.Text = thisDataRow.InstrumentLiquidityComment
        End If

        If thisDataRow.InstrumentLastValidTradeDate <= KNOWLEDGEDATE_NOW Then
          Me.edit_LatestTradeDate.Value = System.DBNull.Value
        Else
          Me.edit_LatestTradeDate.Value = thisDataRow.InstrumentLastValidTradeDate
        End If

        If thisDataRow.InstrumentExpiryDate <= KNOWLEDGEDATE_NOW Then
          Me.edit_ExpiryDate.Value = System.DBNull.Value
          Date_BondMaturityDate.Value = Renaissance_EndDate_Data
          Date_CB_MaturityDate.Value = Renaissance_EndDate_Data
        Else
          Me.edit_ExpiryDate.Value = thisDataRow.InstrumentExpiryDate
          Date_BondMaturityDate.Value = thisDataRow.InstrumentExpiryDate
          Date_CB_MaturityDate.Value = thisDataRow.InstrumentExpiryDate
        End If

        If (thisDataRow.InstrumentExcludeGAV = 0) Then
          Me.Check_ExcludeFromGAV.CheckState = CheckState.Unchecked
        Else
          Me.Check_ExcludeFromGAV.CheckState = CheckState.Checked
        End If

        If (thisDataRow.InstrumentExcludeAUM = 0) Then
          Me.Check_ExcludeFromAUM.CheckState = CheckState.Unchecked
        Else
          Me.Check_ExcludeFromAUM.CheckState = CheckState.Checked
        End If

        If (thisDataRow.InstrumentIsManagementFee = 0) Then
          Me.Check_IsManagementFee.CheckState = CheckState.Unchecked
        Else
          Me.Check_IsManagementFee.CheckState = CheckState.Checked
        End If

        If (thisDataRow.InstrumentIsIncentiveFee = 0) Then
          Me.Check_IsIncentiveFee.CheckState = CheckState.Unchecked
        Else
          Me.Check_IsIncentiveFee.CheckState = CheckState.Checked
        End If

        If (thisDataRow.InstrumentIsCrystalisedIncentive = 0) Then
          Me.Check_IsCrystalisedIncentiveFee.CheckState = CheckState.Unchecked
        Else
          Me.Check_IsCrystalisedIncentiveFee.CheckState = CheckState.Checked
          Me.Check_IsIncentiveFee.CheckState = CheckState.Checked
        End If

        If (thisDataRow.InstrumentIsMovementCommission = 0) Then
          Me.Check_IsMovementCommission.CheckState = CheckState.Unchecked
        Else
          Me.Check_IsMovementCommission.CheckState = CheckState.Checked
        End If

        If (thisDataRow.InstrumentIsEqualisation = 0) Then
          Me.Check_IsEqualisation.CheckState = CheckState.Unchecked
        Else
          Me.Check_IsEqualisation.CheckState = CheckState.Checked
        End If

        If (thisDataRow.InstrumentIsLeverageInstrument = 0) Then
          Me.Check_IsLeverageInstrument.CheckState = CheckState.Unchecked
          MainForm.ClearComboSelection(Combo_LeverageCounterparty)
        Else
          Me.Check_IsLeverageInstrument.CheckState = CheckState.Checked

          If (thisDataRow.InstrumentLeverageCounterparty > 0) And (thisDataRow.InstrumentLeverageCounterparty <> thisAuditID) Then
            Me.Combo_LeverageCounterparty.SelectedValue = thisDataRow.InstrumentLeverageCounterparty
          Else
            MainForm.ClearComboSelection(Combo_LeverageCounterparty)
          End If
        End If

        If (thisDataRow.InstrumentReviewGroup > 0) Then
          Me.Combo_ReviewGroup.SelectedValue = thisDataRow.InstrumentReviewGroup
        Else
          If (Combo_ReviewGroup.Items.Count > 0) Then
            Combo_ReviewGroup.SelectedIndex = 0
          End If
        End If

        ' Attribution Combos

        Combo_AttributionMarket.Text = thisDataRow.AttributionMarket
        Combo_AttributionSector.Text = thisDataRow.AttributionSector

        ' Bonds & CB fields

        Text_BondIssuer.Text = ""
        Text_BondRanking.Text = ""
        Text_BondGoverningLaw.Text = ""
        ' Date_BondMaturityDate.Value = Now.Date
        edit_BondInterestPercentage.Value = 0.0#

        Text_CB_Issuer.Text = ""
        Text_CB_Ranking.Text = ""
        Text_CB_GoverningLaw.Text = ""
        ' Date_CB_MaturityDate.Value = Now.Date
        edit_CB_InterestRate.Value = 0.0#

        ' Populate Entity List with data.
        Dim tmpCommand As New SqlCommand
        Dim ListTable As RenaissanceDataClass.DSFlorenceEntity.tblFlorenceEntityDataTable

        Try

          tmpCommand.CommandType = CommandType.StoredProcedure
          tmpCommand.CommandText = "adp_tblInstrumentFlorenceEntityLink_SelectGroup"
          tmpCommand.Connection = MainForm.GetVeniceConnection
          tmpCommand.Parameters.Add("@InstrumentID", SqlDbType.Int).Value = thisDataRow.InstrumentID
          tmpCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime).Value = MainForm.Main_Knowledgedate
          tmpCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

          List_Entity.SuspendLayout()
          ListTable = CType(List_Entity.DataSource, RenaissanceDataClass.DSFlorenceEntity.tblFlorenceEntityDataTable)
          ListTable.Rows.Clear()
          MainForm.LoadTable_Custom(ListTable, tmpCommand)
          'ListTable.Load(tmpCommand.ExecuteReader)
          List_Entity.ResumeLayout()

          ' MainForm.SetComboSelectionLengths(Me, THIS_FORM_SelectingCombo)

          Me.btnCancel.Enabled = False
          Me.THIS_FORM_SelectingCombo.Enabled = True

        Catch ex As Exception

          Call MainForm.LogError(Me.Name & ", GetFormData", 0, ex.Message, "Error Showing Data", ex.StackTrace, True)
          Call GetFormData(Nothing)

        Finally
          Try
            If (tmpCommand IsNot Nothing) AndAlso (tmpCommand.Connection IsNot Nothing) Then
              tmpCommand.Connection.Close()
              tmpCommand.Connection = Nothing
            End If
          Catch ex As Exception
          End Try
        End Try



        AddNewRecord = False
        ' MainForm.SetComboSelectionLengths(Me)

        Me.btnCancel.Enabled = False
        Me.THIS_FORM_SelectingCombo.Enabled = True

      Catch ex As Exception

        Call MainForm.LogError(Me.Name & ", GetFormData", 0, ex.Message, "Error Showing Data", ex.StackTrace, True)
        Call GetFormData(Nothing)

      End Try

    End If

    Call GetNotesData(ThisRow)

    ' Allow Field events to trigger before 'InPaint' Is re-set. 
    ' (Should) Prevent Validation errors during Form Draw.
    Application.DoEvents()

    ' Restore 'Paint' flag.
    InPaint = OrgInpaint
    FormChanged = False
    Me.btnSave.Enabled = False

    ' As it says on the can....
    Call SetButtonStatus()

  End Sub

  Private Sub GetNotesData(ByRef ThisRow As RenaissanceDataClass.DSInstrument.tblInstrumentRow)

    Try
      If (ThisRow Is Nothing) OrElse ((ThisRow.RowState And DataRowState.Detached) = DataRowState.Detached) OrElse (FormIsValid = False) OrElse (Me.InUse = False) Then
        GetNotesData(0)
      Else
        GetNotesData(ThisRow.InstrumentID)
      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub GetNotesData(ByVal InstrumentID As Integer)
    ' Routine to populate form Controls given a data row.
    ' Form is cleared for invalid Datarows or Invalid Form.

    Dim OrgInpaint As Boolean

    ' Set 'Paint' flag to prevent event actions caused by changing the value of form fields.
    OrgInpaint = InPaint
    InPaint = True

    Try

      If (InstrumentID <= 0) Then
        ' Bad / New Datarow - Clear Form.

        Text_ManagerNotes.Text = ""
        Text_ProspectusNotes.Text = ""

      Else

        ' Populate Form with given data.
        Try
          Dim ThisInstrumentID As Integer = thisDataRow.InstrumentID
          Dim tempNotesTbl As New RenaissanceDataClass.DSInstrumentNotes.tblInstrumentNotesDataTable

          If tempNotesAdaptor Is Nothing Then
            tempNotesAdaptor = New SqlDataAdapter
            MainForm.MainAdaptorHandler.Set_AdaptorCommands(Nothing, tempNotesAdaptor, RenaissanceStandardDatasets.tblInstrumentNotes.TableName)
          End If

          tempNotesAdaptor.SelectCommand.Connection = MainForm.GetVeniceConnection
          tempNotesAdaptor.SelectCommand.Parameters("@AuditID").Value = ThisInstrumentID
          tempNotesAdaptor.SelectCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate

          tempNotesTbl.Load(tempNotesAdaptor.SelectCommand.ExecuteReader)

          If (tempNotesTbl.Rows.Count > 0) Then
            Text_ManagerNotes.Text = tempNotesTbl(0).InstrumentManagerNote
            Text_ProspectusNotes.Text = tempNotesTbl(0).InstrumentUnitNote
          Else
            Text_ManagerNotes.Text = ""
            Text_ProspectusNotes.Text = ""
          End If
        Catch ex As Exception
        Finally
          If (tempNotesAdaptor IsNot Nothing) AndAlso (tempNotesAdaptor.SelectCommand IsNot Nothing) AndAlso (tempNotesAdaptor.SelectCommand.Connection IsNot Nothing) Then
            Try
              tempNotesAdaptor.SelectCommand.Connection.Close()
              tempNotesAdaptor.SelectCommand.Connection = Nothing
            Catch ex As Exception
            End Try
          End If
        End Try

      End If

    Catch ex As Exception
      Call MainForm.LogError(Me.Name & ", GetFormData", 0, ex.Message, "Error Showing Data", ex.StackTrace, True)
      Text_ManagerNotes.Text = ""
      Text_ProspectusNotes.Text = ""
    Finally

      ' Allow Field events to trigger before 'InPaint' Is re-set. 
      ' (Should) Prevent Validation errors during Form Draw.
      Application.DoEvents()

      ' Restore 'Paint' flag.
      InPaint = OrgInpaint
      Notes_ManagerNoteChanged = False
      Notes_ProspectusNoteChanged = False

    End Try

  End Sub

  ''' <summary>
  ''' Sets the form data.
  ''' </summary>
  ''' <param name="pConfirm">if set to <c>true</c> [p confirm].</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function SetFormData(Optional ByVal pConfirm As Boolean = True) As Boolean
    ' *************************************************************
    '
    ' *************************************************************
    Dim ErrMessage As String
    Dim ErrFlag As Boolean
    Dim ErrStack As String
    Dim ProtectedItem As Boolean = False

    ErrMessage = ""
    ErrStack = ""

    ' *************************************************************
    ' Appropriate Save permission :-
    ' *************************************************************

    If (Me.HasUpdatePermission = False) And ((Me.HasInsertPermission = False) Or (Me.AddNewRecord = False)) Then
      MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "You do not have permission to save this record.", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Function
    End If

    ' *************************************************************
    ' If Save button is disabled then should not be able to save, exit silently.
    ' *************************************************************
    If Me.btnSave.Enabled = False Then
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Function
    End If

    ' *************************************************************
    ' KnowledgeDate OK :-
    ' *************************************************************

    If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
      MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Changes to Venice are not allowed when the Knowledgedate is not set to `NOW`", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Function
    End If

    ' Confirm Save, if required.

    If (pConfirm = True) Then
      If MessageBox.Show("Save Changes ?", "Save Changes ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
        Return True
        Exit Function
      End If
    End If

    ' *************************************************************
    ' Procedure to Save the current form information.
    ' *************************************************************

    Dim StatusString As String = ""
    Dim LogString As String
    Dim UpdateRows(0) As DataRow
    Dim Position As Integer
    Dim thisInstrumentType As InstrumentTypes = 0

    If (FormChanged = False) Or (FormIsValid = False) Then
      Return False
      Exit Function
    End If

    ' Validation
    If ValidateForm(StatusString) = False Then
      MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Validation Error." & vbCrLf & StatusString, "", True)
      Return False
      Exit Function
    End If

    ' Check current position in the table.
    Position = Get_Position(thisAuditID)

    ' Allow for new or missing ID.
    If Position < 0 Then
      If (AddNewRecord = True) OrElse (SortedRows.Length <= 0) Then
        thisDataRow = myTable.NewRow
        LogString = "Add: "
        AddNewRecord = True
      Else
        Return False
        Exit Function
      End If
    Else
      ' Row found OK.
      LogString = "Edit : AuditID = " & thisAuditID.ToString

      thisDataRow = Me.SortedRows(Position)

      ' Check for Protected Instrument
      If (thisDataRow.InstrumentID > 0) AndAlso (System.Enum.IsDefined(GetType(RenaissanceGlobals.Instrument), thisDataRow.InstrumentID) = True) Then

        ProtectedItem = True

      End If

    End If

    ' Set 'Paint' flag.
    InPaint = True

    ' *************************************************************
    ' Lock the Data Table, to prevent update conflicts.
    ' *************************************************************
    SyncLock myTable

      ' Initiate Edit,
      thisDataRow.BeginEdit()

      ' Set Data Values
      If (ProtectedItem = True) AndAlso (AddNewRecord = False) AndAlso (thisDataRow.InstrumentDescription <> Me.edit_InstrumentDescription.Text) Then
        MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "This Instrument is protected, Instrument Description may not be changed.", "", True)
      Else
        thisDataRow.InstrumentDescription = Me.edit_InstrumentDescription.Text
      End If

      LogString &= ", InstrumentDescription = " & thisDataRow.InstrumentDescription

      thisDataRow.InstrumentCode = Me.edit_InstrumentCode.Text
      LogString &= ", InstrumentCode = " & thisDataRow.InstrumentCode

      thisDataRow.InstrumentPFPCID = Me.edit_PFPCID.Text

      If Me.Combo_PertracIndex.SelectedIndex > 0 Then ' 0 is the blank entry
        thisDataRow.InstrumentPertracCode = Me.Combo_PertracIndex.SelectedValue
      Else
        thisDataRow.SetInstrumentPertracCodeNull()
      End If

      If Me.Combo_InstrumentType.SelectedIndex >= 0 Then
        thisInstrumentType = CType(CInt(Me.Combo_InstrumentType.SelectedValue), InstrumentTypes)

        If (thisInstrumentType = RenaissanceGlobals.InstrumentTypes.Cash) Then
          If MessageBox.Show("Are you Sure the Instrument Type should be `Cash` ?" & vbCrLf & _
           "This type must only be applied to actual currencies. i.e. US Dollars, Euros etc.", "Are You Sure ?", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) <> Windows.Forms.DialogResult.OK Then
            If Not ((thisDataRow.RowState And DataRowState.Detached) = DataRowState.Detached) Then
              Try
                thisDataRow.RejectChanges()
              Catch ex As Exception
              End Try
            End If

            Return False
            Exit Function
          End If
        End If

        If (ProtectedItem = True) AndAlso (AddNewRecord = False) AndAlso (thisDataRow.InstrumentType <> CInt(Me.Combo_InstrumentType.SelectedValue)) Then
          MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "This Instrument is protected, Instrument Type may not be changed.", "", True)
        Else
          thisDataRow.InstrumentType = Me.Combo_InstrumentType.SelectedValue
        End If

      Else
        thisDataRow.RejectChanges()
        Me.MainForm.LogError(Me.Name & ", SetFormData", LOG_LEVELS.Warning, "Add/Edit Record", "InstrumentType must be set.", "", True)
        Return False
        Exit Function
      End If

      If Me.Combo_InstrumentParent.SelectedIndex > 0 Then
        thisDataRow.InstrumentParent = Me.Combo_InstrumentParent.SelectedValue
      Else
        thisDataRow.InstrumentParent = (0)
      End If

      ' Instrument Underlying

      Select Case thisInstrumentType

        Case InstrumentTypes.Cash
          If Me.Combo_CB_Underlying.SelectedIndex > 0 Then
            thisDataRow.InstrumentUnderlying = Me.Combo_CB_Underlying.SelectedValue
          Else
            thisDataRow.InstrumentUnderlying = (0)
          End If

        Case Else
          If Me.Combo_InstrumentUnderlying.SelectedIndex > 0 Then
            thisDataRow.InstrumentUnderlying = Me.Combo_InstrumentUnderlying.SelectedValue
          Else
            thisDataRow.InstrumentUnderlying = (0)
          End If
      End Select

      If Me.Combo_Benckmark.SelectedIndex > 0 Then
        thisDataRow.InstrumentBenchmark = Me.Combo_Benckmark.SelectedValue
      Else
        thisDataRow.InstrumentBenchmark = (0)
      End If

      thisDataRow.InstrumentParentEquivalentRatio = 1

      thisDataRow.InstrumentClass = Me.edit_InstrumentClass.Text

      If Me.Combo_InstrumentFundType.SelectedIndex >= 0 Then
        If (ProtectedItem = True) AndAlso (AddNewRecord = False) AndAlso (thisDataRow.InstrumentFundType <> CInt(Me.Combo_InstrumentFundType.SelectedValue)) Then
          MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "This Instrument is protected, Instrument Fund Type may not be changed.", "", True)
        Else
          thisDataRow.InstrumentFundType = Me.Combo_InstrumentFundType.SelectedValue
        End If

      Else
        thisDataRow.RejectChanges()
        Me.MainForm.LogError(Me.Name & ", SetFormData", LOG_LEVELS.Warning, "Add/Edit Record", "InstrumentFundType must be set.", "", True)
        Return False
        Exit Function
      End If

      If Me.Combo_InstrumentCurrency.SelectedIndex >= 0 Then
        If (ProtectedItem = True) AndAlso (AddNewRecord = False) AndAlso (thisDataRow.InstrumentCurrencyID <> CInt(Me.Combo_InstrumentCurrency.SelectedValue)) Then
          MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "This Instrument is protected, Instrument Currency may not be changed.", "", True)
        Else
          thisDataRow.InstrumentCurrencyID = Me.Combo_InstrumentCurrency.SelectedValue
          thisDataRow.InstrumentCurrency = Me.Combo_InstrumentCurrency.Text
        End If
      Else
        thisDataRow.RejectChanges()
        Me.MainForm.LogError(Me.Name & ", SetFormData", LOG_LEVELS.Warning, "Add/Edit Record", "InstrumentCurrency must be set.", "", True)
        Return False
        Exit Function
      End If

      If (thisInstrumentType = InstrumentTypes.Unit) Then
        thisDataRow.InstrumentFundID = CInt(Nz(Combo_FundID.SelectedValue, 0))
        thisDataRow.InstrumentUnitPerformanceBenchmark = CInt(Nz(Me.Combo_PerformanceVsBenchmark.SelectedValue, 0))
        thisDataRow.InstrumentUnitManagementFees = edit_FundManagementFees.Value
        thisDataRow.InstrumentUnitPerformanceFees = edit_FundPerformanceFees.Value
        thisDataRow.InstrumentUnitPerformanceHurdle = edit_PerformanceHurdle.Value
      Else
        thisDataRow.InstrumentFundID = 0
        thisDataRow.InstrumentUnitPerformanceBenchmark = 0
        thisDataRow.InstrumentUnitManagementFees = 0.0#
        thisDataRow.InstrumentUnitPerformanceFees = 0.0#
        thisDataRow.InstrumentUnitPerformanceHurdle = 0.0#
      End If

      thisDataRow.InstrumentPortfolioTicker = Me.edit_PortfolioTicker.Text
      thisDataRow.InstrumentExchangeCode = Me.edit_ExchangeCode.Text
      thisDataRow.InstrumentSEDOL = Me.edit_SEDOL.Text
      thisDataRow.InstrumentMorningstar = Me.edit_Morningstar.Text
      thisDataRow.InstrumentISIN = Me.edit_ISIN.Text
      thisDataRow.InstrumentCountryISO2 = Me.edit_ISO2_CountryCode.Text

      thisDataRow.InstrumentExpectedReturn = Me.edit_ExpectedReturn.Value
      thisDataRow.InstrumentWorstCase = Me.edit_WorstCaseFall.Value

      ' InstrumentMultiplier, InstrumentContractSize
      Try


        Select Case thisInstrumentType

          Case InstrumentTypes.Cash, InstrumentTypes.Notional, InstrumentTypes.Unit

            thisDataRow.InstrumentMultiplier = 1.0#
            thisDataRow.InstrumentContractSize = 1.0#

            If (Me.edit_ExpiryDate.Text.Length <= 0) Or (IsDate(Me.edit_ExpiryDate.Text) = False) Then
              thisDataRow.InstrumentExpiryDate = KNOWLEDGEDATE_NOW
            Else
              thisDataRow.InstrumentExpiryDate = Me.edit_ExpiryDate.Value
            End If

          Case InstrumentTypes.ConvertibleBond

            thisDataRow.InstrumentContractSize = Math.Max(Math.Abs(edit_CB_ContractSize.Value), 0.000001)
            thisDataRow.InstrumentMultiplier = 1.0# ' Field not shown on Bond Tab.
            thisDataRow.InstrumentExpiryDate = Date_CB_MaturityDate.Value.Date

          Case InstrumentTypes.Bond

            thisDataRow.InstrumentContractSize = Math.Max(Math.Abs(edit_BondMultiplier.Value), 0.000001)
            thisDataRow.InstrumentMultiplier = 1.0# ' Field not shown on Bond Tab.
            thisDataRow.InstrumentExpiryDate = Date_BondMaturityDate.Value.Date

          Case Else

            thisDataRow.InstrumentMultiplier = Math.Max(Math.Abs(edit_InstrumentMultiplier.Value), 0.000001)
            thisDataRow.InstrumentContractSize = Math.Max(Math.Abs(edit_ContractSize.Value), 0.000001)

            If (Me.edit_ExpiryDate.Text.Length <= 0) Or (IsDate(Me.edit_ExpiryDate.Text) = False) Then
              thisDataRow.InstrumentExpiryDate = KNOWLEDGEDATE_NOW
            Else
              thisDataRow.InstrumentExpiryDate = Me.edit_ExpiryDate.Value
            End If

        End Select

        Select Case thisInstrumentType

          Case InstrumentTypes.Bond, InstrumentTypes.ConvertibleBond, InstrumentTypes.Option, InstrumentTypes.Forward, InstrumentTypes.Future

            If (thisDataRow.InstrumentExpiryDate <= Now.Date) Then
              MessageBox.Show("This Instrument has an Expiry Date today or in the past." & vbCrLf & "Are you sure that this is correct ?")
            End If
        End Select

        If thisInstrumentType = InstrumentTypes.Future Then
          thisDataRow.InstrumentIsFXFuture = Check_IsFXFuture.Checked
          thisDataRow.InstrumentFXFutureConstantCurrency = CInt(Nz(Combo_FXFixedCurrency.SelectedValue, 0))
        Else
          thisDataRow.InstrumentIsFXFuture = False
          thisDataRow.InstrumentFXFutureConstantCurrency = 0
        End If

        If thisInstrumentType = InstrumentTypes.Option Then
          thisDataRow.InstrumentSharesPerOption = edit_SharesPerOption.Value
        Else
          thisDataRow.InstrumentSharesPerOption = 1.0#
        End If

      Catch ex As Exception
        thisDataRow.InstrumentMultiplier = 1.0#
        thisDataRow.InstrumentContractSize = 1.0#
        thisDataRow.InstrumentExpiryDate = KNOWLEDGEDATE_NOW
        thisDataRow.InstrumentSharesPerOption = 1.0#
      End Try

      If Me.Check_LimitDisplay.CheckState = CheckState.Checked Then
        thisDataRow.InstrumentLimitDisplay = (-1)
      Else
        thisDataRow.InstrumentLimitDisplay = 0
      End If

      thisDataRow.InstrumentNoticeDays = Me.edit_DealingNoticeDays.Value

      If Me.Combo_DealingPeriod.SelectedIndex >= 0 Then
        thisDataRow.InstrumentDealingPeriod = Me.Combo_DealingPeriod.SelectedValue
      Else
        thisDataRow.RejectChanges()
        Me.MainForm.LogError(Me.Name & ", SetFormData", LOG_LEVELS.Warning, "Add/Edit Record", "Instrument Dealing Period must be set.", "", True)
        Return False
        Exit Function
      End If


      If (Me.edit_DealingBaseDate.Text.Length <= 0) Or (IsDate(Me.edit_DealingBaseDate.Text) = False) Then
        thisDataRow.SetInstrumentDealingBaseDateNull()
      Else
        thisDataRow.InstrumentDealingBaseDate = Me.edit_DealingBaseDate.Value
      End If

      thisDataRow.InstrumentLockupPeriod = Me.edit_LockupPeriod.Value
      thisDataRow.InstrumentDefaultSettlementDays_Buy = edit_Settlemet_Buy.Value
      thisDataRow.InstrumentOrderPrecision = CInt(edit_MaxDecimalPlaces.Value)
      thisDataRow.InstrumentDefaultSettlementDays_Sell = edit_Settlemet_Sell.Value
      thisDataRow.InstrumentCapturePrice = Check_PriceCapture.Checked
      thisDataRow.InstrumentOrderOnlyByValue = Check_TradeAsValue.Checked
      thisDataRow.InstrumentDealingCutOffTime = CInt(Date_Cutoff.Value.TimeOfDay.TotalSeconds)  ' New Date(1900, 1, 1, Date_Cutoff.Value.Hour, Date_Cutoff.Value.Minute, Date_Cutoff.Value.Second)

      If AddNewRecord Then
        thisDataRow.InstrumentCalendar = 0
        thisDataRow.InstrumentDealingCutOffTime = 0
      End If

      If Me.Check_UpfrontFees.CheckState = CheckState.Checked Then
        thisDataRow.InstrumentUpfrontfees = (-1)
      Else
        thisDataRow.InstrumentUpfrontfees = 0
      End If

      thisDataRow.InstrumentPenalty = Me.edit_Penalty.Value
      thisDataRow.InstrumentLiquidityComment = Me.edit_LiquidityComment.Text
      thisDataRow.InstrumentPenaltyComment = Me.edit_LiquidityComment.Text

      If (Me.edit_LatestTradeDate.Text.Length <= 0) Or (IsDate(Me.edit_LatestTradeDate.Text) = False) Then
        thisDataRow.InstrumentLastValidTradeDate = KNOWLEDGEDATE_NOW
      Else
        thisDataRow.InstrumentLastValidTradeDate = Me.edit_LatestTradeDate.Value
      End If

      If Me.Check_ExcludeFromGAV.CheckState = CheckState.Checked Then
        thisDataRow.InstrumentExcludeGAV = (-1)
      Else
        thisDataRow.InstrumentExcludeGAV = 0
      End If

      If Me.Check_ExcludeFromAUM.CheckState = CheckState.Checked Then
        thisDataRow.InstrumentExcludeAUM = (-1)
      Else
        thisDataRow.InstrumentExcludeAUM = 0
      End If

      If Me.Check_IsManagementFee.CheckState = CheckState.Checked Then
        thisDataRow.InstrumentIsManagementFee = (-1)
      Else
        thisDataRow.InstrumentIsManagementFee = 0
      End If

      If Me.Check_IsIncentiveFee.CheckState = CheckState.Checked Then
        thisDataRow.InstrumentIsIncentiveFee = (-1)
      Else
        thisDataRow.InstrumentIsIncentiveFee = 0
      End If

      If Me.Check_IsCrystalisedIncentiveFee.CheckState = CheckState.Checked Then
        thisDataRow.InstrumentIsCrystalisedIncentive = (-1)
        thisDataRow.InstrumentIsIncentiveFee = (-1)
      Else
        thisDataRow.InstrumentIsCrystalisedIncentive = 0
      End If

      If Me.Check_IsMovementCommission.CheckState = CheckState.Checked Then
        thisDataRow.InstrumentIsMovementCommission = (-1)
      Else
        thisDataRow.InstrumentIsMovementCommission = 0
      End If

      If Me.Check_IsEqualisation.CheckState = CheckState.Checked Then
        thisDataRow.InstrumentIsEqualisation = (-1)
      Else
        thisDataRow.InstrumentIsEqualisation = 0
      End If

      If Me.Check_IsLeverageInstrument.CheckState = CheckState.Checked Then
        If Me.Combo_LeverageCounterparty.SelectedIndex >= 0 Then
          thisDataRow.InstrumentIsLeverageInstrument = (-1)
          thisDataRow.InstrumentLeverageCounterparty = Me.Combo_LeverageCounterparty.SelectedValue
        Else
          thisDataRow.InstrumentIsLeverageInstrument = 0
          thisDataRow.InstrumentLeverageCounterparty = 0
        End If
      Else
        thisDataRow.InstrumentIsLeverageInstrument = 0
        thisDataRow.InstrumentLeverageCounterparty = 0
      End If

      If (Combo_ReviewGroup.SelectedIndex > 0) AndAlso (IsNumeric(Combo_ReviewGroup.SelectedValue)) Then
        thisDataRow.InstrumentReviewGroup = CInt(Combo_ReviewGroup.SelectedValue)
      Else
        thisDataRow.InstrumentReviewGroup = 0
      End If

      ' Attribution 

      thisDataRow.AttributionMarket = Combo_AttributionMarket.Text
      thisDataRow.AttributionSector = Combo_AttributionSector.Text

      thisDataRow.EndEdit()
      InPaint = False

      ' Add and Update DataRow. 

      ErrFlag = False

      If AddNewRecord = True Then
        Try
          myTable.Rows.Add(thisDataRow)
        Catch ex As Exception
          ErrFlag = True
          ErrMessage = ex.Message
          ErrStack = ex.StackTrace
        End Try
      End If

      UpdateRows(0) = thisDataRow

      ' Post Additions / Updates to the underlying table :-

      Dim temp As Integer
      Try
        If (ErrFlag = False) Then
          myAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
          myAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

          temp = MainForm.AdaptorUpdate(Me.Name, myAdaptor, UpdateRows)
        End If

      Catch ex As Exception

        ErrMessage = ex.Message
        ErrFlag = True
        ErrStack = ex.StackTrace

      End Try

      If (ErrFlag = False) Then
        thisAuditID = thisDataRow.AuditID
      End If

    End SyncLock

    ' Save notes Data if necessary

    If (Notes_ManagerNoteChanged) OrElse (Notes_ProspectusNoteChanged) Then

      Try
        Dim tempNotesTbl As New RenaissanceDataClass.DSInstrumentNotes.tblInstrumentNotesDataTable
        Dim tempNotesRow As RenaissanceDataClass.DSInstrumentNotes.tblInstrumentNotesRow

        If tempNotesAdaptor Is Nothing Then
          tempNotesAdaptor = New SqlDataAdapter
          MainForm.MainAdaptorHandler.Set_AdaptorCommands(Nothing, tempNotesAdaptor, RenaissanceStandardDatasets.tblInstrumentNotes.TableName)
        End If

        tempNotesAdaptor.InsertCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate
        tempNotesAdaptor.UpdateCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate

        tempNotesRow = tempNotesTbl.NewtblInstrumentNotesRow

        tempNotesRow.InstrumentID = Nz(thisDataRow.InstrumentID, 0)
        tempNotesRow.InstrumentManagerNote = Text_ManagerNotes.Text
        tempNotesRow.InstrumentUnitNote = Text_ProspectusNotes.Text

        tempNotesTbl.Rows.Add(tempNotesRow)

        Call MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblInstrumentNotes, tempNotesTbl)

      Catch ex As Exception
      End Try
    End If

    ' Save Compound Characteristics if necessary...

    If (Not thisDataRow.IsInstrumentIDNull()) Then
      Try

        Dim LinkItemChanged As Boolean

        Dim ListTable As RenaissanceDataClass.DSFlorenceEntity.tblFlorenceEntityDataTable
        ListTable = CType(List_Entity.DataSource, RenaissanceDataClass.DSFlorenceEntity.tblFlorenceEntityDataTable)
        Dim tblEntityRow As RenaissanceDataClass.DSFlorenceEntity.tblFlorenceEntityRow

        Dim DSLinkItems As RenaissanceDataClass.DSInstrumentFlorenceEntityLink
        Dim tblLinkItems As RenaissanceDataClass.DSInstrumentFlorenceEntityLink.tblInstrumentFlorenceEntityLinkDataTable
        Dim SelectedRows() As RenaissanceDataClass.DSInstrumentFlorenceEntityLink.tblInstrumentFlorenceEntityLinkRow
        Dim tblLinkItemRow As RenaissanceDataClass.DSInstrumentFlorenceEntityLink.tblInstrumentFlorenceEntityLinkRow
        Dim ItemCount As Integer
        Dim ParentInstrumentID As Integer = thisDataRow.InstrumentID

        DSLinkItems = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblInstrumentFlorenceEntityLink, False), RenaissanceDataClass.DSInstrumentFlorenceEntityLink)
        tblLinkItems = DSLinkItems.tblInstrumentFlorenceEntityLink

        SelectedRows = tblLinkItems.Select("InstrumentID=" & ParentInstrumentID.ToString, "InstrumentID")

        SyncLock tblLinkItems

          Try

            ' Delete any rows that are no longer in the Group

            If (SelectedRows IsNot Nothing) AndAlso (SelectedRows.Length > 0) Then
              For ItemCount = 0 To (SelectedRows.Length - 1)
                tblLinkItemRow = SelectedRows(ItemCount)

                If (GroupContainsID(tblLinkItemRow.EntityID) = False) Then
                  tblLinkItemRow.Delete()
                  LinkItemChanged = True
                End If
              Next
            End If

            ' Add / Update Group Items to the table

            Dim FoundFlag As Boolean = False

            For Each tblEntityRow In ListTable.Rows
              FoundFlag = False
              tblLinkItemRow = Nothing

              If (SelectedRows.Length > 0) Then
                For ItemCount = 0 To (SelectedRows.Length - 1)
                  tblLinkItemRow = SelectedRows(ItemCount)

                  If tblLinkItemRow.RowState <> DataRowState.Deleted Then
                    If (tblLinkItemRow.EntityID = tblEntityRow.EntityID) Then
                      FoundFlag = True

                      Exit For
                    End If
                  End If
                Next
              End If

              If (FoundFlag = False) Then

                ' Add New Row

                tblLinkItemRow = tblLinkItems.NewRow

                tblLinkItemRow.LinkID = 0
                tblLinkItemRow.InstrumentID = ParentInstrumentID
                tblLinkItemRow.EntityID = tblEntityRow.EntityID

                tblLinkItems.Rows.Add(tblLinkItemRow)
                LinkItemChanged = True

              End If
            Next

            ' Post Additions / Updates to the underlying table :-

            If (ErrFlag = False) AndAlso (LinkItemChanged) Then
              Call MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblInstrumentFlorenceEntityLink, tblLinkItems.ToArray())
              Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceStandardDatasets.tblInstrumentFlorenceEntityLink.ChangeID), True)
            End If

          Catch ex As Exception

            ErrMessage = ex.Message
            ErrFlag = True
            ErrStack = ex.StackTrace

          End Try

        End SyncLock

      Catch ex As Exception
        ErrMessage = ex.Message
        ErrFlag = True
        ErrStack = ex.StackTrace
      End Try

    End If

    ' *************************************************************
    ' Error report. Not done in the Try-Catch as that would serve to persist the SyncLock.
    ' *************************************************************

    If (ErrFlag = True) Then
      Call MainForm.LogError(Me.Name & ", SetFormData", 0, ErrMessage, "Error Saving Data", ErrStack, True)
    End If

    ' Finish off

    AddNewRecord = False
    FormChanged = False

    Me.THIS_FORM_SelectingCombo.Enabled = True

    ' Propagate changes

    Dim thisEventArgs As New RenaissanceGlobals.RenaissanceUpdateEventArgs

    thisEventArgs.TableChanged(THIS_FORM_ChangeID) = True

    If (thisDataRow IsNot Nothing) AndAlso (thisDataRow.IsInstrumentIDNull = False) AndAlso (thisDataRow.InstrumentID > 0) Then
      thisEventArgs.UpdateDetail(THIS_FORM_ChangeID) = thisDataRow.InstrumentID.ToString
    End If

    If (Notes_ManagerNoteChanged) OrElse (Notes_ProspectusNoteChanged) Then
      thisEventArgs.TableChanged(RenaissanceChangeID.tblInstrumentNotes) = True
      thisEventArgs.UpdateDetail(RenaissanceChangeID.tblInstrumentNotes) = thisDataRow.InstrumentID.ToString
    End If

    Call MainForm.Main_RaiseEvent_Background(thisEventArgs, True)

  End Function

  ''' <summary>
  ''' Sets the button status.
  ''' </summary>
  Private Sub SetButtonStatus()
    ' Sets the status of the form controlls appropriate to the current users 
    'permissions and the 'Changed' or 'New' status of the form.

    Dim ThisType As InstrumentTypes = CType(CInt(Nz(Combo_InstrumentType.SelectedValue, 0)), InstrumentTypes)

    ' No Read permission :-

    If Me.HasReadPermission = False Then
      MainForm.LogError(Me.Name & ", SetButtonStatus()", 0, "", "You do not have Read permission for this Form", "", True)
      Me.Close()
      Exit Sub
    End If

    ' Has Insert Permission.
    If Me.HasInsertPermission Then
      Me.btnAdd.Enabled = True
      Me.btnAddCopy.Enabled = True
    Else
      Me.btnAdd.Enabled = False
      Me.btnAddCopy.Enabled = False
    End If

    ' Has Delete permission. 
    If (Me.HasDeletePermission) And (Me.AddNewRecord = False) And (thisPosition >= 0) Then
      Me.btnDelete.Enabled = True
    Else
      Me.btnDelete.Enabled = False
    End If

    ' Futures specific 

    If (ThisType = InstrumentTypes.Future) Then
      Me.Check_IsFXFuture.Visible = True
      Me.Label_IsFxFuture.Visible = True
      Me.Label_FixedCurrency.Visible = True
      Me.Combo_FXFixedCurrency.Visible = True
      Me.Label_FixedCurrencyNote.Visible = True
    Else
      Me.Check_IsFXFuture.Visible = False
      Me.Label_IsFxFuture.Visible = False
      Me.Label_FixedCurrency.Visible = False
      Me.Combo_FXFixedCurrency.Visible = False
      Me.Label_FixedCurrencyNote.Visible = False
    End If

    If (ThisType = InstrumentTypes.Option) Then
      edit_SharesPerOption.Visible = True
      Label_SharesPerOptionComment.Visible = True
      Label_SharesPerOption.Visible = True
    Else
      edit_SharesPerOption.Visible = False
      Label_SharesPerOptionComment.Visible = False
      Label_SharesPerOption.Visible = False
    End If
    '

    If (Me.MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And _
     ((Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord)) Then

      Me.edit_InstrumentDescription.Enabled = True
      Me.edit_InstrumentCode.Enabled = True
      Me.edit_PFPCID.Enabled = True
      Me.Combo_PertracIndex.Enabled = True
      Me.Combo_InstrumentType.Enabled = True
      Me.Combo_InstrumentParent.Enabled = True
      Me.Combo_Benckmark.Enabled = True
      Me.Combo_InstrumentUnderlying.Enabled = True
      Me.edit_InstrumentClass.Enabled = True
      Me.Combo_InstrumentFundType.Enabled = True
      Me.Combo_FundID.Enabled = True
      Me.Combo_PerformanceVsBenchmark.Enabled = True
      Me.Combo_InstrumentCurrency.Enabled = True
      Me.edit_ExpectedReturn.Enabled = True
      Me.edit_WorstCaseFall.Enabled = True
      Me.Check_LimitDisplay.Enabled = True
      Me.edit_DealingNoticeDays.Enabled = True
      Me.Combo_DealingPeriod.Enabled = True
      Me.edit_DealingBaseDate.Enabled = True
      Me.edit_LockupPeriod.Enabled = True

      Me.edit_FundManagementFees.Enabled = True
      Me.edit_FundPerformanceFees.Enabled = True
      Me.edit_PerformanceHurdle.Enabled = True

      edit_Settlemet_Buy.Enabled = True
      edit_MaxDecimalPlaces.Enabled = True
      Date_Cutoff.Enabled = True
      edit_Settlemet_Sell.Enabled = True
      Check_PriceCapture.Enabled = True
      Check_TradeAsValue.Enabled = True

      Me.Check_UpfrontFees.Enabled = True
      Me.edit_Penalty.Enabled = True
      Me.edit_LiquidityComment.Enabled = True
      Me.edit_LatestTradeDate.Enabled = True
      Me.edit_ExpiryDate.Enabled = True
      Me.Check_ExcludeFromGAV.Enabled = True
      Me.Check_ExcludeFromAUM.Enabled = True
      Me.Check_IsManagementFee.Enabled = True
      Me.Check_IsIncentiveFee.Enabled = True
      Me.Check_IsCrystalisedIncentiveFee.Enabled = True
      Me.Check_IsMovementCommission.Enabled = True
      Me.Check_IsEqualisation.Enabled = True
      Me.Check_IsLeverageInstrument.Enabled = True
      Me.edit_PortfolioTicker.Enabled = True
      Me.edit_ISIN.Enabled = True
      Me.edit_ISO2_CountryCode.Enabled = True
      Me.edit_SEDOL.Enabled = True
      Me.edit_Morningstar.Enabled = True
      Me.edit_ExchangeCode.Enabled = True
      Me.Group_LinkedEntities.Enabled = True
      Me.Combo_AttributionMarket.Enabled = True
      Me.Combo_AttributionSector.Enabled = True

      If Me.Check_IsLeverageInstrument.CheckState = CheckState.Checked Then
        Me.Combo_LeverageCounterparty.Enabled = True
      Else
        Me.Combo_LeverageCounterparty.Enabled = False
      End If

      Check_IsFXFuture.Enabled = True
      Me.edit_SharesPerOption.Enabled = True

      If (Me.Combo_InstrumentType.SelectedIndex >= 0) AndAlso (IsNumeric(Combo_InstrumentType.SelectedValue)) Then

        If (ThisType = InstrumentTypes.Cash) OrElse (ThisType = InstrumentTypes.Notional) OrElse (ThisType = InstrumentTypes.Unit) Then
          Me.edit_InstrumentMultiplier.Enabled = False
          Me.edit_BondMultiplier.Enabled = False
          Me.edit_CB_ContractSize.Enabled = False
          Me.edit_ContractSize.Enabled = False
        Else
          Me.edit_InstrumentMultiplier.Enabled = True
          Me.edit_BondMultiplier.Enabled = True
          Me.edit_CB_ContractSize.Enabled = True
          Me.edit_ContractSize.Enabled = True
        End If

      Else
        Me.edit_InstrumentMultiplier.Enabled = True
        Me.edit_BondMultiplier.Enabled = True
        Me.edit_CB_ContractSize.Enabled = True
        Me.edit_ContractSize.Enabled = True
      End If

      Me.Combo_ReviewGroup.Enabled = True

      Text_ManagerNotes.Enabled = True
      Text_ProspectusNotes.Enabled = True

      Text_BondIssuer.Enabled = True
      Text_BondRanking.Enabled = True
      Text_BondGoverningLaw.Enabled = True
      Date_BondMaturityDate.Enabled = True
      edit_BondInterestPercentage.Enabled = True

      Text_CB_Issuer.Enabled = True
      Text_CB_Ranking.Enabled = True
      Text_CB_GoverningLaw.Enabled = True
      Date_CB_MaturityDate.Enabled = True
      edit_CB_InterestRate.Enabled = True
      Combo_CB_Underlying.Enabled = True

    Else

      Me.edit_InstrumentDescription.Enabled = False
      Me.edit_InstrumentCode.Enabled = False
      Me.edit_PFPCID.Enabled = False
      Me.Combo_PertracIndex.Enabled = False
      Me.Combo_InstrumentType.Enabled = False
      Me.Combo_InstrumentParent.Enabled = False
      Me.Combo_Benckmark.Enabled = False
      Me.Combo_InstrumentUnderlying.Enabled = False
      Me.edit_InstrumentClass.Enabled = False
      Me.Combo_InstrumentFundType.Enabled = False
      Me.Combo_FundID.Enabled = False
      Me.Combo_PerformanceVsBenchmark.Enabled = False
      Me.Combo_InstrumentCurrency.Enabled = False
      Me.edit_ExpectedReturn.Enabled = False
      Me.edit_WorstCaseFall.Enabled = False
      Me.Check_LimitDisplay.Enabled = False
      Me.edit_DealingNoticeDays.Enabled = False
      Me.Combo_DealingPeriod.Enabled = False
      Me.edit_DealingBaseDate.Enabled = False
      Me.edit_LockupPeriod.Enabled = False
      Me.Combo_AttributionMarket.Enabled = False
      Me.Combo_AttributionSector.Enabled = False

      Me.edit_FundManagementFees.Enabled = False
      Me.edit_FundPerformanceFees.Enabled = False
      Me.edit_PerformanceHurdle.Enabled = False

      edit_Settlemet_Buy.Enabled = False
      edit_MaxDecimalPlaces.Enabled = False
      Date_Cutoff.Enabled = False
      edit_Settlemet_Sell.Enabled = False
      Check_PriceCapture.Enabled = False
      Check_TradeAsValue.Enabled = False

      Me.Check_UpfrontFees.Enabled = False
      Me.edit_Penalty.Enabled = False
      Me.edit_LiquidityComment.Enabled = False
      Me.edit_LatestTradeDate.Enabled = False
      Me.edit_ExpiryDate.Enabled = False
      Me.Check_ExcludeFromGAV.Enabled = False
      Me.Check_ExcludeFromAUM.Enabled = False
      Me.Check_IsManagementFee.Enabled = False
      Me.Check_IsIncentiveFee.Enabled = False
      Me.Check_IsCrystalisedIncentiveFee.Enabled = False
      Me.Check_IsMovementCommission.Enabled = False
      Me.Check_IsEqualisation.Enabled = False
      Me.Check_IsLeverageInstrument.Enabled = False
      Me.Combo_LeverageCounterparty.Enabled = False
      Me.edit_PortfolioTicker.Enabled = False
      Me.edit_ISIN.Enabled = False
      Me.edit_ISO2_CountryCode.Enabled = False
      Me.edit_SEDOL.Enabled = False
      Me.edit_Morningstar.Enabled = False
      Me.edit_ExchangeCode.Enabled = False
      Me.Combo_ReviewGroup.Enabled = False
      Me.edit_InstrumentMultiplier.Enabled = False
      Me.edit_BondMultiplier.Enabled = False
      Me.edit_CB_ContractSize.Enabled = False
      Check_IsFXFuture.Enabled = False
      Me.edit_SharesPerOption.Enabled = False

      Me.edit_ContractSize.Enabled = False
      Me.Group_LinkedEntities.Enabled = False

      Text_ManagerNotes.Enabled = False
      Text_ProspectusNotes.Enabled = False

      Text_BondIssuer.Enabled = False
      Text_BondRanking.Enabled = False
      Text_BondGoverningLaw.Enabled = False
      Date_BondMaturityDate.Enabled = False
      edit_BondInterestPercentage.Enabled = False

      Text_CB_Issuer.Enabled = False
      Text_CB_Ranking.Enabled = False
      Text_CB_GoverningLaw.Enabled = False
      Date_CB_MaturityDate.Enabled = False
      edit_CB_InterestRate.Enabled = False
      Combo_CB_Underlying.Enabled = False

    End If

    Call SetInstrumentTabVisibilities()

  End Sub

  Private Sub SetInstrumentTabVisibilities()
    ' **************************************************************************************************************
    ' Function to show only the required tab on the Instruments form.
    ' A second (hidden) tab control is used to contain the 'hidden' tab pages as they do not seem to update correctly if
    ' they are held as disconnected objects.
    ' **************************************************************************************************************

    Try

      Dim thisInstrumentType As InstrumentTypes = 0
      Try
        If (IsNumeric(Combo_InstrumentType.SelectedValue)) Then
          thisInstrumentType = CType(Combo_InstrumentType.SelectedValue, InstrumentTypes)
        End If
      Catch ex As Exception
        thisInstrumentType = 0
      End Try

      Dim PagesToHide() As String = {}
      Dim PagesToShow() As String = {}

      Select Case thisInstrumentType

        Case InstrumentTypes.Unit
          PagesToHide = New String() {Tab_Convertibles.Name, Tab_Derivatives.Name, Tab_Bonds.Name}
          PagesToShow = New String() {Tab_Unit.Name}

        Case InstrumentTypes.Bond
          PagesToHide = New String() {Tab_Convertibles.Name, Tab_Derivatives.Name, Tab_Unit.Name}
          PagesToShow = New String() {Tab_Bonds.Name}

        Case InstrumentTypes.ConvertibleBond
          PagesToHide = New String() {Tab_Bonds.Name, Tab_Derivatives.Name, Tab_Unit.Name}
          PagesToShow = New String() {Tab_Convertibles.Name}

        Case Else
          PagesToHide = New String() {Tab_Convertibles.Name, Tab_Unit.Name, Tab_Bonds.Name}
          PagesToShow = New String() {Tab_Derivatives.Name}

      End Select

      For Each ThisPageName As String In PagesToHide
        Try
          If (Tab_InstrumentDetails.TabPages.Contains(InstrumentTabs(ThisPageName))) Then
            Tab_InstrumentDetails.TabPages.Remove(InstrumentTabs(ThisPageName))
          End If
          If (Not TabControl_HiddenTabs.TabPages.Contains(InstrumentTabs(ThisPageName))) Then
            TabControl_HiddenTabs.TabPages.Insert(0, InstrumentTabs(ThisPageName))
          End If
        Catch ex As Exception
        End Try
      Next

      For Each ThisPageName As String In PagesToShow
        Try
          If (TabControl_HiddenTabs.TabPages.Contains(InstrumentTabs(ThisPageName))) Then
            TabControl_HiddenTabs.TabPages.Remove(InstrumentTabs(ThisPageName))
          End If
          If (Not Tab_InstrumentDetails.TabPages.Contains(InstrumentTabs(ThisPageName))) Then
            Tab_InstrumentDetails.TabPages.Insert(1, InstrumentTabs(ThisPageName))
          End If
        Catch ex As Exception
        End Try
      Next

    Catch ex As Exception
    End Try
  End Sub
  ''' <summary>
  ''' Validates the form.
  ''' </summary>
  ''' <param name="pReturnString">The p return string.</param>
  ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function ValidateForm(ByRef pReturnString As String) As Boolean
    ' Form Validation code.
    ' 
    ' This code should be the final arbiter of what is allowed. no assumptions regarding 
    ' prior validation should be made.
    ' 
    ' This Code is called by the SetFormData routine before position changes.
    '
    Dim RVal As Boolean

    RVal = True
    pReturnString = ""


    ' Enforce Uniqueness of Leverage Counterparty at the Instrument Level.

    Dim thisSortedRows() As DataRow
    If Me.Check_IsLeverageInstrument.CheckState = CheckState.Checked Then
      If Me.Combo_LeverageCounterparty.SelectedIndex >= 0 Then
        thisSortedRows = myDataset.Tables(0).Select("(InstrumentLeverageCounterparty=" & Me.Combo_LeverageCounterparty.SelectedValue.ToString & ") AND (AuditID<>" & thisAuditID & ")")

        If thisSortedRows.Length > 0 Then
          pReturnString = "Leverage Counterparty must be unique." & vbCrLf & "The selected value is already used on another instrument : " & vbCrLf & thisSortedRows(0)("InstrumentDescription").ToString
          RVal = False
        End If
      End If
    End If

    ' Validate 

    If (RVal = True) AndAlso (Me.edit_InstrumentDescription.Text.Length <= 0) Then
      pReturnString = "Instrument Description must not be left blank."
      RVal = False
    End If

    If (RVal = True) AndAlso (Me.Combo_InstrumentType.SelectedIndex < 0) Then
      pReturnString = "An Instrument Type must be selected."
      RVal = False
    End If

    If (RVal = True) AndAlso (Me.Combo_InstrumentFundType.SelectedIndex < 0) Then
      pReturnString = "An Instrument Fund Type must be selected."
      RVal = False
    End If

    If (RVal = True) AndAlso (Me.Combo_FundID.SelectedIndex < 0) AndAlso (CType(Combo_InstrumentType.SelectedValue, InstrumentTypes) = InstrumentTypes.Unit) Then
      pReturnString = "Fund Units must be associated with a Fund."
      RVal = False
    End If

    If (RVal = True) AndAlso (Me.Combo_InstrumentCurrency.SelectedIndex < 0) Then
      pReturnString = "An Instrument Currency must be selected."
      RVal = False
    End If

    If (RVal = True) AndAlso (Me.Combo_DealingPeriod.SelectedIndex < 0) Then
      pReturnString = "An Instrument Dealing Period must be selected."
      RVal = False
    End If


    Return RVal

  End Function

  ''' <summary>
  ''' Handles the MouseEnter event of the btnCancel control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnCancel_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseEnter
    ' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
    Me.IsOverCancelButton = True
  End Sub

  ''' <summary>
  ''' Handles the MouseLeave event of the btnCancel control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnCancel_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseLeave
    ' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
    Me.IsOverCancelButton = False
  End Sub

  ''' <summary>
  ''' Handles the CheckedChanged event of the Check_IsLeverageInstrument control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Check_IsLeverageInstrument_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_IsLeverageInstrument.CheckedChanged
    If (Me.Check_IsLeverageInstrument.Enabled = True) AndAlso (Me.Check_IsLeverageInstrument.CheckState = CheckState.Checked) Then
      Me.Combo_LeverageCounterparty.Enabled = True
    Else
      Me.Combo_LeverageCounterparty.Enabled = False
    End If
  End Sub

#End Region

  ''' <summary>
  ''' Handles the Click event of the Button_ShowParentHeirarchy control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Button_ShowParentHeirarchy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_ShowParentHeirarchy.Click
    ' **************************************************************************************
    '
    ' **************************************************************************************

    Try

      Me.MainForm.New_VeniceForm(VeniceFormID.frmInstrumentParent)

    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Handles the SelectedIndexChanged event of the Combo_InstrumentType control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_InstrumentType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Combo_InstrumentType.SelectedIndexChanged
    ' **************************************************************************************
    ' Enable / Disable the Instrument Multiplier field depending on the Selecte dInstrument 
    ' Type.
    '
    ' **************************************************************************************

    Try

      If (Not Me.InPaint) Then

        Me.Check_IsFXFuture.Visible = False
        Me.Label_IsFxFuture.Visible = False
        Me.Label_FixedCurrency.Visible = False
        Me.Combo_FXFixedCurrency.Visible = False
        Me.Label_FixedCurrencyNote.Visible = False

        edit_SharesPerOption.Visible = False
        Label_SharesPerOptionComment.Visible = False
        Label_SharesPerOption.Visible = False

        If (Me.Combo_InstrumentType.SelectedIndex >= 0) AndAlso (IsNumeric(Combo_InstrumentType.SelectedValue)) Then
          Dim ThisType As InstrumentTypes
          ThisType = CType(CInt(Nz(Combo_InstrumentType.SelectedValue, 0)), InstrumentTypes)

          If (ThisType = InstrumentTypes.Cash) OrElse (ThisType = InstrumentTypes.Notional) OrElse (ThisType = InstrumentTypes.Unit) Then
            Me.edit_InstrumentMultiplier.Enabled = False
            Me.edit_BondMultiplier.Enabled = False
            Me.edit_CB_ContractSize.Enabled = False
            Me.edit_ContractSize.Enabled = False
          Else
            Me.edit_InstrumentMultiplier.Enabled = True
            Me.edit_BondMultiplier.Enabled = True
            Me.edit_CB_ContractSize.Enabled = True
            Me.edit_ContractSize.Enabled = True
          End If

          ' Futures specific 

          If (ThisType = InstrumentTypes.Future) Then
            Me.Check_IsFXFuture.Visible = True
            Me.Label_IsFxFuture.Visible = True
            Me.Label_FixedCurrency.Visible = True
            Me.Combo_FXFixedCurrency.Visible = True
            Me.Label_FixedCurrencyNote.Visible = True
          End If

          ' Options specific

          If (ThisType = InstrumentTypes.Option) Then
            edit_SharesPerOption.Visible = True
            Label_SharesPerOptionComment.Visible = True
            Label_SharesPerOption.Visible = True
          End If

        Else
          Me.edit_InstrumentMultiplier.Enabled = True
          Me.edit_BondMultiplier.Enabled = True
          Me.edit_CB_ContractSize.Enabled = True
          Me.edit_ContractSize.Enabled = True

        End If

      End If

    Catch ex As Exception

      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error in Combo_InstrumentType_SelectedIndexChanged(), setting Instrument Multiplier status.", ex.Message, ex.StackTrace, TRANSACTION_SQLCOMMAND_TIMEOUT)

    End Try

  End Sub

  Private Sub Check_IsFXFuture_EnabledChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_IsFXFuture.EnabledChanged
    ' **************************************************************************************
    ' **************************************************************************************

    Try
      Combo_FXFixedCurrency.Enabled = (Check_IsFXFuture.Enabled And Check_IsFXFuture.Checked)
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Check_IsFXFuture_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_IsFXFuture.CheckedChanged
    ' **************************************************************************************
    ' **************************************************************************************

    Try
      Combo_FXFixedCurrency.Enabled = (Check_IsFXFuture.Enabled And Check_IsFXFuture.Checked)
    Catch ex As Exception
    End Try

  End Sub

  'Private Sub edit_DealingBaseDate_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles edit_DealingBaseDate.LostFocus, edit_LatestTradeDate.LostFocus, edit_ExpiryDate.LostFocus
  '	' **************************************************************************************
  '	'
  '	'
  '	' **************************************************************************************

  '	Try
  '		Dim ThisTextBox As C1.Win.C1Input.C1TextBox

  '		If (Not InPaint) AndAlso (Me.Created) AndAlso (Not Me.IsDisposed) Then
  '			If (TypeOf sender Is C1.Win.C1Input.C1TextBox) Then
  '				ThisTextBox = CType(sender, C1.Win.C1Input.C1TextBox)

  '				If IsDate(ThisTextBox.Text) Then
  '					If ThisTextBox.Text <> CDate(ThisTextBox.Text).ToString(RenaissanceGlobals.Globals.DISPLAYMEMBER_DATEFORMAT) Then
  '						ThisTextBox.Text = CDate(ThisTextBox.Text).ToString(RenaissanceGlobals.Globals.DISPLAYMEMBER_DATEFORMAT)
  '					End If
  '				End If

  '			End If

  '		End If
  '	Catch ex As Exception
  '	End Try

  'End Sub

#Region " Navigation Code / GetPosition() (Generic Code) "

  ''' <summary>
  ''' Moves to audit ID.
  ''' </summary>
  ''' <param name="pAuditID">The p audit ID.</param>
  Public Sub MoveToAuditID(ByVal pAuditID As Integer)
    ' ****************************************************************
    ' Subroutine to move the current form focus to a given AuditID
    ' ****************************************************************

    Dim findDataRow As DataRow
    Dim Counter As Integer

    Try
      If (SortedRows Is Nothing) Then Exit Sub
      If (SortedRows.Length <= 0) Then Exit Sub

      For Counter = 0 To (SortedRows.Length - 1)
        findDataRow = SortedRows(Counter)
        If findDataRow("AuditID").Equals(pAuditID) Then
          thisPosition = Counter
          THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
          Combo_SelectComboChanged(THIS_FORM_SelectingCombo, New EventArgs)
          Exit For
        End If
      Next
    Catch ex As Exception
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Moving to given AuditID.", ex.StackTrace, True)
    End Try

  End Sub

  ''' <summary>
  ''' Handles the SelectComboChanged event of the Combo control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_SelectComboChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' Selection Combo. SelectedItem changed.
    '

    ' Don't react to changes made in paint routines etc.
    If InPaint = True Then Exit Sub

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    ' Find the correct data row, then show it...
    thisPosition = THIS_FORM_SelectingCombo.SelectedIndex

    If (thisPosition >= 0) Then
      thisDataRow = Me.SortedRows(thisPosition)
    Else
      thisDataRow = Nothing
    End If

    Call GetFormData(thisDataRow)

  End Sub

  '  Private Sub Combo_SelectCombo_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
  '    ' Key Up Event.
  '    '
  '    ' Managed the item selection process where the user types the name.
  '    '
  '    ' Changed 20/Jan/06 Made into Generic (I Hope) Combo Selection Code
  '    '
  '    Dim SelectString As String
  '    Dim thisRow As DataRow
  '    Dim selectRow As DataRow
  '    Dim selectRowView As DataRowView
  '    Dim CompareValue As Integer
  '    Dim IndexCounter As Integer
  '    Dim ThisComboBox As ComboBox

  '    Try
  '      ThisComboBox = sender
  '    Catch ex As Exception
  '      Exit Sub
  '    End Try

  '    ' Check for <Enter>
  '    If e.KeyCode = System.Windows.Forms.Keys.Enter Then
  '      ThisComboBox.SelectionStart = 0
  '      ThisComboBox.SelectionLength = ThisComboBox.Text.Length
  '      Exit Sub
  '    End If

  '    SelectString = ThisComboBox.Text

  '    If e.KeyCode = System.Windows.Forms.Keys.Back Then
  '      If SelectString.Length > 0 Then
  '        SelectString = SelectString.Substring(0, SelectString.Length - 1)
  '      End If
  '    End If

  '    If e.KeyCode = System.Windows.Forms.Keys.Down Then
  '      Exit Sub
  '    End If

  '    If e.KeyCode = System.Windows.Forms.Keys.Down Then
  '      Exit Sub
  '    End If

  'startSearch:

  '    If SelectString.Length <= 0 Then
  '      If ThisComboBox.Items.Count > 0 Then
  '        ThisComboBox.SelectedIndex = 0
  '        ThisComboBox.SelectionStart = 0
  '        ThisComboBox.SelectionLength = ThisComboBox.Text.Length
  '      Else
  '        ThisComboBox.SelectedIndex = -1
  '      End If

  '      Exit Sub
  '    End If

  '    For IndexCounter = 0 To (ThisComboBox.Items.Count - 1)
  '      Try
  '        If (ThisComboBox.Items(IndexCounter).GetType Is GetType(DataRowView)) Then
  '          selectRowView = ThisComboBox.Items(IndexCounter)
  '          selectRow = selectRowView.Row
  '        Else
  '          selectRow = ThisComboBox.Items(IndexCounter)
  '        End If
  '        If selectRow(ThisComboBox.DisplayMember).ToString.ToUpper.StartsWith(SelectString.ToUpper) Then
  '          If (ThisComboBox.SelectedIndex <> IndexCounter) Then
  '            ThisComboBox.SelectedIndex = IndexCounter
  '            ThisComboBox.SelectionStart = SelectString.Length
  '            ThisComboBox.SelectionLength = selectRow(ThisComboBox.DisplayMember).ToString.Length - SelectString.Length
  '          End If
  '          Exit Sub
  '        End If
  '      Catch ex As Exception
  '      End Try
  '    Next

  '    ' String not found
  '    SelectString = SelectString.Substring(0, SelectString.Length - 1)
  '    GoTo startSearch

  '  End Sub

  ''' <summary>
  ''' Handles the Click event of the btnNavPrev control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnNavPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavPrev.Click
    ' 'Previous' Button.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    If thisPosition > 0 Then thisPosition -= 1
    If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
      thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
    End If

    THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

    Exit Sub

    If myTable.Rows.Count > thisPosition Then
      thisDataRow = myTable.Rows(thisPosition)
      Call GetFormData(thisDataRow)
    Else
      Call GetFormData(Nothing)
    End If

  End Sub

  ''' <summary>
  ''' Handles the Click event of the btnNavNext control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnNavNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavNext.Click
    ' 'Next' Button.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    If thisPosition < (myTable.Rows.Count - 1) Then thisPosition += 1
    If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
      thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
    End If

    THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

    Exit Sub

    If myTable.Rows.Count > thisPosition Then
      thisDataRow = myTable.Rows(thisPosition)
      Call GetFormData(thisDataRow)
    Else
      Call GetFormData(Nothing)
    End If

  End Sub

  ''' <summary>
  ''' Handles the Click event of the btnNavFirst control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnNavFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavFirst.Click
    ' 'First' Button.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    thisPosition = 0
    If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
      thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
    End If

    If (THIS_FORM_SelectingCombo.SelectedIndex = thisPosition) Then
      Combo_SelectComboChanged(THIS_FORM_SelectingCombo, New EventArgs)
    Else
      THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
    End If

  End Sub

  ''' <summary>
  ''' Handles the Click event of the btnLast control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLast.Click
    ' 'Last' Button.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    thisPosition = THIS_FORM_SelectingCombo.Items.Count - 1

    THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

  End Sub

  ''' <summary>
  ''' Get_s the position.
  ''' </summary>
  ''' <param name="pAuditID">The p audit ID.</param>
  ''' <returns>System.Int32.</returns>
  Private Function Get_Position(ByVal pAuditID As Integer) As Integer
    ' Returns the position in the current 'SortedRows' array of the entry corresponding to the given 
    ' AudidID.

    Dim MinIndex As Integer
    Dim MaxIndex As Integer
    Dim CurrentMin As Integer
    Dim CurrentMax As Integer
    Dim searchDataRow As DataRow

    Try
      ' SortedRows exists ?

      If SortedRows Is Nothing Then
        Return (-1)
        Exit Function
      End If

      ' Return (-1) if there are no rows in the 'SortedRows'

      If (SortedRows.GetLength(0) <= 0) Then
        Return (-1)
        Exit Function
      End If


      ' Use a modified Search moving outwards from the last returned value to locate the required value.
      ' Reflecting the fact that for the most part One looks for closely located records.

      MinIndex = 0
      MaxIndex = SortedRows.GetLength(0) - 1


      ' Check First and Last records (Incase 'First' or 'Last' was pressed).

      searchDataRow = SortedRows(MinIndex)

      If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(searchDataRow("AuditID")) = pAuditID) Then
        Return MinIndex
        Exit Function
      End If

      searchDataRow = SortedRows(MaxIndex)
      If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(searchDataRow("AuditID")) = pAuditID) Then
        Return MaxIndex
        Exit Function
      End If

      ' now search outwards from the last returned value.

      If (Me.thisPosition >= MinIndex) And (Me.thisPosition <= MaxIndex) Then
        CurrentMin = thisPosition
        CurrentMax = CurrentMin + 1
      Else
        CurrentMin = CInt((MinIndex + MaxIndex) / 2)
        CurrentMax = CurrentMin + 1
      End If

      While (CurrentMin >= MinIndex) Or (CurrentMax <= MaxIndex)
        If (CurrentMin >= MinIndex) Then
          searchDataRow = SortedRows(CurrentMin)

          If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(searchDataRow("AuditID")) = pAuditID) Then
            Return CurrentMin
            Exit Function
          End If

          CurrentMin -= 1
        End If

        If (CurrentMax <= MaxIndex) Then
          searchDataRow = SortedRows(CurrentMax)

          If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(searchDataRow("AuditID")) = pAuditID) Then
            Return CurrentMax
            Exit Function
          End If

          CurrentMax += 1
        End If

      End While

      Return (-1)
      Exit Function

    Catch ex As Exception
      MainForm.LogError(Me.Name & ", Get_Position()", LOG_LEVELS.Warning, ex.Message, "", ex.StackTrace, True)
      Return (-1)
      Exit Function
    End Try

  End Function

#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

  ''' <summary>
  ''' Handles the Click event of the btnCancel control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    ' Cancel Changes, redisplay form.

    FormChanged = False
    AddNewRecord = False

    Me.THIS_FORM_SelectingCombo.Enabled = True

    If (thisPosition >= 0) And (thisPosition < Me.SortedRows.GetLength(0)) Then
      thisDataRow = Me.SortedRows(thisPosition)
      Call GetFormData(thisDataRow)
    Else
      Call btnNavFirst_Click(Me, New System.EventArgs)
    End If

  End Sub

  ''' <summary>
  ''' Handles the Click event of the btnSave control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
    ' Save Changes, if any, without prompting.

    If (FormChanged = True) Then
      Call SetFormData(False)
    End If

  End Sub

  ''' <summary>
  ''' Handles the Click event of the btnDelete control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
    Dim Position As Integer
    Dim NextAuditID As Integer

    If (AddNewRecord = True) Then
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Sub
    End If

    ' No Appropriate Save permission :-

    If (Me.HasDeletePermission = False) Then
      MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "You do not have permission to Delete this record.", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Sub
    End If

    ' *************************************************************
    ' KnowledgeDate OK :-
    ' *************************************************************

    If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
      MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "Changes to Venice are not allowed when the Knowledgedate is not set to `NOW`", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Sub
    End If

    ' *************************************************************
    ' Confirm :-
    ' *************************************************************
    If MsgBox("Are you Sure ?", MsgBoxStyle.OkCancel, "Confirm Delete") = MsgBoxResult.Cancel Then
      Exit Sub
    End If

    ' Check Data position.

    Position = Get_Position(thisAuditID)

    If Position < 0 Then Exit Sub

    ' Check Referential Integrity 
    If CheckReferentialIntegrity(_MainForm, Me.SortedRows(Position)) = False Then
      MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "Deleting this record would voilate referential integrity.", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Sub
    End If

    ' Resolve row to show after deleting this one.

    NextAuditID = (-1)
    If (Position + 1) < Me.SortedRows.GetLength(0) Then
      NextAuditID = CInt(Me.SortedRows(Position + 1)("AuditID"))
    ElseIf (Position - 1) >= 0 Then
      NextAuditID = CInt(Me.SortedRows(Position - 1)("AuditID"))
    Else
      NextAuditID = (-1)
    End If

    ' Delete this row

    Try
      Me.SortedRows(Position).Delete()

      MainForm.AdaptorUpdate(Me.Name, myAdaptor, myTable)

    Catch ex As Exception

      Call MainForm.LogError(Me.Name & ", Delete()", LOG_LEVELS.Error, ex.Message, "Error Deleting Record.", ex.StackTrace, True)
    End Try

    ' Tidy Up.

    FormChanged = False

    thisAuditID = NextAuditID
    Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID), True)

  End Sub

  ''' <summary>
  ''' Handles the Click event of the btnAdd control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click, btnAddCopy.Click
    ' Prepare form to Add a new record.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    thisPosition = -1
    InPaint = True
    MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
    InPaint = False

    If (CType(sender, Button) IsNot btnAddCopy) Then
      GetFormData(Nothing)
    Else
      thisAuditID = (-1)
      Me.edit_InstrumentDescription.Text &= " (Copy)"
    End If
    AddNewRecord = True

    Me.THIS_FORM_SelectingCombo.Enabled = False
    Me.btnCancel.Enabled = True

    Call SetButtonStatus()

    THIS_FORM_NewMoveToControl.Focus()

  End Sub

  ''' <summary>
  ''' Handles the Click event of the btnClose control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
    ' Close Form

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    Me.Close()

  End Sub

#End Region

#Region " Bug hunting "

  ' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
  ' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
  ' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

  ''' <summary>
  ''' Called when [row updating].
  ''' </summary>
  ''' <param name="Sender">The sender.</param>
  ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  ''' <summary>
  ''' Called when [row updated].
  ''' </summary>
  ''' <param name="Sender">The sender.</param>
  ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  ''' <summary>
  ''' Called when [row fill error].
  ''' </summary>
  ''' <param name="Sender">The sender.</param>
  ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
    Dim a As Integer

    a = 1
  End Sub

#End Region







End Class
