' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmSetFundMilestone.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals
Imports RenaissanceUtilities.DatePeriodFunctions

Imports System.Threading
Imports RenaissanceDataClass
Imports System.Math

''' <summary>
''' Class frmSetFundMilestone
''' </summary>
Public Class frmSetFundMilestone

	Inherits System.Windows.Forms.Form
	Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmSetFundMilestone"/> class from being created.
    ''' </summary>
	Private Sub New()
		MyBase.New()

		'This call is required by the Windows Form Designer.
		InitializeComponent()

		'Add any initialization after the InitializeComponent() call

	End Sub

	'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
    ''' <summary>
    ''' The BTN set milestone
    ''' </summary>
	Friend WithEvents btnSetMilestone As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN close
    ''' </summary>
	Friend WithEvents btnClose As System.Windows.Forms.Button
    ''' <summary>
    ''' The label2
    ''' </summary>
	Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ fund
    ''' </summary>
	Friend WithEvents Combo_Fund As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The panel5
    ''' </summary>
	Friend WithEvents Panel5 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The label1
    ''' </summary>
	Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label3
    ''' </summary>
	Friend WithEvents Label3 As System.Windows.Forms.Label
    ''' <summary>
    ''' The panel2
    ''' </summary>
	Friend WithEvents Panel2 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The panel3
    ''' </summary>
	Friend WithEvents Panel3 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The radio_ precise time
    ''' </summary>
	Friend WithEvents Radio_PreciseTime As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The radio_ whole day
    ''' </summary>
	Friend WithEvents Radio_WholeDay As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The date_ knowledge date
    ''' </summary>
	Friend WithEvents Date_KnowledgeDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The radio_ live
    ''' </summary>
	Friend WithEvents Radio_Live As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The edit_ unit price
    ''' </summary>
	Friend WithEvents edit_UnitPrice As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The label4
    ''' </summary>
	Friend WithEvents Label4 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ milestone date
    ''' </summary>
	Friend WithEvents Combo_MilestoneDate As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The button_ set knowledge date
    ''' </summary>
	Friend WithEvents Button_SetKnowledgeDate As System.Windows.Forms.Button
    ''' <summary>
    ''' The date_ MS date picker
    ''' </summary>
	Friend WithEvents Date_MSDatePicker As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The form_ status strip
    ''' </summary>
	Friend WithEvents Form_StatusStrip As System.Windows.Forms.StatusStrip
    ''' <summary>
    ''' The form_ progress bar
    ''' </summary>
	Friend WithEvents Form_ProgressBar As System.Windows.Forms.ToolStripProgressBar
    ''' <summary>
    ''' The label_ status
    ''' </summary>
	Friend WithEvents Label_Status As System.Windows.Forms.ToolStripStatusLabel
    ''' <summary>
    ''' The label5
    ''' </summary>
	Friend WithEvents Label5 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label6
    ''' </summary>
	Friend WithEvents Label6 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ BNAV
    ''' </summary>
	Friend WithEvents edit_BNAV As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The label7
    ''' </summary>
	Friend WithEvents Label7 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ GNAV
    ''' </summary>
	Friend WithEvents edit_GNAV As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The label8
    ''' </summary>
	Friend WithEvents Label8 As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit_ GAV
    ''' </summary>
	Friend WithEvents edit_GAV As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The BTN_ save all fund fees
    ''' </summary>
	Friend WithEvents btn_SaveAllFundFees As System.Windows.Forms.Button
    ''' <summary>
    ''' The check_ save fund fees
    ''' </summary>
	Friend WithEvents Check_SaveFundFees As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The group box1
    ''' </summary>
	Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The check_ S r_ ignore todays trades
    ''' </summary>
	Friend WithEvents Check_SR_IgnoreTodaysTrades As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ S r_ ignore status
    ''' </summary>
	Friend WithEvents Check_SR_IgnoreStatus As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The combo_ transaction group
    ''' </summary>
	Friend WithEvents Combo_TransactionGroup As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label9
    ''' </summary>
	Friend WithEvents Label9 As System.Windows.Forms.Label
    ''' <summary>
    ''' The button_ get price
    ''' </summary>
	Friend WithEvents Button_GetPrice As System.Windows.Forms.Button
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Me.btnSetMilestone = New System.Windows.Forms.Button
		Me.btnClose = New System.Windows.Forms.Button
		Me.Label2 = New System.Windows.Forms.Label
		Me.Combo_Fund = New System.Windows.Forms.ComboBox
		Me.Panel5 = New System.Windows.Forms.Panel
		Me.Radio_Live = New System.Windows.Forms.RadioButton
		Me.Radio_PreciseTime = New System.Windows.Forms.RadioButton
		Me.Radio_WholeDay = New System.Windows.Forms.RadioButton
		Me.Date_KnowledgeDate = New System.Windows.Forms.DateTimePicker
		Me.Label1 = New System.Windows.Forms.Label
		Me.Label3 = New System.Windows.Forms.Label
		Me.Panel2 = New System.Windows.Forms.Panel
		Me.Panel3 = New System.Windows.Forms.Panel
		Me.edit_UnitPrice = New RenaissanceControls.NumericTextBox
		Me.Label4 = New System.Windows.Forms.Label
		Me.Combo_MilestoneDate = New System.Windows.Forms.ComboBox
		Me.Button_SetKnowledgeDate = New System.Windows.Forms.Button
		Me.Date_MSDatePicker = New System.Windows.Forms.DateTimePicker
		Me.Button_GetPrice = New System.Windows.Forms.Button
		Me.Form_StatusStrip = New System.Windows.Forms.StatusStrip
		Me.Form_ProgressBar = New System.Windows.Forms.ToolStripProgressBar
		Me.Label_Status = New System.Windows.Forms.ToolStripStatusLabel
		Me.Label5 = New System.Windows.Forms.Label
		Me.Label6 = New System.Windows.Forms.Label
		Me.edit_BNAV = New RenaissanceControls.NumericTextBox
		Me.Label7 = New System.Windows.Forms.Label
		Me.edit_GNAV = New RenaissanceControls.NumericTextBox
		Me.Label8 = New System.Windows.Forms.Label
		Me.edit_GAV = New RenaissanceControls.NumericTextBox
		Me.btn_SaveAllFundFees = New System.Windows.Forms.Button
		Me.Check_SaveFundFees = New System.Windows.Forms.CheckBox
		Me.GroupBox1 = New System.Windows.Forms.GroupBox
		Me.Check_SR_IgnoreTodaysTrades = New System.Windows.Forms.CheckBox
		Me.Check_SR_IgnoreStatus = New System.Windows.Forms.CheckBox
		Me.Combo_TransactionGroup = New System.Windows.Forms.ComboBox
		Me.Label9 = New System.Windows.Forms.Label
		Me.Panel5.SuspendLayout()
		Me.Panel2.SuspendLayout()
		Me.Form_StatusStrip.SuspendLayout()
		Me.GroupBox1.SuspendLayout()
		Me.SuspendLayout()
		'
		'btnSetMilestone
		'
		Me.btnSetMilestone.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnSetMilestone.Location = New System.Drawing.Point(16, 365)
		Me.btnSetMilestone.Name = "btnSetMilestone"
		Me.btnSetMilestone.Size = New System.Drawing.Size(168, 38)
		Me.btnSetMilestone.TabIndex = 13
		Me.btnSetMilestone.Text = "Set Milestone"
		'
		'btnClose
		'
		Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnClose.Location = New System.Drawing.Point(220, 413)
		Me.btnClose.Name = "btnClose"
		Me.btnClose.Size = New System.Drawing.Size(168, 38)
		Me.btnClose.TabIndex = 16
		Me.btnClose.Text = "&Close"
		'
		'Label2
		'
		Me.Label2.Location = New System.Drawing.Point(16, 16)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(120, 20)
		Me.Label2.TabIndex = 53
		Me.Label2.Text = "Fund Name"
		'
		'Combo_Fund
		'
		Me.Combo_Fund.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_Fund.Location = New System.Drawing.Point(140, 12)
		Me.Combo_Fund.Name = "Combo_Fund"
		Me.Combo_Fund.Size = New System.Drawing.Size(252, 21)
		Me.Combo_Fund.TabIndex = 0
		'
		'Panel5
		'
		Me.Panel5.Controls.Add(Me.Radio_Live)
		Me.Panel5.Controls.Add(Me.Radio_PreciseTime)
		Me.Panel5.Controls.Add(Me.Radio_WholeDay)
		Me.Panel5.Location = New System.Drawing.Point(16, 100)
		Me.Panel5.Name = "Panel5"
		Me.Panel5.Size = New System.Drawing.Size(372, 24)
		Me.Panel5.TabIndex = 4
		'
		'Radio_Live
		'
		Me.Radio_Live.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Radio_Live.Checked = True
		Me.Radio_Live.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Radio_Live.Location = New System.Drawing.Point(12, 4)
		Me.Radio_Live.Name = "Radio_Live"
		Me.Radio_Live.Size = New System.Drawing.Size(104, 16)
		Me.Radio_Live.TabIndex = 0
		Me.Radio_Live.TabStop = True
		Me.Radio_Live.Text = "'Live' KD"
		'
		'Radio_PreciseTime
		'
		Me.Radio_PreciseTime.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Radio_PreciseTime.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Radio_PreciseTime.Location = New System.Drawing.Point(252, 4)
		Me.Radio_PreciseTime.Name = "Radio_PreciseTime"
		Me.Radio_PreciseTime.Size = New System.Drawing.Size(104, 16)
		Me.Radio_PreciseTime.TabIndex = 2
		Me.Radio_PreciseTime.Text = "Precise Time"
		'
		'Radio_WholeDay
		'
		Me.Radio_WholeDay.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Radio_WholeDay.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Radio_WholeDay.Location = New System.Drawing.Point(128, 4)
		Me.Radio_WholeDay.Name = "Radio_WholeDay"
		Me.Radio_WholeDay.Size = New System.Drawing.Size(104, 16)
		Me.Radio_WholeDay.TabIndex = 1
		Me.Radio_WholeDay.Text = "WholeDay"
		'
		'Date_KnowledgeDate
		'
		Me.Date_KnowledgeDate.CustomFormat = "dd MMM yyyy"
		Me.Date_KnowledgeDate.Enabled = False
		Me.Date_KnowledgeDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
		Me.Date_KnowledgeDate.Location = New System.Drawing.Point(140, 76)
		Me.Date_KnowledgeDate.Name = "Date_KnowledgeDate"
		Me.Date_KnowledgeDate.Size = New System.Drawing.Size(252, 20)
		Me.Date_KnowledgeDate.TabIndex = 3
		'
		'Label1
		'
		Me.Label1.Location = New System.Drawing.Point(16, 48)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(116, 16)
		Me.Label1.TabIndex = 108
		Me.Label1.Text = "Milestone Date"
		'
		'Label3
		'
		Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label3.Location = New System.Drawing.Point(16, 80)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(116, 16)
		Me.Label3.TabIndex = 112
		Me.Label3.Text = "Knowledge Date"
		'
		'Panel2
		'
		Me.Panel2.Controls.Add(Me.Panel3)
		Me.Panel2.Location = New System.Drawing.Point(-8, 357)
		Me.Panel2.Name = "Panel2"
		Me.Panel2.Size = New System.Drawing.Size(428, 2)
		Me.Panel2.TabIndex = 116
		'
		'Panel3
		'
		Me.Panel3.Location = New System.Drawing.Point(0, 116)
		Me.Panel3.Name = "Panel3"
		Me.Panel3.Size = New System.Drawing.Size(428, 2)
		Me.Panel3.TabIndex = 48
		'
		'edit_UnitPrice
		'
		Me.edit_UnitPrice.Location = New System.Drawing.Point(140, 227)
		Me.edit_UnitPrice.Name = "edit_UnitPrice"
		Me.edit_UnitPrice.RenaissanceTag = Nothing
		Me.edit_UnitPrice.Size = New System.Drawing.Size(125, 20)
		Me.edit_UnitPrice.TabIndex = 7
		Me.edit_UnitPrice.Text = "0.0000"
		Me.edit_UnitPrice.TextFormat = "#,##0.0000####"
		Me.edit_UnitPrice.Value = 0
		'
		'Label4
		'
		Me.Label4.Location = New System.Drawing.Point(16, 230)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(120, 20)
		Me.Label4.TabIndex = 119
		Me.Label4.Text = "Final Fund Unit Price"
		'
		'Combo_MilestoneDate
		'
		Me.Combo_MilestoneDate.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_MilestoneDate.Location = New System.Drawing.Point(140, 44)
		Me.Combo_MilestoneDate.Name = "Combo_MilestoneDate"
		Me.Combo_MilestoneDate.Size = New System.Drawing.Size(228, 21)
		Me.Combo_MilestoneDate.TabIndex = 1
		'
		'Button_SetKnowledgeDate
		'
		Me.Button_SetKnowledgeDate.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Button_SetKnowledgeDate.Location = New System.Drawing.Point(220, 365)
		Me.Button_SetKnowledgeDate.Name = "Button_SetKnowledgeDate"
		Me.Button_SetKnowledgeDate.Size = New System.Drawing.Size(168, 38)
		Me.Button_SetKnowledgeDate.TabIndex = 14
		Me.Button_SetKnowledgeDate.Text = "Set Knowledge Date to this Milestone"
		'
		'Date_MSDatePicker
		'
		Me.Date_MSDatePicker.Location = New System.Drawing.Point(371, 44)
		Me.Date_MSDatePicker.Name = "Date_MSDatePicker"
		Me.Date_MSDatePicker.Size = New System.Drawing.Size(21, 20)
		Me.Date_MSDatePicker.TabIndex = 2
		'
		'Button_GetPrice
		'
		Me.Button_GetPrice.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Button_GetPrice.Location = New System.Drawing.Point(312, 226)
		Me.Button_GetPrice.Name = "Button_GetPrice"
		Me.Button_GetPrice.Size = New System.Drawing.Size(72, 20)
		Me.Button_GetPrice.TabIndex = 8
		Me.Button_GetPrice.Text = "Get Price"
		'
		'Form_StatusStrip
		'
		Me.Form_StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Form_ProgressBar, Me.Label_Status})
		Me.Form_StatusStrip.Location = New System.Drawing.Point(0, 471)
		Me.Form_StatusStrip.Name = "Form_StatusStrip"
		Me.Form_StatusStrip.Size = New System.Drawing.Size(402, 22)
		Me.Form_StatusStrip.TabIndex = 127
		Me.Form_StatusStrip.Text = " "
		'
		'Form_ProgressBar
		'
		Me.Form_ProgressBar.Maximum = 20
		Me.Form_ProgressBar.Name = "Form_ProgressBar"
		Me.Form_ProgressBar.Size = New System.Drawing.Size(150, 17)
		Me.Form_ProgressBar.Step = 1
		Me.Form_ProgressBar.Visible = False
		'
		'Label_Status
		'
		Me.Label_Status.Name = "Label_Status"
		Me.Label_Status.Size = New System.Drawing.Size(10, 17)
		Me.Label_Status.Text = " "
		'
		'Label5
		'
		Me.Label5.Location = New System.Drawing.Point(265, 230)
		Me.Label5.Name = "Label5"
		Me.Label5.Size = New System.Drawing.Size(43, 19)
		Me.Label5.TabIndex = 128
		Me.Label5.Text = "NAV"
		'
		'Label6
		'
		Me.Label6.Location = New System.Drawing.Point(265, 256)
		Me.Label6.Name = "Label6"
		Me.Label6.Size = New System.Drawing.Size(127, 19)
		Me.Label6.TabIndex = 130
		Me.Label6.Text = "GNAV - Ex Performance"
		'
		'edit_BNAV
		'
		Me.edit_BNAV.Location = New System.Drawing.Point(140, 279)
		Me.edit_BNAV.Name = "edit_BNAV"
		Me.edit_BNAV.RenaissanceTag = Nothing
		Me.edit_BNAV.Size = New System.Drawing.Size(125, 20)
		Me.edit_BNAV.TabIndex = 10
		Me.edit_BNAV.Text = "0.0000"
		Me.edit_BNAV.TextFormat = "#,##0.0000####"
		Me.edit_BNAV.Value = 0
		'
		'Label7
		'
		Me.Label7.Location = New System.Drawing.Point(265, 282)
		Me.Label7.Name = "Label7"
		Me.Label7.Size = New System.Drawing.Size(125, 19)
		Me.Label7.TabIndex = 132
		Me.Label7.Text = "BNAV - Ex Perf && Mgmt"
		'
		'edit_GNAV
		'
		Me.edit_GNAV.Location = New System.Drawing.Point(140, 253)
		Me.edit_GNAV.Name = "edit_GNAV"
		Me.edit_GNAV.RenaissanceTag = Nothing
		Me.edit_GNAV.Size = New System.Drawing.Size(125, 20)
		Me.edit_GNAV.TabIndex = 9
		Me.edit_GNAV.Text = "0.0000"
		Me.edit_GNAV.TextFormat = "#,##0.0000####"
		Me.edit_GNAV.Value = 0
		'
		'Label8
		'
		Me.Label8.Location = New System.Drawing.Point(265, 307)
		Me.Label8.Name = "Label8"
		Me.Label8.Size = New System.Drawing.Size(119, 19)
		Me.Label8.TabIndex = 134
		Me.Label8.Text = "GAV - Ex All Fees"
		'
		'edit_GAV
		'
		Me.edit_GAV.Location = New System.Drawing.Point(140, 304)
		Me.edit_GAV.Name = "edit_GAV"
		Me.edit_GAV.RenaissanceTag = Nothing
		Me.edit_GAV.Size = New System.Drawing.Size(125, 20)
		Me.edit_GAV.TabIndex = 11
		Me.edit_GAV.Text = "0.0000"
		Me.edit_GAV.TextFormat = "#,##0.0000####"
		Me.edit_GAV.Value = 0
		'
		'btn_SaveAllFundFees
		'
		Me.btn_SaveAllFundFees.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btn_SaveAllFundFees.Location = New System.Drawing.Point(16, 413)
		Me.btn_SaveAllFundFees.Name = "btn_SaveAllFundFees"
		Me.btn_SaveAllFundFees.Size = New System.Drawing.Size(168, 38)
		Me.btn_SaveAllFundFees.TabIndex = 15
		Me.btn_SaveAllFundFees.Text = "Save ALL Milestoned Fund Fees"
		'
		'Check_SaveFundFees
		'
		Me.Check_SaveFundFees.AutoSize = True
		Me.Check_SaveFundFees.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Check_SaveFundFees.Location = New System.Drawing.Point(19, 334)
		Me.Check_SaveFundFees.Name = "Check_SaveFundFees"
		Me.Check_SaveFundFees.Size = New System.Drawing.Size(104, 17)
		Me.Check_SaveFundFees.TabIndex = 12
		Me.Check_SaveFundFees.Text = "Save Fund Fees"
		Me.Check_SaveFundFees.UseVisualStyleBackColor = True
		'
		'GroupBox1
		'
		Me.GroupBox1.Controls.Add(Me.Check_SR_IgnoreTodaysTrades)
		Me.GroupBox1.Controls.Add(Me.Check_SR_IgnoreStatus)
		Me.GroupBox1.Location = New System.Drawing.Point(5, 166)
		Me.GroupBox1.Name = "GroupBox1"
		Me.GroupBox1.Size = New System.Drawing.Size(391, 48)
		Me.GroupBox1.TabIndex = 6
		Me.GroupBox1.TabStop = False
		Me.GroupBox1.Text = "Subscriptions and Redemptions"
		'
		'Check_SR_IgnoreTodaysTrades
		'
		Me.Check_SR_IgnoreTodaysTrades.AutoSize = True
		Me.Check_SR_IgnoreTodaysTrades.Location = New System.Drawing.Point(183, 22)
		Me.Check_SR_IgnoreTodaysTrades.Name = "Check_SR_IgnoreTodaysTrades"
		Me.Check_SR_IgnoreTodaysTrades.Size = New System.Drawing.Size(130, 17)
		Me.Check_SR_IgnoreTodaysTrades.TabIndex = 1
		Me.Check_SR_IgnoreTodaysTrades.Text = "Ignore 'Todays' trades"
		Me.Check_SR_IgnoreTodaysTrades.UseVisualStyleBackColor = True
		'
		'Check_SR_IgnoreStatus
		'
		Me.Check_SR_IgnoreStatus.AutoSize = True
		Me.Check_SR_IgnoreStatus.Location = New System.Drawing.Point(25, 22)
		Me.Check_SR_IgnoreStatus.Name = "Check_SR_IgnoreStatus"
		Me.Check_SR_IgnoreStatus.Size = New System.Drawing.Size(120, 17)
		Me.Check_SR_IgnoreStatus.TabIndex = 0
		Me.Check_SR_IgnoreStatus.Text = "Ignore Trade Status"
		Me.Check_SR_IgnoreStatus.UseVisualStyleBackColor = True
		'
		'Combo_TransactionGroup
		'
		Me.Combo_TransactionGroup.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_TransactionGroup.Location = New System.Drawing.Point(140, 135)
		Me.Combo_TransactionGroup.Name = "Combo_TransactionGroup"
		Me.Combo_TransactionGroup.Size = New System.Drawing.Size(252, 21)
		Me.Combo_TransactionGroup.TabIndex = 5
		'
		'Label9
		'
		Me.Label9.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label9.Location = New System.Drawing.Point(18, 138)
		Me.Label9.Name = "Label9"
		Me.Label9.Size = New System.Drawing.Size(114, 16)
		Me.Label9.TabIndex = 140
		Me.Label9.Text = "Transaction Group"
		'
		'frmSetFundMilestone
		'
		Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
		Me.ClientSize = New System.Drawing.Size(402, 493)
		Me.Controls.Add(Me.GroupBox1)
		Me.Controls.Add(Me.Combo_TransactionGroup)
		Me.Controls.Add(Me.Label9)
		Me.Controls.Add(Me.Check_SaveFundFees)
		Me.Controls.Add(Me.btn_SaveAllFundFees)
		Me.Controls.Add(Me.Label8)
		Me.Controls.Add(Me.edit_GAV)
		Me.Controls.Add(Me.Label7)
		Me.Controls.Add(Me.edit_GNAV)
		Me.Controls.Add(Me.Label6)
		Me.Controls.Add(Me.edit_BNAV)
		Me.Controls.Add(Me.Label5)
		Me.Controls.Add(Me.Form_StatusStrip)
		Me.Controls.Add(Me.Button_GetPrice)
		Me.Controls.Add(Me.Date_MSDatePicker)
		Me.Controls.Add(Me.Button_SetKnowledgeDate)
		Me.Controls.Add(Me.Combo_MilestoneDate)
		Me.Controls.Add(Me.edit_UnitPrice)
		Me.Controls.Add(Me.Label4)
		Me.Controls.Add(Me.Panel2)
		Me.Controls.Add(Me.Label3)
		Me.Controls.Add(Me.Label1)
		Me.Controls.Add(Me.Panel5)
		Me.Controls.Add(Me.Date_KnowledgeDate)
		Me.Controls.Add(Me.Combo_Fund)
		Me.Controls.Add(Me.Label2)
		Me.Controls.Add(Me.btnClose)
		Me.Controls.Add(Me.btnSetMilestone)
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Name = "frmSetFundMilestone"
		Me.Text = "Set Fund Milestone"
		Me.Panel5.ResumeLayout(False)
		Me.Panel2.ResumeLayout(False)
		Me.Form_StatusStrip.ResumeLayout(False)
		Me.Form_StatusStrip.PerformLayout()
		Me.GroupBox1.ResumeLayout(False)
		Me.GroupBox1.PerformLayout()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

#End Region

#Region " Form Locals and Constants "


	' Form 'Parent', the Main Venice form.
	' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
	Private WithEvents _MainForm As VeniceMain

	' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
	Private FormTooltip As New ToolTip()

    ''' <summary>
    ''' The form controls
    ''' </summary>
	Private FormControls As ArrayList = Nothing
    ''' <summary>
    ''' The report timer
    ''' </summary>
	Private WithEvents ReportTimer As New Windows.Forms.Timer()

	' Form Constants, specific to the table being updated.

    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
	Private ALWAYS_CLOSE_THIS_FORM As Boolean = True

	' The standard ChangeID for this form. e.g. tblPerson
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
	Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

	' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
	Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
	Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

	' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
	Private THIS_FORM_FormID As VeniceFormID

	' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
	Private FormIsValid As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
	Private _FormOpenFailed As Boolean
    ''' <summary>
    ''' The in paint
    ''' </summary>
	Private InPaint As Boolean

	' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
	Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
	Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
	Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
	Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
	Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
	Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return False
		End Get
		Set(ByVal Value As Boolean)
		End Set
	End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
	Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
		Get
			Return False
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
		Get
			Return True
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmSetFundMilestone"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
	Public Sub New(ByVal pMainForm As VeniceMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()

		_MainForm = pMainForm
		AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate
		AddHandler ReportTimer.Tick, AddressOf MainForm.IncrementStatusBarTimerEvent

		_FormOpenFailed = False
		InPaint = False

		' ******************************************************
		' Form Specific Settings :
		' ******************************************************

		' Form Permissioning :-

		THIS_FORM_PermissionArea = Me.Name
		THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

		' 'This' form ID

		THIS_FORM_FormID = VeniceFormID.frmSetFundMilestone

		' Format Event Handlers for form controls

		AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

		AddHandler Combo_Fund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_Fund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_Fund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_Fund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

		' ******************************************************
		' End Form Specific.
		' ******************************************************

		THIS_FORM_ChangeID = RenaissanceChangeID.KnowledgeDate

	End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

	' Form Initialisation code.
	'
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
	Public Sub ResetForm() Implements StandardVeniceForm.ResetForm

		Call Form_Load(Me, New System.EventArgs)
	End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
	Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
		_FormOpenFailed = False

		If Not (MainForm Is Nothing) Then
			FormIsValid = True
		Else
			MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		' Check User permissions

		Call CheckPermissions()
		If (HasReadPermission = False) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		' Build Combos

		Try
			Call SetFundCombo()
			Call SetTransactionGroupCombo()

			MainForm.SetComboSelectionLengths(Me)

			Call SetMilestoneDateCombo()
		Catch ex As Exception
		End Try


		' Default KnowledgeDate values

		Me.Radio_Live.Checked = True
		Me.Date_KnowledgeDate.Value = CDate(Now.Date.ToString(DISPLAYMEMBER_LONGDATEFORMAT))

		Check_SR_IgnoreStatus.Checked = False
		Check_SR_IgnoreTodaysTrades.Checked = True

		If (Combo_TransactionGroup.Items.Count > 0) Then
			Me.Combo_TransactionGroup.SelectedValue = DEFAULT_STATUSGROUPFILTER_EOD
		End If

		' Default Report Date Values - End Of month dates for the 'Current'ish month .
		' Set this after the default values above as the selection of a report date may set Report KD values.


	End Sub

    ''' <summary>
    ''' Handles the Closing event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
	Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		Dim HideForm As Boolean

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...

		If (Me.btnClose.Enabled = False) Then
			e.Cancel = True
			Exit Sub
		End If

		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else
			HideForm = True
			If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.NoCache_CacheCount Then	' MAX_CACHED_FORM_COUNT
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			e.Cancel = True
		Else
			Try
				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

				RemoveHandler Combo_Fund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_Fund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_Fund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_Fund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

			Catch ex As Exception
			End Try
		End If

	End Sub


#End Region

	' Routine to handle changes / updates to tables by this and other windows.
	' If this, or any other, form posts a change to a table, then it will invoke an update event 
	' detailing what tables have been altered.
	' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
	' the 'VeniceAutoUpdate' event of the main Venice form.
	' Each form may them react as appropriate to changes in any table that might impact it.
	'
    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		Dim KnowledgeDateChanged As Boolean
		Dim SetButtonStatus_Flag As Boolean

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		KnowledgeDateChanged = False
		SetButtonStatus_Flag = False

		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
			KnowledgeDateChanged = True
		End If

		' ****************************************************************
		' Check for changes relevant to this form
		' ****************************************************************

		' Changes to the KnowledgeDate :-
		If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
			SetButtonStatus_Flag = True
		End If

		' Changes to the tblFund table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFund) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			Call SetFundCombo()
		End If


		' Changes to the tblUserPermissions table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

			' Check ongoing permissions.

			Call CheckPermissions()
			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

				FormIsValid = False
				Me.Close()
				Exit Sub
			End If

			SetButtonStatus_Flag = True

		End If

		' ****************************************************************
		'
		' ****************************************************************
		' Changes to the tblUserPermissions table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFundMilestones) = True) Or KnowledgeDateChanged Then
			Call Me.SetMilestoneDateCombo()

			GetFormData()

		End If


	End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

	' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
	Private Sub CheckPermissions()
		' *****************************************************************************
		'
		' *****************************************************************************

		Dim Permissions As Integer

		Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

		HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
		HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
		HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
		HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

	End Sub


    ''' <summary>
    ''' Gets the form data.
    ''' </summary>
    ''' <param name="pIgnoreInPaint">if set to <c>true</c> [p ignore in paint].</param>
	Private Sub GetFormData(Optional ByVal pIgnoreInPaint As Boolean = False)
		' *****************************************************************************
		'
		' *****************************************************************************

		Dim FundID As Integer
		Dim MilestoneDate As Date
		Dim MilestoneDS As RenaissanceDataClass.DSFundMilestones
		Dim MilestoneTable As RenaissanceDataClass.DSFundMilestones.tblFundMilestonesDataTable
		Dim SelectedMilestones() As RenaissanceDataClass.DSFundMilestones.tblFundMilestonesRow

		If InPaint And (Not pIgnoreInPaint) Then
			Exit Sub
		End If

		Try

			If (Me.Combo_Fund.SelectedIndex > 0) Then
				FundID = Me.Combo_Fund.SelectedValue
			Else
				If Combo_MilestoneDate.SelectedIndex > 0 Then
					Me.Combo_MilestoneDate.SelectedIndex = 0
				End If
				' Me.Date_KnowledgeDate.Value = Now()
				Me.Date_KnowledgeDate.Value = CDate(Now.ToString(DISPLAYMEMBER_LONGDATEFORMAT))
				Me.Radio_Live.Checked = True
				Exit Sub
			End If

			If (Me.Combo_MilestoneDate.SelectedIndex >= 0) AndAlso (IsDate(Me.Combo_MilestoneDate.SelectedValue)) Then
				Me.Date_KnowledgeDate.Value = CDate(Me.Combo_MilestoneDate.SelectedValue)
				Me.Radio_PreciseTime.Checked = True
				Exit Sub
			ElseIf IsDate(Me.Combo_MilestoneDate.Text) Then
				MilestoneDate = CDate(Me.Combo_MilestoneDate.Text)
			Else
				' Me.Date_KnowledgeDate.Value = Now()
				Me.Date_KnowledgeDate.Value = CDate(Now.ToString(DISPLAYMEMBER_LONGDATEFORMAT))
				Me.Radio_Live.Checked = True
				Exit Sub
			End If

			MilestoneDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblFundMilestones, False)
			If MilestoneDS Is Nothing Then
				MainForm.LogError(Me.Name & ", GetFormData()", LOG_LEVELS.Warning, "", "Failed to load Fund Milestone Table", "", True)
				' Me.Date_KnowledgeDate.Value = Now()
				Me.Date_KnowledgeDate.Value = CDate(Now.ToString(DISPLAYMEMBER_LONGDATEFORMAT))
				Me.Radio_Live.Checked = True
				Exit Sub
			End If

			MilestoneTable = MilestoneDS.tblFundMilestones
			SelectedMilestones = MilestoneTable.Select("(MilestoneFundID=" & FundID.ToString & ") AND (MilestoneDate='" & MilestoneDate.ToString(QUERY_SHORTDATEFORMAT) & "')")

			If (SelectedMilestones Is Nothing) OrElse (SelectedMilestones.Length <= 0) Then
				' Me.Date_KnowledgeDate.Value = Now()
				Me.Date_KnowledgeDate.Value = CDate(Now.ToString(DISPLAYMEMBER_LONGDATEFORMAT))
				Me.Radio_Live.Checked = True
				Exit Sub
			End If

			Me.Date_KnowledgeDate.Value = SelectedMilestones(0).MilestoneKnowledgeDate
			Me.Radio_PreciseTime.Checked = True

		Catch ex As Exception
			MainForm.LogError(Me.Name & ", GetFormData()", LOG_LEVELS.Error, ex.Message, "Error Displayimg Milestone details.", ex.StackTrace, True)
			Exit Sub
		End Try

	End Sub





#End Region

#Region " Control Events"

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_Fund control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_Fund_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_Fund.SelectedIndexChanged
		' *****************************************************************************
		' If a Fund is selected, Update the Milestone Date Combo.
		' *****************************************************************************

		Call SetMilestoneDateCombo()

	End Sub


    ''' <summary>
    ''' Handles the CheckedChanged event of the Radios control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Radios_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_Live.CheckedChanged, Radio_WholeDay.CheckedChanged, Radio_PreciseTime.CheckedChanged
		' *******************************************************************************
		'
		'
		' *******************************************************************************

		If Me.Radio_Live.Checked = True Then
			' Me.Date_KnowledgeDate.Value = Now
			Me.Date_KnowledgeDate.Value = CDate(Now.ToString(DISPLAYMEMBER_LONGDATEFORMAT))
			Me.Radio_PreciseTime.Checked = True
		Else
			Me.Date_KnowledgeDate.Enabled = True

			If Me.Radio_WholeDay.Checked = True Then
				Me.Date_KnowledgeDate.CustomFormat = DISPLAYMEMBER_DATEFORMAT
			Else
				Me.Date_KnowledgeDate.CustomFormat = DISPLAYMEMBER_LONGDATEFORMAT
			End If
		End If

	End Sub


    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_MilestoneDate control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_MilestoneDate_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_MilestoneDate.SelectedIndexChanged
		' *****************************************************************************
		'
		'
		' *****************************************************************************

		If Combo_MilestoneDate.SelectedIndex >= 0 Then
			Call GetFormData()
		End If

		If Me.InPaint = False Then
			Me.edit_UnitPrice.Value = 0
		End If
	End Sub


    ''' <summary>
    ''' Handles the LostFocus event of the Combo_MilestoneDate control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_MilestoneDate_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Combo_MilestoneDate.LostFocus
		' *****************************************************************************
		'
		'
		' *****************************************************************************

		Dim MilestoneDate As Date

		Try

			If Me.Combo_MilestoneDate.SelectedIndex < 0 Then
				Dim FundRow As DSFund.tblFundRow
				Dim FundID As Integer = 0
				Dim FundPricingPeriod As RenaissanceGlobals.DealingPeriod

				If IsNumeric(Me.Combo_Fund.SelectedValue) Then
					FundID = Me.Combo_Fund.SelectedValue
				End If

				FundRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, FundID), DSFund.tblFundRow)

				If IsDate(Me.Combo_MilestoneDate.Text) Then
					MilestoneDate = CDate(Me.Combo_MilestoneDate.Text)

					If (FundID > 0) And (FundRow IsNot Nothing) Then
						FundPricingPeriod = CType(FundRow.FundPricingPeriod, RenaissanceGlobals.DealingPeriod)

						MilestoneDate = FitDateToPeriod(FundPricingPeriod, MilestoneDate, True)

						'MilestoneDate = MilestoneDate.AddMonths(1)
						'MilestoneDate = MilestoneDate.AddDays(0 - MilestoneDate.Day)

					End If

					If Not MilestoneDate.ToString(DISPLAYMEMBER_DATEFORMAT).Equals(Me.Combo_MilestoneDate.Text) Then
						Me.Combo_MilestoneDate.Text = MilestoneDate.ToString(DISPLAYMEMBER_DATEFORMAT)
					End If

					Call GetFormData()

				Else
					' Not a Date
					If (Combo_MilestoneDate.Text.Length > 0) Then
						MainForm.LogError(Me.Name & ", Combo_MilestoneDate_LostFocus()", LOG_LEVELS.Warning, "", "Milestone Date is not recognised as a date.", "", True)
					End If
					Exit Sub
				End If

				Call Button_GetPrice_Click(Me, New EventArgs)
			End If

		Catch ex As Exception
			MainForm.LogError(Me.Name & ", Combo_MilestoneDate_LostFocus()", LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
		End Try
	End Sub


    ''' <summary>
    ''' Handles the KeyPress event of the Combo_MilestoneDate control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.KeyPressEventArgs"/> instance containing the event data.</param>
	Private Sub Combo_MilestoneDate_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Combo_MilestoneDate.KeyPress
		' *****************************************************************************
		'
		'
		' *****************************************************************************

		If e.KeyChar = Chr(13) Then
			If Me.Combo_MilestoneDate.SelectedIndex < 0 Then
				Call GetFormData()
			End If
		End If
	End Sub


    ''' <summary>
    ''' Handles the ValueChanged event of the Date_MSDatePicker control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Date_MSDatePicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Date_MSDatePicker.ValueChanged
		' ****************************************************************
		' Process a date selected by the Date-Picker.
		'
		' ****************************************************************

		Dim thisDate As Date
		Dim IndexCounter As Integer
		Dim selectRow As DataRow
		Dim selectRowView As DataRowView
		Dim IndexDate As Date

		Try

			' Get period for this Fund

			Dim FundID As Integer = 0
			Dim FundRow As DSFund.tblFundRow
			Dim FundPricingPeriod As Integer = RenaissanceGlobals.DealingPeriod.Daily
			Dim FundUsesEqualisation As Integer = 0

			If IsNumeric(Me.Combo_Fund.SelectedValue) Then
				FundID = Me.Combo_Fund.SelectedValue
			End If

			FundRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, FundID), DSFund.tblFundRow)

			If (FundRow IsNot Nothing) Then
				FundPricingPeriod = CType(FundRow.FundPricingPeriod, RenaissanceGlobals.DealingPeriod)
				FundUsesEqualisation = CInt(FundRow.FundUsesEqualisation)
			Else
				Exit Sub
			End If

			' Resolve chosen date to End-Of-period

			thisDate = Date_MSDatePicker.Value.Date
			thisDate = FitDateToPeriod(FundPricingPeriod, thisDate, True)
			'thisDate = thisDate.AddMonths(1)
			'thisDate = thisDate.AddDays(0 - thisDate.Day)

			' Search for this date on the Combo, Select Item if found.

			If Combo_MilestoneDate.Items.Count <= 0 Then
				Combo_MilestoneDate.Text = Date_MSDatePicker.Value.Date.ToString(DISPLAYMEMBER_DATEFORMAT)
				Exit Sub
			End If

			For IndexCounter = 0 To (Combo_MilestoneDate.Items.Count - 1)
				Try
					IndexDate = Renaissance_BaseDate

					If GetType(DataRowView).IsInstanceOfType(Combo_MilestoneDate.Items(IndexCounter)) Then
						selectRowView = Combo_MilestoneDate.Items(IndexCounter)
						selectRow = selectRowView.Row
						If (IsDate(selectRow(Combo_MilestoneDate.DisplayMember))) Then
							IndexDate = CDate(selectRow(Combo_MilestoneDate.DisplayMember))
						End If
					ElseIf GetType(DataRow).IsInstanceOfType(Combo_MilestoneDate.Items(IndexCounter)) Then
						selectRow = Combo_MilestoneDate.Items(IndexCounter)
						If (IsDate(selectRow(Combo_MilestoneDate.DisplayMember))) Then
							IndexDate = CDate(selectRow(Combo_MilestoneDate.DisplayMember))
						End If
					Else
						Try
							If (IsDate(Combo_MilestoneDate.Items(IndexCounter))) Then
								IndexDate = CDate(Combo_MilestoneDate.Items(IndexCounter))
							End If
						Catch ex As Exception
						End Try
					End If
				Catch ex As Exception
				End Try

				If thisDate.Equals(IndexDate) Then
					Combo_MilestoneDate.SelectedIndex = IndexCounter
					Exit Sub
				End If
			Next
		Catch ex As Exception
		End Try

		' This date does not exist as an item, thus set the Combo text.

		If (Combo_MilestoneDate.SelectedIndex > 0) Then
			Combo_MilestoneDate.SelectedIndex = 0
		End If
		Combo_MilestoneDate.SelectedIndex = -1

		Combo_MilestoneDate.Text = thisDate.ToString(DISPLAYMEMBER_DATEFORMAT)
		Call Button_GetPrice_Click(Me, New EventArgs)
	End Sub

#End Region

#Region " Set Form Combos (Form Specific Code) "

    ''' <summary>
    ''' Sets the fund combo.
    ''' </summary>
	Private Sub SetFundCombo()
		Dim OrgInpaint As Boolean
		OrgInpaint = InPaint
		InPaint = True

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_Fund, _
    RenaissanceStandardDatasets.tblFund, _
    "FundName", _
    "FundID", _
    "FundClosed=0", False, True, True)   ' 

		Call SetMilestoneDateCombo(True)

		InPaint = OrgInpaint

		GetFormData()

	End Sub

    ''' <summary>
    ''' Sets the transaction group combo.
    ''' </summary>
	Private Sub SetTransactionGroupCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_TransactionGroup, _
		RenaissanceStandardDatasets.tblSophisStatusGroups, _
		"GROUP_NAME", _
		"GROUP_NAME", _
		"", True, True, True)		' 

	End Sub


    ''' <summary>
    ''' Sets the milestone date combo.
    ''' </summary>
    ''' <param name="pIgnoreInPaint">if set to <c>true</c> [p ignore in paint].</param>
	Private Sub SetMilestoneDateCombo(Optional ByVal pIgnoreInPaint As Boolean = False)
		Dim SelectString As String
		Dim OrgInpaint As Boolean

		If InPaint And (Not pIgnoreInPaint) Then
			Exit Sub
		End If

		OrgInpaint = InPaint
		InPaint = True


		SelectString = "False"
		If (Combo_Fund.SelectedIndex >= 0) AndAlso (IsNumeric(Combo_Fund.SelectedValue)) Then
			SelectString = "MilestoneFundID=" & CInt(Combo_Fund.SelectedValue).ToString
		End If

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_MilestoneDate, _
		RenaissanceStandardDatasets.tblFundMilestones, _
		"MilestoneDate", _
		"MilestoneKnowledgeDate", _
		SelectString, False, False, True)		 ' 

		InPaint = OrgInpaint
		GetFormData()

	End Sub



#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close "

    ''' <summary>
    ''' Handles the Click event of the btnSetMilestone control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnSetMilestone_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSetMilestone.Click
		' *****************************************************************************
		' Set Milestone
		' *****************************************************************************

		Try
			If (RenaissanceStandardDatasets.tblFundMilestones.ActivityCount > 0) Then
				MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "There appears to be a Milestone process already running, Attribution cancelled.", "", True)
				Exit Sub
			End If
			If (RenaissanceStandardDatasets.tblAttribution.ActivityCount > 0) Then
				MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "There appears to be an Attribution process already running, Attribution cancelled.", "", True)
				Exit Sub
			End If
		Catch ex As Exception
			Exit Sub
		End Try

		Try
			FormControls = MainForm.MainReportHandler.DisableFormControls(Me)
			Form_ProgressBar.Value = Form_ProgressBar.Minimum
			MainForm.SetToolStripText(Label_Status, "Saving Milestone")

			ReportTimer.Interval = 25
			ReportTimer.Tag = Me.Form_ProgressBar
			ReportTimer.Start()

			Try
				RenaissanceStandardDatasets.tblFundMilestones.ActivityCount_Increment()

				Call SetMilestone()

			Catch ex As Exception
			Finally
				RenaissanceStandardDatasets.tblFundMilestones.ActivityCount_Decrement()
			End Try

		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
		End Try

		ReportTimer.Stop()
		MainForm.MainReportHandler.EnableFormControls(FormControls)
		MainForm.SetToolStripText(Label_Status, "")

		'Form_ProgressBar = MainForm.ResetProgressBar(Form_StatusStrip, Form_ProgressBar)
		Form_ProgressBar.Value = Form_ProgressBar.Minimum
		Form_ProgressBar.Visible = False

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btn_SaveAllFundFees control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btn_SaveAllFundFees_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_SaveAllFundFees.Click
		' *****************************************************************************
		' 
		' *****************************************************************************
		Dim FundsDS As RenaissanceDataClass.DSFund
		Dim FundsTable As RenaissanceDataClass.DSFund.tblFundDataTable
		Dim FundCounter As Integer

		Dim MilestonesDS As DSFundMilestones
		Dim SelectedMilestones() As DSFundMilestones.tblFundMilestonesRow
		Dim PreviousMilestones() As DSFundMilestones.tblFundMilestonesRow
		Dim thisMilestones As DSFundMilestones.tblFundMilestonesRow
		Dim Counter As Integer
		Dim FundID As Integer
		Dim FundRow As DSFund.tblFundRow = Nothing
		Dim FundPricingPeriod As RenaissanceGlobals.DealingPeriod = RenaissanceGlobals.DealingPeriod.Daily = DealingPeriod.Daily

		Dim MilestoneDate As Date
		Dim MilestoneStartDate As Date

		Try
			' Get pricing period for Milestone Fund
			If IsNumeric(Me.Combo_Fund.SelectedValue) Then
				FundID = Me.Combo_Fund.SelectedValue
				FundRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, FundID), DSFund.tblFundRow)
				FundPricingPeriod = CType(FundRow.FundPricingPeriod, RenaissanceGlobals.DealingPeriod)
			End If

			FundsDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblFund, False)
			FundsTable = FundsDS.tblFund

			MilestonesDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblFundMilestones, False)

			MilestoneDate = CDate(MainForm.GetControlText(Combo_MilestoneDate))
			MilestoneDate = FitDateToPeriod(FundPricingPeriod, MilestoneDate, True)

			Dim UnitHolderFeeCalcObject As New UnitHolderFeeClass(MainForm, DEFAULT_STATUSGROUPFILTER, DEFAULT_ADMINISTRATORDATESFILTER)

			If (FundsTable.Rows.Count > 0) Then
				For FundCounter = 0 To (FundsTable.Rows.Count - 1)
					FundID = CInt(FundsTable.Rows(FundCounter)("FundID"))

					SelectedMilestones = MilestonesDS.tblFundMilestones.Select("MilestoneDate='" & MilestoneDate.ToString(QUERY_SHORTDATEFORMAT) & "'", "MilestoneFundID")

					For Counter = 0 To (SelectedMilestones.Length - 1)
						thisMilestones = SelectedMilestones(Counter)

						' Get previous Milestone Date
						PreviousMilestones = MilestonesDS.tblFundMilestones.Select("(MilestoneFundID=" & thisMilestones.MilestoneFundID.ToString & ") AND (MilestoneDate<'" & MilestoneDate.ToString(QUERY_SHORTDATEFORMAT) & "')", "MilestoneDate Desc")

						If (PreviousMilestones.Length > 0) Then
							MilestoneStartDate = PreviousMilestones(0).MilestoneDate
						Else
							MilestoneStartDate = FitDateToPeriod(FundPricingPeriod, AddPeriodToDate(FundPricingPeriod, thisMilestones.MilestoneDate, -1), True)
						End If

						Call UnitHolderFeeCalcObject.SaveSinglePeriodFundFees(thisMilestones.MilestoneFundID, MilestoneStartDate, thisMilestones.MilestoneDate, True)

					Next

				Next
			End If

		Catch ex As Exception
		End Try

	End Sub

    ''' <summary>
    ''' Sets the milestone.
    ''' </summary>
	Private Sub SetMilestone()
		' *****************************************************************************
		' 
		' *****************************************************************************

		Dim FundID As Integer
		Dim FundRow As DSFund.tblFundRow
		Dim FundPricingPeriod As DealingPeriod = RenaissanceGlobals.DealingPeriod.Daily
		Dim FundUsesEqualisation As Integer = 0
		Dim MilestoneDate As Date
		Dim KnowledgeDate As Date
		Dim FundPrice As Double
		Dim StatusGroupFilter As String
		Dim AdministratorDatesFilter As RenaissanceGlobals.AdministratorDatesFilter = RenaissanceGlobals.AdministratorDatesFilter.None

		Dim AttribPeriod As RenaissanceGlobals.DealingPeriod = DealingPeriod.Monthly

		Try

			StatusGroupFilter = Nz(MainForm.GetComboSelectedValue(Combo_TransactionGroup), "")

			' AdministratorDatesFilter (bitmap)
			'
			'--	1		- Ignore the transaction status for Subscriptions and Redemptions
			'--	2		- Ignore the Subscriptions and Redemptions on (or after of course) the given Value Date

			If (MainForm.GetCheckBoxChecked(Me.Check_SR_IgnoreStatus)) Then
        AdministratorDatesFilter += RenaissanceGlobals.AdministratorDatesFilter.SubscriptionRedemption_IgnoreTransactionStatus
			End If
			If (MainForm.GetCheckBoxChecked(Me.Check_SR_IgnoreTodaysTrades)) Then
				AdministratorDatesFilter += RenaissanceGlobals.AdministratorDatesFilter.SubscriptionRedemption_IgnoreTodaysTrades
			End If



			FundPrice = MainForm.GetNumericTextBoxValue(Me.edit_UnitPrice)

			' Validate Form Inputs

			If (MainForm.GetComboSelectedValue(Combo_Fund) Is Nothing) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Fund must be selected.", "", True)
				Exit Sub
			End If

			If (CInt(MainForm.GetComboSelectedIndex(Combo_Fund)) <= 0) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Fund must be selected.", "", True)
				Exit Sub
			End If
			FundID = CInt(MainForm.GetComboSelectedValue(Combo_Fund))

			If IsDate(MainForm.GetControlText(Combo_MilestoneDate)) = False Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Milestone Date field does not contain a valid Date.", "", True)
				Exit Sub
			End If

			FundRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, FundID), DSFund.tblFundRow)

			If (FundRow IsNot Nothing) Then
				FundPricingPeriod = CType(FundRow.FundPricingPeriod, RenaissanceGlobals.DealingPeriod)
				FundUsesEqualisation = CInt(FundRow.FundUsesEqualisation)
			Else
				Exit Sub
			End If

			' MilestoneDate, Ensure that it is the last day of the month

			MilestoneDate = CDate(MainForm.GetControlText(Combo_MilestoneDate))
			MilestoneDate = FitDateToPeriod(FundPricingPeriod, MilestoneDate, True)	'  MilestoneDate.AddMonths(1)
			'MilestoneDate = MilestoneDate.AddDays(0 - MilestoneDate.Day)

			' KDs

			KnowledgeDate = Now()

			If MainForm.GetRadioChecked(Radio_WholeDay) = True Then
				KnowledgeDate = CDate(CDate(MainForm.GetDatetimeValue(Date_KnowledgeDate).Date).ToString(DISPLAYMEMBER_DATEFORMAT) & " 23:59:59")
			ElseIf MainForm.GetRadioChecked(Radio_PreciseTime) = True Then
				KnowledgeDate = MainForm.GetDatetimeValue(Date_KnowledgeDate)
			End If
			If (KnowledgeDate <= KNOWLEDGEDATE_NOW) Then
				KnowledgeDate = KNOWLEDGEDATE_NOW
			End If

		Catch ex As Exception
			MainForm.LogError(Me.Name & ", btnSetMilestone_Click()", LOG_LEVELS.Error, ex.Message, "Error Validating Set Milestone Form inputs.", ex.StackTrace, True)
			Exit Sub
		End Try

		' This has the effect of removing milliseconds from the KD (and rounds?) to
		' the nearest second.

		KnowledgeDate = CDate(KnowledgeDate.ToString(DISPLAYMEMBER_LONGDATEFORMAT))

		' Check Instrument Prices are present

		Try
			Dim MissingInstrumentList As ArrayList

			MainForm.SetToolStripText(Label_Status, "Checking Prices are present.")

			MissingInstrumentList = MissingInstruments(MainForm, Me.Name, FundID, "", MilestoneDate)
			If MissingInstrumentList.Count > 0 Then
				Dim MessageString As String
				Dim thisCount As Integer

				MessageString = "Prices are missing for the following Instruments :" & vbCrLf

				For thisCount = 0 To (MissingInstrumentList.Count - 1)
					MessageString &= "  " & MissingInstrumentList(thisCount).ToString & vbCrLf
				Next
				MessageString &= vbCrLf & "Do you wish to continue running the Milestone ?"

				If MessageBox.Show(MessageString, "Abort Milestone ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then

					Me.MainForm.SetToolStripText(Label_Status, "")
					Exit Sub
				End If
			End If

		Catch ex As Exception
			MainForm.LogError(Me.Name & ", btnSetMilestone_Click()", LOG_LEVELS.Error, ex.Message, "Error checking Instrument Prices.", ex.StackTrace, True)
			Exit Sub

		End Try

		' Save Milestone

		Dim MilestoneEndDate As Date
		Dim MilestoneStartDate As Date
		Dim MilestoneEndKnowledgeDate As Date
		Dim MilestoneStartKnowledgeDate As Date

		Dim MilestoneDS As RenaissanceDataClass.DSFundMilestones
		Dim MilestoneTable As RenaissanceDataClass.DSFundMilestones.tblFundMilestonesDataTable
		Dim SelectedMilestones() As RenaissanceDataClass.DSFundMilestones.tblFundMilestonesRow

		Try

			' (Re)Load the Milestone table.

			MilestoneDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblFundMilestones, True)
			If MilestoneDS Is Nothing Then
				MainForm.LogError(Me.Name & ", SetMilestone()", LOG_LEVELS.Warning, "", "Failed to load Fund Milestone Table", "", True)
				Exit Sub
			End If
			MilestoneTable = MilestoneDS.tblFundMilestones

			' ***********************************************************************
			' Set MS Start & End Dates
			' ***********************************************************************

			MilestoneEndDate = MilestoneDate
			MilestoneStartDate = FitDateToPeriod(FundPricingPeriod, AddPeriodToDate(FundPricingPeriod, MilestoneDate, -1), True)
			' MilestoneDate.AddDays(0 - MilestoneDate.Day)

			MilestoneEndKnowledgeDate = KnowledgeDate

			' ***********************************************************************
			' Are there any Milestones subsequent to this date ?
			' ***********************************************************************

			SelectedMilestones = MilestoneTable.Select("(MilestoneFundID=" & FundID.ToString & ") AND (MilestoneDate>'" & MilestoneEndDate.ToString(QUERY_SHORTDATEFORMAT) & "')", "MilestoneDate")

			If (Not (SelectedMilestones Is Nothing)) AndAlso (SelectedMilestones.Length > 0) Then
				' Subsequent Milestones Exist

				MainForm.LogError(Me.Name & ", SetMilestone", LOG_LEVELS.Warning, "", "Milestones already exist after this date. Add Milestone Aborted.", "", True)
				Exit Sub
			End If

			' ***********************************************************************
			' Are there any Milestones ON this date ?
			' ***********************************************************************

			SelectedMilestones = MilestoneTable.Select("(MilestoneFundID=" & FundID.ToString & ") AND (MilestoneDate='" & MilestoneEndDate.ToString(QUERY_SHORTDATEFORMAT) & "')", "MilestoneDate")

			If (Not (SelectedMilestones Is Nothing)) AndAlso (SelectedMilestones.Length > 0) Then
				' ***********************************************************************
				' This Milestone already Exists
				' Confirm the desire to replace
				' Don't allow the decision to be taken too quickly.
				' ***********************************************************************

				Dim StartTime As Date

				StartTime = Now

				If MessageBox.Show("A Milestone already exists on this date. Are you SURE you want to replace it with a new one ?", "Replace milestone", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) <> Windows.Forms.DialogResult.Yes Then
					Exit Sub
				End If

				While Now.Subtract(StartTime).TotalSeconds < 10
					If MessageBox.Show(CInt(Now.Subtract(StartTime).TotalSeconds).ToString & ") A Milestone already exists on this date. " & vbCrLf & "Are you SURE you want to replace it with a new one ?" & vbCrLf & "Don't rush your decision, Take your time !", "Replace milestone", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) <> Windows.Forms.DialogResult.Yes Then
						Exit Sub
					End If
				End While
			End If

			' ***********************************************************************
			' Resolve Start Knowledgedate
			' ***********************************************************************

			SelectedMilestones = MilestoneTable.Select("(MilestoneFundID=" & FundID.ToString & ") AND (MilestoneDate<'" & MilestoneEndDate.ToString(QUERY_SHORTDATEFORMAT) & "')", "MilestoneDate Desc")

			If (SelectedMilestones Is Nothing) OrElse (SelectedMilestones.Length <= 0) Then

				' No Milestone Exists on or before the Milestone Start Date
				' ***********************************************************************

				If MessageBox.Show("This appears to be the first Milestone for this Fund" & vbCrLf & "Do you wish to continue ?", "First Milestone", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) <> Windows.Forms.DialogResult.Yes Then
					Exit Sub
				End If

				MilestoneStartKnowledgeDate = MilestoneEndKnowledgeDate

			ElseIf SelectedMilestones(0).MilestoneDate.Equals(MilestoneStartDate) Then
				MilestoneStartKnowledgeDate = SelectedMilestones(0).MilestoneKnowledgeDate
			Else

				' Earlier Milestone Exists, but not on the calculated start date.
				' ***********************************************************************

				If MessageBox.Show("The previous Milestone (" & SelectedMilestones(0).MilestoneDate.ToString(DISPLAYMEMBER_DATEFORMAT) & ") does not appear to be" & vbCrLf & "at the end of the previous period (" & MilestoneStartDate.ToString(DISPLAYMEMBER_DATEFORMAT) & ")." & vbCrLf & "Do you wish to continue, using the found Milestone as the Period Start Date ?", "Alter Milestone Date?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) <> Windows.Forms.DialogResult.Yes Then
					Exit Sub
				End If

				MilestoneStartDate = SelectedMilestones(0).MilestoneDate
				MilestoneStartKnowledgeDate = SelectedMilestones(0).MilestoneKnowledgeDate
			End If

		Catch ex As Exception
			MainForm.LogError(Me.Name & ", btnSetMilestone_Click()", LOG_LEVELS.Error, ex.Message, "Error Validating Milestone Dates and KnowledgeDates.", ex.StackTrace, True)
			Exit Sub
		End Try


		' ***********************************************************************
		'
		' OK, Milestone Dates and KnowledgeDates should be OK at this point.
		' Any issues of Previous / Existing Milestones have been resolved
		'
		' ***********************************************************************

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "Set Fund Milestone. FundID=" & FundID.ToString & ", MSDate=" & MilestoneEndDate.ToString(DISPLAYMEMBER_DATEFORMAT), "", "", False)

		Try

			' Calculate Attributions 

			MainForm.SetToolStripText(Label_Status, "Calculating Attributions")

			Call Attribution_SaveAtributions_Asynch(MainForm, _
			FundID, _
			MilestoneEndDate, _
			AttribPeriod, _
			True, _
			True, _
			True, _
			False, _
			MilestoneStartKnowledgeDate, MilestoneEndKnowledgeDate, _
			MainForm.Main_Knowledgedate, Nothing)

			' Propogate changes

			'MainForm.ReloadTable(RenaissanceChangeID.tblAttribution, False)
			'Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblAttribution))
			MainForm.ReloadTable_Background(RenaissanceChangeID.tblAttribution, "", True)
		Catch ex As Exception
			MainForm.LogError(Me.Name & ", btnSetMilestone_Click()", LOG_LEVELS.Error, ex.Message, "Error calculating Attributions.", ex.StackTrace, True)
			Me.MainForm.SetToolStripText(Label_Status, "")
			Exit Sub
		End Try


		Try
			' Save Fund Price

			Dim FundDS As RenaissanceDataClass.DSFund
			Dim FundTable As RenaissanceDataClass.DSFund.tblFundDataTable
			Dim SelectedFunds() As RenaissanceDataClass.DSFund.tblFundRow

			Dim FundUnitInstrument As Integer

			MainForm.SetToolStripText(Label_Status, "Save Fund Unit Price")

			FundDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblFund, True)
			FundTable = FundDS.tblFund
			SelectedFunds = FundTable.Select("FundID=" & FundID.ToString)

			If (Not (SelectedFunds Is Nothing)) AndAlso (SelectedFunds.Length > 0) Then
				FundUnitInstrument = SelectedFunds(0).FundUnit
			Else
				MainForm.LogError(Me.Name & ", btnSetMilestone_Click()", LOG_LEVELS.Warning, "", "Save Fund Unit Price. Failed to get Fund Unit Instrument.", "", True)
				Me.MainForm.SetToolStripText(Label_Status, "")
				Exit Sub
			End If

			' Save Price

			Dim thisCommand As SqlCommand = Nothing
			Try
				thisCommand = New SqlCommand()
				thisCommand.CommandType = CommandType.StoredProcedure
				thisCommand.CommandText = "spu_SavePrice"
				thisCommand.Parameters.Add("@Instrument_ID", SqlDbType.Int).Value = FundUnitInstrument
				thisCommand.Parameters.Add("@PriceDate", SqlDbType.DateTime).Value = MilestoneEndDate
				thisCommand.Parameters.Add("@PriceLevel", SqlDbType.Float).Value = FundPrice
				thisCommand.Parameters.Add("@PriceBasicNAV", SqlDbType.Float).Value = MainForm.GetNumericTextBoxValue(Me.edit_BNAV)
				thisCommand.Parameters.Add("@PriceGNAV", SqlDbType.Float).Value = MainForm.GetNumericTextBoxValue(Me.edit_GNAV)
				thisCommand.Parameters.Add("@PriceGAV", SqlDbType.Float).Value = MainForm.GetNumericTextBoxValue(Me.edit_GAV)
				thisCommand.Parameters.Add("@PriceIsPercent", SqlDbType.Bit).Value = 0
				thisCommand.Parameters.Add("@PricePercent", SqlDbType.Float).Value = 0
				thisCommand.Parameters.Add("@PriceBaseDate", SqlDbType.DateTime).Value = Renaissance_BaseDate
				thisCommand.Parameters.Add("@PriceIsFinal", SqlDbType.Int).Value = 1
				thisCommand.Parameters.Add("@PriceIsAdministrator", SqlDbType.Bit).Value = 0
				thisCommand.Parameters.Add("@PriceIsMilestone", SqlDbType.Bit).Value = 1
        thisCommand.Parameters.Add("@DateEntered", SqlDbType.DateTime).Value = MilestoneEndKnowledgeDate
				thisCommand.Parameters.Add("@PriceComment", SqlDbType.VarChar, 100).Value = "Milestone Price " & MilestoneEndDate.ToString(DISPLAYMEMBER_DATEFORMAT)
				thisCommand.Parameters.Add("@DontReplaceExistingPrices", SqlDbType.Int).Value = 0
				thisCommand.Parameters.Add("@DontReplaceExistingFinalPrices", SqlDbType.Int).Value = 0
				thisCommand.Parameters.Add("@DontReplaceExistingMilestonePrices", SqlDbType.Int).Value = 0
				thisCommand.Parameters.Add("@DontUpdateDependentPrices", SqlDbType.Int).Value = 0
				thisCommand.Parameters.Add("@DontPostPricesAutoUpdate", SqlDbType.Int).Value = 1
				thisCommand.Parameters.Add(New SqlParameter("@rvStatusString", SqlDbType.VarChar, 200, ParameterDirection.Output, False, CType(0, Byte), CType(0, Byte), "", DataRowVersion.Current, Nothing))

				thisCommand.Connection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)

				SyncLock thisCommand.Connection
					thisCommand.ExecuteNonQuery()
				End SyncLock

			Catch ex As Exception
			Finally
				If (thisCommand IsNot Nothing) Then
					thisCommand.Connection = Nothing
					thisCommand.Parameters.Clear()
					thisCommand = Nothing
				End If
			End Try

			' Always Re-Load the Prices table since a price cascade may have altered other prices.
      Me.MainForm.ReloadTable(RenaissanceStandardDatasets.tblPrice.ChangeID, "", True)
			' Propagate changes
			' Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblPrice))

			' Update Transactions

			Update_Dependent_Transactions_Milestone(MainForm, "SetMilestone()", FundUnitInstrument, MilestoneEndDate)


		Catch ex As Exception
			MainForm.LogError(Me.Name & ", btnSetMilestone_Click()", LOG_LEVELS.Error, ex.Message, "Error Saving Fund Unit Price.", ex.StackTrace, True)
			Me.MainForm.SetToolStripText(Label_Status, "")
			Exit Sub
		End Try

		Try
			' Save Milestone 

			MainForm.SetToolStripText(Label_Status, "Saving Milestone")

			MilestoneTable = MilestoneDS.tblFundMilestones

			Dim MilestoneAdaptor As SqlDataAdapter
			Dim NewMilestoneRow As RenaissanceDataClass.DSFundMilestones.tblFundMilestonesRow

			MilestoneAdaptor = MainForm.MainDataHandler.Get_Adaptor(RenaissanceStandardDatasets.tblFundMilestones.Adaptorname, VENICE_CONNECTION, RenaissanceStandardDatasets.tblFundMilestones.TableName)

			SyncLock MilestoneDS

				SelectedMilestones = MilestoneTable.Select("(MilestoneFundID=" & FundID.ToString & ") AND (MilestoneDate='" & MilestoneEndDate.ToString(QUERY_SHORTDATEFORMAT) & "')", "MilestoneDate")

				If (Not (SelectedMilestones Is Nothing)) AndAlso (SelectedMilestones.Length > 0) Then
					' Edit Existing Record

					SelectedMilestones(0).MilestoneKnowledgeDate = MilestoneEndKnowledgeDate
				Else
					NewMilestoneRow = MilestoneTable.NewtblFundMilestonesRow

					NewMilestoneRow.MilestoneFundID = FundID
					NewMilestoneRow.MilestoneDate = MilestoneEndDate
					NewMilestoneRow.MilestoneDateString = MilestoneEndDate.ToString(DISPLAYMEMBER_DATEFORMAT)
					NewMilestoneRow.MilestoneKnowledgeDate = MilestoneEndKnowledgeDate

					MilestoneTable.Rows.Add(NewMilestoneRow)
				End If

				MilestoneAdaptor.InsertCommand.Parameters("@KnowledgeDate").Value = KNOWLEDGEDATE_NOW
				MilestoneAdaptor.UpdateCommand.Parameters("@KnowledgeDate").Value = KNOWLEDGEDATE_NOW

				MainForm.AdaptorUpdate(Me.Name, MilestoneAdaptor, MilestoneTable)

			End SyncLock

			Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblFundMilestones))

		Catch ex As Exception
			MainForm.LogError(Me.Name & ", btnSetMilestone_Click()", LOG_LEVELS.Error, ex.Message, "Error Saving Milestone entry.", ex.StackTrace, True)
			Me.MainForm.SetToolStripText(Label_Status, "")
			Exit Sub
		End Try

		' Save fund Fees, MUST be done after the Milestone is saved.

		If Me.Check_SaveFundFees.Checked Then
			Dim UnitHolderFeeCalcObject As New UnitHolderFeeClass(MainForm, StatusGroupFilter, AdministratorDatesFilter)
			Try
				Call UnitHolderFeeCalcObject.SaveSinglePeriodFundFees(FundID, MilestoneStartDate, MilestoneEndDate, True)
			Catch ex As Exception
				MainForm.LogError(Me.Name & ", btnSetMilestone_Click()", LOG_LEVELS.Error, ex.Message, "Error Saving Fund Fees.", ex.StackTrace, True)
			Finally
				UnitHolderFeeCalcObject.Dispose()
				UnitHolderFeeCalcObject = Nothing
			End Try
		End If

    ' Save Historic Valuation data
    MainForm.LogError(Me.Name & ", SetMilestone()", LOG_LEVELS.Audit, "Milestone Form, Set Historic Valuations. " & FundID.ToString & ", " & MilestoneDate.ToString(RenaissanceGlobals.Globals.QUERY_SHORTDATEFORMAT), "Milestone Form, Set Historic Valuations. " & FundID.ToString & ", " & MilestoneDate.ToString(RenaissanceGlobals.Globals.QUERY_SHORTDATEFORMAT), "", False)

    Dim SaveAttributionCommand As New SqlCommand

    Try
      SaveAttributionCommand.Connection = MainForm.GetVeniceConnection
      SaveAttributionCommand.CommandType = CommandType.StoredProcedure
      SaveAttributionCommand.CommandText = "[adp_tblHistoricValuations_Populate]"
      SaveAttributionCommand.CommandTimeout = ATTRIBUTION_SQLCOMMAND_TIMEOUT

      SaveAttributionCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
      SaveAttributionCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@StartDate", System.Data.SqlDbType.Date)).Value = MilestoneDate
      SaveAttributionCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@EndDate", System.Data.SqlDbType.Date)).Value = MilestoneDate
      SaveAttributionCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FundID", System.Data.SqlDbType.Int)).Value = FundID
      SaveAttributionCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime)).Value = KNOWLEDGEDATE_NOW

      If (Not (SaveAttributionCommand.Connection Is Nothing)) Then
        SaveAttributionCommand.ExecuteNonQuery()
      End If

      Call AllocationSetReturns(MainForm, FundID, AddPeriodToDate(FundPricingPeriod, MilestoneDate, IIf(FundPricingPeriod = DealingPeriod.Daily, -5, -1)), MilestoneDate, 0, Nothing)

    Catch ex As Exception
      MainForm.LogError(Me.Name & ", button_SetMilestone_Click()", LOG_LEVELS.Error, ex.Message, "Error Saving Historic Valuation.", ex.StackTrace, True)
    Finally
      If (SaveAttributionCommand.Connection IsNot Nothing) Then
        SaveAttributionCommand.Connection.Close()
        SaveAttributionCommand.Connection = Nothing
      End If
    End Try

		' End

		Call MessageBox.Show("Milestone Saved", "Save Milestone", MessageBoxButtons.OK)

		Me.GetFormData()

		Me.MainForm.SetToolStripText(Label_Status, "")

	End Sub

    ''' <summary>
    ''' Handles the Click event of the Button_SetKnowledgeDate control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Button_SetKnowledgeDate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_SetKnowledgeDate.Click
		' *****************************************************************************
		'
		'
		' *****************************************************************************

		Dim SetKDForm As frmSetKnowledgedate
		Dim SetKDFormHandle As RenaissanceFormHandle

		SetKDFormHandle = MainForm.VeniceForms.OpenItem(VeniceFormID.frmSetKnowledgeDate)
		If (SetKDFormHandle Is Nothing) Then
			SetKDFormHandle = MainForm.New_VeniceForm(VeniceFormID.frmSetKnowledgeDate)
		End If
		SetKDForm = SetKDFormHandle.Form

		SetKDForm.Activate()

		If Me.Radio_Live.Checked = True Then
			SetKDForm.SetKnowledgeDate(KNOWLEDGEDATE_NOW)
		Else
			If IsDate(Date_KnowledgeDate.Text) Then
				If Me.Radio_WholeDay.Checked = True Then
					SetKDForm.SetKnowledgeDate(CDate(Date_KnowledgeDate.Text).Date.AddSeconds(LAST_SECOND))
				Else
					SetKDForm.SetKnowledgeDate(CDate(Date_KnowledgeDate.Text))
				End If
			End If
		End If

	End Sub


    ''' <summary>
    ''' Handles the Click event of the btnClose control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
		' *****************************************************************************
		' Close Form
		' *****************************************************************************

		Me.Close()

	End Sub

#End Region

    ''' <summary>
    ''' Handles the Click event of the Button_GetPrice control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Button_GetPrice_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_GetPrice.Click
		Dim FundID As Integer = 0
		Dim MilestoneDate As Date
		Dim MilestoneStartDate As Date
		Dim KnowledgeDate As Date
		Dim StatusGroupFilter As String
		Dim AdministratorDatesFilter As RenaissanceGlobals.AdministratorDatesFilter = RenaissanceGlobals.AdministratorDatesFilter.None
		Dim ThisNAV As Double
		Dim ThisGNAV As Double
    'Dim ThisBNAV As Double

		Dim FundRow As DSFund.tblFundRow
		Dim FundPricingPeriod As RenaissanceGlobals.DealingPeriod = RenaissanceGlobals.DealingPeriod.Daily
		Dim FundUsesEqualisation As Integer = 0
		Dim MilestoneDS As RenaissanceDataClass.DSFundMilestones
		Dim MilestoneTable As RenaissanceDataClass.DSFundMilestones.tblFundMilestonesDataTable

		Try


			If IsNumeric(Me.Combo_Fund.SelectedValue) Then
				FundID = Me.Combo_Fund.SelectedValue
			End If

			FundRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, FundID), DSFund.tblFundRow)

			If (FundRow IsNot Nothing) Then
				FundPricingPeriod = CType(FundRow.FundPricingPeriod, RenaissanceGlobals.DealingPeriod)
				FundUsesEqualisation = CInt(FundRow.FundUsesEqualisation)
			Else
				Exit Sub
			End If

			' Validate Form Inputs

			If (Nz(Me.Combo_Fund.SelectedIndex, 0) <= 0) Then
				Me.edit_UnitPrice.Value = 0
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Fund must be selected.", "", True)
				Exit Sub
			End If

			If IsDate(Me.Combo_MilestoneDate.Text) = False Then
				Me.edit_UnitPrice.Value = 0
				Exit Sub
			End If

			StatusGroupFilter = Nz(MainForm.GetComboSelectedValue(Combo_TransactionGroup), "")

			' AdministratorDatesFilter (bitmap)
			'
			'--	1		- Ignore the transaction status for Subscriptions and Redemptions
			'--	2		- Ignore the Subscriptions and Redemptions on (or after of course) the given Value Date

			If (MainForm.GetCheckBoxChecked(Me.Check_SR_IgnoreStatus)) Then
        AdministratorDatesFilter += RenaissanceGlobals.AdministratorDatesFilter.SubscriptionRedemption_IgnoreTransactionStatus
			End If
			If (MainForm.GetCheckBoxChecked(Me.Check_SR_IgnoreTodaysTrades)) Then
				AdministratorDatesFilter += RenaissanceGlobals.AdministratorDatesFilter.SubscriptionRedemption_IgnoreTodaysTrades
			End If

			' MilestoneDate, Ensure that it is the last day of the period

			MilestoneDate = CDate(Me.Combo_MilestoneDate.Text)
			MilestoneDate = FitDateToPeriod(FundPricingPeriod, MilestoneDate, True)

			' (Re)Load the Milestone table.

			MilestoneDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblFundMilestones, True)
			If MilestoneDS Is Nothing Then
				MainForm.LogError(Me.Name & ", SetMilestone()", LOG_LEVELS.Warning, "", "Failed to load Fund Milestone Table", "", True)
				Exit Sub
			End If
			MilestoneTable = MilestoneDS.tblFundMilestones

			' ***********************************************************************
			' Set MS Start & End Dates
			' ***********************************************************************
			' Set LastPeriod_ThisDate to last Milestone, if available.

			Dim tmpMilestoneRows As DSFundMilestones.tblFundMilestonesRow() = Nothing
			tmpMilestoneRows = CType(MilestoneTable.Select("(MilestoneFundID=" & FundID.ToString & ") AND (MilestoneDate<'" & MilestoneDate.ToString(QUERY_SHORTDATEFORMAT) & "')", "MilestoneDate Desc"), DSFundMilestones.tblFundMilestonesRow())

			If (tmpMilestoneRows.Length > 0) Then
				MilestoneStartDate = tmpMilestoneRows(0).MilestoneDate
				MilestoneStartDate = FitDateToPeriod(FundPricingPeriod, MilestoneStartDate, True)
			Else
				MilestoneStartDate = FitDateToPeriod(FundPricingPeriod, AddPeriodToDate(FundPricingPeriod, MilestoneDate, -1), True)
			End If

			' MilestoneDate.AddDays(0 - MilestoneDate.Day)

			' KDs

			KnowledgeDate = Now()

			If Me.Radio_WholeDay.Checked = True Then
				KnowledgeDate = CDate(CDate(Date_KnowledgeDate.Value.Date).ToString(DISPLAYMEMBER_DATEFORMAT) & " 23:59:59")
			ElseIf Me.Radio_PreciseTime.Checked = True Then
				KnowledgeDate = Me.Date_KnowledgeDate.Value
			End If
			If (KnowledgeDate <= KNOWLEDGEDATE_NOW) Then
				KnowledgeDate = KNOWLEDGEDATE_NOW
			End If

		Catch ex As Exception
			MainForm.LogError(Me.Name & ", btnSetMilestone_Click()", LOG_LEVELS.Error, ex.Message, "Error Validating Set Milestone Form inputs.", ex.StackTrace, True)
			Exit Sub
		End Try

		Try
			ThisNAV = GetUnitPrice(MainForm, Me.Name, FundID, MilestoneDate, StatusGroupFilter, AdministratorDatesFilter, KnowledgeDate, UnitPriceType.GNAV)
			ThisGNAV = GetUnitPrice(MainForm, Me.Name, FundID, MilestoneDate, StatusGroupFilter, AdministratorDatesFilter, KnowledgeDate, UnitPriceType.GAV)

			Me.edit_UnitPrice.Value = ThisNAV

			'Me.edit_BNAV.Value = GetUnitPrice(MainForm, Me.Name, FundID, MilestoneDate, StatusGroupFilter, AdministratorDatesFilter, KnowledgeDate, UnitPriceType.BNAV)

			If (FundUsesEqualisation) Then

				' 2) Get Performance and Equalisation calcs

				Dim ReportData As DataTable
				ReportData = Nothing
				ReportData = MainForm.MainReportHandler.GetEqualisationCalculations(FundID, 0, 0, MilestoneDate, KnowledgeDate)

				Dim ThisRow As DataRow
				Dim RowCounter As Integer
				Dim Sumshares As Double = 0
				Dim IsPending As Integer
				Dim SumCurrentEqualisation As Double = 0
				Dim SumIncentiveFee As Double = 0
				Dim ThisContingentRedemptionFactor As Double
				Dim ThisRedemptionRatio As Double
				Dim ThisCurrentEq As Double
				Dim ThisAccIncentiveFee As Double
				Dim GrossAssets As Double = 0

				'				Shares = [Text_SumSignedUnits] = Sum(IIf([PendingTransaction] = 0, [Signedunits], 0))
				'				Net_Assets = [NAV_Price] * [Text_SumSignedUnits])
				'				Equalisation_Lost = Sum(([InitialEqualisationFactor] - [CurrentEqualisationFactor]) * [RedemptionRatio]))
				'				Equalisation_Factor = Sum([InitialEqualisationFactor] * [RedemptionRatio]))
				'Incentive Fee Accrual	=Sum(([AccIncentiveFee]-[ContingentRedemptionFactor])*[RedemptionRatio])	
				'				Gross_Assets = Text_NetAssets.Value + Text_IncentiveFeeAccrual.Value + Text_SumInitialEqualisation.Value - Text_SumEqualisationLost.Value)
				'				Gross_NAV = Text_GrossAssets.Value / Text_SumSignedUnits.Value)

				Sumshares = 0

				For RowCounter = 0 To (ReportData.Rows.Count - 1)
					ThisRow = ReportData.Rows(RowCounter)

					IsPending = CInt(ThisRow("PendingTransaction"))
					If (IsPending = 0) Then
						Sumshares += CDbl(ThisRow("Signedunits"))
					End If

					ThisRedemptionRatio = CDbl(ThisRow("RedemptionRatio"))
					ThisCurrentEq = CDbl(ThisRow("CurrentEqualisationFactor")) * ThisRedemptionRatio
					SumCurrentEqualisation += ThisCurrentEq
					ThisContingentRedemptionFactor = CDbl(ThisRow("ContingentRedemptionFactor"))
					ThisAccIncentiveFee = CDbl(ThisRow("AccIncentiveFee"))
					SumIncentiveFee += ((ThisAccIncentiveFee - ThisContingentRedemptionFactor) * ThisRedemptionRatio)

				Next

				GrossAssets = (ThisNAV * Sumshares) + SumIncentiveFee + SumCurrentEqualisation
				ThisGNAV = GrossAssets / Sumshares

			Else

				' ToDo
				' More_Code_Here()

			End If


			' 3) Calc Mgmt Fees

      'Dim FundMgmtFees As Double = FundRow.FundManagementFees
      'Dim DaysThisYear As Double = (FitDateToPeriod(DealingPeriod.Annually, AddPeriodToDate(DealingPeriod.Annually, MilestoneDate, -1), True) - FitDateToPeriod(DealingPeriod.Annually, MilestoneDate, True)).TotalDays
      'ThisBNAV = ThisGNAV / (1.0 - (FundMgmtFees * ((MilestoneDate - MilestoneStartDate).TotalDays / DaysThisYear)))

			Me.edit_GNAV.Value = ThisGNAV
      'Me.edit_BNAV.Value = ThisBNAV

			Exit Sub
		Catch ex As Exception
		End Try

		Me.edit_UnitPrice.Value = 0

	End Sub



End Class
