' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmCalculateAttributions.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals

Imports RenaissanceUtilities.DatePeriodFunctions

''' <summary>
''' Class frmCalculateAttributions
''' </summary>
Public Class frmCalculateAttributions

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmCalculateAttributions"/> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The BTN save
    ''' </summary>
  Friend WithEvents btnSave As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN close
    ''' </summary>
  Friend WithEvents btnClose As System.Windows.Forms.Button
    ''' <summary>
    ''' The panel1
    ''' </summary>
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The label2
    ''' </summary>
  Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label3
    ''' </summary>
  Friend WithEvents Label3 As System.Windows.Forms.Label
    ''' <summary>
    ''' The panel2
    ''' </summary>
  Friend WithEvents Panel2 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The label1
    ''' </summary>
  Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ attribution fund
    ''' </summary>
  Friend WithEvents Combo_AttributionFund As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ start knowledge date
    ''' </summary>
  Friend WithEvents Combo_StartKnowledgeDate As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The date_ attribution date
    ''' </summary>
  Friend WithEvents Date_AttributionDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The combo_ end knowledge date
    ''' </summary>
  Friend WithEvents Combo_EndKnowledgeDate As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label5
    ''' </summary>
  Friend WithEvents Label5 As System.Windows.Forms.Label
    ''' <summary>
    ''' The check_ use existing milestone attributions
    ''' </summary>
  Friend WithEvents Check_UseExistingMilestoneAttributions As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ calculate lookthrough attributions
    ''' </summary>
  Friend WithEvents Check_CalculateLookthroughAttributions As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The form_ status strip
    ''' </summary>
  Friend WithEvents Form_StatusStrip As System.Windows.Forms.StatusStrip
    ''' <summary>
    ''' The form_ progress bar
    ''' </summary>
  Friend WithEvents Form_ProgressBar As System.Windows.Forms.ToolStripProgressBar
    ''' <summary>
    ''' The label_ status
    ''' </summary>
  Friend WithEvents Label_Status As System.Windows.Forms.ToolStripStatusLabel
    ''' <summary>
    ''' The check_ save prices
    ''' </summary>
  Friend WithEvents Check_SavePrices As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.btnSave = New System.Windows.Forms.Button
    Me.btnClose = New System.Windows.Forms.Button
    Me.Panel1 = New System.Windows.Forms.Panel
    Me.Panel2 = New System.Windows.Forms.Panel
    Me.Label2 = New System.Windows.Forms.Label
    Me.Label3 = New System.Windows.Forms.Label
    Me.Combo_AttributionFund = New System.Windows.Forms.ComboBox
    Me.Combo_StartKnowledgeDate = New System.Windows.Forms.ComboBox
    Me.Label1 = New System.Windows.Forms.Label
    Me.Date_AttributionDate = New System.Windows.Forms.DateTimePicker
    Me.Combo_EndKnowledgeDate = New System.Windows.Forms.ComboBox
    Me.Label5 = New System.Windows.Forms.Label
    Me.Check_UseExistingMilestoneAttributions = New System.Windows.Forms.CheckBox
    Me.Check_CalculateLookthroughAttributions = New System.Windows.Forms.CheckBox
    Me.Check_SavePrices = New System.Windows.Forms.CheckBox
    Me.Form_StatusStrip = New System.Windows.Forms.StatusStrip
    Me.Form_ProgressBar = New System.Windows.Forms.ToolStripProgressBar
    Me.Label_Status = New System.Windows.Forms.ToolStripStatusLabel
    Me.Panel1.SuspendLayout()
    Me.Form_StatusStrip.SuspendLayout()
    Me.SuspendLayout()
    '
    'btnSave
    '
    Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnSave.Location = New System.Drawing.Point(44, 268)
    Me.btnSave.Name = "btnSave"
    Me.btnSave.Size = New System.Drawing.Size(148, 28)
    Me.btnSave.TabIndex = 7
    Me.btnSave.Text = "Set Attributions"
    '
    'btnClose
    '
    Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnClose.Location = New System.Drawing.Point(300, 268)
    Me.btnClose.Name = "btnClose"
    Me.btnClose.Size = New System.Drawing.Size(75, 28)
    Me.btnClose.TabIndex = 9
    Me.btnClose.Text = "&Close"
    '
    'Panel1
    '
    Me.Panel1.Controls.Add(Me.Panel2)
    Me.Panel1.Location = New System.Drawing.Point(8, 76)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(404, 2)
    Me.Panel1.TabIndex = 47
    '
    'Panel2
    '
    Me.Panel2.Location = New System.Drawing.Point(0, 116)
    Me.Panel2.Name = "Panel2"
    Me.Panel2.Size = New System.Drawing.Size(428, 2)
    Me.Panel2.TabIndex = 0
    '
    'Label2
    '
    Me.Label2.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label2.Location = New System.Drawing.Point(12, 16)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(120, 20)
    Me.Label2.TabIndex = 48
    Me.Label2.Text = "Fund Name"
    '
    'Label3
    '
    Me.Label3.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label3.Location = New System.Drawing.Point(12, 96)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(148, 20)
    Me.Label3.TabIndex = 49
    Me.Label3.Text = "Start KnowledgeDate"
    '
    'Combo_AttributionFund
    '
    Me.Combo_AttributionFund.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_AttributionFund.Location = New System.Drawing.Point(164, 12)
    Me.Combo_AttributionFund.Name = "Combo_AttributionFund"
    Me.Combo_AttributionFund.Size = New System.Drawing.Size(252, 21)
    Me.Combo_AttributionFund.TabIndex = 0
    '
    'Combo_StartKnowledgeDate
    '
    Me.Combo_StartKnowledgeDate.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_StartKnowledgeDate.Items.AddRange(New Object() {"Live"})
    Me.Combo_StartKnowledgeDate.Location = New System.Drawing.Point(164, 92)
    Me.Combo_StartKnowledgeDate.Name = "Combo_StartKnowledgeDate"
    Me.Combo_StartKnowledgeDate.Size = New System.Drawing.Size(252, 21)
    Me.Combo_StartKnowledgeDate.TabIndex = 2
    '
    'Label1
    '
    Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label1.Location = New System.Drawing.Point(12, 48)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(120, 20)
    Me.Label1.TabIndex = 56
    Me.Label1.Text = "Attribution Date"
    '
    'Date_AttributionDate
    '
    Me.Date_AttributionDate.Location = New System.Drawing.Point(164, 44)
    Me.Date_AttributionDate.Name = "Date_AttributionDate"
    Me.Date_AttributionDate.Size = New System.Drawing.Size(252, 20)
    Me.Date_AttributionDate.TabIndex = 1
    Me.Date_AttributionDate.Value = New Date(2006, 3, 1, 11, 55, 39, 906)
    '
    'Combo_EndKnowledgeDate
    '
    Me.Combo_EndKnowledgeDate.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_EndKnowledgeDate.Items.AddRange(New Object() {"Live"})
    Me.Combo_EndKnowledgeDate.Location = New System.Drawing.Point(164, 124)
    Me.Combo_EndKnowledgeDate.Name = "Combo_EndKnowledgeDate"
    Me.Combo_EndKnowledgeDate.Size = New System.Drawing.Size(252, 21)
    Me.Combo_EndKnowledgeDate.TabIndex = 3
    '
    'Label5
    '
    Me.Label5.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label5.Location = New System.Drawing.Point(12, 128)
    Me.Label5.Name = "Label5"
    Me.Label5.Size = New System.Drawing.Size(148, 20)
    Me.Label5.TabIndex = 58
    Me.Label5.Text = "End KnowledgeDate"
    '
    'Check_UseExistingMilestoneAttributions
    '
    Me.Check_UseExistingMilestoneAttributions.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_UseExistingMilestoneAttributions.Location = New System.Drawing.Point(12, 192)
    Me.Check_UseExistingMilestoneAttributions.Name = "Check_UseExistingMilestoneAttributions"
    Me.Check_UseExistingMilestoneAttributions.Size = New System.Drawing.Size(408, 32)
    Me.Check_UseExistingMilestoneAttributions.TabIndex = 5
    Me.Check_UseExistingMilestoneAttributions.Text = "Use Existing Standard Milestone Attributions as the basis for new look -through A" & _
        "ttributions (if they exist)."
    '
    'Check_CalculateLookthroughAttributions
    '
    Me.Check_CalculateLookthroughAttributions.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_CalculateLookthroughAttributions.Location = New System.Drawing.Point(12, 160)
    Me.Check_CalculateLookthroughAttributions.Name = "Check_CalculateLookthroughAttributions"
    Me.Check_CalculateLookthroughAttributions.Size = New System.Drawing.Size(408, 24)
    Me.Check_CalculateLookthroughAttributions.TabIndex = 4
    Me.Check_CalculateLookthroughAttributions.Text = "Calculate Look-Through Attributions, Also calculates standard Attributions."
    '
    'Check_SavePrices
    '
    Me.Check_SavePrices.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Check_SavePrices.Location = New System.Drawing.Point(12, 232)
    Me.Check_SavePrices.Name = "Check_SavePrices"
    Me.Check_SavePrices.Size = New System.Drawing.Size(408, 24)
    Me.Check_SavePrices.TabIndex = 6
    Me.Check_SavePrices.Text = "Save Unit prices before Attribution calculation."
    '
    'Form_StatusStrip
    '
    Me.Form_StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Form_ProgressBar, Me.Label_Status})
    Me.Form_StatusStrip.Location = New System.Drawing.Point(0, 313)
    Me.Form_StatusStrip.Name = "Form_StatusStrip"
    Me.Form_StatusStrip.Size = New System.Drawing.Size(426, 22)
    Me.Form_StatusStrip.TabIndex = 106
    Me.Form_StatusStrip.Text = " "
    '
    'Form_ProgressBar
    '
    Me.Form_ProgressBar.Maximum = 20
    Me.Form_ProgressBar.Name = "Form_ProgressBar"
    Me.Form_ProgressBar.Size = New System.Drawing.Size(150, 17)
    Me.Form_ProgressBar.Step = 1
    Me.Form_ProgressBar.Visible = False
    '
    'Label_Status
    '
    Me.Label_Status.Name = "Label_Status"
    Me.Label_Status.Size = New System.Drawing.Size(10, 17)
    Me.Label_Status.Text = " "
    '
    'frmCalculateAttributions
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(426, 335)
    Me.Controls.Add(Me.Form_StatusStrip)
    Me.Controls.Add(Me.Check_SavePrices)
    Me.Controls.Add(Me.Check_CalculateLookthroughAttributions)
    Me.Controls.Add(Me.Check_UseExistingMilestoneAttributions)
    Me.Controls.Add(Me.Combo_EndKnowledgeDate)
    Me.Controls.Add(Me.Label5)
    Me.Controls.Add(Me.Date_AttributionDate)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.Combo_StartKnowledgeDate)
    Me.Controls.Add(Me.Combo_AttributionFund)
    Me.Controls.Add(Me.Label3)
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.Panel1)
    Me.Controls.Add(Me.btnClose)
    Me.Controls.Add(Me.btnSave)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.Name = "frmCalculateAttributions"
    Me.Text = "Calculate Attributions"
    Me.Panel1.ResumeLayout(False)
    Me.Form_StatusStrip.ResumeLayout(False)
    Me.Form_StatusStrip.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "


  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
  Private WithEvents _MainForm As VeniceMain

  ' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
  Private FormTooltip As New ToolTip()

    ''' <summary>
    ''' The report worker
    ''' </summary>
	Private ReportWorker As BackgroundWorkerThreadSafe = Nothing
    ''' <summary>
    ''' The form controls
    ''' </summary>
  Private FormControls As ArrayList = Nothing
    ''' <summary>
    ''' The report timer
    ''' </summary>
  Private WithEvents ReportTimer As New Windows.Forms.Timer()

  ' Form Constants, specific to the table being updated.
    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
  Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

  ' The standard ChangeID for this form. e.g. tblPerson
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

  ' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
  Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
  Private THIS_FORM_FormID As VeniceFormID

  ' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
  Private FormIsValid As Boolean
    ''' <summary>
    ''' The form changed
    ''' </summary>
  Private FormChanged As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
  Private _FormOpenFailed As Boolean

  ' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
  Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
  Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
  Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
  Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
  Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
    ' Public property to return handle to the 'Main' Venice form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
  Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
    ' Public property maintaining a value indicating if the cursor is over the 'Cancel'
    ' Button on this form.
    ' This property is specifically designed for use by the field formating Event functions
    ' In order that they do not impose format restrictions if the user is about to click the 
    ' 'Cancel' button.
    '
    Get
      Return False
    End Get
    Set(ByVal Value As Boolean)
    End Set
  End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
  Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
    Get
      Return False
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
    Get
      Return True
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmCalculateAttributions"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  Public Sub New(ByVal pMainForm As VeniceMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()

    _MainForm = pMainForm
    AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate
    AddHandler ReportTimer.Tick, AddressOf MainForm.IncrementStatusBarTimerEvent

    _FormOpenFailed = False

    ' ******************************************************
    ' Form Specific Settings :
    ' ******************************************************

    ' Form Permissioning :-

    THIS_FORM_PermissionArea = Me.Name
    THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

    ' 'This' form ID

    THIS_FORM_FormID = VeniceFormID.frmCalculateAttributions

    ' Format Event Handlers for form controls

    AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

    AddHandler Combo_AttributionFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_AttributionFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_AttributionFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_AttributionFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

    ' Set up the ToolTip
    MainForm.SetFormToolTip(Me, FormTooltip)

    THIS_FORM_ChangeID = RenaissanceChangeID.KnowledgeDate

  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
  Public Sub ResetForm() Implements StandardVeniceForm.ResetForm

    Call Form_Load(Me, New System.EventArgs)
  End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
  Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
    ALWAYS_CLOSE_THIS_FORM = True
    Me.Close()
  End Sub


    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
    _FormOpenFailed = False

    If Not (MainForm Is Nothing) Then
      FormIsValid = True
    Else
      MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If


    ' Check User permissions
    Call CheckPermissions()
    If (HasReadPermission = False) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    ' Build Combos

    Call Me.SetFundCombo()

    ' Initialise controls

    Me.Date_AttributionDate.Value = Now.AddDays(-Now.Day)
    Me.Combo_StartKnowledgeDate.SelectedIndex = 0
    Me.Combo_EndKnowledgeDate.SelectedIndex = 0

    Me.MainForm.SetToolStripText(Label_Status, "")

    MainForm.SetComboSelectionLengths(Me)

  End Sub

    ''' <summary>
    ''' Handles the Closing event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
  Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    Dim HideForm As Boolean

    If (ReportWorker IsNot Nothing) Then
      If ReportWorker.IsBusy Then
        e.Cancel = True
        Exit Sub
      End If

      Try
        RemoveHandler ReportTimer.Tick, AddressOf MainForm.IncrementStatusBarTimerEvent
        RemoveHandler ReportWorker.DoWork, AddressOf ReportWorker_DoWork
        RemoveHandler ReportWorker.RunWorkerCompleted, AddressOf ReportWorkerCompleted
        ReportWorker.Dispose()
        ReportWorker = Nothing
      Catch ex As Exception
      End Try
    End If

    ' Hide or Close this form ?
    ' All depends on how many of this form type are Open or in Cache...

    If (btnClose.Enabled = False) Then
      e.Cancel = True
      Exit Sub
    End If

    If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
      HideForm = False
    Else
      HideForm = True
      If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.SingleCache_CacheCount Then
        HideForm = False
      End If
    End If

    If HideForm = True Then
      MainForm.HideInFormsCollection(Me)
      Me.Hide() ' NPP Fix

      e.Cancel = True
    Else
      Try
        MainForm.RemoveFromFormsCollection(Me)
        RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate
        RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler
				RemoveHandler Combo_AttributionFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_AttributionFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_AttributionFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_AttributionFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

      Catch ex As Exception
      End Try
    End If

  End Sub


#End Region

  ' Routine to handle changes / updates to tables by this and other windows.
  ' If this, or any other, form posts a change to a table, then it will invoke an update event 
  ' detailing what tables have been altered.
  ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
  ' the 'VeniceAutoUpdate' event of the main Venice form.
  ' Each form may them react as appropriate to changes in any table that might impact it.
  '
  ''' <summary>
  ''' Process the update event.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The RenaissanceGlobals.RenaissanceUpdateEventArgs instance containing the event data.</param>
  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    Dim KnowledgeDateChanged As Boolean
    Dim SetButtonStatus_Flag As Boolean

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

    KnowledgeDateChanged = False
    SetButtonStatus_Flag = False

    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
      KnowledgeDateChanged = True
    End If

    ' ****************************************************************
    ' Check for changes relevant to this form
    ' ****************************************************************

    ' Changes to the tblFund table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFund) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Call SetFundCombo()
    End If

    ' Changes to the tblUserPermissions table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

      ' Check ongoing permissions.

      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

        FormIsValid = False
        Me.Close()
        Exit Sub
      End If

    End If

    ' ****************************************************************
    '
    ' ****************************************************************


  End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

  ' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
  Private Sub CheckPermissions()
    ' *****************************************************************************
    '
    ' *****************************************************************************

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

  End Sub

    ''' <summary>
    ''' Handles the ValueChanged event of the Date_AttributionDate control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Date_AttributionDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Date_AttributionDate.ValueChanged
    ' *************************************************************************
    ' Ensure that the selected Attribution Date is always the last day of the month.
    ' *************************************************************************

    Dim AttribPeriod As RenaissanceGlobals.DealingPeriod = DealingPeriod.Monthly
    Dim tempDate As Date

    tempDate = FitDateToPeriod(AttribPeriod, Date_AttributionDate.Value.Date, True)

    'tempDate = Date_AttributionDate.Value.AddMonths(1)
    'tempDate = tempDate.AddDays(-tempDate.Day)

    If Date_AttributionDate.Value.Equals(tempDate) = False Then
      Date_AttributionDate.Value = tempDate
    End If

  End Sub

#End Region

#Region " Set Form Combos (Form Specific Code) "

    ''' <summary>
    ''' Sets the fund combo.
    ''' </summary>
  Private Sub SetFundCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_AttributionFund, _
    RenaissanceStandardDatasets.tblFund, _
    "FundName", _
    "FundID", _
    "FundClosed=0")    ' 

  End Sub


#End Region

#Region " SetButton / Control Events (Form Specific Code) "


    ''' <summary>
    ''' Handles the LostFocus event of the Combo_StartKnowledgeDate control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_StartKnowledgeDate_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Combo_StartKnowledgeDate.LostFocus
    If Combo_StartKnowledgeDate.Text.Trim.Length <= 0 Then
      Combo_StartKnowledgeDate.Text = "Live"
      Exit Sub
    End If

    If (Combo_StartKnowledgeDate.Text.Trim.ToUpper = "LIVE") Then
      Exit Sub
    End If

    If IsDate(Combo_StartKnowledgeDate.Text) Then
      If CDate(Combo_StartKnowledgeDate.Text).TimeOfDay.TotalSeconds <= 0 Then
        Combo_StartKnowledgeDate.Text = CDate(Combo_StartKnowledgeDate.Text).ToString(DISPLAYMEMBER_DATEFORMAT)
      Else
        Combo_StartKnowledgeDate.Text = CDate(Combo_StartKnowledgeDate.Text).ToString(DISPLAYMEMBER_LONGDATEFORMAT)
      End If
    Else
      If Not (Combo_StartKnowledgeDate.Text.StartsWith("<Date Not Recognised>")) Then
        Combo_StartKnowledgeDate.Text &= " <Date Not Recognised>"
      End If
    End If
  End Sub

    ''' <summary>
    ''' Handles the KeyPress event of the Combo_StartKnowledgeDate control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.KeyPressEventArgs"/> instance containing the event data.</param>
  Private Sub Combo_StartKnowledgeDate_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Combo_StartKnowledgeDate.KeyPress
    If e.KeyChar = Chr(13) Then
      If Combo_StartKnowledgeDate.Text.Trim.Length <= 0 Then
        Combo_StartKnowledgeDate.Text = "Live"
        Exit Sub
      End If

      If (Combo_StartKnowledgeDate.Text.Trim.ToUpper = "LIVE") Then
        Exit Sub
      End If

      If IsDate(Combo_StartKnowledgeDate.Text) Then
        If CDate(Combo_StartKnowledgeDate.Text).TimeOfDay.TotalSeconds <= 0 Then
          Combo_StartKnowledgeDate.Text = CDate(Combo_StartKnowledgeDate.Text).ToString(DISPLAYMEMBER_DATEFORMAT)
        Else
          Combo_StartKnowledgeDate.Text = CDate(Combo_StartKnowledgeDate.Text).ToString(DISPLAYMEMBER_LONGDATEFORMAT)
        End If
      Else
        If Not (Combo_StartKnowledgeDate.Text.StartsWith("<Date Not Recognised>")) Then
          Combo_StartKnowledgeDate.Text &= " <Date Not Recognised>"
        End If
      End If
    End If

  End Sub

    ''' <summary>
    ''' Handles the LostFocus event of the Combo_EndKnowledgeDate control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_EndKnowledgeDate_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Combo_EndKnowledgeDate.LostFocus
    If Combo_EndKnowledgeDate.Text.Trim.Length <= 0 Then
      Combo_EndKnowledgeDate.Text = "Live"
      Exit Sub
    End If

    If (Combo_EndKnowledgeDate.Text.Trim.ToUpper = "LIVE") Then
      Exit Sub
    End If

    If IsDate(Combo_EndKnowledgeDate.Text) Then
      If CDate(Combo_EndKnowledgeDate.Text).TimeOfDay.TotalSeconds <= 0 Then
        Combo_EndKnowledgeDate.Text = CDate(Combo_EndKnowledgeDate.Text).ToString(DISPLAYMEMBER_DATEFORMAT)
      Else
        Combo_EndKnowledgeDate.Text = CDate(Combo_EndKnowledgeDate.Text).ToString(DISPLAYMEMBER_LONGDATEFORMAT)
      End If
    Else
      If Not (Combo_EndKnowledgeDate.Text.EndsWith("<Date Not Recognised>")) Then
        Combo_EndKnowledgeDate.Text &= " <Date Not Recognised>"
      End If
    End If
  End Sub

    ''' <summary>
    ''' Handles the KeyPress event of the Combo_EndKnowledgeDate control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.KeyPressEventArgs"/> instance containing the event data.</param>
  Private Sub Combo_EndKnowledgeDate_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Combo_EndKnowledgeDate.KeyPress
    If e.KeyChar = Chr(13) Then
      If Combo_EndKnowledgeDate.Text.Trim.Length <= 0 Then
        Combo_EndKnowledgeDate.Text = "Live"
        Exit Sub
      End If

      If (Combo_EndKnowledgeDate.Text.Trim.ToUpper = "LIVE") Then
        Exit Sub
      End If

      If IsDate(Combo_EndKnowledgeDate.Text) Then
        If CDate(Combo_EndKnowledgeDate.Text).TimeOfDay.TotalSeconds <= 0 Then
          Combo_EndKnowledgeDate.Text = CDate(Combo_EndKnowledgeDate.Text).ToString(DISPLAYMEMBER_DATEFORMAT)
        Else
          Combo_EndKnowledgeDate.Text = CDate(Combo_EndKnowledgeDate.Text).ToString(DISPLAYMEMBER_LONGDATEFORMAT)
        End If
      Else
        If Not (Combo_EndKnowledgeDate.Text.EndsWith("<Date Not Recognised>")) Then
          Combo_EndKnowledgeDate.Text &= " <Date Not Recognised>"
        End If
      End If
    End If

  End Sub

    ''' <summary>
  ''' Basic event handler, called when a form value is changed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_AttributionFund.SelectedIndexChanged, Date_AttributionDate.ValueChanged, Combo_StartKnowledgeDate.TextChanged, Combo_EndKnowledgeDate.TextChanged, Check_CalculateLookthroughAttributions.CheckedChanged, Check_SavePrices.CheckedChanged, Check_UseExistingMilestoneAttributions.CheckedChanged
    ' *****************************************************************************
    ' Simple routine to clear the status bar when any form control changes.
    ' *****************************************************************************
    If (MainForm IsNot Nothing) Then Me.MainForm.SetToolStripText(Label_Status, "")

  End Sub


#End Region


#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "


    ''' <summary>
    ''' Handles the Click event of the btnSave control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
    ' *****************************************************************************
    ' Save Attributions
    ' *****************************************************************************

    If (RenaissanceStandardDatasets.tblAttribution.ActivityCount > 0) Then
      MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "There appears to be an Attribution process already running, Attribution cancelled.", "", True)
      Exit Sub
    End If

    Try
      FormControls = MainForm.MainReportHandler.DisableFormControls(Me)
      Form_ProgressBar.Value = Form_ProgressBar.Minimum
      MainForm.SetToolStripText(Label_Status, "Saving Attributions")

      If (ReportWorker Is Nothing) Then
				ReportWorker = New BackgroundWorkerThreadSafe(Nothing)
        AddHandler ReportWorker.DoWork, AddressOf ReportWorker_DoWork
        AddHandler ReportWorker.RunWorkerCompleted, AddressOf ReportWorkerCompleted
      End If

      ReportTimer.Interval = 25
      ReportTimer.Tag = Me.Form_ProgressBar
      ReportTimer.Start()

      ReportWorker.RunWorkerAsync()

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
    End Try
  End Sub

    ''' <summary>
    ''' Handles the DoWork event of the ReportWorker control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.DoWorkEventArgs"/> instance containing the event data.</param>
  Private Sub ReportWorker_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) ' Handles backgroundWorker1.DoWork
    SaveAttributions(ReportTimer)
  End Sub

    ''' <summary>
    ''' Reports the worker completed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.RunWorkerCompletedEventArgs"/> instance containing the event data.</param>
  Private Sub ReportWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) ' Handles backgroundWorker1.RunWorkerCompleted
    Try
      ReportTimer.Stop()
      MainForm.MainReportHandler.EnableFormControls(FormControls)
      MainForm.SetToolStripText(Label_Status, "")

      'Form_ProgressBar = MainForm.ResetProgressBar(Form_StatusStrip, Form_ProgressBar)
      Form_ProgressBar.Value = Form_ProgressBar.Minimum
      Form_ProgressBar.Visible = False
      ReportTimer.Tag = Me.Form_ProgressBar
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
    End Try

  End Sub

    ''' <summary>
    ''' Saves the attributions.
    ''' </summary>
    ''' <param name="FormProgressReportTimer">The form progress report timer.</param>
  Private Sub SaveAttributions(ByVal FormProgressReportTimer As Windows.Forms.Timer)
    ' *****************************************************************************
    ' Save Changes, if any, without prompting.
    ' *****************************************************************************
    Dim FundID As Integer
    Dim AttributionDate As Date
    Dim StartKnowledgeDate As Date
    Dim EndKnowledgeDate As Date

    Dim AttribPeriod As RenaissanceGlobals.DealingPeriod = DealingPeriod.Monthly

    Try

      If (MainForm.GetComboSelectedValue(Combo_AttributionFund) Is Nothing) Then
        FormProgressReportTimer.Stop()
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Attribution Fund must be selected.", "", True)
        Exit Sub
      End If

			If (MainForm.GetComboSelectedIndex(Combo_AttributionFund) < 0) OrElse (CInt(MainForm.GetComboSelectedValue(Combo_AttributionFund)) <= 0) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Attribution Fund must be selected.", "", True)
				Exit Sub
			End If
      FundID = CInt(MainForm.GetComboSelectedValue(Combo_AttributionFund))

			If (MainForm.GetDatetimeValue(Date_AttributionDate) < Renaissance_BaseDate) Then
				FormProgressReportTimer.Stop()
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Attribution Date must be after 1 Jan 1900.", "", True)
				Exit Sub
			End If
      AttributionDate = MainForm.GetDatetimeValue(Date_AttributionDate)

      ' Resolve Start and End Knowledge Dates.
      ' If the date has no time, assume that the whole day is to be used.

      If (MainForm.GetControlText(Combo_StartKnowledgeDate).Trim.ToUpper = "LIVE") Then
        StartKnowledgeDate = KNOWLEDGEDATE_NOW
      ElseIf IsDate(MainForm.GetControlText(Combo_StartKnowledgeDate)) = False Then
        FormProgressReportTimer.Stop()
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Attribution Start Knowledgedate is not a recognised Date.", "", True)
        Exit Sub
      Else
        StartKnowledgeDate = CDate(MainForm.GetControlText(Combo_StartKnowledgeDate))
        If StartKnowledgeDate.TimeOfDay.TotalSeconds = 0 Then
          StartKnowledgeDate = StartKnowledgeDate.AddSeconds(LAST_SECOND)
        End If
      End If

      If (MainForm.GetControlText(Combo_EndKnowledgeDate).ToUpper = "LIVE") Then
        EndKnowledgeDate = KNOWLEDGEDATE_NOW
      ElseIf IsDate(MainForm.GetControlText(Combo_EndKnowledgeDate)) = False Then
        FormProgressReportTimer.Stop()
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Attribution End Knowledgedate is not a recognised Date.", "", True)
        Exit Sub
      Else
        EndKnowledgeDate = CDate(MainForm.GetControlText(Combo_EndKnowledgeDate))
        If EndKnowledgeDate.TimeOfDay.TotalSeconds = 0 Then
          EndKnowledgeDate = EndKnowledgeDate.AddSeconds(LAST_SECOND)
        End If
      End If

      ' Save Unit Prices
      If MainForm.GetCheckBoxChecked(Check_SavePrices) = True Then
        MainForm.SetToolStripText(Label_Status, "Saving Prices.")
        Application.DoEvents()

				Call Attribution_SaveFundPrices(MainForm, FundID, AttributionDate, DEFAULT_STATUSGROUPFILTER, DEFAULT_ADMINISTRATORDATESFILTER)
				Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblPrice), True)
      End If

      MainForm.SetToolStripText(Label_Status, "Saving Attributions.")

      ' Calculate Attributions

      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "Calculate Attributions.", "", "", False)

      Call Attribution_SaveAtributions(MainForm, _
        FundID, _
        AttributionDate, _
        AttribPeriod, _
        False, _
        MainForm.GetCheckBoxChecked(Check_CalculateLookthroughAttributions), _
        MainForm.GetCheckBoxChecked(Check_UseExistingMilestoneAttributions), _
        MainForm.GetCheckBoxChecked(Check_UseExistingMilestoneAttributions), _
        StartKnowledgeDate, EndKnowledgeDate, _
        MainForm.Main_Knowledgedate)

			Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblAttribution), True)

      MainForm.SetToolStripText(Label_Status, "Done.")

    Catch ex As Exception
      FormProgressReportTimer.Stop()
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error calculating Attributions.", ex.StackTrace, True)

      MainForm.SetToolStripText(Label_Status, "Error saving attributions.")
    End Try

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnClose control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
    ' *****************************************************************************
    ' Close Form
    ' *****************************************************************************

    Me.Close()

  End Sub

#End Region


End Class
