' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmPriceGrid.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals
Imports RenaissanceDataClass
Imports RenaissanceUtilities.DatePeriodFunctions

''' <summary>
''' Class frmPriceGrid
''' </summary>
Public Class frmPriceGrid

	Inherits System.Windows.Forms.Form
	Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmPriceGrid"/> class from being created.
    ''' </summary>
	Private Sub New()
		MyBase.New()

		'This call is required by the Windows Form Designer.
		InitializeComponent()

		'Add any initialization after the InitializeComponent() call

	End Sub

	'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
    ''' <summary>
    ''' The edit audit ID
    ''' </summary>
	Friend WithEvents editAuditID As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The BTN nav first
    ''' </summary>
	Friend WithEvents btnNavFirst As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN nav prev
    ''' </summary>
	Friend WithEvents btnNavPrev As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN nav next
    ''' </summary>
	Friend WithEvents btnNavNext As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN last
    ''' </summary>
	Friend WithEvents btnLast As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN cancel
    ''' </summary>
	Friend WithEvents btnCancel As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN save
    ''' </summary>
	Friend WithEvents btnSave As System.Windows.Forms.Button
    ''' <summary>
    ''' The combo_ select instrument
    ''' </summary>
	Friend WithEvents Combo_SelectInstrument As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The panel1
    ''' </summary>
	Friend WithEvents Panel1 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The label1
    ''' </summary>
	Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The group box1
    ''' </summary>
	Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The root menu
    ''' </summary>
	Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
    ''' <summary>
    ''' The panel_ information edit
    ''' </summary>
	Friend WithEvents Panel_InformationEdit As System.Windows.Forms.Panel
    ''' <summary>
    ''' The grid_ prices
    ''' </summary>
	Friend WithEvents Grid_Prices As System.Windows.Forms.DataGridView
    ''' <summary>
    ''' The edit data vendor
    ''' </summary>
	Friend WithEvents editDataVendor As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The check_ show recent
    ''' </summary>
	Friend WithEvents Check_ShowRecent As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The BTN close
    ''' </summary>
	Friend WithEvents btnClose As System.Windows.Forms.Button
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Me.editAuditID = New System.Windows.Forms.TextBox
		Me.btnNavFirst = New System.Windows.Forms.Button
		Me.btnNavPrev = New System.Windows.Forms.Button
		Me.btnNavNext = New System.Windows.Forms.Button
		Me.btnLast = New System.Windows.Forms.Button
		Me.btnCancel = New System.Windows.Forms.Button
		Me.btnSave = New System.Windows.Forms.Button
		Me.Combo_SelectInstrument = New System.Windows.Forms.ComboBox
		Me.Panel1 = New System.Windows.Forms.Panel
		Me.Label1 = New System.Windows.Forms.Label
		Me.GroupBox1 = New System.Windows.Forms.GroupBox
		Me.btnClose = New System.Windows.Forms.Button
		Me.RootMenu = New System.Windows.Forms.MenuStrip
		Me.Panel_InformationEdit = New System.Windows.Forms.Panel
		Me.Grid_Prices = New System.Windows.Forms.DataGridView
		Me.editDataVendor = New System.Windows.Forms.TextBox
		Me.Check_ShowRecent = New System.Windows.Forms.CheckBox
		Me.Panel1.SuspendLayout()
		Me.Panel_InformationEdit.SuspendLayout()
		CType(Me.Grid_Prices, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.SuspendLayout()
		'
		'editAuditID
		'
		Me.editAuditID.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.editAuditID.Enabled = False
		Me.editAuditID.Location = New System.Drawing.Point(922, 33)
		Me.editAuditID.Name = "editAuditID"
		Me.editAuditID.Size = New System.Drawing.Size(69, 20)
		Me.editAuditID.TabIndex = 2
		'
		'btnNavFirst
		'
		Me.btnNavFirst.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnNavFirst.Location = New System.Drawing.Point(8, 8)
		Me.btnNavFirst.Name = "btnNavFirst"
		Me.btnNavFirst.Size = New System.Drawing.Size(40, 28)
		Me.btnNavFirst.TabIndex = 0
		Me.btnNavFirst.Text = "<<"
		'
		'btnNavPrev
		'
		Me.btnNavPrev.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnNavPrev.Location = New System.Drawing.Point(50, 8)
		Me.btnNavPrev.Name = "btnNavPrev"
		Me.btnNavPrev.Size = New System.Drawing.Size(35, 28)
		Me.btnNavPrev.TabIndex = 1
		Me.btnNavPrev.Text = "<"
		'
		'btnNavNext
		'
		Me.btnNavNext.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnNavNext.Location = New System.Drawing.Point(88, 8)
		Me.btnNavNext.Name = "btnNavNext"
		Me.btnNavNext.Size = New System.Drawing.Size(35, 28)
		Me.btnNavNext.TabIndex = 2
		Me.btnNavNext.Text = ">"
		'
		'btnLast
		'
		Me.btnLast.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnLast.Location = New System.Drawing.Point(124, 8)
		Me.btnLast.Name = "btnLast"
		Me.btnLast.Size = New System.Drawing.Size(40, 28)
		Me.btnLast.TabIndex = 3
		Me.btnLast.Text = ">>"
		'
		'btnCancel
		'
		Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
		Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnCancel.Location = New System.Drawing.Point(434, 632)
		Me.btnCancel.Name = "btnCancel"
		Me.btnCancel.Size = New System.Drawing.Size(75, 28)
		Me.btnCancel.TabIndex = 6
		Me.btnCancel.Text = "&Cancel"
		'
		'btnSave
		'
		Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnSave.Location = New System.Drawing.Point(349, 632)
		Me.btnSave.Name = "btnSave"
		Me.btnSave.Size = New System.Drawing.Size(75, 28)
		Me.btnSave.TabIndex = 5
		Me.btnSave.Text = "&Save"
		'
		'Combo_SelectInstrument
		'
		Me.Combo_SelectInstrument.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_SelectInstrument.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_SelectInstrument.Location = New System.Drawing.Point(121, 33)
		Me.Combo_SelectInstrument.Name = "Combo_SelectInstrument"
		Me.Combo_SelectInstrument.Size = New System.Drawing.Size(669, 21)
		Me.Combo_SelectInstrument.TabIndex = 0
		'
		'Panel1
		'
		Me.Panel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Panel1.Controls.Add(Me.btnNavFirst)
		Me.Panel1.Controls.Add(Me.btnNavPrev)
		Me.Panel1.Controls.Add(Me.btnNavNext)
		Me.Panel1.Controls.Add(Me.btnLast)
		Me.Panel1.Location = New System.Drawing.Point(131, 623)
		Me.Panel1.Name = "Panel1"
		Me.Panel1.Size = New System.Drawing.Size(176, 48)
		Me.Panel1.TabIndex = 4
		'
		'Label1
		'
		Me.Label1.Location = New System.Drawing.Point(6, 33)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(100, 23)
		Me.Label1.TabIndex = 18
		Me.Label1.Text = "Select"
		'
		'GroupBox1
		'
		Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.GroupBox1.Location = New System.Drawing.Point(5, 59)
		Me.GroupBox1.Name = "GroupBox1"
		Me.GroupBox1.Size = New System.Drawing.Size(988, 10)
		Me.GroupBox1.TabIndex = 77
		Me.GroupBox1.TabStop = False
		'
		'btnClose
		'
		Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnClose.Location = New System.Drawing.Point(518, 632)
		Me.btnClose.Name = "btnClose"
		Me.btnClose.Size = New System.Drawing.Size(75, 28)
		Me.btnClose.TabIndex = 7
		Me.btnClose.Text = "&Close"
		'
		'RootMenu
		'
		Me.RootMenu.AllowMerge = False
		Me.RootMenu.Location = New System.Drawing.Point(0, 0)
		Me.RootMenu.Name = "RootMenu"
		Me.RootMenu.Size = New System.Drawing.Size(1004, 24)
		Me.RootMenu.TabIndex = 8
		Me.RootMenu.Text = "MenuStrip1"
		'
		'Panel_InformationEdit
		'
		Me.Panel_InformationEdit.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
								Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Panel_InformationEdit.AutoScroll = True
		Me.Panel_InformationEdit.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Panel_InformationEdit.Controls.Add(Me.Grid_Prices)
		Me.Panel_InformationEdit.Location = New System.Drawing.Point(9, 96)
		Me.Panel_InformationEdit.Name = "Panel_InformationEdit"
		Me.Panel_InformationEdit.Size = New System.Drawing.Size(988, 511)
		Me.Panel_InformationEdit.TabIndex = 3
		'
		'Grid_Prices
		'
		Me.Grid_Prices.AllowDrop = True
		Me.Grid_Prices.AllowUserToOrderColumns = True
		Me.Grid_Prices.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
								Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Grid_Prices.Location = New System.Drawing.Point(3, 3)
		Me.Grid_Prices.Name = "Grid_Prices"
		Me.Grid_Prices.Size = New System.Drawing.Size(979, 501)
		Me.Grid_Prices.TabIndex = 0
		'
		'editDataVendor
		'
		Me.editDataVendor.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.editDataVendor.Enabled = False
		Me.editDataVendor.Location = New System.Drawing.Point(796, 33)
		Me.editDataVendor.Name = "editDataVendor"
		Me.editDataVendor.Size = New System.Drawing.Size(120, 20)
		Me.editDataVendor.TabIndex = 1
		'
		'Check_ShowRecent
		'
		Me.Check_ShowRecent.AutoSize = True
		Me.Check_ShowRecent.Location = New System.Drawing.Point(9, 73)
		Me.Check_ShowRecent.Name = "Check_ShowRecent"
		Me.Check_ShowRecent.Size = New System.Drawing.Size(336, 17)
		Me.Check_ShowRecent.TabIndex = 2
		Me.Check_ShowRecent.Text = "Display recent prices only. (May make it easier to add new prices)."
		Me.Check_ShowRecent.UseVisualStyleBackColor = True
		'
		'frmPriceGrid
		'
		Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
		Me.CancelButton = Me.btnCancel
		Me.ClientSize = New System.Drawing.Size(1004, 683)
		Me.Controls.Add(Me.Check_ShowRecent)
		Me.Controls.Add(Me.editDataVendor)
		Me.Controls.Add(Me.Panel_InformationEdit)
		Me.Controls.Add(Me.btnClose)
		Me.Controls.Add(Me.GroupBox1)
		Me.Controls.Add(Me.Label1)
		Me.Controls.Add(Me.Panel1)
		Me.Controls.Add(Me.Combo_SelectInstrument)
		Me.Controls.Add(Me.btnSave)
		Me.Controls.Add(Me.editAuditID)
		Me.Controls.Add(Me.btnCancel)
		Me.Controls.Add(Me.RootMenu)
		Me.MainMenuStrip = Me.RootMenu
		Me.MinimumSize = New System.Drawing.Size(800, 500)
		Me.Name = "frmPriceGrid"
		Me.Text = "Add/Edit Instrument Prices"
		Me.Panel1.ResumeLayout(False)
		Me.Panel_InformationEdit.ResumeLayout(False)
		CType(Me.Grid_Prices, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

#End Region

#Region " Form Locals and Constants "


	' Form 'Parent', the Main Venice form.
	' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
	Private WithEvents _MainForm As VeniceMain

	' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
	Private FormTooltip As New ToolTip()

	' Form Constants, specific to the table being updated.

    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
	Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

	' Form Locals, initialised on 'New' defining what standard data items to use
    ''' <summary>
  ''' Variable holding the form's data table name. taken from the "ThisStandardDataset" object.
    ''' </summary>
	Private THIS_TABLENAME As String
    ''' <summary>
  ''' Variable holding the form's data adaptor name. taken from the "ThisStandardDataset" object.
    ''' </summary>
	Private THIS_ADAPTORNAME As String
    ''' <summary>
  ''' Variable holding the form's dataset name. taken from the "ThisStandardDataset" object.
    ''' </summary>
	Private THIS_DATASETNAME As String

    ''' <summary>
    ''' The generic update object
    ''' </summary>
	Private GenericUpdateObject As RenaissanceTimerUpdateClass

	' The standard ChangeID for this form. e.g. tblGroupList
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
	Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

    ''' <summary>
  ''' Principal control used for selecting items on this form. 
    ''' </summary>
	Private THIS_FORM_SelectingCombo As ComboBox
    ''' <summary>
  ''' Control to select after proessing the "New" button.
    ''' </summary>
	Private THIS_FORM_NewMoveToControl As Control

	' Form Specific Order fields
    ''' <summary>
  ''' Field Name to show in the Select combo.
    ''' </summary>
	Private THIS_FORM_SelectBy As String
    ''' <summary>
  ''' Field Name to order the Select combo by.
    ''' </summary>
	Private THIS_FORM_OrderBy As String

    ''' <summary>
  ''' The field name that holds the unique identifier for each entry. e.g. FundID for the tblFund table.
    ''' </summary>
	Private THIS_FORM_ValueMember As String

	' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
	Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
	Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

	' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
	Private THIS_FORM_FormID As VeniceFormID

	' Data Structures

    ''' <summary>
    ''' My dataset
    ''' </summary>
	Private myDataset As RenaissanceDataClass.DSPrice		 ' Form Specific !!!!
    ''' <summary>
    ''' My table
    ''' </summary>
	Private myTable As RenaissanceDataClass.DSPrice.tblPriceDataTable
    ''' <summary>
    ''' My connection
    ''' </summary>
	Private myConnection As SqlConnection
    ''' <summary>
    ''' My adaptor
    ''' </summary>
	Private myAdaptor As SqlDataAdapter

	'Private PerformanceDataset As RenaissanceDataClass.DSPerformance			' Form Specific !!!!
	'Private PerformanceTable As RenaissanceDataClass.DSPerformance.tblPerformanceDataTable
	'Private PerformanceAdaptor As SqlDataAdapter

    ''' <summary>
    ''' The this standard dataset
    ''' </summary>
	Private ThisStandardDataset As RenaissanceGlobals.StandardDataset


	' Active Element.

    ''' <summary>
    ''' The sorted rows
    ''' </summary>
	Private SortedRows() As DataRow
	'Private SelectBySortedRows() As DataRow
	'Private thisDataRow As RenaissanceDataClass.DSPrice.tblPriceRow		' Form Specific !!!!
    ''' <summary>
    ''' The this audit ID
    ''' </summary>
	Private thisAuditID As Integer
	'Private thisPosition As Integer
    ''' <summary>
    ''' The _ is over cancel button
    ''' </summary>
	Private _IsOverCancelButton As Boolean
    ''' <summary>
    ''' The _ in use
    ''' </summary>
	Private _InUse As Boolean

	' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
	Private FormIsValid As Boolean
    ''' <summary>
    ''' The form changed
    ''' </summary>
	Private FormChanged As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
	Private _FormOpenFailed As Boolean
    ''' <summary>
    ''' The in paint
    ''' </summary>
	Private InPaint As Boolean
    ''' <summary>
    ''' The in get form data_ tick
    ''' </summary>
	Private InGetFormData_Tick As Boolean
    ''' <summary>
    ''' The add new record
    ''' </summary>
	Private AddNewRecord As Boolean

	' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
	Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
	Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
	Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
	Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
	Public ReadOnly Property MainForm() As VeniceMain Implements StandardVeniceForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [form changed flag].
    ''' </summary>
    ''' <value><c>true</c> if [form changed flag]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property FormChangedFlag() As Boolean
		Get
			Return FormChanged
		End Get
	End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
	Public Property IsOverCancelButton() As Boolean Implements StandardVeniceForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return _IsOverCancelButton
		End Get
		Set(ByVal Value As Boolean)
			_IsOverCancelButton = Value
		End Set
	End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
	Public ReadOnly Property IsInPaint() As Boolean Implements StandardVeniceForm.IsInPaint
		Get
			Return InPaint
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property InUse() As Boolean Implements StandardVeniceForm.InUse
		Get
			Return _InUse
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property FormOpenFailed() As Boolean Implements StandardVeniceForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

#End Region

#Region " Local Classes"


#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmPriceGrid"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
	Public Sub New(ByVal pMainForm As VeniceMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()

		_MainForm = pMainForm
		AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

		_FormOpenFailed = False
		_InUse = True

		' ******************************************************
		' Form Specific Settings :
		' ******************************************************

		THIS_FORM_SelectingCombo = Me.Combo_SelectInstrument
		THIS_FORM_NewMoveToControl = Me.Grid_Prices

		' Default Select and Order fields.

		THIS_FORM_SelectBy = "PriceDate"
		THIS_FORM_OrderBy = "PriceDate"

		THIS_FORM_ValueMember = "ID"

		' Form Permissioning :-

		THIS_FORM_PermissionArea = Me.Name
		THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

		' 'This' form ID

		THIS_FORM_FormID = VeniceFormID.frmPriceGrid

		' This form's dataset type.

		ThisStandardDataset = RenaissanceStandardDatasets.tblPrice	' This Defines the Form Data !!! 

		' Format Event Handlers for form controls

		AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

		' Form Control Changed events

		AddHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
		AddHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

		' ******************************************************
		' End Form Specific.
		' ******************************************************

		' Data object names standard to this Form type.

		THIS_TABLENAME = ThisStandardDataset.TableName
		THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
		THIS_DATASETNAME = ThisStandardDataset.DatasetName

		THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

		' Establish / Retrieve data objects for this form.

		myConnection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)

		myAdaptor = New SqlDataAdapter
		MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION), myAdaptor, "TBLPRICEBYINSTRUMENT")

		myDataset = New RenaissanceDataClass.DSPrice ' MainForm.Load_Table(ThisStandardDataset, False)
		myTable = myDataset.Tables(0)

		'PerformanceAdaptor = New SqlDataAdapter
		'MainForm.MainAdaptorHandler.Set_AdaptorCommands(myConnection, PerformanceAdaptor, RenaissanceStandardDatasets.Performance.TableName)
		'PerformanceDataset = New DSPerformance
		'PerformanceTable = PerformanceDataset.tblPerformance
		'If (PerformanceTable.Columns.Contains("ID")) Then
		'	PerformanceTable.Columns("ID").AllowDBNull = True
		'End If

		MainForm.BuildStandardFormMenu(Me.RootMenu, myDataset.Tables(0), AddressOf SelectMenuEvent, AddressOf OrderMenuEvent, AddressOf AuditReportMenuEvent)
		Me.RootMenu.PerformLayout()

	End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

	' Form Initialisation code.
	'
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
	Public Sub ResetForm() Implements StandardVeniceForm.ResetForm
		THIS_FORM_SelectBy = "PriceDate"
		THIS_FORM_OrderBy = "PriceDate"

		Call Form_Load(Me, New System.EventArgs)
	End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
	Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
		_FormOpenFailed = False
		_InUse = True

		' Initialise Data structures. Connection, Adaptor and Dataset.

		If Not (MainForm Is Nothing) Then
			FormIsValid = True
		Else
			MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myConnection Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myAdaptor Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myDataset Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		' Initialse form

		InPaint = True
		IsOverCancelButton = False

		' Check User permissions
		Call CheckPermissions()
		If (HasReadPermission = False) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		Call SetSortedRows()

		' Initialise Grid

		Grid_Prices.DataSource = Nothing
		Grid_Prices.Columns.Clear()

		Try
			Grid_Prices.AutoGenerateColumns = True
			Grid_Prices.DataSource = myTable
		Catch ex As Exception
		Finally
			Grid_Prices.AutoGenerateColumns = False
			Grid_Prices.DataSource = Nothing
		End Try

		Grid_Prices.Columns("RN").Visible = False
		Grid_Prices.Columns("AuditID").Visible = False
		Grid_Prices.Columns("PriceID").Visible = False
		Grid_Prices.Columns("PriceInstrument").Visible = False
    Grid_Prices.Columns("DateEntered").Visible = False
    Grid_Prices.Columns("DateDeleted").Visible = False
    Grid_Prices.Columns("UserName").Visible = False
		Grid_Prices.Columns("PriceComment").Visible = False

		Grid_Prices.Columns("PriceDate").DefaultCellStyle.Format = "dd MMM yyyy"
		Grid_Prices.Columns("PriceDate").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
		Grid_Prices.Columns("PriceDate").HeaderText = "    Date    "

		Grid_Prices.Columns("PricePercent").DefaultCellStyle.Format = "#,##0.00####%;-#,##0.00####%"
		Grid_Prices.Columns("PricePercent").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
		Grid_Prices.Columns("PricePercent").HeaderText = "  Percent   "
		Grid_Prices.Columns("PriceLevel").DefaultCellStyle.Format = "#,##0.00;-#,##0.00"
		Grid_Prices.Columns("PriceLevel").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
		Grid_Prices.Columns("PriceLevel").HeaderText = " PriceLevel "
		Grid_Prices.Columns("PriceLevel").ReadOnly = True
		Grid_Prices.Columns("PriceMultiplier").DefaultCellStyle.Format = "#,##0.00;-#,##0.00"
		Grid_Prices.Columns("PriceMultiplier").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
		Grid_Prices.Columns("PriceMultiplier").HeaderText = " Multiplier "
		Grid_Prices.Columns("PriceNAV").DefaultCellStyle.Format = "#,##0.00;-#,##0.00"
		Grid_Prices.Columns("PriceNAV").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
		Grid_Prices.Columns("PriceNAV").HeaderText = "  Price  "
		Grid_Prices.Columns("PriceBasicNAV").DefaultCellStyle.Format = "#,##0.00;-#,##0.00"
		Grid_Prices.Columns("PriceBasicNAV").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
		Grid_Prices.Columns("PriceBasicNAV").HeaderText = " BASIC NAV  "
		Grid_Prices.Columns("PriceBasicNAV").Visible = False
		Grid_Prices.Columns("PriceGNAV").DefaultCellStyle.Format = "#,##0.00;-#,##0.00"
		Grid_Prices.Columns("PriceGNAV").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
		Grid_Prices.Columns("PriceGNAV").HeaderText = "   GNAV "
		Grid_Prices.Columns("PriceGNAV").Visible = False
		Grid_Prices.Columns("PriceGAV").DefaultCellStyle.Format = "#,##0.00;-#,##0.00"
		Grid_Prices.Columns("PriceGAV").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
		Grid_Prices.Columns("PriceGAV").HeaderText = "   GAV   "
		Grid_Prices.Columns("PriceGAV").Visible = False

		Grid_Prices.Columns.Remove(Grid_Prices.Columns("PriceBaseDate"))
		Dim BaseDateCol As DataGridViewComboBoxColumn = New DataGridViewComboBoxColumn()
		BaseDateCol.Name = "PriceBaseDate"
		BaseDateCol.HeaderText = "% Reference Date"
		BaseDateCol.DefaultCellStyle.Format = "dd MMM yyyy"
		BaseDateCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
		BaseDateCol.DisplayMember = "PriceDate"
		BaseDateCol.ValueMember = "PriceDate"
		BaseDateCol.DataSource = New DataView(myTable, "True", "PriceDate DESC", DataViewRowState.CurrentRows)

		Grid_Prices.Columns.Add(BaseDateCol)

		Grid_Prices.Columns.Remove(Grid_Prices.Columns("PriceFinal"))
		Dim PriceFinalCol As DataGridViewCheckBoxColumn = New DataGridViewCheckBoxColumn()
		PriceFinalCol.Name = "PriceFinal"
		PriceFinalCol.HeaderText = "   Final   "
		PriceFinalCol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
		Grid_Prices.Columns.Add(PriceFinalCol)
		PriceFinalCol.DisplayIndex = Grid_Prices.Columns("PriceIsPercent").DisplayIndex

		Grid_Prices.Columns("PriceIsPercent").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
		Grid_Prices.Columns("PriceIsPercent").HeaderText = " Is Percent "
		Grid_Prices.Columns("PriceIsAdministrator").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
		Grid_Prices.Columns("PriceIsAdministrator").HeaderText = "  Administrator  "
		Grid_Prices.Columns("PriceIsMilestone").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
		Grid_Prices.Columns("PriceIsMilestone").HeaderText = " Milestone "
		Grid_Prices.Columns("PriceIsMilestone").ReadOnly = True
		Grid_Prices.Columns("PriceComment").HeaderText = "  Comment  "

		Dim ModifiedColumn As DataGridViewCheckBoxColumn = New DataGridViewCheckBoxColumn(False)
		ModifiedColumn.Name = "Modified"
		ModifiedColumn.HeaderText = ""
		ModifiedColumn.ReadOnly = True
		ModifiedColumn.Visible = True	' False
		Grid_Prices.Columns.Add(ModifiedColumn)
		ModifiedColumn.DisplayIndex = 0
		ModifiedColumn.Resizable = DataGridViewTriState.False
		ModifiedColumn.Frozen = True
		ModifiedColumn.Width = 30

		Dim DeleteColumn As DataGridViewCheckBoxColumn = New DataGridViewCheckBoxColumn(False)
		DeleteColumn.Name = "Delete"
		DeleteColumn.HeaderText = "Delete"
		Grid_Prices.Columns.Add(DeleteColumn)
		Grid_Prices.Columns("Delete").Visible = True
		Grid_Prices.AllowUserToAddRows = True
		Grid_Prices.AllowUserToDeleteRows = False
		Grid_Prices.Sort(Grid_Prices.Columns("PriceDate"), System.ComponentModel.ListSortDirection.Ascending)
		Grid_Prices.MultiSelect = False

		Grid_Prices.Columns("Modified").DisplayIndex = 0
		Grid_Prices.Columns("PriceDate").DisplayIndex = 1
		Grid_Prices.Columns("PriceNAV").DisplayIndex = 2
		Grid_Prices.Columns("PriceMultiplier").DisplayIndex = 3
		Grid_Prices.Columns("PriceLevel").DisplayIndex = 4
		Grid_Prices.Columns("PriceIsPercent").DisplayIndex = 5
		Grid_Prices.Columns("PricePercent").DisplayIndex = 6
		Grid_Prices.Columns("PriceBaseDate").DisplayIndex = 7
		Grid_Prices.Columns("PriceFinal").DisplayIndex = 8
		Grid_Prices.Columns("PriceIsAdministrator").DisplayIndex = 9
		Grid_Prices.Columns("PriceIsMilestone").DisplayIndex = 10
		Grid_Prices.Columns("Delete").DisplayIndex = 11

		' Initialise Timer

		GenericUpdateObject = MainForm.AddFormUpdate(Me, AddressOf GetFormData_Tick)

		' Display initial record.

		Call SetSelectCombo()

		If THIS_FORM_SelectingCombo.Items.Count > 0 Then
			Me.THIS_FORM_SelectingCombo.SelectedIndex = 0

			LoadSelectedPriceData(myTable, THIS_FORM_SelectingCombo.SelectedValue)

			' Re-Set Controls etc.
			Call SetSortedRows()

			Call GetFormData()

		Else

			Me.THIS_FORM_SelectingCombo.SelectedIndex = (-1)
			Call GetFormData()
		End If

		InPaint = False

	End Sub

    ''' <summary>
    ''' Handles the Closing event of the frmEntity control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
	Private Sub frmEntity_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		Dim HideForm As Boolean

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...

		_InUse = False
		MainForm.RemoveFormUpdate(Me)

		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else
			If (FormChanged = True) Then
				Call SetFormData()
			End If

			HideForm = True
			If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			e.Cancel = True
		Else
			Try
				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

				' Form Control Changed events

				RemoveHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
				RemoveHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

			Catch ex As Exception
			End Try
		End If

	End Sub


#End Region

	' Routine to handle changes / updates to tables by this and other windows.
	' If this, or any other, form posts a change to a table, then it will invoke an update event 
	' detailing what tables have been altered.
	' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
	' the 'VeniceAutoUpdate' event of the main Venice form.
	' Each form may them react as appropriate to changes in any table that might impact it.
	'
    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		Dim OrgInPaint As Boolean
		Dim KnowledgeDateChanged As Boolean
		Dim SetButtonStatus_Flag As Boolean
		Dim RefreshForm As Boolean = False

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		OrgInPaint = InPaint
		InPaint = True
		KnowledgeDateChanged = False
		SetButtonStatus_Flag = False

		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
			KnowledgeDateChanged = True
			RefreshForm = True
		End If

		' ****************************************************************
		' Check for changes relevant to this form
		' ****************************************************************

		' Changes to the tblInstrument table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrument) = True) Or KnowledgeDateChanged Then

			' ReBuild the Instrument derived combos

			Call Me.SetSelectCombo()

			RefreshForm = True
		End If

		' Changes to the KnowledgeDate :-
		If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
			SetButtonStatus_Flag = True
		End If

		' Changes to the tblUserPermissions table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

			' Check ongoing permissions.

			Call CheckPermissions()
			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

				FormIsValid = False
				Me.Close()
				Exit Sub
			End If

			SetButtonStatus_Flag = True

		End If

		' ****************************************************************
		' Changes to the Main FORM table :-
		' ****************************************************************

		If (e.TableChanged(THIS_FORM_ChangeID) = True) Or (e.TableChanged(RenaissanceChangeID.Performance) = True) Or KnowledgeDateChanged Then
			RefreshForm = True

			LoadSelectedPriceData(myTable, THIS_FORM_SelectingCombo.SelectedValue)

			' Re-Set Controls etc.
			Call SetSortedRows()

			If (Not FormChanged) Then
				RefreshForm = True
			End If

		End If

		' ****************************************************************
		' Repaint if not currently in Edit Mode
		'
		' ****************************************************************

		InPaint = OrgInPaint

		' Retrieve form data if appropriate, i.e. this data item is not already being edited.
		If (RefreshForm = True) AndAlso (FormChanged = False) AndAlso (AddNewRecord = False) Then
			GetFormData()	' Includes a call to 'SetButtonStatus()'
		Else
			If SetButtonStatus_Flag Then
				Call SetButtonStatus()
			End If
		End If

	End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

	' Build Sorted list from the Source dataset and update the Select Combo
    ''' <summary>
    ''' Sets the sorted rows.
    ''' </summary>
	Private Sub SetSortedRows()

		Dim OrgInPaint As Boolean

		'Dim thisrow As DataRow
		'Dim thisDrowDownWidth As Integer
		'Dim SizingBitmap As Bitmap
		'Dim SizingGraphics As Graphics
		Dim SelectString As String = "True"

		' Form Specific Selection Combo :-
		If (THIS_FORM_SelectingCombo Is Nothing) Then Exit Sub

		' Code is pretty Generic from here on...

		Try

			' Set paint local so that changes to the selection combo do not trigger form updates.

			OrgInPaint = InPaint
			InPaint = True

			' Get selected Row sets, or exit if no data is present.
			If myDataset Is Nothing Then
				ReDim SortedRows(0)
				'ReDim SelectBySortedRows(0)
			Else
				SortedRows = myTable.Select(SelectString, THIS_FORM_OrderBy)
				'SelectBySortedRows = myTable.Select(SelectString, THIS_FORM_SelectBy)
			End If

			'' Set Combo data source
			'THIS_FORM_SelectingCombo.DataSource = SortedRows
			'THIS_FORM_SelectingCombo.DisplayMember = THIS_FORM_SelectBy
			'THIS_FORM_SelectingCombo.ValueMember = THIS_FORM_ValueMember

			'' Ensure that the Selection Combo's DrowDownWidth is wide enough to take the required strings

			'thisDrowDownWidth = THIS_FORM_SelectingCombo.Width
			'For Each thisrow In SortedRows

			'	' Compute the string dimensions in the given font
			'	SizingBitmap = New Bitmap(1, 1, System.Drawing.Imaging.PixelFormat.Format32bppArgb)
			'	SizingGraphics = Graphics.FromImage(SizingBitmap)
			'	Dim stringSize As SizeF = SizingGraphics.MeasureString(thisrow(THIS_FORM_SelectBy).ToString, THIS_FORM_SelectingCombo.Font)
			'	If (stringSize.Width) > thisDrowDownWidth Then
			'		thisDrowDownWidth = CInt(stringSize.Width)
			'	End If
			'Next

			'THIS_FORM_SelectingCombo.DropDownWidth = thisDrowDownWidth

		Catch ex As Exception
		Finally

			InPaint = OrgInPaint

		End Try

	End Sub


	' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
	Private Sub CheckPermissions()

		Dim Permissions As Integer

		Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

		HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
		HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
		HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
		HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

	End Sub

	' Flag changes to form controls, Event Associations made in 'New' routine.
    ''' <summary>
  ''' Basic event handler, called when a form value is changed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

		If InPaint = False Then
			If (Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord) Then
				FormChanged = True
				Me.btnSave.Enabled = True
				Me.btnCancel.Enabled = True
				Me.Check_ShowRecent.Enabled = False
			End If
		End If

	End Sub

    ''' <summary>
    ''' Selects the menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Public Sub SelectMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Select Menu
		' Event association is made during the dynamic menu creation

		THIS_FORM_SelectBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

		Call SetSortedRows()

	End Sub

    ''' <summary>
    ''' Orders the menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Public Sub OrderMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Order Menu
		' Event association is made during the dynamic menu creation

		THIS_FORM_OrderBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

		Call SetSortedRows()

	End Sub


    ''' <summary>
    ''' Audits the report menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Public Sub AuditReportMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Audit Report Menu
		' Event association is made during the dynamic menu creation

		Select Case CInt(CType(sender, System.Windows.Forms.ToolStripMenuItem).Tag)
			Case 0 ' This Record
				If (Me.AddNewRecord = False) AndAlso (thisAuditID >= 0) Then
					Run_AuditReport_ThisRecord(MainForm, ThisStandardDataset, thisAuditID, Nothing)
				End If

			Case 1 ' All Records
        ' Run_AuditReport_ALLRecords(MainForm, ThisStandardDataset, Nothing) ' Disable this....

		End Select

	End Sub

  ''' <summary>
  ''' Sets the form select criteria.
  ''' </summary>
  ''' <param name="ThisInstrument">The Instrument ID.</param>
  ''' <param name="ShowRecent">Desired value for the 'ShowRecent' check box.</param>
  Public Sub SetCurrentInstrument(ByVal ThisInstrument As Integer, ByVal ShowRecent As Boolean)
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Try

      Check_ShowRecent.Checked = ShowRecent

      If (ThisInstrument > 0) Then
        Combo_SelectInstrument.SelectedValue = ThisInstrument
      Else
        Combo_SelectInstrument.SelectedIndex = 0
      End If

    Catch ex As Exception
    End Try

  End Sub

#End Region

#Region " Set Form Combos (Form Specific Code) "



#End Region

#Region " Get & Set Form Data / SetButton / ValidateForm / btnCancel Events (Form Specific Code) "

    ''' <summary>
    ''' Gets the form data_ tick.
    ''' </summary>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	Private Function GetFormData_Tick() As Boolean
		' *******************************************************************************
		'
		' Callback process for the Generic Form Update Process.
		'
		' Function MUST return True / False on Update.
		' True will clear the update request. False will not.
		'
		' *******************************************************************************
		Dim RVal As Boolean = False

		If (InGetFormData_Tick) OrElse (Not _InUse) Then
			Return False
			Exit Function
		End If

		Try
			InGetFormData_Tick = True

			' Find the correct data row, then show it...

			GetFormData()

			RVal = True

			' Grid_Prices.Sort(Grid_Prices.Columns("PerformanceDate"), System.ComponentModel.ListSortDirection.Ascending)

		Catch ex As Exception
			RVal = False
		Finally
			InGetFormData_Tick = False
		End Try

		Return RVal

	End Function

    ''' <summary>
    ''' Gets the form data.
    ''' </summary>
	Private Sub GetFormData()
		' ******************************************************************************
		'
		' Routine to populate form Controls given a data row.
		' Form is cleared for invalid Datarows or Invalid Form.
		'
		' ******************************************************************************

		Dim OrgInpaint As Boolean

		' Set 'Paint' flag to prevent event actions caused by changing the value of form fields.
		OrgInpaint = InPaint
		InPaint = True

		If (FormIsValid = False) Or (Me.InUse = False) Then
			' Bad / New Datarow - Clear Form.

			thisAuditID = (-1)

			Me.editAuditID.Text = ""
			editDataVendor.Text = ""

			Grid_Prices.DataSource = Nothing
			Grid_Prices.Rows.Clear()

			If AddNewRecord = True Then
				Me.btnCancel.Enabled = True
				Me.Check_ShowRecent.Enabled = False
				Me.THIS_FORM_SelectingCombo.Enabled = False
			Else
				Me.btnCancel.Enabled = False
				Me.Check_ShowRecent.Enabled = True
				Me.THIS_FORM_SelectingCombo.Enabled = True
			End If

		Else

      ' Populate Form with given data.
      Try
        If (Combo_SelectInstrument.SelectedIndex >= 0) AndAlso (IsNumeric(Combo_SelectInstrument.SelectedValue)) Then
          thisAuditID = CInt(Combo_SelectInstrument.SelectedValue)
        Else
          thisAuditID = 0
        End If

        Me.editAuditID.Text = thisAuditID.ToString

        ' Get Instrument Price Data
        If (myTable Is Nothing) OrElse (myTable.Rows.Count <= 0) OrElse (CType(myTable.Rows(0), DSPrice.tblPriceRow).PriceInstrument <> thisAuditID) Then

          LoadSelectedPriceData(myTable, THIS_FORM_SelectingCombo.SelectedValue)

        End If

        ' Re-Set Controls etc.
        Call SetSortedRows()

        'PerformanceTable.Rows.Clear()
        'If (Grid_Prices.DataSource Is Nothing) Then
        '	Grid_Prices.DataSource = PerformanceTable
        'End If

        ' Populate Grid.
        ' Check_ShowRecent

        Grid_Prices.Rows.Clear()
        Dim NewGridRow As DataGridViewRow
        Dim ThisTableRow As DSPrice.tblPriceRow
        Dim RowCounter As Integer
        Dim StartCounter As Integer = 0

        If (Check_ShowRecent.Checked) Then
          StartCounter = Math.Max(0, myTable.Rows.Count - 10)
        End If

        For RowCounter = StartCounter To (myTable.Rows.Count - 1)
          'For Each ThisTableRow As DSPrice.tblPriceRow In myTable.Rows
          ThisTableRow = myTable.Rows(RowCounter)

          NewGridRow = Grid_Prices.Rows(Grid_Prices.Rows.Add())
          NewGridRow.ReadOnly = False

          NewGridRow.Cells("RN").Value = ThisTableRow.RN
          NewGridRow.Cells("AuditID").Value = ThisTableRow.AuditID
          NewGridRow.Cells("PriceID").Value = ThisTableRow.PriceID
          NewGridRow.Cells("PriceInstrument").Value = ThisTableRow.PriceInstrument
          NewGridRow.Cells("PriceDate").Value = ThisTableRow.PriceDate
          NewGridRow.Cells("PricePercent").Value = ThisTableRow.PricePercent
          NewGridRow.Cells("PriceLevel").Value = ThisTableRow.PriceLevel
          NewGridRow.Cells("PriceMultiplier").Value = ThisTableRow.PriceMultiplier
          NewGridRow.Cells("PriceNAV").Value = ThisTableRow.PriceNAV
          NewGridRow.Cells("PriceBasicNAV").Value = ThisTableRow.PriceBasicNAV
          NewGridRow.Cells("PriceGNAV").Value = ThisTableRow.PriceGNAV
          NewGridRow.Cells("PriceGAV").Value = ThisTableRow.PriceGAV
          If (ThisTableRow.IsPriceBaseDateNull) Then
            NewGridRow.Cells("PriceBaseDate").Value = Nothing
          Else
            NewGridRow.Cells("PriceBaseDate").Value = ThisTableRow.PriceBaseDate
          End If
          NewGridRow.Cells("PriceFinal").Value = ThisTableRow.PriceFinal
          NewGridRow.Cells("PriceIsPercent").Value = ThisTableRow.PriceIsPercent
          NewGridRow.Cells("PriceIsAdministrator").Value = ThisTableRow.PriceIsAdministrator
          NewGridRow.Cells("PriceIsMilestone").Value = ThisTableRow.PriceIsMilestone
          NewGridRow.Cells("PriceComment").Value = ThisTableRow.PriceComment
          NewGridRow.Cells("Modified").Value = False
          NewGridRow.Cells("Delete").Value = False

          If CBool(ThisTableRow.PriceIsPercent) Then
            NewGridRow.Cells("PricePercent").ReadOnly = False
            NewGridRow.Cells("PriceBaseDate").ReadOnly = False

            NewGridRow.Cells("PriceLevel").ReadOnly = True
            NewGridRow.Cells("PriceNAV").ReadOnly = True
            NewGridRow.Cells("PriceBasicNAV").ReadOnly = True
            NewGridRow.Cells("PriceGNAV").ReadOnly = True
            NewGridRow.Cells("PriceGAV").ReadOnly = True

          Else
            NewGridRow.Cells("PricePercent").ReadOnly = True
            NewGridRow.Cells("PriceBaseDate").ReadOnly = True

            NewGridRow.Cells("PriceLevel").ReadOnly = True
            NewGridRow.Cells("PriceNAV").ReadOnly = False
            NewGridRow.Cells("PriceBasicNAV").ReadOnly = True
            NewGridRow.Cells("PriceGNAV").ReadOnly = True
            NewGridRow.Cells("PriceGAV").ReadOnly = True

          End If

          If (ThisTableRow.PriceIsMilestone) Then
            NewGridRow.ReadOnly = True
          End If

        Next

        ' Format Grid.

        If (Grid_Prices.SortOrder = Windows.Forms.SortOrder.Descending) Then
          Grid_Prices.Sort(Grid_Prices.SortedColumn, System.ComponentModel.ListSortDirection.Descending)
        Else
          Grid_Prices.Sort(Grid_Prices.SortedColumn, System.ComponentModel.ListSortDirection.Ascending)
        End If
        FormatNumericGridColumn(Grid_Prices.Columns("PricePercent").Index)

        AddNewRecord = False

        Me.btnCancel.Enabled = False
        Me.Check_ShowRecent.Enabled = True
        Me.THIS_FORM_SelectingCombo.Enabled = True

      Catch ex As Exception

        Call MainForm.LogError(Me.Name & ", GetFormData", 0, ex.Message, "Error Showing Data", ex.StackTrace, True)
        Grid_Prices.Rows.Clear()

      End Try

		End If

    '' Allow Field events to trigger before 'InPaint' Is re-set. 
    '' (Should) Prevent Validation errors during Form Draw.
    'Application.DoEvents()

		' Restore 'Paint' flag.
		InPaint = OrgInpaint
		FormChanged = False
		Me.btnSave.Enabled = False

		' As it says on the can....
		Call SetButtonStatus()

	End Sub

    ''' <summary>
    ''' Sets the form data.
    ''' </summary>
    ''' <param name="pConfirm">if set to <c>true</c> [p confirm].</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	Private Function SetFormData(Optional ByVal pConfirm As Boolean = True) As Boolean
		' *************************************************************
		'
		' Note that GroupList Entries are also added from the FundSearch Form. Changes
		' to the GroupList table should be reflected there also.
		'
		' *************************************************************
		Dim ErrMessage As String
		Dim ErrStack As String
		Dim ThisInstrumentID As Integer
		Dim ProtectedItem As Boolean = False
		Dim ChildInstrumentIDs As ArrayList = Nothing
		Dim ChildCount As Integer
		Dim InstrumentList As String

		Dim ChangedPriceDates As New ArrayList
		Dim ChangedChildPriceDates As New ArrayList
		Dim RaiseTransactionsEventFlag As Boolean = False

		ErrMessage = ""
		ErrStack = ""

		' *************************************************************
		' Appropriate Save permission :-
		' *************************************************************

		If (Me.HasUpdatePermission = False) And ((Me.HasInsertPermission = False) Or (Me.AddNewRecord = False)) Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "You do not have permission to save this record.", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' *************************************************************
		' If Save button is disabled then should not be able to save, exit silently.
		' *************************************************************
		If Me.btnSave.Enabled = False Then
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' *************************************************************
		' KnowledgeDate OK :-
		' *************************************************************

		If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Changes to Venice are not allowed when the Knowledgedate is not set to `NOW`", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' Confirm Save, if required.

		If (pConfirm = True) Then
			If MessageBox.Show("Save Changes ?", "Save Changes ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
				Return True
				Exit Function
			End If
		End If

		' *************************************************************
		' Procedure to Save the current form information.
		' *************************************************************

		Dim StatusString As String

		If (FormChanged = False) Or (FormIsValid = False) Then
			Return False
			Exit Function
		End If

		' Instrument ID

		If (Combo_SelectInstrument.SelectedIndex >= 0) AndAlso (IsNumeric(Combo_SelectInstrument.SelectedValue)) AndAlso (CInt(Combo_SelectInstrument.SelectedValue) > 0) Then
			ThisInstrumentID = CInt(Combo_SelectInstrument.SelectedValue)
			InstrumentList = ThisInstrumentID.ToString
		Else
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Instrument Combo : Instrument appears not to be selected. Cant get Instrument ID.", "", True)
			Return False
			Exit Function
		End If

		' Validation
		StatusString = ""
		If ValidateForm(StatusString) = False Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Validation Error." & vbCrLf & StatusString, "", True)
			Return False
			Exit Function
		End If

	
		' *************************************************************
		' Collect Prices to Change
		' *************************************************************

		Dim PriceUpdateTable As New DSPrice.tblPriceDataTable
		Dim NewPriceRow As DSPrice.tblPriceRow
		Dim SortedPriceArray() As DSPrice.tblPriceRow = Nothing

		Dim ChildInstrumentUpdateNecessary As Boolean = False
		Dim ChildPriceTable As RenaissanceDataClass.DSPrice.tblPriceDataTable = Nothing
		Dim ThisGridRow As DataGridViewRow = Nothing

		Try

			For Each ThisGridRow In Grid_Prices.Rows
				If (Not ThisGridRow.IsNewRow) AndAlso (ThisGridRow.Cells("Modified").Value) Then
					NewPriceRow = PriceUpdateTable.NewtblPriceRow

					NewPriceRow.PriceInstrument = ThisInstrumentID
					NewPriceRow.PriceDate = CDate(ThisGridRow.Cells("PriceDate").Value)

					If (Not ChangedPriceDates.Contains(NewPriceRow.PriceDate)) Then
						ChangedPriceDates.Add(NewPriceRow.PriceDate)
					End If

					If CBool(ThisGridRow.Cells("PriceFinal").Value) Then
						NewPriceRow.PriceFinal = True
					Else
						NewPriceRow.PriceFinal = False
					End If

					NewPriceRow.PriceIsMilestone = False ' Can't Update Milestone Prices.

					If CBool(ThisGridRow.Cells("PriceIsAdministrator").Value) Then
						NewPriceRow.PriceIsAdministrator = True
					Else
						NewPriceRow.PriceIsAdministrator = False
					End If

					NewPriceRow.PriceMultiplier = CDbl(ThisGridRow.Cells("PriceMultiplier").Value)
					NewPriceRow.PriceBasicNAV = 0
					NewPriceRow.PriceGNAV = 0
					NewPriceRow.PriceGAV = 0
					NewPriceRow.PriceComment = ""

					If CBool(ThisGridRow.Cells("PriceIsPercent").Value) Then
						NewPriceRow.PriceIsPercent = (-1)
						NewPriceRow.PricePercent = CDbl(ThisGridRow.Cells("PricePercent").Value)
						NewPriceRow.PriceBaseDate = CDate(ThisGridRow.Cells("PriceBaseDate").Value)

						' If it is a Percent Price and NAV and Level are Zero, then the SQL Update
						' routine will calculate the Prices

						NewPriceRow.PriceNAV = 0
						NewPriceRow.PriceLevel = 0

						ChildInstrumentUpdateNecessary = True

					Else
						NewPriceRow.PriceIsPercent = 0
						NewPriceRow.PricePercent = 0
						NewPriceRow.SetPriceBaseDateNull()
						NewPriceRow.PriceNAV = CDbl(ThisGridRow.Cells("PriceNAV").Value)
						NewPriceRow.PriceLevel = NewPriceRow.PriceNAV * NewPriceRow.PriceMultiplier
					End If

					' Add New Row to the Update Table.

					PriceUpdateTable.Rows.Add(NewPriceRow)
					NewPriceRow = Nothing

				End If

			Next

		Catch ex As Exception

			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Error, "Error Getting changed Prices from Grid.", ex.Message, ex.StackTrace, True)
			Return False
			Exit Function

		End Try

		' Child Instruments

		Try

			If (ChildInstrumentUpdateNecessary) Then

				ChildInstrumentIDs = GetPriceChildInstrumentIDs(MainForm, ThisInstrumentID)
				ChildPriceTable = New RenaissanceDataClass.DSPrice.tblPriceDataTable

				' Set Instrument List string 

				For ChildCount = 0 To (ChildInstrumentIDs.Count - 1)
					InstrumentList &= "," & ChildInstrumentIDs(ChildCount).ToString
				Next

			Else

				ChildInstrumentIDs = Nothing

			End If

		Catch ex As Exception

			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Error, "Error Getting Child Instrument List.", ex.Message, ex.StackTrace, True)
			Return False
			Exit Function

		End Try

		Try

			SortedPriceArray = PriceUpdateTable.Select("True", "PriceDate")

		Catch ex As Exception
		End Try

		Try


			Dim InstrumentCounter As Integer

			If (SortedPriceArray IsNot Nothing) AndAlso (SortedPriceArray.Length > 0) Then

				For InstrumentCounter = 0 To (SortedPriceArray.Length - 1)
					NewPriceRow = SortedPriceArray(InstrumentCounter)

					MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblPrice, New DataRow() {NewPriceRow})

					' Update Child Price ?

					If (ChildInstrumentUpdateNecessary) AndAlso (NewPriceRow.PriceIsPercent) AndAlso (ChildInstrumentIDs IsNot Nothing) AndAlso (ChildInstrumentIDs.Count > 0) Then

						' OK, Propagate Price Changes.
						Dim ChildDataRow As RenaissanceDataClass.DSPrice.tblPriceRow

						For ChildCount = 0 To (ChildInstrumentIDs.Count - 1)
							' If a price exists for the Base Date, then propogate the Child price.

							If PriceExists(MainForm, CInt(ChildInstrumentIDs(ChildCount)), NewPriceRow.PriceBaseDate) Then

								If (Not ChangedChildPriceDates.Contains(NewPriceRow.PriceDate)) Then
									ChangedChildPriceDates.Add(NewPriceRow.PriceDate)
								End If

								ChildDataRow = ChildPriceTable.NewRow

								ChildDataRow.PriceInstrument = CInt(ChildInstrumentIDs(ChildCount))
								ChildDataRow.PriceDate = NewPriceRow.PriceDate
								ChildDataRow.PricePercent = NewPriceRow.PricePercent
								ChildDataRow.PriceLevel = 0
								ChildDataRow.PriceNAV = 0
								ChildDataRow.PriceMultiplier = NewPriceRow.PriceMultiplier
								ChildDataRow.PriceBasicNAV = 0
								ChildDataRow.PriceGNAV = 0
								ChildDataRow.PriceGAV = 0
								ChildDataRow.PriceBaseDate = NewPriceRow.PriceBaseDate
								ChildDataRow.PriceFinal = NewPriceRow.PriceFinal
								ChildDataRow.PriceIsPercent = NewPriceRow.PriceIsPercent
								ChildDataRow.PriceIsAdministrator = NewPriceRow.PriceIsAdministrator
								ChildDataRow.PriceIsMilestone = False
								ChildDataRow.PriceComment = "Child % Price Cascade"

								ChildPriceTable.Rows.Add(ChildDataRow)

								MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblPrice, New DataRow() {ChildDataRow})

							End If

						Next

					End If

				Next

			End If

		Catch ex As Exception

			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Error, "Error Saving Instrument Prices.", ex.Message, ex.StackTrace, True)

		End Try

		' *************************************************************
		' Update dependent transactions
		' *************************************************************

		Try

			If (SortedPriceArray IsNot Nothing) AndAlso (SortedPriceArray.Length > 0) Then

				Dim RoughTransactionCount As Integer
				Dim SortedPriceDates() As Date = ChangedPriceDates.ToArray(GetType(Date))
				Array.Sort(SortedPriceDates)
				Dim SortedChildPriceDates() As Date = ChangedChildPriceDates.ToArray(GetType(Date))
				Array.Sort(SortedChildPriceDates)

				' Get RoughTransactionCount

				Dim SelectQuery As New SqlCommand
				Dim RVal As Object = Nothing
				Dim FirstPriceDate As Date = Renaissance_EndDate_Data

				Try
					If (SortedPriceDates IsNot Nothing) AndAlso (SortedPriceDates.Length > 0) AndAlso (SortedPriceDates(0) < FirstPriceDate) Then
						FirstPriceDate = SortedPriceDates(0)
					End If

					Try
						SelectQuery.CommandType = CommandType.Text
						SelectQuery.CommandText = "SELECT TOP 1 TransactionInstrument FROM tblTransaction WHERE (TransactionDateDeleted IS NULL) AND (TransactionExemptFromUpdate=0) AND (TransactionInstrument IN (" & InstrumentList & ")) AND ( TransactionValueDate >= '" & FirstPriceDate.ToString(QUERY_SHORTDATEFORMAT) & "')"
						SelectQuery.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

						SelectQuery.Connection = MainForm.GetVeniceConnection()	' MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)

						SyncLock SelectQuery.Connection
							RVal = SelectQuery.ExecuteScalar
						End SyncLock

					Catch ex As Exception

						RVal = Nothing

					Finally

						If (SelectQuery IsNot Nothing) Then
							SelectQuery.Connection.Close()
							SelectQuery.Connection = Nothing
						End If

					End Try

					If (RVal IsNot Nothing) AndAlso (IsNumeric(RVal)) AndAlso (CInt(RVal) > 0) Then
						RoughTransactionCount = 1
					End If

				Catch ex As Exception

					RoughTransactionCount = 1

					Call MainForm.LogError(Me.Name & ", SetFormData", 0, ErrMessage, "Error whilst checking for Transactions to update.", ErrStack, True)

				End Try

				' 

				If (RoughTransactionCount > 0) Then

					If MessageBox.Show("Do you want to update Transactions" & vbCrLf & "that have been affected by this price change ?", "Update Transactions ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then

						' Update transactions for Main Instrument

						If Update_Dependent_Transactions_NonMilestone(Me.MainForm, Me.Name, ThisInstrumentID, SortedPriceDates) = False Then
							RaiseTransactionsEventFlag = True
							Application.DoEvents()
						End If

						' Update Transactions for child instruments.

						If (Not (ChildInstrumentIDs Is Nothing)) AndAlso (ChildInstrumentIDs.Count > 0) AndAlso (ChangedChildPriceDates.Count > 0) Then
							For ChildCount = 0 To (ChildInstrumentIDs.Count - 1)
								If Update_Dependent_Transactions_NonMilestone(Me.MainForm, Me.Name, CInt(ChildInstrumentIDs(ChildCount)), SortedChildPriceDates) = False Then
									RaiseTransactionsEventFlag = True
									Application.DoEvents()
								End If
							Next
						End If

					End If

				End If

			End If

		Catch ex As Exception

			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Error, "Error Updating related transactions.", ex.Message, ex.StackTrace, True)

		End Try

		Try

			' Reload table / Update Event.

      MainForm.ReloadTable_Background(RenaissanceChangeID.tblPrice, "", False)

			FormChanged = False

      Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID), True)

			If (RaiseTransactionsEventFlag) Then
				Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblTransaction), True)
				Application.DoEvents()
			End If

			Application.DoEvents()

		Catch ex As Exception

			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Error, "Error Reloading / Updating after Price Updates.", ex.Message, ex.StackTrace, True)

		End Try


	End Function

    ''' <summary>
    ''' Sets the button status.
    ''' </summary>
	Private Sub SetButtonStatus()
		' Sets the status of the form controlls appropriate to the current users 
		'permissions and the 'Changed' or 'New' status of the form.

		' No Read permission :-

		If Me.HasReadPermission = False Then
			MainForm.LogError(Me.Name & ", SetButtonStatus()", 0, "", "You do not have Read permission for this Form", "", True)
			Me.Close()
			Exit Sub
		End If

		If (Me.MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And _
		 ((Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord)) Then

			Me.Grid_Prices.Enabled = True

		Else

			Me.Grid_Prices.Enabled = False

		End If

	End Sub

    ''' <summary>
    ''' Validates the form.
    ''' </summary>
    ''' <param name="pReturnString">The p return string.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	Private Function ValidateForm(ByRef pReturnString As String) As Boolean
		' Form Validation code.
		' 
		' This code should be the final arbiter of what is allowed. no assumptions regarding 
		' prior validation should be made.
		' 
		' This Code is called by the SetFormData routine before position changes.
		'
		Dim RVal As Boolean

		RVal = True
		pReturnString = ""

		' Validate 

		Return RVal

	End Function

    ''' <summary>
    ''' Handles the MouseEnter event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnCancel_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseEnter
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = True
	End Sub

    ''' <summary>
    ''' Handles the MouseLeave event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnCancel_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseLeave
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = False
	End Sub

#End Region

#Region " Navigation Code / GetPosition() (Generic Code) "

    ''' <summary>
    ''' Handles the SelectComboChanged event of the Combo control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_SelectComboChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
		' ***************************************************************
		' Selection Combo. SelectedItem changed.
		'
		' ***************************************************************

		' Don't react to changes made in paint routines etc.

		If InPaint Then Exit Sub

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		If (GenericUpdateObject IsNot Nothing) Then
			Try
				GenericUpdateObject.FormToUpdate = True	 '	GetFormDataToUpdate = True
			Catch ex As Exception
			End Try
		Else
			Call GetFormData()
		End If

	End Sub


    ''' <summary>
    ''' Handles the Click event of the btnNavPrev control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnNavPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavPrev.Click
		' 'Previous' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		If (THIS_FORM_SelectingCombo.Items.Count > 0) AndAlso (THIS_FORM_SelectingCombo.SelectedIndex > 0) Then
			THIS_FORM_SelectingCombo.SelectedIndex = Math.Max(THIS_FORM_SelectingCombo.SelectedIndex - 1, 0)
		End If
	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnNavNext control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnNavNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavNext.Click
		' 'Next' Button.

		If (THIS_FORM_SelectingCombo.Items.Count > 0) AndAlso (THIS_FORM_SelectingCombo.SelectedIndex < THIS_FORM_SelectingCombo.Items.Count) Then
			THIS_FORM_SelectingCombo.SelectedIndex = Math.Min(THIS_FORM_SelectingCombo.SelectedIndex + 1, THIS_FORM_SelectingCombo.Items.Count - 1)
		End If


	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnNavFirst control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnNavFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavFirst.Click
		' 'First' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		If (THIS_FORM_SelectingCombo.Items.Count > 0) Then
			THIS_FORM_SelectingCombo.SelectedIndex = 0
		End If

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnLast control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLast.Click
		' 'Last' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		If (THIS_FORM_SelectingCombo.Items.Count > 0) Then
			THIS_FORM_SelectingCombo.SelectedIndex = THIS_FORM_SelectingCombo.Items.Count - 1
		End If

	End Sub


#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

    ''' <summary>
    ''' Handles the Click event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
		' Cancel Changes, redisplay form.

		FormChanged = False
		AddNewRecord = False

		Me.THIS_FORM_SelectingCombo.Enabled = True

		Call GetFormData()

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnSave control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
		' Save Changes, if any, without prompting.

		If (FormChanged = True) Then
			Call SetFormData(False)
		End If

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnClose control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
		' *************************************************************
		' Close Form
		' *************************************************************


		If (FormChanged = True) Then
			Call SetFormData()
		End If

		Me.Close()

	End Sub

#End Region

#Region " Form Control Event Code"

    ''' <summary>
    ''' Handles the CheckedChanged event of the Check_ShowRecent control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Check_ShowRecent_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_ShowRecent.CheckedChanged
		' ************************************************************************************
		'
		'
		' ************************************************************************************

		Try
			If (Not InPaint) AndAlso (Not FormChangedFlag) Then
				GetFormData()
			End If
		Catch ex As Exception
		End Try
	End Sub

    ''' <summary>
    ''' Handles the CellEnter event of the Grid_Prices control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.DataGridViewCellEventArgs"/> instance containing the event data.</param>
	Private Sub Grid_Prices_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Grid_Prices.CellEnter
		' ****************************************************************************************
		'
		'
		' ****************************************************************************************

		Try
			If (Me.IsDisposed) OrElse (Not Me.Created) Then
				Exit Sub
			End If

			If (Grid_Prices.Rows(e.RowIndex).IsNewRow) Then

				If IsNumeric(Grid_Prices.Item(Grid_Prices.Columns("PriceID").Index, e.RowIndex).Value) = False Then
					Grid_Prices.Item(Grid_Prices.Columns("RN").Index, e.RowIndex).Value = 0
					Grid_Prices.Item(Grid_Prices.Columns("AuditID").Index, e.RowIndex).Value = 0
					Grid_Prices.Item(Grid_Prices.Columns("PriceID").Index, e.RowIndex).Value = 0
					Grid_Prices.Item(Grid_Prices.Columns("PriceInstrument").Index, e.RowIndex).Value = thisAuditID
					Grid_Prices.Item(Grid_Prices.Columns("PriceDate").Index, e.RowIndex).Value = Now.Date
					Grid_Prices.Item(Grid_Prices.Columns("PricePercent").Index, e.RowIndex).Value = 0
					Grid_Prices.Item(Grid_Prices.Columns("PriceLevel").Index, e.RowIndex).Value = 0
					Grid_Prices.Item(Grid_Prices.Columns("PriceMultiplier").Index, e.RowIndex).Value = 1.0#
					Grid_Prices.Item(Grid_Prices.Columns("PriceNAV").Index, e.RowIndex).Value = 0
					Grid_Prices.Item(Grid_Prices.Columns("PriceBasicNAV").Index, e.RowIndex).Value = 0
					Grid_Prices.Item(Grid_Prices.Columns("PriceGNAV").Index, e.RowIndex).Value = 0
					Grid_Prices.Item(Grid_Prices.Columns("PriceGAV").Index, e.RowIndex).Value = 0
					Grid_Prices.Item(Grid_Prices.Columns("PriceBaseDate").Index, e.RowIndex).Value = Renaissance_BaseDate
          Grid_Prices.Item(Grid_Prices.Columns("PriceFinal").Index, e.RowIndex).Value = 1
					Grid_Prices.Item(Grid_Prices.Columns("PriceIsPercent").Index, e.RowIndex).Value = False
					Grid_Prices.Item(Grid_Prices.Columns("PriceIsAdministrator").Index, e.RowIndex).Value = False
					Grid_Prices.Item(Grid_Prices.Columns("PriceIsMilestone").Index, e.RowIndex).Value = False
					Grid_Prices.Item(Grid_Prices.Columns("PriceComment").Index, e.RowIndex).Value = ""
					Grid_Prices.Item(Grid_Prices.Columns("Modified").Index, e.RowIndex).Value = False
					Grid_Prices.Item(Grid_Prices.Columns("Delete").Index, e.RowIndex).Value = False

					If (e.RowIndex > 0) AndAlso IsDate(Grid_Prices.Item(Grid_Prices.Columns("PriceDate").Index, e.RowIndex - 1).Value) Then
						Grid_Prices.Item(Grid_Prices.Columns("PriceDate").Index, e.RowIndex).Value = FitDateToPeriod(DealingPeriod.Monthly, AddPeriodToDate(DealingPeriod.Monthly, CDate(Grid_Prices.Item(Grid_Prices.Columns("PriceDate").Index, e.RowIndex - 1).Value), 1), True)
					Else
						Grid_Prices.Item(Grid_Prices.Columns("PriceDate").Index, e.RowIndex).Value = FitDateToPeriod(DealingPeriod.Monthly, Now.Date, True)
					End If

					Grid_Prices.BeginEdit(True)

				End If

			End If

			If (e.RowIndex >= 0) Then
				If Grid_Prices.Columns(e.ColumnIndex).Visible AndAlso (Grid_Prices.Columns(e.ColumnIndex).ValueType Is GetType(Double)) Then
					Dim ThisGridRow As DataGridViewRow = Grid_Prices.Rows(e.RowIndex)

					Select Case e.ColumnIndex

						Case Grid_Prices.Columns("PricePercent").Index

							ThisGridRow.Cells(e.ColumnIndex).Style.Format = "#,##0.00######%;-#,##0.00######%"

						Case Else

							ThisGridRow.Cells(e.ColumnIndex).Style.Format = "#,##0.00######;-#,##0.00######"

					End Select

				End If
			End If
		Catch ex As Exception

		End Try
	End Sub

    ''' <summary>
    ''' Handles the CellLeave event of the Grid_Prices control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.DataGridViewCellEventArgs"/> instance containing the event data.</param>
	Private Sub Grid_Prices_CellLeave(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Grid_Prices.CellLeave
		' ****************************************************************************************
		'
		'
		' ****************************************************************************************

		Try
			If (Me.IsDisposed) OrElse (Not Me.Created) Then
				Exit Sub
			End If

			' If the row being left id the 'NewRow' row, then nothing has been entered on it and it should be cleared.

			If (Grid_Prices.Rows(e.RowIndex).IsNewRow) Then
				Grid_Prices.Item(Grid_Prices.Columns("RN").Index, e.RowIndex).Value = 0
				Grid_Prices.Item(Grid_Prices.Columns("AuditID").Index, e.RowIndex).Value = 0
				Grid_Prices.Item(Grid_Prices.Columns("PriceID").Index, e.RowIndex).Value = 0
				Grid_Prices.Item(Grid_Prices.Columns("PriceInstrument").Index, e.RowIndex).Value = 0
				Grid_Prices.Item(Grid_Prices.Columns("PriceDate").Index, e.RowIndex).Value = Now.Date
				Grid_Prices.Item(Grid_Prices.Columns("PricePercent").Index, e.RowIndex).Value = 0
				Grid_Prices.Item(Grid_Prices.Columns("PriceLevel").Index, e.RowIndex).Value = 0
				Grid_Prices.Item(Grid_Prices.Columns("PriceMultiplier").Index, e.RowIndex).Value = 0
				Grid_Prices.Item(Grid_Prices.Columns("PriceNAV").Index, e.RowIndex).Value = 0
				Grid_Prices.Item(Grid_Prices.Columns("PriceBasicNAV").Index, e.RowIndex).Value = 0
				Grid_Prices.Item(Grid_Prices.Columns("PriceGNAV").Index, e.RowIndex).Value = 0
				Grid_Prices.Item(Grid_Prices.Columns("PriceGAV").Index, e.RowIndex).Value = 0
				Grid_Prices.Item(Grid_Prices.Columns("PriceBaseDate").Index, e.RowIndex).Value = Renaissance_BaseDate
        Grid_Prices.Item(Grid_Prices.Columns("PriceFinal").Index, e.RowIndex).Value = 1
				Grid_Prices.Item(Grid_Prices.Columns("PriceIsPercent").Index, e.RowIndex).Value = False
				Grid_Prices.Item(Grid_Prices.Columns("PriceIsAdministrator").Index, e.RowIndex).Value = False
				Grid_Prices.Item(Grid_Prices.Columns("PriceIsMilestone").Index, e.RowIndex).Value = False
				Grid_Prices.Item(Grid_Prices.Columns("PriceComment").Index, e.RowIndex).Value = ""
				Grid_Prices.Item(Grid_Prices.Columns("Modified").Index, e.RowIndex).Value = False
				Grid_Prices.Item(Grid_Prices.Columns("Delete").Index, e.RowIndex).Value = False

			End If

			If (e.RowIndex >= 0) Then
				If Grid_Prices.Columns(e.ColumnIndex).Visible AndAlso (Grid_Prices.Columns(e.ColumnIndex).ValueType Is GetType(Double)) Then
					Dim ThisGridRow As DataGridViewRow = Grid_Prices.Rows(e.RowIndex)

					ThisGridRow.Cells(e.ColumnIndex).Style.Format = Grid_Prices.Columns(e.ColumnIndex).DefaultCellStyle.Format

				End If
			End If

		Catch ex As Exception

		End Try

	End Sub

    ''' <summary>
    ''' Handles the CellValueChanged event of the Grid_Prices control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.DataGridViewCellEventArgs"/> instance containing the event data.</param>
	Private Sub Grid_Prices_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Grid_Prices.CellValueChanged
		' ****************************************************************************************
		'
		'
		' ****************************************************************************************

		Try
			If (Grid_Prices.Columns.Contains("Modified")) Then
				If (e.ColumnIndex = Grid_Prices.Columns("Modified").Index) Then
					Exit Sub
				End If
			End If

			Dim ThisGridRow As DataGridViewRow = Nothing
			Dim UpdatePercentPrice As Boolean = False
			Dim UpdateContingentPrices As Boolean = False

			If (Not InPaint) Then
				Call FormControlChanged(Grid_Prices, Nothing)
				Grid_Prices.Item(Grid_Prices.Columns("Modified").Index, e.RowIndex).Value = True
			End If

			' Format Cell

			If (e.RowIndex >= 0) Then
				ThisGridRow = Grid_Prices.Rows(e.RowIndex)

				Select Case e.ColumnIndex

					Case Grid_Prices.Columns("PricePercent").Index

						If IsNumeric(Grid_Prices.Item(e.ColumnIndex, e.RowIndex).Value) Then
							If CDbl(Grid_Prices.Item(e.ColumnIndex, e.RowIndex).Value) < 0 Then
								Grid_Prices.Item(e.ColumnIndex, e.RowIndex).Style.ForeColor = Color.Red
							Else
								If Grid_Prices.Item(e.ColumnIndex, e.RowIndex).Style.ForeColor <> Grid_Prices.Columns(e.ColumnIndex).DefaultCellStyle.ForeColor Then
									Grid_Prices.Item(e.ColumnIndex, e.RowIndex).Style.ForeColor = Grid_Prices.Columns(e.ColumnIndex).DefaultCellStyle.ForeColor
								End If
							End If

							If (Not InPaint) Then
								UpdatePercentPrice = True
							End If
						End If

					Case Grid_Prices.Columns("PriceIsPercent").Index

						If CBool(Grid_Prices.Item(e.ColumnIndex, e.RowIndex).Value) Then
							ThisGridRow.Cells("PricePercent").ReadOnly = False
							ThisGridRow.Cells("PriceBaseDate").ReadOnly = False

							ThisGridRow.Cells("PriceLevel").ReadOnly = True
							ThisGridRow.Cells("PriceNAV").ReadOnly = True
							ThisGridRow.Cells("PriceBasicNAV").ReadOnly = True
							ThisGridRow.Cells("PriceGNAV").ReadOnly = True
							ThisGridRow.Cells("PriceGAV").ReadOnly = True

							If (Not InPaint) Then
								UpdatePercentPrice = True
							End If

						Else
							ThisGridRow.Cells("PricePercent").ReadOnly = True
							ThisGridRow.Cells("PriceBaseDate").ReadOnly = True

							ThisGridRow.Cells("PriceLevel").ReadOnly = True
							ThisGridRow.Cells("PriceNAV").ReadOnly = False
							ThisGridRow.Cells("PriceBasicNAV").ReadOnly = True
							ThisGridRow.Cells("PriceGNAV").ReadOnly = True
							ThisGridRow.Cells("PriceGAV").ReadOnly = True


						End If

					Case Grid_Prices.Columns("PriceMultiplier").Index

						If (Not InPaint) Then
              ThisGridRow.Cells("PriceLevel").Value = Nz(ThisGridRow.Cells("PriceNAV").Value, 0.0#) * Nz(ThisGridRow.Cells("PriceMultiplier").Value, 0.0#)
						End If

					Case Grid_Prices.Columns("PriceNAV").Index

						If (Not InPaint) Then
              ThisGridRow.Cells("PriceLevel").Value = Nz(ThisGridRow.Cells("PriceNAV").Value, 0.0#) * Nz(ThisGridRow.Cells("PriceMultiplier"), 0.0#).Value
							UpdateContingentPrices = True
						End If

					Case Grid_Prices.Columns("PriceBaseDate").Index

						If (Not InPaint) Then
							UpdatePercentPrice = True
						End If

				End Select
			End If


			If UpdatePercentPrice Then

        If (Not InPaint) AndAlso (ThisGridRow IsNot Nothing) AndAlso (Nz(ThisGridRow.Cells("PriceIsPercent").Value, False)) AndAlso (IsDate(ThisGridRow.Cells("PriceBaseDate").Value)) Then
          ' Update Price Level etc...

          ' First Get Row for Base reference
          Dim BaseRow As DataGridViewRow
          For Each BaseRow In Grid_Prices.Rows
            If (Not BaseRow.IsNewRow) AndAlso (IsDate(BaseRow.Cells("PriceBaseDate").Value)) AndAlso (CDate(BaseRow.Cells("PriceDate").Value) = CDate(ThisGridRow.Cells("PriceBaseDate").Value)) Then

              Dim orgInpaint As Boolean
              Try
                orgInpaint = InPaint
                InPaint = True

                ThisGridRow.Cells("PriceNAV").Value = BaseRow.Cells("PriceNAV").Value * (1.0# + ThisGridRow.Cells("PricePercent").Value)
                ThisGridRow.Cells("PriceLevel").Value = ThisGridRow.Cells("PriceNAV").Value * ThisGridRow.Cells("PriceMultiplier").Value
                ThisGridRow.Cells("PriceBasicNAV").Value = 0
                ThisGridRow.Cells("PriceGNAV").Value = 0
                ThisGridRow.Cells("PriceGAV").Value = 0

              Catch ex As Exception
              Finally
                InPaint = orgInpaint
              End Try

              Exit For
            End If
          Next

        End If

			End If

			If (UpdateContingentPrices) Then

				If (Not InPaint) AndAlso (ThisGridRow IsNot Nothing) Then

					Dim UpdateRow As DataGridViewRow
					For Each UpdateRow In Grid_Prices.Rows

						If (Not UpdateRow.IsNewRow) AndAlso (UpdateRow IsNot ThisGridRow) AndAlso (CBool(UpdateRow.Cells("PriceIsPercent").Value)) AndAlso (IsDate(UpdateRow.Cells("PriceDate").Value)) AndAlso (CDate(UpdateRow.Cells("PriceBaseDate").Value) = CDate(ThisGridRow.Cells("PriceDate").Value)) AndAlso (CDate(UpdateRow.Cells("PriceDate").Value) > CDate(ThisGridRow.Cells("PriceDate").Value)) Then

							Try

								UpdateRow.Cells("PriceNAV").Value = ThisGridRow.Cells("PriceNAV").Value * (1.0# + UpdateRow.Cells("PricePercent").Value)

							Catch ex As Exception
							End Try

						End If

					Next

				End If

			End If

		Catch ex As Exception
		End Try

	End Sub

    ''' <summary>
    ''' Handles the CellContentClick event of the Grid_Prices control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.DataGridViewCellEventArgs"/> instance containing the event data.</param>
	Private Sub Grid_Prices_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Grid_Prices.CellContentClick
		' ****************************************************************************************
		'
		'
		' ****************************************************************************************

		'Try

		'	If (Not InPaint) AndAlso (e.RowIndex >= 0) Then
		'		Call FormControlChanged(Grid_Prices, Nothing)
		'		Grid_Prices.Item(Grid_Prices.Columns("Modified").Index, e.RowIndex).Value = True
		'	End If

		'Catch ex As Exception
		'End Try

	End Sub

    ''' <summary>
    ''' Handles the CellParsing event of the Grid_Prices control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.DataGridViewCellParsingEventArgs"/> instance containing the event data.</param>
	Private Sub Grid_Prices_CellParsing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellParsingEventArgs) Handles Grid_Prices.CellParsing
		' ****************************************************************************************
		'
		'
		' ****************************************************************************************

		Try
			e.Value = ConvertValue(e.Value, e.DesiredType)
			e.ParsingApplied = True
		Catch ex As Exception
			e.ParsingApplied = False
		End Try
	End Sub

    ''' <summary>
    ''' Handles the RowsRemoved event of the Grid_Prices control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.DataGridViewRowsRemovedEventArgs"/> instance containing the event data.</param>
	Private Sub Grid_Prices_RowsRemoved(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowsRemovedEventArgs) Handles Grid_Prices.RowsRemoved
		' ****************************************************************************************
		'
		'
		' ****************************************************************************************


		Try
			If (Not InPaint) Then
        If (Grid_Prices.Rows.Count > e.RowIndex) AndAlso (Not Grid_Prices.Rows(e.RowIndex).IsNewRow) Then
          Try
            ' Reflect deletion in data table

            'Dim ThisID As Integer
            'Dim ThisDate As Date

            'ThisID = Grid_Prices.Rows(e.RowIndex).Cells("ID").Value
            'ThisDate = Grid_Prices.Rows(e.RowIndex).Cells("PerformanceDate").Value

            'For Each ThisTableRow As DSPerformance.tblPerformanceRow In PerformanceTable.Select("(ID=" & ThisID.ToString & ") AND (PerformanceDate='" & ThisDate.ToString(QUERY_SHORTDATEFORMAT) & "')")
            '	ThisTableRow.Delete()
            'Next

          Catch ex As Exception
          End Try
        End If

				Call FormControlChanged(Grid_Prices, Nothing)
			End If
		Catch ex As Exception
		End Try

	End Sub

    ''' <summary>
    ''' Handles the DataError event of the Grid_Prices control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.DataGridViewDataErrorEventArgs"/> instance containing the event data.</param>
	Private Sub Grid_Prices_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles Grid_Prices.DataError

	End Sub

    ''' <summary>
    ''' The _valid data
    ''' </summary>
	Private _validData As Boolean

    ''' <summary>
    ''' Handles the DragEnter event of the Grid_Prices control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.DragEventArgs"/> instance containing the event data.</param>
	Private Sub Grid_Prices_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles Grid_Prices.DragEnter
		' ***********************************************************************************
		'
		'
		' ***********************************************************************************

		Try
			' Get the data
			Dim DragString As String = CType(e.Data.GetData(GetType(String)), String)

			' No empty data
			If DragString Is Nothing OrElse DragString.Length = 0 Then
				_validData = False
				Return
			End If

			If (DragString IsNot Nothing) Then
				If DragString.Split(Chr(13)).Length > 1 Then
					_validData = True
					Return
				End If
			End If

			_validData = False
		Catch ex As Exception
		End Try
	End Sub

    ''' <summary>
    ''' Handles the DragLeave event of the Grid_Prices control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Grid_Prices_DragLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid_Prices.DragLeave
		' ***********************************************************************************
		'
		'
		' ***********************************************************************************

		Try
			_validData = False
		Catch ex As Exception
		End Try
	End Sub

    ''' <summary>
    ''' Handles the DragOver event of the Grid_Prices control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.DragEventArgs"/> instance containing the event data.</param>
	Private Sub Grid_Prices_DragOver(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles Grid_Prices.DragOver
		' ***********************************************************************************
		'
		'
		' ***********************************************************************************

		Try
			If _validData Then
				e.Effect = DragDropEffects.Copy
			Else
				e.Effect = DragDropEffects.None
			End If
		Catch ex As Exception
		End Try
	End Sub

    ''' <summary>
    ''' Handles the DragDrop event of the Grid_Prices control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.DragEventArgs"/> instance containing the event data.</param>
	Private Sub Grid_Prices_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles Grid_Prices.DragDrop
		' ***********************************************************************************
		'
		'
		' ***********************************************************************************

		Try
			'get the data
			Dim DragString As String = CType(e.Data.GetData(GetType(String)), String)

			Grid_Prices_ParseString(DragString)

		Catch ex As Exception
		End Try
	End Sub

    ''' <summary>
    ''' Grid_s the prices_ parse string.
    ''' </summary>
    ''' <param name="ReturnsString">The returns string.</param>
	Private Sub Grid_Prices_ParseString(ByVal ReturnsString As String)
		' ***********************************************************************************
		'
		'
		' ***********************************************************************************

		Try
			' No data
			If (ReturnsString Is Nothing) OrElse (ReturnsString.Length <= 0) Then
				Exit Sub
			End If

			'get the data
			Dim DragString As String = ReturnsString ' CType(e.Data.GetData(GetType(String)), String)

			Dim DroppedRows() As String
			Dim DroppedCols() As String
			Dim RowCount As Integer
			Dim ColCount As Integer
			Dim IsInRows As Boolean = False
			Dim IsInCols As Boolean = False
			Dim DateValues(-1) As Date
			Dim ReturnValue(-1) As Double
			Dim ReturnIndex As Integer
			Dim SourceIndex As Integer

			DroppedRows = DragString.Split(New String() {CStr(Chr(13)) & CStr(Chr(10))}, StringSplitOptions.RemoveEmptyEntries)

			If (DroppedRows Is Nothing) OrElse (DroppedRows.Length <= 0) Then
				Exit Sub
			End If
			RowCount = DroppedRows.Length

			DroppedCols = DroppedRows(0).Split(Chr(9))
			If (DroppedCols Is Nothing) OrElse (DroppedCols.Length <= 0) Then
				Exit Sub
			End If
			ColCount = DroppedCols.Length

			If (RowCount <= 1) And (ColCount <= 1) Then
				Exit Sub
			End If

			' OK, Now try to figure out what format the Data is in.

			If (RowCount = 1) Then
				' Must be <Date>, <Value> [,<Date>, <Value>] .... on a single line to be valid.

				ReturnIndex = 0

				For SourceIndex = 0 To (CInt(ColCount / 2) - 1)
					If (ConvertIsNumeric(DroppedCols((SourceIndex * 2) + 1))) Then
						If IsDate(DroppedCols(SourceIndex * 2)) Then
							ReturnIndex += 1
						ElseIf IsNumeric(DroppedCols(SourceIndex * 2)) AndAlso (CDbl(DroppedCols(SourceIndex * 2)) > 10000) Then
							ReturnIndex += 1
						End If
					End If
				Next

				If (ReturnIndex > 0) Then
					ReDim DateValues(ReturnIndex - 1)
					ReDim ReturnValue(ReturnIndex - 1)

					ReturnIndex = 0

					For SourceIndex = 0 To (CInt(ColCount / 2) - 1)
						If (ConvertIsNumeric(DroppedCols((SourceIndex * 2) + 1))) Then
							If IsDate(DroppedCols(SourceIndex * 2)) Then
								DateValues(ReturnIndex) = Date.Parse(DroppedCols(SourceIndex * 2))
								ReturnValue(ReturnIndex) = ConvertValue(DroppedCols((SourceIndex * 2) + 1), GetType(Double))

								ReturnIndex += 1
							ElseIf IsNumeric(DroppedCols(SourceIndex * 2)) AndAlso (CDbl(DroppedCols(SourceIndex * 2)) > 10000) Then
								DateValues(ReturnIndex) = Date.FromOADate(CDbl(DroppedCols(SourceIndex * 2)))
								ReturnValue(ReturnIndex) = ConvertValue(DroppedCols((SourceIndex * 2) + 1), GetType(Double))

								ReturnIndex += 1
							End If
						End If

						If (ReturnIndex >= DateValues.Length) Then
							Exit For
						End If
					Next
				End If

			ElseIf (ColCount = 1) And (RowCount = 2) Then
				' Must be in <Date>, <Value> to be valid.

				If (ConvertIsNumeric(DroppedRows(1))) Then
					If IsDate(DroppedRows(0)) Then
						ReDim DateValues(0)
						ReDim ReturnValue(0)

						DateValues(0) = Date.Parse(DroppedRows(0))
						ReturnValue(0) = ConvertValue(DroppedRows(1), GetType(Double))
					ElseIf IsNumeric(DroppedRows(0)) AndAlso (CDbl(IsNumeric(DroppedRows(0))) > 10000) Then
						ReDim DateValues(0)
						ReDim ReturnValue(0)

						DateValues(0) = Date.FromOADate(CDbl(DroppedRows(0)))
						ReturnValue(0) = ConvertValue(DroppedRows(1), GetType(Double))
					End If

				End If

			ElseIf (ColCount > 1) And (RowCount > 1) Then

				If (ColCount = 2) And (RowCount = 2) Then
					' In Rows or Cols >

					If IsDate(DroppedCols(1)) Then
						IsInRows = True
					ElseIf IsNumeric(DroppedCols(1)) AndAlso (CDbl(DroppedCols(1)) > 10000) Then
						IsInRows = True
					Else
						IsInCols = True
					End If

				ElseIf (ColCount = 2) Then
					IsInCols = True

				ElseIf (RowCount = 2) Then
					IsInRows = True
				End If

				If (IsInRows) Then
					Dim DroppedDates() As String = DroppedRows(0).Split(Chr(9))
					Dim DroppedReturns() As String = DroppedRows(1).Split(Chr(9))

					ReturnIndex = 0

					If (DroppedDates IsNot Nothing) AndAlso (DroppedReturns IsNot Nothing) AndAlso (DroppedDates.Length = DroppedReturns.Length) Then
						For SourceIndex = 0 To (ColCount - 1)
							If (ConvertIsNumeric(DroppedReturns(SourceIndex))) Then
								If (IsDate(DroppedDates(SourceIndex))) Then
									ReturnIndex += 1
								ElseIf (IsNumeric(DroppedDates(SourceIndex))) AndAlso (CDbl(DroppedDates(SourceIndex)) > 10000) Then
									ReturnIndex += 1
								End If
							End If
						Next

						If (ReturnIndex > 0) Then
							ReDim DateValues(ReturnIndex - 1)
							ReDim ReturnValue(ReturnIndex - 1)

							ReturnIndex = 0

							For SourceIndex = 0 To (ColCount - 1)
								If (ConvertIsNumeric(DroppedReturns(SourceIndex))) Then
									If (IsDate(DroppedDates(SourceIndex))) Then
										DateValues(ReturnIndex) = Date.Parse(DroppedDates(SourceIndex))
										ReturnValue(ReturnIndex) = ConvertValue(DroppedReturns(SourceIndex), GetType(Double))

										ReturnIndex += 1
									ElseIf (IsNumeric(DroppedDates(SourceIndex))) AndAlso (CDbl(DroppedDates(SourceIndex)) > 10000) Then
										DateValues(ReturnIndex) = Date.FromOADate(CDbl(DroppedDates(SourceIndex)))
										ReturnValue(ReturnIndex) = ConvertValue(DroppedReturns(SourceIndex), GetType(Double))

										ReturnIndex += 1
									End If
								End If

								If (ReturnIndex >= DateValues.Length) Then
									Exit For
								End If
							Next

						End If
					End If

				ElseIf (IsInCols) Then
					ReturnIndex = 0

					For SourceIndex = 0 To (RowCount - 1)
						DroppedCols = DroppedRows(SourceIndex).Split(Chr(9))

						If (DroppedCols.Length = 2) AndAlso (ConvertIsNumeric(DroppedCols(1))) Then
							If IsDate(DroppedCols(0)) Then
								ReturnIndex += 1
							ElseIf IsNumeric(DroppedCols(0)) AndAlso (CDbl(DroppedCols(0)) > 10000) Then
								ReturnIndex += 1
							End If
						End If
					Next

					If (ReturnIndex > 0) Then
						ReDim DateValues(ReturnIndex - 1)
						ReDim ReturnValue(ReturnIndex - 1)

						ReturnIndex = 0

						For SourceIndex = 0 To (RowCount - 1)
							DroppedCols = DroppedRows(SourceIndex).Split(Chr(9))

							If (DroppedCols.Length = 2) AndAlso (ConvertIsNumeric(DroppedCols(1))) Then
								If IsDate(DroppedCols(0)) Then

									DateValues(ReturnIndex) = Date.Parse(DroppedCols(0))
									ReturnValue(ReturnIndex) = ConvertValue(DroppedCols(1), GetType(Double))

									ReturnIndex += 1
								ElseIf IsNumeric(DroppedCols(0)) AndAlso (CDbl(DroppedCols(0)) > 10000) Then
									DateValues(ReturnIndex) = Date.FromOADate(CDbl(DroppedCols(0)))
									ReturnValue(ReturnIndex) = ConvertValue(DroppedCols(1), GetType(Double))

									ReturnIndex += 1
								End If
							End If

							If (ReturnIndex >= DateValues.Length) Then
								Exit For
							End If
						Next

					End If
				End If
			End If

			' Merge in Data....
			' Ahhhh, at last....

			If (DateValues Is Nothing) OrElse (DateValues.Length <= 0) Then
				Exit Sub
			End If
			If (ReturnValue Is Nothing) OrElse (ReturnValue.Length <= 0) Then
				Exit Sub
			End If

			Dim ThisDate As Date
			Dim ThisReturn As Double
			Dim GridCounter As Integer
			Dim NewGridRow As DataGridViewRow
			Dim GridDateOrdinal As Integer = Grid_Prices.Columns("PriceDate").Index
			Dim GridPriceOrdinal As Integer = Grid_Prices.Columns("PriceLevel").Index
			Dim FoundIt As Boolean

			For ReturnIndex = 0 To (DateValues.Length - 1)
				ThisDate = DateValues(ReturnIndex)
				ThisReturn = ReturnValue(ReturnIndex)
				FoundIt = False

				For GridCounter = 0 To (Grid_Prices.Rows.Count - 1)
					Try
						If (Grid_Prices.Item(GridDateOrdinal, GridCounter).Value = ThisDate) Then

							Grid_Prices.Item(GridPriceOrdinal, GridCounter).Value = ThisReturn
							Grid_Prices.UpdateCellValue(GridPriceOrdinal, GridCounter)
							FoundIt = True
							Exit For
						End If
					Catch ex As Exception
					End Try
				Next

				If (Not FoundIt) Then
					NewGridRow = Grid_Prices.Rows(Grid_Prices.Rows.Add())

					NewGridRow.Cells("AuditID").Value = 0
					NewGridRow.Cells("PriceID").Value = 0
					NewGridRow.Cells("PriceInstrument").Value = thisAuditID
					NewGridRow.Cells("PriceDate").Value = ThisDate
					NewGridRow.Cells("PriceLevel").Value = ThisReturn
					NewGridRow.Cells("PricePercent").Value = 0
					NewGridRow.Cells("PriceMultiplier").Value = 1
					NewGridRow.Cells("PriceNAV").Value = ThisReturn
					NewGridRow.Cells("PriceBasicNAV").Value = 0
					NewGridRow.Cells("PriceGNAV").Value = 0
					NewGridRow.Cells("PriceGAV").Value = 0
          NewGridRow.Cells("PriceFinal").Value = 1
					NewGridRow.Cells("PriceIsPercent").Value = False
					NewGridRow.Cells("PriceIsAdministrator").Value = False
					NewGridRow.Cells("PriceIsMilestone").Value = 0
					NewGridRow.Cells("PriceComment").Value = ""
					NewGridRow.Cells("Modified").Value = True
				End If
			Next

			' Format Grid.

			If (Grid_Prices.SortOrder = Windows.Forms.SortOrder.Descending) Then
				Grid_Prices.Sort(Grid_Prices.SortedColumn, System.ComponentModel.ListSortDirection.Descending)
			Else
				Grid_Prices.Sort(Grid_Prices.SortedColumn, System.ComponentModel.ListSortDirection.Ascending)
			End If
			FormatNumericGridColumn(Grid_Prices.Columns("PricePercent").Index)

		Catch ex As Exception
		End Try
	End Sub

    ''' <summary>
    ''' Handles the KeyDown event of the Grid_Prices control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
	Private Sub Grid_Prices_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Grid_Prices.KeyDown
		' ***********************************************************************************
		'
		'
		' ***********************************************************************************

		Try
			If e.Control And (e.KeyCode = Keys.C) Then
				Dim HeaderString As String = "Instrument" & Chr(9) & "Date" & Chr(9) & "Price" & Chr(9) & "Multiplier" & Chr(9) & "NAV" & Chr(9) & "Percent" & Chr(13) & Chr(10)
				Dim GridRow As Integer

				For GridRow = 0 To (Grid_Prices.Rows.Count - 1)
					Try
						If (GridRow <> Grid_Prices.NewRowIndex) Then
							HeaderString &= Grid_Prices.Item(Grid_Prices.Columns("PriceInstrument").Index, GridRow).Value.ToString & Chr(9) & _
							CDate(Nz(Grid_Prices.Item(Grid_Prices.Columns("PriceDate").Index, GridRow).Value, Renaissance_BaseDate)).ToString(DISPLAYMEMBER_DATEFORMAT) & Chr(9) & _
							Grid_Prices.Item(Grid_Prices.Columns("PriceLevel").Index, GridRow).Value.ToString & Chr(9) & _
							Grid_Prices.Item(Grid_Prices.Columns("PriceMultiplier").Index, GridRow).Value.ToString() & Chr(9) & _
							Grid_Prices.Item(Grid_Prices.Columns("PriceNAV").Index, GridRow).Value.ToString & Chr(9) & _
							Grid_Prices.Item(Grid_Prices.Columns("PricePercent").Index, GridRow).Value.ToString & Chr(13) & Chr(10)
						End If
					Catch ex As Exception
					End Try
				Next

				Clipboard.Clear()
				Clipboard.SetData(System.Windows.Forms.DataFormats.Text, (HeaderString))

				e.Handled = True
			ElseIf e.Control And (e.KeyCode = Keys.V) Then
				Dim ClipboardData As String

				ClipboardData = Clipboard.GetData(System.Windows.Forms.DataFormats.Text)

				Grid_Prices_ParseString(ClipboardData)

				e.Handled = True
			End If
		Catch ex As Exception
		End Try

	End Sub

    ''' <summary>
    ''' Formats the numeric grid column.
    ''' </summary>
    ''' <param name="pColumnID">The p column ID.</param>
	Private Sub FormatNumericGridColumn(ByVal pColumnID As Integer)
		' ****************************************************************************************
		'
		'
		' ****************************************************************************************
		Dim RowCount As Integer

		Try
			For RowCount = 0 To (Grid_Prices.Rows.Count - 1)
				If (Not Grid_Prices.Rows(RowCount).IsNewRow) Then
					If IsNumeric(Grid_Prices.Item(pColumnID, RowCount).Value) Then
						If CDbl(Grid_Prices.Item(pColumnID, RowCount).Value) < 0 Then
							Grid_Prices.Item(pColumnID, RowCount).Style.ForeColor = Color.Red
						Else
							If Grid_Prices.Item(pColumnID, RowCount).Style.ForeColor <> Grid_Prices.Columns(pColumnID).DefaultCellStyle.ForeColor Then
								Grid_Prices.Item(pColumnID, RowCount).Style.ForeColor = Grid_Prices.Columns(pColumnID).DefaultCellStyle.ForeColor
							End If
						End If

					End If
				End If
			Next
		Catch ex As Exception
		End Try

	End Sub

#End Region

#Region " Bug hunting "

	' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
	' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
	' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

    ''' <summary>
    ''' Called when [row updating].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
	Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
		Dim a As Integer

		If Not e.Command Is Nothing Then
			a = 1
		End If

		If Not e.Errors Is Nothing Then
			a = 2
		End If
	End Sub

    ''' <summary>
    ''' Called when [row updated].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
	Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
		Dim a As Integer

		If Not e.Command Is Nothing Then
			a = 1
		End If

		If Not e.Errors Is Nothing Then
			a = 2
		End If
	End Sub

    ''' <summary>
    ''' Called when [row fill error].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
	Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
		Dim a As Integer

		a = 1
	End Sub

#End Region

    ''' <summary>
    ''' Sets the select combo.
    ''' </summary>
	Private Sub SetSelectCombo()


		Call MainForm.SetTblGenericCombo( _
		THIS_FORM_SelectingCombo, _
		RenaissanceStandardDatasets.tblInstrument, _
		"InstrumentDescription", _
		"InstrumentID", _
		"")		' 


	End Sub

    ''' <summary>
    ''' Loads the selected price data.
    ''' </summary>
    ''' <param name="pPriceTable">The p price table.</param>
    ''' <param name="pInstrumentID">The p instrument ID.</param>
	Private Sub LoadSelectedPriceData(ByRef pPriceTable As RenaissanceDataClass.DSPrice.tblPriceDataTable, ByVal pInstrumentID As Object)
		' ************************************************************************************
		'
		'
		' ************************************************************************************

		Try

			If (pPriceTable Is Nothing) Then
				pPriceTable = New RenaissanceDataClass.DSPrice.tblPriceDataTable
			Else
				pPriceTable.Clear()
			End If

			If IsNumeric(pInstrumentID) AndAlso (CInt(pInstrumentID) > 0) Then
				myAdaptor.SelectCommand.Parameters("@InstrumentID").Value = CInt(pInstrumentID)
			Else
				myAdaptor.SelectCommand.Parameters("@InstrumentID").Value = (-1)
			End If

			myAdaptor.SelectCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

			SyncLock myAdaptor.SelectCommand.Connection
        MainForm.LoadTable_Custom(pPriceTable, myAdaptor.SelectCommand)
        'pPriceTable.Load(myAdaptor.SelectCommand.ExecuteReader)
			End SyncLock

		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error loading Price Data (LoadSelectedPriceData)", ex.Message, ex.StackTrace, True)
			pPriceTable.Clear()
		End Try

	End Sub



	

End Class
