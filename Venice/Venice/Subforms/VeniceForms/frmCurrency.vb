' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmCurrency.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals


''' <summary>
''' Class frmCurrency
''' </summary>
Public Class frmCurrency

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmCurrency"/> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The LBL currency code
    ''' </summary>
  Friend WithEvents lblCurrencyCode As System.Windows.Forms.Label
    ''' <summary>
    ''' The LBL currency description
    ''' </summary>
  Friend WithEvents lblCurrencyDescription As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit audit ID
    ''' </summary>
  Friend WithEvents editAuditID As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The edit currency code
    ''' </summary>
  Friend WithEvents editCurrencyCode As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The edit currency description
    ''' </summary>
  Friend WithEvents editCurrencyDescription As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The BTN nav first
    ''' </summary>
  Friend WithEvents btnNavFirst As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN nav prev
    ''' </summary>
  Friend WithEvents btnNavPrev As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN nav next
    ''' </summary>
  Friend WithEvents btnNavNext As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN last
    ''' </summary>
  Friend WithEvents btnLast As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN add
    ''' </summary>
  Friend WithEvents btnAdd As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN delete
    ''' </summary>
  Friend WithEvents btnDelete As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN cancel
    ''' </summary>
  Friend WithEvents btnCancel As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN save
    ''' </summary>
  Friend WithEvents btnSave As System.Windows.Forms.Button
    ''' <summary>
    ''' The combo_ select currency code
    ''' </summary>
  Friend WithEvents Combo_SelectCurrencyCode As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The panel1
    ''' </summary>
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The label1
    ''' </summary>
  Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The group box1
    ''' </summary>
  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The root menu
    ''' </summary>
	Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
    ''' <summary>
    ''' The combo_ cash instrument
    ''' </summary>
	Friend WithEvents Combo_CashInstrument As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label4
    ''' </summary>
	Friend WithEvents Label4 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ notional instrument
    ''' </summary>
	Friend WithEvents Combo_NotionalInstrument As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label2
    ''' </summary>
	Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label3
    ''' </summary>
	Friend WithEvents Label3 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label5
    ''' </summary>
  Friend WithEvents Label5 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label6
    ''' </summary>
  Friend WithEvents Label6 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ default brokerage expense
    ''' </summary>
  Friend WithEvents Combo_DefaultBrokerageExpense As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label7
    ''' </summary>
  Friend WithEvents Label7 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label8
    ''' </summary>
  Friend WithEvents Label8 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ default retrocession
    ''' </summary>
  Friend WithEvents Combo_DefaultRetrocession As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label9
    ''' </summary>
  Friend WithEvents Label9 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label10
    ''' </summary>
  Friend WithEvents Label10 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ initial margin instrument
    ''' </summary>
  Friend WithEvents Combo_InitialMarginInstrument As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label11
    ''' </summary>
  Friend WithEvents Label11 As System.Windows.Forms.Label
  Friend WithEvents Label12 As System.Windows.Forms.Label
  Friend WithEvents Combo_MovementCommissionInstrument As System.Windows.Forms.ComboBox
  Friend WithEvents Label13 As System.Windows.Forms.Label
  Friend WithEvents Label14 As System.Windows.Forms.Label
  Friend WithEvents Combo_FXForwardRiskInstrument As System.Windows.Forms.ComboBox
  Friend WithEvents Label15 As System.Windows.Forms.Label
  ''' <summary>
  ''' The BTN close
  ''' </summary>
  Friend WithEvents btnClose As System.Windows.Forms.Button
  ''' <summary>
  ''' Initializes the component.
  ''' </summary>
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.lblCurrencyCode = New System.Windows.Forms.Label
    Me.lblCurrencyDescription = New System.Windows.Forms.Label
    Me.editAuditID = New System.Windows.Forms.TextBox
    Me.editCurrencyCode = New System.Windows.Forms.TextBox
    Me.editCurrencyDescription = New System.Windows.Forms.TextBox
    Me.btnNavFirst = New System.Windows.Forms.Button
    Me.btnNavPrev = New System.Windows.Forms.Button
    Me.btnNavNext = New System.Windows.Forms.Button
    Me.btnLast = New System.Windows.Forms.Button
    Me.btnAdd = New System.Windows.Forms.Button
    Me.btnDelete = New System.Windows.Forms.Button
    Me.btnCancel = New System.Windows.Forms.Button
    Me.btnSave = New System.Windows.Forms.Button
    Me.Combo_SelectCurrencyCode = New System.Windows.Forms.ComboBox
    Me.Panel1 = New System.Windows.Forms.Panel
    Me.Label1 = New System.Windows.Forms.Label
    Me.GroupBox1 = New System.Windows.Forms.GroupBox
    Me.btnClose = New System.Windows.Forms.Button
    Me.RootMenu = New System.Windows.Forms.MenuStrip
    Me.Combo_CashInstrument = New System.Windows.Forms.ComboBox
    Me.Label4 = New System.Windows.Forms.Label
    Me.Combo_NotionalInstrument = New System.Windows.Forms.ComboBox
    Me.Label2 = New System.Windows.Forms.Label
    Me.Label3 = New System.Windows.Forms.Label
    Me.Label5 = New System.Windows.Forms.Label
    Me.Label6 = New System.Windows.Forms.Label
    Me.Combo_DefaultBrokerageExpense = New System.Windows.Forms.ComboBox
    Me.Label7 = New System.Windows.Forms.Label
    Me.Label8 = New System.Windows.Forms.Label
    Me.Combo_DefaultRetrocession = New System.Windows.Forms.ComboBox
    Me.Label9 = New System.Windows.Forms.Label
    Me.Label10 = New System.Windows.Forms.Label
    Me.Combo_InitialMarginInstrument = New System.Windows.Forms.ComboBox
    Me.Label11 = New System.Windows.Forms.Label
    Me.Label12 = New System.Windows.Forms.Label
    Me.Combo_MovementCommissionInstrument = New System.Windows.Forms.ComboBox
    Me.Label13 = New System.Windows.Forms.Label
    Me.Label14 = New System.Windows.Forms.Label
    Me.Combo_FXForwardRiskInstrument = New System.Windows.Forms.ComboBox
    Me.Label15 = New System.Windows.Forms.Label
    Me.Panel1.SuspendLayout()
    Me.SuspendLayout()
    '
    'lblCurrencyCode
    '
    Me.lblCurrencyCode.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.lblCurrencyCode.Location = New System.Drawing.Point(8, 74)
    Me.lblCurrencyCode.Name = "lblCurrencyCode"
    Me.lblCurrencyCode.Size = New System.Drawing.Size(100, 23)
    Me.lblCurrencyCode.TabIndex = 39
    Me.lblCurrencyCode.Text = "Currency Code"
    '
    'lblCurrencyDescription
    '
    Me.lblCurrencyDescription.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.lblCurrencyDescription.Location = New System.Drawing.Point(8, 100)
    Me.lblCurrencyDescription.Name = "lblCurrencyDescription"
    Me.lblCurrencyDescription.Size = New System.Drawing.Size(100, 23)
    Me.lblCurrencyDescription.TabIndex = 40
    Me.lblCurrencyDescription.Text = "Description"
    '
    'editAuditID
    '
    Me.editAuditID.Enabled = False
    Me.editAuditID.Location = New System.Drawing.Point(451, 30)
    Me.editAuditID.Name = "editAuditID"
    Me.editAuditID.Size = New System.Drawing.Size(64, 20)
    Me.editAuditID.TabIndex = 1
    '
    'editCurrencyCode
    '
    Me.editCurrencyCode.Location = New System.Drawing.Point(120, 74)
    Me.editCurrencyCode.Name = "editCurrencyCode"
    Me.editCurrencyCode.Size = New System.Drawing.Size(395, 20)
    Me.editCurrencyCode.TabIndex = 2
    '
    'editCurrencyDescription
    '
    Me.editCurrencyDescription.Location = New System.Drawing.Point(120, 100)
    Me.editCurrencyDescription.Name = "editCurrencyDescription"
    Me.editCurrencyDescription.Size = New System.Drawing.Size(395, 20)
    Me.editCurrencyDescription.TabIndex = 3
    '
    'btnNavFirst
    '
    Me.btnNavFirst.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNavFirst.Location = New System.Drawing.Point(8, 8)
    Me.btnNavFirst.Name = "btnNavFirst"
    Me.btnNavFirst.Size = New System.Drawing.Size(40, 28)
    Me.btnNavFirst.TabIndex = 0
    Me.btnNavFirst.Text = "<<"
    '
    'btnNavPrev
    '
    Me.btnNavPrev.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNavPrev.Location = New System.Drawing.Point(49, 8)
    Me.btnNavPrev.Name = "btnNavPrev"
    Me.btnNavPrev.Size = New System.Drawing.Size(35, 28)
    Me.btnNavPrev.TabIndex = 1
    Me.btnNavPrev.Text = "<"
    '
    'btnNavNext
    '
    Me.btnNavNext.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNavNext.Location = New System.Drawing.Point(88, 8)
    Me.btnNavNext.Name = "btnNavNext"
    Me.btnNavNext.Size = New System.Drawing.Size(35, 28)
    Me.btnNavNext.TabIndex = 2
    Me.btnNavNext.Text = ">"
    '
    'btnLast
    '
    Me.btnLast.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnLast.Location = New System.Drawing.Point(124, 8)
    Me.btnLast.Name = "btnLast"
    Me.btnLast.Size = New System.Drawing.Size(40, 28)
    Me.btnLast.TabIndex = 3
    Me.btnLast.Text = ">>"
    '
    'btnAdd
    '
    Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnAdd.Location = New System.Drawing.Point(176, 8)
    Me.btnAdd.Name = "btnAdd"
    Me.btnAdd.Size = New System.Drawing.Size(75, 28)
    Me.btnAdd.TabIndex = 4
    Me.btnAdd.Text = "&New"
    '
    'btnDelete
    '
    Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnDelete.Location = New System.Drawing.Point(177, 550)
    Me.btnDelete.Name = "btnDelete"
    Me.btnDelete.Size = New System.Drawing.Size(75, 26)
    Me.btnDelete.TabIndex = 13
    Me.btnDelete.Text = "&Delete"
    '
    'btnCancel
    '
    Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnCancel.Location = New System.Drawing.Point(261, 550)
    Me.btnCancel.Name = "btnCancel"
    Me.btnCancel.Size = New System.Drawing.Size(75, 26)
    Me.btnCancel.TabIndex = 14
    Me.btnCancel.Text = "&Cancel"
    '
    'btnSave
    '
    Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnSave.Location = New System.Drawing.Point(89, 550)
    Me.btnSave.Name = "btnSave"
    Me.btnSave.Size = New System.Drawing.Size(75, 26)
    Me.btnSave.TabIndex = 12
    Me.btnSave.Text = "&Save"
    '
    'Combo_SelectCurrencyCode
    '
    Me.Combo_SelectCurrencyCode.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectCurrencyCode.Location = New System.Drawing.Point(120, 30)
    Me.Combo_SelectCurrencyCode.Name = "Combo_SelectCurrencyCode"
    Me.Combo_SelectCurrencyCode.Size = New System.Drawing.Size(325, 21)
    Me.Combo_SelectCurrencyCode.TabIndex = 0
    '
    'Panel1
    '
    Me.Panel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Panel1.Controls.Add(Me.btnNavFirst)
    Me.Panel1.Controls.Add(Me.btnNavPrev)
    Me.Panel1.Controls.Add(Me.btnNavNext)
    Me.Panel1.Controls.Add(Me.btnLast)
    Me.Panel1.Controls.Add(Me.btnAdd)
    Me.Panel1.Location = New System.Drawing.Point(125, 494)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(260, 46)
    Me.Panel1.TabIndex = 11
    '
    'Label1
    '
    Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label1.Location = New System.Drawing.Point(8, 30)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(100, 23)
    Me.Label1.TabIndex = 18
    Me.Label1.Text = "Select"
    '
    'GroupBox1
    '
    Me.GroupBox1.Location = New System.Drawing.Point(8, 62)
    Me.GroupBox1.Name = "GroupBox1"
    Me.GroupBox1.Size = New System.Drawing.Size(507, 4)
    Me.GroupBox1.TabIndex = 77
    Me.GroupBox1.TabStop = False
    Me.GroupBox1.Text = "GroupBox1"
    '
    'btnClose
    '
    Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnClose.Location = New System.Drawing.Point(345, 550)
    Me.btnClose.Name = "btnClose"
    Me.btnClose.Size = New System.Drawing.Size(75, 26)
    Me.btnClose.TabIndex = 15
    Me.btnClose.Text = "&Close"
    '
    'RootMenu
    '
    Me.RootMenu.Location = New System.Drawing.Point(0, 0)
    Me.RootMenu.Name = "RootMenu"
    Me.RootMenu.Size = New System.Drawing.Size(523, 24)
    Me.RootMenu.TabIndex = 17
    Me.RootMenu.Text = " "
    '
    'Combo_CashInstrument
    '
    Me.Combo_CashInstrument.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_CashInstrument.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_CashInstrument.Location = New System.Drawing.Point(120, 126)
    Me.Combo_CashInstrument.Name = "Combo_CashInstrument"
    Me.Combo_CashInstrument.Size = New System.Drawing.Size(395, 21)
    Me.Combo_CashInstrument.TabIndex = 4
    '
    'Label4
    '
    Me.Label4.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label4.Location = New System.Drawing.Point(8, 129)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(100, 20)
    Me.Label4.TabIndex = 87
    Me.Label4.Text = "Cash Instrument"
    '
    'Combo_NotionalInstrument
    '
    Me.Combo_NotionalInstrument.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_NotionalInstrument.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_NotionalInstrument.Location = New System.Drawing.Point(120, 169)
    Me.Combo_NotionalInstrument.Name = "Combo_NotionalInstrument"
    Me.Combo_NotionalInstrument.Size = New System.Drawing.Size(395, 21)
    Me.Combo_NotionalInstrument.TabIndex = 5
    '
    'Label2
    '
    Me.Label2.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label2.Location = New System.Drawing.Point(8, 172)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(100, 37)
    Me.Label2.TabIndex = 89
    Me.Label2.Text = "Futures notional Cash Instrument"
    '
    'Label3
    '
    Me.Label3.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label3.Location = New System.Drawing.Point(123, 150)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(392, 14)
    Me.Label3.TabIndex = 90
    Me.Label3.Text = "Instrument that represents cash transactions or considerations."
    '
    'Label5
    '
    Me.Label5.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label5.Location = New System.Drawing.Point(123, 193)
    Me.Label5.Name = "Label5"
    Me.Label5.Size = New System.Drawing.Size(400, 32)
    Me.Label5.TabIndex = 91
    Me.Label5.Text = "Instrument that represents the Notional consideration value of transactions in Fu" & _
        "tures and CFD style Instruments."
    '
    'Label6
    '
    Me.Label6.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label6.Location = New System.Drawing.Point(123, 252)
    Me.Label6.Name = "Label6"
    Me.Label6.Size = New System.Drawing.Size(400, 27)
    Me.Label6.TabIndex = 94
    Me.Label6.Text = "Instrument used to book fees on Futures style instruments via the 'Update Transac" & _
        "tions' Form."
    '
    'Combo_DefaultBrokerageExpense
    '
    Me.Combo_DefaultBrokerageExpense.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_DefaultBrokerageExpense.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_DefaultBrokerageExpense.Location = New System.Drawing.Point(120, 228)
    Me.Combo_DefaultBrokerageExpense.Name = "Combo_DefaultBrokerageExpense"
    Me.Combo_DefaultBrokerageExpense.Size = New System.Drawing.Size(395, 21)
    Me.Combo_DefaultBrokerageExpense.TabIndex = 6
    '
    'Label7
    '
    Me.Label7.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label7.Location = New System.Drawing.Point(8, 231)
    Me.Label7.Name = "Label7"
    Me.Label7.Size = New System.Drawing.Size(100, 37)
    Me.Label7.TabIndex = 93
    Me.Label7.Text = "Default Brokerage Expense"
    '
    'Label8
    '
    Me.Label8.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label8.Location = New System.Drawing.Point(123, 306)
    Me.Label8.Name = "Label8"
    Me.Label8.Size = New System.Drawing.Size(400, 23)
    Me.Label8.TabIndex = 97
    Me.Label8.Text = "Instrument used to book retrocessions on from the Fund Reconciliation (EOD) form." & _
        ""
    '
    'Combo_DefaultRetrocession
    '
    Me.Combo_DefaultRetrocession.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_DefaultRetrocession.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_DefaultRetrocession.Location = New System.Drawing.Point(120, 282)
    Me.Combo_DefaultRetrocession.Name = "Combo_DefaultRetrocession"
    Me.Combo_DefaultRetrocession.Size = New System.Drawing.Size(395, 21)
    Me.Combo_DefaultRetrocession.TabIndex = 7
    '
    'Label9
    '
    Me.Label9.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label9.Location = New System.Drawing.Point(8, 285)
    Me.Label9.Name = "Label9"
    Me.Label9.Size = New System.Drawing.Size(100, 32)
    Me.Label9.TabIndex = 96
    Me.Label9.Text = "Default Retrocession Instrument (Fee)"
    '
    'Label10
    '
    Me.Label10.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label10.Location = New System.Drawing.Point(123, 356)
    Me.Label10.Name = "Label10"
    Me.Label10.Size = New System.Drawing.Size(400, 23)
    Me.Label10.TabIndex = 100
    Me.Label10.Text = "Instrument used to book Initial Margin deposits on from the Fund Reconciliation (" & _
        "EOD) form."
    '
    'Combo_InitialMarginInstrument
    '
    Me.Combo_InitialMarginInstrument.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_InitialMarginInstrument.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_InitialMarginInstrument.Location = New System.Drawing.Point(120, 332)
    Me.Combo_InitialMarginInstrument.Name = "Combo_InitialMarginInstrument"
    Me.Combo_InitialMarginInstrument.Size = New System.Drawing.Size(395, 21)
    Me.Combo_InitialMarginInstrument.TabIndex = 8
    '
    'Label11
    '
    Me.Label11.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label11.Location = New System.Drawing.Point(8, 335)
    Me.Label11.Name = "Label11"
    Me.Label11.Size = New System.Drawing.Size(100, 32)
    Me.Label11.TabIndex = 99
    Me.Label11.Text = "Default Initial Margin (Deposit) Instrument"
    '
    'Label12
    '
    Me.Label12.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label12.Location = New System.Drawing.Point(123, 406)
    Me.Label12.Name = "Label12"
    Me.Label12.Size = New System.Drawing.Size(400, 23)
    Me.Label12.TabIndex = 103
    Me.Label12.Text = "Instrument used to book Movement Commission on from the Fund Reconciliation (EOD)" & _
        " form."
    '
    'Combo_MovementCommissionInstrument
    '
    Me.Combo_MovementCommissionInstrument.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_MovementCommissionInstrument.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_MovementCommissionInstrument.Location = New System.Drawing.Point(120, 382)
    Me.Combo_MovementCommissionInstrument.Name = "Combo_MovementCommissionInstrument"
    Me.Combo_MovementCommissionInstrument.Size = New System.Drawing.Size(395, 21)
    Me.Combo_MovementCommissionInstrument.TabIndex = 9
    '
    'Label13
    '
    Me.Label13.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label13.Location = New System.Drawing.Point(8, 385)
    Me.Label13.Name = "Label13"
    Me.Label13.Size = New System.Drawing.Size(109, 32)
    Me.Label13.TabIndex = 102
    Me.Label13.Text = "Default Movement Commission Instrument"
    '
    'Label14
    '
    Me.Label14.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label14.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label14.Location = New System.Drawing.Point(123, 456)
    Me.Label14.Name = "Label14"
    Me.Label14.Size = New System.Drawing.Size(400, 23)
    Me.Label14.TabIndex = 106
    Me.Label14.Text = "Separate Instrument used to display Risk on Forward FX trades. (Inst. price shoul" & _
        "d = 1.0)"
    '
    'Combo_FXForwardRiskInstrument
    '
    Me.Combo_FXForwardRiskInstrument.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_FXForwardRiskInstrument.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_FXForwardRiskInstrument.Location = New System.Drawing.Point(120, 432)
    Me.Combo_FXForwardRiskInstrument.Name = "Combo_FXForwardRiskInstrument"
    Me.Combo_FXForwardRiskInstrument.Size = New System.Drawing.Size(395, 21)
    Me.Combo_FXForwardRiskInstrument.TabIndex = 10
    '
    'Label15
    '
    Me.Label15.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label15.Location = New System.Drawing.Point(8, 435)
    Me.Label15.Name = "Label15"
    Me.Label15.Size = New System.Drawing.Size(109, 32)
    Me.Label15.TabIndex = 105
    Me.Label15.Text = "FX Forward Risk Instrument"
    '
    'frmCurrency
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.CancelButton = Me.btnCancel
    Me.ClientSize = New System.Drawing.Size(523, 595)
    Me.Controls.Add(Me.Label14)
    Me.Controls.Add(Me.Combo_FXForwardRiskInstrument)
    Me.Controls.Add(Me.Label15)
    Me.Controls.Add(Me.Label12)
    Me.Controls.Add(Me.Combo_MovementCommissionInstrument)
    Me.Controls.Add(Me.Label13)
    Me.Controls.Add(Me.Label10)
    Me.Controls.Add(Me.Combo_InitialMarginInstrument)
    Me.Controls.Add(Me.Label11)
    Me.Controls.Add(Me.Label8)
    Me.Controls.Add(Me.Combo_DefaultRetrocession)
    Me.Controls.Add(Me.Label9)
    Me.Controls.Add(Me.Label6)
    Me.Controls.Add(Me.Combo_DefaultBrokerageExpense)
    Me.Controls.Add(Me.Label7)
    Me.Controls.Add(Me.Label5)
    Me.Controls.Add(Me.Label3)
    Me.Controls.Add(Me.Combo_NotionalInstrument)
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.Combo_CashInstrument)
    Me.Controls.Add(Me.Label4)
    Me.Controls.Add(Me.RootMenu)
    Me.Controls.Add(Me.btnClose)
    Me.Controls.Add(Me.GroupBox1)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.Panel1)
    Me.Controls.Add(Me.Combo_SelectCurrencyCode)
    Me.Controls.Add(Me.btnSave)
    Me.Controls.Add(Me.editAuditID)
    Me.Controls.Add(Me.editCurrencyCode)
    Me.Controls.Add(Me.editCurrencyDescription)
    Me.Controls.Add(Me.lblCurrencyCode)
    Me.Controls.Add(Me.lblCurrencyDescription)
    Me.Controls.Add(Me.btnDelete)
    Me.Controls.Add(Me.btnCancel)
    Me.Name = "frmCurrency"
    Me.Text = "Add/Edit Currency"
    Me.Panel1.ResumeLayout(False)
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "


  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
  Private WithEvents _MainForm As VeniceMain

  ' Form ToolTip
    ''' <summary>
  ''' Control to provide tooltip functionality.
    ''' </summary>
  Private FormTooltip As New ToolTip()

  ' Form Menu


  ' Form Constants, specific to the table being updated.

    ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
    ''' </summary>
  Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

  ' Form Locals, initialised on 'New' defining what standard data items to use
    ''' <summary>
  ''' Variable holding the form's data table name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_TABLENAME As String
    ''' <summary>
  ''' Variable holding the form's data adaptor name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_ADAPTORNAME As String
    ''' <summary>
  ''' Variable holding the form's dataset name. taken from the "ThisStandardDataset" object.
    ''' </summary>
  Private THIS_DATASETNAME As String


  ' The standard ChangeID for this form. e.g. tblCurrency
    ''' <summary>
  ''' The standard ChangeID for this form.
    ''' </summary>
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

    ''' <summary>
  ''' Principal control used for selecting items on this form. 
    ''' </summary>
  Private THIS_FORM_SelectingCombo As ComboBox
    ''' <summary>
  ''' Control to select after proessing the "New" button.
    ''' </summary>
  Private THIS_FORM_NewMoveToControl As Control

  ' Form Specific Order fields
    ''' <summary>
  ''' Field Name to show in the Select combo.
    ''' </summary>
  Private THIS_FORM_SelectBy As String
    ''' <summary>
  ''' Field Name to order the Select combo by.
    ''' </summary>
  Private THIS_FORM_OrderBy As String

    ''' <summary>
  ''' The field name that holds the unique identifier for each entry. e.g. FundID for the tblFund table.
    ''' </summary>
  Private THIS_FORM_ValueMember As String

  ' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
  Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
  Private THIS_FORM_FormID As VeniceFormID

  ' Data Structures

    ''' <summary>
    ''' My dataset
    ''' </summary>
  Private myDataset As RenaissanceDataClass.DSCurrency    ' Form Specific !!!!
    ''' <summary>
    ''' My table
    ''' </summary>
  Private myTable As RenaissanceDataClass.DSCurrency.tblCurrencyDataTable
    ''' <summary>
    ''' My connection
    ''' </summary>
  Private myConnection As SqlConnection
    ''' <summary>
    ''' My adaptor
    ''' </summary>
  Private myAdaptor As SqlDataAdapter

    ''' <summary>
    ''' The this standard dataset
    ''' </summary>
  Private ThisStandardDataset As RenaissanceGlobals.StandardDataset


  ' Active Element.

    ''' <summary>
    ''' The sorted rows
    ''' </summary>
  Private SortedRows() As DataRow
    ''' <summary>
    ''' The select by sorted rows
    ''' </summary>
  Private SelectBySortedRows() As DataRow
    ''' <summary>
    ''' The this data row
    ''' </summary>
  Private thisDataRow As RenaissanceDataClass.DSCurrency.tblCurrencyRow   ' Form Specific !!!!
    ''' <summary>
    ''' The this audit ID
    ''' </summary>
  Private thisAuditID As Integer
    ''' <summary>
    ''' The this position
    ''' </summary>
  Private thisPosition As Integer
    ''' <summary>
    ''' The _ is over cancel button
    ''' </summary>
  Private _IsOverCancelButton As Boolean
    ''' <summary>
    ''' The _ in use
    ''' </summary>
  Private _InUse As Boolean

  ' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
  Private FormIsValid As Boolean
    ''' <summary>
    ''' The form changed
    ''' </summary>
  Private FormChanged As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
  Private _FormOpenFailed As Boolean
    ''' <summary>
    ''' The in paint
    ''' </summary>
  Private InPaint As Boolean
    ''' <summary>
    ''' The add new record
    ''' </summary>
  Private AddNewRecord As Boolean

  ' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
  Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
  Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
  Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
  Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
  Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
    ' Public property to return handle to the 'Main' Venice form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
  Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
    ' Public property maintaining a value indicating if the cursor is over the 'Cancel'
    ' Button on this form.
    ' This property is specifically designed for use by the field formating Event functions
    ' In order that they do not impose format restrictions if the user is about to click the 
    ' 'Cancel' button.
    '
    Get
      Return _IsOverCancelButton
    End Get
    Set(ByVal Value As Boolean)
      _IsOverCancelButton = Value
    End Set
  End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
  Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
    Get
      Return InPaint
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
    Get
      Return _InUse
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmCurrency"/> class.
    ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  Public Sub New(ByVal pMainForm As VeniceMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()

    _MainForm = pMainForm
    AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

    _FormOpenFailed = False
    _InUse = True

    ' ******************************************************
    ' Form Specific Settings :
    ' ******************************************************

    THIS_FORM_SelectingCombo = Me.Combo_SelectCurrencyCode
    THIS_FORM_NewMoveToControl = Me.editCurrencyCode

    ' Default Select and Order fields.

    THIS_FORM_SelectBy = "CurrencyCode"
    THIS_FORM_OrderBy = "CurrencyCode"

    THIS_FORM_ValueMember = "CurrencyID"

    ' Form Permissioning :-

    THIS_FORM_PermissionArea = Me.Name
    THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

    ' 'This' form ID

    THIS_FORM_FormID = VeniceFormID.frmCurrency

    ' This form's dataset type.

    ThisStandardDataset = RenaissanceStandardDatasets.tblCurrency ' This Defines the Form Data !!! 

    ' Format Event Handlers for form controls

    AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler
    AddHandler editCurrencyCode.LostFocus, AddressOf MainForm.Text_NotNull
    AddHandler editCurrencyDescription.LostFocus, AddressOf MainForm.Text_NotNull

    ' Form Control Changed events
    AddHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
    AddHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

    AddHandler editCurrencyCode.TextChanged, AddressOf Me.FormControlChanged
    AddHandler editCurrencyDescription.TextChanged, AddressOf Me.FormControlChanged

    AddHandler Combo_CashInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_CashInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_CashInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_CashInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_CashInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged

    AddHandler Combo_NotionalInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_NotionalInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_NotionalInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_NotionalInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_NotionalInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged

    AddHandler Combo_DefaultBrokerageExpense.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_DefaultBrokerageExpense.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_DefaultBrokerageExpense.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_DefaultBrokerageExpense.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_DefaultBrokerageExpense.SelectedValueChanged, AddressOf Me.FormControlChanged

    AddHandler Combo_DefaultRetrocession.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_DefaultRetrocession.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_DefaultRetrocession.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_DefaultRetrocession.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_DefaultRetrocession.SelectedValueChanged, AddressOf Me.FormControlChanged

    AddHandler Combo_InitialMarginInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_InitialMarginInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_InitialMarginInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_InitialMarginInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_InitialMarginInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged

    AddHandler Combo_MovementCommissionInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_MovementCommissionInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_MovementCommissionInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_MovementCommissionInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_MovementCommissionInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged

    AddHandler Combo_FXForwardRiskInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_FXForwardRiskInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_FXForwardRiskInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_FXForwardRiskInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_FXForwardRiskInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged

    ' Set up the ToolTip
    MainForm.SetFormToolTip(Me, FormTooltip)

    ' ******************************************************
    ' End Form Specific.
    ' ******************************************************

    ' Data object names standard to this Form type.

    THIS_TABLENAME = ThisStandardDataset.TableName
    THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
    THIS_DATASETNAME = ThisStandardDataset.DatasetName

    THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

    ' Establish / Retrieve data objects for this form.

    myConnection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)
    myAdaptor = MainForm.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, VENICE_CONNECTION, THIS_TABLENAME)
    myDataset = MainForm.Load_Table(ThisStandardDataset, False)
    myTable = myDataset.Tables(0)

    MainForm.BuildStandardFormMenu(Me.RootMenu, myDataset.Tables(0), AddressOf SelectMenuEvent, AddressOf OrderMenuEvent, AddressOf AuditReportMenuEvent)

  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
  Public Sub ResetForm() Implements StandardVeniceForm.ResetForm
    ' ********************************************************************************
    '
    '
    ' ********************************************************************************

    THIS_FORM_SelectBy = "CurrencyCode"
    THIS_FORM_OrderBy = "CurrencyCode"

    Call Form_Load(Me, New System.EventArgs)
  End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
  Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
    ' ********************************************************************************
    '
    '
    ' ********************************************************************************

    ALWAYS_CLOSE_THIS_FORM = True
    Me.Close()
  End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    ' ********************************************************************************
    '
    '
    ' ********************************************************************************

    Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
    _FormOpenFailed = False
    _InUse = True

    Try

      ' Initialise Data structures. Connection, Adaptor and Dataset.

      If Not (MainForm Is Nothing) Then
        FormIsValid = True
      Else
        MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        FormIsValid = False
        _FormOpenFailed = True
        Exit Sub
      End If

      If (myConnection Is Nothing) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

        FormIsValid = False
        _FormOpenFailed = True
        Exit Sub
      End If

      If (myAdaptor Is Nothing) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

        FormIsValid = False
        _FormOpenFailed = True
        Exit Sub
      End If

      If (myDataset Is Nothing) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

        FormIsValid = False
        _FormOpenFailed = True
        Exit Sub
      End If

    Catch ex As Exception
    End Try

    Try

      ' Initialse form

      InPaint = True
      IsOverCancelButton = False

      ' Check User permissions
      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

        FormIsValid = False
        _FormOpenFailed = True
        Exit Sub
      End If

      ' Build Sorted data list from which this form operates

      Call SetSortedRows()

      Call SetInstrumentCombos()

      ' Display initial record.

      thisPosition = 0
      If THIS_FORM_SelectingCombo.Items.Count > 0 Then
        Me.THIS_FORM_SelectingCombo.SelectedIndex = 0
        thisDataRow = SortedRows(thisPosition)
        Call GetFormData(thisDataRow)
      Else
        Me.THIS_FORM_SelectingCombo.SelectedIndex = (-1)
        Call GetFormData(Nothing)
      End If

    Catch ex As Exception

      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Initialising Form (Form.Load).", ex.StackTrace, True)

    Finally

      InPaint = False

    End Try

  End Sub

    ''' <summary>
    ''' Handles the Closing event of the frmCurrency control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
  Private Sub frmCurrency_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    ' ********************************************************************************
    '
    '
    ' ********************************************************************************

    Dim HideForm As Boolean

    ' Hide or Close this form ?
    ' All depends on how many of this form type are Open or in Cache...

    _InUse = False

    If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
      HideForm = False
    Else
      If (FormChanged = True) Then
        Call SetFormData()
      End If

      HideForm = True
      If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
        HideForm = False
      End If
    End If

    If HideForm = True Then
      MainForm.HideInFormsCollection(Me)
      Me.Hide() ' NPP Fix

      e.Cancel = True
    Else
      Try
        MainForm.RemoveFromFormsCollection(Me)
        RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

        RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler
        RemoveHandler editCurrencyCode.LostFocus, AddressOf MainForm.Text_NotNull
        RemoveHandler editCurrencyDescription.LostFocus, AddressOf MainForm.Text_NotNull

        RemoveHandler Combo_CashInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_CashInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_CashInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_CashInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_CashInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged

        RemoveHandler Combo_NotionalInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_NotionalInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_NotionalInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_NotionalInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_NotionalInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged

        RemoveHandler Combo_DefaultBrokerageExpense.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_DefaultBrokerageExpense.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_DefaultBrokerageExpense.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_DefaultBrokerageExpense.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_DefaultBrokerageExpense.SelectedValueChanged, AddressOf Me.FormControlChanged

        RemoveHandler Combo_DefaultRetrocession.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_DefaultRetrocession.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_DefaultRetrocession.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_DefaultRetrocession.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_DefaultRetrocession.SelectedValueChanged, AddressOf Me.FormControlChanged

        RemoveHandler Combo_InitialMarginInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_InitialMarginInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_InitialMarginInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_InitialMarginInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_InitialMarginInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged

        RemoveHandler Combo_MovementCommissionInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_MovementCommissionInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_MovementCommissionInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_MovementCommissionInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_MovementCommissionInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged

        RemoveHandler Combo_FXForwardRiskInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_FXForwardRiskInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_FXForwardRiskInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_FXForwardRiskInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_FXForwardRiskInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged

        RemoveHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
        RemoveHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        RemoveHandler editCurrencyCode.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler editCurrencyDescription.TextChanged, AddressOf Me.FormControlChanged

      Catch ex As Exception
      End Try
    End If

  End Sub


#End Region

  ' Routine to handle changes / updates to tables by this and other windows.
  ' If this, or any other, form posts a change to a table, then it will invoke an update event 
  ' detailing what tables have been altered.
  ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
  ' the 'VeniceAutoUpdate' event of the main Venice form.
  ' Each form may them react as appropriate to changes in any table that might impact it.
  '
    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    Dim OrgInPaint As Boolean
    Dim KnowledgeDateChanged As Boolean
    Dim SetButtonStatus_Flag As Boolean
    Dim RefreshForm As Boolean = False

    If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

    OrgInPaint = InPaint
    InPaint = True
    KnowledgeDateChanged = False
    SetButtonStatus_Flag = False

    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
      KnowledgeDateChanged = True
      RefreshForm = True
    End If

    ' ****************************************************************
    ' Check for changes relevant to this form
    ' ****************************************************************

    ' Changes to the tblInstrument table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrument) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Call SetInstrumentCombos()
    End If

    ' Changes to the KnowledgeDate :-
    If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
      SetButtonStatus_Flag = True
    End If

    ' Changes to the tblUserPermissions table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

      ' Check ongoing permissions.

      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

        FormIsValid = False
        Me.Close()
        Exit Sub
      End If

      SetButtonStatus_Flag = True

    End If


    ' ****************************************************************
    ' Changes to the Main FORM table :-
    ' ****************************************************************

    If (e.TableChanged(THIS_FORM_ChangeID) = True) Or KnowledgeDateChanged Then
      RefreshForm = True

      ' Re-Set Controls etc.
      Call SetSortedRows()

      ' Move again to the correct item
      thisPosition = Get_Position(thisAuditID)

      ' Validate current position.
      If thisPosition >= 0 And thisPosition < Me.SortedRows.GetLength(0) Then
        thisDataRow = Me.SortedRows(thisPosition)
      Else
        If (Me.SortedRows.GetLength(0) > 0) And (AddNewRecord = False) Then
          thisPosition = 0
          thisDataRow = Me.SortedRows(thisPosition)
          FormChanged = False
        Else
          thisDataRow = Nothing
        End If
      End If

      ' Set SelectingCombo Index.
      If (Me.THIS_FORM_SelectingCombo.Items.Count <= 0) Then
        Try
          Me.THIS_FORM_SelectingCombo.SelectedIndex = -1
        Catch ex As Exception
        End Try
      ElseIf (thisPosition < 0) Then
        Try
          MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
        Catch ex As Exception
        End Try
      Else
        Try
          Me.THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
        Catch ex As Exception
        End Try
      End If

    End If

    ' ****************************************************************
    ' Repaint if not currently in Edit Mode
    '
    ' ****************************************************************

    InPaint = OrgInPaint

    ' Retrieve form data if appropriate, i.e. this data item is not already being edited.
    If (RefreshForm = True) AndAlso (FormChanged = False) AndAlso (AddNewRecord = False) Then
      GetFormData(thisDataRow) ' Includes a call to 'SetButtonStatus()'
    Else
      If SetButtonStatus_Flag Then
        Call SetButtonStatus()
      End If
    End If

  End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

  ' Build Sorted list from the Source dataset and update the Select Combo
    ''' <summary>
    ''' Sets the sorted rows.
    ''' </summary>
  Private Sub SetSortedRows()

    Dim OrgInPaint As Boolean

    Dim thisrow As DataRow
    Dim thisDrowDownWidth As Integer
    Dim SizingBitmap As Bitmap
    Dim SizingGraphics As Graphics

    ' Form Specific Selection Combo :-
    If (THIS_FORM_SelectingCombo Is Nothing) Then Exit Sub

    ' Code is pretty Generic form here on...

    ' Set paint local so that changes to the selection combo do not trigger form updates.

    OrgInPaint = InPaint
    InPaint = True

    ' Get selected Row sets, or exit if no data is present.
    If myDataset Is Nothing Then
      ReDim SortedRows(0)
      ReDim SelectBySortedRows(0)
    Else
      SortedRows = myTable.Select("RN >= 0", THIS_FORM_OrderBy)
      SelectBySortedRows = myTable.Select("RN >= 0", THIS_FORM_SelectBy)
    End If

    ' Set Combo data source
    THIS_FORM_SelectingCombo.DataSource = SortedRows
    THIS_FORM_SelectingCombo.DisplayMember = THIS_FORM_SelectBy
    THIS_FORM_SelectingCombo.ValueMember = THIS_FORM_ValueMember

    ' Ensure that the Selection Combo's DrowDownWidth is wide enough to take the required strings

    thisDrowDownWidth = THIS_FORM_SelectingCombo.Width
    For Each thisrow In SortedRows

      ' Compute the string dimensions in the given font
      SizingBitmap = New Bitmap(1, 1, System.Drawing.Imaging.PixelFormat.Format32bppArgb)
      SizingGraphics = Graphics.FromImage(SizingBitmap)
      Dim stringSize As SizeF = SizingGraphics.MeasureString(thisrow(THIS_FORM_SelectBy).ToString, THIS_FORM_SelectingCombo.Font)
      If (stringSize.Width) > thisDrowDownWidth Then
        thisDrowDownWidth = CInt(stringSize.Width)
      End If
    Next

    THIS_FORM_SelectingCombo.DropDownWidth = thisDrowDownWidth

    InPaint = OrgInPaint
  End Sub


  ' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
  Private Sub CheckPermissions()

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

  End Sub

  ' Flag changes to form controls, Event Associations made in 'New' routine.
    ''' <summary>
  ''' Basic event handler, called when a form value is changed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    If InPaint = False Then
      If (Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord) Then
        FormChanged = True
        Me.btnSave.Enabled = True
        Me.btnCancel.Enabled = True
      End If
    End If

  End Sub

    ''' <summary>
    ''' Selects the menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
  Public Sub SelectMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
    ' Event handler for the Select Menu
    ' Event association is made during the dynamic menu creation

    THIS_FORM_SelectBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

    Call SetSortedRows()

  End Sub

    ''' <summary>
    ''' Orders the menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
  Public Sub OrderMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
    ' Event handler for the Order Menu
    ' Event association is made during the dynamic menu creation

    THIS_FORM_OrderBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

    Call SetSortedRows()

  End Sub


    ''' <summary>
    ''' Audits the report menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
  Public Sub AuditReportMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
    ' Event handler for the Audit Report Menu
    ' Event association is made during the dynamic menu creation

    Try
      Select Case CInt(CType(sender, System.Windows.Forms.ToolStripMenuItem).Tag)
        Case 0 ' This Record
          If (Me.AddNewRecord = False) AndAlso (thisAuditID >= 0) Then
            Run_AuditReport_ThisRecord(MainForm, ThisStandardDataset, thisAuditID, MainForm.VeniceProgressBar)
          End If

        Case 1 ' All Records
          Run_AuditReport_ALLRecords(MainForm, ThisStandardDataset, MainForm.VeniceProgressBar)

      End Select
    Catch ex As Exception
      Call MainForm.LogError(Me.Name & ", AuditReportMenuEvent()", 0, ex.Message, "Error calling Audit report.", ex.StackTrace, True)
    End Try

  End Sub




#End Region

#Region " Set Form Combos (Form Specific Code) "

    ''' <summary>
    ''' Sets the instrument combos.
    ''' </summary>
  Private Sub SetInstrumentCombos()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_CashInstrument, _
    RenaissanceStandardDatasets.tblInstrument, _
    "InstrumentDescription", _
    "InstrumentID", _
    "InstrumentType=" & CInt(RenaissanceGlobals.InstrumentTypes.Cash).ToString, False, True, True, 0, "") ' 

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_NotionalInstrument, _
    RenaissanceStandardDatasets.tblInstrument, _
    "InstrumentDescription", _
    "InstrumentID", _
    "InstrumentType=" & CInt(RenaissanceGlobals.InstrumentTypes.Notional).ToString, False, True, True, 0, "") ' 

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_DefaultBrokerageExpense, _
    RenaissanceStandardDatasets.tblInstrument, _
    "InstrumentDescription", _
    "InstrumentID", _
    "InstrumentType=" & CInt(RenaissanceGlobals.InstrumentTypes.Expense).ToString, False, True, True, 0, "") ' 

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_DefaultRetrocession, _
    RenaissanceStandardDatasets.tblInstrument, _
    "InstrumentDescription", _
    "InstrumentID", _
    "InstrumentType=" & CInt(RenaissanceGlobals.InstrumentTypes.Fee).ToString, False, True, True, 0, "") ' 

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_InitialMarginInstrument, _
    RenaissanceStandardDatasets.tblInstrument, _
    "InstrumentDescription", _
    "InstrumentID", _
    "InstrumentType=" & CInt(RenaissanceGlobals.InstrumentTypes.Margin).ToString, False, True, True, 0, "") ' 

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_MovementCommissionInstrument, _
    RenaissanceStandardDatasets.tblInstrument, _
    "InstrumentDescription", _
    "InstrumentID", _
    "InstrumentType=" & CInt(RenaissanceGlobals.InstrumentTypes.Expense).ToString, False, True, True, 0, "") ' 

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_FXForwardRiskInstrument, _
    RenaissanceStandardDatasets.tblInstrument, _
    "InstrumentDescription", _
    "InstrumentID", _
    "true", False, True, True, 0, "") ' 

  End Sub

#End Region

#Region " Get & Set Form Data / SetButton / ValidateForm / btnCancel Events (Form Specific Code) "

    ''' <summary>
    ''' Gets the form data.
    ''' </summary>
    ''' <param name="ThisRow">The this row.</param>
	Private Sub GetFormData(ByRef ThisRow As RenaissanceDataClass.DSCurrency.tblCurrencyRow)
		' Routine to populate form Controls given a data row.
		' Form is cleared for invalid Datarows or Invalid Form.

		Dim OrgInpaint As Boolean

		' Set 'Paint' flag to prevent event actions caused by changing the value of form fields.
		OrgInpaint = InPaint
		InPaint = True


		If (ThisRow Is Nothing) Or (FormIsValid = False) Or (Me.InUse = False) Then
			' Bad / New Datarow - Clear Form.

			thisAuditID = (-1)

			Me.editAuditID.Text = ""

			Me.editCurrencyCode.Text = ""
			Me.editCurrencyDescription.Text = ""

			If Me.Combo_CashInstrument.Items.Count > 0 Then
				Combo_CashInstrument.SelectedIndex = 0
			End If

			If Me.Combo_NotionalInstrument.Items.Count > 0 Then
				Combo_NotionalInstrument.SelectedIndex = 0
      End If

      If Me.Combo_DefaultBrokerageExpense.Items.Count > 0 Then
        Combo_DefaultBrokerageExpense.SelectedIndex = 0
      End If

      If Me.Combo_DefaultRetrocession.Items.Count > 0 Then
        Combo_DefaultRetrocession.SelectedIndex = 0
      End If

      If Me.Combo_InitialMarginInstrument.Items.Count > 0 Then
        Combo_InitialMarginInstrument.SelectedIndex = 0
      End If

      If Me.Combo_MovementCommissionInstrument.Items.Count > 0 Then
        Combo_MovementCommissionInstrument.SelectedIndex = 0
      End If

      If Me.Combo_FXForwardRiskInstrument.Items.Count > 0 Then
        Combo_FXForwardRiskInstrument.SelectedIndex = 0
      End If

      If AddNewRecord = True Then
        Me.btnCancel.Enabled = True
        Me.THIS_FORM_SelectingCombo.Enabled = False
      Else
        Me.btnCancel.Enabled = False
        Me.THIS_FORM_SelectingCombo.Enabled = True
      End If

		Else

			' Populate Form with given data.
			Try
				thisAuditID = thisDataRow.AuditID

				Me.editAuditID.Text = thisDataRow.AuditID.ToString

				Me.editCurrencyCode.Text = thisDataRow.CurrencyCode
				Me.editCurrencyDescription.Text = thisDataRow.CurrencyDescription

				If Me.Combo_CashInstrument.Items.Count > 0 Then
					Combo_CashInstrument.SelectedValue = thisDataRow.CurrencyCashInstrumentID
				End If

				If Me.Combo_NotionalInstrument.Items.Count > 0 Then
					Combo_NotionalInstrument.SelectedValue = thisDataRow.CurrencyFuturesNominalInstrumentID
				End If

        If Me.Combo_DefaultBrokerageExpense.Items.Count > 0 Then
          Combo_DefaultBrokerageExpense.SelectedValue = thisDataRow.DefaultBrokerageExpense
        End If

        If Me.Combo_DefaultRetrocession.Items.Count > 0 Then
          Combo_DefaultRetrocession.SelectedValue = thisDataRow.DefaultRetrocessionInstrument
        End If

        If Me.Combo_InitialMarginInstrument.Items.Count > 0 Then
          Combo_InitialMarginInstrument.SelectedValue = thisDataRow.DefaultInitialMarginInstrument
        End If

        If Me.Combo_MovementCommissionInstrument.Items.Count > 0 Then
          Combo_MovementCommissionInstrument.SelectedValue = thisDataRow.DefaultMovementCommissionInstrument
        End If

        If Me.Combo_FXForwardRiskInstrument.Items.Count > 0 Then
          Combo_FXForwardRiskInstrument.SelectedValue = thisDataRow.FXForwardRiskInstrument
        End If

        AddNewRecord = False
				' MainForm.SetComboSelectionLengths(Me)

				Me.btnCancel.Enabled = False
				Me.THIS_FORM_SelectingCombo.Enabled = True

			Catch ex As Exception

				Call MainForm.LogError(Me.Name & ", GetFormData", 0, ex.Message, "Error Showing Data", ex.StackTrace, True)
				Call GetFormData(Nothing)

			End Try

		End If

		' Allow Field events to trigger before 'InPaint' Is re-set. 
		' (Should) Prevent Validation errors during Form Draw.
		Application.DoEvents()

		' Restore 'Paint' flag.
		InPaint = OrgInpaint
		FormChanged = False
		Me.btnSave.Enabled = False

		' As it says on the can....
		Call SetButtonStatus()

	End Sub

    ''' <summary>
    ''' Sets the form data.
    ''' </summary>
    ''' <param name="pConfirm">if set to <c>true</c> [p confirm].</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	Private Function SetFormData(Optional ByVal pConfirm As Boolean = True) As Boolean
		' *************************************************************
		'
		' *************************************************************
		Dim ErrMessage As String
		Dim ErrFlag As Boolean
		Dim ErrStack As String
		Dim ProtectedItem As Boolean = False

		ErrMessage = ""
		ErrStack = ""

		' *************************************************************
		' Appropriate Save permission :-
		' *************************************************************

		If (Me.HasUpdatePermission = False) And ((Me.HasInsertPermission = False) Or (Me.AddNewRecord = False)) Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "You do not have permission to save this record.", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' *************************************************************
		' If Save button is disabled then should not be able to save, exit silently.
		' *************************************************************
		If Me.btnSave.Enabled = False Then
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' *************************************************************
		' KnowledgeDate OK :-
		' *************************************************************

		If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Changes to Venice are not allowed when the Knowledgedate is not set to `NOW`", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' Confirm Save, if required.

		If (pConfirm = True) Then
			If MessageBox.Show("Save Changes ?", "Save Changes ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
				Return True
				Exit Function
			End If
		End If

		' *************************************************************
		' Procedure to Save the current form information.
		' *************************************************************

		Dim StatusString As String
		Dim LogString As String
		Dim UpdateRows(0) As DataRow
		Dim Position As Integer

		If (FormChanged = False) Or (FormIsValid = False) Then
			Return False
			Exit Function
		End If

		' Validation
		StatusString = ""
		If ValidateForm(StatusString) = False Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Validation Error." & vbCrLf & StatusString, "", True)
			Return False
			Exit Function
		End If

		' Check current position in the table.
		Position = Get_Position(thisAuditID)

		' Allow for new or missing ID.
		If Position < 0 Then
			If (AddNewRecord = True) OrElse (SortedRows.Length <= 0) Then
				thisDataRow = myTable.NewRow
				LogString = "Add: "
				AddNewRecord = True
			Else
				Return False
				Exit Function
			End If
		Else
			' Row found OK.
			LogString = "Edit : AuditID = " & thisAuditID.ToString

			thisDataRow = Me.SortedRows(Position)

			' Check for Protected Currency
			If Not thisDataRow.IsCurrencyIDNull Then
				If (thisDataRow.CurrencyID > 0) AndAlso (System.Enum.IsDefined(GetType(RenaissanceGlobals.Currency), thisDataRow.CurrencyID) = True) Then
					ProtectedItem = True
				End If
			End If

		End If

		' Set 'Paint' flag.
		InPaint = True

		' *************************************************************
		' Lock the Data Table, to prevent update conflicts.
		' *************************************************************
		SyncLock myTable

			' Initiate Edit,
			thisDataRow.BeginEdit()

			' Set Data Values
			If (ProtectedItem = True) AndAlso (AddNewRecord = False) AndAlso (thisDataRow.CurrencyCode <> Me.editCurrencyCode.Text) Then
				MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "This Currency is protected, Currency Code may not be changed.", "", True)
			Else
				thisDataRow.CurrencyCode = Me.editCurrencyCode.Text
			End If

			If (thisAuditID > 0) AndAlso (Me.Combo_CashInstrument.SelectedIndex >= 0) AndAlso (IsNumeric(Combo_CashInstrument.SelectedValue)) Then
				thisDataRow.CurrencyCashInstrumentID = Combo_CashInstrument.SelectedValue
			Else
				thisDataRow.CurrencyCashInstrumentID = 0
			End If

			If (thisAuditID > 0) AndAlso (Me.Combo_NotionalInstrument.SelectedIndex >= 0) AndAlso (IsNumeric(Combo_NotionalInstrument.SelectedValue)) Then
				thisDataRow.CurrencyFuturesNominalInstrumentID = Combo_NotionalInstrument.SelectedValue
			Else
				thisDataRow.CurrencyFuturesNominalInstrumentID = 0
			End If

      If (thisAuditID > 0) AndAlso (Me.Combo_DefaultBrokerageExpense.SelectedIndex >= 0) AndAlso (IsNumeric(Combo_DefaultBrokerageExpense.SelectedValue)) Then
        thisDataRow.DefaultBrokerageExpense = Combo_DefaultBrokerageExpense.SelectedValue
      Else
        thisDataRow.DefaultBrokerageExpense = 0
      End If

      If (thisAuditID > 0) AndAlso (Me.Combo_DefaultRetrocession.SelectedIndex >= 0) AndAlso (IsNumeric(Combo_DefaultRetrocession.SelectedValue)) Then
        thisDataRow.DefaultRetrocessionInstrument = Combo_DefaultRetrocession.SelectedValue
      Else
        thisDataRow.DefaultRetrocessionInstrument = 0
      End If

      If (thisAuditID > 0) AndAlso (Me.Combo_InitialMarginInstrument.SelectedIndex >= 0) AndAlso (IsNumeric(Combo_InitialMarginInstrument.SelectedValue)) Then
        thisDataRow.DefaultInitialMarginInstrument = Combo_InitialMarginInstrument.SelectedValue
      Else
        thisDataRow.DefaultInitialMarginInstrument = 0
      End If

      If (thisAuditID > 0) AndAlso (Me.Combo_MovementCommissionInstrument.SelectedIndex >= 0) AndAlso (IsNumeric(Combo_MovementCommissionInstrument.SelectedValue)) Then
        thisDataRow.DefaultMovementCommissionInstrument = Combo_MovementCommissionInstrument.SelectedValue
      Else
        thisDataRow.DefaultMovementCommissionInstrument = 0
      End If

      If (thisAuditID > 0) AndAlso (Me.Combo_FXForwardRiskInstrument.SelectedIndex >= 0) AndAlso (IsNumeric(Combo_FXForwardRiskInstrument.SelectedValue)) Then
        thisDataRow.FXForwardRiskInstrument = Combo_FXForwardRiskInstrument.SelectedValue
      Else
        thisDataRow.FXForwardRiskInstrument = 0
      End If

      LogString &= ", CashInstId =" & thisDataRow.CurrencyCashInstrumentID.ToString & ", NominalInstId =" & thisDataRow.CurrencyFuturesNominalInstrumentID.ToString & ", CurrencyCode = " & thisDataRow.CurrencyCode

			thisDataRow.CurrencyDescription = Me.editCurrencyDescription.Text
			LogString &= ", CurrencyDescription = " & thisDataRow.CurrencyDescription

			thisDataRow.EndEdit()
			InPaint = False

			' Add and Update DataRow. 

			ErrFlag = False

			If AddNewRecord = True Then
				Try
					myTable.Rows.Add(thisDataRow)
				Catch ex As Exception
					ErrFlag = True
					ErrMessage = ex.Message
					ErrStack = ex.StackTrace
				End Try
			End If

			UpdateRows(0) = thisDataRow

			' Post Additions / Updates to the underlying table :-
			Dim temp As Integer
			Try
				If (ErrFlag = False) Then
					myAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
					myAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

					temp = MainForm.AdaptorUpdate(Me.Name, myAdaptor, UpdateRows)
				End If

			Catch ex As Exception

				ErrMessage = ex.Message
				ErrFlag = True
				ErrStack = ex.StackTrace

			End Try

			thisAuditID = thisDataRow.AuditID


		End SyncLock

		' *************************************************************
		' Error report. Not done in the Try-Catch as that would serve to persist the SyncLock.
		' *************************************************************

		If (ErrFlag = True) Then
			Call MainForm.LogError(Me.Name & ", SetFormData", 0, ErrMessage, "Error Saving Data", ErrStack, True)
		End If

		' Finish off

		AddNewRecord = False
		FormChanged = False

		Me.THIS_FORM_SelectingCombo.Enabled = True

		' Propogate changes

    If (thisDataRow IsNot Nothing) AndAlso (thisDataRow.IsAuditIDNull = False) AndAlso (thisDataRow.AuditID > 0) Then
      Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID, thisDataRow.AuditID.ToString), True)
    Else
      Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID), True)
    End If

	End Function

    ''' <summary>
    ''' Sets the button status.
    ''' </summary>
	Private Sub SetButtonStatus()
		' Sets the status of the form controlls appropriate to the current users 
		'permissions and the 'Changed' or 'New' status of the form.

		' No Read permission :-

		If Me.HasReadPermission = False Then
			MainForm.LogError(Me.Name & ", SetButtonStatus()", 0, "", "You do not have Read permission for this Form", "", True)
			Me.Close()
			Exit Sub
		End If

		' Has Insert Permission.
		If Me.HasInsertPermission Then
			Me.btnAdd.Enabled = True
		Else
			Me.btnAdd.Enabled = False
		End If

		' Has Delete permission. 
		If (Me.HasDeletePermission) And (Me.AddNewRecord = False) And (thisPosition >= 0) Then
			Me.btnDelete.Enabled = True
		Else
			Me.btnDelete.Enabled = False
		End If

		If (Me.MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And _
			((Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord)) Then

			Me.editCurrencyCode.Enabled = True
			Me.editCurrencyDescription.Enabled = True
			Me.Combo_CashInstrument.Enabled = True
			Me.Combo_NotionalInstrument.Enabled = True
      Me.Combo_DefaultBrokerageExpense.Enabled = True
      Me.Combo_DefaultRetrocession.Enabled = True
      Me.Combo_InitialMarginInstrument.Enabled = True
      Me.Combo_MovementCommissionInstrument.Enabled = True
      Me.Combo_FXForwardRiskInstrument.Enabled = True

		Else

			Me.editCurrencyCode.Enabled = False
			Me.editCurrencyDescription.Enabled = False
			Me.Combo_CashInstrument.Enabled = False
			Me.Combo_NotionalInstrument.Enabled = False
      Me.Combo_DefaultBrokerageExpense.Enabled = False
      Me.Combo_DefaultRetrocession.Enabled = False
      Me.Combo_InitialMarginInstrument.Enabled = False
      Me.Combo_MovementCommissionInstrument.Enabled = False
      Me.Combo_FXForwardRiskInstrument.Enabled = False

		End If

	End Sub

    ''' <summary>
    ''' Validates the form.
    ''' </summary>
    ''' <param name="pReturnString">The p return string.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	Private Function ValidateForm(ByRef pReturnString As String) As Boolean
		' Form Validation code.
		' 
		' This code should be the final arbiter of what is allowed. no assumptions regarding 
		' prior validation should be made.
		' 
		' This Code is called by the SetFormData routine before position changes.
		'
		Dim RVal As Boolean

		RVal = True
		pReturnString = ""

		' Validate 

		If Me.editCurrencyCode.Text.Length <= 0 Then
			pReturnString = "Currency Code must not be left blank."
			RVal = False
		End If

		If Me.editCurrencyDescription.Text.Length <= 0 Then
			pReturnString = "Currency Description must not be left blank."
			RVal = False
		End If

		Try

			If (Me.Combo_CashInstrument.SelectedIndex >= 0) AndAlso IsNumeric(Me.Combo_CashInstrument.SelectedValue) AndAlso (CInt(Combo_CashInstrument.SelectedValue) > 0) Then
				' Check Currency of Cash Instrument matches this Currency record.

				If (CInt(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, CInt(Combo_CashInstrument.SelectedValue), "InstrumentCurrencyID")) <> thisAuditID) Then

					pReturnString = "Cash Instrument currency must match this Currency."
					RVal = False

				End If

			End If

			If (Me.Combo_NotionalInstrument.SelectedIndex >= 0) AndAlso IsNumeric(Me.Combo_NotionalInstrument.SelectedValue) AndAlso (CInt(Combo_NotionalInstrument.SelectedValue) > 0) Then
				' Check Currency of Cash Instrument matches this Currency record.

				If (CInt(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, CInt(Combo_NotionalInstrument.SelectedValue), "InstrumentCurrencyID")) <> thisAuditID) Then

					pReturnString = "Notional Cash Instrument currency must match this Currency."
					RVal = False

				End If

			End If

		Catch ex As Exception
			pReturnString = ex.Message
			RVal = False
		End Try


		Return RVal

	End Function

    ''' <summary>
    ''' Handles the MouseEnter event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnCancel_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseEnter
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = True
	End Sub

    ''' <summary>
    ''' Handles the MouseLeave event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnCancel_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseLeave
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = False
	End Sub


#End Region

#Region " Navigation Code / GetPosition() (Generic Code) "

    ''' <summary>
    ''' Handles the SelectComboChanged event of the Combo control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_SelectComboChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' Selection Combo. SelectedItem changed.
    '

    ' Don't react to changes made in paint routines etc.
    If InPaint = True Then Exit Sub

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    ' Find the correct data row, then show it...
    thisPosition = THIS_FORM_SelectingCombo.SelectedIndex

    If (thisPosition >= 0) Then
      thisDataRow = Me.SortedRows(thisPosition)
    Else
      thisDataRow = Nothing
    End If

    Call GetFormData(thisDataRow)

  End Sub


    ''' <summary>
    ''' Handles the Click event of the btnNavPrev control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnNavPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavPrev.Click
    ' 'Previous' Button.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    If thisPosition > 0 Then thisPosition -= 1
    If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
      thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
    End If

    THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

    Exit Sub

    If myTable.Rows.Count > thisPosition Then
      thisDataRow = myTable.Rows(thisPosition)
      Call GetFormData(thisDataRow)
    Else
      Call GetFormData(Nothing)
    End If

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnNavNext control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnNavNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavNext.Click
    ' 'Next' Button.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    If thisPosition < (myTable.Rows.Count - 1) Then thisPosition += 1
    If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
      thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
    End If

    THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

    Exit Sub

    If myTable.Rows.Count > thisPosition Then
      thisDataRow = myTable.Rows(thisPosition)
      Call GetFormData(thisDataRow)
    Else
      Call GetFormData(Nothing)
    End If

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnNavFirst control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnNavFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavFirst.Click
    ' 'First' Button.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    thisPosition = 0
    If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
      thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
    End If

    If (THIS_FORM_SelectingCombo.SelectedIndex = thisPosition) Then
      Combo_SelectComboChanged(THIS_FORM_SelectingCombo, New EventArgs)
    Else
      THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
    End If

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnLast control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLast.Click
    ' 'Last' Button.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    thisPosition = THIS_FORM_SelectingCombo.Items.Count - 1

    THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

  End Sub

    ''' <summary>
    ''' Get_s the position.
    ''' </summary>
    ''' <param name="pAuditID">The p audit ID.</param>
    ''' <returns>System.Int32.</returns>
  Private Function Get_Position(ByVal pAuditID As Integer) As Integer
    ' Returns the position in the current 'SortedRows' array of the entry corresponding to the given 
    ' AudidID.

    Dim MinIndex As Integer
    Dim MaxIndex As Integer
    Dim CurrentMin As Integer
    Dim CurrentMax As Integer
    Dim searchDataRow As DataRow

    Try
      ' SortedRows exists ?

      If SortedRows Is Nothing Then
        Return (-1)
        Exit Function
      End If

      ' Return (-1) if there are no rows in the 'SortedRows'

      If (SortedRows.GetLength(0) <= 0) Then
        Return (-1)
        Exit Function
      End If


      ' Use a modified Search moving outwards from the last returned value to locate the required value.
      ' Reflecting the fact that for the most part One looks for closely located records.

      MinIndex = 0
      MaxIndex = SortedRows.GetLength(0) - 1


      ' Check First and Last records (Incase 'First' or 'Last' was pressed).

      searchDataRow = SortedRows(MinIndex)

      If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
        Return MinIndex
        Exit Function
      End If

      searchDataRow = SortedRows(MaxIndex)
      If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
        Return MaxIndex
        Exit Function
      End If

      ' now search outwards from the last returned value.

      If (Me.thisPosition >= MinIndex) And (Me.thisPosition <= MaxIndex) Then
        CurrentMin = thisPosition
        CurrentMax = CurrentMin + 1
      Else
        CurrentMin = CInt((MinIndex + MaxIndex) / 2)
        CurrentMax = CurrentMin + 1
      End If

      While (CurrentMin >= MinIndex) Or (CurrentMax <= MaxIndex)
        If (CurrentMin >= MinIndex) Then
          searchDataRow = SortedRows(CurrentMin)

          If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
            Return CurrentMin
            Exit Function
          End If

          CurrentMin -= 1
        End If

        If (CurrentMax <= MaxIndex) Then
          searchDataRow = SortedRows(CurrentMax)

          If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
            Return CurrentMax
            Exit Function
          End If

          CurrentMax += 1
        End If

      End While

      Return (-1)
      Exit Function

    Catch ex As Exception
      MainForm.LogError(Me.Name & ", Get_Position()", LOG_LEVELS.Warning, ex.Message, "", ex.StackTrace, True)
      Return (-1)
      Exit Function
    End Try

  End Function

#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

    ''' <summary>
    ''' Handles the Click event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    ' Cancel Changes, redisplay form.

    FormChanged = False
    AddNewRecord = False

    Me.THIS_FORM_SelectingCombo.Enabled = True

    If (thisPosition >= 0) And (thisPosition < Me.SortedRows.GetLength(0)) Then
      thisDataRow = Me.SortedRows(thisPosition)
      Call GetFormData(thisDataRow)
    Else
      Call btnNavFirst_Click(Me, New System.EventArgs)
    End If

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnSave control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
    ' Save Changes, if any, without prompting.

    If (FormChanged = True) Then
      Call SetFormData(False)
    End If

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnDelete control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
    Dim Position As Integer
    Dim NextAuditID As Integer

    If (AddNewRecord = True) Then
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Sub
    End If

    ' No Appropriate Save permission :-

    If (Me.HasDeletePermission = False) Then
      MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "You do not have permission to Delete this record.", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Sub
    End If

    ' *************************************************************
    ' KnowledgeDate OK :-
    ' *************************************************************

    If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
      MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "Changes to Venice are not allowed when the Knowledgedate is not set to `NOW`", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Sub
    End If

    ' *************************************************************
    ' Confirm :-
    ' *************************************************************
    If MsgBox("Are you Sure ?", MsgBoxStyle.OKCancel, "Confirm Delete") = MsgBoxResult.Cancel Then
      Exit Sub
    End If

    ' Check Data position.

    Position = Get_Position(thisAuditID)

    If Position < 0 Then Exit Sub

    ' Check Referential Integrity 
    If CheckReferentialIntegrity(_MainForm, Me.SortedRows(Position)) = False Then
      MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "Deleting this record would voilate referential integrity.", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Sub
    End If

    ' Resolve row to show after deleting this one.

    NextAuditID = (-1)
    If (Position + 1) < Me.SortedRows.GetLength(0) Then
      NextAuditID = CInt(Me.SortedRows(Position + 1)("AuditID"))
    ElseIf (Position - 1) >= 0 Then
      NextAuditID = CInt(Me.SortedRows(Position - 1)("AuditID"))
    Else
      NextAuditID = (-1)
    End If

    ' Delete this row

    Try
      Me.SortedRows(Position).Delete()

      MainForm.AdaptorUpdate(Me.Name, myAdaptor, myTable)

    Catch ex As Exception

      Call MainForm.LogError(Me.Name & ", Delete()", LOG_LEVELS.Error, ex.Message, "Error Deleting Record.", ex.StackTrace, True)
    End Try

    ' Tidy Up.

    FormChanged = False

    thisAuditID = NextAuditID
		Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID), True)

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnAdd control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
    ' Prepare form to Add a new record.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    thisPosition = -1
    InPaint = True
    MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
    InPaint = False

    GetFormData(Nothing)
    AddNewRecord = True
    Me.btnCancel.Enabled = True
    Me.THIS_FORM_SelectingCombo.Enabled = False

    Call SetButtonStatus()

    THIS_FORM_NewMoveToControl.Focus()

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnClose control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
    ' Close Form

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    Me.Close()

  End Sub

#End Region

#Region " Bug hunting "

  ' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
  ' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
  ' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

    ''' <summary>
    ''' Called when [row updating].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

    ''' <summary>
    ''' Called when [row updated].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

    ''' <summary>
    ''' Called when [row fill error].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
    Dim a As Integer

    a = 1
  End Sub

#End Region


End Class
