﻿' ***********************************************************************
' Assembly         : Venice
' Author           : Nicholas Pennington
' Created          : 08-15-2013
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="frmUpdateTransactions.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals
Imports RenaissanceUtilities.DatePeriodFunctions

Imports RenaissanceDataClass
Imports C1.Win.C1FlexGrid
Imports TCPServer_Common

''' <summary>
''' Class frmUpdateTransactions
''' </summary>
Public Class frmUpdateTransactions

  Inherits System.Windows.Forms.Form
  Implements StandardVeniceForm

#Region " Windows Form Designer generated code "

  ''' <summary>
  ''' Prevents a default instance of the <see cref="frmUpdateTransactions"/> class from being created.
  ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  ''' <summary>
  ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
  ''' </summary>
  ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  ''' <summary>
  ''' The components
  ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  ''' <summary>
  ''' The combo_ transaction fund
  ''' </summary>
  Friend WithEvents Combo_TransactionFund As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The label_ cpty is fund
  ''' </summary>
  Friend WithEvents label_CptyIsFund As System.Windows.Forms.Label
  ''' <summary>
  ''' The combo_ transaction instrument
  ''' </summary>
  Friend WithEvents Combo_TransactionInstrument As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The label2
  ''' </summary>
  Friend WithEvents Label2 As System.Windows.Forms.Label
  ''' <summary>
  ''' The label3
  ''' </summary>
  Friend WithEvents Label3 As System.Windows.Forms.Label
  ''' <summary>
  ''' The date_ value date
  ''' </summary>
  Friend WithEvents Date_ValueDate As System.Windows.Forms.DateTimePicker
  ''' <summary>
  ''' The combo_ value date operator
  ''' </summary>
  Friend WithEvents Combo_ValueDateOperator As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The grid_ transactions
  ''' </summary>
  Friend WithEvents Grid_Transactions As C1.Win.C1FlexGrid.C1FlexGrid
  ''' <summary>
  ''' The combo_ field select1
  ''' </summary>
  Friend WithEvents Combo_FieldSelect1 As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The combo_ select1_ operator
  ''' </summary>
  Friend WithEvents Combo_Select1_Operator As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The combo_ select1_ value
  ''' </summary>
  Friend WithEvents Combo_Select1_Value As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The root menu
  ''' </summary>
  Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
  ''' <summary>
  ''' The form_ status strip
  ''' </summary>
  Friend WithEvents Form_StatusStrip As System.Windows.Forms.StatusStrip
  ''' <summary>
  ''' The form_ progress bar
  ''' </summary>
  Friend WithEvents Form_ProgressBar As System.Windows.Forms.ToolStripProgressBar
  ''' <summary>
  ''' The label_ status
  ''' </summary>
  Friend WithEvents Label_Status As System.Windows.Forms.ToolStripStatusLabel
  ''' <summary>
  ''' The combo_ value date operator2
  ''' </summary>
  Friend WithEvents Combo_ValueDateOperator2 As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The date_ value date2
  ''' </summary>
  Friend WithEvents Date_ValueDate2 As System.Windows.Forms.DateTimePicker
  ''' <summary>
  ''' The combo_ trade status group
  ''' </summary>
  Friend WithEvents Combo_TradeStatusGroup As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The label1
  ''' </summary>
  Friend WithEvents Label1 As System.Windows.Forms.Label
  ''' <summary>
  ''' The panel1
  ''' </summary>
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
  ''' <summary>
  ''' The label_ order type
  ''' </summary>
  Friend WithEvents Label_OrderType As System.Windows.Forms.Label
  ''' <summary>
  ''' The label_ instrument name
  ''' </summary>
  Friend WithEvents Label_InstrumentName As System.Windows.Forms.Label
  ''' <summary>
  ''' The label4
  ''' </summary>
  Friend WithEvents Label4 As System.Windows.Forms.Label
  ''' <summary>
  ''' The edit_ fees
  ''' </summary>
  Friend WithEvents edit_Fees As RenaissanceControls.NumericTextBox
  ''' <summary>
  ''' The label_ fees
  ''' </summary>
  Friend WithEvents Label_Fees As System.Windows.Forms.Label
  ''' <summary>
  ''' The edit_ price
  ''' </summary>
  Friend WithEvents edit_Price As RenaissanceControls.NumericTextBox
  ''' <summary>
  ''' The label_ price
  ''' </summary>
  Friend WithEvents Label_Price As System.Windows.Forms.Label
  ''' <summary>
  ''' The edit_ amount
  ''' </summary>
  Friend WithEvents edit_Amount As RenaissanceControls.NumericTextBox
  ''' <summary>
  ''' The combo_ settlement currency
  ''' </summary>
  Friend WithEvents Combo_SettlementCurrency As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The label_ transaction parent ID
  ''' </summary>
  Friend WithEvents Label_TransactionParentID As System.Windows.Forms.Label
  ''' <summary>
  ''' The date_ trade settlement date
  ''' </summary>
  Friend WithEvents Date_TradeSettlementDate As System.Windows.Forms.DateTimePicker
  ''' <summary>
  ''' The label_ settlement date
  ''' </summary>
  Friend WithEvents Label_SettlementDate As System.Windows.Forms.Label
  ''' <summary>
  ''' The date_ trade value date
  ''' </summary>
  Friend WithEvents Date_TradeValueDate As System.Windows.Forms.DateTimePicker
  ''' <summary>
  ''' The label_ trade date
  ''' </summary>
  Friend WithEvents Label_TradeDate As System.Windows.Forms.Label
  ''' <summary>
  ''' The BTN save
  ''' </summary>
  Friend WithEvents btnSave As System.Windows.Forms.Button
  ''' <summary>
  ''' The BTN cancel
  ''' </summary>
  Friend WithEvents btnCancel As System.Windows.Forms.Button
  ''' <summary>
  ''' The check_ futures
  ''' </summary>
  Friend WithEvents Check_Futures As System.Windows.Forms.CheckBox
  ''' <summary>
  ''' The check_ options
  ''' </summary>
  Friend WithEvents Check_Options As System.Windows.Forms.CheckBox
  ''' <summary>
  ''' The label5
  ''' </summary>
  Friend WithEvents Label5 As System.Windows.Forms.Label
  ''' <summary>
  ''' The combo_ trade status
  ''' </summary>
  Friend WithEvents Combo_TradeStatus As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The label6
  ''' </summary>
  Friend WithEvents Label6 As System.Windows.Forms.Label
  ''' <summary>
  ''' The combo_ fee instrument
  ''' </summary>
  Friend WithEvents Combo_FeeInstrument As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' The check_ book fees separately
  ''' </summary>
  Friend WithEvents Check_BookFeesSeparately As System.Windows.Forms.CheckBox
  ''' <summary>
  ''' The label_ multiplier
  ''' </summary>
  Friend WithEvents Label_Multiplier As System.Windows.Forms.Label
  ''' <summary>
  ''' The date_ fee settlement
  ''' </summary>
  Friend WithEvents Date_FeeSettlement As System.Windows.Forms.DateTimePicker
  ''' <summary>
  ''' The label_ fee settlement
  ''' </summary>
  Friend WithEvents Label_FeeSettlement As System.Windows.Forms.Label
  Friend WithEvents Check_FXForwards As System.Windows.Forms.CheckBox
  Friend WithEvents Check_FX As System.Windows.Forms.CheckBox
  Friend WithEvents Check_Bonds As System.Windows.Forms.CheckBox
  Friend WithEvents Button_SaveToExecution As System.Windows.Forms.Button
  Friend WithEvents ContextMenu_Trades As System.Windows.Forms.ContextMenuStrip
  Friend WithEvents Menu_SaveToExecutionFile As System.Windows.Forms.ToolStripMenuItem
  ''' <summary>
  ''' The combo_ trade status operator
  ''' </summary>
  Friend WithEvents Combo_TradeStatusOperator As System.Windows.Forms.ComboBox
  ''' <summary>
  ''' Initializes the component.
  ''' </summary>
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container
    Me.Grid_Transactions = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.ContextMenu_Trades = New System.Windows.Forms.ContextMenuStrip(Me.components)
    Me.Menu_SaveToExecutionFile = New System.Windows.Forms.ToolStripMenuItem
    Me.Combo_TransactionFund = New System.Windows.Forms.ComboBox
    Me.label_CptyIsFund = New System.Windows.Forms.Label
    Me.Combo_TransactionInstrument = New System.Windows.Forms.ComboBox
    Me.Label2 = New System.Windows.Forms.Label
    Me.Label3 = New System.Windows.Forms.Label
    Me.Date_ValueDate = New System.Windows.Forms.DateTimePicker
    Me.Combo_ValueDateOperator = New System.Windows.Forms.ComboBox
    Me.Combo_Select1_Operator = New System.Windows.Forms.ComboBox
    Me.Combo_FieldSelect1 = New System.Windows.Forms.ComboBox
    Me.Combo_Select1_Value = New System.Windows.Forms.ComboBox
    Me.RootMenu = New System.Windows.Forms.MenuStrip
    Me.Form_StatusStrip = New System.Windows.Forms.StatusStrip
    Me.Form_ProgressBar = New System.Windows.Forms.ToolStripProgressBar
    Me.Label_Status = New System.Windows.Forms.ToolStripStatusLabel
    Me.Combo_ValueDateOperator2 = New System.Windows.Forms.ComboBox
    Me.Date_ValueDate2 = New System.Windows.Forms.DateTimePicker
    Me.Combo_TradeStatusGroup = New System.Windows.Forms.ComboBox
    Me.Label1 = New System.Windows.Forms.Label
    Me.Combo_TradeStatusOperator = New System.Windows.Forms.ComboBox
    Me.Panel1 = New System.Windows.Forms.Panel
    Me.Date_FeeSettlement = New System.Windows.Forms.DateTimePicker
    Me.Label_FeeSettlement = New System.Windows.Forms.Label
    Me.Label_Multiplier = New System.Windows.Forms.Label
    Me.Combo_FeeInstrument = New System.Windows.Forms.ComboBox
    Me.Check_BookFeesSeparately = New System.Windows.Forms.CheckBox
    Me.Combo_TradeStatus = New System.Windows.Forms.ComboBox
    Me.Label6 = New System.Windows.Forms.Label
    Me.btnSave = New System.Windows.Forms.Button
    Me.btnCancel = New System.Windows.Forms.Button
    Me.Label_TransactionParentID = New System.Windows.Forms.Label
    Me.Date_TradeSettlementDate = New System.Windows.Forms.DateTimePicker
    Me.Label_SettlementDate = New System.Windows.Forms.Label
    Me.Date_TradeValueDate = New System.Windows.Forms.DateTimePicker
    Me.Label_TradeDate = New System.Windows.Forms.Label
    Me.Combo_SettlementCurrency = New System.Windows.Forms.ComboBox
    Me.edit_Fees = New RenaissanceControls.NumericTextBox
    Me.Label_Fees = New System.Windows.Forms.Label
    Me.edit_Price = New RenaissanceControls.NumericTextBox
    Me.Label_Price = New System.Windows.Forms.Label
    Me.edit_Amount = New RenaissanceControls.NumericTextBox
    Me.Label_OrderType = New System.Windows.Forms.Label
    Me.Label_InstrumentName = New System.Windows.Forms.Label
    Me.Label4 = New System.Windows.Forms.Label
    Me.Button_SaveToExecution = New System.Windows.Forms.Button
    Me.Check_Futures = New System.Windows.Forms.CheckBox
    Me.Check_Options = New System.Windows.Forms.CheckBox
    Me.Label5 = New System.Windows.Forms.Label
    Me.Check_FXForwards = New System.Windows.Forms.CheckBox
    Me.Check_FX = New System.Windows.Forms.CheckBox
    Me.Check_Bonds = New System.Windows.Forms.CheckBox
    CType(Me.Grid_Transactions, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.ContextMenu_Trades.SuspendLayout()
    Me.Form_StatusStrip.SuspendLayout()
    Me.Panel1.SuspendLayout()
    Me.SuspendLayout()
    '
    'Grid_Transactions
    '
    Me.Grid_Transactions.AllowEditing = False
    Me.Grid_Transactions.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_Transactions.AutoClipboard = True
    Me.Grid_Transactions.CausesValidation = False
    Me.Grid_Transactions.ColumnInfo = "5,1,0,0,0,85,Columns:"
    Me.Grid_Transactions.ContextMenuStrip = Me.ContextMenu_Trades
    Me.Grid_Transactions.EditOptions = C1.Win.C1FlexGrid.EditFlags.None
    Me.Grid_Transactions.KeyActionTab = C1.Win.C1FlexGrid.KeyActionEnum.MoveAcross
    Me.Grid_Transactions.Location = New System.Drawing.Point(4, 201)
    Me.Grid_Transactions.Name = "Grid_Transactions"
    Me.Grid_Transactions.Rows.DefaultSize = 17
    Me.Grid_Transactions.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.ListBox
    Me.Grid_Transactions.Size = New System.Drawing.Size(1004, 263)
    Me.Grid_Transactions.TabIndex = 16
    '
    'ContextMenu_Trades
    '
    Me.ContextMenu_Trades.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_SaveToExecutionFile})
    Me.ContextMenu_Trades.Name = "ContextMenu_Trades"
    Me.ContextMenu_Trades.Size = New System.Drawing.Size(186, 26)
    '
    'Menu_SaveToExecutionFile
    '
    Me.Menu_SaveToExecutionFile.Name = "Menu_SaveToExecutionFile"
    Me.Menu_SaveToExecutionFile.Size = New System.Drawing.Size(185, 22)
    Me.Menu_SaveToExecutionFile.Text = "Save to Execution file"
    '
    'Combo_TransactionFund
    '
    Me.Combo_TransactionFund.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_TransactionFund.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_TransactionFund.Location = New System.Drawing.Point(128, 32)
    Me.Combo_TransactionFund.Name = "Combo_TransactionFund"
    Me.Combo_TransactionFund.Size = New System.Drawing.Size(876, 21)
    Me.Combo_TransactionFund.TabIndex = 0
    '
    'label_CptyIsFund
    '
    Me.label_CptyIsFund.Location = New System.Drawing.Point(16, 36)
    Me.label_CptyIsFund.Name = "label_CptyIsFund"
    Me.label_CptyIsFund.Size = New System.Drawing.Size(104, 16)
    Me.label_CptyIsFund.TabIndex = 82
    Me.label_CptyIsFund.Text = "Fund"
    '
    'Combo_TransactionInstrument
    '
    Me.Combo_TransactionInstrument.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_TransactionInstrument.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_TransactionInstrument.Location = New System.Drawing.Point(128, 60)
    Me.Combo_TransactionInstrument.Name = "Combo_TransactionInstrument"
    Me.Combo_TransactionInstrument.Size = New System.Drawing.Size(876, 21)
    Me.Combo_TransactionInstrument.TabIndex = 1
    '
    'Label2
    '
    Me.Label2.Location = New System.Drawing.Point(16, 64)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(104, 16)
    Me.Label2.TabIndex = 84
    Me.Label2.Text = "Instrument"
    '
    'Label3
    '
    Me.Label3.Location = New System.Drawing.Point(16, 93)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(104, 16)
    Me.Label3.TabIndex = 88
    Me.Label3.Text = "NAV Date"
    '
    'Date_ValueDate
    '
    Me.Date_ValueDate.CustomFormat = "dd MMM yyyy"
    Me.Date_ValueDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
    Me.Date_ValueDate.Location = New System.Drawing.Point(228, 89)
    Me.Date_ValueDate.Name = "Date_ValueDate"
    Me.Date_ValueDate.Size = New System.Drawing.Size(174, 20)
    Me.Date_ValueDate.TabIndex = 3
    '
    'Combo_ValueDateOperator
    '
    Me.Combo_ValueDateOperator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_ValueDateOperator.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_ValueDateOperator.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", ""})
    Me.Combo_ValueDateOperator.Location = New System.Drawing.Point(128, 89)
    Me.Combo_ValueDateOperator.Name = "Combo_ValueDateOperator"
    Me.Combo_ValueDateOperator.Size = New System.Drawing.Size(96, 21)
    Me.Combo_ValueDateOperator.TabIndex = 2
    '
    'Combo_Select1_Operator
    '
    Me.Combo_Select1_Operator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_Select1_Operator.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Select1_Operator.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like"})
    Me.Combo_Select1_Operator.Location = New System.Drawing.Point(272, 148)
    Me.Combo_Select1_Operator.Name = "Combo_Select1_Operator"
    Me.Combo_Select1_Operator.Size = New System.Drawing.Size(96, 21)
    Me.Combo_Select1_Operator.TabIndex = 9
    '
    'Combo_FieldSelect1
    '
    Me.Combo_FieldSelect1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_FieldSelect1.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_FieldSelect1.Location = New System.Drawing.Point(128, 148)
    Me.Combo_FieldSelect1.Name = "Combo_FieldSelect1"
    Me.Combo_FieldSelect1.Size = New System.Drawing.Size(136, 21)
    Me.Combo_FieldSelect1.Sorted = True
    Me.Combo_FieldSelect1.TabIndex = 8
    '
    'Combo_Select1_Value
    '
    Me.Combo_Select1_Value.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Select1_Value.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Select1_Value.Location = New System.Drawing.Point(376, 148)
    Me.Combo_Select1_Value.Name = "Combo_Select1_Value"
    Me.Combo_Select1_Value.Size = New System.Drawing.Size(340, 21)
    Me.Combo_Select1_Value.TabIndex = 10
    '
    'RootMenu
    '
    Me.RootMenu.Location = New System.Drawing.Point(0, 0)
    Me.RootMenu.Name = "RootMenu"
    Me.RootMenu.Size = New System.Drawing.Size(1012, 24)
    Me.RootMenu.TabIndex = 104
    Me.RootMenu.Text = " "
    '
    'Form_StatusStrip
    '
    Me.Form_StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Form_ProgressBar, Me.Label_Status})
    Me.Form_StatusStrip.Location = New System.Drawing.Point(0, 647)
    Me.Form_StatusStrip.Name = "Form_StatusStrip"
    Me.Form_StatusStrip.Size = New System.Drawing.Size(1012, 22)
    Me.Form_StatusStrip.TabIndex = 105
    Me.Form_StatusStrip.Text = " "
    '
    'Form_ProgressBar
    '
    Me.Form_ProgressBar.Maximum = 20
    Me.Form_ProgressBar.Name = "Form_ProgressBar"
    Me.Form_ProgressBar.Size = New System.Drawing.Size(150, 17)
    Me.Form_ProgressBar.Step = 1
    Me.Form_ProgressBar.Visible = False
    '
    'Label_Status
    '
    Me.Label_Status.Name = "Label_Status"
    Me.Label_Status.Size = New System.Drawing.Size(10, 17)
    Me.Label_Status.Text = " "
    '
    'Combo_ValueDateOperator2
    '
    Me.Combo_ValueDateOperator2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_ValueDateOperator2.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_ValueDateOperator2.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", ""})
    Me.Combo_ValueDateOperator2.Location = New System.Drawing.Point(426, 89)
    Me.Combo_ValueDateOperator2.Name = "Combo_ValueDateOperator2"
    Me.Combo_ValueDateOperator2.Size = New System.Drawing.Size(96, 21)
    Me.Combo_ValueDateOperator2.TabIndex = 4
    '
    'Date_ValueDate2
    '
    Me.Date_ValueDate2.CustomFormat = "dd MMM yyyy"
    Me.Date_ValueDate2.Format = System.Windows.Forms.DateTimePickerFormat.Custom
    Me.Date_ValueDate2.Location = New System.Drawing.Point(526, 89)
    Me.Date_ValueDate2.Name = "Date_ValueDate2"
    Me.Date_ValueDate2.Size = New System.Drawing.Size(174, 20)
    Me.Date_ValueDate2.TabIndex = 5
    '
    'Combo_TradeStatusGroup
    '
    Me.Combo_TradeStatusGroup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_TradeStatusGroup.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_TradeStatusGroup.Location = New System.Drawing.Point(228, 119)
    Me.Combo_TradeStatusGroup.Name = "Combo_TradeStatusGroup"
    Me.Combo_TradeStatusGroup.Size = New System.Drawing.Size(776, 21)
    Me.Combo_TradeStatusGroup.TabIndex = 7
    '
    'Label1
    '
    Me.Label1.Location = New System.Drawing.Point(16, 123)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(104, 16)
    Me.Label1.TabIndex = 109
    Me.Label1.Text = "Trade Status Group"
    '
    'Combo_TradeStatusOperator
    '
    Me.Combo_TradeStatusOperator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_TradeStatusOperator.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_TradeStatusOperator.Items.AddRange(New Object() {"", "IN", "NOT IN"})
    Me.Combo_TradeStatusOperator.Location = New System.Drawing.Point(128, 118)
    Me.Combo_TradeStatusOperator.Name = "Combo_TradeStatusOperator"
    Me.Combo_TradeStatusOperator.Size = New System.Drawing.Size(96, 21)
    Me.Combo_TradeStatusOperator.TabIndex = 6
    '
    'Panel1
    '
    Me.Panel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Panel1.Controls.Add(Me.Date_FeeSettlement)
    Me.Panel1.Controls.Add(Me.Label_FeeSettlement)
    Me.Panel1.Controls.Add(Me.Label_Multiplier)
    Me.Panel1.Controls.Add(Me.Combo_FeeInstrument)
    Me.Panel1.Controls.Add(Me.Check_BookFeesSeparately)
    Me.Panel1.Controls.Add(Me.Combo_TradeStatus)
    Me.Panel1.Controls.Add(Me.Label6)
    Me.Panel1.Controls.Add(Me.btnSave)
    Me.Panel1.Controls.Add(Me.btnCancel)
    Me.Panel1.Controls.Add(Me.Label_TransactionParentID)
    Me.Panel1.Controls.Add(Me.Date_TradeSettlementDate)
    Me.Panel1.Controls.Add(Me.Label_SettlementDate)
    Me.Panel1.Controls.Add(Me.Date_TradeValueDate)
    Me.Panel1.Controls.Add(Me.Label_TradeDate)
    Me.Panel1.Controls.Add(Me.Combo_SettlementCurrency)
    Me.Panel1.Controls.Add(Me.edit_Fees)
    Me.Panel1.Controls.Add(Me.Label_Fees)
    Me.Panel1.Controls.Add(Me.edit_Price)
    Me.Panel1.Controls.Add(Me.Label_Price)
    Me.Panel1.Controls.Add(Me.edit_Amount)
    Me.Panel1.Controls.Add(Me.Label_OrderType)
    Me.Panel1.Controls.Add(Me.Label_InstrumentName)
    Me.Panel1.Controls.Add(Me.Label4)
    Me.Panel1.Controls.Add(Me.Button_SaveToExecution)
    Me.Panel1.Location = New System.Drawing.Point(4, 470)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(1004, 172)
    Me.Panel1.TabIndex = 17
    '
    'Date_FeeSettlement
    '
    Me.Date_FeeSettlement.Location = New System.Drawing.Point(758, 107)
    Me.Date_FeeSettlement.Name = "Date_FeeSettlement"
    Me.Date_FeeSettlement.Size = New System.Drawing.Size(184, 20)
    Me.Date_FeeSettlement.TabIndex = 11
    '
    'Label_FeeSettlement
    '
    Me.Label_FeeSettlement.Location = New System.Drawing.Point(616, 111)
    Me.Label_FeeSettlement.Name = "Label_FeeSettlement"
    Me.Label_FeeSettlement.Size = New System.Drawing.Size(136, 16)
    Me.Label_FeeSettlement.TabIndex = 215
    Me.Label_FeeSettlement.Text = "Fee Settlement Date"
    Me.Label_FeeSettlement.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Label_Multiplier
    '
    Me.Label_Multiplier.Location = New System.Drawing.Point(482, 54)
    Me.Label_Multiplier.Name = "Label_Multiplier"
    Me.Label_Multiplier.Size = New System.Drawing.Size(169, 13)
    Me.Label_Multiplier.TabIndex = 213
    Me.Label_Multiplier.Text = "Multiplier"
    Me.Label_Multiplier.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Combo_FeeInstrument
    '
    Me.Combo_FeeInstrument.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_FeeInstrument.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_FeeInstrument.Location = New System.Drawing.Point(453, 81)
    Me.Combo_FeeInstrument.Name = "Combo_FeeInstrument"
    Me.Combo_FeeInstrument.Size = New System.Drawing.Size(535, 21)
    Me.Combo_FeeInstrument.TabIndex = 8
    '
    'Check_BookFeesSeparately
    '
    Me.Check_BookFeesSeparately.Checked = True
    Me.Check_BookFeesSeparately.CheckState = System.Windows.Forms.CheckState.Checked
    Me.Check_BookFeesSeparately.Location = New System.Drawing.Point(333, 82)
    Me.Check_BookFeesSeparately.Name = "Check_BookFeesSeparately"
    Me.Check_BookFeesSeparately.Size = New System.Drawing.Size(114, 18)
    Me.Check_BookFeesSeparately.TabIndex = 7
    Me.Check_BookFeesSeparately.Text = "Book Separately"
    Me.Check_BookFeesSeparately.UseVisualStyleBackColor = True
    '
    'Combo_TradeStatus
    '
    Me.Combo_TradeStatus.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_TradeStatus.Location = New System.Drawing.Point(124, 137)
    Me.Combo_TradeStatus.Name = "Combo_TradeStatus"
    Me.Combo_TradeStatus.Size = New System.Drawing.Size(184, 21)
    Me.Combo_TradeStatus.TabIndex = 12
    '
    'Label6
    '
    Me.Label6.Location = New System.Drawing.Point(12, 141)
    Me.Label6.Name = "Label6"
    Me.Label6.Size = New System.Drawing.Size(104, 16)
    Me.Label6.TabIndex = 210
    Me.Label6.Text = "Trade Status"
    Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'btnSave
    '
    Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnSave.Location = New System.Drawing.Point(795, 10)
    Me.btnSave.Name = "btnSave"
    Me.btnSave.Size = New System.Drawing.Size(75, 28)
    Me.btnSave.TabIndex = 13
    Me.btnSave.Text = "&Save"
    '
    'btnCancel
    '
    Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnCancel.Location = New System.Drawing.Point(913, 10)
    Me.btnCancel.Name = "btnCancel"
    Me.btnCancel.Size = New System.Drawing.Size(75, 28)
    Me.btnCancel.TabIndex = 14
    Me.btnCancel.Text = "&Cancel"
    '
    'Label_TransactionParentID
    '
    Me.Label_TransactionParentID.Location = New System.Drawing.Point(117, 7)
    Me.Label_TransactionParentID.Name = "Label_TransactionParentID"
    Me.Label_TransactionParentID.Size = New System.Drawing.Size(218, 18)
    Me.Label_TransactionParentID.TabIndex = 1
    Me.Label_TransactionParentID.Text = "0"
    '
    'Date_TradeSettlementDate
    '
    Me.Date_TradeSettlementDate.Location = New System.Drawing.Point(426, 107)
    Me.Date_TradeSettlementDate.Name = "Date_TradeSettlementDate"
    Me.Date_TradeSettlementDate.Size = New System.Drawing.Size(184, 20)
    Me.Date_TradeSettlementDate.TabIndex = 10
    '
    'Label_SettlementDate
    '
    Me.Label_SettlementDate.Location = New System.Drawing.Point(314, 111)
    Me.Label_SettlementDate.Name = "Label_SettlementDate"
    Me.Label_SettlementDate.Size = New System.Drawing.Size(100, 16)
    Me.Label_SettlementDate.TabIndex = 149
    Me.Label_SettlementDate.Text = "Settlement Date"
    Me.Label_SettlementDate.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Date_TradeValueDate
    '
    Me.Date_TradeValueDate.Location = New System.Drawing.Point(124, 107)
    Me.Date_TradeValueDate.Name = "Date_TradeValueDate"
    Me.Date_TradeValueDate.Size = New System.Drawing.Size(184, 20)
    Me.Date_TradeValueDate.TabIndex = 9
    '
    'Label_TradeDate
    '
    Me.Label_TradeDate.Location = New System.Drawing.Point(12, 111)
    Me.Label_TradeDate.Name = "Label_TradeDate"
    Me.Label_TradeDate.Size = New System.Drawing.Size(104, 16)
    Me.Label_TradeDate.TabIndex = 148
    Me.Label_TradeDate.Text = "NAV Date"
    Me.Label_TradeDate.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Combo_SettlementCurrency
    '
    Me.Combo_SettlementCurrency.Enabled = False
    Me.Combo_SettlementCurrency.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SettlementCurrency.Location = New System.Drawing.Point(244, 79)
    Me.Combo_SettlementCurrency.Name = "Combo_SettlementCurrency"
    Me.Combo_SettlementCurrency.Size = New System.Drawing.Size(64, 21)
    Me.Combo_SettlementCurrency.TabIndex = 6
    '
    'edit_Fees
    '
    Me.edit_Fees.Location = New System.Drawing.Point(124, 79)
    Me.edit_Fees.Name = "edit_Fees"
    Me.edit_Fees.RenaissanceTag = Nothing
    Me.edit_Fees.SelectTextOnFocus = False
    Me.edit_Fees.Size = New System.Drawing.Size(114, 20)
    Me.edit_Fees.TabIndex = 5
    Me.edit_Fees.Text = "0.00"
    Me.edit_Fees.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.edit_Fees.TextFormat = "#,##0.00"
    Me.edit_Fees.Value = 0
    '
    'Label_Fees
    '
    Me.Label_Fees.Location = New System.Drawing.Point(6, 82)
    Me.Label_Fees.Name = "Label_Fees"
    Me.Label_Fees.Size = New System.Drawing.Size(110, 13)
    Me.Label_Fees.TabIndex = 7
    Me.Label_Fees.Text = "Fees"
    Me.Label_Fees.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'edit_Price
    '
    Me.edit_Price.Location = New System.Drawing.Point(362, 51)
    Me.edit_Price.Name = "edit_Price"
    Me.edit_Price.RenaissanceTag = Nothing
    Me.edit_Price.SelectTextOnFocus = False
    Me.edit_Price.Size = New System.Drawing.Size(114, 20)
    Me.edit_Price.TabIndex = 4
    Me.edit_Price.Text = "0.00"
    Me.edit_Price.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.edit_Price.TextFormat = "#,##0.00"
    Me.edit_Price.Value = 0
    '
    'Label_Price
    '
    Me.Label_Price.Location = New System.Drawing.Point(244, 54)
    Me.Label_Price.Name = "Label_Price"
    Me.Label_Price.Size = New System.Drawing.Size(110, 13)
    Me.Label_Price.TabIndex = 5
    Me.Label_Price.Text = "Price"
    Me.Label_Price.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'edit_Amount
    '
    Me.edit_Amount.Location = New System.Drawing.Point(124, 51)
    Me.edit_Amount.Name = "edit_Amount"
    Me.edit_Amount.RenaissanceTag = Nothing
    Me.edit_Amount.SelectTextOnFocus = False
    Me.edit_Amount.Size = New System.Drawing.Size(114, 20)
    Me.edit_Amount.TabIndex = 3
    Me.edit_Amount.Text = "0.00"
    Me.edit_Amount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.edit_Amount.TextFormat = "#,##0.00"
    Me.edit_Amount.Value = 0
    '
    'Label_OrderType
    '
    Me.Label_OrderType.Location = New System.Drawing.Point(6, 54)
    Me.Label_OrderType.Name = "Label_OrderType"
    Me.Label_OrderType.Size = New System.Drawing.Size(110, 13)
    Me.Label_OrderType.TabIndex = 2
    Me.Label_OrderType.Text = "Type"
    Me.Label_OrderType.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Label_InstrumentName
    '
    Me.Label_InstrumentName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label_InstrumentName.Location = New System.Drawing.Point(12, 25)
    Me.Label_InstrumentName.Name = "Label_InstrumentName"
    Me.Label_InstrumentName.Size = New System.Drawing.Size(644, 13)
    Me.Label_InstrumentName.TabIndex = 2
    Me.Label_InstrumentName.Text = "."
    '
    'Label4
    '
    Me.Label4.Location = New System.Drawing.Point(6, 7)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(121, 18)
    Me.Label4.TabIndex = 0
    Me.Label4.Text = "Trade Details :"
    '
    'Button_SaveToExecution
    '
    Me.Button_SaveToExecution.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Button_SaveToExecution.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Button_SaveToExecution.Location = New System.Drawing.Point(795, 43)
    Me.Button_SaveToExecution.Name = "Button_SaveToExecution"
    Me.Button_SaveToExecution.Size = New System.Drawing.Size(193, 28)
    Me.Button_SaveToExecution.TabIndex = 216
    Me.Button_SaveToExecution.Text = "Save to Execution file"
    Me.Button_SaveToExecution.Visible = False
    '
    'Check_Futures
    '
    Me.Check_Futures.Checked = True
    Me.Check_Futures.CheckState = System.Windows.Forms.CheckState.Checked
    Me.Check_Futures.Location = New System.Drawing.Point(128, 178)
    Me.Check_Futures.Name = "Check_Futures"
    Me.Check_Futures.Size = New System.Drawing.Size(114, 18)
    Me.Check_Futures.TabIndex = 12
    Me.Check_Futures.Text = "Futures"
    Me.Check_Futures.UseVisualStyleBackColor = True
    '
    'Check_Options
    '
    Me.Check_Options.Checked = True
    Me.Check_Options.CheckState = System.Windows.Forms.CheckState.Checked
    Me.Check_Options.Location = New System.Drawing.Point(248, 178)
    Me.Check_Options.Name = "Check_Options"
    Me.Check_Options.Size = New System.Drawing.Size(110, 17)
    Me.Check_Options.TabIndex = 13
    Me.Check_Options.Text = "Options"
    Me.Check_Options.UseVisualStyleBackColor = True
    '
    'Label5
    '
    Me.Label5.Location = New System.Drawing.Point(18, 178)
    Me.Label5.Name = "Label5"
    Me.Label5.Size = New System.Drawing.Size(104, 16)
    Me.Label5.TabIndex = 11
    Me.Label5.Text = "Limit To"
    '
    'Check_FXForwards
    '
    Me.Check_FXForwards.Checked = True
    Me.Check_FXForwards.CheckState = System.Windows.Forms.CheckState.Checked
    Me.Check_FXForwards.Location = New System.Drawing.Point(437, 177)
    Me.Check_FXForwards.Name = "Check_FXForwards"
    Me.Check_FXForwards.Size = New System.Drawing.Size(110, 17)
    Me.Check_FXForwards.TabIndex = 15
    Me.Check_FXForwards.Text = "FX Forwards"
    Me.Check_FXForwards.UseVisualStyleBackColor = True
    '
    'Check_FX
    '
    Me.Check_FX.Checked = True
    Me.Check_FX.CheckState = System.Windows.Forms.CheckState.Checked
    Me.Check_FX.Location = New System.Drawing.Point(364, 177)
    Me.Check_FX.Name = "Check_FX"
    Me.Check_FX.Size = New System.Drawing.Size(67, 18)
    Me.Check_FX.TabIndex = 14
    Me.Check_FX.Text = "FX"
    Me.Check_FX.UseVisualStyleBackColor = True
    '
    'Check_Bonds
    '
    Me.Check_Bonds.Checked = True
    Me.Check_Bonds.CheckState = System.Windows.Forms.CheckState.Checked
    Me.Check_Bonds.Location = New System.Drawing.Point(545, 177)
    Me.Check_Bonds.Name = "Check_Bonds"
    Me.Check_Bonds.Size = New System.Drawing.Size(86, 17)
    Me.Check_Bonds.TabIndex = 110
    Me.Check_Bonds.Text = "Bonds"
    Me.Check_Bonds.UseVisualStyleBackColor = True
    '
    'frmUpdateTransactions
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(1012, 669)
    Me.Controls.Add(Me.Check_Bonds)
    Me.Controls.Add(Me.Check_FXForwards)
    Me.Controls.Add(Me.Check_FX)
    Me.Controls.Add(Me.Label5)
    Me.Controls.Add(Me.Check_Options)
    Me.Controls.Add(Me.Check_Futures)
    Me.Controls.Add(Me.Panel1)
    Me.Controls.Add(Me.Combo_TradeStatusOperator)
    Me.Controls.Add(Me.Combo_TradeStatusGroup)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.Combo_ValueDateOperator2)
    Me.Controls.Add(Me.Date_ValueDate2)
    Me.Controls.Add(Me.Form_StatusStrip)
    Me.Controls.Add(Me.RootMenu)
    Me.Controls.Add(Me.Combo_Select1_Value)
    Me.Controls.Add(Me.Combo_FieldSelect1)
    Me.Controls.Add(Me.Combo_Select1_Operator)
    Me.Controls.Add(Me.Combo_ValueDateOperator)
    Me.Controls.Add(Me.Date_ValueDate)
    Me.Controls.Add(Me.Label3)
    Me.Controls.Add(Me.Combo_TransactionInstrument)
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.Combo_TransactionFund)
    Me.Controls.Add(Me.label_CptyIsFund)
    Me.Controls.Add(Me.Grid_Transactions)
    Me.MinimumSize = New System.Drawing.Size(850, 475)
    Me.Name = "frmUpdateTransactions"
    Me.Text = "Update Transactions"
    CType(Me.Grid_Transactions, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ContextMenu_Trades.ResumeLayout(False)
    Me.Form_StatusStrip.ResumeLayout(False)
    Me.Form_StatusStrip.PerformLayout()
    Me.Panel1.ResumeLayout(False)
    Me.Panel1.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "

  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
  ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
  ''' </summary>
  Private WithEvents _MainForm As VeniceMain

  ' Form ToolTip
  ''' <summary>
  ''' Control to provide tooltip functionality.
  ''' </summary>
  Private FormTooltip As New ToolTip()

  ' Form Menu


  ' Form Constants, specific to the table being updated.

  ''' <summary>
  ''' Flag indicating whether this form can be cached or should always be closed.
  ''' </summary>
  Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

  ' Form Locals, initialised on 'New' defining what standard data items to use
  ''' <summary>
  ''' Variable holding the form's data table name. taken from the "ThisStandardDataset" object.
  ''' </summary>
  Private THIS_TABLENAME As String
  ''' <summary>
  ''' Variable holding the form's data adaptor name. taken from the "ThisStandardDataset" object.
  ''' </summary>
  Private THIS_ADAPTORNAME As String
  ''' <summary>
  ''' Variable holding the form's dataset name. taken from the "ThisStandardDataset" object.
  ''' </summary>
  Private THIS_DATASETNAME As String


  ' The standard ChangeID for this form. e.g. tblFund
  ''' <summary>
  ''' The standard ChangeID for this form.
  ''' </summary>
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

  ' Form Specific Order fields
  ''' <summary>
  ''' Field Name to order the Select combo by.
  ''' </summary>
  Private THIS_FORM_OrderBy As String

  ' Form specific Permissioning variables
  ''' <summary>
  ''' The THI s_ FOR m_ permission area
  ''' </summary>
  Private THIS_FORM_PermissionArea As String
  ''' <summary>
  ''' The THI s_ FOR m_ permission type
  ''' </summary>
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
  ''' <summary>
  ''' The THI s_ FOR m_ form ID
  ''' </summary>
  Private THIS_FORM_FormID As VeniceFormID

  ' Data Structures

  ''' <summary>
  ''' My dataset
  ''' </summary>
  Private myDataset As DataSet
  ''' <summary>
  ''' My table
  ''' </summary>
  Private myTable As DataTable
  ''' <summary>
  ''' My connection
  ''' </summary>
  Private myConnection As SqlConnection
  ''' <summary>
  ''' My adaptor
  ''' </summary>
  Private myAdaptor As SqlDataAdapter
  ''' <summary>
  ''' My data view
  ''' </summary>
  Private myDataView As DataView

  ''' <summary>
  ''' The this standard dataset
  ''' </summary>
  Private ThisStandardDataset As RenaissanceGlobals.StandardDataset

  ' Active Element.

  ''' <summary>
  ''' The __ is over cancel button
  ''' </summary>
  Private __IsOverCancelButton As Boolean
  ''' <summary>
  ''' The _ in use
  ''' </summary>
  Private _InUse As Boolean

  ' Form Status Flags

  ''' <summary>
  ''' The form is valid
  ''' </summary>
  Private FormIsValid As Boolean
  ''' <summary>
  ''' The in paint
  ''' </summary>
  Private InPaint As Boolean
  ''' <summary>
  ''' The _ form open failed
  ''' </summary>
  Private _FormOpenFailed As Boolean

  ' User Permission Flags

  ''' <summary>
  ''' The has read permission
  ''' </summary>
  Private HasReadPermission As Boolean
  ''' <summary>
  ''' The has update permission
  ''' </summary>
  Private HasUpdatePermission As Boolean
  ''' <summary>
  ''' The has insert permission
  ''' </summary>
  Private HasInsertPermission As Boolean
  ''' <summary>
  ''' The has delete permission
  ''' </summary>
  Private HasDeletePermission As Boolean

  '
  ''' <summary>
  ''' The transaction changed
  ''' </summary>
  Private TransactionChanged As Boolean = False
  ''' <summary>
  ''' The current transaction ID
  ''' </summary>
  Private CurrentTransactionID As Integer = 0

  ' Futures and options Instrument Types
  ''' <summary>
  ''' The instrument type_ futures
  ''' </summary>
  Private InstrumentType_Futures(-1) As Integer
  ''' <summary>
  ''' The instrument type_ options
  ''' </summary>
  Private InstrumentType_Options(-1) As Integer
  ''' <summary>
  ''' The instrument type_ futures and options
  ''' </summary>
  Private InstrumentType_FuturesAndOptions(-1) As Integer

#End Region

#Region " Form 'Properties' "

  ''' <summary>
  ''' Gets the main form.
  ''' </summary>
  ''' <value>The main form.</value>
  Public ReadOnly Property MainForm() As VeniceMain Implements Globals.StandardVeniceForm.MainForm
    ' Public property to return handle to the 'Main' Venice form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

  ''' <summary>
  ''' Gets or sets a value indicating whether this instance is over cancel button.
  ''' </summary>
  ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
  Public Property IsOverCancelButton() As Boolean Implements Globals.StandardVeniceForm.IsOverCancelButton
    ' Public property maintaining a value indicating if the cursor is over the 'Cancel'
    ' Button on this form.
    ' This property is specifically designed for use by the field formating Event functions
    ' In order that they do not impose format restrictions if the user is about to click the 
    ' 'Cancel' button.
    '
    Get
      Return __IsOverCancelButton
    End Get
    Set(ByVal Value As Boolean)
      __IsOverCancelButton = Value
    End Set
  End Property

  ''' <summary>
  ''' Gets a value indicating whether this instance is in paint.
  ''' </summary>
  ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
  Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardVeniceForm.IsInPaint
    Get
      Return InPaint
    End Get
  End Property

  ''' <summary>
  ''' Gets a value indicating whether [in use].
  ''' </summary>
  ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property InUse() As Boolean Implements Globals.StandardVeniceForm.InUse
    Get
      Return _InUse
    End Get
  End Property

  ''' <summary>
  ''' Gets a value indicating whether [form open failed].
  ''' </summary>
  ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardVeniceForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

#End Region

  ''' <summary>
  ''' Initializes a new instance of the <see cref="frmUpdateTransactions"/> class.
  ''' </summary>
  ''' <param name="pMainForm">Reference to the main Venice form. Provides a handle to common functionality.</param>
  Public Sub New(ByVal pMainForm As VeniceMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************
    Me.New()

    _MainForm = pMainForm
    AddHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

    _FormOpenFailed = False
    _InUse = True

    ' Default Select and Order fields.

    THIS_FORM_OrderBy = "TransactionFund, TransactionInstrument"

    ' Form Permissioning :-

    THIS_FORM_PermissionArea = Me.Name
    THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

    ' 'This' form ID

    THIS_FORM_FormID = VeniceFormID.frmUpdateTransactions

    ' This form's dataset type.

    ThisStandardDataset = RenaissanceStandardDatasets.tblSelectTransaction ' This Defines the Form Data !!! 


    ' Data object names standard to this Form type.

    THIS_TABLENAME = ThisStandardDataset.TableName
    THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
    THIS_DATASETNAME = ThisStandardDataset.DatasetName

    THIS_FORM_ChangeID = ThisStandardDataset.ChangeID


    ' Establish / Retrieve data objects for this form.

    myConnection = MainForm.MainDataHandler.Get_Connection(VENICE_CONNECTION)
    myAdaptor = MainForm.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, VENICE_CONNECTION, THIS_TABLENAME)
    myDataset = MainForm.Load_Table(ThisStandardDataset, False)
    myTable = myDataset.Tables(0)

    ' Grid Fields
    Dim FieldsMenu As ToolStripMenuItem = SetFieldSelectMenu(RootMenu)

    ' Establish initial DataView and Initialise Transactions Grid.

    Dim thisCol As C1.Win.C1FlexGrid.Column
    Dim Counter As Integer

    Try
      myDataView = New DataView(myTable, "False", "", DataViewRowState.CurrentRows)

      Grid_Transactions.DataSource = myDataView

      ' Format Transactions Grid, hide unwanted columns.

      Grid_Transactions.Cols("TransactionTicket").Move(1)
      Grid_Transactions.Cols("FundName").Move(2)
      Grid_Transactions.Cols("InstrumentDescription").Move(3)
      Grid_Transactions.Cols("TransactionTypeName").Move(4)
      Grid_Transactions.Cols("TransactionUnits").Move(5)
      Grid_Transactions.Cols("TransactionPrice").Move(6)
      Grid_Transactions.Cols("TransactionValueDate").Move(7)
      Grid_Transactions.Cols("AllCosts").Move(8)
      Grid_Transactions.Cols("TradeStatusName").Move(9)
    Catch ex As Exception
    End Try

    For Each thisCol In Grid_Transactions.Cols
      Try
        Select Case thisCol.Name
          Case "TransactionTicket"
          Case "FundName"
          Case "InstrumentDescription"
          Case "TransactionTypeName"
          Case "TransactionUnits"
            thisCol.Format = "#,##0.0000"
          Case "TransactionPrice"
            thisCol.Format = "#,##0.0000"
          Case "TransactionSettlement"
            thisCol.Format = "#,##0.0000"
            thisCol.Visible = False
          Case "TransactionValueDate"
            thisCol.Format = DISPLAYMEMBER_DATEFORMAT
          Case "TradeStatusName"
          Case "AllCosts"
            thisCol.Format = "#,##0"
          Case "TransactionCosts"
            thisCol.Visible = False
            thisCol.Format = "#,##0"

          Case Else
            thisCol.Visible = False

            Try
              If thisCol.DataType Is GetType(Date) Then
                thisCol.Format = DISPLAYMEMBER_DATEFORMAT
              ElseIf thisCol.DataType Is GetType(Integer) Then
                thisCol.Format = "#,##0"
              ElseIf thisCol.DataType Is GetType(Double) Then
                thisCol.Format = "#,##0.0000"
              End If
            Catch ex As Exception
            End Try
        End Select

        If (thisCol.Caption.ToUpper.StartsWith("TRANSACTION")) Then
          thisCol.Caption = thisCol.Caption.Substring(11)
        End If

        If (thisCol.Visible) Then
          Try
            CType(FieldsMenu.DropDownItems("Menu_GridField_" & thisCol.Name), ToolStripMenuItem).Checked = True
          Catch ex As Exception
          End Try
        End If
      Catch ex As Exception
      End Try
    Next

    Try
      For Counter = 0 To (Grid_Transactions.Cols.Count - 1)
        If (Grid_Transactions.Cols(Counter).Caption.ToUpper.StartsWith("TRANSACTION")) Then
          Grid_Transactions.Cols(Counter).Caption = Grid_Transactions.Cols(Counter).Caption.Substring(11)
        End If
      Next
    Catch ex As Exception
    End Try

    ' Initialise Field select area

    Try
      Call BuildFieldSelectCombos()
    Catch ex As Exception
    End Try

    ' Form Control Changed events
    AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

    AddHandler Combo_TransactionFund.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_TransactionInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_TradeStatusGroup.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_ValueDateOperator.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_ValueDateOperator2.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_TradeStatusOperator.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Date_ValueDate.ValueChanged, AddressOf Me.FormControlChanged
    AddHandler Date_ValueDate2.ValueChanged, AddressOf Me.FormControlChanged
    AddHandler Check_Futures.CheckedChanged, AddressOf Me.FormControlChanged
    AddHandler Check_Options.CheckedChanged, AddressOf Me.FormControlChanged
    AddHandler Check_FX.CheckedChanged, AddressOf Me.FormControlChanged
    AddHandler Check_FXForwards.CheckedChanged, AddressOf Me.FormControlChanged
    AddHandler Check_Bonds.CheckedChanged, AddressOf Me.FormControlChanged

    AddHandler edit_Amount.ValueChanged, AddressOf Me.TradeDetailsChanged
    AddHandler edit_Price.ValueChanged, AddressOf Me.TradeDetailsChanged
    AddHandler edit_Fees.ValueChanged, AddressOf Me.TradeDetailsChanged
    AddHandler Check_BookFeesSeparately.CheckedChanged, AddressOf Me.TradeDetailsChanged
    AddHandler Combo_FeeInstrument.SelectedValueChanged, AddressOf Me.TradeDetailsChanged
    AddHandler Date_TradeValueDate.ValueChanged, AddressOf Me.TradeDetailsChanged
    AddHandler Date_TradeSettlementDate.ValueChanged, AddressOf Me.TradeDetailsChanged
    AddHandler Combo_TradeStatus.SelectedValueChanged, AddressOf Me.TradeDetailsChanged

    AddHandler Combo_Select1_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Select1_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Select1_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp

    AddHandler Combo_TransactionFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_TransactionInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_TradeStatusGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_ValueDateOperator.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_ValueDateOperator2.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_TradeStatusOperator.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_TradeStatus.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_FeeInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType

    AddHandler Combo_TransactionFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_TransactionInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_TradeStatusGroup.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_ValueDateOperator.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_ValueDateOperator2.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_TradeStatusOperator.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_TradeStatus.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_FeeInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

    AddHandler Combo_TransactionFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_TransactionInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_TradeStatusGroup.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_ValueDateOperator.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_ValueDateOperator2.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_TradeStatusOperator.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_TradeStatus.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_FeeInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

    AddHandler Combo_TransactionFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_TransactionInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_TradeStatusGroup.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_ValueDateOperator.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_ValueDateOperator2.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_TradeStatusOperator.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_TradeStatus.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_FeeInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

    ' Set up the ToolTip
    MainForm.SetFormToolTip(Me, FormTooltip)

  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
  ''' <summary>
  ''' Resets the form.
  ''' </summary>
  Public Sub ResetForm() Implements StandardVeniceForm.ResetForm
    THIS_FORM_OrderBy = "TransactionFund, TransactionInstrument"

    Call Form_Load(Me, New System.EventArgs)
  End Sub

  ''' <summary>
  ''' Closes the form.
  ''' </summary>
  Public Sub CloseForm() Implements StandardVeniceForm.CloseForm
    ALWAYS_CLOSE_THIS_FORM = True
    Me.Close()
  End Sub

  ''' <summary>
  ''' Handles the Load event of the Form control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
    _FormOpenFailed = False
    _InUse = True

    ' Initialise Data structures. Connection, Adaptor and Dataset.

    If Not (MainForm Is Nothing) Then
      FormIsValid = True
    Else
      MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (myConnection Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (myAdaptor Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (myDataset Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If


    ' Check User permissions
    Try
      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

        FormIsValid = False
        _FormOpenFailed = True

        Exit Sub
      End If
    Catch ex As Exception
      FormIsValid = False
      _FormOpenFailed = True

      Exit Sub
    End Try

    Try
      ' Initialse form

      InPaint = True
      IsOverCancelButton = False

      CurrentTransactionID = 0
      TransactionChanged = False

      ' Initialise main select controls
      ' Build Sorted data list from which this form operates

      Call SetFundCombo()
      Call SetCurrencyCombo()
      Call SetTradeStatusCombo()
      Call SetTradeStatusGroupCombo()
      Call SetInstrumentTypeArrays()
      Call SetTransactionCostsInstrumentCombo()

      If (Combo_TransactionFund.Items.Count > 1) Then
        Me.Combo_TransactionFund.SelectedIndex = 0
      Else
        Me.Combo_TransactionFund.SelectedIndex = -1
      End If

      Call SetInstrumentCombo()

      Me.Combo_TransactionInstrument.SelectedIndex = -1
      Me.Combo_ValueDateOperator.SelectedIndex = 0 ' N/A
      Me.Date_ValueDate.Value = RenaissanceUtilities.DatePeriodFunctions.AddPeriodToDate(DealingPeriod.Daily, Now.Date(), -1)
      Me.Combo_ValueDateOperator2.SelectedIndex = 4 ' <=
      Me.Date_ValueDate2.Value = RenaissanceUtilities.DatePeriodFunctions.FitDateToPeriod(DealingPeriod.Daily, Now.Date())
      Me.Combo_TradeStatusOperator.SelectedIndex = 0
      Me.Combo_TradeStatusOperator.SelectedIndex = 1
      Me.Combo_TradeStatusGroup.SelectedValue = StatusGroups.PendingConfirmationMO

      ' Set Default.

      'For counter As Integer = 0 To (Combo_FieldSelect1.Items.Count - 1)
      '  If (Combo_FieldSelect1.Items(counter).ToString.EndsWith("TradeStatusID.")) Then
      '    Combo_FieldSelect1.SelectedIndex = counter
      '    Exit For
      '  End If
      'Next

      Combo_FieldSelect1.SelectedIndex = 0
      Me.Combo_Select1_Operator.SelectedIndex = 0
      Call UpdateSelectedValueCombo(Me.Combo_FieldSelect1.SelectedItem, Me.Combo_Select1_Value)
      'Me.Combo_Select1_Value.SelectedValue = RenaissanceGlobals.TradeStatus.PendingDerivativeMO

      Me.Check_Futures.Checked = False
      Me.Check_Options.Checked = False
      Me.Check_FX.Checked = False
      Me.Check_FXForwards.Checked = False
      Me.Check_Bonds.Checked = False

      ' Default Grid Sort : By Value Date and ParentID (Effectively Entry Order)
      For Each thisCol As C1.Win.C1FlexGrid.Column In Grid_Transactions.Cols
        thisCol.Sort = SortFlags.None
      Next
      Grid_Transactions.Cols("TransactionValueDate").Sort = SortFlags.Ascending
      Grid_Transactions.Cols("TransactionParentID").Sort = SortFlags.Ascending
      Grid_Transactions.Sort(SortFlags.UseColSort, Grid_Transactions.Cols("TransactionValueDate").Index, Grid_Transactions.Cols("TransactionParentID").Index)

      Call SetSortedRows()

      Call MainForm.SetComboSelectionLengths(Me)
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error initialising Select Transaction Form.", ex.StackTrace, True)
    Finally
      InPaint = False
    End Try


  End Sub

  ''' <summary>
  ''' Handles the Closing event of the frm control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
  Private Sub frm_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    Dim HideForm As Boolean

    ' Hide or Close this form ?
    ' All depends on how many of this form type are Open or in Cache...

    _InUse = False

    If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
      HideForm = False
    Else
      HideForm = True
      If MainForm.VeniceForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
        HideForm = False
      End If
    End If

    If HideForm = True Then
      MainForm.HideInFormsCollection(Me)
      Me.Hide() ' NPP Fix

      e.Cancel = True
    Else
      Try
        MainForm.RemoveFromFormsCollection(Me)
        RemoveHandler _MainForm.VeniceAutoUpdate, AddressOf Me.AutoUpdate

        RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

        RemoveHandler Combo_TransactionFund.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_TransactionInstrument.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_TradeStatusGroup.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_ValueDateOperator.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_ValueDateOperator2.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_TradeStatusOperator.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Date_ValueDate.ValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Date_ValueDate2.ValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_Futures.CheckedChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_Options.CheckedChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_FX.CheckedChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_FXForwards.CheckedChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_Bonds.CheckedChanged, AddressOf Me.FormControlChanged

        RemoveHandler edit_Amount.ValueChanged, AddressOf Me.TradeDetailsChanged
        RemoveHandler edit_Price.ValueChanged, AddressOf Me.TradeDetailsChanged
        RemoveHandler edit_Fees.ValueChanged, AddressOf Me.TradeDetailsChanged
        RemoveHandler Check_BookFeesSeparately.CheckedChanged, AddressOf Me.TradeDetailsChanged
        RemoveHandler Combo_FeeInstrument.SelectedValueChanged, AddressOf Me.TradeDetailsChanged
        RemoveHandler Date_TradeValueDate.ValueChanged, AddressOf Me.TradeDetailsChanged
        RemoveHandler Date_TradeSettlementDate.ValueChanged, AddressOf Me.TradeDetailsChanged
        RemoveHandler Combo_TradeStatus.SelectedValueChanged, AddressOf Me.TradeDetailsChanged

        RemoveHandler Combo_Select1_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_Select1_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_Select1_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp

        RemoveHandler Combo_TransactionFund.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_TransactionInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_TradeStatusGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_ValueDateOperator.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_ValueDateOperator2.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_TradeStatusOperator.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_TradeStatus.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_FeeInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        RemoveHandler Combo_TransactionFund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_TransactionInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_TradeStatusGroup.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_ValueDateOperator.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_ValueDateOperator2.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_TradeStatusOperator.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_TradeStatus.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_FeeInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

        RemoveHandler Combo_TransactionFund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_TransactionInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_TradeStatusGroup.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_ValueDateOperator.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_ValueDateOperator2.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_TradeStatusOperator.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_TradeStatus.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_FeeInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

        RemoveHandler Combo_TransactionFund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_TransactionInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_TradeStatusGroup.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_ValueDateOperator.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_ValueDateOperator2.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_TradeStatusOperator.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_TradeStatus.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_FeeInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

      Catch ex As Exception
      End Try
    End If

  End Sub


#End Region

  ' Routine to handle changes / updates to tables by this and other windows.
  ' If this, or any other, form posts a change to a table, then it will invoke an update event 
  ' detailing what tables have been altered.
  ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
  ' the 'VeniceAutoUpdate' event of the main Venice form.
  ' Each form may them react as appropriate to changes in any table that might impact it.
  '
  ''' <summary>
  ''' Autoes the update.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    Dim OrgInPaint As Boolean
    Dim KnowledgeDateChanged As Boolean
    Dim RefreshGrid As Boolean

    If (Not Me.Created) OrElse (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then
      Exit Sub
    End If

    OrgInPaint = InPaint

    Try
      InPaint = True
      KnowledgeDateChanged = False
      RefreshGrid = False

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
        KnowledgeDateChanged = True
      End If
    Catch ex As Exception
    End Try

    ' ****************************************************************
    ' Check for changes relevant to this form
    ' ****************************************************************

    ' Changes to the tblCurrency table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblCurrency) = True) Or KnowledgeDateChanged Then
        Call SetCurrencyCombo()
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblCurrency", ex.StackTrace, True)
    End Try

    ' Changes to the tblInstrumentType table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrumentType) = True) Or KnowledgeDateChanged Then
        Call SetInstrumentTypeArrays()
        RefreshGrid = True
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblInstrumentType", ex.StackTrace, True)
    End Try

    ' Changes to the tblFund table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFund) = True) Or KnowledgeDateChanged Then
        Call SetFundCombo()
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblFund", ex.StackTrace, True)
    End Try

    ' Changes to the tblInstrument table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrument) = True) Or KnowledgeDateChanged Then
        Call SetInstrumentCombo()
        Call SetTransactionCostsInstrumentCombo()
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblInstrument", ex.StackTrace, True)
    End Try

    ' Changes to the tblTradeStatusGroup table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblTradeStatusGroup) = True) Or KnowledgeDateChanged Then
        Call SetTradeStatusGroupCombo()
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblTradeStatusGroup", ex.StackTrace, True)
    End Try

    ' Changes to the tblTradeStatus table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblTradeStatus) = True) Or KnowledgeDateChanged Then
      Try
        ' Re-Set combo.
        Call SetTradeStatusCombo()
      Catch ex As Exception
        MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblTradeStatus", ex.StackTrace, True)
      End Try
    End If

    ' Changes to the tblTransaction table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblTransaction) = True) Or KnowledgeDateChanged Then
        ' RefreshGrid = True ' Grid will be refreshed by subsequent tblSelectTransactions Update.
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblTransaction", ex.StackTrace, True)
    End Try

    ' Changes to the KnowledgeDate :-
    Try
      If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
        RefreshGrid = True
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: KnowledgeDate", ex.StackTrace, True)
    End Try

    ' Check TableUpdate against first Select Field
    Try
      If Me.Combo_FieldSelect1.SelectedIndex >= 0 Then
        Try

          ' If the First FieldSelect combo has selected a Transaction Field which is related to
          ' another table, as defined in tblReferentialIntegrity (e.g. TransactionCounterparty)
          ' Then if that related table is updated, the FieldSelect-Values combo must be updated.

          ' If ChangedID is ChangeID of related table then ,..

          If (e.TableChanged(GetFieldChangeID(Combo_FieldSelect1.SelectedItem)) = True) Or KnowledgeDateChanged Then
            Dim SelectedValue As Object
            Dim TextValue As String

            ' Save current Value Combo Value.

            SelectedValue = Nothing
            TextValue = ""

            If Me.Combo_Select1_Value.SelectedIndex >= 0 Then
              SelectedValue = Me.Combo_Select1_Value.SelectedValue
            ElseIf (SelectedValue Is Nothing) Then
              TextValue = Me.Combo_Select1_Value.Text
            End If

            ' Update FieldSelect-Values Combo.

            Call UpdateSelectedValueCombo(Me.Combo_FieldSelect1.SelectedItem, Me.Combo_Select1_Value)

            ' Restore Saved Value.

            If (Not (SelectedValue Is Nothing)) Then
              Me.Combo_Select1_Value.SelectedValue = SelectedValue
            ElseIf TextValue.Length > 0 Then
              Me.Combo_Select1_Value.Text = TextValue
            End If
          End If
        Catch ex As Exception
        End Try
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: First SelectField", ex.StackTrace, True)
    End Try


    ' Changes to the tblUserPermissions table :-

    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

        ' Check ongoing permissions.

        Call CheckPermissions()
        If (HasReadPermission = False) Then
          Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

          FormIsValid = False
          InPaint = OrgInPaint
          Me.Close()
          Exit Sub
        End If

        RefreshGrid = True

      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: tblUserPermissions", ex.StackTrace, True)
    End Try


    ' ****************************************************************
    ' Changes to the Main FORM table :-
    ' (or the tblTransactions table)
    ' ****************************************************************

    Try
      If (e.TableChanged(THIS_FORM_ChangeID) = True) Or _
         (RefreshGrid = True) Or _
         KnowledgeDateChanged Then

        Dim Counter As Integer
        Dim CurrentID As Integer = CurrentTransactionID
        ' Re-Set Controls etc.
        Call SetSortedRows(True)

        If (CurrentID > 0) Then
          For Counter = 0 To (Grid_Transactions.Rows.Count - 1)
            If (Me.Grid_Transactions.Rows(Counter).DataSource IsNot Nothing) AndAlso (Me.Grid_Transactions.Rows(Counter).DataSource("TransactionParentID") = CurrentID) Then
              Grid_Transactions.Select(Counter, 0, True)
              Exit For
            End If
          Next
        End If
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: Main Form table", ex.StackTrace, True)
    End Try

    InPaint = OrgInPaint

  End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

  ''' <summary>
  ''' Sets the form select criteria.
  ''' </summary>
  ''' <param name="FundID">The fund ID.</param>
  ''' <param name="InstrumentID">The instrument ID.</param>
  Public Sub SetFormSelectCriteria(ByVal FundID As Integer, ByVal InstrumentID As Integer)
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Try

      If (FundID > 0) Then
        Combo_TransactionFund.SelectedValue = FundID
      Else
        Combo_TransactionFund.SelectedIndex = 0
      End If

      If (InstrumentID > 0) Then
        Combo_TransactionInstrument.SelectedValue = InstrumentID
      Else
        Combo_TransactionInstrument.SelectedIndex = 0
      End If

      Combo_FieldSelect1.SelectedIndex = 0
      MainForm.ClearComboSelection(Combo_Select1_Value)

    Catch ex As Exception

    End Try

  End Sub

  ''' <summary>
  ''' Gets the transaction select string.
  ''' </summary>
  ''' <param name="OnlyUseTblTransactionFields">if set to <c>true</c> [only use TBL transaction fields].</param>
  ''' <returns>System.String.</returns>
  Private Function GetTransactionSelectString(Optional ByVal OnlyUseTblTransactionFields As Boolean = False) As String
    ' *******************************************************************************
    ' Build a Select String appropriate to the form status.
    ' *******************************************************************************

    Dim SelectString As String
    Dim FieldSelectString As String
    Dim InstrumentTypeList As New List(Of Integer)

    SelectString = ""
    FieldSelectString = ""
    Me.MainForm.SetToolStripText(Label_Status, "")

    Try

      ' Select on Fund...

      If (Me.Combo_TransactionFund.SelectedIndex > 0) Then
        If IsFieldTransactionField("TransactionFund") = True Then
          SelectString = "(TransactionFund=" & Combo_TransactionFund.SelectedValue.ToString & ")"
        End If
      End If

      ' Select on Instrument

      If (Me.Combo_TransactionInstrument.SelectedIndex > 0) Then
        If IsFieldTransactionField("TransactionInstrument") = True Then
          If (SelectString.Length > 0) Then
            SelectString &= " AND "
          End If
          SelectString &= "(TransactionInstrument=" & Combo_TransactionInstrument.SelectedValue.ToString & ")"
        End If
      End If

      ' Select on Value Date

      If (Me.Combo_ValueDateOperator.SelectedIndex > 0) Then
        If (SelectString.Length > 0) Then
          SelectString &= " AND "
        End If

        SelectString &= "(TransactionValueDate " & Combo_ValueDateOperator.SelectedItem.ToString & " #" & Me.Date_ValueDate.Value.ToString(QUERY_SHORTDATEFORMAT) & "#)"
      End If

      If (Me.Combo_ValueDateOperator2.SelectedIndex > 0) Then
        If (SelectString.Length > 0) Then
          SelectString &= " AND "
        End If

        SelectString &= "(TransactionValueDate " & Combo_ValueDateOperator2.SelectedItem.ToString & " #" & Me.Date_ValueDate2.Value.ToString(QUERY_SHORTDATEFORMAT) & "#)"
      End If

      ' Select on Trade Status Group
      ' Combo_TradeStatusOperator
      ' Combo_TradeStatusGroup

      If (Combo_TradeStatusOperator.SelectedIndex > 0) AndAlso (Combo_TradeStatusGroup.SelectedIndex > 0) Then
        Dim GroupID As Integer = CInt(Combo_TradeStatusGroup.SelectedValue)
        Dim GroupItemsDS As RenaissanceDataClass.DSTradeStatusGroupItems
        Dim SelectedItems() As RenaissanceDataClass.DSTradeStatusGroupItems.tblTradeStatusGroupItemsRow
        Dim ItemIndex As Integer

        GroupItemsDS = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblTradeStatusGroupItems), RenaissanceDataClass.DSTradeStatusGroupItems)

        If (GroupItemsDS IsNot Nothing) Then
          SelectedItems = GroupItemsDS.tblTradeStatusGroupItems.Select("GroupID=" & GroupID.ToString())

          If (SelectString.Length > 0) Then
            SelectString &= " AND "
          End If

          If (SelectedItems.Count > 0) Then

            If (Combo_TradeStatusOperator.SelectedIndex = 2) Then ' NOT IN
              SelectString &= " (TransactionTradeStatusID NOT IN ("
            Else
              SelectString &= " (TransactionTradeStatusID IN ("
            End If

            Try
              For ItemIndex = 0 To (SelectedItems.Count - 1)
                If (ItemIndex > 0) Then
                  SelectString &= ","
                End If

                SelectString &= SelectedItems(ItemIndex).StatusID
              Next

            Catch ex As Exception
            Finally
              SelectString &= ")) "
            End Try

          Else
            SelectString &= " (TransactionTradeStatusID < 0) "
          End If
        End If

      End If


      ' Field Select ...

      FieldSelectString = "("
      Dim TransactionDataset As New RenaissanceDataClass.DSSelectTransaction
      Dim TransactionTable As RenaissanceDataClass.DSSelectTransaction.tblSelectTransactionDataTable = TransactionDataset.tblSelectTransaction
      Dim FieldName As String

      ' Don't need the data, just the column defs.
      'TransactionDataset = MainForm.Load_Table(RenaissanceStandardDatasets.tblTransaction, False)
      'TransactionTable = myDataset.Tables(0)

      ' Select Field One.

      Try
        If (Me.Combo_FieldSelect1.SelectedIndex > 0) AndAlso (Me.Combo_Select1_Operator.SelectedIndex > 0) And (Me.Combo_Select1_Value.Text.Length > 0) Then
          ' Get Field Name, Add 'Transaction' back on if necessary.

          FieldName = Combo_FieldSelect1.SelectedItem
          If (FieldName.EndsWith(".")) Then
            FieldName = "Transaction" & FieldName.Substring(0, FieldName.Length - 1)
          End If

          If (OnlyUseTblTransactionFields = False) OrElse (IsFieldTransactionField(FieldName) = True) Then
            ' If the Selected Field Name is a String Field ...

            If TransactionTable.Columns.Contains(FieldName) Then
              If (TransactionTable.Columns(FieldName).DataType Is GetType(System.String)) Then

                ' If there is a value Selected, Use it - else use the Combo Text.

                If (Not (Combo_Select1_Value.SelectedValue Is Nothing)) Then
                  FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.Text & " '" & Combo_Select1_Value.SelectedValue.ToString & "')"
                Else
                  FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.Text & " '" & Combo_Select1_Value.Text.ToString & "')"
                End If

              ElseIf (TransactionTable.Columns(FieldName).DataType Is GetType(System.DateTime)) Then

                ' For Dates, If there is no 'Selected' value use the Combo text if it is a Valid date, else
                ' Use the 'Selected' Value if it is a date.

                If (Me.Combo_Select1_Value.SelectedValue Is Nothing) AndAlso (IsDate(Me.Combo_Select1_Value.Text)) Then
                  FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select1_Value.Text).ToString(QUERY_SHORTDATEFORMAT) & "#)"
                ElseIf (Not (Me.Combo_Select1_Value.SelectedValue Is Nothing)) And IsDate(Combo_Select1_Value.SelectedValue) Then
                  FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select1_Value.SelectedValue).ToString(QUERY_SHORTDATEFORMAT) & "#)"
                Else
                  FieldSelectString &= "(true)"
                End If

              Else
                ' Now Deemed to be numeric, or at least not in need of quotes / delimeters...
                ' If there is no 'Selected' value use the Combo text if it is a Valid number, else
                ' Use the 'Selected' Value if it is a valid numeric.

                If (Not (Combo_Select1_Value.SelectedValue Is Nothing)) AndAlso (IsNumeric(Combo_Select1_Value.SelectedValue)) Then
                  FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.SelectedItem.ToString & " " & Combo_Select1_Value.SelectedValue.ToString & ")"
                ElseIf IsNumeric(Combo_Select1_Value.Text) Then
                  FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.SelectedItem.ToString & " " & Combo_Select1_Value.Text & ")"
                Else
                  FieldSelectString &= "(true)"
                End If
              End If
            End If

          End If
        End If

      Catch ex As Exception
      End Try

      FieldSelectString &= ")"

      ' If the FieldSelect string is used, then tag it on to the main select string.

      If FieldSelectString.Length > 2 Then
        If (SelectString.Length > 0) Then
          SelectString &= " AND "
        End If
        SelectString &= FieldSelectString
      End If

      If (OnlyUseTblTransactionFields = False) AndAlso (Check_Futures.Checked OrElse Check_Options.Checked OrElse Check_FX.Checked OrElse Check_FXForwards.Checked OrElse Check_Bonds.Checked) Then
        Dim TableSelectString As String = ""
        Dim DerivativesSelectString As String = ""

        Dim InstrumentTypeDS As RenaissanceDataClass.DSInstrumentType
        Dim InstrumentTypeTable As RenaissanceDataClass.DSInstrumentType.tblInstrumentTypeDataTable
        Dim InstrumentTypeRows() As RenaissanceDataClass.DSInstrumentType.tblInstrumentTypeRow
        Dim Counter As Integer

        InstrumentTypeDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblInstrumentType)

        ' Futures and Options Instrument Types.

        If (InstrumentTypeDS IsNot Nothing) Then
          InstrumentTypeTable = InstrumentTypeDS.tblInstrumentType

          If (Check_Futures.Checked OrElse Check_Options.Checked) Then

            If (Check_Futures.Checked) Then
              TableSelectString = ("InstrumentTypeFuturesStylePricing<>0")

              If (Check_Options.Checked) Then
                TableSelectString = "(" & TableSelectString & ") OR (InstrumentTypeHasDelta<>0)"
              End If
            ElseIf (Check_Options.Checked) Then
              TableSelectString = ("InstrumentTypeHasDelta<>0")
            End If

            InstrumentTypeRows = InstrumentTypeTable.Select(TableSelectString)

            If (InstrumentTypeRows IsNot Nothing) AndAlso (InstrumentTypeRows.Length > 0) Then

              'DerivativesSelectString = "(InstrumentType IN ("

              For Counter = 0 To (InstrumentTypeRows.Length - 1)
                If Not InstrumentTypeList.Contains(InstrumentTypeRows(Counter).InstrumentTypeID) Then InstrumentTypeList.Add(InstrumentTypeRows(Counter).InstrumentTypeID)

                'If (Counter = 0) Then
                '  DerivativesSelectString &= InstrumentTypeRows(Counter).InstrumentTypeID.ToString
                'Else
                '  DerivativesSelectString &= "," & InstrumentTypeRows(Counter).InstrumentTypeID.ToString
                'End If
              Next

              'DerivativesSelectString &= "))"
            End If
          End If

          If (Check_Bonds.Checked) Then
            If Not InstrumentTypeList.Contains(InstrumentTypes.Bond) Then InstrumentTypeList.Add(CInt(InstrumentTypes.Bond))
          End If

          ' Stitch all instrumentTypes together.

          If (InstrumentTypeList.Count > 0) Then
            DerivativesSelectString = "(InstrumentType IN ("

            For Counter = 0 To (InstrumentTypeList.Count - 1)
              If (Counter = 0) Then
                DerivativesSelectString &= InstrumentTypeList(Counter).ToString
              Else
                DerivativesSelectString &= "," & InstrumentTypeList(Counter).ToString
              End If
            Next

            DerivativesSelectString &= "))"
          End If


          If (Check_FX.Checked OrElse Check_FXForwards.Checked) Then
            Dim FXSelectString As String = ""

            If (Check_FX.Checked) AndAlso (Check_FXForwards.Checked) Then
              FXSelectString = "(TransactionType IN (" & CInt(TransactionTypes.BuyFX).ToString() & "," & CInt(TransactionTypes.SellFX).ToString() & "," & CInt(TransactionTypes.BuyFXForward).ToString() & "," & CInt(TransactionTypes.SellFXForward).ToString() & "))"
            ElseIf (Check_FX.Checked) Then
              FXSelectString = "(TransactionType IN (" & CInt(TransactionTypes.BuyFX).ToString() & "," & CInt(TransactionTypes.SellFX).ToString() & "))"
            ElseIf (Check_FXForwards.Checked) Then
              FXSelectString = "(TransactionType IN (" & CInt(TransactionTypes.BuyFXForward).ToString() & "," & CInt(TransactionTypes.SellFXForward).ToString() & "))"
            End If

            If (DerivativesSelectString.Length > 0) Then
              DerivativesSelectString = "(" & DerivativesSelectString & " OR " & FXSelectString & ")"
            Else
              DerivativesSelectString = FXSelectString
            End If

          End If

          If (DerivativesSelectString.Length > 0) Then
            If SelectString.Length <= 0 Then
              SelectString = DerivativesSelectString
            Else
              SelectString = "(" & SelectString & ") AND " & DerivativesSelectString
            End If

          End If
        End If

      End If

      If SelectString.Length <= 0 Then
        SelectString = "true"
      End If

    Catch ex As Exception
      SelectString = "true"
    End Try

    Return SelectString

  End Function

  ''' <summary>
  ''' Determines whether [is field transaction field] [the specified field name].
  ''' </summary>
  ''' <param name="FieldName">Name of the field.</param>
  ''' <returns><c>true</c> if [is field transaction field] [the specified field name]; otherwise, <c>false</c>.</returns>
  Private Function IsFieldTransactionField(ByVal FieldName As String) As Boolean
    ' *******************************************************************************
    ' Simple function to return a boolean value indicating whether or not a column name is
    ' part of the tblTransactions table.
    ' *******************************************************************************

    Try
      Dim TransactionTable As New RenaissanceDataClass.DSTransaction.tblTransactionDataTable

      If (TransactionTable Is Nothing) Then
        Return False
      Else
        Return TransactionTable.Columns.Contains(FieldName)
      End If
    Catch ex As Exception
      Return (False)
    End Try

    Return (False)

  End Function

  ''' <summary>
  ''' Sets the sorted rows.
  ''' </summary>
  ''' <param name="ForceRefresh">if set to <c>true</c> [force refresh].</param>
  Private Sub SetSortedRows(Optional ByVal ForceRefresh As Boolean = False)
    ' *******************************************************************************
    ' Build a Select String appropriate to the form status and apply it to the DataView object.
    ' *******************************************************************************

    Dim SelectString As String

    If (Me.Created) And (Not Me.IsDisposed) Then

      SelectString = GetTransactionSelectString()

      Try

        If ForceRefresh Then
          myDataView.RowFilter = "False"
        End If
        If (myDataView IsNot Nothing) AndAlso (Not (myDataView.RowFilter = SelectString)) Then
          myDataView.RowFilter = SelectString

          SetAllCostsColumn()
        End If

      Catch ex As Exception
        Try
          SelectString = "true"
          myDataView.RowFilter = SelectString

          SetAllCostsColumn()
        Catch ex_inner As Exception
        End Try

      End Try


      Me.Label_Status.Text = "(" & myDataView.Count.ToString & " Records) " & SelectString

    End If

  End Sub

  ''' <summary>
  ''' Sets all costs column.
  ''' </summary>
  Private Sub SetAllCostsColumn()
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Try
      If (Grid_Transactions.Rows.Count <= 1) Then ' first Row is heading.
        Exit Sub
      End If
    Catch ex As Exception
    End Try

    Try
      Dim Counter As Integer
      Dim DataRow As RenaissanceDataClass.DSSelectTransaction.tblSelectTransactionRow
      Dim FeeTransaction As RenaissanceDataClass.DSTransaction.tblTransactionRow = Nothing
      Dim CostsIndex As Integer = Grid_Transactions.Cols.IndexOf("TransactionCosts")
      Dim AllCostsIndex As Integer = Grid_Transactions.Cols.IndexOf("AllCosts")

      For Counter = 1 To (Grid_Transactions.Rows.Count - 1)
        Try
          DataRow = CType(CType(Grid_Transactions.Rows(Counter).DataSource, DataRowView).Row, RenaissanceDataClass.DSSelectTransaction.tblSelectTransactionRow)

          If (DataRow.TransactionRedemptionID = 0) OrElse (DataRow.TransactionType = TransactionTypes.Subscribe) OrElse (DataRow.TransactionType = TransactionTypes.Redeem) Then
            'Grid_Transactions.Rows(Counter)(AllCostsIndex) = Grid_Transactions.Rows(Counter)(CostsIndex)
            DataRow.AllCosts = Grid_Transactions.Rows(Counter)(CostsIndex)
          Else
            FeeTransaction = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblTransaction, DataRow.TransactionRedemptionID), RenaissanceDataClass.DSTransaction.tblTransactionRow)

            DataRow.AllCosts = Grid_Transactions.Rows(Counter)(CostsIndex) + (FeeTransaction.TransactionSignedUnits * FeeTransaction.TransactionPrice)
          End If

        Catch ex As Exception
          'Grid_Transactions.Rows(Counter)(AllCostsIndex) = Grid_Transactions.Rows(Counter)(CostsIndex)
        End Try


      Next


      ' More_Code_Here()

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in SetAllCostsColumn()", ex.StackTrace, True)
    End Try
  End Sub

  ''' <summary>
  ''' Sets the instrument type arrays.
  ''' </summary>
  Private Sub SetInstrumentTypeArrays()
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Try
      Dim InstrumentTypeDS As RenaissanceDataClass.DSInstrumentType
      Dim InstrumentTypeRows() As RenaissanceDataClass.DSInstrumentType.tblInstrumentTypeRow
      Dim Counter As Integer

      Dim ListFutures As New ArrayList()
      Dim ListOptions As New ArrayList()
      Dim ListBoth As New ArrayList()

      InstrumentTypeDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblInstrumentType)

      If (InstrumentTypeDS IsNot Nothing) Then
        InstrumentTypeRows = InstrumentTypeDS.tblInstrumentType.Select("(InstrumentTypeFuturesStylePricing<>0) OR (InstrumentTypeHasDelta<>0)")

        If (InstrumentTypeRows IsNot Nothing) AndAlso (InstrumentTypeRows.Length > 0) Then
          For Counter = 0 To (InstrumentTypeRows.Length - 1)
            ListBoth.Add(InstrumentTypeRows(Counter).InstrumentTypeID)

            If (InstrumentTypeRows(Counter).InstrumentTypeFuturesStylePricing) Then
              ListFutures.Add(InstrumentTypeRows(Counter).InstrumentTypeID)
            End If

            If (InstrumentTypeRows(Counter).InstrumentTypeHasDelta) Then
              ListOptions.Add(InstrumentTypeRows(Counter).InstrumentTypeID)
            End If
          Next
        End If
      End If

      InstrumentType_Futures = ListFutures.ToArray(GetType(Integer))
      InstrumentType_Options = ListOptions.ToArray(GetType(Integer))
      InstrumentType_FuturesAndOptions = ListBoth.ToArray(GetType(Integer))

    Catch ex As Exception
    End Try

  End Sub

  ' Check User permissions
  ''' <summary>
  ''' Checks the permissions.
  ''' </summary>
  Private Sub CheckPermissions()
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

  End Sub

  ''' <summary>
  ''' Basic event handler, called when a form value is changed.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' *******************************************************************************
    ' In the event of one of the Form Controls changing, refresh the data grid.
    ' *******************************************************************************

    If (InPaint = False) Then

      If sender Is Combo_TransactionFund Then
        Call SetInstrumentCombo()
      End If

      Call SetSortedRows()

      Call Grid_Transactions_SelChange(Nothing, Nothing) ' Redraw Trade Details, as changing the selection criteria may not cause a 'SelChange' event.
    End If
  End Sub

  ''' <summary>
  ''' Trades the details changed.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub TradeDetailsChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' *******************************************************************************
    ' 
    ' *******************************************************************************

    If (InPaint = False) AndAlso (CurrentTransactionID > 0) Then

      TransactionChanged = True

      btnCancel.Enabled = True
      btnSave.Enabled = True

      btnCancel.Visible = True
      btnSave.Visible = True
      Button_SaveToExecution.Visible = False
    End If
  End Sub

  ''' <summary>
  ''' Handles the KeyUp event of the Combo_FieldSelect control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
  Private Sub Combo_FieldSelect_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Combo_Select1_Value.KeyUp
    ' *******************************************************************************
    ' In the event of one of the Form Controls changing, refresh the data grid.
    ' *******************************************************************************

    If InPaint = False Then

      Call SetSortedRows()

    End If
  End Sub


  ''' <summary>
  ''' Builds the field select combos.
  ''' </summary>
  Private Sub BuildFieldSelectCombos()
    ' *********************************************************************************
    ' Build the FieldSelect combo boxes.
    '
    ' Field Names starting with 'Transaction' are modified to start with '.'
    ' This is done in order to make the name more readable.
    '
    ' *********************************************************************************

    Dim ColumnName As String
    Dim thisColumn As DataColumn

    Combo_FieldSelect1.Items.Clear()
    Combo_Select1_Value.Items.Clear()
    Combo_FieldSelect1.Items.Add("")

    For Each thisColumn In myTable.Columns
      Try
        ColumnName = thisColumn.ColumnName
        If (ColumnName.StartsWith("Transaction")) Then
          ColumnName = ColumnName.Substring(11) & "."
        End If

        Combo_FieldSelect1.Items.Add(ColumnName)

      Catch ex As Exception
      End Try
    Next

  End Sub

  ''' <summary>
  ''' Updates the selected value combo.
  ''' </summary>
  ''' <param name="pFieldName">Name of the p field.</param>
  ''' <param name="pValueCombo">The p value combo.</param>
  Private Sub UpdateSelectedValueCombo(ByVal pFieldName As String, ByRef pValueCombo As ComboBox)
    ' *******************************************************************************
    ' Build a Value Combo Box appropriate to the chosen DataTable Select Field.
    '
    ' By default the combo will contain all existing values for the chosen field, however
    ' If a relationship is defined for thi table and field, then a Combo will be built
    ' which reflects this relationship.
    ' e.g. the tblReferentialIntegrity table defined a relationship between the
    ' TransactionCounterparty field and the tblCounterparty.Counterparty field, thus if
    ' the chosen Select field ti 'Counterparty.' then the Value combo will be built using
    ' the CounterpartyID & CounterpartyName details from the tblCounterparty table.
    '
    ' *******************************************************************************

    Dim tblRefIntegrity As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityDataTable
    Dim IntegrityRows() As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityRow
    Dim TransactionDataset As DataSet
    Dim TransactionTable As DataTable
    Dim SortOrder As Boolean

    SortOrder = True

    ' Clear the Value Combo.

    Try
      pValueCombo.DataSource = Nothing
      pValueCombo.DisplayMember = ""
      pValueCombo.ValueMember = ""
      pValueCombo.Items.Clear()
    Catch ex As Exception
    End Try

    ' By default, make the Combo a Standard Dropdown, so that non-list items can be entered.

    pValueCombo.DropDownStyle = ComboBoxStyle.DropDown

    ' Exit if no FieldName is given (having cleared the combo).

    If (pFieldName.Length <= 0) Then Exit Sub

    ' Adjust the Field name if appropriate.

    If (pFieldName.EndsWith(".")) Then
      pFieldName = "Transaction" & pFieldName.Substring(0, pFieldName.Length - 1)
    End If

    ' Get a handle to the Standard Transactions Table.
    ' This is so that the field types can be determined and used.

    Try
      TransactionDataset = MainForm.Load_Table(RenaissanceStandardDatasets.tblSelectTransaction, False)
      TransactionTable = myDataset.Tables(0)
    Catch ex As Exception
      Exit Sub
    End Try

    ' If the selected field is a Date then sort the selection combo in reverse order.

    If (TransactionTable.Columns(pFieldName).DataType Is GetType(System.DateTime)) Then
      SortOrder = False
    End If

    ' Get a handle to the Referential Integrity table and the row matching this table and field.
    ' this table defines a relationship between Transaction Table fields and other tables.
    ' This information can be used to build more meaningfull Value Combos.

    Try
      tblRefIntegrity = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblReferentialIntegrity, False), RenaissanceDataClass.DSReferentialIntegrity).tblReferentialIntegrity
      IntegrityRows = tblRefIntegrity.Select("(FeedsTable LIKE '%tblSelectTransaction') AND (FeedsField = '" & pFieldName & "')", "RN")
      If (IntegrityRows.Length <= 0) Then
        GoTo StandardExit
      End If

    Catch ex As Exception
      Exit Sub
    End Try

    If (IntegrityRows(0).IsDescriptionFieldNull) Then
      ' No Linked Description Field

      GoTo StandardExit
    End If

    ' OK, a referential record exists.
    ' Determine the Table and Field Names to use to build the Value Combo.

    Dim TableName As String
    Dim TableField As String
    Dim DescriptionField As String
    Dim stdDS As StandardDataset
    Dim thisChangeID As RenaissanceChangeID

    Try

      TableName = IntegrityRows(0).TableName
      TableField = IntegrityRows(0).TableField
      DescriptionField = IntegrityRows(0).DescriptionField

      thisChangeID = CType(System.Enum.Parse(GetType(RenaissanceChangeID), TableName), RenaissanceChangeID)

      stdDS = RenaissanceStandardDatasets.GetStandardDataset(thisChangeID)

      ' Build the appropriate Values Combo.

      Call MainForm.SetTblGenericCombo( _
      pValueCombo, _
      stdDS, _
      DescriptionField, _
      TableField, _
      "", False, SortOrder, True)    ' 

      ' For Referential Integrity generated combo's, make the Combo a 
      ' DropDownList

      pValueCombo.DropDownStyle = ComboBoxStyle.DropDownList

    Catch ex As Exception
      GoTo StandardExit

    End Try

    Exit Sub

StandardExit:

    ' Build the standard Combo, just use the values from the given field.

    Try

      Call MainForm.SetTblGenericCombo( _
       pValueCombo, _
       RenaissanceStandardDatasets.tblSelectTransaction, _
       pFieldName, _
       pFieldName, _
       "", True, SortOrder, False)    ' 

      MainForm.ClearComboSelection(pValueCombo)

    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Gets the field change ID.
  ''' </summary>
  ''' <param name="pFieldName">Name of the p field.</param>
  ''' <returns>RenaissanceChangeID.</returns>
  Private Function GetFieldChangeID(ByVal pFieldName As String) As RenaissanceChangeID
    ' ***************************************************************************************
    '
    '
    ' ***************************************************************************************

    Dim tblRefIntegrity As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityDataTable
    Dim IntegrityRows() As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityRow

    If (pFieldName.EndsWith(".")) Then
      pFieldName = "Transaction" & pFieldName.Substring(0, pFieldName.Length - 1)
    End If

    Try
      tblRefIntegrity = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblReferentialIntegrity, False), RenaissanceDataClass.DSReferentialIntegrity).tblReferentialIntegrity
      IntegrityRows = tblRefIntegrity.Select("(FeedsTable LIKE '%tblSelectTransaction') AND (FeedsField = '" & pFieldName & "')", "RN")
      If (IntegrityRows.Length <= 0) Then
        Return RenaissanceChangeID.None
        Exit Function
      End If

    Catch ex As Exception
      Return RenaissanceChangeID.None
      Exit Function
    End Try


    If (IntegrityRows(0).IsDescriptionFieldNull) Then
      Return RenaissanceChangeID.None
      Exit Function
    End If

    ' OK, a referential record exists.
    ' Determine the Table and Field Names to use to build the Value Combo.

    Dim TableName As String
    Dim TableField As String
    Dim DescriptionField As String
    Dim thisChangeID As RenaissanceChangeID

    Try

      TableName = IntegrityRows(0).TableName
      TableField = IntegrityRows(0).TableField
      DescriptionField = IntegrityRows(0).DescriptionField

      thisChangeID = CType(System.Enum.Parse(GetType(RenaissanceChangeID), TableName), RenaissanceChangeID)

      Return thisChangeID
      Exit Function
    Catch ex As Exception
    End Try

    Return RenaissanceChangeID.None
    Exit Function

  End Function

  ''' <summary>
  ''' Handles the Click event of the Grid_Transactions control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Transactions_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid_Transactions.Click
    ' ***************************************************************************************
    '
    '
    ' ***************************************************************************************
    'Dim ParentID As Integer

    'Try
    '  ParentID = Me.Grid_Transactions.Rows(Grid_Transactions.RowSel).DataSource("TransactionParentID")
    '  ShowTransactionDetails(ParentID)
    'Catch ex As Exception
    'End Try

  End Sub

  ''' <summary>
  ''' Handles the SelChange event of the Grid_Transactions control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Transactions_SelChange(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid_Transactions.SelChange
    ' ***************************************************************************************
    ' Update displayed transaction.
    '
    ' ***************************************************************************************
    Dim ParentID As Integer

    Try
      If (Me.Visible) Then
        If (Grid_Transactions.RowSel > 0) Then
          ParentID = Me.Grid_Transactions.Rows(Grid_Transactions.RowSel).DataSource("TransactionParentID")
          ShowTransactionDetails(ParentID)
        Else
          ShowTransactionDetails(0)
        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Handles the DoubleClick event of the Grid_Transactions control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Transactions_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid_Transactions.DoubleClick
    ' ***************************************************************************************
    ' Spawn an Add/Edit Transactions form reflecting the chosen selection of transactions
    ' and pointing to the transaction which was double clicked.
    ' ***************************************************************************************

    Dim ParentID As Integer
    Dim thisFormHandle As RenaissanceGlobals.RenaissanceFormHandle

    Try
      ParentID = Me.Grid_Transactions.Rows(Grid_Transactions.RowSel).DataSource("TransactionParentID")

      thisFormHandle = MainForm.New_VeniceForm(VeniceFormID.frmTransaction)
      CType(thisFormHandle.Form, frmTransaction).FormSelectCriteria = GetTransactionSelectString(True)
      CType(thisFormHandle.Form, frmTransaction).MoveToAuditID(ParentID)

    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Handles the SelectedIndexChanged event of the Combo_FieldSelect1 control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_FieldSelect1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_FieldSelect1.SelectedIndexChanged
    ' ***************************************************************************************
    ' Field Select Combo Chosen, Update the associated Values Combo.
    '
    ' ***************************************************************************************

    Try
      If (InPaint = False) Then
        Call UpdateSelectedValueCombo(Me.Combo_FieldSelect1.SelectedItem, Me.Combo_Select1_Value)
        Me.Combo_Select1_Operator.SelectedIndex = 1
      End If
    Catch ex As Exception
    End Try

  End Sub


  ''' <summary>
  ''' Handles the CheckedChanged event of the Check_BookFeesSeparately control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Check_BookFeesSeparately_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_BookFeesSeparately.CheckedChanged
    ' ***************************************************************************************
    '
    ' ***************************************************************************************

    Try
      Combo_FeeInstrument.Visible = Check_BookFeesSeparately.Checked
      Label_FeeSettlement.Visible = Check_BookFeesSeparately.Checked
      Date_FeeSettlement.Visible = Check_BookFeesSeparately.Checked
    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Shows the transaction details.
  ''' </summary>
  ''' <param name="TransactionParentID">The transaction parent ID.</param>
  Private Sub ShowTransactionDetails(ByVal TransactionParentID As Integer)
    ' ***************************************************************************************
    ' Populate Transaction Details area.
    '
    ' ***************************************************************************************
    Dim EnableSave As Boolean = False
    Dim thisTransactionType As TransactionTypes = 0
    Dim thisInstrumentType As InstrumentTypes = 0

    Try
      Dim TransactionRow As RenaissanceDataClass.DSSelectTransaction.tblSelectTransactionRow = Nothing
      Dim InstrumentRow As RenaissanceDataClass.DSInstrument.tblInstrumentRow = Nothing

      If (TransactionChanged) Then

        If (MessageBox.Show("Save Transaction Changes ?", "Save Transaction Changes ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
          Call SaveTransactionChanges()
        End If

      End If

      ' Get Transaction Details

      ' TransactionRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblTransaction, TransactionParentID), RenaissanceDataClass.DSTransaction.tblTransactionRow)
      If (Grid_Transactions.RowSel > 0) Then
        TransactionRow = CType(CType(Grid_Transactions.Rows(Grid_Transactions.RowSel).DataSource, DataRowView).Row, RenaissanceDataClass.DSSelectTransaction.tblSelectTransactionRow)
      End If

      ' Show Transaction Details

      If (TransactionParentID > 0) AndAlso (TransactionRow IsNot Nothing) Then
        InstrumentRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, TransactionRow.TransactionInstrument), RenaissanceDataClass.DSInstrument.tblInstrumentRow)

        thisTransactionType = CType(TransactionRow.TransactionType, TransactionTypes)
        thisInstrumentType = CType(InstrumentRow.InstrumentType, InstrumentTypes)

        SetTransactionCostsInstrumentCombo(InstrumentRow.InstrumentCurrencyID)

        Label_TransactionParentID.Text = TransactionParentID.ToString()
        Label_InstrumentName.Text = InstrumentRow.InstrumentDescription
        Label_OrderType.Text = Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblTransactionType, thisTransactionType, "TransactionType"), "<Unknown>")
        edit_Amount.Value = TransactionRow.TransactionUnits
        edit_Price.Value = TransactionRow.TransactionPrice
        Label_Multiplier.Text = "Contract Size : " & CDbl(InstrumentRow.InstrumentContractSize * InstrumentRow.InstrumentMultiplier).ToString()
        Date_TradeValueDate.Value = TransactionRow.TransactionValueDate
        Date_TradeSettlementDate.Value = TransactionRow.TransactionSettlementDate
        If (Combo_TradeStatus.Items.Count > 0) Then
          Combo_TradeStatus.SelectedValue = TransactionRow.TransactionTradeStatusID
        End If

        ' Tricky Bit...

        If (Combo_SettlementCurrency.Items.Count > 0) Then
          Combo_SettlementCurrency.SelectedValue = InstrumentRow.InstrumentCurrencyID
        End If

        If (thisTransactionType = TransactionTypes.Subscribe) OrElse (thisTransactionType = TransactionTypes.Redeem) Then
          ' Can't use TransactionRedemptionID for Fee info, for Subs & Reds it may be used to indicate Redemption vs specific Subscription. (Used for Fees and Equalisation)

          Check_BookFeesSeparately.Checked = False
          Combo_FeeInstrument.Visible = False
          Label_FeeSettlement.Visible = False
          Date_FeeSettlement.Visible = False

          edit_Fees.Value = TransactionRow.TransactionCosts
          Combo_SettlementCurrency.Enabled = False

        ElseIf (TransactionRow.TransactionRedemptionID > 0) Then

          ' Costs Booked as Expense already.

          Dim FeeTransaction As RenaissanceDataClass.DSTransaction.tblTransactionRow = Nothing
          FeeTransaction = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblTransaction, TransactionRow.TransactionRedemptionID), RenaissanceDataClass.DSTransaction.tblTransactionRow)

          Check_BookFeesSeparately.Checked = True
          Combo_FeeInstrument.Visible = True
          Label_FeeSettlement.Visible = True
          Date_FeeSettlement.Visible = True

          If (FeeTransaction IsNot Nothing) Then
            Combo_FeeInstrument.SelectedValue = FeeTransaction.TransactionInstrument
            edit_Fees.Value = TransactionRow.TransactionCosts + (FeeTransaction.TransactionSignedUnits * FeeTransaction.TransactionPrice)
            Date_FeeSettlement.Value = FeeTransaction.TransactionSettlementDate
          Else
            Combo_FeeInstrument.SelectedValue = CInt(Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblCurrency, InstrumentRow.InstrumentCurrencyID, "DefaultBrokerageExpense"), 0))
            edit_Fees.Value = TransactionRow.TransactionCosts
            Date_FeeSettlement.Value = AddPeriodToDate(DealingPeriod.Daily, TransactionRow.TransactionValueDate, 2)
          End If

          Combo_SettlementCurrency.Enabled = False

        ElseIf (InstrumentType_Futures.Contains(InstrumentRow.InstrumentType)) Then
          ' Costs Not Booked as Expense. Present option if this is a Future-Type instrument.

          Check_BookFeesSeparately.Checked = True
          Combo_FeeInstrument.Visible = True
          Label_FeeSettlement.Visible = True
          Date_FeeSettlement.Visible = True
          Combo_FeeInstrument.SelectedValue = CInt(Nz(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblCurrency, InstrumentRow.InstrumentCurrencyID, "DefaultBrokerageExpense"), 0))
          Date_FeeSettlement.Value = AddPeriodToDate(DealingPeriod.Daily, TransactionRow.TransactionValueDate, 2)
          edit_Fees.Value = TransactionRow.TransactionCosts
          Combo_SettlementCurrency.Enabled = False

        Else
          ' Default. Ordinary Costs.

          Check_BookFeesSeparately.Checked = False
          Combo_FeeInstrument.Visible = False
          Label_FeeSettlement.Visible = False
          Date_FeeSettlement.Visible = False
          edit_Fees.Value = TransactionRow.TransactionCosts
          Combo_SettlementCurrency.Enabled = False
        End If

        Try
          ' EnableSave Save if the transaction is in the PendingConfirmationMO group.

          If LookupTableRow(MainForm, RenaissanceStandardDatasets.tblTradeStatusGroupItems, 0, "", "RN", "(GroupID=" & CInt(RenaissanceGlobals.StatusGroups.PendingConfirmationMO).ToString & ") AND (StatusID=" & CInt(Nz(TransactionRow.TransactionTradeStatusID, 0)).ToString & ")") IsNot Nothing Then
            If (Combo_TradeStatus.Items.Count > 0) Then
              Combo_TradeStatus.SelectedValue = TradeStatus.ValidatedMO
              EnableSave = True
            End If
          End If

        Catch ex As Exception
        End Try

      Else
        ' Clear

        Label_TransactionParentID.Text = "."
        Label_InstrumentName.Text = "."
        Label_OrderType.Text = ""
        edit_Amount.Value = 0
        edit_Price.Value = 0
        Label_Multiplier.Text = ""
        edit_Fees.Value = 0
        If (Combo_SettlementCurrency.Items.Count > 0) Then
          Combo_SettlementCurrency.SelectedIndex = 0
        End If
        Check_BookFeesSeparately.Checked = False
        Combo_FeeInstrument.Visible = False
        Label_FeeSettlement.Visible = False
        Date_FeeSettlement.Visible = False
        Date_TradeValueDate.Value = Now.Date()
        Date_TradeSettlementDate.Value = Now.Date()
        If (Combo_TradeStatus.Items.Count > 0) Then
          Combo_TradeStatus.SelectedIndex = 0
        End If
      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in ShowTransactionDetails()", ex.StackTrace, True)
    Finally
      TransactionChanged = False
      btnSave.Enabled = (False Or EnableSave)
      btnCancel.Enabled = False

      'btnCancel.Visible = False
      'btnSave.Visible = False

      ' Only Enable Administrator file button for valid instrument types.

      Select Case thisInstrumentType

        Case InstrumentTypes.Cash, InstrumentTypes.Fee, InstrumentTypes.Expense, InstrumentTypes.Forward, InstrumentTypes.Interest, InstrumentTypes.Loan, InstrumentTypes.Margin, InstrumentTypes.Notional, InstrumentTypes.UnrealisedPnL

          Button_SaveToExecution.Visible = False

        Case Else

          Button_SaveToExecution.Visible = Not btnSave.Enabled

      End Select

      CurrentTransactionID = TransactionParentID
    End Try

  End Sub

  ''' <summary>
  ''' Handles the Click event of the btnCancel control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Try
      TransactionChanged = False
      Call ShowTransactionDetails(CurrentTransactionID)
    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Handles the Click event of the btnSave control.
  ''' </summary>
  ''' <param name="sender">The source of the event.</param>
  ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Try

      Call SaveTransactionChanges()

    Catch ex As Exception
    End Try
  End Sub


  ''' <summary>
  ''' Saves the transaction changes.
  ''' </summary>
  Private Sub SaveTransactionChanges()
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Try
      If (Me.Disposing) OrElse (Me.IsDisposed) OrElse (CurrentTransactionID <= 0) Then ' Don't require TransactionChanged=True so that when the 'PendingDerivativeMO' status is set to 'ValiatedMO' as a default the user is not always prompted to save.
        Exit Sub
      End If
    Catch ex As Exception
    End Try

    Try
      Dim Permissions As Integer = MainForm.CheckPermissions(Me.Name, PermissionFeatureType.TypeForm)

      If ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) = 0) AndAlso ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) = 0) Then
        MessageBox.Show("You do not have Permissions to change transactions from this form.")
        Exit Sub
      End If
    Catch ex As Exception
      Exit Sub
    End Try

    Try
      Dim TransactionDS As RenaissanceDataClass.DSTransaction
      Dim TransactionTable As RenaissanceDataClass.DSTransaction.tblTransactionDataTable
      Dim TransactionRow As RenaissanceDataClass.DSTransaction.tblTransactionRow = Nothing
      Dim FeeRow As RenaissanceDataClass.DSTransaction.tblTransactionRow = Nothing
      Dim FeeInstrumentRow As RenaissanceDataClass.DSInstrument.tblInstrumentRow = Nothing
      Dim TransactionRowChanged As Boolean = False
      Dim FeeRowNew As Boolean = False
      Dim FeeRowChanged As Boolean = False
      Dim UseFeeRow As Boolean = False
      Dim TransactionParentID As Integer = CurrentTransactionID
      Dim FeeTransactionID As Integer = 0

      Dim Trans_Units As Double = edit_Amount.Value
      Dim Trans_Price As Double = edit_Price.Value
      Dim Trans_Fees As Double = edit_Fees.Value
      Dim Fee_Instrument As Integer = 0
      Dim ValueDate As Date = Date_TradeValueDate.Value
      Dim SettlementDate As Date = Date_TradeSettlementDate.Value
      Dim FeeSettlementDate As Date = Date_FeeSettlement.Value
      Dim Trade_Stataus As Integer = 0

      If (Combo_FeeInstrument.SelectedValue IsNot Nothing) AndAlso (IsNumeric(Combo_FeeInstrument.SelectedValue)) Then
        Fee_Instrument = CInt(Combo_FeeInstrument.SelectedValue)

        FeeInstrumentRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, Fee_Instrument), RenaissanceDataClass.DSInstrument.tblInstrumentRow)

        ' Don't Save to Fee Instrument if Instrument is not found or is not an Expense
        If (FeeInstrumentRow Is Nothing) Then
          Fee_Instrument = 0
        ElseIf (FeeInstrumentRow.InstrumentType <> InstrumentTypes.Expense) Then
          Fee_Instrument = 0
        End If
      End If

      If (Combo_TradeStatus.SelectedValue IsNot Nothing) AndAlso (IsNumeric(Combo_TradeStatus.SelectedValue)) Then
        Trade_Stataus = CInt(Combo_TradeStatus.SelectedValue)
      End If

      ' OK, Get Current Transaction And Referenced Fee transaction if Appropriate.

      TransactionDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblTransaction)
      TransactionRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblTransaction, TransactionParentID), RenaissanceDataClass.DSTransaction.tblTransactionRow)

      If (TransactionDS Is Nothing) OrElse (TransactionRow Is Nothing) Then
        MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "Error, Could not load the Transactions table or else find this transaction : " & TransactionParentID.ToString(), "", "", True)

        ' Can't do anything if the Transaction row does not exist. Exit.
        Exit Sub
      End If

      ' Get Existing Fee Row (If any)

      If (TransactionRow.TransactionRedemptionID > 0) AndAlso (TransactionRow.TransactionType <> TransactionTypes.Subscribe) AndAlso (TransactionRow.TransactionType <> TransactionTypes.Redeem) Then
        FeeRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblTransaction, TransactionRow.TransactionRedemptionID), RenaissanceDataClass.DSTransaction.tblTransactionRow)
      End If

      ' Should we use a separate fee transaction ?
      ' IF : Check Box checked, Fee Instrument Selected and Not a subscription or Redemption.

      If (Check_BookFeesSeparately.Checked) AndAlso (Fee_Instrument > 0) AndAlso (TransactionRow.TransactionType <> TransactionTypes.Subscribe) AndAlso (TransactionRow.TransactionType <> TransactionTypes.Redeem) Then
        UseFeeRow = True

        If (FeeRow Is Nothing) AndAlso (Trans_Fees <> 0) Then
          FeeRowNew = True
        End If
      End If

      ' Has the transaction Changed ?

      If (Math.Abs(TransactionRow.TransactionUnits - Trans_Units) > 0.000000001) Then
        TransactionRowChanged = True
      ElseIf (Math.Abs(TransactionRow.TransactionPrice - Trans_Price) > 0.000000001) Then
        TransactionRowChanged = True
      ElseIf (UseFeeRow = False) AndAlso (Math.Abs(TransactionRow.TransactionCosts - Trans_Fees) > 0.000000001) Then
        TransactionRowChanged = True
      ElseIf (UseFeeRow) AndAlso (Math.Abs(TransactionRow.TransactionCosts) > 0) Then
        TransactionRowChanged = True
      ElseIf (TransactionRow.TransactionValueDate.CompareTo(ValueDate) <> 0) Then
        TransactionRowChanged = True
      ElseIf (TransactionRow.TransactionSettlementDate.CompareTo(SettlementDate) <> 0) Then
        TransactionRowChanged = True
      ElseIf (Trade_Stataus > 0) AndAlso (TransactionRow.TransactionTradeStatusID <> Trade_Stataus) Then
        TransactionRowChanged = True
      End If
      If (UseFeeRow = False) AndAlso (TransactionRow.TransactionRedemptionID > 0) AndAlso (TransactionRow.TransactionType <> TransactionTypes.Subscribe) AndAlso (TransactionRow.TransactionType <> TransactionTypes.Redeem) Then
        ' If this Transaction has a Fee Reference, but should not, then flag to clear it.
        TransactionRowChanged = True
      End If

      ' Has the Fee Row Changed ?

      If (FeeRow IsNot Nothing) Then
        If (UseFeeRow) Then

          If (FeeRow.TransactionFund <> TransactionRow.TransactionFund) Then ' Fund
            FeeRowChanged = True
          ElseIf (Math.Abs(Math.Abs(FeeRow.TransactionUnits) - Math.Abs(Trans_Fees)) > 0.000000001) Then ' Amount
            FeeRowChanged = True
          ElseIf (Trans_Fees > 0) AndAlso (FeeRow.TransactionPrice <> 1) Then ' Price
            FeeRowChanged = True
          ElseIf (Trans_Fees < 0) AndAlso (FeeRow.TransactionPrice <> (-1)) Then ' Price
            FeeRowChanged = True
          ElseIf (FeeRow.TransactionInstrument <> Fee_Instrument) Then ' Instrument 
            FeeRowChanged = True
          ElseIf (FeeRow.TransactionSettlementDate.CompareTo(FeeSettlementDate) <> 0) Then ' SettlementDate 
            FeeRowChanged = True
          ElseIf (FeeRow.TransactionValueDate.CompareTo(ValueDate) <> 0) Then ' Date
            FeeRowChanged = True
          ElseIf (FeeRow.TransactionSettlementDate.CompareTo(SettlementDate) <> 0) Then ' Date
            FeeRowChanged = True
          ElseIf (FeeInstrumentRow IsNot Nothing) AndAlso (FeeInstrumentRow.InstrumentCurrencyID <> FeeRow.TransactionSettlementCurrencyID) Then ' Currency
            FeeRowChanged = True
          End If
        Else
          ' Is a Fee Row, but should not be.
          FeeRowChanged = True
        End If
      End If

      TransactionTable = TransactionDS.tblTransaction

      If (TransactionRowChanged) OrElse (FeeRowNew) OrElse (FeeRowChanged) Then

        SyncLock TransactionTable

          ' Do Fee first, so we can reference it in the main transaction...

          If (FeeRowNew) Then
            FeeRow = TransactionTable.NewtblTransactionRow
          End If

          If (FeeRow IsNot Nothing) Then
            FeeRow.BeginEdit()

            Try
              If (FeeRowNew) Then
                ' Background Stuff.
                FeeRow.TransactionInvestment = ""
                FeeRow.TransactionExecution = ""
                FeeRow.TransactionDataEntry = ""
                FeeRow.TransactionDecisionDate = Now.Date()
                FeeRow.TransactionConfirmationDate = TransactionRow.TransactionConfirmationDate
                FeeRow.TransactionFinalAmount = 0
                FeeRow.TransactionFinalCosts = 0
                FeeRow.TransactionFinalDataEntry = 0
                FeeRow.TransactionFinalPrice = 0
                FeeRow.TransactionFinalSettlement = 0
                FeeRow.TransactionInstructionFlag = 0
                FeeRow.TransactionRedeemWholeHoldingFlag = False
                FeeRow.TransactionBankConfirmation = 0
                FeeRow.TransactionInitialDataEntry = 0
                FeeRow.TransactionAmountConfirmed = 0
                FeeRow.TransactionSettlementConfirmed = 0
                FeeRow.TransactionFinalDataEntry = 0
                FeeRow.TransactionVersusInstrument = 0
              End If

              If (UseFeeRow) Then
                FeeRow.TransactionValueorAmount = "Amount"
                FeeRow.TransactionFund = TransactionRow.TransactionFund
                FeeRow.TransactionInstrument = Fee_Instrument

                If (Trans_Fees >= 0) Then
                  FeeRow.TransactionType = RenaissanceGlobals.TransactionTypes.Expense_Increase
                Else
                  FeeRow.TransactionType = RenaissanceGlobals.TransactionTypes.Expense_Reduce
                End If

                FeeRow.TransactionCounterparty = RenaissanceGlobals.Counterparty.MARKET
                FeeRow.TransactionUnits = Math.Abs(Trans_Fees)
                FeeRow.TransactionPrice = 1
                'If (Trans_Fees >= 0) Then
                '  FeeRow.TransactionPrice = 1
                'Else
                '  FeeRow.TransactionPrice = (-1)
                'End If
                FeeRow.TransactionValueDate = ValueDate
                FeeRow.TransactionSettlementDate = FeeSettlementDate
                FeeRow.TransactionTradeStatusID = RenaissanceGlobals.TradeStatus.Reconciled
                FeeRow.TransactionRedemptionID = 0
                FeeRow.TransactionCosts = 0
                FeeRow.TransactionCostPercent = 0
                FeeRow.TransactionCostIsPercent = False
                FeeRow.TransactionFXRate = 1
                FeeRow.TransactionSettlementCurrencyID = FeeInstrumentRow.InstrumentCurrencyID
                FeeRow.TransactionComment = "Fees related to transaction " & TransactionRow.TransactionParentID.ToString()
                FeeRow.TransactionGroup = TransactionRow.TransactionGroup
                FeeRow.TransactionIsTransfer = False
                FeeRow.TransactionEffectivePrice = FeeRow.TransactionPrice
                FeeRow.TransactionEffectiveValueDate = FeeRow.TransactionValueDate
                FeeRow.TransactionSpecificInitialEqualisationFlag = False
                FeeRow.TransactionSpecificInitialEqualisationValue = 0
                FeeRow.TransactionEffectiveWatermark = 0
                FeeRow.TransactionExemptFromUpdate = True
                FeeRow.TransactionCTFLAGS = 0
                FeeRow.TransactionCompoundRN = 0
              Else
                FeeRow.TransactionUnits = 0
                FeeRow.TransactionCosts = 0
                FeeRow.TransactionCostPercent = 0
                FeeRow.TransactionExemptFromUpdate = True
                FeeRow.TransactionComment = "Old Fees transaction. Now cleared."
              End If

            Catch ex As Exception
            Finally
              FeeRow.EndEdit()
            End Try

            If (FeeRowNew) Then
              TransactionTable.Rows.Add(FeeRow)
            End If

            MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblTransaction, New DataRow() {FeeRow})

            FeeTransactionID = FeeRow.TransactionParentID

          End If ' (FeeRow IsNot Nothing) Then

          ' Update Transaction.

          If (TransactionRowChanged) OrElse (FeeTransactionID > 0) Then

            If (TransactionRowChanged = False) AndAlso (FeeTransactionID > 0) Then
              ' No changes to the Transaction itself, perhaps a new Fee Transaction ?

              If (TransactionRow.TransactionRedemptionID <> FeeTransactionID) Then

                Try
                  TransactionRow.BeginEdit()

                  If (UseFeeRow) Then
                    TransactionRow.TransactionRedemptionID = FeeTransactionID
                  ElseIf (TransactionRow.TransactionType <> TransactionTypes.Subscribe) AndAlso (TransactionRow.TransactionType <> TransactionTypes.Redeem) Then
                    TransactionRow.TransactionRedemptionID = 0
                  End If

                Catch ex As Exception
                Finally
                  TransactionRow.EndEdit()
                End Try

                MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblTransaction, New DataRow() {TransactionRow})

              End If

            Else
              ' Transaction has changed.

              Try
                TransactionRow.BeginEdit()

                TransactionRow.TransactionUnits = Trans_Units
                TransactionRow.TransactionPrice = Trans_Price
                TransactionRow.TransactionValueDate = ValueDate
                TransactionRow.TransactionSettlementDate = SettlementDate

                If (Trade_Stataus > 0) Then
                  TransactionRow.TransactionTradeStatusID = Trade_Stataus
                End If

                If (UseFeeRow) Then
                  ' If using a fee Row, Costs are Nil on this transaction.

                  TransactionRow.TransactionRedemptionID = FeeTransactionID
                  TransactionRow.TransactionCosts = 0
                  TransactionRow.TransactionCostPercent = 0
                  TransactionRow.TransactionCostIsPercent = False
                ElseIf (TransactionRow.TransactionType <> TransactionTypes.Subscribe) AndAlso (TransactionRow.TransactionType <> TransactionTypes.Redeem) Then
                  ' NOT using a fee row and NOT a Subscription / Redemption, then RedemptionID = 0 and Costs are set.

                  TransactionRow.TransactionRedemptionID = 0
                  TransactionRow.TransactionCosts = Trans_Fees
                  TransactionRow.TransactionCostPercent = 0
                  TransactionRow.TransactionCostIsPercent = False
                Else
                  ' NOT using a fee row and IS a Subscription / Redemption, then RedemptionID = <Unchanged> and Costs are set.

                  TransactionRow.TransactionCosts = Trans_Fees
                  TransactionRow.TransactionCostPercent = 0
                  TransactionRow.TransactionCostIsPercent = False
                End If

              Catch ex As Exception
              Finally
                TransactionRow.EndEdit()
              End Try

              MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblTransaction, New DataRow() {TransactionRow})

            End If

          End If

        End SyncLock

        TransactionChanged = False

        ShowTransactionDetails(TransactionParentID)

        ' Propogate changes

        Dim UpdateDetails As String

        If (FeeTransactionID > 0) Then
          UpdateDetails = FeeTransactionID.ToString() & "," & TransactionParentID.ToString()
        Else
          UpdateDetails = TransactionParentID.ToString()
        End If
        Call MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceStandardDatasets.tblTransaction.ChangeID, UpdateDetails), True)

      Else ' NOT  (TransactionRowChanged) OrElse (FeeRowNew) OrElse (FeeRowChanged) Then

        TransactionChanged = False

        ShowTransactionDetails(TransactionParentID)

      End If '  (TransactionRowChanged) OrElse (FeeRowNew) OrElse (FeeRowChanged) Then


    Catch ex As Exception
    End Try

  End Sub


#End Region

#Region " Set Form Combos (Form Specific Code) "

  ''' <summary>
  ''' Sets the fund combo.
  ''' </summary>
  Private Sub SetFundCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_TransactionFund, _
    RenaissanceStandardDatasets.tblFund, _
    "FundName", _
    "FundID", _
    "FundClosed=0", False, True, True)   ' 

  End Sub

  ''' <summary>
  ''' Sets the currency combo.
  ''' </summary>
  Private Sub SetCurrencyCombo()
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_SettlementCurrency, _
    RenaissanceStandardDatasets.tblCurrency, _
    "CurrencyCode", _
    "CurrencyID", _
    "")   ' 

  End Sub

  ''' <summary>
  ''' Sets the trade status combo.
  ''' </summary>
  Private Sub SetTradeStatusCombo()
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_TradeStatus, _
    RenaissanceStandardDatasets.tblTradeStatus, _
    "tradeStatusName", _
    "tradeStatusID", _
    "")   ' 

  End Sub

  ''' <summary>
  ''' Sets the instrument combo.
  ''' </summary>
  Private Sub SetInstrumentCombo()

    Try
      Dim FundID As Integer = 0
      If (Combo_TransactionFund.SelectedIndex >= 0) AndAlso (CInt(Nz(Combo_TransactionFund.SelectedValue, 0)) > 0) Then

        FundID = CInt(Combo_TransactionFund.SelectedValue)

        Call MainForm.SetTblGenericCombo( _
          Me.Combo_TransactionInstrument, _
          RenaissanceStandardDatasets.tblSelectTransaction, _
          "InstrumentDescription", _
          "TransactionInstrument", _
          "TransactionFund=" & FundID.ToString, True, True, True, 0)    ' 
      Else
        'Call MainForm.SetTblGenericCombo( _
        '  Me.Combo_TransactionInstrument, _
        '  RenaissanceStandardDatasets.tblInstrument, _
        '  "InstrumentDescription", _
        '  "InstrumentID", _
        '  "", False, True, True)    ' 
        Call MainForm.SetTblGenericCombo( _
          Me.Combo_TransactionInstrument, _
          RenaissanceStandardDatasets.tblSelectTransaction, _
          "InstrumentDescription", _
          "TransactionInstrument", _
          "", True, True, True, 0)    ' 
      End If

    Catch ex As Exception
    End Try

  End Sub

  ''' <summary>
  ''' Sets the transaction costs instrument combo.
  ''' </summary>
  ''' <param name="pCurrencyID">The p currency ID.</param>
  Private Sub SetTransactionCostsInstrumentCombo(Optional ByVal pCurrencyID As Integer = 0)

    Try

      If (pCurrencyID <= 0) Then

        Call MainForm.SetTblGenericCombo( _
          Me.Combo_FeeInstrument, _
          RenaissanceStandardDatasets.tblInstrument, _
          "InstrumentDescription", _
          "InstrumentID", _
          "InstrumentType=" & CInt(RenaissanceGlobals.InstrumentTypes.Expense).ToString, False, True, True, 0, "") ' 

      Else

        Call MainForm.SetTblGenericCombo( _
          Me.Combo_FeeInstrument, _
          RenaissanceStandardDatasets.tblInstrument, _
          "InstrumentDescription", _
          "InstrumentID", _
          "(InstrumentType=" & CInt(RenaissanceGlobals.InstrumentTypes.Expense).ToString & ") AND (InstrumentCurrencyID=" & pCurrencyID.ToString & ")", False, True, True, 0, "") ' 

      End If

    Catch ex As Exception
    End Try
  End Sub

  ''' <summary>
  ''' Sets the trade status group combo.
  ''' </summary>
  Private Sub SetTradeStatusGroupCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_TradeStatusGroup, _
    RenaissanceStandardDatasets.tblTradeStatusGroup, _
    "TradeStatusGroupName", _
    "TradeStatusGroupID", _
    "", False, True, True)    ' 

  End Sub


#End Region

#Region " Grid Fields Menu"


  ''' <summary>
  ''' Sets the field select menu.
  ''' </summary>
  ''' <param name="RootMenu">The root menu.</param>
  ''' <returns>ToolStripMenuItem.</returns>
  Private Function SetFieldSelectMenu(ByRef RootMenu As MenuStrip) As ToolStripMenuItem
    ' ***************************************************************************************
    ' Build the FieldSelect Menu.
    '
    ' Add an item for each field in the SelectTransactions table, when selected the item
    ' will show or hide the associated field on the transactions grid.
    ' ***************************************************************************************

    Dim ColumnNames(-1) As String
    Dim ColumnCount As Integer

    Dim FieldsMenu As New ToolStripMenuItem("Grid &Fields")
    FieldsMenu.Name = "Menu_GridField"

    Dim newMenuItem As ToolStripMenuItem

    ReDim ColumnNames(myTable.Columns.Count - 1)
    For ColumnCount = 0 To (myTable.Columns.Count - 1)
      ColumnNames(ColumnCount) = myTable.Columns(ColumnCount).ColumnName
      If (ColumnNames(ColumnCount).StartsWith("Transaction")) Then
        ColumnNames(ColumnCount) = ColumnNames(ColumnCount).Substring(11) & "."
      End If
    Next
    Array.Sort(ColumnNames)

    For ColumnCount = 0 To (ColumnNames.Length - 1)
      newMenuItem = FieldsMenu.DropDownItems.Add(ColumnNames(ColumnCount), Nothing, AddressOf Me.ShowGridField)

      If (ColumnNames(ColumnCount).EndsWith(".")) Then
        newMenuItem.Name = "Menu_GridField_Transaction" & ColumnNames(ColumnCount).Substring(0, ColumnNames(ColumnCount).Length - 1)
      Else
        newMenuItem.Name = "Menu_GridField_" & ColumnNames(ColumnCount)
      End If
    Next

    RootMenu.Items.Add(FieldsMenu)
    Return FieldsMenu

  End Function

  ''' <summary>
  ''' Shows the grid field.
  ''' </summary>
  ''' <param name="sender">The sender.</param>
  ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
  Private Sub ShowGridField(ByVal sender As Object, ByVal e As EventArgs)
    ' ***************************************************************************************
    ' Callback function for the Grid Fields Menu items.
    '
    ' ***************************************************************************************

    Try
      If TypeOf (sender) Is ToolStripMenuItem Then
        Dim thisMenuItem As ToolStripMenuItem
        Dim FieldName As String

        thisMenuItem = CType(sender, ToolStripMenuItem)

        FieldName = thisMenuItem.Name
        If (FieldName.StartsWith("Menu_GridField_")) Then
          FieldName = FieldName.Substring(15)
        End If

        thisMenuItem.Checked = Not thisMenuItem.Checked

        If (thisMenuItem.Checked) Then
          Grid_Transactions.Cols(FieldName).Visible = True
        Else
          Grid_Transactions.Cols(FieldName).Visible = False
        End If
      End If
    Catch ex As Exception

    End Try
  End Sub

#End Region

#Region " Bug hunting "

  ' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
  ' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
  ' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

  ''' <summary>
  ''' Called when [row updating].
  ''' </summary>
  ''' <param name="Sender">The sender.</param>
  ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  ''' <summary>
  ''' Called when [row updated].
  ''' </summary>
  ''' <param name="Sender">The sender.</param>
  ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  ''' <summary>
  ''' Called when [row fill error].
  ''' </summary>
  ''' <param name="Sender">The sender.</param>
  ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
    Dim a As Integer

    a = 1
  End Sub

#End Region



  Private Sub Button_SaveToExecution_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_SaveToExecution.Click
    ' ***************************************************************************************
    ' ***************************************************************************************

    Try
      Dim TransactionRow As RenaissanceDataClass.DSSelectTransaction.tblSelectTransactionRow = Nothing
      Dim InstrumentRow As RenaissanceDataClass.DSInstrument.tblInstrumentRow = Nothing
      Dim BrokerRow As RenaissanceDataClass.DSBroker.tblBrokerRow = Nothing

      If (TransactionChanged) Then

        If (MessageBox.Show("Save Transaction Changes ?", "Save Transaction Changes ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
          Call SaveTransactionChanges()
        End If

      End If

      If (MessageBox.Show("Write Trade Details to Administrator Executions file ?", "Write Trade Details", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) <> Windows.Forms.DialogResult.OK) Then
        Exit Sub
      End If

      If (Grid_Transactions.RowSel > 0) Then
        TransactionRow = CType(CType(Grid_Transactions.Rows(Grid_Transactions.RowSel).DataSource, DataRowView).Row, RenaissanceDataClass.DSSelectTransaction.tblSelectTransactionRow)
      End If

      If (TransactionRow IsNot Nothing) Then

        Call Set_AdministratorExecutionFile(TransactionRow, True)

      Else

        MessageBox.Show("Transaction not selected ?" & vbCrLf & "Please (Re)Select a line on the grid and try again.")

      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Button_SaveToExecution_Click()", ex.StackTrace, True)
    End Try

  End Sub


  Private Function Set_AdministratorExecutionFile(ByVal TransactionRow As RenaissanceDataClass.DSSelectTransaction.tblSelectTransactionRow, ByVal PostUpdateMessage As Boolean) As Integer
    ' ********************************************************************************
    '
    '
    ' ********************************************************************************
    Dim RVal As Integer = 0

    Try
      Dim FeeTransaction As RenaissanceDataClass.DSTransaction.tblTransactionRow = Nothing
      Dim Fundrow As DSFund.tblFundRow = Nothing

      Dim InstrumentAdaptor As New SqlDataAdapter
      Dim InstrumentTable As New DSInstrument.tblInstrumentDataTable
      Dim Instrumentrow As DSInstrument.tblInstrumentRow = Nothing

      Dim BrokerAdaptor As New SqlDataAdapter
      Dim BrokerTable As New DSBroker.tblBrokerDataTable
      Dim Brokerrow As DSBroker.tblBrokerRow = Nothing

      Dim TransactionAdaptor As New SqlDataAdapter

      Dim TradeFileExecutionDS As RenaissanceDataClass.DSTradeFileExecutionReporting
      Dim TradeFileExecutionTable As RenaissanceDataClass.DSTradeFileExecutionReporting.tblTradeFileExecutionReportingDataTable
      Dim TradeFileExecutionRows() As RenaissanceDataClass.DSTradeFileExecutionReporting.tblTradeFileExecutionReportingRow
      Dim TradeFileExecutionRow As RenaissanceDataClass.DSTradeFileExecutionReporting.tblTradeFileExecutionReportingRow

      Dim ExecutionDetails As New RenaissanceGlobals.BNP_TradeReportingClass_Securities("", 0, 2, 6) ' Note, all quantities are Integer.
      Dim ExecutionTableIsLoaded As Boolean = False

      ' Get fund Row

      Fundrow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFund, TransactionRow.TransactionFund), RenaissanceDataClass.DSFund.tblFundRow)

      ' Get Instrument Row

      Instrumentrow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, TransactionRow.TransactionInstrument), RenaissanceDataClass.DSInstrument.tblInstrumentRow)

      '
      ' Get Broker Details

      Brokerrow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblBroker, TransactionRow.TransactionBroker), RenaissanceDataClass.DSBroker.tblBrokerRow)

      ' Get Fees transaction (if necessary)

      If (TransactionRow.TransactionType <> TransactionTypes.Subscribe) AndAlso (TransactionRow.TransactionType <> TransactionTypes.Redeem) AndAlso (TransactionRow.TransactionRedemptionID > 0) Then
        FeeTransaction = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblTransaction, TransactionRow.TransactionRedemptionID), RenaissanceDataClass.DSTransaction.tblTransactionRow)
      End If


      ' Build Execution Details

      If (Fundrow IsNot Nothing) AndAlso (Not Fundrow.IsFundAdministratorTraderIDNull) AndAlso (CStr(Nz(Fundrow.FundAdministratorTraderID, "")).Length > 0) Then
        ExecutionDetails.TraderID = Fundrow.FundAdministratorTraderID
      End If

      ExecutionDetails.ClientPortfolioCode = Fundrow.FundAdministratorCode
      ExecutionDetails.ExternalReferenceCode = TransactionRow.TransactionParentID.ToString("###0")
      ExecutionDetails.SecutityType_Renaissance = CType(Instrumentrow.InstrumentType, InstrumentTypes)
      ExecutionDetails.TradeDate = TransactionRow.TransactionValueDate
      ExecutionDetails.TradeTime = Now()
      ExecutionDetails.SettlementDate = TransactionRow.TransactionSettlementDate
      ExecutionDetails.TradeType_Renaissance = CType(TransactionRow.TransactionType, TransactionTypes)
      ExecutionDetails.ISIN = Instrumentrow.InstrumentISIN
      ExecutionDetails.QuoteCountry = Strings.Left(Instrumentrow.InstrumentISIN, 2)
      ExecutionDetails.Quantity_Value = TransactionRow.TransactionUnits
      ExecutionDetails.Price_Value = TransactionRow.TransactionPrice

      Select Case CType(Instrumentrow.InstrumentType, InstrumentTypes)

        Case InstrumentTypes.Bond, InstrumentTypes.ConvertibleBond
          ExecutionDetails.PriceType = 10 '  10 for nominal percent
        Case Else
          ExecutionDetails.PriceType = 20 '  20 for units

      End Select

      ExecutionDetails.SecurityCurrency = Instrumentrow.InstrumentCurrency
      Dim GrossAmount_Value As Double = TransactionRow.TransactionUnits * TransactionRow.TransactionPrice * Instrumentrow.InstrumentContractSize * Instrumentrow.InstrumentMultiplier
      ExecutionDetails.GrossAmount_Value = GrossAmount_Value

      If (FeeTransaction IsNot Nothing) Then
        ExecutionDetails.BrokerFees_Value = TransactionRow.TransactionCosts + (FeeTransaction.TransactionSignedUnits * FeeTransaction.TransactionPrice)
        ExecutionDetails.BrokerNetAmount_Value = GrossAmount_Value - (TransactionRow.TransactionCosts + (FeeTransaction.TransactionSignedUnits * FeeTransaction.TransactionPrice))
      Else
        ExecutionDetails.BrokerFees_Value = TransactionRow.TransactionCosts
        ExecutionDetails.BrokerNetAmount_Value = GrossAmount_Value - TransactionRow.TransactionCosts
      End If

      ExecutionDetails.AccruedInterest_Value = 0.0#
      ExecutionDetails.SettlementCurrency = Instrumentrow.InstrumentCurrency
      ExecutionDetails.NetAmount = ExecutionDetails.BrokerNetAmount
      If (Brokerrow IsNot Nothing) Then
        ExecutionDetails.BrokerBicCode = Brokerrow.BIC_Code
        ExecutionDetails.FinalBrokerBicCode = Brokerrow.BIC_Code
      End If
      ExecutionDetails.BlockReference = TransactionRow.TransactionParentID.ToString("###0")
      ExecutionDetails.CodificationType = "ISIN"

      Try
        TradeFileExecutionDS = CType(MainForm.MainDataHandler.Get_Dataset(RenaissanceStandardDatasets.tblTradeFileExecutionReporting.TableName), RenaissanceDataClass.DSTradeFileExecutionReporting)
        If (TradeFileExecutionDS Is Nothing) Then
          TradeFileExecutionDS = New DSTradeFileExecutionReporting
          ExecutionTableIsLoaded = False
        End If
        TradeFileExecutionTable = TradeFileExecutionDS.tblTradeFileExecutionReporting

        TradeFileExecutionRows = TradeFileExecutionTable.Select("TransactionParentID=" & TransactionRow.TransactionParentID.ToString("###0"))

        If (TradeFileExecutionRows IsNot Nothing) AndAlso (TradeFileExecutionRows.Count > 0) Then
          TradeFileExecutionRow = TradeFileExecutionRows(0)
        Else
          TradeFileExecutionRow = TradeFileExecutionTable.NewtblTradeFileExecutionReportingRow
        End If

        TradeFileExecutionRow.AdministratorID = 0
        TradeFileExecutionRow.TransactionID = TransactionRow.TransactionID
        TradeFileExecutionRow.TransactionParentID = TransactionRow.TransactionParentID
        TradeFileExecutionRow.TradeReportString = ExecutionDetails.TradeString

        If (TradeFileExecutionRow.RowState And DataRowState.Detached) Then
          TradeFileExecutionTable.Rows.Add(TradeFileExecutionRow)
        End If

        MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblTradeFileExecutionReporting, New DSTradeFileExecutionReporting.tblTradeFileExecutionReportingRow() {TradeFileExecutionRow})

        RVal = TransactionRow.TransactionParentID

        ' Post Update Message.

        If (ExecutionTableIsLoaded = False) Then
          MainForm.ReloadTable_Background(RenaissanceStandardDatasets.tblTradeFileExecutionReporting.ChangeID, TradeFileExecutionRow.AuditID.ToString(), False)
        End If

        If (PostUpdateMessage) Then
          MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceStandardDatasets.tblTradeFileExecutionReporting.ChangeID, TradeFileExecutionRow.AuditID.ToString), True)
        End If

      Catch ex As Exception
        MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Set_AdministratorExecutionFile()", ex.StackTrace, True)
      End Try

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Set_AdministratorExecutionFile()", ex.StackTrace, True)
    End Try

    Return RVal
  End Function

  Private Sub ContextMenu_Trades_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ContextMenu_Trades.Click
    ' ********************************************************************************
    '
    '
    ' ********************************************************************************
    Dim UpdateDetail As String = ""

    Try
      Dim SelectedRows As C1.Win.C1FlexGrid.RowCollection = Nothing
      Dim ThisRow As C1.Win.C1FlexGrid.Row = Nothing
      Dim ItemCount As Integer
      Dim TransactionRow As RenaissanceDataClass.DSSelectTransaction.tblSelectTransactionRow = Nothing
      Dim ParentID As Integer
      Dim BrokerIdWarning As Boolean = False

      SelectedRows = Grid_Transactions.Rows.Selected

      If (SelectedRows Is Nothing) OrElse (SelectedRows.Count <= 0) Then
        Exit Sub
      End If

      ' Confirm.

      If (MessageBox.Show("Write selected trades to Administrator Executions file ?", "Write Selected Trades", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) <> Windows.Forms.DialogResult.OK) Then
        Exit Sub
      End If

      ' Quick Validation

      For ItemCount = 0 To (SelectedRows.Count - 1)
        If (ItemCount < SelectedRows.Count) Then
          ThisRow = SelectedRows(ItemCount)

          If (ThisRow IsNot Nothing) Then

            TransactionRow = CType(CType(ThisRow.DataSource, DataRowView).Row, RenaissanceDataClass.DSSelectTransaction.tblSelectTransactionRow)

            If (TransactionRow IsNot Nothing) AndAlso (TransactionRow.TransactionSignedUnits <> 0.0#) AndAlso (TransactionRow.TransactionBroker = 0) Then
              BrokerIdWarning = True
            End If

          End If
        End If
      Next

      If (BrokerIdWarning) Then
        If (MessageBox.Show("Not all of these trades have a Broker associated with them." & vbCrLf & "Do you wish to continue?", "Write Selected Trades", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) <> Windows.Forms.DialogResult.OK) Then
          Exit Sub
        End If
      End If

      ' OK, away we go...

      For ItemCount = 0 To (SelectedRows.Count - 1)
        If (ItemCount < SelectedRows.Count) Then
          ThisRow = SelectedRows(ItemCount)

          If (ThisRow IsNot Nothing) Then

            TransactionRow = CType(CType(ThisRow.DataSource, DataRowView).Row, RenaissanceDataClass.DSSelectTransaction.tblSelectTransactionRow)

            If (TransactionRow IsNot Nothing) AndAlso (TransactionRow.TransactionSignedUnits <> 0.0#) Then
              ParentID = Set_AdministratorExecutionFile(TransactionRow, False)

              If (UpdateDetail.Length > 0) Then
                UpdateDetail &= "," & ParentID.ToString("###0")
              Else
                UpdateDetail = ParentID.ToString("###0")
              End If

            End If

          End If
        End If

      Next

      ' Post Update Message

      If (UpdateDetail.Length > 0) Then
        MainForm.Main_RaiseEvent_Background(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceStandardDatasets.tblTradeFileExecutionReporting.ChangeID, UpdateDetail), True)
      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in ContextMenu_Trades_Click()", ex.StackTrace, True)
    End Try



  End Sub
End Class
