﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDeltaGrid
	Inherits System.Windows.Forms.Form

	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> _
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	Friend WithEvents editAuditID As System.Windows.Forms.TextBox
	Friend WithEvents btnNavFirst As System.Windows.Forms.Button
	Friend WithEvents btnNavPrev As System.Windows.Forms.Button
	Friend WithEvents btnNavNext As System.Windows.Forms.Button
	Friend WithEvents btnLast As System.Windows.Forms.Button
	Friend WithEvents btnCancel As System.Windows.Forms.Button
	Friend WithEvents btnSave As System.Windows.Forms.Button
	Friend WithEvents Combo_Select As System.Windows.Forms.ComboBox
	Friend WithEvents Panel1 As System.Windows.Forms.Panel
	Friend WithEvents Label1 As System.Windows.Forms.Label
	Friend WithEvents GroupBox_SelectByInstrument As System.Windows.Forms.GroupBox
	Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
	Friend WithEvents Panel_InformationEdit As System.Windows.Forms.Panel
	Friend WithEvents Grid_Greeks As System.Windows.Forms.DataGridView
	Friend WithEvents Check_ShowRecent As System.Windows.Forms.CheckBox
	Friend WithEvents btnClose As System.Windows.Forms.Button

	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Me.editAuditID = New System.Windows.Forms.TextBox
		Me.btnNavFirst = New System.Windows.Forms.Button
		Me.btnNavPrev = New System.Windows.Forms.Button
		Me.btnNavNext = New System.Windows.Forms.Button
		Me.btnLast = New System.Windows.Forms.Button
		Me.btnCancel = New System.Windows.Forms.Button
		Me.btnSave = New System.Windows.Forms.Button
		Me.Combo_Select = New System.Windows.Forms.ComboBox
		Me.Panel1 = New System.Windows.Forms.Panel
		Me.Label1 = New System.Windows.Forms.Label
		Me.GroupBox_SelectByInstrument = New System.Windows.Forms.GroupBox
		Me.Check_ShowRecent = New System.Windows.Forms.CheckBox
		Me.btnClose = New System.Windows.Forms.Button
		Me.RootMenu = New System.Windows.Forms.MenuStrip
		Me.Panel_InformationEdit = New System.Windows.Forms.Panel
		Me.Grid_Greeks = New System.Windows.Forms.DataGridView
		Me.GroupBox_SelectByDate = New System.Windows.Forms.GroupBox
		Me.Check_DisplayActiveInstruments = New System.Windows.Forms.CheckBox
		Me.DateTime_SelectedDate = New System.Windows.Forms.DateTimePicker
		Me.Label2 = New System.Windows.Forms.Label
		Me.Radio_ByInstrument = New System.Windows.Forms.RadioButton
		Me.Radio_ByDate = New System.Windows.Forms.RadioButton
		Me.Panel1.SuspendLayout()
		Me.GroupBox_SelectByInstrument.SuspendLayout()
		Me.Panel_InformationEdit.SuspendLayout()
		CType(Me.Grid_Greeks, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.GroupBox_SelectByDate.SuspendLayout()
		Me.SuspendLayout()
		'
		'editAuditID
		'
		Me.editAuditID.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.editAuditID.Enabled = False
		Me.editAuditID.Location = New System.Drawing.Point(941, 33)
		Me.editAuditID.Name = "editAuditID"
		Me.editAuditID.Size = New System.Drawing.Size(50, 20)
		Me.editAuditID.TabIndex = 2
		'
		'btnNavFirst
		'
		Me.btnNavFirst.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnNavFirst.Location = New System.Drawing.Point(8, 8)
		Me.btnNavFirst.Name = "btnNavFirst"
		Me.btnNavFirst.Size = New System.Drawing.Size(40, 28)
		Me.btnNavFirst.TabIndex = 0
		Me.btnNavFirst.Text = "<<"
		'
		'btnNavPrev
		'
		Me.btnNavPrev.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnNavPrev.Location = New System.Drawing.Point(50, 8)
		Me.btnNavPrev.Name = "btnNavPrev"
		Me.btnNavPrev.Size = New System.Drawing.Size(35, 28)
		Me.btnNavPrev.TabIndex = 1
		Me.btnNavPrev.Text = "<"
		'
		'btnNavNext
		'
		Me.btnNavNext.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnNavNext.Location = New System.Drawing.Point(88, 8)
		Me.btnNavNext.Name = "btnNavNext"
		Me.btnNavNext.Size = New System.Drawing.Size(35, 28)
		Me.btnNavNext.TabIndex = 2
		Me.btnNavNext.Text = ">"
		'
		'btnLast
		'
		Me.btnLast.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnLast.Location = New System.Drawing.Point(124, 8)
		Me.btnLast.Name = "btnLast"
		Me.btnLast.Size = New System.Drawing.Size(40, 28)
		Me.btnLast.TabIndex = 3
		Me.btnLast.Text = ">>"
		'
		'btnCancel
		'
		Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
		Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnCancel.Location = New System.Drawing.Point(434, 458)
		Me.btnCancel.Name = "btnCancel"
		Me.btnCancel.Size = New System.Drawing.Size(75, 28)
		Me.btnCancel.TabIndex = 6
		Me.btnCancel.Text = "&Cancel"
		'
		'btnSave
		'
		Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnSave.Location = New System.Drawing.Point(349, 458)
		Me.btnSave.Name = "btnSave"
		Me.btnSave.Size = New System.Drawing.Size(75, 28)
		Me.btnSave.TabIndex = 5
		Me.btnSave.Text = "&Save"
		'
		'Combo_Select
		'
		Me.Combo_Select.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_Select.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_Select.Location = New System.Drawing.Point(122, 16)
		Me.Combo_Select.Name = "Combo_Select"
		Me.Combo_Select.Size = New System.Drawing.Size(441, 21)
		Me.Combo_Select.TabIndex = 0
		'
		'Panel1
		'
		Me.Panel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Panel1.Controls.Add(Me.btnNavFirst)
		Me.Panel1.Controls.Add(Me.btnNavPrev)
		Me.Panel1.Controls.Add(Me.btnNavNext)
		Me.Panel1.Controls.Add(Me.btnLast)
		Me.Panel1.Location = New System.Drawing.Point(131, 449)
		Me.Panel1.Name = "Panel1"
		Me.Panel1.Size = New System.Drawing.Size(176, 48)
		Me.Panel1.TabIndex = 4
		'
		'Label1
		'
		Me.Label1.Location = New System.Drawing.Point(7, 16)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(100, 23)
		Me.Label1.TabIndex = 18
		Me.Label1.Text = "Select"
		'
		'GroupBox_SelectByInstrument
		'
		Me.GroupBox_SelectByInstrument.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.GroupBox_SelectByInstrument.Controls.Add(Me.Label1)
		Me.GroupBox_SelectByInstrument.Controls.Add(Me.Check_ShowRecent)
		Me.GroupBox_SelectByInstrument.Controls.Add(Me.Combo_Select)
		Me.GroupBox_SelectByInstrument.Location = New System.Drawing.Point(5, 55)
		Me.GroupBox_SelectByInstrument.Name = "GroupBox_SelectByInstrument"
		Me.GroupBox_SelectByInstrument.Size = New System.Drawing.Size(932, 72)
		Me.GroupBox_SelectByInstrument.TabIndex = 77
		Me.GroupBox_SelectByInstrument.TabStop = False
		'
		'Check_ShowRecent
		'
		Me.Check_ShowRecent.AutoSize = True
		Me.Check_ShowRecent.Location = New System.Drawing.Point(122, 43)
		Me.Check_ShowRecent.Name = "Check_ShowRecent"
		Me.Check_ShowRecent.Size = New System.Drawing.Size(336, 17)
		Me.Check_ShowRecent.TabIndex = 2
		Me.Check_ShowRecent.Text = "Display recent prices only. (May make it easier to add new prices)."
		Me.Check_ShowRecent.UseVisualStyleBackColor = True
		'
		'btnClose
		'
		Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnClose.Location = New System.Drawing.Point(518, 458)
		Me.btnClose.Name = "btnClose"
		Me.btnClose.Size = New System.Drawing.Size(75, 28)
		Me.btnClose.TabIndex = 7
		Me.btnClose.Text = "&Close"
		'
		'RootMenu
		'
		Me.RootMenu.AllowMerge = False
		Me.RootMenu.Location = New System.Drawing.Point(0, 0)
		Me.RootMenu.Name = "RootMenu"
		Me.RootMenu.Size = New System.Drawing.Size(1004, 24)
		Me.RootMenu.TabIndex = 8
		Me.RootMenu.Text = "MenuStrip1"
		'
		'Panel_InformationEdit
		'
		Me.Panel_InformationEdit.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
								Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Panel_InformationEdit.AutoScroll = True
		Me.Panel_InformationEdit.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Panel_InformationEdit.Controls.Add(Me.Grid_Greeks)
		Me.Panel_InformationEdit.Location = New System.Drawing.Point(9, 133)
		Me.Panel_InformationEdit.Name = "Panel_InformationEdit"
		Me.Panel_InformationEdit.Size = New System.Drawing.Size(988, 308)
		Me.Panel_InformationEdit.TabIndex = 3
		'
		'Grid_Greeks
		'
		Me.Grid_Greeks.AllowDrop = True
		Me.Grid_Greeks.AllowUserToOrderColumns = True
		Me.Grid_Greeks.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
								Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Grid_Greeks.Location = New System.Drawing.Point(3, 3)
		Me.Grid_Greeks.Name = "Grid_Greeks"
		Me.Grid_Greeks.Size = New System.Drawing.Size(979, 298)
		Me.Grid_Greeks.TabIndex = 0
		'
		'GroupBox_SelectByDate
		'
		Me.GroupBox_SelectByDate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.GroupBox_SelectByDate.Controls.Add(Me.Check_DisplayActiveInstruments)
		Me.GroupBox_SelectByDate.Controls.Add(Me.DateTime_SelectedDate)
		Me.GroupBox_SelectByDate.Controls.Add(Me.Label2)
		Me.GroupBox_SelectByDate.Location = New System.Drawing.Point(5, 55)
		Me.GroupBox_SelectByDate.Name = "GroupBox_SelectByDate"
		Me.GroupBox_SelectByDate.Size = New System.Drawing.Size(932, 72)
		Me.GroupBox_SelectByDate.TabIndex = 78
		Me.GroupBox_SelectByDate.TabStop = False
		Me.GroupBox_SelectByDate.Visible = False
		'
		'Check_DisplayActiveInstruments
		'
		Me.Check_DisplayActiveInstruments.AutoSize = True
		Me.Check_DisplayActiveInstruments.Location = New System.Drawing.Point(122, 44)
		Me.Check_DisplayActiveInstruments.Name = "Check_DisplayActiveInstruments"
		Me.Check_DisplayActiveInstruments.Size = New System.Drawing.Size(171, 17)
		Me.Check_DisplayActiveInstruments.TabIndex = 21
		Me.Check_DisplayActiveInstruments.Text = "Display active Instruments only"
		Me.Check_DisplayActiveInstruments.UseVisualStyleBackColor = True
		'
		'DateTime_SelectedDate
		'
		Me.DateTime_SelectedDate.Location = New System.Drawing.Point(122, 14)
		Me.DateTime_SelectedDate.Name = "DateTime_SelectedDate"
		Me.DateTime_SelectedDate.Size = New System.Drawing.Size(441, 20)
		Me.DateTime_SelectedDate.TabIndex = 20
		'
		'Label2
		'
		Me.Label2.Location = New System.Drawing.Point(7, 16)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(100, 23)
		Me.Label2.TabIndex = 19
		Me.Label2.Text = "Select"
		'
		'Radio_ByInstrument
		'
		Me.Radio_ByInstrument.Appearance = System.Windows.Forms.Appearance.Button
		Me.Radio_ByInstrument.Location = New System.Drawing.Point(9, 27)
		Me.Radio_ByInstrument.Name = "Radio_ByInstrument"
		Me.Radio_ByInstrument.Size = New System.Drawing.Size(180, 26)
		Me.Radio_ByInstrument.TabIndex = 79
		Me.Radio_ByInstrument.TabStop = True
		Me.Radio_ByInstrument.Text = "Update Greeks by Instrument"
		Me.Radio_ByInstrument.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.Radio_ByInstrument.UseVisualStyleBackColor = True
		'
		'Radio_ByDate
		'
		Me.Radio_ByDate.Appearance = System.Windows.Forms.Appearance.Button
		Me.Radio_ByDate.Location = New System.Drawing.Point(195, 27)
		Me.Radio_ByDate.Name = "Radio_ByDate"
		Me.Radio_ByDate.Size = New System.Drawing.Size(180, 26)
		Me.Radio_ByDate.TabIndex = 80
		Me.Radio_ByDate.TabStop = True
		Me.Radio_ByDate.Text = "Update Greeks by date"
		Me.Radio_ByDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.Radio_ByDate.UseVisualStyleBackColor = True
		'
		'frmDeltaGrid
		'
		Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
		Me.CancelButton = Me.btnCancel
		Me.ClientSize = New System.Drawing.Size(1004, 507)
		Me.Controls.Add(Me.Radio_ByDate)
		Me.Controls.Add(Me.Radio_ByInstrument)
		Me.Controls.Add(Me.Panel_InformationEdit)
		Me.Controls.Add(Me.btnClose)
		Me.Controls.Add(Me.Panel1)
		Me.Controls.Add(Me.btnSave)
		Me.Controls.Add(Me.editAuditID)
		Me.Controls.Add(Me.btnCancel)
		Me.Controls.Add(Me.RootMenu)
		Me.Controls.Add(Me.GroupBox_SelectByDate)
		Me.Controls.Add(Me.GroupBox_SelectByInstrument)
		Me.MainMenuStrip = Me.RootMenu
		Me.MinimumSize = New System.Drawing.Size(800, 500)
		Me.Name = "frmDeltaGrid"
		Me.Text = "Add/Edit Instrument Greeks"
		Me.Panel1.ResumeLayout(False)
		Me.GroupBox_SelectByInstrument.ResumeLayout(False)
		Me.GroupBox_SelectByInstrument.PerformLayout()
		Me.Panel_InformationEdit.ResumeLayout(False)
		CType(Me.Grid_Greeks, System.ComponentModel.ISupportInitialize).EndInit()
		Me.GroupBox_SelectByDate.ResumeLayout(False)
		Me.GroupBox_SelectByDate.PerformLayout()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub
	Friend WithEvents GroupBox_SelectByDate As System.Windows.Forms.GroupBox
	Friend WithEvents DateTime_SelectedDate As System.Windows.Forms.DateTimePicker
	Friend WithEvents Label2 As System.Windows.Forms.Label
	Friend WithEvents Check_DisplayActiveInstruments As System.Windows.Forms.CheckBox
	Friend WithEvents Radio_ByInstrument As System.Windows.Forms.RadioButton
	Friend WithEvents Radio_ByDate As System.Windows.Forms.RadioButton

End Class
