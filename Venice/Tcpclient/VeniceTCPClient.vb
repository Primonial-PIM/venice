' ***********************************************************************
' Assembly         : VeniceTCPClient
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : Nicholas Pennington
' Last Modified On : 08-15-2013
' ***********************************************************************
' <copyright file="VeniceTCPClient.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.IO
Imports System.Net
Imports System.Net.Sockets
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Runtime.Serialization

Imports TCPServer_Common
Imports TCPServer_Common.PostMessageUtils

Imports NaplesGlobals

''' <summary>
''' Class VeniceTCPClient
''' </summary>
Public Class VeniceTCPClient
  ' *****************************************************************************************************
  ' VeniceTCPClient CLASS
  ' -----------------
  '
  ' 
  ' ' MessageFolder
  '
  ' *****************************************************************************************************
  Inherits ThreadWrapperBase

  ' MessageReceived
    ''' <summary>
    ''' Occurs when [message received event].
    ''' </summary>
	Public Event MessageReceivedEvent(ByVal sender As Object, ByVal e As TCPServer_Common.MessageReceivedEventArgs)
    ''' <summary>
    ''' Occurs when [TCP error event].
    ''' </summary>
	Public Event TCPErrorEvent(ByVal sender As Object, ByVal e As System.Exception)

    ''' <summary>
    ''' The new messages queue
    ''' </summary>
  Private NewMessagesQueue As New Queue
    ''' <summary>
    ''' The message work items
    ''' </summary>
  Private MessageWorkItems As New Hashtable

    ''' <summary>
    ''' The connection list
    ''' </summary>
  Private ConnectionList As New ArrayList
    ''' <summary>
    ''' The venice client ID
    ''' </summary>
  Private VeniceClientID As Guid = Guid.NewGuid

    ''' <summary>
    ''' The _ close down flag
    ''' </summary>
  Private _CloseDownFlag As Boolean
    ''' <summary>
  ''' Handle to the venice 'Menu' form which provides much common functionality.
    ''' </summary>
  Private _MainForm As System.Windows.Forms.Form

    ''' <summary>
    ''' The default message server name
    ''' </summary>
  Private DefaultMessageServerName As String = ""
    ''' <summary>
    ''' The default message server ip address
    ''' </summary>
  Private DefaultMessageServerIpAddress As IPAddress = IPAddress.None
  ''' <summary>
  ''' The default Message Server instance name.
  ''' </summary>
  Private DefaultInstanceName As String = ""

    ''' <summary>
    ''' Prevents a default instance of the <see cref="VeniceTCPClient"/> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()
    Me.Thread.Name = "VeniceTCPClient"
  End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="VeniceTCPClient"/> class.
    ''' </summary>
    ''' <param name="MainForm">The main form.</param>
    ''' <param name="pServerName">Name of the p server.</param>
    ''' <param name="pServerAddress">The p server address.</param>
  Public Sub New(ByVal MainForm As System.Windows.Forms.Form, ByVal pServerName As String, ByVal pServerAddress As IPAddress, ByVal pInstanceName As String)
    Me.New()

    _MainForm = MainForm

    DefaultMessageServerName = pServerName
    DefaultMessageServerIpAddress = pServerAddress
    DefaultInstanceName = pInstanceName.ToUpper

    ' Add default server connection.
    If Not (IPAddress.None.Equals(DefaultMessageServerIpAddress)) Then
      ConnectionList.Add(New ServerConnectionClass(DefaultMessageServerName, DefaultMessageServerIpAddress))
    End If

  End Sub

    ''' <summary>
    ''' Gets or sets a value indicating whether [close down].
    ''' </summary>
    ''' <value><c>true</c> if [close down]; otherwise, <c>false</c>.</value>
  Public Property CloseDown() As Boolean
    Get
      Return _CloseDownFlag
    End Get
    Set(ByVal Value As Boolean)
      _CloseDownFlag = Value
    End Set
  End Property

    ''' <summary>
    ''' Does the task.
    ''' </summary>
  Protected Overrides Sub DoTask()

    Try
      Initialise()

      RunDataServer()
    Catch ex As Exception
    Finally
      TidyUp()
    End Try

  End Sub

    ''' <summary>
    ''' Initialises this instance.
    ''' </summary>
  Private Sub Initialise()
    ' Initialise Data Server

    _CloseDownFlag = False


  End Sub

    ''' <summary>
    ''' Tidies up.
    ''' </summary>
  Private Sub TidyUp()
    Dim thisConnection As ServerConnectionClass

    ' On ending the Server

    Try
      For Each thisConnection In ConnectionList
        Try
          If (Not (thisConnection.Stream Is Nothing)) Then
            PostToMessageServer(thisConnection.Stream, TCP_ServerMessageType.Client_Disconnect, FCP_Application.Venice, thisConnection.InstanceName, "")
          End If
          thisConnection.Stream.Close()
          thisConnection.Client.Close()
				Catch ex As Exception
					RaiseEvent TCPErrorEvent(Me, ex)
				Finally
					thisConnection.Client = Nothing
					thisConnection = Nothing
        End Try
      Next
    Catch ex As Exception
			RaiseEvent TCPErrorEvent(Me, ex)
		End Try

    Try
      ConnectionList.Clear()
    Catch ex As Exception
			RaiseEvent TCPErrorEvent(Me, ex)
		End Try

    _MainForm = Nothing

  End Sub


  ' *******************************************************************
  ' Post New Message 
  ' *******************************************************************
    ''' <summary>
    ''' Posts the new message.
    ''' </summary>
    ''' <param name="pMessageFolder">The p message folder.</param>
  Public Sub PostNewMessage(ByRef pMessageFolder As MessageFolder)
    SyncLock NewMessagesQueue
      NewMessagesQueue.Enqueue(pMessageFolder)
    End SyncLock
  End Sub


  ' *******************************************************************
  '
  ' *******************************************************************
    ''' <summary>
    ''' Raises the <see cref="E:MessageReceived" /> event.
    ''' </summary>
    ''' <param name="pMessageReceived">The <see cref="MessageReceivedEventArgs"/> instance containing the event data.</param>
  Public Sub OnMessageReceived(ByVal pMessageReceived As MessageReceivedEventArgs)
    RaiseEvent MessageReceivedEvent(Me, pMessageReceived)
  End Sub


  ' *******************************************************************
  '
  ' *******************************************************************
    ''' <summary>
    ''' Runs the data server.
    ''' </summary>
  Private Sub RunDataServer()
    Dim LoopWorkDone As Boolean
    Dim ConnectionCount As Integer
    Dim MaxMessageCounter As Integer
    Dim thisConnection As ServerConnectionClass
    Dim DefaultServerConnection As ServerConnectionClass
    Dim MessageHeader As TCPServerHeader
    Dim SerializeFormatter As New BinaryFormatter
    Dim ServerCount As Integer

    DefaultServerConnection = Nothing

    ' ******************************************************
    ' Work loop.
    ' ******************************************************

    While (_CloseDownFlag = False)
      LoopWorkDone = False

      ' ******************************************************
      ' Check for new messages to send
      ' ******************************************************

      If Me.NewMessagesQueue.Count > 0 Then
        Dim MessageObject As Object

        ' ******************************************************
        ' Retrieve next Message
        ' ******************************************************

        SyncLock NewMessagesQueue
          'MessageObject = NewMessagesQueue.Dequeue
          MessageObject = NewMessagesQueue.Peek
        End SyncLock

        ' ******************************************************
        ' Only accept Message folder objects, (Is this Sensible ?)
        ' ******************************************************

        If MessageObject.GetType Is GetType(MessageFolder) Then
          Dim ThisMessageFolder As MessageFolder
          Dim ObjectCount As Integer
          Dim ObjectsToSend As Integer

          ThisMessageFolder = CType(MessageObject, MessageFolder)
          ObjectsToSend = ThisMessageFolder.HeaderMessage.FollowingMessageCount

          If Not (ThisMessageFolder.FollowingObjects Is Nothing) Then
            If ThisMessageFolder.FollowingObjects.Count < ObjectsToSend Then
              ObjectsToSend = ThisMessageFolder.FollowingObjects.Count
            End If
          Else
            ObjectsToSend = 0
          End If

          ' ******************************************************
          ' Resolve Default server connection.
          ' ******************************************************

          If (DefaultServerConnection Is Nothing) Then
            For Each thisConnection In ConnectionList
              If (thisConnection.ApplicationType And FCP_Application.MessageServer) = FCP_Application.MessageServer Then
                DefaultServerConnection = thisConnection
                Exit For
              End If
            Next
          End If

          ' ******************************************************
          ' Post message.
          ' ******************************************************

          If Not (DefaultServerConnection Is Nothing) AndAlso (ConnectToMessageServer(DefaultServerConnection) = True) Then
            If (ThisMessageFolder.HeaderMessage.FollowingMessageCount <> ObjectsToSend) Then
              ThisMessageFolder.HeaderMessage.FollowingMessageCount = ObjectsToSend
            End If

            If (Not PostToMessageServer(DefaultServerConnection.Stream, ThisMessageFolder.HeaderMessage)) Then
              ' Failed to send

              ' No need to Enqueue as we now use .peek to get the message and onll Dequeue if the header goes correctly.

              'SyncLock NewMessagesQueue
              '  NewMessagesQueue.Enqueue(MessageObject)
              'End SyncLock

              ' Refresh Server connection

              Try
                DefaultServerConnection.MsgServerStream.Close()

              Catch ex As Exception
                RaiseEvent TCPErrorEvent(Me, ex)
              Finally
                DefaultServerConnection.MsgServerClient = Nothing
                DefaultServerConnection.MsgServerStream = Nothing
              End Try

              ConnectToMessageServer(DefaultServerConnection)

            Else
              ' Header Sent OK.

              SyncLock NewMessagesQueue
                NewMessagesQueue.Dequeue()
              End SyncLock

              ' AddTCPServerWrapperClass and Send objects.

              If (ThisMessageFolder.HeaderMessage.FollowingMessageCount > 0) AndAlso (ObjectsToSend > 0) Then
                For ObjectCount = 1 To ObjectsToSend
                  Try
                    PostToMessageServer(DefaultServerConnection.Stream, AddTCPServerWrapperClass(ThisMessageFolder.FollowingObjects(ObjectCount - 1)))
                  Catch ex As Exception
                    RaiseEvent TCPErrorEvent(Me, ex)
                  End Try

                Next
              End If

            End If

          Else
            DefaultServerConnection = Nothing
          End If
        End If
      End If

      ' ******************************************************
      ' Check for received messages
      ' ******************************************************

      ServerCount = 0
      If (ConnectionList.Count > 0) Then

        ' ******************************************************
        ' Loop through available connections
        ' ******************************************************

        For ConnectionCount = 0 To (ConnectionList.Count - 1)

          Try
            If (ConnectionCount < ConnectionList.Count) Then
              thisConnection = ConnectionList(ConnectionCount)

              ' Count Message Servers

              If (thisConnection.ApplicationType And FCP_Application.MessageServer) = FCP_Application.MessageServer Then
                ServerCount += 1

                If (DefaultServerConnection Is Nothing) Then
                  DefaultServerConnection = thisConnection
                  Exit For
                End If
              End If
            Else
              thisConnection = Nothing
            End If
          Catch ex As Exception
            thisConnection = Nothing
						RaiseEvent TCPErrorEvent(Me, ex)
					End Try

          If (Not (thisConnection Is Nothing)) AndAlso (ConnectToMessageServer(thisConnection) = True) Then
            ' *************************************************
            ' Check for Incoming messages
            ' *************************************************

            MaxMessageCounter = 0
            While (Not (thisConnection Is Nothing)) AndAlso (thisConnection.MsgServerStream.DataAvailable = True) AndAlso (MaxMessageCounter < 5)
              Try
                MessageHeader = DirectCast(SerializeFormatter.Deserialize(thisConnection.MsgServerStream), TCPServerHeader)
                thisConnection.LastWakeupTime = Now()
              Catch ex As Exception
                MessageHeader = Nothing
								RaiseEvent TCPErrorEvent(Me, ex)
							End Try

              If (Not (MessageHeader Is Nothing)) Then
                Dim FollowingObjects As New ArrayList
                Dim Counter As Integer

                ' Take in Following objects, if any.

                If MessageHeader.FollowingMessageCount > 0 Then

                  For Counter = 1 To MessageHeader.FollowingMessageCount
                    Try
                      FollowingObjects.Add(SerializeFormatter.Deserialize(thisConnection.MsgServerStream))
										Catch ex As Exception
											RaiseEvent TCPErrorEvent(Me, ex)
										End Try
                  Next
                End If

                ' Strip off Wrapper objects as necessary.
                If (FollowingObjects.Count > 0) Then
                  For Counter = 0 To (FollowingObjects.Count - 1)
                    Try
                      If (FollowingObjects(Counter).GetType Is GetType(TCPServerObjectWrapper)) Then
                        Dim ThisObjectWrapper As TCPServerObjectWrapper
                        Dim NewObject As Object

                        ThisObjectWrapper = CType(FollowingObjects(Counter), TCPServerObjectWrapper)
                        NewObject = FollowingObjects(Counter)

                        Select Case ThisObjectWrapper.ObjectFormat.ToUpper
                          Case "BINARYFORMATTER"       ' BinaryFormatter
                            Try
                              NewObject = SerializeFormatter.Deserialize(New MemoryStream(ThisObjectWrapper.ObjectData))
                            Catch ex As Exception
															NewObject = FollowingObjects(Counter)
															RaiseEvent TCPErrorEvent(Me, ex)
                            End Try

                          Case "XMLFORMATTER"
                            Select Case ThisObjectWrapper.ObjectType
                              Case "VeniceUpdateClass"
                                ' More Code Here ....
                                '
                                '
                                '

                              Case "ANOther"
                                ' More Code Here ....
                                '
                                '
                                '
                            End Select


                        End Select

                        FollowingObjects(Counter) = NewObject

                      End If
                    Catch ex As Exception
											RaiseEvent TCPErrorEvent(Me, ex)
										End Try
                  Next
                End If

                ' Process Message

                Select Case MessageHeader.MessageType

                  Case TCP_ServerMessageType.Server_ApplicationUpdateMessage, TCP_ServerMessageType.Client_ApplicationUpdateMessage
                    RaiseEvent MessageReceivedEvent(MessageHeader, New MessageReceivedEventArgs(MessageHeader, FollowingObjects))

                  Case TCP_ServerMessageType.Server_VeniceDataRequest, TCP_ServerMessageType.Client_VeniceDataRequest


                  Case TCP_ServerMessageType.Server_MessageRejected
                    ' ******************************************************
                    ' Reply Rejected ?
                    ' 
                    ' If a data reply was rejected, then cancel the related work object, if
                    ' it exists.
                    ' ******************************************************







                  Case TCP_ServerMessageType.Server_Touchbase
                    ' ******************************************************
                    ' TouchBase Message
                    ' ******************************************************

                    Call PostToMessageServer(thisConnection.MsgServerStream, TCP_ServerMessageType.Client_AcknowledgeOK, FCP_Application.Venice, DefaultInstanceName, "")

                  Case TCP_ServerMessageType.Server_Disconnect, TCP_ServerMessageType.Client_Disconnect
                    ' ******************************************************
                    ' Disconnect, Remove this Connection from the collection
                    ' ******************************************************

                  Case TCP_ServerMessageType.Client_RequestConnect, TCP_ServerMessageType.Server_RequestConnect
                    ' ******************************************************
                    ' Connection Requested
                    ' ******************************************************

                    ' Start a dialogue.

                    PostToMessageServer(thisConnection.MsgServerStream, TCP_ServerMessageType.Client_SetGUID, FCP_Application.Venice, DefaultInstanceName, VeniceClientID.ToString)
                    PostToMessageServer(thisConnection.MsgServerStream, TCP_ServerMessageType.Client_RegisterApplication, FCP_Application.Renaissance, DefaultInstanceName, "")
                    PostToMessageServer(thisConnection.MsgServerStream, TCP_ServerMessageType.Client_RegisterApplication, FCP_Application.Venice, DefaultInstanceName, "")
                    PostToMessageServer(thisConnection.MsgServerStream, TCP_ServerMessageType.Client_RegisterApplication, FCP_Application.VeniceDataServer, DefaultInstanceName, "")

                  Case TCP_ServerMessageType.Client_RegisterApplication
                    thisConnection.ApplicationType = thisConnection.ApplicationType Or MessageHeader.Application


                  Case Else    ' MessageHeader.MessageType
                    ' Ignore other Messages



                End Select



              End If

            End While ' Data Available

          End If ' Connection is valid

          ' Heartbeat.

          If (thisConnection IsNot Nothing) AndAlso (CType(Now() - thisConnection.LastWakeupTime, TimeSpan).TotalSeconds > 30) Then
            Try

              If PostToMessageServer(thisConnection.MsgServerStream, TCP_ServerMessageType.Client_Touchbase, FCP_Application.Venice, DefaultInstanceName, "") Then
                thisConnection.LastWakeupTime = Now
              Else

                Try
                  thisConnection.MsgServerStream.Close()

                Catch Inner_ex As Exception
                  RaiseEvent TCPErrorEvent(Me, Inner_ex)
                Finally
                  thisConnection.MsgServerClient = Nothing
                  thisConnection.MsgServerStream = Nothing
                End Try

                ConnectToMessageServer(thisConnection)

              End If

            Catch ex As Exception


              Try
                thisConnection.MsgServerStream.Close()

              Catch Inner_ex As Exception
              Finally
                thisConnection.MsgServerClient = Nothing
                thisConnection.MsgServerStream = Nothing
              End Try

              ConnectToMessageServer(thisConnection)

              RaiseEvent TCPErrorEvent(Me, ex)

            End Try
          End If

        Next ' ConnectionCount

      End If ' (ConnectionList.Count > 0)

      ' Delete Marked Connections



      ' Ensure the Default connection persists
      If (Not (DefaultServerConnection Is Nothing)) AndAlso ConnectToMessageServer(DefaultServerConnection) = False Then
        DefaultServerConnection = Nothing
      End If

      'If Not (IPAddress.None.Equals(DefaultMessageServerIpAddress)) Then
      '  ConnectionList.Add(New ServerConnectionClass(DefaultMessageServerName, DefaultMessageServerIpAddress))
      'End If


      If (LoopWorkDone = False) Then
        Threading.Thread.Sleep(25)
      End If

      ' Exit loop if Venice has exited. (Crashed for example)

      If (_MainForm.IsDisposed) Then
        _CloseDownFlag = True
      End If

    End While

  End Sub



  ''' <summary>
  ''' Connects to message server.
  ''' </summary>
  ''' <param name="pServerConnection">The server connection.</param>
  ''' <returns><c>true</c> if connection appears sucessfull, <c>false</c> otherwise</returns>
  Private Function ConnectToMessageServer(ByVal pServerConnection As ServerConnectionClass) As Boolean

    ' *******************************************************
    ' If the Stream appears to be valid, then Exit OK.
    ' *******************************************************

    If (pServerConnection.MsgServerStream IsNot Nothing) Then

      If (pServerConnection.MsgServerClient IsNot Nothing) Then

        If (pServerConnection.MsgServerClient.Connected) Then

          Return True
          Exit Function

        Else

          Try
            pServerConnection.MsgServerStream.Close()

          Catch ex As Exception
            RaiseEvent TCPErrorEvent(Me, ex)
          Finally
            pServerConnection.MsgServerClient = Nothing
            pServerConnection.MsgServerStream = Nothing
          End Try
        End If
      End If
    End If

    ' *******************************************************
    '
    ' *******************************************************

    If (pServerConnection.MsgServerIPAddress.Equals(IPAddress.None)) Then
      Return False
      Exit Function
    End If

    ' *******************************************************
    ' Initiate the Connection.
    ' *******************************************************

    Try
      pServerConnection.MsgServerClient = New TcpClient(TCPServerGlobals.RENAISSANCE_NETWORK_FAMILY)
      pServerConnection.MsgServerClient.ReceiveTimeout = TCPServerGlobals.FCP_Server_ReceiveTimeout
      pServerConnection.MsgServerClient.SendTimeout = TCPServerGlobals.FCP_Server_SendTimeout
      pServerConnection.MsgServerClient.NoDelay = True
      pServerConnection.MsgServerClient.ReceiveBufferSize = (2 ^ 14) ' 16K
      pServerConnection.MsgServerClient.SendBufferSize = (2 ^ 14)  ' 16K

      pServerConnection.MsgServerClient.Connect(pServerConnection.MsgServerIPAddress, TCPServerGlobals.FCP_ServerPort)

      pServerConnection.MsgServerStream = pServerConnection.MsgServerClient.GetStream()

      ' Start a dialogue.

      PostToMessageServer(pServerConnection.MsgServerStream, TCP_ServerMessageType.Client_RequestConnect, FCP_Application.Venice, DefaultInstanceName, System.Security.Principal.WindowsIdentity.GetCurrent().Name)
      PostToMessageServer(pServerConnection.MsgServerStream, TCP_ServerMessageType.Client_SetGUID, FCP_Application.Venice, DefaultInstanceName, VeniceClientID.ToString)
      PostToMessageServer(pServerConnection.MsgServerStream, TCP_ServerMessageType.Client_RegisterApplication, FCP_Application.Venice, DefaultInstanceName, "")
      PostToMessageServer(pServerConnection.MsgServerStream, TCP_ServerMessageType.Client_RegisterApplication, FCP_Application.VeniceDataServer, DefaultInstanceName, "")
      PostToMessageServer(pServerConnection.MsgServerStream, TCP_ServerMessageType.Client_RegisterApplication, FCP_Application.Renaissance, DefaultInstanceName, "")

    Catch ex As Exception

      Try
        pServerConnection.MsgServerClient.Close()
      Catch ex1 As Exception
      Finally
        pServerConnection.MsgServerStream = Nothing
      End Try

      RaiseEvent TCPErrorEvent(Me, ex)

      Return False
      Exit Function
    End Try

    Return True

  End Function


    ''' <summary>
    ''' Adds the TCP server wrapper class.
    ''' </summary>
    ''' <param name="pSourceObject">The p source object.</param>
    ''' <returns>TCPServerObjectWrapper.</returns>
  Public Function AddTCPServerWrapperClass(ByRef pSourceObject As Object) As TCPServerObjectWrapper
    Dim SerializeFormatter As New BinaryFormatter
    Dim ThisWrapperObject As TCPServerObjectWrapper

    If (Not (pSourceObject.GetType Is GetType(TCPServerObjectWrapper))) Then
      Try
        ThisWrapperObject = New TCPServerObjectWrapper
        ThisWrapperObject.ObjectType = pSourceObject.GetType.Name
        ThisWrapperObject.ObjectFormat = "BinaryFormatter"

        Dim ms As New MemoryStream
        SerializeFormatter.Serialize(ms, pSourceObject)
        ms.Position = 0
        ThisWrapperObject.ObjectData = ms.ToArray()
        ms.Close()
      Catch ex As Exception
        ThisWrapperObject = New TCPServerObjectWrapper
				RaiseEvent TCPErrorEvent(Me, ex)
			End Try

    Else
      ThisWrapperObject = pSourceObject
    End If

    Return ThisWrapperObject

  End Function


    ''' <summary>
    ''' Class ServerConnectionClass
    ''' </summary>
  Private Class ServerConnectionClass
    Inherits TcpConnClass

    ' *************************************************
    ' Simple class used to manage connection to the TCP Server.
    ' *************************************************

        ''' <summary>
        ''' The MSG server name
        ''' </summary>
    Public MsgServerName As String
        ''' <summary>
        ''' The MSG server IP address
        ''' </summary>
    Public MsgServerIPAddress As IPAddress
        ''' <summary>
        ''' The last wakeup time
        ''' </summary>
    Public LastWakeupTime As Date

        ''' <summary>
        ''' Initializes a new instance of the <see cref="ServerConnectionClass"/> class.
        ''' </summary>
    Public Sub New()
      Me.GUID = System.Guid.NewGuid
      MsgServerName = ""
      MsgServerIPAddress = IPAddress.None
      Me.Stream = Nothing
      Me.Client = Nothing
      LastWakeupTime = Now()
    End Sub

        ''' <summary>
        ''' Initializes a new instance of the <see cref="ServerConnectionClass"/> class.
        ''' </summary>
        ''' <param name="pMsgServerName">Name of the p MSG server.</param>
        ''' <param name="pMsgServerIPAddress">The p MSG server IP address.</param>
    Public Sub New(ByVal pMsgServerName As String, ByVal pMsgServerIPAddress As IPAddress)
      Me.New()
      Me.MsgServerName = pMsgServerName
      Me.MsgServerIPAddress = pMsgServerIPAddress
    End Sub

        ''' <summary>
        ''' Gets or sets the MSG server stream.
        ''' </summary>
        ''' <value>The MSG server stream.</value>
    Public Property MsgServerStream() As NetworkStream
      Get
        Return Me.Stream
      End Get
      Set(ByVal Value As NetworkStream)
        Me.Stream = Value
      End Set
    End Property

        ''' <summary>
        ''' Gets or sets the MSG server client.
        ''' </summary>
        ''' <value>The MSG server client.</value>
    Public Property MsgServerClient() As TcpClient
      Get
        Return Me.Client
      End Get
      Set(ByVal Value As TcpClient)
        Me.Client = Value
      End Set
    End Property
  End Class



    ''' <summary>
    ''' Class ClientWorkClass
    ''' </summary>
  Private Class ClientWorkClass
    ' *************************************************
    ' Simple Class used to manage active work items being processed bt the TCP Client / Data Servers
    ' *************************************************

        ''' <summary>
        ''' The client request id
        ''' </summary>
    Public ClientRequestId As Guid

        ''' <summary>
        ''' The server connection
        ''' </summary>
    Public ServerConnection As ServerConnectionClass

        ''' <summary>
        ''' The message header
        ''' </summary>
    Public MessageHeader As TCPServerHeader   ' 
        ''' <summary>
        ''' The message body
        ''' </summary>
    Public MessageBody As Object  ' e.g. NaplesDataRequest object

        ''' <summary>
        ''' The data request ID
        ''' </summary>
    Public DataRequestID As Guid
        ''' <summary>
        ''' The data request
        ''' </summary>
    Public DataRequest As Object  ' e.g. DataServerRequest object
        ''' <summary>
        ''' The data item ID
        ''' </summary>
    Public DataItemID As Guid
        ''' <summary>
        ''' The data item
        ''' </summary>
    Public DataItem As Object  ' e.g. DataServerRTItem object

        ''' <summary>
        ''' Initializes a new instance of the <see cref="ClientWorkClass"/> class.
        ''' </summary>
        ''' <param name="pServerConnection">The p server connection.</param>
        ''' <param name="pMessageHeader">The p message header.</param>
        ''' <param name="pMessageBody">The p message body.</param>
        ''' <param name="pClientRequestId">The p client request id.</param>
    Public Sub New(ByRef pServerConnection As ServerConnectionClass, ByVal pMessageHeader As TCPServerHeader, ByVal pMessageBody As Object, ByVal pClientRequestId As Guid)
      Me.ClientRequestId = pClientRequestId
      Me.ServerConnection = pServerConnection
      Me.MessageHeader = pMessageHeader
      Me.MessageBody = pMessageBody
    End Sub

        ''' <summary>
        ''' Initializes a new instance of the <see cref="ClientWorkClass"/> class.
        ''' </summary>
        ''' <param name="pServerConnection">The p server connection.</param>
        ''' <param name="pMessageHeader">The p message header.</param>
        ''' <param name="pMessageBody">The p message body.</param>
    Public Sub New(ByRef pServerConnection As ServerConnectionClass, ByVal pMessageHeader As TCPServerHeader, ByVal pMessageBody As Object)
      Me.MessageHeader = pMessageHeader
      Me.MessageBody = pMessageBody
      Me.ServerConnection = pServerConnection
      Me.ClientRequestId = Guid.NewGuid

      Me.DataRequestID = Guid.Empty
      Me.DataRequest = Nothing
      Me.DataItemID = Guid.Empty
      Me.DataItem = Nothing

      If Not (pMessageBody Is Nothing) Then
        If (pMessageBody.GetType Is GetType(NaplesDataRequest)) Then
          Me.ClientRequestId = CType(pMessageBody, NaplesDataRequest).RequestID
        End If
      End If
    End Sub

        ''' <summary>
        ''' Initializes a new instance of the <see cref="ClientWorkClass"/> class.
        ''' </summary>
        ''' <param name="pServerConnection">The p server connection.</param>
        ''' <param name="pMessageHeader">The p message header.</param>
        ''' <param name="pMessageBody">The p message body.</param>
        ''' <param name="pRequestID">The p request ID.</param>
        ''' <param name="pDataRequest">The p data request.</param>
    Public Sub New(ByRef pServerConnection As ServerConnectionClass, ByRef pMessageHeader As TCPServerHeader, ByRef pMessageBody As Object, ByVal pRequestID As Guid, ByRef pDataRequest As Object)
      Me.New(pServerConnection, pMessageHeader, pMessageBody)
      Me.DataRequest = pDataRequest
      Me.DataRequestID = pRequestID
    End Sub

        ''' <summary>
        ''' Prevents a default instance of the <see cref="ClientWorkClass"/> class from being created.
        ''' </summary>
    Private Sub New()
      ' Enforce the use of Constructor arguments
    End Sub

  End Class

End Class
