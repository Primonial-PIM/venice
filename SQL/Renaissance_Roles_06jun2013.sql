USE [Renaissance]
GO

DECLARE @RoleName sysname
set @RoleName = N'web_TradeProcessing'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
IF @RoleName <> N'public' and (select is_fixed_role from sys.database_principals where name = @RoleName) = 0
BEGIN
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id
		from sys.database_role_members
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName AND type = 'R'))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName
	
	DECLARE @SQL NVARCHAR(4000)

	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		SET @SQL = 'ALTER ROLE '+ QUOTENAME(@RoleName,'[') +' DROP MEMBER '+ QUOTENAME(@RoleMemberName,'[')
		EXEC(@SQL)
		
		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
END
/****** Object:  DatabaseRole [web_TradeProcessing]    Script Date: 6/7/2013 5:03:52 PM ******/
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'web_TradeProcessing' AND type = 'R')
DROP ROLE [web_TradeProcessing]
GO

DECLARE @RoleName sysname
set @RoleName = N'web_server'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
IF @RoleName <> N'public' and (select is_fixed_role from sys.database_principals where name = @RoleName) = 0
BEGIN
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id
		from sys.database_role_members
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName AND type = 'R'))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName
	
	DECLARE @SQL NVARCHAR(4000)

	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		SET @SQL = 'ALTER ROLE '+ QUOTENAME(@RoleName,'[') +' DROP MEMBER '+ QUOTENAME(@RoleMemberName,'[')
		EXEC(@SQL)
		
		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
END
/****** Object:  DatabaseRole [web_server]    Script Date: 6/7/2013 5:03:52 PM ******/
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'web_server' AND type = 'R')
DROP ROLE [web_server]
GO

DECLARE @RoleName sysname
set @RoleName = N'Sienna_Update'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
IF @RoleName <> N'public' and (select is_fixed_role from sys.database_principals where name = @RoleName) = 0
BEGIN
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id
		from sys.database_role_members
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName AND type = 'R'))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName
	
	DECLARE @SQL NVARCHAR(4000)

	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		SET @SQL = 'ALTER ROLE '+ QUOTENAME(@RoleName,'[') +' DROP MEMBER '+ QUOTENAME(@RoleMemberName,'[')
		EXEC(@SQL)
		
		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
END
/****** Object:  DatabaseRole [Sienna_Update]    Script Date: 6/7/2013 5:03:52 PM ******/
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'Sienna_Update' AND type = 'R')
DROP ROLE [Sienna_Update]
GO

DECLARE @RoleName sysname
set @RoleName = N'Sienna_Read'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
IF @RoleName <> N'public' and (select is_fixed_role from sys.database_principals where name = @RoleName) = 0
BEGIN
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id
		from sys.database_role_members
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName AND type = 'R'))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName
	
	DECLARE @SQL NVARCHAR(4000)

	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		SET @SQL = 'ALTER ROLE '+ QUOTENAME(@RoleName,'[') +' DROP MEMBER '+ QUOTENAME(@RoleMemberName,'[')
		EXEC(@SQL)
		
		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
END
/****** Object:  DatabaseRole [Sienna_Read]    Script Date: 6/7/2013 5:03:52 PM ******/
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'Sienna_Read' AND type = 'R')
DROP ROLE [Sienna_Read]
GO

DECLARE @RoleName sysname
set @RoleName = N'Reporter'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
IF @RoleName <> N'public' and (select is_fixed_role from sys.database_principals where name = @RoleName) = 0
BEGIN
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id
		from sys.database_role_members
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName AND type = 'R'))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName
	
	DECLARE @SQL NVARCHAR(4000)

	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		SET @SQL = 'ALTER ROLE '+ QUOTENAME(@RoleName,'[') +' DROP MEMBER '+ QUOTENAME(@RoleMemberName,'[')
		EXEC(@SQL)
		
		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
END
/****** Object:  DatabaseRole [Reporter]    Script Date: 6/7/2013 5:03:52 PM ******/
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'Reporter' AND type = 'R')
DROP ROLE [Reporter]
GO

DECLARE @RoleName sysname
set @RoleName = N'Naples_Role'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
IF @RoleName <> N'public' and (select is_fixed_role from sys.database_principals where name = @RoleName) = 0
BEGIN
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id
		from sys.database_role_members
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName AND type = 'R'))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName
	
	DECLARE @SQL NVARCHAR(4000)

	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		SET @SQL = 'ALTER ROLE '+ QUOTENAME(@RoleName,'[') +' DROP MEMBER '+ QUOTENAME(@RoleMemberName,'[')
		EXEC(@SQL)
		
		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
END
/****** Object:  DatabaseRole [Naples_Role]    Script Date: 6/7/2013 5:03:52 PM ******/
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'Naples_Role' AND type = 'R')
DROP ROLE [Naples_Role]
GO

DECLARE @RoleName sysname
set @RoleName = N'Michigan_Role'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
IF @RoleName <> N'public' and (select is_fixed_role from sys.database_principals where name = @RoleName) = 0
BEGIN
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id
		from sys.database_role_members
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName AND type = 'R'))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName
	
	DECLARE @SQL NVARCHAR(4000)

	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		SET @SQL = 'ALTER ROLE '+ QUOTENAME(@RoleName,'[') +' DROP MEMBER '+ QUOTENAME(@RoleMemberName,'[')
		EXEC(@SQL)
		
		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
END
/****** Object:  DatabaseRole [Michigan_Role]    Script Date: 6/7/2013 5:03:52 PM ******/
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'Michigan_Role' AND type = 'R')
DROP ROLE [Michigan_Role]
GO

DECLARE @RoleName sysname
set @RoleName = N'InvestMaster_Sales'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
IF @RoleName <> N'public' and (select is_fixed_role from sys.database_principals where name = @RoleName) = 0
BEGIN
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id
		from sys.database_role_members
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName AND type = 'R'))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName
	
	DECLARE @SQL NVARCHAR(4000)

	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		SET @SQL = 'ALTER ROLE '+ QUOTENAME(@RoleName,'[') +' DROP MEMBER '+ QUOTENAME(@RoleMemberName,'[')
		EXEC(@SQL)
		
		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
END
/****** Object:  DatabaseRole [InvestMaster_Sales]    Script Date: 6/7/2013 5:03:52 PM ******/
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'InvestMaster_Sales' AND type = 'R')
DROP ROLE [InvestMaster_Sales]
GO

DECLARE @RoleName sysname
set @RoleName = N'InvestMaster_RiskManager'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
IF @RoleName <> N'public' and (select is_fixed_role from sys.database_principals where name = @RoleName) = 0
BEGIN
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id
		from sys.database_role_members
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName AND type = 'R'))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName
	
	DECLARE @SQL NVARCHAR(4000)

	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		SET @SQL = 'ALTER ROLE '+ QUOTENAME(@RoleName,'[') +' DROP MEMBER '+ QUOTENAME(@RoleMemberName,'[')
		EXEC(@SQL)
		
		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
END
/****** Object:  DatabaseRole [InvestMaster_RiskManager]    Script Date: 6/7/2013 5:03:52 PM ******/
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'InvestMaster_RiskManager' AND type = 'R')
DROP ROLE [InvestMaster_RiskManager]
GO

DECLARE @RoleName sysname
set @RoleName = N'InvestMaster_MonthEnd'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
IF @RoleName <> N'public' and (select is_fixed_role from sys.database_principals where name = @RoleName) = 0
BEGIN
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id
		from sys.database_role_members
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName AND type = 'R'))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName
	
	DECLARE @SQL NVARCHAR(4000)

	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		SET @SQL = 'ALTER ROLE '+ QUOTENAME(@RoleName,'[') +' DROP MEMBER '+ QUOTENAME(@RoleMemberName,'[')
		EXEC(@SQL)
		
		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
END
/****** Object:  DatabaseRole [InvestMaster_MonthEnd]    Script Date: 6/7/2013 5:03:52 PM ******/
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'InvestMaster_MonthEnd' AND type = 'R')
DROP ROLE [InvestMaster_MonthEnd]
GO

DECLARE @RoleName sysname
set @RoleName = N'InvestMaster_Manager'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
IF @RoleName <> N'public' and (select is_fixed_role from sys.database_principals where name = @RoleName) = 0
BEGIN
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id
		from sys.database_role_members
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName AND type = 'R'))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName
	
	DECLARE @SQL NVARCHAR(4000)

	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		SET @SQL = 'ALTER ROLE '+ QUOTENAME(@RoleName,'[') +' DROP MEMBER '+ QUOTENAME(@RoleMemberName,'[')
		EXEC(@SQL)
		
		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
END
/****** Object:  DatabaseRole [InvestMaster_Manager]    Script Date: 6/7/2013 5:03:52 PM ******/
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'InvestMaster_Manager' AND type = 'R')
DROP ROLE [InvestMaster_Manager]
GO

DECLARE @RoleName sysname
set @RoleName = N'InvestMaster_DataEntry'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
IF @RoleName <> N'public' and (select is_fixed_role from sys.database_principals where name = @RoleName) = 0
BEGIN
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id
		from sys.database_role_members
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName AND type = 'R'))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName
	
	DECLARE @SQL NVARCHAR(4000)

	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		SET @SQL = 'ALTER ROLE '+ QUOTENAME(@RoleName,'[') +' DROP MEMBER '+ QUOTENAME(@RoleMemberName,'[')
		EXEC(@SQL)
		
		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
END
/****** Object:  DatabaseRole [InvestMaster_DataEntry]    Script Date: 6/7/2013 5:03:52 PM ******/
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'InvestMaster_DataEntry' AND type = 'R')
DROP ROLE [InvestMaster_DataEntry]
GO

DECLARE @RoleName sysname
set @RoleName = N'InvestMaster_Admin'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
IF @RoleName <> N'public' and (select is_fixed_role from sys.database_principals where name = @RoleName) = 0
BEGIN
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id
		from sys.database_role_members
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName AND type = 'R'))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName
	
	DECLARE @SQL NVARCHAR(4000)

	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		SET @SQL = 'ALTER ROLE '+ QUOTENAME(@RoleName,'[') +' DROP MEMBER '+ QUOTENAME(@RoleMemberName,'[')
		EXEC(@SQL)
		
		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
END
/****** Object:  DatabaseRole [InvestMaster_Admin]    Script Date: 6/7/2013 5:03:52 PM ******/
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'InvestMaster_Admin' AND type = 'R')
DROP ROLE [InvestMaster_Admin]
GO

DECLARE @RoleName sysname
set @RoleName = N'InvestMaster_AddTransaction'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
IF @RoleName <> N'public' and (select is_fixed_role from sys.database_principals where name = @RoleName) = 0
BEGIN
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id
		from sys.database_role_members
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName AND type = 'R'))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName
	
	DECLARE @SQL NVARCHAR(4000)

	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		SET @SQL = 'ALTER ROLE '+ QUOTENAME(@RoleName,'[') +' DROP MEMBER '+ QUOTENAME(@RoleMemberName,'[')
		EXEC(@SQL)
		
		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
END
/****** Object:  DatabaseRole [InvestMaster_AddTransaction]    Script Date: 6/7/2013 5:03:52 PM ******/
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'InvestMaster_AddTransaction' AND type = 'R')
DROP ROLE [InvestMaster_AddTransaction]
GO

DECLARE @RoleName sysname
set @RoleName = N'Genoa_Read'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
IF @RoleName <> N'public' and (select is_fixed_role from sys.database_principals where name = @RoleName) = 0
BEGIN
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id
		from sys.database_role_members
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName AND type = 'R'))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName
	
	DECLARE @SQL NVARCHAR(4000)

	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		SET @SQL = 'ALTER ROLE '+ QUOTENAME(@RoleName,'[') +' DROP MEMBER '+ QUOTENAME(@RoleMemberName,'[')
		EXEC(@SQL)
		
		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
END
/****** Object:  DatabaseRole [Genoa_Read]    Script Date: 6/7/2013 5:03:52 PM ******/
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'Genoa_Read' AND type = 'R')
DROP ROLE [Genoa_Read]
GO

DECLARE @RoleName sysname
set @RoleName = N'Genoa_PriceUpdate'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
IF @RoleName <> N'public' and (select is_fixed_role from sys.database_principals where name = @RoleName) = 0
BEGIN
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id
		from sys.database_role_members
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName AND type = 'R'))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName
	
	DECLARE @SQL NVARCHAR(4000)

	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		SET @SQL = 'ALTER ROLE '+ QUOTENAME(@RoleName,'[') +' DROP MEMBER '+ QUOTENAME(@RoleMemberName,'[')
		EXEC(@SQL)
		
		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
END
/****** Object:  DatabaseRole [Genoa_PriceUpdate]    Script Date: 6/7/2013 5:03:52 PM ******/
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'Genoa_PriceUpdate' AND type = 'R')
DROP ROLE [Genoa_PriceUpdate]
GO

DECLARE @RoleName sysname
set @RoleName = N'Genoa_Manager'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
IF @RoleName <> N'public' and (select is_fixed_role from sys.database_principals where name = @RoleName) = 0
BEGIN
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id
		from sys.database_role_members
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName AND type = 'R'))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName
	
	DECLARE @SQL NVARCHAR(4000)

	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		SET @SQL = 'ALTER ROLE '+ QUOTENAME(@RoleName,'[') +' DROP MEMBER '+ QUOTENAME(@RoleMemberName,'[')
		EXEC(@SQL)
		
		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
END
/****** Object:  DatabaseRole [Genoa_Manager]    Script Date: 6/7/2013 5:03:52 PM ******/
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'Genoa_Manager' AND type = 'R')
DROP ROLE [Genoa_Manager]
GO

DECLARE @RoleName sysname
set @RoleName = N'Genoa_GroupEdit'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
IF @RoleName <> N'public' and (select is_fixed_role from sys.database_principals where name = @RoleName) = 0
BEGIN
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id
		from sys.database_role_members
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName AND type = 'R'))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName
	
	DECLARE @SQL NVARCHAR(4000)

	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		SET @SQL = 'ALTER ROLE '+ QUOTENAME(@RoleName,'[') +' DROP MEMBER '+ QUOTENAME(@RoleMemberName,'[')
		EXEC(@SQL)
		
		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
END
/****** Object:  DatabaseRole [Genoa_GroupEdit]    Script Date: 6/7/2013 5:03:52 PM ******/
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'Genoa_GroupEdit' AND type = 'R')
DROP ROLE [Genoa_GroupEdit]
GO

DECLARE @RoleName sysname
set @RoleName = N'Genoa_Admin'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
IF @RoleName <> N'public' and (select is_fixed_role from sys.database_principals where name = @RoleName) = 0
BEGIN
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id
		from sys.database_role_members
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName AND type = 'R'))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName
	
	DECLARE @SQL NVARCHAR(4000)

	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		SET @SQL = 'ALTER ROLE '+ QUOTENAME(@RoleName,'[') +' DROP MEMBER '+ QUOTENAME(@RoleMemberName,'[')
		EXEC(@SQL)
		
		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
END
/****** Object:  DatabaseRole [Genoa_Admin]    Script Date: 6/7/2013 5:03:52 PM ******/
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'Genoa_Admin' AND type = 'R')
DROP ROLE [Genoa_Admin]
GO

DECLARE @RoleName sysname
set @RoleName = N'Florence_Update'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
IF @RoleName <> N'public' and (select is_fixed_role from sys.database_principals where name = @RoleName) = 0
BEGIN
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id
		from sys.database_role_members
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName AND type = 'R'))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName
	
	DECLARE @SQL NVARCHAR(4000)

	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		SET @SQL = 'ALTER ROLE '+ QUOTENAME(@RoleName,'[') +' DROP MEMBER '+ QUOTENAME(@RoleMemberName,'[')
		EXEC(@SQL)
		
		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
END
/****** Object:  DatabaseRole [Florence_Update]    Script Date: 6/7/2013 5:03:52 PM ******/
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'Florence_Update' AND type = 'R')
DROP ROLE [Florence_Update]
GO

DECLARE @RoleName sysname
set @RoleName = N'Florence_Read'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
IF @RoleName <> N'public' and (select is_fixed_role from sys.database_principals where name = @RoleName) = 0
BEGIN
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id
		from sys.database_role_members
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName AND type = 'R'))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName
	
	DECLARE @SQL NVARCHAR(4000)

	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		SET @SQL = 'ALTER ROLE '+ QUOTENAME(@RoleName,'[') +' DROP MEMBER '+ QUOTENAME(@RoleMemberName,'[')
		EXEC(@SQL)
		
		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
END
/****** Object:  DatabaseRole [Florence_Read]    Script Date: 6/7/2013 5:03:52 PM ******/
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'Florence_Read' AND type = 'R')
DROP ROLE [Florence_Read]
GO

DECLARE @RoleName sysname
set @RoleName = N'CTA_Write'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
IF @RoleName <> N'public' and (select is_fixed_role from sys.database_principals where name = @RoleName) = 0
BEGIN
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id
		from sys.database_role_members
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName AND type = 'R'))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName
	
	DECLARE @SQL NVARCHAR(4000)

	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		SET @SQL = 'ALTER ROLE '+ QUOTENAME(@RoleName,'[') +' DROP MEMBER '+ QUOTENAME(@RoleMemberName,'[')
		EXEC(@SQL)
		
		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
END
/****** Object:  DatabaseRole [CTA_Write]    Script Date: 6/7/2013 5:03:52 PM ******/
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'CTA_Write' AND type = 'R')
DROP ROLE [CTA_Write]
GO

DECLARE @RoleName sysname
set @RoleName = N'CTA_Read'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
IF @RoleName <> N'public' and (select is_fixed_role from sys.database_principals where name = @RoleName) = 0
BEGIN
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from sys.database_principals 
	where principal_id in ( 
		select member_principal_id
		from sys.database_role_members
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName AND type = 'R'))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName
	
	DECLARE @SQL NVARCHAR(4000)

	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		SET @SQL = 'ALTER ROLE '+ QUOTENAME(@RoleName,'[') +' DROP MEMBER '+ QUOTENAME(@RoleMemberName,'[')
		EXEC(@SQL)
		
		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
END
/****** Object:  DatabaseRole [CTA_Read]    Script Date: 6/7/2013 5:03:52 PM ******/
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'CTA_Read' AND type = 'R')
DROP ROLE [CTA_Read]
GO
/****** Object:  DatabaseRole [CTA_Read]    Script Date: 6/7/2013 5:03:52 PM ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'CTA_Read' AND type = 'R')
CREATE ROLE [CTA_Read]
GO
/****** Object:  DatabaseRole [CTA_Write]    Script Date: 6/7/2013 5:03:52 PM ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'CTA_Write' AND type = 'R')
CREATE ROLE [CTA_Write]
GO
/****** Object:  DatabaseRole [Florence_Read]    Script Date: 6/7/2013 5:03:52 PM ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'Florence_Read' AND type = 'R')
CREATE ROLE [Florence_Read]
GO
/****** Object:  DatabaseRole [Florence_Update]    Script Date: 6/7/2013 5:03:52 PM ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'Florence_Update' AND type = 'R')
CREATE ROLE [Florence_Update]
GO
/****** Object:  DatabaseRole [Genoa_Admin]    Script Date: 6/7/2013 5:03:52 PM ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'Genoa_Admin' AND type = 'R')
CREATE ROLE [Genoa_Admin]
GO
/****** Object:  DatabaseRole [Genoa_GroupEdit]    Script Date: 6/7/2013 5:03:52 PM ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'Genoa_GroupEdit' AND type = 'R')
CREATE ROLE [Genoa_GroupEdit]
GO
/****** Object:  DatabaseRole [Genoa_Manager]    Script Date: 6/7/2013 5:03:52 PM ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'Genoa_Manager' AND type = 'R')
CREATE ROLE [Genoa_Manager]
GO
/****** Object:  DatabaseRole [Genoa_PriceUpdate]    Script Date: 6/7/2013 5:03:52 PM ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'Genoa_PriceUpdate' AND type = 'R')
CREATE ROLE [Genoa_PriceUpdate]
GO
/****** Object:  DatabaseRole [Genoa_Read]    Script Date: 6/7/2013 5:03:52 PM ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'Genoa_Read' AND type = 'R')
CREATE ROLE [Genoa_Read]
GO
/****** Object:  DatabaseRole [InvestMaster_AddTransaction]    Script Date: 6/7/2013 5:03:52 PM ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'InvestMaster_AddTransaction' AND type = 'R')
CREATE ROLE [InvestMaster_AddTransaction]
GO
/****** Object:  DatabaseRole [InvestMaster_Admin]    Script Date: 6/7/2013 5:03:52 PM ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'InvestMaster_Admin' AND type = 'R')
CREATE ROLE [InvestMaster_Admin]
GO
/****** Object:  DatabaseRole [InvestMaster_DataEntry]    Script Date: 6/7/2013 5:03:52 PM ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'InvestMaster_DataEntry' AND type = 'R')
CREATE ROLE [InvestMaster_DataEntry]
GO
/****** Object:  DatabaseRole [InvestMaster_Manager]    Script Date: 6/7/2013 5:03:52 PM ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'InvestMaster_Manager' AND type = 'R')
CREATE ROLE [InvestMaster_Manager]
GO
/****** Object:  DatabaseRole [InvestMaster_MonthEnd]    Script Date: 6/7/2013 5:03:52 PM ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'InvestMaster_MonthEnd' AND type = 'R')
CREATE ROLE [InvestMaster_MonthEnd]
GO
/****** Object:  DatabaseRole [InvestMaster_RiskManager]    Script Date: 6/7/2013 5:03:52 PM ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'InvestMaster_RiskManager' AND type = 'R')
CREATE ROLE [InvestMaster_RiskManager]
GO
/****** Object:  DatabaseRole [InvestMaster_Sales]    Script Date: 6/7/2013 5:03:52 PM ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'InvestMaster_Sales' AND type = 'R')
CREATE ROLE [InvestMaster_Sales]
GO
/****** Object:  DatabaseRole [Michigan_Role]    Script Date: 6/7/2013 5:03:52 PM ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'Michigan_Role' AND type = 'R')
CREATE ROLE [Michigan_Role]
GO
/****** Object:  DatabaseRole [Naples_Role]    Script Date: 6/7/2013 5:03:52 PM ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'Naples_Role' AND type = 'R')
CREATE ROLE [Naples_Role]
GO
/****** Object:  DatabaseRole [Reporter]    Script Date: 6/7/2013 5:03:52 PM ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'Reporter' AND type = 'R')
CREATE ROLE [Reporter]
GO
/****** Object:  DatabaseRole [Sienna_Read]    Script Date: 6/7/2013 5:03:52 PM ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'Sienna_Read' AND type = 'R')
CREATE ROLE [Sienna_Read]
GO
/****** Object:  DatabaseRole [Sienna_Update]    Script Date: 6/7/2013 5:03:52 PM ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'Sienna_Update' AND type = 'R')
CREATE ROLE [Sienna_Update]
GO
/****** Object:  DatabaseRole [web_server]    Script Date: 6/7/2013 5:03:52 PM ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'web_server' AND type = 'R')
CREATE ROLE [web_server]
GO
/****** Object:  DatabaseRole [web_TradeProcessing]    Script Date: 6/7/2013 5:03:52 PM ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'web_TradeProcessing' AND type = 'R')
CREATE ROLE [web_TradeProcessing]
GO
ALTER ROLE [InvestMaster_Sales] ADD MEMBER [web_TradeProcessing]
GO
