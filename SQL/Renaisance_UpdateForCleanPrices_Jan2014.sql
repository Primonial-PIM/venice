USE [Renaissance]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_tblTransaction_SelectKD]    Script Date: 22/01/2014 10:50:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[fn_tblTransaction_SelectKD]
(
@KnowledgeDate datetime
)
RETURNS TABLE
AS
RETURN
(
SELECT 	RN ,
	AuditID ,
	TransactionID ,
	TransactionTicket ,
	TransactionFund ,
	IIF(TransactionSubFund = 0, TransactionFund, TransactionSubFund) AS TransactionSubFund,
	TransactionInstrument ,
	TransactionType ,
	TransactionType_Contra ,
	TransactionValueorAmount ,
	TransactionUnits ,
	TransactionSignedUnits ,
	TransactionPrice ,
	TransactionCleanPrice ,
	TransactionAccruedInterest ,
	TransactionBrokerFees ,
	TransactionCosts ,
	TransactionCostPercent , 
	TransactionSettlement ,
	TransactionSignedSettlement ,
	TransactionFXRate ,
	TransactionSettlementCurrencyID, 
	TransactionEffectivePrice , 
	TransactionEffectiveWatermark , 
	TransactionSpecificInitialEqualisationValue ,
	TransactionCounterparty ,
	TransactionBroker, 
	TransactionBrokerStrategy,
	TransactionInvestment ,
	TransactionExecution ,
	TransactionDataEntry ,
	TransactionDecisionDate ,
	TransactionValueDate ,
	TransactionFIFOValueDate ,
	TransactionEffectiveValueDate , 
	TransactionSettlementDate ,
	TransactionConfirmationDate ,
	TransactionEntryDate ,
	TransactionCostIsPercent , 
	TransactionExemptFromUpdate , 
	TransactionRedeemWholeHoldingFlag , 
	TransactionIsTransfer , 
	TransactionSpecificInitialEqualisationFlag , 
	TransactionFuturesStylePricing , 
	TransactionClosingTrade, 
	TransactionIsFX,
	TransactionFinalAmount ,
	TransactionFinalPrice ,
	TransactionFinalCosts ,
	TransactionFinalSettlement ,
	TransactionInstructionFlag ,
	TransactionBankConfirmation ,
	TransactionInitialDataEntry ,
	TransactionAmountConfirmed ,
	TransactionSettlementConfirmed ,
	TransactionFinalDataEntry ,
	TransactionComment ,
	TransactionParentID ,
	TransactionRedemptionID, 
	TransactionVersusInstrument, 
	TransactionSpecificToFundUnitID,
	TransactionUser ,
	TransactionGroup ,
	TransactionTradeStatusID, 
	TransactionLeg ,
	TransactionAdminStatus ,
	TransactionCTFLAGS ,
	TransactionCompoundRN ,
	TransactionDateEntered,
	TransactionDateDeleted
FROM tblTransaction
WHERE RN IN ( SELECT MAX(RN) 
              FROM tblTransaction 
              WHERE (((TransactionDateEntered <= @KnowledgeDate) or (TransactionDateEntered IS NULL)) OR (ISNULL(@KnowledgeDate, '1900-01-01') <= '1900-01-01')) AND 
                    (((TransactionDateDeleted > @KnowledgeDate) or (TransactionDateDeleted IS NULL)) AND (NOT ((ISNULL(@KnowledgeDate, '1900-01-01')  <= '1900-01-01') AND (NOT (TransactionDateDeleted is NULL)))))
              GROUP BY TransactionParentID, TransactionLeg )


)


GO
USE [Renaissance]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_tblTransaction_ContraLeg_SelectKD]    Script Date: 22/01/2014 10:51:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER FUNCTION [dbo].[fn_tblTransaction_ContraLeg_SelectKD]
(
@KnowledgeDate datetime
)
RETURNS TABLE
AS
RETURN
(
SELECT 	RN ,
	AuditID ,
	TransactionID ,
	TransactionTicket ,
	TransactionFund ,
	IIF(TransactionSubFund = 0, TransactionFund, TransactionSubFund) AS TransactionSubFund,
	TransactionInstrument ,
	TransactionType ,
	TransactionType_Contra ,
	TransactionValueorAmount ,
	TransactionUnits ,
	TransactionSignedUnits ,
	TransactionPrice ,
	TransactionCleanPrice ,
	TransactionAccruedInterest ,
	TransactionBrokerFees ,
	TransactionCosts ,
	TransactionCostPercent , 
	TransactionSettlement ,
	TransactionSignedSettlement ,
	TransactionFXRate ,
	TransactionSettlementCurrencyID, 
	TransactionEffectivePrice , 
	TransactionEffectiveWatermark , 
	TransactionSpecificInitialEqualisationValue ,
	TransactionCounterparty ,
	TransactionBroker, 
	TransactionBrokerStrategy,
	TransactionInvestment ,
	TransactionExecution ,
	TransactionDataEntry ,
	TransactionDecisionDate ,
	TransactionValueDate ,
	TransactionFIFOValueDate ,
	TransactionEffectiveValueDate , 
	TransactionSettlementDate ,
	TransactionConfirmationDate ,
	TransactionEntryDate ,
	TransactionCostIsPercent , 
	TransactionExemptFromUpdate , 
	TransactionRedeemWholeHoldingFlag , 
	TransactionIsTransfer , 
	TransactionSpecificInitialEqualisationFlag , 
	TransactionFuturesStylePricing , 
	TransactionClosingTrade, 
	TransactionIsFX,
	TransactionFinalAmount ,
	TransactionFinalPrice ,
	TransactionFinalCosts ,
	TransactionFinalSettlement ,
	TransactionInstructionFlag ,
	TransactionBankConfirmation ,
	TransactionInitialDataEntry ,
	TransactionAmountConfirmed ,
	TransactionSettlementConfirmed ,
	TransactionFinalDataEntry ,
	TransactionComment ,
	TransactionParentID ,
	TransactionRedemptionID, 
	TransactionVersusInstrument, 
	TransactionSpecificToFundUnitID,
	TransactionUser ,
	TransactionGroup ,
	TransactionTradeStatusID, 
	TransactionLeg ,
	TransactionAdminStatus ,
	TransactionCTFLAGS ,
	TransactionCompoundRN ,
	TransactionDateEntered,
	TransactionDateDeleted
FROM tblTransaction
WHERE RN IN ( SELECT MAX(RN) 
              FROM tblTransaction 
              WHERE (((TransactionDateEntered <= @KnowledgeDate) or (TransactionDateEntered IS NULL)) OR (ISNULL(@KnowledgeDate, '1900-01-01') <= '1900-01-01')) AND 
                    (((TransactionDateDeleted > @KnowledgeDate) or (TransactionDateDeleted IS NULL)) AND (NOT ((ISNULL(@KnowledgeDate, '1900-01-01')  <= '1900-01-01') AND (NOT (TransactionDateDeleted is NULL))))) AND 
                    (TransactionParentID > 0) AND (TransactionLeg = 2)
              GROUP BY TransactionParentID, TransactionLeg )

)


GO
USE [Renaissance]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_tblTransaction_FirstLeg_SelectKD]    Script Date: 22/01/2014 10:52:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[fn_tblTransaction_FirstLeg_SelectKD]
(
@KnowledgeDate datetime
)
RETURNS TABLE
AS
RETURN
(
SELECT 	RN ,
	AuditID ,
	TransactionID ,
	TransactionTicket ,
	TransactionFund ,
	IIF(TransactionSubFund = 0, TransactionFund, TransactionSubFund) AS TransactionSubFund,
	TransactionInstrument ,
	TransactionType ,
	TransactionType_Contra ,
	TransactionValueorAmount ,
	TransactionUnits ,
	TransactionSignedUnits ,
	TransactionPrice ,
	TransactionCleanPrice ,
	TransactionAccruedInterest ,
	TransactionBrokerFees ,
	TransactionCosts ,
	TransactionCostPercent , 
	TransactionSettlement ,
	TransactionSignedSettlement ,
	TransactionFXRate ,
	TransactionSettlementCurrencyID, 
	TransactionEffectivePrice , 
	TransactionEffectiveWatermark , 
	TransactionSpecificInitialEqualisationValue ,
	TransactionCounterparty ,
	TransactionBroker, 
	TransactionBrokerStrategy,
	TransactionInvestment ,
	TransactionExecution ,
	TransactionDataEntry ,
	TransactionDecisionDate ,
	TransactionValueDate ,
	TransactionFIFOValueDate ,
	TransactionEffectiveValueDate , 
	TransactionSettlementDate ,
	TransactionConfirmationDate ,
	TransactionEntryDate ,
	TransactionCostIsPercent , 
	TransactionExemptFromUpdate , 
	TransactionRedeemWholeHoldingFlag , 
	TransactionIsTransfer , 
	TransactionSpecificInitialEqualisationFlag , 
	TransactionFuturesStylePricing , 
	TransactionClosingTrade, 
	TransactionIsFX,
	TransactionFinalAmount ,
	TransactionFinalPrice ,
	TransactionFinalCosts ,
	TransactionFinalSettlement ,
	TransactionInstructionFlag ,
	TransactionBankConfirmation ,
	TransactionInitialDataEntry ,
	TransactionAmountConfirmed ,
	TransactionSettlementConfirmed ,
	TransactionFinalDataEntry ,
	TransactionComment ,
	TransactionParentID ,
	TransactionRedemptionID, 
	TransactionVersusInstrument, 
	TransactionSpecificToFundUnitID,
	TransactionUser ,
	TransactionGroup ,
	TransactionTradeStatusID, 
	TransactionLeg ,
	TransactionAdminStatus ,
	TransactionCTFLAGS ,
	TransactionCompoundRN ,
	TransactionDateEntered,
	TransactionDateDeleted
FROM tblTransaction
WHERE RN IN ( SELECT MAX(RN) 
              FROM tblTransaction 
              WHERE (((TransactionDateEntered <= @KnowledgeDate) or (TransactionDateEntered IS NULL)) OR (ISNULL(@KnowledgeDate, '1900-01-01') <= '1900-01-01')) AND 
                    (((TransactionDateDeleted > @KnowledgeDate) or (TransactionDateDeleted IS NULL)) AND (NOT ((ISNULL(@KnowledgeDate, '1900-01-01')  <= '1900-01-01') AND (NOT (TransactionDateDeleted is NULL))))) AND 
                    (TransactionParentID > 0) AND (TransactionLeg = 1)
              GROUP BY TransactionParentID, TransactionLeg )

)

GO
USE [Renaissance]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_tblTransactionSingle_SelectKD]    Script Date: 22/01/2014 10:53:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER FUNCTION [dbo].[fn_tblTransactionSingle_SelectKD]
(
@TransactionParentID int,
@BothLegs int,
@KnowledgeDate datetime
)
RETURNS TABLE
AS
RETURN
(
SELECT 	RN ,
	AuditID ,
	TransactionID ,
	TransactionTicket ,
	TransactionFund ,
	IIF(TransactionSubFund = 0, TransactionFund, TransactionSubFund) AS TransactionSubFund,
	TransactionInstrument ,
	TransactionType ,
	TransactionType_Contra ,
	TransactionValueorAmount ,
	TransactionUnits ,
	TransactionSignedUnits ,
	TransactionPrice ,
	TransactionCleanPrice ,
	TransactionAccruedInterest ,
	TransactionBrokerFees ,
	TransactionCosts ,
	TransactionCostPercent , 
	TransactionSettlement ,
	TransactionSignedSettlement ,
	TransactionFXRate ,
	TransactionSettlementCurrencyID, 
	TransactionEffectivePrice , 
	TransactionEffectiveWatermark , 
	TransactionSpecificInitialEqualisationValue ,
	TransactionCounterparty ,
	TransactionBroker, 
	TransactionBrokerStrategy,
	TransactionInvestment ,
	TransactionExecution ,
	TransactionDataEntry ,
	TransactionDecisionDate ,
	TransactionValueDate ,
	TransactionFIFOValueDate ,
	TransactionEffectiveValueDate , 
	TransactionSettlementDate ,
	TransactionConfirmationDate ,
	TransactionEntryDate ,
	TransactionCostIsPercent , 
	TransactionExemptFromUpdate , 
	TransactionRedeemWholeHoldingFlag , 
	TransactionIsTransfer , 
	TransactionSpecificInitialEqualisationFlag , 
	TransactionFuturesStylePricing , 
	TransactionClosingTrade ,
	TransactionIsFX,
	TransactionFinalAmount ,
	TransactionFinalPrice ,
	TransactionFinalCosts ,
	TransactionFinalSettlement ,
	TransactionInstructionFlag ,
	TransactionBankConfirmation ,
	TransactionInitialDataEntry ,
	TransactionAmountConfirmed ,
	TransactionSettlementConfirmed ,
	TransactionFinalDataEntry ,
	TransactionComment ,
	TransactionParentID ,
	TransactionRedemptionID, 
	TransactionVersusInstrument, 
	TransactionSpecificToFundUnitID, 
	TransactionUser ,
	TransactionGroup ,
	TransactionTradeStatusID, 
	TransactionLeg ,
	TransactionAdminStatus ,
	TransactionCTFLAGS ,
	TransactionCompoundRN ,
	TransactionDateEntered,
	TransactionDateDeleted
FROM tblTransaction
WHERE RN IN ( SELECT MAX(RN) 
              FROM tblTransaction 
              WHERE 
                    ((@TransactionParentID <= 0) OR (TransactionParentID = @TransactionParentID)) AND 
                    ((@BothLegs <> 0) OR (TransactionLeg = 1)) AND
                    (((TransactionDateEntered <= @KnowledgeDate) or (TransactionDateEntered IS NULL)) OR (ISNULL(@KnowledgeDate, '1900-01-01') <= '1900-01-01')) AND 
                    (((TransactionDateDeleted > @KnowledgeDate) or (TransactionDateDeleted IS NULL)) AND (NOT ((ISNULL(@KnowledgeDate, '1900-01-01')  <= '1900-01-01') AND (NOT (TransactionDateDeleted is NULL)))))
              GROUP BY TransactionParentID, TransactionLeg )


)

GO

USE [Renaissance]
GO
/****** Object:  StoredProcedure [dbo].[adp_tblTransaction_FirstLeg_SelectCommand]    Script Date: 22/01/2014 10:54:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[adp_tblTransaction_FirstLeg_SelectCommand]
(
@AuditID int = 0,
@KnowledgeDate datetime = Null
)
AS
	SET NOCOUNT ON;

  IF (ISNULL(@AuditID, 0) <= 0)
  BEGIN
	  SELECT	[RN], 
		  [AuditID], 
		  TransactionID ,
		  TransactionTicket ,
		  TransactionFund ,
		  TransactionSubFund ,
		  TransactionInstrument ,
		  TransactionType ,
		  TransactionType_Contra ,
		  TransactionValueorAmount ,
		  TransactionUnits ,
		  TransactionSignedUnits ,
		  TransactionPrice ,
			TransactionCleanPrice ,
			TransactionAccruedInterest ,
			TransactionBrokerFees ,
		  TransactionCosts ,
		  TransactionCostPercent , 
		  TransactionSettlement ,
		  TransactionSignedSettlement ,
		  TransactionFXRate ,
		  TransactionSettlementCurrencyID, 
		  TransactionEffectivePrice , 
		  TransactionEffectiveWatermark , 
		  TransactionSpecificInitialEqualisationValue ,
		  TransactionCounterparty ,
			TransactionBroker, 
			TransactionBrokerStrategy,
		  TransactionInvestment ,
		  TransactionExecution ,
		  TransactionDataEntry ,
		  TransactionDecisionDate ,
		  TransactionValueDate ,
		  TransactionFIFOValueDate ,
		  TransactionEffectiveValueDate , 
		  TransactionSettlementDate ,
		  TransactionConfirmationDate ,
		  TransactionEntryDate ,
		  TransactionCostIsPercent , 
		  TransactionExemptFromUpdate , 
		  TransactionRedeemWholeHoldingFlag ,
		  TransactionIsTransfer , 
		  TransactionSpecificInitialEqualisationFlag , 
		  TransactionFuturesStylePricing , 
		  TransactionFinalAmount ,
		  TransactionFinalPrice ,
		  TransactionFinalCosts ,
		  TransactionFinalSettlement ,
		  TransactionInstructionFlag ,
		  TransactionBankConfirmation ,
		  TransactionInitialDataEntry ,
		  TransactionAmountConfirmed ,
		  TransactionSettlementConfirmed ,
		  TransactionFinalDataEntry ,
		  TransactionComment ,
		  TransactionParentID ,
		  TransactionRedemptionID ,
		  TransactionVersusInstrument , 
			TransactionSpecificToFundUnitID,
		  TransactionUser ,
		  TransactionGroup ,
      TransactionTradeStatusID, 
		  TransactionLeg ,
		  TransactionAdminStatus ,
		  TransactionCTFLAGS ,
		  TransactionCompoundRN ,
		  TransactionDateEntered,
		  TransactionDateDeleted
			
	  FROM fn_tblTransaction_FirstLeg_SelectKD(@Knowledgedate)
	  WHERE TransactionLeg = 1
	END
  ELSE
  BEGIN
	  SELECT	[RN], 
		  [AuditID], 
		  TransactionID ,
		  TransactionTicket ,
		  TransactionFund ,
		  TransactionSubFund ,
		  TransactionInstrument ,
		  TransactionType ,
		  TransactionType_Contra ,
		  TransactionValueorAmount ,
		  TransactionUnits ,
		  TransactionSignedUnits ,
		  TransactionPrice ,
			TransactionCleanPrice ,
			TransactionAccruedInterest ,
			TransactionBrokerFees ,
		  TransactionCosts ,
		  TransactionCostPercent , 
		  TransactionSettlement ,
		  TransactionSignedSettlement ,
		  TransactionFXRate ,
		  TransactionSettlementCurrencyID, 
		  TransactionEffectivePrice , 
		  TransactionEffectiveWatermark , 
		  TransactionSpecificInitialEqualisationValue ,
		  TransactionCounterparty ,
			TransactionBroker, 
			TransactionBrokerStrategy,
		  TransactionInvestment ,
		  TransactionExecution ,
		  TransactionDataEntry ,
		  TransactionDecisionDate ,
		  TransactionValueDate ,
		  TransactionFIFOValueDate ,
		  TransactionEffectiveValueDate , 
		  TransactionSettlementDate ,
		  TransactionConfirmationDate ,
		  TransactionEntryDate ,
		  TransactionCostIsPercent , 
		  TransactionExemptFromUpdate , 
		  TransactionRedeemWholeHoldingFlag ,
		  TransactionIsTransfer , 
		  TransactionSpecificInitialEqualisationFlag , 
		  TransactionFuturesStylePricing , 
		  TransactionFinalAmount ,
		  TransactionFinalPrice ,
		  TransactionFinalCosts ,
		  TransactionFinalSettlement ,
		  TransactionInstructionFlag ,
		  TransactionBankConfirmation ,
		  TransactionInitialDataEntry ,
		  TransactionAmountConfirmed ,
		  TransactionSettlementConfirmed ,
		  TransactionFinalDataEntry ,
		  TransactionComment ,
		  TransactionParentID ,
		  TransactionRedemptionID ,
		  TransactionVersusInstrument , 
			TransactionSpecificToFundUnitID,
		  TransactionUser ,
		  TransactionGroup ,
      TransactionTradeStatusID, 
		  TransactionLeg ,
		  TransactionAdminStatus ,
		  TransactionCTFLAGS ,
		  TransactionCompoundRN ,
		  TransactionDateEntered,
		  TransactionDateDeleted
			
	  FROM fn_tblTransactionSingle_SelectKD(@AuditID, 0, @Knowledgedate)
	  WHERE TransactionLeg = 1
  END
  		
	RETURN -1

GO

USE [Renaissance]
GO
/****** Object:  StoredProcedure [dbo].[adp_tblTransaction_SelectCommand]    Script Date: 22/01/2014 10:54:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[adp_tblTransaction_SelectCommand]
(
@AuditID int = 0,
@KnowledgeDate datetime = Null
)
AS
	SET NOCOUNT ON

  IF (ISNULL(@AuditID, 0) <= 0)
  BEGIN
	  SELECT	[RN], 
		  [AuditID], 
		  TransactionID ,
		  TransactionTicket ,
		  TransactionFund ,
		  TransactionSubFund ,
		  TransactionInstrument ,
		  TransactionType ,
		  TransactionType_Contra ,
		  TransactionValueorAmount ,
		  TransactionUnits ,
		  TransactionSignedUnits ,
		  TransactionPrice ,
			TransactionCleanPrice ,
			TransactionAccruedInterest ,
			TransactionBrokerFees ,
		  TransactionCosts ,
		  TransactionCostPercent , 
		  TransactionSettlement ,
		  TransactionSignedSettlement ,
		  TransactionFXRate ,
		  TransactionSettlementCurrencyID, 
		  TransactionEffectivePrice , 
		  TransactionEffectiveWatermark , 
		  TransactionSpecificInitialEqualisationValue ,
		  TransactionCounterparty ,
			TransactionBroker, 
			TransactionBrokerStrategy,
		  TransactionInvestment ,
		  TransactionExecution ,
		  TransactionDataEntry ,
		  TransactionDecisionDate ,
		  TransactionValueDate ,
		  TransactionFIFOValueDate ,
		  TransactionEffectiveValueDate , 
		  TransactionSettlementDate ,
		  TransactionConfirmationDate ,
		  TransactionEntryDate ,
		  TransactionCostIsPercent , 
		  TransactionExemptFromUpdate , 
		  TransactionRedeemWholeHoldingFlag ,
		  TransactionIsTransfer , 
		  TransactionSpecificInitialEqualisationFlag , 
		  TransactionFuturesStylePricing , 
			TransactionClosingTrade ,
		  TransactionFinalAmount ,
		  TransactionFinalPrice ,
		  TransactionFinalCosts ,
		  TransactionFinalSettlement ,
		  TransactionInstructionFlag ,
		  TransactionBankConfirmation ,
		  TransactionInitialDataEntry ,
		  TransactionAmountConfirmed ,
		  TransactionSettlementConfirmed ,
		  TransactionFinalDataEntry ,
		  TransactionComment ,
		  TransactionParentID ,
		  TransactionRedemptionID ,
		  TransactionVersusInstrument , 
			TransactionSpecificToFundUnitID, 
		  TransactionUser ,
		  TransactionGroup ,
      TransactionTradeStatusID, 
		  TransactionLeg ,
		  TransactionAdminStatus ,
		  TransactionCTFLAGS ,
		  TransactionCompoundRN ,
		  TransactionDateEntered,
		  TransactionDateDeleted
			
	  FROM fn_tblTransaction_SelectKD(@Knowledgedate)
	END
  ELSE
  BEGIN
	  SELECT	[RN], 
		  [AuditID], 
		  TransactionID ,
		  TransactionTicket ,
		  TransactionFund ,
		  TransactionSubFund ,
		  TransactionInstrument ,
		  TransactionType ,
		  TransactionType_Contra ,
		  TransactionValueorAmount ,
		  TransactionUnits ,
		  TransactionSignedUnits ,
		  TransactionPrice ,
			TransactionCleanPrice ,
			TransactionAccruedInterest ,
			TransactionBrokerFees ,
		  TransactionCosts ,
		  TransactionCostPercent , 
		  TransactionSettlement ,
		  TransactionSignedSettlement ,
		  TransactionFXRate ,
		  TransactionSettlementCurrencyID, 
		  TransactionEffectivePrice , 
		  TransactionEffectiveWatermark , 
		  TransactionSpecificInitialEqualisationValue ,
		  TransactionCounterparty ,
			TransactionBroker, 
			TransactionBrokerStrategy,
		  TransactionInvestment ,
		  TransactionExecution ,
		  TransactionDataEntry ,
		  TransactionDecisionDate ,
		  TransactionValueDate ,
		  TransactionFIFOValueDate ,
		  TransactionEffectiveValueDate , 
		  TransactionSettlementDate ,
		  TransactionConfirmationDate ,
		  TransactionEntryDate ,
		  TransactionCostIsPercent , 
		  TransactionExemptFromUpdate , 
		  TransactionRedeemWholeHoldingFlag ,
		  TransactionIsTransfer , 
		  TransactionSpecificInitialEqualisationFlag , 
		  TransactionFuturesStylePricing , 
			TransactionClosingTrade ,
		  TransactionFinalAmount ,
		  TransactionFinalPrice ,
		  TransactionFinalCosts ,
		  TransactionFinalSettlement ,
		  TransactionInstructionFlag ,
		  TransactionBankConfirmation ,
		  TransactionInitialDataEntry ,
		  TransactionAmountConfirmed ,
		  TransactionSettlementConfirmed ,
		  TransactionFinalDataEntry ,
		  TransactionComment ,
		  TransactionParentID ,
		  TransactionRedemptionID ,
		  TransactionVersusInstrument , 
			TransactionSpecificToFundUnitID, 
		  TransactionUser ,
		  TransactionGroup ,
      TransactionTradeStatusID, 
		  TransactionLeg ,
		  TransactionAdminStatus ,
		  TransactionCTFLAGS ,
		  TransactionCompoundRN ,
		  TransactionDateEntered,
		  TransactionDateDeleted
			
	  FROM fn_tblTransactionSingle_SelectKD(@AuditID, 1, @Knowledgedate)
  END
  		
	RETURN -1

GO

USE [Renaissance]
GO
/****** Object:  StoredProcedure [dbo].[adp_tblTransaction_InsertCommand]    Script Date: 22/01/2014 10:54:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[adp_tblTransaction_InsertCommand]
	(
	@TransactionTicket nvarchar (50),
	@TransactionFund int,
	@TransactionSubFund int = Null, 
	@TransactionInstrument int,
	@TransactionType int,
	@TransactionValueorAmount nvarchar (10),
	@TransactionUnits float = 0,
	@TransactionPrice float = 0,
	@TransactionCleanPrice float = 0,
	@TransactionAccruedInterest float = 0,
	@TransactionBrokerFees float = 0,
	@TransactionCosts float = 0,
	@TransactionCostPercent  float = 0, 
	@TransactionFXRate float = 1,                     -- If Settlement Currency != Instrument Currency, Cash Settlement = Holding * Price * Multiplier * FX
	@TransactionSettlementCurrencyID int = 0,
	@TransactionUseFixedSettlement int = 0,           -- Ignore the usual rules and just use the given Settlement figure in @TransactionFixedSettlementValue
	@TransactionFixedSettlementValue float = 0,       -- See above...   All due to B'stard Sophis.
	@TransactionEffectivePrice float = NULL, 
	@TransactionEffectiveWatermark float = NULL, 
	@TransactionSpecificInitialEqualisationValue float = 0,
	@TransactionCounterparty int,
	@TransactionBroker int = 0, 
	@TransactionBrokerStrategy varchar (50) = '',
	@TransactionInvestment nvarchar (50),
	@TransactionExecution nvarchar (50),
	@TransactionDataEntry nvarchar (50),
	@TransactionDecisionDate datetime,
	@TransactionValueDate datetime,
	@TransactionFIFOValueDate datetime,
	@TransactionEffectiveValueDate datetime = NULL, 
	@TransactionSettlementDate datetime,
	@TransactionConfirmationDate datetime,
	@TransactionCostIsPercent bit = 0, 
	@TransactionExemptFromUpdate bit = 0, 
	@TransactionRedeemWholeHoldingFlag bit = 0,
	@TransactionIsTransfer bit = 0, 
	@TransactionSpecificInitialEqualisationFlag bit = 0, 
	@TransactionClosingTrade bit = 0, 
	@TransactionFinalAmount int = 0,
	@TransactionFinalPrice int = 0,
	@TransactionFinalCosts int = 0,
	@TransactionFinalSettlement int = 0,
	@TransactionInstructionFlag int = 0,
	@TransactionBankConfirmation int = 0,
	@TransactionInitialDataEntry int = 0,
	@TransactionAmountConfirmed int = 0,
	@TransactionSettlementConfirmed int = 0,
	@TransactionFinalDataEntry int = 0,
	@TransactionComment varchar (500),
	@TransactionParentID int = 0,
	@TransactionRedemptionID int = 0, 
	@TransactionVersusInstrument int = 0, 
	@TransactionSpecificToFundUnitID int = 0, 
	@TransactionGroup nvarchar (50),
	@TransactionTradeStatusID int = 0,        -- For Sophis Import transactions = HISTOMVTS.BACKOFFICE
	@TransactionAdminStatus int = 0,         
	@TransactionCTFLAGS int = 0,
	@TransactionCompoundRN int = 0,
	@KnowledgeDate datetime, 
	@ValidationOnly int = 0,
	@rvStatusString nvarchar(200) OUTPUT	
	)
  --**************************************************************************************************
  -- Purpose: Post a transaction to the 'tblTransaction' table.
  --          This involves posting two records, The first describing the instrument transacted and the
  --          second describing the cash consideration. The consideration is posted in the currency of
  --          the instrument unless it is a FX transaction in which case the consideration is in USD.
  --
  --          UPDATE (October 2012) : Trades and FXs may be entered against a different currency.
  --
  --
  -- Accepts:
  --
  -- Returns: ParentID of the new Transaction.
  --
  --**************************************************************************************************
AS
	SET NOCOUNT OFF

	-- **************************************************
	-- CONSTANTS.
	--
	-- These values used to be looked-up by reference to their
	-- various descriptions, but since the descriptions are as
	-- liable to change as the IDs it seemed easier just to 
	-- consider these IDs as constant.
	-- By and Large, changes to these items are restricted so that
	-- Under any presently imagined scenario, these values ARE
	-- constant.
	-- **************************************************
	DECLARE @DEBUG_INT int
	DECLARE @ERROR_INT int

	DECLARE @TRANSACTIONTYPE_Consideration int
	DECLARE @TRANSACTIONTYPE_Subscribe int
	DECLARE @TRANSACTIONTYPE_Redeem int
	DECLARE @TRANSACTIONTYPE_Buy int
	DECLARE @TRANSACTIONTYPE_Sell int
	DECLARE @TRANSACTIONTYPE_BuyFX int
	DECLARE @TRANSACTIONTYPE_SellFX int
	DECLARE @TRANSACTIONTYPE_BuyFXForward int
	DECLARE @TRANSACTIONTYPE_SellFXForward int
	DECLARE @TRANSACTIONTYPE_Fee int
	DECLARE @TRANSACTIONTYPE_InterestPayment int = 10
	DECLARE @TRANSACTIONTYPE_InterestIncome int = 11
	DECLARE @TRANSACTIONTYPE_Expense_Increase int = 13
	DECLARE @TRANSACTIONTYPE_Expense_Reduce int = 16
	DECLARE @TRANSACTIONTYPE_Provision_Increase int = 17
	DECLARE @TRANSACTIONTYPE_Provision_Reduce int = 18
	
	DECLARE @COUNTERPARTY_MARKET int
	
	DECLARE @INSTRUMENTTYPE_Unit int
	DECLARE @INSTRUMENTTYPE_Cash int
	DECLARE @INSTRUMENTTYPE_Fee int
	DECLARE @INSTRUMENTTYPE_Interest int
	DECLARE @INSTRUMENTTYPE_Expense int
	DECLARE @INSTRUMENTTYPE_UnrealisedPnL int 
	DECLARE @INSTRUMENTTYPE_Notional int 
	
	DECLARE @INSTRUMENT_USD int
	
	SET @TRANSACTIONTYPE_Consideration = 1
	SET @TRANSACTIONTYPE_Subscribe = 2
	SET @TRANSACTIONTYPE_Redeem = 3
	SET @TRANSACTIONTYPE_Buy = 4
	SET @TRANSACTIONTYPE_Sell = 5	
	SET @TRANSACTIONTYPE_BuyFX = 6
	SET @TRANSACTIONTYPE_SellFX = 7	
	SET @TRANSACTIONTYPE_Fee = 8

	SET @TRANSACTIONTYPE_BuyFXForward = 14
	SET @TRANSACTIONTYPE_SellFXForward = 15	
	
	SET @COUNTERPARTY_MARKET = 1

	SET @INSTRUMENTTYPE_Unit = 2
	SET @INSTRUMENTTYPE_Cash = 3
	SET @INSTRUMENTTYPE_Interest = 5
	SET @INSTRUMENTTYPE_Fee = 7
	SET @INSTRUMENTTYPE_Expense = 10
	SET @INSTRUMENTTYPE_UnrealisedPnL = 11
	SET @INSTRUMENTTYPE_Notional = 14

	SET @INSTRUMENT_USD = 1 -- USD Instrument ID - Default settlement instrument for FX transactions

	-- **************************************************

	DECLARE @New_TransactionID int
	DECLARE @New_TransactionParentID int
	DECLARE @New_TransactionTicket nvarchar (50)
	DECLARE @FXFlag bit = 0                       -- Used to set 'TransactionIsFX'
	DECLARE @CashMultiplier float
	DECLARE @TransactionSettlement float
	DECLARE @ModifiedSettlement float             -- Settlement figure for the first leg, for most transactions it equals TransactionSettlement, for FX it equals TransactionUnits.
	DECLARE @SecondLeg_InstrumentID int
	DECLARE @SecondLeg_TransactionType int
	DECLARE @TransactionTypeName nvarchar(50)

	DECLARE @InstrumentType int
	DECLARE @InstrumentCurrencyID int
	DECLARE @InstrumentTypeIsFuturesStyle bit
	DECLARE @InstrumentOrderPrecision int = 4
	DECLARE @InstrumentOrderOnlyByValue bit = 0
	DECLARE @InstrumentLastValidTradeDate datetime
	DECLARE @InstrumentContractSize float = 1
	DECLARE @InstrumentMultiplier float = 1
	DECLARE @CurrencyCode nvarchar (50)

	-- Validate KD, Don't save for non 'LIVE' Knowledgedates.
	IF (ISNULL(@Knowledgedate, '1900-01-01') < (getdate())) AND (ISNULL(@Knowledgedate, '1900-01-01') > '1900-01-01')
	  BEGIN
		SELECT @rvStatusString = 'KnowledgeDate is not LIVE.'
		GOTO ExitWithError
	  END
	  
	-- Validate Parameters
	IF ISNULL(@TransactionFund, 0) <= 0
	  BEGIN
		SELECT @rvStatusString = 'Invalid Fund ID'
		GOTO ExitWithError
	  END

	IF ISNULL(@TransactionInstrument, 0) <= 0
	  BEGIN
		SELECT @rvStatusString = 'Invalid Instrument ID'
		GOTO ExitWithError
	  END

	IF ISNULL(@TransactionType, 0) <= 0
	  BEGIN
		SELECT @rvStatusString = 'Invalid Transaction Type'
		GOTO ExitWithError
	  END
	  
	SELECT @TransactionTypeName = ISNULL(TransactionType, 'Missing Type')
	FROM fn_tblTransactionType_SelectKD(@KnowledgeDate)
	WHERE TransactionTypeID = @TransactionType
	
	IF ISNULL(@TransactionCounterparty, 0) <= 0
	  BEGIN
		SELECT @rvStatusString = 'Invalid Counterparty'
		GOTO ExitWithError
	  END

	IF ISNULL(@TransactionValueDate, '1900-01-01') <= '1900-01-01'
	  BEGIN
		SELECT @rvStatusString = 'ValueDate not set'
		GOTO ExitWithError
	  END

	IF ISNULL(@TransactionSettlementDate, '1900-01-01') <= '1900-01-01'
	  BEGIN
		SELECT @rvStatusString = 'SettlementDate not set'
		GOTO ExitWithError
	  END

	SET @TransactionDecisionDate = ISNULL(@TransactionDecisionDate, @TransactionValueDate)
	SET @TransactionConfirmationDate = ISNULL(@TransactionConfirmationDate, @TransactionValueDate)


	-- ************************************************************************
	-- Parameters are OK, continue...
	-- ************************************************************************

	-- Get Static Data items that we will need.
	-- Get Instrument Currency Code 

	SELECT 
		@InstrumentType = ISNULL(Max(InstrumentType), 0), 
		@InstrumentLastValidTradeDate = ISNULL(Max(InstrumentLastValidTradeDate), '1900-01-01'), 
		@InstrumentCurrencyID = ISNULL(MAX(InstrumentCurrencyID), 0), 
		@InstrumentContractSize = ISNULL(MAX(InstrumentContractSize), 1.0), 
		@InstrumentMultiplier = ISNULL(MAX(InstrumentMultiplier), 1.0),
		@InstrumentOrderPrecision = ISNULL(MAX(InstrumentOrderPrecision), 4),
		@InstrumentOrderOnlyByValue = ISNULL(MAX(IIF(InstrumentOrderOnlyByValue=0,0,1)), 0)
	FROM fn_tblInstrument_SelectSingle(@TransactionInstrument, @KnowledgeDate)
	WHERE InstrumentID = @TransactionInstrument

	-- Get 'Futures Style' indicator.
	
	SELECT @InstrumentTypeIsFuturesStyle = 0
	
	SELECT @InstrumentTypeIsFuturesStyle = ISNULL(InstrumentTypeFuturesStylePricing, 0)
	FROM fn_tblInstrumentType_SelectKD(@KnowledgeDate)
	WHERE InstrumentTypeID = @InstrumentType


	-- Get 'CashMove' appropriate to this TransactionType

	  --  Note :
	  --  TransactionSignedUnits = TransactionUnits * (-1 * CashMultiplier)
	  --  TransactionSignedSettlement = TransactionSettlement * (-1 * CashMultiplier)
	  --  Consideration_TransactionSettlement = TransactionSettlement
	  --  Consideration_TransactionSignedSettlement = -TransactionSignedSettlement
	  --
	  --  set TransactionSigned and TransactionSignedSettlement using the cash multiplier.
	  --  note that the cash multiplier is negated.  This is because it refers to the cash movement on
	  --  the other side of the transaction so for a BUY it is -1 becuase cash is going out.
	
	SELECT @CashMultiplier = ISNULL(Max(TransactionTypeCashmove), 0)
	FROM fn_tblTransactionType_SelectKD(@KnowledgeDate)
	WHERE TransactionTypeID = @TransactionType

	IF @CashMultiplier = 0
	  BEGIN
		SELECT @rvStatusString = 'Failed to resolve Cash Multiplier (or Zero.)'
		GOTO ExitWithError
	  END

  -- Check FXRate.

  IF (ISNULL(@TransactionFXRate, 0) <= 0)
  BEGIN
    SELECT @TransactionFXRate = 1.0
  END

	-- Calculate Settlement Value
	
	IF (@TransactionCostIsPercent <> 0)
	  BEGIN
	    SELECT @TransactionCosts = @TransactionCostPercent * (@TransactionUnits * @TransactionPrice * @InstrumentContractSize * @InstrumentMultiplier)
	  END
	ELSE
	  BEGIN
	    IF (@TransactionUnits * @TransactionPrice * @InstrumentContractSize * @InstrumentMultiplier) = 0
	      BEGIN
	        SELECT @TransactionCostPercent = 0
	        SELECT @TransactionCostIsPercent = 0
	      END
	    ELSE
	      BEGIN
	        SELECT @TransactionCostPercent = @TransactionCosts / (@TransactionUnits * @TransactionPrice * @InstrumentContractSize * @InstrumentMultiplier)
	      END
	  END
	
	-- Allow for Futures Style Transactions. NPP Jan 2009.
	-- Settlement is conducted on a Futures / CFD basis where there is no initial settlement (Not withstanding Margin accounted for separately).
	
	IF ISNULL(@TransactionUseFixedSettlement, 0) = 0
	BEGIN
		SELECT @TransactionSettlement = Round(((@TransactionUnits * @TransactionPrice * @InstrumentContractSize * @InstrumentMultiplier) - ((@TransactionCosts + @TransactionBrokerFees) * @CashMultiplier)) * @TransactionFXRate, 5)
	END
	ELSE
	BEGIN
		-- @TransactionUseFixedSettlement != 0
		-- Transaction Settlement will always (?) be positive unless the Price is negative

		SET @TransactionSettlement = ABS(@TransactionFixedSettlementValue)

		IF (@TransactionPrice < 0)
		BEGIN
			SET @TransactionSettlement = -ABS(@TransactionFixedSettlementValue)
		END
	END

	-- Validate Counterparty vs TransactionType
    
	IF @TransactionCounterparty = @COUNTERPARTY_MARKET 
	  BEGIN
		IF (@TransactionType IN (@TRANSACTIONTYPE_Subscribe, @TRANSACTIONTYPE_Redeem)) 
		  BEGIN
			SET @rvStatusString = 'Counterparty must not be `Market` for Transaction type of `Subscribe` or `Redeem`.'
			GOTO ExitWithError
		  END
	  END
	ELSE
	  BEGIN
		-- Counterparty is NOT 'MARKET'
		
		IF (@TransactionType IN (@TRANSACTIONTYPE_Subscribe, @TRANSACTIONTYPE_Redeem)) 
		  BEGIN
			-- Subscribe and Redeem, Unit count always positive (Odd effects otherwise)

			IF (@TransactionUnits < 0)
				BEGIN
					SET @TransactionUnits = ABS(@TransactionUnits)
				END

			-- Counterparty is not MARKET and TransactionType IS Subscribe or Redeem.
			-- Check that the transaction is in the Fund's Units. Subscriptions and Redemptions
			-- May only be done in the given Fund's Units.
			
/*		DECLARE @FundCount Int
			
			SELECT @FundCount = ISNULL(Count(RN), 0)
			FROM fn_tblFund_SelectKD(@KnowledgeDate)
			WHERE (FundID = @TransactionFund) AND (FundUnit = @TransactionInstrument)
			
			IF @FundCount = 0
			  BEGIN
				SELECT @rvStatusString = 'You can only Subscribe or Redeem this fund`s units.'
				GOTO ExitWithError
			  END
*/
			DECLARE @ISUnit int = 0
			SELECT @ISUnit = ISNULL(Count(RN), 0)
			FROM [fn_tblInstrument_SelectSingle](@TransactionInstrument, @KnowledgeDate)
			WHERE (InstrumentType = @INSTRUMENTTYPE_Unit) AND (InstrumentID = @TransactionInstrument)

			IF @ISUnit = 0
			  BEGIN
				SELECT @rvStatusString = 'You may only Subscribe or Redeem to Instruments of type UNIT.'
				GOTO ExitWithError
			  END

		  END
/*
		ELSE -- NOT (@TRANSACTIONTYPE_Subscribe, @TRANSACTIONTYPE_Redeem)
		  BEGIN
			-- Counterparty is Not Market and Transaction Type is NOT Subscribe ot Redeem.
			
			SELECT @rvStatusString = 'Counterparty must be `Market` unless Transaction type is `Subscribe` or `Redeem`.'
			GOTO ExitWithError
		  END
*/
	  END -- Counterparty is NOT 'MARKET'
  
	-- Validate Transaction Type for Cash Instruments
	-- Get @InstrumentType AND @InstrumentLastValidTradeDate to save a second query later.
		
	IF @InstrumentType = @INSTRUMENTTYPE_Cash 
	  BEGIN
		IF NOT (@TransactionType IN (@TRANSACTIONTYPE_BuyFX, @TRANSACTIONTYPE_SellFX, @TRANSACTIONTYPE_BuyFXForward, @TRANSACTIONTYPE_SellFXForward)) 
		  BEGIN
			SELECT @rvStatusString = 'Transaction type must be `BUY FX` or `SELL FX` if the Instrument is of type `Cash`.'
			GOTO ExitWithError
		  END
	  END
	 
	IF (@InstrumentType = @INSTRUMENTTYPE_Fee) OR (@TransactionType = @TRANSACTIONTYPE_Fee)
	  BEGIN
		IF (@InstrumentType <> @INSTRUMENTTYPE_Fee) OR ((@TransactionType <> @TRANSACTIONTYPE_Fee) AND (@TransactionType <> @TRANSACTIONTYPE_Provision_Increase) AND (@TransactionType <> @TRANSACTIONTYPE_Provision_Reduce))
		  BEGIN
			SELECT @rvStatusString = 'Transaction Type `FEE` must be used with FEE Instruments.'
			GOTO ExitWithError
		  END
	  END
	  		  
	IF (@InstrumentType = @INSTRUMENTTYPE_Interest) OR (@TransactionType IN (@TRANSACTIONTYPE_InterestPayment, @TRANSACTIONTYPE_InterestIncome))
	  BEGIN

		IF (@InstrumentType <> @INSTRUMENTTYPE_Interest) OR (NOT (@TransactionType IN (@TRANSACTIONTYPE_InterestPayment, @TRANSACTIONTYPE_InterestIncome)))
		  BEGIN
			SELECT @rvStatusString = 'Transaction Types `INTEREST INCOME` or `INTEREST PAYMENT` must be used with INTEREST Instruments.'

			GOTO ExitWithError
		  END
	  END
	  		  
	IF (@InstrumentType = @INSTRUMENTTYPE_Expense) OR (@TransactionType = @TRANSACTIONTYPE_Expense_Increase) OR (@TransactionType = @TRANSACTIONTYPE_Expense_Reduce )
	  BEGIN
		IF (@InstrumentType <> @INSTRUMENTTYPE_Expense) OR ((@TransactionType <> @TRANSACTIONTYPE_Expense_Increase) AND (@TransactionType <> @TRANSACTIONTYPE_Expense_Reduce))
		  BEGIN
			SELECT @rvStatusString = 'Transaction Type `EXPENSE` must be used with EXPENSE Instruments.'
			GOTO ExitWithError
		  END
	  END
	  
	-- Resolve the FX Status
	-- Resolve also the Modified Settlement for the First Transaction Leg. 
	-- Resolve also the Second-Leg TransactionType and InstrumentID

	SET @FXFlag = 0

	IF (@TransactionType = @TRANSACTIONTYPE_BuyFX) OR (@TransactionType = @TRANSACTIONTYPE_SellFX) OR (@TransactionType = @TRANSACTIONTYPE_BuyFXForward) OR (@TransactionType = @TRANSACTIONTYPE_SellFXForward)
	  BEGIN
		SET @FXFlag = 1
		SET @ModifiedSettlement = (@TransactionUnits)
		SET @SecondLeg_TransactionType = @TransactionType
		SET @SecondLeg_InstrumentID = @INSTRUMENT_USD  -- Assume FX vs USD, unless Settlement currency is given.

		IF (@TransactionSettlementCurrencyID > 0)
		  BEGIN
			-- Resolve Instrument for second leg of FX, if given..
   				-- Get Cash Instrument ID.
			
				  SELECT @SecondLeg_InstrumentID = ISNULL(CurrencyCashInstrumentID, 0), @CurrencyCode = ISNULL(CurrencyCode, 'USD')
				  FROM fn_tblCurrency_SelectKD(@KnowledgeDate)
				  WHERE CurrencyID = @TransactionSettlementCurrencyID
			
				  IF (@SecondLeg_InstrumentID = 0)
				  BEGIN
				
					  SELECT @SecondLeg_InstrumentID = ISNULL(InstrumentID, 0)
					  FROM fn_tblInstrument_SelectKD(@KnowledgeDate)
					  WHERE (InstrumentCode = @CurrencyCode) AND (InstrumentType = @INSTRUMENTTYPE_Cash)
				
				  END
		  END
		ELSE
		  -- Get @TransactionSettlementCurrencyID, if not already got.
		  BEGIN
			SELECT @TransactionSettlementCurrencyID = ISNULL(InstrumentCurrencyID, 0)
			FROM fn_tblInstrument_SelectSingle(@SecondLeg_InstrumentID, @KnowledgeDate)
			WHERE (InstrumentID = @SecondLeg_InstrumentID)
		  END

	  END -- (@TransactionType = @TRANSACTIONTYPE_BuyFX) OR (@TransactionType = @TRANSACTIONTYPE_SellFX)
	ELSE
	  BEGIN
	  
	    -- NOT FX
	    
		SELECT @ModifiedSettlement = (@TransactionSettlement)

		SELECT @SecondLeg_TransactionType = @TRANSACTIONTYPE_Consideration
						
		-- Resolve 'Second Leg' Instrument ID.
		
		IF (@InstrumentTypeIsFuturesStyle = 0)
		BEGIN
			-- Normal Cash settlement.
			-- Get Cash Instrument ID.
			
		  -- Settle in '@TransactionSettlementCurrencyID' if given.
		  IF (ISNULL(@TransactionSettlementCurrencyID, 0) > 0)
		  BEGIN
				  SELECT @SecondLeg_InstrumentID = ISNULL(CurrencyCashInstrumentID, 0), @CurrencyCode = ISNULL(CurrencyCode, 'USD')
				  FROM fn_tblCurrency_SelectKD(@KnowledgeDate)
				  WHERE CurrencyID = ISNULL(@TransactionSettlementCurrencyID, 0)
		  END
      ELSE
      BEGIN
        SET @TransactionSettlementCurrencyID = @InstrumentCurrencyID
      END

      -- Otherwise settle in Instrument Currency, fetched earlier to @InstrumentCurrencyID
      IF (ISNULL(@SecondLeg_InstrumentID, 0) = 0)
			BEGIN
			  SELECT @SecondLeg_InstrumentID = ISNULL(CurrencyCashInstrumentID, 0), @CurrencyCode = ISNULL(CurrencyCode, 'USD')
			  FROM fn_tblCurrency_SelectKD(@KnowledgeDate)
			  WHERE CurrencyID = @InstrumentCurrencyID
			END

      -- Last resort, try to resolve Currency instrument from @CurrencyCode, fetched earlier.
			IF (@SecondLeg_InstrumentID = 0)
			BEGIN
				SELECT @SecondLeg_InstrumentID = ISNULL(InstrumentID, 0), @TransactionSettlementCurrencyID = InstrumentCurrencyID 
				FROM fn_tblInstrument_SelectKD(@KnowledgeDate)
				WHERE (InstrumentCode = @CurrencyCode) AND (InstrumentType = @INSTRUMENTTYPE_Cash)
			END

			IF (@SecondLeg_InstrumentID = 0)
			BEGIN
				
				-- Failed to resolve Cash Instrument.
				
				SELECT @rvStatusString = 'Failed to resolve Cash Instrument ID for transaction consideration.'
				GOTO ExitWithError
				
			END
		END
		ELSE
		BEGIN
			-- Futures Style (Nominal) settlement.
			-- Get Nominal Cash Instrument ID.

			SELECT @SecondLeg_InstrumentID = ISNULL(CurrencyFuturesNominalInstrumentID, 0), @CurrencyCode = ISNULL(CurrencyCode, 'USD')
			FROM fn_tblCurrency_SelectKD(@KnowledgeDate)
			WHERE CurrencyID = @InstrumentCurrencyID
			
      SET @TransactionSettlementCurrencyID = @InstrumentCurrencyID

			-- OK, No reference in the tblCurrency table, Search the Instruments table.
				
			IF (@SecondLeg_InstrumentID = 0)
			BEGIN
				
				SELECT @SecondLeg_InstrumentID = ISNULL(InstrumentID, 0), @TransactionSettlementCurrencyID = InstrumentCurrencyID
				FROM fn_tblInstrument_SelectKD(@KnowledgeDate)
				WHERE (InstrumentCode LIKE (@CurrencyCode + '%')) AND (InstrumentType = @INSTRUMENTTYPE_Notional)
				
			END

			-- OK, No luck so far, Settle in cash.
			
			IF (@SecondLeg_InstrumentID = 0)
			BEGIN
				
			SELECT @SecondLeg_InstrumentID = ISNULL(CurrencyCashInstrumentID, 0), @CurrencyCode = ISNULL(CurrencyCode, 'USD')
			FROM fn_tblCurrency_SelectKD(@KnowledgeDate)
			WHERE CurrencyID = @InstrumentCurrencyID
				
			END

			-- No Cash Instrument in the tblCurrency, search the Instruments table again for a straight Cash instrument.
			
			IF (@SecondLeg_InstrumentID = 0)
			BEGIN
				
				SELECT @SecondLeg_InstrumentID = ISNULL(InstrumentID, 0), @TransactionSettlementCurrencyID = InstrumentCurrencyID
				FROM fn_tblInstrument_SelectKD(@KnowledgeDate)
				WHERE (InstrumentCode = @CurrencyCode) AND (InstrumentType = @INSTRUMENTTYPE_Cash)
				
			END

			-- OK, Give up.
			
			IF (@SecondLeg_InstrumentID = 0)
			BEGIN
				
				-- Failed to resolve Cash Instrument.
				
				SELECT @rvStatusString = 'Failed to resolve Nominal Cash Instrument ID for Futures Style transaction consideration.'
				GOTO ExitWithError
				
			END

		END
		
	END
  
  	  		  
	-- Validate Latest Trade Date
	-- Always Allow Sells or Redemptions.

	IF (ISNULL(@InstrumentLastValidTradeDate, '1900-01-01') > '1904-01-01') AND (ISNULL(@InstrumentLastValidTradeDate, '1900-01-01') < ISNULL(@TransactionValueDate, '1900-01-01')) 
	  BEGIN
		IF NOT (@TransactionType IN (@TRANSACTIONTYPE_Sell, @TRANSACTIONTYPE_Redeem))
		  BEGIN
			SELECT @rvStatusString = 'The latest allowed Trade Date for this Instrument is ' + CONVERT(varchar(20), @InstrumentLastValidTradeDate, 106)
			GOTO ExitWithError
		  END
	  END


	-- **************************************************
	-- Start to Insert Transactions :-
	-- **************************************************

	-- Get tblTransaction Lock.
	DECLARE @sp_LockReturn int

	IF ISNULL(@ValidationOnly, 0) = 0
	  BEGIN
		EXEC @sp_LockReturn = sp_getapplock @Resource = 'tblTransaction', @LockMode  = 'exclusive', @LockOwner = 'session'
		IF @sp_LockReturn < 0
		  BEGIN
			SELECT @rvStatusString = 'Failed to get sp_getapplock on tblTransaction.'
			GOTO ExitWithError
		  END
	  END

	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE

	-- Resolve Transaction Ticket
	
	IF ISNULL(@TransactionTicket, '') = ''
	  BEGIN
	  
		-- FX Trade ?  (Ticket 'FX')
		IF @FXFlag <> 0
		  BEGIN
			SELECT @New_TransactionTicket = dbo.fn_NewTransactionTicket('FX', 0)
		  END
		
		
		IF (@TransactionType IN (@TRANSACTIONTYPE_Subscribe, @TRANSACTIONTYPE_Redeem)) 
		  BEGIN
			-- Is this Counterparty a Fund ? (Ticket 'IN' - Internal) 
			-- Else TicketType of 'SR' - Subscribe / Redeem
			
			IF ISNULL(@New_TransactionTicket, '') = ''
			  BEGIN
				DECLARE @CounterpartyFundID Int
			
				SELECT @CounterpartyFundID = ISNULL(MAX(CounterpartyFundID), 0)
				FROM fn_tblCounterparty_SelectKD(@KnowledgeDate)
				WHERE CounterpartyID = @TransactionCounterparty
			
				IF ISNULL(@CounterpartyFundID, 0) > 0
				  BEGIN
					SELECT @New_TransactionTicket = dbo.fn_NewTransactionTicket('IN', 0)
				  END
				ELSE
				  BEGIN
					SELECT @New_TransactionTicket = dbo.fn_NewTransactionTicket('SR', 0)
				  END
			  END
		  END -- TransactionType IN (Subscribe , Redeem)	
		
		IF (@TransactionType IN (@TRANSACTIONTYPE_Buy, @TRANSACTIONTYPE_Sell)) 
		  BEGIN
			IF ISNULL(@New_TransactionTicket, '') = ''
			  BEGIN
				-- If this Instrument is a Unit, then TicketType = 'IN' - i.e. We are buying one of our own Units.
				-- else TicketType = 'BS' - we are buying / Selling something else.
			  
				DECLARE @IsAnFandCFund int
				
				SELECT @IsAnFandCFund = ISNULL(Count(RN), 0)
				FROM fn_tblInstrument_SelectSingle(@TransactionInstrument, @KnowledgeDate)
				WHERE (InstrumentType = @INSTRUMENTTYPE_Unit) AND (InstrumentID = @TransactionInstrument)
				
				IF @IsAnFandCFund > 0
				  BEGIN
					SELECT @New_TransactionTicket = dbo.fn_NewTransactionTicket('IN', 0)
				  END
				ELSE
				  BEGIN
					SELECT @New_TransactionTicket = dbo.fn_NewTransactionTicket('BS', 0)
				  END
			  END
		  END
		
		IF ISNULL(@New_TransactionTicket, '') = ''
		  BEGIN
			IF (@TransactionType = @TRANSACTIONTYPE_Fee) 
			  BEGIN
				SELECT @New_TransactionTicket = dbo.fn_NewTransactionTicket('EX', 0) -- 'Ex'pense
			  END
			ELSE
			IF NOT (@TransactionType IN (@TRANSACTIONTYPE_InterestPayment, @TRANSACTIONTYPE_InterestIncome))
			  BEGIN
				SELECT @New_TransactionTicket = dbo.fn_NewTransactionTicket('RE', 0) -- 'Revenue & Expenditure'
			  END
		  END
		
		IF ISNULL(@New_TransactionTicket, '') = ''
		  BEGIN
			SELECT @New_TransactionTicket = dbo.fn_NewTransactionTicket('OT', 0)  -- 'Ot'her
		  END
		
	  END
	ELSE
	  BEGIN
		SELECT @New_TransactionTicket = @TransactionTicket
	  END


	-- Validation Only

	IF ISNULL(@ValidationOnly, 0) <> 0
	  BEGIN
	    RETURN (1)
	  END
	  
	-- Start Transaction.	
		
	BEGIN TRANSACTION
	
	-- Parent ID
	
	IF ISNULL(@TransactionParentID, 0) > 0 
	  BEGIN
		SELECT @New_TransactionParentID = ISNULL(@TransactionParentID, 0)
		
		-- Clear 'Current' Status of Transactions being superceeded.
		--UPDATE tblTransaction
		--SET TransactionCurrent = 0
		--WHERE TransactionParentID = ISNULL(@TransactionParentID, 0)
		
	  END
	ELSE
	  BEGIN
	    SELECT @New_TransactionParentID = (IDENT_CURRENT('tblTransaction') +1)
	  END
	
	-- Insert First Leg
	
	INSERT INTO tblTransaction(
		TransactionTicket, 
		TransactionFund, 
		TransactionSubFund,
		TransactionInstrument,  
		TransactionType, 
		TransactionType_Contra, 
		TransactionValueorAmount, 
		TransactionUnits, 
		TransactionSignedUnits, 
		TransactionPrice, 
		TransactionCleanPrice ,
		TransactionAccruedInterest ,
		TransactionBrokerFees ,
		TransactionCosts, 
		TransactionCostPercent, 
		TransactionSettlement, 
		TransactionSignedSettlement,
		TransactionFXRate, 
		TransactionSettlementCurrencyID,
		TransactionEffectivePrice , 
		TransactionEffectiveWatermark , 
		TransactionSpecificInitialEqualisationValue ,
		TransactionCounterparty, 
		TransactionBroker,
		TransactionBrokerStrategy,
		TransactionInvestment, 
		TransactionExecution,  
		TransactionDataEntry, 
		TransactionDecisionDate, 
		TransactionValueDate, 
		TransactionFIFOValueDate, 
		TransactionEffectiveValueDate , 
		TransactionSettlementDate,  
		TransactionConfirmationDate, 
		TransactionCostIsPercent, 
		TransactionExemptFromUpdate, 
		TransactionRedeemWholeHoldingFlag, 
		TransactionIsTransfer , 
		TransactionSpecificInitialEqualisationFlag , 
		TransactionFuturesStylePricing , 
		TransactionClosingTrade,
		TransactionIsFX,
		TransactionFinalAmount, 
		TransactionFinalPrice, 
		TransactionFinalCosts,  
		TransactionFinalSettlement, 
		TransactionInstructionFlag, 
		TransactionBankConfirmation, 
		TransactionInitialDataEntry, 
		TransactionAmountConfirmed,  
		TransactionSettlementConfirmed, 
		TransactionFinalDataEntry, 
		TransactionComment, 
		TransactionParentID, 
		TransactionRedemptionID, 
		TransactionVersusInstrument, 
		TransactionSpecificToFundUnitID, 
		TransactionGroup, 
		TransactionTradeStatusID, 
		TransactionLeg, 
		TransactionAdminStatus, 
		TransactionCompoundRN, 
		TransactionCTFLAGS
		)
	VALUES (
		@New_TransactionTicket, 
		@TransactionFund, 
		@TransactionSubFund ,
		@TransactionInstrument,  
		@TransactionType, 
		@TransactionType, 
		@TransactionValueorAmount, 
		(@TransactionUnits), 
		@TransactionUnits * ((-1.0) * @CashMultiplier), 
		@TransactionPrice, 
		@TransactionCleanPrice ,
		@TransactionAccruedInterest ,
		@TransactionBrokerFees ,
		@TransactionCosts,  
		@TransactionCostPercent, 
		(@ModifiedSettlement), 
		@ModifiedSettlement * ((-1.0) * @CashMultiplier), 
    ISNULL(@TransactionFXRate, 1), 
    ISNULL(@TransactionSettlementCurrencyID, 0),
		ISNULL(@TransactionEffectivePrice, @TransactionPrice) , 
		ISNULL(@TransactionEffectiveWatermark, 0) , 
		ISNULL(@TransactionSpecificInitialEqualisationValue, 0) ,
		@TransactionCounterparty, 
		ISNULL(@TransactionBroker, 0),
		ISNULL(@TransactionBrokerStrategy, ''),
		@TransactionInvestment, 
		@TransactionExecution,  
		@TransactionDataEntry, 
		ISNULL(@TransactionDecisionDate, @TransactionValueDate), 
		@TransactionValueDate, 
		ISNULL(@TransactionFIFOValueDate, @TransactionValueDate), 
		ISNULL(@TransactionEffectiveValueDate, @TransactionValueDate), 
		ISNULL(@TransactionSettlementDate, @TransactionValueDate), 
		ISNULL(@TransactionConfirmationDate, @TransactionValueDate), 
		@TransactionCostIsPercent, 
		@TransactionExemptFromUpdate, 
		ISNULL(@TransactionRedeemWholeHoldingFlag, 0), 
		ISNULL(@TransactionIsTransfer, 0), 
		ISNULL(@TransactionSpecificInitialEqualisationFlag, 0), 
		ISNULL(@InstrumentTypeIsFuturesStyle , 0), 
		ISNULL(@TransactionClosingTrade, 0),
		@FXFlag,
		@TransactionFinalAmount, 
		@TransactionFinalPrice, 
		@TransactionFinalCosts,  
		@TransactionFinalSettlement, 
		@TransactionInstructionFlag, 
		@TransactionBankConfirmation, 
		@TransactionInitialDataEntry, 
		@TransactionAmountConfirmed,  
		@TransactionSettlementConfirmed, 
		@TransactionFinalDataEntry, 
		ISNULL(@TransactionComment, ''), 
		@New_TransactionParentID, 
		ISNULL(@TransactionRedemptionID, 0), 
		ISNULL(@TransactionVersusInstrument, 0), 
		ISNULL(@TransactionSpecificToFundUnitID, 0), 
		ISNULL(@TransactionGroup, ''), 
    ISNULL(@TransactionTradeStatusID, 0), 
		1, 
		@TransactionAdminStatus, 
		@TransactionCompoundRN, 
		@TransactionCTFLAGS
		)
	
	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		EXEC sp_releaseapplock @Resource = 'tblTransaction', @LockOwner = 'session'
		SELECT @rvStatusString = 'Error inserting first Transaction leg.'
		GOTO ExitWithError
	END
	
	-- Verify that in the event of a New Transaction, that the ParentID was set to the
	-- correct value, i.e. The TransactionID. This was predicted using the IDENT_CURRENT()
	-- function, but this is not ABSOLUTELY reliable, thus a check is made.
	-- If Transactions are ONLY entered using this procedure and the User Table Locking is 
	-- applioed, then this prediction should be correct, but just in case....
	
	IF ISNULL(@TransactionParentID, 0) <= 0 
	BEGIN
		IF @New_TransactionParentID <> SCOPE_IDENTITY()
		BEGIN
			SELECT @New_TransactionParentID = SCOPE_IDENTITY()

			UPDATE tblTransaction
			SET TransactionParentID = @New_TransactionParentID
			WHERE TransactionID = @New_TransactionParentID
		END
	END

	-- Second Leg
	
	INSERT INTO tblTransaction(
		TransactionTicket, 
		TransactionFund, 
		TransactionSubFund, 
		TransactionInstrument,  
		TransactionType, 
		TransactionType_Contra, 
		TransactionValueorAmount, 
		TransactionUnits, 
		TransactionSignedUnits, 
		TransactionPrice, 
		TransactionCleanPrice ,
		TransactionAccruedInterest ,
		TransactionBrokerFees ,
		TransactionCosts,  
		TransactionCostPercent, 
		TransactionSettlement, 
		TransactionSignedSettlement, 
		TransactionFXRate, 
		TransactionSettlementCurrencyID,
		TransactionEffectivePrice , 
		TransactionEffectiveWatermark , 
		TransactionSpecificInitialEqualisationValue ,
		TransactionCounterparty, 
		TransactionBroker,
		TransactionBrokerStrategy,
		TransactionInvestment, 
		TransactionExecution,  
		TransactionDataEntry, 
		TransactionDecisionDate, 
		TransactionValueDate, 
		TransactionFIFOValueDate, 
		TransactionEffectiveValueDate , 
		TransactionSettlementDate,  
		TransactionConfirmationDate, 
		TransactionCostIsPercent, 
		TransactionExemptFromUpdate, 
		TransactionRedeemWholeHoldingFlag, 
		TransactionIsTransfer , 
		TransactionSpecificInitialEqualisationFlag , 
		TransactionFuturesStylePricing , 
		TransactionClosingTrade,
		TransactionIsFX,
		TransactionFinalAmount, 
		TransactionFinalPrice, 
		TransactionFinalCosts,  
		TransactionFinalSettlement, 
		TransactionInstructionFlag, 
		TransactionBankConfirmation, 
		TransactionInitialDataEntry, 
		TransactionAmountConfirmed,  
		TransactionSettlementConfirmed, 
		TransactionFinalDataEntry, 
		TransactionComment, 
		TransactionParentID, 
		TransactionRedemptionID, 
		TransactionVersusInstrument, 
		TransactionSpecificToFundUnitID,
		TransactionGroup, 
		TransactionTradeStatusID, 
		TransactionLeg, 
		TransactionAdminStatus, 
		TransactionCompoundRN, 
		TransactionCTFLAGS
		)
	VALUES (
		@New_TransactionTicket, 
		@TransactionFund, 
		@TransactionSubFund, 
		@SecondLeg_InstrumentID,  
		@SecondLeg_TransactionType, 
		@TransactionType, 
		'Amount', 
		IIF(ISNULL(@TransactionUseFixedSettlement, 0) = 0, ABS(@TransactionSettlement), ABS(@TransactionFixedSettlementValue)), 
		IIF(ISNULL(@TransactionUseFixedSettlement, 0) = 0, @TransactionSettlement * @CashMultiplier, @TransactionFixedSettlementValue), 
		1, 
		1, 
		0, 
		0, 
		0, 
		0,  
		IIF(ISNULL(@TransactionUseFixedSettlement, 0) = 0, ABS(@TransactionSettlement), ABS(@TransactionFixedSettlementValue)), 
		IIF(ISNULL(@TransactionUseFixedSettlement, 0) = 0, @TransactionSettlement * @CashMultiplier, @TransactionFixedSettlementValue), 
		ISNULL(@TransactionFXRate, 1), 
		ISNULL(@TransactionSettlementCurrencyID, 0),
		ISNULL(@TransactionEffectivePrice, @TransactionPrice) , 
		ISNULL(@TransactionEffectiveWatermark, 0) , 
		ISNULL(@TransactionSpecificInitialEqualisationValue, 0) ,
		@TransactionCounterparty, 
		ISNULL(@TransactionBroker, 0),
		ISNULL(@TransactionBrokerStrategy, ''),
		@TransactionInvestment, 
		@TransactionExecution,  
		@TransactionDataEntry, 
		ISNULL(@TransactionDecisionDate, @TransactionValueDate), 
		@TransactionValueDate, 
		ISNULL(@TransactionFIFOValueDate, @TransactionValueDate), 
		ISNULL(@TransactionEffectiveValueDate, @TransactionValueDate), 
		ISNULL(@TransactionSettlementDate, @TransactionValueDate), 
		ISNULL(@TransactionConfirmationDate, @TransactionValueDate), 
		0, 
		@TransactionExemptFromUpdate, 
		ISNULL(@TransactionRedeemWholeHoldingFlag, 0), 
		ISNULL(@TransactionIsTransfer, 0), 
		ISNULL(@TransactionSpecificInitialEqualisationFlag, 0), 
		0, --	TransactionFuturesStylePricing , 
		ISNULL(@TransactionClosingTrade, 0),
		@FXFlag,
		@TransactionFinalAmount, 
		@TransactionFinalPrice, 
		@TransactionFinalCosts,  
		@TransactionFinalSettlement, 
		@TransactionInstructionFlag, 
		@TransactionBankConfirmation, 
		@TransactionInitialDataEntry, 
		@TransactionAmountConfirmed,  
		@TransactionSettlementConfirmed, 
		@TransactionFinalDataEntry, 
		ISNULL(@TransactionTypeName, ''), 
		@New_TransactionParentID, 
		ISNULL(@TransactionRedemptionID, 0), 
		(CASE WHEN @InstrumentTypeIsFuturesStyle = 0 THEN 0 ELSE @TransactionInstrument END), --		0, -- Versus Instrument
		ISNULL(@TransactionSpecificToFundUnitID, 0), 
		ISNULL(@TransactionGroup, ''), 
    ISNULL(@TransactionTradeStatusID, 0), 
		2, 
		@TransactionAdminStatus, 
		@TransactionCompoundRN, 
		@TransactionCTFLAGS
		)
	
	SET @ERROR_INT = @@ERROR
	IF @ERROR_INT <> 0
	BEGIN
		ROLLBACK TRANSACTION
		EXEC sp_releaseapplock @Resource = 'tblTransaction', @LockOwner = 'session'
		SELECT @rvStatusString = 'Error inserting Second Transaction leg. ' + CAST(@ERROR_INT AS nvarchar(10))
		GOTO ExitWithError
	END
	
	-- Return value and Commit / Rollback transaction accordingly.
	
	IF @ERROR_INT = 0
	  BEGIN
	    COMMIT TRANSACTION
		EXEC sp_releaseapplock @Resource = 'tblTransaction', @LockOwner = 'session'
		
		SELECT	
			 [RN]
      ,[AuditID]
      ,[TransactionID]
      ,[TransactionTicket]
      ,[TransactionFund]
      ,[TransactionSubFund]
      ,[TransactionInstrument]
      ,[TransactionType]
      ,[TransactionType_Contra]
      ,[TransactionValueorAmount]
      ,[TransactionUnits]
      ,[TransactionSignedUnits]
      ,[TransactionPrice]
			,[TransactionCleanPrice]
			,[TransactionAccruedInterest]
			,[TransactionBrokerFees]
      ,[TransactionCosts]
      ,[TransactionCostPercent]
      ,[TransactionSettlement]
      ,[TransactionSignedSettlement]
      ,[TransactionFXRate]
      ,[TransactionSettlementCurrencyID]
      ,[TransactionEffectivePrice]
      ,[TransactionEffectiveWatermark]
      ,[TransactionSpecificInitialEqualisationValue]
      ,[TransactionCounterparty]
			,[TransactionBroker]
			,[TransactionBrokerStrategy]
      ,[TransactionInvestment]
      ,[TransactionExecution]
      ,[TransactionDataEntry]
      ,[TransactionDecisionDate]
      ,[TransactionValueDate]
      ,[TransactionFIFOValueDate]
      ,[TransactionSettlementDate]
      ,[TransactionConfirmationDate]
      ,[TransactionEntryDate]
      ,[TransactionEffectiveValueDate]
      ,[TransactionCostIsPercent]
      ,[TransactionExemptFromUpdate]
      ,[TransactionRedeemWholeHoldingFlag]
      ,[TransactionIsTransfer]
      ,[TransactionSpecificInitialEqualisationFlag]
      ,[TransactionFuturesStylePricing]
			,[TransactionClosingTrade]
      ,[TransactionFinalAmount]
      ,[TransactionFinalPrice]
      ,[TransactionFinalCosts]
      ,[TransactionFinalSettlement]
      ,[TransactionInstructionFlag]
      ,[TransactionBankConfirmation]
      ,[TransactionInitialDataEntry]
      ,[TransactionAmountConfirmed]
      ,[TransactionSettlementConfirmed]
      ,[TransactionFinalDataEntry]
      ,[TransactionComment]
      ,[TransactionParentID]
      ,[TransactionRedemptionID]
      ,[TransactionVersusInstrument]
			,TransactionSpecificToFundUnitID
      ,[TransactionUser]
      ,[TransactionGroup]
      ,[TransactionLeg]
      ,[TransactionTradeStatusID]
      ,[TransactionAdminStatus]
      ,[TransactionCTFLAGS]
      ,[TransactionCompoundRN]
      ,[TransactionDateEntered]
      ,[TransactionDateDeleted]
		FROM fn_tblTransaction_SelectKD(@KnowledgeDate)
		WHERE (TransactionParentID = @New_TransactionParentID) AND (TransactionLeg = 1)
		
		SELECT @rvStatusString = 'OK'
	    RETURN @New_TransactionParentID
	  END
	ELSE
	  BEGIN
	    ROLLBACK TRANSACTION
		EXEC sp_releaseapplock @Resource = 'tblTransaction', @LockOwner = 'session'
		SELECT @rvStatusString = 'Error Saving Transaction.'
		GOTO ExitWithError
	  END
	
ExitWithError:

	-- Return 'Current' Row.
	
	IF ISNULL(@TransactionParentID, 0) > 0
	    BEGIN
		SELECT	
			 [RN]
      ,[AuditID]
      ,[TransactionID]
      ,[TransactionTicket]
      ,[TransactionFund]
      ,[TransactionSubFund]
      ,[TransactionInstrument]
      ,[TransactionType]
      ,[TransactionType_Contra]
      ,[TransactionValueorAmount]
      ,[TransactionUnits]
      ,[TransactionSignedUnits]
      ,[TransactionPrice]
			,[TransactionCleanPrice]
			,[TransactionAccruedInterest]
			,[TransactionBrokerFees]
      ,[TransactionCosts]
      ,[TransactionCostPercent]
      ,[TransactionSettlement]
      ,[TransactionSignedSettlement]
      ,[TransactionFXRate]
      ,[TransactionSettlementCurrencyID]
      ,[TransactionEffectivePrice]
      ,[TransactionEffectiveWatermark]
      ,[TransactionSpecificInitialEqualisationValue]
      ,[TransactionCounterparty]
			,[TransactionBroker]
			,[TransactionBrokerStrategy]
      ,[TransactionInvestment]
      ,[TransactionExecution]
      ,[TransactionDataEntry]
      ,[TransactionDecisionDate]
      ,[TransactionValueDate]
      ,[TransactionFIFOValueDate]
      ,[TransactionSettlementDate]
      ,[TransactionConfirmationDate]
      ,[TransactionEntryDate]
      ,[TransactionEffectiveValueDate]
      ,[TransactionCostIsPercent]
      ,[TransactionExemptFromUpdate]
      ,[TransactionRedeemWholeHoldingFlag]
      ,[TransactionIsTransfer]
      ,[TransactionSpecificInitialEqualisationFlag]
      ,[TransactionFuturesStylePricing]
			,[TransactionClosingTrade]
      ,[TransactionFinalAmount]
      ,[TransactionFinalPrice]
      ,[TransactionFinalCosts]
      ,[TransactionFinalSettlement]
      ,[TransactionInstructionFlag]
      ,[TransactionBankConfirmation]
      ,[TransactionInitialDataEntry]
      ,[TransactionAmountConfirmed]
      ,[TransactionSettlementConfirmed]
      ,[TransactionFinalDataEntry]
      ,[TransactionComment]
      ,[TransactionParentID]
      ,[TransactionRedemptionID]
      ,[TransactionVersusInstrument]
			,[TransactionSpecificToFundUnitID]
      ,[TransactionUser]
      ,[TransactionGroup]
      ,[TransactionLeg]
      ,[TransactionTradeStatusID]
      ,[TransactionAdminStatus]
      ,[TransactionCTFLAGS]
      ,[TransactionCompoundRN]
      ,[TransactionDateEntered]
      ,[TransactionDateDeleted]
		FROM fn_tblTransaction_SelectKD(@KnowledgeDate)
		WHERE (TransactionParentID = @TransactionParentID) AND (TransactionLeg = 1)
	    END

	RETURN 0
	
GO

USE [Renaissance]
GO
/****** Object:  StoredProcedure [dbo].[adp_tblTransaction_UpdateCommand]    Script Date: 22/01/2014 10:54:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[adp_tblTransaction_UpdateCommand]
	(
	@TransactionTicket nvarchar (50),
	@TransactionFund int,
	@TransactionSubFund int = Null, 
	@TransactionInstrument int,
	@TransactionType int,
	@TransactionValueorAmount nvarchar (10),
	@TransactionUnits float = 0,
	@TransactionPrice float = 0,
	@TransactionCleanPrice float = 0,
	@TransactionAccruedInterest float = 0,
	@TransactionBrokerFees float = 0,
	@TransactionCosts float = 0,
	@TransactionCostPercent  float = 0, 
	@TransactionFXRate float = 0,
	@TransactionSettlementCurrencyID int = 0,
  @TransactionUseFixedSettlement int = 0,           -- Ignore the usual rules and just use the given Settlement figure in @TransactionFixedSettlementValue
  @TransactionFixedSettlementValue float = 0,       -- See above...   All due to B'stard Sophis.
	@TransactionEffectivePrice float = NULL, 
	@TransactionEffectiveWatermark float = NULL, 
	@TransactionSpecificInitialEqualisationValue float = 0,
	@TransactionCounterparty int,
	@TransactionBroker int = 0, 
	@TransactionBrokerStrategy varchar (50) = '',
	@TransactionInvestment nvarchar (50),
	@TransactionExecution nvarchar (50),
	@TransactionDataEntry nvarchar (50),
	@TransactionDecisionDate datetime,
	@TransactionValueDate datetime,
	@TransactionFIFOValueDate datetime,
	@TransactionEffectiveValueDate datetime = NULL, 
	@TransactionSettlementDate datetime,
	@TransactionConfirmationDate datetime,
	@TransactionCostIsPercent bit = 0, 
	@TransactionExemptFromUpdate bit = 0, 
	@TransactionRedeemWholeHoldingFlag bit = 0,
	@TransactionIsTransfer bit = 0, 
	@TransactionSpecificInitialEqualisationFlag bit = 0, 
	@TransactionClosingTrade bit = 0, 
	@TransactionFinalAmount int = 0,
	@TransactionFinalPrice int = 0,
	@TransactionFinalCosts int = 0,
	@TransactionFinalSettlement int = 0,
	@TransactionInstructionFlag int = 0,
	@TransactionBankConfirmation int = 0,
	@TransactionInitialDataEntry int = 0,
	@TransactionAmountConfirmed int = 0,
	@TransactionSettlementConfirmed int = 0,
	@TransactionFinalDataEntry int = 0,
	@TransactionComment varchar (500),
	@TransactionParentID int = 0,
	@TransactionRedemptionID int = 0, 
	@TransactionVersusInstrument int = 0, 
	@TransactionSpecificToFundUnitID int = 0, 
	@TransactionGroup nvarchar (50),
  @TransactionTradeStatusID int = 0,        -- For Sophis Import transactions = HISTOMVTS.BACKOFFICE
	@TransactionAdminStatus int = 0,
	@TransactionCTFLAGS int = 0,
	@TransactionCompoundRN int = 0,
	@KnowledgeDate datetime, 
	@ValidationOnly int = 0,
	@rvStatusString nvarchar(200) OUTPUT	
	)
AS
	SET NOCOUNT OFF;

	DECLARE @RVal int 

	IF ((@Knowledgedate >= (getdate())) OR (ISNULL(@Knowledgedate, '1900-01-01') <= '1900-01-01')) AND 
		(ISNULL(@TransactionParentID, 0) > 0)
	  BEGIN
			EXECUTE @RVal = [dbo].[adp_tblTransaction_InsertCommand] 
				@TransactionTicket = @TransactionTicket, 
				@TransactionFund = @TransactionFund, 
				@TransactionSubFund = @TransactionSubFund, 
				@TransactionInstrument = @TransactionInstrument, 
				@TransactionType = @TransactionType, 
				@TransactionValueorAmount = @TransactionValueorAmount, 
				@TransactionUnits = @TransactionUnits, 
				@TransactionPrice = @TransactionPrice, 
				@TransactionCleanPrice = @TransactionCleanPrice,
				@TransactionAccruedInterest = @TransactionAccruedInterest, 
				@TransactionBrokerFees = @TransactionBrokerFees,
				@TransactionCosts = @TransactionCosts, 
				@TransactionCostPercent = @TransactionCostPercent, 
				@TransactionFXRate = @TransactionFXRate, 	
        @TransactionSettlementCurrencyID = @TransactionSettlementCurrencyID, 		
				@TransactionUseFixedSettlement = @TransactionUseFixedSettlement,
				@TransactionFixedSettlementValue = @TransactionFixedSettlementValue,
				@TransactionEffectivePrice = @TransactionEffectivePrice, 
				@TransactionEffectiveWatermark = @TransactionEffectiveWatermark, 
				@TransactionSpecificInitialEqualisationValue = @TransactionSpecificInitialEqualisationValue,			
				@TransactionCounterparty = @TransactionCounterparty, 
				@TransactionBroker = @TransactionBroker, 
				@TransactionBrokerStrategy = @TransactionBrokerStrategy,
				@TransactionInvestment = @TransactionInvestment, 
				@TransactionExecution = @TransactionExecution, 
				@TransactionDataEntry = @TransactionDataEntry, 
				@TransactionDecisionDate = @TransactionDecisionDate, 
				@TransactionValueDate = @TransactionValueDate,
				@TransactionFIFOValueDate = @TransactionFIFOValueDate,
				@TransactionEffectiveValueDate = @TransactionEffectiveValueDate, 
				@TransactionSettlementDate = @TransactionSettlementDate, 
				@TransactionConfirmationDate = @TransactionConfirmationDate, 
				@TransactionCostIsPercent = @TransactionCostIsPercent, 
				@TransactionExemptFromUpdate = @TransactionExemptFromUpdate, 
				@TransactionRedeemWholeHoldingFlag = @TransactionRedeemWholeHoldingFlag, 
				@TransactionIsTransfer = @TransactionIsTransfer, 
				@TransactionSpecificInitialEqualisationFlag = @TransactionSpecificInitialEqualisationFlag, 
				@TransactionClosingTrade = @TransactionClosingTrade,
				@TransactionFinalAmount = @TransactionFinalAmount,
				@TransactionFinalPrice = @TransactionFinalPrice,
				@TransactionFinalCosts = @TransactionFinalCosts,
				@TransactionFinalSettlement = @TransactionFinalSettlement,
				@TransactionInstructionFlag = @TransactionInstructionFlag,
				@TransactionBankConfirmation = @TransactionBankConfirmation, 
				@TransactionInitialDataEntry = @TransactionInitialDataEntry,
				@TransactionAmountConfirmed = @TransactionAmountConfirmed,
				@TransactionSettlementConfirmed = @TransactionSettlementConfirmed,
				@TransactionFinalDataEntry= @TransactionFinalDataEntry,
				@TransactionComment = @TransactionComment,
				@TransactionParentID = @TransactionParentID,
				@TransactionRedemptionID = @TransactionRedemptionID, 
				@TransactionVersusInstrument = @TransactionVersusInstrument, 
				@TransactionSpecificToFundUnitID = @TransactionSpecificToFundUnitID,
				@TransactionGroup = @TransactionGroup,
        @TransactionTradeStatusID = @TransactionTradeStatusID, 
				@TransactionAdminStatus = @TransactionAdminStatus,
				@TransactionCTFLAGS = @TransactionCTFLAGS,
				@TransactionCompoundRN = @TransactionCompoundRN,
				@KnowledgeDate = @KnowledgeDate,
				@ValidationOnly = @ValidationOnly,
				@rvStatusString = @rvStatusString OUTPUT	
				
		RETURN @RVal
	  END
	ELSE
	  BEGIN
			SELECT	
			 [RN]
      ,[AuditID]
      ,[TransactionID]
      ,[TransactionTicket]
      ,[TransactionFund]
      ,[TransactionSubFund]
      ,[TransactionInstrument]
      ,[TransactionType]
      ,[TransactionType_Contra]
      ,[TransactionValueorAmount]
      ,[TransactionUnits]
      ,[TransactionSignedUnits]
      ,[TransactionPrice]
			,[TransactionCleanPrice]
			,[TransactionAccruedInterest]
			,[TransactionBrokerFees]
      ,[TransactionCosts]
      ,[TransactionCostPercent]
      ,[TransactionSettlement]
      ,[TransactionSignedSettlement]
      ,[TransactionFXRate]
      ,[TransactionSettlementCurrencyID]
      ,[TransactionEffectivePrice]
      ,[TransactionEffectiveWatermark]
      ,[TransactionSpecificInitialEqualisationValue]
      ,[TransactionCounterparty]
			,TransactionBroker
			,TransactionBrokerStrategy
      ,[TransactionInvestment]
      ,[TransactionExecution]
      ,[TransactionDataEntry]
      ,[TransactionDecisionDate]
      ,[TransactionValueDate]
      ,[TransactionFIFOValueDate]
      ,[TransactionSettlementDate]
      ,[TransactionConfirmationDate]
      ,[TransactionEntryDate]
      ,[TransactionEffectiveValueDate]
      ,[TransactionCostIsPercent]
      ,[TransactionExemptFromUpdate]
      ,[TransactionRedeemWholeHoldingFlag]
      ,[TransactionIsTransfer]
      ,[TransactionSpecificInitialEqualisationFlag]
      ,[TransactionFuturesStylePricing]
			,[TransactionClosingTrade]
      ,[TransactionFinalAmount]
      ,[TransactionFinalPrice]
      ,[TransactionFinalCosts]
      ,[TransactionFinalSettlement]
      ,[TransactionInstructionFlag]
      ,[TransactionBankConfirmation]
      ,[TransactionInitialDataEntry]
      ,[TransactionAmountConfirmed]
      ,[TransactionSettlementConfirmed]
      ,[TransactionFinalDataEntry]
      ,[TransactionComment]
      ,[TransactionParentID]
      ,[TransactionRedemptionID]
      ,[TransactionVersusInstrument]
			,[TransactionSpecificToFundUnitID]
      ,[TransactionUser]
      ,[TransactionGroup]
      ,[TransactionLeg]
      ,[TransactionTradeStatusID]
      ,[TransactionAdminStatus]
      ,[TransactionCTFLAGS]
      ,[TransactionCompoundRN]
      ,[TransactionDateEntered]
      ,[TransactionDateDeleted]

			FROM fn_tblTransaction_SelectKD(@KnowledgeDate)
			WHERE (TransactionParentID = @TransactionParentID) AND (TransactionLeg = 1)
			
			SELECT @RVal = @TransactionParentID
	  END
	  
	IF @@ERROR = 0
	  BEGIN
	    RETURN @RVal
	  END
	ELSE
	  BEGIN
	    RETURN 0
	  END

	RETURN 0

GO

USE [Renaissance]
GO
/****** Object:  StoredProcedure [dbo].[adp_tblTransaction_SetTradeDates]    Script Date: 22/01/2014 10:54:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[adp_tblTransaction_SetTradeDates]
	(
	@TransactionParentID int = 0,
	@TransactionSettlementDate datetime,
	@TransactionValueDate datetime
	)
  --**************************************************************************************************
  -- Purpose: Procedure to quickly change the trade status of an existing transaction
  --
  --
  -- Accepts:
  --
  --
  --**************************************************************************************************
AS
	SET NOCOUNT OFF;

	DECLARE @KnowledgeDate datetime = CONVERT(date, '1900-01-01')

	DECLARE @TransactionFIFOValueDate datetime
	DECLARE @TransactionEffectiveValueDate datetime
	DECLARE @TransactionConfirmationDate datetime
	
	SET @TransactionFIFOValueDate = @TransactionValueDate
	SET @TransactionEffectiveValueDate = @TransactionValueDate
	SET @TransactionConfirmationDate = CONVERT(date, GETDATE())

	IF (@TransactionValueDate > '1970-01-01') AND (@TransactionSettlementDate > '1970-01-01')
	BEGIN

		INSERT INTO [renaissance].[dbo].tblTransaction(
			TransactionTicket, 
			TransactionFund, 
			TransactionSubFund,
			TransactionInstrument,  
			TransactionType, 
			TransactionType_Contra, 
			TransactionValueorAmount, 
			TransactionUnits, 
			TransactionSignedUnits, 
			TransactionPrice, 
			TransactionCleanPrice ,
			TransactionAccruedInterest ,
			TransactionBrokerFees ,
			TransactionCosts, 
			TransactionCostPercent, 
			TransactionSettlement, 
			TransactionSignedSettlement,
			TransactionFXRate, 
			TransactionSettlementCurrencyID,
			TransactionEffectivePrice , 
			TransactionEffectiveWatermark , 
			TransactionSpecificInitialEqualisationValue ,
			TransactionCounterparty, 
			TransactionBroker, 
			TransactionBrokerStrategy,
			TransactionInvestment, 
			TransactionExecution,  
			TransactionDataEntry, 
			TransactionDecisionDate, 
			TransactionValueDate, 
			TransactionFIFOValueDate, 
			TransactionEffectiveValueDate , 
			TransactionSettlementDate,  
			TransactionConfirmationDate, 
			TransactionCostIsPercent, 
			TransactionExemptFromUpdate, 
			TransactionRedeemWholeHoldingFlag, 
			TransactionIsTransfer , 
			TransactionSpecificInitialEqualisationFlag , 
			TransactionFuturesStylePricing , 
			TransactionClosingTrade, 
			TransactionIsFX, 
			TransactionFinalAmount, 
			TransactionFinalPrice, 
			TransactionFinalCosts,  
			TransactionFinalSettlement, 
			TransactionInstructionFlag, 
			TransactionBankConfirmation, 
			TransactionInitialDataEntry, 
			TransactionAmountConfirmed,  
			TransactionSettlementConfirmed, 
			TransactionFinalDataEntry, 
			TransactionComment, 
			TransactionParentID, 
			TransactionRedemptionID, 
			TransactionVersusInstrument, 
			TransactionSpecificToFundUnitID, 
			TransactionGroup, 
			TransactionTradeStatusID, 
			TransactionLeg, 
			TransactionAdminStatus, 
			TransactionCompoundRN, 
			TransactionCTFLAGS,
			TransactionVersion
			)
		SELECT
			TransactionTicket, 
			TransactionFund, 
			TransactionSubFund,
			TransactionInstrument,  
			TransactionType, 
			TransactionType_Contra, 
			TransactionValueorAmount, 
			TransactionUnits, 
			TransactionSignedUnits, 
			TransactionPrice, 
			TransactionCleanPrice ,
			TransactionAccruedInterest ,
			TransactionBrokerFees ,
			TransactionCosts, 
			TransactionCostPercent, 
			TransactionSettlement, 
			TransactionSignedSettlement,
			TransactionFXRate, 
			TransactionSettlementCurrencyID,
			TransactionEffectivePrice , 
			TransactionEffectiveWatermark , 
			TransactionSpecificInitialEqualisationValue ,
			TransactionCounterparty, 
			TransactionBroker, 
			TransactionBrokerStrategy,
			TransactionInvestment, 
			TransactionExecution,  
			TransactionDataEntry, 
			TransactionDecisionDate, 
			@TransactionValueDate, 
			@TransactionFIFOValueDate, 
			@TransactionEffectiveValueDate , 
			@TransactionSettlementDate,  
			@TransactionConfirmationDate, 
			TransactionCostIsPercent, 
			TransactionExemptFromUpdate, 
			TransactionRedeemWholeHoldingFlag, 
			TransactionIsTransfer , 
			TransactionSpecificInitialEqualisationFlag , 
			TransactionFuturesStylePricing , 
			TransactionClosingTrade ,
			TransactionIsFX, 
			TransactionFinalAmount, 
			TransactionFinalPrice, 
			TransactionFinalCosts,  
			TransactionFinalSettlement, 
			TransactionInstructionFlag, 
			TransactionBankConfirmation, 
			TransactionInitialDataEntry, 
			TransactionAmountConfirmed,  
			TransactionSettlementConfirmed, 
			TransactionFinalDataEntry, 
			LEFT(TransactionComment + ', neo_Confirmed', 500), 
			TransactionParentID, 
			TransactionRedemptionID, 
			TransactionVersusInstrument, 
			TransactionSpecificToFundUnitID, 
			TransactionGroup, 
			TransactionTradeStatusID, 
			TransactionLeg, 
			TransactionAdminStatus, 
			TransactionCompoundRN, 
			TransactionCTFLAGS,
			TransactionVersion
		FROM tblTransaction
		WHERE RN IN ( SELECT MAX(RN) 
					FROM tblTransaction 
					WHERE (TransactionParentID = @TransactionParentID) AND
							(((TransactionDateEntered <= @KnowledgeDate) or (TransactionDateEntered IS NULL)) OR (ISNULL(@KnowledgeDate, '1900-01-01') <= '1900-01-01')) AND 
							(((TransactionDateDeleted > @KnowledgeDate) or (TransactionDateDeleted IS NULL)) AND (NOT ((ISNULL(@KnowledgeDate, '1900-01-01')  <= '1900-01-01') AND (NOT (TransactionDateDeleted is NULL)))))
					GROUP BY TransactionParentID, TransactionLeg )
			AND (TransactionValueDate <> @TransactionValueDate)
			AND (TransactionSettlementDate <> @TransactionSettlementDate)
	

	END	

	RETURN 0
	
GO

USE [Renaissance]
GO
/****** Object:  StoredProcedure [dbo].[adp_tblTransaction_SetTradeStatus]    Script Date: 22/01/2014 10:54:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[adp_tblTransaction_SetTradeStatus]
	(
	@TransactionParentID int = 0,
  @TransactionTradeStatusID int = 0,      
  @SetToZero int = 0, 
  @ConfirmedTrade int = 0,
  @ConfirmationDate date = null,
	@Comment varchar (500) = '',
	@KnowledgeDate datetime
	)
  --**************************************************************************************************
  -- Purpose: Procedure to quickly change the trade status of an existing transaction
  --
  --
  -- Accepts:
  --
  --
  --**************************************************************************************************
AS
	SET NOCOUNT OFF
	

	INSERT INTO tblTransaction(
		TransactionTicket, 
		TransactionFund, 
		TransactionSubFund,
		TransactionInstrument,  
		TransactionType, 
		TransactionType_Contra, 
		TransactionValueorAmount, 
		TransactionUnits, 
		TransactionSignedUnits, 
		TransactionPrice, 
		TransactionCleanPrice ,
		TransactionAccruedInterest ,
		TransactionBrokerFees ,
		TransactionCosts, 
		TransactionCostPercent, 
		TransactionSettlement, 
		TransactionSignedSettlement,
		TransactionFXRate, 
		TransactionSettlementCurrencyID,
		TransactionEffectivePrice , 
		TransactionEffectiveWatermark , 
		TransactionSpecificInitialEqualisationValue ,
		TransactionCounterparty, 
		TransactionBroker, 
		TransactionBrokerStrategy,
		TransactionInvestment, 
		TransactionExecution,  
		TransactionDataEntry, 
		TransactionDecisionDate, 
		TransactionValueDate, 
		TransactionFIFOValueDate, 
		TransactionEffectiveValueDate , 
		TransactionSettlementDate,  
		TransactionConfirmationDate, 
		TransactionCostIsPercent, 
		TransactionExemptFromUpdate, 
		TransactionRedeemWholeHoldingFlag, 
		TransactionIsTransfer , 
		TransactionSpecificInitialEqualisationFlag , 
		TransactionFuturesStylePricing , 
		TransactionClosingTrade, 
		TransactionIsFX, 
		TransactionFinalAmount, 
		TransactionFinalPrice, 
		TransactionFinalCosts,  
		TransactionFinalSettlement, 
		TransactionInstructionFlag, 
		TransactionBankConfirmation, 
		TransactionInitialDataEntry, 
		TransactionAmountConfirmed,  
		TransactionSettlementConfirmed, 
		TransactionFinalDataEntry, 
		TransactionComment, 
		TransactionParentID, 
		TransactionRedemptionID, 
		TransactionVersusInstrument, 
		TransactionSpecificToFundUnitID, 
		TransactionGroup, 
    TransactionTradeStatusID, 
		TransactionLeg, 
		TransactionAdminStatus, 
		TransactionCompoundRN, 
		TransactionCTFLAGS,
    TransactionVersion
		)
  SELECT
		TransactionTicket, 
		TransactionFund, 
		TransactionSubFund,
		TransactionInstrument,  
		TransactionType, 
		TransactionType_Contra, 
		TransactionValueorAmount, 
		IIF(ISNULL(@SetToZero, 0) = 0, TransactionUnits, 0), 
		IIF(ISNULL(@SetToZero, 0) = 0, TransactionSignedUnits, 0), 
		TransactionPrice, 
		TransactionCleanPrice,
		IIF(ISNULL(@SetToZero, 0) = 0, TransactionAccruedInterest, 0),
		IIF(ISNULL(@SetToZero, 0) = 0, TransactionBrokerFees, 0),
		IIF(ISNULL(@SetToZero, 0) = 0, TransactionCosts, 0), 
		TransactionCostPercent, 
		IIF(ISNULL(@SetToZero, 0) = 0, TransactionSettlement, 0), 
		IIF(ISNULL(@SetToZero, 0) = 0, TransactionSignedSettlement, 0), 
		TransactionFXRate, 
		TransactionSettlementCurrencyID,
		TransactionEffectivePrice , 
		TransactionEffectiveWatermark , 
		TransactionSpecificInitialEqualisationValue ,
		TransactionCounterparty, 
		TransactionBroker, 
		TransactionBrokerStrategy,
		TransactionInvestment, 
		TransactionExecution,  
		TransactionDataEntry, 
		TransactionDecisionDate, 
		TransactionValueDate, 
		TransactionFIFOValueDate, 
		TransactionEffectiveValueDate , 
		TransactionSettlementDate,  
		IIF(ISNULL(@ConfirmationDate, '1900-01-01') <= '1900-01-01', TransactionConfirmationDate, @ConfirmationDate), 
		TransactionCostIsPercent, 
		TransactionExemptFromUpdate, 
		TransactionRedeemWholeHoldingFlag, 
		TransactionIsTransfer , 
		TransactionSpecificInitialEqualisationFlag , 
		TransactionFuturesStylePricing , 
		TransactionClosingTrade, 
		TransactionIsFX, 
		IIF(ISNULL(@ConfirmedTrade, 0) = 0, TransactionFinalAmount, 1), 
		IIF(ISNULL(@ConfirmedTrade, 0) = 0, TransactionFinalPrice, 1), 
		IIF(ISNULL(@ConfirmedTrade, 0) = 0, TransactionFinalCosts, 1), 
		IIF(ISNULL(@ConfirmedTrade, 0) = 0, TransactionFinalSettlement, 1), 
		IIF(ISNULL(@ConfirmedTrade, 0) = 0, TransactionInstructionFlag, 1), 
		IIF(ISNULL(@ConfirmedTrade, 0) = 0, TransactionBankConfirmation, 1), 
		IIF(ISNULL(@ConfirmedTrade, 0) = 0, TransactionInitialDataEntry, 1), 
		IIF(ISNULL(@ConfirmedTrade, 0) = 0, TransactionAmountConfirmed, 1), 
		IIF(ISNULL(@ConfirmedTrade, 0) = 0, TransactionSettlementConfirmed, 1), 
		IIF(ISNULL(@ConfirmedTrade, 0) = 0, TransactionFinalDataEntry, 1), 
    LEFT(ISNULL(TransactionComment, '') + IIF(LEN(ISNULL(@Comment, '')) > 0, ' : ' + @Comment, ''), 500),
		TransactionParentID, 
		TransactionRedemptionID, 
		TransactionVersusInstrument, 
		TransactionSpecificToFundUnitID, 
		TransactionGroup, 
    ISNULL(@TransactionTradeStatusID, 0), 
		TransactionLeg, 
		TransactionAdminStatus, 
		TransactionCompoundRN, 
		TransactionCTFLAGS,
    TransactionVersion
  FROM tblTransaction
  WHERE RN IN ( SELECT MAX(RN) 
                FROM tblTransaction 
                WHERE (TransactionParentID = @TransactionParentID) AND
                      (((TransactionDateEntered <= @KnowledgeDate) or (TransactionDateEntered IS NULL)) OR (ISNULL(@KnowledgeDate, '1900-01-01') <= '1900-01-01')) AND 
                      (((TransactionDateDeleted > @KnowledgeDate) or (TransactionDateDeleted IS NULL)) AND (NOT ((ISNULL(@KnowledgeDate, '1900-01-01')  <= '1900-01-01') AND (NOT (TransactionDateDeleted is NULL)))))
                GROUP BY TransactionParentID, TransactionLeg )

	RETURN 0
	
GO

USE [Renaissance]
GO
/****** Object:  StoredProcedure [dbo].[web_setTradeConfirmation]    Script Date: 22/01/2014 10:55:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:	Nicholas Pennington
-- Create date: 8 March 2013
-- Description:	Utility function to update transactions with Confirmation details
--
-- Broker IDs, at the time of writing :
--    1 : BNP Paris,
--    2 : BNP Luxembourg.
--
-- Update :
--	17 Apr 2013, NPP, Change CURSOR to use @TradeTable, Fixes extreme performance issue. (Strange).
--	17 Apr 2013, NPP, Other Misc Fixes.
--  07 Jan 2014, NPP, Added new Transaction fields
-- =============================================
ALTER PROCEDURE [dbo].[web_setTradeConfirmation]
	@FileName [nvarchar](50) = '',
	@BrokerID int = 0,
	@TransactionParentID int = 0,
	@Status [nvarchar](50) = '',
	@Cancelled [nvarchar](50) = '',
	@AccountNumber [nvarchar](50) = '',
	@BankReference [nvarchar](50) = '',
	@SettlementDate [nvarchar](20) = '',
	@NavDate [nvarchar](20) = '',
	@ISIN [nvarchar](50) = '',
	@Category [nvarchar](50) = '',
	@OrderType [nvarchar](50) = '',
	@Quantity [float] = 0,
	@QuantityMetric [nvarchar](50) = '',
	@Price [float] = 0,
	@PriceCurrency [nvarchar](5) = '',
	@GrossCashLocal [float] = 0,
	@GrossCashLocalCurrency [nvarchar](5) = '',
	@NetCashSettlement [float] = 0,
	@NetCashSettlementCurrency [nvarchar](5) = '',
	@ConfirmationText [nvarchar](2000) = ''
AS
BEGIN
	SET NOCOUNT ON;

	SET @Status = RTRIM(LTRIM(ISNULL(@Status, '')))
	SET @Cancelled = RTRIM(LTRIM(ISNULL(@Cancelled, '')))

	DECLARE @TRADE_UPDATED [int] = 0

	DECLARE @RC_Trade int
	DECLARE @RC2 int
	DECLARE @TEMPSTRING nvarchar(500)
	DECLARE @TransactionTicket nvarchar(50)
	DECLARE @TransactionFund int
	DECLARE @TransactionSubFund int
	DECLARE @TransactionInstrument int
	DECLARE @TransactionType int
	DECLARE @TransactionValueorAmount nvarchar(10) = 'Amount'
	DECLARE @TransactionUnits float
	DECLARE @TransactionPrice float
	DECLARE @TransactionCleanPrice float
	DECLARE @TransactionAccruedInterest float
	DECLARE @TransactionBrokerFees float
	DECLARE @TransactionCosts float
	DECLARE @TransactionCostPercent float           
	DECLARE @TransactionFXRate float               
	DECLARE @TransactionSettlementCurrencyID int    
	DECLARE @TransactionEffectivePrice float       
	DECLARE @TransactionEffectiveWatermark float  
	DECLARE @TransactionSpecificInitialEqualisationValue float 
	DECLARE @TransactionCounterparty int       
	DECLARE @TransactionBroker int       
	DECLARE @TransactionBrokerStrategy varchar(50)
	DECLARE @TransactionInvestment nvarchar(50) 
	DECLARE @TransactionExecution nvarchar(50)   
	DECLARE @TransactionDataEntry nvarchar(50) 
	DECLARE @TransactionDecisionDate datetime
	DECLARE @TransactionValueDate datetime
	DECLARE @TransactionFIFOValueDate datetime
	DECLARE @TransactionEffectiveValueDate datetime
	DECLARE @TransactionSettlementDate datetime
	DECLARE @TransactionConfirmationDate datetime
	DECLARE @TransactionCostIsPercent bit     
	DECLARE @TransactionExemptFromUpdate bit     
	DECLARE @TransactionRedeemWholeHoldingFlag bit 
	DECLARE @TransactionIsTransfer bit             
	DECLARE @TransactionSpecificInitialEqualisationFlag bit
	DECLARE @TransactionClosingTrade bit            = 0  
	DECLARE @TransactionFinalAmount int             = 1
	DECLARE @TransactionFinalPrice int              = 1
	DECLARE @TransactionFinalCosts int              = 1
	DECLARE @TransactionFinalSettlement int         = 1
	DECLARE @TransactionInstructionFlag int         = 1
	DECLARE @TransactionBankConfirmation int        = 1
	DECLARE @TransactionInitialDataEntry int        = 1
	DECLARE @TransactionAmountConfirmed int         = 1
	DECLARE @TransactionSettlementConfirmed int     = 1
	DECLARE @TransactionFinalDataEntry int          = 1
	DECLARE @TransactionComment nvarchar(500)
	DECLARE @TransactionRedemptionID int          
	DECLARE @TransactionVersusInstrument int        
	DECLARE @TransactionSpecificToFundUnitID int        
	DECLARE @TransactionGroup nvarchar(50)     
	DECLARE @TransactionAdminStatus int       
	DECLARE @TransactionCTFLAGS int                 = 0
	DECLARE @TransactionCompoundRN int              = 0
	DECLARE @KnowledgeDate datetime                 = null
	DECLARE @ValidationOnly int                     = 0
	DECLARE @rvStatusString nvarchar(200)

	-- Use Temporary Transactions table as the performance of a CURSOR directly on the tblTransaction table is EXTREMELY bad.

	DECLARE @TradeTable TABLE
		(
		[TransactionTicket] [nvarchar](50) NOT NULL,
		[TransactionFund] [int] NOT NULL,
		[TransactionSubFund] [int] NULL,
		[TransactionInstrument] [int] NOT NULL,
		[TransactionType] [int] NOT NULL,
		[TransactionValueorAmount] [nvarchar](10) NOT NULL,
		[TransactionUnits] [float] NOT NULL,
		[TransactionPrice] [float] NOT NULL,
		[TransactionCosts] [float] NOT NULL,
		[TransactionCleanPrice] [float] NOT NULL,
		[TransactionAccruedInterest] [float] NOT NULL,
		[TransactionBrokerFees] [float] NOT NULL,
		[TransactionCostPercent] [float] NOT NULL,
		[TransactionFXRate] [float] NOT NULL,
		[TransactionSettlementCurrencyID] [float] NOT NULL,
		[TransactionEffectivePrice] [float] NOT NULL,
		[TransactionEffectiveWatermark] [float] NOT NULL,
		[TransactionSpecificInitialEqualisationValue] [float] NOT NULL,
		[TransactionCounterparty] [int] NOT NULL,
		[TransactionBroker] [int] NOT NULL,
		[TransactionBrokerStrategy] [varchar](50) NOT NULL,
		[TransactionInvestment] [nvarchar](50) NOT NULL,
		[TransactionExecution] [nvarchar](50) NOT NULL,
		[TransactionDataEntry] [nvarchar](50) NOT NULL,
		[TransactionDecisionDate] [datetime] NULL,
		[TransactionValueDate] [datetime] NOT NULL,
		[TransactionFIFOValueDate] [datetime] NOT NULL,
		[TransactionEffectiveValueDate] [datetime] NULL,
		[TransactionSettlementDate] [datetime] NOT NULL,
		[TransactionConfirmationDate] [datetime] NULL,
		[TransactionCostIsPercent] [bit] NOT NULL,
		[TransactionExemptFromUpdate] [bit] NOT NULL,
		[TransactionRedeemWholeHoldingFlag] [bit] NOT NULL,
		[TransactionIsTransfer] [bit] NOT NULL,
		[TransactionSpecificInitialEqualisationFlag] [bit] NOT NULL,
		[TransactionClosingTrade] [bit],																-- New
		[TransactionFinalAmount] [int] NOT NULL,
		[TransactionFinalPrice] [int] NOT NULL,
		[TransactionFinalCosts] [int] NOT NULL,
		[TransactionFinalSettlement] [int] NOT NULL,
		[TransactionInstructionFlag] [int] NOT NULL,
		[TransactionBankConfirmation] [int] NOT NULL,
		[TransactionInitialDataEntry] [int] NOT NULL,
		[TransactionAmountConfirmed] [int] NOT NULL,
		[TransactionSettlementConfirmed] [int] NOT NULL,
		[TransactionFinalDataEntry] [int] NOT NULL,
		[TransactionComment] [varchar](500) NOT NULL,
		[TransactionRedemptionID] [int] NOT NULL,
		[TransactionVersusInstrument] [int] NOT NULL,
		[TransactionSpecificToFundUnitID] [int] NOT NULL,								-- New
		[TransactionGroup] [nvarchar](50) NOT NULL,
		[TransactionAdminStatus] [int] NOT NULL
		)

	INSERT tblTradeFileConfirmations
		(
		[BrokerID],
		[TransactionParentID],
		[FileName],
		[ConfirmationText]
		)
	SELECT
		ISNULL(@BrokerID, 0), 
		ISNULL(@TransactionParentID, 0), 
		ISNULL(@FileName, ''),
		ISNULL(@ConfirmationText, '')


	-- Update Transaction.

	INSERT @TradeTable
		(
		TransactionTicket,
		TransactionFund,
		TransactionSubFund,
		TransactionInstrument,
		TransactionType,
		TransactionValueorAmount,
		TransactionUnits,
		TransactionPrice,
		TransactionCleanPrice ,
		TransactionAccruedInterest ,
		TransactionBrokerFees ,
		TransactionCosts,
		TransactionCostPercent,           
		TransactionFXRate,               
		TransactionSettlementCurrencyID,
		TransactionEffectivePrice,
		TransactionEffectiveWatermark,
		TransactionSpecificInitialEqualisationValue, 
		TransactionCounterparty,
		TransactionBroker,
		TransactionBrokerStrategy,
		TransactionInvestment,
		TransactionExecution,
		TransactionDataEntry,
		TransactionDecisionDate,
		TransactionValueDate,
		TransactionFIFOValueDate,
		TransactionEffectiveValueDate,
		TransactionSettlementDate,
		TransactionConfirmationDate,
		TransactionCostIsPercent,
		TransactionExemptFromUpdate,
		TransactionRedeemWholeHoldingFlag,
		TransactionIsTransfer,
		TransactionSpecificInitialEqualisationFlag,
		TransactionClosingTrade,
		TransactionFinalAmount,
		TransactionFinalPrice,
		TransactionFinalCosts,
		TransactionFinalSettlement,
		TransactionInstructionFlag,
		TransactionBankConfirmation,
		TransactionInitialDataEntry,
		TransactionAmountConfirmed,
		TransactionSettlementConfirmed,
		TransactionFinalDataEntry,
		TransactionComment,
		TransactionRedemptionID,
		TransactionVersusInstrument,
		TransactionSpecificToFundUnitID,
		TransactionGroup,
		TransactionAdminStatus
		)
	SELECT
		TransactionTicket,
		TransactionFund,
		TransactionSubFund,
		TransactionInstrument,
		TransactionType,
		TransactionValueorAmount,
		TransactionUnits,
		TransactionPrice,
		TransactionCleanPrice ,
		TransactionAccruedInterest ,
		TransactionBrokerFees ,
		TransactionCosts,
		TransactionCostPercent,           
		TransactionFXRate,               
		TransactionSettlementCurrencyID,
		TransactionEffectivePrice,
		TransactionEffectiveWatermark,
		TransactionSpecificInitialEqualisationValue, 
		TransactionCounterparty,
		TransactionBroker,
		TransactionBrokerStrategy,
		TransactionInvestment,
		TransactionExecution,
		TransactionDataEntry,
		TransactionDecisionDate,
		TransactionValueDate,
		TransactionFIFOValueDate,
		TransactionEffectiveValueDate,
		TransactionSettlementDate,
		TransactionConfirmationDate,
		TransactionCostIsPercent,
		TransactionExemptFromUpdate,
		TransactionRedeemWholeHoldingFlag,
		TransactionIsTransfer,
		TransactionSpecificInitialEqualisationFlag,
		TransactionClosingTrade,
		TransactionFinalAmount,
		TransactionFinalPrice,
		TransactionFinalCosts,
		TransactionFinalSettlement,
		TransactionInstructionFlag,
		TransactionBankConfirmation,
		TransactionInitialDataEntry,
		TransactionAmountConfirmed,
		TransactionSettlementConfirmed,
		TransactionFinalDataEntry,
		TransactionComment,
		TransactionRedemptionID,
		TransactionVersusInstrument,
		TransactionSpecificToFundUnitID,
		TransactionGroup,
		TransactionAdminStatus
	FROM 
		fn_tblTransactionSingle_SelectKD(@TransactionParentID, 0, Null)
	WHERE 
		(TransactionLeg = 1) AND (TransactionDateDeleted IS NULL)

  	IF (ISNULL(@TransactionParentID, 0) > 0)
	BEGIN
		DECLARE THIS_TRADE CURSOR LOCAL 
		FOR 
		SELECT
			TransactionTicket,
			TransactionFund,
			TransactionSubFund,
			TransactionInstrument,
			TransactionType,
			TransactionValueorAmount,
			TransactionUnits,
			TransactionPrice,
			TransactionCleanPrice ,
			TransactionAccruedInterest ,
			TransactionBrokerFees ,
			TransactionCosts,
			TransactionCostPercent,           
			TransactionFXRate,               
			TransactionSettlementCurrencyID,
			TransactionEffectivePrice,
			TransactionEffectiveWatermark,
			TransactionSpecificInitialEqualisationValue, 
			TransactionCounterparty,
			TransactionBroker,
			TransactionBrokerStrategy,
			TransactionInvestment,
			TransactionExecution,
			TransactionDataEntry,
			TransactionDecisionDate,
			TransactionValueDate,
			TransactionFIFOValueDate,
			TransactionEffectiveValueDate,
			TransactionSettlementDate,
			TransactionConfirmationDate,
			TransactionCostIsPercent,
			TransactionExemptFromUpdate,
			TransactionRedeemWholeHoldingFlag,
			TransactionIsTransfer,
			TransactionSpecificInitialEqualisationFlag,
			TransactionClosingTrade,
			TransactionFinalAmount,
			TransactionFinalPrice,
			TransactionFinalCosts,
			TransactionFinalSettlement,
			TransactionInstructionFlag,
			TransactionBankConfirmation,
			TransactionInitialDataEntry,
			TransactionAmountConfirmed,
			TransactionSettlementConfirmed,
			TransactionFinalDataEntry,
			TransactionComment,
			TransactionRedemptionID,
			TransactionVersusInstrument,
			TransactionSpecificToFundUnitID,
			TransactionGroup,
			TransactionAdminStatus
		FROM @TradeTable
	

		OPEN THIS_TRADE

		FETCH NEXT 
		FROM THIS_TRADE 
		INTO 
			@TransactionTicket,
			@TransactionFund,
			@TransactionSubFund,
			@TransactionInstrument,
			@TransactionType,
			@TransactionValueorAmount,
			@TransactionUnits,
			@TransactionPrice,
			@TransactionCleanPrice ,
			@TransactionAccruedInterest ,
			@TransactionBrokerFees ,
			@TransactionCosts,
			@TransactionCostPercent,           
			@TransactionFXRate,               
			@TransactionSettlementCurrencyID,
			@TransactionEffectivePrice,
			@TransactionEffectiveWatermark,
			@TransactionSpecificInitialEqualisationValue, 
			@TransactionCounterparty,
			@TransactionBroker,
			@TransactionBrokerStrategy,
			@TransactionInvestment,
			@TransactionExecution,
			@TransactionDataEntry,
			@TransactionDecisionDate,
			@TransactionValueDate,
			@TransactionFIFOValueDate,
			@TransactionEffectiveValueDate,
			@TransactionSettlementDate,
			@TransactionConfirmationDate,
			@TransactionCostIsPercent,
			@TransactionExemptFromUpdate,
			@TransactionRedeemWholeHoldingFlag,
			@TransactionIsTransfer,
			@TransactionSpecificInitialEqualisationFlag,
			@TransactionClosingTrade,
			@TransactionFinalAmount,
			@TransactionFinalPrice,
			@TransactionFinalCosts,
			@TransactionFinalSettlement,
			@TransactionInstructionFlag,
			@TransactionBankConfirmation,
			@TransactionInitialDataEntry,
			@TransactionAmountConfirmed,
			@TransactionSettlementConfirmed,
			@TransactionFinalDataEntry,
			@TransactionComment,
			@TransactionRedemptionID,
			@TransactionVersusInstrument,
			@TransactionSpecificToFundUnitID,
			@TransactionGroup,
			@TransactionAdminStatus

		IF  @@FETCH_STATUS = 0
		BEGIN

			DECLARE @InstrumentType int
			DECLARE @InstrumentCurrencyID int
			DECLARE @SettlementCurrencyID int
			DECLARE @InstrumentTypeIsFuturesStyle bit
			DECLARE @InstrumentContractSize float = 1
			DECLARE @InstrumentMultiplier float = 1
			DECLARE @BACKOFFICE int = 0

			-- Get Some Static Data

			-- Get Instrument Currency Code 

			SELECT	@InstrumentType = ISNULL(Max(InstrumentType), 0), 
					@InstrumentCurrencyID = ISNULL(MAX(InstrumentCurrencyID), 0), 
					@InstrumentContractSize = ISNULL(MAX(InstrumentContractSize), 1.0), 
					@InstrumentMultiplier = ISNULL(MAX(InstrumentMultiplier), 1.0)
			FROM fn_tblInstrument_SelectSingle(@TransactionInstrument, @KnowledgeDate)
			WHERE InstrumentID = @TransactionInstrument

			SET @SettlementCurrencyID = @InstrumentCurrencyID

			IF (@GrossCashLocalCurrency <> @NetCashSettlementCurrency)
			BEGIN
				SELECT @SettlementCurrencyID = ISNULL(MAX(CurrencyID), @InstrumentCurrencyID)
				FROM fn_tblCurrency_SelectKD(Null)
				WHERE CurrencyCode = @NetCashSettlementCurrency
			END

			--

			IF (@BrokerID = 1) -- BNP Paris
			BEGIN
				
				SET @BACKOFFICE = 185 -- Validated BP2S

				SET @TransactionValueDate = CONVERT(datetime, @NavDate)
				SET @TransactionFIFOValueDate = @TransactionValueDate
				SET @TransactionEffectiveValueDate = @TransactionValueDate
				SET @TransactionSettlementDate  = CONVERT(datetime, @SettlementDate)
				SET @TransactionFXRate = 1
				SET @TransactionUnits = ABS(@Quantity)
				SET @TransactionConfirmationDate = CONVERT(date, GETDATE())

				-- Calculate Local Price

				IF (@Quantity <> 0)
				BEGIN
					SET @TransactionPrice = ABS(@GrossCashLocal) / (ABS(@Quantity) * @InstrumentContractSize * @InstrumentMultiplier)
					SET @TransactionEffectivePrice = @TransactionPrice

					IF (@TransactionCleanPrice <> 0) OR (@TransactionAccruedInterest <> 0)
					BEGIN
						SET @TransactionCleanPrice = @TransactionPrice
						SET @TransactionAccruedInterest = 0
					END

				END

				-- If Settlement currency is Instrument Currency then calculate costs figure, otherwise it gets lost in the FX.
				SET @TransactionCostIsPercent = 0
				SET @TransactionCosts = 0
				SET @TransactionBrokerFees = 0

				IF (@GrossCashLocalCurrency = @NetCashSettlementCurrency)
				BEGIN
					SET @TransactionSettlementCurrencyID = 0

					IF @GrossCashLocal <> @NetCashSettlement
					BEGIN
						SET @TransactionCosts = @GrossCashLocal - @NetCashSettlement
					END

				END
				ELSE
				BEGIN
					SET @TransactionSettlementCurrencyID = @SettlementCurrencyID
					
					IF @GrossCashLocal <> 0
					BEGIN
						SET @TransactionFXRate = ABS(@NetCashSettlement) / ABS(@GrossCashLocal)
					END
				END

				SET @TransactionComment = @TransactionComment + ', Confirmed'


				-- Check for Cancelled or Rejected trade

				IF @Cancelled <> 'N'
				BEGIN					
					SET @BACKOFFICE = 180 -- Cancelled
					SET @TransactionUnits = 0
				    SET @TransactionComment = @TransactionComment + ' - Cancelled BP2S'
				END

				IF @Status = 'RJ'
				BEGIN					
					SET @BACKOFFICE = 191 -- Rejected
					SET @TransactionUnits = 0
				    SET @TransactionComment = @TransactionComment + ' - Rejected BP2S'
				END

				SET @TRADE_UPDATED = 1 -- Increment HAS_TRANSACTION

			END

			IF (@BrokerID = 2) -- BNP Luxembourg
			BEGIN






				SET @TRADE_UPDATED = 1 -- Increment HAS_TRANSACTION

			END

			IF (@BrokerID = 1) OR (@BrokerID = 2) 
			BEGIN

				EXECUTE @RC_Trade = [dbo].[adp_tblTransaction_InsertCommand] 
					 @TransactionTicket               = @TransactionTicket
					,@TransactionFund                 = @TransactionFund
					,@TransactionSubFund              = @TransactionSubFund
					,@TransactionInstrument           = @TransactionInstrument
					,@TransactionType                 = @TransactionType
					,@TransactionValueorAmount        = @TransactionValueorAmount
					,@TransactionUnits                = @TransactionUnits
					,@TransactionPrice                = @TransactionPrice
					,@TransactionCleanPrice						= @TransactionCleanPrice
					,@TransactionAccruedInterest			= @TransactionAccruedInterest
					,@TransactionBrokerFees						= @TransactionBrokerFees
					,@TransactionCosts                = @TransactionCosts
					,@TransactionCostPercent          = @TransactionCostPercent
					,@TransactionFXRate               = @TransactionFXRate
					,@TransactionSettlementCurrencyID = @TransactionSettlementCurrencyID
					,@TransactionUseFixedSettlement   = 0
					,@TransactionFixedSettlementValue = 0
					,@TransactionEffectivePrice       = @TransactionEffectivePrice
					,@TransactionEffectiveWatermark   = @TransactionEffectiveWatermark
					,@TransactionSpecificInitialEqualisationValue = @TransactionSpecificInitialEqualisationValue
					,@TransactionCounterparty         = @TransactionCounterparty
					,@TransactionBroker								= @TransactionBroker
					,@TransactionBrokerStrategy				= @TransactionBrokerStrategy
					,@TransactionInvestment           = @TransactionInvestment
					,@TransactionExecution            = @TransactionExecution
					,@TransactionDataEntry            = @TransactionDataEntry
					,@TransactionDecisionDate         = @TransactionDecisionDate
					,@TransactionValueDate            = @TransactionValueDate
					,@TransactionFIFOValueDate        = @TransactionFIFOValueDate
					,@TransactionEffectiveValueDate   = @TransactionEffectiveValueDate
					,@TransactionSettlementDate       = @TransactionSettlementDate
					,@TransactionConfirmationDate     = @TransactionConfirmationDate
					,@TransactionCostIsPercent        = @TransactionCostIsPercent
					,@TransactionExemptFromUpdate     = @TransactionExemptFromUpdate
					,@TransactionRedeemWholeHoldingFlag = @TransactionRedeemWholeHoldingFlag
					,@TransactionIsTransfer           = @TransactionIsTransfer
					,@TransactionSpecificInitialEqualisationFlag = @TransactionSpecificInitialEqualisationFlag
					,@TransactionClosingTrade					= @TransactionClosingTrade
					,@TransactionFinalAmount          = @TransactionFinalAmount
					,@TransactionFinalPrice           = @TransactionFinalPrice
					,@TransactionFinalCosts           = @TransactionFinalCosts
					,@TransactionFinalSettlement      = @TransactionFinalSettlement
					,@TransactionInstructionFlag      = @TransactionInstructionFlag
					,@TransactionBankConfirmation     = @TransactionBankConfirmation
					,@TransactionInitialDataEntry     = @TransactionInitialDataEntry
					,@TransactionAmountConfirmed      = @TransactionAmountConfirmed
					,@TransactionSettlementConfirmed  = @TransactionSettlementConfirmed
					,@TransactionFinalDataEntry       = @TransactionFinalDataEntry
					,@TransactionComment              = @TransactionComment
					,@TransactionParentID             = @TransactionParentID
					,@TransactionRedemptionID         = @TransactionRedemptionID
					,@TransactionVersusInstrument     = @TransactionVersusInstrument
					,@TransactionSpecificToFundUnitID = @TransactionSpecificToFundUnitID
					,@TransactionGroup                = @TransactionGroup
					,@TransactionTradeStatusID        = @BACKOFFICE
					,@TransactionAdminStatus          = @TransactionAdminStatus
					,@TransactionCTFLAGS              = @TransactionCTFLAGS
					,@TransactionCompoundRN           = @TransactionCompoundRN
					,@KnowledgeDate                   = @KnowledgeDate
					,@ValidationOnly                  = @ValidationOnly
					,@rvStatusString                  = @rvStatusString OUTPUT;

			END

		END -- IF @@FETCH_STATUS = 0

	END -- IF TransactionParentID > 0

	CLOSE THIS_TRADE
	DEALLOCATE THIS_TRADE

	-- Update 'Auto Update' table.

	IF (@TRADE_UPDATED > 0)
	BEGIN
		IF (ISNULL(@RC_Trade, 0) > 0)
		BEGIN
			UPDATE tblAutoUpdate_Detail
			SET [DETAIL_TRANSACTION] = CONVERT(varchar(2000), ISNULL([DETAIL_TRANSACTION], '') + CONVERT(varchar, ISNULL(@RC_Trade, 0)) + ',')
			FROM tblAutoUpdate_Detail
		END
		ELSE
		BEGIN
			UPDATE tblAutoUpdate_Detail
			SET [DETAIL_TRANSACTION] = '-' -- Update whole table.
			FROM tblAutoUpdate_Detail
		END
	END

	-- Update 'Auto Update' table, Hmmm......

	UPDATE [dbo].[tblAutoUpdate]
	SET HAS_TRADEFILECONFIRMATIONS = HAS_TRADEFILECONFIRMATIONS + 1, HAS_TRANSACTION = HAS_TRANSACTION + @TRADE_UPDATED

	SELECT @rvStatusString AS STATUS

END

GO

