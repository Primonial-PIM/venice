USE [Renaissance]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblWorkflows_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblWorkflows] DROP CONSTRAINT [DF_tblWorkflows_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblWorkflows_UserName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblWorkflows] DROP CONSTRAINT [DF_tblWorkflows_UserName]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblWorkflows_ResultingTradeStatus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblWorkflows] DROP CONSTRAINT [DF_tblWorkflows_ResultingTradeStatus]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblWorkflows_TRADEFILE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblWorkflows] DROP CONSTRAINT [DF_tblWorkflows_TRADEFILE]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblWorkflows_WorkDetails]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblWorkflows] DROP CONSTRAINT [DF_tblWorkflows_WorkDetails]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblWorkflows_WorkflowDescription]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblWorkflows] DROP CONSTRAINT [DF_tblWorkflows_WorkflowDescription]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblWorkflows_WorkflowName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblWorkflows] DROP CONSTRAINT [DF_tblWorkflows_WorkflowName]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblWorkflowRules_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblWorkflowRules] DROP CONSTRAINT [DF_tblWorkflowRules_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblWorkflowRules_UserName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblWorkflowRules] DROP CONSTRAINT [DF_tblWorkflowRules_UserName]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblWorkflowRules_RuleDescription]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblWorkflowRules] DROP CONSTRAINT [DF_tblWorkflowRules_RuleDescription]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblWorkflowRules_ResultingWorkflow]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblWorkflowRules] DROP CONSTRAINT [DF_tblWorkflowRules_ResultingWorkflow]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblWorkflowRules_IsNewTrade]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblWorkflowRules] DROP CONSTRAINT [DF_tblWorkflowRules_IsNewTrade]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblWorkflowRules_IsApprovalRule]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblWorkflowRules] DROP CONSTRAINT [DF_tblWorkflowRules_IsApprovalRule]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblWorkflowRules_CurrentStatus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblWorkflowRules] DROP CONSTRAINT [DF_tblWorkflowRules_CurrentStatus]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblWorkflowRules_InstrumentID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblWorkflowRules] DROP CONSTRAINT [DF_tblWorkflowRules_InstrumentID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblWorkflowRules_Currency]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblWorkflowRules] DROP CONSTRAINT [DF_tblWorkflowRules_Currency]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblWorkflowRules_Exchange]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblWorkflowRules] DROP CONSTRAINT [DF_tblWorkflowRules_Exchange]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblWorkflowRules_InstrumentType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblWorkflowRules] DROP CONSTRAINT [DF_tblWorkflowRules_InstrumentType]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblWorkflowRules_Fund]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblWorkflowRules] DROP CONSTRAINT [DF_tblWorkflowRules_Fund]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblVeniceFormTooltips_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblVeniceFormTooltips] DROP CONSTRAINT [DF_tblVeniceFormTooltips_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblVeniceFormTooltips_ToolTip]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblVeniceFormTooltips] DROP CONSTRAINT [DF_tblVeniceFormTooltips_ToolTip]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblVeniceFormTooltips_ControlNane]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblVeniceFormTooltips] DROP CONSTRAINT [DF_tblVeniceFormTooltips_ControlNane]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblVeniceFormTooltips_FormName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblVeniceFormTooltips] DROP CONSTRAINT [DF_tblVeniceFormTooltips_FormName]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblVeniceFormTooltips_APPName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblVeniceFormTooltips] DROP CONSTRAINT [DF_tblVeniceFormTooltips_APPName]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblUserPermissions_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblUserPermissions] DROP CONSTRAINT [DF_tblUserPermissions_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHedgeOPPermissions_Perm_Delete]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblUserPermissions] DROP CONSTRAINT [DF_tblHedgeOPPermissions_Perm_Delete]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHedgeOPPermissions_Perm_Update]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblUserPermissions] DROP CONSTRAINT [DF_tblHedgeOPPermissions_Perm_Update]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHedgeOPPermissions_Perm_Insert]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblUserPermissions] DROP CONSTRAINT [DF_tblHedgeOPPermissions_Perm_Insert]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHedgeOPPermissions_Perm_Read]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblUserPermissions] DROP CONSTRAINT [DF_tblHedgeOPPermissions_Perm_Read]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblUserPermissions_AppName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblUserPermissions] DROP CONSTRAINT [DF_tblUserPermissions_AppName]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblUserPermissions_ID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblUserPermissions] DROP CONSTRAINT [DF_tblUserPermissions_ID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblUnitHolderFees_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblUnitHolderFees] DROP CONSTRAINT [DF_tblUnitHolderFees_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblUnitHolderFees_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblUnitHolderFees] DROP CONSTRAINT [DF_tblUnitHolderFees_UserEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblUnitHolderFees_IsMilestone]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblUnitHolderFees] DROP CONSTRAINT [DF_tblUnitHolderFees_IsMilestone]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblUnitHolderFees_FXtoUSD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblUnitHolderFees] DROP CONSTRAINT [DF_tblUnitHolderFees_FXtoUSD]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTriggerReferencePrices_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTriggerReferencePrices] DROP CONSTRAINT [DF_tblTriggerReferencePrices_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTriggerReferencePrices_ReferencePrice]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTriggerReferencePrices] DROP CONSTRAINT [DF_tblTriggerReferencePrices_ReferencePrice]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransactionType_TransactionTypeDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransactionType] DROP CONSTRAINT [DF_tblTransactionType_TransactionTypeDateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransactionType_TransactionTypeUser]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransactionType] DROP CONSTRAINT [DF_tblTransactionType_TransactionTypeUser]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionVersion]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] DROP CONSTRAINT [DF_tblTransaction_TransactionVersion]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionCompoundID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] DROP CONSTRAINT [DF_tblTransaction_TransactionCompoundID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionFLAGS]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] DROP CONSTRAINT [DF_tblTransaction_TransactionFLAGS]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionAdminStatus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] DROP CONSTRAINT [DF_tblTransaction_TransactionAdminStatus]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionTradeStatus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] DROP CONSTRAINT [DF_tblTransaction_TransactionTradeStatus]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionGroup]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] DROP CONSTRAINT [DF_tblTransaction_TransactionGroup]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionUser]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] DROP CONSTRAINT [DF_tblTransaction_TransactionUser]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionVersusInstrument]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] DROP CONSTRAINT [DF_tblTransaction_TransactionVersusInstrument]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionRedemptionID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] DROP CONSTRAINT [DF_tblTransaction_TransactionRedemptionID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionComment]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] DROP CONSTRAINT [DF_tblTransaction_TransactionComment]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionFuturesStylePricing]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] DROP CONSTRAINT [DF_tblTransaction_TransactionFuturesStylePricing]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionSpecificInitialEqualisationFlag]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] DROP CONSTRAINT [DF_tblTransaction_TransactionSpecificInitialEqualisationFlag]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionIsTransfer]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] DROP CONSTRAINT [DF_tblTransaction_TransactionIsTransfer]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionRedeemWholeHoldingFlag]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] DROP CONSTRAINT [DF_tblTransaction_TransactionRedeemWholeHoldingFlag]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionExemptFromUpdate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] DROP CONSTRAINT [DF_tblTransaction_TransactionExemptFromUpdate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionCostIsPercent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] DROP CONSTRAINT [DF_tblTransaction_TransactionCostIsPercent]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionEntryDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] DROP CONSTRAINT [DF_tblTransaction_TransactionEntryDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionDataEntry]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] DROP CONSTRAINT [DF_tblTransaction_TransactionDataEntry]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionExecution]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] DROP CONSTRAINT [DF_tblTransaction_TransactionExecution]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionInvestment]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] DROP CONSTRAINT [DF_tblTransaction_TransactionInvestment]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionNewCounterparty]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] DROP CONSTRAINT [DF_tblTransaction_TransactionNewCounterparty]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionSpecificInitialEqualisationValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] DROP CONSTRAINT [DF_tblTransaction_TransactionSpecificInitialEqualisationValue]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionEffectiveWatermark]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] DROP CONSTRAINT [DF_tblTransaction_TransactionEffectiveWatermark]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionEffectivePrice]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] DROP CONSTRAINT [DF_tblTransaction_TransactionEffectivePrice]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionSettlementCurrencyID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] DROP CONSTRAINT [DF_tblTransaction_TransactionSettlementCurrencyID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionFXRate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] DROP CONSTRAINT [DF_tblTransaction_TransactionFXRate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionSignedSettlement]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] DROP CONSTRAINT [DF_tblTransaction_TransactionSignedSettlement]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionSettlement]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] DROP CONSTRAINT [DF_tblTransaction_TransactionSettlement]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionCostPercent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] DROP CONSTRAINT [DF_tblTransaction_TransactionCostPercent]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionCosts]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] DROP CONSTRAINT [DF_tblTransaction_TransactionCosts]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionPrice]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] DROP CONSTRAINT [DF_tblTransaction_TransactionPrice]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionSignedUnits]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] DROP CONSTRAINT [DF_tblTransaction_TransactionSignedUnits]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionUnits]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] DROP CONSTRAINT [DF_tblTransaction_TransactionUnits]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionValueorAmount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] DROP CONSTRAINT [DF_tblTransaction_TransactionValueorAmount]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionType_Contra]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] DROP CONSTRAINT [DF_tblTransaction_TransactionType_Contra]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] DROP CONSTRAINT [DF_tblTransaction_TransactionType]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionInstrument]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] DROP CONSTRAINT [DF_tblTransaction_TransactionInstrument]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionFund]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] DROP CONSTRAINT [DF_tblTransaction_TransactionFund]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionTicket]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] DROP CONSTRAINT [DF_tblTransaction_TransactionTicket]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTradeStatus_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTradeStatus] DROP CONSTRAINT [DF_tblTradeStatus_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTradeStatus_UserName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTradeStatus] DROP CONSTRAINT [DF_tblTradeStatus_UserName]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTradeFilePending_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTradeFilePending] DROP CONSTRAINT [DF_tblTradeFilePending_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTradeFilePending_UserName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTradeFilePending] DROP CONSTRAINT [DF_tblTradeFilePending_UserName]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTradeFilePending_Comment]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTradeFilePending] DROP CONSTRAINT [DF_tblTradeFilePending_Comment]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTradeFilePending_TriggerDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTradeFilePending] DROP CONSTRAINT [DF_tblTradeFilePending_TriggerDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTradeFilePending_TradefileText]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTradeFilePending] DROP CONSTRAINT [DF_tblTradeFilePending_TradefileText]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTradeFilePending_WorkflowID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTradeFilePending] DROP CONSTRAINT [DF_tblTradeFilePending_WorkflowID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTradeFilePending_BrokerID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTradeFilePending] DROP CONSTRAINT [DF_tblTradeFilePending_BrokerID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTradeFileConfirmations_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTradeFileConfirmations] DROP CONSTRAINT [DF_tblTradeFileConfirmations_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTradeFileConfirmations_UserName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTradeFileConfirmations] DROP CONSTRAINT [DF_tblTradeFileConfirmations_UserName]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTradeFileConfirmations_ConfirmationText]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTradeFileConfirmations] DROP CONSTRAINT [DF_tblTradeFileConfirmations_ConfirmationText]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTradeFileConfirmations_TransactionParentID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTradeFileConfirmations] DROP CONSTRAINT [DF_tblTradeFileConfirmations_TransactionParentID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTradeFileConfirmations_BrokerID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTradeFileConfirmations] DROP CONSTRAINT [DF_tblTradeFileConfirmations_BrokerID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTradeFileAcknowledgement_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTradeFileAcknowledgement] DROP CONSTRAINT [DF_tblTradeFileAcknowledgement_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTradeFileAcknowledgement_UserName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTradeFileAcknowledgement] DROP CONSTRAINT [DF_tblTradeFileAcknowledgement_UserName]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTradeFileAcknowledgement_AcknowledgementText]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTradeFileAcknowledgement] DROP CONSTRAINT [DF_tblTradeFileAcknowledgement_AcknowledgementText]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTradeFileAcknowledgement_TransactionParentID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTradeFileAcknowledgement] DROP CONSTRAINT [DF_tblTradeFileAcknowledgement_TransactionParentID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTradeFileAcknowledgement_BrokerID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTradeFileAcknowledgement] DROP CONSTRAINT [DF_tblTradeFileAcknowledgement_BrokerID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblSystemStrings_StringValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblSystemStrings] DROP CONSTRAINT [DF_tblSystemStrings_StringValue]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblSystemDates_SysdateDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblSystemDates] DROP CONSTRAINT [DF_tblSystemDates_SysdateDateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblSystemDates_SysdateUser]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblSystemDates] DROP CONSTRAINT [DF_tblSystemDates_SysdateUser]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblSubscriptionLog_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblSubscriptionLog] DROP CONSTRAINT [DF_tblSubscriptionLog_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblSubscriptionLog_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblSubscriptionLog] DROP CONSTRAINT [DF_tblSubscriptionLog_UserEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblSubFund_SubFundDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblSubFund] DROP CONSTRAINT [DF_tblSubFund_SubFundDateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblSubFund_UserName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblSubFund] DROP CONSTRAINT [DF_tblSubFund_UserName]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblSophisStatusGroups_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblSophisStatusGroups] DROP CONSTRAINT [DF_tblSophisStatusGroups_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblSophisStatusGroups_UserName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblSophisStatusGroups] DROP CONSTRAINT [DF_tblSophisStatusGroups_UserName]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblSophisBusinessEvents_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblSophisBusinessEvents] DROP CONSTRAINT [DF_tblSophisBusinessEvents_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblSophisBusinessEvents_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblSophisBusinessEvents] DROP CONSTRAINT [DF_tblSophisBusinessEvents_UserEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblSophisBusinessEvents_SubstituteInstrument]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblSophisBusinessEvents] DROP CONSTRAINT [DF_tblSophisBusinessEvents_SubstituteInstrument]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblSophisBusinessEvents_ExistingInstrument]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblSophisBusinessEvents] DROP CONSTRAINT [DF_tblSophisBusinessEvents_ExistingInstrument]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblSectorLimit_SectorDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblSectorLimit] DROP CONSTRAINT [DF_tblSectorLimit_SectorDateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblSectorLimit_UserName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblSectorLimit] DROP CONSTRAINT [DF_tblSectorLimit_UserName]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataLimits_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataLimits] DROP CONSTRAINT [DF_tblRiskDataLimits_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataLimits_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataLimits] DROP CONSTRAINT [DF_tblRiskDataLimits_UserEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataLimits_isPercent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataLimits] DROP CONSTRAINT [DF_tblRiskDataLimits_isPercent]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataLimits_LimitLevel]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataLimits] DROP CONSTRAINT [DF_tblRiskDataLimits_LimitLevel]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataLimits_GreaterOrLessThan]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataLimits] DROP CONSTRAINT [DF_tblRiskDataLimits_GreaterOrLessThan]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataLimits_Granularity]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataLimits] DROP CONSTRAINT [DF_tblRiskDataLimits_Granularity]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataLimits_InstrumentID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataLimits] DROP CONSTRAINT [DF_tblRiskDataLimits_InstrumentID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataLimits_FundID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataLimits] DROP CONSTRAINT [DF_tblRiskDataLimits_FundID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataLimits_LimitDescription]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataLimits] DROP CONSTRAINT [DF_tblRiskDataLimits_LimitDescription]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataCompoundCharacteristic_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataCompoundCharacteristic] DROP CONSTRAINT [DF_tblRiskDataCompoundCharacteristic_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataCompoundCharacteristic_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataCompoundCharacteristic] DROP CONSTRAINT [DF_tblRiskDataCompoundCharacteristic_UserEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataCompoundCharacteristic_ChildCharacteristicID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataCompoundCharacteristic] DROP CONSTRAINT [DF_tblRiskDataCompoundCharacteristic_ChildCharacteristicID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataCompoundCharacteristic_ParentCharacteristicID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataCompoundCharacteristic] DROP CONSTRAINT [DF_tblRiskDataCompoundCharacteristic_ParentCharacteristicID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataCompoundCharacteristic_CompoundID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataCompoundCharacteristic] DROP CONSTRAINT [DF_tblRiskDataCompoundCharacteristic_CompoundID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataCharacteristicTypes_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataCharacteristicTypes] DROP CONSTRAINT [DF_tblRiskDataCharacteristicTypes_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataCharacteristicTypes_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataCharacteristicTypes] DROP CONSTRAINT [DF_tblRiskDataCharacteristicTypes_UserEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataCharacteristicTypes_DataCharacteristicType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataCharacteristicTypes] DROP CONSTRAINT [DF_tblRiskDataCharacteristicTypes_DataCharacteristicType]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataCharacteristicTypes_DataCharacteristicTypeID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataCharacteristicTypes] DROP CONSTRAINT [DF_tblRiskDataCharacteristicTypes_DataCharacteristicTypeID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataCharacteristic_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataCharacteristic] DROP CONSTRAINT [DF_tblRiskDataCharacteristic_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataCharacteristic_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataCharacteristic] DROP CONSTRAINT [DF_tblRiskDataCharacteristic_UserEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataCharacteristic_CharacteristicType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataCharacteristic] DROP CONSTRAINT [DF_tblRiskDataCharacteristic_CharacteristicType]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataCharacteristic_DataCharacteristic]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataCharacteristic] DROP CONSTRAINT [DF_tblRiskDataCharacteristic_DataCharacteristic]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataCharacteristic_DataCharacteristicId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataCharacteristic] DROP CONSTRAINT [DF_tblRiskDataCharacteristic_DataCharacteristicId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataCharacteristic_DataCategoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataCharacteristic] DROP CONSTRAINT [DF_tblRiskDataCharacteristic_DataCategoryId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataCategory_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataCategory] DROP CONSTRAINT [DF_tblRiskDataCategory_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataCategory_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataCategory] DROP CONSTRAINT [DF_tblRiskDataCategory_UserEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskData_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskData] DROP CONSTRAINT [DF_tblRiskData_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskData_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskData] DROP CONSTRAINT [DF_tblRiskData_UserEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskData_DataWeighting]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskData] DROP CONSTRAINT [DF_tblRiskData_DataWeighting]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskData_RiskDataID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskData] DROP CONSTRAINT [DF_tblRiskData_RiskDataID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblReportDetailLog_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblReportDetailLog] DROP CONSTRAINT [DF_tblReportDetailLog_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblReportDetailLog_AppName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblReportDetailLog] DROP CONSTRAINT [DF_tblReportDetailLog_AppName]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblReportCustomisation_IsNumeric]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblReportCustomisation] DROP CONSTRAINT [DF_tblReportCustomisation_IsNumeric]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblReferentialIntegrity_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblReferentialIntegrity] DROP CONSTRAINT [DF_tblReferentialIntegrity_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblReferentialIntegrity_IsDescriptiveReferenceOnly]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblReferentialIntegrity] DROP CONSTRAINT [DF_tblReferentialIntegrity_IsDescriptiveReferenceOnly]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPrice_PriceDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPrice] DROP CONSTRAINT [DF_tblPrice_PriceDateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPrice_PriceUser]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPrice] DROP CONSTRAINT [DF_tblPrice_PriceUser]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPrice_PriceIsMilestone]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPrice] DROP CONSTRAINT [DF_tblPrice_PriceIsMilestone]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPrice_PriceIsAdministrator]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPrice] DROP CONSTRAINT [DF_tblPrice_PriceIsAdministrator]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPrice_PriceIsPercent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPrice] DROP CONSTRAINT [DF_tblPrice_PriceIsPercent]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPrice_PriceFinal]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPrice] DROP CONSTRAINT [DF_tblPrice_PriceFinal]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPrice_PriceGAV]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPrice] DROP CONSTRAINT [DF_tblPrice_PriceGAV]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPrice_PriceGNAV]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPrice] DROP CONSTRAINT [DF_tblPrice_PriceGNAV]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPrice_PriceBasicNAV]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPrice] DROP CONSTRAINT [DF_tblPrice_PriceBasicNAV]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPrice_PriceNAV]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPrice] DROP CONSTRAINT [DF_tblPrice_PriceNAV]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPrice_PriceMultiplier]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPrice] DROP CONSTRAINT [DF_tblPrice_PriceMultiplier]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPrice_PriceLevel]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPrice] DROP CONSTRAINT [DF_tblPrice_PriceLevel]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPrice_PricePercent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPrice] DROP CONSTRAINT [DF_tblPrice_PricePercent]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPrice_PriceInstrument]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPrice] DROP CONSTRAINT [DF_tblPrice_PriceInstrument]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPortfolioType_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPortfolioType] DROP CONSTRAINT [DF_tblPortfolioType_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPortfolioItemTypes_PortfolioItemTypeDescription]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPortfolioItemTypes] DROP CONSTRAINT [DF_tblPortfolioItemTypes_PortfolioItemTypeDescription]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPortfolioIndex_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPortfolioIndex] DROP CONSTRAINT [DF_tblPortfolioIndex_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPortfolioIndex_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPortfolioIndex] DROP CONSTRAINT [DF_tblPortfolioIndex_UserEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPortfolioIndex_InstrumentId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPortfolioIndex] DROP CONSTRAINT [DF_tblPortfolioIndex_InstrumentId]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPortfolioIndex_PortfolioDataID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPortfolioIndex] DROP CONSTRAINT [DF_tblPortfolioIndex_PortfolioDataID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPortfolioIndex_PortfolioFilingDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPortfolioIndex] DROP CONSTRAINT [DF_tblPortfolioIndex_PortfolioFilingDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPortfolioIndex_PortfolioType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPortfolioIndex] DROP CONSTRAINT [DF_tblPortfolioIndex_PortfolioType]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPortfolioIndex_PortfolioDescription]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPortfolioIndex] DROP CONSTRAINT [DF_tblPortfolioIndex_PortfolioDescription]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPortfolioIndex_PortfolioID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPortfolioIndex] DROP CONSTRAINT [DF_tblPortfolioIndex_PortfolioID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPortfolioData_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPortfolioData] DROP CONSTRAINT [DF_tblPortfolioData_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPortfolioData_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPortfolioData] DROP CONSTRAINT [DF_tblPortfolioData_UserEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPortfolioData_XMLData]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPortfolioData] DROP CONSTRAINT [DF_tblPortfolioData_XMLData]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPortfolioData_MonitorItem]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPortfolioData] DROP CONSTRAINT [DF_tblPortfolioData_MonitorItem]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPortfolioData_Weight]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPortfolioData] DROP CONSTRAINT [DF_tblPortfolioData_Weight]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPortfolioData_Holding]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPortfolioData] DROP CONSTRAINT [DF_tblPortfolioData_Holding]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPortfolioData_MarketInstrumentID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPortfolioData] DROP CONSTRAINT [DF_tblPortfolioData_MarketInstrumentID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPortfolioData_FilingDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPortfolioData] DROP CONSTRAINT [DF_tblPortfolioData_FilingDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPortfolioData_PortfolioItemID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPortfolioData] DROP CONSTRAINT [DF_tblPortfolioData_PortfolioItemID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCShareAllocation_CashInvested]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCShareAllocation] DROP CONSTRAINT [DF_tblPFPCShareAllocation_CashInvested]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCShareAllocation_IssuedShares]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCShareAllocation] DROP CONSTRAINT [DF_tblPFPCShareAllocation_IssuedShares]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCShareAllocation_TransactionType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCShareAllocation] DROP CONSTRAINT [DF_tblPFPCShareAllocation_TransactionType]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCShareAllocation_AccountID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCShareAllocation] DROP CONSTRAINT [DF_tblPFPCShareAllocation_AccountID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCShareAllocation_TradeDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCShareAllocation] DROP CONSTRAINT [DF_tblPFPCShareAllocation_TradeDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCShareAllocation_ReconciliationDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCShareAllocation] DROP CONSTRAINT [DF_tblPFPCShareAllocation_ReconciliationDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCShareAllocation_FundID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCShareAllocation] DROP CONSTRAINT [DF_tblPFPCShareAllocation_FundID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioValuation_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioValuation] DROP CONSTRAINT [DF_tblPFPCPortfolioValuation_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioValuation_BookCcyValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioValuation] DROP CONSTRAINT [DF_tblPFPCPortfolioValuation_BookCcyValue]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioValuation_LocalCcyValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioValuation] DROP CONSTRAINT [DF_tblPFPCPortfolioValuation_LocalCcyValue]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioValuation_FxRate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioValuation] DROP CONSTRAINT [DF_tblPFPCPortfolioValuation_FxRate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioValuation_LocalCcyPrice]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioValuation] DROP CONSTRAINT [DF_tblPFPCPortfolioValuation_LocalCcyPrice]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioValuation_Amount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioValuation] DROP CONSTRAINT [DF_tblPFPCPortfolioValuation_Amount]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioValuation_InstrumentCurrency]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioValuation] DROP CONSTRAINT [DF_tblPFPCPortfolioValuation_InstrumentCurrency]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioValuation_InstrumentDescription]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioValuation] DROP CONSTRAINT [DF_tblPFPCPortfolioValuation_InstrumentDescription]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioValuation_InstrumentID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioValuation] DROP CONSTRAINT [DF_tblPFPCPortfolioValuation_InstrumentID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioValuation_FundCurrency]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioValuation] DROP CONSTRAINT [DF_tblPFPCPortfolioValuation_FundCurrency]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioValuation_FundName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioValuation] DROP CONSTRAINT [DF_tblPFPCPortfolioValuation_FundName]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioValuation_FundID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioValuation] DROP CONSTRAINT [DF_tblPFPCPortfolioValuation_FundID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] DROP CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_BookCcyValue_Difference]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] DROP CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_BookCcyValue_Difference]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_PFPC_BookCcyValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] DROP CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_PFPC_BookCcyValue]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_FCP_BookCcyValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] DROP CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_FCP_BookCcyValue]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_LocalCcyValue_Difference]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] DROP CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_LocalCcyValue_Difference]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_PFPC_LocalCcyValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] DROP CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_PFPC_LocalCcyValue]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_FCP_LocalCcyValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] DROP CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_FCP_LocalCcyValue]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_FXRate_Difference]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] DROP CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_FXRate_Difference]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_PFPC_FXRate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] DROP CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_PFPC_FXRate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_FCP_FXRate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] DROP CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_FCP_FXRate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_Price_Difference]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] DROP CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_Price_Difference]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_PFPC_Price]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] DROP CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_PFPC_Price]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_FCP_PriceAdministrator]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] DROP CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_FCP_PriceAdministrator]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_FCP_PriceFinal]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] DROP CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_FCP_PriceFinal]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_FCP_Price]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] DROP CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_FCP_Price]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_Holding_Difference]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] DROP CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_Holding_Difference]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_PFPC_Holding]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] DROP CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_PFPC_Holding]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_FCP_HoldingCompleteness]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] DROP CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_FCP_HoldingCompleteness]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_FCP_Holding]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] DROP CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_FCP_Holding]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_PFPCInstrumentDescription]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] DROP CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_PFPCInstrumentDescription]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_PFPCInstrumentID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] DROP CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_PFPCInstrumentID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_FCPInstrumentID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] DROP CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_FCPInstrumentID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_FundID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] DROP CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_FundID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCCustodyRecord_Holding]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCCustodyRecord] DROP CONSTRAINT [DF_tblPFPCCustodyRecord_Holding]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCCustodyRecord_ShareClass]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCCustodyRecord] DROP CONSTRAINT [DF_tblPFPCCustodyRecord_ShareClass]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCCustodyRecord_SecurityID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCCustodyRecord] DROP CONSTRAINT [DF_tblPFPCCustodyRecord_SecurityID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCCustodyRecord_FundID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCCustodyRecord] DROP CONSTRAINT [DF_tblPFPCCustodyRecord_FundID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracInstrumentFlags_IsVenice]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracInstrumentFlags] DROP CONSTRAINT [DF_tblPertracInstrumentFlags_IsVenice]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracInstrumentFlags_IsSimulationFlag]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracInstrumentFlags] DROP CONSTRAINT [DF_tblPertracInstrumentFlags_IsSimulationFlag]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracInstrumentFlags_IsGroupFlag]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracInstrumentFlags] DROP CONSTRAINT [DF_tblPertracInstrumentFlags_IsGroupFlag]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracFieldMapping_Multiplier]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracFieldMapping] DROP CONSTRAINT [DF_tblPertracFieldMapping_Multiplier]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracFieldMapping_IsNumeric]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracFieldMapping] DROP CONSTRAINT [DF_tblPertracFieldMapping_IsNumeric]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracFieldMapping_ProviderFieldName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracFieldMapping] DROP CONSTRAINT [DF_tblPertracFieldMapping_ProviderFieldName]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracFieldMapping_PertracField]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracFieldMapping] DROP CONSTRAINT [DF_tblPertracFieldMapping_PertracField]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFields_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFields] DROP CONSTRAINT [DF_tblPertracCustomFields_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFields_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFields] DROP CONSTRAINT [DF_tblPertracCustomFields_UserEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFields_DefaultValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFields] DROP CONSTRAINT [DF_tblPertracCustomFields_DefaultValue]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFields_FieldIsOptimisable]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFields] DROP CONSTRAINT [DF_tblPertracCustomFields_FieldIsOptimisable]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFields_FieldIsSearchable]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFields] DROP CONSTRAINT [DF_tblPertracCustomFields_FieldIsSearchable]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFields_IsCalculated]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFields] DROP CONSTRAINT [DF_tblPertracCustomFields_IsCalculated]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFields_FieldIsVolatile]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFields] DROP CONSTRAINT [DF_tblPertracCustomFields_FieldIsVolatile]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFields_FieldPeriodCount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFields] DROP CONSTRAINT [DF_tblPertracCustomFields_FieldPeriodCount]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFields_IsIRR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFields] DROP CONSTRAINT [DF_tblPertracCustomFields_IsIRR]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFields_IsAverageValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFields] DROP CONSTRAINT [DF_tblPertracCustomFields_IsAverageValue]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFields_IsLastValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFields] DROP CONSTRAINT [DF_tblPertracCustomFields_IsLastValue]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFields_IsFirstValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFields] DROP CONSTRAINT [DF_tblPertracCustomFields_IsFirstValue]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFields_IsMinValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFields] DROP CONSTRAINT [DF_tblPertracCustomFields_IsMinValue]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFields_IsMaxValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFields] DROP CONSTRAINT [DF_tblPertracCustomFields_IsMaxValue]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFields_FieldName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFields] DROP CONSTRAINT [DF_tblPertracCustomFields_FieldName]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFieldData_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFieldData] DROP CONSTRAINT [DF_tblPertracCustomFieldData_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFieldData_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFieldData] DROP CONSTRAINT [DF_tblPertracCustomFieldData_UserEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFieldData_UpdateRequired]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFieldData] DROP CONSTRAINT [DF_tblPertracCustomFieldData_UpdateRequired]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFieldData_FieldBooleandData]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFieldData] DROP CONSTRAINT [DF_tblPertracCustomFieldData_FieldBooleandData]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFieldData_FieldDateData]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFieldData] DROP CONSTRAINT [DF_tblPertracCustomFieldData_FieldDateData]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFieldData_FieldTextData]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFieldData] DROP CONSTRAINT [DF_tblPertracCustomFieldData_FieldTextData]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFieldData_FieldNumericData]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFieldData] DROP CONSTRAINT [DF_tblPertracCustomFieldData_FieldNumericData]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFieldData_GroupID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFieldData] DROP CONSTRAINT [DF_tblPertracCustomFieldData_GroupID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPerson_PersonDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPerson] DROP CONSTRAINT [DF_tblPerson_PersonDateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPerson_PersonUser]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPerson] DROP CONSTRAINT [DF_tblPerson_PersonUser]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPerson_PersonEmail]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPerson] DROP CONSTRAINT [DF_tblPerson_PersonEmail]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPeriod_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPeriod] DROP CONSTRAINT [DF_tblPeriod_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPeriod_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPeriod] DROP CONSTRAINT [DF_tblPeriod_UserEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPeriod_IsGlobal]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPeriod] DROP CONSTRAINT [DF_tblPeriod_IsGlobal]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPeriod_AppName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPeriod] DROP CONSTRAINT [DF_tblPeriod_AppName]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionCompoundID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] DROP CONSTRAINT [DF_tblPendingTransactions_TransactionCompoundID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionFLAGS]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] DROP CONSTRAINT [DF_tblPendingTransactions_TransactionFLAGS]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionAdminStatus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] DROP CONSTRAINT [DF_tblPendingTransactions_TransactionAdminStatus]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionGroup]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] DROP CONSTRAINT [DF_tblPendingTransactions_TransactionGroup]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionUser]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] DROP CONSTRAINT [DF_tblPendingTransactions_TransactionUser]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionComment]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] DROP CONSTRAINT [DF_tblPendingTransactions_TransactionComment]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionTradeStatusID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] DROP CONSTRAINT [DF_tblPendingTransactions_TransactionTradeStatusID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionSpecificInitialEqualisationFlag]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] DROP CONSTRAINT [DF_tblPendingTransactions_TransactionSpecificInitialEqualisationFlag]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionExemptFromUpdate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] DROP CONSTRAINT [DF_tblPendingTransactions_TransactionExemptFromUpdate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionIsTransfer]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] DROP CONSTRAINT [DF_tblPendingTransactions_TransactionIsTransfer]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionCostIsPercent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] DROP CONSTRAINT [DF_tblPendingTransactions_TransactionCostIsPercent]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionDataEntry]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] DROP CONSTRAINT [DF_tblPendingTransactions_TransactionDataEntry]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionExecution]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] DROP CONSTRAINT [DF_tblPendingTransactions_TransactionExecution]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionInvestment]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] DROP CONSTRAINT [DF_tblPendingTransactions_TransactionInvestment]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionNewCounterparty]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] DROP CONSTRAINT [DF_tblPendingTransactions_TransactionNewCounterparty]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionSpecificInitialEqualisationValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] DROP CONSTRAINT [DF_tblPendingTransactions_TransactionSpecificInitialEqualisationValue]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionEffectiveWatermark]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] DROP CONSTRAINT [DF_tblPendingTransactions_TransactionEffectiveWatermark]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionEffectivePrice]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] DROP CONSTRAINT [DF_tblPendingTransactions_TransactionEffectivePrice]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionFXRate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] DROP CONSTRAINT [DF_tblPendingTransactions_TransactionFXRate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionMainFundPrice]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] DROP CONSTRAINT [DF_tblPendingTransactions_TransactionMainFundPrice]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionCostPercent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] DROP CONSTRAINT [DF_tblPendingTransactions_TransactionCostPercent]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionCosts]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] DROP CONSTRAINT [DF_tblPendingTransactions_TransactionCosts]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionPrice]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] DROP CONSTRAINT [DF_tblPendingTransactions_TransactionPrice]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionUnits]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] DROP CONSTRAINT [DF_tblPendingTransactions_TransactionUnits]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionValueorAmount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] DROP CONSTRAINT [DF_tblPendingTransactions_TransactionValueorAmount]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] DROP CONSTRAINT [DF_tblPendingTransactions_TransactionType]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionInstrument]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] DROP CONSTRAINT [DF_tblPendingTransactions_TransactionInstrument]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionSubFundValueFlag]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] DROP CONSTRAINT [DF_tblPendingTransactions_TransactionSubFundValueFlag]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionFund]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] DROP CONSTRAINT [DF_tblPendingTransactions_TransactionFund]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionTicket]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] DROP CONSTRAINT [DF_tblPendingTransactions_TransactionTicket]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTradeFileEntries_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTradeFileEntries] DROP CONSTRAINT [DF_tblPendingTradeFileEntries_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTradeFileEntries_UserName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTradeFileEntries] DROP CONSTRAINT [DF_tblPendingTradeFileEntries_UserName]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTradeFileEntries_TradefileText]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTradeFileEntries] DROP CONSTRAINT [DF_tblPendingTradeFileEntries_TradefileText]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTradeFileEntries_WorkflowID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTradeFileEntries] DROP CONSTRAINT [DF_tblPendingTradeFileEntries_WorkflowID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTradeFileEntries_BrokerID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTradeFileEntries] DROP CONSTRAINT [DF_tblPendingTradeFileEntries_BrokerID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingDataTaskEmails_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingDataTaskEmails] DROP CONSTRAINT [DF_tblPendingDataTaskEmails_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingDataTaskEmails_DataTaskID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingDataTaskEmails] DROP CONSTRAINT [DF_tblPendingDataTaskEmails_DataTaskID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMonthlyComment_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMonthlyComment] DROP CONSTRAINT [DF_tblMonthlyComment_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMonthlyComment_CommentText]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMonthlyComment] DROP CONSTRAINT [DF_tblMonthlyComment_CommentText]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMonthlyComment_FundID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMonthlyComment] DROP CONSTRAINT [DF_tblMonthlyComment_FundID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeetingStatus_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeetingStatus] DROP CONSTRAINT [DF_tblMeetingStatus_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeetingPeople_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeetingPeople] DROP CONSTRAINT [DF_tblMeetingPeople_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__tblPeople__Peopl__398D8EEE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeetingPeople] DROP CONSTRAINT [DF__tblPeople__Peopl__398D8EEE]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeetingPeople_PeoplePerson]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeetingPeople] DROP CONSTRAINT [DF_tblMeetingPeople_PeoplePerson]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] DROP CONSTRAINT [DF_tblMeeting_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] DROP CONSTRAINT [DF_tblMeeting_UserEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__tblMeetin__Meeti__4BAC3F29]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] DROP CONSTRAINT [DF__tblMeetin__Meeti__4BAC3F29]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__tblMeetin__Meeti__4AB81AF0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] DROP CONSTRAINT [DF__tblMeetin__Meeti__4AB81AF0]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__tblMeetin__Meeti__49C3F6B7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] DROP CONSTRAINT [DF__tblMeetin__Meeti__49C3F6B7]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingFundPerson5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] DROP CONSTRAINT [DF_tblMeeting_MeetingFundPerson5]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingFundPerson4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] DROP CONSTRAINT [DF_tblMeeting_MeetingFundPerson4]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingFundPerson3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] DROP CONSTRAINT [DF_tblMeeting_MeetingFundPerson3]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingFundPerson2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] DROP CONSTRAINT [DF_tblMeeting_MeetingFundPerson2]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingFundPerson1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] DROP CONSTRAINT [DF_tblMeeting_MeetingFundPerson1]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingFCPerson5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] DROP CONSTRAINT [DF_tblMeeting_MeetingFCPerson5]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingFCPerson4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] DROP CONSTRAINT [DF_tblMeeting_MeetingFCPerson4]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingFCPerson3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] DROP CONSTRAINT [DF_tblMeeting_MeetingFCPerson3]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingFCPerson2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] DROP CONSTRAINT [DF_tblMeeting_MeetingFCPerson2]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingFCPerson1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] DROP CONSTRAINT [DF_tblMeeting_MeetingFCPerson1]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingFundID5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] DROP CONSTRAINT [DF_tblMeeting_MeetingFundID5]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingFundID4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] DROP CONSTRAINT [DF_tblMeeting_MeetingFundID4]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingFundID3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] DROP CONSTRAINT [DF_tblMeeting_MeetingFundID3]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingFundID2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] DROP CONSTRAINT [DF_tblMeeting_MeetingFundID2]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingFundID1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] DROP CONSTRAINT [DF_tblMeeting_MeetingFundID1]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingLocation]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] DROP CONSTRAINT [DF_tblMeeting_MeetingLocation]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingManagementCompanyID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] DROP CONSTRAINT [DF_tblMeeting_MeetingManagementCompanyID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingWisdom]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] DROP CONSTRAINT [DF_tblMeeting_MeetingWisdom]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingBad]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] DROP CONSTRAINT [DF_tblMeeting_MeetingBad]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingGood]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] DROP CONSTRAINT [DF_tblMeeting_MeetingGood]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingPlace]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] DROP CONSTRAINT [DF_tblMeeting_MeetingPlace]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] DROP CONSTRAINT [DF_tblMeeting_MeetingDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingContact]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] DROP CONSTRAINT [DF_tblMeeting_MeetingContact]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingTitle]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] DROP CONSTRAINT [DF_tblMeeting_MeetingTitle]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMarketPriceTypes_TypeDescription]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMarketPriceTypes] DROP CONSTRAINT [DF_tblMarketPriceTypes_TypeDescription]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMarketPriceTypes_FieldName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMarketPriceTypes] DROP CONSTRAINT [DF_tblMarketPriceTypes_FieldName]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMarketPrices_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMarketPrices] DROP CONSTRAINT [DF_tblMarketPrices_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMarketPrices_DataType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMarketPrices] DROP CONSTRAINT [DF_tblMarketPrices_DataType]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMarketInstruments_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMarketInstruments] DROP CONSTRAINT [DF_tblMarketInstruments_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMarketInstruments_InstrumentCurrencyID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMarketInstruments] DROP CONSTRAINT [DF_tblMarketInstruments_InstrumentCurrencyID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMarketInstruments_InstrumentType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMarketInstruments] DROP CONSTRAINT [DF_tblMarketInstruments_InstrumentType]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMarketInstruments_Description]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMarketInstruments] DROP CONSTRAINT [DF_tblMarketInstruments_Description]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMarketInstruments_Ticker]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMarketInstruments] DROP CONSTRAINT [DF_tblMarketInstruments_Ticker]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblManagementCompany_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblManagementCompany] DROP CONSTRAINT [DF_tblManagementCompany_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblManagementCompany_FirmPrimaryLocation]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblManagementCompany] DROP CONSTRAINT [DF_tblManagementCompany_FirmPrimaryLocation]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblLiquidityLimit_LiquidityDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblLiquidityLimit] DROP CONSTRAINT [DF_tblLiquidityLimit_LiquidityDateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblLimitType_ltpDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblLimitType] DROP CONSTRAINT [DF_tblLimitType_ltpDateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblLimitType_ltpLimitFormat]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblLimitType] DROP CONSTRAINT [DF_tblLimitType_ltpLimitFormat]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblLimits_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblLimits] DROP CONSTRAINT [DF_tblLimits_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblLimits_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblLimits] DROP CONSTRAINT [DF_tblLimits_UserEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblLimits_limIsPercent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblLimits] DROP CONSTRAINT [DF_tblLimits_limIsPercent]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblLimits_limDealingPeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblLimits] DROP CONSTRAINT [DF_tblLimits_limDealingPeriod]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblLimits_limSectorGroup]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblLimits] DROP CONSTRAINT [DF_tblLimits_limSectorGroup]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblLimits_limCharacteristic]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblLimits] DROP CONSTRAINT [DF_tblLimits_limCharacteristic]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblLimits_limOnPremium]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblLimits] DROP CONSTRAINT [DF_tblLimits_limOnPremium]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblLimits_limDescription]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblLimits] DROP CONSTRAINT [DF_tblLimits_limDescription]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblJob_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblJob] DROP CONSTRAINT [DF_tblJob_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__tblJob__JobCurre__5EBF139D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblJob] DROP CONSTRAINT [DF__tblJob__JobCurre__5EBF139D]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__tblJob__JobTo__5DCAEF64]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblJob] DROP CONSTRAINT [DF__tblJob__JobTo__5DCAEF64]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__tblJob__JobFrom__5CD6CB2B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblJob] DROP CONSTRAINT [DF__tblJob__JobFrom__5CD6CB2B]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__tblJob__JobLocat__5BE2A6F2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblJob] DROP CONSTRAINT [DF__tblJob__JobLocat__5BE2A6F2]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__tblJob__JobFirm__5AEE82B9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblJob] DROP CONSTRAINT [DF__tblJob__JobFirm__5AEE82B9]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__tblJob__JobPerso__59FA5E80]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblJob] DROP CONSTRAINT [DF__tblJob__JobPerso__59FA5E80]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInvestorGroup_IGDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInvestorGroup] DROP CONSTRAINT [DF_tblInvestorGroup_IGDateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInvestorGroup_IGUser]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInvestorGroup] DROP CONSTRAINT [DF_tblInvestorGroup_IGUser]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrumentType_InstrumentTypeDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrumentType] DROP CONSTRAINT [DF_tblInstrumentType_InstrumentTypeDateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrumentType_InstrumentTypeUser]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrumentType] DROP CONSTRAINT [DF_tblInstrumentType_InstrumentTypeUser]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrumentType_InstrumentTypeIsExpense]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrumentType] DROP CONSTRAINT [DF_tblInstrumentType_InstrumentTypeIsExpense]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrumentType_InstrumentTypeHasDelta]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrumentType] DROP CONSTRAINT [DF_tblInstrumentType_InstrumentTypeHasDelta]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrumentType_InstrumentTypeFuturesStylePricing]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrumentType] DROP CONSTRAINT [DF_tblInstrumentType_InstrumentTypeFuturesStylePricing]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrumentType_InstrumentTypeTreatAs]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrumentType] DROP CONSTRAINT [DF_tblInstrumentType_InstrumentTypeTreatAs]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrumentFlorenceEntityLink_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrumentFlorenceEntityLink] DROP CONSTRAINT [DF_tblInstrumentFlorenceEntityLink_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrumentFlorenceEntityLink_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrumentFlorenceEntityLink] DROP CONSTRAINT [DF_tblInstrumentFlorenceEntityLink_UserEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrumentFlorenceEntityLink_EntityID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrumentFlorenceEntityLink] DROP CONSTRAINT [DF_tblInstrumentFlorenceEntityLink_EntityID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrumentFlorenceEntityLink_InstrumentID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrumentFlorenceEntityLink] DROP CONSTRAINT [DF_tblInstrumentFlorenceEntityLink_InstrumentID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrumentFlorenceEntityLink_LinkID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrumentFlorenceEntityLink] DROP CONSTRAINT [DF_tblInstrumentFlorenceEntityLink_LinkID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentDateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentUser]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentUser]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentReviewGroup]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentReviewGroup]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentCapturePrice]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentCapturePrice]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentPortfolioTicker]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentPortfolioTicker]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentMorningstar]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentMorningstar]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentSEDOL]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentSEDOL]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentISIN]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentISIN]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentExchangeCode]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentExchangeCode]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentLeverageCounterparty]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentLeverageCounterparty]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentIsLeverageInstrument]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentIsLeverageInstrument]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_IsntrumentIsEqualisation]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_IsntrumentIsEqualisation]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentExcludeGNAV]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentExcludeGNAV]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentIsManagementFee]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentIsManagementFee]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentExcludeAUM]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentExcludeAUM]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentExcludeGAV]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentExcludeGAV]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentUpfrontfees]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentUpfrontfees]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentPenaltyComment]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentPenaltyComment]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentPenalty]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentPenalty]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentExpiryDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentExpiryDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentStrike]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentStrike]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentLastValidTradeDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentLastValidTradeDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentLockupPeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentLockupPeriod]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentDataPeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentDataPeriod]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentCalendar]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentCalendar]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentDealingCutOffTime]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentDealingCutOffTime]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentDefaultSettlementDays_Sell]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentDefaultSettlementDays_Sell]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentDefaultSettlementDays_Buy]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentDefaultSettlementDays_Buy]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentNoticeDays]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentNoticeDays]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentWorstCase]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentWorstCase]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentExpectedReturn]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentExpectedReturn]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentMultiplier]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentMultiplier]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentContractSize]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentContractSize]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentBenchmark]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentBenchmark]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentUnderlying]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentUnderlying]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentCurrencyID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentCurrencyID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentClass]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentClass]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentParentEquivalentRatio]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentParentEquivalentRatio]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentParent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentParent]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentPFPCID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentPFPCID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentCode]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] DROP CONSTRAINT [DF_tblInstrument_InstrumentCode]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblIndexMapping_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblIndexMapping] DROP CONSTRAINT [DF_tblIndexMapping_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblIndexMapping_MappingEndDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblIndexMapping] DROP CONSTRAINT [DF_tblIndexMapping_MappingEndDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblIndexMapping_MappingStartDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblIndexMapping] DROP CONSTRAINT [DF_tblIndexMapping_MappingStartDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] DROP CONSTRAINT [DF_tblHistoricFundValuations_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_ReturnsSet]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] DROP CONSTRAINT [DF_tblHistoricFundValuations_ReturnsSet]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_PreviousFundWeightPercent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] DROP CONSTRAINT [DF_tblHistoricFundValuations_PreviousFundWeightPercent]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_BenchmarkModifiedBasisPoints]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] DROP CONSTRAINT [DF_tblHistoricFundValuations_BenchmarkModifiedBasisPoints]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_BenchmarkBasisPoints]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] DROP CONSTRAINT [DF_tblHistoricFundValuations_BenchmarkBasisPoints]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_InstrumentModifiedBasisPoints]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] DROP CONSTRAINT [DF_tblHistoricFundValuations_InstrumentModifiedBasisPoints]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_InstrumentBasisPoints]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] DROP CONSTRAINT [DF_tblHistoricFundValuations_InstrumentBasisPoints]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_BenchmarkModifiedReturn]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] DROP CONSTRAINT [DF_tblHistoricFundValuations_BenchmarkModifiedReturn]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_BenchmarkReturn]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] DROP CONSTRAINT [DF_tblHistoricFundValuations_BenchmarkReturn]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_InstrumentModifedReturn]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] DROP CONSTRAINT [DF_tblHistoricFundValuations_InstrumentModifedReturn]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_InstrumentReturn]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] DROP CONSTRAINT [DF_tblHistoricFundValuations_InstrumentReturn]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_FundWeightPercent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] DROP CONSTRAINT [DF_tblHistoricFundValuations_FundWeightPercent]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_ConsiderationValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] DROP CONSTRAINT [DF_tblHistoricFundValuations_ConsiderationValue]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_LocalValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] DROP CONSTRAINT [DF_tblHistoricFundValuations_LocalValue]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_USDValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] DROP CONSTRAINT [DF_tblHistoricFundValuations_USDValue]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_Value]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] DROP CONSTRAINT [DF_tblHistoricFundValuations_Value]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_CompoundFXRate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] DROP CONSTRAINT [DF_tblHistoricFundValuations_CompoundFXRate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_InstrumentFXRate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] DROP CONSTRAINT [DF_tblHistoricFundValuations_InstrumentFXRate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_FundFXRate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] DROP CONSTRAINT [DF_tblHistoricFundValuations_FundFXRate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_PriceMultiplier]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] DROP CONSTRAINT [DF_tblHistoricFundValuations_PriceMultiplier]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_PriceLevel]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] DROP CONSTRAINT [DF_tblHistoricFundValuations_PriceLevel]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_PriceNAV]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] DROP CONSTRAINT [DF_tblHistoricFundValuations_PriceNAV]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_PriceDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] DROP CONSTRAINT [DF_tblHistoricFundValuations_PriceDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_SignedSettlement]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] DROP CONSTRAINT [DF_tblHistoricFundValuations_SignedSettlement]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_SignedUnits]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] DROP CONSTRAINT [DF_tblHistoricFundValuations_SignedUnits]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_InstrumentMultiplier]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] DROP CONSTRAINT [DF_tblHistoricFundValuations_InstrumentMultiplier]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_InstrumentContractSize]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] DROP CONSTRAINT [DF_tblHistoricFundValuations_InstrumentContractSize]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupList_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupList] DROP CONSTRAINT [DF_tblGroupList_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupList_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupList] DROP CONSTRAINT [DF_tblGroupList_UserEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupList_GroupPricingPeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupList] DROP CONSTRAINT [DF_tblGroupList_GroupPricingPeriod]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupList_DefaultCovarianceMatrix]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupList] DROP CONSTRAINT [DF_tblGroupList_DefaultCovarianceMatrix]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupList_DefaultConstraintGroup]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupList] DROP CONSTRAINT [DF_tblGroupList_DefaultConstraintGroup]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupList_GroupDateTo]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupList] DROP CONSTRAINT [DF_tblGroupList_GroupDateTo]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupList_GroupDateFrom]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupList] DROP CONSTRAINT [DF_tblGroupList_GroupDateFrom]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupList_GroupGroup]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupList] DROP CONSTRAINT [DF_tblGroupList_GroupGroup]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupList_GroupListName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupList] DROP CONSTRAINT [DF_tblGroupList_GroupListName]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItems_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItems] DROP CONSTRAINT [DF_tblGroupItems_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItems_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItems] DROP CONSTRAINT [DF_tblGroupItems_UserEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItems_GroupSector]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItems] DROP CONSTRAINT [DF_tblGroupItems_GroupSector]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItems_GroupItemID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItems] DROP CONSTRAINT [DF_tblGroupItems_GroupItemID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItemData_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItemData] DROP CONSTRAINT [DF_tblGroupItemData_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItemData_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItemData] DROP CONSTRAINT [DF_tblGroupItemData_UserEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItemData_GroupScalingFactor]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItemData] DROP CONSTRAINT [DF_tblGroupItemData_GroupScalingFactor]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItemData_GroupStdErr]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItemData] DROP CONSTRAINT [DF_tblGroupItemData_GroupStdErr]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItemData_GroupAlphaE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItemData] DROP CONSTRAINT [DF_tblGroupItemData_GroupAlphaE]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItemData_GroupBetaE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItemData] DROP CONSTRAINT [DF_tblGroupItemData_GroupBetaE]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItemData_GroupIndexE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItemData] DROP CONSTRAINT [DF_tblGroupItemData_GroupIndexE]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItemData_GroupAlpha]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItemData] DROP CONSTRAINT [DF_tblGroupItemData_GroupAlpha]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItemData_GroupBeta]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItemData] DROP CONSTRAINT [DF_tblGroupItemData_GroupBeta]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItemData_GroupTradeSize]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItemData] DROP CONSTRAINT [DF_tblGroupItemData_GroupTradeSize]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItemData_GroupCap]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItemData] DROP CONSTRAINT [DF_tblGroupItemData_GroupCap]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItemData_GroupFloor]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItemData] DROP CONSTRAINT [DF_tblGroupItemData_GroupFloor]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItemData_GroupUpperBound]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItemData] DROP CONSTRAINT [DF_tblGroupItemData_GroupUpperBound]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItemData_GroupExpectedReturn]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItemData] DROP CONSTRAINT [DF_tblGroupItemData_GroupExpectedReturn]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItemData_GroupPercent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItemData] DROP CONSTRAINT [DF_tblGroupItemData_GroupPercent]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItemData_GroupNewHolding]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItemData] DROP CONSTRAINT [DF_tblGroupItemData_GroupNewHolding]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItemData_GroupHolding]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItemData] DROP CONSTRAINT [DF_tblGroupItemData_GroupHolding]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItemData_GroupLiquidity]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItemData] DROP CONSTRAINT [DF_tblGroupItemData_GroupLiquidity]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItemData_GroupPertracCode]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItemData] DROP CONSTRAINT [DF_tblGroupItemData_GroupPertracCode]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGreeks_GreekDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGreeks] DROP CONSTRAINT [DF_tblGreeks_GreekDateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGreeks_GreekUser]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGreeks] DROP CONSTRAINT [DF_tblGreeks_GreekUser]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGreeks_Volatility]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGreeks] DROP CONSTRAINT [DF_tblGreeks_Volatility]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGreeks_Theta]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGreeks] DROP CONSTRAINT [DF_tblGreeks_Theta]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGreeks_Vega]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGreeks] DROP CONSTRAINT [DF_tblGreeks_Vega]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGreeks_Rho]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGreeks] DROP CONSTRAINT [DF_tblGreeks_Rho]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGreeks_Gamma]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGreeks] DROP CONSTRAINT [DF_tblGreeks_Gamma]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGreeks_Delta]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGreeks] DROP CONSTRAINT [DF_tblGreeks_Delta]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGreeks_InstrumentID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGreeks] DROP CONSTRAINT [DF_tblGreeks_InstrumentID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingScenarios_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingScenarios] DROP CONSTRAINT [DF_tblGLPostingScenarios_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingScenarios_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingScenarios] DROP CONSTRAINT [DF_tblGLPostingScenarios_UserEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingScenarios_PostingValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingScenarios] DROP CONSTRAINT [DF_tblGLPostingScenarios_PostingValue]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingScenarios_PostingValue_Enum]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingScenarios] DROP CONSTRAINT [DF_tblGLPostingScenarios_PostingValue_Enum]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingScenarios_Category6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingScenarios] DROP CONSTRAINT [DF_tblGLPostingScenarios_Category6]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingScenarios_Category6_Enum]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingScenarios] DROP CONSTRAINT [DF_tblGLPostingScenarios_Category6_Enum]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingScenarios_Category5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingScenarios] DROP CONSTRAINT [DF_tblGLPostingScenarios_Category5]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingScenarios_Category5_Enum]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingScenarios] DROP CONSTRAINT [DF_tblGLPostingScenarios_Category5_Enum]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingScenarios_Category4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingScenarios] DROP CONSTRAINT [DF_tblGLPostingScenarios_Category4]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingScenarios_Category4_Enum]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingScenarios] DROP CONSTRAINT [DF_tblGLPostingScenarios_Category4_Enum]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingScenarios_Category3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingScenarios] DROP CONSTRAINT [DF_tblGLPostingScenarios_Category3]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingScenarios_Category3_Enum]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingScenarios] DROP CONSTRAINT [DF_tblGLPostingScenarios_Category3_Enum]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingScenarios_Category2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingScenarios] DROP CONSTRAINT [DF_tblGLPostingScenarios_Category2]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingScenarios_Category2_Enum]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingScenarios] DROP CONSTRAINT [DF_tblGLPostingScenarios_Category2_Enum]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingScenarios_Category1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingScenarios] DROP CONSTRAINT [DF_tblGLPostingScenarios_Category1]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingScenarios_Category1_Enum]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingScenarios] DROP CONSTRAINT [DF_tblGLPostingScenarios_Category1_Enum]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostings_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostings] DROP CONSTRAINT [DF_tblGLPostings_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostings_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostings] DROP CONSTRAINT [DF_tblGLPostings_UserEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostings_PeriodNumber]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostings] DROP CONSTRAINT [DF_tblGLPostings_PeriodNumber]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostings_PeriodYear]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostings] DROP CONSTRAINT [DF_tblGLPostings_PeriodYear]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostings_ClosingTransactionParentID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostings] DROP CONSTRAINT [DF_tblGLPostings_ClosingTransactionParentID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostings_PostingType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostings] DROP CONSTRAINT [DF_tblGLPostings_PostingType]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingRules_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingRules] DROP CONSTRAINT [DF_tblGLPostingRules_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingRules_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingRules] DROP CONSTRAINT [DF_tblGLPostingRules_UserEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingRules_MatchValueToPostingRuleID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingRules] DROP CONSTRAINT [DF_tblGLPostingRules_MatchValueToPostingRuleID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingRules_CREDIT_TransferOfExistingValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingRules] DROP CONSTRAINT [DF_tblGLPostingRules_CREDIT_TransferOfExistingValue]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingRules_DEBIT_TransferOfExistingValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingRules] DROP CONSTRAINT [DF_tblGLPostingRules_DEBIT_TransferOfExistingValue]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingRules_ScenarioKey]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingRules] DROP CONSTRAINT [DF_tblGLPostingRules_ScenarioKey]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGenoaSavedSearchList_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGenoaSavedSearchList] DROP CONSTRAINT [DF_tblGenoaSavedSearchList_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGenoaSavedSearchList_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGenoaSavedSearchList] DROP CONSTRAINT [DF_tblGenoaSavedSearchList_UserEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGenoaSavedSearchList_VisibleToAllUsers]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGenoaSavedSearchList] DROP CONSTRAINT [DF_tblGenoaSavedSearchList_VisibleToAllUsers]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGenoaSavedSearchItems_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGenoaSavedSearchItems] DROP CONSTRAINT [DF_tblGenoaSavedSearchItems_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGenoaSavedSearchItems_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGenoaSavedSearchItems] DROP CONSTRAINT [DF_tblGenoaSavedSearchItems_UserEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGenoaSavedSearchItems_IsOR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGenoaSavedSearchItems] DROP CONSTRAINT [DF_tblGenoaSavedSearchItems_IsOR]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGenoaSavedSearchItems_IsAND]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGenoaSavedSearchItems] DROP CONSTRAINT [DF_tblGenoaSavedSearchItems_IsAND]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGenoaSavedSearchItems_FieldIsCustom]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGenoaSavedSearchItems] DROP CONSTRAINT [DF_tblGenoaSavedSearchItems_FieldIsCustom]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGenoaSavedSearchItems_FieldIsStatic]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGenoaSavedSearchItems] DROP CONSTRAINT [DF_tblGenoaSavedSearchItems_FieldIsStatic]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGenoaConstraintList_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGenoaConstraintList] DROP CONSTRAINT [DF_tblGenoaConstraintList_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGenoaConstraintList_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGenoaConstraintList] DROP CONSTRAINT [DF_tblGenoaConstraintList_UserEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGenoaConstraintItems_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGenoaConstraintItems] DROP CONSTRAINT [DF_tblGenoaConstraintItems_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGenoaConstraintItems_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGenoaConstraintItems] DROP CONSTRAINT [DF_tblGenoaConstraintItems_UserEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGenoaConstraintItems_TestValueDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGenoaConstraintItems] DROP CONSTRAINT [DF_tblGenoaConstraintItems_TestValueDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGenoaConstraintItems_TestValueNumeric]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGenoaConstraintItems] DROP CONSTRAINT [DF_tblGenoaConstraintItems_TestValueNumeric]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGenoaConstraintItems_TestValueString]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGenoaConstraintItems] DROP CONSTRAINT [DF_tblGenoaConstraintItems_TestValueString]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGenoaConstraintItems_TestFieldGreaterThan]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGenoaConstraintItems] DROP CONSTRAINT [DF_tblGenoaConstraintItems_TestFieldGreaterThan]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGenoaConstraintItems_TestFieldLessEqualTo]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGenoaConstraintItems] DROP CONSTRAINT [DF_tblGenoaConstraintItems_TestFieldLessEqualTo]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGenoaConstraintItems_TestFieldLessThan]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGenoaConstraintItems] DROP CONSTRAINT [DF_tblGenoaConstraintItems_TestFieldLessThan]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFX_FXDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFX] DROP CONSTRAINT [DF_tblFX_FXDateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFX_FXUser]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFX] DROP CONSTRAINT [DF_tblFX_FXUser]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundType_FundTypeDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundType] DROP CONSTRAINT [DF_tblFundType_FundTypeDateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundType_FundTypeUser]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundType] DROP CONSTRAINT [DF_tblFundType_FundTypeUser]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundType_FundTypeShowInReporter]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundType] DROP CONSTRAINT [DF_tblFundType_FundTypeShowInReporter]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundType_FundTypeReporterGrouping]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundType] DROP CONSTRAINT [DF_tblFundType_FundTypeReporterGrouping]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundType_FundTypeIsAdministrativeOnly]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundType] DROP CONSTRAINT [DF_tblFundType_FundTypeIsAdministrativeOnly]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundType_FundTypeAttributionGrouping]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundType] DROP CONSTRAINT [DF_tblFundType_FundTypeAttributionGrouping]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundType_FundTypeSortOrder]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundType] DROP CONSTRAINT [DF_tblFundType_FundTypeSortOrder]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundType_FundTypeGroup]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundType] DROP CONSTRAINT [DF_tblFundType_FundTypeGroup]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundPerformanceFeePoints_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundPerformanceFeePoints] DROP CONSTRAINT [DF_tblFundPerformanceFeePoints_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundPerformanceFeePoints_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundPerformanceFeePoints] DROP CONSTRAINT [DF_tblFundPerformanceFeePoints_UserEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundPerformanceFeePoints_FeeFundPrice]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundPerformanceFeePoints] DROP CONSTRAINT [DF_tblFundPerformanceFeePoints_FeeFundPrice]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundPerformanceFeePoints_FeeFund]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundPerformanceFeePoints] DROP CONSTRAINT [DF_tblFundPerformanceFeePoints_FeeFund]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundPerformanceFeePoints_FeeID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundPerformanceFeePoints] DROP CONSTRAINT [DF_tblFundPerformanceFeePoints_FeeID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundMilestones_MilestoneDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundMilestones] DROP CONSTRAINT [DF_tblFundMilestones_MilestoneDateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundMilestones_MileStoneUser]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundMilestones] DROP CONSTRAINT [DF_tblFundMilestones_MileStoneUser]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundContactDetails_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundContactDetails] DROP CONSTRAINT [DF_tblFundContactDetails_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundContactDetails_InvestorRelationsEmail]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundContactDetails] DROP CONSTRAINT [DF_tblFundContactDetails_InvestorRelationsEmail]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundContactDetails_InvestorRelationsPhone]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundContactDetails] DROP CONSTRAINT [DF_tblFundContactDetails_InvestorRelationsPhone]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundContactDetails_InvestorRelationsContact]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundContactDetails] DROP CONSTRAINT [DF_tblFundContactDetails_InvestorRelationsContact]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundContactDetails_AdminSecondaryEmail]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundContactDetails] DROP CONSTRAINT [DF_tblFundContactDetails_AdminSecondaryEmail]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundContactDetails_AdminMainEmail]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundContactDetails] DROP CONSTRAINT [DF_tblFundContactDetails_AdminMainEmail]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundContactDetails_AdminFax]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundContactDetails] DROP CONSTRAINT [DF_tblFundContactDetails_AdminFax]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundContactDetails_AdminPhone]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundContactDetails] DROP CONSTRAINT [DF_tblFundContactDetails_AdminPhone]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundDateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_UserName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_UserName]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundClosed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundClosed]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundAdministratorCode]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundAdministratorCode]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundReutersCode]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundReutersCode]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundBloomberg]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundBloomberg]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundSEDOL]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundSEDOL]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundISIN]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundISIN]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundReviewGroup]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundReviewGroup]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundLinkedListedInstrument]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundLinkedListedInstrument]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundListings]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundListings]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundCustodian]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundCustodian]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundAdministrator]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundAdministrator]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundJurisdiction]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundJurisdiction]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundRedemptionNotice]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundRedemptionNotice]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundRedemptionPeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundRedemptionPeriod]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundSubscriptionNotice]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundSubscriptionNotice]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundSubscriptionPeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundSubscriptionPeriod]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundMinimumInvestment]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundMinimumInvestment]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundContactEmail]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundContactEmail]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundContactPhone]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundContactPhone]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundContactAddress]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundContactAddress]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundManagers]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundManagers]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundManagementCompany]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundManagementCompany]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundPerformanceFeeText]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundPerformanceFeeText]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundManagementFeeText]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundManagementFeeText]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundInitialSubscriptionFee]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundInitialSubscriptionFee]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundReportGroup]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundReportGroup]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundRedemptionSettlementDatePeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundRedemptionSettlementDatePeriod]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundRedemptionNAVDatePeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundRedemptionNAVDatePeriod]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundSubscriptionSettlementDatePeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundSubscriptionSettlementDatePeriod]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundSubscriptionNAVDatePeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundSubscriptionNAVDatePeriod]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundPerformanceFeesPaymentPeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundPerformanceFeesPaymentPeriod]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundPerformanceFeesAccruePeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundPerformanceFeesAccruePeriod]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundFeesPaymentPeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundFeesPaymentPeriod]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundFeesAccruePeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundFeesAccruePeriod]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundUsesEqualisation]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundUsesEqualisation]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundPerformanceHurdle]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundPerformanceHurdle]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundPerformanceFees]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundPerformanceFees]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundPerformanceVsBenchmark]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundPerformanceVsBenchmark]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundManagementFees]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundManagementFees]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundPerformanceFeeInstrument]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundPerformanceFeeInstrument]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundManagementFeeInstrument]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundManagementFeeInstrument]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundUser]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundUser]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundDefaultTradesToFundFX]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundDefaultTradesToFundFX]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__tblFund__FundPar__01142BA1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF__tblFund__FundPar__01142BA1]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundUnit]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundUnit]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundPricingPeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundPricingPeriod]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundLegalEntity]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] DROP CONSTRAINT [DF_tblFund_FundLegalEntity]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceStatusIDs_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceStatusIDs] DROP CONSTRAINT [DF_tblFlorenceStatusIDs_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceStatusIDs_ItemIsComplete]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceStatusIDs] DROP CONSTRAINT [DF_tblFlorenceStatusIDs_ItemIsComplete]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceStatus_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceStatus] DROP CONSTRAINT [DF_tblFlorenceStatus_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceStatus_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceStatus] DROP CONSTRAINT [DF_tblFlorenceStatus_UserEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceStatus_Comment]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceStatus] DROP CONSTRAINT [DF_tblFlorenceStatus_Comment]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceStatus_ItemStatus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceStatus] DROP CONSTRAINT [DF_tblFlorenceStatus_ItemStatus]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceItems_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceItems] DROP CONSTRAINT [DF_tblFlorenceItems_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceItems_LimitThreshold]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceItems] DROP CONSTRAINT [DF_tblFlorenceItems_LimitThreshold]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceItems_ToLimit]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceItems] DROP CONSTRAINT [DF_tblFlorenceItems_ToLimit]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceItems_FromLimit]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceItems] DROP CONSTRAINT [DF_tblFlorenceItems_FromLimit]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceItems_LimitIsPercent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceItems] DROP CONSTRAINT [DF_tblFlorenceItems_LimitIsPercent]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceItems_LimitType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceItems] DROP CONSTRAINT [DF_tblFlorenceItems_LimitType]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceItems_IsLimitItem]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceItems] DROP CONSTRAINT [DF_tblFlorenceItems_IsLimitItem]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceItems_UpdateStatusOnDataUpdate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceItems] DROP CONSTRAINT [DF_tblFlorenceItems_UpdateStatusOnDataUpdate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceItems_DataItemType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceItems] DROP CONSTRAINT [DF_tblFlorenceItems_DataItemType]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceItems_IsDataItem]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceItems] DROP CONSTRAINT [DF_tblFlorenceItems_IsDataItem]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceItems_KeyDetail]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceItems] DROP CONSTRAINT [DF_tblFlorenceItems_KeyDetail]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceItems_ItemDiscontinued]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceItems] DROP CONSTRAINT [DF_tblFlorenceItems_ItemDiscontinued]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceItems_ItemGroup]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceItems] DROP CONSTRAINT [DF_tblFlorenceItems_ItemGroup]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceItems_DataUpdatePeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceItems] DROP CONSTRAINT [DF_tblFlorenceItems_DataUpdatePeriod]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceItems_UpdatePeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceItems] DROP CONSTRAINT [DF_tblFlorenceItems_UpdatePeriod]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceGroup_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceGroup] DROP CONSTRAINT [DF_tblFlorenceGroup_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceGroup_LegacyGroup]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceGroup] DROP CONSTRAINT [DF_tblFlorenceGroup_LegacyGroup]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceEntity_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceEntity] DROP CONSTRAINT [DF_tblFlorenceEntity_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceEntity_DDStatusComment]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceEntity] DROP CONSTRAINT [DF_tblFlorenceEntity_DDStatusComment]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceEntity_DDStatus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceEntity] DROP CONSTRAINT [DF_tblFlorenceEntity_DDStatus]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceEntity_AdminFax]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceEntity] DROP CONSTRAINT [DF_tblFlorenceEntity_AdminFax]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceEntity_AdminEMail]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceEntity] DROP CONSTRAINT [DF_tblFlorenceEntity_AdminEMail]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceEntity_AdminPhone]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceEntity] DROP CONSTRAINT [DF_tblFlorenceEntity_AdminPhone]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceEntity_AdminEmailSalutation]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceEntity] DROP CONSTRAINT [DF_tblFlorenceEntity_AdminEmailSalutation]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceEntity_AdminContact]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceEntity] DROP CONSTRAINT [DF_tblFlorenceEntity_AdminContact]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceEntity_InstrumentID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceEntity] DROP CONSTRAINT [DF_tblFlorenceEntity_InstrumentID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceEntity_EntityIsInactive]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceEntity] DROP CONSTRAINT [DF_tblFlorenceEntity_EntityIsInactive]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceDataValues_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceDataValues] DROP CONSTRAINT [DF_tblFlorenceDataValues_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceDataValues_BooleanData]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceDataValues] DROP CONSTRAINT [DF_tblFlorenceDataValues_BooleanData]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblExposure_expFlags]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblExposure] DROP CONSTRAINT [DF_tblExposure_expFlags]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblExposure_expSystemKnowledgeDateWholeday]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblExposure] DROP CONSTRAINT [DF_tblExposure_expSystemKnowledgeDateWholeday]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblExposure_expSystemKnowledgeDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblExposure] DROP CONSTRAINT [DF_tblExposure_expSystemKnowledgeDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblExposure_expKnowledgeDateWholeday]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblExposure] DROP CONSTRAINT [DF_tblExposure_expKnowledgeDateWholeday]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblExposure_expLimitDealingPeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblExposure] DROP CONSTRAINT [DF_tblExposure_expLimitDealingPeriod]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblExposure_expLimitSectorGroup]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblExposure] DROP CONSTRAINT [DF_tblExposure_expLimitSectorGroup]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblExposure_expLimitSector]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblExposure] DROP CONSTRAINT [DF_tblExposure_expLimitSector]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblExposure_expLimitInstrument]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblExposure] DROP CONSTRAINT [DF_tblExposure_expLimitInstrument]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblExposure_expLimitCategory]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblExposure] DROP CONSTRAINT [DF_tblExposure_expLimitCategory]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblExposure_limID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblExposure] DROP CONSTRAINT [DF_tblExposure_limID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblExposure_expIsDetailRow]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblExposure] DROP CONSTRAINT [DF_tblExposure_expIsDetailRow]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblEstimate_EstimateDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblEstimate] DROP CONSTRAINT [DF_tblEstimate_EstimateDateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblEstimate_User]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblEstimate] DROP CONSTRAINT [DF_tblEstimate_User]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblEstimate_EstimateAmount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblEstimate] DROP CONSTRAINT [DF_tblEstimate_EstimateAmount]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblEstimate_EstimateDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblEstimate] DROP CONSTRAINT [DF_tblEstimate_EstimateDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblEstimate_EstimateItem]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblEstimate] DROP CONSTRAINT [DF_tblEstimate_EstimateItem]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblEstimate_EstimateFund]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblEstimate] DROP CONSTRAINT [DF_tblEstimate_EstimateFund]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDealingPeriod_DealingPeriodDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDealingPeriod] DROP CONSTRAINT [DF_tblDealingPeriod_DealingPeriodDateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataTaskType_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataTaskType] DROP CONSTRAINT [DF_tblDataTaskType_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataTaskType_TaskDescription]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataTaskType] DROP CONSTRAINT [DF_tblDataTaskType_TaskDescription]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataTaskType_TaskType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataTaskType] DROP CONSTRAINT [DF_tblDataTaskType_TaskType]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataTask_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataTask] DROP CONSTRAINT [DF_tblDataTask_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataTask_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataTask] DROP CONSTRAINT [DF_tblDataTask_UserEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataTask_TaskDefinition]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataTask] DROP CONSTRAINT [DF_tblDataTask_TaskDefinition]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataTask_SecurityTicker]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataTask] DROP CONSTRAINT [DF_tblDataTask_SecurityTicker]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataTask_InstrumentID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataTask] DROP CONSTRAINT [DF_tblDataTask_InstrumentID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataTask_TaskType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataTask] DROP CONSTRAINT [DF_tblDataTask_TaskType]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataTask_TaskDescription]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataTask] DROP CONSTRAINT [DF_tblDataTask_TaskDescription]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataTask_TaskID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataTask] DROP CONSTRAINT [DF_tblDataTask_TaskID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataDictionary_TransactionFormLabels_FieldText6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataDictionary_TransactionFormLabels] DROP CONSTRAINT [DF_tblDataDictionary_TransactionFormLabels_FieldText6]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataDictionary_TransactionFormLabels_FieldText5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataDictionary_TransactionFormLabels] DROP CONSTRAINT [DF_tblDataDictionary_TransactionFormLabels_FieldText5]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataDictionary_TransactionFormLabels_FieldText4_1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataDictionary_TransactionFormLabels] DROP CONSTRAINT [DF_tblDataDictionary_TransactionFormLabels_FieldText4_1]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataDictionary_TransactionFormLabels_FieldText3_1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataDictionary_TransactionFormLabels] DROP CONSTRAINT [DF_tblDataDictionary_TransactionFormLabels_FieldText3_1]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataDictionary_TransactionFormLabels_FieldText2_1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataDictionary_TransactionFormLabels] DROP CONSTRAINT [DF_tblDataDictionary_TransactionFormLabels_FieldText2_1]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataDictionary_TransactionFormLabels_FieldText1_1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataDictionary_TransactionFormLabels] DROP CONSTRAINT [DF_tblDataDictionary_TransactionFormLabels_FieldText1_1]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataDictionary_TransactionFormLabels_FieldText4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataDictionary_TransactionFormLabels] DROP CONSTRAINT [DF_tblDataDictionary_TransactionFormLabels_FieldText4]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataDictionary_TransactionFormLabels_FieldText3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataDictionary_TransactionFormLabels] DROP CONSTRAINT [DF_tblDataDictionary_TransactionFormLabels_FieldText3]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataDictionary_TransactionFormLabels_FieldText2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataDictionary_TransactionFormLabels] DROP CONSTRAINT [DF_tblDataDictionary_TransactionFormLabels_FieldText2]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataDictionary_TransactionFormLabels_FieldText1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataDictionary_TransactionFormLabels] DROP CONSTRAINT [DF_tblDataDictionary_TransactionFormLabels_FieldText1]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataDictionary_TransactionFormLabels_TransactionSettlement]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataDictionary_TransactionFormLabels] DROP CONSTRAINT [DF_tblDataDictionary_TransactionFormLabels_TransactionSettlement]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataDictionary_TransactionFormLabels_TransactionCosts]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataDictionary_TransactionFormLabels] DROP CONSTRAINT [DF_tblDataDictionary_TransactionFormLabels_TransactionCosts]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataDictionary_TransactionFormLabels_TransactionPrice]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataDictionary_TransactionFormLabels] DROP CONSTRAINT [DF_tblDataDictionary_TransactionFormLabels_TransactionPrice]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataDictionary_TransactionFormLabels_TransactionAmount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataDictionary_TransactionFormLabels] DROP CONSTRAINT [DF_tblDataDictionary_TransactionFormLabels_TransactionAmount]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataDictionary_TransactionFormLabels_CompoundFlag]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataDictionary_TransactionFormLabels] DROP CONSTRAINT [DF_tblDataDictionary_TransactionFormLabels_CompoundFlag]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataDictionary_TransactionFormLabels_CounterpartyText]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataDictionary_TransactionFormLabels] DROP CONSTRAINT [DF_tblDataDictionary_TransactionFormLabels_CounterpartyText]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataDictionary_TransactionFormLabels_TransactionType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataDictionary_TransactionFormLabels] DROP CONSTRAINT [DF_tblDataDictionary_TransactionFormLabels_TransactionType]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCurrency_CurrencyDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCurrency] DROP CONSTRAINT [DF_tblCurrency_CurrencyDateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCurrency_CurrencyUser]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCurrency] DROP CONSTRAINT [DF_tblCurrency_CurrencyUser]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCurrency_DefaultBrokerageExpense]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCurrency] DROP CONSTRAINT [DF_tblCurrency_DefaultBrokerageExpense]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCurrency_DefaultFeeInstrument]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCurrency] DROP CONSTRAINT [DF_tblCurrency_DefaultFeeInstrument]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCurrency_DefaultExpenseInstrument]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCurrency] DROP CONSTRAINT [DF_tblCurrency_DefaultExpenseInstrument]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCurrency_CurrencyNotionalInstrumentID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCurrency] DROP CONSTRAINT [DF_tblCurrency_CurrencyNotionalInstrumentID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCurrency_CurrencyInstrumentID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCurrency] DROP CONSTRAINT [DF_tblCurrency_CurrencyInstrumentID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCTA_Simulation_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCTA_Simulation] DROP CONSTRAINT [DF_tblCTA_Simulation_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCTA_Simulation_UserName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCTA_Simulation] DROP CONSTRAINT [DF_tblCTA_Simulation_UserName]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCTA_Simulation_SimulationDataPeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCTA_Simulation] DROP CONSTRAINT [DF_tblCTA_Simulation_SimulationDataPeriod]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCTA_Simulation_InterestRateID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCTA_Simulation] DROP CONSTRAINT [DF_tblCTA_Simulation_InterestRateID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCTA_Simulation_InstrumentID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCTA_Simulation] DROP CONSTRAINT [DF_tblCTA_Simulation_InstrumentID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCTA_Folders_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCTA_Folders] DROP CONSTRAINT [DF_tblCTA_Folders_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCTA_Folders_UserName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCTA_Folders] DROP CONSTRAINT [DF_tblCTA_Folders_UserName]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCovarianceList_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCovarianceList] DROP CONSTRAINT [DF_tblCovarianceList_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCovarianceList_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCovarianceList] DROP CONSTRAINT [DF_tblCovarianceList_UserEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCovarianceList_IsAnnualised]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCovarianceList] DROP CONSTRAINT [DF_tblCovarianceList_IsAnnualised]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCovarianceList_DataPeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCovarianceList] DROP CONSTRAINT [DF_tblCovarianceList_DataPeriod]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCovarianceList_CovListName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCovarianceList] DROP CONSTRAINT [DF_tblCovarianceList_CovListName]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCovariance_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCovariance] DROP CONSTRAINT [DF_tblCovariance_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCovariance_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCovariance] DROP CONSTRAINT [DF_tblCovariance_UserEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_CounterpartyDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] DROP CONSTRAINT [DF_tblCounterparty_CounterpartyDateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_CounterpartyUser]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] DROP CONSTRAINT [DF_tblCounterparty_CounterpartyUser]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_CounterpartyFaxNumber]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] DROP CONSTRAINT [DF_tblCounterparty_CounterpartyFaxNumber]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_CounterpartyPhoneNumber]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] DROP CONSTRAINT [DF_tblCounterparty_CounterpartyPhoneNumber]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_CounterpartyEMail]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] DROP CONSTRAINT [DF_tblCounterparty_CounterpartyEMail]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_CounterpartyAccountName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] DROP CONSTRAINT [DF_tblCounterparty_CounterpartyAccountName]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_CounterpartyContactName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] DROP CONSTRAINT [DF_tblCounterparty_CounterpartyContactName]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_CounterpartyPostCode]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] DROP CONSTRAINT [DF_tblCounterparty_CounterpartyPostCode]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_CounterpartyAddress5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] DROP CONSTRAINT [DF_tblCounterparty_CounterpartyAddress5]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_CounterpartyAddress4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] DROP CONSTRAINT [DF_tblCounterparty_CounterpartyAddress4]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_CounterpartyAddress3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] DROP CONSTRAINT [DF_tblCounterparty_CounterpartyAddress3]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_CounterpartyAddress2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] DROP CONSTRAINT [DF_tblCounterparty_CounterpartyAddress2]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_CounterpartyAddress1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] DROP CONSTRAINT [DF_tblCounterparty_CounterpartyAddress1]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_CounterpartyLeverageProvider]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] DROP CONSTRAINT [DF_tblCounterparty_CounterpartyLeverageProvider]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_CounterpartyFundID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] DROP CONSTRAINT [DF_tblCounterparty_CounterpartyFundID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_x8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] DROP CONSTRAINT [DF_tblCounterparty_x8]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_x7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] DROP CONSTRAINT [DF_tblCounterparty_x7]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_x6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] DROP CONSTRAINT [DF_tblCounterparty_x6]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_x5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] DROP CONSTRAINT [DF_tblCounterparty_x5]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_x4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] DROP CONSTRAINT [DF_tblCounterparty_x4]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_x3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] DROP CONSTRAINT [DF_tblCounterparty_x3]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_x2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] DROP CONSTRAINT [DF_tblCounterparty_x2]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_x1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] DROP CONSTRAINT [DF_tblCounterparty_x1]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__tblCounte__Count__04E4BC85]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] DROP CONSTRAINT [DF__tblCounte__Count__04E4BC85]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__tblCounte__Count__7C4F7684]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] DROP CONSTRAINT [DF__tblCounte__Count__7C4F7684]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__tblCounte__Count__7B5B524B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] DROP CONSTRAINT [DF__tblCounte__Count__7B5B524B]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_CounterpartyInvestorGroup]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] DROP CONSTRAINT [DF_tblCounterparty_CounterpartyInvestorGroup]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_CounterpartyPFPCID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] DROP CONSTRAINT [DF_tblCounterparty_CounterpartyPFPCID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] DROP CONSTRAINT [DF_tblCompoundTransactionTemplate_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] DROP CONSTRAINT [DF_tblCompoundTransactionTemplate_UserEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_R2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] DROP CONSTRAINT [DF_tblCompoundTransactionTemplate_R2]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_R1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] DROP CONSTRAINT [DF_tblCompoundTransactionTemplate_R1]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCTTemplate_TransactionGroup]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] DROP CONSTRAINT [DF_tblCTTemplate_TransactionGroup]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCTTemplate_TransactionComment]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] DROP CONSTRAINT [DF_tblCTTemplate_TransactionComment]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionSpecificInitialEqualisationFlag]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] DROP CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionSpecificInitialEqualisationFlag]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionIsTransfer]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] DROP CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionIsTransfer]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionCostIsPercent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] DROP CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionCostIsPercent]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionSpecificInitialEqualisationValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] DROP CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionSpecificInitialEqualisationValue]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionEffectiveWatermark]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] DROP CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionEffectiveWatermark]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionConfirmationDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] DROP CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionConfirmationDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionSettlementDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] DROP CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionSettlementDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionEffectiveValueDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] DROP CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionEffectiveValueDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionFIFOValueDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] DROP CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionFIFOValueDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionValueDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] DROP CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionValueDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionDecisionDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] DROP CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionDecisionDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionCounterparty]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] DROP CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionCounterparty]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionSettlement]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] DROP CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionSettlement]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionCostPercent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] DROP CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionCostPercent]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCTTemplate_TransactionCosts]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] DROP CONSTRAINT [DF_tblCTTemplate_TransactionCosts]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionEffectivePrice]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] DROP CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionEffectivePrice]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionPrice]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] DROP CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionPrice]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionUnits]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] DROP CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionUnits]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionValueorAmount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] DROP CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionValueorAmount]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] DROP CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionType]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionInstrument]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] DROP CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionInstrument]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionFund]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] DROP CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionFund]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCTTemplate_TransactionTicket]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] DROP CONSTRAINT [DF_tblCTTemplate_TransactionTicket]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCTTemplate_TemplateFlags]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] DROP CONSTRAINT [DF_tblCTTemplate_TemplateFlags]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCTTemplate_TemplateTotalSteps]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] DROP CONSTRAINT [DF_tblCTTemplate_TemplateTotalSteps]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCTTemplate_TemplateStep]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] DROP CONSTRAINT [DF_tblCTTemplate_TemplateStep]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TemplateDescription]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] DROP CONSTRAINT [DF_tblCompoundTransactionTemplate_TemplateDescription]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionParameters_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionParameters] DROP CONSTRAINT [DF_tblCompoundTransactionParameters_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionParameters_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionParameters] DROP CONSTRAINT [DF_tblCompoundTransactionParameters_UserEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCTParameters_ParamFloat]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionParameters] DROP CONSTRAINT [DF_tblCTParameters_ParamFloat]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCTParameters_ParamString]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionParameters] DROP CONSTRAINT [DF_tblCTParameters_ParamString]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCTParameters_ParamLong]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionParameters] DROP CONSTRAINT [DF_tblCTParameters_ParamLong]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionParameters_ParamType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionParameters] DROP CONSTRAINT [DF_tblCompoundTransactionParameters_ParamType]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_UserEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_CompoundTransactionPosted]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_CompoundTransactionPosted]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_CompoundTransactionFlags]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_CompoundTransactionFlags]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_CompoundTransactionGroup]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_CompoundTransactionGroup]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_Parameter5_Type]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_Parameter5_Type]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_Parameter4_Type]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_Parameter4_Type]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_Parameter3_Type]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_Parameter3_Type]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_Parameter2_Type]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_Parameter2_Type]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_Parameter1_Type]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_Parameter1_Type]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_ParameterCount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_ParameterCount]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionGroup]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_TransactionGroup]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionComment]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_TransactionComment]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TradeStatusID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_TradeStatusID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionFinalDataEntry]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_TransactionFinalDataEntry]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionSettlementConfirmed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_TransactionSettlementConfirmed]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionAmountConfirmed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_TransactionAmountConfirmed]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionInitialDataEntry]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_TransactionInitialDataEntry]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionBankConfirmation]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_TransactionBankConfirmation]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionInstructionFlag]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_TransactionInstructionFlag]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionFinalSettlement]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_TransactionFinalSettlement]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionFinalCosts]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_TransactionFinalCosts]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionFinalPrice]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_TransactionFinalPrice]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionFinalAmount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_TransactionFinalAmount]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionSpecificInitialEqualisationFlag]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_TransactionSpecificInitialEqualisationFlag]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionIsTransfer]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_TransactionIsTransfer]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionExemptFromUpdate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_TransactionExemptFromUpdate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionCostIsPercent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_TransactionCostIsPercent]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionConfirmationDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_TransactionConfirmationDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionSettlementDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_TransactionSettlementDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionDecisionDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_TransactionDecisionDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionDataEntry]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_TransactionDataEntry]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionExecution]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_TransactionExecution]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionInvestment]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_TransactionInvestment]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionSpecificInitialEqualisationValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_TransactionSpecificInitialEqualisationValue]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionEffectiveWatermark]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_TransactionEffectiveWatermark]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionEffectivePrice]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_TransactionEffectivePrice]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionCostPercent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_TransactionCostPercent]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionCosts]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_TransactionCosts]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionValueorAmount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_TransactionValueorAmount]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionInstrument]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_TransactionInstrument]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionTicket]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_TransactionTicket]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_CompoundTransactionType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_CompoundTransactionType]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionCompoundID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_TransactionCompoundID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionParentID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_TransactionParentID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_AdminStatus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] DROP CONSTRAINT [DF_tblCompoundTransaction_AdminStatus]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblChangeControl_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblChangeControl] DROP CONSTRAINT [DF_tblChangeControl_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblChangeControl_appCurrentUser]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblChangeControl] DROP CONSTRAINT [DF_tblChangeControl_appCurrentUser]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblChangeControl_User_AcceptedBusiness]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblChangeControl] DROP CONSTRAINT [DF_tblChangeControl_User_AcceptedBusiness]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblChangeControl_User_AcceptedOwner]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblChangeControl] DROP CONSTRAINT [DF_tblChangeControl_User_AcceptedOwner]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblChangeControl_User_AcceptedIT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblChangeControl] DROP CONSTRAINT [DF_tblChangeControl_User_AcceptedIT]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblChangeControl_User_AuthoriseRejected]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblChangeControl] DROP CONSTRAINT [DF_tblChangeControl_User_AuthoriseRejected]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblChangeControl_User_AuthorisedBusiness]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblChangeControl] DROP CONSTRAINT [DF_tblChangeControl_User_AuthorisedBusiness]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblChangeControl_User_AuthorisedOwner]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblChangeControl] DROP CONSTRAINT [DF_tblChangeControl_User_AuthorisedOwner]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblChangeControl_User_AuthorisedIT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblChangeControl] DROP CONSTRAINT [DF_tblChangeControl_User_AuthorisedIT]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblChangeControl_User_ReviewRejected]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblChangeControl] DROP CONSTRAINT [DF_tblChangeControl_User_ReviewRejected]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblChangeControl_User_ReviewedIT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblChangeControl] DROP CONSTRAINT [DF_tblChangeControl_User_ReviewedIT]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblChangeControl_Text_TestPlan]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblChangeControl] DROP CONSTRAINT [DF_tblChangeControl_Text_TestPlan]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblChangeControl_Text_ImplementationPlan]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblChangeControl] DROP CONSTRAINT [DF_tblChangeControl_Text_ImplementationPlan]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblChangeControl_Text_Restrictions]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblChangeControl] DROP CONSTRAINT [DF_tblChangeControl_Text_Restrictions]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblChangeControl_Text_Dependencies]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblChangeControl] DROP CONSTRAINT [DF_tblChangeControl_Text_Dependencies]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblChangeControl_Text_ScopeOfChange]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblChangeControl] DROP CONSTRAINT [DF_tblChangeControl_Text_ScopeOfChange]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblChangeControl_Text_Detail]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblChangeControl] DROP CONSTRAINT [DF_tblChangeControl_Text_Detail]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblChangeControl_Text_Change]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblChangeControl] DROP CONSTRAINT [DF_tblChangeControl_Text_Change]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblChangeControl_Product]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblChangeControl] DROP CONSTRAINT [DF_tblChangeControl_Product]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBrokerAccounts_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBrokerAccounts] DROP CONSTRAINT [DF_tblBrokerAccounts_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBrokerAccounts_UserName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBrokerAccounts] DROP CONSTRAINT [DF_tblBrokerAccounts_UserName]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBrokerAccounts_InstrumentID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBrokerAccounts] DROP CONSTRAINT [DF_tblBrokerAccounts_InstrumentID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBrokerAccounts_CurrencyID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBrokerAccounts] DROP CONSTRAINT [DF_tblBrokerAccounts_CurrencyID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBrokerAccounts_Exchange]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBrokerAccounts] DROP CONSTRAINT [DF_tblBrokerAccounts_Exchange]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBrokerAccounts_InstrumentTypeID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBrokerAccounts] DROP CONSTRAINT [DF_tblBrokerAccounts_InstrumentTypeID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBrokerAccounts_FundID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBrokerAccounts] DROP CONSTRAINT [DF_tblBrokerAccounts_FundID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBroker_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBroker] DROP CONSTRAINT [DF_tblBroker_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBroker_UserName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBroker] DROP CONSTRAINT [DF_tblBroker_UserName]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBroker_NumericPrecision]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBroker] DROP CONSTRAINT [DF_tblBroker_NumericPrecision]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBroker_NLS_Culture]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBroker] DROP CONSTRAINT [DF_tblBroker_NLS_Culture]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBookmark_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBookmark] DROP CONSTRAINT [DF_tblBookmark_DateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBookmark_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBookmark] DROP CONSTRAINT [DF_tblBookmark_UserEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBookmarks_BookmarkVisibleToAll]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBookmark] DROP CONSTRAINT [DF_tblBookmarks_BookmarkVisibleToAll]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBookmarks_BookmarkWholeDay]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBookmark] DROP CONSTRAINT [DF_tblBookmarks_BookmarkWholeDay]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBookmarks_BookmarkDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBookmark] DROP CONSTRAINT [DF_tblBookmarks_BookmarkDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBookmarks_BookMarkDescription]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBookmark] DROP CONSTRAINT [DF_tblBookmarks_BookMarkDescription]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBookmarks_BookmarkUserID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBookmark] DROP CONSTRAINT [DF_tblBookmarks_BookmarkUserID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBenchmarkItems_FundDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBenchmarkItems] DROP CONSTRAINT [DF_tblBenchmarkItems_FundDateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBenchmarkItems_UserName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBenchmarkItems] DROP CONSTRAINT [DF_tblBenchmarkItems_UserName]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBenchmarkItems_BenchmarkFixedRate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBenchmarkItems] DROP CONSTRAINT [DF_tblBenchmarkItems_BenchmarkFixedRate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBenchmarkItems_BenchmarkInstrumentID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBenchmarkItems] DROP CONSTRAINT [DF_tblBenchmarkItems_BenchmarkInstrumentID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBenchmarkItems_BenchmarkItemCalculationMethod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBenchmarkItems] DROP CONSTRAINT [DF_tblBenchmarkItems_BenchmarkItemCalculationMethod]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBenchmarkIndex_FundDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBenchmarkIndex] DROP CONSTRAINT [DF_tblBenchmarkIndex_FundDateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBenchmarkIndex_UserName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBenchmarkIndex] DROP CONSTRAINT [DF_tblBenchmarkIndex_UserName]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBenchmarkIndex_BenchmarkCalculationMethod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBenchmarkIndex] DROP CONSTRAINT [DF_tblBenchmarkIndex_BenchmarkCalculationMethod]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBenchmarkIndex_BenchmarkType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBenchmarkIndex] DROP CONSTRAINT [DF_tblBenchmarkIndex_BenchmarkType]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_TRADEFILECONFIRMATIONS]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] DROP CONSTRAINT [DF_tblAutoUpdate_HAS_TRADEFILECONFIRMATIONS]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_TRADEFILEACKNOWLEDGEMENT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] DROP CONSTRAINT [DF_tblAutoUpdate_HAS_TRADEFILEACKNOWLEDGEMENT]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_TRADEFILEPENDING]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] DROP CONSTRAINT [DF_tblAutoUpdate_HAS_TRADEFILEPENDING]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_BOOKMARK]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] DROP CONSTRAINT [DF_tblAutoUpdate_HAS_BOOKMARK]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_TRANSACTIONTEMPLATE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] DROP CONSTRAINT [DF_tblAutoUpdate_HAS_TRANSACTIONTEMPLATE]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_COMPOUNDTRANSACTION]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] DROP CONSTRAINT [DF_tblAutoUpdate_HAS_COMPOUNDTRANSACTION]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_CHANGECONTROL]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] DROP CONSTRAINT [DF_tblAutoUpdate_HAS_CHANGECONTROL]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_FUNDMILESTONE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] DROP CONSTRAINT [DF_tblAutoUpdate_HAS_FUNDMILESTONE]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_KNOWLEDGEDATE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] DROP CONSTRAINT [DF_tblAutoUpdate_HAS_KNOWLEDGEDATE]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_PERMISSIONS]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] DROP CONSTRAINT [DF_tblAutoUpdate_HAS_PERMISSIONS]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_SYSTEMDATES]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] DROP CONSTRAINT [DF_tblAutoUpdate_HAS_SYSTEMDATES]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_PRICE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] DROP CONSTRAINT [DF_tblAutoUpdate_HAS_PRICE]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_FX]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] DROP CONSTRAINT [DF_tblAutoUpdate_HAS_FX]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_ESTIMATE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] DROP CONSTRAINT [DF_tblAutoUpdate_HAS_ESTIMATE]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_TRANSACTIONTYPE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] DROP CONSTRAINT [DF_tblAutoUpdate_HAS_TRANSACTIONTYPE]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_TRANSACTION]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] DROP CONSTRAINT [DF_tblAutoUpdate_HAS_TRANSACTION]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_PERSON]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] DROP CONSTRAINT [DF_tblAutoUpdate_HAS_PERSON]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_LIMITTYPE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] DROP CONSTRAINT [DF_tblAutoUpdate_HAS_LIMITTYPE]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_LIMITS]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] DROP CONSTRAINT [DF_tblAutoUpdate_HAS_LIMITS]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_INVESTORGROUP]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] DROP CONSTRAINT [DF_tblAutoUpdate_HAS_INVESTORGROUP]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_INSTRUMENTTYPE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] DROP CONSTRAINT [DF_tblAutoUpdate_HAS_INSTRUMENTTYPE]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_INSTRUMENT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] DROP CONSTRAINT [DF_tblAutoUpdate_HAS_INSTRUMENT]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_FUNDTYPE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] DROP CONSTRAINT [DF_tblAutoUpdate_HAS_FUNDTYPE]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_FUND]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] DROP CONSTRAINT [DF_tblAutoUpdate_HAS_FUND]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_DEALINGPERIOD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] DROP CONSTRAINT [DF_tblAutoUpdate_HAS_DEALINGPERIOD]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_COUNTERPARTY]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] DROP CONSTRAINT [DF_tblAutoUpdate_HAS_COUNTERPARTY]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_CURRENCY]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] DROP CONSTRAINT [DF_tblAutoUpdate_HAS_CURRENCY]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAttribution_AttribDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAttribution] DROP CONSTRAINT [DF_tblAttribution_AttribDateEntered]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAttribution_AttribLookthroughGeneratedRecord]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAttribution] DROP CONSTRAINT [DF_tblAttribution_AttribLookthroughGeneratedRecord]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAttribution_AttribIsLookthrough]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAttribution] DROP CONSTRAINT [DF_tblAttribution_AttribIsLookthrough]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAttribution_AttribIsMilestone]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAttribution] DROP CONSTRAINT [DF_tblAttribution_AttribIsMilestone]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAttribution_AttribDatesFilter]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAttribution] DROP CONSTRAINT [DF_tblAttribution_AttribDatesFilter]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAttribution_AttribStatusGroupID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAttribution] DROP CONSTRAINT [DF_tblAttribution_AttribStatusGroupID]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAttribution_AttribFinalUnits]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAttribution] DROP CONSTRAINT [DF_tblAttribution_AttribFinalUnits]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAttribution_AttribTotalValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAttribution] DROP CONSTRAINT [DF_tblAttribution_AttribTotalValue]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAttribution_AttribTargetBps]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAttribution] DROP CONSTRAINT [DF_tblAttribution_AttribTargetBps]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAttribution_AttribBips]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAttribution] DROP CONSTRAINT [DF_tblAttribution_AttribBips]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAttribution_AttribPercentReturn]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAttribution] DROP CONSTRAINT [DF_tblAttribution_AttribPercentReturn]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAttribution_AttribProfit]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAttribution] DROP CONSTRAINT [DF_tblAttribution_AttribProfit]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAttribution_AttribEndWeight]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAttribution] DROP CONSTRAINT [DF_tblAttribution_AttribEndWeight]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAttribution_AttribWeight]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAttribution] DROP CONSTRAINT [DF_tblAttribution_AttribWeight]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAttribution_AttribInvested]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAttribution] DROP CONSTRAINT [DF_tblAttribution_AttribInvested]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAttribution_AttribPeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAttribution] DROP CONSTRAINT [DF_tblAttribution_AttribPeriod]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAttribution_AttribDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAttribution] DROP CONSTRAINT [DF_tblAttribution_AttribDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAttribution_AttribInstrument]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAttribution] DROP CONSTRAINT [DF_tblAttribution_AttribInstrument]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAttribution_AttribFund]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAttribution] DROP CONSTRAINT [DF_tblAttribution_AttribFund]
END

GO
/****** Object:  Table [dbo].[viewChangeControl]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[viewChangeControl]') AND type in (N'U'))
DROP TABLE [dbo].[viewChangeControl]
GO
/****** Object:  Table [dbo].[TITRES]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TITRES]') AND type in (N'U'))
DROP TABLE [dbo].[TITRES]
GO
/****** Object:  Table [dbo].[tblWorkflows]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblWorkflows]') AND type in (N'U'))
DROP TABLE [dbo].[tblWorkflows]
GO
/****** Object:  Table [dbo].[tblWorkflowRules]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblWorkflowRules]') AND type in (N'U'))
DROP TABLE [dbo].[tblWorkflowRules]
GO
/****** Object:  Table [dbo].[tblVeniceFormTooltips]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblVeniceFormTooltips]') AND type in (N'U'))
DROP TABLE [dbo].[tblVeniceFormTooltips]
GO
/****** Object:  Table [dbo].[tblUserPermissions]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblUserPermissions]') AND type in (N'U'))
DROP TABLE [dbo].[tblUserPermissions]
GO
/****** Object:  Table [dbo].[tblUnitHolderFees]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblUnitHolderFees]') AND type in (N'U'))
DROP TABLE [dbo].[tblUnitHolderFees]
GO
/****** Object:  Table [dbo].[tblTriggerReferencePrices]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblTriggerReferencePrices]') AND type in (N'U'))
DROP TABLE [dbo].[tblTriggerReferencePrices]
GO
/****** Object:  Table [dbo].[tblTransactionType]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblTransactionType]') AND type in (N'U'))
DROP TABLE [dbo].[tblTransactionType]
GO
/****** Object:  Table [dbo].[tblTransaction]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblTransaction]') AND type in (N'U'))
DROP TABLE [dbo].[tblTransaction]
GO
/****** Object:  Table [dbo].[tblTradeStatus]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblTradeStatus]') AND type in (N'U'))
DROP TABLE [dbo].[tblTradeStatus]
GO
/****** Object:  Table [dbo].[tblTradeFilePending]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblTradeFilePending]') AND type in (N'U'))
DROP TABLE [dbo].[tblTradeFilePending]
GO
/****** Object:  Table [dbo].[tblTradeFileConfirmations]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblTradeFileConfirmations]') AND type in (N'U'))
DROP TABLE [dbo].[tblTradeFileConfirmations]
GO
/****** Object:  Table [dbo].[tblTradeFileAcknowledgement]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblTradeFileAcknowledgement]') AND type in (N'U'))
DROP TABLE [dbo].[tblTradeFileAcknowledgement]
GO
/****** Object:  Table [dbo].[tblSystemStrings]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblSystemStrings]') AND type in (N'U'))
DROP TABLE [dbo].[tblSystemStrings]
GO
/****** Object:  Table [dbo].[tblSystemDates]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblSystemDates]') AND type in (N'U'))
DROP TABLE [dbo].[tblSystemDates]
GO
/****** Object:  Table [dbo].[tblSubscriptionLog]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblSubscriptionLog]') AND type in (N'U'))
DROP TABLE [dbo].[tblSubscriptionLog]
GO
/****** Object:  Table [dbo].[tblSubFund]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblSubFund]') AND type in (N'U'))
DROP TABLE [dbo].[tblSubFund]
GO
/****** Object:  Table [dbo].[tblSophisStatusGroups]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblSophisStatusGroups]') AND type in (N'U'))
DROP TABLE [dbo].[tblSophisStatusGroups]
GO
/****** Object:  Table [dbo].[tblSophisBusinessEvents]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblSophisBusinessEvents]') AND type in (N'U'))
DROP TABLE [dbo].[tblSophisBusinessEvents]
GO
/****** Object:  Table [dbo].[tblSectorLimit]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblSectorLimit]') AND type in (N'U'))
DROP TABLE [dbo].[tblSectorLimit]
GO
/****** Object:  Table [dbo].[tblRiskDataLimits]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRiskDataLimits]') AND type in (N'U'))
DROP TABLE [dbo].[tblRiskDataLimits]
GO
/****** Object:  Table [dbo].[tblRiskDataCompoundCharacteristic]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRiskDataCompoundCharacteristic]') AND type in (N'U'))
DROP TABLE [dbo].[tblRiskDataCompoundCharacteristic]
GO
/****** Object:  Table [dbo].[tblRiskDataCharacteristicTypes]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRiskDataCharacteristicTypes]') AND type in (N'U'))
DROP TABLE [dbo].[tblRiskDataCharacteristicTypes]
GO
/****** Object:  Table [dbo].[tblRiskDataCharacteristic]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRiskDataCharacteristic]') AND type in (N'U'))
DROP TABLE [dbo].[tblRiskDataCharacteristic]
GO
/****** Object:  Table [dbo].[tblRiskDataCategory]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRiskDataCategory]') AND type in (N'U'))
DROP TABLE [dbo].[tblRiskDataCategory]
GO
/****** Object:  Table [dbo].[tblRiskData]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRiskData]') AND type in (N'U'))
DROP TABLE [dbo].[tblRiskData]
GO
/****** Object:  Table [dbo].[tblReportType]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblReportType]') AND type in (N'U'))
DROP TABLE [dbo].[tblReportType]
GO
/****** Object:  Table [dbo].[tblReportStatus]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblReportStatus]') AND type in (N'U'))
DROP TABLE [dbo].[tblReportStatus]
GO
/****** Object:  Table [dbo].[tblReportDetailLog]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblReportDetailLog]') AND type in (N'U'))
DROP TABLE [dbo].[tblReportDetailLog]
GO
/****** Object:  Table [dbo].[tblReportCustomisation]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblReportCustomisation]') AND type in (N'U'))
DROP TABLE [dbo].[tblReportCustomisation]
GO
/****** Object:  Table [dbo].[tblReport]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblReport]') AND type in (N'U'))
DROP TABLE [dbo].[tblReport]
GO
/****** Object:  Table [dbo].[tblRelationship]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRelationship]') AND type in (N'U'))
DROP TABLE [dbo].[tblRelationship]
GO
/****** Object:  Table [dbo].[tblReferentialIntegrity]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblReferentialIntegrity]') AND type in (N'U'))
DROP TABLE [dbo].[tblReferentialIntegrity]
GO
/****** Object:  Table [dbo].[tblPrice]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPrice]') AND type in (N'U'))
DROP TABLE [dbo].[tblPrice]
GO
/****** Object:  Table [dbo].[tblPortfolioType]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPortfolioType]') AND type in (N'U'))
DROP TABLE [dbo].[tblPortfolioType]
GO
/****** Object:  Table [dbo].[tblPortfolioItemTypes]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPortfolioItemTypes]') AND type in (N'U'))
DROP TABLE [dbo].[tblPortfolioItemTypes]
GO
/****** Object:  Table [dbo].[tblPortfolioIndex]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPortfolioIndex]') AND type in (N'U'))
DROP TABLE [dbo].[tblPortfolioIndex]
GO
/****** Object:  Table [dbo].[tblPortfolioData]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPortfolioData]') AND type in (N'U'))
DROP TABLE [dbo].[tblPortfolioData]
GO
/****** Object:  Table [dbo].[tblPFPCShareAllocation]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPFPCShareAllocation]') AND type in (N'U'))
DROP TABLE [dbo].[tblPFPCShareAllocation]
GO
/****** Object:  Table [dbo].[tblPFPCPortfolioValuation]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPFPCPortfolioValuation]') AND type in (N'U'))
DROP TABLE [dbo].[tblPFPCPortfolioValuation]
GO
/****** Object:  Table [dbo].[tblPFPCPortfolioReconcilliation]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPFPCPortfolioReconcilliation]') AND type in (N'U'))
DROP TABLE [dbo].[tblPFPCPortfolioReconcilliation]
GO
/****** Object:  Table [dbo].[tblPFPCCustodyRecord]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPFPCCustodyRecord]') AND type in (N'U'))
DROP TABLE [dbo].[tblPFPCCustodyRecord]
GO
/****** Object:  Table [dbo].[tblPertracInstrumentFlags]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPertracInstrumentFlags]') AND type in (N'U'))
DROP TABLE [dbo].[tblPertracInstrumentFlags]
GO
/****** Object:  Table [dbo].[tblPertracFieldMapping]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPertracFieldMapping]') AND type in (N'U'))
DROP TABLE [dbo].[tblPertracFieldMapping]
GO
/****** Object:  Table [dbo].[tblPertracCustomFields]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPertracCustomFields]') AND type in (N'U'))
DROP TABLE [dbo].[tblPertracCustomFields]
GO
/****** Object:  Table [dbo].[tblPertracCustomFieldData]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPertracCustomFieldData]') AND type in (N'U'))
DROP TABLE [dbo].[tblPertracCustomFieldData]
GO
/****** Object:  Table [dbo].[tblPerson]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPerson]') AND type in (N'U'))
DROP TABLE [dbo].[tblPerson]
GO
/****** Object:  Table [dbo].[tblPeriod]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPeriod]') AND type in (N'U'))
DROP TABLE [dbo].[tblPeriod]
GO
/****** Object:  Table [dbo].[tblPendingTransactions]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPendingTransactions]') AND type in (N'U'))
DROP TABLE [dbo].[tblPendingTransactions]
GO
/****** Object:  Table [dbo].[tblPendingTradeFileEntries]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPendingTradeFileEntries]') AND type in (N'U'))
DROP TABLE [dbo].[tblPendingTradeFileEntries]
GO
/****** Object:  Table [dbo].[tblPendingDataTaskEmails]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPendingDataTaskEmails]') AND type in (N'U'))
DROP TABLE [dbo].[tblPendingDataTaskEmails]
GO
/****** Object:  Table [dbo].[tblMonthlyComment]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblMonthlyComment]') AND type in (N'U'))
DROP TABLE [dbo].[tblMonthlyComment]
GO
/****** Object:  Table [dbo].[tblMeetingStatus]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblMeetingStatus]') AND type in (N'U'))
DROP TABLE [dbo].[tblMeetingStatus]
GO
/****** Object:  Table [dbo].[tblMeetingPeople]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblMeetingPeople]') AND type in (N'U'))
DROP TABLE [dbo].[tblMeetingPeople]
GO
/****** Object:  Table [dbo].[tblMeeting]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblMeeting]') AND type in (N'U'))
DROP TABLE [dbo].[tblMeeting]
GO
/****** Object:  Table [dbo].[tblMarketPriceTypes]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblMarketPriceTypes]') AND type in (N'U'))
DROP TABLE [dbo].[tblMarketPriceTypes]
GO
/****** Object:  Table [dbo].[tblMarketPrices]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblMarketPrices]') AND type in (N'U'))
DROP TABLE [dbo].[tblMarketPrices]
GO
/****** Object:  Table [dbo].[tblMarketInstruments]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblMarketInstruments]') AND type in (N'U'))
DROP TABLE [dbo].[tblMarketInstruments]
GO
/****** Object:  Table [dbo].[tblManagementCompany]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblManagementCompany]') AND type in (N'U'))
DROP TABLE [dbo].[tblManagementCompany]
GO
/****** Object:  Table [dbo].[tblLocations]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblLocations]') AND type in (N'U'))
DROP TABLE [dbo].[tblLocations]
GO
/****** Object:  Table [dbo].[tblLiquidityLimit]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblLiquidityLimit]') AND type in (N'U'))
DROP TABLE [dbo].[tblLiquidityLimit]
GO
/****** Object:  Table [dbo].[tblLimitType]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblLimitType]') AND type in (N'U'))
DROP TABLE [dbo].[tblLimitType]
GO
/****** Object:  Table [dbo].[tblLimits]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblLimits]') AND type in (N'U'))
DROP TABLE [dbo].[tblLimits]
GO
/****** Object:  Table [dbo].[tblJob]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblJob]') AND type in (N'U'))
DROP TABLE [dbo].[tblJob]
GO
/****** Object:  Table [dbo].[tblInvestorGroup]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblInvestorGroup]') AND type in (N'U'))
DROP TABLE [dbo].[tblInvestorGroup]
GO
/****** Object:  Table [dbo].[tblInstrumentType]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblInstrumentType]') AND type in (N'U'))
DROP TABLE [dbo].[tblInstrumentType]
GO
/****** Object:  Table [dbo].[tblInstrumentFlorenceEntityLink]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblInstrumentFlorenceEntityLink]') AND type in (N'U'))
DROP TABLE [dbo].[tblInstrumentFlorenceEntityLink]
GO
/****** Object:  Table [dbo].[tblInstrument]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblInstrument]') AND type in (N'U'))
DROP TABLE [dbo].[tblInstrument]
GO
/****** Object:  Table [dbo].[tblIndexMapping]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblIndexMapping]') AND type in (N'U'))
DROP TABLE [dbo].[tblIndexMapping]
GO
/****** Object:  Table [dbo].[tblHistoricFundValuations]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblHistoricFundValuations]') AND type in (N'U'))
DROP TABLE [dbo].[tblHistoricFundValuations]
GO
/****** Object:  Table [dbo].[tblGroupList_13Feb2013]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblGroupList_13Feb2013]') AND type in (N'U'))
DROP TABLE [dbo].[tblGroupList_13Feb2013]
GO
/****** Object:  Table [dbo].[tblGroupList]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblGroupList]') AND type in (N'U'))
DROP TABLE [dbo].[tblGroupList]
GO
/****** Object:  Table [dbo].[tblGroupItems_13Feb2013]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblGroupItems_13Feb2013]') AND type in (N'U'))
DROP TABLE [dbo].[tblGroupItems_13Feb2013]
GO
/****** Object:  Table [dbo].[tblGroupItems]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblGroupItems]') AND type in (N'U'))
DROP TABLE [dbo].[tblGroupItems]
GO
/****** Object:  Table [dbo].[tblGroupItemData_13Feb2013]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblGroupItemData_13Feb2013]') AND type in (N'U'))
DROP TABLE [dbo].[tblGroupItemData_13Feb2013]
GO
/****** Object:  Table [dbo].[tblGroupItemData]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblGroupItemData]') AND type in (N'U'))
DROP TABLE [dbo].[tblGroupItemData]
GO
/****** Object:  Table [dbo].[tblGreeks]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblGreeks]') AND type in (N'U'))
DROP TABLE [dbo].[tblGreeks]
GO
/****** Object:  Table [dbo].[tblGLPostingScenarios]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblGLPostingScenarios]') AND type in (N'U'))
DROP TABLE [dbo].[tblGLPostingScenarios]
GO
/****** Object:  Table [dbo].[tblGLPostings]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblGLPostings]') AND type in (N'U'))
DROP TABLE [dbo].[tblGLPostings]
GO
/****** Object:  Table [dbo].[tblGLPostingRules]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblGLPostingRules]') AND type in (N'U'))
DROP TABLE [dbo].[tblGLPostingRules]
GO
/****** Object:  Table [dbo].[tblGenoaSavedSearchList]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblGenoaSavedSearchList]') AND type in (N'U'))
DROP TABLE [dbo].[tblGenoaSavedSearchList]
GO
/****** Object:  Table [dbo].[tblGenoaSavedSearchItems]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblGenoaSavedSearchItems]') AND type in (N'U'))
DROP TABLE [dbo].[tblGenoaSavedSearchItems]
GO
/****** Object:  Table [dbo].[tblGenoaConstraintList]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblGenoaConstraintList]') AND type in (N'U'))
DROP TABLE [dbo].[tblGenoaConstraintList]
GO
/****** Object:  Table [dbo].[tblGenoaConstraintItems]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblGenoaConstraintItems]') AND type in (N'U'))
DROP TABLE [dbo].[tblGenoaConstraintItems]
GO
/****** Object:  Table [dbo].[tblFX]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblFX]') AND type in (N'U'))
DROP TABLE [dbo].[tblFX]
GO
/****** Object:  Table [dbo].[tblFundType]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblFundType]') AND type in (N'U'))
DROP TABLE [dbo].[tblFundType]
GO
/****** Object:  Table [dbo].[tblFundPerformanceFeePoints]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblFundPerformanceFeePoints]') AND type in (N'U'))
DROP TABLE [dbo].[tblFundPerformanceFeePoints]
GO
/****** Object:  Table [dbo].[tblFundMilestones]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblFundMilestones]') AND type in (N'U'))
DROP TABLE [dbo].[tblFundMilestones]
GO
/****** Object:  Table [dbo].[tblFundContactDetails]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblFundContactDetails]') AND type in (N'U'))
DROP TABLE [dbo].[tblFundContactDetails]
GO
/****** Object:  Table [dbo].[tblFund]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblFund]') AND type in (N'U'))
DROP TABLE [dbo].[tblFund]
GO
/****** Object:  Table [dbo].[tblFlorenceStatusIDs]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblFlorenceStatusIDs]') AND type in (N'U'))
DROP TABLE [dbo].[tblFlorenceStatusIDs]
GO
/****** Object:  Table [dbo].[tblFlorenceStatus]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblFlorenceStatus]') AND type in (N'U'))
DROP TABLE [dbo].[tblFlorenceStatus]
GO
/****** Object:  Table [dbo].[tblFlorenceItems]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblFlorenceItems]') AND type in (N'U'))
DROP TABLE [dbo].[tblFlorenceItems]
GO
/****** Object:  Table [dbo].[tblFlorenceGroup]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblFlorenceGroup]') AND type in (N'U'))
DROP TABLE [dbo].[tblFlorenceGroup]
GO
/****** Object:  Table [dbo].[tblFlorenceEntity]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblFlorenceEntity]') AND type in (N'U'))
DROP TABLE [dbo].[tblFlorenceEntity]
GO
/****** Object:  Table [dbo].[tblFlorenceDataValues]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblFlorenceDataValues]') AND type in (N'U'))
DROP TABLE [dbo].[tblFlorenceDataValues]
GO
/****** Object:  Table [dbo].[tblExposure]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblExposure]') AND type in (N'U'))
DROP TABLE [dbo].[tblExposure]
GO
/****** Object:  Table [dbo].[tblEstimate]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblEstimate]') AND type in (N'U'))
DROP TABLE [dbo].[tblEstimate]
GO
/****** Object:  Table [dbo].[tblDealingPeriod]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblDealingPeriod]') AND type in (N'U'))
DROP TABLE [dbo].[tblDealingPeriod]
GO
/****** Object:  Table [dbo].[tblDataTaskType]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblDataTaskType]') AND type in (N'U'))
DROP TABLE [dbo].[tblDataTaskType]
GO
/****** Object:  Table [dbo].[tblDataTask]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblDataTask]') AND type in (N'U'))
DROP TABLE [dbo].[tblDataTask]
GO
/****** Object:  Table [dbo].[tblDataProviderInstrumentType]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblDataProviderInstrumentType]') AND type in (N'U'))
DROP TABLE [dbo].[tblDataProviderInstrumentType]
GO
/****** Object:  Table [dbo].[tblDataDictionary_TransactionFormLabels]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblDataDictionary_TransactionFormLabels]') AND type in (N'U'))
DROP TABLE [dbo].[tblDataDictionary_TransactionFormLabels]
GO
/****** Object:  Table [dbo].[tblCurrency]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCurrency]') AND type in (N'U'))
DROP TABLE [dbo].[tblCurrency]
GO
/****** Object:  Table [dbo].[tblCTA_Simulation_13Feb2013]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCTA_Simulation_13Feb2013]') AND type in (N'U'))
DROP TABLE [dbo].[tblCTA_Simulation_13Feb2013]
GO
/****** Object:  Table [dbo].[tblCTA_Simulation]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCTA_Simulation]') AND type in (N'U'))
DROP TABLE [dbo].[tblCTA_Simulation]
GO
/****** Object:  Table [dbo].[tblCTA_Folders_13Feb2013]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCTA_Folders_13Feb2013]') AND type in (N'U'))
DROP TABLE [dbo].[tblCTA_Folders_13Feb2013]
GO
/****** Object:  Table [dbo].[tblCTA_Folders]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCTA_Folders]') AND type in (N'U'))
DROP TABLE [dbo].[tblCTA_Folders]
GO
/****** Object:  Table [dbo].[tblCovarianceList]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCovarianceList]') AND type in (N'U'))
DROP TABLE [dbo].[tblCovarianceList]
GO
/****** Object:  Table [dbo].[tblCovariance]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCovariance]') AND type in (N'U'))
DROP TABLE [dbo].[tblCovariance]
GO
/****** Object:  Table [dbo].[tblCounterparty]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCounterparty]') AND type in (N'U'))
DROP TABLE [dbo].[tblCounterparty]
GO
/****** Object:  Table [dbo].[tblCompoundTransactionTemplate]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCompoundTransactionTemplate]') AND type in (N'U'))
DROP TABLE [dbo].[tblCompoundTransactionTemplate]
GO
/****** Object:  Table [dbo].[tblCompoundTransactionParameters]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCompoundTransactionParameters]') AND type in (N'U'))
DROP TABLE [dbo].[tblCompoundTransactionParameters]
GO
/****** Object:  Table [dbo].[tblCompoundTransaction]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCompoundTransaction]') AND type in (N'U'))
DROP TABLE [dbo].[tblCompoundTransaction]
GO
/****** Object:  Table [dbo].[tblChangeControl]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblChangeControl]') AND type in (N'U'))
DROP TABLE [dbo].[tblChangeControl]
GO
/****** Object:  Table [dbo].[tblBrokerAccounts]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblBrokerAccounts]') AND type in (N'U'))
DROP TABLE [dbo].[tblBrokerAccounts]
GO
/****** Object:  Table [dbo].[tblBroker]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblBroker]') AND type in (N'U'))
DROP TABLE [dbo].[tblBroker]
GO
/****** Object:  Table [dbo].[tblBookmark]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblBookmark]') AND type in (N'U'))
DROP TABLE [dbo].[tblBookmark]
GO
/****** Object:  Table [dbo].[tblBenchmarkItems]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblBenchmarkItems]') AND type in (N'U'))
DROP TABLE [dbo].[tblBenchmarkItems]
GO
/****** Object:  Table [dbo].[tblBenchmarkIndex]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblBenchmarkIndex]') AND type in (N'U'))
DROP TABLE [dbo].[tblBenchmarkIndex]
GO
/****** Object:  Table [dbo].[tblAutoUpdate_Detail]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblAutoUpdate_Detail]') AND type in (N'U'))
DROP TABLE [dbo].[tblAutoUpdate_Detail]
GO
/****** Object:  Table [dbo].[tblAutoUpdate]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblAutoUpdate]') AND type in (N'U'))
DROP TABLE [dbo].[tblAutoUpdate]
GO
/****** Object:  Table [dbo].[tblAttribution]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblAttribution]') AND type in (N'U'))
DROP TABLE [dbo].[tblAttribution]
GO
/****** Object:  Table [dbo].[tbl_DevItem]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_DevItem]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_DevItem]
GO
/****** Object:  Table [dbo].[tbl_DevEntity]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_DevEntity]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_DevEntity]
GO
/****** Object:  Table [dbo].[sophis_TRANSACTIONS_ToIgnore]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sophis_TRANSACTIONS_ToIgnore]') AND type in (N'U'))
DROP TABLE [dbo].[sophis_TRANSACTIONS_ToIgnore]
GO
/****** Object:  Table [dbo].[sophis_TRANSACTIONS]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sophis_TRANSACTIONS]') AND type in (N'U'))
DROP TABLE [dbo].[sophis_TRANSACTIONS]
GO
/****** Object:  Table [dbo].[sophis_TITRES_Override]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sophis_TITRES_Override]') AND type in (N'U'))
DROP TABLE [dbo].[sophis_TITRES_Override]
GO
/****** Object:  Table [dbo].[sophis_TITRES]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sophis_TITRES]') AND type in (N'U'))
DROP TABLE [dbo].[sophis_TITRES]
GO
/****** Object:  Table [dbo].[sophis_STATUS_GROUPS]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sophis_STATUS_GROUPS]') AND type in (N'U'))
DROP TABLE [dbo].[sophis_STATUS_GROUPS]
GO
/****** Object:  Table [dbo].[sophis_PRICES]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sophis_PRICES]') AND type in (N'U'))
DROP TABLE [dbo].[sophis_PRICES]
GO
/****** Object:  Table [dbo].[sophis_FX]    Script Date: 6/7/2013 5:05:30 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sophis_FX]') AND type in (N'U'))
DROP TABLE [dbo].[sophis_FX]
GO
/****** Object:  Table [dbo].[sophis_FX]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sophis_FX]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[sophis_FX](
	[FXCurrencyCode] [int] NOT NULL,
	[FXDate] [datetime] NOT NULL,
	[FXRate] [float] NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[sophis_PRICES]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sophis_PRICES]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[sophis_PRICES](
	[PriceInstrument] [int] NOT NULL,
	[PriceDate] [smalldatetime] NOT NULL,
	[PriceLevel] [float] NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[sophis_STATUS_GROUPS]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sophis_STATUS_GROUPS]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[sophis_STATUS_GROUPS](
	[RECORD_TYPE] [int] NULL,
	[GROUP_NAME] [nvarchar](50) NULL,
	[KERNEL_STATUS_GROUP_ID] [int] NULL,
	[PRIORITY] [int] NULL,
	[STATUS_ID] [int] NULL,
	[STATUS_NAME] [nvarchar](200) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[sophis_TITRES]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sophis_TITRES]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[sophis_TITRES](
	[SICOVAM] [int] NOT NULL,
	[LIBELLE] [nvarchar](40) NULL,
	[TYPE] [nvarchar](1) NULL,
	[COMM] [nvarchar](200) NULL,
	[EXSOC] [date] NULL,
	[CROIDIV] [int] NULL,
	[QUOTITE] [float] NULL,
	[ECHEANCE] [date] NULL,
	[NOMINAL] [float] NULL,
	[DATEREGL] [date] NULL,
	[NBTITRES] [int] NULL,
	[CODESJ] [int] NULL,
	[TYPESJ] [int] NULL,
	[TYPEPRO] [int] NULL,
	[PRIXEXER] [int] NULL,
	[PROPORTION] [int] NULL,
	[PARITE] [float] NULL,
	[EMISSION] [date] NULL,
	[PRIXEMI] [int] NULL,
	[DEBUTPER] [date] NULL,
	[FINPER] [date] NULL,
	[NBPTS] [int] NULL,
	[MONTANT] [int] NULL,
	[TAUX] [int] NULL,
	[JAMBE1] [int] NULL,
	[DUREE1] [int] NULL,
	[FIXE1] [int] NULL,
	[DATECOUPON1] [date] NULL,
	[COUPON1] [int] NULL,
	[JAMBE2] [int] NULL,
	[DUREE2] [int] NULL,
	[FIXE2] [int] NULL,
	[DATECOUPON2] [date] NULL,
	[COUPON2] [int] NULL,
	[DATEFINAL] [date] NULL,
	[DATECALCUL] [date] NULL,
	[J1REFCON1] [int] NULL,
	[J1REFCON2] [int] NULL,
	[J2REFCON1] [int] NULL,
	[J2REFCON2] [int] NULL,
	[J1REFCON3] [int] NULL,
	[J2REFCON3] [int] NULL,
	[DEVISECTT] [int] NULL,
	[DEVISEEXER] [int] NULL,
	[CODE_EMET] [int] NULL,
	[DEVISEAC] [int] NULL,
	[MARCHE] [int] NULL,
	[BASE1] [int] NULL,
	[BASE2] [int] NULL,
	[MARCHE_ACT] [int] NULL,
	[BETA] [int] NULL,
	[TAUX_VAR] [int] NULL,
	[MNEMO] [int] NULL,
	[GROSCOUPON1] [int] NULL,
	[GROSCOUPON2] [int] NULL,
	[AFFECTATION] [int] NULL,
	[MODELE] [nvarchar](40) NULL,
	[REFERENCE] [nvarchar](24) NULL,
	[TYPEDERIVE] [int] NULL,
	[CODESJ2] [int] NULL,
	[QUANTITE2] [int] NULL,
	[MNEMO_V2] [nvarchar](50) NULL,
	[EXTERNREF] [nvarchar](40) NULL,
	[FIXING_TYPE] [int] NULL,
	[HOLIDAYADJUSTMENT] [int] NULL,
	[STATUS] [int] NULL,
	[CREDIT_SPREAD_VOLAT] [int] NULL,
	[CREDIT_SPREAD_CORRELATION] [int] NULL,
	[QUOTATION_TYPE] [int] NULL,
	[SENIORITY] [int] NULL,
	[ISSUER] [int] NULL,
	[FAMILY] [int] NULL,
	[FREQ_COUPON] [int] NULL,
	[PERIMETERID] [int] NULL,
	[PREPAYMENT] [nvarchar](40) NULL,
	[UNDERLYING] [int] NULL,
	[NAV_CALENDAR] [int] NULL,
	[CUT_OFF_CALENDAR] [int] NULL,
	[PAYMENT_LAG_SUBSCRIPTION] [int] NULL,
	[PAYMENT_LAG_REDEMPTION] [int] NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[sophis_TITRES_Override]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sophis_TITRES_Override]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[sophis_TITRES_Override](
	[SICOVAM] [int] NOT NULL,
	[LIBELLE] [nvarchar](40) NULL,
	[TYPE] [nvarchar](1) NULL,
	[COMM] [nvarchar](200) NULL,
	[EXSOC] [date] NULL,
	[CROIDIV] [int] NULL,
	[QUOTITE] [float] NULL,
	[ECHEANCE] [date] NULL,
	[NOMINAL] [float] NULL,
	[DATEREGL] [date] NULL,
	[NBTITRES] [int] NULL,
	[CODESJ] [int] NULL,
	[TYPESJ] [int] NULL,
	[TYPEPRO] [int] NULL,
	[PRIXEXER] [int] NULL,
	[PROPORTION] [int] NULL,
	[PARITE] [float] NULL,
	[EMISSION] [date] NULL,
	[PRIXEMI] [int] NULL,
	[DEBUTPER] [date] NULL,
	[FINPER] [date] NULL,
	[NBPTS] [int] NULL,
	[MONTANT] [int] NULL,
	[TAUX] [int] NULL,
	[JAMBE1] [int] NULL,
	[DUREE1] [int] NULL,
	[FIXE1] [int] NULL,
	[DATECOUPON1] [date] NULL,
	[COUPON1] [int] NULL,
	[JAMBE2] [int] NULL,
	[DUREE2] [int] NULL,
	[FIXE2] [int] NULL,
	[DATECOUPON2] [date] NULL,
	[COUPON2] [int] NULL,
	[DATEFINAL] [date] NULL,
	[DATECALCUL] [date] NULL,
	[J1REFCON1] [int] NULL,
	[J1REFCON2] [int] NULL,
	[J2REFCON1] [int] NULL,
	[J2REFCON2] [int] NULL,
	[J1REFCON3] [int] NULL,
	[J2REFCON3] [int] NULL,
	[DEVISECTT] [int] NULL,
	[DEVISEEXER] [int] NULL,
	[CODE_EMET] [int] NULL,
	[DEVISEAC] [int] NULL,
	[MARCHE] [int] NULL,
	[BASE1] [int] NULL,
	[BASE2] [int] NULL,
	[MARCHE_ACT] [int] NULL,
	[BETA] [int] NULL,
	[TAUX_VAR] [int] NULL,
	[MNEMO] [int] NULL,
	[GROSCOUPON1] [int] NULL,
	[GROSCOUPON2] [int] NULL,
	[AFFECTATION] [int] NULL,
	[MODELE] [nvarchar](40) NULL,
	[REFERENCE] [nvarchar](24) NULL,
	[TYPEDERIVE] [int] NULL,
	[CODESJ2] [int] NULL,
	[QUANTITE2] [int] NULL,
	[MNEMO_V2] [nvarchar](50) NULL,
	[EXTERNREF] [nvarchar](40) NULL,
	[FIXING_TYPE] [int] NULL,
	[HOLIDAYADJUSTMENT] [int] NULL,
	[STATUS] [int] NULL,
	[CREDIT_SPREAD_VOLAT] [int] NULL,
	[CREDIT_SPREAD_CORRELATION] [int] NULL,
	[QUOTATION_TYPE] [int] NULL,
	[SENIORITY] [int] NULL,
	[ISSUER] [int] NULL,
	[FAMILY] [int] NULL,
	[FREQ_COUPON] [int] NULL,
	[PERIMETERID] [int] NULL,
	[PREPAYMENT] [nvarchar](40) NULL,
	[UNDERLYING] [int] NULL,
	[ISMANAGEMENTFEE] [int] NULL,
	[ISPERFORMANCEFEE] [int] NULL,
	[MORNINGSTAR] [varchar](25) NULL,
 CONSTRAINT [PK_sophis_TITRES_Override] PRIMARY KEY CLUSTERED 
(
	[SICOVAM] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[sophis_TRANSACTIONS]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sophis_TRANSACTIONS]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[sophis_TRANSACTIONS](
	[FUNDID] [int] NOT NULL,
	[SUBFUND] [int] NOT NULL,
	[INSTRUMENTID] [int] NOT NULL,
	[TRADEID] [int] NOT NULL,
	[TRADE_LEG] [int] NOT NULL,
	[VERSION] [int] NOT NULL,
	[LIBELLE] [nvarchar](100) NOT NULL,
	[HOLDING] [float] NOT NULL,
	[SETTLEMENT] [float] NOT NULL,
	[TYPE] [int] NOT NULL,
	[FEES] [float] NOT NULL,
	[DEVISEPAY] [int] NOT NULL,
	[DEVISECTT] [int] NULL,
	[MARCHE] [int] NOT NULL,
	[BACKOFFICE] [int] NOT NULL,
	[MULTIPLIER] [float] NOT NULL,
	[REFERENCE_PRICE] [float] NOT NULL,
	[DATENEG] [date] NOT NULL,
	[HEURENEG] [int] NOT NULL,
	[DATEVAL] [date] NOT NULL,
	[DATECOMPTABLE] [date] NOT NULL,
	[DELIVERY_DATE] [date] NULL,
	[DATECOUPON] [date] NULL,
	[TKT_NAVDATE] [date] NULL,
	[ENTRY_DATE] [date] NULL,
	[DEPOSITAIRE] [int] NULL,
	[INFO] [nvarchar](250) NOT NULL,
	[INSTRUMENT_TYPE] [nvarchar](5) NULL,
	[REFERENCE] [nvarchar](50) NOT NULL,
	[ISIN] [nvarchar](50) NOT NULL,
	[QUERYFLAG] [int] NOT NULL,
	[UNIT_COUNT] [float] NOT NULL,
 CONSTRAINT [PK_sophis_TRANSACTIONS] PRIMARY KEY CLUSTERED 
(
	[TRADEID] ASC,
	[TRADE_LEG] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[sophis_TRANSACTIONS_ToIgnore]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sophis_TRANSACTIONS_ToIgnore]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[sophis_TRANSACTIONS_ToIgnore](
	[TradeID] [int] NOT NULL,
	[Note] [varchar](100) NULL,
 CONSTRAINT [PK_sophis_TRANSACTIONS_ToIgnore] PRIMARY KEY CLUSTERED 
(
	[TradeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_DevEntity]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_DevEntity]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_DevEntity](
	[OrgID] [int] NOT NULL,
	[NewId] [int] NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tbl_DevItem]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_DevItem]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_DevItem](
	[OrgID] [int] NOT NULL,
	[NewId] [int] NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblAttribution]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblAttribution]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblAttribution](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([RN]),
	[AttribID]  AS ([RN]),
	[AttribFund] [int] NOT NULL,
	[AttribInstrument] [int] NOT NULL,
	[AttribDate] [datetime] NOT NULL,
	[AttribPeriod] [int] NOT NULL,
	[AttribInvested] [float] NOT NULL,
	[AttribWeight] [float] NOT NULL,
	[AttribEndWeight] [float] NOT NULL,
	[AttribProfit] [float] NOT NULL,
	[AttribPercentReturn] [float] NOT NULL,
	[AttribBips] [float] NOT NULL,
	[AttribExpectedBips] [float] NOT NULL,
	[AttribTotalValue] [float] NOT NULL,
	[AttribFinalUnits] [float] NOT NULL,
	[AttribStatusGroupID] [int] NULL,
	[AttribDatesFilter] [int] NULL,
	[AttribIsMilestone] [int] NOT NULL,
	[AttribIsLookthrough] [int] NOT NULL,
	[AttribLookthroughGeneratedRecord] [int] NOT NULL,
	[AttribKnowledgedate] [datetime] NULL,
	[DateEntered] [datetime] NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblAttribution] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT DELETE ON [dbo].[tblAttribution] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblAttribution] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblAttribution] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblAttribution] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblAttribution] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT DELETE ON [dbo].[tblAttribution] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblAttribution] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblAttribution] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT DELETE ON [dbo].[tblAttribution] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblAttribution] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblAttribution] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblAttribution] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblAttribution] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblAutoUpdate]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblAutoUpdate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblAutoUpdate](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([RN]),
	[HAS_CURRENCY] [int] NOT NULL,
	[HAS_COUNTERPARTY] [int] NOT NULL,
	[HAS_DEALINGPERIOD] [int] NOT NULL,
	[HAS_FUND] [int] NOT NULL,
	[HAS_FUNDTYPE] [int] NOT NULL,
	[HAS_INSTRUMENT] [int] NOT NULL,
	[HAS_INSTRUMENTTYPE] [int] NOT NULL,
	[HAS_INVESTORGROUP] [int] NOT NULL,
	[HAS_LIMITS] [int] NOT NULL,
	[HAS_LIMITTYPE] [int] NOT NULL,
	[HAS_PERSON] [int] NOT NULL,
	[HAS_TRANSACTION] [int] NOT NULL,
	[HAS_TRANSACTIONTYPE] [int] NOT NULL,
	[HAS_ESTIMATE] [int] NOT NULL,
	[HAS_FX] [int] NOT NULL,
	[HAS_PRICE] [int] NOT NULL,
	[HAS_SYSTEMDATES] [int] NOT NULL,
	[HAS_PERMISSIONS] [int] NOT NULL,
	[HAS_KNOWLEDGEDATE] [int] NOT NULL,
	[HAS_FUNDMILESTONE] [int] NOT NULL,
	[HAS_CHANGECONTROL] [int] NOT NULL,
	[HAS_COMPOUNDTRANSACTION] [int] NOT NULL,
	[HAS_TRANSACTIONTEMPLATE] [int] NOT NULL,
	[HAS_BOOKMARK] [int] NOT NULL,
	[HAS_TRADEFILEPENDING] [int] NOT NULL,
	[HAS_TRADEFILEACKNOWLEDGEMENT] [int] NOT NULL,
	[HAS_TRADEFILECONFIRMATIONS] [int] NOT NULL,
 CONSTRAINT [PK_tblAutoUpdate] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT INSERT ON [dbo].[tblAutoUpdate] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblAutoUpdate] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblAutoUpdate] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblAutoUpdate] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblAutoUpdate] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblAutoUpdate] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblAutoUpdate] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblAutoUpdate] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblAutoUpdate] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblAutoUpdate] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblAutoUpdate] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblAutoUpdate] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblAutoUpdate] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblAutoUpdate] TO [Michigan_Role] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblAutoUpdate] TO [Michigan_Role] AS [dbo]
GO
/****** Object:  Table [dbo].[tblAutoUpdate_Detail]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblAutoUpdate_Detail]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblAutoUpdate_Detail](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([RN]),
	[DETAIL_CURRENCY] [varchar](2000) NULL,
	[DETAIL_COUNTERPARTY] [varchar](2000) NULL,
	[DETAIL_DEALINGPERIOD] [varchar](2000) NULL,
	[DETAIL_FUND] [varchar](2000) NULL,
	[DETAIL_FUNDTYPE] [varchar](2000) NULL,
	[DETAIL_INSTRUMENT] [varchar](2000) NULL,
	[DETAIL_INSTRUMENTTYPE] [varchar](2000) NULL,
	[DETAIL_INVESTORGROUP] [varchar](2000) NULL,
	[DETAIL_LIMITS] [varchar](2000) NULL,
	[DETAIL_LIMITTYPE] [varchar](2000) NULL,
	[DETAIL_PERSON] [varchar](2000) NULL,
	[DETAIL_TRANSACTION] [varchar](2000) NULL,
	[DETAIL_TRANSACTIONTYPE] [varchar](2000) NULL,
	[DETAIL_ESTIMATE] [varchar](2000) NULL,
	[DETAIL_FX] [varchar](2000) NULL,
	[DETAIL_PRICE] [varchar](2000) NULL,
	[DETAIL_SYSTEMDATES] [varchar](2000) NULL,
	[DETAIL_PERMISSIONS] [varchar](2000) NULL,
	[DETAIL_KNOWLEDGEDATE] [varchar](2000) NULL,
	[DETAIL_FUNDMILESTONE] [varchar](2000) NULL,
	[DETAIL_CHANGECONTROL] [varchar](2000) NULL,
	[DETAIL_COMPOUNDTRANSACTION] [varchar](2000) NULL,
	[DETAIL_TRANSACTIONTEMPLATE] [varchar](2000) NULL,
	[DETAIL_BOOKMARK] [varchar](2000) NULL,
	[DETAIL_TRADEFILEPENDING] [varchar](2000) NULL,
	[DETAIL_TRADEFILEACKNOWLEDGEMENT] [varchar](2000) NULL,
	[DETAIL_TRADEFILECONFIRMATIONS] [varchar](2000) NULL,
 CONSTRAINT [PK_tblAutoUpdate_Detail] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblBenchmarkIndex]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblBenchmarkIndex]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblBenchmarkIndex](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([BenchmarkID]),
	[BenchmarkID] [int] NOT NULL,
	[BenchmarkDescription] [nvarchar](100) NOT NULL,
	[BenchmarkType] [int] NOT NULL,
	[BenchmarkCalculationMethod] [int] NOT NULL,
	[UserEntered] [nvarchar](100) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblBenchmarkIndex] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT VIEW DEFINITION ON [dbo].[tblBenchmarkIndex] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblBenchmarkIndex] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblBenchmarkItems]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblBenchmarkItems]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblBenchmarkItems](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([BenchmarkItemID]),
	[BenchmarkItemID] [int] NOT NULL,
	[BenchmarkID] [int] NOT NULL,
	[BenchmarkItemType] [int] NOT NULL,
	[BenchmarkItemCalculationMethod] [int] NULL,
	[BenchmarkInstrumentID] [int] NULL,
	[BenchmarkFixedRate] [float] NULL,
	[UserEntered] [nvarchar](100) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblBenchmarkItems] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblBookmark]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblBookmark]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblBookmark](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([BookmarkID]),
	[BookmarkID] [int] NOT NULL,
	[BookmarkUserID] [int] NOT NULL,
	[BookMarkDescription] [nvarchar](100) NOT NULL,
	[BookmarkDate] [datetime] NOT NULL,
	[BookmarkWholeDay] [int] NOT NULL,
	[BookmarkVisibleToAll] [int] NOT NULL,
	[UserEntered] [nvarchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblBookmarks] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT DELETE ON [dbo].[tblBookmark] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblBookmark] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblBookmark] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblBookmark] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblBookmark] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblBookmark] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblBookmark] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblBookmark] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblBookmark] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblBookmark] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblBookmark] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblBookmark] TO [web_server] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblBookmark] ([DateDeleted]) TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblBookmark] ([DateDeleted]) TO [InvestMaster_Manager] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblBookmark] ([DateDeleted]) TO [InvestMaster_Sales] AS [dbo]
GO
/****** Object:  Table [dbo].[tblBroker]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblBroker]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblBroker](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([BrokerID]),
	[BrokerID] [int] NOT NULL,
	[BrokerDescription] [nvarchar](200) NOT NULL,
	[NLS_Culture] [nvarchar](50) NOT NULL,
	[NumericPrecision] [int] NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblBroker] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT VIEW DEFINITION ON [dbo].[tblBroker] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblBroker] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblBrokerAccounts]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblBrokerAccounts]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblBrokerAccounts](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([VeniceAccountID]),
	[VeniceAccountID] [int] NOT NULL,
	[FundID] [int] NOT NULL,
	[InstrumentTypeID] [int] NOT NULL,
	[Exchange] [int] NOT NULL,
	[CurrencyID] [int] NOT NULL,
	[InstrumentID] [int] NOT NULL,
	[BrokerID] [int] NOT NULL,
	[AccountDescription] [nvarchar](200) NOT NULL,
	[AccountNumber] [nvarchar](50) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblBrokerAccounts] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT VIEW DEFINITION ON [dbo].[tblBrokerAccounts] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblBrokerAccounts] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblChangeControl]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblChangeControl]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblChangeControl](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([ChangeID]),
	[ChangeID] [int] NOT NULL,
	[Product] [nvarchar](50) NOT NULL,
	[Date_Raised] [datetime] NOT NULL,
	[User_Raised] [nvarchar](50) NOT NULL,
	[Change_Category] [nvarchar](50) NOT NULL,
	[Change_Priority] [int] NOT NULL,
	[Change_ReferenceNumber] [nvarchar](50) NOT NULL,
	[Text_Change] [text] NOT NULL,
	[Text_Detail] [text] NOT NULL,
	[Text_ScopeOfChange] [text] NOT NULL,
	[Text_Dependencies] [text] NOT NULL,
	[Text_Restrictions] [text] NOT NULL,
	[Text_ImplementationPlan] [text] NOT NULL,
	[Text_TestPlan] [text] NOT NULL,
	[Date_ReviewedIT] [datetime] NULL,
	[Date_ReviewRejected] [datetime] NULL,
	[Date_AuthorisedIT] [datetime] NULL,
	[Date_AuthorisedOwner] [datetime] NULL,
	[Date_AuthorisedBusiness] [datetime] NULL,
	[Date_AuthoriseRejected] [datetime] NULL,
	[Date_AcceptedIT] [datetime] NULL,
	[Date_AcceptedOwner] [datetime] NULL,
	[Date_AcceptedBusiness] [datetime] NULL,
	[User_ReviewedIT] [nvarchar](50) NOT NULL,
	[User_ReviewRejected] [nvarchar](50) NOT NULL,
	[User_AuthorisedIT] [nvarchar](50) NOT NULL,
	[User_AuthorisedOwner] [nvarchar](50) NOT NULL,
	[User_AuthorisedBusiness] [nvarchar](50) NOT NULL,
	[User_AuthoriseRejected] [nvarchar](50) NOT NULL,
	[User_AcceptedIT] [nvarchar](50) NOT NULL,
	[User_AcceptedOwner] [nvarchar](50) NOT NULL,
	[User_AcceptedBusiness] [nvarchar](50) NOT NULL,
	[Flag_ReviewedIT] [bit] NOT NULL,
	[Flag_ReviewRejected] [bit] NOT NULL,
	[Flag_AuthorisedIT] [bit] NOT NULL,
	[Flag_AuthorisedOwner] [bit] NOT NULL,
	[Flag_AuthorisedBusiness] [bit] NOT NULL,
	[Flag_AuthoriseRejected] [bit] NOT NULL,
	[Flag_AcceptedIT] [bit] NOT NULL,
	[Flag_AcceptedOwner] [bit] NOT NULL,
	[Flag_AcceptedBusiness] [bit] NOT NULL,
	[appCurrentUser] [nvarchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblChangeControl] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
GRANT SELECT ON [dbo].[tblChangeControl] TO [Florence_Read] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblChangeControl] TO [Genoa_Read] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblChangeControl] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblChangeControl] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblChangeControl] TO [Naples_Role] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblChangeControl] TO [Reporter] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblChangeControl] TO [Sienna_Read] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblChangeControl] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblCompoundTransaction]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCompoundTransaction]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblCompoundTransaction](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([TransactionCompoundID]),
	[AdminStatus] [int] NOT NULL,
	[TransactionParentID] [int] NOT NULL,
	[TransactionCompoundID] [int] NOT NULL,
	[TransactionTemplateID] [int] NOT NULL,
	[TransactionTicket] [nvarchar](50) NOT NULL,
	[TransactionFund] [int] NOT NULL,
	[TransactionInstrument] [int] NOT NULL,
	[TransactionType] [int] NOT NULL,
	[TransactionValueorAmount] [nvarchar](50) NOT NULL,
	[TransactionUnits] [float] NOT NULL,
	[TransactionPrice] [float] NOT NULL,
	[TransactionCosts] [float] NOT NULL,
	[TransactionCostPercent] [float] NOT NULL,
	[TransactionEffectivePrice] [float] NOT NULL,
	[TransactionEffectiveWatermark] [float] NOT NULL,
	[TransactionSpecificInitialEqualisationValue] [float] NOT NULL,
	[TransactionCounterparty] [int] NOT NULL,
	[TransactionInvestment] [nvarchar](50) NOT NULL,
	[TransactionExecution] [nvarchar](50) NOT NULL,
	[TransactionDataEntry] [nvarchar](50) NOT NULL,
	[TransactionDecisionDate] [datetime] NOT NULL,
	[TransactionValueDate] [datetime] NOT NULL,
	[TransactionFIFOValueDate] [datetime] NOT NULL,
	[TransactionEffectiveValueDate] [datetime] NULL,
	[TransactionSettlementDate] [datetime] NOT NULL,
	[TransactionConfirmationDate] [datetime] NOT NULL,
	[TransactionCostIsPercent] [bit] NOT NULL,
	[TransactionExemptFromUpdate] [bit] NOT NULL,
	[TransactionIsTransfer] [bit] NOT NULL,
	[TransactionSpecificInitialEqualisationFlag] [bit] NOT NULL,
	[TransactionFinalAmount] [int] NOT NULL,
	[TransactionFinalPrice] [int] NOT NULL,
	[TransactionFinalCosts] [int] NOT NULL,
	[TransactionFinalSettlement] [int] NOT NULL,
	[TransactionInstructionFlag] [int] NOT NULL,
	[TransactionBankConfirmation] [int] NOT NULL,
	[TransactionInitialDataEntry] [int] NOT NULL,
	[TransactionAmountConfirmed] [int] NOT NULL,
	[TransactionSettlementConfirmed] [int] NOT NULL,
	[TransactionFinalDataEntry] [int] NOT NULL,
	[TransactionTradeStatusID] [int] NULL,
	[TransactionComment] [nvarchar](500) NOT NULL,
	[TransactionGroup] [nvarchar](50) NULL,
	[ParameterCount] [int] NOT NULL,
	[Parameter1_Name] [nvarchar](50) NULL,
	[Parameter1_Type] [int] NOT NULL,
	[Parameter1_Value] [nvarchar](50) NULL,
	[Parameter2_Name] [nvarchar](50) NULL,
	[Parameter2_Type] [int] NOT NULL,
	[Parameter2_Value] [nvarchar](50) NULL,
	[Parameter3_Name] [nvarchar](50) NULL,
	[Parameter3_Type] [int] NOT NULL,
	[Parameter3_Value] [nvarchar](50) NULL,
	[Parameter4_Name] [nvarchar](50) NULL,
	[Parameter4_Type] [int] NOT NULL,
	[Parameter4_Value] [nvarchar](50) NULL,
	[Parameter5_Name] [nvarchar](50) NULL,
	[Parameter5_Type] [int] NOT NULL,
	[Parameter5_Value] [nvarchar](50) NULL,
	[CompoundTransactionGroup] [int] NOT NULL,
	[CompoundTransactionComment] [nvarchar](50) NULL,
	[CompoundTransactionFlags] [int] NOT NULL,
	[CompoundTransactionPosted] [int] NOT NULL,
	[UserEntered] [nvarchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblCompoundTransaction] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT INSERT ON [dbo].[tblCompoundTransaction] TO [InvestMaster_AddTransaction] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblCompoundTransaction] TO [InvestMaster_AddTransaction] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblCompoundTransaction] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblCompoundTransaction] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblCompoundTransaction] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblCompoundTransaction] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblCompoundTransaction] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblCompoundTransaction] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblCompoundTransaction] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblCompoundTransaction] TO [web_server] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblCompoundTransaction] ([CompoundTransactionPosted]) TO [InvestMaster_AddTransaction] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblCompoundTransaction] ([CompoundTransactionPosted]) TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblCompoundTransaction] ([CompoundTransactionPosted]) TO [InvestMaster_Manager] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblCompoundTransaction] ([DateDeleted]) TO [InvestMaster_AddTransaction] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblCompoundTransaction] ([DateDeleted]) TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblCompoundTransaction] ([DateDeleted]) TO [InvestMaster_Manager] AS [dbo]
GO
/****** Object:  Table [dbo].[tblCompoundTransactionParameters]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCompoundTransactionParameters]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblCompoundTransactionParameters](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([TransactionCompoundID]),
	[TransactionCompoundID] [int] NOT NULL,
	[ParamName] [nvarchar](20) NOT NULL,
	[ParamType] [int] NOT NULL,
	[ParamLong] [int] NOT NULL,
	[ParamString] [nvarchar](30) NOT NULL,
	[ParamFloat] [float] NOT NULL,
	[UserEntered] [nvarchar](30) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblCompoundTransactionParameters] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT INSERT ON [dbo].[tblCompoundTransactionParameters] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblCompoundTransactionParameters] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblCompoundTransactionParameters] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblCompoundTransactionParameters] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblCompoundTransactionParameters] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblCompoundTransactionParameters] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblCompoundTransactionParameters] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblCompoundTransactionParameters] TO [web_server] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblCompoundTransactionParameters] ([DateDeleted]) TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblCompoundTransactionParameters] ([DateDeleted]) TO [InvestMaster_Manager] AS [dbo]
GO
/****** Object:  Table [dbo].[tblCompoundTransactionTemplate]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCompoundTransactionTemplate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblCompoundTransactionTemplate](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([TemplateID]),
	[TemplateID] [int] NOT NULL,
	[TemplateDescription] [nvarchar](100) NOT NULL,
	[TemplateStep] [int] NOT NULL,
	[TemplateTotalSteps] [int] NOT NULL,
	[TemplateFlags] [int] NOT NULL,
	[TransactionTicket] [nvarchar](400) NOT NULL,
	[TransactionFund] [nvarchar](400) NOT NULL,
	[TransactionInstrument] [nvarchar](400) NOT NULL,
	[TransactionType] [nvarchar](400) NOT NULL,
	[TransactionValueorAmount] [nvarchar](400) NOT NULL,
	[TransactionUnits] [nvarchar](400) NOT NULL,
	[TransactionPrice] [nvarchar](400) NOT NULL,
	[TransactionEffectivePrice] [nvarchar](400) NOT NULL,
	[TransactionCosts] [nvarchar](400) NOT NULL,
	[TransactionCostPercent] [nvarchar](400) NOT NULL,
	[TransactionSettlement] [nvarchar](400) NOT NULL,
	[TransactionCounterparty] [nvarchar](400) NOT NULL,
	[TransactionDecisionDate] [nvarchar](400) NOT NULL,
	[TransactionValueDate] [nvarchar](400) NOT NULL,
	[TransactionFIFOValueDate] [nvarchar](400) NOT NULL,
	[TransactionEffectiveValueDate] [nvarchar](400) NOT NULL,
	[TransactionSettlementDate] [nvarchar](400) NOT NULL,
	[TransactionConfirmationDate] [nvarchar](400) NOT NULL,
	[TransactionEffectiveWatermark] [nvarchar](400) NOT NULL,
	[TransactionSpecificInitialEqualisationValue] [nvarchar](400) NOT NULL,
	[TransactionCostIsPercent] [nvarchar](400) NOT NULL,
	[TransactionIsTransfer] [nvarchar](400) NOT NULL,
	[TransactionSpecificInitialEqualisationFlag] [nvarchar](400) NOT NULL,
	[TransactionComment] [nvarchar](400) NOT NULL,
	[TransactionGroup] [nvarchar](400) NOT NULL,
	[R1] [nvarchar](400) NOT NULL,
	[R2] [nvarchar](400) NOT NULL,
	[UserEntered] [nvarchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblCompoundTransactionTemplate] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT INSERT ON [dbo].[tblCompoundTransactionTemplate] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblCompoundTransactionTemplate] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblCompoundTransactionTemplate] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblCompoundTransactionTemplate] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblCompoundTransactionTemplate] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblCompoundTransactionTemplate] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblCompoundTransactionTemplate] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblCompoundTransactionTemplate] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblCounterparty]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCounterparty]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblCounterparty](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([CounterpartyID]),
	[CounterpartyID] [int] NOT NULL,
	[CounterpartyName] [nvarchar](100) NOT NULL,
	[CounterpartyPFPCID] [nvarchar](50) NOT NULL,
	[CounterpartyInvestorGroup] [int] NOT NULL,
	[CounterpartyManagementFee] [float] NOT NULL,
	[CounterpartyPerformanceFee] [float] NOT NULL,
	[CounterpartyPerformanceFeeThreshold] [float] NOT NULL,
	[Fee_ManagementEquityRebate] [float] NOT NULL,
	[Fee_ManagementDebtRebate] [float] NOT NULL,
	[Fee_PerformanceEquityRebate] [float] NOT NULL,
	[Fee_PerformanceDebtRebate] [float] NOT NULL,
	[Fee_ManagementMarketingToFCAM] [float] NOT NULL,
	[Fee_PerformanceMarketingToFCAM] [float] NOT NULL,
	[Fee_ManagementMarketingToOthers] [float] NOT NULL,
	[Fee_PerformanceMarketingToOthers] [float] NOT NULL,
	[CounterpartyFundID] [int] NOT NULL,
	[CounterpartyLeverageProvider] [int] NOT NULL,
	[CounterpartyAddress1] [nvarchar](50) NOT NULL,
	[CounterpartyAddress2] [nvarchar](50) NOT NULL,
	[CounterpartyAddress3] [nvarchar](50) NOT NULL,
	[CounterpartyAddress4] [nvarchar](50) NOT NULL,
	[CounterpartyAddress5] [nvarchar](50) NOT NULL,
	[CounterpartyPostCode] [nvarchar](50) NOT NULL,
	[CounterpartyContactName] [nvarchar](100) NOT NULL,
	[CounterpartyAccountName] [nvarchar](100) NOT NULL,
	[CounterpartyEMail] [nvarchar](50) NOT NULL,
	[CounterpartyPhoneNumber] [nvarchar](50) NOT NULL,
	[CounterpartyFaxNumber] [nvarchar](50) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblCounterparty] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT INSERT ON [dbo].[tblCounterparty] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblCounterparty] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblCounterparty] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblCounterparty] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblCounterparty] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblCounterparty] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblCounterparty] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblCounterparty] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblCounterparty] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblCovariance]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCovariance]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblCovariance](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([CovID]),
	[CovID] [int] NOT NULL,
	[CovListID] [int] NOT NULL,
	[PertracID1] [int] NOT NULL,
	[PertracID2] [int] NOT NULL,
	[Covariance] [float] NOT NULL,
	[Correlation] [float] NOT NULL,
	[UserEntered] [nvarchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblCovariance] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT VIEW DEFINITION ON [dbo].[tblCovariance] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblCovariance] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblCovarianceList]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCovarianceList]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblCovarianceList](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([CovListID]),
	[CovListID] [int] NOT NULL,
	[ListName] [nvarchar](100) NOT NULL,
	[GroupListID] [int] NOT NULL,
	[DateFrom] [datetime] NOT NULL,
	[DateTo] [datetime] NOT NULL,
	[Lamda] [float] NOT NULL,
	[DataPeriod] [int] NOT NULL,
	[IsAnnualised] [bit] NOT NULL,
	[UserEntered] [nvarchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblCovarianceList] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT VIEW DEFINITION ON [dbo].[tblCovarianceList] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblCovarianceList] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblCTA_Folders]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCTA_Folders]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblCTA_Folders](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([FolderId]),
	[FolderID] [int] NOT NULL,
	[ParentFolderId] [int] NOT NULL,
	[FolderName] [nvarchar](50) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblCTA_Folders] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT VIEW DEFINITION ON [dbo].[tblCTA_Folders] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblCTA_Folders] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblCTA_Folders_13Feb2013]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCTA_Folders_13Feb2013]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblCTA_Folders_13Feb2013](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID] [int] NOT NULL,
	[FolderID] [int] NOT NULL,
	[ParentFolderId] [int] NOT NULL,
	[FolderName] [nvarchar](50) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblCTA_Simulation]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCTA_Simulation]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblCTA_Simulation](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([SimulationID]),
	[SimulationID] [int] NOT NULL,
	[InstrumentID] [int] NOT NULL,
	[InterestRateID] [int] NOT NULL,
	[SimulationName] [nvarchar](200) NOT NULL,
	[SimulationFolder] [int] NOT NULL,
	[SimulationDataPeriod] [int] NOT NULL,
	[SimulationDefinition] [text] NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblCTA_Simulation] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
GRANT VIEW DEFINITION ON [dbo].[tblCTA_Simulation] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblCTA_Simulation] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblCTA_Simulation_13Feb2013]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCTA_Simulation_13Feb2013]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblCTA_Simulation_13Feb2013](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID] [int] NOT NULL,
	[SimulationID] [int] NOT NULL,
	[InstrumentID] [int] NOT NULL,
	[InterestRateID] [int] NOT NULL,
	[SimulationName] [nvarchar](200) NOT NULL,
	[SimulationFolder] [int] NOT NULL,
	[SimulationDataPeriod] [int] NOT NULL,
	[SimulationDefinition] [text] NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblCurrency]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCurrency]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblCurrency](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([CurrencyID]),
	[CurrencyID] [int] NOT NULL,
	[CurrencyCashInstrumentID] [int] NOT NULL,
	[CurrencyFuturesNominalInstrumentID] [int] NOT NULL,
	[DefaultExpenseInstrument] [int] NOT NULL,
	[DefaultFeeInstrument] [int] NOT NULL,
	[DefaultBrokerageExpense] [int] NOT NULL,
	[CurrencyCode] [nvarchar](50) NOT NULL,
	[CurrencyDescription] [nvarchar](50) NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblCurrency] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT INSERT ON [dbo].[tblCurrency] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblCurrency] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblCurrency] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblCurrency] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblCurrency] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblCurrency] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblCurrency] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblCurrency] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblCurrency] TO [web_server] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblCurrency] ([DateDeleted]) TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblCurrency] ([DateDeleted]) TO [InvestMaster_Manager] AS [dbo]
GO
/****** Object:  Table [dbo].[tblDataDictionary_TransactionFormLabels]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblDataDictionary_TransactionFormLabels]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblDataDictionary_TransactionFormLabels](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([RN]),
	[TransactionType] [int] NOT NULL,
	[CounterpartyText] [nvarchar](50) NOT NULL,
	[CompoundFlag] [int] NOT NULL,
	[TransactionAmount] [nvarchar](50) NOT NULL,
	[TransactionPrice] [nvarchar](50) NOT NULL,
	[TransactionCosts] [nvarchar](50) NOT NULL,
	[TransactionSettlement] [nvarchar](50) NOT NULL,
	[DecisionDateText] [nvarchar](50) NOT NULL,
	[TradeDateText] [nvarchar](50) NOT NULL,
	[SettlementDateText] [nvarchar](50) NOT NULL,
	[ConfirmationDateText] [nvarchar](50) NOT NULL,
	[FieldText1] [nvarchar](50) NOT NULL,
	[FieldText2] [nvarchar](50) NOT NULL,
	[FieldText3] [nvarchar](50) NOT NULL,
	[FieldText4] [nvarchar](50) NOT NULL,
	[FieldText5] [nvarchar](50) NOT NULL,
	[FieldText6] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_tblDataDictionary_TransactionFormLabels] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT DELETE ON [dbo].[tblDataDictionary_TransactionFormLabels] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblDataDictionary_TransactionFormLabels] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblDataDictionary_TransactionFormLabels] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblDataDictionary_TransactionFormLabels] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblDataDictionary_TransactionFormLabels] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblDataDictionary_TransactionFormLabels] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblDataDictionary_TransactionFormLabels] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblDataDictionary_TransactionFormLabels] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblDataProviderInstrumentType]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblDataProviderInstrumentType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblDataProviderInstrumentType](
	[RN] [int] IDENTITY(0,1) NOT NULL,
	[AuditID]  AS ([RN]),
	[TypeID]  AS ([RN]),
	[TypeDescription] [varchar](20) NOT NULL,
	[TickerMnemonic] [varchar](20) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT DELETE ON [dbo].[tblDataProviderInstrumentType] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblDataProviderInstrumentType] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblDataProviderInstrumentType] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblDataProviderInstrumentType] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblDataProviderInstrumentType] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblDataProviderInstrumentType] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblDataProviderInstrumentType] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblDataProviderInstrumentType] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblDataTask]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblDataTask]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblDataTask](
	[RN] [bigint] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([TaskID]),
	[TaskID] [int] NOT NULL,
	[TaskDescription] [nvarchar](50) NOT NULL,
	[TaskType] [int] NOT NULL,
	[InstrumentID] [int] NOT NULL,
	[SecurityTicker] [nvarchar](50) NOT NULL,
	[TaskDefinition] [text] NOT NULL,
	[UserEntered] [nvarchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblDataTask] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
GRANT INSERT ON [dbo].[tblDataTask] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblDataTask] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblDataTask] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblDataTask] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblDataTask] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblDataTask] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblDataTask] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblDataTask] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblDataTask] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblDataTaskType]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblDataTaskType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblDataTaskType](
	[RN] [bigint] IDENTITY(1,1) NOT NULL,
	[TaskType] [int] NOT NULL,
	[TaskDescription] [nvarchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblDataTaskType] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT SELECT ON [dbo].[tblDataTaskType] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblDataTaskType] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblDataTaskType] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblDealingPeriod]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblDealingPeriod]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblDealingPeriod](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([DealingPeriodID]),
	[DealingPeriodID] [int] NOT NULL,
	[DealingPeriodDescription] [nvarchar](50) NULL,
	[DealingPeriodDays] [nvarchar](50) NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblDealingPeriod] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT INSERT ON [dbo].[tblDealingPeriod] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblDealingPeriod] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblDealingPeriod] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblDealingPeriod] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblDealingPeriod] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblDealingPeriod] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblDealingPeriod] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblDealingPeriod] TO [web_server] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblDealingPeriod] ([DateDeleted]) TO [InvestMaster_Admin] AS [dbo]
GO
/****** Object:  Table [dbo].[tblEstimate]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblEstimate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblEstimate](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([EstimateID]),
	[EstimateID] [int] NOT NULL,
	[EstimateFund] [int] NOT NULL,
	[EstimateItem] [nvarchar](50) NOT NULL,
	[EstimateDate] [datetime] NOT NULL,
	[EstimateAmount] [nvarchar](50) NOT NULL,
	[EstimateUser] [nvarchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblEstimates] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT INSERT ON [dbo].[tblEstimate] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblEstimate] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblEstimate] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblEstimate] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblEstimate] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblEstimate] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblEstimate] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblEstimate] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblEstimate] TO [web_server] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblEstimate] ([DateDeleted]) TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblEstimate] ([DateDeleted]) TO [InvestMaster_Manager] AS [dbo]
GO
/****** Object:  Table [dbo].[tblExposure]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblExposure]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblExposure](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([ExpID]),
	[expID] [int] NULL,
	[expDate] [datetime] NULL,
	[expIsDetailRow] [int] NOT NULL,
	[expFund] [int] NULL,
	[expLimitID] [int] NOT NULL,
	[expLimitType] [int] NULL,
	[expLimitCharacteristic] [int] NOT NULL,
	[expLimitInstrument] [int] NULL,
	[expDetailInstrument] [int] NULL,
	[expLimitSector] [int] NULL,
	[expLimitSectorName] [varchar](100) NOT NULL,
	[expLimitDealingPeriod] [int] NOT NULL,
	[expLimitGreaterOrLessThan] [int] NULL,
	[expLimit] [float] NULL,
	[expExposure] [float] NULL,
	[expUsage] [float] NULL,
	[expStatus] [nvarchar](50) NULL,
	[expDateCalculated] [datetime] NULL,
	[expKnowledgeDate] [datetime] NULL,
	[expKnowledgeDateWholeday] [int] NOT NULL,
	[expSystemKnowledgeDate] [datetime] NOT NULL,
	[expSystemKnowledgeDateWholeday] [int] NOT NULL,
	[expFlags] [int] NOT NULL,
 CONSTRAINT [PK_tblExposure] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT DELETE ON [dbo].[tblExposure] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblExposure] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblExposure] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblExposure] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT DELETE ON [dbo].[tblExposure] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblExposure] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblExposure] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblExposure] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT DELETE ON [dbo].[tblExposure] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblExposure] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblExposure] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblExposure] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT DELETE ON [dbo].[tblExposure] TO [InvestMaster_RiskManager] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblExposure] TO [InvestMaster_RiskManager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblExposure] TO [InvestMaster_RiskManager] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblExposure] TO [InvestMaster_RiskManager] AS [dbo]
GO
GRANT DELETE ON [dbo].[tblExposure] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblExposure] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblExposure] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblExposure] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblExposure] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblExposure] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblFlorenceDataValues]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblFlorenceDataValues]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblFlorenceDataValues](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([DataValueID]),
	[DataValueID] [int] NOT NULL,
	[DataEntityID] [int] NOT NULL,
	[DataItemID] [int] NOT NULL,
	[DataValueDate] [datetime] NOT NULL,
	[DataValueType] [int] NOT NULL,
	[TextData] [text] NULL,
	[NumericData] [float] NULL,
	[PercentageData] [float] NULL,
	[DateData] [datetime] NULL,
	[BooleanData] [bit] NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblFlorenceDataValues] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
GRANT SELECT ON [dbo].[tblFlorenceDataValues] TO [Florence_Read] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblFlorenceDataValues] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblFlorenceDataValues] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblFlorenceEntity]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblFlorenceEntity]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblFlorenceEntity](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([EntityID]),
	[EntityID] [int] NOT NULL,
	[EntityName] [varchar](100) NOT NULL,
	[EntityIsInactive] [bit] NOT NULL,
	[ItemGroupID] [int] NOT NULL,
	[InstrumentID] [int] NOT NULL,
	[AdminContact] [varchar](100) NOT NULL,
	[AdminEmailSalutation] [varchar](50) NOT NULL,
	[AdminPhone] [varchar](50) NOT NULL,
	[AdminEMail] [varchar](50) NOT NULL,
	[AdminFax] [varchar](50) NOT NULL,
	[DDStatus] [int] NOT NULL,
	[DDStatusComment] [nvarchar](100) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblFlorenceEntity] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT SELECT ON [dbo].[tblFlorenceEntity] TO [Florence_Read] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblFlorenceEntity] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblFlorenceEntity] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblFlorenceGroup]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblFlorenceGroup]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblFlorenceGroup](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([ItemGroup]),
	[ItemGroup] [int] NOT NULL,
	[ItemGroupDescription] [varchar](100) NOT NULL,
	[LegacyGroup] [int] NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblFlorenceGroup] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT SELECT ON [dbo].[tblFlorenceGroup] TO [Florence_Read] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblFlorenceGroup] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblFlorenceGroup] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblFlorenceItems]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblFlorenceItems]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblFlorenceItems](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([ItemID]),
	[ItemID] [int] NOT NULL,
	[Category] [varchar](50) NOT NULL,
	[ItemDescription] [varchar](100) NOT NULL,
	[UpdatePeriod] [int] NOT NULL,
	[DataUpdatePeriod] [int] NOT NULL,
	[ItemMessage] [varchar](500) NOT NULL,
	[ItemGroup] [int] NOT NULL,
	[ItemDiscontinued] [int] NOT NULL,
	[KeyDetail] [int] NOT NULL,
	[IsDataItem] [int] NOT NULL,
	[DataItemType] [int] NOT NULL,
	[UpdateStatusOnDataUpdate] [bit] NOT NULL,
	[IsLimitItem] [int] NOT NULL,
	[LimitType] [int] NOT NULL,
	[LimitIsPercent] [bit] NOT NULL,
	[FromLimit] [float] NOT NULL,
	[ToLimit] [float] NOT NULL,
	[LimitThreshold] [float] NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblFlorenceItems] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT SELECT ON [dbo].[tblFlorenceItems] TO [Florence_Read] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblFlorenceItems] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblFlorenceItems] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblFlorenceStatus]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblFlorenceStatus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblFlorenceStatus](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([StatusID]),
	[StatusID] [int] NOT NULL,
	[EntityID] [int] NOT NULL,
	[ItemID] [int] NOT NULL,
	[ItemStatusID] [int] NOT NULL,
	[DateCompleted] [datetime] NOT NULL,
	[Comment] [text] NOT NULL,
	[UserEntered] [varchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblFlorenceStatus] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT SELECT ON [dbo].[tblFlorenceStatus] TO [Florence_Read] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblFlorenceStatus] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblFlorenceStatus] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblFlorenceStatusIDs]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblFlorenceStatusIDs]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblFlorenceStatusIDs](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([ItemStatusID]),
	[ItemStatusID] [int] NOT NULL,
	[ItemStatusDescription] [varchar](50) NOT NULL,
	[ItemIsComplete] [int] NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblFlorenceStatusIDs] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT SELECT ON [dbo].[tblFlorenceStatusIDs] TO [Florence_Read] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblFlorenceStatusIDs] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblFlorenceStatusIDs] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblFund]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblFund]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblFund](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([FundID]),
	[FundID] [int] NOT NULL,
	[FundCode] [nvarchar](100) NOT NULL,
	[FundName] [nvarchar](100) NOT NULL,
	[FundLegalEntity] [nvarchar](100) NOT NULL,
	[FundBaseCurrency] [int] NOT NULL,
	[FundPricingPeriod] [int] NULL,
	[FundUnit] [int] NOT NULL,
	[FundParentFund] [int] NOT NULL,
	[FundYearEnd] [date] NULL,
	[FundDefaultTradeFXToFundCurrency] [bit] NOT NULL,
	[FundUser] [nvarchar](50) NOT NULL,
	[FundManagementFeeInstrument] [int] NULL,
	[FundPerformanceFeeInstrument] [int] NULL,
	[FundPerformanceFeeCrystalisedInstrument] [int] NULL,
	[FundManagementFees] [float] NOT NULL,
	[FundPerformanceVsBenchmark] [int] NULL,
	[FundPerformanceFees] [float] NOT NULL,
	[FundPerformanceHurdle] [float] NOT NULL,
	[FundUsesEqualisation] [int] NULL,
	[FundManagementFeesAccruePeriod] [int] NULL,
	[FundManagementFeesPaymentPeriod] [int] NULL,
	[FundPerformanceFeesAccruePeriod] [int] NULL,
	[FundPerformanceFeesPaymentPeriod] [int] NULL,
	[FundSubscriptionNAVDatePeriod] [int] NOT NULL,
	[FundSubscriptionSettlementDatePeriod] [int] NOT NULL,
	[FundRedemptionNAVDatePeriod] [int] NOT NULL,
	[FundRedemptionSettlementDatePeriod] [int] NOT NULL,
	[FundReportGroup] [nvarchar](50) NOT NULL,
	[FundInitialSubscriptionFee] [nvarchar](50) NOT NULL,
	[FundManagementFeeText] [nvarchar](50) NOT NULL,
	[FundPerformanceFeeText] [nvarchar](50) NOT NULL,
	[FundManagementCompany] [nvarchar](50) NOT NULL,
	[FundManagers] [nvarchar](100) NOT NULL,
	[FundContactAddress] [nvarchar](100) NOT NULL,
	[FundContactPhone] [nvarchar](50) NOT NULL,
	[FundContactEmail] [nvarchar](50) NOT NULL,
	[FundMinimumInvestment] [nvarchar](50) NOT NULL,
	[FundSubscriptionPeriod] [nvarchar](50) NOT NULL,
	[FundSubscriptionNotice] [nvarchar](50) NOT NULL,
	[FundRedemptionPeriod] [nvarchar](50) NOT NULL,
	[FundRedemptionNotice] [nvarchar](50) NOT NULL,
	[FundJurisdiction] [nvarchar](50) NOT NULL,
	[FundAdministrator] [nvarchar](50) NOT NULL,
	[FundCustodian] [nvarchar](50) NOT NULL,
	[FundListings] [nvarchar](50) NOT NULL,
	[FundLinkedListedInstrument] [int] NOT NULL,
	[FundReviewGroup] [int] NOT NULL,
	[FundISIN] [nvarchar](50) NOT NULL,
	[FundSEDOL] [nvarchar](50) NOT NULL,
	[FundBloomberg] [nvarchar](50) NOT NULL,
	[FundReutersCode] [nvarchar](50) NOT NULL,
	[FundInvestmentStartDate] [datetime] NULL,
	[FundDisclaimer] [text] NULL,
	[FundObjective] [text] NULL,
	[FundAdministratorCode] [nvarchar](50) NULL,
	[FundClosed] [int] NOT NULL,
	[UserName] [nvarchar](100) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblFund] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
GRANT INSERT ON [dbo].[tblFund] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblFund] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblFund] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblFund] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblFund] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblFund] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblFund] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblFund] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblFund] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblFundContactDetails]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblFundContactDetails]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblFundContactDetails](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([FundID]),
	[FundID] [int] NOT NULL,
	[AdminContact] [varchar](100) NOT NULL,
	[AdminPhone] [varchar](50) NOT NULL,
	[AdminFax] [varchar](50) NOT NULL,
	[AdminMainEmail] [varchar](100) NOT NULL,
	[AdminSecondaryEmail] [varchar](100) NOT NULL,
	[EMailSalutation] [varchar](50) NOT NULL,
	[InvestorRelationsContact] [varchar](100) NOT NULL,
	[InvestorRelationsPhone] [varchar](50) NOT NULL,
	[InvestorRelationsEmail] [varchar](100) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblFundContactDetails] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT VIEW DEFINITION ON [dbo].[tblFundContactDetails] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblFundContactDetails] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblFundMilestones]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblFundMilestones]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblFundMilestones](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([MilestoneID]),
	[MileStoneID] [int] NOT NULL,
	[MilestoneFundID] [int] NOT NULL,
	[MilestoneDate] [datetime] NOT NULL,
	[MilestoneKnowledgeDate] [datetime] NOT NULL,
	[MilestoneLastDate] [datetime] NULL,
	[Fund_NAV] [float] NULL,
	[Fund_GNAV] [float] NULL,
	[Fund_BNAV] [float] NULL,
	[Fund_GAV] [float] NULL,
	[Fund_UnitsSubscribed] [float] NULL,
	[Fund_UnitsRedeemed] [float] NULL,
	[Administrator_NAV] [float] NULL,
	[Administrator_GNAV] [float] NULL,
	[Administrator_BNAV] [float] NULL,
	[Administrator_GAV] [float] NULL,
	[Administrator_UnitsSubscribed] [float] NULL,
	[Administrator_UnitsRedeemed] [float] NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblFundMilestones] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT INSERT ON [dbo].[tblFundMilestones] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblFundMilestones] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblFundMilestones] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblFundMilestones] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblFundMilestones] TO [InvestMaster_MonthEnd] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblFundMilestones] TO [InvestMaster_MonthEnd] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblFundMilestones] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblFundMilestones] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblFundMilestones] TO [web_server] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblFundMilestones] ([DateDeleted]) TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblFundMilestones] ([DateDeleted]) TO [InvestMaster_MonthEnd] AS [dbo]
GO
/****** Object:  Table [dbo].[tblFundPerformanceFeePoints]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblFundPerformanceFeePoints]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblFundPerformanceFeePoints](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([FeeID]),
	[FeeID] [int] NOT NULL,
	[FeeFund] [int] NOT NULL,
	[FeeDate] [datetime] NOT NULL,
	[FeeFundPrice] [float] NOT NULL,
	[UserEntered] [varchar](30) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblFundPerformanceFeePoints] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT INSERT ON [dbo].[tblFundPerformanceFeePoints] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblFundPerformanceFeePoints] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblFundPerformanceFeePoints] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblFundPerformanceFeePoints] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblFundPerformanceFeePoints] TO [InvestMaster_MonthEnd] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblFundPerformanceFeePoints] TO [InvestMaster_MonthEnd] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblFundPerformanceFeePoints] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblFundPerformanceFeePoints] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblFundPerformanceFeePoints] TO [web_server] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblFundPerformanceFeePoints] ([DateDeleted]) TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblFundPerformanceFeePoints] ([DateDeleted]) TO [InvestMaster_MonthEnd] AS [dbo]
GO
/****** Object:  Table [dbo].[tblFundType]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblFundType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblFundType](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([FundTypeID]),
	[FundTypeID] [int] NOT NULL,
	[FundTypeDescription] [nvarchar](100) NOT NULL,
	[FundTypeGroup] [varchar](100) NOT NULL,
	[FundTypePertracIndex] [int] NULL,
	[FundTypeShortTrackRecordVolatility] [float] NOT NULL,
	[FundTypeSortOrder] [int] NOT NULL,
	[FundTypeAttributionGrouping] [varchar](100) NOT NULL,
	[FundTypeIsAdministrativeOnly] [int] NOT NULL,
	[FundTypeReporterGrouping] [varchar](100) NOT NULL,
	[FundTypeShowInReporter] [bit] NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblFundType] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT INSERT ON [dbo].[tblFundType] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblFundType] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblFundType] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblFundType] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblFundType] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblFundType] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblFundType] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblFundType] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblFundType] TO [web_server] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblFundType] ([DateDeleted]) TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblFundType] ([DateDeleted]) TO [InvestMaster_Manager] AS [dbo]
GO
/****** Object:  Table [dbo].[tblFX]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblFX]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblFX](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([FXID]),
	[FXID] [int] NOT NULL,
	[FXCurrencyCode] [int] NOT NULL,
	[FXDate] [datetime] NOT NULL,
	[FXRate] [float] NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[DateEntered] [datetime] NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblFX] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT INSERT ON [dbo].[tblFX] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblFX] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblFX] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblFX] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblFX] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblFX] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblFX] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblFX] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblFX] TO [web_server] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblFX] ([DateDeleted]) TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblFX] ([DateDeleted]) TO [InvestMaster_Manager] AS [dbo]
GO
/****** Object:  Table [dbo].[tblGenoaConstraintItems]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblGenoaConstraintItems]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblGenoaConstraintItems](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([ConstraintID]),
	[ConstraintGroupID] [int] NOT NULL,
	[ConstraintID] [int] NOT NULL,
	[CustomFieldID] [int] NOT NULL,
	[TestFieldLessThan] [bit] NOT NULL,
	[TestFieldEqualTo] [bit] NOT NULL,
	[TestFieldGreaterThan] [bit] NOT NULL,
	[TestValueString] [varchar](50) NOT NULL,
	[TestValueNumeric] [float] NOT NULL,
	[TestValueDate] [datetime] NOT NULL,
	[LowerLimit] [float] NOT NULL,
	[UpperLimit] [float] NOT NULL,
	[UserEntered] [varchar](20) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblGenoaConstraintItems] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT VIEW DEFINITION ON [dbo].[tblGenoaConstraintItems] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblGenoaConstraintItems] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblGenoaConstraintList]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblGenoaConstraintList]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblGenoaConstraintList](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([ConstraintGroupID]),
	[ConstraintGroupID] [int] NOT NULL,
	[ConstraintGroupName] [varchar](100) NOT NULL,
	[UserEntered] [varchar](20) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblGenoaConstraintList] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT VIEW DEFINITION ON [dbo].[tblGenoaConstraintList] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblGenoaConstraintList] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblGenoaSavedSearchItems]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblGenoaSavedSearchItems]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblGenoaSavedSearchItems](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([SearchItemID]),
	[SearchID] [int] NOT NULL,
	[SearchItemID] [int] NOT NULL,
	[FieldIsStatic] [bit] NOT NULL,
	[FieldIsCustom] [bit] NOT NULL,
	[IsAND] [bit] NOT NULL,
	[IsOR] [bit] NOT NULL,
	[SelectFieldID] [varchar](50) NOT NULL,
	[ConditionOperand] [varchar](10) NOT NULL,
	[SelectValue1] [varchar](100) NOT NULL,
	[SelectValue2] [varchar](100) NOT NULL,
	[UserEntered] [varchar](20) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblGenoaSavedSearchItems] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT VIEW DEFINITION ON [dbo].[tblGenoaSavedSearchItems] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblGenoaSavedSearchItems] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblGenoaSavedSearchList]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblGenoaSavedSearchList]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblGenoaSavedSearchList](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([SearchID]),
	[SearchID] [int] NOT NULL,
	[SearchName] [varchar](100) NOT NULL,
	[VisibleToAllUsers] [bit] NOT NULL,
	[UserEntered] [varchar](20) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblGenoaSavedSearchList] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT VIEW DEFINITION ON [dbo].[tblGenoaSavedSearchList] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblGenoaSavedSearchList] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblGLPostingRules]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblGLPostingRules]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblGLPostingRules](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID] [int] NOT NULL,
	[PostingRuleID] [int] NOT NULL,
	[PostingKey]  AS (CONVERT([bigint],[Precedence],0)*(1073741824)+[ScenarioID]),
	[ScenarioKey] [bigint] NOT NULL,
	[ScenarioID] [int] NOT NULL,
	[Precedence] [int] NOT NULL,
	[PostingNumber] [int] NOT NULL,
	[DEBIT_Account] [int] NOT NULL,
	[DEBIT_TransferOfExistingValue] [bit] NOT NULL,
	[CREDIT_Account] [int] NOT NULL,
	[CREDIT_TransferOfExistingValue] [bit] NOT NULL,
	[MatchValueToPostingRuleID] [int] NOT NULL,
	[UserEntered] [varchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblGLPostingRules] PRIMARY KEY CLUSTERED 
(
	[PostingRuleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT VIEW DEFINITION ON [dbo].[tblGLPostingRules] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblGLPostingRules] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblGLPostings]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblGLPostings]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblGLPostings](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([RN]),
	[PostingID]  AS ([RN]),
	[PostingType] [int] NOT NULL,
	[PostingRuleID] [int] NOT NULL,
	[FundID] [int] NOT NULL,
	[InstrumentID] [int] NOT NULL,
	[OpeningTransactionParentID] [int] NOT NULL,
	[ClosingTransactionParentID] [int] NOT NULL,
	[AccountID] [int] NOT NULL,
	[ValueID] [int] NOT NULL,
	[PeriodYear] [int] NOT NULL,
	[PeriodNumber] [int] NOT NULL,
	[PostingValue] [float] NOT NULL,
	[PostingComment] [varchar](200) NULL,
	[UserEntered] [varchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
 CONSTRAINT [PK_tblGLPostings] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT VIEW DEFINITION ON [dbo].[tblGLPostings] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblGLPostings] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblGLPostingScenarios]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblGLPostingScenarios]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblGLPostingScenarios](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([ScenarioID]),
	[ScenarioID] [int] NOT NULL,
	[ScenarioKey]  AS ((((CONVERT([bigint],[Category1],0)*(256)+CONVERT([bigint],[Category2],0))*(256)+CONVERT([bigint],[Category3],0))*(256)+CONVERT([bigint],[Category4],0))*(65536)),
	[Category1_Enum] [varchar](50) NOT NULL,
	[Category1] [tinyint] NOT NULL,
	[Category2_Enum] [varchar](50) NOT NULL,
	[Category2] [tinyint] NOT NULL,
	[Category3_Enum] [varchar](50) NOT NULL,
	[Category3] [tinyint] NOT NULL,
	[Category4_Enum] [varchar](50) NOT NULL,
	[Category4] [tinyint] NOT NULL,
	[Category5_Enum] [varchar](50) NOT NULL,
	[Category5] [tinyint] NOT NULL,
	[Category6_Enum] [varchar](50) NOT NULL,
	[Category6] [tinyint] NOT NULL,
	[PostingValue_Enum] [varchar](50) NOT NULL,
	[PostingValue] [int] NOT NULL,
	[UserEntered] [varchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblGLPostingScenarios] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT VIEW DEFINITION ON [dbo].[tblGLPostingScenarios] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblGLPostingScenarios] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblGreeks]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblGreeks]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblGreeks](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([GreekID]),
	[GreekID] [int] NOT NULL,
	[InstrumentID] [int] NOT NULL,
	[GreekDate] [smalldatetime] NOT NULL,
	[Delta] [float] NOT NULL,
	[Gamma] [float] NOT NULL,
	[Rho] [float] NOT NULL,
	[Vega] [float] NOT NULL,
	[Theta] [float] NOT NULL,
	[Volatility] [float] NOT NULL,
	[UserEntered] [nvarchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblGreeks] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT INSERT ON [dbo].[tblGreeks] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblGreeks] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblGreeks] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblGreeks] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblGreeks] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblGreeks] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblGreeks] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblGreeks] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblGreeks] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblGroupItemData]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblGroupItemData]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblGroupItemData](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([GroupItemID]),
	[GroupItemID] [int] NOT NULL,
	[GroupPertracCode] [int] NOT NULL,
	[GroupLiquidity] [int] NOT NULL,
	[GroupHolding] [float] NOT NULL,
	[GroupNewHolding] [float] NOT NULL,
	[GroupPercent] [float] NOT NULL,
	[GroupExpectedReturn] [float] NOT NULL,
	[GroupUpperBound] [float] NOT NULL,
	[GroupLowerBound] [float] NOT NULL,
	[GroupFloor] [bit] NOT NULL,
	[GroupCap] [bit] NOT NULL,
	[GroupTradeSize] [float] NOT NULL,
	[GroupBeta] [float] NOT NULL,
	[GroupAlpha] [float] NOT NULL,
	[GroupIndexE] [float] NOT NULL,
	[GroupBetaE] [float] NOT NULL,
	[GroupAlphaE] [float] NOT NULL,
	[GroupStdErr] [float] NOT NULL,
	[GroupScalingFactor] [float] NOT NULL,
	[UserEntered] [varchar](20) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblGroupItemData] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT VIEW DEFINITION ON [dbo].[tblGroupItemData] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblGroupItemData] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblGroupItemData_13Feb2013]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblGroupItemData_13Feb2013]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblGroupItemData_13Feb2013](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID] [int] NOT NULL,
	[GroupItemID] [int] NOT NULL,
	[GroupPertracCode] [int] NOT NULL,
	[GroupLiquidity] [int] NOT NULL,
	[GroupHolding] [float] NOT NULL,
	[GroupNewHolding] [float] NOT NULL,
	[GroupPercent] [float] NOT NULL,
	[GroupExpectedReturn] [float] NOT NULL,
	[GroupUpperBound] [float] NOT NULL,
	[GroupLowerBound] [float] NOT NULL,
	[GroupFloor] [bit] NOT NULL,
	[GroupCap] [bit] NOT NULL,
	[GroupTradeSize] [float] NOT NULL,
	[GroupBeta] [float] NOT NULL,
	[GroupAlpha] [float] NOT NULL,
	[GroupIndexE] [float] NOT NULL,
	[GroupBetaE] [float] NOT NULL,
	[GroupAlphaE] [float] NOT NULL,
	[GroupStdErr] [float] NOT NULL,
	[GroupScalingFactor] [float] NOT NULL,
	[UserEntered] [varchar](20) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblGroupItems]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblGroupItems]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblGroupItems](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([GroupID]),
	[GroupID] [int] NOT NULL,
	[GroupItemID] [int] NOT NULL,
	[GroupPertracCode] [int] NOT NULL,
	[GroupIndexCode] [int] NOT NULL,
	[GroupSector] [varchar](100) NOT NULL,
	[UserEntered] [varchar](20) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblGroupItems] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT DELETE ON [dbo].[tblGroupItems] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblGroupItems] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblGroupItems] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblGroupItems] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblGroupItems] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblGroupItems] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblGroupItems] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblGroupItems] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblGroupItems] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblGroupItems] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblGroupItems] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblGroupItems_13Feb2013]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblGroupItems_13Feb2013]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblGroupItems_13Feb2013](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID] [int] NOT NULL,
	[GroupID] [int] NOT NULL,
	[GroupItemID] [int] NOT NULL,
	[GroupPertracCode] [int] NOT NULL,
	[GroupIndexCode] [int] NOT NULL,
	[GroupSector] [varchar](100) NOT NULL,
	[UserEntered] [varchar](20) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblGroupList]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblGroupList]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblGroupList](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([GroupListID]),
	[GroupListID] [int] NOT NULL,
	[GroupListName] [varchar](100) NOT NULL,
	[GroupGroup] [varchar](50) NOT NULL,
	[GroupDateFrom] [datetime] NOT NULL,
	[GroupDateTo] [datetime] NOT NULL,
	[DefaultConstraintGroup] [int] NOT NULL,
	[DefaultCovarianceMatrix] [int] NOT NULL,
	[GroupPricingPeriod] [int] NOT NULL,
	[UserEntered] [varchar](20) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblGroupList] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT DELETE ON [dbo].[tblGroupList] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblGroupList] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblGroupList] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblGroupList] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblGroupList] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblGroupList] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblGroupList] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblGroupList] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblGroupList] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblGroupList] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblGroupList] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblGroupList_13Feb2013]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblGroupList_13Feb2013]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblGroupList_13Feb2013](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID] [int] NOT NULL,
	[GroupListID] [int] NOT NULL,
	[GroupListName] [varchar](100) NOT NULL,
	[GroupGroup] [varchar](50) NOT NULL,
	[GroupDateFrom] [datetime] NOT NULL,
	[GroupDateTo] [datetime] NOT NULL,
	[DefaultConstraintGroup] [int] NOT NULL,
	[DefaultCovarianceMatrix] [int] NOT NULL,
	[UserEntered] [varchar](20) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblHistoricFundValuations]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblHistoricFundValuations]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblHistoricFundValuations](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([RN]),
	[FundID] [int] NOT NULL,
	[InstrumentID] [int] NOT NULL,
	[ValuationDate] [date] NOT NULL,
	[InstrumentContractSize] [float] NOT NULL,
	[InstrumentMultiplier] [float] NOT NULL,
	[SignedUnits] [float] NOT NULL,
	[SignedSettlement] [float] NOT NULL,
	[PriceDate] [date] NOT NULL,
	[PriceNAV] [float] NOT NULL,
	[PriceLevel] [float] NOT NULL,
	[PriceMultiplier] [float] NOT NULL,
	[FundFXRate] [float] NOT NULL,
	[InstrumentFXRate] [float] NOT NULL,
	[CompoundFXRate] [float] NOT NULL,
	[Value] [float] NOT NULL,
	[USDValue] [float] NOT NULL,
	[LocalValue] [float] NOT NULL,
	[ConsiderationValue] [float] NOT NULL,
	[FundWeightPercent] [float] NOT NULL,
	[InstrumentReturn] [float] NOT NULL,
	[InstrumentModifedReturn] [float] NOT NULL,
	[BenchmarkReturn] [float] NOT NULL,
	[BenchmarkModifiedReturn] [float] NOT NULL,
	[InstrumentBasisPoints] [float] NOT NULL,
	[InstrumentModifiedBasisPoints] [float] NOT NULL,
	[BenchmarkBasisPoints] [float] NOT NULL,
	[BenchmarkModifiedBasisPoints] [float] NOT NULL,
	[PreviousFundWeightPercent] [float] NOT NULL,
	[ReturnsSet] [bit] NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblHistoricFundValuations] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT SELECT ON [dbo].[tblHistoricFundValuations] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblHistoricFundValuations] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblHistoricFundValuations] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblIndexMapping]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblIndexMapping]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblIndexMapping](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([MappingID]),
	[MappingID] [int] NOT NULL,
	[IndexTicker] [varchar](50) NOT NULL,
	[IndexInstrumentID] [int] NOT NULL,
	[MappedFundTypeID] [int] NOT NULL,
	[MappingStartDate] [datetime] NOT NULL,
	[MappingEndDate] [datetime] NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblIndexMapping] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT VIEW DEFINITION ON [dbo].[tblIndexMapping] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblIndexMapping] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblInstrument]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblInstrument]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblInstrument](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([InstrumentID]),
	[InstrumentID] [int] NOT NULL,
	[InstrumentCode] [nvarchar](100) NULL,
	[InstrumentPFPCID] [varchar](100) NOT NULL,
	[InstrumentPertracCode] [int] NULL,
	[InstrumentParent] [int] NULL,
	[InstrumentParentEquivalentRatio] [float] NOT NULL,
	[InstrumentDescription] [nvarchar](100) NOT NULL,
	[InstrumentClass] [nvarchar](100) NULL,
	[InstrumentType] [int] NOT NULL,
	[InstrumentFundType] [int] NOT NULL,
	[InstrumentCurrency] [nvarchar](50) NOT NULL,
	[InstrumentCurrencyID] [int] NOT NULL,
	[InstrumentUnderlying] [int] NOT NULL,
	[InstrumentBenchmark] [int] NOT NULL,
	[InstrumentContractSize] [float] NOT NULL,
	[InstrumentMultiplier] [float] NOT NULL,
	[InstrumentExpectedReturn] [float] NOT NULL,
	[InstrumentWorstCase] [float] NOT NULL,
	[InstrumentLimitDisplay] [bit] NOT NULL,
	[InstrumentNoticeDays] [int] NOT NULL,
	[InstrumentDefaultSettlementDays_Buy] [int] NOT NULL,
	[InstrumentDefaultSettlementDays_Sell] [int] NOT NULL,
	[InstrumentDealingCutOffTime] [int] NOT NULL,
	[InstrumentCalendar] [int] NOT NULL,
	[InstrumentDataPeriod] [int] NOT NULL,
	[InstrumentDealingPeriod] [int] NOT NULL,
	[InstrumentDealingBaseDate] [smalldatetime] NULL,
	[InstrumentLockupPeriod] [int] NOT NULL,
	[InstrumentLastValidTradeDate] [datetime] NOT NULL,
	[InstrumentStrike] [float] NOT NULL,
	[InstrumentExpiryDate] [datetime] NOT NULL,
	[InstrumentPenalty] [float] NOT NULL,
	[InstrumentPenaltyComment] [varchar](100) NOT NULL,
	[InstrumentUpfrontfees] [int] NOT NULL,
	[InstrumentLiquidityComment] [nvarchar](100) NULL,
	[InstrumentExcludeGAV] [int] NOT NULL,
	[InstrumentExcludeAUM] [bit] NOT NULL,
	[InstrumentIsManagementFee] [int] NOT NULL,
	[InstrumentIsIncentiveFee] [int] NOT NULL,
	[InstrumentIsEqualisation] [int] NOT NULL,
	[InstrumentIsLeverageInstrument] [int] NOT NULL,
	[InstrumentLeverageCounterparty] [int] NOT NULL,
	[InstrumentExchangeCode] [varchar](50) NOT NULL,
	[InstrumentISIN] [varchar](25) NOT NULL,
	[InstrumentSEDOL] [varchar](25) NOT NULL,
	[InstrumentMorningstar] [varchar](25) NOT NULL,
	[InstrumentPortfolioTicker] [varchar](50) NOT NULL,
	[InstrumentCapturePrice] [bit] NOT NULL,
	[InstrumentReviewGroup] [int] NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblInstrument] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT INSERT ON [dbo].[tblInstrument] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblInstrument] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblInstrument] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblInstrument] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblInstrument] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblInstrument] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblInstrument] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblInstrument] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblInstrument] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblInstrumentFlorenceEntityLink]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblInstrumentFlorenceEntityLink]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblInstrumentFlorenceEntityLink](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([LinkID]),
	[LinkID] [int] NOT NULL,
	[InstrumentID] [int] NOT NULL,
	[EntityID] [int] NOT NULL,
	[UserEntered] [nvarchar](100) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblInstrumentFlorenceEntityLink] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT VIEW DEFINITION ON [dbo].[tblInstrumentFlorenceEntityLink] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblInstrumentFlorenceEntityLink] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblInstrumentType]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblInstrumentType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblInstrumentType](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([InstrumentTypeID]),
	[InstrumentTypeID] [int] NOT NULL,
	[InstrumentTypeDescription] [nvarchar](100) NOT NULL,
	[InstrumentTypeTreatAs] [int] NOT NULL,
	[InstrumentTypeFuturesStylePricing] [bit] NOT NULL,
	[InstrumentTypeHasDelta] [bit] NOT NULL,
	[InstrumentTypeIsExpense] [bit] NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblInstrumentType] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT INSERT ON [dbo].[tblInstrumentType] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblInstrumentType] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblInstrumentType] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblInstrumentType] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblInstrumentType] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblInstrumentType] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblInstrumentType] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblInstrumentType] TO [web_server] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblInstrumentType] ([DateEntered]) TO [InvestMaster_Manager] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblInstrumentType] ([DateDeleted]) TO [InvestMaster_Admin] AS [dbo]
GO
/****** Object:  Table [dbo].[tblInvestorGroup]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblInvestorGroup]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblInvestorGroup](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([IGID]),
	[IGID] [int] NOT NULL,
	[IGName] [nvarchar](50) NULL,
	[UserName] [varchar](50) NOT NULL,
	[DateEntered] [datetime] NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblInvestorGroup] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT INSERT ON [dbo].[tblInvestorGroup] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblInvestorGroup] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblInvestorGroup] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblInvestorGroup] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblInvestorGroup] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblInvestorGroup] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblInvestorGroup] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblInvestorGroup] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblInvestorGroup] TO [web_server] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblInvestorGroup] ([DateDeleted]) TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblInvestorGroup] ([DateDeleted]) TO [InvestMaster_Manager] AS [dbo]
GO
/****** Object:  Table [dbo].[tblJob]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblJob]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblJob](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[Auditid]  AS ([JobID]),
	[JobID] [int] NOT NULL,
	[JobPerson] [int] NOT NULL,
	[JobFirm] [int] NOT NULL,
	[JobLocation] [int] NOT NULL,
	[JobFrom] [datetime] NOT NULL,
	[JobTo] [datetime] NULL,
	[JobTitle] [varchar](255) NULL,
	[JobCurrent] [bit] NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [aaaaatblJob_PK] PRIMARY KEY NONCLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT VIEW DEFINITION ON [dbo].[tblJob] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblJob] TO [Sienna_Read] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblJob] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblLimits]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblLimits]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblLimits](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([limID]),
	[limID] [int] NOT NULL,
	[limDescription] [nvarchar](200) NOT NULL,
	[limType] [int] NULL,
	[limFund] [int] NULL,
	[limInstrument] [int] NULL,
	[limOnPremium] [int] NULL,
	[limCharacteristic] [int] NOT NULL,
	[limSector] [int] NULL,
	[limSectorGroup] [nvarchar](100) NOT NULL,
	[limDealingPeriod] [int] NOT NULL,
	[limGreaterOrLessThan] [int] NULL,
	[limLimit] [float] NULL,
	[limIsPercent] [int] NOT NULL,
	[UserEntered] [nvarchar](100) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblLimits] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT INSERT ON [dbo].[tblLimits] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblLimits] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblLimits] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblLimits] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblLimits] TO [InvestMaster_RiskManager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblLimits] TO [InvestMaster_RiskManager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblLimits] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblLimits] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblLimits] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblLimitType]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblLimitType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblLimitType](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([ltpID]),
	[ltpID] [int] NOT NULL,
	[ltpLimitType] [nvarchar](50) NULL,
	[ltpLabel] [nvarchar](50) NULL,
	[ltpLimitFormat] [varchar](50) NOT NULL,
	[ltpSortOrder] [int] NULL,
	[ltpDescription] [ntext] NULL,
	[DateEntered] [datetime] NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblLimitType] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT INSERT ON [dbo].[tblLimitType] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblLimitType] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblLimitType] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblLimitType] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblLimitType] TO [InvestMaster_RiskManager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblLimitType] TO [InvestMaster_RiskManager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblLimitType] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblLimitType] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblLimitType] TO [web_server] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblLimitType] ([DateDeleted]) TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblLimitType] ([DateDeleted]) TO [InvestMaster_RiskManager] AS [dbo]
GO
/****** Object:  Table [dbo].[tblLiquidityLimit]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblLiquidityLimit]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblLiquidityLimit](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([LiquidityID]),
	[LiquidityID] [int] NOT NULL,
	[LiquidityGroup] [int] NULL,
	[LiquidityDays] [nvarchar](50) NULL,
	[LiquidityLower] [float] NULL,
	[LiquidityUpper] [float] NULL,
	[DateEntered] [datetime] NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblLiquidityLimit] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT DELETE ON [dbo].[tblLiquidityLimit] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblLiquidityLimit] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblLiquidityLimit] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblLiquidityLimit] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblLiquidityLimit] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblLiquidityLimit] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblLiquidityLimit] TO [InvestMaster_RiskManager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblLiquidityLimit] TO [InvestMaster_RiskManager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblLiquidityLimit] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblLiquidityLimit] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblLiquidityLimit] TO [web_server] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblLiquidityLimit] ([DateDeleted]) TO [InvestMaster_RiskManager] AS [dbo]
GO
/****** Object:  Table [dbo].[tblLocations]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblLocations]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblLocations](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([CityID]),
	[CityID] [int] NOT NULL,
	[Country] [varchar](5) NOT NULL,
	[CountryName] [varchar](100) NOT NULL,
	[City] [varchar](100) NOT NULL,
	[Region] [varchar](5) NOT NULL,
	[RegionName] [varchar](100) NOT NULL,
	[Population] [int] NOT NULL,
	[Lattitude] [float] NOT NULL,
	[Longitude] [float] NOT NULL,
 CONSTRAINT [PK_tblLocations] PRIMARY KEY CLUSTERED 
(
	[CityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT SELECT ON [dbo].[tblLocations] TO [Florence_Read] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblLocations] TO [Genoa_Read] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblLocations] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblLocations] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblLocations] TO [Reporter] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblLocations] TO [Sienna_Read] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblLocations] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblManagementCompany]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblManagementCompany]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblManagementCompany](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([FirmID]),
	[FirmID] [int] NOT NULL,
	[FirmName] [varchar](100) NOT NULL,
	[FirmDescription] [text] NULL,
	[FirmPrimaryLocation] [int] NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT VIEW DEFINITION ON [dbo].[tblManagementCompany] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblManagementCompany] TO [Sienna_Read] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblManagementCompany] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblMarketInstruments]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblMarketInstruments]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblMarketInstruments](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([MarketInstrumentID]),
	[MarketInstrumentID] [int] NOT NULL,
	[Ticker] [varchar](35) NOT NULL,
	[Description] [varchar](100) NOT NULL,
	[InstrumentType] [int] NOT NULL,
	[InstrumentCurrencyID] [int] NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblMarketInstruments] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT DELETE ON [dbo].[tblMarketInstruments] TO [InvestMaster_AddTransaction] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblMarketInstruments] TO [InvestMaster_AddTransaction] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblMarketInstruments] TO [InvestMaster_AddTransaction] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblMarketInstruments] TO [InvestMaster_AddTransaction] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblMarketInstruments] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblMarketInstruments] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblMarketInstruments] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblMarketInstruments] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblMarketInstruments] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblMarketInstruments] TO [web_server] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblMarketInstruments] ([DateDeleted]) TO [InvestMaster_Manager] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblMarketInstruments] ([DateDeleted]) TO [InvestMaster_Sales] AS [dbo]
GO
/****** Object:  Table [dbo].[tblMarketPrices]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblMarketPrices]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblMarketPrices](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([RN]),
	[MarketInstrumentID] [int] NOT NULL,
	[PriceDate] [datetime] NOT NULL,
	[PriceDataType] [int] NOT NULL,
	[PriceValue] [float] NOT NULL,
	[TextValue] [varchar](20) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblMarketPrices] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT DELETE ON [dbo].[tblMarketPrices] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblMarketPrices] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblMarketPrices] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblMarketPrices] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblMarketPrices] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblMarketPrices] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblMarketPrices] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblMarketPrices] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblMarketPriceTypes]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblMarketPriceTypes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblMarketPriceTypes](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[PriceDataType] [int] NOT NULL,
	[FieldName] [varchar](20) NOT NULL,
	[TypeDescription] [varchar](100) NOT NULL,
 CONSTRAINT [PK_tblMarketPriceTypes] PRIMARY KEY CLUSTERED 
(
	[PriceDataType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT DELETE ON [dbo].[tblMarketPriceTypes] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblMarketPriceTypes] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblMarketPriceTypes] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblMarketPriceTypes] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblMarketPriceTypes] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblMarketPriceTypes] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblMarketPriceTypes] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblMarketPriceTypes] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblMeeting]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblMeeting]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblMeeting](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([MeetingID]),
	[MeetingID] [int] NOT NULL,
	[MeetingSelect]  AS ((rtrim([MeetingTitle])+', ')+CONVERT([varchar],[MeetingDate],(106))),
	[MeetingTitle] [varchar](255) NOT NULL,
	[MeetingContact] [varchar](255) NOT NULL,
	[MeetingDate] [datetime] NOT NULL,
	[MeetingPlace] [varchar](50) NOT NULL,
	[MeetingNote] [text] NOT NULL,
	[MeetingGood] [varchar](500) NOT NULL,
	[MeetingBad] [varchar](500) NOT NULL,
	[MeetingWisdom] [varchar](255) NOT NULL,
	[MeetingManagementCompanyID] [int] NOT NULL,
	[MeetingLocation] [int] NOT NULL,
	[MeetingFundID1] [int] NOT NULL,
	[MeetingFundID2] [int] NOT NULL,
	[MeetingFundID3] [int] NOT NULL,
	[MeetingFundID4] [int] NOT NULL,
	[MeetingFundID5] [int] NOT NULL,
	[MeetingFCPerson1] [int] NOT NULL,
	[MeetingFCPerson2] [int] NOT NULL,
	[MeetingFCPerson3] [int] NOT NULL,
	[MeetingFCPerson4] [int] NOT NULL,
	[MeetingFCPerson5] [int] NOT NULL,
	[MeetingFundPerson1] [int] NOT NULL,
	[MeetingFundPerson2] [int] NOT NULL,
	[MeetingFundPerson3] [int] NOT NULL,
	[MeetingFundPerson4] [int] NOT NULL,
	[MeetingFundPerson5] [int] NOT NULL,
	[MeetingStatus] [int] NOT NULL,
	[MeetingExistingFund] [bit] NOT NULL,
	[MeetingFundType] [int] NOT NULL,
	[UserEntered] [varchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblMeeting] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT VIEW DEFINITION ON [dbo].[tblMeeting] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblMeeting] TO [Sienna_Read] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblMeeting] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblMeetingPeople]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblMeetingPeople]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblMeetingPeople](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([PeopleID]),
	[PeopleID] [int] NOT NULL,
	[PeoplePerson] [varchar](50) NOT NULL,
	[PeopleDescription] [text] NULL,
	[PeopleFirm] [int] NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [aaaaatblPeople_PK] PRIMARY KEY NONCLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT VIEW DEFINITION ON [dbo].[tblMeetingPeople] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblMeetingPeople] TO [Sienna_Read] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblMeetingPeople] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblMeetingStatus]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblMeetingStatus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblMeetingStatus](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([StatusID]),
	[StatusID] [int] NOT NULL,
	[StatusDescription] [varchar](50) NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblMeetingStatus] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT VIEW DEFINITION ON [dbo].[tblMeetingStatus] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblMeetingStatus] TO [Sienna_Read] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblMeetingStatus] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblMonthlyComment]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblMonthlyComment]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblMonthlyComment](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([RN]),
	[CommentID]  AS ([RN]),
	[FundID] [int] NOT NULL,
	[CommentDate] [datetime] NOT NULL,
	[CommentText] [text] NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
GRANT INSERT ON [dbo].[tblMonthlyComment] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblMonthlyComment] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblMonthlyComment] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblMonthlyComment] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblMonthlyComment] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblMonthlyComment] TO [InvestMaster_RiskManager] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblMonthlyComment] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblMonthlyComment] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblMonthlyComment] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblMonthlyComment] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblPendingDataTaskEmails]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPendingDataTaskEmails]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblPendingDataTaskEmails](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([RN]),
	[DataTaskID] [int] NOT NULL,
	[EMailLine] [varchar](500) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblPendingDataTaskEmails] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT DELETE ON [dbo].[tblPendingDataTaskEmails] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblPendingDataTaskEmails] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPendingDataTaskEmails] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblPendingDataTaskEmails] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPendingDataTaskEmails] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPendingDataTaskEmails] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblPendingDataTaskEmails] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT DELETE ON [dbo].[tblPendingDataTaskEmails] TO [Naples_Role] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblPendingDataTaskEmails] TO [Naples_Role] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPendingDataTaskEmails] TO [Naples_Role] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblPendingDataTaskEmails] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblPendingTradeFileEntries]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPendingTradeFileEntries]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblPendingTradeFileEntries](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([TransactionParentID]),
	[BrokerID] [int] NOT NULL,
	[BrokerSpecific] [nvarchar](50) NOT NULL,
	[TransactionParentID] [int] NOT NULL,
	[WorkflowID] [int] NOT NULL,
	[TradefileText] [varchar](1000) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblPendingTradeFileEntries] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT VIEW DEFINITION ON [dbo].[tblPendingTradeFileEntries] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblPendingTradeFileEntries] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblPendingTransactions]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPendingTransactions]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblPendingTransactions](
	[RN]  AS ([TransactionID]),
	[AuditID]  AS ([TransactionParentID]),
	[TransactionID] [int] IDENTITY(10000,1) NOT NULL,
	[TransactionTicket] [nvarchar](50) NOT NULL,
	[TransactionFund] [int] NOT NULL,
	[TransactionSubFund] [int] NULL,
	[TransactionSubFundValueFlag] [smallint] NOT NULL,
	[TransactionInstrument] [int] NOT NULL,
	[TransactionType] [int] NOT NULL,
	[TransactionValueorAmount] [nvarchar](10) NOT NULL,
	[TransactionUnits] [float] NOT NULL,
	[TransactionPrice] [float] NOT NULL,
	[TransactionCosts] [float] NOT NULL,
	[TransactionCostPercent] [float] NOT NULL,
	[TransactionMainFundPrice] [float] NOT NULL,
	[TransactionFXRate] [float] NOT NULL,
	[TransactionEffectivePrice] [float] NOT NULL,
	[TransactionEffectiveWatermark] [float] NOT NULL,
	[TransactionSpecificInitialEqualisationValue] [float] NOT NULL,
	[TransactionCounterparty] [int] NOT NULL,
	[TransactionInvestment] [nvarchar](50) NOT NULL,
	[TransactionExecution] [nvarchar](50) NOT NULL,
	[TransactionDataEntry] [nvarchar](50) NOT NULL,
	[TransactionDecisionDate] [datetime] NULL,
	[TransactionValueDate] [datetime] NOT NULL,
	[TransactionFIFOValueDate] [datetime] NOT NULL,
	[TransactionSettlementDate] [datetime] NOT NULL,
	[TransactionConfirmationDate] [datetime] NULL,
	[TransactionCostIsPercent] [bit] NOT NULL,
	[TransactionIsTransfer] [bit] NOT NULL,
	[TransactionExemptFromUpdate] [bit] NOT NULL,
	[TransactionSpecificInitialEqualisationFlag] [bit] NOT NULL,
	[TransactionFinalAmount] [int] NOT NULL,
	[TransactionFinalPrice] [int] NOT NULL,
	[TransactionFinalCosts] [int] NOT NULL,
	[TransactionFinalSettlement] [int] NOT NULL,
	[TransactionInstructionFlag] [int] NOT NULL,
	[TransactionBankConfirmation] [int] NOT NULL,
	[TransactionInitialDataEntry] [int] NOT NULL,
	[TransactionAmountConfirmed] [int] NOT NULL,
	[TransactionSettlementConfirmed] [int] NOT NULL,
	[TransactionFinalDataEntry] [int] NOT NULL,
	[TransactionTradeStatusID] [int] NULL,
	[TransactionComment] [varchar](500) NOT NULL,
	[TransactionParentID] [int] NOT NULL,
	[TransactionUser] [varchar](50) NOT NULL,
	[TransactionGroup] [nvarchar](50) NOT NULL,
	[TransactionAdminStatus] [int] NOT NULL,
	[TransactionCTFLAGS] [int] NOT NULL,
	[TransactionCompoundRN] [int] NOT NULL,
	[TransactionEffectiveValueDate] [datetime] NULL,
 CONSTRAINT [PK_tblPendingTransactions] PRIMARY KEY NONCLUSTERED 
(
	[TransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT DELETE ON [dbo].[tblPendingTransactions] TO [InvestMaster_AddTransaction] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblPendingTransactions] TO [InvestMaster_AddTransaction] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPendingTransactions] TO [InvestMaster_AddTransaction] AS [dbo]
GO
GRANT DELETE ON [dbo].[tblPendingTransactions] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblPendingTransactions] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPendingTransactions] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPendingTransactions] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT DELETE ON [dbo].[tblPendingTransactions] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblPendingTransactions] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPendingTransactions] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPendingTransactions] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblPendingTransactions] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblPendingTransactions] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblPeriod]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPeriod]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblPeriod](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([RN]),
	[PeriodID] [int] NOT NULL,
	[PeriodTitle] [varchar](50) NOT NULL,
	[PeriodDescription] [varchar](200) NOT NULL,
	[PeriodDateFrom] [datetime] NOT NULL,
	[PeriodDateTo] [datetime] NOT NULL,
	[AppName] [varchar](50) NOT NULL,
	[IsGlobal] [bit] NOT NULL,
	[UserEntered] [varchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblPeriod] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT VIEW DEFINITION ON [dbo].[tblPeriod] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblPeriod] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblPerson]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPerson]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblPerson](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([PersonID]),
	[PersonID] [int] NOT NULL,
	[Person] [nvarchar](100) NOT NULL,
	[PersonUserName] [nvarchar](50) NULL,
	[PersonEmail] [nvarchar](100) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblPerson] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT INSERT ON [dbo].[tblPerson] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPerson] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPerson] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblPerson] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPerson] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPerson] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblPerson] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblPerson] TO [web_server] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblPerson] ([DateDeleted]) TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblPerson] ([DateDeleted]) TO [InvestMaster_Manager] AS [dbo]
GO
/****** Object:  Table [dbo].[tblPertracCustomFieldData]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPertracCustomFieldData]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblPertracCustomFieldData](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([FieldDataID]),
	[FieldDataID] [int] NOT NULL,
	[CustomFieldID] [int] NOT NULL,
	[PertracID] [int] NOT NULL,
	[GroupID] [int] NOT NULL,
	[FieldNumericData] [float] NOT NULL,
	[FieldTextData] [varchar](50) NOT NULL,
	[FieldDateData] [datetime] NOT NULL,
	[FieldBooleanData] [bit] NOT NULL,
	[UpdateRequired] [bit] NOT NULL,
	[LatestReturnDate] [datetime] NULL,
	[UserEntered] [varchar](20) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblPertracCustomFieldData] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT SELECT ON [dbo].[tblPertracCustomFieldData] TO [Genoa_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPertracCustomFieldData] TO [Genoa_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPertracCustomFieldData] TO [Genoa_Read] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPertracCustomFieldData] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblPertracCustomFieldData] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblPertracCustomFieldData] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblPertracCustomFields]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPertracCustomFields]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblPertracCustomFields](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([FieldID]),
	[FieldID] [int] NOT NULL,
	[FieldName] [varchar](100) NOT NULL,
	[CustomFieldType] [int] NOT NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[IsMaxValue] [bit] NOT NULL,
	[IsMinValue] [bit] NOT NULL,
	[IsFirstValue] [bit] NOT NULL,
	[IsLastValue] [bit] NOT NULL,
	[IsAverageValue] [bit] NOT NULL,
	[IsIRR] [bit] NOT NULL,
	[FieldPeriodCount] [int] NOT NULL,
	[FieldIsVolatile] [bit] NOT NULL,
	[FieldIsCalculated] [bit] NOT NULL,
	[FieldIsSearchable] [bit] NOT NULL,
	[FieldIsOptimisable] [bit] NOT NULL,
	[FieldDataType] [int] NOT NULL,
	[FieldDetails] [varchar](500) NOT NULL,
	[DefaultValue] [varchar](100) NOT NULL,
	[UserEntered] [varchar](20) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblPertracCustomFields] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT VIEW DEFINITION ON [dbo].[tblPertracCustomFields] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblPertracCustomFields] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblPertracFieldMapping]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPertracFieldMapping]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblPertracFieldMapping](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([RN]),
	[DataProvider] [varchar](30) NOT NULL,
	[FCP_FieldName] [varchar](30) NOT NULL,
	[PertracField] [varchar](30) NULL,
	[ProviderFieldName] [varchar](30) NULL,
	[FieldIsNumeric] [bit] NOT NULL,
	[Multiplier] [float] NOT NULL,
 CONSTRAINT [PK_tblPertracFieldMapping] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT VIEW DEFINITION ON [dbo].[tblPertracFieldMapping] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblPertracFieldMapping] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblPertracInstrumentFlags]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPertracInstrumentFlags]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblPertracInstrumentFlags](
	[GroupID] [int] NOT NULL,
	[GroupName] [varchar](20) NOT NULL,
	[IsGroupFlag] [bit] NOT NULL,
	[IsSimulationFlag] [bit] NOT NULL,
	[IsVeniceFlag] [bit] NOT NULL,
 CONSTRAINT [PK_tblPertracInstrumentFlags] PRIMARY KEY CLUSTERED 
(
	[GroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT VIEW DEFINITION ON [dbo].[tblPertracInstrumentFlags] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblPertracInstrumentFlags] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblPFPCCustodyRecord]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPFPCCustodyRecord]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblPFPCCustodyRecord](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([RN]),
	[FundID] [int] NOT NULL,
	[ValuationDate] [datetime] NOT NULL,
	[SecurityID] [varchar](50) NOT NULL,
	[ShareClass] [varchar](50) NOT NULL,
	[Holding] [float] NOT NULL,
 CONSTRAINT [PK_tblPFPCCustodyRecord] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT DELETE ON [dbo].[tblPFPCCustodyRecord] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblPFPCCustodyRecord] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPFPCCustodyRecord] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblPFPCCustodyRecord] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT DELETE ON [dbo].[tblPFPCCustodyRecord] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblPFPCCustodyRecord] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPFPCCustodyRecord] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblPFPCCustodyRecord] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT DELETE ON [dbo].[tblPFPCCustodyRecord] TO [InvestMaster_MonthEnd] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblPFPCCustodyRecord] TO [InvestMaster_MonthEnd] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPFPCCustodyRecord] TO [InvestMaster_MonthEnd] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblPFPCCustodyRecord] TO [InvestMaster_MonthEnd] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPFPCCustodyRecord] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblPFPCCustodyRecord] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblPFPCCustodyRecord] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblPFPCPortfolioReconcilliation]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPFPCPortfolioReconcilliation]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblPFPCPortfolioReconcilliation](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[ValuationDate] [datetime] NOT NULL,
	[FundID] [int] NOT NULL,
	[FCPInstrumentID] [int] NOT NULL,
	[PFPCInstrumentID] [varchar](50) NOT NULL,
	[PFPCInstrumentDescription] [varchar](100) NOT NULL,
	[FCP_Holding] [float] NOT NULL,
	[FCP_HoldingCompleteness] [float] NOT NULL,
	[PFPC_Holding] [float] NOT NULL,
	[Holding_Difference] [float] NOT NULL,
	[FCP_Price] [float] NOT NULL,
	[FCP_PriceFinal] [int] NOT NULL,
	[FCP_PriceAdministrator] [int] NOT NULL,
	[FCP_PriceDate] [datetime] NULL,
	[PFPC_Price] [float] NOT NULL,
	[Price_Difference] [float] NOT NULL,
	[FCP_FXRate] [float] NOT NULL,
	[PFPC_FXRate] [float] NOT NULL,
	[FXRate_Difference] [float] NOT NULL,
	[FCP_LocalCcyValue] [float] NOT NULL,
	[PFPC_LocalCcyValue] [float] NOT NULL,
	[LocalCcyValue_Difference] [float] NOT NULL,
	[FCP_BookCcyValue] [float] NOT NULL,
	[PFPC_BookCcyValue] [float] NOT NULL,
	[BookCcyValue_Difference] [float] NOT NULL,
	[DateEntered] [datetime] NOT NULL,
 CONSTRAINT [PK_tblPFPCPortfolioReconcilliation] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT DELETE ON [dbo].[tblPFPCPortfolioReconcilliation] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblPFPCPortfolioReconcilliation] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPFPCPortfolioReconcilliation] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblPFPCPortfolioReconcilliation] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT DELETE ON [dbo].[tblPFPCPortfolioReconcilliation] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblPFPCPortfolioReconcilliation] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPFPCPortfolioReconcilliation] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblPFPCPortfolioReconcilliation] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT DELETE ON [dbo].[tblPFPCPortfolioReconcilliation] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblPFPCPortfolioReconcilliation] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPFPCPortfolioReconcilliation] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblPFPCPortfolioReconcilliation] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT DELETE ON [dbo].[tblPFPCPortfolioReconcilliation] TO [InvestMaster_MonthEnd] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblPFPCPortfolioReconcilliation] TO [InvestMaster_MonthEnd] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPFPCPortfolioReconcilliation] TO [InvestMaster_MonthEnd] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblPFPCPortfolioReconcilliation] TO [InvestMaster_MonthEnd] AS [dbo]
GO
GRANT DELETE ON [dbo].[tblPFPCPortfolioReconcilliation] TO [InvestMaster_RiskManager] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblPFPCPortfolioReconcilliation] TO [InvestMaster_RiskManager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPFPCPortfolioReconcilliation] TO [InvestMaster_RiskManager] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblPFPCPortfolioReconcilliation] TO [InvestMaster_RiskManager] AS [dbo]
GO
GRANT DELETE ON [dbo].[tblPFPCPortfolioReconcilliation] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblPFPCPortfolioReconcilliation] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPFPCPortfolioReconcilliation] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblPFPCPortfolioReconcilliation] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblPFPCPortfolioReconcilliation] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblPFPCPortfolioReconcilliation] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblPFPCPortfolioValuation]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPFPCPortfolioValuation]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblPFPCPortfolioValuation](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[ValuationDate] [datetime] NOT NULL,
	[FundID] [int] NOT NULL,
	[FundName] [varchar](200) NOT NULL,
	[FundCurrency] [varchar](10) NOT NULL,
	[InstrumentID] [varchar](50) NOT NULL,
	[InstrumentDescription] [varchar](100) NOT NULL,
	[InstrumentCurrency] [varchar](10) NOT NULL,
	[Amount] [float] NOT NULL,
	[LocalCcyPrice] [float] NOT NULL,
	[FxRate] [float] NOT NULL,
	[LocalCcyValue] [float] NOT NULL,
	[BookCcyValue] [float] NOT NULL,
	[DateEntered] [datetime] NOT NULL,
 CONSTRAINT [PK_tblPFPCPortfolioValuation] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT SELECT ON [dbo].[tblPFPCPortfolioValuation] TO [InvestMaster_AddTransaction] AS [dbo]
GO
GRANT DELETE ON [dbo].[tblPFPCPortfolioValuation] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblPFPCPortfolioValuation] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPFPCPortfolioValuation] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblPFPCPortfolioValuation] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPFPCPortfolioValuation] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT DELETE ON [dbo].[tblPFPCPortfolioValuation] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblPFPCPortfolioValuation] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPFPCPortfolioValuation] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblPFPCPortfolioValuation] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT DELETE ON [dbo].[tblPFPCPortfolioValuation] TO [InvestMaster_MonthEnd] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblPFPCPortfolioValuation] TO [InvestMaster_MonthEnd] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPFPCPortfolioValuation] TO [InvestMaster_MonthEnd] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblPFPCPortfolioValuation] TO [InvestMaster_MonthEnd] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPFPCPortfolioValuation] TO [InvestMaster_RiskManager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPFPCPortfolioValuation] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblPFPCPortfolioValuation] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblPFPCPortfolioValuation] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblPFPCShareAllocation]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPFPCShareAllocation]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblPFPCShareAllocation](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([RN]),
	[FundID] [int] NOT NULL,
	[ValuationDate] [datetime] NOT NULL,
	[TradeDate] [datetime] NOT NULL,
	[AccountID] [varchar](50) NOT NULL,
	[TransactionType] [varchar](50) NOT NULL,
	[IssuedShares] [float] NOT NULL,
	[CashInvested] [float] NOT NULL,
 CONSTRAINT [PK_tblPFPCShareAllocation] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT DELETE ON [dbo].[tblPFPCShareAllocation] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblPFPCShareAllocation] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPFPCShareAllocation] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblPFPCShareAllocation] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT DELETE ON [dbo].[tblPFPCShareAllocation] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblPFPCShareAllocation] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPFPCShareAllocation] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblPFPCShareAllocation] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT DELETE ON [dbo].[tblPFPCShareAllocation] TO [InvestMaster_MonthEnd] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblPFPCShareAllocation] TO [InvestMaster_MonthEnd] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPFPCShareAllocation] TO [InvestMaster_MonthEnd] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblPFPCShareAllocation] TO [InvestMaster_MonthEnd] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPFPCShareAllocation] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblPFPCShareAllocation] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblPFPCShareAllocation] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblPortfolioData]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPortfolioData]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblPortfolioData](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([PortfolioItemID]),
	[PortfolioDataID] [int] NOT NULL,
	[PortfolioItemID] [int] NOT NULL,
	[PortfolioItemType] [int] NOT NULL,
	[Ticker] [varchar](35) NOT NULL,
	[FilingDate] [datetime] NOT NULL,
	[MarketInstrumentID] [int] NOT NULL,
	[Holding] [float] NOT NULL,
	[Weight] [float] NOT NULL,
	[MonitorItem] [bit] NOT NULL,
	[XMLData] [text] NOT NULL,
	[UserEntered] [varchar](20) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblPortfolioData] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT DELETE ON [dbo].[tblPortfolioData] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblPortfolioData] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPortfolioData] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblPortfolioData] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblPortfolioData] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPortfolioData] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblPortfolioData] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPortfolioData] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblPortfolioData] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblPortfolioData] TO [web_server] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblPortfolioData] ([DateDeleted]) TO [InvestMaster_Manager] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblPortfolioData] ([DateDeleted]) TO [InvestMaster_Sales] AS [dbo]
GO
/****** Object:  Table [dbo].[tblPortfolioIndex]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPortfolioIndex]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblPortfolioIndex](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([PortfolioID]),
	[PortfolioID] [int] NOT NULL,
	[PortfolioTicker] [varchar](35) NOT NULL,
	[PortfolioDescription] [varchar](200) NOT NULL,
	[PortfolioType] [int] NOT NULL,
	[PortfolioFilingDate] [datetime] NOT NULL,
	[PortfolioDataID] [int] NOT NULL,
	[InstrumentID] [int] NOT NULL,
	[UserEntered] [varchar](20) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblPortfolioIndex] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT DELETE ON [dbo].[tblPortfolioIndex] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblPortfolioIndex] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPortfolioIndex] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblPortfolioIndex] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblPortfolioIndex] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPortfolioIndex] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblPortfolioIndex] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPortfolioIndex] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblPortfolioIndex] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblPortfolioIndex] TO [web_server] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblPortfolioIndex] ([DateDeleted]) TO [InvestMaster_Manager] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblPortfolioIndex] ([DateDeleted]) TO [InvestMaster_Sales] AS [dbo]
GO
/****** Object:  Table [dbo].[tblPortfolioItemTypes]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPortfolioItemTypes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblPortfolioItemTypes](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[PortfolioItemType] [int] NOT NULL,
	[PortfolioItemTypeDescription] [varchar](200) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT DELETE ON [dbo].[tblPortfolioItemTypes] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblPortfolioItemTypes] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPortfolioItemTypes] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblPortfolioItemTypes] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPortfolioItemTypes] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPortfolioItemTypes] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblPortfolioItemTypes] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPortfolioItemTypes] TO [Naples_Role] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblPortfolioItemTypes] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblPortfolioType]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPortfolioType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblPortfolioType](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[PortfolioTypeID] [int] NOT NULL,
	[PortfolioTypeName] [varchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT VIEW DEFINITION ON [dbo].[tblPortfolioType] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblPortfolioType] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblPrice]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblPrice]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblPrice](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([PriceID]),
	[PriceID] [int] NOT NULL,
	[PriceInstrument] [int] NOT NULL,
	[PriceDate] [smalldatetime] NOT NULL,
	[PricePercent] [float] NOT NULL,
	[PriceLevel] [float] NOT NULL,
	[PriceMultiplier] [float] NOT NULL,
	[PriceNAV] [float] NOT NULL,
	[PriceBasicNAV] [float] NOT NULL,
	[PriceGNAV] [float] NOT NULL,
	[PriceGAV] [float] NOT NULL,
	[PriceBaseDate] [smalldatetime] NULL,
	[PriceFinal] [int] NOT NULL,
	[PriceIsPercent] [bit] NOT NULL,
	[PriceIsAdministrator] [bit] NOT NULL,
	[PriceIsMilestone] [bit] NOT NULL,
	[PriceComment] [nvarchar](100) NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblPrice] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT INSERT ON [dbo].[tblPrice] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPrice] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblPrice] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPrice] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblPrice] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPrice] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblPrice] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblPrice] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblPrice] TO [web_server] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblPrice] ([DateDeleted]) TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblPrice] ([DateDeleted]) TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblPrice] ([DateDeleted]) TO [InvestMaster_Manager] AS [dbo]
GO
/****** Object:  Table [dbo].[tblReferentialIntegrity]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblReferentialIntegrity]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblReferentialIntegrity](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([ReferentialID]),
	[ReferentialID] [int] NOT NULL,
	[TableName] [nvarchar](30) NOT NULL,
	[TableField] [nvarchar](30) NOT NULL,
	[FeedsTable] [nvarchar](30) NOT NULL,
	[FeedsField] [nvarchar](30) NOT NULL,
	[DescriptionField] [nvarchar](30) NULL,
	[IsDescriptiveReferenceOnly] [bit] NOT NULL,
	[DateEntered] [datetime] NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblReferentialIntegrity] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT INSERT ON [dbo].[tblReferentialIntegrity] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblReferentialIntegrity] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblReferentialIntegrity] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblReferentialIntegrity] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblReferentialIntegrity] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblReferentialIntegrity] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblReferentialIntegrity] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblRelationship]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRelationship]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblRelationship](
	[relID] [int] IDENTITY(1,1) NOT NULL,
	[relRelationship] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_tblRelationship] PRIMARY KEY CLUSTERED 
(
	[relID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT SELECT ON [dbo].[tblRelationship] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblRelationship] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblRelationship] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblRelationship] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblRelationship] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblRelationship] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblReport]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblReport]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblReport](
	[ReportID] [int] IDENTITY(210,1) NOT NULL,
	[ReportFundID] [int] NULL,
	[ReportType] [int] NULL,
	[ReportPerson] [int] NULL,
	[ReportDate] [smalldatetime] NULL,
	[ReportStatus] [int] NULL,
	[ReportHeading] [nvarchar](50) NULL,
	[ReportText] [ntext] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
GRANT DELETE ON [dbo].[tblReport] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblReport] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblReport] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblReport] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblReport] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblReport] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblReport] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblReport] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblReport] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblReport] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblReportCustomisation]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblReportCustomisation]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblReportCustomisation](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([RN]),
	[ApplicationName] [varchar](50) NOT NULL,
	[FundID] [int] NOT NULL,
	[ReportName] [varchar](50) NOT NULL,
	[FieldName] [varchar](50) NOT NULL,
	[PropertyName] [varchar](20) NOT NULL,
	[PropertyValue] [varchar](1000) NOT NULL,
	[IsNumeric] [bit] NOT NULL,
 CONSTRAINT [PK_tblReportCustomisation] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT DELETE ON [dbo].[tblReportCustomisation] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblReportCustomisation] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblReportCustomisation] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblReportCustomisation] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT DELETE ON [dbo].[tblReportCustomisation] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblReportCustomisation] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblReportCustomisation] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblReportCustomisation] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblReportCustomisation] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblReportCustomisation] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblReportCustomisation] TO [Naples_Role] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblReportCustomisation] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblReportDetailLog]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblReportDetailLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblReportDetailLog](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([RN]),
	[AppName] [varchar](30) NOT NULL,
	[ReportName] [varchar](30) NOT NULL,
	[KeyField] [varchar](50) NOT NULL,
	[ValueField] [int] NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblReportDetailLog] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT SELECT ON [dbo].[tblReportDetailLog] TO [Florence_Read] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblReportDetailLog] TO [Genoa_Read] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblReportDetailLog] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblReportDetailLog] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblReportDetailLog] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblReportDetailLog] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblReportDetailLog] TO [Reporter] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblReportDetailLog] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblReportStatus]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblReportStatus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblReportStatus](
	[ReportStatusID] [int] IDENTITY(8,1) NOT NULL,
	[ReportStatusDescription] [nvarchar](50) NULL
) ON [PRIMARY]
END
GO
GRANT DELETE ON [dbo].[tblReportStatus] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblReportStatus] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblReportStatus] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblReportStatus] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblReportStatus] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblReportStatus] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblReportStatus] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblReportStatus] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblReportStatus] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblReportStatus] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblReportType]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblReportType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblReportType](
	[ReportTypeID] [int] IDENTITY(8,1) NOT NULL,
	[ReportTypeDescription] [nvarchar](50) NULL
) ON [PRIMARY]
END
GO
GRANT DELETE ON [dbo].[tblReportType] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblReportType] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblReportType] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblReportType] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblReportType] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblReportType] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblReportType] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblReportType] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblReportType] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblReportType] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblRiskData]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRiskData]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblRiskData](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([RiskDataID]),
	[RiskDataID] [int] NOT NULL,
	[DataCharacteristicId] [int] NOT NULL,
	[InstrumentID] [int] NOT NULL,
	[DataWeighting] [float] NOT NULL,
	[UserEntered] [nvarchar](100) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblRiskData] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT VIEW DEFINITION ON [dbo].[tblRiskData] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblRiskData] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblRiskDataCategory]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRiskDataCategory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblRiskDataCategory](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([DataCategoryId]),
	[DataCategoryId] [int] NOT NULL,
	[DataCategory] [nvarchar](100) NOT NULL,
	[UserEntered] [nvarchar](100) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblRiskDataCategory] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT VIEW DEFINITION ON [dbo].[tblRiskDataCategory] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblRiskDataCategory] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblRiskDataCharacteristic]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRiskDataCharacteristic]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblRiskDataCharacteristic](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([DataCharacteristicId]),
	[DataCategoryId] [int] NOT NULL,
	[DataCharacteristicId] [int] NOT NULL,
	[DataCharacteristic] [nvarchar](200) NOT NULL,
	[DataCharacteristicTypeID] [int] NOT NULL,
	[FixedValue] [float] NULL,
	[DefaultValue] [float] NULL,
	[BooleanValue] [float] NULL,
	[UserEntered] [nvarchar](100) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblRiskDataCharacteristic] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT VIEW DEFINITION ON [dbo].[tblRiskDataCharacteristic] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblRiskDataCharacteristic] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblRiskDataCharacteristicTypes]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRiskDataCharacteristicTypes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblRiskDataCharacteristicTypes](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([DataCharacteristicTypeID]),
	[DataCharacteristicTypeID] [int] NOT NULL,
	[DataCharacteristicType] [nvarchar](200) NOT NULL,
	[UserEntered] [nvarchar](100) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblRiskDataCharacteristicTypes] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT VIEW DEFINITION ON [dbo].[tblRiskDataCharacteristicTypes] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblRiskDataCharacteristicTypes] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblRiskDataCompoundCharacteristic]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRiskDataCompoundCharacteristic]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblRiskDataCompoundCharacteristic](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([CompoundID]),
	[CompoundID] [int] NOT NULL,
	[ParentCharacteristicID] [int] NOT NULL,
	[ChildCharacteristicID] [int] NOT NULL,
	[UserEntered] [nvarchar](100) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblRiskDataCompoundCharacteristic] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT VIEW DEFINITION ON [dbo].[tblRiskDataCompoundCharacteristic] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblRiskDataCompoundCharacteristic] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblRiskDataLimits]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRiskDataLimits]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblRiskDataLimits](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([RiskDataLimitID]),
	[RiskDataLimitID] [int] NOT NULL,
	[LimitDescription] [nvarchar](200) NOT NULL,
	[FundID] [int] NOT NULL,
	[InstrumentID] [int] NOT NULL,
	[CharacteristicID] [int] NOT NULL,
	[Granularity] [int] NOT NULL,
	[GreaterOrLessThan] [float] NOT NULL,
	[LimitLevel] [float] NOT NULL,
	[isPercent] [int] NOT NULL,
	[UserEntered] [nchar](100) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]
END
GO
GRANT VIEW DEFINITION ON [dbo].[tblRiskDataLimits] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblRiskDataLimits] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblSectorLimit]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblSectorLimit]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblSectorLimit](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([SectorID]),
	[SectorID] [int] NOT NULL,
	[SectorGroup] [int] NULL,
	[SectorDescription] [nvarchar](50) NULL,
	[SectorLowerBound] [float] NULL,
	[SectorUpperBound] [float] NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblSectorLimit] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT DELETE ON [dbo].[tblSectorLimit] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblSectorLimit] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblSectorLimit] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblSectorLimit] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblSectorLimit] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblSectorLimit] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblSectorLimit] TO [InvestMaster_RiskManager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblSectorLimit] TO [InvestMaster_RiskManager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblSectorLimit] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblSectorLimit] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblSectorLimit] TO [web_server] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblSectorLimit] ([DateDeleted]) TO [InvestMaster_RiskManager] AS [dbo]
GO
/****** Object:  Table [dbo].[tblSophisBusinessEvents]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblSophisBusinessEvents]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblSophisBusinessEvents](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([ID]),
	[ID] [int] NOT NULL,
	[Account] [int] NOT NULL,
	[Method] [int] NOT NULL,
	[Party] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Name_FR] [nvarchar](100) NOT NULL,
	[ExistingInstrument] [int] NOT NULL,
	[SubstituteInstrument] [int] NOT NULL,
	[UserEntered] [nvarchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblSophisBusinessEvents] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblSophisStatusGroups]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblSophisStatusGroups]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblSophisStatusGroups](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([RN]),
	[RECORD_TYPE] [int] NOT NULL,
	[GROUP_NAME] [nvarchar](50) NULL,
	[KERNEL_STATUS_GROUP_ID] [int] NOT NULL,
	[PRIORITY] [int] NULL,
	[STATUS_ID] [int] NOT NULL,
	[STATUS_NAME] [nvarchar](200) NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblSophisStatusGroups] PRIMARY KEY CLUSTERED 
(
	[RECORD_TYPE] ASC,
	[KERNEL_STATUS_GROUP_ID] ASC,
	[STATUS_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT SELECT ON [dbo].[tblSophisStatusGroups] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblSophisStatusGroups] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblSubFund]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblSubFund]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblSubFund](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([SubFundID]),
	[SubFundID] [int] NOT NULL,
	[SubFundName] [nvarchar](50) NULL,
	[SubFundParent] [int] NULL,
	[SubFundCurrency] [int] NULL,
	[UserName] [nvarchar](100) NULL,
	[DateEntered] [datetime] NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblSubFund] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT DELETE ON [dbo].[tblSubFund] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblSubFund] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblSubFund] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblSubFund] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblSubFund] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblSubFund] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblSubFund] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblSubFund] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblSubFund] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblSubFund] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblSubscriptionLog]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblSubscriptionLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblSubscriptionLog](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([RN]),
	[Source] [nvarchar](50) NOT NULL,
	[FileName] [nvarchar](50) NOT NULL,
	[OrderDate] [date] NOT NULL,
	[LineType] [nvarchar](50) NOT NULL,
	[TransactionID] [int] NOT NULL,
	[FundID] [int] NOT NULL,
	[InstrumentID] [int] NOT NULL,
	[Status] [nvarchar](100) NULL,
	[OrderText] [nvarchar](2000) NOT NULL,
	[UserEntered] [nvarchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]
END
GO
GRANT INSERT ON [dbo].[tblSubscriptionLog] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblSubscriptionLog] TO [web_TradeProcessing] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblSubscriptionLog] TO [web_TradeProcessing] AS [dbo]
GO
/****** Object:  Table [dbo].[tblSystemDates]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblSystemDates]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblSystemDates](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([SysdateID]),
	[SysdateID] [int] NOT NULL,
	[SysdateDescription] [nvarchar](50) NULL,
	[SysdateDate] [smalldatetime] NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblSystemDates] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT INSERT ON [dbo].[tblSystemDates] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblSystemDates] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblSystemDates] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblSystemDates] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblSystemDates] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblSystemDates] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblSystemDates] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblSystemDates] TO [web_server] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblSystemDates] ([DateDeleted]) TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblSystemDates] ([DateDeleted]) TO [InvestMaster_Manager] AS [dbo]
GO
/****** Object:  Table [dbo].[tblSystemStrings]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblSystemStrings]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblSystemStrings](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[StringDescription] [varchar](100) NOT NULL,
	[StringValue] [varchar](250) NOT NULL,
 CONSTRAINT [PK_tblSystemStrings] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT SELECT ON [dbo].[tblSystemStrings] TO [InvestMaster_AddTransaction] AS [dbo]
GO
GRANT DELETE ON [dbo].[tblSystemStrings] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblSystemStrings] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblSystemStrings] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblSystemStrings] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblSystemStrings] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT DELETE ON [dbo].[tblSystemStrings] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblSystemStrings] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblSystemStrings] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblSystemStrings] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblSystemStrings] TO [InvestMaster_MonthEnd] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblSystemStrings] TO [InvestMaster_RiskManager] AS [dbo]
GO
GRANT DELETE ON [dbo].[tblSystemStrings] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblSystemStrings] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblSystemStrings] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblSystemStrings] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblSystemStrings] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblSystemStrings] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblTradeFileAcknowledgement]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblTradeFileAcknowledgement]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblTradeFileAcknowledgement](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([TransactionParentID]),
	[BrokerID] [int] NOT NULL,
	[TransactionParentID] [int] NOT NULL,
	[AcknowledgementText] [nvarchar](1000) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblTradeFileAcknowledgement] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblTradeFileConfirmations]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblTradeFileConfirmations]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblTradeFileConfirmations](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([TransactionParentID]),
	[BrokerID] [int] NOT NULL,
	[TransactionParentID] [int] NOT NULL,
	[FileName] [nvarchar](50) NOT NULL,
	[ConfirmationText] [nvarchar](1000) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblTradeFileConfirmations] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblTradeFilePending]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblTradeFilePending]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblTradeFilePending](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([TransactionParentID]),
	[BrokerID] [int] NOT NULL,
	[BrokerSpecific] [nvarchar](50) NOT NULL,
	[TransactionParentID] [int] NOT NULL,
	[WorkflowID] [int] NOT NULL,
	[TradefileText] [varchar](1000) NOT NULL,
	[TriggerDate] [datetime] NOT NULL,
	[Comment] [nvarchar](100) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblTradeFilePending] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT SELECT ON [dbo].[tblTradeFilePending] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblTradeFilePending] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblTradeFilePending] TO [InvestMaster_MonthEnd] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblTradeFilePending] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblTradeFilePending] TO [web_server] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblTradeFilePending] ([Comment]) TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblTradeFilePending] ([Comment]) TO [InvestMaster_Manager] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblTradeFilePending] ([Comment]) TO [InvestMaster_MonthEnd] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblTradeFilePending] ([DateDeleted]) TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblTradeFilePending] ([DateDeleted]) TO [InvestMaster_Manager] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblTradeFilePending] ([DateDeleted]) TO [InvestMaster_MonthEnd] AS [dbo]
GO
/****** Object:  Table [dbo].[tblTradeStatus]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblTradeStatus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblTradeStatus](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([TradeStatusID]),
	[TradeStatusID] [int] NOT NULL,
	[TradeStatusName] [nvarchar](50) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblTradeStatus] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblTransaction]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblTransaction]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblTransaction](
	[RN] [int] IDENTITY(3200000,1) NOT NULL,
	[AuditID]  AS ([TransactionParentID]),
	[TransactionID]  AS ([RN]),
	[TransactionTicket] [nvarchar](50) NOT NULL,
	[TransactionFund] [int] NOT NULL,
	[TransactionSubFund] [int] NULL,
	[TransactionInstrument] [int] NOT NULL,
	[TransactionType] [int] NOT NULL,
	[TransactionType_Contra] [int] NOT NULL,
	[TransactionValueorAmount] [nvarchar](10) NOT NULL,
	[TransactionUnits] [float] NOT NULL,
	[TransactionSignedUnits] [float] NOT NULL,
	[TransactionPrice] [float] NOT NULL,
	[TransactionCosts] [float] NOT NULL,
	[TransactionCostPercent] [float] NOT NULL,
	[TransactionSettlement] [float] NOT NULL,
	[TransactionSignedSettlement] [float] NOT NULL,
	[TransactionFXRate] [float] NOT NULL,
	[TransactionSettlementCurrencyID] [float] NOT NULL,
	[TransactionEffectivePrice] [float] NOT NULL,
	[TransactionEffectiveWatermark] [float] NOT NULL,
	[TransactionSpecificInitialEqualisationValue] [float] NOT NULL,
	[TransactionCounterparty] [int] NOT NULL,
	[TransactionInvestment] [nvarchar](50) NOT NULL,
	[TransactionExecution] [nvarchar](50) NOT NULL,
	[TransactionDataEntry] [nvarchar](50) NOT NULL,
	[TransactionDecisionDate] [datetime] NULL,
	[TransactionValueDate] [datetime] NOT NULL,
	[TransactionFIFOValueDate] [datetime] NOT NULL,
	[TransactionSettlementDate] [datetime] NOT NULL,
	[TransactionConfirmationDate] [datetime] NULL,
	[TransactionEntryDate] [datetime] NOT NULL,
	[TransactionEffectiveValueDate] [datetime] NULL,
	[TransactionCostIsPercent] [bit] NOT NULL,
	[TransactionExemptFromUpdate] [bit] NOT NULL,
	[TransactionRedeemWholeHoldingFlag] [bit] NOT NULL,
	[TransactionIsTransfer] [bit] NOT NULL,
	[TransactionSpecificInitialEqualisationFlag] [bit] NOT NULL,
	[TransactionFuturesStylePricing] [bit] NOT NULL,
	[TransactionFinalAmount] [int] NOT NULL,
	[TransactionFinalPrice] [int] NOT NULL,
	[TransactionFinalCosts] [int] NOT NULL,
	[TransactionFinalSettlement] [int] NOT NULL,
	[TransactionInstructionFlag] [int] NOT NULL,
	[TransactionBankConfirmation] [int] NOT NULL,
	[TransactionInitialDataEntry] [int] NOT NULL,
	[TransactionAmountConfirmed] [int] NOT NULL,
	[TransactionSettlementConfirmed] [int] NOT NULL,
	[TransactionFinalDataEntry] [int] NOT NULL,
	[TransactionComment] [varchar](500) NOT NULL,
	[TransactionParentID] [int] NOT NULL,
	[TransactionRedemptionID] [int] NOT NULL,
	[TransactionVersusInstrument] [int] NOT NULL,
	[TransactionUser] [varchar](50) NOT NULL,
	[TransactionGroup] [nvarchar](50) NOT NULL,
	[TransactionLeg] [int] NOT NULL,
	[TransactionTradeStatusID] [int] NULL,
	[TransactionAdminStatus] [int] NOT NULL,
	[TransactionCTFLAGS] [int] NOT NULL,
	[TransactionCompoundRN] [int] NOT NULL,
	[TransactionVersion] [int] NOT NULL,
	[TransactionDateEntered]  AS ([TransactionEntryDate]),
	[TransactionDateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblTransaction] PRIMARY KEY NONCLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT INSERT ON [dbo].[tblTransaction] TO [InvestMaster_AddTransaction] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblTransaction] TO [InvestMaster_AddTransaction] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblTransaction] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblTransaction] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblTransaction] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblTransaction] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblTransaction] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblTransaction] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblTransaction] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblTransaction] TO [web_server] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblTransaction] ([TransactionParentID]) TO [InvestMaster_AddTransaction] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblTransaction] ([TransactionParentID]) TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblTransaction] ([TransactionParentID]) TO [InvestMaster_Manager] AS [dbo]
GO
/****** Object:  Table [dbo].[tblTransactionType]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblTransactionType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblTransactionType](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([TransactionTypeID]),
	[TransactionTypeID] [int] NOT NULL,
	[TransactionType] [nvarchar](50) NULL,
	[TransactionTypeCashMove] [int] NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblTransactionType] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT INSERT ON [dbo].[tblTransactionType] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblTransactionType] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblTransactionType] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblTransactionType] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblTransactionType] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblTransactionType] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblTransactionType] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblTransactionType] TO [web_server] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblTransactionType] ([DateDeleted]) TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblTransactionType] ([DateDeleted]) TO [InvestMaster_Manager] AS [dbo]
GO
/****** Object:  Table [dbo].[tblTriggerReferencePrices]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblTriggerReferencePrices]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblTriggerReferencePrices](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([RN]),
	[TaskID] [int] NOT NULL,
	[Ticker] [varchar](50) NOT NULL,
	[ReferencePrice] [float] NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblTriggerReferencePrices] PRIMARY KEY CLUSTERED 
(
	[TaskID] ASC,
	[Ticker] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT DELETE ON [dbo].[tblTriggerReferencePrices] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblTriggerReferencePrices] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblTriggerReferencePrices] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblTriggerReferencePrices] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblTriggerReferencePrices] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblTriggerReferencePrices] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblTriggerReferencePrices] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT DELETE ON [dbo].[tblTriggerReferencePrices] TO [Naples_Role] AS [dbo]
GO
GRANT INSERT ON [dbo].[tblTriggerReferencePrices] TO [Naples_Role] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblTriggerReferencePrices] TO [Naples_Role] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblTriggerReferencePrices] TO [Naples_Role] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblTriggerReferencePrices] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblUnitHolderFees]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblUnitHolderFees]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblUnitHolderFees](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([RN]),
	[FeeTransactionID] [int] NOT NULL,
	[FeeTransactionParentID] [int] NOT NULL,
	[FeeFund] [int] NOT NULL,
	[FundBaseCurrency] [int] NOT NULL,
	[FeeCounterparty] [int] NOT NULL,
	[FeeInstrumentID] [int] NOT NULL,
	[FeeDate] [datetime] NOT NULL,
	[TransactionType] [int] NOT NULL,
	[TransactionHolding] [float] NOT NULL,
	[TransactionPrice] [float] NOT NULL,
	[TransactionValueDate] [datetime] NOT NULL,
	[InitialEqualisation] [float] NOT NULL,
	[CurrentEqualisation] [float] NOT NULL,
	[ContingentRedemption] [float] NOT NULL,
	[RedemptionRatio] [float] NOT NULL,
	[ManagementFees_Gross] [float] NOT NULL,
	[ManagementFees_Net] [float] NOT NULL,
	[ManagementFees_DebtRebate] [float] NOT NULL,
	[ManagementFees_EquityRebate] [float] NOT NULL,
	[ManagementFees_MarketingFCAM] [float] NOT NULL,
	[ManagementFees_MarketingOthers] [float] NOT NULL,
	[PerformanceFees_AccruedGross] [float] NOT NULL,
	[PerformanceFees_Gross] [float] NOT NULL,
	[PerformanceFees_Net] [float] NOT NULL,
	[PerformanceFees_DebtRebate] [float] NOT NULL,
	[PerformanceFees_EquityRebate] [float] NOT NULL,
	[PerformanceFees_MarketingFCAM] [float] NOT NULL,
	[PerformanceFees_MarketingOthers] [float] NOT NULL,
	[FXtoUSD] [float] NOT NULL,
	[GrossManagementFee_Percent] [float] NOT NULL,
	[GrossPerformanceFee_Percent] [float] NOT NULL,
	[PerformanceHurdle] [float] NOT NULL,
	[FeeTransactionWatermarkDate] [datetime] NOT NULL,
	[FeeKnowledgedateUsed] [datetime] NOT NULL,
	[IsMilestone] [bit] NOT NULL,
	[UserEntered] [varchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblUnitHolderFees] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT SELECT ON [dbo].[tblUnitHolderFees] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblUnitHolderFees] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblUnitHolderFees] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblUnitHolderFees] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblUnitHolderFees] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblUserPermissions]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblUserPermissions]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblUserPermissions](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([RN]),
	[ID] [int] NOT NULL,
	[AppName] [varchar](30) NOT NULL,
	[Username] [varchar](50) NOT NULL,
	[UserType] [int] NOT NULL,
	[Feature] [varchar](50) NOT NULL,
	[FeatureType] [int] NOT NULL,
	[Perm_Read] [int] NOT NULL,
	[Perm_Insert] [int] NOT NULL,
	[Perm_Update] [int] NOT NULL,
	[Perm_Delete] [int] NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_UserPermissions] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT INSERT ON [dbo].[tblUserPermissions] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblUserPermissions] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblUserPermissions] TO [InvestMaster_DataEntry] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblUserPermissions] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[tblUserPermissions] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblUserPermissions] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblUserPermissions] TO [web_server] AS [dbo]
GO
GRANT UPDATE ON [dbo].[tblUserPermissions] ([DateDeleted]) TO [InvestMaster_Admin] AS [dbo]
GO
/****** Object:  Table [dbo].[tblVeniceFormTooltips]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblVeniceFormTooltips]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblVeniceFormTooltips](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([RN]),
	[AppName] [varchar](30) NOT NULL,
	[FormName] [varchar](50) NOT NULL,
	[ControlName] [varchar](50) NOT NULL,
	[ToolTip] [varchar](100) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblVeniceFormTooltips] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
GRANT VIEW DEFINITION ON [dbo].[tblVeniceFormTooltips] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblVeniceFormTooltips] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[tblWorkflowRules]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblWorkflowRules]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblWorkflowRules](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([RuleID]),
	[RuleID] [int] NOT NULL,
	[Fund] [int] NOT NULL,
	[InstrumentType] [int] NOT NULL,
	[Exchange] [int] NOT NULL,
	[Currency] [int] NOT NULL,
	[InstrumentID] [int] NOT NULL,
	[CurrentStatus] [int] NOT NULL,
	[RequiresApproval] [int] NOT NULL,
	[IsApprovalRule] [int] NOT NULL,
	[IsNewTrade] [int] NOT NULL,
	[ResultingWorkflow] [int] NOT NULL,
	[RuleDescription] [nvarchar](100) NOT NULL,
	[UserName] [nchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblWorkflowRules] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tblWorkflows]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblWorkflows]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tblWorkflows](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([WorkflowID]),
	[WorkflowID] [int] NOT NULL,
	[WorkflowName] [nvarchar](50) NOT NULL,
	[WorkflowDescription] [nvarchar](255) NOT NULL,
	[WorkDetails] [int] NOT NULL,
	[CreateTradeFile] [nvarchar](50) NOT NULL,
	[ResultingTradeStatus] [int] NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblWorkflows] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
GRANT VIEW DEFINITION ON [dbo].[tblWorkflows] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[tblWorkflows] TO [web_server] AS [dbo]
GO
/****** Object:  Table [dbo].[TITRES]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TITRES]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TITRES](
	[SICOVAM] [int] NOT NULL,
	[LIBELLE] [nvarchar](40) NULL,
	[TYPE] [nvarchar](1) NULL,
	[COMM] [nvarchar](200) NULL,
	[EXSOC] [date] NULL,
	[CROIDIV] [int] NULL,
	[QUOTITE] [float] NULL,
	[ECHEANCE] [date] NULL,
	[NOMINAL] [float] NULL,
	[DATEREGL] [date] NULL,
	[NBTITRES] [int] NULL,
	[CODESJ] [int] NULL,
	[TYPESJ] [int] NULL,
	[TYPEPRO] [int] NULL,
	[PRIXEXER] [int] NULL,
	[PROPORTION] [int] NULL,
	[PARITE] [float] NULL,
	[EMISSION] [date] NULL,
	[PRIXEMI] [int] NULL,
	[DEBUTPER] [date] NULL,
	[FINPER] [date] NULL,
	[NBPTS] [int] NULL,
	[MONTANT] [int] NULL,
	[TAUX] [int] NULL,
	[JAMBE1] [int] NULL,
	[DUREE1] [int] NULL,
	[FIXE1] [int] NULL,
	[DATECOUPON1] [date] NULL,
	[COUPON1] [int] NULL,
	[JAMBE2] [int] NULL,
	[DUREE2] [int] NULL,
	[FIXE2] [int] NULL,
	[DATECOUPON2] [date] NULL,
	[COUPON2] [int] NULL,
	[DATEFINAL] [date] NULL,
	[DATECALCUL] [date] NULL,
	[J1REFCON1] [int] NULL,
	[J1REFCON2] [int] NULL,
	[J2REFCON1] [int] NULL,
	[J2REFCON2] [int] NULL,
	[J1REFCON3] [int] NULL,
	[J2REFCON3] [int] NULL,
	[DEVISECTT] [int] NULL,
	[DEVISEEXER] [int] NULL,
	[CODE_EMET] [int] NULL,
	[DEVISEAC] [int] NULL,
	[MARCHE] [int] NULL,
	[BASE1] [int] NULL,
	[BASE2] [int] NULL,
	[MARCHE_ACT] [int] NULL,
	[BETA] [int] NULL,
	[TAUX_VAR] [int] NULL,
	[MNEMO] [int] NULL,
	[GROSCOUPON1] [int] NULL,
	[GROSCOUPON2] [int] NULL,
	[AFFECTATION] [int] NULL,
	[MODELE] [nvarchar](40) NULL,
	[REFERENCE] [nvarchar](24) NULL,
	[TYPEDERIVE] [int] NULL,
	[CODESJ2] [int] NULL,
	[QUANTITE2] [int] NULL,
	[MNEMO_V2] [nvarchar](50) NULL,
	[EXTERNREF] [nvarchar](40) NULL,
	[FIXING_TYPE] [int] NULL,
	[HOLIDAYADJUSTMENT] [int] NULL,
	[STATUS] [int] NULL,
	[CREDIT_SPREAD_VOLAT] [int] NULL,
	[CREDIT_SPREAD_CORRELATION] [int] NULL,
	[QUOTATION_TYPE] [int] NULL,
	[SENIORITY] [int] NULL,
	[ISSUER] [int] NULL,
	[FAMILY] [int] NULL,
	[FREQ_COUPON] [int] NULL,
	[PERIMETERID] [int] NULL,
	[PREPAYMENT] [nvarchar](40) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[viewChangeControl]    Script Date: 6/7/2013 5:05:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[viewChangeControl]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[viewChangeControl](
	[RN] [int] NOT NULL,
	[AuditID] [int] NOT NULL,
	[ChangeID] [int] NOT NULL,
	[Product] [varchar](50) NOT NULL,
	[Date_Raised] [datetime] NOT NULL,
	[User_Raised] [varchar](50) NOT NULL,
	[Change_Category] [varchar](50) NOT NULL,
	[Change_Priority] [int] NOT NULL,
	[Change_ReferenceNumber] [varchar](50) NOT NULL,
	[Text_Change] [varchar](500) NULL,
	[Text_Detail] [varchar](500) NULL,
	[Text_ScopeOfChange] [varchar](500) NULL,
	[Text_Dependencies] [varchar](500) NULL,
	[Text_Restrictions] [varchar](500) NULL,
	[Text_ImplementationPlan] [varchar](500) NULL,
	[Text_TestPlan] [varchar](500) NULL,
	[Date_ReviewedIT] [datetime] NULL,
	[Date_ReviewRejected] [datetime] NULL,
	[Date_AuthorisedIT] [datetime] NULL,
	[Date_AuthorisedOwner] [datetime] NULL,
	[Date_AuthorisedBusiness] [datetime] NULL,
	[Date_AuthoriseRejected] [datetime] NULL,
	[Date_AcceptedIT] [datetime] NULL,
	[Date_AcceptedOwner] [datetime] NULL,
	[Date_AcceptedBusiness] [datetime] NULL,
	[User_ReviewedIT] [varchar](50) NULL,
	[User_ReviewRejected] [varchar](50) NULL,
	[User_AuthorisedIT] [varchar](50) NULL,
	[User_AuthorisedOwner] [varchar](50) NULL,
	[User_AuthorisedBusiness] [varchar](50) NULL,
	[User_AuthoriseRejected] [varchar](50) NULL,
	[User_AcceptedIT] [varchar](50) NULL,
	[User_AcceptedOwner] [varchar](50) NULL,
	[User_AcceptedBusiness] [varchar](50) NULL,
	[Flag_ReviewedIT] [int] NOT NULL,
	[Flag_ReviewRejected] [int] NOT NULL,
	[Flag_AuthorisedIT] [int] NOT NULL,
	[Flag_AuthorisedOwner] [int] NOT NULL,
	[Flag_AuthorisedBusiness] [int] NOT NULL,
	[Flag_AuthoriseRejected] [int] NOT NULL,
	[Flag_AcceptedIT] [int] NOT NULL,
	[Flag_AcceptedOwner] [int] NOT NULL,
	[Flag_AcceptedBusiness] [int] NOT NULL,
	[appCurrentUser] [varchar](50) NULL,
	[DateEntered] [datetime] NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAttribution_AttribFund]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAttribution] ADD  CONSTRAINT [DF_tblAttribution_AttribFund]  DEFAULT ((0)) FOR [AttribFund]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAttribution_AttribInstrument]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAttribution] ADD  CONSTRAINT [DF_tblAttribution_AttribInstrument]  DEFAULT ((0)) FOR [AttribInstrument]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAttribution_AttribDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAttribution] ADD  CONSTRAINT [DF_tblAttribution_AttribDate]  DEFAULT ('1900-01-01') FOR [AttribDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAttribution_AttribPeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAttribution] ADD  CONSTRAINT [DF_tblAttribution_AttribPeriod]  DEFAULT ((4)) FOR [AttribPeriod]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAttribution_AttribInvested]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAttribution] ADD  CONSTRAINT [DF_tblAttribution_AttribInvested]  DEFAULT ((0)) FOR [AttribInvested]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAttribution_AttribWeight]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAttribution] ADD  CONSTRAINT [DF_tblAttribution_AttribWeight]  DEFAULT ((0)) FOR [AttribWeight]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAttribution_AttribEndWeight]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAttribution] ADD  CONSTRAINT [DF_tblAttribution_AttribEndWeight]  DEFAULT ((0)) FOR [AttribEndWeight]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAttribution_AttribProfit]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAttribution] ADD  CONSTRAINT [DF_tblAttribution_AttribProfit]  DEFAULT ((0)) FOR [AttribProfit]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAttribution_AttribPercentReturn]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAttribution] ADD  CONSTRAINT [DF_tblAttribution_AttribPercentReturn]  DEFAULT ((0)) FOR [AttribPercentReturn]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAttribution_AttribBips]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAttribution] ADD  CONSTRAINT [DF_tblAttribution_AttribBips]  DEFAULT ((0)) FOR [AttribBips]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAttribution_AttribTargetBps]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAttribution] ADD  CONSTRAINT [DF_tblAttribution_AttribTargetBps]  DEFAULT ((0)) FOR [AttribExpectedBips]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAttribution_AttribTotalValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAttribution] ADD  CONSTRAINT [DF_tblAttribution_AttribTotalValue]  DEFAULT ((0)) FOR [AttribTotalValue]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAttribution_AttribFinalUnits]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAttribution] ADD  CONSTRAINT [DF_tblAttribution_AttribFinalUnits]  DEFAULT ((0)) FOR [AttribFinalUnits]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAttribution_AttribStatusGroupID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAttribution] ADD  CONSTRAINT [DF_tblAttribution_AttribStatusGroupID]  DEFAULT ((0)) FOR [AttribStatusGroupID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAttribution_AttribDatesFilter]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAttribution] ADD  CONSTRAINT [DF_tblAttribution_AttribDatesFilter]  DEFAULT ((0)) FOR [AttribDatesFilter]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAttribution_AttribIsMilestone]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAttribution] ADD  CONSTRAINT [DF_tblAttribution_AttribIsMilestone]  DEFAULT ((0)) FOR [AttribIsMilestone]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAttribution_AttribIsLookthrough]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAttribution] ADD  CONSTRAINT [DF_tblAttribution_AttribIsLookthrough]  DEFAULT ((0)) FOR [AttribIsLookthrough]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAttribution_AttribLookthroughGeneratedRecord]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAttribution] ADD  CONSTRAINT [DF_tblAttribution_AttribLookthroughGeneratedRecord]  DEFAULT ((0)) FOR [AttribLookthroughGeneratedRecord]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAttribution_AttribDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAttribution] ADD  CONSTRAINT [DF_tblAttribution_AttribDateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_CURRENCY]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] ADD  CONSTRAINT [DF_tblAutoUpdate_HAS_CURRENCY]  DEFAULT ((0)) FOR [HAS_CURRENCY]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_COUNTERPARTY]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] ADD  CONSTRAINT [DF_tblAutoUpdate_HAS_COUNTERPARTY]  DEFAULT ((0)) FOR [HAS_COUNTERPARTY]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_DEALINGPERIOD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] ADD  CONSTRAINT [DF_tblAutoUpdate_HAS_DEALINGPERIOD]  DEFAULT ((0)) FOR [HAS_DEALINGPERIOD]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_FUND]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] ADD  CONSTRAINT [DF_tblAutoUpdate_HAS_FUND]  DEFAULT ((0)) FOR [HAS_FUND]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_FUNDTYPE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] ADD  CONSTRAINT [DF_tblAutoUpdate_HAS_FUNDTYPE]  DEFAULT ((0)) FOR [HAS_FUNDTYPE]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_INSTRUMENT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] ADD  CONSTRAINT [DF_tblAutoUpdate_HAS_INSTRUMENT]  DEFAULT ((0)) FOR [HAS_INSTRUMENT]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_INSTRUMENTTYPE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] ADD  CONSTRAINT [DF_tblAutoUpdate_HAS_INSTRUMENTTYPE]  DEFAULT ((0)) FOR [HAS_INSTRUMENTTYPE]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_INVESTORGROUP]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] ADD  CONSTRAINT [DF_tblAutoUpdate_HAS_INVESTORGROUP]  DEFAULT ((0)) FOR [HAS_INVESTORGROUP]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_LIMITS]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] ADD  CONSTRAINT [DF_tblAutoUpdate_HAS_LIMITS]  DEFAULT ((0)) FOR [HAS_LIMITS]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_LIMITTYPE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] ADD  CONSTRAINT [DF_tblAutoUpdate_HAS_LIMITTYPE]  DEFAULT ((0)) FOR [HAS_LIMITTYPE]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_PERSON]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] ADD  CONSTRAINT [DF_tblAutoUpdate_HAS_PERSON]  DEFAULT ((0)) FOR [HAS_PERSON]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_TRANSACTION]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] ADD  CONSTRAINT [DF_tblAutoUpdate_HAS_TRANSACTION]  DEFAULT ((0)) FOR [HAS_TRANSACTION]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_TRANSACTIONTYPE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] ADD  CONSTRAINT [DF_tblAutoUpdate_HAS_TRANSACTIONTYPE]  DEFAULT ((0)) FOR [HAS_TRANSACTIONTYPE]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_ESTIMATE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] ADD  CONSTRAINT [DF_tblAutoUpdate_HAS_ESTIMATE]  DEFAULT ((0)) FOR [HAS_ESTIMATE]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_FX]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] ADD  CONSTRAINT [DF_tblAutoUpdate_HAS_FX]  DEFAULT ((0)) FOR [HAS_FX]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_PRICE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] ADD  CONSTRAINT [DF_tblAutoUpdate_HAS_PRICE]  DEFAULT ((0)) FOR [HAS_PRICE]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_SYSTEMDATES]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] ADD  CONSTRAINT [DF_tblAutoUpdate_HAS_SYSTEMDATES]  DEFAULT ((0)) FOR [HAS_SYSTEMDATES]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_PERMISSIONS]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] ADD  CONSTRAINT [DF_tblAutoUpdate_HAS_PERMISSIONS]  DEFAULT ((0)) FOR [HAS_PERMISSIONS]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_KNOWLEDGEDATE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] ADD  CONSTRAINT [DF_tblAutoUpdate_HAS_KNOWLEDGEDATE]  DEFAULT ((0)) FOR [HAS_KNOWLEDGEDATE]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_FUNDMILESTONE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] ADD  CONSTRAINT [DF_tblAutoUpdate_HAS_FUNDMILESTONE]  DEFAULT ((0)) FOR [HAS_FUNDMILESTONE]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_CHANGECONTROL]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] ADD  CONSTRAINT [DF_tblAutoUpdate_HAS_CHANGECONTROL]  DEFAULT ((0)) FOR [HAS_CHANGECONTROL]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_COMPOUNDTRANSACTION]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] ADD  CONSTRAINT [DF_tblAutoUpdate_HAS_COMPOUNDTRANSACTION]  DEFAULT ((0)) FOR [HAS_COMPOUNDTRANSACTION]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_TRANSACTIONTEMPLATE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] ADD  CONSTRAINT [DF_tblAutoUpdate_HAS_TRANSACTIONTEMPLATE]  DEFAULT ((0)) FOR [HAS_TRANSACTIONTEMPLATE]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_BOOKMARK]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] ADD  CONSTRAINT [DF_tblAutoUpdate_HAS_BOOKMARK]  DEFAULT ((0)) FOR [HAS_BOOKMARK]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_TRADEFILEPENDING]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] ADD  CONSTRAINT [DF_tblAutoUpdate_HAS_TRADEFILEPENDING]  DEFAULT ((0)) FOR [HAS_TRADEFILEPENDING]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_TRADEFILEACKNOWLEDGEMENT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] ADD  CONSTRAINT [DF_tblAutoUpdate_HAS_TRADEFILEACKNOWLEDGEMENT]  DEFAULT ((0)) FOR [HAS_TRADEFILEACKNOWLEDGEMENT]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblAutoUpdate_HAS_TRADEFILECONFIRMATIONS]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblAutoUpdate] ADD  CONSTRAINT [DF_tblAutoUpdate_HAS_TRADEFILECONFIRMATIONS]  DEFAULT ((0)) FOR [HAS_TRADEFILECONFIRMATIONS]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBenchmarkIndex_BenchmarkType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBenchmarkIndex] ADD  CONSTRAINT [DF_tblBenchmarkIndex_BenchmarkType]  DEFAULT ((0)) FOR [BenchmarkType]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBenchmarkIndex_BenchmarkCalculationMethod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBenchmarkIndex] ADD  CONSTRAINT [DF_tblBenchmarkIndex_BenchmarkCalculationMethod]  DEFAULT ((1)) FOR [BenchmarkCalculationMethod]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBenchmarkIndex_UserName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBenchmarkIndex] ADD  CONSTRAINT [DF_tblBenchmarkIndex_UserName]  DEFAULT (user_name()) FOR [UserEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBenchmarkIndex_FundDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBenchmarkIndex] ADD  CONSTRAINT [DF_tblBenchmarkIndex_FundDateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBenchmarkItems_BenchmarkItemCalculationMethod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBenchmarkItems] ADD  CONSTRAINT [DF_tblBenchmarkItems_BenchmarkItemCalculationMethod]  DEFAULT ((0)) FOR [BenchmarkItemCalculationMethod]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBenchmarkItems_BenchmarkInstrumentID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBenchmarkItems] ADD  CONSTRAINT [DF_tblBenchmarkItems_BenchmarkInstrumentID]  DEFAULT ((0)) FOR [BenchmarkInstrumentID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBenchmarkItems_BenchmarkFixedRate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBenchmarkItems] ADD  CONSTRAINT [DF_tblBenchmarkItems_BenchmarkFixedRate]  DEFAULT ((0)) FOR [BenchmarkFixedRate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBenchmarkItems_UserName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBenchmarkItems] ADD  CONSTRAINT [DF_tblBenchmarkItems_UserName]  DEFAULT (user_name()) FOR [UserEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBenchmarkItems_FundDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBenchmarkItems] ADD  CONSTRAINT [DF_tblBenchmarkItems_FundDateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBookmarks_BookmarkUserID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBookmark] ADD  CONSTRAINT [DF_tblBookmarks_BookmarkUserID]  DEFAULT ((0)) FOR [BookmarkUserID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBookmarks_BookMarkDescription]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBookmark] ADD  CONSTRAINT [DF_tblBookmarks_BookMarkDescription]  DEFAULT ('') FOR [BookMarkDescription]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBookmarks_BookmarkDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBookmark] ADD  CONSTRAINT [DF_tblBookmarks_BookmarkDate]  DEFAULT (getdate()) FOR [BookmarkDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBookmarks_BookmarkWholeDay]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBookmark] ADD  CONSTRAINT [DF_tblBookmarks_BookmarkWholeDay]  DEFAULT ((0)) FOR [BookmarkWholeDay]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBookmarks_BookmarkVisibleToAll]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBookmark] ADD  CONSTRAINT [DF_tblBookmarks_BookmarkVisibleToAll]  DEFAULT ((-1)) FOR [BookmarkVisibleToAll]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBookmark_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBookmark] ADD  CONSTRAINT [DF_tblBookmark_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBookmark_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBookmark] ADD  CONSTRAINT [DF_tblBookmark_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBroker_NLS_Culture]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBroker] ADD  CONSTRAINT [DF_tblBroker_NLS_Culture]  DEFAULT (N'fr-FR') FOR [NLS_Culture]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBroker_NumericPrecision]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBroker] ADD  CONSTRAINT [DF_tblBroker_NumericPrecision]  DEFAULT ((6)) FOR [NumericPrecision]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBroker_UserName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBroker] ADD  CONSTRAINT [DF_tblBroker_UserName]  DEFAULT (user_name()) FOR [UserName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBroker_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBroker] ADD  CONSTRAINT [DF_tblBroker_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBrokerAccounts_FundID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBrokerAccounts] ADD  CONSTRAINT [DF_tblBrokerAccounts_FundID]  DEFAULT ((0)) FOR [FundID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBrokerAccounts_InstrumentTypeID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBrokerAccounts] ADD  CONSTRAINT [DF_tblBrokerAccounts_InstrumentTypeID]  DEFAULT ((0)) FOR [InstrumentTypeID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBrokerAccounts_Exchange]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBrokerAccounts] ADD  CONSTRAINT [DF_tblBrokerAccounts_Exchange]  DEFAULT ((0)) FOR [Exchange]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBrokerAccounts_CurrencyID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBrokerAccounts] ADD  CONSTRAINT [DF_tblBrokerAccounts_CurrencyID]  DEFAULT ((0)) FOR [CurrencyID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBrokerAccounts_InstrumentID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBrokerAccounts] ADD  CONSTRAINT [DF_tblBrokerAccounts_InstrumentID]  DEFAULT ((0)) FOR [InstrumentID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBrokerAccounts_UserName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBrokerAccounts] ADD  CONSTRAINT [DF_tblBrokerAccounts_UserName]  DEFAULT (user_name()) FOR [UserName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblBrokerAccounts_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblBrokerAccounts] ADD  CONSTRAINT [DF_tblBrokerAccounts_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblChangeControl_Product]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblChangeControl] ADD  CONSTRAINT [DF_tblChangeControl_Product]  DEFAULT ('<none>') FOR [Product]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblChangeControl_Text_Change]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblChangeControl] ADD  CONSTRAINT [DF_tblChangeControl_Text_Change]  DEFAULT ('') FOR [Text_Change]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblChangeControl_Text_Detail]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblChangeControl] ADD  CONSTRAINT [DF_tblChangeControl_Text_Detail]  DEFAULT ('') FOR [Text_Detail]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblChangeControl_Text_ScopeOfChange]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblChangeControl] ADD  CONSTRAINT [DF_tblChangeControl_Text_ScopeOfChange]  DEFAULT ('') FOR [Text_ScopeOfChange]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblChangeControl_Text_Dependencies]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblChangeControl] ADD  CONSTRAINT [DF_tblChangeControl_Text_Dependencies]  DEFAULT ('') FOR [Text_Dependencies]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblChangeControl_Text_Restrictions]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblChangeControl] ADD  CONSTRAINT [DF_tblChangeControl_Text_Restrictions]  DEFAULT ('') FOR [Text_Restrictions]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblChangeControl_Text_ImplementationPlan]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblChangeControl] ADD  CONSTRAINT [DF_tblChangeControl_Text_ImplementationPlan]  DEFAULT ('') FOR [Text_ImplementationPlan]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblChangeControl_Text_TestPlan]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblChangeControl] ADD  CONSTRAINT [DF_tblChangeControl_Text_TestPlan]  DEFAULT ('') FOR [Text_TestPlan]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblChangeControl_User_ReviewedIT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblChangeControl] ADD  CONSTRAINT [DF_tblChangeControl_User_ReviewedIT]  DEFAULT ('') FOR [User_ReviewedIT]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblChangeControl_User_ReviewRejected]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblChangeControl] ADD  CONSTRAINT [DF_tblChangeControl_User_ReviewRejected]  DEFAULT ('') FOR [User_ReviewRejected]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblChangeControl_User_AuthorisedIT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblChangeControl] ADD  CONSTRAINT [DF_tblChangeControl_User_AuthorisedIT]  DEFAULT ('') FOR [User_AuthorisedIT]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblChangeControl_User_AuthorisedOwner]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblChangeControl] ADD  CONSTRAINT [DF_tblChangeControl_User_AuthorisedOwner]  DEFAULT ('') FOR [User_AuthorisedOwner]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblChangeControl_User_AuthorisedBusiness]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblChangeControl] ADD  CONSTRAINT [DF_tblChangeControl_User_AuthorisedBusiness]  DEFAULT ('') FOR [User_AuthorisedBusiness]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblChangeControl_User_AuthoriseRejected]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblChangeControl] ADD  CONSTRAINT [DF_tblChangeControl_User_AuthoriseRejected]  DEFAULT ('') FOR [User_AuthoriseRejected]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblChangeControl_User_AcceptedIT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblChangeControl] ADD  CONSTRAINT [DF_tblChangeControl_User_AcceptedIT]  DEFAULT ('') FOR [User_AcceptedIT]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblChangeControl_User_AcceptedOwner]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblChangeControl] ADD  CONSTRAINT [DF_tblChangeControl_User_AcceptedOwner]  DEFAULT ('') FOR [User_AcceptedOwner]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblChangeControl_User_AcceptedBusiness]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblChangeControl] ADD  CONSTRAINT [DF_tblChangeControl_User_AcceptedBusiness]  DEFAULT ('') FOR [User_AcceptedBusiness]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblChangeControl_appCurrentUser]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblChangeControl] ADD  CONSTRAINT [DF_tblChangeControl_appCurrentUser]  DEFAULT (user_name()) FOR [appCurrentUser]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblChangeControl_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblChangeControl] ADD  CONSTRAINT [DF_tblChangeControl_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_AdminStatus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_AdminStatus]  DEFAULT ((-10)) FOR [AdminStatus]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionParentID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_TransactionParentID]  DEFAULT ((0)) FOR [TransactionParentID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionCompoundID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_TransactionCompoundID]  DEFAULT ((0)) FOR [TransactionCompoundID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_CompoundTransactionType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_CompoundTransactionType]  DEFAULT ((0)) FOR [TransactionTemplateID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionTicket]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_TransactionTicket]  DEFAULT ('') FOR [TransactionTicket]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionInstrument]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_TransactionInstrument]  DEFAULT ((0)) FOR [TransactionInstrument]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionValueorAmount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_TransactionValueorAmount]  DEFAULT ('Amount') FOR [TransactionValueorAmount]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionCosts]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_TransactionCosts]  DEFAULT ((0)) FOR [TransactionCosts]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionCostPercent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_TransactionCostPercent]  DEFAULT ((0)) FOR [TransactionCostPercent]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionEffectivePrice]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_TransactionEffectivePrice]  DEFAULT ((0)) FOR [TransactionEffectivePrice]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionEffectiveWatermark]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_TransactionEffectiveWatermark]  DEFAULT ((0)) FOR [TransactionEffectiveWatermark]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionSpecificInitialEqualisationValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_TransactionSpecificInitialEqualisationValue]  DEFAULT ((0)) FOR [TransactionSpecificInitialEqualisationValue]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionInvestment]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_TransactionInvestment]  DEFAULT ('') FOR [TransactionInvestment]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionExecution]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_TransactionExecution]  DEFAULT ('') FOR [TransactionExecution]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionDataEntry]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_TransactionDataEntry]  DEFAULT ('') FOR [TransactionDataEntry]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionDecisionDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_TransactionDecisionDate]  DEFAULT ((1)) FOR [TransactionDecisionDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionSettlementDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_TransactionSettlementDate]  DEFAULT ((1)) FOR [TransactionSettlementDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionConfirmationDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_TransactionConfirmationDate]  DEFAULT ((1)) FOR [TransactionConfirmationDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionCostIsPercent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_TransactionCostIsPercent]  DEFAULT ((0)) FOR [TransactionCostIsPercent]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionExemptFromUpdate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_TransactionExemptFromUpdate]  DEFAULT ((0)) FOR [TransactionExemptFromUpdate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionIsTransfer]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_TransactionIsTransfer]  DEFAULT ((0)) FOR [TransactionIsTransfer]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionSpecificInitialEqualisationFlag]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_TransactionSpecificInitialEqualisationFlag]  DEFAULT ((0)) FOR [TransactionSpecificInitialEqualisationFlag]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionFinalAmount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_TransactionFinalAmount]  DEFAULT ((0)) FOR [TransactionFinalAmount]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionFinalPrice]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_TransactionFinalPrice]  DEFAULT ((0)) FOR [TransactionFinalPrice]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionFinalCosts]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_TransactionFinalCosts]  DEFAULT ((0)) FOR [TransactionFinalCosts]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionFinalSettlement]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_TransactionFinalSettlement]  DEFAULT ((0)) FOR [TransactionFinalSettlement]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionInstructionFlag]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_TransactionInstructionFlag]  DEFAULT ((0)) FOR [TransactionInstructionFlag]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionBankConfirmation]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_TransactionBankConfirmation]  DEFAULT ((0)) FOR [TransactionBankConfirmation]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionInitialDataEntry]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_TransactionInitialDataEntry]  DEFAULT ((0)) FOR [TransactionInitialDataEntry]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionAmountConfirmed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_TransactionAmountConfirmed]  DEFAULT ((0)) FOR [TransactionAmountConfirmed]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionSettlementConfirmed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_TransactionSettlementConfirmed]  DEFAULT ((0)) FOR [TransactionSettlementConfirmed]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionFinalDataEntry]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_TransactionFinalDataEntry]  DEFAULT ((0)) FOR [TransactionFinalDataEntry]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TradeStatusID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_TradeStatusID]  DEFAULT ((0)) FOR [TransactionTradeStatusID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionComment]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_TransactionComment]  DEFAULT ('') FOR [TransactionComment]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_TransactionGroup]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_TransactionGroup]  DEFAULT ('') FOR [TransactionGroup]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_ParameterCount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_ParameterCount]  DEFAULT ((0)) FOR [ParameterCount]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_Parameter1_Type]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_Parameter1_Type]  DEFAULT ((0)) FOR [Parameter1_Type]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_Parameter2_Type]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_Parameter2_Type]  DEFAULT ((0)) FOR [Parameter2_Type]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_Parameter3_Type]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_Parameter3_Type]  DEFAULT ((0)) FOR [Parameter3_Type]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_Parameter4_Type]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_Parameter4_Type]  DEFAULT ((0)) FOR [Parameter4_Type]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_Parameter5_Type]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_Parameter5_Type]  DEFAULT ((0)) FOR [Parameter5_Type]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_CompoundTransactionGroup]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_CompoundTransactionGroup]  DEFAULT ((0)) FOR [CompoundTransactionGroup]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_CompoundTransactionFlags]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_CompoundTransactionFlags]  DEFAULT ((0)) FOR [CompoundTransactionFlags]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_CompoundTransactionPosted]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_CompoundTransactionPosted]  DEFAULT ((0)) FOR [CompoundTransactionPosted]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransaction_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransaction] ADD  CONSTRAINT [DF_tblCompoundTransaction_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionParameters_ParamType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionParameters] ADD  CONSTRAINT [DF_tblCompoundTransactionParameters_ParamType]  DEFAULT ((0)) FOR [ParamType]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCTParameters_ParamLong]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionParameters] ADD  CONSTRAINT [DF_tblCTParameters_ParamLong]  DEFAULT ((0)) FOR [ParamLong]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCTParameters_ParamString]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionParameters] ADD  CONSTRAINT [DF_tblCTParameters_ParamString]  DEFAULT ('') FOR [ParamString]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCTParameters_ParamFloat]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionParameters] ADD  CONSTRAINT [DF_tblCTParameters_ParamFloat]  DEFAULT ((0)) FOR [ParamFloat]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionParameters_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionParameters] ADD  CONSTRAINT [DF_tblCompoundTransactionParameters_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionParameters_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionParameters] ADD  CONSTRAINT [DF_tblCompoundTransactionParameters_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TemplateDescription]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] ADD  CONSTRAINT [DF_tblCompoundTransactionTemplate_TemplateDescription]  DEFAULT ('') FOR [TemplateDescription]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCTTemplate_TemplateStep]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] ADD  CONSTRAINT [DF_tblCTTemplate_TemplateStep]  DEFAULT ((0)) FOR [TemplateStep]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCTTemplate_TemplateTotalSteps]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] ADD  CONSTRAINT [DF_tblCTTemplate_TemplateTotalSteps]  DEFAULT ((0)) FOR [TemplateTotalSteps]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCTTemplate_TemplateFlags]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] ADD  CONSTRAINT [DF_tblCTTemplate_TemplateFlags]  DEFAULT ((0)) FOR [TemplateFlags]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCTTemplate_TransactionTicket]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] ADD  CONSTRAINT [DF_tblCTTemplate_TransactionTicket]  DEFAULT ('') FOR [TransactionTicket]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionFund]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] ADD  CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionFund]  DEFAULT ('') FOR [TransactionFund]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionInstrument]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] ADD  CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionInstrument]  DEFAULT ('') FOR [TransactionInstrument]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] ADD  CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionType]  DEFAULT ('') FOR [TransactionType]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionValueorAmount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] ADD  CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionValueorAmount]  DEFAULT ('') FOR [TransactionValueorAmount]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionUnits]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] ADD  CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionUnits]  DEFAULT ('') FOR [TransactionUnits]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionPrice]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] ADD  CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionPrice]  DEFAULT ('') FOR [TransactionPrice]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionEffectivePrice]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] ADD  CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionEffectivePrice]  DEFAULT ('') FOR [TransactionEffectivePrice]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCTTemplate_TransactionCosts]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] ADD  CONSTRAINT [DF_tblCTTemplate_TransactionCosts]  DEFAULT ('') FOR [TransactionCosts]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionCostPercent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] ADD  CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionCostPercent]  DEFAULT ('') FOR [TransactionCostPercent]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionSettlement]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] ADD  CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionSettlement]  DEFAULT ('') FOR [TransactionSettlement]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionCounterparty]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] ADD  CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionCounterparty]  DEFAULT ('') FOR [TransactionCounterparty]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionDecisionDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] ADD  CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionDecisionDate]  DEFAULT ('') FOR [TransactionDecisionDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionValueDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] ADD  CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionValueDate]  DEFAULT ('') FOR [TransactionValueDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionFIFOValueDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] ADD  CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionFIFOValueDate]  DEFAULT ('') FOR [TransactionFIFOValueDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionEffectiveValueDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] ADD  CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionEffectiveValueDate]  DEFAULT ('') FOR [TransactionEffectiveValueDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionSettlementDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] ADD  CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionSettlementDate]  DEFAULT ('') FOR [TransactionSettlementDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionConfirmationDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] ADD  CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionConfirmationDate]  DEFAULT ('') FOR [TransactionConfirmationDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionEffectiveWatermark]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] ADD  CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionEffectiveWatermark]  DEFAULT ('') FOR [TransactionEffectiveWatermark]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionSpecificInitialEqualisationValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] ADD  CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionSpecificInitialEqualisationValue]  DEFAULT ('') FOR [TransactionSpecificInitialEqualisationValue]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionCostIsPercent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] ADD  CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionCostIsPercent]  DEFAULT ('') FOR [TransactionCostIsPercent]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionIsTransfer]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] ADD  CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionIsTransfer]  DEFAULT ('') FOR [TransactionIsTransfer]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_TransactionSpecificInitialEqualisationFlag]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] ADD  CONSTRAINT [DF_tblCompoundTransactionTemplate_TransactionSpecificInitialEqualisationFlag]  DEFAULT ('') FOR [TransactionSpecificInitialEqualisationFlag]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCTTemplate_TransactionComment]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] ADD  CONSTRAINT [DF_tblCTTemplate_TransactionComment]  DEFAULT ('') FOR [TransactionComment]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCTTemplate_TransactionGroup]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] ADD  CONSTRAINT [DF_tblCTTemplate_TransactionGroup]  DEFAULT ('') FOR [TransactionGroup]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_R1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] ADD  CONSTRAINT [DF_tblCompoundTransactionTemplate_R1]  DEFAULT ('') FOR [R1]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_R2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] ADD  CONSTRAINT [DF_tblCompoundTransactionTemplate_R2]  DEFAULT ('') FOR [R2]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] ADD  CONSTRAINT [DF_tblCompoundTransactionTemplate_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCompoundTransactionTemplate_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCompoundTransactionTemplate] ADD  CONSTRAINT [DF_tblCompoundTransactionTemplate_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_CounterpartyPFPCID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] ADD  CONSTRAINT [DF_tblCounterparty_CounterpartyPFPCID]  DEFAULT ('') FOR [CounterpartyPFPCID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_CounterpartyInvestorGroup]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] ADD  CONSTRAINT [DF_tblCounterparty_CounterpartyInvestorGroup]  DEFAULT ((0)) FOR [CounterpartyInvestorGroup]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__tblCounte__Count__7B5B524B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] ADD  CONSTRAINT [DF__tblCounte__Count__7B5B524B]  DEFAULT ((0.01)) FOR [CounterpartyManagementFee]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__tblCounte__Count__7C4F7684]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] ADD  CONSTRAINT [DF__tblCounte__Count__7C4F7684]  DEFAULT ((0.1)) FOR [CounterpartyPerformanceFee]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__tblCounte__Count__04E4BC85]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] ADD  CONSTRAINT [DF__tblCounte__Count__04E4BC85]  DEFAULT ((0)) FOR [CounterpartyPerformanceFeeThreshold]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_x1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] ADD  CONSTRAINT [DF_tblCounterparty_x1]  DEFAULT ((0)) FOR [Fee_ManagementEquityRebate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_x2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] ADD  CONSTRAINT [DF_tblCounterparty_x2]  DEFAULT ((0)) FOR [Fee_ManagementDebtRebate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_x3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] ADD  CONSTRAINT [DF_tblCounterparty_x3]  DEFAULT ((0)) FOR [Fee_PerformanceEquityRebate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_x4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] ADD  CONSTRAINT [DF_tblCounterparty_x4]  DEFAULT ((0)) FOR [Fee_PerformanceDebtRebate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_x5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] ADD  CONSTRAINT [DF_tblCounterparty_x5]  DEFAULT ((0)) FOR [Fee_ManagementMarketingToFCAM]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_x6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] ADD  CONSTRAINT [DF_tblCounterparty_x6]  DEFAULT ((0)) FOR [Fee_PerformanceMarketingToFCAM]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_x7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] ADD  CONSTRAINT [DF_tblCounterparty_x7]  DEFAULT ((0)) FOR [Fee_ManagementMarketingToOthers]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_x8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] ADD  CONSTRAINT [DF_tblCounterparty_x8]  DEFAULT ((0)) FOR [Fee_PerformanceMarketingToOthers]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_CounterpartyFundID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] ADD  CONSTRAINT [DF_tblCounterparty_CounterpartyFundID]  DEFAULT ((0)) FOR [CounterpartyFundID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_CounterpartyLeverageProvider]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] ADD  CONSTRAINT [DF_tblCounterparty_CounterpartyLeverageProvider]  DEFAULT ((0)) FOR [CounterpartyLeverageProvider]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_CounterpartyAddress1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] ADD  CONSTRAINT [DF_tblCounterparty_CounterpartyAddress1]  DEFAULT ('') FOR [CounterpartyAddress1]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_CounterpartyAddress2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] ADD  CONSTRAINT [DF_tblCounterparty_CounterpartyAddress2]  DEFAULT ('') FOR [CounterpartyAddress2]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_CounterpartyAddress3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] ADD  CONSTRAINT [DF_tblCounterparty_CounterpartyAddress3]  DEFAULT ('') FOR [CounterpartyAddress3]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_CounterpartyAddress4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] ADD  CONSTRAINT [DF_tblCounterparty_CounterpartyAddress4]  DEFAULT ('') FOR [CounterpartyAddress4]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_CounterpartyAddress5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] ADD  CONSTRAINT [DF_tblCounterparty_CounterpartyAddress5]  DEFAULT ('') FOR [CounterpartyAddress5]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_CounterpartyPostCode]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] ADD  CONSTRAINT [DF_tblCounterparty_CounterpartyPostCode]  DEFAULT ('') FOR [CounterpartyPostCode]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_CounterpartyContactName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] ADD  CONSTRAINT [DF_tblCounterparty_CounterpartyContactName]  DEFAULT ('') FOR [CounterpartyContactName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_CounterpartyAccountName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] ADD  CONSTRAINT [DF_tblCounterparty_CounterpartyAccountName]  DEFAULT ('') FOR [CounterpartyAccountName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_CounterpartyEMail]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] ADD  CONSTRAINT [DF_tblCounterparty_CounterpartyEMail]  DEFAULT ('') FOR [CounterpartyEMail]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_CounterpartyPhoneNumber]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] ADD  CONSTRAINT [DF_tblCounterparty_CounterpartyPhoneNumber]  DEFAULT ('') FOR [CounterpartyPhoneNumber]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_CounterpartyFaxNumber]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] ADD  CONSTRAINT [DF_tblCounterparty_CounterpartyFaxNumber]  DEFAULT ('') FOR [CounterpartyFaxNumber]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_CounterpartyUser]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] ADD  CONSTRAINT [DF_tblCounterparty_CounterpartyUser]  DEFAULT (user_name()) FOR [UserName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCounterparty_CounterpartyDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCounterparty] ADD  CONSTRAINT [DF_tblCounterparty_CounterpartyDateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCovariance_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCovariance] ADD  CONSTRAINT [DF_tblCovariance_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCovariance_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCovariance] ADD  CONSTRAINT [DF_tblCovariance_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCovarianceList_CovListName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCovarianceList] ADD  CONSTRAINT [DF_tblCovarianceList_CovListName]  DEFAULT ('<No Name>') FOR [ListName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCovarianceList_DataPeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCovarianceList] ADD  CONSTRAINT [DF_tblCovarianceList_DataPeriod]  DEFAULT ((4)) FOR [DataPeriod]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCovarianceList_IsAnnualised]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCovarianceList] ADD  CONSTRAINT [DF_tblCovarianceList_IsAnnualised]  DEFAULT ((0)) FOR [IsAnnualised]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCovarianceList_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCovarianceList] ADD  CONSTRAINT [DF_tblCovarianceList_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCovarianceList_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCovarianceList] ADD  CONSTRAINT [DF_tblCovarianceList_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCTA_Folders_UserName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCTA_Folders] ADD  CONSTRAINT [DF_tblCTA_Folders_UserName]  DEFAULT (user_name()) FOR [UserName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCTA_Folders_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCTA_Folders] ADD  CONSTRAINT [DF_tblCTA_Folders_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCTA_Simulation_InstrumentID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCTA_Simulation] ADD  CONSTRAINT [DF_tblCTA_Simulation_InstrumentID]  DEFAULT ((0)) FOR [InstrumentID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCTA_Simulation_InterestRateID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCTA_Simulation] ADD  CONSTRAINT [DF_tblCTA_Simulation_InterestRateID]  DEFAULT ((0)) FOR [InterestRateID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCTA_Simulation_SimulationDataPeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCTA_Simulation] ADD  CONSTRAINT [DF_tblCTA_Simulation_SimulationDataPeriod]  DEFAULT ((6)) FOR [SimulationDataPeriod]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCTA_Simulation_UserName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCTA_Simulation] ADD  CONSTRAINT [DF_tblCTA_Simulation_UserName]  DEFAULT (user_name()) FOR [UserName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCTA_Simulation_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCTA_Simulation] ADD  CONSTRAINT [DF_tblCTA_Simulation_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCurrency_CurrencyInstrumentID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCurrency] ADD  CONSTRAINT [DF_tblCurrency_CurrencyInstrumentID]  DEFAULT ((0)) FOR [CurrencyCashInstrumentID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCurrency_CurrencyNotionalInstrumentID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCurrency] ADD  CONSTRAINT [DF_tblCurrency_CurrencyNotionalInstrumentID]  DEFAULT ((0)) FOR [CurrencyFuturesNominalInstrumentID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCurrency_DefaultExpenseInstrument]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCurrency] ADD  CONSTRAINT [DF_tblCurrency_DefaultExpenseInstrument]  DEFAULT ((0)) FOR [DefaultExpenseInstrument]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCurrency_DefaultFeeInstrument]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCurrency] ADD  CONSTRAINT [DF_tblCurrency_DefaultFeeInstrument]  DEFAULT ((0)) FOR [DefaultFeeInstrument]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCurrency_DefaultBrokerageExpense]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCurrency] ADD  CONSTRAINT [DF_tblCurrency_DefaultBrokerageExpense]  DEFAULT ((0)) FOR [DefaultBrokerageExpense]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCurrency_CurrencyUser]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCurrency] ADD  CONSTRAINT [DF_tblCurrency_CurrencyUser]  DEFAULT (user_name()) FOR [UserName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblCurrency_CurrencyDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblCurrency] ADD  CONSTRAINT [DF_tblCurrency_CurrencyDateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataDictionary_TransactionFormLabels_TransactionType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataDictionary_TransactionFormLabels] ADD  CONSTRAINT [DF_tblDataDictionary_TransactionFormLabels_TransactionType]  DEFAULT ((0)) FOR [TransactionType]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataDictionary_TransactionFormLabels_CounterpartyText]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataDictionary_TransactionFormLabels] ADD  CONSTRAINT [DF_tblDataDictionary_TransactionFormLabels_CounterpartyText]  DEFAULT ('Counterparty') FOR [CounterpartyText]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataDictionary_TransactionFormLabels_CompoundFlag]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataDictionary_TransactionFormLabels] ADD  CONSTRAINT [DF_tblDataDictionary_TransactionFormLabels_CompoundFlag]  DEFAULT ((0)) FOR [CompoundFlag]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataDictionary_TransactionFormLabels_TransactionAmount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataDictionary_TransactionFormLabels] ADD  CONSTRAINT [DF_tblDataDictionary_TransactionFormLabels_TransactionAmount]  DEFAULT ('Amount') FOR [TransactionAmount]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataDictionary_TransactionFormLabels_TransactionPrice]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataDictionary_TransactionFormLabels] ADD  CONSTRAINT [DF_tblDataDictionary_TransactionFormLabels_TransactionPrice]  DEFAULT ('Price') FOR [TransactionPrice]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataDictionary_TransactionFormLabels_TransactionCosts]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataDictionary_TransactionFormLabels] ADD  CONSTRAINT [DF_tblDataDictionary_TransactionFormLabels_TransactionCosts]  DEFAULT ('Costs') FOR [TransactionCosts]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataDictionary_TransactionFormLabels_TransactionSettlement]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataDictionary_TransactionFormLabels] ADD  CONSTRAINT [DF_tblDataDictionary_TransactionFormLabels_TransactionSettlement]  DEFAULT ('Settlement') FOR [TransactionSettlement]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataDictionary_TransactionFormLabels_FieldText1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataDictionary_TransactionFormLabels] ADD  CONSTRAINT [DF_tblDataDictionary_TransactionFormLabels_FieldText1]  DEFAULT ('Decision Date') FOR [DecisionDateText]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataDictionary_TransactionFormLabels_FieldText2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataDictionary_TransactionFormLabels] ADD  CONSTRAINT [DF_tblDataDictionary_TransactionFormLabels_FieldText2]  DEFAULT ('Trade Date') FOR [TradeDateText]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataDictionary_TransactionFormLabels_FieldText3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataDictionary_TransactionFormLabels] ADD  CONSTRAINT [DF_tblDataDictionary_TransactionFormLabels_FieldText3]  DEFAULT ('Settlement Date') FOR [SettlementDateText]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataDictionary_TransactionFormLabels_FieldText4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataDictionary_TransactionFormLabels] ADD  CONSTRAINT [DF_tblDataDictionary_TransactionFormLabels_FieldText4]  DEFAULT ('Confirmation Date') FOR [ConfirmationDateText]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataDictionary_TransactionFormLabels_FieldText1_1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataDictionary_TransactionFormLabels] ADD  CONSTRAINT [DF_tblDataDictionary_TransactionFormLabels_FieldText1_1]  DEFAULT ('Instruction Given') FOR [FieldText1]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataDictionary_TransactionFormLabels_FieldText2_1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataDictionary_TransactionFormLabels] ADD  CONSTRAINT [DF_tblDataDictionary_TransactionFormLabels_FieldText2_1]  DEFAULT ('Bank Confirmation') FOR [FieldText2]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataDictionary_TransactionFormLabels_FieldText3_1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataDictionary_TransactionFormLabels] ADD  CONSTRAINT [DF_tblDataDictionary_TransactionFormLabels_FieldText3_1]  DEFAULT ('Initial Data entry') FOR [FieldText3]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataDictionary_TransactionFormLabels_FieldText4_1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataDictionary_TransactionFormLabels] ADD  CONSTRAINT [DF_tblDataDictionary_TransactionFormLabels_FieldText4_1]  DEFAULT ('Price Confirmation') FOR [FieldText4]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataDictionary_TransactionFormLabels_FieldText5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataDictionary_TransactionFormLabels] ADD  CONSTRAINT [DF_tblDataDictionary_TransactionFormLabels_FieldText5]  DEFAULT ('Settlement Confirmed') FOR [FieldText5]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataDictionary_TransactionFormLabels_FieldText6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataDictionary_TransactionFormLabels] ADD  CONSTRAINT [DF_tblDataDictionary_TransactionFormLabels_FieldText6]  DEFAULT ('Final Data Entry') FOR [FieldText6]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataTask_TaskID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataTask] ADD  CONSTRAINT [DF_tblDataTask_TaskID]  DEFAULT ((0)) FOR [TaskID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataTask_TaskDescription]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataTask] ADD  CONSTRAINT [DF_tblDataTask_TaskDescription]  DEFAULT ('') FOR [TaskDescription]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataTask_TaskType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataTask] ADD  CONSTRAINT [DF_tblDataTask_TaskType]  DEFAULT ((0)) FOR [TaskType]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataTask_InstrumentID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataTask] ADD  CONSTRAINT [DF_tblDataTask_InstrumentID]  DEFAULT ((0)) FOR [InstrumentID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataTask_SecurityTicker]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataTask] ADD  CONSTRAINT [DF_tblDataTask_SecurityTicker]  DEFAULT ('') FOR [SecurityTicker]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataTask_TaskDefinition]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataTask] ADD  CONSTRAINT [DF_tblDataTask_TaskDefinition]  DEFAULT ('') FOR [TaskDefinition]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataTask_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataTask] ADD  CONSTRAINT [DF_tblDataTask_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataTask_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataTask] ADD  CONSTRAINT [DF_tblDataTask_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataTaskType_TaskType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataTaskType] ADD  CONSTRAINT [DF_tblDataTaskType_TaskType]  DEFAULT ((0)) FOR [TaskType]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataTaskType_TaskDescription]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataTaskType] ADD  CONSTRAINT [DF_tblDataTaskType_TaskDescription]  DEFAULT ('') FOR [TaskDescription]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDataTaskType_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDataTaskType] ADD  CONSTRAINT [DF_tblDataTaskType_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblDealingPeriod_DealingPeriodDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblDealingPeriod] ADD  CONSTRAINT [DF_tblDealingPeriod_DealingPeriodDateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblEstimate_EstimateFund]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblEstimate] ADD  CONSTRAINT [DF_tblEstimate_EstimateFund]  DEFAULT ((0)) FOR [EstimateFund]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblEstimate_EstimateItem]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblEstimate] ADD  CONSTRAINT [DF_tblEstimate_EstimateItem]  DEFAULT ('') FOR [EstimateItem]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblEstimate_EstimateDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblEstimate] ADD  CONSTRAINT [DF_tblEstimate_EstimateDate]  DEFAULT (getdate()) FOR [EstimateDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblEstimate_EstimateAmount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblEstimate] ADD  CONSTRAINT [DF_tblEstimate_EstimateAmount]  DEFAULT ((0)) FOR [EstimateAmount]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblEstimate_User]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblEstimate] ADD  CONSTRAINT [DF_tblEstimate_User]  DEFAULT (user_name()) FOR [EstimateUser]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblEstimate_EstimateDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblEstimate] ADD  CONSTRAINT [DF_tblEstimate_EstimateDateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblExposure_expIsDetailRow]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblExposure] ADD  CONSTRAINT [DF_tblExposure_expIsDetailRow]  DEFAULT ((0)) FOR [expIsDetailRow]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblExposure_limID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblExposure] ADD  CONSTRAINT [DF_tblExposure_limID]  DEFAULT ((0)) FOR [expLimitID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblExposure_expLimitCategory]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblExposure] ADD  CONSTRAINT [DF_tblExposure_expLimitCategory]  DEFAULT ((0)) FOR [expLimitCharacteristic]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblExposure_expLimitInstrument]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblExposure] ADD  CONSTRAINT [DF_tblExposure_expLimitInstrument]  DEFAULT ((0)) FOR [expLimitInstrument]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblExposure_expLimitSector]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblExposure] ADD  CONSTRAINT [DF_tblExposure_expLimitSector]  DEFAULT ((0)) FOR [expLimitSector]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblExposure_expLimitSectorGroup]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblExposure] ADD  CONSTRAINT [DF_tblExposure_expLimitSectorGroup]  DEFAULT ('') FOR [expLimitSectorName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblExposure_expLimitDealingPeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblExposure] ADD  CONSTRAINT [DF_tblExposure_expLimitDealingPeriod]  DEFAULT ((0)) FOR [expLimitDealingPeriod]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblExposure_expKnowledgeDateWholeday]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblExposure] ADD  CONSTRAINT [DF_tblExposure_expKnowledgeDateWholeday]  DEFAULT ((0)) FOR [expKnowledgeDateWholeday]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblExposure_expSystemKnowledgeDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblExposure] ADD  CONSTRAINT [DF_tblExposure_expSystemKnowledgeDate]  DEFAULT ('31 Dec 1899') FOR [expSystemKnowledgeDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblExposure_expSystemKnowledgeDateWholeday]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblExposure] ADD  CONSTRAINT [DF_tblExposure_expSystemKnowledgeDateWholeday]  DEFAULT ((0)) FOR [expSystemKnowledgeDateWholeday]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblExposure_expFlags]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblExposure] ADD  CONSTRAINT [DF_tblExposure_expFlags]  DEFAULT ((0)) FOR [expFlags]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceDataValues_BooleanData]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceDataValues] ADD  CONSTRAINT [DF_tblFlorenceDataValues_BooleanData]  DEFAULT ((0)) FOR [BooleanData]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceDataValues_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceDataValues] ADD  CONSTRAINT [DF_tblFlorenceDataValues_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceEntity_EntityIsInactive]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceEntity] ADD  CONSTRAINT [DF_tblFlorenceEntity_EntityIsInactive]  DEFAULT ((0)) FOR [EntityIsInactive]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceEntity_InstrumentID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceEntity] ADD  CONSTRAINT [DF_tblFlorenceEntity_InstrumentID]  DEFAULT ((0)) FOR [InstrumentID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceEntity_AdminContact]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceEntity] ADD  CONSTRAINT [DF_tblFlorenceEntity_AdminContact]  DEFAULT ('') FOR [AdminContact]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceEntity_AdminEmailSalutation]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceEntity] ADD  CONSTRAINT [DF_tblFlorenceEntity_AdminEmailSalutation]  DEFAULT ('') FOR [AdminEmailSalutation]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceEntity_AdminPhone]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceEntity] ADD  CONSTRAINT [DF_tblFlorenceEntity_AdminPhone]  DEFAULT ('') FOR [AdminPhone]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceEntity_AdminEMail]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceEntity] ADD  CONSTRAINT [DF_tblFlorenceEntity_AdminEMail]  DEFAULT ('') FOR [AdminEMail]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceEntity_AdminFax]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceEntity] ADD  CONSTRAINT [DF_tblFlorenceEntity_AdminFax]  DEFAULT ('') FOR [AdminFax]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceEntity_DDStatus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceEntity] ADD  CONSTRAINT [DF_tblFlorenceEntity_DDStatus]  DEFAULT ((0)) FOR [DDStatus]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceEntity_DDStatusComment]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceEntity] ADD  CONSTRAINT [DF_tblFlorenceEntity_DDStatusComment]  DEFAULT ('') FOR [DDStatusComment]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceEntity_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceEntity] ADD  CONSTRAINT [DF_tblFlorenceEntity_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceGroup_LegacyGroup]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceGroup] ADD  CONSTRAINT [DF_tblFlorenceGroup_LegacyGroup]  DEFAULT ((0)) FOR [LegacyGroup]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceGroup_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceGroup] ADD  CONSTRAINT [DF_tblFlorenceGroup_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceItems_UpdatePeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceItems] ADD  CONSTRAINT [DF_tblFlorenceItems_UpdatePeriod]  DEFAULT ((0)) FOR [UpdatePeriod]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceItems_DataUpdatePeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceItems] ADD  CONSTRAINT [DF_tblFlorenceItems_DataUpdatePeriod]  DEFAULT ((0)) FOR [DataUpdatePeriod]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceItems_ItemGroup]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceItems] ADD  CONSTRAINT [DF_tblFlorenceItems_ItemGroup]  DEFAULT ((1)) FOR [ItemGroup]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceItems_ItemDiscontinued]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceItems] ADD  CONSTRAINT [DF_tblFlorenceItems_ItemDiscontinued]  DEFAULT ((0)) FOR [ItemDiscontinued]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceItems_KeyDetail]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceItems] ADD  CONSTRAINT [DF_tblFlorenceItems_KeyDetail]  DEFAULT ((0)) FOR [KeyDetail]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceItems_IsDataItem]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceItems] ADD  CONSTRAINT [DF_tblFlorenceItems_IsDataItem]  DEFAULT ((0)) FOR [IsDataItem]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceItems_DataItemType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceItems] ADD  CONSTRAINT [DF_tblFlorenceItems_DataItemType]  DEFAULT ((0)) FOR [DataItemType]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceItems_UpdateStatusOnDataUpdate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceItems] ADD  CONSTRAINT [DF_tblFlorenceItems_UpdateStatusOnDataUpdate]  DEFAULT ((0)) FOR [UpdateStatusOnDataUpdate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceItems_IsLimitItem]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceItems] ADD  CONSTRAINT [DF_tblFlorenceItems_IsLimitItem]  DEFAULT ((0)) FOR [IsLimitItem]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceItems_LimitType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceItems] ADD  CONSTRAINT [DF_tblFlorenceItems_LimitType]  DEFAULT ((0)) FOR [LimitType]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceItems_LimitIsPercent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceItems] ADD  CONSTRAINT [DF_tblFlorenceItems_LimitIsPercent]  DEFAULT ((0)) FOR [LimitIsPercent]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceItems_FromLimit]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceItems] ADD  CONSTRAINT [DF_tblFlorenceItems_FromLimit]  DEFAULT ((0)) FOR [FromLimit]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceItems_ToLimit]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceItems] ADD  CONSTRAINT [DF_tblFlorenceItems_ToLimit]  DEFAULT ((0)) FOR [ToLimit]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceItems_LimitThreshold]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceItems] ADD  CONSTRAINT [DF_tblFlorenceItems_LimitThreshold]  DEFAULT ((0)) FOR [LimitThreshold]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceItems_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceItems] ADD  CONSTRAINT [DF_tblFlorenceItems_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceStatus_ItemStatus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceStatus] ADD  CONSTRAINT [DF_tblFlorenceStatus_ItemStatus]  DEFAULT ((0)) FOR [ItemStatusID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceStatus_Comment]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceStatus] ADD  CONSTRAINT [DF_tblFlorenceStatus_Comment]  DEFAULT ('') FOR [Comment]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceStatus_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceStatus] ADD  CONSTRAINT [DF_tblFlorenceStatus_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceStatus_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceStatus] ADD  CONSTRAINT [DF_tblFlorenceStatus_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceStatusIDs_ItemIsComplete]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceStatusIDs] ADD  CONSTRAINT [DF_tblFlorenceStatusIDs_ItemIsComplete]  DEFAULT ((0)) FOR [ItemIsComplete]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFlorenceStatusIDs_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFlorenceStatusIDs] ADD  CONSTRAINT [DF_tblFlorenceStatusIDs_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundLegalEntity]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundLegalEntity]  DEFAULT ('') FOR [FundLegalEntity]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundPricingPeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundPricingPeriod]  DEFAULT ((6)) FOR [FundPricingPeriod]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundUnit]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundUnit]  DEFAULT ((0)) FOR [FundUnit]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__tblFund__FundPar__01142BA1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF__tblFund__FundPar__01142BA1]  DEFAULT ((0)) FOR [FundParentFund]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundDefaultTradesToFundFX]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundDefaultTradesToFundFX]  DEFAULT ((0)) FOR [FundDefaultTradeFXToFundCurrency]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundUser]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundUser]  DEFAULT (user_name()) FOR [FundUser]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundManagementFeeInstrument]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundManagementFeeInstrument]  DEFAULT ((0)) FOR [FundManagementFeeInstrument]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundPerformanceFeeInstrument]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundPerformanceFeeInstrument]  DEFAULT ((0)) FOR [FundPerformanceFeeInstrument]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundManagementFees]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundManagementFees]  DEFAULT ((0.01)) FOR [FundManagementFees]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundPerformanceVsBenchmark]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundPerformanceVsBenchmark]  DEFAULT ((0)) FOR [FundPerformanceVsBenchmark]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundPerformanceFees]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundPerformanceFees]  DEFAULT ((0.1)) FOR [FundPerformanceFees]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundPerformanceHurdle]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundPerformanceHurdle]  DEFAULT ((0)) FOR [FundPerformanceHurdle]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundUsesEqualisation]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundUsesEqualisation]  DEFAULT ((0)) FOR [FundUsesEqualisation]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundFeesAccruePeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundFeesAccruePeriod]  DEFAULT ((6)) FOR [FundManagementFeesAccruePeriod]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundFeesPaymentPeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundFeesPaymentPeriod]  DEFAULT ((4)) FOR [FundManagementFeesPaymentPeriod]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundPerformanceFeesAccruePeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundPerformanceFeesAccruePeriod]  DEFAULT ((6)) FOR [FundPerformanceFeesAccruePeriod]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundPerformanceFeesPaymentPeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundPerformanceFeesPaymentPeriod]  DEFAULT ((1)) FOR [FundPerformanceFeesPaymentPeriod]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundSubscriptionNAVDatePeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundSubscriptionNAVDatePeriod]  DEFAULT ((0)) FOR [FundSubscriptionNAVDatePeriod]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundSubscriptionSettlementDatePeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundSubscriptionSettlementDatePeriod]  DEFAULT ((0)) FOR [FundSubscriptionSettlementDatePeriod]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundRedemptionNAVDatePeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundRedemptionNAVDatePeriod]  DEFAULT ((0)) FOR [FundRedemptionNAVDatePeriod]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundRedemptionSettlementDatePeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundRedemptionSettlementDatePeriod]  DEFAULT ((0)) FOR [FundRedemptionSettlementDatePeriod]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundReportGroup]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundReportGroup]  DEFAULT ('') FOR [FundReportGroup]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundInitialSubscriptionFee]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundInitialSubscriptionFee]  DEFAULT ('') FOR [FundInitialSubscriptionFee]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundManagementFeeText]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundManagementFeeText]  DEFAULT ('') FOR [FundManagementFeeText]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundPerformanceFeeText]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundPerformanceFeeText]  DEFAULT ('') FOR [FundPerformanceFeeText]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundManagementCompany]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundManagementCompany]  DEFAULT ('') FOR [FundManagementCompany]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundManagers]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundManagers]  DEFAULT ('') FOR [FundManagers]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundContactAddress]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundContactAddress]  DEFAULT ('') FOR [FundContactAddress]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundContactPhone]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundContactPhone]  DEFAULT ('') FOR [FundContactPhone]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundContactEmail]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundContactEmail]  DEFAULT ('') FOR [FundContactEmail]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundMinimumInvestment]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundMinimumInvestment]  DEFAULT ('') FOR [FundMinimumInvestment]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundSubscriptionPeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundSubscriptionPeriod]  DEFAULT ('') FOR [FundSubscriptionPeriod]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundSubscriptionNotice]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundSubscriptionNotice]  DEFAULT ('') FOR [FundSubscriptionNotice]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundRedemptionPeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundRedemptionPeriod]  DEFAULT ('') FOR [FundRedemptionPeriod]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundRedemptionNotice]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundRedemptionNotice]  DEFAULT ('') FOR [FundRedemptionNotice]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundJurisdiction]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundJurisdiction]  DEFAULT ('') FOR [FundJurisdiction]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundAdministrator]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundAdministrator]  DEFAULT ('') FOR [FundAdministrator]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundCustodian]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundCustodian]  DEFAULT ('') FOR [FundCustodian]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundListings]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundListings]  DEFAULT ('') FOR [FundListings]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundLinkedListedInstrument]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundLinkedListedInstrument]  DEFAULT ((0)) FOR [FundLinkedListedInstrument]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundReviewGroup]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundReviewGroup]  DEFAULT ((0)) FOR [FundReviewGroup]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundISIN]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundISIN]  DEFAULT ('') FOR [FundISIN]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundSEDOL]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundSEDOL]  DEFAULT ('') FOR [FundSEDOL]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundBloomberg]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundBloomberg]  DEFAULT ('') FOR [FundBloomberg]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundReutersCode]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundReutersCode]  DEFAULT ('') FOR [FundReutersCode]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundAdministratorCode]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundAdministratorCode]  DEFAULT ('') FOR [FundAdministratorCode]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundClosed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundClosed]  DEFAULT ((0)) FOR [FundClosed]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_UserName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_UserName]  DEFAULT (user_name()) FOR [UserName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFund_FundDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFund] ADD  CONSTRAINT [DF_tblFund_FundDateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundContactDetails_AdminPhone]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundContactDetails] ADD  CONSTRAINT [DF_tblFundContactDetails_AdminPhone]  DEFAULT ('') FOR [AdminPhone]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundContactDetails_AdminFax]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundContactDetails] ADD  CONSTRAINT [DF_tblFundContactDetails_AdminFax]  DEFAULT ('') FOR [AdminFax]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundContactDetails_AdminMainEmail]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundContactDetails] ADD  CONSTRAINT [DF_tblFundContactDetails_AdminMainEmail]  DEFAULT ('') FOR [AdminMainEmail]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundContactDetails_AdminSecondaryEmail]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundContactDetails] ADD  CONSTRAINT [DF_tblFundContactDetails_AdminSecondaryEmail]  DEFAULT ('') FOR [AdminSecondaryEmail]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundContactDetails_InvestorRelationsContact]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundContactDetails] ADD  CONSTRAINT [DF_tblFundContactDetails_InvestorRelationsContact]  DEFAULT ('') FOR [InvestorRelationsContact]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundContactDetails_InvestorRelationsPhone]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundContactDetails] ADD  CONSTRAINT [DF_tblFundContactDetails_InvestorRelationsPhone]  DEFAULT ('') FOR [InvestorRelationsPhone]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundContactDetails_InvestorRelationsEmail]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundContactDetails] ADD  CONSTRAINT [DF_tblFundContactDetails_InvestorRelationsEmail]  DEFAULT ('') FOR [InvestorRelationsEmail]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundContactDetails_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundContactDetails] ADD  CONSTRAINT [DF_tblFundContactDetails_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundMilestones_MileStoneUser]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundMilestones] ADD  CONSTRAINT [DF_tblFundMilestones_MileStoneUser]  DEFAULT (user_name()) FOR [UserName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundMilestones_MilestoneDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundMilestones] ADD  CONSTRAINT [DF_tblFundMilestones_MilestoneDateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundPerformanceFeePoints_FeeID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundPerformanceFeePoints] ADD  CONSTRAINT [DF_tblFundPerformanceFeePoints_FeeID]  DEFAULT ((0)) FOR [FeeID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundPerformanceFeePoints_FeeFund]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundPerformanceFeePoints] ADD  CONSTRAINT [DF_tblFundPerformanceFeePoints_FeeFund]  DEFAULT ((0)) FOR [FeeFund]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundPerformanceFeePoints_FeeFundPrice]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundPerformanceFeePoints] ADD  CONSTRAINT [DF_tblFundPerformanceFeePoints_FeeFundPrice]  DEFAULT ((0)) FOR [FeeFundPrice]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundPerformanceFeePoints_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundPerformanceFeePoints] ADD  CONSTRAINT [DF_tblFundPerformanceFeePoints_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundPerformanceFeePoints_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundPerformanceFeePoints] ADD  CONSTRAINT [DF_tblFundPerformanceFeePoints_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundType_FundTypeGroup]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundType] ADD  CONSTRAINT [DF_tblFundType_FundTypeGroup]  DEFAULT ('') FOR [FundTypeGroup]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundType_FundTypeSortOrder]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundType] ADD  CONSTRAINT [DF_tblFundType_FundTypeSortOrder]  DEFAULT ((0)) FOR [FundTypeSortOrder]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundType_FundTypeAttributionGrouping]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundType] ADD  CONSTRAINT [DF_tblFundType_FundTypeAttributionGrouping]  DEFAULT ('') FOR [FundTypeAttributionGrouping]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundType_FundTypeIsAdministrativeOnly]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundType] ADD  CONSTRAINT [DF_tblFundType_FundTypeIsAdministrativeOnly]  DEFAULT ((0)) FOR [FundTypeIsAdministrativeOnly]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundType_FundTypeReporterGrouping]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundType] ADD  CONSTRAINT [DF_tblFundType_FundTypeReporterGrouping]  DEFAULT ('') FOR [FundTypeReporterGrouping]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundType_FundTypeShowInReporter]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundType] ADD  CONSTRAINT [DF_tblFundType_FundTypeShowInReporter]  DEFAULT ((1)) FOR [FundTypeShowInReporter]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundType_FundTypeUser]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundType] ADD  CONSTRAINT [DF_tblFundType_FundTypeUser]  DEFAULT (user_name()) FOR [UserName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFundType_FundTypeDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFundType] ADD  CONSTRAINT [DF_tblFundType_FundTypeDateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFX_FXUser]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFX] ADD  CONSTRAINT [DF_tblFX_FXUser]  DEFAULT (user_name()) FOR [UserName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblFX_FXDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblFX] ADD  CONSTRAINT [DF_tblFX_FXDateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGenoaConstraintItems_TestFieldLessThan]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGenoaConstraintItems] ADD  CONSTRAINT [DF_tblGenoaConstraintItems_TestFieldLessThan]  DEFAULT ((0)) FOR [TestFieldLessThan]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGenoaConstraintItems_TestFieldLessEqualTo]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGenoaConstraintItems] ADD  CONSTRAINT [DF_tblGenoaConstraintItems_TestFieldLessEqualTo]  DEFAULT ((0)) FOR [TestFieldEqualTo]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGenoaConstraintItems_TestFieldGreaterThan]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGenoaConstraintItems] ADD  CONSTRAINT [DF_tblGenoaConstraintItems_TestFieldGreaterThan]  DEFAULT ((0)) FOR [TestFieldGreaterThan]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGenoaConstraintItems_TestValueString]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGenoaConstraintItems] ADD  CONSTRAINT [DF_tblGenoaConstraintItems_TestValueString]  DEFAULT ('') FOR [TestValueString]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGenoaConstraintItems_TestValueNumeric]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGenoaConstraintItems] ADD  CONSTRAINT [DF_tblGenoaConstraintItems_TestValueNumeric]  DEFAULT ((0)) FOR [TestValueNumeric]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGenoaConstraintItems_TestValueDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGenoaConstraintItems] ADD  CONSTRAINT [DF_tblGenoaConstraintItems_TestValueDate]  DEFAULT ('1 Jan 1900') FOR [TestValueDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGenoaConstraintItems_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGenoaConstraintItems] ADD  CONSTRAINT [DF_tblGenoaConstraintItems_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGenoaConstraintItems_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGenoaConstraintItems] ADD  CONSTRAINT [DF_tblGenoaConstraintItems_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGenoaConstraintList_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGenoaConstraintList] ADD  CONSTRAINT [DF_tblGenoaConstraintList_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGenoaConstraintList_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGenoaConstraintList] ADD  CONSTRAINT [DF_tblGenoaConstraintList_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGenoaSavedSearchItems_FieldIsStatic]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGenoaSavedSearchItems] ADD  CONSTRAINT [DF_tblGenoaSavedSearchItems_FieldIsStatic]  DEFAULT ((0)) FOR [FieldIsStatic]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGenoaSavedSearchItems_FieldIsCustom]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGenoaSavedSearchItems] ADD  CONSTRAINT [DF_tblGenoaSavedSearchItems_FieldIsCustom]  DEFAULT ((0)) FOR [FieldIsCustom]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGenoaSavedSearchItems_IsAND]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGenoaSavedSearchItems] ADD  CONSTRAINT [DF_tblGenoaSavedSearchItems_IsAND]  DEFAULT ((0)) FOR [IsAND]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGenoaSavedSearchItems_IsOR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGenoaSavedSearchItems] ADD  CONSTRAINT [DF_tblGenoaSavedSearchItems_IsOR]  DEFAULT ((0)) FOR [IsOR]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGenoaSavedSearchItems_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGenoaSavedSearchItems] ADD  CONSTRAINT [DF_tblGenoaSavedSearchItems_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGenoaSavedSearchItems_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGenoaSavedSearchItems] ADD  CONSTRAINT [DF_tblGenoaSavedSearchItems_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGenoaSavedSearchList_VisibleToAllUsers]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGenoaSavedSearchList] ADD  CONSTRAINT [DF_tblGenoaSavedSearchList_VisibleToAllUsers]  DEFAULT ((1)) FOR [VisibleToAllUsers]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGenoaSavedSearchList_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGenoaSavedSearchList] ADD  CONSTRAINT [DF_tblGenoaSavedSearchList_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGenoaSavedSearchList_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGenoaSavedSearchList] ADD  CONSTRAINT [DF_tblGenoaSavedSearchList_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingRules_ScenarioKey]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingRules] ADD  CONSTRAINT [DF_tblGLPostingRules_ScenarioKey]  DEFAULT ((0)) FOR [ScenarioKey]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingRules_DEBIT_TransferOfExistingValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingRules] ADD  CONSTRAINT [DF_tblGLPostingRules_DEBIT_TransferOfExistingValue]  DEFAULT ((0)) FOR [DEBIT_TransferOfExistingValue]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingRules_CREDIT_TransferOfExistingValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingRules] ADD  CONSTRAINT [DF_tblGLPostingRules_CREDIT_TransferOfExistingValue]  DEFAULT ((0)) FOR [CREDIT_TransferOfExistingValue]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingRules_MatchValueToPostingRuleID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingRules] ADD  CONSTRAINT [DF_tblGLPostingRules_MatchValueToPostingRuleID]  DEFAULT ((0)) FOR [MatchValueToPostingRuleID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingRules_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingRules] ADD  CONSTRAINT [DF_tblGLPostingRules_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingRules_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingRules] ADD  CONSTRAINT [DF_tblGLPostingRules_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostings_PostingType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostings] ADD  CONSTRAINT [DF_tblGLPostings_PostingType]  DEFAULT ((0)) FOR [PostingType]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostings_ClosingTransactionParentID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostings] ADD  CONSTRAINT [DF_tblGLPostings_ClosingTransactionParentID]  DEFAULT ((0)) FOR [ClosingTransactionParentID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostings_PeriodYear]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostings] ADD  CONSTRAINT [DF_tblGLPostings_PeriodYear]  DEFAULT ((0)) FOR [PeriodYear]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostings_PeriodNumber]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostings] ADD  CONSTRAINT [DF_tblGLPostings_PeriodNumber]  DEFAULT ((0)) FOR [PeriodNumber]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostings_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostings] ADD  CONSTRAINT [DF_tblGLPostings_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostings_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostings] ADD  CONSTRAINT [DF_tblGLPostings_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingScenarios_Category1_Enum]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingScenarios] ADD  CONSTRAINT [DF_tblGLPostingScenarios_Category1_Enum]  DEFAULT ('GL_PostingPhase') FOR [Category1_Enum]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingScenarios_Category1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingScenarios] ADD  CONSTRAINT [DF_tblGLPostingScenarios_Category1]  DEFAULT ((0)) FOR [Category1]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingScenarios_Category2_Enum]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingScenarios] ADD  CONSTRAINT [DF_tblGLPostingScenarios_Category2_Enum]  DEFAULT ('') FOR [Category2_Enum]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingScenarios_Category2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingScenarios] ADD  CONSTRAINT [DF_tblGLPostingScenarios_Category2]  DEFAULT ((0)) FOR [Category2]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingScenarios_Category3_Enum]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingScenarios] ADD  CONSTRAINT [DF_tblGLPostingScenarios_Category3_Enum]  DEFAULT ('') FOR [Category3_Enum]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingScenarios_Category3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingScenarios] ADD  CONSTRAINT [DF_tblGLPostingScenarios_Category3]  DEFAULT ((0)) FOR [Category3]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingScenarios_Category4_Enum]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingScenarios] ADD  CONSTRAINT [DF_tblGLPostingScenarios_Category4_Enum]  DEFAULT ('') FOR [Category4_Enum]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingScenarios_Category4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingScenarios] ADD  CONSTRAINT [DF_tblGLPostingScenarios_Category4]  DEFAULT ((0)) FOR [Category4]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingScenarios_Category5_Enum]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingScenarios] ADD  CONSTRAINT [DF_tblGLPostingScenarios_Category5_Enum]  DEFAULT ('') FOR [Category5_Enum]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingScenarios_Category5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingScenarios] ADD  CONSTRAINT [DF_tblGLPostingScenarios_Category5]  DEFAULT ((0)) FOR [Category5]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingScenarios_Category6_Enum]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingScenarios] ADD  CONSTRAINT [DF_tblGLPostingScenarios_Category6_Enum]  DEFAULT ('') FOR [Category6_Enum]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingScenarios_Category6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingScenarios] ADD  CONSTRAINT [DF_tblGLPostingScenarios_Category6]  DEFAULT ((0)) FOR [Category6]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingScenarios_PostingValue_Enum]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingScenarios] ADD  CONSTRAINT [DF_tblGLPostingScenarios_PostingValue_Enum]  DEFAULT ('GL_PostingValues') FOR [PostingValue_Enum]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingScenarios_PostingValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingScenarios] ADD  CONSTRAINT [DF_tblGLPostingScenarios_PostingValue]  DEFAULT ((0)) FOR [PostingValue]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingScenarios_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingScenarios] ADD  CONSTRAINT [DF_tblGLPostingScenarios_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGLPostingScenarios_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGLPostingScenarios] ADD  CONSTRAINT [DF_tblGLPostingScenarios_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGreeks_InstrumentID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGreeks] ADD  CONSTRAINT [DF_tblGreeks_InstrumentID]  DEFAULT ((0)) FOR [InstrumentID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGreeks_Delta]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGreeks] ADD  CONSTRAINT [DF_tblGreeks_Delta]  DEFAULT ((0.0)) FOR [Delta]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGreeks_Gamma]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGreeks] ADD  CONSTRAINT [DF_tblGreeks_Gamma]  DEFAULT ((0.0)) FOR [Gamma]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGreeks_Rho]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGreeks] ADD  CONSTRAINT [DF_tblGreeks_Rho]  DEFAULT ((0.0)) FOR [Rho]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGreeks_Vega]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGreeks] ADD  CONSTRAINT [DF_tblGreeks_Vega]  DEFAULT ((0.0)) FOR [Vega]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGreeks_Theta]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGreeks] ADD  CONSTRAINT [DF_tblGreeks_Theta]  DEFAULT ((0.0)) FOR [Theta]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGreeks_Volatility]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGreeks] ADD  CONSTRAINT [DF_tblGreeks_Volatility]  DEFAULT ((0.0)) FOR [Volatility]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGreeks_GreekUser]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGreeks] ADD  CONSTRAINT [DF_tblGreeks_GreekUser]  DEFAULT (user_name()) FOR [UserEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGreeks_GreekDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGreeks] ADD  CONSTRAINT [DF_tblGreeks_GreekDateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItemData_GroupPertracCode]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItemData] ADD  CONSTRAINT [DF_tblGroupItemData_GroupPertracCode]  DEFAULT ((0)) FOR [GroupPertracCode]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItemData_GroupLiquidity]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItemData] ADD  CONSTRAINT [DF_tblGroupItemData_GroupLiquidity]  DEFAULT ((0)) FOR [GroupLiquidity]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItemData_GroupHolding]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItemData] ADD  CONSTRAINT [DF_tblGroupItemData_GroupHolding]  DEFAULT ((0)) FOR [GroupHolding]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItemData_GroupNewHolding]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItemData] ADD  CONSTRAINT [DF_tblGroupItemData_GroupNewHolding]  DEFAULT ((0)) FOR [GroupNewHolding]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItemData_GroupPercent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItemData] ADD  CONSTRAINT [DF_tblGroupItemData_GroupPercent]  DEFAULT ((0)) FOR [GroupPercent]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItemData_GroupExpectedReturn]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItemData] ADD  CONSTRAINT [DF_tblGroupItemData_GroupExpectedReturn]  DEFAULT ((0)) FOR [GroupExpectedReturn]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItemData_GroupUpperBound]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItemData] ADD  CONSTRAINT [DF_tblGroupItemData_GroupUpperBound]  DEFAULT ((0)) FOR [GroupUpperBound]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItemData_GroupFloor]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItemData] ADD  CONSTRAINT [DF_tblGroupItemData_GroupFloor]  DEFAULT ((0)) FOR [GroupFloor]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItemData_GroupCap]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItemData] ADD  CONSTRAINT [DF_tblGroupItemData_GroupCap]  DEFAULT ((0)) FOR [GroupCap]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItemData_GroupTradeSize]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItemData] ADD  CONSTRAINT [DF_tblGroupItemData_GroupTradeSize]  DEFAULT ((0)) FOR [GroupTradeSize]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItemData_GroupBeta]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItemData] ADD  CONSTRAINT [DF_tblGroupItemData_GroupBeta]  DEFAULT ((0)) FOR [GroupBeta]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItemData_GroupAlpha]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItemData] ADD  CONSTRAINT [DF_tblGroupItemData_GroupAlpha]  DEFAULT ((0)) FOR [GroupAlpha]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItemData_GroupIndexE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItemData] ADD  CONSTRAINT [DF_tblGroupItemData_GroupIndexE]  DEFAULT ((0)) FOR [GroupIndexE]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItemData_GroupBetaE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItemData] ADD  CONSTRAINT [DF_tblGroupItemData_GroupBetaE]  DEFAULT ((0)) FOR [GroupBetaE]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItemData_GroupAlphaE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItemData] ADD  CONSTRAINT [DF_tblGroupItemData_GroupAlphaE]  DEFAULT ((0)) FOR [GroupAlphaE]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItemData_GroupStdErr]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItemData] ADD  CONSTRAINT [DF_tblGroupItemData_GroupStdErr]  DEFAULT ((0)) FOR [GroupStdErr]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItemData_GroupScalingFactor]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItemData] ADD  CONSTRAINT [DF_tblGroupItemData_GroupScalingFactor]  DEFAULT ((1)) FOR [GroupScalingFactor]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItemData_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItemData] ADD  CONSTRAINT [DF_tblGroupItemData_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItemData_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItemData] ADD  CONSTRAINT [DF_tblGroupItemData_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItems_GroupItemID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItems] ADD  CONSTRAINT [DF_tblGroupItems_GroupItemID]  DEFAULT (ident_current('dbo.tblGroupItems')) FOR [GroupItemID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItems_GroupSector]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItems] ADD  CONSTRAINT [DF_tblGroupItems_GroupSector]  DEFAULT ('') FOR [GroupSector]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItems_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItems] ADD  CONSTRAINT [DF_tblGroupItems_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupItems_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupItems] ADD  CONSTRAINT [DF_tblGroupItems_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupList_GroupListName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupList] ADD  CONSTRAINT [DF_tblGroupList_GroupListName]  DEFAULT ('') FOR [GroupListName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupList_GroupGroup]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupList] ADD  CONSTRAINT [DF_tblGroupList_GroupGroup]  DEFAULT ('') FOR [GroupGroup]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupList_GroupDateFrom]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupList] ADD  CONSTRAINT [DF_tblGroupList_GroupDateFrom]  DEFAULT ('1 Jan 1900') FOR [GroupDateFrom]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupList_GroupDateTo]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupList] ADD  CONSTRAINT [DF_tblGroupList_GroupDateTo]  DEFAULT ('1 Jan 3000') FOR [GroupDateTo]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupList_DefaultConstraintGroup]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupList] ADD  CONSTRAINT [DF_tblGroupList_DefaultConstraintGroup]  DEFAULT ((0)) FOR [DefaultConstraintGroup]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupList_DefaultCovarianceMatrix]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupList] ADD  CONSTRAINT [DF_tblGroupList_DefaultCovarianceMatrix]  DEFAULT ((0)) FOR [DefaultCovarianceMatrix]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupList_GroupPricingPeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupList] ADD  CONSTRAINT [DF_tblGroupList_GroupPricingPeriod]  DEFAULT ((6)) FOR [GroupPricingPeriod]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupList_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupList] ADD  CONSTRAINT [DF_tblGroupList_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblGroupList_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblGroupList] ADD  CONSTRAINT [DF_tblGroupList_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_InstrumentContractSize]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] ADD  CONSTRAINT [DF_tblHistoricFundValuations_InstrumentContractSize]  DEFAULT ((1)) FOR [InstrumentContractSize]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_InstrumentMultiplier]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] ADD  CONSTRAINT [DF_tblHistoricFundValuations_InstrumentMultiplier]  DEFAULT ((1)) FOR [InstrumentMultiplier]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_SignedUnits]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] ADD  CONSTRAINT [DF_tblHistoricFundValuations_SignedUnits]  DEFAULT ((0)) FOR [SignedUnits]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_SignedSettlement]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] ADD  CONSTRAINT [DF_tblHistoricFundValuations_SignedSettlement]  DEFAULT ((0)) FOR [SignedSettlement]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_PriceDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] ADD  CONSTRAINT [DF_tblHistoricFundValuations_PriceDate]  DEFAULT ('1900-01-01') FOR [PriceDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_PriceNAV]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] ADD  CONSTRAINT [DF_tblHistoricFundValuations_PriceNAV]  DEFAULT ((0)) FOR [PriceNAV]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_PriceLevel]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] ADD  CONSTRAINT [DF_tblHistoricFundValuations_PriceLevel]  DEFAULT ((0)) FOR [PriceLevel]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_PriceMultiplier]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] ADD  CONSTRAINT [DF_tblHistoricFundValuations_PriceMultiplier]  DEFAULT ((1)) FOR [PriceMultiplier]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_FundFXRate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] ADD  CONSTRAINT [DF_tblHistoricFundValuations_FundFXRate]  DEFAULT ((1)) FOR [FundFXRate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_InstrumentFXRate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] ADD  CONSTRAINT [DF_tblHistoricFundValuations_InstrumentFXRate]  DEFAULT ((1)) FOR [InstrumentFXRate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_CompoundFXRate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] ADD  CONSTRAINT [DF_tblHistoricFundValuations_CompoundFXRate]  DEFAULT ((1)) FOR [CompoundFXRate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_Value]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] ADD  CONSTRAINT [DF_tblHistoricFundValuations_Value]  DEFAULT ((0)) FOR [Value]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_USDValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] ADD  CONSTRAINT [DF_tblHistoricFundValuations_USDValue]  DEFAULT ((0)) FOR [USDValue]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_LocalValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] ADD  CONSTRAINT [DF_tblHistoricFundValuations_LocalValue]  DEFAULT ((0)) FOR [LocalValue]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_ConsiderationValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] ADD  CONSTRAINT [DF_tblHistoricFundValuations_ConsiderationValue]  DEFAULT ((0)) FOR [ConsiderationValue]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_FundWeightPercent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] ADD  CONSTRAINT [DF_tblHistoricFundValuations_FundWeightPercent]  DEFAULT ((0)) FOR [FundWeightPercent]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_InstrumentReturn]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] ADD  CONSTRAINT [DF_tblHistoricFundValuations_InstrumentReturn]  DEFAULT ((0)) FOR [InstrumentReturn]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_InstrumentModifedReturn]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] ADD  CONSTRAINT [DF_tblHistoricFundValuations_InstrumentModifedReturn]  DEFAULT ((0)) FOR [InstrumentModifedReturn]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_BenchmarkReturn]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] ADD  CONSTRAINT [DF_tblHistoricFundValuations_BenchmarkReturn]  DEFAULT ((0)) FOR [BenchmarkReturn]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_BenchmarkModifiedReturn]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] ADD  CONSTRAINT [DF_tblHistoricFundValuations_BenchmarkModifiedReturn]  DEFAULT ((0)) FOR [BenchmarkModifiedReturn]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_InstrumentBasisPoints]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] ADD  CONSTRAINT [DF_tblHistoricFundValuations_InstrumentBasisPoints]  DEFAULT ((0)) FOR [InstrumentBasisPoints]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_InstrumentModifiedBasisPoints]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] ADD  CONSTRAINT [DF_tblHistoricFundValuations_InstrumentModifiedBasisPoints]  DEFAULT ((0)) FOR [InstrumentModifiedBasisPoints]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_BenchmarkBasisPoints]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] ADD  CONSTRAINT [DF_tblHistoricFundValuations_BenchmarkBasisPoints]  DEFAULT ((0)) FOR [BenchmarkBasisPoints]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_BenchmarkModifiedBasisPoints]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] ADD  CONSTRAINT [DF_tblHistoricFundValuations_BenchmarkModifiedBasisPoints]  DEFAULT ((0)) FOR [BenchmarkModifiedBasisPoints]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_PreviousFundWeightPercent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] ADD  CONSTRAINT [DF_tblHistoricFundValuations_PreviousFundWeightPercent]  DEFAULT ((0)) FOR [PreviousFundWeightPercent]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_ReturnsSet]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] ADD  CONSTRAINT [DF_tblHistoricFundValuations_ReturnsSet]  DEFAULT ((0)) FOR [ReturnsSet]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHistoricFundValuations_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblHistoricFundValuations] ADD  CONSTRAINT [DF_tblHistoricFundValuations_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblIndexMapping_MappingStartDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblIndexMapping] ADD  CONSTRAINT [DF_tblIndexMapping_MappingStartDate]  DEFAULT (((1)/(1))/(1900)) FOR [MappingStartDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblIndexMapping_MappingEndDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblIndexMapping] ADD  CONSTRAINT [DF_tblIndexMapping_MappingEndDate]  DEFAULT (((1)/(1))/(3000)) FOR [MappingEndDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblIndexMapping_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblIndexMapping] ADD  CONSTRAINT [DF_tblIndexMapping_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentCode]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentCode]  DEFAULT ('') FOR [InstrumentCode]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentPFPCID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentPFPCID]  DEFAULT ('') FOR [InstrumentPFPCID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentParent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentParent]  DEFAULT ((0)) FOR [InstrumentParent]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentParentEquivalentRatio]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentParentEquivalentRatio]  DEFAULT ((1.0)) FOR [InstrumentParentEquivalentRatio]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentClass]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentClass]  DEFAULT ('') FOR [InstrumentClass]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentCurrencyID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentCurrencyID]  DEFAULT ((0)) FOR [InstrumentCurrencyID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentUnderlying]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentUnderlying]  DEFAULT ((0)) FOR [InstrumentUnderlying]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentBenchmark]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentBenchmark]  DEFAULT ((0)) FOR [InstrumentBenchmark]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentContractSize]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentContractSize]  DEFAULT ((1.0)) FOR [InstrumentContractSize]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentMultiplier]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentMultiplier]  DEFAULT ((1.0)) FOR [InstrumentMultiplier]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentExpectedReturn]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentExpectedReturn]  DEFAULT ((0)) FOR [InstrumentExpectedReturn]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentWorstCase]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentWorstCase]  DEFAULT ((0)) FOR [InstrumentWorstCase]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentNoticeDays]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentNoticeDays]  DEFAULT ((0)) FOR [InstrumentNoticeDays]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentDefaultSettlementDays_Buy]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentDefaultSettlementDays_Buy]  DEFAULT ((3)) FOR [InstrumentDefaultSettlementDays_Buy]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentDefaultSettlementDays_Sell]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentDefaultSettlementDays_Sell]  DEFAULT ((3)) FOR [InstrumentDefaultSettlementDays_Sell]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentDealingCutOffTime]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentDealingCutOffTime]  DEFAULT ((0)) FOR [InstrumentDealingCutOffTime]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentCalendar]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentCalendar]  DEFAULT ((0)) FOR [InstrumentCalendar]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentDataPeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentDataPeriod]  DEFAULT ((6)) FOR [InstrumentDataPeriod]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentLockupPeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentLockupPeriod]  DEFAULT ((0)) FOR [InstrumentLockupPeriod]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentLastValidTradeDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentLastValidTradeDate]  DEFAULT ('1900-01-01') FOR [InstrumentLastValidTradeDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentStrike]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentStrike]  DEFAULT ((0)) FOR [InstrumentStrike]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentExpiryDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentExpiryDate]  DEFAULT ('1900-01-01') FOR [InstrumentExpiryDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentPenalty]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentPenalty]  DEFAULT ((0)) FOR [InstrumentPenalty]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentPenaltyComment]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentPenaltyComment]  DEFAULT ('') FOR [InstrumentPenaltyComment]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentUpfrontfees]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentUpfrontfees]  DEFAULT ((0)) FOR [InstrumentUpfrontfees]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentExcludeGAV]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentExcludeGAV]  DEFAULT ((0)) FOR [InstrumentExcludeGAV]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentExcludeAUM]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentExcludeAUM]  DEFAULT ((0)) FOR [InstrumentExcludeAUM]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentIsManagementFee]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentIsManagementFee]  DEFAULT ((0)) FOR [InstrumentIsManagementFee]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentExcludeGNAV]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentExcludeGNAV]  DEFAULT ((0)) FOR [InstrumentIsIncentiveFee]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_IsntrumentIsEqualisation]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_IsntrumentIsEqualisation]  DEFAULT ((0)) FOR [InstrumentIsEqualisation]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentIsLeverageInstrument]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentIsLeverageInstrument]  DEFAULT ((0)) FOR [InstrumentIsLeverageInstrument]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentLeverageCounterparty]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentLeverageCounterparty]  DEFAULT ((0)) FOR [InstrumentLeverageCounterparty]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentExchangeCode]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentExchangeCode]  DEFAULT ('') FOR [InstrumentExchangeCode]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentISIN]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentISIN]  DEFAULT ('') FOR [InstrumentISIN]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentSEDOL]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentSEDOL]  DEFAULT ('') FOR [InstrumentSEDOL]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentMorningstar]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentMorningstar]  DEFAULT ('') FOR [InstrumentMorningstar]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentPortfolioTicker]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentPortfolioTicker]  DEFAULT ('') FOR [InstrumentPortfolioTicker]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentCapturePrice]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentCapturePrice]  DEFAULT ((0)) FOR [InstrumentCapturePrice]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentReviewGroup]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentReviewGroup]  DEFAULT ((0)) FOR [InstrumentReviewGroup]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentUser]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentUser]  DEFAULT (user_name()) FOR [UserName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrument_InstrumentDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrument] ADD  CONSTRAINT [DF_tblInstrument_InstrumentDateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrumentFlorenceEntityLink_LinkID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrumentFlorenceEntityLink] ADD  CONSTRAINT [DF_tblInstrumentFlorenceEntityLink_LinkID]  DEFAULT ((0)) FOR [LinkID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrumentFlorenceEntityLink_InstrumentID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrumentFlorenceEntityLink] ADD  CONSTRAINT [DF_tblInstrumentFlorenceEntityLink_InstrumentID]  DEFAULT ((0)) FOR [InstrumentID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrumentFlorenceEntityLink_EntityID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrumentFlorenceEntityLink] ADD  CONSTRAINT [DF_tblInstrumentFlorenceEntityLink_EntityID]  DEFAULT ((0)) FOR [EntityID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrumentFlorenceEntityLink_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrumentFlorenceEntityLink] ADD  CONSTRAINT [DF_tblInstrumentFlorenceEntityLink_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrumentFlorenceEntityLink_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrumentFlorenceEntityLink] ADD  CONSTRAINT [DF_tblInstrumentFlorenceEntityLink_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrumentType_InstrumentTypeTreatAs]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrumentType] ADD  CONSTRAINT [DF_tblInstrumentType_InstrumentTypeTreatAs]  DEFAULT ((1)) FOR [InstrumentTypeTreatAs]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrumentType_InstrumentTypeFuturesStylePricing]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrumentType] ADD  CONSTRAINT [DF_tblInstrumentType_InstrumentTypeFuturesStylePricing]  DEFAULT ((0)) FOR [InstrumentTypeFuturesStylePricing]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrumentType_InstrumentTypeHasDelta]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrumentType] ADD  CONSTRAINT [DF_tblInstrumentType_InstrumentTypeHasDelta]  DEFAULT ((0)) FOR [InstrumentTypeHasDelta]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrumentType_InstrumentTypeIsExpense]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrumentType] ADD  CONSTRAINT [DF_tblInstrumentType_InstrumentTypeIsExpense]  DEFAULT ((0)) FOR [InstrumentTypeIsExpense]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrumentType_InstrumentTypeUser]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrumentType] ADD  CONSTRAINT [DF_tblInstrumentType_InstrumentTypeUser]  DEFAULT (user_name()) FOR [UserName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInstrumentType_InstrumentTypeDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInstrumentType] ADD  CONSTRAINT [DF_tblInstrumentType_InstrumentTypeDateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInvestorGroup_IGUser]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInvestorGroup] ADD  CONSTRAINT [DF_tblInvestorGroup_IGUser]  DEFAULT (user_name()) FOR [UserName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblInvestorGroup_IGDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblInvestorGroup] ADD  CONSTRAINT [DF_tblInvestorGroup_IGDateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__tblJob__JobPerso__59FA5E80]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblJob] ADD  CONSTRAINT [DF__tblJob__JobPerso__59FA5E80]  DEFAULT ((0)) FOR [JobPerson]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__tblJob__JobFirm__5AEE82B9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblJob] ADD  CONSTRAINT [DF__tblJob__JobFirm__5AEE82B9]  DEFAULT ((0)) FOR [JobFirm]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__tblJob__JobLocat__5BE2A6F2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblJob] ADD  CONSTRAINT [DF__tblJob__JobLocat__5BE2A6F2]  DEFAULT ((0)) FOR [JobLocation]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__tblJob__JobFrom__5CD6CB2B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblJob] ADD  CONSTRAINT [DF__tblJob__JobFrom__5CD6CB2B]  DEFAULT ((0)) FOR [JobFrom]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__tblJob__JobTo__5DCAEF64]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblJob] ADD  CONSTRAINT [DF__tblJob__JobTo__5DCAEF64]  DEFAULT ((0)) FOR [JobTo]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__tblJob__JobCurre__5EBF139D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblJob] ADD  CONSTRAINT [DF__tblJob__JobCurre__5EBF139D]  DEFAULT ((0)) FOR [JobCurrent]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblJob_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblJob] ADD  CONSTRAINT [DF_tblJob_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblLimits_limDescription]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblLimits] ADD  CONSTRAINT [DF_tblLimits_limDescription]  DEFAULT ('') FOR [limDescription]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblLimits_limOnPremium]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblLimits] ADD  CONSTRAINT [DF_tblLimits_limOnPremium]  DEFAULT ((0)) FOR [limOnPremium]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblLimits_limCharacteristic]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblLimits] ADD  CONSTRAINT [DF_tblLimits_limCharacteristic]  DEFAULT ((0)) FOR [limCharacteristic]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblLimits_limSectorGroup]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblLimits] ADD  CONSTRAINT [DF_tblLimits_limSectorGroup]  DEFAULT ('') FOR [limSectorGroup]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblLimits_limDealingPeriod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblLimits] ADD  CONSTRAINT [DF_tblLimits_limDealingPeriod]  DEFAULT ((0)) FOR [limDealingPeriod]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblLimits_limIsPercent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblLimits] ADD  CONSTRAINT [DF_tblLimits_limIsPercent]  DEFAULT ((1)) FOR [limIsPercent]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblLimits_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblLimits] ADD  CONSTRAINT [DF_tblLimits_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblLimits_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblLimits] ADD  CONSTRAINT [DF_tblLimits_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblLimitType_ltpLimitFormat]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblLimitType] ADD  CONSTRAINT [DF_tblLimitType_ltpLimitFormat]  DEFAULT ('') FOR [ltpLimitFormat]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblLimitType_ltpDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblLimitType] ADD  CONSTRAINT [DF_tblLimitType_ltpDateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblLiquidityLimit_LiquidityDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblLiquidityLimit] ADD  CONSTRAINT [DF_tblLiquidityLimit_LiquidityDateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblManagementCompany_FirmPrimaryLocation]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblManagementCompany] ADD  CONSTRAINT [DF_tblManagementCompany_FirmPrimaryLocation]  DEFAULT ((0)) FOR [FirmPrimaryLocation]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblManagementCompany_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblManagementCompany] ADD  CONSTRAINT [DF_tblManagementCompany_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMarketInstruments_Ticker]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMarketInstruments] ADD  CONSTRAINT [DF_tblMarketInstruments_Ticker]  DEFAULT ('') FOR [Ticker]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMarketInstruments_Description]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMarketInstruments] ADD  CONSTRAINT [DF_tblMarketInstruments_Description]  DEFAULT ('') FOR [Description]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMarketInstruments_InstrumentType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMarketInstruments] ADD  CONSTRAINT [DF_tblMarketInstruments_InstrumentType]  DEFAULT ((0)) FOR [InstrumentType]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMarketInstruments_InstrumentCurrencyID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMarketInstruments] ADD  CONSTRAINT [DF_tblMarketInstruments_InstrumentCurrencyID]  DEFAULT ((1)) FOR [InstrumentCurrencyID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMarketInstruments_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMarketInstruments] ADD  CONSTRAINT [DF_tblMarketInstruments_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMarketPrices_DataType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMarketPrices] ADD  CONSTRAINT [DF_tblMarketPrices_DataType]  DEFAULT ((0)) FOR [PriceDataType]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMarketPrices_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMarketPrices] ADD  CONSTRAINT [DF_tblMarketPrices_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMarketPriceTypes_FieldName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMarketPriceTypes] ADD  CONSTRAINT [DF_tblMarketPriceTypes_FieldName]  DEFAULT ('') FOR [FieldName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMarketPriceTypes_TypeDescription]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMarketPriceTypes] ADD  CONSTRAINT [DF_tblMarketPriceTypes_TypeDescription]  DEFAULT ('') FOR [TypeDescription]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingTitle]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] ADD  CONSTRAINT [DF_tblMeeting_MeetingTitle]  DEFAULT ('') FOR [MeetingTitle]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingContact]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] ADD  CONSTRAINT [DF_tblMeeting_MeetingContact]  DEFAULT ('') FOR [MeetingContact]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] ADD  CONSTRAINT [DF_tblMeeting_MeetingDate]  DEFAULT (getdate()) FOR [MeetingDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingPlace]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] ADD  CONSTRAINT [DF_tblMeeting_MeetingPlace]  DEFAULT ('') FOR [MeetingPlace]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingGood]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] ADD  CONSTRAINT [DF_tblMeeting_MeetingGood]  DEFAULT ('') FOR [MeetingGood]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingBad]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] ADD  CONSTRAINT [DF_tblMeeting_MeetingBad]  DEFAULT ('') FOR [MeetingBad]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingWisdom]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] ADD  CONSTRAINT [DF_tblMeeting_MeetingWisdom]  DEFAULT ('') FOR [MeetingWisdom]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingManagementCompanyID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] ADD  CONSTRAINT [DF_tblMeeting_MeetingManagementCompanyID]  DEFAULT ((0)) FOR [MeetingManagementCompanyID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingLocation]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] ADD  CONSTRAINT [DF_tblMeeting_MeetingLocation]  DEFAULT ((0)) FOR [MeetingLocation]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingFundID1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] ADD  CONSTRAINT [DF_tblMeeting_MeetingFundID1]  DEFAULT ((0)) FOR [MeetingFundID1]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingFundID2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] ADD  CONSTRAINT [DF_tblMeeting_MeetingFundID2]  DEFAULT ((0)) FOR [MeetingFundID2]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingFundID3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] ADD  CONSTRAINT [DF_tblMeeting_MeetingFundID3]  DEFAULT ((0)) FOR [MeetingFundID3]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingFundID4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] ADD  CONSTRAINT [DF_tblMeeting_MeetingFundID4]  DEFAULT ((0)) FOR [MeetingFundID4]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingFundID5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] ADD  CONSTRAINT [DF_tblMeeting_MeetingFundID5]  DEFAULT ((0)) FOR [MeetingFundID5]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingFCPerson1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] ADD  CONSTRAINT [DF_tblMeeting_MeetingFCPerson1]  DEFAULT ((0)) FOR [MeetingFCPerson1]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingFCPerson2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] ADD  CONSTRAINT [DF_tblMeeting_MeetingFCPerson2]  DEFAULT ((0)) FOR [MeetingFCPerson2]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingFCPerson3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] ADD  CONSTRAINT [DF_tblMeeting_MeetingFCPerson3]  DEFAULT ((0)) FOR [MeetingFCPerson3]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingFCPerson4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] ADD  CONSTRAINT [DF_tblMeeting_MeetingFCPerson4]  DEFAULT ((0)) FOR [MeetingFCPerson4]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingFCPerson5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] ADD  CONSTRAINT [DF_tblMeeting_MeetingFCPerson5]  DEFAULT ((0)) FOR [MeetingFCPerson5]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingFundPerson1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] ADD  CONSTRAINT [DF_tblMeeting_MeetingFundPerson1]  DEFAULT ((0)) FOR [MeetingFundPerson1]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingFundPerson2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] ADD  CONSTRAINT [DF_tblMeeting_MeetingFundPerson2]  DEFAULT ((0)) FOR [MeetingFundPerson2]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingFundPerson3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] ADD  CONSTRAINT [DF_tblMeeting_MeetingFundPerson3]  DEFAULT ((3)) FOR [MeetingFundPerson3]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingFundPerson4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] ADD  CONSTRAINT [DF_tblMeeting_MeetingFundPerson4]  DEFAULT ((0)) FOR [MeetingFundPerson4]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_MeetingFundPerson5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] ADD  CONSTRAINT [DF_tblMeeting_MeetingFundPerson5]  DEFAULT ((0)) FOR [MeetingFundPerson5]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__tblMeetin__Meeti__49C3F6B7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] ADD  CONSTRAINT [DF__tblMeetin__Meeti__49C3F6B7]  DEFAULT ((0)) FOR [MeetingStatus]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__tblMeetin__Meeti__4AB81AF0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] ADD  CONSTRAINT [DF__tblMeetin__Meeti__4AB81AF0]  DEFAULT ((0)) FOR [MeetingExistingFund]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__tblMeetin__Meeti__4BAC3F29]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] ADD  CONSTRAINT [DF__tblMeetin__Meeti__4BAC3F29]  DEFAULT ((0)) FOR [MeetingFundType]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] ADD  CONSTRAINT [DF_tblMeeting_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeeting_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeeting] ADD  CONSTRAINT [DF_tblMeeting_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeetingPeople_PeoplePerson]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeetingPeople] ADD  CONSTRAINT [DF_tblMeetingPeople_PeoplePerson]  DEFAULT ('') FOR [PeoplePerson]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__tblPeople__Peopl__398D8EEE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeetingPeople] ADD  CONSTRAINT [DF__tblPeople__Peopl__398D8EEE]  DEFAULT ((0)) FOR [PeopleFirm]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeetingPeople_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeetingPeople] ADD  CONSTRAINT [DF_tblMeetingPeople_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMeetingStatus_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMeetingStatus] ADD  CONSTRAINT [DF_tblMeetingStatus_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMonthlyComment_FundID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMonthlyComment] ADD  CONSTRAINT [DF_tblMonthlyComment_FundID]  DEFAULT ((0)) FOR [FundID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMonthlyComment_CommentText]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMonthlyComment] ADD  CONSTRAINT [DF_tblMonthlyComment_CommentText]  DEFAULT ('') FOR [CommentText]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblMonthlyComment_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblMonthlyComment] ADD  CONSTRAINT [DF_tblMonthlyComment_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingDataTaskEmails_DataTaskID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingDataTaskEmails] ADD  CONSTRAINT [DF_tblPendingDataTaskEmails_DataTaskID]  DEFAULT ((0)) FOR [DataTaskID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingDataTaskEmails_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingDataTaskEmails] ADD  CONSTRAINT [DF_tblPendingDataTaskEmails_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTradeFileEntries_BrokerID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTradeFileEntries] ADD  CONSTRAINT [DF_tblPendingTradeFileEntries_BrokerID]  DEFAULT ((0)) FOR [BrokerID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTradeFileEntries_WorkflowID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTradeFileEntries] ADD  CONSTRAINT [DF_tblPendingTradeFileEntries_WorkflowID]  DEFAULT ((0)) FOR [WorkflowID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTradeFileEntries_TradefileText]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTradeFileEntries] ADD  CONSTRAINT [DF_tblPendingTradeFileEntries_TradefileText]  DEFAULT ('') FOR [TradefileText]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTradeFileEntries_UserName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTradeFileEntries] ADD  CONSTRAINT [DF_tblPendingTradeFileEntries_UserName]  DEFAULT (user_name()) FOR [UserName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTradeFileEntries_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTradeFileEntries] ADD  CONSTRAINT [DF_tblPendingTradeFileEntries_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionTicket]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] ADD  CONSTRAINT [DF_tblPendingTransactions_TransactionTicket]  DEFAULT ('') FOR [TransactionTicket]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionFund]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] ADD  CONSTRAINT [DF_tblPendingTransactions_TransactionFund]  DEFAULT ((0)) FOR [TransactionFund]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionSubFundValueFlag]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] ADD  CONSTRAINT [DF_tblPendingTransactions_TransactionSubFundValueFlag]  DEFAULT ((0)) FOR [TransactionSubFundValueFlag]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionInstrument]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] ADD  CONSTRAINT [DF_tblPendingTransactions_TransactionInstrument]  DEFAULT ((0)) FOR [TransactionInstrument]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] ADD  CONSTRAINT [DF_tblPendingTransactions_TransactionType]  DEFAULT ((0)) FOR [TransactionType]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionValueorAmount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] ADD  CONSTRAINT [DF_tblPendingTransactions_TransactionValueorAmount]  DEFAULT ('Amount') FOR [TransactionValueorAmount]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionUnits]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] ADD  CONSTRAINT [DF_tblPendingTransactions_TransactionUnits]  DEFAULT ((0)) FOR [TransactionUnits]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionPrice]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] ADD  CONSTRAINT [DF_tblPendingTransactions_TransactionPrice]  DEFAULT ((0)) FOR [TransactionPrice]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionCosts]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] ADD  CONSTRAINT [DF_tblPendingTransactions_TransactionCosts]  DEFAULT ((0)) FOR [TransactionCosts]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionCostPercent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] ADD  CONSTRAINT [DF_tblPendingTransactions_TransactionCostPercent]  DEFAULT ((0)) FOR [TransactionCostPercent]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionMainFundPrice]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] ADD  CONSTRAINT [DF_tblPendingTransactions_TransactionMainFundPrice]  DEFAULT ((0)) FOR [TransactionMainFundPrice]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionFXRate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] ADD  CONSTRAINT [DF_tblPendingTransactions_TransactionFXRate]  DEFAULT ((0)) FOR [TransactionFXRate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionEffectivePrice]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] ADD  CONSTRAINT [DF_tblPendingTransactions_TransactionEffectivePrice]  DEFAULT ((0)) FOR [TransactionEffectivePrice]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionEffectiveWatermark]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] ADD  CONSTRAINT [DF_tblPendingTransactions_TransactionEffectiveWatermark]  DEFAULT ((0)) FOR [TransactionEffectiveWatermark]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionSpecificInitialEqualisationValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] ADD  CONSTRAINT [DF_tblPendingTransactions_TransactionSpecificInitialEqualisationValue]  DEFAULT ((0)) FOR [TransactionSpecificInitialEqualisationValue]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionNewCounterparty]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] ADD  CONSTRAINT [DF_tblPendingTransactions_TransactionNewCounterparty]  DEFAULT ((0)) FOR [TransactionCounterparty]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionInvestment]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] ADD  CONSTRAINT [DF_tblPendingTransactions_TransactionInvestment]  DEFAULT ('') FOR [TransactionInvestment]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionExecution]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] ADD  CONSTRAINT [DF_tblPendingTransactions_TransactionExecution]  DEFAULT ('') FOR [TransactionExecution]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionDataEntry]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] ADD  CONSTRAINT [DF_tblPendingTransactions_TransactionDataEntry]  DEFAULT ('') FOR [TransactionDataEntry]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionCostIsPercent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] ADD  CONSTRAINT [DF_tblPendingTransactions_TransactionCostIsPercent]  DEFAULT ((0)) FOR [TransactionCostIsPercent]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionIsTransfer]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] ADD  CONSTRAINT [DF_tblPendingTransactions_TransactionIsTransfer]  DEFAULT ((0)) FOR [TransactionIsTransfer]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionExemptFromUpdate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] ADD  CONSTRAINT [DF_tblPendingTransactions_TransactionExemptFromUpdate]  DEFAULT ((0)) FOR [TransactionExemptFromUpdate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionSpecificInitialEqualisationFlag]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] ADD  CONSTRAINT [DF_tblPendingTransactions_TransactionSpecificInitialEqualisationFlag]  DEFAULT ((0)) FOR [TransactionSpecificInitialEqualisationFlag]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionTradeStatusID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] ADD  CONSTRAINT [DF_tblPendingTransactions_TransactionTradeStatusID]  DEFAULT ((0)) FOR [TransactionTradeStatusID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionComment]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] ADD  CONSTRAINT [DF_tblPendingTransactions_TransactionComment]  DEFAULT ('') FOR [TransactionComment]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionUser]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] ADD  CONSTRAINT [DF_tblPendingTransactions_TransactionUser]  DEFAULT (user_name()) FOR [TransactionUser]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionGroup]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] ADD  CONSTRAINT [DF_tblPendingTransactions_TransactionGroup]  DEFAULT ('') FOR [TransactionGroup]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionAdminStatus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] ADD  CONSTRAINT [DF_tblPendingTransactions_TransactionAdminStatus]  DEFAULT ((0)) FOR [TransactionAdminStatus]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionFLAGS]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] ADD  CONSTRAINT [DF_tblPendingTransactions_TransactionFLAGS]  DEFAULT ((0)) FOR [TransactionCTFLAGS]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPendingTransactions_TransactionCompoundID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPendingTransactions] ADD  CONSTRAINT [DF_tblPendingTransactions_TransactionCompoundID]  DEFAULT ((0)) FOR [TransactionCompoundRN]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPeriod_AppName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPeriod] ADD  CONSTRAINT [DF_tblPeriod_AppName]  DEFAULT (app_name()) FOR [AppName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPeriod_IsGlobal]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPeriod] ADD  CONSTRAINT [DF_tblPeriod_IsGlobal]  DEFAULT ((0)) FOR [IsGlobal]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPeriod_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPeriod] ADD  CONSTRAINT [DF_tblPeriod_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPeriod_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPeriod] ADD  CONSTRAINT [DF_tblPeriod_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPerson_PersonEmail]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPerson] ADD  CONSTRAINT [DF_tblPerson_PersonEmail]  DEFAULT ('') FOR [PersonEmail]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPerson_PersonUser]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPerson] ADD  CONSTRAINT [DF_tblPerson_PersonUser]  DEFAULT (user_name()) FOR [UserName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPerson_PersonDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPerson] ADD  CONSTRAINT [DF_tblPerson_PersonDateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFieldData_GroupID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFieldData] ADD  CONSTRAINT [DF_tblPertracCustomFieldData_GroupID]  DEFAULT ((0)) FOR [GroupID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFieldData_FieldNumericData]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFieldData] ADD  CONSTRAINT [DF_tblPertracCustomFieldData_FieldNumericData]  DEFAULT ((0)) FOR [FieldNumericData]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFieldData_FieldTextData]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFieldData] ADD  CONSTRAINT [DF_tblPertracCustomFieldData_FieldTextData]  DEFAULT ('') FOR [FieldTextData]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFieldData_FieldDateData]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFieldData] ADD  CONSTRAINT [DF_tblPertracCustomFieldData_FieldDateData]  DEFAULT ('1/1/1900') FOR [FieldDateData]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFieldData_FieldBooleandData]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFieldData] ADD  CONSTRAINT [DF_tblPertracCustomFieldData_FieldBooleandData]  DEFAULT ((0)) FOR [FieldBooleanData]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFieldData_UpdateRequired]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFieldData] ADD  CONSTRAINT [DF_tblPertracCustomFieldData_UpdateRequired]  DEFAULT ((0)) FOR [UpdateRequired]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFieldData_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFieldData] ADD  CONSTRAINT [DF_tblPertracCustomFieldData_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFieldData_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFieldData] ADD  CONSTRAINT [DF_tblPertracCustomFieldData_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFields_FieldName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFields] ADD  CONSTRAINT [DF_tblPertracCustomFields_FieldName]  DEFAULT ('<Name not Set>') FOR [FieldName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFields_IsMaxValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFields] ADD  CONSTRAINT [DF_tblPertracCustomFields_IsMaxValue]  DEFAULT ((0)) FOR [IsMaxValue]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFields_IsMinValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFields] ADD  CONSTRAINT [DF_tblPertracCustomFields_IsMinValue]  DEFAULT ((0)) FOR [IsMinValue]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFields_IsFirstValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFields] ADD  CONSTRAINT [DF_tblPertracCustomFields_IsFirstValue]  DEFAULT ((0)) FOR [IsFirstValue]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFields_IsLastValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFields] ADD  CONSTRAINT [DF_tblPertracCustomFields_IsLastValue]  DEFAULT ((0)) FOR [IsLastValue]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFields_IsAverageValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFields] ADD  CONSTRAINT [DF_tblPertracCustomFields_IsAverageValue]  DEFAULT ((0)) FOR [IsAverageValue]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFields_IsIRR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFields] ADD  CONSTRAINT [DF_tblPertracCustomFields_IsIRR]  DEFAULT ((0)) FOR [IsIRR]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFields_FieldPeriodCount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFields] ADD  CONSTRAINT [DF_tblPertracCustomFields_FieldPeriodCount]  DEFAULT ((0)) FOR [FieldPeriodCount]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFields_FieldIsVolatile]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFields] ADD  CONSTRAINT [DF_tblPertracCustomFields_FieldIsVolatile]  DEFAULT ((0)) FOR [FieldIsVolatile]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFields_IsCalculated]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFields] ADD  CONSTRAINT [DF_tblPertracCustomFields_IsCalculated]  DEFAULT ((1)) FOR [FieldIsCalculated]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFields_FieldIsSearchable]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFields] ADD  CONSTRAINT [DF_tblPertracCustomFields_FieldIsSearchable]  DEFAULT ((1)) FOR [FieldIsSearchable]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFields_FieldIsOptimisable]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFields] ADD  CONSTRAINT [DF_tblPertracCustomFields_FieldIsOptimisable]  DEFAULT ((1)) FOR [FieldIsOptimisable]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFields_DefaultValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFields] ADD  CONSTRAINT [DF_tblPertracCustomFields_DefaultValue]  DEFAULT ('') FOR [DefaultValue]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFields_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFields] ADD  CONSTRAINT [DF_tblPertracCustomFields_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracCustomFields_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracCustomFields] ADD  CONSTRAINT [DF_tblPertracCustomFields_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracFieldMapping_PertracField]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracFieldMapping] ADD  CONSTRAINT [DF_tblPertracFieldMapping_PertracField]  DEFAULT ('') FOR [PertracField]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracFieldMapping_ProviderFieldName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracFieldMapping] ADD  CONSTRAINT [DF_tblPertracFieldMapping_ProviderFieldName]  DEFAULT ('') FOR [ProviderFieldName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracFieldMapping_IsNumeric]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracFieldMapping] ADD  CONSTRAINT [DF_tblPertracFieldMapping_IsNumeric]  DEFAULT ((0)) FOR [FieldIsNumeric]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracFieldMapping_Multiplier]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracFieldMapping] ADD  CONSTRAINT [DF_tblPertracFieldMapping_Multiplier]  DEFAULT ((1)) FOR [Multiplier]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracInstrumentFlags_IsGroupFlag]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracInstrumentFlags] ADD  CONSTRAINT [DF_tblPertracInstrumentFlags_IsGroupFlag]  DEFAULT ((1)) FOR [IsGroupFlag]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracInstrumentFlags_IsSimulationFlag]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracInstrumentFlags] ADD  CONSTRAINT [DF_tblPertracInstrumentFlags_IsSimulationFlag]  DEFAULT ((0)) FOR [IsSimulationFlag]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPertracInstrumentFlags_IsVenice]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPertracInstrumentFlags] ADD  CONSTRAINT [DF_tblPertracInstrumentFlags_IsVenice]  DEFAULT ((0)) FOR [IsVeniceFlag]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCCustodyRecord_FundID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCCustodyRecord] ADD  CONSTRAINT [DF_tblPFPCCustodyRecord_FundID]  DEFAULT ((0)) FOR [FundID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCCustodyRecord_SecurityID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCCustodyRecord] ADD  CONSTRAINT [DF_tblPFPCCustodyRecord_SecurityID]  DEFAULT ('') FOR [SecurityID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCCustodyRecord_ShareClass]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCCustodyRecord] ADD  CONSTRAINT [DF_tblPFPCCustodyRecord_ShareClass]  DEFAULT ('') FOR [ShareClass]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCCustodyRecord_Holding]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCCustodyRecord] ADD  CONSTRAINT [DF_tblPFPCCustodyRecord_Holding]  DEFAULT ((0)) FOR [Holding]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_FundID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] ADD  CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_FundID]  DEFAULT ((0)) FOR [FundID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_FCPInstrumentID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] ADD  CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_FCPInstrumentID]  DEFAULT ((0)) FOR [FCPInstrumentID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_PFPCInstrumentID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] ADD  CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_PFPCInstrumentID]  DEFAULT ('') FOR [PFPCInstrumentID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_PFPCInstrumentDescription]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] ADD  CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_PFPCInstrumentDescription]  DEFAULT ('') FOR [PFPCInstrumentDescription]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_FCP_Holding]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] ADD  CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_FCP_Holding]  DEFAULT ((0)) FOR [FCP_Holding]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_FCP_HoldingCompleteness]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] ADD  CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_FCP_HoldingCompleteness]  DEFAULT ((0)) FOR [FCP_HoldingCompleteness]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_PFPC_Holding]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] ADD  CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_PFPC_Holding]  DEFAULT ((0)) FOR [PFPC_Holding]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_Holding_Difference]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] ADD  CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_Holding_Difference]  DEFAULT ((0)) FOR [Holding_Difference]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_FCP_Price]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] ADD  CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_FCP_Price]  DEFAULT ((0)) FOR [FCP_Price]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_FCP_PriceFinal]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] ADD  CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_FCP_PriceFinal]  DEFAULT ((0)) FOR [FCP_PriceFinal]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_FCP_PriceAdministrator]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] ADD  CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_FCP_PriceAdministrator]  DEFAULT ((0)) FOR [FCP_PriceAdministrator]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_PFPC_Price]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] ADD  CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_PFPC_Price]  DEFAULT ((0)) FOR [PFPC_Price]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_Price_Difference]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] ADD  CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_Price_Difference]  DEFAULT ((0)) FOR [Price_Difference]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_FCP_FXRate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] ADD  CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_FCP_FXRate]  DEFAULT ((0)) FOR [FCP_FXRate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_PFPC_FXRate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] ADD  CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_PFPC_FXRate]  DEFAULT ((0)) FOR [PFPC_FXRate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_FXRate_Difference]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] ADD  CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_FXRate_Difference]  DEFAULT ((0)) FOR [FXRate_Difference]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_FCP_LocalCcyValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] ADD  CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_FCP_LocalCcyValue]  DEFAULT ((0)) FOR [FCP_LocalCcyValue]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_PFPC_LocalCcyValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] ADD  CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_PFPC_LocalCcyValue]  DEFAULT ((0)) FOR [PFPC_LocalCcyValue]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_LocalCcyValue_Difference]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] ADD  CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_LocalCcyValue_Difference]  DEFAULT ((0)) FOR [LocalCcyValue_Difference]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_FCP_BookCcyValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] ADD  CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_FCP_BookCcyValue]  DEFAULT ((0)) FOR [FCP_BookCcyValue]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_PFPC_BookCcyValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] ADD  CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_PFPC_BookCcyValue]  DEFAULT ((0)) FOR [PFPC_BookCcyValue]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_BookCcyValue_Difference]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] ADD  CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_BookCcyValue_Difference]  DEFAULT ((0)) FOR [BookCcyValue_Difference]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioReconcilliation_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioReconcilliation] ADD  CONSTRAINT [DF_tblPFPCPortfolioReconcilliation_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioValuation_FundID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioValuation] ADD  CONSTRAINT [DF_tblPFPCPortfolioValuation_FundID]  DEFAULT ((0)) FOR [FundID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioValuation_FundName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioValuation] ADD  CONSTRAINT [DF_tblPFPCPortfolioValuation_FundName]  DEFAULT ('') FOR [FundName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioValuation_FundCurrency]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioValuation] ADD  CONSTRAINT [DF_tblPFPCPortfolioValuation_FundCurrency]  DEFAULT ('') FOR [FundCurrency]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioValuation_InstrumentID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioValuation] ADD  CONSTRAINT [DF_tblPFPCPortfolioValuation_InstrumentID]  DEFAULT ('') FOR [InstrumentID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioValuation_InstrumentDescription]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioValuation] ADD  CONSTRAINT [DF_tblPFPCPortfolioValuation_InstrumentDescription]  DEFAULT ('') FOR [InstrumentDescription]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioValuation_InstrumentCurrency]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioValuation] ADD  CONSTRAINT [DF_tblPFPCPortfolioValuation_InstrumentCurrency]  DEFAULT ('') FOR [InstrumentCurrency]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioValuation_Amount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioValuation] ADD  CONSTRAINT [DF_tblPFPCPortfolioValuation_Amount]  DEFAULT ((0)) FOR [Amount]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioValuation_LocalCcyPrice]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioValuation] ADD  CONSTRAINT [DF_tblPFPCPortfolioValuation_LocalCcyPrice]  DEFAULT ((0)) FOR [LocalCcyPrice]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioValuation_FxRate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioValuation] ADD  CONSTRAINT [DF_tblPFPCPortfolioValuation_FxRate]  DEFAULT ((0)) FOR [FxRate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioValuation_LocalCcyValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioValuation] ADD  CONSTRAINT [DF_tblPFPCPortfolioValuation_LocalCcyValue]  DEFAULT ((0)) FOR [LocalCcyValue]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioValuation_BookCcyValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioValuation] ADD  CONSTRAINT [DF_tblPFPCPortfolioValuation_BookCcyValue]  DEFAULT ((0)) FOR [BookCcyValue]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCPortfolioValuation_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCPortfolioValuation] ADD  CONSTRAINT [DF_tblPFPCPortfolioValuation_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCShareAllocation_FundID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCShareAllocation] ADD  CONSTRAINT [DF_tblPFPCShareAllocation_FundID]  DEFAULT ((0)) FOR [FundID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCShareAllocation_ReconciliationDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCShareAllocation] ADD  CONSTRAINT [DF_tblPFPCShareAllocation_ReconciliationDate]  DEFAULT (getdate()) FOR [ValuationDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCShareAllocation_TradeDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCShareAllocation] ADD  CONSTRAINT [DF_tblPFPCShareAllocation_TradeDate]  DEFAULT (getdate()) FOR [TradeDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCShareAllocation_AccountID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCShareAllocation] ADD  CONSTRAINT [DF_tblPFPCShareAllocation_AccountID]  DEFAULT ('') FOR [AccountID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCShareAllocation_TransactionType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCShareAllocation] ADD  CONSTRAINT [DF_tblPFPCShareAllocation_TransactionType]  DEFAULT ('') FOR [TransactionType]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCShareAllocation_IssuedShares]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCShareAllocation] ADD  CONSTRAINT [DF_tblPFPCShareAllocation_IssuedShares]  DEFAULT ((0)) FOR [IssuedShares]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPFPCShareAllocation_CashInvested]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPFPCShareAllocation] ADD  CONSTRAINT [DF_tblPFPCShareAllocation_CashInvested]  DEFAULT ((0)) FOR [CashInvested]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPortfolioData_PortfolioItemID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPortfolioData] ADD  CONSTRAINT [DF_tblPortfolioData_PortfolioItemID]  DEFAULT (ident_current('tblPortfolioData')+(1)) FOR [PortfolioItemID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPortfolioData_FilingDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPortfolioData] ADD  CONSTRAINT [DF_tblPortfolioData_FilingDate]  DEFAULT ('1 Jan 1900') FOR [FilingDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPortfolioData_MarketInstrumentID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPortfolioData] ADD  CONSTRAINT [DF_tblPortfolioData_MarketInstrumentID]  DEFAULT ((0)) FOR [MarketInstrumentID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPortfolioData_Holding]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPortfolioData] ADD  CONSTRAINT [DF_tblPortfolioData_Holding]  DEFAULT ((0)) FOR [Holding]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPortfolioData_Weight]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPortfolioData] ADD  CONSTRAINT [DF_tblPortfolioData_Weight]  DEFAULT ((0)) FOR [Weight]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPortfolioData_MonitorItem]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPortfolioData] ADD  CONSTRAINT [DF_tblPortfolioData_MonitorItem]  DEFAULT ((0)) FOR [MonitorItem]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPortfolioData_XMLData]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPortfolioData] ADD  CONSTRAINT [DF_tblPortfolioData_XMLData]  DEFAULT ('') FOR [XMLData]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPortfolioData_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPortfolioData] ADD  CONSTRAINT [DF_tblPortfolioData_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPortfolioData_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPortfolioData] ADD  CONSTRAINT [DF_tblPortfolioData_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPortfolioIndex_PortfolioID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPortfolioIndex] ADD  CONSTRAINT [DF_tblPortfolioIndex_PortfolioID]  DEFAULT (ident_current('tblPortfolioIndex')+(1)) FOR [PortfolioID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPortfolioIndex_PortfolioDescription]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPortfolioIndex] ADD  CONSTRAINT [DF_tblPortfolioIndex_PortfolioDescription]  DEFAULT ('') FOR [PortfolioDescription]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPortfolioIndex_PortfolioType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPortfolioIndex] ADD  CONSTRAINT [DF_tblPortfolioIndex_PortfolioType]  DEFAULT ((0)) FOR [PortfolioType]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPortfolioIndex_PortfolioFilingDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPortfolioIndex] ADD  CONSTRAINT [DF_tblPortfolioIndex_PortfolioFilingDate]  DEFAULT (((1)/(1))/(1900)) FOR [PortfolioFilingDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPortfolioIndex_PortfolioDataID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPortfolioIndex] ADD  CONSTRAINT [DF_tblPortfolioIndex_PortfolioDataID]  DEFAULT (ident_current('tblPortfolioIndex')+(1)) FOR [PortfolioDataID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPortfolioIndex_InstrumentId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPortfolioIndex] ADD  CONSTRAINT [DF_tblPortfolioIndex_InstrumentId]  DEFAULT ((0)) FOR [InstrumentID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPortfolioIndex_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPortfolioIndex] ADD  CONSTRAINT [DF_tblPortfolioIndex_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPortfolioIndex_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPortfolioIndex] ADD  CONSTRAINT [DF_tblPortfolioIndex_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPortfolioItemTypes_PortfolioItemTypeDescription]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPortfolioItemTypes] ADD  CONSTRAINT [DF_tblPortfolioItemTypes_PortfolioItemTypeDescription]  DEFAULT ('') FOR [PortfolioItemTypeDescription]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPortfolioType_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPortfolioType] ADD  CONSTRAINT [DF_tblPortfolioType_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPrice_PriceInstrument]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPrice] ADD  CONSTRAINT [DF_tblPrice_PriceInstrument]  DEFAULT ((0)) FOR [PriceInstrument]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPrice_PricePercent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPrice] ADD  CONSTRAINT [DF_tblPrice_PricePercent]  DEFAULT ((0)) FOR [PricePercent]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPrice_PriceLevel]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPrice] ADD  CONSTRAINT [DF_tblPrice_PriceLevel]  DEFAULT ((0)) FOR [PriceLevel]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPrice_PriceMultiplier]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPrice] ADD  CONSTRAINT [DF_tblPrice_PriceMultiplier]  DEFAULT ((1.0)) FOR [PriceMultiplier]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPrice_PriceNAV]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPrice] ADD  CONSTRAINT [DF_tblPrice_PriceNAV]  DEFAULT ((0)) FOR [PriceNAV]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPrice_PriceBasicNAV]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPrice] ADD  CONSTRAINT [DF_tblPrice_PriceBasicNAV]  DEFAULT ((0)) FOR [PriceBasicNAV]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPrice_PriceGNAV]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPrice] ADD  CONSTRAINT [DF_tblPrice_PriceGNAV]  DEFAULT ((0)) FOR [PriceGNAV]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPrice_PriceGAV]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPrice] ADD  CONSTRAINT [DF_tblPrice_PriceGAV]  DEFAULT ((0)) FOR [PriceGAV]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPrice_PriceFinal]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPrice] ADD  CONSTRAINT [DF_tblPrice_PriceFinal]  DEFAULT ((0)) FOR [PriceFinal]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPrice_PriceIsPercent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPrice] ADD  CONSTRAINT [DF_tblPrice_PriceIsPercent]  DEFAULT ((0)) FOR [PriceIsPercent]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPrice_PriceIsAdministrator]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPrice] ADD  CONSTRAINT [DF_tblPrice_PriceIsAdministrator]  DEFAULT ((0)) FOR [PriceIsAdministrator]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPrice_PriceIsMilestone]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPrice] ADD  CONSTRAINT [DF_tblPrice_PriceIsMilestone]  DEFAULT ((0)) FOR [PriceIsMilestone]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPrice_PriceUser]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPrice] ADD  CONSTRAINT [DF_tblPrice_PriceUser]  DEFAULT (user_name()) FOR [UserName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblPrice_PriceDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblPrice] ADD  CONSTRAINT [DF_tblPrice_PriceDateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblReferentialIntegrity_IsDescriptiveReferenceOnly]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblReferentialIntegrity] ADD  CONSTRAINT [DF_tblReferentialIntegrity_IsDescriptiveReferenceOnly]  DEFAULT ((0)) FOR [IsDescriptiveReferenceOnly]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblReferentialIntegrity_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblReferentialIntegrity] ADD  CONSTRAINT [DF_tblReferentialIntegrity_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblReportCustomisation_IsNumeric]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblReportCustomisation] ADD  CONSTRAINT [DF_tblReportCustomisation_IsNumeric]  DEFAULT ((0)) FOR [IsNumeric]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblReportDetailLog_AppName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblReportDetailLog] ADD  CONSTRAINT [DF_tblReportDetailLog_AppName]  DEFAULT (app_name()) FOR [AppName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblReportDetailLog_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblReportDetailLog] ADD  CONSTRAINT [DF_tblReportDetailLog_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskData_RiskDataID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskData] ADD  CONSTRAINT [DF_tblRiskData_RiskDataID]  DEFAULT ((0)) FOR [RiskDataID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskData_DataWeighting]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskData] ADD  CONSTRAINT [DF_tblRiskData_DataWeighting]  DEFAULT ((0)) FOR [DataWeighting]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskData_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskData] ADD  CONSTRAINT [DF_tblRiskData_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskData_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskData] ADD  CONSTRAINT [DF_tblRiskData_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataCategory_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataCategory] ADD  CONSTRAINT [DF_tblRiskDataCategory_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataCategory_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataCategory] ADD  CONSTRAINT [DF_tblRiskDataCategory_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataCharacteristic_DataCategoryId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataCharacteristic] ADD  CONSTRAINT [DF_tblRiskDataCharacteristic_DataCategoryId]  DEFAULT ((0)) FOR [DataCategoryId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataCharacteristic_DataCharacteristicId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataCharacteristic] ADD  CONSTRAINT [DF_tblRiskDataCharacteristic_DataCharacteristicId]  DEFAULT ((0)) FOR [DataCharacteristicId]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataCharacteristic_DataCharacteristic]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataCharacteristic] ADD  CONSTRAINT [DF_tblRiskDataCharacteristic_DataCharacteristic]  DEFAULT ('') FOR [DataCharacteristic]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataCharacteristic_CharacteristicType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataCharacteristic] ADD  CONSTRAINT [DF_tblRiskDataCharacteristic_CharacteristicType]  DEFAULT ((0)) FOR [DataCharacteristicTypeID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataCharacteristic_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataCharacteristic] ADD  CONSTRAINT [DF_tblRiskDataCharacteristic_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataCharacteristic_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataCharacteristic] ADD  CONSTRAINT [DF_tblRiskDataCharacteristic_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataCharacteristicTypes_DataCharacteristicTypeID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataCharacteristicTypes] ADD  CONSTRAINT [DF_tblRiskDataCharacteristicTypes_DataCharacteristicTypeID]  DEFAULT ((0)) FOR [DataCharacteristicTypeID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataCharacteristicTypes_DataCharacteristicType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataCharacteristicTypes] ADD  CONSTRAINT [DF_tblRiskDataCharacteristicTypes_DataCharacteristicType]  DEFAULT ('') FOR [DataCharacteristicType]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataCharacteristicTypes_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataCharacteristicTypes] ADD  CONSTRAINT [DF_tblRiskDataCharacteristicTypes_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataCharacteristicTypes_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataCharacteristicTypes] ADD  CONSTRAINT [DF_tblRiskDataCharacteristicTypes_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataCompoundCharacteristic_CompoundID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataCompoundCharacteristic] ADD  CONSTRAINT [DF_tblRiskDataCompoundCharacteristic_CompoundID]  DEFAULT ((0)) FOR [CompoundID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataCompoundCharacteristic_ParentCharacteristicID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataCompoundCharacteristic] ADD  CONSTRAINT [DF_tblRiskDataCompoundCharacteristic_ParentCharacteristicID]  DEFAULT ((0)) FOR [ParentCharacteristicID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataCompoundCharacteristic_ChildCharacteristicID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataCompoundCharacteristic] ADD  CONSTRAINT [DF_tblRiskDataCompoundCharacteristic_ChildCharacteristicID]  DEFAULT ((0)) FOR [ChildCharacteristicID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataCompoundCharacteristic_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataCompoundCharacteristic] ADD  CONSTRAINT [DF_tblRiskDataCompoundCharacteristic_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataCompoundCharacteristic_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataCompoundCharacteristic] ADD  CONSTRAINT [DF_tblRiskDataCompoundCharacteristic_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataLimits_LimitDescription]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataLimits] ADD  CONSTRAINT [DF_tblRiskDataLimits_LimitDescription]  DEFAULT ('') FOR [LimitDescription]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataLimits_FundID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataLimits] ADD  CONSTRAINT [DF_tblRiskDataLimits_FundID]  DEFAULT ((0)) FOR [FundID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataLimits_InstrumentID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataLimits] ADD  CONSTRAINT [DF_tblRiskDataLimits_InstrumentID]  DEFAULT ((0)) FOR [InstrumentID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataLimits_Granularity]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataLimits] ADD  CONSTRAINT [DF_tblRiskDataLimits_Granularity]  DEFAULT ((0)) FOR [Granularity]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataLimits_GreaterOrLessThan]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataLimits] ADD  CONSTRAINT [DF_tblRiskDataLimits_GreaterOrLessThan]  DEFAULT ((-1)) FOR [GreaterOrLessThan]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataLimits_LimitLevel]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataLimits] ADD  CONSTRAINT [DF_tblRiskDataLimits_LimitLevel]  DEFAULT ((0)) FOR [LimitLevel]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataLimits_isPercent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataLimits] ADD  CONSTRAINT [DF_tblRiskDataLimits_isPercent]  DEFAULT ((1)) FOR [isPercent]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataLimits_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataLimits] ADD  CONSTRAINT [DF_tblRiskDataLimits_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblRiskDataLimits_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblRiskDataLimits] ADD  CONSTRAINT [DF_tblRiskDataLimits_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblSectorLimit_UserName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblSectorLimit] ADD  CONSTRAINT [DF_tblSectorLimit_UserName]  DEFAULT (user_name()) FOR [UserName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblSectorLimit_SectorDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblSectorLimit] ADD  CONSTRAINT [DF_tblSectorLimit_SectorDateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblSophisBusinessEvents_ExistingInstrument]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblSophisBusinessEvents] ADD  CONSTRAINT [DF_tblSophisBusinessEvents_ExistingInstrument]  DEFAULT ((0)) FOR [ExistingInstrument]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblSophisBusinessEvents_SubstituteInstrument]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblSophisBusinessEvents] ADD  CONSTRAINT [DF_tblSophisBusinessEvents_SubstituteInstrument]  DEFAULT ((0)) FOR [SubstituteInstrument]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblSophisBusinessEvents_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblSophisBusinessEvents] ADD  CONSTRAINT [DF_tblSophisBusinessEvents_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblSophisBusinessEvents_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblSophisBusinessEvents] ADD  CONSTRAINT [DF_tblSophisBusinessEvents_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblSophisStatusGroups_UserName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblSophisStatusGroups] ADD  CONSTRAINT [DF_tblSophisStatusGroups_UserName]  DEFAULT (user_name()) FOR [UserName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblSophisStatusGroups_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblSophisStatusGroups] ADD  CONSTRAINT [DF_tblSophisStatusGroups_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblSubFund_UserName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblSubFund] ADD  CONSTRAINT [DF_tblSubFund_UserName]  DEFAULT (user_name()) FOR [UserName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblSubFund_SubFundDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblSubFund] ADD  CONSTRAINT [DF_tblSubFund_SubFundDateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblSubscriptionLog_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblSubscriptionLog] ADD  CONSTRAINT [DF_tblSubscriptionLog_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblSubscriptionLog_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblSubscriptionLog] ADD  CONSTRAINT [DF_tblSubscriptionLog_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblSystemDates_SysdateUser]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblSystemDates] ADD  CONSTRAINT [DF_tblSystemDates_SysdateUser]  DEFAULT (user_name()) FOR [UserName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblSystemDates_SysdateDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblSystemDates] ADD  CONSTRAINT [DF_tblSystemDates_SysdateDateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblSystemStrings_StringValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblSystemStrings] ADD  CONSTRAINT [DF_tblSystemStrings_StringValue]  DEFAULT ('') FOR [StringValue]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTradeFileAcknowledgement_BrokerID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTradeFileAcknowledgement] ADD  CONSTRAINT [DF_tblTradeFileAcknowledgement_BrokerID]  DEFAULT ((0)) FOR [BrokerID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTradeFileAcknowledgement_TransactionParentID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTradeFileAcknowledgement] ADD  CONSTRAINT [DF_tblTradeFileAcknowledgement_TransactionParentID]  DEFAULT ((0)) FOR [TransactionParentID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTradeFileAcknowledgement_AcknowledgementText]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTradeFileAcknowledgement] ADD  CONSTRAINT [DF_tblTradeFileAcknowledgement_AcknowledgementText]  DEFAULT ('') FOR [AcknowledgementText]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTradeFileAcknowledgement_UserName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTradeFileAcknowledgement] ADD  CONSTRAINT [DF_tblTradeFileAcknowledgement_UserName]  DEFAULT (user_name()) FOR [UserName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTradeFileAcknowledgement_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTradeFileAcknowledgement] ADD  CONSTRAINT [DF_tblTradeFileAcknowledgement_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTradeFileConfirmations_BrokerID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTradeFileConfirmations] ADD  CONSTRAINT [DF_tblTradeFileConfirmations_BrokerID]  DEFAULT ((0)) FOR [BrokerID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTradeFileConfirmations_TransactionParentID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTradeFileConfirmations] ADD  CONSTRAINT [DF_tblTradeFileConfirmations_TransactionParentID]  DEFAULT ((0)) FOR [TransactionParentID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTradeFileConfirmations_ConfirmationText]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTradeFileConfirmations] ADD  CONSTRAINT [DF_tblTradeFileConfirmations_ConfirmationText]  DEFAULT ('') FOR [ConfirmationText]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTradeFileConfirmations_UserName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTradeFileConfirmations] ADD  CONSTRAINT [DF_tblTradeFileConfirmations_UserName]  DEFAULT (user_name()) FOR [UserName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTradeFileConfirmations_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTradeFileConfirmations] ADD  CONSTRAINT [DF_tblTradeFileConfirmations_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTradeFilePending_BrokerID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTradeFilePending] ADD  CONSTRAINT [DF_tblTradeFilePending_BrokerID]  DEFAULT ((0)) FOR [BrokerID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTradeFilePending_WorkflowID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTradeFilePending] ADD  CONSTRAINT [DF_tblTradeFilePending_WorkflowID]  DEFAULT ((0)) FOR [WorkflowID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTradeFilePending_TradefileText]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTradeFilePending] ADD  CONSTRAINT [DF_tblTradeFilePending_TradefileText]  DEFAULT ('') FOR [TradefileText]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTradeFilePending_TriggerDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTradeFilePending] ADD  CONSTRAINT [DF_tblTradeFilePending_TriggerDate]  DEFAULT ('1900-01-01') FOR [TriggerDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTradeFilePending_Comment]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTradeFilePending] ADD  CONSTRAINT [DF_tblTradeFilePending_Comment]  DEFAULT ('') FOR [Comment]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTradeFilePending_UserName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTradeFilePending] ADD  CONSTRAINT [DF_tblTradeFilePending_UserName]  DEFAULT (user_name()) FOR [UserName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTradeFilePending_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTradeFilePending] ADD  CONSTRAINT [DF_tblTradeFilePending_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTradeStatus_UserName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTradeStatus] ADD  CONSTRAINT [DF_tblTradeStatus_UserName]  DEFAULT (user_name()) FOR [UserName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTradeStatus_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTradeStatus] ADD  CONSTRAINT [DF_tblTradeStatus_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionTicket]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD  CONSTRAINT [DF_tblTransaction_TransactionTicket]  DEFAULT ('') FOR [TransactionTicket]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionFund]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD  CONSTRAINT [DF_tblTransaction_TransactionFund]  DEFAULT ((0)) FOR [TransactionFund]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionInstrument]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD  CONSTRAINT [DF_tblTransaction_TransactionInstrument]  DEFAULT ((0)) FOR [TransactionInstrument]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD  CONSTRAINT [DF_tblTransaction_TransactionType]  DEFAULT ((0)) FOR [TransactionType]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionType_Contra]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD  CONSTRAINT [DF_tblTransaction_TransactionType_Contra]  DEFAULT ((0)) FOR [TransactionType_Contra]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionValueorAmount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD  CONSTRAINT [DF_tblTransaction_TransactionValueorAmount]  DEFAULT ('Amount') FOR [TransactionValueorAmount]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionUnits]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD  CONSTRAINT [DF_tblTransaction_TransactionUnits]  DEFAULT ((0)) FOR [TransactionUnits]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionSignedUnits]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD  CONSTRAINT [DF_tblTransaction_TransactionSignedUnits]  DEFAULT ((0)) FOR [TransactionSignedUnits]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionPrice]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD  CONSTRAINT [DF_tblTransaction_TransactionPrice]  DEFAULT ((0)) FOR [TransactionPrice]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionCosts]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD  CONSTRAINT [DF_tblTransaction_TransactionCosts]  DEFAULT ((0)) FOR [TransactionCosts]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionCostPercent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD  CONSTRAINT [DF_tblTransaction_TransactionCostPercent]  DEFAULT ((0)) FOR [TransactionCostPercent]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionSettlement]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD  CONSTRAINT [DF_tblTransaction_TransactionSettlement]  DEFAULT ((0)) FOR [TransactionSettlement]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionSignedSettlement]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD  CONSTRAINT [DF_tblTransaction_TransactionSignedSettlement]  DEFAULT ((0)) FOR [TransactionSignedSettlement]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionFXRate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD  CONSTRAINT [DF_tblTransaction_TransactionFXRate]  DEFAULT ((1)) FOR [TransactionFXRate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionSettlementCurrencyID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD  CONSTRAINT [DF_tblTransaction_TransactionSettlementCurrencyID]  DEFAULT ((0)) FOR [TransactionSettlementCurrencyID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionEffectivePrice]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD  CONSTRAINT [DF_tblTransaction_TransactionEffectivePrice]  DEFAULT ((0)) FOR [TransactionEffectivePrice]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionEffectiveWatermark]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD  CONSTRAINT [DF_tblTransaction_TransactionEffectiveWatermark]  DEFAULT ((0)) FOR [TransactionEffectiveWatermark]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionSpecificInitialEqualisationValue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD  CONSTRAINT [DF_tblTransaction_TransactionSpecificInitialEqualisationValue]  DEFAULT ((0)) FOR [TransactionSpecificInitialEqualisationValue]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionNewCounterparty]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD  CONSTRAINT [DF_tblTransaction_TransactionNewCounterparty]  DEFAULT ((0)) FOR [TransactionCounterparty]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionInvestment]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD  CONSTRAINT [DF_tblTransaction_TransactionInvestment]  DEFAULT ('') FOR [TransactionInvestment]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionExecution]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD  CONSTRAINT [DF_tblTransaction_TransactionExecution]  DEFAULT ('') FOR [TransactionExecution]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionDataEntry]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD  CONSTRAINT [DF_tblTransaction_TransactionDataEntry]  DEFAULT ('') FOR [TransactionDataEntry]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionEntryDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD  CONSTRAINT [DF_tblTransaction_TransactionEntryDate]  DEFAULT (getdate()) FOR [TransactionEntryDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionCostIsPercent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD  CONSTRAINT [DF_tblTransaction_TransactionCostIsPercent]  DEFAULT ((0)) FOR [TransactionCostIsPercent]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionExemptFromUpdate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD  CONSTRAINT [DF_tblTransaction_TransactionExemptFromUpdate]  DEFAULT ((0)) FOR [TransactionExemptFromUpdate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionRedeemWholeHoldingFlag]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD  CONSTRAINT [DF_tblTransaction_TransactionRedeemWholeHoldingFlag]  DEFAULT ((0)) FOR [TransactionRedeemWholeHoldingFlag]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionIsTransfer]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD  CONSTRAINT [DF_tblTransaction_TransactionIsTransfer]  DEFAULT ((0)) FOR [TransactionIsTransfer]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionSpecificInitialEqualisationFlag]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD  CONSTRAINT [DF_tblTransaction_TransactionSpecificInitialEqualisationFlag]  DEFAULT ((0)) FOR [TransactionSpecificInitialEqualisationFlag]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionFuturesStylePricing]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD  CONSTRAINT [DF_tblTransaction_TransactionFuturesStylePricing]  DEFAULT ((0)) FOR [TransactionFuturesStylePricing]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionComment]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD  CONSTRAINT [DF_tblTransaction_TransactionComment]  DEFAULT ('') FOR [TransactionComment]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionRedemptionID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD  CONSTRAINT [DF_tblTransaction_TransactionRedemptionID]  DEFAULT ((0)) FOR [TransactionRedemptionID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionVersusInstrument]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD  CONSTRAINT [DF_tblTransaction_TransactionVersusInstrument]  DEFAULT ((0)) FOR [TransactionVersusInstrument]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionUser]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD  CONSTRAINT [DF_tblTransaction_TransactionUser]  DEFAULT (user_name()) FOR [TransactionUser]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionGroup]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD  CONSTRAINT [DF_tblTransaction_TransactionGroup]  DEFAULT ('') FOR [TransactionGroup]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionTradeStatus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD  CONSTRAINT [DF_tblTransaction_TransactionTradeStatus]  DEFAULT ((0)) FOR [TransactionTradeStatusID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionAdminStatus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD  CONSTRAINT [DF_tblTransaction_TransactionAdminStatus]  DEFAULT ((0)) FOR [TransactionAdminStatus]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionFLAGS]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD  CONSTRAINT [DF_tblTransaction_TransactionFLAGS]  DEFAULT ((0)) FOR [TransactionCTFLAGS]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionCompoundID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD  CONSTRAINT [DF_tblTransaction_TransactionCompoundID]  DEFAULT ((0)) FOR [TransactionCompoundRN]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransaction_TransactionVersion]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransaction] ADD  CONSTRAINT [DF_tblTransaction_TransactionVersion]  DEFAULT ((0)) FOR [TransactionVersion]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransactionType_TransactionTypeUser]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransactionType] ADD  CONSTRAINT [DF_tblTransactionType_TransactionTypeUser]  DEFAULT (user_name()) FOR [UserName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTransactionType_TransactionTypeDateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTransactionType] ADD  CONSTRAINT [DF_tblTransactionType_TransactionTypeDateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTriggerReferencePrices_ReferencePrice]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTriggerReferencePrices] ADD  CONSTRAINT [DF_tblTriggerReferencePrices_ReferencePrice]  DEFAULT ((0)) FOR [ReferencePrice]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblTriggerReferencePrices_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblTriggerReferencePrices] ADD  CONSTRAINT [DF_tblTriggerReferencePrices_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblUnitHolderFees_FXtoUSD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblUnitHolderFees] ADD  CONSTRAINT [DF_tblUnitHolderFees_FXtoUSD]  DEFAULT ((1)) FOR [FXtoUSD]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblUnitHolderFees_IsMilestone]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblUnitHolderFees] ADD  CONSTRAINT [DF_tblUnitHolderFees_IsMilestone]  DEFAULT ((0)) FOR [IsMilestone]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblUnitHolderFees_UserEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblUnitHolderFees] ADD  CONSTRAINT [DF_tblUnitHolderFees_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblUnitHolderFees_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblUnitHolderFees] ADD  CONSTRAINT [DF_tblUnitHolderFees_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblUserPermissions_ID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblUserPermissions] ADD  CONSTRAINT [DF_tblUserPermissions_ID]  DEFAULT ((0)) FOR [ID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblUserPermissions_AppName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblUserPermissions] ADD  CONSTRAINT [DF_tblUserPermissions_AppName]  DEFAULT (app_name()) FOR [AppName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHedgeOPPermissions_Perm_Read]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblUserPermissions] ADD  CONSTRAINT [DF_tblHedgeOPPermissions_Perm_Read]  DEFAULT ((0)) FOR [Perm_Read]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHedgeOPPermissions_Perm_Insert]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblUserPermissions] ADD  CONSTRAINT [DF_tblHedgeOPPermissions_Perm_Insert]  DEFAULT ((0)) FOR [Perm_Insert]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHedgeOPPermissions_Perm_Update]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblUserPermissions] ADD  CONSTRAINT [DF_tblHedgeOPPermissions_Perm_Update]  DEFAULT ((0)) FOR [Perm_Update]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblHedgeOPPermissions_Perm_Delete]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblUserPermissions] ADD  CONSTRAINT [DF_tblHedgeOPPermissions_Perm_Delete]  DEFAULT ((0)) FOR [Perm_Delete]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblUserPermissions_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblUserPermissions] ADD  CONSTRAINT [DF_tblUserPermissions_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblVeniceFormTooltips_APPName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblVeniceFormTooltips] ADD  CONSTRAINT [DF_tblVeniceFormTooltips_APPName]  DEFAULT (app_name()) FOR [AppName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblVeniceFormTooltips_FormName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblVeniceFormTooltips] ADD  CONSTRAINT [DF_tblVeniceFormTooltips_FormName]  DEFAULT ('') FOR [FormName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblVeniceFormTooltips_ControlNane]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblVeniceFormTooltips] ADD  CONSTRAINT [DF_tblVeniceFormTooltips_ControlNane]  DEFAULT ('') FOR [ControlName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblVeniceFormTooltips_ToolTip]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblVeniceFormTooltips] ADD  CONSTRAINT [DF_tblVeniceFormTooltips_ToolTip]  DEFAULT ('') FOR [ToolTip]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblVeniceFormTooltips_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblVeniceFormTooltips] ADD  CONSTRAINT [DF_tblVeniceFormTooltips_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblWorkflowRules_Fund]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblWorkflowRules] ADD  CONSTRAINT [DF_tblWorkflowRules_Fund]  DEFAULT ((0)) FOR [Fund]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblWorkflowRules_InstrumentType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblWorkflowRules] ADD  CONSTRAINT [DF_tblWorkflowRules_InstrumentType]  DEFAULT ((0)) FOR [InstrumentType]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblWorkflowRules_Exchange]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblWorkflowRules] ADD  CONSTRAINT [DF_tblWorkflowRules_Exchange]  DEFAULT ((0)) FOR [Exchange]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblWorkflowRules_Currency]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblWorkflowRules] ADD  CONSTRAINT [DF_tblWorkflowRules_Currency]  DEFAULT ((0)) FOR [Currency]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblWorkflowRules_InstrumentID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblWorkflowRules] ADD  CONSTRAINT [DF_tblWorkflowRules_InstrumentID]  DEFAULT ((0)) FOR [InstrumentID]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblWorkflowRules_CurrentStatus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblWorkflowRules] ADD  CONSTRAINT [DF_tblWorkflowRules_CurrentStatus]  DEFAULT ((0)) FOR [CurrentStatus]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblWorkflowRules_IsApprovalRule]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblWorkflowRules] ADD  CONSTRAINT [DF_tblWorkflowRules_IsApprovalRule]  DEFAULT ((1)) FOR [IsApprovalRule]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblWorkflowRules_IsNewTrade]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblWorkflowRules] ADD  CONSTRAINT [DF_tblWorkflowRules_IsNewTrade]  DEFAULT ((0)) FOR [IsNewTrade]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblWorkflowRules_ResultingWorkflow]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblWorkflowRules] ADD  CONSTRAINT [DF_tblWorkflowRules_ResultingWorkflow]  DEFAULT ((0)) FOR [ResultingWorkflow]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblWorkflowRules_RuleDescription]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblWorkflowRules] ADD  CONSTRAINT [DF_tblWorkflowRules_RuleDescription]  DEFAULT ('') FOR [RuleDescription]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblWorkflowRules_UserName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblWorkflowRules] ADD  CONSTRAINT [DF_tblWorkflowRules_UserName]  DEFAULT (user_name()) FOR [UserName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblWorkflowRules_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblWorkflowRules] ADD  CONSTRAINT [DF_tblWorkflowRules_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblWorkflows_WorkflowName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblWorkflows] ADD  CONSTRAINT [DF_tblWorkflows_WorkflowName]  DEFAULT ('') FOR [WorkflowName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblWorkflows_WorkflowDescription]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblWorkflows] ADD  CONSTRAINT [DF_tblWorkflows_WorkflowDescription]  DEFAULT ('') FOR [WorkflowDescription]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblWorkflows_WorkDetails]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblWorkflows] ADD  CONSTRAINT [DF_tblWorkflows_WorkDetails]  DEFAULT ((0)) FOR [WorkDetails]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblWorkflows_TRADEFILE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblWorkflows] ADD  CONSTRAINT [DF_tblWorkflows_TRADEFILE]  DEFAULT ('') FOR [CreateTradeFile]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblWorkflows_ResultingTradeStatus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblWorkflows] ADD  CONSTRAINT [DF_tblWorkflows_ResultingTradeStatus]  DEFAULT ((0)) FOR [ResultingTradeStatus]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblWorkflows_UserName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblWorkflows] ADD  CONSTRAINT [DF_tblWorkflows_UserName]  DEFAULT (user_name()) FOR [UserName]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tblWorkflows_DateEntered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tblWorkflows] ADD  CONSTRAINT [DF_tblWorkflows_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
END

GO
