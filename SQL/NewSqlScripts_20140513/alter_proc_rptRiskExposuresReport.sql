/*
---------------------
- Lotfi on 04-june- 2014 : fix of a bug in the rpt calculatiions caused by a wrong order by
  => new oerder by : ltpLimitType,ltpSortOrder,DataCategory,DataCharacteristic,InstrumentDescription,expLimitGreaterOrLessThan,expLimit,InstrumentDescriptionDetail

--------------------
*/
USE [Renaissance]
GO
/****** Object:  StoredProcedure [dbo].[rptRiskExposuresReport]    Script Date: 03/06/2014 18:19:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[rptRiskExposuresReport]
	(
	@FundID int, 
  @IsDetailRow int = 0,
	@ExposureDate datetime, 
	@ExposureLimitCategory int = 0, 
	@ExposureLimitInstrument int = 0, 
	@ExposureLimitMinUsage float = 0, 
	@ExposureFlags int = 0, 
	@ExposureKnowledgeDate datetime,
	@KnowledgeDate datetime
	)
AS
	SET NOCOUNT ON
	
	-- **************************************************
	-- CONSTANTS.
	--
	-- These values used to be looked-up by reference to their
	-- various descriptions, but since the descriptions are as
	-- liable to change as the IDs it seemed easier just to 
	-- consider these IDs as constant.
	-- By and Large, changes to these items are restricted so that
	-- Under any presently imagined scenario, these values ARE
	-- constant.
	-- **************************************************
		    
	DECLARE @SECTOR_Cash int
	DECLARE @SECTOR_CashFund int
		    
	SET @SECTOR_Cash = 3
	SET @SECTOR_CashFund = 24
	
  SET @IsDetailRow = ISNULL(@IsDetailRow, 0)

  DECLARE @ResultsTable TABLE
  (
  [RN] int,
  [AuditID] int, 
  [expID] int, 
  [expDate] date,
  [expFund] int,
  [expLimitID] int,
  [expLimitType] int,
  [expLimitCharacteristic] int,
  [expLimitInstrument] int,
  [expLimitSector] int,
  [expLimitSectorName] nvarchar(100),
  [expLimitDealingPeriod] int,
  [expLimitGreaterOrLessThan] int,
  [expLimit] float,
  [expDetailInstrument] int,
  [expExposure] float,
  [expUsage] float,
  [expStatus] nvarchar(50),
  [expDateCalculated] datetime,
  [expKnowledgeDate] datetime,
  [expKnowledgeDateWholeday] int,
  [expSystemKnowledgeDate] datetime,
  [expSystemKnowledgeDateWholeday] int,
  [expFlags] int,
  [ltpLimitType] nvarchar(50),
  [ltpSortOrder] int,
  [ltpLabel] nvarchar(50),
  [ltpLimitFormat] nvarchar(50),
  [FundName] nvarchar(100),
  [InstrumentDescription] nvarchar(100),
  [InstrumentDescriptionDetail] nvarchar(100),
  [DataCategoryId] int,
  [DataCategory] nvarchar(100),
  [DataCharacteristic] nvarchar(200)
  )

	-- **************************************************
	
  IF (@IsDetailRow = 0) OR (ISNULL(@ExposureLimitMinUsage, 0) <= 0)
  BEGIN
    INSERT @ResultsTable 
              (
              [RN] ,
              [AuditID] , 
              [expID] , 
              [expDate] ,
              [expFund] ,
              [expLimitID] ,
              [expLimitType] ,
              [expLimitCharacteristic] ,
              [expLimitInstrument] ,
              [expLimitSector] ,
              [expLimitSectorName] ,
              [expLimitDealingPeriod] ,
              [expLimitGreaterOrLessThan] ,
              [expLimit] ,
              [expDetailInstrument] ,
              [expExposure] ,
              [expUsage] ,
              [expStatus] ,
              [expDateCalculated] ,
              [expKnowledgeDate] ,
              [expKnowledgeDateWholeday] ,
              [expSystemKnowledgeDate] ,
              [expSystemKnowledgeDateWholeday] ,
              [expFlags] ,
              [ltpLimitType] ,
              [ltpSortOrder] ,
              [ltpLabel] ,
              [ltpLimitFormat] ,
              [FundName] ,
              [InstrumentDescription] ,
              [InstrumentDescriptionDetail] ,
              [DataCategoryId] ,
              [DataCategory] ,
              [DataCharacteristic]
              )
    SELECT    
              Exposures.RN ,
	            Exposures.AuditID ,
	            Exposures.expID ,
	            Exposures.expDate ,
	            Exposures.expFund ,
	            Exposures.expLimitID ,
	            Exposures.expLimitType ,
              ISNULL(Exposures.expLimitCharacteristic, 0) AS expLimitCharacteristic,
	            ISNULL(Exposures.expLimitInstrument, 0) AS expLimitInstrument ,
	            ISNULL(Exposures.expLimitSector, 0) AS expLimitSector ,
	            IIF(ISNULL(Exposures.expLimitCharacteristic, 0) = 0, Exposures.expLimitSectorName, ISNULL(Category.DataCategory, '')) AS expLimitSectorName ,
	            Exposures.expLimitDealingPeriod ,
	            Exposures.expLimitGreaterOrLessThan ,
	            Exposures.expLimit ,
	            Exposures.expDetailInstrument ,
	            Exposures.expExposure ,
	            Exposures.expUsage ,
	            Exposures.expStatus ,
	            Exposures.expDateCalculated ,
	            Exposures.expKnowledgeDate ,
	            Exposures.expKnowledgeDateWholeday ,
	            Exposures.expSystemKnowledgeDate ,
	            Exposures.expSystemKnowledgeDateWholeday,
	            Exposures.expFlags, 
              LimitTypes.ltpLimitType,
              LimitTypes.ltpSortOrder, 
              LimitTypes.ltpLabel, 
              LimitTypes.ltpLimitFormat, 
              ISNULL(Funds.FundName, '') AS FundName, 
              IIF(ISNULL(Exposures.expLimitCharacteristic, 0) = 0, ISNULL(Instruments.InstrumentDescription, ''), ISNULL(Characteristics.DataCharacteristic, '')) AS InstrumentDescription ,
              ISNULL(InstrumentsDetail.InstrumentDescription, '') AS InstrumentDescriptionDetail ,
              ISNULL(Characteristics.DataCategoryId, 0) AS DataCategoryId,
              ISNULL(Category.DataCategory, '') AS DataCategory,
              ISNULL(Characteristics.DataCharacteristic, '') AS DataCharacteristic
    FROM      fn_ExposureSelect(@FundID, @ExposureDate, @ExposureFlags, @ExposureKnowledgeDate, @KnowledgeDate) Exposures INNER JOIN
              fn_tblLimitType_SelectKD(@KnowledgeDate) LimitTypes ON Exposures.expLimitType = LimitTypes.ltpID LEFT OUTER JOIN
              fn_tblInstrument_SelectKD(@KnowledgeDate) Instruments ON Exposures.expLimitInstrument = Instruments.InstrumentID LEFT OUTER JOIN
              fn_tblInstrument_SelectKD(@KnowledgeDate) InstrumentsDetail ON Exposures.expDetailInstrument = InstrumentsDetail.InstrumentID LEFT OUTER JOIN
              fn_tblFund_SelectKD(@KnowledgeDate) Funds ON Exposures.expFund = Funds.FundID LEFT OUTER JOIN
              fn_tblRiskDataCharacteristic_SelectKD(@KnowledgeDate) Characteristics ON Exposures.expLimitCharacteristic = Characteristics.DataCharacteristicId  LEFT OUTER JOIN
              fn_tblRiskDataCategory_SelectKD(@KnowledgeDate) Category ON Characteristics.DataCategoryId = Category.DataCategoryId
-- 	WHERE     (((@IsDetailRow = 0) AND (Exposures.expIsDetailRow = 0)) OR ((@IsDetailRow <> 0) AND (Exposures.expIsDetailRow <> 0) AND (ABS(Exposures.expUsage) > 0))) AND 
	WHERE     (((@IsDetailRow = 0) AND (Exposures.expIsDetailRow = 0)) OR ((@IsDetailRow <> 0) AND (Exposures.expIsDetailRow <> 0))) AND 
              (ISNULL(@ExposureLimitCategory, 0) IN (Exposures.expLimitType, 0)) AND 
              (ISNULL(@ExposureLimitInstrument, 0) IN (Exposures.expLimitInstrument, 0)) AND 
--			  (((@IsDetailRow = 0) AND (Exposures.expUsage >= ISNULL(@ExposureLimitMinUsage, 0))) OR ((@IsDetailRow <> 0) AND (ABS(Exposures.expUsage) > 0))) AND 
			  (((@IsDetailRow = 0) AND (Exposures.expUsage >= ISNULL(@ExposureLimitMinUsage, 0))) OR ( @IsDetailRow <> 0)) 
              -- AND (NOT (Exposures.expLimitSector IN (@SECTOR_Cash, @SECTOR_CashFund)))
    ORDER BY LimitTypes.ltpSortOrder
  END
  ELSE
  BEGIN
    -- OK, for detail report, with a minimum risk exposure, we need to select those exposures that meet the given criteria and then select the related detail records.

    INSERT @ResultsTable 
              (
              [RN] ,
              [AuditID] , 
              [expID] , 
              [expDate] ,
              [expFund] ,
              [expLimitID] ,
              [expLimitType] ,
              [expLimitCharacteristic] ,
              [expLimitInstrument] ,
              [expLimitSector] ,
              [expLimitSectorName] ,
              [expLimitDealingPeriod] ,
              [expLimitGreaterOrLessThan] ,
              [expLimit] ,
              [expDetailInstrument] ,
              [expExposure] ,
              [expUsage] ,
              [expStatus] ,
              [expDateCalculated] ,
              [expKnowledgeDate] ,
              [expKnowledgeDateWholeday] ,
              [expSystemKnowledgeDate] ,
              [expSystemKnowledgeDateWholeday] ,
              [expFlags] ,
              [ltpLimitType] ,
              [ltpSortOrder] ,
              [ltpLabel] ,
              [ltpLimitFormat] ,
              [FundName] ,
              [InstrumentDescription] ,
              [InstrumentDescriptionDetail] ,
              [DataCategoryId] ,
              [DataCategory] ,
              [DataCharacteristic]
              )
    SELECT    
              Exposures.RN ,
	            Exposures.AuditID ,
	            Exposures.expID ,
	            Exposures.expDate ,
	            Exposures.expFund ,
	            Exposures.expLimitID ,
	            Exposures.expLimitType ,
              ISNULL(Exposures.expLimitCharacteristic, 0) AS expLimitCharacteristic,
	            ISNULL(Exposures.expLimitInstrument, 0) AS expLimitInstrument ,
	            ISNULL(Exposures.expLimitSector, 0) AS expLimitSector ,
	            IIF(ISNULL(Exposures.expLimitCharacteristic, 0) = 0, Exposures.expLimitSectorName, ISNULL(Category.DataCategory, '')) AS expLimitSectorName ,
	            Exposures.expLimitDealingPeriod ,
	            Exposures.expLimitGreaterOrLessThan ,
	            Exposures.expLimit ,
	            Exposures.expDetailInstrument ,
	            Exposures.expExposure ,
	            Exposures.expUsage ,
	            Exposures.expStatus ,
	            Exposures.expDateCalculated ,
	            Exposures.expKnowledgeDate ,
	            Exposures.expKnowledgeDateWholeday ,
	            Exposures.expSystemKnowledgeDate ,
	            Exposures.expSystemKnowledgeDateWholeday,
	            Exposures.expFlags, 
              LimitTypes.ltpLimitType,
              LimitTypes.ltpSortOrder, 
              LimitTypes.ltpLabel, 
              LimitTypes.ltpLimitFormat, 
              ISNULL(Funds.FundName, '') AS FundName, 
	            IIF(ISNULL(Exposures.expLimitCharacteristic, 0) = 0, ISNULL(Instruments.InstrumentDescription, ''), ISNULL(Characteristics.DataCharacteristic, '')) AS InstrumentDescription ,
	            ISNULL(InstrumentsDetail.InstrumentDescription, '') AS InstrumentDescriptionDetail ,
              ISNULL(Characteristics.DataCategoryId, 0) AS DataCategoryId,
              ISNULL(Category.DataCategory, '') AS DataCategory,
              ISNULL(Characteristics.DataCharacteristic, '') AS DataCharacteristic
    FROM      fn_ExposureSelect(@FundID, @ExposureDate, @ExposureFlags, @ExposureKnowledgeDate, @KnowledgeDate) SelectedExposures INNER JOIN
              fn_ExposureSelect(@FundID, @ExposureDate, @ExposureFlags, @ExposureKnowledgeDate, @KnowledgeDate) Exposures ON (SelectedExposures.expFund = Exposures.expFund) AND (SelectedExposures.expLimitID = Exposures.expLimitID) AND (SelectedExposures.expLimitInstrument = Exposures.expLimitInstrument) AND (SelectedExposures.expLimitSector = Exposures.expLimitSector) LEFT JOIN
              fn_tblLimitType_SelectKD(@KnowledgeDate) LimitTypes ON Exposures.expLimitType = LimitTypes.ltpID LEFT OUTER JOIN
              fn_tblInstrument_SelectKD(@KnowledgeDate) Instruments ON Exposures.expLimitInstrument = Instruments.InstrumentID LEFT OUTER JOIN
              fn_tblInstrument_SelectKD(@KnowledgeDate) InstrumentsDetail ON Exposures.expDetailInstrument = InstrumentsDetail.InstrumentID LEFT OUTER JOIN
              fn_tblFund_SelectKD(@KnowledgeDate) Funds ON Exposures.expFund = Funds.FundID LEFT OUTER JOIN
              fn_tblRiskDataCharacteristic_SelectKD(@KnowledgeDate) Characteristics ON Exposures.expLimitCharacteristic = Characteristics.DataCharacteristicId  LEFT OUTER JOIN
              fn_tblRiskDataCategory_SelectKD(@KnowledgeDate) Category ON Characteristics.DataCategoryId = Category.DataCategoryId
    WHERE     (SelectedExposures.expIsDetailRow = 0) AND ((Exposures.expIsDetailRow <> 0)) AND -- AND (ABS(Exposures.expUsage) > 0)) AND 
              (ISNULL(@ExposureLimitCategory, 0) IN (SelectedExposures.expLimitType, 0)) AND 
              (ISNULL(@ExposureLimitInstrument, 0) IN (SelectedExposures.expLimitInstrument, 0)) AND 
              (SelectedExposures.expUsage >= ISNULL(@ExposureLimitMinUsage, 0)) 
              -- AND (NOT (SelectedExposures.expLimitSector IN (@SECTOR_Cash, @SECTOR_CashFund)))
    ORDER BY LimitTypes.ltpSortOrder


  END

  IF (@IsDetailRow <> 0)
  BEGIN
    -- Ensure that there is a place holder item for each limit.

    INSERT @ResultsTable 
              (
              [RN] ,
              [AuditID] , 
              [expID] , 
              [expDate] ,
              [expFund] ,
              [expLimitID] ,
              [expLimitType] ,
              [expLimitCharacteristic] ,
              [expLimitInstrument] ,
              [expLimitSector] ,
              [expLimitSectorName] ,
              [expLimitDealingPeriod] ,
              [expLimitGreaterOrLessThan] ,
              [expLimit] ,
              [expDetailInstrument] ,
              [expExposure] ,
              [expUsage] ,
              [expStatus] ,
              [expDateCalculated] ,
              [expKnowledgeDate] ,
              [expKnowledgeDateWholeday] ,
              [expSystemKnowledgeDate] ,
              [expSystemKnowledgeDateWholeday] ,
              [expFlags] ,
              [ltpLimitType] ,
              [ltpSortOrder] ,
              [ltpLabel] ,
              [ltpLimitFormat] ,
              [FundName] ,
              [InstrumentDescription] ,
              [InstrumentDescriptionDetail] ,
              [DataCategoryId] ,
              [DataCategory] ,
              [DataCharacteristic]
              )
    SELECT    
              Exposures.RN ,
	            Exposures.AuditID ,
	            Exposures.expID ,
	            Exposures.expDate ,
	            Exposures.expFund ,
	            Exposures.expLimitID ,
	            Exposures.expLimitType ,
              ISNULL(Exposures.expLimitCharacteristic, 0) AS expLimitCharacteristic,
	            ISNULL(Exposures.expLimitInstrument, 0) AS expLimitInstrument ,
	            ISNULL(Exposures.expLimitSector, 0) AS expLimitSector ,
	            IIF(ISNULL(Exposures.expLimitCharacteristic, 0) = 0, Exposures.expLimitSectorName, ISNULL(Category.DataCategory, '')) AS expLimitSectorName ,
	            Exposures.expLimitDealingPeriod ,
	            Exposures.expLimitGreaterOrLessThan ,
	            Exposures.expLimit ,
	            Exposures.expDetailInstrument ,
	            Exposures.expExposure ,
	            Exposures.expUsage ,
	            Exposures.expStatus ,
	            Exposures.expDateCalculated ,
	            Exposures.expKnowledgeDate ,
	            Exposures.expKnowledgeDateWholeday ,
	            Exposures.expSystemKnowledgeDate ,
	            Exposures.expSystemKnowledgeDateWholeday,
	            Exposures.expFlags, 
              LimitTypes.ltpLimitType,
              LimitTypes.ltpSortOrder, 
              LimitTypes.ltpLabel, 
              LimitTypes.ltpLimitFormat, 
              ISNULL(Funds.FundName, '') AS FundName, 
	            IIF(ISNULL(Exposures.expLimitCharacteristic, 0) = 0, ISNULL(Instruments.InstrumentDescription, ''), ISNULL(Characteristics.DataCharacteristic, '')) AS InstrumentDescription ,
	            '' AS InstrumentDescriptionDetail ,
              ISNULL(Characteristics.DataCategoryId, 0) AS DataCategoryId,
              ISNULL(Category.DataCategory, '') AS DataCategory,
              ISNULL(Characteristics.DataCharacteristic, '') AS DataCharacteristic
    FROM      fn_ExposureSelect(@FundID, @ExposureDate, @ExposureFlags, @ExposureKnowledgeDate, @KnowledgeDate) Exposures INNER JOIN
              fn_tblLimitType_SelectKD(@KnowledgeDate) LimitTypes ON Exposures.expLimitType = LimitTypes.ltpID LEFT OUTER JOIN
              fn_tblInstrument_SelectKD(@KnowledgeDate) Instruments ON Exposures.expLimitInstrument = Instruments.InstrumentID LEFT OUTER JOIN
              fn_tblFund_SelectKD(@KnowledgeDate) Funds ON Exposures.expFund = Funds.FundID LEFT OUTER JOIN
              fn_tblRiskDataCharacteristic_SelectKD(@KnowledgeDate) Characteristics ON Exposures.expLimitCharacteristic = Characteristics.DataCharacteristicId  LEFT OUTER JOIN
              fn_tblRiskDataCategory_SelectKD(@KnowledgeDate) Category ON Characteristics.DataCategoryId = Category.DataCategoryId
    WHERE     (Exposures.expLimitType IN (15,16)) AND /* Currency Limit and Characteristic Limits only */
							(Exposures.expIsDetailRow = 0) AND 
              (NOT Exposures.expLimitID IN (SELECT DISTINCT expLimitID FROM @ResultsTable )) AND 
              (ISNULL(@ExposureLimitCategory, 0) IN (Exposures.expLimitType, 0)) AND 
              (ISNULL(@ExposureLimitInstrument, 0) IN (Exposures.expLimitInstrument, 0)) AND 
              (Exposures.expUsage >= ISNULL(@ExposureLimitMinUsage, 0))
              -- AND (NOT (Exposures.expLimitSector IN (@SECTOR_Cash, @SECTOR_CashFund)))
    ORDER BY LimitTypes.ltpSortOrder


  END

  SELECT *
  FROM @ResultsTable
  ORDER BY
  -- fix of a bug in the rpt calculatiions caused by a wrong order by
  --ltpSortOrder
ltpLimitType,ltpSortOrder,DataCategory,DataCharacteristic,InstrumentDescription,expLimitGreaterOrLessThan,expLimit,InstrumentDescriptionDetail
	RETURN -1

