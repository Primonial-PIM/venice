USE [Renaissance]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_rptManagementFeesByPeriod]    Script Date: 12/06/2014 16:47:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
-- =============================================
	Nicholas Pennington
	Dec 2013

	Function to return Fund management and performance fees.
	Fees are reported in the base currency of their respective funds, FX'd at year end or Now() whichever is earlier.
-- =============================================
*/
alter FUNCTION [dbo].[fn_rptManagementFeesByPeriod]
(
	@FundID int = 0,
	@StartDate date = '1900-01-01', 
	@EndDate date = GETDATE,
	@AccountingReport int = 0,
	@Period [varchar](20) = 'D',
	@StatusGroupFilter nvarchar(50) = '',
	@AdministratorDatesFilter int = 0, 
	@ReportingCurrencyID int = 3, -- EUR
  @KnowledgeDate datetime
)
RETURNS 
	@tblManagementFeesByMonth TABLE
		(
		FundID [int],
		UnitID [int],
		FeeDate date,
		FeeYear [int],
		FeeMonth [int],
		ManagementFee [float],
		PerformanceFee [float],
		CrystalisedFee [float],
		MovementCommission [float] DEFAULT(0),
		ReportCurrency [varchar](5), 
		CarriedOver [int],
		FundClosed [int],
		FundName [nvarchar](100),
		FundReportGroup [nvarchar](50), 
		FundYearStart [date],
		FundYearEnd [date], 
		SampleValueDate [Date], 
		FundValue [float] DEFAULT(0)
		)
AS
BEGIN
	-- DECLARE @StartDate date = CONVERT(date, CONVERT(varchar(4), @Year) + '-01-01')
	-- DECLARE @EndDate date = CONVERT(date, CONVERT(varchar(4), @Year + 1) + '-01-01')
	DECLARE @Year int = 0

  SET @EndDate = ISNULL(@EndDate, GETDATE())
	SET @AccountingReport = ISNULL(@AccountingReport, 0)
	SET @Period = LEFT(UPPER(ISNULL(@Period, 'D')), 1)

	DECLARE @FXValueDate datetime = @EndDate
	IF (@FXValueDate > GETDATE())
	BEGIN
		SET @FXValueDate = GETDATE()
	END

	DECLARE @FundDates TABLE
			(
			FundID [int] Primary Key,
			FundYearStart date, 
			FundYearEnd date
			)

	IF (@AccountingReport = 0)
	BEGIN
		INSERT @FundDates(FundID, FundYearStart, FundYearEnd)
		SELECT FundID, DATEADD(day, -1, @StartDate), @EndDate  
		FROM fn_tblFund_SelectKD(@KnowledgeDate)
	END
	ELSE
	BEGIN
		SET @Year = YEAR(@EndDate)

		INSERT @FundDates(FundID, FundYearStart, FundYearEnd)
		SELECT FundID, FundYearEnd, FundYearEnd 
		FROM fn_tblFund_SelectKD(@KnowledgeDate)

		-- Set Year for Fund Financial year dates
		Update @FundDates
		SET FundYearStart = DATEADD(year, (@Year-1) - YEAR(FundYearStart), FundYearStart),  FundYearEnd = DATEADD(year, (@Year) - YEAR(FundYearStart), FundYearEnd)
		WHERE FundYearStart IS NOT NULL

		-- To get to the last day of the month (Esp Feb in leap years)
		-- Add a month, then take off the days

		-- UPDATE @tblManagementFeesByMonth
		-- SET FundValue = FundValues.FundValue
		-- FROM @tblManagementFeesByMonth Fees LEFT JOIN
		-- @FundValue FundValues ON (Fees.FundID = FundValues.FundID)

		Update @FundDates
		SET FundYearStart = DATEADD(month, 1, FundYearStart),  FundYearEnd = DATEADD(month, 1, FundYearEnd)
		WHERE FundYearStart IS NOT NULL

		Update @FundDates
		SET FundYearStart = DATEADD(day, -DAY(FundYearStart), FundYearStart),  FundYearEnd = DATEADD(day, -DAY(FundYearEnd), FundYearEnd)
		WHERE FundYearStart IS NOT NULL

	END

	DECLARE @CrystalisedFeeInstruments TABLE
		(
		FundID int,
		CrystalisedInstrumentID int,
		PRIMARY KEY (FundID, CrystalisedInstrumentID)
		)

	DECLARE @FeeInstrument TABLE
		(
		InstrumentID int PRIMARY KEY,
		InstrumentCurrencyID int, 
		InstrumentIsManagementFee bit, 
		InstrumentIsIncentiveFee bit, 
		InstrumentIsCrystalisedFee bit,
		InstrumentIsMovementCommission bit
		)

	DECLARE @FundValue TABLE
		(
		FundID int PRIMARY KEY,
		FundValue float
		)

	-- INSERT @FundValue(FundID, FundValue)
	-- SELECT Fund, SUM(EURValue)
	-- FROM fn_Valuation(@FundID, Null, @FXValueDate, 0, @StatusGroupFilter, @AdministratorDatesFilter, 1, Null, Null, Null, @KnowledgeDate)
	-- GROUP BY Fund 

	-- Transaction Status information.

	DECLARE @SELECT_BY_TRANSACTION_STATUS int = 0
	DECLARE @TransactionStatusList TABLE (TradeStatusID int PRIMARY KEY)

	IF (ISNULL(@StatusGroupFilter, '') <> '') 
	BEGIN
		SET @SELECT_BY_TRANSACTION_STATUS = 1

		INSERT @TransactionStatusList(TradeStatusID)
		SELECT DISTINCT [STATUS_ID]
		FROM fn_tblSophisStatusGroups_SelectedGroup(0, @StatusGroupFilter, @KnowledgeDate)
	END

	-- Get Crystalised fee instruments

	INSERT @CrystalisedFeeInstruments(FundID, CrystalisedInstrumentID)
	SELECT DISTINCT FundID, FundPerformanceFeeCrystalisedInstrument 
	FROM fn_tblFund_SelectKD(@KnowledgeDate)

	-- Get Fee Instruments
	-- Ensure that Instruments marked as being the Fund Fee Crystalisation Instruments are included and marked correctly.

	INSERT @FeeInstrument(InstrumentID, InstrumentCurrencyID, InstrumentIsManagementFee, InstrumentIsIncentiveFee, InstrumentIsCrystalisedFee, InstrumentIsMovementCommission)
	SELECT DISTINCT InstrumentID, InstrumentCurrencyID, InstrumentIsManagementFee, IIF(Cryst.CrystalisedInstrumentID IS NOT NULL, 1, InstrumentIsIncentiveFee), IIF(Cryst.CrystalisedInstrumentID IS NOT NULL, 1, 0), InstrumentIsMovementCommission 
	FROM fn_tblInstrument_SelectKD(@KnowledgeDate) Inst
		LEFT JOIN @CrystalisedFeeInstruments Cryst ON (Inst.InstrumentID = Cryst.CrystalisedInstrumentID)
	WHERE (Inst.InstrumentIsIncentiveFee <> 0 OR Inst.InstrumentIsManagementFee <> 0 OR Inst.InstrumentIsMovementCommission <> 0) OR (Cryst.CrystalisedInstrumentID IS NOT NULL)

	-- Get Data

	INSERT @tblManagementFeesByMonth(FundID, UnitID, FeeDate, FeeYear, FeeMonth, ManagementFee, PerformanceFee, CrystalisedFee, MovementCommission, ReportCurrency, FundName, FundReportGroup, FundYearStart, FundYearEnd, FundClosed, SampleValueDate, CarriedOver)
	SELECT 
		TransactionFund, 
		Trans.TransactionSpecificToFundUnitID,
		CONVERT(date, IIF(@Period = 'Y',CONVERT(varchar(4), YEAR(Trans.TransactionValueDate))+'-01-01', IIF(@Period = 'M', CONVERT(varchar(4), YEAR(Trans.TransactionValueDate))+'-'+CONVERT(varchar(2), MONTH(Trans.TransactionValueDate))+'-01', Trans.TransactionValueDate))),
		0, 
		0, 
		Sum(IIF(InstrumentIsManagementFee <> 0, TransactionSignedSettlement  * (ISNULL(InstrumentFX.FXRate, 1.0)/ISNULL(ReportingFX.FXRate, 1.0)), 0)) AS ManagementFee, 
		Sum(IIF((InstrumentIsIncentiveFee <> 0) AND (InstrumentIsCrystalisedFee = 0), TransactionSignedSettlement  * (ISNULL(InstrumentFX.FXRate, 1.0)/ISNULL(ReportingFX.FXRate, 1.0)), 0)) AS IncentiveFee, 
		Sum(IIF(InstrumentIsCrystalisedFee <> 0, TransactionSignedSettlement  * (ISNULL(InstrumentFX.FXRate, 1.0)/ISNULL(ReportingFX.FXRate, 1.0)), 0)) AS CrystalisedFee, 
		Sum(IIF(InstrumentIsMovementCommission <> 0, TransactionSignedSettlement  * (ISNULL(InstrumentFX.FXRate, 1.0)/ISNULL(ReportingFX.FXRate, 1.0)), 0)) AS MovementCommission, 
		MAX(ReportingCcy.CurrencyCode), 
		MAX(Fund.FundName), 
		MAX(Fund.FundReportGroup), 
		MAX(FundDate.FundYearStart),
		MAX(FundDate.FundYearEnd), 
		MAX(Fund.FundClosed), 
		CONVERT(date, MIN(TransactionValueDate)), 
		0
	FROM fn_tblTransaction_SelectKD(@Knowledgedate) Trans INNER JOIN
		@FeeInstrument FeeInst ON (Trans.TransactionInstrument = FeeInst.InstrumentID) LEFT JOIN
		fn_tblFund_SelectKD(@KnowledgeDate) Fund ON Trans.TransactionFund = Fund.FundID LEFT JOIN
		@FundDates FundDate ON (Fund.FundID = FundDate.FundID) LEFT JOIN
		fn_BestFX(@FXValueDate, @KnowledgeDate) ReportingFX ON @ReportingCurrencyID = ReportingFX.FXCurrencyCode LEFT JOIN
		fn_tblCurrency_SelectKD(@KnowledgeDate) ReportingCcy ON (ReportingCcy.CurrencyID = @ReportingCurrencyID) LEFT JOIN
		fn_BestFX(@FXValueDate,@KnowledgeDate) InstrumentFX ON FeeInst.InstrumentCurrencyID = InstrumentFX.FXCurrencyCode
	WHERE 
		((TransactionFund = @FundID) OR (@FundID = 0)) AND 
		(TransactionValueDate > FundDate.FundYearStart) AND 
		(TransactionValueDate <= @EndDate) AND
		(@Year = 0 OR ((YEAR(TransactionValueDate) >= (@Year-1)) AND (YEAR(TransactionValueDate) <= @Year))) AND 
		((@SELECT_BY_TRANSACTION_STATUS = 0) OR (Trans.TransactionTradeStatusID = 0) OR (Trans.TransactionTradeStatusID IN (SELECT TradeStatusID FROM @TransactionStatusList)))
	GROUP BY TransactionFund, TransactionSpecificToFundUnitID, CONVERT(date, IIF(@Period = 'Y',CONVERT(varchar(4), YEAR(Trans.TransactionValueDate))+'-01-01', IIF(@Period = 'M', CONVERT(varchar(4), YEAR(Trans.TransactionValueDate))+'-'+CONVERT(varchar(2), MONTH(Trans.TransactionValueDate))+'-01', Trans.TransactionValueDate)))
	ORDER BY TransactionFund, TransactionSpecificToFundUnitID, CONVERT(date, IIF(@Period = 'Y',CONVERT(varchar(4), YEAR(Trans.TransactionValueDate))+'-01-01', IIF(@Period = 'M', CONVERT(varchar(4), YEAR(Trans.TransactionValueDate))+'-'+CONVERT(varchar(2), MONTH(Trans.TransactionValueDate))+'-01', Trans.TransactionValueDate)))
	
	UPDATE @tblManagementFeesByMonth
	SET FeeYear = YEAR(FeeDate), FeeMonth = MONTH(FeeDate)

  Update @tblManagementFeesByMonth
  SET CarriedOver = 1, ManagementFee = 0
  WHERE FeeYear < @Year

  Update @tblManagementFeesByMonth
  SET CarriedOver = -1
  WHERE SampleValueDate > FundYearEnd

-- ignore perf and crystallized fees for some funds which FundYearEnd has changed during the year
-- modif commented  after Anthony has fixed the Excel report itself
	--declare @fund int
	--declare @fundYearDateEnd date
	
	-- arrange perf and crys fees for  flex equilbre fund
	--set @fund=12548 
	--set @fundYearDateEnd=convert(date,'2014-03-31')

	--if (@Year=2014 and @EndDate> @fundYearDateEnd)
	--begin
	--  Update @tblManagementFeesByMonth
	--	SET PerformanceFee=0,CrystalisedFee=0
	--	WHERE FundId = @fund and FeeDate <= @fundYearDateEnd		
	--end

	-- To be done for the flex dynamique : wait for the go from Hugues
	---- arrange perf and crys fees for  flex equilbre dynamique
	--set @fund=12549 
	--set @fundYearDateEnd=convert(date,'2014-06-30')

	--if (@Year=2014 and @EndDate> @fundYearDateEnd)
	--begin
	--  Update @tblManagementFeesByMonth
	--	SET PerformanceFee=0,CrystalisedFee=0
	--	WHERE FundId = @fund and FeeDate <= @fundYearDateEnd		
	--end

	-- to be completed and tested  (try to make it general )
	--  Update @tblManagementFeesByMonth
	--	SET PerformanceFee=0,CrystalisedFee=0
	--	WHERE FundId in (12548,12549)
	--	and FeeDate <= FundYearEnd
	--	and @EndDate>FundYearEnd



	RETURN 
END
