USE [Renaissance]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_SingleInstrumentBestNAV]    Script Date: 04/08/2014 13:28:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create  procedure [dbo].[spu_GetSingleInstrumentBestNAVandDate]
	(
	@InstrumentID int,
	@ValueDate datetime,
	@OnlyFinalPrices int, 
	@KnowledgeDate datetime
	)
	
-- A Scalar Valued function to return the best NAV for a specific Instrument on a Specific Day	
	
-- 15 Oct 2008 NPP

AS
BEGIN
	DECLARE @ReturnPrice as float
	DECLARE @MaxPriceDate as datetime 
	
	SELECT Top 1 P.PriceNAV,p.PriceDate,it.InstrumentTypeID,it.InstrumentTypeDescription
	FROM tblPrice p
	left join fn_tblInstrument_SelectKD(@KnowledgeDate) i on (p.PriceInstrument=i.InstrumentID)
	left join fn_tblInstrumentType_SelectKD(@KnowledgeDate) it on (i.InstrumentType=it.InstrumentTypeID)

	WHERE (p.PriceInstrument = ISNULL(@InstrumentID, (-1))) AND 
		  ((p.PriceDate <= ISNULL(@ValueDate, '1900-01-01')) OR (ISNULL(@ValueDate, '1900-01-01') = '1900-01-01')) AND 
		  ((ISNULL(PriceFinal, 0) <> 0) OR (ISNULL(p.PriceIsMilestone, 0) <> 0) OR (ISNULL(@OnlyFinalPrices, 0) = 0)) AND 
          (((p.DateEntered <= @KnowledgeDate) or (p.DateEntered IS NULL)) OR (ISNULL(@KnowledgeDate, '1900-01-01') <= '1900-01-01')) AND 
          (((p.DateDeleted > @KnowledgeDate) or (p.DateDeleted IS NULL)) AND (NOT ((ISNULL(@KnowledgeDate, '1900-01-01') <= '1900-01-01') AND (NOT (p.DateDeleted is NULL)))))
	ORDER BY p.PriceDate Desc, p.RN Desc
	
END









