USE [Renaissance]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Add AveragePrice column

alter table tblTradeFileEmsxStatus add AveragePrice float null
GO
ALTER TABLE [dbo].[tblTradeFileEmsxStatus] ADD  CONSTRAINT [DF_tblTradeFileEmsxStatus_AveragePrice]  DEFAULT ((0)) FOR [AveragePrice]
GO

print 'AveragePrice column added'
/*-------------------*/
-- alter function fn_tblTradeFileEmsxStatus_SelectKD 
/*--------------------*/

USE [Renaissance]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_tblTradeFileEmsxStatus_SelectKD]    Script Date: 26/05/2014 15:16:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER  FUNCTION [dbo].[fn_tblTradeFileEmsxStatus_SelectKD]
(
@KnowledgeDate datetime
)
RETURNS @tblTradeFileEmsxStatus TABLE
(
	[RN] [int] NOT NULL ,
	[AuditID] [int] NOT NULL,
	[TransactionParentID] [int] NOT NULL,
	[SequenceNumber] [bigint] NOT NULL,
	[OrderStatus] [int] NOT NULL,
	[CorrelationID] [int] NOT NULL,
	[StatusString] [nvarchar](500) NOT NULL,
	[BrokerID] [int] NOT NULL,
	[EMSX_BrokerID] [nvarchar](50) NOT NULL,
	[VeniceAccountID] [int] NOT NULL,
	[EMSX_Ticker] [nvarchar](50) NOT NULL,
	[RouteIDs] [varchar](500) NULL,
	[OrderAmount] [float] NOT NULL,
	[FilledAmount] [float] NOT NULL,
	[AveragePrice] [float] NULL,
	[Cancelled] [bit],
	[DateEntered] [datetime] NULL
	PRIMARY KEY ([TransactionParentID])
)
AS
BEGIN

  IF (ISNULL(@KnowledgeDate, '1900-01-01') <= '1900-01-01')
  BEGIN
    -- LIVE

    INSERT @tblTradeFileEmsxStatus
	    (
	    RN ,
	    AuditID ,
      [TransactionParentID],
      [SequenceNumber],
      [OrderStatus],
      [CorrelationID],
      [StatusString],
			[BrokerID],
      [EMSX_BrokerID],
			[VeniceAccountID],
      [EMSX_Ticker],
      [RouteIDs],
      [OrderAmount],
      [FilledAmount],
	  [AveragePrice],
			[Cancelled],
      [DateEntered]
	    )
    SELECT 	
	    RN ,
	    AuditID ,
      [TransactionParentID],
      [SequenceNumber],
      [OrderStatus],
      [CorrelationID],
      [StatusString],
			[BrokerID],
      [EMSX_BrokerID],
			[VeniceAccountID],
      [EMSX_Ticker],
      [RouteIDs],
      [OrderAmount],
      [FilledAmount],
	  [AveragePrice],
			[Cancelled],
      [DateEntered]
    FROM [tblTradeFileEmsxStatus]
    WHERE RN IN ( SELECT MAX(RN) 
                  FROM [tblTradeFileEmsxStatus] 
                  GROUP BY [TransactionParentID])
    AND (DateDeleted IS NULL)

  END
  ELSE
  BEGIN
     -- KnowledgeDated

    INSERT @tblTradeFileEmsxStatus
	    (
	    RN ,
	    AuditID ,
      [TransactionParentID],
      [SequenceNumber],
      [OrderStatus],
      [CorrelationID],
      [StatusString],
			[BrokerID],
      [EMSX_BrokerID],
			[VeniceAccountID],
      [EMSX_Ticker],
      [RouteIDs],
      [OrderAmount],
      [FilledAmount],
	  [AveragePrice],
			[Cancelled],
      [DateEntered]
	    )
    SELECT 	
	    RN ,
	    AuditID ,
      [TransactionParentID],
      [SequenceNumber],
      [OrderStatus],
      [CorrelationID],
      [StatusString],
			[BrokerID],
      [EMSX_BrokerID],
			[VeniceAccountID],
      [EMSX_Ticker],
      [RouteIDs],
      [OrderAmount],
      [FilledAmount],
	  [AveragePrice],
			[Cancelled],
      [DateEntered]
    FROM [tblTradeFileEmsxStatus]
    WHERE RN IN ( SELECT MAX(RN) 
                  FROM [tblTradeFileEmsxStatus] 
                  WHERE DateEntered <= @KnowledgeDate
                  GROUP BY [TransactionParentID])
    AND ((DateDeleted IS NULL) OR (DateDeleted > @KnowledgeDate))

  END

RETURN
END

GO
print '[fn_tblTradeFileEmsxStatus_SelectKD] altered'

/*-------------------*/
-- alter function fn_tblTradeFileEmsxStatus_SelectSingle 
/*--------------------*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER  FUNCTION [dbo].[fn_tblTradeFileEmsxStatus_SelectSingle]
(
@TransactionParentID [int] ,
@KnowledgeDate datetime
)
RETURNS @tblTradeFileEmsxStatus TABLE
(
	[RN] [int] NOT NULL ,
	[AuditID] [int] NOT NULL,
	[TransactionParentID] [int] NOT NULL,
	[SequenceNumber] [bigint] NOT NULL,
	[OrderStatus] [int] NOT NULL,
	[CorrelationID] [int] NOT NULL,
	[StatusString] [nvarchar](500) NOT NULL,
	[BrokerID] [int] NOT NULL,
	[EMSX_BrokerID] [nvarchar](50) NOT NULL,
	[VeniceAccountID] [int] NOT NULL,
	[EMSX_Ticker] [nvarchar](50) NOT NULL,
	[RouteIDs] [varchar](500) NULL,
	[OrderAmount] [float] NOT NULL,
	[FilledAmount] [float] NOT NULL,
	[AveragePrice] [float] NULL,
	[Cancelled] [bit],
	[DateEntered] [datetime] NULL
	PRIMARY KEY ([TransactionParentID])
)
AS
BEGIN

  IF (ISNULL(@KnowledgeDate, '1900-01-01') <= '1900-01-01')
  BEGIN
    -- LIVE

    INSERT @tblTradeFileEmsxStatus
	    (
      RN ,
      AuditID ,
      [TransactionParentID],
      [SequenceNumber],
      [OrderStatus],
      [CorrelationID],
      [StatusString],
			[BrokerID],
      [EMSX_BrokerID],
			[VeniceAccountID],
      [EMSX_Ticker],
      [RouteIDs],
      [OrderAmount],
      [FilledAmount],
	  [AveragePrice],
			[Cancelled],
      [DateEntered]
	    )
    SELECT 	
	    RN ,
	    AuditID ,
      [TransactionParentID],
      [SequenceNumber],
      [OrderStatus],
      [CorrelationID],
      [StatusString],
			[BrokerID],
      [EMSX_BrokerID],
			[VeniceAccountID],
      [EMSX_Ticker],
      [RouteIDs],
      [OrderAmount],
      [FilledAmount],
	  [AveragePrice],
			[Cancelled],
      [DateEntered]
    FROM [tblTradeFileEmsxStatus]
    WHERE RN IN ( SELECT MAX(RN) 
                  FROM [tblTradeFileEmsxStatus] 
                  WHERE (@TransactionParentID = 0) OR ([TransactionParentID] =  @TransactionParentID)
                  GROUP BY [TransactionParentID])
    AND (DateDeleted IS NULL)

  END
  ELSE
  BEGIN
     -- KnowledgeDated

    INSERT @tblTradeFileEmsxStatus
	    (
	    RN ,
	    AuditID ,
      [TransactionParentID],
      [SequenceNumber],
      [OrderStatus],
      [CorrelationID],
      [StatusString],
			[BrokerID],
      [EMSX_BrokerID],
			[VeniceAccountID],
      [EMSX_Ticker],
      [RouteIDs],
      [OrderAmount],
      [FilledAmount],
	  [AveragePrice],
			[Cancelled],
      [DateEntered]
	    )
    SELECT 	
	    RN ,
	    AuditID ,
      [TransactionParentID],
      [SequenceNumber],
      [OrderStatus],
      [CorrelationID],
      [StatusString],
			[BrokerID],
      [EMSX_BrokerID],
			[VeniceAccountID],
      [EMSX_Ticker],
      [RouteIDs],
      [OrderAmount],
      [FilledAmount],
	  [AveragePrice],
			[Cancelled],
      [DateEntered]
    FROM [tblTradeFileEmsxStatus]
    WHERE RN IN ( SELECT MAX(RN) 
                  FROM [tblTradeFileEmsxStatus] 
                  WHERE (DateEntered <= @KnowledgeDate) AND ((@TransactionParentID = 0) OR ([TransactionParentID] =  @TransactionParentID))
                  GROUP BY [TransactionParentID])
    AND ((DateDeleted IS NULL) OR (DateDeleted > @KnowledgeDate))

  END

RETURN
END

GO
print '[fn_tblTradeFileEmsxStatus_SelectSingle] altered'

/*-------------------------------------------------------------*/
-- Alter adp_tblTradeFileEmsxStatus_SelectForApproveTransactions to select the AveragePrice
/*-------------------------------------------------------------*/
/****** Object:  StoredProcedure [dbo].[adp_tblTradeFileEmsxStatus_SelectForApproveTransactions]    Script Date: 26/05/2014 15:01:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[adp_tblTradeFileEmsxStatus_SelectForApproveTransactions]
(
@KnowledgeDate datetime = Null
)
AS
begin
-- Returns entries with an OrderStatus of <Not Completed> AND <Not Deleted>
	SET NOCOUNT ON;

	DECLARE @EMSX_OrderStatus_Completed [int] = 80
	DECLARE @EMSX_OrderStatus_Deleted [int] = 1000
	DECLARE @EMSX_OrderStatus_Error [int] = (-10)
	DECLARE @EMSX_OrderStatus_Cancel_RoutesPendingResponse      [int] = 100 
	DECLARE @EMSX_OrderStatus_Cancel_RoutesPendingConfirmation  [int] = 110   
	DECLARE @EMSX_OrderStatus_Cancel_RoutesConfirmed            [int] = 120   
	DECLARE @EMSX_OrderStatus_Cancel_Route_Unexpected           [int] = 121  
	DECLARE @EMSX_OrderStatus_Cancel_OrderPendingConfirmation   [int] = 130   
	DECLARE @EMSX_OrderStatus_Cancel_OrderConfirmed             [int] = 140 

SELECT	
	    RN ,
	    AuditID ,
      [TransactionParentID],
      [SequenceNumber],
      [OrderStatus],
      [CorrelationID],
      [StatusString],
			[BrokerID],
      [EMSX_BrokerID],
			[VeniceAccountID],
      [EMSX_Ticker],
      [RouteIDs],
      [OrderAmount],
      [FilledAmount],
	  [AveragePrice],
	  			[Cancelled],
			IIF(@EMSX_OrderStatus_Completed = [OrderStatus] OR @EMSX_OrderStatus_Deleted = [OrderStatus], 1, 0) AS Sort, 
      [DateEntered],
			CONVERT([date], [DateEntered]) AS DatePartOnly
FROM [fn_tblTradeFileEmsxStatus_SelectKD](@KnowledgeDate)
WHERE 
  ((([OrderStatus] <> @EMSX_OrderStatus_Completed) 
  AND ([OrderStatus] <> @EMSX_OrderStatus_Deleted)
  AND ([OrderStatus] <> @EMSX_OrderStatus_Cancel_RoutesPendingConfirmation) 
  AND ([DateEntered] > DATEADD(day, -7, CONVERT(date, GETDATE())))) OR 
 	 ([DateEntered] >= [dbo].[fn_AddBusinessDays](CONVERT(date, GETDATE()), -1, 0))) --  DATEADD(day, -3, CONVERT(date, GETDATE()))

end

go

print '[adp_tblTradeFileEmsxStatus_SelectForApproveTransactions] altered'

/*-----------------------*/
/****** Object:  StoredProcedure [dbo].[adp_tblTradeFileEmsxStatus_InsertCommand]    Script Date: 26/05/2014 16:43:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[adp_tblTradeFileEmsxStatus_InsertCommand]
	(
	@TransactionParentID [int] ,
	@SequenceNumber [bigint] ,
	@OrderStatus [int] ,
	@CorrelationID [int] ,
	@StatusString [nvarchar](500) ,
	@BrokerID [int] = 0,
	@EMSX_BrokerID [nvarchar](50) ,
	@VeniceAccountID [int] = 0,
	@EMSX_Ticker [nvarchar](50) ,
	@RouteIDs [varchar](500) ,
  @OrderAmount [float] = 0,
  @FilledAmount [float] = 0,
  @AveragePrice [float] = 0,
	@Cancelled [bit] = 0,
	@KnowledgeDate datetime
	)
AS

begin	SET NOCOUNT OFF

	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
	
	BEGIN TRANSACTION
	
	DECLARE @Count [int] = 0

  -- For this table, Don't allow identical updates.
  -- It's just a waste....

  SELECT @Count = Count(*)
  FROM [fn_tblTradeFileEmsxStatus_SelectSingle](@TransactionParentID, '1900-01-01')
  WHERE 
    SequenceNumber = @SequenceNumber AND
    OrderStatus = @OrderStatus AND 
    CorrelationID = @CorrelationID AND
    StatusString = @StatusString AND 
    BrokerID = @BrokerID AND 
    EMSX_BrokerID = @EMSX_BrokerID AND 
		VeniceAccountID = @VeniceAccountID AND
    EMSX_Ticker = @EMSX_Ticker AND 
    OrderAmount = @OrderAmount AND 
    FilledAmount = @FilledAmount AND 
	AveragePrice = @AveragePrice AND
		Cancelled= @Cancelled AND 
    RouteIDs = @RouteIDs


	-- If OK, then insert new record.

  IF @Count = 0
  BEGIN
	  INSERT INTO tblTradeFileEmsxStatus
      (
        [TransactionParentID],
        [SequenceNumber],
        [OrderStatus],
        [CorrelationID],
        [StatusString],
        [BrokerID],
        [EMSX_BrokerID],
				[VeniceAccountID],
        [EMSX_Ticker],
        [RouteIDs],
        [OrderAmount],
        [FilledAmount],
		[AveragePrice],
				[Cancelled]
		  )
	  VALUES 
      (
		    @TransactionParentID,
        @SequenceNumber,
        @OrderStatus,
        @CorrelationID,
        @StatusString,
				ISNULL(@BrokerID, 0),
        @EMSX_BrokerID,
				ISNULL(@VeniceAccountID, 0),
        @EMSX_Ticker,
        @RouteIDs,
        @OrderAmount,
        @FilledAmount,
		@AveragePrice,
				ISNULL(@Cancelled, 0)
		  )
	END

	SELECT 
	    RN ,
	    AuditID ,
      [TransactionParentID],
      [SequenceNumber],
      [OrderStatus],
      [CorrelationID],
      [StatusString],
      [BrokerID],
      [EMSX_BrokerID],
			[VeniceAccountID],
      [EMSX_Ticker],
      [RouteIDs],
      [OrderAmount],
      [FilledAmount],
	  [AveragePrice],
			[Cancelled],
      [DateEntered]
  FROM [fn_tblTradeFileEmsxStatus_SelectSingle](@TransactionParentID, @KnowledgeDate)
	WHERE ([TransactionParentID] = @TransactionParentID)

	-- Return value and Commit / Rollback transaction accordingly.
	
	IF @@ERROR = 0
	  BEGIN
	    COMMIT TRANSACTION
	    RETURN @TransactionParentID
	  END
	ELSE
	  BEGIN
	    ROLLBACK TRANSACTION
	    RETURN 0
	  END

end
go
print '[adp_tblTradeFileEmsxStatus_InsertCommand] altered'

/*-----------------------*/
/****** Object:  StoredProcedure [dbo].[adp_tblTradeFileEmsxStatus_UpdateCommand]    Script Date: 26/05/2014 16:50:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[adp_tblTradeFileEmsxStatus_UpdateCommand]
	(
	@TransactionParentID [int] ,
	@SequenceNumber [bigint] ,
	@OrderStatus [int] ,
	@CorrelationID [int] ,
	@StatusString [nvarchar](500) ,
	@BrokerID [int] = 0,
	@EMSX_BrokerID [nvarchar](50) ,
	@VeniceAccountID [int] = 0,
	@EMSX_Ticker [nvarchar](50) ,
	@RouteIDs [varchar](500) ,
  @OrderAmount [float] = 0,
  @FilledAmount [float] = 0,
  @AveragePrice [float] = 0,
	@Cancelled [bit] = 0,
	@KnowledgeDate datetime
	)
AS
begin
	SET NOCOUNT OFF;

	DECLARE @RVal int 

	IF (@Knowledgedate >= (getdate())) OR (ISNULL(@Knowledgedate, '1900-01-01') <= '1900-01-01')
	  BEGIN
		EXECUTE @RVal  =  [dbo].[adp_tblTradeFileEmsxStatus_InsertCommand] 
			@TransactionParentID = @TransactionParentID,
			@SequenceNumber = @SequenceNumber,
			@OrderStatus = @OrderStatus,
			@CorrelationID = @CorrelationID,
			@StatusString = @StatusString,
			@BrokerID = @BrokerID,
			@EMSX_BrokerID = @EMSX_BrokerID,
			@VeniceAccountID = @VeniceAccountID,
			@EMSX_Ticker = @EMSX_Ticker,
      @RouteIDs = @RouteIDs,
      @OrderAmount = @OrderAmount,
      @FilledAmount = @FilledAmount,
	  @AveragePrice = @AveragePrice,
			@Cancelled = @Cancelled,
			@KnowledgeDate  =  @KnowledgeDate
			
		RETURN @RVal
	  END
	ELSE
	  BEGIN
		SELECT	
 	    RN ,
	    AuditID ,
      [TransactionParentID],
      [SequenceNumber],
      [OrderStatus],
      [CorrelationID],
      [StatusString],
			[BrokerID],
      [EMSX_BrokerID],
			[VeniceAccountID],
      [EMSX_Ticker],
      [RouteIDs],
      [OrderAmount],
      [FilledAmount],
	  [AveragePrice],
			[Cancelled],
      [DateEntered]

		FROM [fn_tblTradeFileEmsxStatus_SelectSingle](@TransactionParentID, @KnowledgeDate)
	  WHERE ([TransactionParentID] = @TransactionParentID)
		
	  END

end
go
print '[adp_tblTradeFileEmsxStatus_UpdateCommand] altered'

/*-----------------------*/
/****** Object:  StoredProcedure [dbo].[adp_tblTradeFileEmsxStatus_DeleteCommand]    Script Date: 26/05/2014 16:42:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[adp_tblTradeFileEmsxStatus_DeleteCommand]
(
	@TransactionParentID int = 0
)
AS
begin
	SET NOCOUNT OFF;

	INSERT tblTradeFileEmsxStatus
    (
      [TransactionParentID],
      [SequenceNumber],
      [OrderStatus],
      [CorrelationID],
      [StatusString],
      [BrokerID],
      [EMSX_BrokerID],
			[VeniceAccountID],
      [EMSX_Ticker],
      [RouteIDs],
      [OrderAmount],
      [FilledAmount],
	  [AveragePrice],
			[Cancelled],
      DateDeleted
	  )
  SELECT 	
      [TransactionParentID],
      [SequenceNumber],
      [OrderStatus],
      [CorrelationID],
      [StatusString],
      [BrokerID],
      [EMSX_BrokerID],
			[VeniceAccountID],
      [EMSX_Ticker],
      [RouteIDs],
      [OrderAmount],
      [FilledAmount],
	  [AveragePrice],
			[Cancelled],
      getdate()
  FROM  [fn_tblTradeFileEmsxStatus_SelectSingle](@TransactionParentID, Null)
  WHERE ([TransactionParentID] = @TransactionParentID)
 
	IF @@ERROR = 0
	  BEGIN
	    RETURN -1
	  END
	ELSE
	  BEGIN
	    RETURN 0
	  END

	RETURN
end

go

print '[adp_tblTradeFileEmsxStatus_DeleteCommand] altered'

/*-------------------------*/
/****** Object:  StoredProcedure [dbo].[adp_tblTradeFileEmsxStatus_SelectActive]    Script Date: 26/05/2014 16:45:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[adp_tblTradeFileEmsxStatus_SelectActive]
(
@AuditID int = 0,
@KnowledgeDate datetime = Null
)
AS
begin
-- Returns entries with an OrderStatus of <Not Completed> AND <Not Deleted>
	SET NOCOUNT ON;

	DECLARE @TransactionParentID int = 0
	SELECT @TransactionParentID = ISNULL(@AuditID, 0)

  DECLARE @EMSX_OrderStatus_Completed [int] = 80
  DECLARE @EMSX_OrderStatus_Deleted [int] = 1000
  DECLARE @EMSX_OrderStatus_Error [int] = (-10)

SELECT	
	    RN ,
	    AuditID ,
      [TransactionParentID],
      [SequenceNumber],
      [OrderStatus],
      [CorrelationID],
      [StatusString],
			[BrokerID],
      [EMSX_BrokerID],
			[VeniceAccountID],
      [EMSX_Ticker],
      [RouteIDs],
      [OrderAmount],
      [FilledAmount],
	  [AveragePrice],
			[Cancelled],
      [DateEntered]
FROM [fn_tblTradeFileEmsxStatus_SelectKD](@KnowledgeDate)
WHERE ((@TransactionParentID = 0) OR ([TransactionParentID] = @TransactionParentID)) AND
  ([OrderStatus] <> @EMSX_OrderStatus_Completed) AND 
  ([OrderStatus] <> @EMSX_OrderStatus_Deleted)

end
go

print '[adp_tblTradeFileEmsxStatus_SelectActive] altered'

/*-----------------------*/
/****** Object:  StoredProcedure [dbo].[adp_tblTradeFileEmsxStatus_SelectCommand]    Script Date: 26/05/2014 16:46:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[adp_tblTradeFileEmsxStatus_SelectCommand]
(
@AuditID int = 0,
@KnowledgeDate datetime = Null
)
AS
begin
	SET NOCOUNT ON;

	DECLARE @TransactionParentID int = 0
	SELECT @TransactionParentID = ISNULL(@AuditID, 0)

SELECT	
	    RN ,
	    AuditID ,
      [TransactionParentID],
      [SequenceNumber],
      [OrderStatus],
      [CorrelationID],
      [StatusString],
			[BrokerID],
      [EMSX_BrokerID],
			[VeniceAccountID],
      [EMSX_Ticker],
      [RouteIDs],
      [OrderAmount],
      [FilledAmount],
	  [AveragePrice],
			[Cancelled],
      [DateEntered]
FROM [fn_tblTradeFileEmsxStatus_SelectKD](@KnowledgeDate)
	WHERE (@TransactionParentID = 0) OR ([TransactionParentID] = @TransactionParentID)

end
go
print '[adp_tblTradeFileEmsxStatus_SelectCommand] altered'
