USE [Renaissance]
GO

INSERT INTO [dbo].[tblLimitType]
           ([ltpID]
           ,[ltpLimitType]
           ,[ltpLabel]
           ,[ltpLimitFormat]
           ,[ltpSortOrder]
           ,[ltpDescription]
           ,[DateEntered]
           ,[DateDeleted])
     VALUES
           (17
           ,'Ratio 5-10-40'
           ,'%'
           ,'#,##0.00'
           ,5
           ,'Checkes three instrument weights limits : less than 10%, less than 5% and sum of buigger than 5% is less than 40%'
           ,getDate()
           ,NULL)
GO