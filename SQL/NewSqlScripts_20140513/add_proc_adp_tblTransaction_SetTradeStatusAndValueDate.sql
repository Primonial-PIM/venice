USE [Renaissance]
GO
/****** Object:  StoredProcedure [dbo].[adp_tblTransaction_SetTradeStatus]    Script Date: 04/08/2014 19:22:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create PROCEDURE [dbo].[adp_tblTransaction_SetTradeStatusAndValueDate]
	(
	@TransactionParentID int = 0,
  @TransactionTradeStatusID int = 0,      
  @SetToZero int = 0, 
  @ConfirmedTrade int = 0,
  @ConfirmationDate date = null,
	@Comment varchar (500) = '',
	@KnowledgeDate datetime,
    @newValueDate date,
    @newSettlementDate date

	)
  --**************************************************************************************************
  -- Purpose: Procedure to quickly change the trade status of an existing transaction
  --
  --
  -- Accepts:
  --
  --
  --**************************************************************************************************
AS
	SET NOCOUNT OFF
	

	INSERT INTO tblTransaction(
		TransactionTicket, 
		TransactionFund, 
		TransactionSubFund,
		TransactionInstrument,  
		TransactionType, 
		TransactionType_Contra, 
		TransactionValueorAmount, 
		TransactionUnits, 
		TransactionSignedUnits, 
		TransactionPrice, 
		TransactionCleanPrice ,
		TransactionAccruedInterest ,
		TransactionBrokerFees ,
		TransactionCosts, 
		TransactionCostPercent, 
		TransactionSettlement, 
		TransactionSignedSettlement,
		TransactionFXRate, 
		TransactionSettlementCurrencyID,
		TransactionEffectivePrice , 
		TransactionEffectiveWatermark , 
		TransactionSpecificInitialEqualisationValue ,
		TransactionCounterparty, 
		TransactionBroker, 
		TransactionBrokerStrategy,
		TransactionInvestment, 
		TransactionExecution,  
		TransactionDataEntry, 
		TransactionDecisionDate, 
		TransactionValueDate, 
		TransactionFIFOValueDate, 
		TransactionEffectiveValueDate , 
		TransactionSettlementDate,  
		TransactionConfirmationDate, 
		TransactionCostIsPercent, 
		TransactionExemptFromUpdate, 
		TransactionRedeemWholeHoldingFlag, 
		TransactionIsTransfer , 
		TransactionSpecificInitialEqualisationFlag , 
		TransactionFuturesStylePricing , 
		TransactionClosingTrade, 
		TransactionIsFX, 
		TransactionIsForwardPayment,
		TransactionFinalAmount, 
		TransactionFinalPrice, 
		TransactionFinalCosts,  
		TransactionFinalSettlement, 
		TransactionInstructionFlag, 
		TransactionBankConfirmation, 
		TransactionInitialDataEntry, 
		TransactionAmountConfirmed,  
		TransactionSettlementConfirmed, 
		TransactionFinalDataEntry, 
		TransactionComment, 
		TransactionParentID, 
		TransactionRedemptionID, 
		TransactionVersusInstrument, 
		TransactionFeeID,
		TransactionSpecificToFundUnitID, 
		TransactionGroup, 
    TransactionTradeStatusID, 
		TransactionLeg, 
		TransactionAdminStatus, 
		TransactionCompoundRN, 
		TransactionCTFLAGS,
    TransactionVersion
		)
  SELECT
		TransactionTicket, 
		TransactionFund, 
		TransactionSubFund,
		TransactionInstrument,  
		TransactionType, 
		TransactionType_Contra, 
		TransactionValueorAmount, 
		IIF(ISNULL(@SetToZero, 0) = 0, TransactionUnits, 0), 
		IIF(ISNULL(@SetToZero, 0) = 0, TransactionSignedUnits, 0), 
		TransactionPrice, 
		TransactionCleanPrice,
		IIF(ISNULL(@SetToZero, 0) = 0, TransactionAccruedInterest, 0),
		IIF(ISNULL(@SetToZero, 0) = 0, TransactionBrokerFees, 0),
		IIF(ISNULL(@SetToZero, 0) = 0, TransactionCosts, 0), 
		TransactionCostPercent, 
		IIF(ISNULL(@SetToZero, 0) = 0, TransactionSettlement, 0), 
		IIF(ISNULL(@SetToZero, 0) = 0, TransactionSignedSettlement, 0), 
		TransactionFXRate, 
		TransactionSettlementCurrencyID,
		TransactionEffectivePrice , 
		TransactionEffectiveWatermark , 
		TransactionSpecificInitialEqualisationValue ,
		TransactionCounterparty, 
		TransactionBroker, 
		TransactionBrokerStrategy,
		TransactionInvestment, 
		TransactionExecution,  
		TransactionDataEntry, 
		TransactionDecisionDate, 

		--TransactionValueDate, 
		--TransactionFIFOValueDate, 
		--TransactionEffectiveValueDate ,
		-- TransactionSettlementDate,  
		IIF(ISNULL(@newValueDate, '1900-01-01') <= '1900-01-01', TransactionValueDate, @newValueDate), 
		IIF(ISNULL(@newValueDate, '1900-01-01') <= '1900-01-01', TransactionFIFOValueDate, @newValueDate), 
		IIF(ISNULL(@newValueDate, '1900-01-01') <= '1900-01-01', TransactionEffectiveValueDate, @newValueDate), 
		IIF(ISNULL(@newSettlementDate, '1900-01-01') <= '1900-01-01', TransactionSettlementDate, @newSettlementDate), 

		IIF(ISNULL(@ConfirmationDate, '1900-01-01') <= '1900-01-01', TransactionConfirmationDate, @ConfirmationDate), 
		TransactionCostIsPercent, 
		TransactionExemptFromUpdate, 
		TransactionRedeemWholeHoldingFlag, 
		TransactionIsTransfer , 
		TransactionSpecificInitialEqualisationFlag , 
		TransactionFuturesStylePricing , 
		TransactionClosingTrade, 
		TransactionIsFX, 
		TransactionIsForwardPayment,
		IIF(ISNULL(@ConfirmedTrade, 0) = 0, TransactionFinalAmount, 1), 
		IIF(ISNULL(@ConfirmedTrade, 0) = 0, TransactionFinalPrice, 1), 
		IIF(ISNULL(@ConfirmedTrade, 0) = 0, TransactionFinalCosts, 1), 
		IIF(ISNULL(@ConfirmedTrade, 0) = 0, TransactionFinalSettlement, 1), 
		IIF(ISNULL(@ConfirmedTrade, 0) = 0, TransactionInstructionFlag, 1), 
		IIF(ISNULL(@ConfirmedTrade, 0) = 0, TransactionBankConfirmation, 1), 
		IIF(ISNULL(@ConfirmedTrade, 0) = 0, TransactionInitialDataEntry, 1), 
		IIF(ISNULL(@ConfirmedTrade, 0) = 0, TransactionAmountConfirmed, 1), 
		IIF(ISNULL(@ConfirmedTrade, 0) = 0, TransactionSettlementConfirmed, 1), 
		IIF(ISNULL(@ConfirmedTrade, 0) = 0, TransactionFinalDataEntry, 1), 
    LEFT(ISNULL(TransactionComment, '') + IIF(LEN(ISNULL(@Comment, '')) > 0, ' : ' + @Comment, ''), 500),
		TransactionParentID, 
		TransactionRedemptionID, 
		TransactionVersusInstrument, 
		TransactionFeeID,
		TransactionSpecificToFundUnitID, 
		TransactionGroup, 
    ISNULL(@TransactionTradeStatusID, 0), 
		TransactionLeg, 
		TransactionAdminStatus, 
		TransactionCompoundRN, 
		TransactionCTFLAGS,
    TransactionVersion
  FROM tblTransaction
  WHERE RN IN ( SELECT MAX(RN) 
                FROM tblTransaction 
                WHERE (TransactionParentID = @TransactionParentID) AND
                      (((TransactionDateEntered <= @KnowledgeDate) or (TransactionDateEntered IS NULL)) OR (ISNULL(@KnowledgeDate, '1900-01-01') <= '1900-01-01')) AND 
                      (((TransactionDateDeleted > @KnowledgeDate) or (TransactionDateDeleted IS NULL)) AND (NOT ((ISNULL(@KnowledgeDate, '1900-01-01')  <= '1900-01-01') AND (NOT (TransactionDateDeleted is NULL)))))
                GROUP BY TransactionParentID, TransactionLeg )

	RETURN 0
	
