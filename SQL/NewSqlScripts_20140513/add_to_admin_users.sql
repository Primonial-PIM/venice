USE [Renaissance]
GO

INSERT INTO [dbo].[tblUserPermissions]
           ([ID]
           ,[AppName]
           ,[Username]
           ,[UserType]
           ,[Feature]
           ,[FeatureType]
           ,[Perm_Read]
           ,[Perm_Insert]
           ,[Perm_Update]
           ,[Perm_Delete]
           ,[UserEntered]
           ,[DateEntered]
           ,[DateDeleted])
     VALUES
           (0
           ,'Venice'
           ,'louedern'
           ,1
           ,'Admin_Users'
           ,10
           ,1
		   ,0
           ,0
		   ,0
		   ,'dbo'
           ,getdate()
           ,NULL)
GO
