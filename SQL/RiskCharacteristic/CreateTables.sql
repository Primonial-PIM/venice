USE [Renaissance]
GO

/****** Object:  Table [dbo].[tblRiskDataCategory]    Script Date: 10/31/2012 11:51:37 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblRiskDataCategory](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([DataCategoryId]),
	[DataCategoryId] [int] NOT NULL,
	[DataCategory] [nvarchar](100) NOT NULL,
	[UserEntered] [nvarchar](100) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblRiskDataCategory] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tblRiskDataCategory] ADD  CONSTRAINT [DF_tblRiskDataCategory_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
GO

ALTER TABLE [dbo].[tblRiskDataCategory] ADD  CONSTRAINT [DF_tblRiskDataCategory_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
GO

/****** Object:  Table [dbo].[tblRiskDataCharacteristic]    Script Date: 10/31/2012 11:51:37 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblRiskDataCharacteristic](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([DataCharacteristicId]),
	[DataCharacteristicId] [int] NOT NULL,
	[DataCharacteristic] [nvarchar](100) NOT NULL,
	[UserEntered] [nvarchar](100) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblRiskDataCharacteristic] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tblRiskDataCharacteristic] ADD  CONSTRAINT [DF_tblRiskDataCharacteristic_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
GO

ALTER TABLE [dbo].[tblRiskDataCharacteristic] ADD  CONSTRAINT [DF_tblRiskDataCharacteristic_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
GO

/****** Object:  Table [dbo].[tblRiskData]    Script Date: 10/31/2012 11:51:37 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblRiskData](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([DataCharacteristicId]),
	[DataCategoryId] [int] NOT NULL,
	[DataCharacteristicId] [int] NOT NULL,
  [DataWeighting] [float] NOT NULL, 
	[UserEntered] [nvarchar](100) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_tblRiskData] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tblRiskData] ADD  CONSTRAINT [DF_tblRiskData_DataWeighting]  DEFAULT (0) FOR [DataWeighting]
GO

ALTER TABLE [dbo].[tblRiskData] ADD  CONSTRAINT [DF_tblRiskData_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
GO

ALTER TABLE [dbo].[tblRiskData] ADD  CONSTRAINT [DF_tblRiskData_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
GO



