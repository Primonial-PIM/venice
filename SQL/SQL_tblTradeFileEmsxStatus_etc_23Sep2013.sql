USE [Renaissance]
GO
ALTER TABLE [dbo].[tblTradeFileEmsxStatus] DROP CONSTRAINT [DF_tblTradeFileEmsxStatus_DateEntered]
GO
ALTER TABLE [dbo].[tblTradeFileEmsxStatus] DROP CONSTRAINT [DF_tblTradeFileEmsxStatus_UserEntered]
GO
ALTER TABLE [dbo].[tblTradeFileEmsxStatus] DROP CONSTRAINT [DF_tblTradeFileEmsxStatus_EMSX_Ticker]
GO
ALTER TABLE [dbo].[tblTradeFileEmsxStatus] DROP CONSTRAINT [DF_tblTradeFileEmsxStatus_EMSX_BrokerID]
GO
ALTER TABLE [dbo].[tblTradeFileEmsxStatus] DROP CONSTRAINT [DF_tblTradeFileEmsxStatus_StatusString]
GO
ALTER TABLE [dbo].[tblTradeFileEmsxStatus] DROP CONSTRAINT [DF_tblTradeFileEmsxStatus_CorrelationID]
GO
ALTER TABLE [dbo].[tblTradeFileEmsxStatus] DROP CONSTRAINT [DF_tblTradeFileEmsxStatus_OrderStatus]
GO
ALTER TABLE [dbo].[tblTradeFileEmsxStatus] DROP CONSTRAINT [DF_tblTradeFileEmsxStatus_SequenceNumber]
GO
ALTER TABLE [dbo].[tblTradeFileEmsxStatus] DROP CONSTRAINT [DF_tblTradeFileEmsxStatus_TransactionParentID]
GO
/****** Object:  Table [dbo].[tblTradeFileEmsxStatus]    Script Date: 9/23/2013 12:28:07 PM ******/
DROP TABLE [dbo].[tblTradeFileEmsxStatus]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_tblTradeFileEmsxStatus_SelectSingle]    Script Date: 9/23/2013 12:28:07 PM ******/
DROP FUNCTION [dbo].[fn_tblTradeFileEmsxStatus_SelectSingle]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_tblTradeFileEmsxStatus_SelectKD]    Script Date: 9/23/2013 12:28:07 PM ******/
DROP FUNCTION [dbo].[fn_tblTradeFileEmsxStatus_SelectKD]
GO
/****** Object:  StoredProcedure [dbo].[adp_tblTradeFileEmsxStatus_UpdateCommand]    Script Date: 9/23/2013 12:28:07 PM ******/
DROP PROCEDURE [dbo].[adp_tblTradeFileEmsxStatus_UpdateCommand]
GO
/****** Object:  StoredProcedure [dbo].[adp_tblTradeFileEmsxStatus_SelectCommand]    Script Date: 9/23/2013 12:28:07 PM ******/
DROP PROCEDURE [dbo].[adp_tblTradeFileEmsxStatus_SelectCommand]
GO
/****** Object:  StoredProcedure [dbo].[adp_tblTradeFileEmsxStatus_InsertCommand]    Script Date: 9/23/2013 12:28:07 PM ******/
DROP PROCEDURE [dbo].[adp_tblTradeFileEmsxStatus_InsertCommand]
GO
/****** Object:  StoredProcedure [dbo].[adp_tblTradeFileEmsxStatus_DeleteCommand]    Script Date: 9/23/2013 12:28:07 PM ******/
DROP PROCEDURE [dbo].[adp_tblTradeFileEmsxStatus_DeleteCommand]
GO
/****** Object:  Table [dbo].[tblTradeFileEmsxStatus]    Script Date: 9/23/2013 12:28:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblTradeFileEmsxStatus](
	[RN] [int] IDENTITY(1,1) NOT NULL,
	[AuditID]  AS ([TransactionParentID]),
	[TransactionParentID] [int] NOT NULL,
	[SequenceNumber] [bigint] NOT NULL,
	[OrderStatus] [int] NOT NULL,
	[CorrelationID] [int] NOT NULL,
	[StatusString] [nvarchar](500) NOT NULL,
	[EMSX_BrokerID] [nvarchar](50) NOT NULL,
	[EMSX_Ticker] [nvarchar](50) NOT NULL,
	[RouteIDs] [varchar](500) NULL,
	[UserEntered] [nvarchar](50) NOT NULL,
	[DateEntered] [date] NOT NULL,
	[DateDeleted] [date] NULL,
 CONSTRAINT [PK_tblTradeFileEmsxStatus] PRIMARY KEY CLUSTERED 
(
	[RN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tblTradeFileEmsxStatus] ADD  CONSTRAINT [DF_tblTradeFileEmsxStatus_TransactionParentID]  DEFAULT ((0)) FOR [TransactionParentID]
GO
ALTER TABLE [dbo].[tblTradeFileEmsxStatus] ADD  CONSTRAINT [DF_tblTradeFileEmsxStatus_SequenceNumber]  DEFAULT ((0)) FOR [SequenceNumber]
GO
ALTER TABLE [dbo].[tblTradeFileEmsxStatus] ADD  CONSTRAINT [DF_tblTradeFileEmsxStatus_OrderStatus]  DEFAULT ((0)) FOR [OrderStatus]
GO
ALTER TABLE [dbo].[tblTradeFileEmsxStatus] ADD  CONSTRAINT [DF_tblTradeFileEmsxStatus_CorrelationID]  DEFAULT ((0)) FOR [CorrelationID]
GO
ALTER TABLE [dbo].[tblTradeFileEmsxStatus] ADD  CONSTRAINT [DF_tblTradeFileEmsxStatus_StatusString]  DEFAULT ('') FOR [StatusString]
GO
ALTER TABLE [dbo].[tblTradeFileEmsxStatus] ADD  CONSTRAINT [DF_tblTradeFileEmsxStatus_EMSX_BrokerID]  DEFAULT ('') FOR [EMSX_BrokerID]
GO
ALTER TABLE [dbo].[tblTradeFileEmsxStatus] ADD  CONSTRAINT [DF_tblTradeFileEmsxStatus_EMSX_Ticker]  DEFAULT ('') FOR [EMSX_Ticker]
GO
ALTER TABLE [dbo].[tblTradeFileEmsxStatus] ADD  CONSTRAINT [DF_tblTradeFileEmsxStatus_UserEntered]  DEFAULT (user_name()) FOR [UserEntered]
GO
ALTER TABLE [dbo].[tblTradeFileEmsxStatus] ADD  CONSTRAINT [DF_tblTradeFileEmsxStatus_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
GO

/****** Object:  UserDefinedFunction [dbo].[fn_tblTradeFileEmsxStatus_SelectKD]    Script Date: 9/23/2013 12:28:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE  FUNCTION [dbo].[fn_tblTradeFileEmsxStatus_SelectKD]
(
@KnowledgeDate datetime
)
RETURNS @tblTradeFileEmsxStatus TABLE
(
	[RN] [int] NOT NULL ,
	[AuditID] [int] NOT NULL,
	[TransactionParentID] [int] NOT NULL,
	[SequenceNumber] [bigint] NOT NULL,
	[OrderStatus] [int] NOT NULL,
	[CorrelationID] [int] NOT NULL,
	[StatusString] [nvarchar](500) NOT NULL,
	[EMSX_BrokerID] [nvarchar](50) NOT NULL,
	[EMSX_Ticker] [nvarchar](50) NOT NULL,
	[RouteIDs] [varchar](500) NULL,
	[DateEntered] [datetime] NULL
	PRIMARY KEY ([TransactionParentID])
)
AS
BEGIN

  IF (ISNULL(@KnowledgeDate, '1900-01-01') <= '1900-01-01')
  BEGIN
    -- LIVE

    INSERT @tblTradeFileEmsxStatus
	    (
	    RN ,
	    AuditID ,
      [TransactionParentID],
      [SequenceNumber],
      [OrderStatus],
      [CorrelationID],
      [StatusString],
      [EMSX_BrokerID],
      [EMSX_Ticker],
      [RouteIDs],
      [DateEntered]
	    )
    SELECT 	
	    RN ,
	    AuditID ,
      [TransactionParentID],
      [SequenceNumber],
      [OrderStatus],
      [CorrelationID],
      [StatusString],
      [EMSX_BrokerID],
      [EMSX_Ticker],
      [RouteIDs],
      [DateEntered]
    FROM [tblTradeFileEmsxStatus]
    WHERE RN IN ( SELECT MAX(RN) 
                  FROM [tblTradeFileEmsxStatus] 
                  GROUP BY [TransactionParentID])
    AND (DateDeleted IS NULL)

  END
  ELSE
  BEGIN
     -- KnowledgeDated

    INSERT @tblTradeFileEmsxStatus
	    (
	    RN ,
	    AuditID ,
      [TransactionParentID],
      [SequenceNumber],
      [OrderStatus],
      [CorrelationID],
      [StatusString],
      [EMSX_BrokerID],
      [EMSX_Ticker],
      [RouteIDs],
      [DateEntered]
	    )
    SELECT 	
	    RN ,
	    AuditID ,
      [TransactionParentID],
      [SequenceNumber],
      [OrderStatus],
      [CorrelationID],
      [StatusString],
      [EMSX_BrokerID],
      [EMSX_Ticker],
      [RouteIDs],
      [DateEntered]
    FROM [tblTradeFileEmsxStatus]
    WHERE RN IN ( SELECT MAX(RN) 
                  FROM [tblTradeFileEmsxStatus] 
                  WHERE DateEntered <= @KnowledgeDate
                  GROUP BY [TransactionParentID])
    AND ((DateDeleted IS NULL) OR (DateDeleted > @KnowledgeDate))

  END

RETURN
END



GO
GRANT SELECT ON [dbo].[fn_tblTradeFileEmsxStatus_SelectKD] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[fn_tblTradeFileEmsxStatus_SelectKD] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[fn_tblTradeFileEmsxStatus_SelectKD] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT SELECT ON [dbo].[fn_tblTradeFileEmsxStatus_SelectKD] TO [web_TradeProcessing] AS [dbo]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_tblTradeFileEmsxStatus_SelectSingle]    Script Date: 9/23/2013 12:28:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE  FUNCTION [dbo].[fn_tblTradeFileEmsxStatus_SelectSingle]
(
@TransactionParentID [int] ,
@KnowledgeDate datetime
)
RETURNS @tblTradeFileEmsxStatus TABLE
(
	[RN] [int] NOT NULL ,
	[AuditID] [int] NOT NULL,
	[TransactionParentID] [int] NOT NULL,
	[SequenceNumber] [bigint] NOT NULL,
	[OrderStatus] [int] NOT NULL,
	[CorrelationID] [int] NOT NULL,
	[StatusString] [nvarchar](500) NOT NULL,
	[EMSX_BrokerID] [nvarchar](50) NOT NULL,
	[EMSX_Ticker] [nvarchar](50) NOT NULL,
	[RouteIDs] [varchar](500) NULL,
	[DateEntered] [datetime] NULL
	PRIMARY KEY ([TransactionParentID])
)
AS
BEGIN

  IF (ISNULL(@KnowledgeDate, '1900-01-01') <= '1900-01-01')
  BEGIN
    -- LIVE

    INSERT @tblTradeFileEmsxStatus
	    (
	    RN ,
	    AuditID ,
      [TransactionParentID],
      [SequenceNumber],
      [OrderStatus],
      [CorrelationID],
      [StatusString],
      [EMSX_BrokerID],
      [EMSX_Ticker],
      [RouteIDs],
      [DateEntered]
	    )
    SELECT 	
	    RN ,
	    AuditID ,
      [TransactionParentID],
      [SequenceNumber],
      [OrderStatus],
      [CorrelationID],
      [StatusString],
      [EMSX_BrokerID],
      [EMSX_Ticker],
      [RouteIDs],
      [DateEntered]
    FROM [tblTradeFileEmsxStatus]
    WHERE RN IN ( SELECT MAX(RN) 
                  FROM [tblTradeFileEmsxStatus] 
                  WHERE (@TransactionParentID = 0) OR ([TransactionParentID] =  @TransactionParentID)
                  GROUP BY [TransactionParentID])
    AND (DateDeleted IS NULL)

  END
  ELSE
  BEGIN
     -- KnowledgeDated

    INSERT @tblTradeFileEmsxStatus
	    (
	    RN ,
	    AuditID ,
      [TransactionParentID],
      [SequenceNumber],
      [OrderStatus],
      [CorrelationID],
      [StatusString],
      [EMSX_BrokerID],
      [EMSX_Ticker],
      [RouteIDs],
      [DateEntered]
	    )
    SELECT 	
	    RN ,
	    AuditID ,
      [TransactionParentID],
      [SequenceNumber],
      [OrderStatus],
      [CorrelationID],
      [StatusString],
      [EMSX_BrokerID],
      [EMSX_Ticker],
      [RouteIDs],
      [DateEntered]
    FROM [tblTradeFileEmsxStatus]
    WHERE RN IN ( SELECT MAX(RN) 
                  FROM [tblTradeFileEmsxStatus] 
                  WHERE (DateEntered <= @KnowledgeDate) AND ((@TransactionParentID = 0) OR ([TransactionParentID] =  @TransactionParentID))
                  GROUP BY [TransactionParentID])
    AND ((DateDeleted IS NULL) OR (DateDeleted > @KnowledgeDate))

  END

RETURN
END




GO
GRANT SELECT ON [dbo].[fn_tblTradeFileEmsxStatus_SelectSingle] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT SELECT ON [dbo].[fn_tblTradeFileEmsxStatus_SelectSingle] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT SELECT ON [dbo].[fn_tblTradeFileEmsxStatus_SelectSingle] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT SELECT ON [dbo].[fn_tblTradeFileEmsxStatus_SelectSingle] TO [web_TradeProcessing] AS [dbo]
GO

/****** Object:  StoredProcedure [dbo].[adp_tblTradeFileEmsxStatus_DeleteCommand]    Script Date: 9/23/2013 12:28:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[adp_tblTradeFileEmsxStatus_DeleteCommand]
(
	@TransactionParentID int = 0
)
AS
	SET NOCOUNT OFF;

	INSERT tblTradeFileEmsxStatus
    (
      [TransactionParentID],
      [SequenceNumber],
      [OrderStatus],
      [CorrelationID],
      [StatusString],
      [EMSX_BrokerID],
      [EMSX_Ticker],
      [RouteIDs],
      DateDeleted
	  )
  SELECT 	
      [TransactionParentID],
      [SequenceNumber],
      [OrderStatus],
      [CorrelationID],
      [StatusString],
      [EMSX_BrokerID],
      [EMSX_Ticker],
      [RouteIDs],
      getdate()
  FROM  [fn_tblTradeFileEmsxStatus_SelectSingle](@TransactionParentID, Null)
  WHERE ([TransactionParentID] = @TransactionParentID)
 
	IF @@ERROR = 0
	  BEGIN
	    RETURN -1
	  END
	ELSE
	  BEGIN
	    RETURN 0
	  END

	RETURN



GO
GRANT EXECUTE ON [dbo].[adp_tblTradeFileEmsxStatus_DeleteCommand] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[adp_tblTradeFileEmsxStatus_DeleteCommand] TO [web_TradeProcessing] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[adp_tblTradeFileEmsxStatus_InsertCommand]    Script Date: 9/23/2013 12:28:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[adp_tblTradeFileEmsxStatus_InsertCommand]
	(
	@TransactionParentID [int] ,
	@SequenceNumber [bigint] ,
	@OrderStatus [int] ,
	@CorrelationID [int] ,
	@StatusString [nvarchar](500) ,
	@EMSX_BrokerID [nvarchar](50) ,
	@EMSX_Ticker [nvarchar](50) ,
	@RouteIDs [varchar](500) ,
	@KnowledgeDate datetime
	)
AS

	SET NOCOUNT OFF

	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
	
	BEGIN TRANSACTION
	
	DECLARE @Count [int] = 0

  -- For this table, Don't allow identical updates.
  -- It's just a waste....

  SELECT @Count = Count(*)
  FROM [fn_tblTradeFileEmsxStatus_SelectSingle](@TransactionParentID, '1900-01-01')
  WHERE 
    SequenceNumber = @SequenceNumber AND
    OrderStatus = @OrderStatus AND 
    CorrelationID = @CorrelationID AND
    StatusString = @StatusString AND 
    EMSX_BrokerID = @EMSX_BrokerID AND 
    EMSX_Ticker = @EMSX_Ticker AND 
    RouteIDs = @RouteIDs


	-- If OK, then insert new record.

  IF @Count = 0
  BEGIN
	  INSERT INTO tblTradeFileEmsxStatus
      (
        [TransactionParentID],
        [SequenceNumber],
        [OrderStatus],
        [CorrelationID],
        [StatusString],
        [EMSX_BrokerID],
        [EMSX_Ticker],
        [RouteIDs]
		  )
	  VALUES 
      (
		    @TransactionParentID,
        @SequenceNumber,
        @OrderStatus,
        @CorrelationID,
        @StatusString,
        @EMSX_BrokerID,
        @EMSX_Ticker,
        @RouteIDs
		  )
	END
	SELECT 
	    RN ,
	    AuditID ,
      [TransactionParentID],
      [SequenceNumber],
      [OrderStatus],
      [CorrelationID],
      [StatusString],
      [EMSX_BrokerID],
      [EMSX_Ticker],
      [RouteIDs]
  FROM [fn_tblTradeFileEmsxStatus_SelectSingle](@TransactionParentID, @KnowledgeDate)
	WHERE ([TransactionParentID] = @TransactionParentID)

	-- Return value and Commit / Rollback transaction accordingly.
	
	IF @@ERROR = 0
	  BEGIN
	    COMMIT TRANSACTION
	    RETURN @TransactionParentID
	  END
	ELSE
	  BEGIN
	    ROLLBACK TRANSACTION
	    RETURN 0
	  END



GO
GRANT EXECUTE ON [dbo].[adp_tblTradeFileEmsxStatus_InsertCommand] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[adp_tblTradeFileEmsxStatus_InsertCommand] TO [web_TradeProcessing] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[adp_tblTradeFileEmsxStatus_SelectCommand]    Script Date: 9/23/2013 12:28:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[adp_tblTradeFileEmsxStatus_SelectCommand]
(
@AuditID int = 0,
@KnowledgeDate datetime = Null
)
AS
	SET NOCOUNT ON;

	DECLARE @TransactionParentID int = 0
	SELECT @TransactionParentID = ISNULL(@AuditID, 0)

SELECT	
	    RN ,
	    AuditID ,
      [TransactionParentID],
      [SequenceNumber],
      [OrderStatus],
      [CorrelationID],
      [StatusString],
      [EMSX_BrokerID],
      [EMSX_Ticker],
      [RouteIDs]
FROM [fn_tblTradeFileEmsxStatus_SelectKD](@KnowledgeDate)
	WHERE (@TransactionParentID = 0) OR ([TransactionParentID] = @TransactionParentID)






GO
GRANT EXECUTE ON [dbo].[adp_tblTradeFileEmsxStatus_SelectCommand] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[adp_tblTradeFileEmsxStatus_SelectCommand] TO [InvestMaster_Manager] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[adp_tblTradeFileEmsxStatus_SelectCommand] TO [InvestMaster_Sales] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[adp_tblTradeFileEmsxStatus_SelectCommand] TO [web_TradeProcessing] AS [dbo]
GO
/****** Object:  StoredProcedure [dbo].[adp_tblTradeFileEmsxStatus_UpdateCommand]    Script Date: 9/23/2013 12:28:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[adp_tblTradeFileEmsxStatus_UpdateCommand]
	(
	@TransactionParentID [int] ,
	@SequenceNumber [bigint] ,
	@OrderStatus [int] ,
	@CorrelationID [int] ,
	@StatusString [nvarchar](500) ,
	@EMSX_BrokerID [nvarchar](50) ,
	@EMSX_Ticker [nvarchar](50) ,
	@RouteIDs [varchar](500) ,
	@KnowledgeDate datetime
	)
AS
	SET NOCOUNT OFF;

	DECLARE @RVal int 

	IF (@Knowledgedate >= (getdate())) OR (ISNULL(@Knowledgedate, '1900-01-01') <= '1900-01-01')
	  BEGIN
		EXECUTE @RVal  =  [dbo].[adp_tblTradeFileEmsxStatus_InsertCommand] 
			@TransactionParentID = @TransactionParentID,
			@SequenceNumber = @SequenceNumber,
			@OrderStatus = @OrderStatus,
			@CorrelationID = @CorrelationID,
			@StatusString = @StatusString,
			@EMSX_BrokerID = @EMSX_BrokerID,
			@EMSX_Ticker = @EMSX_Ticker,
      @RouteIDs = @RouteIDs,
			@KnowledgeDate  =  @KnowledgeDate
			
		RETURN @RVal
	  END
	ELSE
	  BEGIN
		SELECT	
 	    RN ,
	    AuditID ,
      [TransactionParentID],
      [SequenceNumber],
      [OrderStatus],
      [CorrelationID],
      [StatusString],
      [EMSX_BrokerID],
      [EMSX_Ticker],
      [RouteIDs]

		FROM [fn_tblTradeFileEmsxStatus_SelectSingle](@TransactionParentID, @KnowledgeDate)
	  WHERE ([TransactionParentID] = @TransactionParentID)
		
	  END



GO
GRANT EXECUTE ON [dbo].[adp_tblTradeFileEmsxStatus_UpdateCommand] TO [InvestMaster_Admin] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[adp_tblTradeFileEmsxStatus_UpdateCommand] TO [web_TradeProcessing] AS [dbo]
GO
