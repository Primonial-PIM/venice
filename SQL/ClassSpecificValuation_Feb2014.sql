USE [Renaissance]
GO

/****** Object:  UserDefinedFunction [dbo].[fn_Valuation_ClassSpecific]    Script Date: 2/6/2014 9:33:33 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE FUNCTION [dbo].[fn_Valuation_ClassSpecific]
(
	@FundID int,
  @SpecificUnitID int,
	@ValueDate datetime,
	@UnitPriceVariant int,
	@PriceVariantExpenseCutoffDate datetime,
	@PriceVariantManagementFeeCutoffDate datetime,
	@PriceVariantPerformanceFeeCutoffDate datetime,
	@StatusGroupFilter nvarchar(50),
	@AdministratorDatesFilter int,
  @ReferenceFXID as integer,
  @UseAdministratorPriceDates int,
	@KnowledgeDate datetime

)
	-- ***************************************************
  -- 
	-- ***************************************************
-- Function to provide a valuation, only for Class-specific transactions..

-- @FundID        : Limit the Funds to calculate for. NULL or 0 returns all Funds.
-- @Valuedate     : Valuedate limit to apply to Transactions. NULL or '1900-01-01' returns all rows.
-- @UnitPriceVariant	: Retrieve Transactions consistent with This Unit Price Variant
--			(1) GAV		Gross Asset Value Excl all expenses and fees.
--			(2) Basic NAV 	Excluding Mgmt & Incentive fees, but including all other expenses
--			(3) GNAV	NAV, excluding only Performance (Incentive) fees.
--			(4) NAV		NAV, including all expenses etc. Also Null, 0
--			Note This value passes from fn_UnitPrice (among others ?) . Update that function if these values change.
--
-- @PriceVariantFeeCutoffDate : For Nav variants which exclude fees, this is the date after which they will be excluded
--
-- @AggregateToInstrumentParentID
--			(0 or Null) : No Aggregation to Parent
--			(Non 0)		: Do Aggregate to parent ID
-- @Knowledgedate : Knowledgedate to apply to source tables. Null or '1900-01-01' returns all rows.


RETURNS @tblValuation
	TABLE(	
		RN int IDENTITY (1, 1), 
		rptRN int DEFAULT 0, 
		Fund int, 				--
		SpecificToFundUnitID int, 				--
		Instrument int, 			--
		InstrumentParent int, 			--
		InstrumentParentEquivalentRatio float, 	--
		InstrumentDescription nvarchar(100), 	--
		InstrumentPFPCID nvarchar(100), 	--
		InstrumentType varchar (100), 	--
		InstrumentTypeID int, 			--
		InstrumentFundType int, 		--
		InstrumentContractSize float, -- Instrument Multiplier
		InstrumentMultiplier float, -- Instrument Multiplier
		InstrumentIsFXFuture bit, 
		PertracCode nvarchar(100), 		--
		InstrumentExpectedReturn float, --
		SignedUnits float, 				--
		SignedSettlement float, 		-- In Instrument Currency
		PriceDate datetime, 			-- Date of Valuation Price
		PriceNAV float, 				-- Valuation Price NAV (In Instrument Ccy)
		PriceLevel float, 				-- Valuation Price (In Instrument Ccy)
		PriceMultiplier float, 			-- Price Multiplier
		PriceFinal int, 				-- Is a Final Price ?
		PriceIsAdministrator int,		-- Is an Administrator sourced Price ?
		AvgTransactionPrice float, 		-- Average Transaction Price (In Instrument Ccy)
		FundFXRate float, 				-- FX Rate Fund to USD
		FundFXDate datetime, 			-- Date for applicable FX rate.
		InstrumentFXRate float, 		-- FX Rate for Instrument to USD
		CompoundFXRate float, 			-- FX Rate From Instrument Ccy to Fund Ccy
		Value float, 					-- Value in Fund Currency
		USDValue float,					-- Value in USD
		ReferenceValue float,					-- Value in ReferenceCurrency
		LocalValue float,				-- Value in Instrument Currency
		ConsiderationValue float, 		-- Consideration in Fund Currency
		CurrencyOfInstrument nvarchar(10), 	--
		FundBaseCurrency int,			-- 
		FundCode nvarchar(100), 			--
		InstrumentLimitDisplay bit,		-- 
		InstrumentWorstCase float, 		--
		InstrumentDealingPeriod int, 	--
		InstrumentExcludeGAV int, 		--
		InstrumentIsManagementFee int, 	--
		InstrumentIsIncentiveFee int, 	--
		InstrumentIsEqualisation int, 	--
		InstrumentISIN nvarchar(25), 	--
		InstrumentSEDOL nvarchar(25), 	--
		FirstValueDate datetime, 		--
		LastValueDate datetime, 		--
		SettlementDate datetime,		--
		InstrumentParentID int,			--
		TransactionCompleteness float,	-- 
		TransactionType nvarchar(50) DEFAULT '',		--
		AggregatedInstrumentCount int DEFAULT 0,
    UseAdministratorPriceDates int,
		FundClosed int DEFAULT 0
		PRIMARY KEY (Fund, SpecificToFundUnitID, Instrument)
		)
AS
BEGIN

	DECLARE @SubscribeTransactionType int = 2
	DECLARE @RedeemTransactionType int = 3

	DECLARE @ExcludeAllExpenses int = 0								-- InstrumentExcludeGAV
	DECLARE @ExcludeEqualisationInstruments int = 0 	-- InstrumentIsEqualisation
	DECLARE @ExcludeManageMentFees int = 0						-- InstrumentIsManagementFee
	DECLARE @ExcludeIncentiveFees int = 0							-- InstrumentIsIncentiveFee

	DECLARE @ValueDateIsLive int = 0
	DECLARE @LiveReferenceDate datetime = CONVERT(datetime , '1900-01-01')

  DECLARE @FundAdministratorCode nvarchar(50)

  DECLARE @AdminPriceDates TABLE
    (
    InstrumentID int,
    InstrumentISIN nvarchar(25),
    neo_pricedate_value date
    PRIMARY KEY (InstrumentID, neo_pricedate_value)
    )

  DECLARE @tblMaxRN TABLE (MaxRN int PRIMARY KEY) -- Used to get @VenicePriceTable

  DECLARE @VenicePriceTable TABLE
    (
	  PriceID int, 
	  InstrumentID int, 
	  PriceDate datetime, 
	  PriceNAV float, 
	  PriceLevel float, 
	  PriceMultiplier float, 
	  PriceFinal int, 
	  PriceIsAdministrator bit, 
	  PRIMARY KEY (InstrumentID, PriceDate)
    )

  DECLARE @Instruments TABLE
		(InstrumentID int PRIMARY KEY, 
		InstrumentISIN nvarchar(25),
		InstrumentType int,
		InstrumentCurrencyID int, 
		InstrumentCurrency nvarchar(50),
		InstrumentContractSize float, -- Instrument Multiplier
		InstrumentMultiplier float, -- Instrument Multiplier
		InstrumentParent int, 
		InstrumentParentEquivalentRatio float, 
		InstrumentExcludeGAV int, 
		InstrumentIsManagementFee int, 
		InstrumentIsIncentiveFee int, 
		InstrumentIsEqualisation int, 
		InstrumentIsCrystalisedIncentive bit, 
		InstrumentIsMovementCommission bit)

	SET @FundID = ISNULL(@FundID, 0)
	SET @SpecificUnitID = ISNULL(@SpecificUnitID, 0)
	SET @ValueDate = ISNULL(@ValueDate, '3000-01-01')
	SET @UnitPriceVariant = ISNULL(@UnitPriceVariant, 0)
	SET @PriceVariantExpenseCutoffDate = ISNULL(@PriceVariantExpenseCutoffDate, '3000-01-01')
	SET @PriceVariantManagementFeeCutoffDate = ISNULL(@PriceVariantManagementFeeCutoffDate, '3000-01-01')
	SET @PriceVariantPerformanceFeeCutoffDate = ISNULL(@PriceVariantPerformanceFeeCutoffDate, '3000-01-01')
	SET @StatusGroupFilter = ISNULL(@StatusGroupFilter, '')
	SET @AdministratorDatesFilter = ISNULL(@AdministratorDatesFilter, 0)
	SET @ReferenceFXID = ISNULL(@ReferenceFXID, 3)
	SET @KnowledgeDate = ISNULL(@KnowledgeDate, '1900-01-01')

	IF (@PriceVariantExpenseCutoffDate >= @ValueDate) AND (@PriceVariantManagementFeeCutoffDate >= @ValueDate)  AND (@PriceVariantPerformanceFeeCutoffDate >= @ValueDate)
	BEGIN
		SET @UnitPriceVariant = 0 -- If you do not actually exclude any expenses, then it is a normal NAV.
	END

	IF (@ValueDate <= @LiveReferenceDate)
	BEGIN
		SET @ValueDateIsLive = 1
	END

	-- Declare Variables.

	DECLARE @BestFxs TABLE
		(
		FXCurrencyCode int, 
		FXDate datetime, 
		FXRate float,
		PRIMARY KEY (FXCurrencyCode)
		)

	INSERT @BestFxs(FXCurrencyCode, FXDate, FXRate)
	SELECT FXCurrencyCode, FXDate, FXRate
	FROM fn_BestFX(@ValueDate, @KnowledgeDate)

	DECLARE @ReferenceFXRate float
	SELECT @ReferenceFXRate = MAX(FXRate)
	FROM @BestFxs
	WHERE FXCurrencyCode = @ReferenceFXID

	SET @ReferenceFXRate = ISNULL(@ReferenceFXRate, 1)

	DECLARE @tblAggregatedTransactions
	TABLE(
		Fund int, 
		TransactionSpecificToFundUnitID int, 
		Instrument int, 
		TransactionTypeID int DEFAULT 0, 
		SignedSettlement float, 
		SignedUnits float, 
		Price float, 
		FirstValueDate datetime, 
		LastValueDate datetime, 
		SettlementDate datetime, 
		Completeness float, 
		PRIMARY KEY (Fund, TransactionSpecificToFundUnitID, Instrument))

	DECLARE @ExcludeInstrumentList TABLE
		(InstrumentID int PRIMARY KEY)


	DECLARE  @InstrumentIDs [dbo].[IntegerListTableType]

	-- Resolve Transaction Status filter

	DECLARE @SELECT_BY_TRANSACTION_STATUS int = 0
	DECLARE @TransactionStatusList TABLE
		(TradeStatusID int PRIMARY KEY)

	IF (ISNULL(@StatusGroupFilter, '') <> '') 
	BEGIN
		SET @SELECT_BY_TRANSACTION_STATUS = 1

		INSERT @TransactionStatusList(TradeStatusID)
		SELECT DISTINCT [STATUS_ID]
		FROM fn_tblSophisStatusGroups_SelectedGroup(0, @StatusGroupFilter, @KnowledgeDate)
	END

	-- Resolve AdministratorDatesFilter
	-- Not relevant as we do not include Subscriptions / Redemptions in the Valuation.

	-- Resolve various Expense flags

	IF ISNULL(@UnitPriceVariant, 0) = 1		-- GAV
		BEGIN
		SELECT @ExcludeAllExpenses = 1
		SELECT @ExcludeEqualisationInstruments = 1
		SELECT @ExcludeManageMentFees = 1
		SELECT @ExcludeIncentiveFees = 1
		END

	IF ISNULL(@UnitPriceVariant, 0) = 2		-- Basic NAV
		BEGIN
		SELECT @ExcludeEqualisationInstruments = 1
		SELECT @ExcludeManageMentFees = 1
		SELECT @ExcludeIncentiveFees = 1
		END

	IF ISNULL(@UnitPriceVariant, 0) = 3		-- GNAV - Gross NAV - Excl Incentive fees
		BEGIN
		SELECT @ExcludeEqualisationInstruments = 1
		SELECT @ExcludeIncentiveFees = 1
		END
			
  -- Get Instrument Details, required for all NAV types.

  INSERT @Instruments(InstrumentID, 
    InstrumentISIN,
    InstrumentType, 
    InstrumentCurrencyID,
    InstrumentCurrency,
	  InstrumentContractSize,
	  InstrumentMultiplier,
	  InstrumentParent, 
	  InstrumentParentEquivalentRatio, 
	  InstrumentExcludeGAV, 
	  InstrumentIsManagementFee, 
	  InstrumentIsIncentiveFee, 
	  InstrumentIsEqualisation , 
		InstrumentIsCrystalisedIncentive , 
		InstrumentIsMovementCommission)
  SELECT Instruments.InstrumentID, 
    InstrumentISIN,
    InstrumentType,
    InstrumentCurrencyID,
    InstrumentCurrency,
	  InstrumentContractSize,
	  InstrumentMultiplier,
	  InstrumentParent, 
	  InstrumentParentEquivalentRatio, 
	  InstrumentExcludeGAV, 
	  InstrumentIsManagementFee, 
	  InstrumentIsIncentiveFee, 
	  InstrumentIsEqualisation , 
		InstrumentIsCrystalisedIncentive , 
		InstrumentIsMovementCommission
  FROM 	
    fn_tblInstrument_SelectKD(@KnowledgeDate) Instruments 

	--
	-- Get Aggregated Transactions.

	IF (ISNULL(@UnitPriceVariant, 0) < 1) OR (ISNULL(@UnitPriceVariant, 0) > 3)
		BEGIN
					INSERT @tblAggregatedTransactions(Fund , TransactionSpecificToFundUnitID, Instrument , SignedSettlement , SignedUnits, Price , FirstValueDate , LastValueDate, SettlementDate, Completeness)
					SELECT TransactionFund, TransactionSpecificToFundUnitID, TransactionInstrument,  Sum(IIF((TransactionLeg = 1) AND (TransactionIsFX = 0), TransactionSignedSettlement / TransactionFXRate, TransactionSignedSettlement)), Sum(TransactionSignedUnits), 
								 Avg(TransactionPrice), Min(TransactionValueDate), Max(TransactionValueDate), 
								 Max(TransactionSettlementDate), 
								 (Cast(Sum(Abs(TransactionFinalDataEntry)) AS float) / Cast(Count(TransactionFinalDataEntry) AS float))
					FROM tblTransaction Transactions
					WHERE RN IN ( SELECT MAX(RN) 
												FROM tblTransaction 
												WHERE (((TransactionDateEntered <= @KnowledgeDate) or (TransactionDateEntered IS NULL)) OR (ISNULL(@KnowledgeDate, '1900-01-01') <= '1900-01-01')) AND 
															(((TransactionDateDeleted > @KnowledgeDate) or (TransactionDateDeleted IS NULL)) AND (NOT ((ISNULL(@KnowledgeDate, '1900-01-01')  <= '1900-01-01') AND (NOT (TransactionDateDeleted is NULL)))))
												GROUP BY TransactionParentID, TransactionLeg )
					AND 
							((TransactionSpecificToFundUnitID > 0) AND ((@SpecificUnitID = 0) OR (TransactionSpecificToFundUnitID = @SpecificUnitID))) AND 
							(NOT ((TransactionType = @SubscribeTransactionType) OR (TransactionType = @RedeemTransactionType))) AND
							((@FundID = 0) OR (TransactionFund = @FundID)) AND
							((@SELECT_BY_TRANSACTION_STATUS = 0) OR (Transactions.TransactionTradeStatusID = 0) OR (Transactions.TransactionTradeStatusID IN (SELECT TradeStatusID FROM @TransactionStatusList))) AND 
							((TransactionValueDate <= @ValueDate) OR (@ValueDateIsLive <> 0))
					GROUP BY TransactionFund, TransactionSpecificToFundUnitID, TransactionInstrument

		END
		ELSE
		BEGIN

					INSERT @tblAggregatedTransactions(Fund , TransactionSpecificToFundUnitID, Instrument , SignedSettlement , SignedUnits, Price , FirstValueDate , LastValueDate, SettlementDate, Completeness)
					SELECT TransactionFund, TransactionSpecificToFundUnitID, TransactionInstrument,  Sum(IIF((TransactionLeg = 1) AND (TransactionIsFX = 0), TransactionSignedSettlement / TransactionFXRate, TransactionSignedSettlement)), Sum(TransactionSignedUnits), 
								 Avg(TransactionPrice), Min(TransactionValueDate), Max(TransactionValueDate), 
								 Max(TransactionSettlementDate), 
								 (Cast(Sum(Abs(TransactionFinalDataEntry)) AS float) / Cast(Count(TransactionFinalDataEntry) AS float))
					FROM	tblTransaction Transactions LEFT JOIN 
								@Instruments Instruments ON (Transactions.TransactionInstrument = Instruments.InstrumentID)
					WHERE RN IN ( SELECT MAX(RN) 
												FROM tblTransaction 
												WHERE (((TransactionDateEntered <= @KnowledgeDate) or (TransactionDateEntered IS NULL)) OR (ISNULL(@KnowledgeDate, '1900-01-01') <= '1900-01-01')) AND 
															(((TransactionDateDeleted > @KnowledgeDate) or (TransactionDateDeleted IS NULL)) AND (NOT ((ISNULL(@KnowledgeDate, '1900-01-01')  <= '1900-01-01') AND (NOT (TransactionDateDeleted is NULL)))))
												GROUP BY TransactionParentID, TransactionLeg )
					AND 
							((TransactionSpecificToFundUnitID > 0) AND ((@SpecificUnitID = 0) OR (TransactionSpecificToFundUnitID = @SpecificUnitID))) AND 
							(NOT ((TransactionType = @SubscribeTransactionType) OR (TransactionType = @RedeemTransactionType))) AND
							((@FundID = 0) OR (TransactionFund = @FundID)) AND
							((@SELECT_BY_TRANSACTION_STATUS = 0) OR (Transactions.TransactionTradeStatusID = 0) OR (Transactions.TransactionTradeStatusID IN (SELECT TradeStatusID FROM @TransactionStatusList))) AND 
							((TransactionValueDate <= @ValueDate) OR (@ValueDateIsLive <> 0)) AND 
							(
								(
									((@ExcludeAllExpenses = 0) OR (Instruments.InstrumentExcludeGAV = 0) OR (TransactionValueDate <= @PriceVariantExpenseCutoffDate)) AND
									((@ExcludeManageMentFees = 0) OR (Instruments.InstrumentIsManagementFee = 0) OR (TransactionValueDate <= @PriceVariantManagementFeeCutoffDate)) AND
									((@ExcludeIncentiveFees = 0) OR (Instruments.InstrumentIsIncentiveFee = 0) OR (TransactionValueDate <= @PriceVariantPerformanceFeeCutoffDate)) 
								)
							)
					GROUP BY TransactionFund, TransactionSpecificToFundUnitID, TransactionInstrument


		END

	IF (@ExcludeEqualisationInstruments <> 0)
	BEGIN
		INSERT @ExcludeInstrumentList(InstrumentID)
		SELECT InstrumentID
		FROM 	@Instruments
		WHERE InstrumentIsEqualisation <> 0

		DELETE FROM @tblAggregatedTransactions
		WHERE Instrument IN 
			(
			SELECT InstrumentID
			FROM @ExcludeInstrumentList
			) 
	END

	--

  INSERT @InstrumentIDs(value)
  SELECT DISTINCT Instrument FROM @tblAggregatedTransactions

  --

  IF (ISNULL(@UseAdministratorPriceDates, 0) <> 0) 
  BEGIN

    SELECT 
        @FundAdministratorCode = ISNULL(MAX(FundAdministratorCode), '')
    FROM fn_tblFund_SelectKD(@KnowledgeDate)
    WHERE FundID = @FundID
      
    INSERT @AdminPriceDates(InstrumentID, InstrumentISIN, neo_pricedate_value)
    SELECT DISTINCT Instrument.InstrumentID, neo_isin, neo_pricedate_value
    FROM [neocapture].[dbo].fn_tblRecInventory_SelectKD(0, @ValueDate, '', @KnowledgeDate) neo_inventory
    INNER JOIN @Instruments Instrument ON (neo_inventory.neo_isin = Instrument.InstrumentISIN) AND (neo_inventory.neo_quotecurrency = Instrument.InstrumentCurrency)
    WHERE (neo_inventory.fundcode = @FundAdministratorCode)

    -- Venice Prices (Code derived from [fn_BestPrice])

    INSERT @tblMaxRN(MaxRN)
    SELECT MAX(tblPrice.RN)
    FROM @AdminPriceDates MaxID INNER JOIN
    tblPrice ON (MaxID.InstrumentID = tblPrice.PriceInstrument) AND (MaxID.neo_pricedate_value = tblPrice.PriceDate)
    GROUP BY tblPrice.PriceInstrument

    INSERT @VenicePriceTable (PriceID, InstrumentID, PriceDate, PriceNAV, PriceLevel, PriceMultiplier, PriceFinal, PriceIsAdministrator)
	    SELECT Prices.PriceID, Prices.PriceInstrument, Prices.PriceDate, Prices.PriceNAV, Prices.PriceLevel, Prices.PriceMultiplier, Prices.PriceFinal, Prices.PriceIsAdministrator
	    FROM @tblMaxRN RNs INNER JOIN 
            tblPrice Prices ON (Prices.RN = RNs.MaxRN)

  END

	--
	-- Aggregated query from Transaction table, calculates totals.
	-- Note the use of Cast(), otherwise Integer arithmetic occurs and you only get return values of 0 or 1.

	INSERT @tblValuation
		(
		Fund, 
		SpecificToFundUnitID,
		Instrument, 
		InstrumentParent, 
		InstrumentParentEquivalentRatio, 
		InstrumentDescription, 
		InstrumentPFPCID, 
		InstrumentType, 
		InstrumentTypeID, 
		InstrumentFundType, 
		InstrumentContractSize, 
		InstrumentMultiplier, 
		InstrumentIsFXFuture,
		PertracCode, 
		InstrumentExpectedReturn, 
		SignedUnits, 
		SignedSettlement, 
		PriceDate, 
		PriceNAV, 
		PriceLevel,
		PriceMultiplier, 
		PriceFinal, 
		PriceIsAdministrator, 
		AvgTransactionPrice, 
		FundFXRate, 
		FundFXDate, 
		InstrumentFXRate, 
		CompoundFXRate, 
		Value, 
		USDValue, 
		ReferenceValue,
		LocalValue, 
		ConsiderationValue, 
		CurrencyOfInstrument, 
		FundBaseCurrency, 
		FundCode, 
		InstrumentLimitDisplay,	
		InstrumentWorstCase, 
		InstrumentDealingPeriod, 
		InstrumentExcludeGAV, 
		InstrumentIsManagementFee, 
		InstrumentIsIncentiveFee, 
		InstrumentIsEqualisation, 
		InstrumentISIN, 	--
  		InstrumentSEDOL, 	--
		FirstValueDate, 
		LastValueDate, 
		SettlementDate, 
		InstrumentParentID,	
		TransactionCompleteness,
		TransactionType, 
		AggregatedInstrumentCount,
    UseAdministratorPriceDates,
		FundClosed
		)
	SELECT	
		Transactions.Fund,
		Transactions.TransactionSpecificToFundUnitID,
		Transactions.Instrument, 
		Instruments.InstrumentParent, 
		Instruments.InstrumentParentEquivalentRatio,
		Instruments.InstrumentDescription AS Description, 
		Instruments.InstrumentPFPCID, 
		InstrumentType.InstrumentTypeDescription AS InstrumentType, 
		Instruments.InstrumentType AS InstrumentTypeID, 
		Instruments.InstrumentFundType, 
		Instruments.InstrumentContractSize, 
		Instruments.InstrumentMultiplier, 
		Instruments.InstrumentIsFXFuture, 
		Instruments.InstrumentPertracCode AS PertracCode, 
		Instruments.InstrumentExpectedReturn, 
		Transactions.SignedUnits, 
		Transactions.SignedSettlement, 
		COALESCE(SelectedPrice.PriceDate, Price.PriceDate, '1900-01-01'), 
		COALESCE(SelectedPrice.PriceNAV, Price.PriceNAV, 0), 
		COALESCE(SelectedPrice.PriceLevel, Price.PriceLevel, 0), 
		COALESCE(SelectedPrice.PriceMultiplier, Price.PriceMultiplier, 1), 
		COALESCE(SelectedPrice.PriceFinal, Price.PriceFinal, 0), 
		COALESCE(SelectedPrice.PriceIsAdministrator, Price.PriceIsAdministrator, 0), 
		Transactions.Price, 
		ISNULL(FundFx.FXRate, 1.0) AS FundFXRate, 
		ISNULL(FundFx.FXDate, '1900-01-01') AS FundFXDate, 
		ISNULL(InstrumentFX.FXRate, 1.0), 
		ISNULL(InstrumentFX.FXRate, 1.0) / ISNULL(FundFx.FXRate, 1.0) AS CompoundFXRate, 				-- FX Rate Instrument -> Book
		SignedUnits * ISNULL(ISNULL(SelectedPrice.PriceLevel, Price.PriceLevel), 0.0) * Instruments.InstrumentContractSize * Instruments.InstrumentMultiplier * (ISNULL(InstrumentFX.FXRate, 1.0)/ISNULL(FundFx.FXRate, 1.0)) AS Value, 	-- Local Value * Compound Rate (Inst -> Fund Ccy)
		SignedUnits * ISNULL(ISNULL(SelectedPrice.PriceLevel, Price.PriceLevel), 0.0) * Instruments.InstrumentContractSize * Instruments.InstrumentMultiplier * ISNULL(InstrumentFX.FXRate, 1.0),						-- USD Value
		SignedUnits * ISNULL(ISNULL(SelectedPrice.PriceLevel, Price.PriceLevel), 0.0) * Instruments.InstrumentContractSize * Instruments.InstrumentMultiplier * (ISNULL(InstrumentFX.FXRate, 1.0) / @ReferenceFXRate),			-- Reference Currency Value
		SignedUnits * ISNULL(ISNULL(SelectedPrice.PriceLevel, Price.PriceLevel), 0.0) * Instruments.InstrumentContractSize * Instruments.InstrumentMultiplier,	-- Local (Instrument) Value
		SignedSettlement * (ISNULL(InstrumentFX.FXRate, 1.0) / ISNULL(FundFx.FXRate, 1.0)) AS ConsiderationValue, -- Consideration Paid (in Book Ccy)
		Instruments.InstrumentCurrency AS CurrencyOfInstrument, 
		Fund.FundBaseCurrency, 
		Fund.FundCode, 
		Instruments.InstrumentLimitDisplay, 
		Instruments.InstrumentWorstCase, 
		Instruments.InstrumentDealingPeriod, 
		Instruments.InstrumentExcludeGAV,
		Instruments.InstrumentIsManagementFee,
		Instruments.InstrumentIsIncentiveFee,
		Instruments.InstrumentIsEqualisation, 
		Instruments.InstrumentISIN, 
		Instruments.InstrumentSEDOL, 
		Transactions.FirstValueDate, 
		Transactions.LastValueDate, 
		Transactions.SettlementDate, 
		IsNull(Instruments.InstrumentParent ,Instruments.InstrumentID) AS ID, 
		Transactions.Completeness,
		T_Type.TransactionType, -- TransactionType
		1,
    ISNULL(@UseAdministratorPriceDates, 0),
		Fund.FundClosed
	FROM    
		@tblAggregatedTransactions Transactions LEFT JOIN
		fn_tblTransactionType_SelectKD(@KnowledgeDate) T_Type ON Transactions.TransactionTypeID = T_Type.TransactionTypeID LEFT JOIN 
		fn_tblFund_SelectKD(@KnowledgeDate) Fund ON Transactions.Fund = Fund.FundID LEFT JOIN
		@BestFxs FundFX ON Fund.FundBaseCurrency = FundFX.FXCurrencyCode LEFT JOIN
		fn_tblInstrument_SelectedIDs(@InstrumentIDs, @KnowledgeDate) Instruments ON Transactions.Instrument = Instruments.InstrumentID LEFT JOIN
    @VenicePriceTable SelectedPrice ON Transactions.Instrument = SelectedPrice.InstrumentID LEFT JOIN
		fn_BestPriceSelected(@ValueDate, Null, @InstrumentIDs, @KnowledgeDate) Price ON Transactions.Instrument = Price.InstrumentID LEFT JOIN
		fn_tblInstrumentType_SelectKD(@KnowledgeDate) InstrumentType ON Instruments.InstrumentType = InstrumentType.InstrumentTypeID LEFT JOIN
		@BestFxs InstrumentFX ON Instruments.InstrumentCurrencyID = InstrumentFX.FXCurrencyCode

	RETURN
END



GO

GRANT SELECT ON [dbo].[fn_Valuation_ClassSpecific] TO [InvestMaster_Admin] AS [dbo]
GO

GRANT SELECT ON [dbo].[fn_Valuation_ClassSpecific] TO [InvestMaster_Manager] AS [dbo]
GO

GRANT SELECT ON [dbo].[fn_Valuation_ClassSpecific] TO [InvestMaster_Sales] AS [dbo]
GO


USE [Renaissance]
GO

/****** Object:  UserDefinedFunction [dbo].[fn_ProfitAndLoss_ClassSpecific]    Script Date: 2/6/2014 9:34:10 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[fn_ProfitAndLoss_ClassSpecific]
(
	@FundID int,
  @SpecificUnitID int,
	@ValueDateFrom datetime,
	@ValueDateTo datetime,
	@UnitPriceVariant int,
  @StatusGroupFilter nvarchar(50), 
  @AdministratorDatesFilter int, 
  @ReferenceFXID as integer,
  @UseAdministratorPriceDates int,
	@KnowledgeDateFrom datetime, 
	@KnowledgeDateTo datetime,
	@KnowledgeDateLinkedTables datetime
)

-- Function to return Aggregated P&L calculations for the given Fund.
-- between the given dates, for class specific transactions only.

-- @FundID        	: Limit the Funds to calculate for. NULL or 0 returns all Funds.
-- @ValuedateFrom 	: Valuedate for P&L Start Point. P&L Is calculated from the END of this day. NULL or '1900-01-01' returns all rows.
-- @ValuedateTo   	: Valuedate for P&L End Point. P&L Is calculated to the END of this day. NULL or '1900-01-01' returns all rows.
-- @UnitPriceVariant	: Retrieve Transactions consistent with This Unit Price Variant
--			(1) GAV		Gross Asset Value Excl all expenses and fees.
--			(2) Basic NAV 	Excluding Mgmt & Incentive fees, but including all other expenses
--			(3) GNAV	NAV, excluding only Performance (Incentive) fees.
--			(4) NAV		NAV, including all expenses etc. Also Null, 0
--		  	: Null (0) would be the usual parameter value.
-- @KnowledgeDateFrom 	: Knowledgedate to apply to P&L From queries. Null or '1900-01-01' returns all rows.
-- @KnowledgeDateTo 	: Knowledgedate to apply to P&L To queries. Null or '1900-01-01' returns all rows.
-- @KnowledgeDateLinkedTables : Knowledgedate to apply to linked tables (eg tblInstrument). Null or '1900-01-01' returns all rows.

/*  Modified :-

	NPP, 11 Jul 2005, Profit calculation altered to correct potential FX infuluence.
*/

RETURNS @tblProfitAndLoss
	TABLE
	(
	Fund int, 
	SpecificToFundUnitID int, 				--
	FundCode nvarchar(100), 
	InstrumentFundType int, 
	FundTypeDescription nvarchar(100), 
	FundTypeGroup nvarchar(100), 
	FundTypeSortOrder int, 
	FundTypeIsAdministrativeOnly int, 
	InstrumentType int, 
	InstrumentTypeDescription nvarchar(100), 
  InstrumentTypeHasDelta bit,
	PertracCode int, 
	Instrument int,
	InstrumentParent int,
	InstrumentParentEquivalentRatio float,
	InstrumentDescription nvarchar(100), 
	InstrumentDealingPeriod int, 
	InstrumentContractSize float, 
	InstrumentMultiplier float, 
	InstrumentIsFXFuture bit, 
	Value2 float, 
	Consideration2 float, 
	FXRate2 float, 
	FundFXRate2 float, 
	Value1 float, 
	Consideration1 float, 
	FXRate1 float, 
	Profit float,
	StartPrice float, 
	StartPriceDate datetime, 
	EndPrice float, 
	EndPriceDate datetime, 
	USDValue float, 		--  ??
	InstrumentWorstCase float, 
	InstrumentExpectedReturn float, 
	StartUnits float,
	FinalUnits float,
	NetFundValue1 float DEFAULT 0, 
	NetFundValue2 float DEFAULT 0,		--
	AggregatedInstrumentCount int DEFAULT 0
	)
AS
BEGIN

-- Declare temporary tables

DECLARE @TempDate datetime

IF @ValueDateFrom > @ValueDateTo
    BEGIN
	Select @TempDate = @ValueDateFrom
	Select @ValueDateFrom = @ValueDateTo
	Select @ValueDateTo = @TempDate
    END

-- @Temp_tblProfitAndLoss is decalred EXACTLY the same as @tblProfitAndLoss
DECLARE @Temp_tblProfitAndLoss
	TABLE
	(
	Fund int, 
	SpecificToFundUnitID int, 				--
	FundCode nvarchar(100), 
	InstrumentFundType int, 
	FundTypeDescription nvarchar(100), 
	FundTypeGroup nvarchar(100), 
	FundTypeSortOrder int, 
	FundTypeIsAdministrativeOnly int, 
	InstrumentType int, 
	InstrumentTypeDescription nvarchar(100), 
	PertracCode int, 
	Instrument int,
	InstrumentParent int,
	InstrumentParentEquivalentRatio float,
	InstrumentDescription nvarchar(100), 
	InstrumentDealingPeriod int, 
	InstrumentContractSize float, 
	InstrumentMultiplier float, 
	InstrumentIsFXFuture bit, 
	Value2 float, 
	Consideration2 float, 
	FXRate2 float, 
	FundFXRate2 float, 
	Value1 float, 
	Consideration1 float, 
	FXRate1 float, 
	Profit float,
	StartPrice float, 
	StartPriceDate datetime, 
	EndPrice float, 
	EndPriceDate datetime, 
	USDValue float, 		--  ??
	InstrumentWorstCase float, 
	InstrumentExpectedReturn float, 
	StartUnits float,
	FinalUnits float,
	NetFundValue1 float DEFAULT 0, 
	NetFundValue2 float DEFAULT 0,		--
	AggregatedInstrumentCount int DEFAULT 0
	)

-- If there is no ParentID aggregation, or if '@AggregateToInstrumentParentID' = 1 (Pass Through Aggregation) then
-- Just perform the basic query and exit.

  INSERT @Temp_tblProfitAndLoss  
		(
		Fund, 
		SpecificToFundUnitID,
		FundCode, 
		InstrumentFundType, 
		FundTypeDescription, 
		FundTypeGroup, 
		FundTypeSortOrder, 
		FundTypeIsAdministrativeOnly, 
		InstrumentType, 
		InstrumentTypeDescription, 
		PertracCode, 
		Instrument,
		InstrumentParent,
		InstrumentParentEquivalentRatio,
		InstrumentDescription, 
		InstrumentDealingPeriod, 
		InstrumentContractSize, 
		InstrumentMultiplier, 
		InstrumentIsFXFuture, 
		Value2, 
		Consideration2, 
		FXRate2, 
		FundFXRate2, 
		Value1, 
		Consideration1, 
		FXRate1, 
		Profit,
		StartPrice, 
		StartPriceDate, 
		EndPrice, 
		EndPriceDate, 
		USDValue, 
		InstrumentWorstCase, 
		InstrumentExpectedReturn, 
		StartUnits,
		FinalUnits, 
		AggregatedInstrumentCount
		)
	SELECT 	
		ISNULL(VTo.Fund, VFrom.Fund), 
		ISNULL(VTo.SpecificToFundUnitID, VFrom.SpecificToFundUnitID), 
		ISNULL(VTo.FundCode, VFrom.FundCode), 
		ISNULL(VTo.InstrumentFundType, VFrom.InstrumentFundType) AS InstrumentFundType, 
		'', 
		'', 
		0, 
		0, 
		ISNULL(VTo.InstrumentTypeID, VFrom.InstrumentTypeID), 
		'', 
		ISNULL(VTo.PertracCode, VFrom.PertracCode), 
		ISNULL(VTo.Instrument,VFrom.Instrument ),
		ISNULL(VTo.InstrumentParent, VFrom.InstrumentParent ),
		ISNULL(VTo.InstrumentParentEquivalentRatio, VFrom.InstrumentParentEquivalentRatio ),
		ISNULL(VTo.InstrumentDescription, VFrom.InstrumentDescription ),
		ISNULL(VTo.InstrumentDealingPeriod, VFrom.InstrumentDealingPeriod ),
		ISNULL(VTo.InstrumentContractSize, VFrom.InstrumentContractSize ),
		ISNULL(VTo.InstrumentMultiplier, VFrom.InstrumentMultiplier ),
		ISNULL(VTo.InstrumentIsFXFuture, VFrom.InstrumentIsFXFuture ),
		ROUND(ISNULL(VTo.Value, 0),6) AS Value2, 
		ROUND(ISNULL(VTo.ConsiderationValue,0),6) AS Consideration2, 
		ISNULL(VTo.CompoundFXRate, VFrom.CompoundFXRate) AS FXRate2, 
		ISNULL(VTo.FundFXRate, VFrom.FundFXRate) AS FundFXRate2, 
		ROUND(ISNULL(VFrom.Value,0),6) AS Value1, 
		ROUND(ISNULL(VFrom.ConsiderationValue,0),6) AS Consideration1, 
		ISNULL(VFrom.CompoundFXRate,1) AS FXRate1, 
		ROUND((ISNULL(((VTo.SignedUnits * VTo.PriceLevel * VTo.InstrumentContractSize * VTo.InstrumentMultiplier) - VTo.SignedSettlement), 0) - ISNULL(((VFrom.SignedUnits * VFrom.PriceLevel * VFrom.InstrumentContractSize * VFrom.InstrumentMultiplier) - VFrom.SignedSettlement) ,0)) * ISNULL(VTo.CompoundFXRate, VFrom.CompoundFXRate), 6) AS Profit,
		VFrom.PriceLevel AS StartPrice, 
		VFrom.PriceDate AS StartPriceDate, 
		VTo.PriceLevel AS EndPrice, 
		VTo.PriceDate AS EndPriceDate, 
		ROUND(ISNULL((VTo.Value * VTo.FundFXRate), 0), 6) AS USDValue, 
		ISNULL(VTo.InstrumentWorstCase, VFrom.InstrumentWorstCase), 
		ISNULL(VTo.InstrumentExpectedReturn, VFrom.InstrumentExpectedReturn), 
		ISNULL(VFrom.SignedUnits, 0) AS StartUnits,
		ISNULL(VTo.SignedUnits, 0) AS FinalUnits, 
		((ISNULL(Vto.AggregatedInstrumentCount, 1) + ISNULL(VFrom.AggregatedInstrumentCount, 1)) / 2)
	
	FROM [fn_Valuation_ClassSpecific](@FundID, @SpecificUnitID, @ValueDateTo,   @UnitPriceVariant, @ValueDateFrom, @ValueDateFrom, @ValueDateFrom, @StatusGroupFilter, @AdministratorDatesFilter, @ReferenceFXID, @UseAdministratorPriceDates, @KnowledgeDateTo) VTo FULL OUTER JOIN 
	     [fn_Valuation_ClassSpecific](@FundID, @SpecificUnitID, @ValueDateFrom, @UnitPriceVariant, @ValueDateFrom, @ValueDateFrom, @ValueDateFrom, @StatusGroupFilter, @AdministratorDatesFilter, @ReferenceFXID, @UseAdministratorPriceDates, @KnowledgeDateFrom) VFrom ON (VTo.Fund = VFrom.Fund) AND (VTo.SpecificToFundUnitID = VFrom.SpecificToFundUnitID) AND (VTo.Instrument = VFrom.Instrument)
  
  INSERT @tblProfitAndLoss
		(
		Fund, 
		SpecificToFundUnitID, 
		FundCode, 
		InstrumentFundType, 
		FundTypeDescription, 
		FundTypeGroup, 
		FundTypeSortOrder, 
		FundTypeIsAdministrativeOnly, 
		InstrumentType, 
		InstrumentTypeDescription, 
    InstrumentTypeHasDelta, 
		PertracCode, 
		Instrument,
		InstrumentParent,
		InstrumentParentEquivalentRatio,
		InstrumentDescription, 
		InstrumentDealingPeriod, 
		InstrumentContractSize, 
		InstrumentMultiplier, 
		InstrumentIsFXFuture, 
		Value2, 
		Consideration2, 
		FXRate2, 
		FundFXRate2, 
		Value1, 
		Consideration1, 
		FXRate1, 
		Profit,
		StartPrice, 
		StartPriceDate, 
		EndPrice, 
		EndPriceDate, 
		USDValue, 
		InstrumentWorstCase, 
		InstrumentExpectedReturn, 
		StartUnits,
		FinalUnits, 
		AggregatedInstrumentCount
		)
	SELECT 	tblPnL.Fund, 
		tblPnL.SpecificToFundUnitID, 
		tblPnL.FundCode, 
		tblPnL.InstrumentFundType, 
		tblFundType.FundTypeDescription, 
		tblFundType.FundTypeGroup, 
		tblFundType.FundTypeSortOrder, 
		tblFundType.FundTypeIsAdministrativeOnly, 
		tblPnL.InstrumentType, 
		tblInstrumentType.InstrumentTypeDescription, 
    tblInstrumentType.InstrumentTypeHasDelta,
		tblPnL.PertracCode, 
		tblPnL.Instrument,
		tblPnL.InstrumentParent,
		tblPnL.InstrumentParentEquivalentRatio,
		tblPnL.InstrumentDescription,
		tblPnL.InstrumentDealingPeriod,
		tblPnL.InstrumentContractSize,
		tblPnL.InstrumentMultiplier,
		tblPnL.InstrumentIsFXFuture,
		tblPnL.Value2, 
		tblPnL.Consideration2, 
		tblPnL.FXRate2, 
		tblPnL.FundFXRate2, 
		tblPnL.Value1, 
		tblPnL.Consideration1, 
		tblPnL.FXRate1, 
		tblPnL.Profit,
		IsNull(tblPnL.StartPrice, BestPrice.PriceLevel) AS StartPrice, 
		IsNull(tblPnL.StartPriceDate, BestPrice.PriceDate) AS StartPriceDate, 
		IsNull(tblPnL.EndPrice, IsNull(tblPnL.StartPrice, BestPrice.PriceLevel)) AS EndPrice, 
		IsNull(tblPnL.EndPriceDate, IsNull(tblPnL.StartPriceDate, BestPrice.PriceDate)) AS EndPriceDate, 
		tblPnL.USDValue, 
		tblPnL.InstrumentWorstCase,
		tblPnL.InstrumentExpectedReturn, 
		tblPnL.StartUnits,
		tblPnL.FinalUnits, 
		tblPnL.AggregatedInstrumentCount
	FROM ((@Temp_tblProfitAndLoss tblPnL LEFT JOIN 
	fn_tblFundType_SelectKD(@KnowledgeDateLinkedTables) tblFundType ON tblPnL.InstrumentFundType = tblFundType.FundTypeID) LEFT JOIN 
	fn_tblInstrumentType_SelectKD(@KnowledgeDateLinkedTables) tblInstrumentType ON tblPnL.InstrumentType = tblInstrumentType.InstrumentTypeID) LEFT JOIN 
	fn_BestPrice(@ValueDateFrom, Null, @KnowledgeDateTo) BestPrice ON tblPnL.Instrument = BestPrice.InstrumentID
	
   
	RETURN

 END


GO

GRANT SELECT ON [dbo].[fn_ProfitAndLoss_ClassSpecific] TO [InvestMaster_Admin] AS [dbo]
GO

GRANT SELECT ON [dbo].[fn_ProfitAndLoss_ClassSpecific] TO [InvestMaster_Manager] AS [dbo]
GO

GRANT SELECT ON [dbo].[fn_ProfitAndLoss_ClassSpecific] TO [InvestMaster_Sales] AS [dbo]
GO


USE [Renaissance]
GO

/****** Object:  StoredProcedure [dbo].[spu_ProfitAndLoss_ClassSpecific]    Script Date: 2/6/2014 9:35:40 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spu_ProfitAndLoss_ClassSpecific]
(
	@FundID int,
  @SpecificUnitID int,
	@ValueDateFrom datetime,
	@ValueDateTo datetime,
	@UnitPriceVariant int,
  @StatusGroupFilter nvarchar(50), 
  @AdministratorDatesFilter int, 
  @ReferenceFXID as integer,
  @UseAdministratorPriceDates int = 0,
	@KnowledgeDateFrom datetime, 
	@KnowledgeDateTo datetime,
	@KnowledgeDateLinkedTables datetime
)
AS

SELECT 
	Fund, 
	SpecificToFundUnitID, 
	FundCode, 
	InstrumentFundType, 
	FundTypeDescription, 
	FundTypeGroup, 
	FundTypeSortOrder, 
	FundTypeIsAdministrativeOnly, 
	InstrumentType, 
	InstrumentTypeDescription, 
  InstrumentTypeHasDelta, 
	PertracCode, 
	Instrument,
	InstrumentParent,
	InstrumentParentEquivalentRatio,
	InstrumentDescription, 
	InstrumentDealingPeriod, 
	InstrumentContractSize, 
	InstrumentMultiplier, 
	InstrumentIsFXFuture, 
	Value2, 
	Consideration2, 
	FXRate2, 
	FundFXRate2, 
	Value1, 
	Consideration1, 
	FXRate1, 
	Profit,
	StartPrice, 
	StartPriceDate, 
	EndPrice, 
	EndPriceDate, 
	USDValue, 
	InstrumentWorstCase, 
	InstrumentExpectedReturn, 
	StartUnits,
	FinalUnits, 
	AggregatedInstrumentCount
FROM [fn_ProfitAndLoss_ClassSpecific]
	(
	@FundID ,
  @SpecificUnitID ,
	@ValueDateFrom ,
	@ValueDateTo ,
	@UnitPriceVariant ,
  @StatusGroupFilter , 
  @AdministratorDatesFilter , 
  @ReferenceFXID ,
  @UseAdministratorPriceDates ,
	@KnowledgeDateFrom , 
	@KnowledgeDateTo ,
	@KnowledgeDateLinkedTables 
	)

RETURN



GO

GRANT EXECUTE ON [dbo].[spu_ProfitAndLoss_ClassSpecific] TO [InvestMaster_Admin] AS [dbo]
GO

GRANT EXECUTE ON [dbo].[spu_ProfitAndLoss_ClassSpecific] TO [InvestMaster_Manager] AS [dbo]
GO

GRANT EXECUTE ON [dbo].[spu_ProfitAndLoss_ClassSpecific] TO [InvestMaster_Sales] AS [dbo]
GO


