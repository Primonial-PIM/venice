USE [Renaissance]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_ProfitAndLoss]    Script Date: 31/01/2014 14:19:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[fn_ProfitAndLoss_ClassSpecific]
(
	@FundID int,
  @SpecificUnitID int,
	@ValueDateFrom datetime,
	@ValueDateTo datetime,
	@UnitPriceVariant int,
  @StatusGroupFilter nvarchar(50), 
  @AdministratorDatesFilter int, 
  @ReferenceFXID as integer,
	@KnowledgeDateFrom datetime, 
	@KnowledgeDateTo datetime,
	@KnowledgeDateLinkedTables datetime
)
`
-- Function to return Aggregated P&L calculations for the given Fund.
-- between the given dates, for class specific transactions only.

-- @FundID        	: Limit the Funds to calculate for. NULL or 0 returns all Funds.
-- @ValuedateFrom 	: Valuedate for P&L Start Point. P&L Is calculated from the END of this day. NULL or '1900-01-01' returns all rows.
-- @ValuedateTo   	: Valuedate for P&L End Point. P&L Is calculated to the END of this day. NULL or '1900-01-01' returns all rows.
-- @UnitPriceVariant	: Retrieve Transactions consistent with This Unit Price Variant
--			(1) GAV		Gross Asset Value Excl all expenses and fees.
--			(2) Basic NAV 	Excluding Mgmt & Incentive fees, but including all other expenses
--			(3) GNAV	NAV, excluding only Performance (Incentive) fees.
--			(4) NAV		NAV, including all expenses etc. Also Null, 0
--		  	: Null (0) would be the usual parameter value.
-- @KnowledgeDateFrom 	: Knowledgedate to apply to P&L From queries. Null or '1900-01-01' returns all rows.
-- @KnowledgeDateTo 	: Knowledgedate to apply to P&L To queries. Null or '1900-01-01' returns all rows.
-- @KnowledgeDateLinkedTables : Knowledgedate to apply to linked tables (eg tblInstrument). Null or '1900-01-01' returns all rows.

/*  Modified :-

	NPP, 11 Jul 2005, Profit calculation altered to correct potential FX infuluence.
*/

RETURNS @tblProfitAndLoss
	TABLE
	(
	Fund int, 
	SpecificToFundUnitID int, 				--
	FundCode nvarchar(100), 
	InstrumentFundType int, 
	FundTypeDescription nvarchar(100), 
	FundTypeGroup nvarchar(100), 
	FundTypeSortOrder int, 
	FundTypeIsAdministrativeOnly int, 
	InstrumentType int, 
	InstrumentTypeDescription nvarchar(100), 
  InstrumentTypeHasDelta bit,
	PertracCode int, 
	Instrument int,
	InstrumentParent int,
	InstrumentParentEquivalentRatio float,
	InstrumentDescription nvarchar(100), 
	InstrumentDealingPeriod int, 
	InstrumentContractSize float, 
	InstrumentMultiplier float, 
	InstrumentIsFXFuture bit, 
	Value2 float, 
	Consideration2 float, 
	FXRate2 float, 
	FundFXRate2 float, 
	Value1 float, 
	Consideration1 float, 
	FXRate1 float, 
	Profit float,
	StartPrice float, 
	StartPriceDate datetime, 
	EndPrice float, 
	EndPriceDate datetime, 
	USDValue float, 		--  ??
	InstrumentWorstCase float, 
	InstrumentExpectedReturn float, 
	StartUnits float,
	FinalUnits float,
	NetFundValue1 float DEFAULT 0, 
	NetFundValue2 float DEFAULT 0,		--
	AggregatedInstrumentCount int DEFAULT 0
	)
AS
BEGIN

-- Declare temporary tables

DECLARE @TempDate datetime

IF @ValueDateFrom > @ValueDateTo
    BEGIN
	Select @TempDate = @ValueDateFrom
	Select @ValueDateFrom = @ValueDateTo
	Select @ValueDateTo = @TempDate
    END

-- @Temp_tblProfitAndLoss is decalred EXACTLY the same as @tblProfitAndLoss
DECLARE @Temp_tblProfitAndLoss
	TABLE
	(
	Fund int, 
	SpecificToFundUnitID int, 				--
	FundCode nvarchar(100), 
	InstrumentFundType int, 
	FundTypeDescription nvarchar(100), 
	FundTypeGroup nvarchar(100), 
	FundTypeSortOrder int, 
	FundTypeIsAdministrativeOnly int, 
	InstrumentType int, 
	InstrumentTypeDescription nvarchar(100), 
	PertracCode int, 
	Instrument int,
	InstrumentParent int,
	InstrumentParentEquivalentRatio float,
	InstrumentDescription nvarchar(100), 
	InstrumentDealingPeriod int, 
	InstrumentContractSize float, 
	InstrumentMultiplier float, 
	InstrumentIsFXFuture bit, 
	Value2 float, 
	Consideration2 float, 
	FXRate2 float, 
	FundFXRate2 float, 
	Value1 float, 
	Consideration1 float, 
	FXRate1 float, 
	Profit float,
	StartPrice float, 
	StartPriceDate datetime, 
	EndPrice float, 
	EndPriceDate datetime, 
	USDValue float, 		--  ??
	InstrumentWorstCase float, 
	InstrumentExpectedReturn float, 
	StartUnits float,
	FinalUnits float,
	NetFundValue1 float DEFAULT 0, 
	NetFundValue2 float DEFAULT 0,		--
	AggregatedInstrumentCount int DEFAULT 0
	)

-- If there is no ParentID aggregation, or if '@AggregateToInstrumentParentID' = 1 (Pass Through Aggregation) then
-- Just perform the basic query and exit.

  INSERT @Temp_tblProfitAndLoss  
		(
		Fund, 
		SpecificToFundUnitID,
		FundCode, 
		InstrumentFundType, 
		FundTypeDescription, 
		FundTypeGroup, 
		FundTypeSortOrder, 
		FundTypeIsAdministrativeOnly, 
		InstrumentType, 
		InstrumentTypeDescription, 
		PertracCode, 
		Instrument,
		InstrumentParent,
		InstrumentParentEquivalentRatio,
		InstrumentDescription, 
		InstrumentDealingPeriod, 
		InstrumentContractSize, 
		InstrumentMultiplier, 
		InstrumentIsFXFuture, 
		Value2, 
		Consideration2, 
		FXRate2, 
		FundFXRate2, 
		Value1, 
		Consideration1, 
		FXRate1, 
		Profit,
		StartPrice, 
		StartPriceDate, 
		EndPrice, 
		EndPriceDate, 
		USDValue, 
		InstrumentWorstCase, 
		InstrumentExpectedReturn, 
		StartUnits,
		FinalUnits, 
		AggregatedInstrumentCount
		)
	SELECT 	
		ISNULL(VTo.Fund, VFrom.Fund), 
		ISNULL(VTo.SpecificToFundUnitID, VFrom.SpecificToFundUnitID), 
		ISNULL(VTo.FundCode, VFrom.FundCode), 
		ISNULL(VTo.InstrumentFundType, VFrom.InstrumentFundType) AS InstrumentFundType, 
		'', 
		'', 
		0, 
		0, 
		ISNULL(VTo.InstrumentTypeID, VFrom.InstrumentTypeID), 
		'', 
		ISNULL(VTo.PertracCode, VFrom.PertracCode), 
		ISNULL(VTo.Instrument,VFrom.Instrument ),
		ISNULL(VTo.InstrumentParent, VFrom.InstrumentParent ),
		ISNULL(VTo.InstrumentParentEquivalentRatio, VFrom.InstrumentParentEquivalentRatio ),
		ISNULL(VTo.InstrumentDescription, VFrom.InstrumentDescription ),
		ISNULL(VTo.InstrumentDealingPeriod, VFrom.InstrumentDealingPeriod ),
		ISNULL(VTo.InstrumentContractSize, VFrom.InstrumentContractSize ),
		ISNULL(VTo.InstrumentMultiplier, VFrom.InstrumentMultiplier ),
		ISNULL(VTo.InstrumentIsFXFuture, VFrom.InstrumentIsFXFuture ),
		ROUND(ISNULL(VTo.Value, 0),6) AS Value2, 
		ROUND(ISNULL(VTo.ConsiderationValue,0),6) AS Consideration2, 
		ISNULL(VTo.CompoundFXRate, VFrom.CompoundFXRate) AS FXRate2, 
		ISNULL(VTo.FundFXRate, VFrom.FundFXRate) AS FundFXRate2, 
		ROUND(ISNULL(VFrom.Value,0),6) AS Value1, 
		ROUND(ISNULL(VFrom.ConsiderationValue,0),6) AS Consideration1, 
		ISNULL(VFrom.CompoundFXRate,1) AS FXRate1, 
		ROUND((ISNULL(((VTo.SignedUnits * VTo.PriceLevel * VTo.InstrumentContractSize * VTo.InstrumentMultiplier) - VTo.SignedSettlement), 0) - ISNULL(((VFrom.SignedUnits * VFrom.PriceLevel * VFrom.InstrumentContractSize * VFrom.InstrumentMultiplier) - VFrom.SignedSettlement) ,0)) * ISNULL(VTo.CompoundFXRate, VFrom.CompoundFXRate), 6) AS Profit,
		VFrom.PriceLevel AS StartPrice, 
		VFrom.PriceDate AS StartPriceDate, 
		VTo.PriceLevel AS EndPrice, 
		VTo.PriceDate AS EndPriceDate, 
		ROUND(ISNULL((VTo.Value * VTo.FundFXRate), 0), 6) AS USDValue, 
		ISNULL(VTo.InstrumentWorstCase, VFrom.InstrumentWorstCase), 
		ISNULL(VTo.InstrumentExpectedReturn, VFrom.InstrumentExpectedReturn), 
		ISNULL(VFrom.SignedUnits, 0) AS StartUnits,
		ISNULL(VTo.SignedUnits, 0) AS FinalUnits, 
		((ISNULL(Vto.AggregatedInstrumentCount, 1) + ISNULL(VFrom.AggregatedInstrumentCount, 1)) / 2)
	
	FROM [fn_Valuation_ClassSpecific](@FundID, @SpecificUnitID, @ValueDateTo,   @UnitPriceVariant, @ValueDateFrom, @ValueDateFrom, @ValueDateFrom, @StatusGroupFilter, @AdministratorDatesFilter, @ReferenceFXID, @KnowledgeDateTo) VTo FULL OUTER JOIN 
	     [fn_Valuation_ClassSpecific](@FundID, @SpecificUnitID, @ValueDateFrom, @UnitPriceVariant, @ValueDateFrom, @ValueDateFrom, @ValueDateFrom, @StatusGroupFilter, @AdministratorDatesFilter, @ReferenceFXID, @KnowledgeDateFrom) VFrom ON (VTo.Fund = VFrom.Fund) AND (VTo.SpecificToFundUnitID = VFrom.SpecificToFundUnitID) AND (VTo.Instrument = VFrom.Instrument)
  
  INSERT @tblProfitAndLoss
		(
		Fund, 
		SpecificToFundUnitID, 
		FundCode, 
		InstrumentFundType, 
		FundTypeDescription, 
		FundTypeGroup, 
		FundTypeSortOrder, 
		FundTypeIsAdministrativeOnly, 
		InstrumentType, 
		InstrumentTypeDescription, 
    InstrumentTypeHasDelta, 
		PertracCode, 
		Instrument,
		InstrumentParent,
		InstrumentParentEquivalentRatio,
		InstrumentDescription, 
		InstrumentDealingPeriod, 
		InstrumentContractSize, 
		InstrumentMultiplier, 
		InstrumentIsFXFuture, 
		Value2, 
		Consideration2, 
		FXRate2, 
		FundFXRate2, 
		Value1, 
		Consideration1, 
		FXRate1, 
		Profit,
		StartPrice, 
		StartPriceDate, 
		EndPrice, 
		EndPriceDate, 
		USDValue, 
		InstrumentWorstCase, 
		InstrumentExpectedReturn, 
		StartUnits,
		FinalUnits, 
		AggregatedInstrumentCount
		)
	SELECT 	tblPnL.Fund, 
		tblPnL.SpecificToFundUnitID, 
		tblPnL.FundCode, 
		tblPnL.InstrumentFundType, 
		tblFundType.FundTypeDescription, 
		tblFundType.FundTypeGroup, 
		tblFundType.FundTypeSortOrder, 
		tblFundType.FundTypeIsAdministrativeOnly, 
		tblPnL.InstrumentType, 
		tblInstrumentType.InstrumentTypeDescription, 
    tblInstrumentType.InstrumentTypeHasDelta,
		tblPnL.PertracCode, 
		tblPnL.Instrument,
		tblPnL.InstrumentParent,
		tblPnL.InstrumentParentEquivalentRatio,
		tblPnL.InstrumentDescription,
		tblPnL.InstrumentDealingPeriod,
		tblPnL.InstrumentContractSize,
		tblPnL.InstrumentMultiplier,
		tblPnL.InstrumentIsFXFuture,
		tblPnL.Value2, 
		tblPnL.Consideration2, 
		tblPnL.FXRate2, 
		tblPnL.FundFXRate2, 
		tblPnL.Value1, 
		tblPnL.Consideration1, 
		tblPnL.FXRate1, 
		tblPnL.Profit,
		IsNull(tblPnL.StartPrice, BestPrice.PriceLevel) AS StartPrice, 
		IsNull(tblPnL.StartPriceDate, BestPrice.PriceDate) AS StartPriceDate, 
		IsNull(tblPnL.EndPrice, IsNull(tblPnL.StartPrice, BestPrice.PriceLevel)) AS EndPrice, 
		IsNull(tblPnL.EndPriceDate, IsNull(tblPnL.StartPriceDate, BestPrice.PriceDate)) AS EndPriceDate, 
		tblPnL.USDValue, 
		tblPnL.InstrumentWorstCase,
		tblPnL.InstrumentExpectedReturn, 
		tblPnL.StartUnits,
		tblPnL.FinalUnits, 
		tblPnL.AggregatedInstrumentCount
	FROM ((@Temp_tblProfitAndLoss tblPnL LEFT JOIN 
	fn_tblFundType_SelectKD(@KnowledgeDateLinkedTables) tblFundType ON tblPnL.InstrumentFundType = tblFundType.FundTypeID) LEFT JOIN 
	fn_tblInstrumentType_SelectKD(@KnowledgeDateLinkedTables) tblInstrumentType ON tblPnL.InstrumentType = tblInstrumentType.InstrumentTypeID) LEFT JOIN 
	fn_BestPrice(@ValueDateFrom, Null, @KnowledgeDateTo) BestPrice ON tblPnL.Instrument = BestPrice.InstrumentID
	
   
	RETURN

 END

