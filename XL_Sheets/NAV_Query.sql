DECLARE @ValueDate datetime
DECLARE @KnowledgeDate datetime
DECLARE @FundID int
DECLARE @ThisRN int
DECLARE @LoopTrue bit

DECLARE @tblUnitPrice
TABLE(	Fund int, 
	Value float,
	Units float, 
	PriceType float,
	UnitPrice float,
	FundBaseCurrency int, 
	ValueDate datetime, 
	KnowledgeDate datetime)

SELECT @ThisRN = (-1)
SELECT @LoopTrue = 1
SELECT @FundID = 10

WHILE @LoopTrue = 1
BEGIN
  SELECT @ThisRN = MIN(RN) 
  FROM fn_tblFundMilestones_SelectKD(NULL)
  WHERE (RN > @ThisRN) AND (MilestoneFundID = @FundID)

  IF ISNULL(@ThisRN, -1) < 0
  BEGIN 
    SELECT @LoopTrue = 0
  END
  ELSE
  BEGIN
    SELECT  @ValueDate = MilestoneDate, @KnowledgeDate = MilestoneKnowledgeDate
    FROM fn_tblFundMilestones_SelectKD(NULL)
    WHERE (RN = @ThisRN) AND (MilestoneFundID = @FundID)
    
    INSERT @tblUnitPrice(Fund, Value, Units, PriceType, UnitPrice, FundBaseCurrency, ValueDate, KnowledgeDate)
    SELECT Fund, Value, Units, 1, UnitPrice, FundBaseCurrency, @ValueDate, @KnowledgeDate
    FROM fn_unitprice(@FundID, @ValueDate, 1, 0, @KnowledgeDate)
    
    INSERT @tblUnitPrice(Fund, Value, Units, PriceType, UnitPrice, FundBaseCurrency, ValueDate, KnowledgeDate)
    SELECT Fund, Value, Units, 2, UnitPrice, FundBaseCurrency, @ValueDate, @KnowledgeDate
    FROM fn_unitprice(@FundID, @ValueDate, 2, 0, @KnowledgeDate)
    
    INSERT @tblUnitPrice(Fund, Value, Units, PriceType, UnitPrice, FundBaseCurrency, ValueDate, KnowledgeDate)
    SELECT Fund, Value, Units, 3, UnitPrice, FundBaseCurrency, @ValueDate, @KnowledgeDate
    FROM fn_unitprice(@FundID, @ValueDate, 3, 0, @KnowledgeDate)
    
    INSERT @tblUnitPrice(Fund, Value, Units, PriceType, UnitPrice, FundBaseCurrency, ValueDate, KnowledgeDate)
    SELECT Fund, Value, Units, 4, UnitPrice, FundBaseCurrency, @ValueDate, @KnowledgeDate
    FROM fn_unitprice(@FundID, @ValueDate, 4, 0, @KnowledgeDate)
  END

END

SELECT * FROM @tblUnitPrice

--select * FROM fn_unitprice(@FundID, @ValueDate, 1, 0, @KnowledgeDate)
--select * FROM fn_unitprice(@FundID, @ValueDate, 2, 0, @KnowledgeDate)
--select * FROM fn_unitprice(@FundID, @ValueDate, 3, 0, @KnowledgeDate)
--select * FROM fn_unitprice(@FundID, @ValueDate, 4, 0, @KnowledgeDate)



-- @UnitPriceVariant : 	(0, 4 or Null) 	NAV, including all expenses etc. 
--			(1) GAV		Gross Asset Value Excl all expenses and fees.
--			(2) Basic NAV 	Excluding Mgmt & Incentive fees, but including all other expenses
--			(3) GNAV	NAV, excluding only Performance (Incentive) fees.
-- @DisregardWatermarkPoints	: Non (0 or Null) Disregard Watermark dates for Fee and Expense inclusion.
