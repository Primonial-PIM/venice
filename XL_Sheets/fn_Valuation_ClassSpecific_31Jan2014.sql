USE [Renaissance]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_Valuation_ClassSpecific]    Script Date: 31/01/2014 14:12:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER FUNCTION [dbo].[fn_Valuation_ClassSpecific]
(
	@FundID int,
  @SpecificUnitID int,
	@ValueDate datetime,
	@UnitPriceVariant int,
	@PriceVariantExpenseCutoffDate datetime,
	@PriceVariantManagementFeeCutoffDate datetime,
	@PriceVariantPerformanceFeeCutoffDate datetime,
	@StatusGroupFilter nvarchar(50),
	@AdministratorDatesFilter int,
  @ReferenceFXID as integer,
	@KnowledgeDate datetime

)
	-- ***************************************************
  -- 
	-- ***************************************************
-- Function to provide a valuation, only for Class-specific transactions..

-- @FundID        : Limit the Funds to calculate for. NULL or 0 returns all Funds.
-- @Valuedate     : Valuedate limit to apply to Transactions. NULL or '1900-01-01' returns all rows.
-- @UnitPriceVariant	: Retrieve Transactions consistent with This Unit Price Variant
--			(1) GAV		Gross Asset Value Excl all expenses and fees.
--			(2) Basic NAV 	Excluding Mgmt & Incentive fees, but including all other expenses
--			(3) GNAV	NAV, excluding only Performance (Incentive) fees.
--			(4) NAV		NAV, including all expenses etc. Also Null, 0
--			Note This value passes from fn_UnitPrice (among others ?) . Update that function if these values change.
--
-- @PriceVariantFeeCutoffDate : For Nav variants which exclude fees, this is the date after which they will be excluded
--
-- @AggregateToInstrumentParentID
--			(0 or Null) : No Aggregation to Parent
--			(Non 0)		: Do Aggregate to parent ID
-- @Knowledgedate : Knowledgedate to apply to source tables. Null or '1900-01-01' returns all rows.


RETURNS @tblValuation
	TABLE(	
		RN int IDENTITY (1, 1), 
		rptRN int DEFAULT 0, 
		Fund int, 				--
		SpecificToFundUnitID int, 				--
		Instrument int, 			--
		InstrumentParent int, 			--
		InstrumentParentEquivalentRatio float, 	--
		InstrumentDescription nvarchar(100), 	--
		InstrumentPFPCID nvarchar(100), 	--
		InstrumentType varchar (100), 	--
		InstrumentTypeID int, 			--
		InstrumentFundType int, 		--
		InstrumentContractSize float, -- Instrument Multiplier
		InstrumentMultiplier float, -- Instrument Multiplier
		InstrumentIsFXFuture bit, 
		PertracCode nvarchar(100), 		--
		InstrumentExpectedReturn float, --
		SignedUnits float, 				--
		SignedSettlement float, 		-- In Instrument Currency
		PriceDate datetime, 			-- Date of Valuation Price
		PriceNAV float, 				-- Valuation Price NAV (In Instrument Ccy)
		PriceLevel float, 				-- Valuation Price (In Instrument Ccy)
		PriceMultiplier float, 			-- Price Multiplier
		PriceFinal int, 				-- Is a Final Price ?
		PriceIsAdministrator int,		-- Is an Administrator sourced Price ?
		AvgTransactionPrice float, 		-- Average Transaction Price (In Instrument Ccy)
		FundFXRate float, 				-- FX Rate Fund to USD
		FundFXDate datetime, 			-- Date for applicable FX rate.
		InstrumentFXRate float, 		-- FX Rate for Instrument to USD
		CompoundFXRate float, 			-- FX Rate From Instrument Ccy to Fund Ccy
		Value float, 					-- Value in Fund Currency
		USDValue float,					-- Value in USD
		ReferenceValue float,					-- Value in ReferenceCurrency
		LocalValue float,				-- Value in Instrument Currency
		ConsiderationValue float, 		-- Consideration in Fund Currency
		CurrencyOfInstrument nvarchar(10), 	--
		FundBaseCurrency int,			-- 
		FundCode nvarchar(100), 			--
		InstrumentLimitDisplay bit,		-- 
		InstrumentWorstCase float, 		--
		InstrumentDealingPeriod int, 	--
		InstrumentExcludeGAV int, 		--
		InstrumentIsManagementFee int, 	--
		InstrumentIsIncentiveFee int, 	--
		InstrumentIsEqualisation int, 	--
		InstrumentISIN nvarchar(25), 	--
		InstrumentSEDOL nvarchar(25), 	--
		FirstValueDate datetime, 		--
		LastValueDate datetime, 		--
		SettlementDate datetime,		--
		InstrumentParentID int,			--
		TransactionCompleteness float,	-- 
		TransactionType nvarchar(50) DEFAULT '',		--
		AggregatedInstrumentCount int DEFAULT 0,
		FundClosed int DEFAULT 0
		PRIMARY KEY (Fund, SpecificToFundUnitID, Instrument)
		)
AS
BEGIN

	DECLARE @SubscribeTransactionType int = 2
	DECLARE @RedeemTransactionType int = 3

	DECLARE @ExcludeAllExpenses int = 0								-- InstrumentExcludeGAV
	DECLARE @ExcludeEqualisationInstruments int = 0 	-- InstrumentIsEqualisation
	DECLARE @ExcludeManageMentFees int = 0						-- InstrumentIsManagementFee
	DECLARE @ExcludeIncentiveFees int = 0							-- InstrumentIsIncentiveFee

	DECLARE @ValueDateIsLive int = 0
	DECLARE @LiveReferenceDate datetime = CONVERT(datetime , '1900-01-01')

	SET @FundID = ISNULL(@FundID, 0)
	SET @SpecificUnitID = ISNULL(@SpecificUnitID, 0)
	SET @ValueDate = ISNULL(@ValueDate, '3000-01-01')
	SET @UnitPriceVariant = ISNULL(@UnitPriceVariant, 0)
	SET @PriceVariantExpenseCutoffDate = ISNULL(@PriceVariantExpenseCutoffDate, '3000-01-01')
	SET @PriceVariantManagementFeeCutoffDate = ISNULL(@PriceVariantManagementFeeCutoffDate, '3000-01-01')
	SET @PriceVariantPerformanceFeeCutoffDate = ISNULL(@PriceVariantPerformanceFeeCutoffDate, '3000-01-01')
	SET @StatusGroupFilter = ISNULL(@StatusGroupFilter, '')
	SET @AdministratorDatesFilter = ISNULL(@AdministratorDatesFilter, 0)
	SET @ReferenceFXID = ISNULL(@ReferenceFXID, 3)
	SET @KnowledgeDate = ISNULL(@KnowledgeDate, '1900-01-01')

	IF (@PriceVariantExpenseCutoffDate >= @ValueDate) AND (@PriceVariantManagementFeeCutoffDate >= @ValueDate)  AND (@PriceVariantPerformanceFeeCutoffDate >= @ValueDate)
	BEGIN
		SET @UnitPriceVariant = 0 -- If you do not actually exclude any expenses, then it is a normal NAV.
	END

	IF (@ValueDate <= @LiveReferenceDate)
	BEGIN
		SET @ValueDateIsLive = 1
	END

	-- Declare Variables.

	DECLARE @BestFxs TABLE
		(
		FXCurrencyCode int, 
		FXDate datetime, 
		FXRate float,
		PRIMARY KEY (FXCurrencyCode)
		)

	INSERT @BestFxs(FXCurrencyCode, FXDate, FXRate)
	SELECT FXCurrencyCode, FXDate, FXRate
	FROM fn_BestFX(@ValueDate, @KnowledgeDate)

	DECLARE @ReferenceFXRate float
	SELECT @ReferenceFXRate = MAX(FXRate)
	FROM @BestFxs
	WHERE FXCurrencyCode = @ReferenceFXID

	SET @ReferenceFXRate = ISNULL(@ReferenceFXRate, 1)

	DECLARE @tblAggregatedTransactions
	TABLE(
		Fund int, 
		TransactionSpecificToFundUnitID int, 
		Instrument int, 
		TransactionTypeID int DEFAULT 0, 
		SignedSettlement float, 
		SignedUnits float, 
		Price float, 
		FirstValueDate datetime, 
		LastValueDate datetime, 
		SettlementDate datetime, 
		Completeness float, 
		PRIMARY KEY (Fund, TransactionSpecificToFundUnitID, Instrument))

	DECLARE @ExcludeInstrumentList TABLE
		(InstrumentID int PRIMARY KEY)

	DECLARE @Instruments TABLE
		(InstrumentID int PRIMARY KEY, 
		InstrumentCurrencyID int, 
		InstrumentExcludeGAV int, 
		InstrumentIsManagementFee int, 
		InstrumentIsIncentiveFee int, 
		InstrumentIsEqualisation int, 
		InstrumentIsCrystalisedIncentive bit, 
		InstrumentIsMovementCommission bit)

	DECLARE  @InstrumentIDs [dbo].[IntegerListTableType]

	-- Resolve Transaction Status filter

	DECLARE @SELECT_BY_TRANSACTION_STATUS int = 0
	DECLARE @TransactionStatusList TABLE
		(TradeStatusID int PRIMARY KEY)

	IF (ISNULL(@StatusGroupFilter, '') <> '') 
	BEGIN
		SET @SELECT_BY_TRANSACTION_STATUS = 1

		INSERT @TransactionStatusList(TradeStatusID)
		SELECT DISTINCT [STATUS_ID]
		FROM fn_tblSophisStatusGroups_SelectedGroup(0, @StatusGroupFilter, @KnowledgeDate)
	END

	-- Resolve AdministratorDatesFilter
	-- Not relevant as we do not include Subscriptions / Redemptions in the Valuation.

	-- Resolve various Expense flags

	IF ISNULL(@UnitPriceVariant, 0) = 1		-- GAV
		BEGIN
		SELECT @ExcludeAllExpenses = 1
		SELECT @ExcludeEqualisationInstruments = 1
		SELECT @ExcludeManageMentFees = 1
		SELECT @ExcludeIncentiveFees = 1
		END

	IF ISNULL(@UnitPriceVariant, 0) = 2		-- Basic NAV
		BEGIN
		SELECT @ExcludeEqualisationInstruments = 1
		SELECT @ExcludeManageMentFees = 1
		SELECT @ExcludeIncentiveFees = 1
		END

	IF ISNULL(@UnitPriceVariant, 0) = 3		-- GNAV - Gross NAV - Excl Incentive fees
		BEGIN
		SELECT @ExcludeEqualisationInstruments = 1
		SELECT @ExcludeIncentiveFees = 1
		END
			
  -- Get Instrument Details, required for all NAV types.

	INSERT @Instruments 
		(InstrumentID , 
		InstrumentCurrencyID , 
		InstrumentExcludeGAV , 
		InstrumentIsManagementFee , 
		InstrumentIsIncentiveFee , 
		InstrumentIsEqualisation , 
		InstrumentIsCrystalisedIncentive , 
		InstrumentIsMovementCommission )
	SELECT  
		InstrumentID , 
		InstrumentCurrencyID , 
		InstrumentExcludeGAV , 
		InstrumentIsManagementFee , 
		InstrumentIsIncentiveFee , 
		InstrumentIsEqualisation , 
		InstrumentIsCrystalisedIncentive , 
		InstrumentIsMovementCommission 
	FROM fn_tblInstrument_SelectKD(@KnowledgeDate)

	--
	-- Get Aggregated Transactions.

	IF (ISNULL(@UnitPriceVariant, 0) < 1) OR (ISNULL(@UnitPriceVariant, 0) > 3)
		BEGIN
					INSERT @tblAggregatedTransactions(Fund , TransactionSpecificToFundUnitID, Instrument , SignedSettlement , SignedUnits, Price , FirstValueDate , LastValueDate, SettlementDate, Completeness)
					SELECT TransactionFund, TransactionSpecificToFundUnitID, TransactionInstrument,  Sum(IIF((TransactionLeg = 1) AND (TransactionIsFX = 0), TransactionSignedSettlement / TransactionFXRate, TransactionSignedSettlement)), Sum(TransactionSignedUnits), 
								 Avg(TransactionPrice), Min(TransactionValueDate), Max(TransactionValueDate), 
								 Max(TransactionSettlementDate), 
								 (Cast(Sum(Abs(TransactionFinalDataEntry)) AS float) / Cast(Count(TransactionFinalDataEntry) AS float))
					FROM tblTransaction Transactions
					WHERE RN IN ( SELECT MAX(RN) 
												FROM tblTransaction 
												WHERE (((TransactionDateEntered <= @KnowledgeDate) or (TransactionDateEntered IS NULL)) OR (ISNULL(@KnowledgeDate, '1900-01-01') <= '1900-01-01')) AND 
															(((TransactionDateDeleted > @KnowledgeDate) or (TransactionDateDeleted IS NULL)) AND (NOT ((ISNULL(@KnowledgeDate, '1900-01-01')  <= '1900-01-01') AND (NOT (TransactionDateDeleted is NULL)))))
												GROUP BY TransactionParentID, TransactionLeg )
					AND 
							((TransactionSpecificToFundUnitID > 0) AND ((@SpecificUnitID = 0) OR (TransactionSpecificToFundUnitID = @SpecificUnitID))) AND 
							(NOT ((TransactionType = @SubscribeTransactionType) OR (TransactionType = @RedeemTransactionType))) AND
							((@FundID = 0) OR (TransactionFund = @FundID)) AND
							((@SELECT_BY_TRANSACTION_STATUS = 0) OR (Transactions.TransactionTradeStatusID = 0) OR (Transactions.TransactionTradeStatusID IN (SELECT TradeStatusID FROM @TransactionStatusList))) AND 
							((TransactionValueDate <= @ValueDate) OR (@ValueDateIsLive <> 0))
					GROUP BY TransactionFund, TransactionSpecificToFundUnitID, TransactionInstrument

		END
		ELSE
		BEGIN

					INSERT @tblAggregatedTransactions(Fund , TransactionSpecificToFundUnitID, Instrument , SignedSettlement , SignedUnits, Price , FirstValueDate , LastValueDate, SettlementDate, Completeness)
					SELECT TransactionFund, TransactionSpecificToFundUnitID, TransactionInstrument,  Sum(IIF((TransactionLeg = 1) AND (TransactionIsFX = 0), TransactionSignedSettlement / TransactionFXRate, TransactionSignedSettlement)), Sum(TransactionSignedUnits), 
								 Avg(TransactionPrice), Min(TransactionValueDate), Max(TransactionValueDate), 
								 Max(TransactionSettlementDate), 
								 (Cast(Sum(Abs(TransactionFinalDataEntry)) AS float) / Cast(Count(TransactionFinalDataEntry) AS float))
					FROM	tblTransaction Transactions LEFT JOIN 
								@Instruments Instruments ON (Transactions.TransactionInstrument = Instruments.InstrumentID)
					WHERE RN IN ( SELECT MAX(RN) 
												FROM tblTransaction 
												WHERE (((TransactionDateEntered <= @KnowledgeDate) or (TransactionDateEntered IS NULL)) OR (ISNULL(@KnowledgeDate, '1900-01-01') <= '1900-01-01')) AND 
															(((TransactionDateDeleted > @KnowledgeDate) or (TransactionDateDeleted IS NULL)) AND (NOT ((ISNULL(@KnowledgeDate, '1900-01-01')  <= '1900-01-01') AND (NOT (TransactionDateDeleted is NULL)))))
												GROUP BY TransactionParentID, TransactionLeg )
					AND 
							((TransactionSpecificToFundUnitID > 0) AND ((@SpecificUnitID = 0) OR (TransactionSpecificToFundUnitID = @SpecificUnitID))) AND 
							(NOT ((TransactionType = @SubscribeTransactionType) OR (TransactionType = @RedeemTransactionType))) AND
							((@FundID = 0) OR (TransactionFund = @FundID)) AND
							((@SELECT_BY_TRANSACTION_STATUS = 0) OR (Transactions.TransactionTradeStatusID = 0) OR (Transactions.TransactionTradeStatusID IN (SELECT TradeStatusID FROM @TransactionStatusList))) AND 
							((TransactionValueDate <= @ValueDate) OR (@ValueDateIsLive <> 0)) AND 
							(
								(
									((@ExcludeAllExpenses = 0) OR (Instruments.InstrumentExcludeGAV = 0) OR (TransactionValueDate <= @PriceVariantExpenseCutoffDate)) AND
									((@ExcludeManageMentFees = 0) OR (Instruments.InstrumentIsManagementFee = 0) OR (TransactionValueDate <= @PriceVariantManagementFeeCutoffDate)) AND
									((@ExcludeIncentiveFees = 0) OR (Instruments.InstrumentIsIncentiveFee = 0) OR (TransactionValueDate <= @PriceVariantPerformanceFeeCutoffDate)) 
								)
							)
					GROUP BY TransactionFund, TransactionSpecificToFundUnitID, TransactionInstrument


		END

	IF (@ExcludeEqualisationInstruments <> 0)
	BEGIN
		INSERT @ExcludeInstrumentList(InstrumentID)
		SELECT InstrumentID
		FROM 	@Instruments
		WHERE InstrumentIsEqualisation <> 0

		DELETE FROM @tblAggregatedTransactions
		WHERE Instrument IN 
			(
			SELECT InstrumentID
			FROM @ExcludeInstrumentList
			) 
	END

	--
	-- Aggregated query from Transaction table, calculates totals.
	-- Note the use of Cast(), otherwise Integer arithmetic occurs and you only get return values of 0 or 1.

			
	INSERT @tblValuation
		(
		Fund, 
		SpecificToFundUnitID,
		Instrument, 
		InstrumentParent, 
		InstrumentParentEquivalentRatio, 
		InstrumentDescription, 
		InstrumentPFPCID, 
		InstrumentType, 
		InstrumentTypeID, 
		InstrumentFundType, 
		InstrumentContractSize, 
		InstrumentMultiplier, 
		InstrumentIsFXFuture,
		PertracCode, 
		InstrumentExpectedReturn, 
		SignedUnits, 
		SignedSettlement, 
		PriceDate, 
		PriceNAV, 
		PriceLevel,
		PriceMultiplier, 
		PriceFinal, 
		PriceIsAdministrator, 
		AvgTransactionPrice, 
		FundFXRate, 
		FundFXDate, 
		InstrumentFXRate, 
		CompoundFXRate, 
		Value, 
		USDValue, 
		ReferenceValue,
		LocalValue, 
		ConsiderationValue, 
		CurrencyOfInstrument, 
		FundBaseCurrency, 
		FundCode, 
		InstrumentLimitDisplay,	
		InstrumentWorstCase, 
		InstrumentDealingPeriod, 
		InstrumentExcludeGAV, 
		InstrumentIsManagementFee, 
		InstrumentIsIncentiveFee, 
		InstrumentIsEqualisation, 
		InstrumentISIN, 	--
  		InstrumentSEDOL, 	--
		FirstValueDate, 
		LastValueDate, 
		SettlementDate, 
		InstrumentParentID,	
		TransactionCompleteness,
		TransactionType, 
		AggregatedInstrumentCount,
		FundClosed
		)
	SELECT	
		Transactions.Fund,
		Transactions.TransactionSpecificToFundUnitID,
		Transactions.Instrument, 
		Instruments.InstrumentParent, 
		Instruments.InstrumentParentEquivalentRatio,
		Instruments.InstrumentDescription AS Description, 
		Instruments.InstrumentPFPCID, 
		InstrumentType.InstrumentTypeDescription AS InstrumentType, 
		Instruments.InstrumentType AS InstrumentTypeID, 
		Instruments.InstrumentFundType, 
		Instruments.InstrumentContractSize, 
		Instruments.InstrumentMultiplier, 
		Instruments.InstrumentIsFXFuture, 
		Instruments.InstrumentPertracCode AS PertracCode, 
		Instruments.InstrumentExpectedReturn, 
		Transactions.SignedUnits, 
		Transactions.SignedSettlement, 
		ISNULL(Price.PriceDate, '1900-01-01'), 
		ISNULL(Price.PriceNAV, 0), 
		ISNULL(Price.PriceLevel, 0), 
		ISNULL(Price.PriceMultiplier, 1), 
		ISNULL(Price.PriceFinal, 0), 
		ISNULL(Price.PriceIsAdministrator, 0), 
		Transactions.Price, 
		ISNULL(FundFx.FXRate, 1.0) AS FundFXRate, 
		ISNULL(FundFx.FXDate, '1900-01-01') AS FundFXDate, 
		ISNULL(InstrumentFX.FXRate, 1.0), 
		ISNULL(InstrumentFX.FXRate, 1.0) / ISNULL(FundFx.FXRate, 1.0) AS CompoundFXRate, 				-- FX Rate Instrument -> Book
		SignedUnits * ISNULL(PriceLevel, 0.0) * Instruments.InstrumentContractSize * Instruments.InstrumentMultiplier * (ISNULL(InstrumentFX.FXRate, 1.0)/ISNULL(FundFx.FXRate, 1.0)) AS Value, 	-- Local Value * Compound Rate (Inst -> Fund Ccy)
		SignedUnits * ISNULL(PriceLevel, 0.0) * Instruments.InstrumentContractSize * Instruments.InstrumentMultiplier * ISNULL(InstrumentFX.FXRate, 1.0),						-- USD Value
		SignedUnits * ISNULL(PriceLevel, 0.0) * Instruments.InstrumentContractSize * Instruments.InstrumentMultiplier * (ISNULL(InstrumentFX.FXRate, 1.0) / @ReferenceFXRate),			-- Reference Currency Value
		SignedUnits * ISNULL(PriceLevel, 0.0) * Instruments.InstrumentContractSize * Instruments.InstrumentMultiplier,	-- Local (Instrument) Value
		SignedSettlement * (ISNULL(InstrumentFX.FXRate, 1.0) / ISNULL(FundFx.FXRate, 1.0)) AS ConsiderationValue, -- Consideration Paid (in Book Ccy)
		Instruments.InstrumentCurrency AS CurrencyOfInstrument, 
		Fund.FundBaseCurrency, 
		Fund.FundCode, 
		Instruments.InstrumentLimitDisplay, 
		Instruments.InstrumentWorstCase, 
		Instruments.InstrumentDealingPeriod, 
		Instruments.InstrumentExcludeGAV,
		Instruments.InstrumentIsManagementFee,
		Instruments.InstrumentIsIncentiveFee,
		Instruments.InstrumentIsEqualisation, 
		Instruments.InstrumentISIN, 
		Instruments.InstrumentSEDOL, 
		Transactions.FirstValueDate, 
		Transactions.LastValueDate, 
		Transactions.SettlementDate, 
		IsNull(Instruments.InstrumentParent ,Instruments.InstrumentID) AS ID, 
		Transactions.Completeness,
		T_Type.TransactionType, -- TransactionType
		1,
		Fund.FundClosed
	FROM    
		@tblAggregatedTransactions Transactions LEFT JOIN
		fn_tblTransactionType_SelectKD(@KnowledgeDate) T_Type ON Transactions.TransactionTypeID = T_Type.TransactionTypeID LEFT JOIN 
		fn_tblFund_SelectKD(@KnowledgeDate) Fund ON Transactions.Fund = Fund.FundID LEFT JOIN
		@BestFxs FundFX ON Fund.FundBaseCurrency = FundFX.FXCurrencyCode LEFT JOIN
		fn_tblInstrument_SelectKD(@KnowledgeDate) Instruments ON Transactions.Instrument = Instruments.InstrumentID LEFT JOIN
		fn_BestPriceSelected(@ValueDate, Null, @InstrumentIDs, @KnowledgeDate) Price ON Transactions.Instrument = Price.InstrumentID LEFT JOIN
		fn_tblInstrumentType_SelectKD(@KnowledgeDate) InstrumentType ON Instruments.InstrumentType = InstrumentType.InstrumentTypeID LEFT JOIN
		@BestFxs InstrumentFX ON Instruments.InstrumentCurrencyID = InstrumentFX.FXCurrencyCode

	RETURN
END


